﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.UI;
using View.UI.Feed;
using View.UI.PopUp;
using View.UI.Profile.FriendProfile;
using View.Utility.DataContainer;
using View.Utility.Doing;
using View.Utility.Feed;
using View.Utility.FriendList;
using View.Utility.RingPlayer;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using System.Windows.Controls;

namespace View.Utility.Auth
{
    public class FeedSignalHandler
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(FeedSignalHandler).Name);
        //int LikesSequnceCount = 0;
        //int BreakingSequenceCount = 0;
        #endregion "Fields"

        #region "Signal Handler Methods"


        public void Process_ACTION_MERGED_COMMENTS_LIST_1084(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null)
                {
                    int activityType = (int)_JobjFromResponse[JsonKeys.ActivityType];

                    if (activityType == SettingsConstants.ACTIVITY_ON_STATUS && _JobjFromResponse[JsonKeys.NewsfeedId] != null)
                    {
                        Guid newsfeedId = (Guid)_JobjFromResponse[JsonKeys.NewsfeedId];
                        FeedModel feedModel = null;
                        if (FeedDataContainer.Instance.FeedModels.TryGetValue(newsfeedId, out feedModel))
                        {
                            if (_JobjFromResponse[JsonKeys.Comments] != null)
                            {
                                if (feedModel.CommentList == null) feedModel.CommentList = new ObservableCollection<CommentModel>();
                                JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Comments];
                                foreach (JObject singleObj in jArray)
                                {
                                    HelperMethods.InsertIntoCommentListByAscTime(feedModel.CommentList, singleObj, newsfeedId, Guid.Empty, Guid.Empty, feedModel.PostOwner.UserTableID);
                                }
                                if (feedModel.LikeCommentShare != null && feedModel.CommentList.Count >= feedModel.LikeCommentShare.NumberOfComments)
                                {
                                    feedModel.LikeCommentShare.NumberOfComments = feedModel.CommentList.Count;
                                    feedModel.NoMoreComments = true;
                                }
                            }
                            else if (!(bool)_JobjFromResponse[JsonKeys.Success])
                                feedModel.NoMoreComments = true;
                        }
                    }
                    else if (activityType == SettingsConstants.ACTIVITY_ON_IMAGE && _JobjFromResponse[JsonKeys.ContentId] != null)
                    {
                        Guid imageId = (Guid)_JobjFromResponse[JsonKeys.ContentId];
                        Guid newsfeedId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (Guid)_JobjFromResponse[JsonKeys.NewsfeedId] : Guid.Empty;
                        bool success = (bool)_JobjFromResponse[JsonKeys.Success];
                        long postOwnerId = 0;
                        ObservableCollection<CommentModel> collectionInMediaView = RingIDViewModel.Instance.MediaComments;

                        if (success && ViewConstants.ImageID == imageId && _JobjFromResponse[JsonKeys.Comments] != null)
                        {
                            ImageModel imageModel = null;
                            if (ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(imageId, out imageModel))
                            {
                                postOwnerId = imageModel.UserTableID;
                            }
                            JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Comments];
                            foreach (JObject singleObj in jArray)
                            {
                                HelperMethods.InsertIntoCommentListByDscTime(collectionInMediaView, singleObj, newsfeedId, Guid.Empty, imageId, postOwnerId);
                                System.Threading.Thread.Sleep(10);
                            }
                        }
                        if (_JobjFromResponse[JsonKeys.Comments] != null)
                        {
                            JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Comments];
                            foreach (JObject singleObj in jArray)
                            {
                                if (collectionInMediaView != null)
                                    HelperMethods.InsertIntoCommentListByAscTime(collectionInMediaView, singleObj, newsfeedId, imageId, Guid.Empty, postOwnerId);
                            }
                        }
                    }
                    else if (activityType == SettingsConstants.ACTIVITY_ON_MULTIMEDIA && _JobjFromResponse[JsonKeys.ContentId] != null)
                    {
                        Guid contentId = (Guid)_JobjFromResponse[JsonKeys.ContentId];
                        Guid newsfeedId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (Guid)_JobjFromResponse[JsonKeys.NewsfeedId] : Guid.Empty;
                        bool success = (bool)_JobjFromResponse[JsonKeys.Success];
                        long postOwnerId = 0;
                        ObservableCollection<CommentModel> collectionInMediaView = RingIDViewModel.Instance.MediaComments;
                        if (success && ViewConstants.ContentID == contentId && _JobjFromResponse[JsonKeys.Comments] != null)
                        {
                            SingleMediaModel singleMediaModel = null;
                            if (MediaDataContainer.Instance.ContentModels.TryGetValue(contentId, out singleMediaModel))
                            {
                                postOwnerId = singleMediaModel.MediaOwner.UserTableID;
                            }
                            JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Comments];
                            foreach (JObject singleObj in jArray)
                            {
                                HelperMethods.InsertIntoCommentListByDscTime(collectionInMediaView, singleObj, newsfeedId, Guid.Empty, contentId, postOwnerId);
                                System.Threading.Thread.Sleep(10);
                            }
                        }
                        if (_JobjFromResponse[JsonKeys.Comments] != null)
                        {
                            JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Comments];
                            foreach (JObject singleObj in jArray)
                            {
                                if (collectionInMediaView != null)
                                    HelperMethods.InsertIntoCommentListByAscTime(collectionInMediaView, singleObj, newsfeedId, Guid.Empty, contentId, postOwnerId);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessCommentsForStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_COMMENTS_FOR_STATUS_84(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && _JobjFromResponse[JsonKeys.NewsfeedId] != null)
                {
                    Guid newsfeedId = (Guid)_JobjFromResponse[JsonKeys.NewsfeedId];
                    FeedModel feedModel = null;
                    if (FeedDataContainer.Instance.FeedModels.TryGetValue(newsfeedId, out feedModel))
                    {
                        if (_JobjFromResponse[JsonKeys.Comments] != null)
                        {
                            if (feedModel.CommentList == null) feedModel.CommentList = new ObservableCollection<CommentModel>();
                            JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Comments];
                            foreach (JObject singleObj in jArray)
                            {
                                HelperMethods.InsertIntoCommentListByAscTime(feedModel.CommentList, singleObj, newsfeedId, Guid.Empty, Guid.Empty, feedModel.PostOwner.UserTableID);
                            }
                            if (feedModel.LikeCommentShare != null && feedModel.CommentList.Count >= feedModel.LikeCommentShare.NumberOfComments)
                            {
                                feedModel.LikeCommentShare.NumberOfComments = feedModel.CommentList.Count;
                                feedModel.NoMoreComments = true;
                            }
                        }
                        else if (!(bool)_JobjFromResponse[JsonKeys.Success])
                            feedModel.NoMoreComments = true;
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessCommentsForStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_NEWS_FEED_88(JObject _JobjFromResponse, string client_packet_id)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
                {
                    int scl = (int)_JobjFromResponse[JsonKeys.Scroll];
                    Guid npUUid = (Guid)_JobjFromResponse[JsonKeys.NpUUID];
                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
                    {
                        int feed_state = client_packet_id.Equals(DefaultSettings.ALL_STARTPKT) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
                        foreach (JObject singleObj in jarray)
                        {
                            Guid nfId = (singleObj[JsonKeys.NewsfeedId] != null) ? (Guid)singleObj[JsonKeys.NewsfeedId] : Guid.Empty;
                            FeedModel model = null;
                            if (!FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out model)) { model = new FeedModel(); FeedDataContainer.Instance.FeedModels[nfId] = model; }
                            model.LoadData(singleObj);

                            if (model.FeedCategory != SettingsConstants.SPECIAL_FEED)
                            {
                                FeedDataContainer.Instance.StorageFeedJObjects[nfId] = singleObj;
                                if (scl != 1)
                                {
                                    if (FeedDataContainer.Instance.AllBottomIds.PvtMinGuid == Guid.Empty
                                        //|| HelperMethods.IsLessThanMin(FeedDataContainer.Instance.AllBottomIds.PvtMinGuid, npUUid))
                                    || !FeedDataContainer.Instance.AllRequestedPivotIds.Contains(npUUid))
                                        FeedDataContainer.Instance.AllBottomIds.PvtMinGuid = npUUid;
                                    //log.Error("PVTGUID==> " + npUUid);
                                    if (feed_state != SettingsConstants.FEED_FIRSTTIME && !NewsFeedViewModel.Instance.CurrentFeedIds.Contains(nfId))
                                        FeedDataContainer.Instance.AllBottomIds.InsertIntoSortedList(model.Time, nfId);
                                }
                                else
                                {
                                    FeedDataContainer.Instance.AllTopIds.PvtMaxGuid = npUUid;
                                    //if (feed_state != SettingsConstants.FEED_FIRSTTIME && !NewsFeedViewModel.Instance.CurrentFeedIds.Contains(nfId))
                                    //    FeedDataContainer.Instance.AllTopIds.InsertIntoSortedList(model.Time, nfId);
                                }

                                if (!DefaultSettings.STORAGE_FEEDS_REMOVED)
                                {
                                    NewsFeedViewModel.Instance.ClearAllStorageFeeds();
<<<<<<< HEAD
=======
                                    Thread.Sleep(1000);
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                                }
                                if (feed_state == SettingsConstants.FEED_FIRSTTIME || scl == 1)
                                {
                                    if (!NewsFeedViewModel.Instance.CurrentFeedIds.Contains(nfId))
                                    {
                                        FeedHolderModel holder = new FeedHolderModel(SettingsConstants.FEED_TYPE_NORMAL_FEED);
                                        holder.FeedId = nfId;
                                        holder.Feed = model;
                                        holder.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                        holder.SelectViewTemplate();
                                        NewsFeedViewModel.Instance.CalculateandInsertAllFeeds(holder);
                                    }
                                }
                            }
                            else
                            {
                                FeedHolderModel holder = new FeedHolderModel(SettingsConstants.FEED_TYPE_NORMAL_FEED);
                                holder.FeedId = nfId;
                                holder.Feed = model;
                                holder.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                holder.FeedPanelType = holder.Feed.FeedPanelType;
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    try
                                    {
                                        lock (NewsFeedViewModel.Instance.AllFeedsViewCollection)
                                        {
                                            if (!NewsFeedViewModel.Instance.AllFeedsViewCollection.Any(P => P.FeedId == holder.FeedId))
                                                NewsFeedViewModel.Instance.AllFeedsViewCollection.Insert(1, holder);
                                        }
                                    }
                                    catch (Exception ex) { log.Error("Error: AllFeedsViewCollection ." + ex.Message + "\n" + ex.StackTrace); }
                                }, DispatcherPriority.Send);
                                NewsFeedViewModel.Instance.AllFeedSpecialFeedCount++;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("processNewsFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_LIKES_FOR_STATUS_92(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
                {
                    int activityType = (int)_JobjFromResponse[JsonKeys.ActivityType];

                    if (activityType == SettingsConstants.ACTIVITY_ON_STATUS && _JobjFromResponse[JsonKeys.NewsfeedId] != null)
                    {

                        Guid newsfeedId = (Guid)_JobjFromResponse[JsonKeys.NewsfeedId];
                        JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Likes];
                        if (newsfeedId == ViewConstants.NewsFeedID && jArray != null)
                        {
                            foreach (JObject singleObj in jArray)
                            {
                                long userTableID = (singleObj[JsonKeys.UserTableID] != null) ? (long)singleObj[JsonKeys.UserTableID] : 0;
                                long userIdentity = (singleObj[JsonKeys.UserIdentity] != null) ? (long)singleObj[JsonKeys.UserIdentity] : 0;
                                string fullName = (singleObj[JsonKeys.FullName] != null) ? (string)singleObj[JsonKeys.FullName] : string.Empty;
                                string profileImage = (singleObj[JsonKeys.ProfileImage] != null) ? (string)singleObj[JsonKeys.ProfileImage] : string.Empty;
                                UserBasicInfoModel userModel = RingIDViewModel.Instance.GetUserBasicInfoModelFromServerNotNullable(userTableID, userIdentity, fullName, profileImage);
                                RingIDViewModel.Instance.LikeList.InvokeAddNoDuplicate(userModel);
                            }
                        }

                    }
                    else if (activityType == SettingsConstants.ACTIVITY_ON_IMAGE && _JobjFromResponse[JsonKeys.ContentId] != null)
                    {
                        Guid imageId = (Guid)_JobjFromResponse[JsonKeys.ContentId];
                        JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Likes];
                        if (imageId == ViewConstants.ImageID && jArray != null)
                        {
                            foreach (JObject singleObj in jArray)
                            {
                                long userTableID = (singleObj[JsonKeys.UserTableID] != null) ? (long)singleObj[JsonKeys.UserTableID] : 0;
                                long userIdentity = (singleObj[JsonKeys.UserIdentity] != null) ? (long)singleObj[JsonKeys.UserIdentity] : 0;
                                string fullName = (singleObj[JsonKeys.FullName] != null) ? (string)singleObj[JsonKeys.FullName] : string.Empty;
                                string profileImage = (singleObj[JsonKeys.ProfileImage] != null) ? (string)singleObj[JsonKeys.ProfileImage] : string.Empty;
                                UserBasicInfoModel userModel = RingIDViewModel.Instance.GetUserBasicInfoModelFromServerNotNullable(userTableID, userIdentity, fullName, profileImage);
                                RingIDViewModel.Instance.LikeList.InvokeAddNoDuplicate(userModel);
                            }
                        }
                    }
                    else if (activityType == SettingsConstants.ACTIVITY_ON_MULTIMEDIA && _JobjFromResponse[JsonKeys.ContentId] != null)
                    {

                        Guid contentId = (Guid)_JobjFromResponse[JsonKeys.ContentId];
                        JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Likes];
                        if (contentId == ViewConstants.ContentID && jArray != null)
                        {
                            foreach (JObject singleObj in jArray)
                            {
                                long userTableID = (singleObj[JsonKeys.UserTableID] != null) ? (long)singleObj[JsonKeys.UserTableID] : 0;
                                long userIdentity = (singleObj[JsonKeys.UserIdentity] != null) ? (long)singleObj[JsonKeys.UserIdentity] : 0;
                                string fullName = (singleObj[JsonKeys.FullName] != null) ? (string)singleObj[JsonKeys.FullName] : string.Empty;
                                string profileImage = (singleObj[JsonKeys.ProfileImage] != null) ? (string)singleObj[JsonKeys.ProfileImage] : string.Empty;
                                UserBasicInfoModel userModel = RingIDViewModel.Instance.GetUserBasicInfoModelFromServerNotNullable(userTableID, userIdentity, fullName, profileImage);
                                RingIDViewModel.Instance.LikeList.InvokeAddNoDuplicate(userModel);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("ProcessFetchLikesForStatus ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }

        public void Process_MY_BOOK_94(JObject _JobjFromResponse, string client_packet_id)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
                {
                    int scl = (int)_JobjFromResponse[JsonKeys.Scroll];
                    Guid npUUid = (Guid)_JobjFromResponse[JsonKeys.NpUUID];
                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
                    {
                        int feed_state = client_packet_id.Equals(DefaultSettings.PROFILEMY_STARTPKT) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
                        foreach (JObject singleObj in jarray)
                        {
                            Guid nfId = (singleObj[JsonKeys.NewsfeedId] != null) ? (Guid)singleObj[JsonKeys.NewsfeedId] : Guid.Empty;
                            FeedModel model = null;
                            if (!FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out model)) { model = new FeedModel(); FeedDataContainer.Instance.FeedModels[nfId] = model; }
                            model.LoadData(singleObj);
                            if (scl != 1)
                            {
                                FeedDataContainer.Instance.ProfileMyBottomIds.PvtMinGuid = npUUid;
                                if (feed_state != SettingsConstants.FEED_FIRSTTIME && !FeedDataContainer.Instance.ProfileMyCurrentIds.Contains(nfId))
                                    FeedDataContainer.Instance.ProfileMyBottomIds.InsertIntoSortedList(model.ActualTime, nfId);
                            }
                            else
                            {
                                FeedDataContainer.Instance.ProfileMyTopIds.PvtMaxGuid = npUUid;
                                //if (feed_state != SettingsConstants.FEED_FIRSTTIME) FeedDataContainer.Instance.ProfileMyTopIds.InsertIntoSortedList(model.ActualTime, nfId);
                            }
                            if (feed_state == SettingsConstants.FEED_FIRSTTIME || scl == 1)
                            {
                                if (!FeedDataContainer.Instance.ProfileMyCurrentIds.Contains(nfId))
                                {
                                    FeedHolderModel holder = new FeedHolderModel(2);
                                    holder.FeedId = nfId;
                                    holder.Feed = model;
                                    holder.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                    holder.FeedPanelType = holder.Feed.FeedPanelType;
                                    RingIDViewModel.Instance.ProfileMyCustomFeeds.CalculateandInsertOtherFeeds(holder, FeedDataContainer.Instance.ProfileMyCurrentIds);
                                }
                                //RingIDViewModel.Instance.ProfileMyCustomFeeds.InsertModel(holder);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessMyBook ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_FRIEND_NEWSFEED_110(JObject _JobjFromResponse, string client_packet_id)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
                {
                    int scl = (int)_JobjFromResponse[JsonKeys.Scroll];
                    Guid npUUid = (Guid)_JobjFromResponse[JsonKeys.NpUUID];
                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
                    {
                        long wallOwnerId = (_JobjFromResponse[JsonKeys.WallOwnerId] != null) ? (long)_JobjFromResponse[JsonKeys.WallOwnerId] : 0;
                        int wallOwnerType = (_JobjFromResponse[JsonKeys.WallOwnerType] != null) ? (int)_JobjFromResponse[JsonKeys.WallOwnerType] : 0;
                        int feed_state = 0;
                        CustomSortedList topSortedIds = null, bottomSortedIds = null;
                        CustomFeedCollection collection = null;
                        List<Guid> CurrentIds = null;
                        //System.Windows.Controls.UserControl userControl = null;

                        switch (wallOwnerType)
                        {
                            case SettingsConstants.WALL_OWNER_TYPE_NEWSPORTAL:
                                feed_state = client_packet_id.Equals(DefaultSettings.PROFILENEWSPORTAL_STARTPKT) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
                                bottomSortedIds = FeedDataContainer.Instance.ProfileNewsPortalBottomIds;
                                topSortedIds = FeedDataContainer.Instance.ProfileNewsPortalTopIds;
                                collection = RingIDViewModel.Instance.ProfileNewsPortalCustomFeeds;
                                CurrentIds = FeedDataContainer.Instance.ProfileNewsPortalCurrentIds;
                                //userControl = UCMiddlePanelSwitcher.View_UCNewsPortalProfile;
                                break;
                            case SettingsConstants.WALL_OWNER_TYPE_MEDIA_PAGE:
                                feed_state = client_packet_id.Equals(DefaultSettings.PROFILEMEDIA_STARTPKT) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
                                bottomSortedIds = FeedDataContainer.Instance.ProfileMediaBottomIds;
                                topSortedIds = FeedDataContainer.Instance.ProfileMediaTopIds;
                                collection = RingIDViewModel.Instance.ProfileMediaCustomFeeds;
                                CurrentIds = FeedDataContainer.Instance.ProfileMediaCurrentIds;
                                //userControl = UCMiddlePanelSwitcher.View_UCPageProfile;
                                break;
                            //case SettingsConstants.PROFILE_TYPE_MUSICPAGE:
                            //    feed_state = client_packet_id.Equals(DefaultSettings.START_MEDIAPAGEPROFILEFEEDS_PACKETID) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
                            //    sortedIds = FeedDataContainer.Instance.MediaPageProfileFeedsSortedIds;
                            //    collection = RingIDViewModel.Instance.MediaPageProfileFeeds;
                            //    userControl = UCMiddlePanelSwitcher.View_MediaPageProfile;
                            //    break;
                            //case SettingsConstants.PROFILE_TYPE_CELEBRITY:
                            //    feed_state = client_packet_id.Equals(DefaultSettings.START_CELEBPROFILEFEEDS_PACKETID) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
                            //    sortedIds = FeedDataContainer.Instance.CelebrityProfileFeedsSortedIds;
                            //    collection = RingIDViewModel.Instance.CelebrityProfileFeeds;
                            //    userControl = (UCMiddlePanelSwitcher.View_CelebrityProfile != null) ? UCMiddlePanelSwitcher.View_CelebrityProfile._UCCelebProfileFeeds : null;
                            //    break;
                            default: //0, friendprofile
                                feed_state = client_packet_id.Equals(DefaultSettings.PROFILEPROFILE_STARTPKT) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
                                bottomSortedIds = FeedDataContainer.Instance.ProfileFriendBottomIds;
                                topSortedIds = FeedDataContainer.Instance.ProfileFriendTopIds;
                                collection = RingIDViewModel.Instance.ProfileFriendCustomFeeds;
                                CurrentIds = FeedDataContainer.Instance.ProfileFriendCurrentIds;
                                //userControl = (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null) ? UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendNewsFeeds : null;
                                break;
                        }
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
                        foreach (JObject singleObj in jarray)
                        {
                            Guid nfId = (singleObj[JsonKeys.NewsfeedId] != null) ? (Guid)singleObj[JsonKeys.NewsfeedId] : Guid.Empty;
                            FeedModel model = null;
                            if (!FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out model)) { model = new FeedModel(); FeedDataContainer.Instance.FeedModels[nfId] = model; }
                            model.LoadData(singleObj);
                            if (scl != 1)
                            {
                                bottomSortedIds.PvtMinGuid = npUUid;
                                if (feed_state != SettingsConstants.FEED_FIRSTTIME && !CurrentIds.Contains(nfId))
                                    bottomSortedIds.InsertIntoSortedList(model.ActualTime, nfId);
                            }
                            else
                            {
                                topSortedIds.PvtMaxGuid = npUUid;
                                //if (feed_state != SettingsConstants.FEED_FIRSTTIME) topSortedIds.InsertIntoSortedList(model.ActualTime, nfId);
                            }
                            if (feed_state == SettingsConstants.FEED_FIRSTTIME || scl == 1)
                            {
                                if (!CurrentIds.Contains(nfId))
                                {
                                    FeedHolderModel holder = new FeedHolderModel(2);
                                    holder.FeedId = nfId;
                                    holder.Feed = model;
                                    holder.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                    holder.FeedPanelType = holder.Feed.FeedPanelType;
                                    collection.CalculateandInsertOtherFeeds(holder, CurrentIds);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessFriendNewsFeeds ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_SINGLE_FEED_SHARE_LIST_115(JObject _JobjFromResponse)
        {
            processe_250_115(_JobjFromResponse);
        }

        public void Process_ACTION_MERGED_LIKES_LIST_OF_COMMENT_1116(JObject _JobjFromResponse)
        {
            try
            {
                if ((bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.CommentId] != null)
                {
                    Guid commentId = (Guid)_JobjFromResponse[JsonKeys.CommentId];
                    JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Likes];
                    if (commentId == ViewConstants.CommentID && jArray != null)
                    {
                        foreach (JObject singleObj in jArray)
                        {
                            long userTableID = (singleObj[JsonKeys.UserTableID] != null) ? (long)singleObj[JsonKeys.UserTableID] : 0;
                            long userIdentity = (singleObj[JsonKeys.UserIdentity] != null) ? (long)singleObj[JsonKeys.UserIdentity] : 0;
                            string fullName = (singleObj[JsonKeys.FullName] != null) ? (string)singleObj[JsonKeys.FullName] : string.Empty;
                            string profileImage = (singleObj[JsonKeys.ProfileImage] != null) ? (string)singleObj[JsonKeys.ProfileImage] : string.Empty;
                            UserBasicInfoModel userModel = RingIDViewModel.Instance.GetUserBasicInfoModelFromServerNotNullable(userTableID, userIdentity, fullName, profileImage);
                            RingIDViewModel.Instance.LikeList.InvokeAdd(userModel);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessFetchLikesForStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_LIST_LIKES_OF_COMMENT_116(JObject _JobjFromResponse)
        {
            try
            {
                if ((bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.CommentId] != null)
                {
                    Guid commentId = (Guid)_JobjFromResponse[JsonKeys.CommentId];
                    JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Likes];
                    if (commentId == ViewConstants.CommentID && jArray != null)
                    {
                        foreach (JObject singleObj in jArray)
                        {
                            long userTableID = (singleObj[JsonKeys.UserTableID] != null) ? (long)singleObj[JsonKeys.UserTableID] : 0;
                            long userIdentity = (singleObj[JsonKeys.UserIdentity] != null) ? (long)singleObj[JsonKeys.UserIdentity] : 0;
                            string fullName = (singleObj[JsonKeys.FullName] != null) ? (string)singleObj[JsonKeys.FullName] : string.Empty;
                            string profileImage = (singleObj[JsonKeys.ProfileImage] != null) ? (string)singleObj[JsonKeys.ProfileImage] : string.Empty;
                            UserBasicInfoModel userModel = RingIDViewModel.Instance.GetUserBasicInfoModelFromServerNotNullable(userTableID, userIdentity, fullName, profileImage);
                            RingIDViewModel.Instance.LikeList.InvokeAdd(userModel);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessFetchLikesForStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_MULTIPLE_IMAGE_POST_117(JObject _JobjFromResponse)
        {
            addStatus177_117(_JobjFromResponse);
        }

        public void Process_ADD_STATUS_177(JObject _JobjFromResponse)
        {
            addStatus177_117(_JobjFromResponse);
        }

        public void Process_WHO_SHARES_LIST_249(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.WhoShareList] != null)
                {
                    Guid intNfid = (Guid)_JobjFromResponse[JsonKeys.NewsfeedId];
                    FeedModel intFeedModel = null;
                    if (FeedDataContainer.Instance.FeedModels.TryGetValue(intNfid, out intFeedModel))
                    {
                        if (intFeedModel.WhoShareList == null) intFeedModel.WhoShareList = new ObservableCollection<FeedHolderModel>();
                        List<Guid> extIds = null;
                        if (!FeedDataContainer.Instance.SharedNewsfeedIds.TryGetValue(intNfid, out extIds))
                        { extIds = new List<Guid>(); FeedDataContainer.Instance.SharedNewsfeedIds[intNfid] = extIds; }

                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.WhoShareList];
                        foreach (JObject extFeedObj in jarray)
                        {
                            Guid extFeedNFid = (Guid)extFeedObj[JsonKeys.NewsfeedId];
                            long extUserTableId = (long)extFeedObj[JsonKeys.UserTableID];
                            if (!extIds.Contains(extFeedNFid)) extIds.Add(extFeedNFid);
                            FeedModel extFeedModel = null;
                            BaseUserProfileModel postOwnerOrWallOwner = null;
                            if (extUserTableId > 0)
                                postOwnerOrWallOwner = RingIDViewModel.Instance.GetUserShortInfoModelNotNullable(extUserTableId);
                            else if (extFeedObj[JsonKeys.WallOwner] != null)
                                postOwnerOrWallOwner = new BaseUserProfileModel((JObject)extFeedObj[JsonKeys.WallOwner]);
                            if (FeedDataContainer.Instance.FeedModels.TryGetValue(extFeedNFid, out extFeedModel))
                            {
                                extFeedModel.WallOrContentOwner = extFeedModel.PostOwner = postOwnerOrWallOwner;
                                extFeedModel.LoadLikeCommentShareUpdatedData(extFeedObj);
                            }
                            else
                            {
                                extFeedModel = new FeedModel();
                                extFeedModel.WallOrContentOwner = extFeedModel.PostOwner = postOwnerOrWallOwner;
                                FeedDataContainer.Instance.FeedModels[extFeedNFid] = extFeedModel;
                                extFeedModel.LoadData(extFeedObj);
                            }
                            if (extFeedModel.ParentFeed == null)
                            {
                                extFeedModel.ParentFeed = intFeedModel;
                                //if (extFeedModel.WallOrContentOwner == null)
                                extFeedModel.WallOrContentOwner = intFeedModel.PostOwner;
                            }
                            //if (!intFeedModel.WhoShareList.Any(P => P.NewsfeedId == extFeedModel.NewsfeedId)) intFeedModel.WhoShareList.Add(extFeedModel);
                            if (!intFeedModel.WhoShareList.Any(P => P.FeedId == extFeedModel.NewsfeedId))
                            {
                                FeedHolderModel holder = new FeedHolderModel(2);
                                holder.FeedId = extFeedModel.NewsfeedId;
                                holder.Feed = extFeedModel;
                                holder.FeedPanelType = 0;
                                holder.ShortModel = false;
                                intFeedModel.WhoShareList.InvokeAdd(holder);
                            }
                            intFeedModel.IsShareListVisible = true;
                            if (intFeedModel.LikeCommentShare != null && intFeedModel.WhoShareList.Count < intFeedModel.LikeCommentShare.NumberOfShares) intFeedModel.ShowMoreSharesState = 2;
                            else intFeedModel.ShowMoreSharesState = 0;
                        }
                    }
                }
                else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false)
                {
                    Guid intNfid = (Guid)_JobjFromResponse[JsonKeys.NewsfeedId];
                    FeedModel intFeedModel = null;
                    if (FeedDataContainer.Instance.FeedModels.TryGetValue(intNfid, out intFeedModel))
                    {
                        intFeedModel.ShowMoreSharesState = 0;
                        intFeedModel.IsShareListVisible = false;
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("Exception in ProcessWhoSharesList ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_VIEW_DOING_LIST_273(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.DoingList] != null)
                {
                    JArray array = (JArray)_JobjFromResponse[JsonKeys.DoingList];
                    List<DoingDTO> tmpList = new List<DoingDTO>();
                    foreach (JObject obj in array)
                    {
                        if (obj[JsonKeys.Id] != null && obj[JsonKeys.Name] != null && obj[JsonKeys.Category] != null && obj[JsonKeys.Url] != null)
                        {
                            DoingDTO doing = new DoingDTO();
                            doing.ID = (long)obj[JsonKeys.Id];
                            doing.Name = HelperMethodsModel.FirstLetterToLower((string)obj[JsonKeys.Name]);
                            doing.Category = (int)obj[JsonKeys.Category];
                            doing.ImgUrl = (string)obj[JsonKeys.Url];
                            if (obj[JsonKeys.DoingSort] != null) doing.SortId = (int)obj[JsonKeys.DoingSort];
                            if (obj[JsonKeys.UpdateTime] != null)
                            {
                                doing.UpdateTime = (long)obj[JsonKeys.UpdateTime];
                                if (doing.UpdateTime > DefaultSettings.VALUE_DOING_LIST_UT)
                                {
                                    DefaultSettings.VALUE_DOING_LIST_UT = doing.UpdateTime;
                                    new DatabaseActivityDAO().SaveDoingUpdateTimeIntoDB();
                                }
                            }
                            lock (NewsFeedDictionaries.Instance.DOING_DICTIONARY)
                            {
                                if (NewsFeedDictionaries.Instance.DOING_DICTIONARY.ContainsKey(doing.ID))
                                    NewsFeedDictionaries.Instance.DOING_DICTIONARY[doing.ID] = doing;
                                else
                                    NewsFeedDictionaries.Instance.DOING_DICTIONARY.Add(doing.ID, doing);
                            }
                            tmpList.Add(doing);
                            EnqueueDoingDTOforImageDownload(doing);
                        }
                    }
                    new InsertIntoDoingTable(tmpList);
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessDoingList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_VIEW_TAGGED_LIST_274(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.FriendsTagList] != null)
                {
                    Guid nfid = (Guid)_JobjFromResponse[JsonKeys.NewsfeedId];
                    string str = (string)_JobjFromResponse[JsonKeys.Sequence];
                    char[] delimiterChars = { '/' };
                    str = str.Split(delimiterChars)[1];
                    int seqTotal = Convert.ToInt32(str);
                    JArray array = (JArray)_JobjFromResponse[JsonKeys.FriendsTagList];
                    List<UserBasicInfoDTO> list = new List<UserBasicInfoDTO>();
                    foreach (JObject obj in array)
                    {
                        if (obj[JsonKeys.UserTableID] != null && obj[JsonKeys.Name] != null && obj[JsonKeys.UserTableID] != null && obj[JsonKeys.ProfileImage] != null)
                        {
                            long utid = (long)obj[JsonKeys.UserTableID];
                            UserBasicInfoDTO user = FriendDictionaries.Instance.GetFromBasicInfoDictionaryNotNuallable(utid);
                            user.FullName = (string)obj[JsonKeys.Name];
                            user.UserTableID = (long)obj[JsonKeys.UserTableID];
                            user.ProfileImage = (string)obj[JsonKeys.ProfileImage];
                            list.Add(user);
                        }
                    }
                    ShowFeedTagList(nfid, list);
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessFeedTagList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_HASHTAG_SUGGESTION_280(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaSuggestion] != null)
                {
                    JArray array = (JArray)_JobjFromResponse[JsonKeys.MediaSuggestion];

                    foreach (JObject obj in array)
                    {
                        if (obj[JsonKeys.MediaSearchKey] != null)
                        {
                            HashTagDTO dto = new HashTagDTO();
                            dto.HashTagSearchKey = (string)obj[JsonKeys.MediaSearchKey];

                            lock (MediaDictionaries.Instance.HashTagSuggestions)
                            {
                                MediaDictionaries.Instance.HashTagSuggestions.Add(dto);
                            }
                        }
                    }
                    StartHashTagSuggestionThread();
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessHashtagSuggestion ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_SEARCH_TRENDS_281(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.MediaSuggestion] != null)
                {
                    DefaultSettings.SearchTrendsSeq++;
                    string str = (string)_JobjFromResponse[JsonKeys.Sequence];
                    char[] delimiterChars = { '/' };
                    int seqTotal = Convert.ToInt32(str.Split(delimiterChars)[1]);
                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaSuggestion];
                    foreach (JObject singleObj in jarray)
                    {
                        SearchMediaDTO dto = new SearchMediaDTO();
                        dto.SearchSuggestion = (string)singleObj[JsonKeys.MediaSearchKey];
                        dto.SearchType = (int)singleObj[JsonKeys.MediaSuggestionType];
                        lock (MediaDictionaries.Instance.MEDIA_TRENDS)
                        {
                            MediaDictionaries.Instance.MEDIA_TRENDS.Add(dto);
                        }
                    }
                    if (DefaultSettings.SearchTrendsSeq == seqTotal)
                    {
                        lock (MediaDictionaries.Instance.MEDIA_TRENDS)
                        {
                            MediaDictionaries.Instance.MEDIA_TRENDS.Sort((x, y) => y.SearchSuggestion.Length.CompareTo(x.SearchSuggestion.Length));
                        }
                        DefaultSettings.SearchTrendsSeq = 0;
                        LoadMediaSearchTrends();
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessMediaSearchTrends ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_CELEBRITIES_CATEGORIES_LIST_285(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true
                    && _JobjFromResponse[JsonKeys.CelebrityCatList] != null && _JobjFromResponse[JsonKeys.Sequence] != null)
                {
                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.CelebrityCatList];
                    foreach (JObject singleObj in jarray)
                    {
                        NewsCategoryModel model = new NewsCategoryModel();
                        if (singleObj[JsonKeys.CelebrityCategoryName] != null)
                        {
                            model.CategoryName = (string)singleObj[JsonKeys.CelebrityCategoryName];
                        }
                        if (singleObj[JsonKeys.CelebrityCategoryId] != null)
                        {
                            model.CategoryId = (Guid)singleObj[JsonKeys.CelebrityCategoryId];
                        }
                        Application.Current.Dispatcher.Invoke((Action)(() =>
                        {
                            if (UCCelebrityDiscoverCategoryPopup.Instance != null &&
                                UCCelebrityDiscoverCategoryPopup.Instance.CategoriesofCelebrities.Where(NewsCategoryModel => NewsCategoryModel.CategoryId == model.CategoryId).Any() == false)
                                UCCelebrityDiscoverCategoryPopup.Instance.CategoriesofCelebrities.Add(model);
                        }));
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessNewsPortalCategoriesList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_NEWSPORTAL_CATEGORIES_LIST_294(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.UserTableID] != null
                    && _JobjFromResponse[JsonKeys.NewsPortalCategoryList] != null && _JobjFromResponse[JsonKeys.Sequence] != null)
                {
                    long userTableID = (long)_JobjFromResponse[JsonKeys.UserTableID];
                    List<NewsCategoryModel> newsPortalCategoryList = new List<NewsCategoryModel>();
                    JArray jArray = (JArray)_JobjFromResponse[JsonKeys.NewsPortalCategoryList];
                    foreach (JObject singleObj in jArray)
                    {
                        NewsCategoryModel newsCategoryModel = new NewsCategoryModel();
                        if (singleObj[JsonKeys.NewsPortalCategoryName] != null)
                        {
                            newsCategoryModel.CategoryName = (string)singleObj[JsonKeys.NewsPortalCategoryName];
                        }
                        if (singleObj[JsonKeys.UuID] != null)
                        {
                            newsCategoryModel.CategoryId = (Guid)singleObj[JsonKeys.UuID];
                        }
                        if (singleObj[JsonKeys.Type] != null)
                        {
                            newsCategoryModel.CategoryType = (Guid)singleObj[JsonKeys.Type];
                        }
                        if (singleObj[JsonKeys.Subscribe] != null)
                        {
                            newsCategoryModel.Subscribed = (bool)singleObj[JsonKeys.Subscribe];
                        }
                        newsPortalCategoryList.Add(newsCategoryModel);
                    }
                    LoadNewsPortalCategories(userTableID, newsPortalCategoryList);
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessNewsPortalCategoriesList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_NEWSPORTAL_LIST_299(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true
                    && _JobjFromResponse[JsonKeys.NewsPortalList] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.SubscribeType] != null)// && _JobjFromResponse[JsonKeys.SearchParam] != null && _JobjFromResponse[JsonKeys.Subscribe] != null)
                {
                    int profileType = (_JobjFromResponse[JsonKeys.ProfileType] != null) ? ((int)_JobjFromResponse[JsonKeys.ProfileType]) : SettingsConstants.PROFILE_TYPE_NEWSPORTAL;
                    int subscribeType = (int)_JobjFromResponse[JsonKeys.SubscribeType];
                    JArray jArray = (JArray)_JobjFromResponse[JsonKeys.NewsPortalList];

                    if (profileType == SettingsConstants.PROFILE_TYPE_NEWSPORTAL)
                    {
                        List<NewsPortalModel> list = new List<NewsPortalModel>();
                        foreach (JObject singleObj in jArray)
                        {
                            long pageId = (long)singleObj[JsonKeys.PageId];
                            NewsPortalModel newsPortalModel = null;
                            if (!UserProfilesContainer.Instance.NewsPortalModels.TryGetValue(pageId, out newsPortalModel))
                            {
                                newsPortalModel = new NewsPortalModel { ProfileType = SettingsConstants.PROFILE_TYPE_NEWSPORTAL, UserTableID = pageId };
                                UserProfilesContainer.Instance.NewsPortalModels[pageId] = newsPortalModel;
                            }
                            newsPortalModel.LoadData(singleObj);
                            list.Add(newsPortalModel);
                        }
                        ShowFollowingOrDiscoverPortals(subscribeType, list);
                    }
                    else if (profileType == SettingsConstants.PROFILE_TYPE_PAGES)
                    {
                        List<PageInfoModel> list = new List<PageInfoModel>();
                        foreach (JObject singleObj in jArray)
                        {
                            long pageId = (long)singleObj[JsonKeys.PageId];
                            PageInfoModel pageInfoModel = null;
                            if (!UserProfilesContainer.Instance.PageModels.TryGetValue(pageId, out pageInfoModel))
                            {
                                pageInfoModel = new PageInfoModel { ProfileType = SettingsConstants.PROFILE_TYPE_PAGES, UserTableID = pageId };
                                UserProfilesContainer.Instance.PageModels[pageId] = pageInfoModel;
                            }
                            pageInfoModel.LoadData(singleObj);
                            list.Add(pageInfoModel);
                        }
                        ShowFollowingOrDiscoverPortals(subscribeType, list);
                    }
                    else if (profileType == SettingsConstants.PROFILE_TYPE_MUSICPAGE)
                    {
                        List<MusicPageModel> list = new List<MusicPageModel>();
                        foreach (JObject singleObj in jArray)
                        {
                            long pageId = (long)singleObj[JsonKeys.PageId];
                            MusicPageModel musicPageModel = null;
                            if (!UserProfilesContainer.Instance.MediaPageModels.TryGetValue(pageId, out musicPageModel))
                            {
                                musicPageModel = new MusicPageModel { ProfileType = SettingsConstants.PROFILE_TYPE_MUSICPAGE, UserTableID = pageId };
                                UserProfilesContainer.Instance.MediaPageModels[pageId] = musicPageModel;
                            }
                            musicPageModel.LoadData(singleObj);
                            list.Add(musicPageModel);
                        }
                        ShowFollowingOrDiscoverPortals(subscribeType, list);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessNewsPortalCategoriesList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_ALL_SAVED_FEEDS_309(JObject _JobjFromResponse, string client_packet_id)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
                {
                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
                    {
                        int scl = (int)_JobjFromResponse[JsonKeys.Scroll];
                        //long PvtsavedTime = (long)_JobjFromResponse[JsonKeys.NpUUID];
                        int ptype = (_JobjFromResponse[JsonKeys.ProfileType] != null) ? (int)_JobjFromResponse[JsonKeys.ProfileType] : 0;
                        int feed_state = 0;
                        CustomSortedList topSortedIds = null, bottomSortedIds = null;
                        CustomFeedCollection collection = null;
                        List<Guid> currentIds = null;
                        //bool IsUserControlVisible = true;
                        switch (ptype)
                        {
                            case SettingsConstants.PROFILE_TYPE_NEWSPORTAL:
                                feed_state = client_packet_id.Equals(DefaultSettings.SAVEDNEWSPORTAL_STARTPKT) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
                                topSortedIds = FeedDataContainer.Instance.SavedNewsPortalTopIds;
                                bottomSortedIds = FeedDataContainer.Instance.SavedNewsPortalBottomIds;
                                collection = RingIDViewModel.Instance.SavedNewsPortalCustomFeeds;
                                currentIds = FeedDataContainer.Instance.SavedNewsPortalCurrentIds;
                                //IsUserControlVisible = ((UCMiddlePanelSwitcher.View_UCSavedFeedPanel != null && UCMiddlePanelSwitcher.View_UCSavedFeedPanel.IsVisible && UCMiddlePanelSwitcher.View_UCSavedFeedPanel.TabType == 3)
                                //    || (UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel != null && UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCSavedContentPanel != null && UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCSavedContentPanel.IsVisible));
                                break;
                            //case SettingsConstants.PROFILE_TYPE_PAGES:
                            //    feed_state = client_packet_id.Equals(DefaultSettings.START_SAVEDPAGESFEEDS_PACKETID) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
                            //    sortedIds = FeedDataContainer.Instance.SavedFeedsPageSortedIds;
                            //    collection = RingIDViewModel.Instance.Saved;
                            //IsUserControlVisible = ((UCMiddlePanelSwitcher.View_UCSavedFeedPanel != null && UCMiddlePanelSwitcher.View_UCSavedFeedPanel.IsVisible && UCMiddlePanelSwitcher.View_UCSavedFeedPanel.TabType == 2)
                            //    || (UCMiddlePanelSwitcher.View_UCPagesMainPanel != null && UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCSavedContentPanel != null && UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCSavedContentPanel.IsVisible));
                            //break;
                            case SettingsConstants.PROFILE_TYPE_MUSICPAGE:
                                feed_state = client_packet_id.Equals(DefaultSettings.SAVEDMEDIA_STARTPKT) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
                                topSortedIds = FeedDataContainer.Instance.SavedMediaTopIds;
                                bottomSortedIds = FeedDataContainer.Instance.SavedMediaBottomIds;
                                collection = RingIDViewModel.Instance.SavedMediaCustomFeeds;
                                currentIds = FeedDataContainer.Instance.SavedMediaCurrentIds;
                                //IsUserControlVisible = (UCMiddlePanelSwitcher.View_UCSavedFeedPanel != null && UCMiddlePanelSwitcher.View_UCSavedFeedPanel.IsVisible && UCMiddlePanelSwitcher.View_UCSavedFeedPanel.TabType == 1);
                                break;
                            //case SettingsConstants.PROFILE_TYPE_CELEBRITY:
                            //feed_state = client_packet_id.Equals(DefaultSettings.START_SAVEDCELEBRITYFEEDS_PACKETID) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
                            //sortedIds = FeedDataContainer.Instance.SavedFeedsCelebritySortedIds;
                            //collection = RingIDViewModel.Instance.SavedFeedsCelebrity;
                            //IsUserControlVisible = ((UCMiddlePanelSwitcher.View_UCSavedFeedPanel != null && UCMiddlePanelSwitcher.View_UCSavedFeedPanel.IsVisible && UCMiddlePanelSwitcher.View_UCSavedFeedPanel.TabType == 4)
                            //    || (UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel != null && UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesSavedFeed != null && UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesSavedFeed.IsVisible));
                            //   break;
                            //case SettingsConstants.PROFILE_TYPE_CIRCLE:
                            //    feed_state = client_packet_id.Equals(DefaultSettings.START_SAVEDCIRCLEFEEDS_PACKETID) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
                            //    sortedIds = FeedDataContainer.Instance.SavedFeedsCircleSortedIds;
                            //    collection = RingIDViewModel.Instance.SavedFeedsCircle;
                            //    //IsUserControlVisible = (UCMiddlePanelSwitcher.View_UCCircleInitPanel != null && UCMiddlePanelSwitcher.View_UCCircleInitPanel._UCSavedFeed != null && UCMiddlePanelSwitcher.View_UCCircleInitPanel._UCSavedFeed.IsVisible);
                            //    break;
                            default: //100
                                feed_state = client_packet_id.Equals(DefaultSettings.SAVEDALL_STARTPKT) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
                                topSortedIds = FeedDataContainer.Instance.SavedAllTopIds;
                                bottomSortedIds = FeedDataContainer.Instance.SavedAllBottomIds;
                                collection = RingIDViewModel.Instance.SavedAllCustomFeeds;
                                currentIds = FeedDataContainer.Instance.SavedAllCurrentIds;
                                //IsUserControlVisible = (UCMiddlePanelSwitcher.View_UCSavedFeedPanel != null && UCMiddlePanelSwitcher.View_UCSavedFeedPanel.IsVisible && UCMiddlePanelSwitcher.View_UCSavedFeedPanel.TabType == 0);
                                break;
                        }
                        JArray jArray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
                        foreach (JObject singleObj in jArray)
                        {
                            Guid newsfeedId = (singleObj[JsonKeys.NewsfeedId] != null) ? (Guid)singleObj[JsonKeys.NewsfeedId] : Guid.Empty;
                            FeedModel model = null;
                            if (!FeedDataContainer.Instance.FeedModels.TryGetValue(newsfeedId, out model)) { model = new FeedModel(); FeedDataContainer.Instance.FeedModels[newsfeedId] = model; }
                            model.LoadData(singleObj);
                            if (bottomSortedIds.PvtMinLong == -1 || model.SavedTime < bottomSortedIds.PvtMinLong) bottomSortedIds.PvtMinLong = model.SavedTime;
                            if (topSortedIds.PvtMaxLong == -1 || model.SavedTime > topSortedIds.PvtMaxLong) topSortedIds.PvtMaxLong = model.SavedTime;
                            if (scl != 1)
                            {
                                if (feed_state != SettingsConstants.FEED_FIRSTTIME && !FeedDataContainer.Instance.SavedAllCurrentIds.Contains(newsfeedId))
                                    bottomSortedIds.InsertIntoSortedList(model.SavedTime, newsfeedId);
                            }
                            else
                            {
                                //if (feed_state != SettingsConstants.FEED_FIRSTTIME) topSortedIds.InsertIntoSortedList(model.SavedTime, newsfeedId);
                            }
                            if (feed_state == SettingsConstants.FEED_FIRSTTIME || scl == 1)
                            {
                                if (!FeedDataContainer.Instance.SavedAllCurrentIds.Contains(newsfeedId))
                                {
                                    FeedHolderModel holder = new FeedHolderModel(2);
                                    holder.FeedId = newsfeedId;
                                    holder.Feed = model;
                                    holder.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                    holder.FeedPanelType = holder.Feed.FeedPanelType;
                                    RingIDViewModel.Instance.SavedAllCustomFeeds.CalculateandInsertSavedFeeds(holder, FeedDataContainer.Instance.SavedAllCurrentIds);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessPagesFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_UPDATE_ADD_STATUS_377(JObject _JobjFromResponse)
        {
            addUpdateStatus(_JobjFromResponse, true);
        }

        public void Process_UPDATE_EDIT_STATUS_378(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.NewsFeed] != null)
                {
                    JObject newsFeed = (JObject)_JobjFromResponse[JsonKeys.NewsFeed];
                    Guid nfId = (Guid)newsFeed[JsonKeys.NewsfeedId];
                    FeedModel feedModel = null;
                    if (FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out feedModel))
                    {
                        if (newsFeed[JsonKeys.Status] != null)
                        {
                            feedModel.Status = (string)newsFeed[JsonKeys.Status];
                        }
                        if (newsFeed[JsonKeys.StatusTags] != null)
                        {
                            JArray stsTags = (JArray)newsFeed[JsonKeys.StatusTags];
                            feedModel.StatusTags = new ObservableCollection<TaggedUserModel>();
                            foreach (JObject obj in stsTags)
                            {
                                TaggedUserModel singleTagModel = new TaggedUserModel();
                                if (obj[JsonKeys.UserIdentity] != null) singleTagModel.UserIdentity = (long)obj[JsonKeys.UserIdentity];//not needed eventually
                                if (obj[JsonKeys.UserTableID] != null) singleTagModel.UserTableID = (long)obj[JsonKeys.UserTableID];
                                if (obj[JsonKeys.FullName] != null) singleTagModel.FullName = (string)obj[JsonKeys.FullName];
                                if (obj[JsonKeys.Position] != null) singleTagModel.Index = (int)obj[JsonKeys.Position];
                                if (obj[JsonKeys.ProfileImage] != null) singleTagModel.ProfileImage = (string)obj[JsonKeys.ProfileImage]; //not needed eventually
                                feedModel.StatusTags.InvokeAdd(singleTagModel);
                            }
                            feedModel.OnPropertyChanged("StatusTags");
                        }
                        //outsidetags
                        //if (_JobjFromResponse[JsonKeys.FriendsTagList] != null)
                        //{
                        //    JArray frnsTags = (JArray)_JobjFromResponse[JsonKeys.FriendsTagList];
                        //    feedModel.TaggedFriendsList = new ObservableCollection<UserShortInfoModel>();
                        //    foreach (JObject obj in frnsTags)
                        //    {
                        //        long UserTableID = (long)obj[JsonKeys.UserTableID];
                        //        string ProfileImage = (string)obj[JsonKeys.ProfileImage]; //not needed eventually
                        //        string FullName = (string)obj[JsonKeys.FullName];
                        //        UserShortInfoModel shortModel = RingIDViewModel.Instance.GetUserShortInfoModelFromServerNotNullable(UserTableID, 0, FullName, ProfileImage);
                        //        feedModel.TaggedFriendsList.InvokeAdd(shortModel);
                        //    }
                        //    if (feedModel.TaggedFriendsList != null && feedModel.TaggedFriendsList.Count > 0)
                        //        feedModel.OnPropertyChanged("TaggedFriendsList");
                        //    if (_JobjFromResponse[JsonKeys.TotalTaggedFriends] != null) feedModel.TotalTaggedFriends = (short)_JobjFromResponse[JsonKeys.TotalTaggedFriends];
                        //}
                    }
                    //{
                    //    FeedDTO singleFeed = HelperMethodsModel.BindFeedDetails(_JobjFromResponse);
                    //    string sts = (_JobjFromResponse[JsonKeys.Status] == null) ? null : singleFeed.Status;
                    //    FeedDTO prevFeed;

                    //    lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
                    //    {
                    //        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.TryGetValue(singleFeed.NewsfeedId, out prevFeed);

                    //        if (prevFeed != null)
                    //        {
                    //            prevFeed.Status = singleFeed.Status;
                    //        }

                    //    }
                    //    List<long> taggedIds = null, removedTags = null;
                    //    if (singleFeed.TaggedFriendsList != null && singleFeed.TaggedFriendsList.Count > 0)
                    //    {
                    //        taggedIds = new List<long>();
                    //        foreach (var item in singleFeed.TaggedFriendsList)
                    //        {
                    //            taggedIds.Add(item.UserTableID);
                    //        }
                    //    }
                    //    if (_JobjFromResponse[JsonKeys.RemovedTagsFriendUtIds] != null)
                    //    {
                    //        removedTags = new List<long>();
                    //        JArray array = (JArray)_JobjFromResponse[JsonKeys.RemovedTagsFriendUtIds];
                    //        for (int j = 0; j < array.Count; j++)
                    //        {
                    //            removedTags.Add((long)array.ElementAt(j));
                    //        }
                    //    }
                    //    else removedTags = taggedIds;
                    //    EditNewsFeed(true, singleFeed.NewsfeedId, sts, singleFeed.StatusTags, singleFeed.FriendUserTableId, singleFeed.GroupId, taggedIds, removedTags, singleFeed.locationDTO, singleFeed.Privacy, singleFeed.TaggedFriendsList);
                    //}
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessUpdateEditStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_UPDATE_DELETE_STATUS_379(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.NewsfeedId] != null)
                {
                    Guid nfId = (Guid)_JobjFromResponse[JsonKeys.NewsfeedId];
                    DeleteFeedActions(nfId);
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessUpdateDeleteStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_ACTION_MERGED_UPDATE_ADD_COMMENT_1381(JObject _JobjFromResponse)
        {
            try
            {
                Guid newsfeedId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (Guid)_JobjFromResponse[JsonKeys.NewsfeedId] : Guid.Empty;
                AddOrUpdateAddAnyComment(_JobjFromResponse, newsfeedId, Guid.Empty, Guid.Empty, true);
            }
            catch (Exception e)
            {
                log.Error(" ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_UPDATE_ADD_STATUS_COMMENT_381(JObject _JobjFromResponse)
        {
            try
            {
                Guid newsfeedId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (Guid)_JobjFromResponse[JsonKeys.NewsfeedId] : Guid.Empty;
                AddOrUpdateAddAnyComment(_JobjFromResponse, newsfeedId, Guid.Empty, Guid.Empty, true);
            }
            catch (Exception e)
            {
                log.Error(" ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_ACTION_MERGED_UPDATE_DELETE_COMMENT_1383(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse[JsonKeys.ActivityType] != null)
                {
                    int activityType = (int)_JobjFromResponse[JsonKeys.ActivityType];
                    Guid NfId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (Guid)_JobjFromResponse[JsonKeys.NewsfeedId] : Guid.Empty;
                    Guid CommentId = (_JobjFromResponse[JsonKeys.CommentId] != null) ? (Guid)_JobjFromResponse[JsonKeys.CommentId] : Guid.Empty;
                    Guid ContentId = (_JobjFromResponse[JsonKeys.ContentId] != null) ? (Guid)_JobjFromResponse[JsonKeys.ContentId] : Guid.Empty;

                    if (activityType == SettingsConstants.ACTIVITY_ON_STATUS)
                    {
                        DeleteOrUpdateDeleteAnyComment(_JobjFromResponse, CommentId, NfId, Guid.Empty, Guid.Empty);
                    }
                    if (activityType == SettingsConstants.ACTIVITY_ON_IMAGE)
                    {
                        DeleteOrUpdateDeleteAnyComment(_JobjFromResponse, CommentId, NfId, ContentId, Guid.Empty);
                    }
                    else if (activityType == SettingsConstants.ACTIVITY_ON_MULTIMEDIA)
                    {
                        DeleteOrUpdateDeleteAnyComment(_JobjFromResponse, CommentId, NfId, Guid.Empty, ContentId);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error(" ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_UPDATE_DELETE_STATUS_COMMENT_383(JObject _JobjFromResponse)
        {
            try
            {
                Guid NfId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (Guid)_JobjFromResponse[JsonKeys.NewsfeedId] : Guid.Empty;
                Guid CommentId = (_JobjFromResponse[JsonKeys.CommentId] != null) ? (Guid)_JobjFromResponse[JsonKeys.CommentId] : Guid.Empty;
                DeleteOrUpdateDeleteAnyComment(_JobjFromResponse, CommentId, NfId, Guid.Empty, Guid.Empty);
            }
            catch (Exception e)
            {
                log.Error(" ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_MERGED_UPDATE_LIKE_UNLIKE_1384(JObject _JobjFromResponse)
        {
            HandleLikeUnlikeUpdateFeed(_JobjFromResponse);
        }

        public void Process_UPDATE_LIKE_STATUS_384(JObject _JobjFromResponse)
        {
            HandleLikeUnlikeUpdateFeed(_JobjFromResponse);
        }

        public void Process_UPDATE_UNLIKE_STATUS_386(JObject _JobjFromResponse)
        {
            HandleLikeUnlikeUpdateFeed(_JobjFromResponse);
        }

        public void Process_UPDATE_EDIT_STATUS_COMMENT_389(JObject _JobjFromResponse)
        {
            try
            {
                Guid NfId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (Guid)_JobjFromResponse[JsonKeys.NewsfeedId] : Guid.Empty;
                Guid CommentId = (_JobjFromResponse[JsonKeys.CommentId] != null) ? (Guid)_JobjFromResponse[JsonKeys.CommentId] : Guid.Empty;
                EditOrUpdateEditAnyComment(_JobjFromResponse, CommentId, NfId, Guid.Empty, Guid.Empty);
            }
            catch (Exception e)
            {
                log.Error(" ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_JobjFromResponse"></param>
        public void Process_UPDATE_SHARE_STATUS_391(JObject _JobjFromResponse)// 377
        {
            addUpdateStatus(_JobjFromResponse);
        }

        /// <summary>
        /// {"sucs":true,"cmnId":83658,"tm":1477300767361,"uId":"2110044433","cmn":"sdf","url":"","uType":0,"fn":"Faiz 044433","nfId":892262,"userId":37407,"imgId":0,"auId":37407,"rc":0,"sc":false,"loc":3,"utId":37407,"cntntId":140705,"prIm":"cloud/uploaded-136/2110044433/484421455356790029.jpg","fpvc":3,"drtn":0}
        /// </summary>
        /// <param name="_JobjFromResponse"></param>
        public void Process_UPDATE_COMMENT_MEDIA_465(JObject _JobjFromResponse)
        {
            try
            {
                Guid NfId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (Guid)_JobjFromResponse[JsonKeys.NewsfeedId] : Guid.Empty;
                Guid ContentId = (_JobjFromResponse[JsonKeys.ContentId] != null) ? (Guid)_JobjFromResponse[JsonKeys.ContentId] : Guid.Empty;
                AddOrUpdateAddAnyComment(_JobjFromResponse, NfId, Guid.Empty, ContentId, true);
            }
            catch (Exception e)
            {
                log.Error(" ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_UPDATE_EDITCOMMENT_MEDIA_466(JObject _JobjFromResponse)
        {
            try
            {
                Guid NfId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (Guid)_JobjFromResponse[JsonKeys.NewsfeedId] : Guid.Empty;
                Guid ContentId = (_JobjFromResponse[JsonKeys.ContentId] != null) ? (Guid)_JobjFromResponse[JsonKeys.ContentId] : Guid.Empty;
                Guid CommentId = (_JobjFromResponse[JsonKeys.CommentId] != null) ? (Guid)_JobjFromResponse[JsonKeys.CommentId] : Guid.Empty;
                EditOrUpdateEditAnyComment(_JobjFromResponse, CommentId, NfId, Guid.Empty, ContentId);
            }
            catch (Exception e)
            {
                log.Error(" ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_UPDATE_DELETECOMMENT_MEDIA_467(JObject _JobjFromResponse)
        {
            try
            {
                Guid NfId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (Guid)_JobjFromResponse[JsonKeys.NewsfeedId] : Guid.Empty;
                Guid ContentId = (_JobjFromResponse[JsonKeys.ContentId] != null) ? (Guid)_JobjFromResponse[JsonKeys.ContentId] : Guid.Empty;
                Guid CommentId = (_JobjFromResponse[JsonKeys.CommentId] != null) ? (Guid)_JobjFromResponse[JsonKeys.CommentId] : Guid.Empty;
                DeleteOrUpdateDeleteAnyComment(_JobjFromResponse, CommentId, NfId, Guid.Empty, ContentId);
            }
            catch (Exception e)
            {
                log.Error(" ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_SPAM_REASON_LIST_1001(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Sequence] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
                {
                    string str = (string)_JobjFromResponse[JsonKeys.Sequence];
                    char[] delimiterChars = { '/' };
                    str = str.Split(delimiterChars)[1];
                    int seqTotal = Convert.ToInt32(str);

                    if (_JobjFromResponse[JsonKeys.SpamReasonList] != null)
                    {
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.SpamReasonList];

                        foreach (JObject singleObj in jarray)
                        {
                            int reasonId = 0;
                            string reason = string.Empty;
                            int type = 0;

                            if (singleObj[JsonKeys.SpamType] != null)
                            {
                                type = (int)singleObj[JsonKeys.SpamType];
                            }

                            if (singleObj[JsonKeys.Id] != null)
                            {
                                reasonId = (int)singleObj[JsonKeys.Id];
                            }

                            if (singleObj[JsonKeys.SpamReason] != null)
                            {
                                reason = (string)singleObj[JsonKeys.SpamReason];
                            }

                            ObservableDictionary<int, string> spamListByType = RingIDViewModel.Instance.GetSpamReasonListByType(type);
                            Application.Current.Dispatcher.BeginInvoke(() => { spamListByType[reasonId] = reason; }, DispatcherPriority.Send);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessSpamReasonList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_NEWSFEED_EDIT_HISTORY_LIST_1016(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.NewsfeedId] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.List] != null)
                {
                    Guid nfId = (Guid)_JobjFromResponse[JsonKeys.NewsfeedId];
                    if (_JobjFromResponse[JsonKeys.List] != null && ucEditedHistoryView != null
                        && ucEditedHistoryView.FeedModel != null
                        && ucEditedHistoryView.FeedModel.NewsfeedId == nfId
                        && ucEditedHistoryView.IsVisible)
                    {
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.List];
                        foreach (JObject singleObj in jarray)
                        {
                            EditHistoryModel model = new EditHistoryModel();
                            if (singleObj[JsonKeys.Id] != null)
                            {
                                model.EditedId = (int)singleObj[JsonKeys.Id];
                            }
                            if (singleObj[JsonKeys.EditTime] != null)
                            {
                                model.EditedTime = (long)singleObj[JsonKeys.EditTime];
                            }
                            if (singleObj[JsonKeys.Status] != null)
                            {
                                model.EditedValue = (string)singleObj[JsonKeys.Status];
                            }
                            if (singleObj[JsonKeys.StatusTags] != null)
                            {
                                model.TaggedUsers = new ObservableCollection<TaggedUserModel>();
                                JArray array = (JArray)singleObj[JsonKeys.StatusTags];
                                foreach (JObject obj in array)
                                {
                                    TaggedUserModel singleTagModel = new TaggedUserModel();
                                    if (obj[JsonKeys.UserIdentity] != null) singleTagModel.UserIdentity = (long)obj[JsonKeys.UserIdentity];//not needed eventually
                                    if (obj[JsonKeys.UserTableID] != null) singleTagModel.UserTableID = (long)obj[JsonKeys.UserTableID];
                                    if (obj[JsonKeys.FullName] != null) singleTagModel.FullName = (string)obj[JsonKeys.FullName];
                                    if (obj[JsonKeys.Position] != null) singleTagModel.Index = (int)obj[JsonKeys.Position];
                                    if (obj[JsonKeys.ProfileImage] != null) singleTagModel.ProfileImage = (string)obj[JsonKeys.ProfileImage]; //not needed eventually
                                    model.TaggedUsers.Add(singleTagModel);
                                }
                                model.TaggedUsers = new ObservableCollection<TaggedUserModel>(model.TaggedUsers.OrderByDescending(a => a.Index));
                            }
                            model.IsStatus = true;
                            if (!ucEditedHistoryView.EditHistories.Any(P => P.EditedId == model.EditedId))
                            {
                                model.ShortInfoModel = ucEditedHistoryView.FeedModel.PostOwner;
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    ucEditedHistoryView.EditHistories.Add(model);
                                }, DispatcherPriority.Send);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("Process_COMMENT_EDIT_HISTORY_LIST_1021 ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_COMMENT_EDIT_HISTORY_LIST_1021(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.CommentId] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.List] != null)
                {
                    Guid cmntId = (Guid)_JobjFromResponse[JsonKeys.CommentId];
                    long nfId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
                    if (_JobjFromResponse[JsonKeys.List] != null && ucEditedHistoryView != null
                        && ucEditedHistoryView.CommentModel != null
                        && ucEditedHistoryView.CommentModel.CommentId == cmntId
                        && ucEditedHistoryView.IsVisible)
                    {
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.List];
                        foreach (JObject singleObj in jarray)
                        {
                            EditHistoryModel model = new EditHistoryModel();
                            if (singleObj[JsonKeys.Id] != null)
                            {
                                model.EditedId = (int)singleObj[JsonKeys.Id];
                            }
                            if (singleObj[JsonKeys.EditTime] != null)
                            {
                                model.EditedTime = (long)singleObj[JsonKeys.EditTime];
                            }
                            if (singleObj[JsonKeys.Comment] != null)
                            {
                                model.EditedValue = (string)singleObj[JsonKeys.Comment];
                            }
                            if (singleObj[JsonKeys.CommentTags] != null)
                            {
                                model.TaggedUsers = new ObservableCollection<TaggedUserModel>();
                                JArray array = (JArray)singleObj[JsonKeys.CommentTags];
                                foreach (JObject obj in array)
                                {
                                    TaggedUserModel singleTagModel = new TaggedUserModel();
                                    if (obj[JsonKeys.UserIdentity] != null) singleTagModel.UserIdentity = (long)obj[JsonKeys.UserIdentity];//not needed eventually
                                    if (obj[JsonKeys.UserTableID] != null) singleTagModel.UserTableID = (long)obj[JsonKeys.UserTableID];
                                    if (obj[JsonKeys.FullName] != null) singleTagModel.FullName = (string)obj[JsonKeys.FullName];
                                    if (obj[JsonKeys.Position] != null) singleTagModel.Index = (int)obj[JsonKeys.Position];
                                    if (obj[JsonKeys.ProfileImage] != null) singleTagModel.ProfileImage = (string)obj[JsonKeys.ProfileImage]; //not needed eventually
                                    model.TaggedUsers.Add(singleTagModel);
                                }
                                model.TaggedUsers = new ObservableCollection<TaggedUserModel>(model.TaggedUsers.OrderByDescending(a => a.Index));
                            }
                            model.IsStatus = false;
                            if (!ucEditedHistoryView.EditHistories.Any(P => P.EditedId == model.EditedId))
                            {
                                model.ShortInfoModel = ucEditedHistoryView.CommentModel.UserShortInfoModel;
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                  {
                                      ucEditedHistoryView.EditHistories.Add(model);
                                  }, DispatcherPriority.Send);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("Process_COMMENT_EDIT_HISTORY_LIST_1021 ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        #endregion "Signal Handler methods"

        #region "Utility methods"

        public void HideSpecificFeed(FeedModel model)
        {
            UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, "Successfully Hidden this Feed!");
            Guid nfId = model.NewsfeedId;
            NewsFeedViewModel.Instance.DropNfIdandSeqViewCollection(model.NewsfeedId);
            if (model.WhoShareList != null && model.WhoShareList.Count > 0)
            {
                foreach (var item in model.WhoShareList)
                {
                    NewsFeedViewModel.Instance.DropNfIdandSeqViewCollection(item.FeedId);
                }
            }
            if (model.ParentFeed.WhoShareList.Count > 0)
            {
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    FeedHolderModel prevExtModel = NewsFeedViewModel.Instance.AllFeedsViewCollection.Where(P => (P.Feed != null && P.Feed.ParentFeed != null && P.Feed.ParentFeed.NewsfeedId == model.ParentFeed.NewsfeedId)).FirstOrDefault();
                    if (prevExtModel != null)
                    {
                        //
                        if (model.ParentFeed.WhoShareList.Count > 1)
                        {
                            prevExtModel.FirstSharer = prevExtModel.Feed.ParentFeed.WhoShareList[0].Feed.PostOwner;
                            if (prevExtModel.Feed.ParentFeed.WhoShareList.Count == 2)
                                prevExtModel.SecondSharer = prevExtModel.Feed.ParentFeed.WhoShareList[1].Feed.PostOwner;
                            else
                                prevExtModel.ExtraSharerCount = (prevExtModel.Feed.ParentFeed.LikeCommentShare.NumberOfShares - 1) + " others";
                        }
                        else
                        {
                            prevExtModel.FeedPanelType = 1;
                        }
                        NewsFeedViewModel.Instance.DropModelandSeqViewCollection(prevExtModel);
                    }
                }, DispatcherPriority.Send);
            }
            FeedDataContainer.Instance.AllTopIds.RemovedFromSortedList(nfId);
            FeedDataContainer.Instance.AllBottomIds.RemovedFromSortedList(nfId);
            CustomSortedList sortedIdsTop = null, sortedIdsBottom = null;
            CustomFeedCollection collection = null;
            List<Guid> currentIds = null;
            //
            switch (model.PostOwner.ProfileType)
            {
                case SettingsConstants.PROFILE_TYPE_MUSICPAGE:
                    sortedIdsTop = FeedDataContainer.Instance.AllMediaTopIds;
                    sortedIdsBottom = FeedDataContainer.Instance.AllMediaBottomIds;
                    collection = RingIDViewModel.Instance.AllMediaCustomFeeds;
                    currentIds = FeedDataContainer.Instance.AllMediaCurrentIds;
                    break;
                case SettingsConstants.PROFILE_TYPE_NEWSPORTAL:
                    sortedIdsTop = FeedDataContainer.Instance.AllNewsPortalTopIds;
                    sortedIdsBottom = FeedDataContainer.Instance.AllNewsPortalBottomIds;
                    collection = RingIDViewModel.Instance.AllNewsPortalCustomFeeds;
                    currentIds = FeedDataContainer.Instance.AllNewsPortalCurrentIds;
                    break;
                default:
                    break;
            }
            if (sortedIdsTop != null)
            {
                sortedIdsTop.RemovedFromSortedList(nfId);
            }
            if (sortedIdsBottom != null)
            {
                sortedIdsBottom.RemovedFromSortedList(nfId);
            }
            if (collection != null)
            {
                collection.DropNfId(nfId, currentIds);
            }
            List<Guid> toDeleteExtIds = null;
            if (FeedDataContainer.Instance.SharedNewsfeedIds.TryRemove(nfId, out toDeleteExtIds))
            {
                foreach (var item in toDeleteExtIds)
                {
                    FeedDataContainer.Instance.AllTopIds.RemovedFromSortedList(item);
                    FeedDataContainer.Instance.AllBottomIds.RemovedFromSortedList(item);
                }
            }
        }

        public void HideSpecificCircle(long circleId, CircleModel model, bool Hide)
        {
            UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, "Successfully " + (Hide ? "Hidden" : "Unhidden") + " this Circle Feeds!");
            model.IsProfileHidden = Hide;
            if (Hide)
            {
                //TODO

                //for (int i = RingIDViewModel.Instance.NewsFeeds.Count - 2; i >= 0; i--)
                //{
                //    FeedModel fm = RingIDViewModel.Instance.NewsFeeds[i];
                //    if (fm.PostOwner.ProfileType == SettingsConstants.PROFILE_TYPE_CIRCLE && fm.PostOwner.UserTableID == circleId)
                //    {
                //        FeedDataContainer.Instance.AllFeedsSortedIds.RemovedFromSortedList(fm.NewsfeedId);
                //        Application.Current.Dispatcher.Invoke((Action)delegate
                //        {
                //            RingIDViewModel.Instance.NewsFeeds.Remove(fm);
                //        }, DispatcherPriority.Send);
                //    }
                //}
                //for (int i = RingIDViewModel.Instance.AllCircleFeeds.Count - 2; i >= 0; i--)
                //{
                //    FeedModel fm = RingIDViewModel.Instance.AllCircleFeeds[i];
                //    if (fm.PostOwner.UserTableID == circleId)
                //    {
                //        FeedDataContainer.Instance.AllCircleFeedsSortedIds.RemovedFromSortedList(fm.NewsfeedId);
                //        Application.Current.Dispatcher.Invoke((Action)delegate
                //        {
                //            RingIDViewModel.Instance.AllCircleFeeds.Remove(fm);
                //        }, DispatcherPriority.Send);
                //    }
                //}
            }
        }
        public void HideSpecificProfile(long utIdorPid, BaseUserProfileModel profileModel, bool Hide)
        {
            try
            {
                profileModel.IsProfileHidden = Hide;
                if (Hide)
                {
                    for (int i = NewsFeedViewModel.Instance.AllFeedsViewCollection.Count - 2; i > 0; i--)
                    {
                        FeedHolderModel fm = NewsFeedViewModel.Instance.AllFeedsViewCollection[i];
                        if (fm.Feed != null && fm.Feed.PostOwner != null && fm.Feed.PostOwner.UserTableID == utIdorPid) //fm.Feed != null && fm.Feed.PostOwner != null && 
                        {
                            Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                NewsFeedViewModel.Instance.DropModelandSeqViewCollection(fm);
                            }, DispatcherPriority.Send);
                        }
                    }
                    CustomFeedCollection collection = null;
                    List<Guid> currentIds = null;
                    switch (profileModel.ProfileType)
                    {
                        case SettingsConstants.PROFILE_TYPE_MUSICPAGE:
                            collection = RingIDViewModel.Instance.AllMediaCustomFeeds;
                            currentIds = FeedDataContainer.Instance.AllMediaCurrentIds;
                            break;
                        case SettingsConstants.PROFILE_TYPE_NEWSPORTAL:
                            collection = RingIDViewModel.Instance.AllNewsPortalCustomFeeds;
                            currentIds = FeedDataContainer.Instance.AllNewsPortalCurrentIds;
                            break;
                        default:
                            break;
                    }
                    if (collection != null)
                    {
                        for (int i = collection.Count - 2; i >= 0; i--)
                        {
                            FeedHolderModel fm = collection[i];
                            if (fm.Feed.PostOwner.UserTableID == utIdorPid)
                            {
                                collection.DropModel(fm, currentIds);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
        }

        public void BreakingNewsSliderUI(List<FeedModel> breakingNews, int profileType)
        {
            try
            {
                Application.Current.Dispatcher.Invoke((Action)(() =>
                {
                    switch (profileType)
                    {
                        case SettingsConstants.PROFILE_TYPE_NEWSPORTAL:
                            if (UCMiddlePanelSwitcher.View_UCNewsPortalProfile != null)
                            {
                                foreach (var item in breakingNews)
                                {
                                    RingIDViewModel.Instance.BreakingNewsPortalFeeds.Add(item);
                                    FeedModel feedModel = new FeedModel();
                                    feedModel.NewsModel = new NewsModel();
                                    feedModel.PostOwner = feedModel.WallOrContentOwner = UCMiddlePanelSwitcher.View_UCNewsPortalProfile.NewsPortalModel;
                                    feedModel.NewsfeedId = feedModel.NewsModel.NewsId = item.NewsfeedId;
                                    feedModel.ImageList = new ObservableCollection<ImageModel>();
                                    feedModel.ImageList.Add(new ImageModel { ImageUrl = feedModel.PostOwner.ProfileImage });
                                    feedModel.NewsModel.NewsTitle = item.NewsModel.NewsTitle;
                                    feedModel.NewsModel.NewsShortDescription = item.NewsModel.NewsShortDescription;
                                    RingIDViewModel.Instance.BreakingNewsPortalFeeds.Add(feedModel);
                                }
                                if (UCMiddlePanelSwitcher.View_UCNewsPortalProfile.BreakingNewsPortalFeeds != null
                                    && UCMiddlePanelSwitcher.View_UCNewsPortalProfile.BreakingNewsPortalFeeds.Count > 0)
                                {
                                    //UCMiddlePanelSwitcher.View_UCNewsPortalProfile.IsFirstTime = true;
                                    UCMiddlePanelSwitcher.View_UCNewsPortalProfile.LoadSliderData();
                                }
                            }
                            break;

                        case SettingsConstants.PROFILE_TYPE_PAGES:
                            if (UCMiddlePanelSwitcher.View_UCPagesMainPanel != null && UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCNewsContainerPanel != null)
                            {
                                foreach (var item in breakingNews)
                                {
                                    UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCNewsContainerPanel.BreakingFeeds.Add(item);
                                    FeedModel feedModel = new FeedModel();
                                    feedModel.NewsModel = new NewsModel();
                                    feedModel.NewsfeedId = feedModel.NewsModel.NewsId = item.NewsfeedId;
                                    feedModel.ImageList = new ObservableCollection<ImageModel>();
                                    feedModel.ImageList.Add(new ImageModel { ImageUrl = item.PostOwner.ProfileImage });
                                    feedModel.NewsModel.NewsTitle = item.NewsModel.NewsTitle;
                                    feedModel.NewsModel.NewsShortDescription = item.NewsModel.NewsShortDescription;
                                    UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCNewsContainerPanel.BreakingFeeds.Add(feedModel);
                                }
                                if (!UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCNewsContainerPanel._IsTimerStart
                                    && UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCNewsContainerPanel.BreakingFeeds.Count > 1)
                                {
                                    UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCNewsContainerPanel.LoadSliderData();
                                }
                            }
                            break;
                    }
                }));
            }
            catch (Exception)
            {
            }
        }

        public void StopLoaders(int profileType, int subscType, int state)
        {
            try
            {
                Application.Current.Dispatcher.Invoke((Action)(() =>
                {
                    //if (profileType == SettingsConstants.PROFILE_TYPE_NEWSPORTAL && UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel != null)
                    //{
                    //    if (subscType == SettingsConstants.CHANNEL_TYPE_SEARCH && UCMiddlePanelSwitcher.View_UCNewsPortalSearchPanel != null)
                    //    {
                    //        UCMiddlePanelSwitcher.View_UCNewsPortalSearchPanel.ShowMoreStates(state);
                    //    }
                    //    else if (subscType == SettingsConstants.CHANNEL_TYPE_DISCOVER && UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel != null
                    //        && UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel != null)
                    //    {
                    //        UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel.ShowMoreStates(state);
                    //    }
                    //    else if (subscType == SettingsConstants.CHANNEL_TYPE_FOLLOWING && UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel != null
                    //        && UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCFollowingListPanel != null)
                    //    {
                    //        UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCFollowingListPanel.ShowMoreStates(state);
                    //    }
                    //}
                    //else 
                    if (profileType == SettingsConstants.PROFILE_TYPE_PAGES && UCMiddlePanelSwitcher.View_UCPagesMainPanel != null)
                    {
                        if (subscType == SettingsConstants.CHANNEL_TYPE_SEARCH && UCMiddlePanelSwitcher.View_UCPagesSearchPanel != null)
                        {
                            UCMiddlePanelSwitcher.View_UCPagesSearchPanel.ShowMoreStates(state);
                        }
                        else if (subscType == SettingsConstants.CHANNEL_TYPE_DISCOVER && UCMiddlePanelSwitcher.View_UCPagesMainPanel != null && UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCDiscoverPanel != null)
                        {
                            UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCDiscoverPanel.ShowMoreStates(state);
                        }
                        else if (subscType == SettingsConstants.CHANNEL_TYPE_FOLLOWING && UCMiddlePanelSwitcher.View_UCPagesMainPanel != null && UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCFollowingListPanel != null)
                        {
                            UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCFollowingListPanel.ShowMoreStates(state);
                        }
                    }
                    else if (profileType == SettingsConstants.PROFILE_TYPE_CELEBRITY && UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel != null)
                    {
                        if (subscType == SettingsConstants.CHANNEL_TYPE_SEARCH && UCMiddlePanelSwitcher._UCCelebritiesSearchPanel != null)
                        {
                            UCMiddlePanelSwitcher._UCCelebritiesSearchPanel.ShowMoreStates(state);
                        }
                    }
                }));
            }
            catch (Exception e)
            {
                log.Error("ex in SearchedDiscoverPortals==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void ShowFollowingOrDiscoverPortals(int subscribeType, Object ob)
        {
            try
            {
                Application.Current.Dispatcher.Invoke((Action)(() =>
                {
                    if (ob is List<NewsPortalModel> && UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel != null)
                    {
                        List<NewsPortalModel> list = (List<NewsPortalModel>)ob;
                        if (subscribeType == SettingsConstants.CHANNEL_TYPE_DISCOVER && UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel != null)
                        {
                            foreach (var newsPortalModel in list)
                            {
                                if (!UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel.SearchedNewsPortals.Any(P => P.PortalId == newsPortalModel.PortalId))
                                    UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel.SearchedNewsPortals.Add(newsPortalModel);
                                int index = UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel.SearchedNewsPortals.IndexOf(newsPortalModel);
                                if ((index + 1) % 3 == 0)
                                {
                                    newsPortalModel.IsRightMarginOff = true;
                                }
                                else
                                {
                                    newsPortalModel.IsRightMarginOff = false;
                                }
                            }
                        }
                        else if (subscribeType == SettingsConstants.CHANNEL_TYPE_FOLLOWING && UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCFollowingListPanel != null)
                        {
                            foreach (var newsPortalModel in list)
                            {
                                if (!UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCFollowingListPanel.FollowingNewsPortals.Any(P => P.PortalId == newsPortalModel.PortalId))
                                    UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCFollowingListPanel.FollowingNewsPortals.Add(newsPortalModel);
                            }
                        }
                    }
                    else if (ob is List<PageInfoModel> && UCMiddlePanelSwitcher.View_UCPagesMainPanel != null)
                    {
                        List<PageInfoModel> list = (List<PageInfoModel>)ob;
                        if (subscribeType == SettingsConstants.CHANNEL_TYPE_DISCOVER && UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCDiscoverPanel != null)
                        {
                            foreach (var pageInfoModel in list)
                            {
                                if (!UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCDiscoverPanel.SearchedNewsPages.Any(P => P.PageId == pageInfoModel.PageId))
                                    UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCDiscoverPanel.SearchedNewsPages.Add(pageInfoModel);
                                int index = UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCDiscoverPanel.SearchedNewsPages.IndexOf(pageInfoModel);
                                if ((index + 1) % 3 == 0)
                                {
                                    pageInfoModel.IsRightMarginOff = true;
                                }
                                else
                                {
                                    pageInfoModel.IsRightMarginOff = false;
                                }
                            }
                        }
                        else if (subscribeType == SettingsConstants.CHANNEL_TYPE_FOLLOWING && UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCFollowingListPanel != null)
                        {
                            foreach (var pageInfoModel in list)
                            {
                                if (!UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCFollowingListPanel.FollowingNewsPages.Any(P => P.PageId == pageInfoModel.PageId))
                                    UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCFollowingListPanel.FollowingNewsPages.Add(pageInfoModel);
                            }
                        }
                    }
                    else if (ob is List<MusicPageModel>)
                    {
                        List<MusicPageModel> list = (List<MusicPageModel>)ob;
                        if (subscribeType == SettingsConstants.CHANNEL_TYPE_DISCOVER)
                        {
                            foreach (var musicPageModel in list)
                            {
                                if (!RingIDViewModel.Instance.MusicPagesDiscovered.Any(P => P.MusicPageId == musicPageModel.MusicPageId))
                                {
                                    RingIDViewModel.Instance.MusicPagesDiscovered.Add(musicPageModel);
                                    int index = RingIDViewModel.Instance.MusicPagesDiscovered.IndexOf(musicPageModel);
                                    if ((index + 1) % 3 == 0)
                                    {
                                        musicPageModel.IsRightMarginOff = true;
                                    }
                                    else
                                    {
                                        musicPageModel.IsRightMarginOff = false;
                                    }
                                }
                            }
                        }
                        else if (subscribeType == SettingsConstants.CHANNEL_TYPE_FOLLOWING)
                        {
                            foreach (var musicPageModel in list)
                            {
                                //MusicPageModel model = null;
                                //if (!UserProfilesContainer.Instance.MediaPageModels.TryGetValue(item.MusicPageId, out model))
                                //{
                                //    model = new MusicPageModel();
                                //    UserProfilesContainer.Instance.MediaPageModels[item.MusicPageId] = model;
                                //}
                                //model.LoadData(item);
                                if (!RingIDViewModel.Instance.MusicPagesFollowing.Any(P => P.MusicPageId == musicPageModel.MusicPageId))
                                {
                                    RingIDViewModel.Instance.MusicPagesFollowing.Add(musicPageModel);
                                }
                            }
                        }
                    }
                }));
            }
            catch (Exception e)
            {
                log.Error("ex in SearchedDiscoverPortals==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void DeleteFeedActions(Guid NfId)
        {
            try
            {
                NewsFeedViewModel.Instance.DropNfIdandSeqViewCollection(NfId);
<<<<<<< HEAD
                /*if (RingIDViewModel.Instance.ProfileFriendCustomFeeds.Any(x => x.FeedId == NfId))
                {
                    FeedHolderModel friendFeedHolderModel = RingIDViewModel.Instance.ProfileFriendCustomFeeds.Where(x => x.FeedId == NfId).FirstOrDefault();
                    RingIDViewModel.Instance.ProfileFriendCustomFeeds.InvokeRemove(friendFeedHolderModel);
                }
                else
                {
                    FeedHolderModel myProfileFeedHolderModel = RingIDViewModel.Instance.ProfileMyCustomFeeds.Where(p => p.FeedId == NfId).FirstOrDefault();
                    RingIDViewModel.Instance.ProfileMyCustomFeeds.InvokeRemove(myProfileFeedHolderModel);
=======
                /*if (RingIDViewModel.Instance.ProfileFriendCustomFeeds.Any(x => x.FeedId == NfId))
                {
                    FeedHolderModel friendFeedHolderModel = RingIDViewModel.Instance.ProfileFriendCustomFeeds.Where(x => x.FeedId == NfId).FirstOrDefault();
                    RingIDViewModel.Instance.ProfileFriendCustomFeeds.InvokeRemove(friendFeedHolderModel);
                }
                else
                {
                    FeedHolderModel myProfileFeedHolderModel = RingIDViewModel.Instance.ProfileMyCustomFeeds.Where(p => p.FeedId == NfId).FirstOrDefault();
                    RingIDViewModel.Instance.ProfileMyCustomFeeds.InvokeRemove(myProfileFeedHolderModel);
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                }*/
                RingIDViewModel.Instance.ProfileFriendCustomFeeds.DropNfId(NfId, FeedDataContainer.Instance.ProfileFriendCurrentIds);
                RingIDViewModel.Instance.ProfileMyCustomFeeds.DropNfId(NfId, FeedDataContainer.Instance.ProfileMyCurrentIds);
                FeedModel fm = null;
                if (FeedDataContainer.Instance.FeedModels.TryGetValue(NfId, out fm) && fm.ParentFeed != null && fm.ParentFeed.WhoShareList != null && fm.ParentFeed.WhoShareList.Count > 0)
                {
                    if (fm.WhoShareList != null && fm.WhoShareList.Count > 0)
                    {
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            foreach (var item in fm.WhoShareList)
                            {
                                NewsFeedViewModel.Instance.DropModelandSeqViewCollection(item);
                            }
                        }, DispatcherPriority.Send);
                    }
                    //
                    FeedHolderModel FeedInwhoShareList = fm.ParentFeed.WhoShareList.Where(x => x.FeedId == NfId).FirstOrDefault();
                    if (FeedInwhoShareList != null)
                    {
                        fm.ParentFeed.WhoShareList.InvokeRemove(FeedInwhoShareList);
                        fm.ParentFeed.LikeCommentShare.NumberOfShares--;
                        fm.ParentFeed.LikeCommentShare.IShare = 0;
                    }
                    //
                    if (fm.ParentFeed.WhoShareList.Count > 0)
                    {
                        Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                FeedHolderModel prevExtModel = NewsFeedViewModel.Instance.AllFeedsViewCollection.Where(P => (P.Feed != null && P.Feed.ParentFeed != null && P.Feed.ParentFeed.NewsfeedId == fm.ParentFeed.NewsfeedId)).FirstOrDefault();
                                if (prevExtModel != null)
                                {
                                    //
                                    if (fm.ParentFeed.WhoShareList.Count > 1)
                                    {
                                        prevExtModel.FirstSharer = prevExtModel.Feed.ParentFeed.WhoShareList[0].Feed.PostOwner;
                                        if (prevExtModel.Feed.ParentFeed.WhoShareList.Count == 2)
                                            prevExtModel.SecondSharer = prevExtModel.Feed.ParentFeed.WhoShareList[1].Feed.PostOwner;
                                        else
                                            prevExtModel.ExtraSharerCount = (prevExtModel.Feed.ParentFeed.LikeCommentShare.NumberOfShares - 1) + " others";
                                    }
                                    else
                                    {
                                        prevExtModel.FeedPanelType = 1;
                                    }
                                    NewsFeedViewModel.Instance.DropModelandSeqViewCollection(prevExtModel);
                                }
                            }, DispatcherPriority.Send);
                    }
                }
                if (FeedDataContainer.Instance.UnreadNewsFeedIds.Contains(NfId))
                    FeedDataContainer.Instance.UnreadNewsFeedIds.Remove(NfId);
                List<Guid> toDeleteExtIds = null;
                if (FeedDataContainer.Instance.SharedNewsfeedIds.TryRemove(NfId, out toDeleteExtIds))
                {
                    foreach (var item in toDeleteExtIds)
                    {
                        FeedDataContainer.Instance.AllFeedsSortedIds.RemovedFromSortedList(item);
                        FeedDataContainer.Instance.SavedFeedsAllSortedIds.RemovedFromSortedList(item);
                        FeedDataContainer.Instance.MyFeedsSortedIds.RemovedFromSortedList(item);
                        FeedDataContainer.Instance.FriendProfileFeedsSortedIds.RemovedFromSortedList(item);
                    }
                }

            }
            catch (Exception ex)
            {
                log.Error("Error: DeleteFeedActions() => " + ex.Message + "\n" + ex.StackTrace);
            }

        }

        //public void ShowMultipleImagesFeedDetails(FeedDTO feed)
        //{
        //    try
        //    {
        //        List<ImageModel> ImageListTemp = new List<ImageModel>();
        //        foreach (ImageDTO img in feed.ImageList)
        //        {
        //            ImageModel model = new ImageModel();
        //            model.NewsFeedId = feed.NewsfeedId;
        //            model.UserShortInfoModel = RingIDViewModel.Instance.GetUserShortInfoModelNotNullable(feed.UserTableID, feed.RingID, feed.FullName, feed.ProfileImage);
        //            model.LoadData(img);
        //            ImageListTemp.Add(model);
        //        }
        //        basicImageViewWrapper().ucBasicImageView.AddIntoImageListForNewsFeed(feed.NewsfeedId, ImageListTemp);
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: ShowMultipleImagesFeedDetails() => " + ex.Message + "\n" + ex.StackTrace);
        //    }
        //}

        public void SaveUnsaveFeed(int option, FeedModel model)
        {
            try
            {
                CustomFeedCollection collection = null;
                List<Guid> currentIds = null;
                switch (model.PostOwner.ProfileType)
                {
                    //case SettingsConstants.PROFILE_TYPE_CELEBRITY:
                    //    collection = RingIDViewModel.Instance.SavedFeedsCelebrity;
                    //    break;
                    //case SettingsConstants.PROFILE_TYPE_PAGES:
                    //    collection = RingIDViewModel.Instance.SavedFeedsPage;
                    //    break;
                    case SettingsConstants.PROFILE_TYPE_MUSICPAGE:
                        collection = RingIDViewModel.Instance.SavedMediaCustomFeeds;
                        currentIds = FeedDataContainer.Instance.SavedMediaCurrentIds;
                        break;
                    case SettingsConstants.PROFILE_TYPE_NEWSPORTAL:
                        collection = RingIDViewModel.Instance.SavedNewsPortalCustomFeeds;
                        currentIds = FeedDataContainer.Instance.SavedNewsPortalCurrentIds;
                        break;
                    //case SettingsConstants.PROFILE_TYPE_CIRCLE:
                    //    collection = RingIDViewModel.Instance.SavedFeedsCircle;
                    //    break;
                    default:
                        break;
                }
                switch (option)
                {
                    case 1:
                        model.IsSaved = true;
                        FeedHolderModel holder = new FeedHolderModel(2);
                        holder.FeedId = model.NewsfeedId;
                        holder.Feed = model;

                        holder.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                        holder.FeedPanelType = holder.Feed.FeedPanelType;
                        if (collection != null)
                            collection.InvokeInsertNoDuplicate(0, holder);
                        if (currentIds != null && !currentIds.Contains(holder.FeedId))
                            currentIds.Add(holder.FeedId);

                        RingIDViewModel.Instance.SavedAllCustomFeeds.InvokeInsertNoDuplicate(0, holder);
                        if (!FeedDataContainer.Instance.SavedAllCurrentIds.Contains(holder.FeedId))
<<<<<<< HEAD
                            currentIds.Add(holder.FeedId);
=======
                            FeedDataContainer.Instance.SavedAllCurrentIds.Add(holder.FeedId);
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

                        UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, "Successfully Saved this Feed!");
                        break;
                    case 2:
                        model.IsSaved = false;
                        if (collection != null)
                            collection.DropNfId(model.NewsfeedId, FeedDataContainer.Instance.SavedAllCurrentIds);
                        //RingIDViewModel.Instance.SavedAllCustomFeeds.RemoveModel(model.NewsfeedId, true);

                        UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, "Successfully Unsaved this Feed!");
                        break;
                    default://3
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
        }

        //public void LoadCommentUI()
        //{
        //    if (BasicMediaViewWrapper.ucBasicMediaView != null)
        //    {
        //        RingPlayer.RingPlayerViewModel.Instance.OnStopAndRemoveAnimationRequested();
        //    }
        //}

        public void LoadCommentList(List<CommentDTO> list, Guid newsfeedId, Guid imageId, Guid contentId)
        {
            Application.Current.Dispatcher.Invoke((Action)(() =>
            {
                try
                {
                    newsfeedId = Guid.Empty;
                    ObservableCollection<CommentModel> CommentsINSingleMediaFeedModel = null;
                    ObservableCollection<CommentModel> CommentsINMediaView = null;
                    FeedModel feedModel = null;
                    long postOwnerUtId = 0;
                    if (newsfeedId != Guid.Empty)
                    {
                        FeedDataContainer.Instance.FeedModels.TryGetValue(newsfeedId, out feedModel);
                        if ((list == null || list.Count == 0) && feedModel != null)
                        {
                            feedModel.NoMoreComments = true;
                            //feedModel.OnPropertyChanged("CurrentInstance");
                            //LoadCommentUI();
                            //if (BasicMediaViewWrapper.ucBasicMediaView != null
                            //    && RingPlayer.RingPlayerViewModel.Instance.ClickedMediaID == contentId//BasicMediaViewWrapper.ucBasicMediaView.clickedMediaID == contentId
                            //    && BasicMediaViewWrapper.ucBasicMediaView.ImageComments != null)
                            //{
                            //    BasicMediaViewWrapper.ucBasicMediaView.ImageComments.PreviousCommentsVisibility = Visibility.Collapsed;
                            //}
                            return;
                        }
                        if (feedModel != null)
                        {
                            if (feedModel.SingleMediaFeedModel != null)
                                CommentsINSingleMediaFeedModel = feedModel.CommentList;
                            if (feedModel.PostOwner.UserTableID > 0)
                                postOwnerUtId = feedModel.PostOwner.UserTableID;
                        }
                    }

                    //if (BasicMediaViewWrapper.ucBasicMediaView != null &&
                    //    RingPlayer.RingPlayerViewModel.Instance.ClickedMediaID == contentId)//BasicMediaViewWrapper.ucBasicMediaView.clickedMediaID == contentId)
                    //{
                    //    if (BasicMediaViewWrapper.ucBasicMediaView.ImageComments != null)
                    //    {
                    //        BasicMediaViewWrapper.ucBasicMediaView.ImageComments.ShowWatch(false);
                    //        CommentsINMediaView = BasicMediaViewWrapper.ucBasicMediaView.ImageComments.ImageCommentList;
                    //        BasicMediaViewWrapper.ucBasicMediaView.ImageComments.PreviousCommentsVisibility = (BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.NumberOfComments > CommentsINMediaView.Count) ? Visibility.Visible : Visibility.Collapsed;
                    //    }
                    //    if (RingPlayer.RingPlayerViewModel.Instance.MediaOwnerShortInfo != null && RingPlayer.RingPlayerViewModel.Instance.MediaOwnerShortInfo.UserTableID > 0)
                    //    {
                    //        postOwnerUtId = RingPlayer.RingPlayerViewModel.Instance.MediaOwnerShortInfo.UserTableID;
                    //    }
                    //}
                    if (CommentsINMediaView != null || CommentsINSingleMediaFeedModel != null)
                    {
                        foreach (var comment in list)
                        {
                            //CommentModel commentFeed = null, commentMdaView = null, commentmodel = null;
                            //if (CommentsINSingleMediaFeedModel != null) commentFeed = CommentsINSingleMediaFeedModel.Where(p => p.CommentId == comment.CommentId).FirstOrDefault();
                            //if (CommentsINMediaView != null) commentMdaView = CommentsINMediaView.Where(p => p.CommentId == comment.CommentId).FirstOrDefault();
                            //if (commentFeed == null && commentMdaView == null)
                            //{
                            //    commentmodel = new CommentModel();
                            //    GC.SuppressFinalize(commentmodel);
                            //    if (CommentsINSingleMediaFeedModel != null) CommentsINSingleMediaFeedModel.Add(commentmodel);
                            //    if (CommentsINMediaView != null) CommentsINMediaView.Add(commentmodel);
                            //}
                            //else if (commentFeed != null && commentMdaView == null)
                            //{
                            //    commentmodel = commentFeed;
                            //    if (CommentsINMediaView != null) CommentsINMediaView.Add(commentmodel);
                            //}
                            //else if (commentMdaView != null && commentFeed == null)
                            //{
                            //    commentmodel = commentMdaView;
                            //    if (CommentsINSingleMediaFeedModel != null) CommentsINSingleMediaFeedModel.Add(commentmodel);
                            //}
                            //else
                            //    commentmodel = commentFeed;
                            //commentmodel.ContentId = contentId;
                            //if (nfid > 0) commentmodel.NewsfeedId = nfid;
                            //if (postOwnerUtId > 0) commentmodel.PostOwnerUtId = postOwnerUtId;
                            //commentmodel.IsEditMode = false;
                        }
                        if (CommentsINSingleMediaFeedModel != null)
                        {

                            feedModel.CommentList = new ObservableCollection<CommentModel>(CommentsINSingleMediaFeedModel.OrderBy(a => a.Time));
                            feedModel.OnPropertyChanged("CommentList");
                        }
                        //if (CommentsINMediaView != null) BasicMediaViewWrapper.ucBasicMediaView.ImageComments.ImageCommentList = new ObservableCollection<CommentModel>(CommentsINMediaView.OrderBy(a => a.Time));
                    }
                    //if (BasicMediaViewWrapper.ucBasicMediaView != null
                    //    && RingPlayer.RingPlayerViewModel.Instance.ClickedMediaID == contentId
                    //    && BasicMediaViewWrapper.ucBasicMediaView.ImageComments != null
                    //    && BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage != null
                    //    && CommentsINMediaView != null)
                    //{
                    //    BasicMediaViewWrapper.ucBasicMediaView.ImageComments.PreviousCommentsVisibility = (BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.NumberOfComments > CommentsINMediaView.Count) ? Visibility.Visible : Visibility.Collapsed;
                    //}
                    //if (feedModel != null) feedModel.OnPropertyChanged("CurrentInstance");
                }
                catch (Exception ex)
                {
                    log.Error("Error: LoadCommentList() => " + ex.Message + "\n" + ex.StackTrace);
                }
            }));
        }

        public void LoadFeedComments(List<CommentDTO> list, Guid newsfeedId, long friendId, long circleId)
        {
            Application.Current.Dispatcher.Invoke((Action)(() =>
            {
                try
                {
                    FeedModel feedModel = null;
                    FeedDataContainer.Instance.FeedModels.TryGetValue(newsfeedId, out feedModel);
                    if (feedModel != null)
                    {
                        if (list != null && newsfeedId != Guid.Empty)
                        {
                            foreach (var comment in list)
                            {
                                //lock (feedModel.CommentList)
                                //{
                                //    CommentModel commentModel = feedModel.CommentList.Where(P => P.CommentId == comment.CommentId).FirstOrDefault();
                                //    if (commentModel == null)
                                //    {
                                //        commentModel = new CommentModel();
                                //        commentModel.NewsfeedId = nfId;
                                //        if (feedModel.UserShortInfoModel != null) commentModel.PostOwnerUtId = feedModel.UserShortInfoModel.UserTableID;
                                //        feedModel.CommentList.Insert(0, commentModel);
                                //    }
                                //}
                            }
                            if (feedModel.CommentList.Count > 0)
                            {
                                feedModel.CommentList = new ObservableCollection<CommentModel>(feedModel.CommentList.OrderBy(a => a.Time));
                            }
                        }
                        else feedModel.NoMoreComments = true;
                        //feedModel.OnPropertyChanged("CurrentInstance");
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: LoadFeedComments() => " + ex.Message + "\n" + ex.StackTrace);
                }

            }));
        }

        public void OnEditFeedFailed(string msg)
        {
            Application.Current.Dispatcher.Invoke((Action)(() =>
                {
                    try
                    {
                        UIHelperMethods.ShowWarning(msg, "Edit feed failed");
                        // CustomMessageBox.ShowWarning(msg);
                        if (ucAddLocationView != null)
                        {
                            ucAddLocationView.EnableButtonsandLoader(true);
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("Error: OnEditFeedFailed() => " + ex.Message + "\n" + ex.StackTrace);
                    }
                }));
        }

        //public void EditNewsFeed(bool suc, Guid nfid, string sts, List<StatusTagDTO> tagLst, long friendIdentity, long CircleId, List<long> AddedTags, List<long> RemovedTags, LocationDTO locationDTO, int pvc, List<UserBasicInfoDTO> users = null)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //        {
        //            try
        //            {
        //                if (DownloadOrAddToAlbumPopUpWrapper.ucAddLocationView != null)
        //                {
        //                    DownloadOrAddToAlbumPopUpWrapper.ucAddLocationView.EnableButtonsandLoader(true);
        //                    DownloadOrAddToAlbumPopUpWrapper.ucAddLocationView.Hide();
        //                }
        //                FeedModel fm = null;
        //                FeedDataContainer.Instance.FeedModels.TryGetValue(nfid, out fm);
        //                FeedDTO dto = null;
        //                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.TryGetValue(nfid, out dto);
        //                if (dto != null && suc)
        //                {
        //                    if (sts != null) dto.Status = sts;
        //                    if (locationDTO != null) dto.locationDTO = locationDTO;
        //                    if (tagLst != null) dto.StatusTags = tagLst;
        //                }
        //                if (fm != null)
        //                {
        //                    if (suc)
        //                    {
        //                        if (sts != null) fm.Status = sts;
        //                        if (locationDTO != null)
        //                        {
        //                            if (fm.locationModel == null) fm.locationModel = new LocationModel();
        //                            if (fm.ParentFeed != null || (fm.ImageList != null && fm.ImageList.Count > 0) || fm.MediaContent != null
        //                                || !string.IsNullOrEmpty(fm.PreviewUrl)) { }
        //                            else fm.FeedPanelType = 9;
        //                            bool MapOff = (!string.IsNullOrEmpty(fm.PreviewUrl) || (fm.BookPostType == SettingsConstants.FEED_TYPE_SINGLE_IMAGE || fm.BookPostType == SettingsConstants.FEED_TYPE_SINGLE_IMAGE_WITH_ALBUM || fm.BookPostType == SettingsConstants.FEED_TYPE_MULTIPLE_IMAGE_WITH_ALBUM) || fm.ParentFeed != null);
        //                            fm.locationModel.LoadData(locationDTO, MapOff);
        //                            fm.OnPropertyChanged("locationModel");
        //                        }
        //                        if (tagLst != null)
        //                        {
        //                            if (fm.StatusTags == null) fm.StatusTags = new ObservableCollection<TaggedUserModel>();
        //                            else fm.StatusTags.Clear();
        //                            foreach (var item in tagLst)
        //                            {
        //                                TaggedUserModel statusTagModel = new TaggedUserModel();
        //                                statusTagModel.LoadData(item);
        //                                fm.StatusTags.Add(statusTagModel);
        //                            }
        //                            fm.OnPropertyChanged("StatusTags");
        //                        }
        //                        if (fm.TaggedFriendsList != null && RemovedTags != null && RemovedTags.Count > 0)
        //                        {
        //                            foreach (long removedUtid in RemovedTags)
        //                            {
        //                                UserShortInfoModel model = fm.TaggedFriendsList.Where(P => P.UserTableID == removedUtid).FirstOrDefault();
        //                                if (model != null)
        //                                {
        //                                    fm.TaggedFriendsList.Remove(model);
        //                                    fm.TotalTaggedFriends = (short)(fm.TotalTaggedFriends - 1);
        //                                    UserBasicInfoDTO user = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(model.UserTableID);
        //                                    FeedDTO feed = null;
        //                                    if (user != null && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.TryGetValue(nfid, out feed))
        //                                    {
        //                                        if (feed.TaggedFriendsList != null && feed.TaggedFriendsList.Any(P => P.UserTableID == model.UserTableID))
        //                                        {
        //                                            feed.TaggedFriendsList.Remove(user);
        //                                            feed.TotalTaggedFriends = (short)(feed.TotalTaggedFriends - 1);
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        if (AddedTags != null && AddedTags.Count > 0)
        //                        {
        //                            if (fm.TaggedFriendsList == null)
        //                                fm.TaggedFriendsList = new ObservableCollection<UserShortInfoModel>();
        //                            foreach (long addedUtid in AddedTags)
        //                            {
        //                                UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(addedUtid);
        //                                if (model == null && users != null && users.Count > 0)
        //                                {
        //                                    UserBasicInfoDTO udto = users.Where(P => P.UserTableID == addedUtid).FirstOrDefault();
        //                                    if (udto != null) model = new UserBasicInfoModel(udto);
        //                                }
        //                                if (model != null && !fm.TaggedFriendsList.Any(P => P.UserTableID == model.ShortInfoModel.UserTableID))
        //                                {
        //                                    fm.TaggedFriendsList.Add(model.ShortInfoModel);
        //                                    fm.TotalTaggedFriends = (short)(fm.TotalTaggedFriends + 1);
        //                                    UserBasicInfoDTO user = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(model.ShortInfoModel.UserTableID);
        //                                    FeedDTO feed = null;
        //                                    if (user != null && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.TryGetValue(nfid, out feed))
        //                                    {
        //                                        if (feed.TaggedFriendsList == null) feed.TaggedFriendsList = new List<UserBasicInfoDTO>();
        //                                        feed.TaggedFriendsList.Add(user);
        //                                        feed.TotalTaggedFriends = (short)(feed.TotalTaggedFriends + 1);
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        fm.OnPropertyChanged("CurrentInstance");
        //                        fm.OnPropertyChanged("TaggedFriendsList");
        //                        fm.IsEditMode = false;
        //                    }
        //                    else
        //                    {
        //                        MessageBoxResult result = CustomMessageBox.ShowWarning("Failed to Edit the Feed!");
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                log.Error("Error: EditNewsFeed() => " + ex.Message + "\n" + ex.StackTrace);
        //            }
        //        }));
        //}

        public void EnablePostBtn(int type, long frndTableId_or_circleId, bool UploadSuccessful)
        {
            Application.Current.Dispatcher.Invoke((Action)(() =>
                {
                    try
                    {
                        if (!UploadSuccessful && DefaultSettings.IsInternetAvailable)
                            UIHelperMethods.ShowFailed(NotificationMessages.FAILED_ADD_FEED_MESSAGE, NotificationMessages.ADD_FEED_MESSAGE_TITLE);
                        //  CustomMessageBox.ShowError("Failed to Add Feed! ");
                        switch (type)
                        {
                            case 0:
                                if (UCAllFeeds.Instance != null && UCAllFeeds.Instance.newStatusViewModel != null)
                                {
                                    UCAllFeeds.Instance.newStatusViewModel.EnablePostbutton(true);
                                    if (UploadSuccessful)
                                    {
                                        UCAllFeeds.Instance.newStatusViewModel.ActionRemoveLinkPreview();
                                        UCAllFeeds.Instance.newStatusViewModel.ActionRemoveLocationFeelingTagStatusImagesMedia();
                                        if (WNVideoRecorderPreviewPanel.Instance != null) WNVideoRecorderPreviewPanel.Instance.DeleteFilesInDirectory();
                                    }
                                }
                                break;
                            case 1:
                                if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyNewsFeeds != null)
                                {
                                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyNewsFeeds.NewStatusViewModel.EnablePostbutton(true);
                                    if (UploadSuccessful)
                                    {
                                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyNewsFeeds.NewStatusViewModel.ActionRemoveLinkPreview();
                                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyNewsFeeds.NewStatusViewModel.ActionRemoveLocationFeelingTagStatusImagesMedia();
                                        if (WNVideoRecorderPreviewPanel.Instance != null) WNVideoRecorderPreviewPanel.Instance.DeleteFilesInDirectory();
                                    }
                                }
                                break;
                            case 2:
                                if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == frndTableId_or_circleId)
                                {
                                    UCFriendProfile ucFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                                    ucFriendProfile._UCFriendNewsFeeds.NewStatusViewModel.EnablePostbutton(true);
                                    if (UploadSuccessful)
                                    {
                                        ucFriendProfile._UCFriendNewsFeeds.NewStatusViewModel.ActionRemoveLinkPreview();
                                        ucFriendProfile._UCFriendNewsFeeds.NewStatusViewModel.ActionRemoveLocationFeelingTagStatusImagesMedia();
                                        if (WNVideoRecorderPreviewPanel.Instance != null) WNVideoRecorderPreviewPanel.Instance.DeleteFilesInDirectory();
                                    }
                                }
                                break;
                            case 3:
                                UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.NewStatusViewModel.EnablePostbutton(true);
                                if (UploadSuccessful)
                                {
                                    UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.NewStatusViewModel.ActionRemoveLinkPreview();
                                    UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.NewStatusViewModel.ActionRemoveLocationFeelingTagStatusImagesMedia();
                                    if (WNVideoRecorderPreviewPanel.Instance != null) WNVideoRecorderPreviewPanel.Instance.DeleteFilesInDirectory();
                                }
                                break;
                            case 4:
                                if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null && MediaCloudUploadPopup != null
                                    && MediaCloudUploadPopup.NewStatusView != null)
                                {
                                    MediaCloudUploadPopup.NewStatusViewModel.EnablePostbutton(true);
                                    if (MediaCloudUploadPopup.NewStatusViewModel.ucMediaCloudUploadPopup != null)
                                        MediaCloudUploadPopup.NewStatusViewModel.ucMediaCloudUploadPopup.LoadStatesInUI((UploadSuccessful) ? 0 : 1);
                                    if (UploadSuccessful)
                                    {
                                        MediaCloudUploadPopup.NewStatusViewModel.ActionRemoveLinkPreview();
                                        MediaCloudUploadPopup.NewStatusViewModel.ActionRemoveLocationFeelingTagStatusImagesMedia();
                                        if (WNVideoRecorderPreviewPanel.Instance != null) WNVideoRecorderPreviewPanel.Instance.DeleteFilesInDirectory();
                                    }
                                }
                                break;
                            case 5:
                                if (UCShareSingleFeedView.Instance != null && UCShareSingleFeedView.Instance.Visibility == Visibility.Visible)
                                {
                                    UCShareSingleFeedView.Instance.Hide();
                                    UCShareSingleFeedView.Instance.HideShareView();
                                    UCShareSingleFeedView.Instance.EnableButtonsandLoader(true);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("Error: EnablePostBtn() => " + ex.Message + "\n" + ex.StackTrace);
                    }
                }));
        }

        public void ShowFeedTagList(Guid nfid, List<UserBasicInfoDTO> list)
        {
            try
            {
                FeedModel feedModel = null;
                if (FeedDataContainer.Instance.FeedModels.TryGetValue(nfid, out feedModel) && feedModel.FirstTagProfile != null)
                {
                    long firstTagUtid = feedModel.FirstTagProfile.UserTableID;
                    if (UCFeedTagListView.Instance != null && UCFeedTagListView.Instance.nfid == nfid &&
                        UCFeedTagListView.Instance.Visibility == Visibility.Visible)
                    {
                        foreach (var item in list)
                        {
                            if (item.UserTableID != firstTagUtid)
                            {
                                UserShortInfoModel model = new UserShortInfoModel { UserTableID = item.UserTableID, UserIdentity = item.RingID, FullName = item.FullName, ProfileType = SettingsConstants.PROFILE_TYPE_GENERAL };
                                UCFeedTagListView.Instance.TagList.InvokeAddNoDuplicate(model);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowFeedTagList() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void EnqueueDoingDTOforImageDownload(DoingDTO dto = null)
        {
            Application.Current.Dispatcher.Invoke((Action)(() =>
            {
                try
                {
                    if (DoingImageLoadUtility.Instance == null)
                    {
                        DoingImageLoadUtility.Instance = new DoingImageLoadUtility();
                    }
                    if (dto != null)
                    {
                        DoingImageLoadUtility.Instance.LoadSingleImageUploaderModel(dto);
                    }
                    else
                    {
                        lock (NewsFeedDictionaries.Instance.DOING_DICTIONARY)
                        {
                            foreach (var item in NewsFeedDictionaries.Instance.DOING_DICTIONARY.Values)
                            {
                                DoingImageLoadUtility.Instance.LoadSingleImageUploaderModel(item);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: EnqueueDoingDTOforImageDownload() => " + ex.Message + "\n" + ex.StackTrace);
                }

            }));
        }

        public UserBasicInfoModel GetExistingorNewUserBasicModelFromDTO(UserBasicInfoDTO dto)
        {
            UserBasicInfoModel model = (dto.UserTableID == DefaultSettings.LOGIN_TABLE_ID) ? RingIDViewModel.Instance.MyBasicInfoModel : RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(dto.UserTableID);
            if (model == null)
            {
                model = new UserBasicInfoModel(dto);
            }
            return model;
        }

        //public void UpdateImageCommentLikeUnlike(Guid imageId, Guid commentId, int liked, UserBasicInfoDTO user, Guid newsfeedId)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //        {
        //            try
        //            {
        //                ObservableCollection<CommentModel> CommentsINSingleImgFeedModel = null;
        //                ObservableCollection<CommentModel> CommentsINImgView = null;
        //                FeedModel feedModel = null;
        //                if (newsfeedId != Guid.Empty)
        //                {
        //                    FeedDataContainer.Instance.FeedModels.TryGetValue(newsfeedId, out feedModel);
        //                    if (feedModel != null && feedModel.SingleImageFeedModel != null)
        //                    {
        //                        CommentsINSingleImgFeedModel = feedModel.CommentList;
        //                    }
        //                }

        //                if (CommentsINImgView != null || CommentsINSingleImgFeedModel != null)
        //                {
        //                    CommentModel commentFeed = null, commentImgView = null;
        //                    if (CommentsINSingleImgFeedModel != null) commentFeed = CommentsINSingleImgFeedModel.Where(p => p.CommentId == commentId).FirstOrDefault();
        //                    if (CommentsINImgView != null) commentImgView = CommentsINImgView.Where(p => p.CommentId == commentId).FirstOrDefault();
        //                    if (commentFeed != null)
        //                    {
        //                        commentFeed.TotalLikeComment = (liked == 1) ? (commentFeed.TotalLikeComment + 1) : (commentFeed.TotalLikeComment - 1);
        //                    }
        //                    else if (commentImgView != null)
        //                    {
        //                        if (commentFeed != null) commentImgView.TotalLikeComment = commentFeed.TotalLikeComment;
        //                        else commentImgView.TotalLikeComment = (liked == 1) ? (commentImgView.TotalLikeComment + 1) : (commentImgView.TotalLikeComment - 1);
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                log.Error("Error: UpdateImageCommentLikeUnlike() => " + ex.Message + "\n" + ex.StackTrace);
        //            }
        //        }));
        //}

        public void HandleLikeUnlikeUpdateFeed(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ActivityType] != null)
                {
                    int activityType = (int)_JobjFromResponse[JsonKeys.ActivityType];
                    int loc = (int)_JobjFromResponse[JsonKeys.LikeOrComment];
                    int like = (int)_JobjFromResponse[JsonKeys.Liked];
                    if (activityType == SettingsConstants.ACTIVITY_ON_STATUS && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.ActivistId] != null && _JobjFromResponse[JsonKeys.LikeOrComment] != null)
                    {
                        Guid newsfeedId = (Guid)_JobjFromResponse[JsonKeys.NewsfeedId];
                        long friendTableId = (long)_JobjFromResponse[JsonKeys.ActivistId];
                        string friendName = (_JobjFromResponse[JsonKeys.FullName] != null) ? (string)_JobjFromResponse[JsonKeys.FullName] : "";
                        long friendId = (_JobjFromResponse[JsonKeys.UserIdentity] != null) ? (long)_JobjFromResponse[JsonKeys.UserIdentity] : 0;
                        FeedModel feedModel = null;
                        if (FeedDataContainer.Instance.FeedModels.TryGetValue(newsfeedId, out feedModel) && feedModel.LikeCommentShare != null)
                        {
                            feedModel.LikeCommentShare.NumberOfLikes = loc;
                            if (feedModel.SingleImageFeedModel != null && feedModel.SingleImageFeedModel.LikeCommentShare != null) feedModel.SingleImageFeedModel.LikeCommentShare.NumberOfLikes = loc;
                            else if (feedModel.SingleMediaFeedModel != null && feedModel.SingleMediaFeedModel.LikeCommentShare != null) feedModel.SingleMediaFeedModel.LikeCommentShare.NumberOfLikes = loc;
                            if (like == 1)
                            {
                                feedModel.Activist = RingIDViewModel.Instance.GetUserShortInfoModelNotNullable(friendTableId, friendId, friendName, "");
                                feedModel.ActivityType = AppConstants.NEWSFEED_LIKED;
                            }
                            //feedModel.OnPropertyChanged("CurrentInstance");
                        }
                    }
                    else if (activityType == SettingsConstants.ACTIVITY_ON_IMAGE && _JobjFromResponse[JsonKeys.ContentId] != null)
                    {
                        Guid ImageId = (Guid)_JobjFromResponse[JsonKeys.ContentId];
                        ImageModel imageModel = null;
                        if (ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(ImageId, out imageModel) && imageModel.LikeCommentShare != null)
                        {
                            imageModel.LikeCommentShare.NumberOfLikes = loc;
                        }
                    }
                    else if (activityType == SettingsConstants.ACTIVITY_ON_MULTIMEDIA && _JobjFromResponse[JsonKeys.ContentId] != null)
                    {
                        Guid ContentId = (Guid)_JobjFromResponse[JsonKeys.ContentId];
                        SingleMediaModel singleMediaModel = null;
                        if (MediaDataContainer.Instance.ContentModels.TryGetValue(ContentId, out singleMediaModel) && singleMediaModel.LikeCommentShare != null)
                        {
                            singleMediaModel.LikeCommentShare.NumberOfLikes = loc;
                        }
                    }
                }
            }
            catch (Exception e)
            {

                log.Error("ProcessUpdateLikeStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

            }
        }
        public void Process_ACTION_UPDATE_LIKE_UNLIKE_COMMENT_1323(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.CommentId] != null && _JobjFromResponse[JsonKeys.LikeOrComment] != null)
                {
                    Guid newsfeedId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (Guid)_JobjFromResponse[JsonKeys.NewsfeedId] : Guid.Empty;
                    Guid commentId = (Guid)_JobjFromResponse[JsonKeys.CommentId];
                    int loc = (int)_JobjFromResponse[JsonKeys.LikeOrComment];
                    Guid contentId = (_JobjFromResponse[JsonKeys.ContentId] != null) ? (Guid)_JobjFromResponse[JsonKeys.ContentId] : Guid.Empty;
                    FeedModel feedModel = null;
                    CommentModel commentModelOnFeed = null, commentModelOnView = null;
                    if (FeedDataContainer.Instance.FeedModels.TryGetValue(newsfeedId, out feedModel) && feedModel.CommentList != null && feedModel.CommentList.Count > 0)
                    {
                        commentModelOnFeed = feedModel.CommentList.Where(P => P.CommentId == commentId).FirstOrDefault();
                        if (feedModel.SingleImageFeedModel != null) contentId = feedModel.SingleImageFeedModel.ImageId;
                        if (feedModel.SingleMediaFeedModel != null) contentId = feedModel.SingleMediaFeedModel.ContentId;
                    }
                    if (commentModelOnFeed != null)
                    {
                        commentModelOnFeed.TotalLikeComment = loc;
                    }
                    if (commentModelOnView != null)
                    {
                        commentModelOnView.TotalLikeComment = loc;
                    }
                }
            }
            catch (Exception e)
            {

                log.Error("ProcessUpdateLikeStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

            }
        }

        public void HandleLikeUnlikeUpdateComment(JObject _JobjFromResponse, int Like)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.CommentId] != null && _JobjFromResponse[JsonKeys.LikeOrComment] != null)
                {
                    Guid nfId = (Guid)_JobjFromResponse[JsonKeys.NewsfeedId];
                    Guid commentId = (Guid)_JobjFromResponse[JsonKeys.CommentId];
                    int loc = (int)_JobjFromResponse[JsonKeys.LikeOrComment];
                    long friendTableId = (_JobjFromResponse[JsonKeys.UserTableID] != null) ? (long)_JobjFromResponse[JsonKeys.UserTableID] : 0;
                    long friendId = (_JobjFromResponse[JsonKeys.UserIdentity] != null) ? (long)_JobjFromResponse[JsonKeys.UserIdentity] : 0;
                    Guid contentId = (_JobjFromResponse[JsonKeys.ContentId] != null) ? (Guid)_JobjFromResponse[JsonKeys.ContentId] : Guid.Empty;
                    Guid imgId = (_JobjFromResponse[JsonKeys.ImageId] != null) ? (Guid)_JobjFromResponse[JsonKeys.ImageId] : Guid.Empty;
                    FeedModel feedModel = null;
                    CommentModel commentModelOnFeed = null, commentModelOnView = null;
                    if (FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out feedModel) && feedModel.CommentList != null && feedModel.CommentList.Count > 0)
                    {
                        commentModelOnFeed = feedModel.CommentList.Where(P => P.CommentId == commentId).FirstOrDefault();
                        if (feedModel.SingleImageFeedModel != null) imgId = feedModel.SingleImageFeedModel.ImageId;
                        if (feedModel.SingleMediaFeedModel != null) contentId = feedModel.SingleMediaFeedModel.ContentId;
                    }
                    //if (imgId != Guid.Empty && basicImageViewWrapper().ucBasicImageView != null && basicImageViewWrapper().ucBasicImageView.clickedImageID == imgId && basicImageViewWrapper().ucBasicImageView.ImageComments != null && basicImageViewWrapper().ucBasicImageView.ImageComments.ImageCommentList.Count > 0)
                    //{
                    //    commentModelOnView = basicImageViewWrapper().ucBasicImageView.ImageComments.ImageCommentList.Where(P => P.CommentId == commentId).FirstOrDefault();
                    //}
                    //else if (contentId != Guid.Empty && BasicMediaViewWrapper.ucBasicMediaView != null && RingPlayerViewModel.Instance.ClickedMediaID == contentId && BasicMediaViewWrapper.ucBasicMediaView.ImageComments != null && BasicMediaViewWrapper.ucBasicMediaView.ImageComments.ImageCommentList.Count > 0)
                    //{
                    //    commentModelOnView = BasicMediaViewWrapper.ucBasicMediaView.ImageComments.ImageCommentList.Where(P => P.CommentId == commentId).FirstOrDefault();
                    //}
                    //
                    if (commentModelOnFeed != null)
                    {
                        commentModelOnFeed.TotalLikeComment = loc;
                    }
                    if (commentModelOnView != null)
                    {
                        commentModelOnView.TotalLikeComment = loc;
                    }
                }
            }
            catch (Exception e)
            {

                log.Error("ProcessUpdateLikeStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

            }
        }

        private void addUpdateStatus(JObject _JobjFromResponse, bool AddStatus = false)
        {
            try
            {
                if (_JobjFromResponse != null)
                {
                    JObject objectToBeLoadInModel = null;
                    JObject newsFeed = (JObject)_JobjFromResponse[JsonKeys.NewsFeed];
                    Guid nfId = newsFeed[JsonKeys.NewsfeedId] != null ? (Guid)newsFeed[JsonKeys.NewsfeedId] : Guid.Empty;
                    FeedModel model = null;
                    if (!FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out model)) { model = new FeedModel(); FeedDataContainer.Instance.FeedModels[nfId] = model; }
                    if (_JobjFromResponse[JsonKeys.NewsFeed] != null)
                    {
                        objectToBeLoadInModel = (JObject)_JobjFromResponse[JsonKeys.NewsFeed];
                        model.LoadData(objectToBeLoadInModel);
                    }
                    else
                    {
                        objectToBeLoadInModel = _JobjFromResponse;
                        model.LoadData(objectToBeLoadInModel);
                    }
                    //FeedDataContainer.Instance.AllFeedsSortedIds.InsertIntoSortedList(model.Time, nfId);

                    if (!NewsFeedViewModel.Instance.CurrentFeedIds.Contains(nfId))
                    {
                        FeedHolderModel holder = new FeedHolderModel(SettingsConstants.FEED_TYPE_NORMAL_FEED);
                        holder.FeedId = nfId;
                        holder.Feed = model;
                        holder.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                        holder.SelectViewTemplate();
                        NewsFeedViewModel.Instance.CalculateandInsertAllFeeds(holder);
                    }

                    if (model.WallOrContentOwner.ProfileType == SettingsConstants.PROFILE_TYPE_GENERAL)
                    {
                        if (RingIDViewModel.Instance.ProfileFriendCustomFeeds.UserProfileUtId == model.WallOrContentOwner.UserTableID)
                        {
                            if (!FeedDataContainer.Instance.ProfileFriendCurrentIds.Contains(model.NewsfeedId))//if (!RingIDViewModel.Instance.ProfileFriendCustomFeeds.Any(P => P.FeedId == model.NewsfeedId))
                            {
                                FeedHolderModel feedHolderModel = new FeedHolderModel(SettingsConstants.FEED_TYPE_NORMAL_FEED);
                                feedHolderModel.FeedId = nfId;
                                feedHolderModel.Feed = model;
                                feedHolderModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                feedHolderModel.FeedPanelType = feedHolderModel.Feed.FeedPanelType;
                                RingIDViewModel.Instance.ProfileFriendCustomFeeds.CalculateandInsertOtherFeeds(feedHolderModel, FeedDataContainer.Instance.ProfileFriendCurrentIds);
                            }
                        }
                        else if (model.WallOrContentOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                        {
                            if (!FeedDataContainer.Instance.ProfileMyCurrentIds.Contains(model.NewsfeedId))//if (!RingIDViewModel.Instance.ProfileMyCustomFeeds.Any(p => p.FeedId == model.NewsfeedId))
                            {
                                FeedHolderModel feedHolderModel = new FeedHolderModel(SettingsConstants.FEED_TYPE_NORMAL_FEED);
                                feedHolderModel.FeedId = nfId;
                                feedHolderModel.Feed = model;
                                feedHolderModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                feedHolderModel.FeedPanelType = feedHolderModel.Feed.FeedPanelType;
                                RingIDViewModel.Instance.ProfileMyCustomFeeds.CalculateandInsertOtherFeeds(feedHolderModel, FeedDataContainer.Instance.ProfileMyCurrentIds);
                            }
                        }
                    }
                    else if (model.WallOrContentOwner.ProfileType == SettingsConstants.PROFILE_TYPE_CIRCLE)
                    {
                        //TODO

                        //if (RingIDViewModel.Instance.CircleProfileFeeds.UserProfileUtId == model.WallOrContentOwner.UserTableID)
                        //{
                        //    FeedDataContainer.Instance.CircleProfileFeedsSortedIds.InsertIntoSortedList(model.ActualTime, nfId);
                        //    if (!RingIDViewModel.Instance.CircleProfileFeeds.Any(P => P.NewsfeedId == model.NewsfeedId))
                        //        RingIDViewModel.Instance.CircleProfileFeeds.InsertModel(model);
                        //}
                        //FeedDataContainer.Instance.AllCircleFeedsSortedIds.InsertIntoSortedList(model.ActualTime, nfId);
                        //if (!RingIDViewModel.Instance.AllCircleFeeds.Any(P => P.NewsfeedId == model.NewsfeedId))
                        //    RingIDViewModel.Instance.AllCircleFeeds.InsertModel(model);
                    }
                    StorageFeeds.UpdateLocallySavedFeeds(objectToBeLoadInModel);
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessUpdateAddStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        private void addStatus177_117(JObject _Jobj)
        {
            try
            {
                if (_Jobj != null && (bool)_Jobj[JsonKeys.Success] == true && _Jobj[JsonKeys.NewsFeed] != null)
                {
                    JObject _JobjFromResponse = (JObject)_Jobj[JsonKeys.NewsFeed];
                    Guid nfId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (Guid)_JobjFromResponse[JsonKeys.NewsfeedId] : Guid.Empty;
                    FeedModel model = null;
                    if (!FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out model))
                    {
                        model = new FeedModel();
                        FeedDataContainer.Instance.FeedModels[nfId] = model;
                    }
                    FeedDataContainer.Instance.StorageFeedJObjects[nfId] = _JobjFromResponse;
                    model.LoadData(_JobjFromResponse);
                    //
                    if (!NewsFeedViewModel.Instance.CurrentFeedIds.Contains(nfId))
                    {
                        FeedHolderModel holder = new FeedHolderModel(SettingsConstants.FEED_TYPE_NORMAL_FEED);
                        holder.FeedId = nfId;
                        holder.Feed = model;
                        holder.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                        holder.SelectViewTemplate();
                        NewsFeedViewModel.Instance.CalculateandInsertAllFeeds(holder);
                    }

                    if (!FeedDataContainer.Instance.ProfileMyCurrentIds.Contains(model.NewsfeedId))//if (!RingIDViewModel.Instance.ProfileMyCustomFeeds.Any(p => p.FeedId == model.NewsfeedId))
                    {
                        FeedHolderModel feedHolderModel = new FeedHolderModel(SettingsConstants.FEED_TYPE_NORMAL_FEED);
                        feedHolderModel.FeedId = nfId;
                        feedHolderModel.Feed = model;
                        feedHolderModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                        feedHolderModel.FeedPanelType = feedHolderModel.Feed.FeedPanelType;
                        RingIDViewModel.Instance.ProfileMyCustomFeeds.CalculateandInsertOtherFeeds(feedHolderModel, FeedDataContainer.Instance.ProfileMyCurrentIds);
                    }

                    if (model.WallOrContentOwner.ProfileType == SettingsConstants.PROFILE_TYPE_GENERAL)
                    {
                        if (RingIDViewModel.Instance.ProfileFriendCustomFeeds.UserProfileUtId == model.WallOrContentOwner.UserTableID)
                        {
                            if (!FeedDataContainer.Instance.ProfileFriendCurrentIds.Contains(model.NewsfeedId))//if (!RingIDViewModel.Instance.ProfileFriendCustomFeeds.Any(P => P.FeedId == model.NewsfeedId))
                            {
                                FeedHolderModel feedHolderModel = new FeedHolderModel(SettingsConstants.FEED_TYPE_NORMAL_FEED);
                                feedHolderModel.FeedId = nfId;
                                feedHolderModel.Feed = model;
                                feedHolderModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                feedHolderModel.FeedPanelType = feedHolderModel.Feed.FeedPanelType;
                                RingIDViewModel.Instance.ProfileFriendCustomFeeds.CalculateandInsertOtherFeeds(feedHolderModel, FeedDataContainer.Instance.ProfileFriendCurrentIds);
                            }
                        }
                    }
                    else if (model.WallOrContentOwner.ProfileType == SettingsConstants.PROFILE_TYPE_CIRCLE)
                    {
                        //TODO

                        //if (RingIDViewModel.Instance.CircleProfileFeeds.UserProfileUtId == model.WallOrContentOwner.UserTableID)
                        //{
                        //    FeedDataContainer.Instance.CircleProfileFeedsSortedIds.InsertIntoSortedList(model.ActualTime, nfId);
                        //    if (!RingIDViewModel.Instance.CircleProfileFeeds.Any(P => P.NewsfeedId == model.NewsfeedId))
                        //        RingIDViewModel.Instance.CircleProfileFeeds.InsertModel(model);
                        //}
                        //FeedDataContainer.Instance.AllCircleFeedsSortedIds.InsertIntoSortedList(model.ActualTime, nfId);
                        //if (!RingIDViewModel.Instance.AllCircleFeeds.Any(P => P.NewsfeedId == model.NewsfeedId))
                        //    RingIDViewModel.Instance.AllCircleFeeds.InsertModel(model);
                    }
                    //}
                    if (model.ImageList != null && model.ImageList.Count > 0 && RingIDViewModel.Instance.FeedImageList.Count > 1)
                    {
                        ImageUtility.AddProfileCoverImageToUIList(model.ImageList, SettingsConstants.TYPE_NORMAL_BOOK_IMAGE);
                    }
                    if (model.MediaContent != null && model.MediaContent.MediaList != null && model.MediaContent.MediaList.Count > 0)
                    {
                        if (model.MediaContent.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO)
                        {
                            MediaContentModel albumModel = RingIDViewModel.Instance.MyAudioAlbums.Where(P => P.AlbumId == model.MediaContent.AlbumId).FirstOrDefault();
                            if (albumModel != null && albumModel.MediaList != null)
                            {
                                foreach (var item in model.MediaContent.MediaList)
                                {
                                    albumModel.MediaList.InvokeAdd(item);
                                }
                                albumModel.TotalMediaCount += albumModel.MediaList.Count;
                            }
                        }
                        else if (model.MediaContent.MediaType == SettingsConstants.MEDIA_TYPE_VIDEO)
                        {
                            MediaContentModel albumModel = RingIDViewModel.Instance.MyVideoAlbums.Where(P => P.AlbumId == model.MediaContent.AlbumId).FirstOrDefault();
                            if (albumModel != null && albumModel.MediaList != null)
                            {
                                foreach (var item in model.MediaContent.MediaList)
                                {
                                    albumModel.MediaList.InvokeAdd(item);
                                }
                                albumModel.TotalMediaCount += albumModel.MediaList.Count;
                            }
                        }
                    }
                }
                else
                {
                    string mg = String.Empty;
                    if (_Jobj[JsonKeys.Message] != null)
                    {
                        mg = (string)_Jobj[JsonKeys.Message];
                    }
                    ShowErrorMessageBox(mg, NotificationMessages.FAILED_CONFIRMATION_MESSAGE_TITLE);
                }
            }
            catch (Exception e)
            {

                log.Error("ProcessAddStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

            }
        }

        private void processe_250_115(JObject _JobjFromResponse)
        {
            try
            {
                //if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Shares] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.NewsfeedId] != null)
                //{
                //    Guid nfId = (Guid)_JobjFromResponse[JsonKeys.NewsfeedId];
                //    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Shares];
                //    List<UserBasicInfoDTO> list = new List<UserBasicInfoDTO>();
                //    long minIdOfSequence = 99999;
                //    foreach (JObject obj in jarray)
                //    {
                //        if (obj[JsonKeys.UserTableID] != null && obj[JsonKeys.Id] != null && obj[JsonKeys.FullName] != null && obj[JsonKeys.UserTableID] != null && obj[JsonKeys.ProfileImage] != null)
                //        {
                //            long utId = (long)obj[JsonKeys.UserTableID];
                //            UserBasicInfoDTO user = null;
                //            if (utId == DefaultSettings.LOGIN_TABLE_ID)
                //            {
                //                user = new UserBasicInfoDTO();
                //                user.UserTableID = utId;
                //            }
                //            else
                //            {
                //                user = FriendDictionaries.Instance.GetFromBasicInfoDictionaryNotNuallable(utId);
                //            }
                //            user.FullName = (string)obj[JsonKeys.FullName];
                //            user.UserTableID = (long)obj[JsonKeys.UserTableID];
                //            user.ProfileImage = (string)obj[JsonKeys.ProfileImage];
                //            long Id = (long)obj[JsonKeys.Id];
                //            if (Id < minIdOfSequence) minIdOfSequence = Id;
                //            list.Add(user);
                //        }
                //    }
                //    ShowSingleFeedShareList(nfId, list, minIdOfSequence);
                //}
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Shares] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.NewsfeedId] != null)
                {
                    Guid nfId = (Guid)_JobjFromResponse[JsonKeys.NewsfeedId];
                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Shares];
                    //List<UserBasicInfoDTO> list = new List<UserBasicInfoDTO>();
                    //long minIdOfSequence = 99999;
                    if (UCFeedTagListView.Instance != null && UCFeedTagListView.Instance.IsVisible)
                    {
                        foreach (JObject obj in jarray)
                        {
                            if (obj[JsonKeys.UserTableID] != null && obj[JsonKeys.FullName] != null)
                            {
                                long utId = (long)obj[JsonKeys.UserTableID];
                                BaseUserProfileModel baseModel = null;
                                if (utId == DefaultSettings.LOGIN_TABLE_ID)
                                {
                                    baseModel = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel;
                                }
                                else
                                {
                                    baseModel = new BaseUserProfileModel(obj);
                                }
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    if (!UCFeedTagListView.Instance.TagList.Any(P => P.UserTableID == utId))
                                        UCFeedTagListView.Instance.TagList.Add(baseModel);
                                }, DispatcherPriority.Send);
                            }
                        }
                        //ShowSingleFeedShareList(nfId, list, minIdOfSequence);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("ProcessMediaShareList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }

        public void ShowErrorMessageBox(string message, string caption)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                UIHelperMethods.ShowFailed(message, caption);
                //  CustomMessageBox.ShowError(msg, caption);
            });
        }

        public void AddOrUpdateAddAnyComment(JObject _JobjFromResponse, Guid newsfeedId, Guid imageId, Guid contentId, bool update = false)
        {
            try
            {
                long likeOrComment = (long)_JobjFromResponse[JsonKeys.LikeOrComment];
                long postOwnerId = 0;

                FeedModel feedModel = null;
                if (newsfeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(newsfeedId, out feedModel) && feedModel.LikeCommentShare != null)
                {
                    if (feedModel.CommentList == null) feedModel.CommentList = new ObservableCollection<CommentModel>();
                    if (feedModel.PostOwner != null) postOwnerId = feedModel.PostOwner.UserTableID;
                    if ((imageId == Guid.Empty && contentId == Guid.Empty) || (feedModel.BookPostType == 2 || feedModel.BookPostType == 5 || feedModel.BookPostType == 8))
                    {
                        feedModel.LikeCommentShare.NumberOfComments = likeOrComment;
                        if (!update) feedModel.LikeCommentShare.IComment = 1;
                        HelperMethods.InsertIntoCommentListByAscTime(feedModel.CommentList, _JobjFromResponse, newsfeedId, imageId, contentId, postOwnerId);
                    }
                }
                if (imageId != Guid.Empty && imageId == ViewConstants.ImageID)
                {
                    ImageModel imageModel = null;
                    if (ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(imageId, out imageModel) && imageModel.LikeCommentShare != null)
                    {
                        postOwnerId = imageModel.UserTableID;
                        if (!update) imageModel.LikeCommentShare.IComment = 1;
                        imageModel.LikeCommentShare.NumberOfComments = likeOrComment;
                    }
                    HelperMethods.InsertIntoCommentListByDscTime(RingIDViewModel.Instance.MediaComments, _JobjFromResponse, newsfeedId, imageId, Guid.Empty, postOwnerId);
                }
                else if (contentId != Guid.Empty && contentId == ViewConstants.ContentID)
                {
                    SingleMediaModel singleMediaModel = null;
                    if (MediaDataContainer.Instance.ContentModels.TryGetValue(contentId, out singleMediaModel) && singleMediaModel.LikeCommentShare != null)
                    {
                        postOwnerId = singleMediaModel.MediaOwner.UserTableID;
                        if (!update) singleMediaModel.LikeCommentShare.IComment = 1;
                        singleMediaModel.LikeCommentShare.NumberOfComments = likeOrComment;
                    }
                    HelperMethods.InsertIntoCommentListByDscTime(RingIDViewModel.Instance.MediaComments, _JobjFromResponse, newsfeedId, Guid.Empty, contentId, postOwnerId);
                }
            }
            catch (Exception ex)
            {
                log.Error("AddOrUpdateAddAnyComment ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }

        }

        public void EditOrUpdateEditAnyComment(JObject _JobjFromResponse, Guid commentId, Guid newsfeedId, Guid imageId, Guid contentId)
        {
            try
            {
                CommentModel commentModelOnFeed = null, commentModelOnView = null;
                FeedModel feedModel = null;
                if (newsfeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(newsfeedId, out feedModel) && feedModel.CommentList != null && feedModel.CommentList.Count > 0)
                {
                    commentModelOnFeed = feedModel.CommentList.Where(P => P.CommentId == commentId).FirstOrDefault();
                }
                if ((contentId != Guid.Empty && ViewConstants.ContentID == contentId) || (imageId != Guid.Empty && imageId == ViewConstants.ImageID))
                {
                    commentModelOnView = RingIDViewModel.Instance.MediaComments.Where(P => P.CommentId == commentId).FirstOrDefault();
                }
                if (commentModelOnFeed != null)
                {
                    commentModelOnFeed.LoadData(_JobjFromResponse);
                    commentModelOnFeed.Edited = true;
                }
                if (commentModelOnView != null)
                {
                    commentModelOnView.LoadData(_JobjFromResponse);
                    commentModelOnView.Edited = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("EditOrUpdateEditAnyComment ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }

        public void DeleteOrUpdateDeleteAnyComment(JObject _JobjFromResponse, Guid commentId, Guid newsfeedId, Guid imageId, Guid contentId)
        {
            try
            {
                long loc = (long)_JobjFromResponse[JsonKeys.LikeOrComment];
                short iComment = (_JobjFromResponse[JsonKeys.IComment] != null) ? (short)_JobjFromResponse[JsonKeys.IComment] : (short)-1;

                FeedModel feedModel = null;
                if (newsfeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(newsfeedId, out feedModel) && feedModel.LikeCommentShare != null)
                {
                    if (feedModel.CommentList != null && feedModel.CommentList.Count > 0)
                    {
                        if ((contentId == Guid.Empty && imageId == Guid.Empty)
                          || ((feedModel.SingleMediaFeedModel != null || feedModel.SingleImageFeedModel != null) && feedModel.BookPostType != 3 && feedModel.BookPostType != 6 && feedModel.BookPostType != 9))
                        {
                            CommentModel commentModelOnFeed = feedModel.CommentList.Where(P => P.CommentId == commentId).FirstOrDefault();
                            if (commentModelOnFeed != null)
                            {
                                feedModel.CommentList.InvokeRemove(commentModelOnFeed);
                            }
                            if (feedModel.ActivityType == AppConstants.NEWSFEED_COMMENTED)
                            {
                                feedModel.Activist = null;
                                feedModel.ActivityType = 0;
                            }
                            feedModel.LikeCommentShare.NumberOfComments = loc;
                            if (iComment >= 0) feedModel.LikeCommentShare.IComment = iComment;
                        }
                    }
                }
                if (contentId != Guid.Empty && ViewConstants.ContentID == contentId)
                {
                    CommentModel commentModelOnView = RingIDViewModel.Instance.MediaComments.Where(P => P.CommentId == commentId).FirstOrDefault();
                    if (commentModelOnView != null)
                    {
                        RingIDViewModel.Instance.MediaComments.InvokeRemove(commentModelOnView);
                    }
                    SingleMediaModel singleMediaModel = null;
                    if (MediaDataContainer.Instance.ContentModels.TryGetValue(contentId, out singleMediaModel) && singleMediaModel.LikeCommentShare != null)
                    {
                        singleMediaModel.LikeCommentShare.NumberOfComments = loc;
                        singleMediaModel.LikeCommentShare.IComment = iComment;
                    }
                }
                else if (imageId != Guid.Empty && imageId == ViewConstants.ImageID)
                {
                    CommentModel commentModelOnView = RingIDViewModel.Instance.MediaComments.Where(P => P.CommentId == commentId).FirstOrDefault();
                    if (commentModelOnView != null)
                    {
                        RingIDViewModel.Instance.MediaComments.InvokeRemove(commentModelOnView);
                    }
                    ImageModel imageModel = null;
                    if (ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(imageId, out imageModel) && imageModel.LikeCommentShare != null)
                    {
                        imageModel.LikeCommentShare.NumberOfComments = loc;
                        imageModel.LikeCommentShare.IComment = iComment;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("DeleteOrUpdateDeleteAnyComment ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }

        public void OnLikeUnlikeFailed(Guid newsfeedId, Guid contentId, string message, Guid commentId, int activityType)
        {
            try
            {
                if (commentId != Guid.Empty)
                {
                    if (contentId != Guid.Empty && (ViewConstants.ImageID == contentId || ViewConstants.ContentID == contentId))
                    {
                        CommentModel commentModel = RingIDViewModel.Instance.MediaComments.Where(P => P.CommentId == commentId).FirstOrDefault();
                        if (commentModel != null)
                        {
                            if (commentModel.ILikeComment == 1)
                            {
                                commentModel.ILikeComment = 0;
                                commentModel.TotalLikeComment = (commentModel.TotalLikeComment - 1) > 0 ? commentModel.TotalLikeComment - 1 : 0;
                            }
                            else
                            {
                                commentModel.TotalLikeComment = (commentModel.TotalLikeComment + 1);
                                commentModel.ILikeComment = 1;
                            }
                        }
                    }
                    else
                    {
                        ObservableCollection<CommentModel> CommentsInFeedModel = null;
                        FeedModel feedModel = null;
                        if (newsfeedId != Guid.Empty)
                        {
                            FeedDataContainer.Instance.FeedModels.TryGetValue(newsfeedId, out feedModel);
                            if (feedModel != null)
                            {
                                CommentsInFeedModel = feedModel.CommentList;
                            }
                        }
                        CommentModel commentOnFeed = null;
                        if (CommentsInFeedModel != null) commentOnFeed = CommentsInFeedModel.Where(p => p.CommentId == commentId).FirstOrDefault();
                        if (commentOnFeed.ILikeComment == 1)
                        {
                            commentOnFeed.ILikeComment = 0;
                            commentOnFeed.TotalLikeComment = (commentOnFeed.TotalLikeComment - 1) > 0 ? commentOnFeed.TotalLikeComment - 1 : 0;
                        }
                        else
                        {
                            commentOnFeed.TotalLikeComment = (commentOnFeed.TotalLikeComment + 1);
                            commentOnFeed.ILikeComment = 1;
                        }
                    }
                }
                else
                {
                    if (activityType == 1)
                    {
                        FeedModel feedModel = null;
                        if (newsfeedId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(newsfeedId, out feedModel))
                        {
                            if (feedModel.LikeCommentShare.ILike == 1)
                            {
                                feedModel.LikeCommentShare.ILike = 0;
                                feedModel.LikeCommentShare.NumberOfLikes = (feedModel.LikeCommentShare.NumberOfLikes - 1) > 0 ? feedModel.LikeCommentShare.NumberOfLikes - 1 : 0;
                            }
                            else
                            {
                                feedModel.LikeCommentShare.NumberOfLikes = (feedModel.LikeCommentShare.NumberOfLikes + 1);
                                feedModel.LikeCommentShare.ILike = 1;
                            }
                        }
                    }
                    if (contentId != Guid.Empty && contentId == ViewConstants.ImageID)
                    {
                        ImageModel imageModel = null;
                        if (ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(contentId, out imageModel))
                        {
                            if (imageModel.LikeCommentShare.ILike == 1)
                            {
                                imageModel.LikeCommentShare.ILike = 0;
                                imageModel.LikeCommentShare.NumberOfLikes = (imageModel.LikeCommentShare.NumberOfLikes - 1) > 0 ? imageModel.LikeCommentShare.NumberOfLikes - 1 : 0;
                            }
                            else
                            {
                                imageModel.LikeCommentShare.NumberOfLikes = (imageModel.LikeCommentShare.NumberOfLikes + 1);
                                imageModel.LikeCommentShare.ILike = 1;
                            }
                        }
                    }
                    else if (contentId != Guid.Empty && contentId == ViewConstants.ContentID)
                    {
                        SingleMediaModel singleMediaModel = null;
                        if (MediaDataContainer.Instance.ContentModels.TryGetValue(contentId, out singleMediaModel))
                        {
                            if (singleMediaModel.LikeCommentShare.ILike == 1)
                            {
                                singleMediaModel.LikeCommentShare.ILike = 0;
                                singleMediaModel.LikeCommentShare.NumberOfLikes = (singleMediaModel.LikeCommentShare.NumberOfLikes - 1) > 0 ? singleMediaModel.LikeCommentShare.NumberOfLikes - 1 : 0;
                            }
                            else
                            {
                                singleMediaModel.LikeCommentShare.NumberOfLikes = (singleMediaModel.LikeCommentShare.NumberOfLikes + 1);
                                singleMediaModel.LikeCommentShare.ILike = 1;
                            }
                        }
                    }
                }
                //UIHelperMethods.ShowErrorMessageBoxFromThread(NotificationMessages.INTERNET_UNAVAILABLE, "Failed!");
            }
            catch (Exception ex)
            {
                log.Error("Error: OnLikeUnlikeFailed() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        //public void OnLikeUnlikeFailed(Guid nfid, Object obj, string msg, Guid cmntId)
        //{
        //    try
        //    {
        //        if (obj is Guid)
        //        {
        //            Guid imgId = (Guid)obj;
        //            if (cmntId != Guid.Empty)// && basicImageViewWrapper().ucBasicImageView != null &&
        //            //basicImageViewWrapper().ucBasicImageView.ImageComments != null && basicImageViewWrapper().ucBasicImageView.ImageComments.ImageCommentList.Count > 0)
        //            {
        //                if (ViewConstants.ImageID == imgId)
        //                {
        //                    CommentModel commentModelOnView = RingIDViewModel.Instance.MediaComments.Where(P => P.CommentId == cmntId).FirstOrDefault();
        //                    if (commentModelOnView != null)
        //                    {
        //                        if (commentModelOnView.ILikeComment == 1)
        //                        {
        //                            commentModelOnView.ILikeComment = 0;
        //                            commentModelOnView.TotalLikeComment = (commentModelOnView.TotalLikeComment - 1) > 0 ? commentModelOnView.TotalLikeComment - 1 : 0;
        //                        }
        //                        else
        //                        {
        //                            commentModelOnView.TotalLikeComment = (commentModelOnView.TotalLikeComment + 1);
        //                            commentModelOnView.ILikeComment = 1;
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                ImageModel model = ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(imgId);
        //                if (model != null && model.LikeCommentShare != null)
        //                {
        //                    if (model.LikeCommentShare.ILike == 0)
        //                    {
        //                        model.LikeCommentShare.ILike = 1;
        //                        model.LikeCommentShare.NumberOfLikes = (model.LikeCommentShare.NumberOfLikes + 1);
        //                    }
        //                    else
        //                    {
        //                        model.LikeCommentShare.ILike = 0;
        //                        model.LikeCommentShare.NumberOfLikes = (model.LikeCommentShare.NumberOfLikes - 1);
        //                    }
        //                }
        //            }
        //        }
        //        else if (obj is FeedModel)
        //        {
        //            FeedModel fm = (FeedModel)obj;
        //            if (fm.LikeCommentShare != null)
        //            {
        //                if (fm.LikeCommentShare.ILike == 0)
        //                {
        //                    fm.LikeCommentShare.ILike = 1;
        //                    fm.LikeCommentShare.NumberOfLikes = fm.LikeCommentShare.NumberOfLikes + 1;
        //                }
        //                else
        //                {
        //                    fm.LikeCommentShare.ILike = 0;
        //                    fm.LikeCommentShare.NumberOfLikes = fm.LikeCommentShare.NumberOfLikes - 1;
        //                }
        //                //fm.OnPropertyChanged("CurrentInstance");
        //            }
        //        }
        //        else if (obj is CommentModel)
        //        {
        //            CommentModel cm = (CommentModel)obj;
        //            if (cm.ILikeComment == 0) { cm.ILikeComment = 1; cm.TotalLikeComment = cm.TotalLikeComment + 1; }
        //            else { cm.ILikeComment = 0; cm.TotalLikeComment = cm.TotalLikeComment - 1; }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: OnLikeUnlikeFailed() ==> " + ex.Message + "\n" + ex.StackTrace);
        //    }
        //}

        public void LoadNewsPortalCategories(long UserTableID, List<NewsCategoryModel> NewsCategoryModelList)
        {
            Application.Current.Dispatcher.Invoke((Action)(() =>
            {
                try
                {
                    if (UCFollowingUnfollowingView.Instance != null && UCFollowingUnfollowingView.Instance.UserModel != null && UCFollowingUnfollowingView.Instance.UserModel.UserTableID == UserTableID)
                    {
                        foreach (var newsCategoryModel in NewsCategoryModelList)
                        {
                            if (UCFollowingUnfollowingView.Instance.IsEdit)
                            {
                                newsCategoryModel.TempSelected = newsCategoryModel.Subscribed;
                            }
                            UCFollowingUnfollowingView.Instance.StartStatesUtids[newsCategoryModel.CategoryId] = newsCategoryModel.Subscribed;
                            UCFollowingUnfollowingView.Instance.CategoriesofthePortal.Add(newsCategoryModel);
                        }
                        UCFollowingUnfollowingView.Instance.SelectDeselect();
                    }
                }
                catch (Exception ex)
                {
                    log.Error("LoadNewsPortalCategories ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
                }
            }));
        }

        public void HideNextCommentsLbl(Guid newsfeedId, Guid contentId, Guid imageId)
        {
            //Application.Current.Dispatcher.Invoke((Action)(() =>
            //{
            //    try
            //    {
            //        if (imgId != Guid.Empty && basicImageViewWrapper().ucBasicImageView != null
            //        && basicImageViewWrapper().ucBasicImageView.clickedImageID == imgId
            //        && basicImageViewWrapper().ucBasicImageView.ImageComments != null)
            //        {
            //            basicImageViewWrapper().ucBasicImageView.ImageComments.ShowNextCommentsVisible = 2;
            //        }
            //        else if (cntId != Guid.Empty && BasicMediaViewWrapper.ucBasicMediaView != null
            //            && RingPlayer.RingPlayerViewModel.Instance.ClickedMediaID == cntId
            //            && BasicMediaViewWrapper.ucBasicMediaView.ImageComments != null)
            //        {
            //            BasicMediaViewWrapper.ucBasicMediaView.ImageComments.ShowNextCommentsVisible = 2;
            //        }
            //        else
            //        {
            //            FeedModel model = null;
            //            if (FeedDataContainer.Instance.FeedModels.TryGetValue(nfid, out model))
            //                model.ShowNextCommentsVisible = 2;
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        log.Error("HideNextCommentsLbl ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            //    }
            //}));
        }

        public void MediaContentDetails(SingleMediaModel singleItemDetailsModel, Guid cmntID, bool fromNot)
        {
            Application.Current.Dispatcher.Invoke((Action)(() =>
            {
                try
                {
                    //if (!fromNot && BasicMediaViewWrapper.ucBasicMediaView != null && RingPlayer.RingPlayerViewModel.Instance.ClickedMedia != null &&
                    //RingPlayer.RingPlayerViewModel.Instance.ClickedMedia.ContentId == singleItemDetailsModel.ContentId &&
                    //BasicMediaViewWrapper.IsVisible && BasicMediaViewWrapper.ucBasicMediaView.IsVisible)
                    //{
                    //    RingPlayer.RingPlayerViewModel.Instance.ClickedMedia = singleItemDetailsModel;
                    //    if (BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage != null)
                    //        BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.ChangeFullNameProfile();
                    //    if (BasicMediaViewWrapper.ucBasicMediaView.ImageComments != null)
                    //        BasicMediaViewWrapper.ucBasicMediaView.ImageComments.ClearComments();
                    //    if (RingPlayer.RingPlayerViewModel.Instance.ClickedMedia.CommentCount > 0)
                    //    {
                    //        new PreviousOrNextFeedComments().StartThread(0, RingPlayer.RingPlayerViewModel.Instance.ClickedNewsFeedID, singleItemDetailsModel.ContentId, Guid.Empty, AppConstants.TYPE_MEDIA_COMMENT_LIST, Guid.Empty);
                    //    }
                    //    else
                    //        RingPlayer.RingPlayerViewModel.Instance.OnStopAndRemoveAnimationRequested();
                    //}
                    //else
                    //{
                    //    ObservableCollection<SingleMediaModel> list = new ObservableCollection<SingleMediaModel>();
                    //    list.Add(singleItemDetailsModel);
                    //    PlayerHelperMethods.RunPlayListFromNotification(Guid.Empty, cmntID, list, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel);
                    //}
                }
                catch (Exception ex)
                {
                    log.Error("MediaContentDetails ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
                }
            }));
            //if (MainSwitcher.MediaPlayerController.VMMediaPlayer != null && MainSwitcher.MediaPlayerController.VMMediaPlayer.SingleMediaModel.ContentId == singleItemDetailsModel.ContentId)
            //{
            //    MainSwitcher.MediaPlayerController.VMMediaPlayer.ChangeSingleMediaModel(singleItemDetailsModel);
            //}
        }

        //public void EnableAddtoExistingOrNewAlbumPopup(Guid albumId)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //    {
        //        try
        //        {
        //            if (UCMediaViewAddToAlbumPopup.Instance != null)
        //            {
        //                UCMediaViewAddToAlbumPopup.Instance.EnableButtons();
        //            }
        //            if (ucDownloadOrAddToAlbumPopUp != null && albumId != Guid.Empty)
        //            {
        //                ucDownloadOrAddToAlbumPopUp.EnableButtons(albumId);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            log.Error("EnableAddtoExistingOrNewAlbumPopup ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
        //        }
        //    }));
        //}

        public void IncreaseMediaViewCount(Guid contentId, long utId, Guid albumId, int mediaType, short NewAccessCount)
        {
            //Application.Current.Dispatcher.Invoke((Action)(() =>
            //{
            //    try
            //    {
            //        if (BasicMediaViewWrapper != null && BasicMediaViewWrapper.ucBasicMediaView != null &&
            //        RingPlayer.RingPlayerViewModel.Instance.ClickedMedia != null && RingPlayer.RingPlayerViewModel.Instance.ClickedMedia.ContentId == contentId &&
            //        BasicMediaViewWrapper.ucBasicMediaView.IsVisible)
            //        {
            //            RingPlayer.RingPlayerViewModel.Instance.ClickedMedia.AccessCount = NewAccessCount;
            //            if (utId == DefaultSettings.LOGIN_TABLE_ID)
            //            {
            //                ObservableCollection<MediaContentModel> tmp = (mediaType == 1) ? RingIDViewModel.Instance.MyAudioAlbums : RingIDViewModel.Instance.MyVideoAlbums;
            //                MediaContentModel albumModel = tmp.Where(P => P.AlbumId == albumId).FirstOrDefault();
            //                if (albumModel != null && albumModel.MediaList != null && albumModel.MediaList.Count > 0)
            //                {
            //                    SingleMediaModel model = albumModel.MediaList.Where(P => P.ContentId == contentId).FirstOrDefault();
            //                    if (model != null) model.AccessCount = NewAccessCount;
            //                }
            //            }
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        log.Error("IncreaseMediaViewCount ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            //    }

            //}));
            try
            {
                SingleMediaModel clickedMedia = null;
                if (MediaDataContainer.Instance.ContentModels.TryGetValue(contentId, out clickedMedia))
                {
                    NewAccessCount++;
                    clickedMedia.AccessCount = NewAccessCount;
                    MediaDAO.Instance.InsertIntoRecentMediasTable(contentId, clickedMedia.MediaOwner.UserTableID, clickedMedia.Title, clickedMedia.Artist, clickedMedia.StreamUrl, clickedMedia.ThumbUrl, clickedMedia.Duration, clickedMedia.ThumbImageWidth, clickedMedia.ThumbImageHeight, clickedMedia.MediaType, clickedMedia.AlbumId, clickedMedia.AlbumName, clickedMedia.AccessCount, clickedMedia.LastPlayedTime);
                }
            }
            catch (Exception)
            {
            }
        }

        public void LoadMediaSearchTrends()
        {
            Application.Current.Dispatcher.Invoke((Action)(() =>
            {
                try
                {
                    if (UCMiddlePanelSwitcher._UCMediaCloudSearch != null && UCMiddlePanelSwitcher._UCMediaCloudSearch.View_RecentTrending != null)
                    {
                        ObservableCollection<SearchMediaModel> RecentMediaTrends = UCMiddlePanelSwitcher._UCMediaCloudSearch.View_RecentTrending.RecentMediaTrends;
                        if (MediaDictionaries.Instance.MEDIA_TRENDS.Count > 0)
                        {
                            foreach (var item in MediaDictionaries.Instance.MEDIA_TRENDS)
                            {
                                if (!RecentMediaTrends.Any(P => (P.SearchSuggestion.Equals(item.SearchSuggestion) && P.SearchType == item.SearchType)))
                                {
                                    SearchMediaModel model = new SearchMediaModel();
                                    model.LoadData(item);
                                    RecentMediaTrends.Add(model);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("LoadMediaSearchTrends ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
                }
            }));
        }

        public void LoadMediaSearchs(string searchParameter, List<SearchMediaDTO> lst)
        {
            Application.Current.Dispatcher.Invoke((Action)(() =>
            {
                try
                {
                    if (UCMiddlePanelSwitcher._UCMediaCloudSearch != null
                    && UCMiddlePanelSwitcher._UCMediaCloudSearch.CurrentlytxtbxSuggestionParam.Equals(searchParameter))
                    {
                        ObservableCollection<SearchMediaModel> SearchSuggestions = UCMiddlePanelSwitcher._UCMediaCloudSearch.View_Suggestions.SearchSuggestions;
                        foreach (var item in lst)
                        {
                            if (!SearchSuggestions.Any(P => P.SearchSuggestion.Equals(item.SearchSuggestion) && P.SearchType == item.SearchType))
                            {
                                SearchMediaModel searchMediaModel = new SearchMediaModel();
                                searchMediaModel.LoadData(item);
                                SearchSuggestions.Add(searchMediaModel);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("LoadMediaSearchs ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
                }

            }));
        }

        public void InitializeReportConentsView(Guid spamId, int spamType, long spamUserTableId, System.Windows.Controls.Grid motherGrid = null, Func<int> onComplete = null)
        {
            Application.Current.Dispatcher.Invoke((Action)(() =>
            {
                try
                {
                    if (motherGrid == null)
                    {
                        motherGrid = UCGuiRingID.Instance.MotherPanel;
                    }
                    UCReportContentsView ucReportContentsView = new UCReportContentsView(motherGrid, () =>
                    {
                        if (onComplete != null)
                        {
                            onComplete();
                        }
                        return 0;
                    });
                    ucReportContentsView.SetSpamIdAndSpamType(spamId, spamType, spamUserTableId);
                    ucReportContentsView.Show();
                    ucReportContentsView.ShowReportContents();
                    ObservableDictionary<int, string> list = RingIDViewModel.Instance.GetSpamReasonListByType(spamType);
                    if (list.Count == 0)
                    {
                        new ThradSpamReasonList().StartThread(spamType, (status) =>
                        {
                            if (status)
                            {
                            }
                            return 0;
                        });
                    }
                }
                catch (Exception ex)
                {
                    log.Error("InitializeReportConentsView ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
                }

            }));
        }

        public void ShowSingleFeedShareList(Guid nfId, List<UserBasicInfoDTO> list, long minIdOfSequence)
        {
            try
            {
                //if (LikeListViewWrapper.ucLikeListView != null && LikeListViewWrapper.ucLikeListView.NfId == nfId)
                //{
                //    foreach (var item in list)
                //    {
                //        UserBasicInfoModel model = GetExistingorNewUserBasicModelFromDTO(item);
                //        Application.Current.Dispatcher.Invoke(() =>
                //        {
                //            LikeListViewWrapper.ucLikeListView.LikeList.Add(model);
                //        });
                //    }
                //    if (LikeListViewWrapper.ucLikeListView.PvtId == 0 || minIdOfSequence < LikeListViewWrapper.ucLikeListView.PvtId)
                //        LikeListViewWrapper.ucLikeListView.PvtId = minIdOfSequence;
                //}

            }
            catch (Exception e)
            {
                log.Error("********Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void StartHashTagSuggestionThread()
        {
            try
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (HashTagPopUp != null)
                    {
                        HashTagPopUp.StartThread();
                    }
                });
            }
            catch (Exception e)
            {
                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void SubscribeUnsubscribeChannels(int profileType, int SubUnsubscrbType, long pId, string msg)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    if (profileType == SettingsConstants.PROFILE_TYPE_NEWSPORTAL)
                    {
                        NewsPortalModel newsPortalModel = null;
                        if (SubUnsubscrbType == 1)
                        {
                            Thread myThread = new System.Threading.Thread(delegate()
                            {
                                UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.UNSUBSCRIBED_SUCCESSFUL_MESSAGE);
                            });
                            myThread.Start();

                            if (UserProfilesContainer.Instance.NewsPortalModels.TryGetValue(pId, out newsPortalModel))
                            {
                                newsPortalModel.SubscriberCount--;
                                newsPortalModel.IsSubscribed = false;
                                if (UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel != null)
                                {
                                    UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCNewsContainerPanel = null;
                                    FeedDataContainer.Instance.AllNewsPortalFeedsSortedIds.Clear();
                                    //RingIDViewModel.Instance.AllNewsPortalFeeds.RemoveAllModels();
                                    if (UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCFollowingListPanel != null)
                                    {
                                        NewsPortalModel model1 = UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCFollowingListPanel.FollowingNewsPortals.Where(P => P.PortalId == pId).FirstOrDefault();
                                        if (model1 != null)
                                        {
                                            UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCFollowingListPanel.FollowingNewsPortals.Remove(model1);
                                            if (UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCFollowingListPanel.FollowingNewsPortals.Count == 0
                                                && !UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCFollowingListPanel.showMoreBtn.IsVisible)
                                            {
                                                UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCFollowingListPanel.noMoreTxt.Visibility = Visibility.Visible;
                                            }
                                        }
                                    }
                                    if (UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel != null
                                        && !UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel.SearchedNewsPortals.Any(P => P.PortalId == pId))
                                        UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel.SearchedNewsPortals.Insert(0, newsPortalModel);
                                }
                            }
                        }
                        else
                        {
                            if (SubUnsubscrbType == 0)
                            {
                                Thread myThread = new System.Threading.Thread(delegate()
                                {
                                    UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.SUBSCRIBED_TO_CATEGORIES_SUCCESSFUL_MESSAGE);
                                });
                                myThread.Start();
                            }
                            else if (SubUnsubscrbType == 2)
                            {
                                Thread myThread = new System.Threading.Thread(delegate()
                                {
                                    UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.SUBSCRIBED_SUCCESSFUL_MESSAGE);
                                });
                                myThread.Start();
                            }
                            //if (FollowingUnfollowingView != null
                            // && FollowingUnfollowingView.PortalModel != null
                            // && FollowingUnfollowingView.PortalModel.PortalId == pId)
                            //{
                            //    FollowingUnfollowingView.Hide();
                            //}
                            if (UserProfilesContainer.Instance.NewsPortalModels.TryGetValue(pId, out newsPortalModel))
                            {
                                if (SubUnsubscrbType == 2 && !newsPortalModel.IsSubscribed)
                                    newsPortalModel.SubscriberCount++;
                                newsPortalModel.IsSubscribed = true;
                                if (UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel != null)
                                {
                                    UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCNewsContainerPanel = null;
                                    FeedDataContainer.Instance.AllNewsPortalFeedsSortedIds.Clear();
                                    //RingIDViewModel.Instance.AllNewsPortalFeeds.RemoveAllModels();
                                    if (UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel != null)
                                    {
                                        NewsPortalModel model1 = UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel.SearchedNewsPortals.Where(P => P.PortalId == pId).FirstOrDefault();
                                        if (model1 != null)
                                        {
                                            UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel.SearchedNewsPortals.Remove(model1);
                                        }
                                        foreach (var item in UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel.SearchedNewsPortals)
                                        {
                                            int index = UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel.SearchedNewsPortals.IndexOf(item);
                                            if ((index + 1) % 3 == 0)
                                            {
                                                item.IsRightMarginOff = true;
                                            }
                                            else
                                            {
                                                item.IsRightMarginOff = false;
                                            }
                                        }
                                    }
                                    if (UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCFollowingListPanel != null
                                        && !UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCFollowingListPanel.FollowingNewsPortals.Any(P => P.PortalId == pId))
                                    {
                                        UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCFollowingListPanel.FollowingNewsPortals.Insert(0, newsPortalModel);
                                        UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCFollowingListPanel.noMoreTxt.Visibility = Visibility.Collapsed;
                                    }
                                }
                            }
                        }
                    }
                    else if (profileType == SettingsConstants.PROFILE_TYPE_PAGES)
                    {
                        PageInfoModel pageInfoModel = null;
                        if (SubUnsubscrbType == 1)
                        {
                            Thread myThread = new System.Threading.Thread(delegate()
                            {
                                UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.UNSUBSCRIBED_PAGE_SUCCESSFUL_MESSAGE);
                            });
                            myThread.Start();
                            {
                                if (UserProfilesContainer.Instance.PageModels.TryGetValue(pId, out pageInfoModel))
                                {
                                    pageInfoModel.SubscriberCount--;
                                    pageInfoModel.IsSubscribed = false;
                                    if (UCMiddlePanelSwitcher.View_UCPagesMainPanel != null)
                                    {
                                        UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCNewsContainerPanel = null;
                                        FeedDataContainer.Instance.AllPagesFeedsSortedIds.Clear();
                                        //RingIDViewModel.Instance.AllPagesFeeds.RemoveAllModels();

                                        if (UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCFollowingListPanel != null)
                                        {
                                            PageInfoModel model1 = UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCFollowingListPanel.FollowingNewsPages.Where(P => P.PageId == pId).FirstOrDefault();
                                            if (model1 != null)
                                                UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCFollowingListPanel.FollowingNewsPages.Remove(model1);
                                            if (UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCFollowingListPanel.FollowingNewsPages.Count == 0
                                                && !UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCFollowingListPanel.showMoreBtn.IsVisible)
                                            {
                                                UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCFollowingListPanel.noMoreTxt.Visibility = Visibility.Visible;
                                            }
                                        }

                                        if (UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCDiscoverPanel != null
                                             && !UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCDiscoverPanel.SearchedNewsPages.Any(P => P.PageId == pId))
                                            UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCDiscoverPanel.SearchedNewsPages.Insert(0, pageInfoModel);
                                    }
                                }
                            }
                            if (UCMiddlePanelSwitcher.View_UCPageProfile != null && UCMiddlePanelSwitcher.View_UCPageProfile.PageModel != null && UCMiddlePanelSwitcher.View_UCPageProfile.PageModel.PageId == pId)
                            {
                                UCMiddlePanelSwitcher.View_UCPageProfile.FollowUnfollowPlus = "+";
                                UCMiddlePanelSwitcher.View_UCPageProfile.FollowUnfollowTxt = "Follow";
                            }
                        }
                        else
                        {
                            if (SubUnsubscrbType == 0)
                            {
                                Thread myThread = new System.Threading.Thread(delegate()
                                {
                                    UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.SUBSCRIBED_PAGE_TO_CATEGORIES_SUCCESSFUL_MESSAGE);
                                });
                                myThread.Start();
                            }
                            else if (SubUnsubscrbType == 2)
                            {
                                Thread myThread = new System.Threading.Thread(delegate()
                                {
                                    UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.SUBSCRIBED_PAGE_SUCCESSFUL_MESSAGE);
                                });
                                myThread.Start();
                            }
                            //if (DownloadOrAddToAlbumPopUpWrapper.ucFollowingUnfollowingView != null
                            // && DownloadOrAddToAlbumPopUpWrapper.ucFollowingUnfollowingView.PageModel != null
                            // && DownloadOrAddToAlbumPopUpWrapper.ucFollowingUnfollowingView.PageModel.PageId == pId)
                            //{
                            //    DownloadOrAddToAlbumPopUpWrapper.ucFollowingUnfollowingView.Hide();
                            //}
                            if (UserProfilesContainer.Instance.PageModels.TryGetValue(pId, out pageInfoModel))
                            {
                                pageInfoModel.SubscriberCount++;
                                pageInfoModel.IsSubscribed = true;
                                if (UCMiddlePanelSwitcher.View_UCPagesMainPanel != null)
                                {
                                    UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCNewsContainerPanel = null;
                                    FeedDataContainer.Instance.AllPagesFeedsSortedIds.Clear();
                                    //RingIDViewModel.Instance.AllPagesFeeds.RemoveAllModels();

                                    if (UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCDiscoverPanel != null)
                                    {
                                        PageInfoModel model1 = UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCDiscoverPanel.SearchedNewsPages.Where(P => P.PageId == pId).FirstOrDefault();
                                        if (model1 != null)
                                            UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCDiscoverPanel.SearchedNewsPages.Remove(model1);

                                        foreach (var item in UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCDiscoverPanel.SearchedNewsPages)
                                        {
                                            int index = UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCDiscoverPanel.SearchedNewsPages.IndexOf(item);
                                            if ((index + 1) % 3 == 0)
                                            {
                                                item.IsRightMarginOff = true;
                                            }
                                            else
                                            {
                                                item.IsRightMarginOff = false;
                                            }
                                        }
                                    }
                                    if (UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCFollowingListPanel != null
                                         && !UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCFollowingListPanel.FollowingNewsPages.Any(P => P.PageId == pId))
                                    {
                                        UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCFollowingListPanel.FollowingNewsPages.Insert(0, pageInfoModel);
                                        UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCFollowingListPanel.noMoreTxt.Visibility = Visibility.Collapsed;
                                    }
                                }
                            }
                            if (UCMiddlePanelSwitcher.View_UCPageProfile != null && UCMiddlePanelSwitcher.View_UCPageProfile.PageModel != null && UCMiddlePanelSwitcher.View_UCPageProfile.PageModel.PageId == pId)
                            {
                                UCMiddlePanelSwitcher.View_UCPageProfile.FollowUnfollowPlus = "-";
                                UCMiddlePanelSwitcher.View_UCPageProfile.FollowUnfollowTxt = "Unfollow";
                            }
                        }
                    }
                    else if (profileType == SettingsConstants.PROFILE_TYPE_MUSICPAGE)
                    {
                        MusicPageModel musicPageModel = null;
                        if (SubUnsubscrbType == 1)
                        {
                            Thread myThread = new System.Threading.Thread(delegate()
                            {
                                UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.UNSUBSCRIBED_MUSIC_PAGE_SUCCESSFUL_MESSAGE);
                            });
                            myThread.Start();
                            {
                                if (UserProfilesContainer.Instance.MediaPageModels.TryGetValue(pId, out musicPageModel))
                                {
                                    musicPageModel.SubscriberCount--;
                                    musicPageModel.IsSubscribed = false;
                                    if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null)
                                    {
                                        UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudFeedsPanel = null;
                                        FeedDataContainer.Instance.AllMediaPageFeedsSortedIds.Clear();
                                        //RingIDViewModel.Instance.AllMediaPageFeeds.RemoveAllModels();

                                        if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCFollowingMediaCloudPanel != null)
                                        {
                                            MusicPageModel model1 = UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCFollowingMediaCloudPanel.FollowingMusicPages.Where(P => P.MusicPageId == pId).FirstOrDefault();
                                            if (model1 != null)
                                                UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCFollowingMediaCloudPanel.FollowingMusicPages.Remove(model1);
                                            if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCFollowingMediaCloudPanel.FollowingMusicPages.Count == 0
                                                && !UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCFollowingMediaCloudPanel.showMoreBtn.IsVisible)
                                            {
                                                UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCFollowingMediaCloudPanel.noMoreTxt.Visibility = Visibility.Visible;
                                            }
                                        }

                                        if (!RingIDViewModel.Instance.MusicPagesDiscovered.Any(P => P.MusicPageId == pId))
                                            RingIDViewModel.Instance.MusicPagesDiscovered.Insert(0, musicPageModel);
                                    }
                                }
                                FeedDataContainer.Instance.AllMediaPageFeedsSortedIds.Clear();
                                //RingIDViewModel.Instance.AllMediaPageFeeds.RemoveAllModels();
                            }
                        }
                        else
                        {
                            if (SubUnsubscrbType == 0)
                            {
                                Thread myThread = new System.Threading.Thread(delegate()
                                {
                                    UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.SUBSCRIBED_MUSIC_PAGE_TO_CATEGORIES_SUCCESSFUL_MESSAGE);
                                });
                                myThread.Start();
                            }
                            else if (SubUnsubscrbType == 2)
                            {
                                Thread myThread = new System.Threading.Thread(delegate()
                                {
                                    UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.SUBSCRIBED_MUSIC_PAGE_SUCCESSFUL_MESSAGE);
                                });
                                myThread.Start();
                            }
                            //if (DownloadOrAddToAlbumPopUpWrapper.ucFollowingUnfollowingView != null
                            // && DownloadOrAddToAlbumPopUpWrapper.ucFollowingUnfollowingView.MusicModel != null
                            // && DownloadOrAddToAlbumPopUpWrapper.ucFollowingUnfollowingView.MusicModel.MusicPageId == pId)
                            //{
                            //    DownloadOrAddToAlbumPopUpWrapper.ucFollowingUnfollowingView.Hide();
                            //}
                            if (UserProfilesContainer.Instance.MediaPageModels.TryGetValue(pId, out musicPageModel))
                            {
                                musicPageModel.SubscriberCount++;
                                musicPageModel.IsSubscribed = true;
                                if (!RingIDViewModel.Instance.MusicPagesFollowing.Any(P => P.MusicPageId == pId))
                                {
                                    RingIDViewModel.Instance.MusicPagesFollowing.Insert(0, musicPageModel);
                                    if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCFollowingMediaCloudPanel != null)
                                        UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCFollowingMediaCloudPanel.noMoreTxt.Visibility = Visibility.Collapsed;
                                }
                            }
                            MusicPageModel model1 = RingIDViewModel.Instance.MusicPagesDiscovered.Where(P => P.MusicPageId == pId).FirstOrDefault();
                            if (model1 != null)
                                RingIDViewModel.Instance.MusicPagesDiscovered.Remove(model1);
                            if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudDiscoverPanel != null && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudDiscoverPanel.SearchedNewsPages != null)
                            {
                                foreach (var item in UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudDiscoverPanel.SearchedNewsPages)
                                {
                                    int index = UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudDiscoverPanel.SearchedNewsPages.IndexOf(item);
                                    if ((index + 1) % 3 == 0)
                                    {
                                        item.IsRightMarginOff = true;
                                    }
                                    else
                                    {
                                        item.IsRightMarginOff = false;
                                    }
                                }
                            }
                            if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudFeedsPanel != null)
                                UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudFeedsPanel = null;

                            FeedDataContainer.Instance.AllMediaPageFeedsSortedIds.Clear();
                            //RingIDViewModel.Instance.AllMediaPageFeeds.RemoveAllModels();
                        }
                    }
                }
                catch (Exception ex) { log.Error(ex.StackTrace); }
            });
        }

        public void OnListLikesorCommentsFailed()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    //if (LikeListViewWrapper != null && LikeListViewWrapper.ucLikeListView != null)
                    //{
                    //    LikeListViewWrapper.ucLikeListView.HideLoaderandSeeMore();
                    //}
                }
                catch (Exception ex)
                {
                    log.Error("Error: OnListLikesorCommentsFailed() ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            });
        }

        //public void PreviousOrNextFeedComments(int st, long nfid, long cntId = 0, long imgId = 0)
        //{
        //    new PreviousOrNextFeedComments().StartThread(st, nfid, cntId, imgId);
        //}

        #endregion "Utility methods"

        #region "Properties"

        private UCEditedHistoryView ucEditedHistoryView
        {
            get
            {
                return MainSwitcher.PopupController.ucEditedHistoryView;
            }
        }

        private UCAddLocationView ucAddLocationView
        {
            get
            {
                return MainSwitcher.PopupController.ucAddLocationView;
            }
        }

        public UCMediaCloudUploadPopup MediaCloudUploadPopup
        {
            get
            {
                return MainSwitcher.PopupController.ucMediaCloudUploadPopup;
            }
        }

        private UCHashTagPopUp HashTagPopUp
        {
            get
            {
                return MainSwitcher.PopupController.HashTagPopUp;
            }
        }

        private UCAddMemberToGroupPopupWrapper Instance
        {
            get
            {
                return MainSwitcher.PopupController.Instance;
            }
        }
        #endregion"Properties"

        //"Commented functions"
        //public void Process_LIKE_STATUS_184(JObject _JobjFromResponse) //ProcessUpdateLikeStatus(bool MyLike = false)
        //{
        //    //likeStatus_184_384(_JobjFromResponse, true);
        //}

        //public void Process_UNLIKE_STATUS_186(JObject _JobjFromResponse)//(bool MyLike = false)
        //{
        //    //unLikeStatus(_JobjFromResponse, true);
        //}


        //public void Process_UPDATE_LIKE_COMMENT_323(JObject _JobjFromResponse)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true
        //            && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.CommentId] != null)
        //        {
        //            long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
        //            long cmntId = (long)_JobjFromResponse[JsonKeys.CommentId];
        //            long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
        //            long futId = (_JobjFromResponse[JsonKeys.FutId] != null) ? (long)_JobjFromResponse[JsonKeys.FutId] : 0;
        //            UpdateCommentLike(nfId, cmntId, futId, circleId);
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //        log.Error("ProcessUpdateLikeComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

        //    }
        //}

        //public void Process_UPDATE_UNLIKE_COMMENT_325(JObject _JobjFromResponse)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true
        //            && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.CommentId] != null)
        //        {
        //            long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
        //            long cmntId = (long)_JobjFromResponse[JsonKeys.CommentId];
        //            long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
        //            //long friendId = (_JobjFromResponse[JsonKeys.FriendId] != null) ? (long)_JobjFromResponse[JsonKeys.FriendId] : 0;
        //            long futId = (_JobjFromResponse[JsonKeys.FutId] != null) ? (long)_JobjFromResponse[JsonKeys.FutId] : 0;
        //            //lock (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST)
        //            //{
        //            //    Dictionary<long, CommentDTO> commentsOfthisFeed = null;
        //            //    if (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST.TryGetValue(nfId, out commentsOfthisFeed))
        //            //    {
        //            //        CommentDTO dto = null;
        //            //        if (commentsOfthisFeed.TryGetValue(cmntId, out dto))
        //            //        {
        //            //            if (dto.TotalLikeComment > 0) { dto.TotalLikeComment--; dto.ILikeComment = 0; }
        //            //        }
        //            //    }
        //            //}
        //            UpdateCommentUnLike(nfId, cmntId, futId, circleId);
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //        log.Error("ProcessUpdateUnLikeComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

        //    }
        //}

        //public void ProcessMediaAlbumContentList()
        //{
        //    try
        //    {
        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaContentList] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.UserTableID] != null && _JobjFromResponse[JsonKeys.AlbumId] != null)
        //        {
        //            //AlbumMediaContentListSeqCount++;
        //            string sequence = ((string)_JobjFromResponse[JsonKeys.Sequence]);
        //            //int seqTotal = Convert.ToInt32(sequence.Split(new Char[] { '/' })[1]);
        //            //string CurrentSequence = sequence.Split(new Char[] { '/' })[0];
        //            long utid = (long)_JobjFromResponse[JsonKeys.UserTableID];
        //            int mediaType = (int)_JobjFromResponse[JsonKeys.MediaType]; ;
        //            //long uid = (utid == DefaultSettings.LOGIN_TABLE_ID) ? DefaultSettings.LOGIN_USER_ID : FriendDictionaries.Instance.UTID_UID_DICTIONARY[utid];
        //            long albumId = (long)_JobjFromResponse[JsonKeys.AlbumId];
        //            //Dictionary<string, Dictionary<long, SingleMediaDTO>> TEMP_DICTIONARY = null;
        //            //TEMP_DICTIONARY = NewsFeedDictionaries.Instance.TEMP_MY_SINGLE_MEDIA_DETAILS;
        //            Dictionary<long, SingleMediaDTO> TEMP_DICTIONARY = new Dictionary<long, SingleMediaDTO>();
        //            if (_JobjFromResponse[JsonKeys.MediaContentList] != null)
        //            {
        //                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaContentList];
        //                // lock (TEMP_DICTIONARY)
        //                // {
        //                //if (!TEMP_DICTIONARY.ContainsKey(CurrentSequence))
        //                //{
        //                //    TEMP_DICTIONARY.Add(CurrentSequence, new Dictionary<long, SingleMediaDTO>());
        //                //}
        //                foreach (JObject ob in jarray)
        //                {
        //                    SingleMediaDTO mediaDTO = HelperMethodsModel.BindSingleMediaDTO(ob);
        //                    mediaDTO.UserTableID = utid;
        //                    mediaDTO.AlbumId = albumId;
        //                    //mediaType = imageDTO.MediaType;
        //                    //if (TEMP_DICTIONARY[CurrentSequence].ContainsKey(imageDTO.ContentId))
        //                    //{
        //                    //    TEMP_DICTIONARY[CurrentSequence][imageDTO.ContentId] = imageDTO;
        //                    //}
        //                    //else
        //                    //{
        //                    //    TEMP_DICTIONARY[CurrentSequence].Add(imageDTO.ContentId, imageDTO);
        //                    //}
        //                    TEMP_DICTIONARY[mediaDTO.ContentId] = mediaDTO;
        //                }
        //                //}
        //            }

        //            // if (TEMP_DICTIONARY.Count == seqTotal)
        //            // {
        //            //int mediaType = TEMP_DICTIONARY.ElementAt(0).Value.MediaType;
        //            Dictionary<long, Dictionary<long, MediaContentDTO>> DICT_BY_UTID = (mediaType == 1) ? NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID : NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID;
        //            Dictionary<long, MediaContentDTO> albumsOfaUtId = null;
        //            if (!DICT_BY_UTID.TryGetValue(utid, out albumsOfaUtId)) { albumsOfaUtId = new Dictionary<long, MediaContentDTO>(); DICT_BY_UTID.Add(utid, albumsOfaUtId); }
        //            MediaContentDTO album = null;
        //            if (albumsOfaUtId.TryGetValue(albumId, out album) && album.MediaList == null) album.MediaList = new List<SingleMediaDTO>();
        //            List<SingleMediaDTO> lst = new List<SingleMediaDTO>();
        //            // foreach (Dictionary<long, SingleMediaDTO> val in TEMP_DICTIONARY.Values)
        //            //{
        //            //    if (album != null) album.MediaList.AddRange(val.Values.ToList());
        //            //    lst.AddRange(val.Values.ToList());
        //            //}
        //            if (album != null) album.MediaList.AddRange(TEMP_DICTIONARY.Values.ToList());
        //            lst.AddRange(TEMP_DICTIONARY.Values.ToList());

        //            LoadItemsOfAnAlbum(lst, albumId, mediaType, utid, (album != null) ? album.TotalMediaCount : 0);//album.MediaList
        //            //System.Diagnostics.Debug.WriteLine("ALBUM CONTENT LIST.." + AlbumMediaContentListSeqCount + "SEQTOTAL.." + seqTotal);                       
        //            TEMP_DICTIONARY.Clear();
        //            // }
        //        }
        //        else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false && _JobjFromResponse[JsonKeys.AlbumId] != null)
        //        {
        //            long albumId = (long)_JobjFromResponse[JsonKeys.AlbumId];
        //            NoMoreItemsInHashTagOrAlbum(albumId, 0);
        //            NoItemFoundInUCSingleAlbumSong();
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        log.Error("ProcessMediaAlbumContentList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);

        //    }
        //}

        //public void Process_UPDATE_LIKEUNLIKE_MEDIA_464(JObject _JobjFromResponse)
        //{
        //    //Received for action=464 from Auth JSON==> {"uId":"2110010086","sucs":true,"fn":"sirat samyoun","cntntId":1894,"lkd":1,"loc":1,"mdaT":2}
        //    try
        //    {
        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaType] != null
        //            && _JobjFromResponse[JsonKeys.ContentId] != null && _JobjFromResponse[JsonKeys.Liked] != null && _JobjFromResponse[JsonKeys.UserTableID] != null)
        //        {
        //            long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
        //            long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
        //            int mediaType = (int)_JobjFromResponse[JsonKeys.MediaType];
        //            int liked = (int)_JobjFromResponse[JsonKeys.Liked];
        //            int loc = (int)_JobjFromResponse[JsonKeys.LikeOrComment];
        //            long utId = (long)_JobjFromResponse[JsonKeys.UserTableID];

        //            Dictionary<long, Dictionary<long, MediaContentDTO>> DICT_BY_UTID = (mediaType == 1) ? NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID : NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID;
        //            Dictionary<long, MediaContentDTO> albumsOfaUtId = null;
        //            if (DICT_BY_UTID.TryGetValue(utId, out albumsOfaUtId))
        //            {
        //                for (int i = 0; i < albumsOfaUtId.Count; i++)
        //                {
        //                    if (albumsOfaUtId[i].MediaList != null && albumsOfaUtId[i].MediaList.Count > 0)
        //                    {
        //                        SingleMediaDTO singleMedia = albumsOfaUtId[i].MediaList.Where(P => P.ContentId == contentId).FirstOrDefault();
        //                        if (singleMedia != null)
        //                        {
        //                            if (liked == 1) singleMedia.LikeCount++;
        //                            else if (singleMedia.LikeCount > 0)
        //                                singleMedia.LikeCount--;
        //                            break;
        //                        }
        //                    }
        //                }
        //            }

        //            UpdateLikeUnlikeMedia(loc, liked, contentId, nfid);
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //        log.Error("ProcessUpdateLikeUnlikeMedia ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

        //    }
        //}

        //public void UpdateFeedModelsByContentId(long contentId, SingleMediaDTO singleMediaDto = null)
        //{

        //    new System.Threading.Thread(delegate()
        //    {
        //        try
        //        {
        //            //lock (RingIDViewModel.Instance.NewsFeeds)
        //            //{
        //            List<FeedModel> newsFeedModelList = RingIDViewModel.Instance.NewsFeeds.ToList();

        //            foreach (FeedModel model in newsFeedModelList)
        //            {
        //                if (model.SingleMediaFeedModel != null && model.SingleMediaFeedModel.ContentId == contentId)
        //                {
        //                    if (singleMediaDto != null)
        //                    {
        //                        model.SingleMediaFeedModel.LoadData(singleMediaDto);
        //                        model.ILike = singleMediaDto.ILike; model.NumberOfLikes = singleMediaDto.LikeCount;
        //                        model.IComment = singleMediaDto.IComment; model.NumberOfComments = singleMediaDto.CommentCount;
        //                        model.IShare = singleMediaDto.IShare; model.NumberOfShares = singleMediaDto.ShareCount;
        //                    }
        //                    model.OnPropertyChanged("CurrentInstance");
        //                }
        //            }
        //            //}

        //            // lock (RingIDViewModel.Instance.MyNewsFeeds)
        //            //  {
        //            foreach (FeedModel model in newsFeedModelList)
        //            {
        //                if (model.SingleMediaFeedModel != null && model.SingleMediaFeedModel.ContentId == contentId)
        //                {
        //                    if (singleMediaDto != null)
        //                    {
        //                        model.SingleMediaFeedModel.LoadData(singleMediaDto);
        //                        model.ILike = singleMediaDto.ILike; model.NumberOfLikes = singleMediaDto.LikeCount;
        //                        model.IComment = singleMediaDto.IComment; model.NumberOfComments = singleMediaDto.CommentCount;
        //                        model.IShare = singleMediaDto.IShare; model.NumberOfShares = singleMediaDto.ShareCount;
        //                    }
        //                    model.OnPropertyChanged("CurrentInstance");
        //                }
        //            }

        //            foreach (FeedModel model in RingIDViewModel.Instance.MediaFeeds)
        //            {
        //                if (model.SingleMediaFeedModel != null && model.SingleMediaFeedModel.ContentId == contentId)
        //                {
        //                    if (singleMediaDto != null)
        //                    {
        //                        model.SingleMediaFeedModel.LoadData(singleMediaDto);
        //                        model.ILike = singleMediaDto.ILike; model.NumberOfLikes = singleMediaDto.LikeCount;
        //                        model.IComment = singleMediaDto.IComment; model.NumberOfComments = singleMediaDto.CommentCount;
        //                        model.IShare = singleMediaDto.IShare; model.NumberOfShares = singleMediaDto.ShareCount;
        //                    }
        //                    model.OnPropertyChanged("CurrentInstance");
        //                }
        //            }

        //            foreach (FeedModel model in RingIDViewModel.Instance.MediaFeedsTrending)
        //            {
        //                if (model.SingleMediaFeedModel != null && model.SingleMediaFeedModel.ContentId == contentId)
        //                {
        //                    if (singleMediaDto != null)
        //                    {
        //                        model.SingleMediaFeedModel.LoadData(singleMediaDto);
        //                        model.ILike = singleMediaDto.ILike; model.NumberOfLikes = singleMediaDto.LikeCount;
        //                        model.IComment = singleMediaDto.IComment; model.NumberOfComments = singleMediaDto.CommentCount;
        //                        model.IShare = singleMediaDto.IShare; model.NumberOfShares = singleMediaDto.ShareCount;
        //                    }
        //                    model.OnPropertyChanged("CurrentInstance");
        //                }
        //            }
        //            //}

        //            foreach (UCFriendProfile _UCFriendProfile in UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.Values)
        //            {
        //                if (_UCFriendProfile != null && _UCFriendProfile._UCFriendNewsFeeds != null)
        //                {
        //                    //  lock (_UCFriendProfile._UCFriendNewsFeeds.FriendNewsFeeds)
        //                    //  {
        //                    //foreach (FeedModel model in _UCFriendProfile._UCFriendNewsFeeds.FriendNewsFeeds)
        //                    //{
        //                    //    if (model.SingleMediaFeedModel != null && model.SingleMediaFeedModel.ContentId == contentId)
        //                    //    {
        //                    //        if (singleMediaDto != null)
        //                    //        {
        //                    //            model.SingleMediaFeedModel.LoadData(singleMediaDto);
        //                    //            model.ILike = singleMediaDto.ILike; model.NumberOfLikes = singleMediaDto.LikeCount;
        //                    //            model.IComment = singleMediaDto.IComment; model.NumberOfComments = singleMediaDto.CommentCount;
        //                    //            model.IShare = singleMediaDto.IShare; model.NumberOfShares = singleMediaDto.ShareCount;
        //                    //        }
        //                    //        model.OnPropertyChanged("CurrentInstance");
        //                    //    }
        //                    //}
        //                    // }
        //                }
        //            }

        //            //foreach (UCCirclePanel _UCCircle in UIDictionaries.Instance.CIRCLE_PANEL_DICTIONARY.Values)
        //            //{
        //            if (UCMiddlePanelSwitcher.View_UCCirclePanel != null && UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds != null)
        //            {
        //                //lock (UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.CircleNewsFeeds)
        //                //{
        //                //    foreach (FeedModel model in UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.CircleNewsFeeds)
        //                //    {
        //                //        if (model.SingleMediaFeedModel != null && model.SingleMediaFeedModel.ContentId == contentId)
        //                //        {
        //                //            if (singleMediaDto != null)
        //                //            {
        //                //                model.SingleMediaFeedModel.LoadData(singleMediaDto);
        //                //                model.ILike = singleMediaDto.ILike; model.NumberOfLikes = singleMediaDto.LikeCount;
        //                //                model.IComment = singleMediaDto.IComment; model.NumberOfComments = singleMediaDto.CommentCount;
        //                //                model.IShare = singleMediaDto.IShare; model.NumberOfShares = singleMediaDto.ShareCount;
        //                //            }
        //                //            model.OnPropertyChanged("CurrentInstance");
        //                //        }
        //                //    }
        //                //}
        //            }

        //            if (UCSingleFeedDetails.Instance != null && UCSingleFeedDetails.Instance.FeedModel != null && UCSingleFeedDetails.Instance.FeedModel.SingleMediaFeedModel != null && UCSingleFeedDetails.Instance.FeedModel.SingleMediaFeedModel.ContentId == contentId)
        //            {
        //                if (singleMediaDto != null)
        //                {
        //                    UCSingleFeedDetails.Instance.FeedModel.SingleMediaFeedModel.LoadData(singleMediaDto);
        //                    UCSingleFeedDetails.Instance.FeedModel.ILike = singleMediaDto.ILike; UCSingleFeedDetails.Instance.FeedModel.NumberOfLikes = singleMediaDto.LikeCount;
        //                    UCSingleFeedDetails.Instance.FeedModel.IComment = singleMediaDto.IComment; UCSingleFeedDetails.Instance.FeedModel.NumberOfComments = singleMediaDto.CommentCount;
        //                    UCSingleFeedDetails.Instance.FeedModel.IShare = singleMediaDto.IShare; UCSingleFeedDetails.Instance.FeedModel.NumberOfShares = singleMediaDto.ShareCount;
        //                }

        //                UCSingleFeedDetails.Instance.FeedModel.OnPropertyChanged("CurrentInstance");
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            log.Error(ex.StackTrace + ex.Message);
        //        }
        //    }).Start();
        //}
        //public void NodataFoundLikeList()
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //    {
        //        try
        //        {
        //            if (LikeListViewWrapper != null && LikeListViewWrapper.ucLikeListView != null)
        //            {
        //                LikeListViewWrapper.ucLikeListView.HideLoaderandSeeMore();
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            log.Error("Error: LoadLikeList() => " + ex.Message + "\n" + ex.StackTrace);
        //        }
        //    }));
        //}

        //public void LoadImageLikes(long imgId, List<UserBasicInfoDTO> users)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //    {
        //        try
        //        {
        //            foreach (UserBasicInfoDTO user in users)
        //            {
        //                UserBasicInfoModel model = GetExistingorNewUserBasicModelFromDTO(user);
        //                if (LikeListViewWrapper.ucLikeListView != null && LikeListViewWrapper.ucLikeListView != null &&
        //                    !LikeListViewWrapper.ucLikeListView.LikeList.Any(P => P.ShortInfoModel.UserIdentity == model.ShortInfoModel.UserIdentity))// && UCLikeListPopup.CurrentInstance.ImageId == imgId && UCLikeListPopup.CurrentInstance.type == 2)
        //                {
        //                    LikeListViewWrapper.ucLikeListView.LikeList.Add(model);
        //                }
        //            }
        //            if (LikeListViewWrapper != null && LikeListViewWrapper.ucLikeListView != null)
        //                LikeListViewWrapper.ucLikeListView.SetVisibilities();
        //        }
        //        catch (Exception ex)
        //        {
        //            log.Error("Error: LoadImageLikes() => " + ex.Message + "\n" + ex.StackTrace);
        //        }
        //    }));
        //}
        //public Object GetFeedModelByNfid(long nfId, long frndId = 0, long circleId = 0)
        //{
        //    FeedModel model = null;

        //    model = RingIDViewModel.Instance.NewsFeeds.GetModel(nfId);

        //    if (model == null)
        //    {
        //        model = RingIDViewModel.Instance.MediaFeeds.GetModel(nfId);
        //    }
        //    if (model == null)
        //    {
        //        model = RingIDViewModel.Instance.MediaFeedsTrending.GetModel(nfId);
        //    }

        //    if (model == null)
        //    {
        //        model = RingIDViewModel.Instance.MyNewsFeeds.GetModel(nfId);
        //    }

        //    if (model == null)
        //    {
        //        if (frndId > 0)
        //        {
        //            UCFriendProfile friendProfile = null;
        //            if (UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.TryGetValue(frndId, out friendProfile) && friendProfile._UCFriendNewsFeeds != null)
        //            {
        //                //model = friendProfile._UCFriendNewsFeeds.FriendNewsFeeds.GetModel(nfId);
        //            }
        //        }
        //        else
        //            foreach (UCFriendProfile _UCFriendProfile in UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.Values)
        //            {
        //                if (model == null && _UCFriendProfile != null && _UCFriendProfile._UCFriendNewsFeeds != null)
        //                {
        //                    //model = _UCFriendProfile._UCFriendNewsFeeds.FriendNewsFeeds.GetModel(nfId);
        //                }
        //                if (model != null)
        //                {
        //                    break;
        //                }
        //            }
        //    }

        //    if (model == null)
        //    {
        //        if (circleId > 0)
        //        {
        //            //UCCirclePanel circlePanel = null;
        //            //if (UIDictionaries.Instance.CIRCLE_PANEL_DICTIONARY.TryGetValue(circleId, out circlePanel) && circlePanel._UCCircleNewsFeeds != null)
        //            //{
        //            //    model = circlePanel._UCCircleNewsFeeds.CircleNewsFeeds.GetModel(nfId);
        //            //}
        //            if (UCMiddlePanelSwitcher.View_UCCirclePanel != null && UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds != null)
        //            {
        //                //model = UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.CircleNewsFeeds.GetModel(nfId);
        //            }
        //        }
        //        else
        //            //foreach (UCCirclePanel _UCCircle in UIDictionaries.Instance.CIRCLE_PANEL_DICTIONARY.Values)
        //            //{
        //            if (model == null && UCMiddlePanelSwitcher.View_UCCirclePanel != null && UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds != null)
        //            {
        //                //model = UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.CircleNewsFeeds.GetModel(nfId);
        //            }
        //        if (model != null)
        //        {
        //            //break;
        //            //return;
        //        }
        //        //}
        //    }

        //    if (model == null)
        //    {
        //        if (UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel != null && UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel != null)
        //        {
        //            if (UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCNewsContainerPanel != null)
        //            {
        //                //model = UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCNewsContainerPanel.NewsPortalFeeds.GetModel(nfId);

        //                if (model == null)
        //                {
        //                    model = UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCNewsContainerPanel.BreakingNewsPortalFeeds.Where(P => P.NewsfeedId == nfId).FirstOrDefault();
        //                }
        //            }
        //            //if (model == null && UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCSavedContentPanel != null)
        //            //    model = UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCSavedContentPanel.SavedNewsPortalFeeds.GetModel(nfId);
        //        }
        //    }
        //    if (model == null)
        //    {
        //        if (UCMiddlePanelSwitcher.View_UCNewsPortalProfile != null)
        //        {
        //            //model = UCMiddlePanelSwitcher.View_UCNewsPortalProfile.NewsFeeds.GetModel(nfId);
        //        }
        //    }
        //    if (model == null)
        //    {
        //        if (UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel != null && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel != null)
        //        {
        //            if (UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCNewsContainerPanel != null)
        //            {
        //                //model = UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCNewsContainerPanel.PagesFeeds.GetModel(nfId);
        //            }
        //            //if (model == null && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCSavedContentPanel != null)
        //            //    model = UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCSavedContentPanel.SavedPagesFeeds.GetModel(nfId);
        //        }
        //    }
        //    if (model == null)
        //    {

        //        if (UCMiddlePanelSwitcher.View_UCPageProfile != null)
        //        {
        //            //model = UCMiddlePanelSwitcher.View_UCPageProfile.NewsFeeds.GetModel(nfId);
        //        }
        //    }
        //    if (model == null)
        //    {
        //        if (UCSingleFeedDetails.Instance != null && UCSingleFeedDetails.Instance.FeedModel != null && UCSingleFeedDetails.Instance.FeedModel.NewsfeedId == nfId)
        //        {
        //            model = UCSingleFeedDetails.Instance.FeedModel;
        //        }
        //    }
        //    if (model == null)
        //    {
        //        if (PopupViewWrapper != null && PopupViewWrapper.IsVisible && PopupViewWrapper.ucSingleFeedDetailsView != null &&
        //            PopupViewWrapper.ucSingleFeedDetailsView.FeedModel != null && PopupViewWrapper.ucSingleFeedDetailsView.FeedModel.NewsfeedId == nfId)
        //        {
        //            model = PopupViewWrapper.ucSingleFeedDetailsView.FeedModel;
        //        }
        //    }
        //    if (model == null)
        //        model = RingIDViewModel.Instance.ShareListFeedModel.Where(P => P.NewsfeedId == nfId).FirstOrDefault();

        //    return model;
        //}

        //public Object GetFeedModel(FeedDTO feedDTO)
        //{
        //    FeedModel model = null;

        //    model = RingIDViewModel.Instance.NewsFeeds.GetModel(feedDTO.NewsfeedId);

        //    if (model == null)
        //    {
        //        model = RingIDViewModel.Instance.MediaFeeds.GetModel(feedDTO.NewsfeedId);
        //    }
        //    if (model == null)
        //    {
        //        model = RingIDViewModel.Instance.MediaFeedsTrending.GetModel(feedDTO.NewsfeedId);
        //    }

        //    if (model == null)
        //    {
        //        model = RingIDViewModel.Instance.MyNewsFeeds.GetModel(feedDTO.NewsfeedId);
        //    }

        //    if (model == null)
        //    {
        //        foreach (UCFriendProfile _UCFriendProfile in UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.Values)
        //        {
        //            if (model == null && _UCFriendProfile != null && _UCFriendProfile._UCFriendNewsFeeds != null)
        //            {
        //                //model = _UCFriendProfile._UCFriendNewsFeeds.FriendNewsFeeds.GetModel(feedDTO.NewsfeedId);
        //            }
        //            if (model != null)
        //            {
        //                break;
        //            }
        //        }
        //    }

        //    if (model == null)
        //    {
        //        //foreach (UCCirclePanel _UCCircle in UIDictionaries.Instance.CIRCLE_PANEL_DICTIONARY.Values)
        //        //{
        //        if (model == null && UCMiddlePanelSwitcher.View_UCCirclePanel != null && UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds != null)
        //        {
        //            //model = UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.CircleNewsFeeds.GetModel(feedDTO.NewsfeedId);
        //        }
        //        if (model != null)
        //        {
        //            // break;
        //        }
        //        //}
        //    }

        //    if (model == null)
        //    {
        //        if (UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel != null && UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel != null)
        //        {
        //            if (UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCNewsContainerPanel != null)
        //            {
        //                //model = UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCNewsContainerPanel.NewsPortalFeeds.GetModel(feedDTO.NewsfeedId);

        //                //if (model == null)
        //                //{
        //                //    model = UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCNewsContainerPanel.BreakingNewsPortalFeeds.Where(P => P.NewsfeedId == feedDTO.NewsfeedId).FirstOrDefault();
        //                //}
        //            }
        //            // if (model == null && UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCSavedContentPanel != null)
        //            //     model = UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCSavedContentPanel.SavedNewsPortalFeeds.GetModel(feedDTO.NewsfeedId);
        //        }
        //    }
        //    if (model == null)
        //    {

        //        if (UCMiddlePanelSwitcher.View_UCNewsPortalProfile != null)
        //        {
        //            //model = UCMiddlePanelSwitcher.View_UCNewsPortalProfile.NewsFeeds.GetModel(feedDTO.NewsfeedId);
        //        }
        //    }
        //    if (model == null)
        //    {
        //        if (UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel != null && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel != null)
        //        {
        //            if (UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCNewsContainerPanel != null)
        //            {
        //                //model = UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCNewsContainerPanel.PagesFeeds.GetModel(feedDTO.NewsfeedId);
        //            }
        //            // if (model == null && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCSavedContentPanel != null)
        //            //    model = UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCSavedContentPanel.SavedPagesFeeds.GetModel(feedDTO.NewsfeedId);
        //        }
        //    }
        //    if (model == null)
        //    {

        //        if (UCMiddlePanelSwitcher.View_UCPageProfile != null)
        //        {
        //            //model = UCMiddlePanelSwitcher.View_UCPageProfile.NewsFeeds.GetModel(feedDTO.NewsfeedId);
        //        }
        //    }
        //    if (model == null)
        //    {
        //        if (UCSingleFeedDetails.Instance != null && UCSingleFeedDetails.Instance.FeedModel != null && UCSingleFeedDetails.Instance.FeedModel.NewsfeedId == feedDTO.NewsfeedId)
        //        {
        //            model = UCSingleFeedDetails.Instance.FeedModel;
        //        }
        //    }
        //    if (model == null)
        //    {
        //        if (PopupViewWrapper != null && PopupViewWrapper.IsVisible && PopupViewWrapper.ucSingleFeedDetailsView != null && PopupViewWrapper.ucSingleFeedDetailsView.FeedModel != null && PopupViewWrapper.ucSingleFeedDetailsView.FeedModel.NewsfeedId == feedDTO.NewsfeedId)
        //        {
        //            model = PopupViewWrapper.ucSingleFeedDetailsView.FeedModel;
        //        }
        //    }

        //    if (model == null)
        //    {
        //        model = new FeedModel();
        //        model.LoadData(feedDTO);
        //    }
        //    else
        //    {
        //        model.LoadData(feedDTO);
        //        if (feedDTO.ParentFeed != null && model.ParentFeed == null)
        //        {
        //            model.LoadParent(feedDTO.ParentFeed);
        //        }
        //    }

        //    return model;
        //}

        //public void UpdateFeedModelWhoShareList(long nfId, FeedDTO feedDTO)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)delegate
        //    {
        //        FeedModel model = (FeedModel)GetFeedModelByNfid(nfId);
        //        if (model != null)
        //        {
        //            if (feedDTO.WhoShareList != null && (model.WhoShareList == null || (model.WhoShareList != null && model.WhoShareList.Count != feedDTO.WhoShareList.Count)))
        //            {
        //                model.NumberOfShares = feedDTO.NumberOfShares;
        //                model.LoadWhoShare(feedDTO, model);
        //                if (feedDTO.WhoShareList.Count > 0)
        //                {
        //                    if (model.WhoShareList == null) model.WhoShareList = new ObservableCollection<FeedModel>();
        //                    foreach (var item in feedDTO.WhoShareList)
        //                    {
        //                        FeedModel fm = model.WhoShareList.Where(P => P.NewsfeedId == item.NewsfeedId).FirstOrDefault();
        //                        if (fm == null)
        //                        {
        //                            fm = new FeedModel();
        //                            model.WhoShareList.Add(fm);
        //                        }
        //                        fm.LoadData(item);
        //                        // Application.Current.Dispatcher.Invoke((Action)delegate
        //                        // {

        //                        //}, DispatcherPriority.Send);
        //                    }
        //                }
        //                model.OnPropertyChanged("CurrentInstance");
        //            }
        //        }
        //    }, DispatcherPriority.Send);
        //}

        //public Object GetNewsFeedsByType(int type, long friendIdOrCircleId = 0)
        //{
        //    CustomObservableCollection Feeds = null;

        //    if (type == DefaultSettings.FEED_TYPE_ALL)
        //    {
        //        Feeds = RingIDViewModel.Instance.NewsFeeds;
        //    }
        //    //else if (type == DefaultSettings.FEED_TYPE_MEDIA)
        //    //{
        //    //    Feeds = RingIDViewModel.Instance.MediaFeeds;
        //    //}
        //    //else if (type == DefaultSettings.FEED_TYPE_MEDIA_TRENDING)
        //    //{
        //    //    Feeds = RingIDViewModel.Instance.MediaFeedsTrending;
        //    //}
        //    else if (type == DefaultSettings.FEED_TYPE_MY)
        //    {
        //        Feeds = RingIDViewModel.Instance.MyNewsFeeds;
        //    }
        //    else if (type == DefaultSettings.FEED_TYPE_FRIEND && friendIdOrCircleId > 0)
        //    {
        //        if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendNewsFeeds != null
        //            && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendIdentity == friendIdOrCircleId)
        //        {
        //            // Feeds = UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendNewsFeeds.FriendNewsFeeds;
        //        }
        //    }
        //    else if (type == DefaultSettings.FEED_TYPE_CIRCLE && friendIdOrCircleId > 0)
        //    {
        //        if (UCMiddlePanelSwitcher.View_UCCirclePanel != null && UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds != null)
        //        {
        //            //Feeds = UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.CircleNewsFeeds;
        //        }
        //    }
        //    else if (type == DefaultSettings.FEED_TYPE_NEWSPORTAL)
        //    {
        //        if (UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel != null && UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel != null
        //            && UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCNewsContainerPanel != null)
        //        {
        //            //Feeds = UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCNewsContainerPanel.NewsPortalFeeds;
        //        }
        //    }
        //    else if (type == DefaultSettings.FEED_TYPE_NEWSPORTAL_PROFILE)
        //    {
        //        if (UCMiddlePanelSwitcher.View_UCNewsPortalProfile != null && UCMiddlePanelSwitcher.View_UCNewsPortalProfile.PortalUserIdentity == friendIdOrCircleId)
        //        {
        //            //Feeds = UCMiddlePanelSwitcher.View_UCNewsPortalProfile.NewsFeeds;
        //        }
        //    }
        //    else if (type == DefaultSettings.FEED_TYPE_NEWSPORTAL_SAVED)
        //    {
        //        if (UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel != null && UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel != null
        //            && UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCSavedContentPanel != null)
        //        {
        //            //Feeds = UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCSavedContentPanel.SavedNewsPortalFeeds;
        //        }
        //    }
        //    else if (type == DefaultSettings.FEED_TYPE_PAGE)
        //    {
        //        if (UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel != null && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel != null
        //                && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCNewsContainerPanel != null)
        //        {
        //            //Feeds = UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCNewsContainerPanel.PagesFeeds;
        //        }
        //    }
        //    else if (type == DefaultSettings.FEED_TYPE_PAGE_PROFILE)
        //    {
        //        if (UCMiddlePanelSwitcher.View_UCPageProfile != null && UCMiddlePanelSwitcher.View_UCPageProfile.PageUserIdentity == friendIdOrCircleId)
        //        {
        //            //Feeds = UCMiddlePanelSwitcher.View_UCPageProfile.NewsFeeds;
        //        }
        //    }
        //    else if (type == DefaultSettings.FEED_TYPE_PAGE_SAVED)
        //    {
        //        if (UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel != null && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel != null
        //                && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCSavedContentPanel != null)
        //        {
        //            //Feeds = UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCSavedContentPanel.SavedPagesFeeds;
        //        }
        //    }
        //    else if (type == DefaultSettings.FEED_TYPE_SAVED_ALL)
        //    {
        //        Feeds = RingIDViewModel.Instance.SavedFeeds;
        //    }
        //    else if (type == DefaultSettings.FEED_TYPE_SAVED_PORTAL)
        //    {
        //        // Feeds = RingIDViewModel.Instance.SavedFeeds;
        //    }
        //    else if (type == DefaultSettings.FEED_TYPE_SAVED_ALL)
        //    {
        //        // Feeds = RingIDViewModel.Instance.SavedFeeds;
        //    }
        //    //TODO
        //    return Feeds;
        //}

        //public void InsertSingleFeedUI(int feed_state, FeedDTO feedDTO, int type, long friendIdOrCircleId = 0)
        //{
        //    try
        //    {
        //        switch (feed_state)
        //        {
        //            case SettingsConstants.FEED_REGULAR:
        //                FeedLoadUtility.Instance.LoadData(feedDTO, type, friendIdOrCircleId, false, false);
        //                break;
        //            case SettingsConstants.FEED_FIRSTTIME:
        //                FeedLoadUtility.Instance.LoadData(feedDTO, type, friendIdOrCircleId, false, true);
        //                break;
        //            case SettingsConstants.FEED_ADDORUPDATE:
        //                FeedModel feedModel = (FeedModel)GetFeedModel(feedDTO);
        //                if (type != DefaultSettings.FEED_TYPE_ALL)
        //                {
        //                    CustomObservableCollection Feeds = (CustomObservableCollection)GetNewsFeedsByType(type, friendIdOrCircleId);
        //                    if (Feeds != null)
        //                    {
        //                        Feeds.InsertModel(feedModel);
        //                    }
        //                }
        //                else if (UCAllFeeds.Instance != null)
        //                {
        //                    UCAllFeeds.Instance.InsertModel(feedModel, true);
        //                    int counter = 0;
        //                    List<FeedModel> list = RingIDViewModel.Instance.NewsFeeds.ToList();
        //                    foreach (FeedModel singlemodel in list)
        //                    {
        //                        singlemodel.SequenceForAllFeeds = counter++;
        //                    }
        //                }
        //                break;
        //        }

        //        if (feedDTO.FeedCategory == SettingsConstants.SPECIAL_FEED)
        //        {
        //            ChatHelpers.AddFeedMessage(feedDTO);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error(ex.StackTrace + ex.Message);
        //    }
        //}

        //public void InsertSingleShareFeedUI(Object feedModel1, int type, long friendIdOrCircleId = 0)
        //{
        //    try
        //    {
        //        FeedModel feedModel = (FeedModel)feedModel1;
        //        if (type != DefaultSettings.FEED_TYPE_ALL)
        //        {
        //            CustomObservableCollection Feeds = (CustomObservableCollection)GetNewsFeedsByType(type, friendIdOrCircleId);
        //            if (Feeds != null)
        //            {
        //                Feeds.InsertModel(feedModel);
        //            }
        //        }
        //        else if (UCAllFeeds.Instance != null)
        //        {
        //            UCAllFeeds.Instance.InsertModel(feedModel, false);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("ex in InsertSingleForAllFeeds==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }

        //}

        //public void RemoveSingleFeedUI(long nfId, int type, long friendIdOrCircleId = 0, bool iDelete = false)
        //{
        //    try
        //    {
        //        if (type == DefaultSettings.FEED_TYPE_ALL)
        //        {
        //            if (UCAllFeeds.Instance != null)
        //                UCAllFeeds.Instance.RemoveModel(nfId, iDelete);
        //        }
        //        else
        //        {
        //            CustomObservableCollection Feeds = (CustomObservableCollection)GetNewsFeedsByType(type, friendIdOrCircleId);
        //            if (Feeds != null)
        //                Feeds.RemoveModel(nfId, iDelete);
        //        }

        //        //shared feeds delete everywhere
        //        if (NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY.ContainsValue(nfId))
        //        {
        //            List<long> deleteNfids = new List<long>();

        //            lock (NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY)
        //            {
        //                foreach (KeyValuePair<long, long> tuple in NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY)
        //                {
        //                    if (tuple.Value == nfId)
        //                    {
        //                        deleteNfids.Add(tuple.Key);
        //                    }
        //                }
        //            }

        //            if (deleteNfids.Count > 0)
        //            {
        //                foreach (long id in deleteNfids)
        //                {
        //                    FeedModel model = null;
        //                    model = RingIDViewModel.Instance.NewsFeeds.GetModel(id);
        //                    if (model != null)
        //                    {
        //                        UCAllFeeds.Instance.RemoveModel(id, iDelete);
        //                        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.RemoveData(id);
        //                        model = null;
        //                    }
        //                    model = RingIDViewModel.Instance.MyNewsFeeds.GetModel(id);
        //                    if (model != null)
        //                    {
        //                        RingIDViewModel.Instance.MyNewsFeeds.RemoveModel(id, iDelete);
        //                        NewsFeedDictionaries.Instance.MY_NEWS_FEEDS_ID_SORTED_BY_TIME.RemoveData(id);
        //                        model = null;
        //                    }
        //                    foreach (UCFriendProfile _UCFriendProfile in UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.Values)
        //                    {
        //                        if (_UCFriendProfile != null && _UCFriendProfile._UCFriendNewsFeeds != null)
        //                        {
        //                            //model = _UCFriendProfile._UCFriendNewsFeeds.FriendNewsFeeds.GetModel(id);
        //                            if (model != null)
        //                            {
        //                                //_UCFriendProfile._UCFriendNewsFeeds.FriendNewsFeeds.RemoveModel(id, iDelete);
        //                                if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(_UCFriendProfile._UCFriendNewsFeeds.FriendIdentity))
        //                                {
        //                                    NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME[_UCFriendProfile._UCFriendNewsFeeds.FriendIdentity].RemoveData(id);
        //                                }
        //                                model = null;
        //                            }
        //                        }
        //                    }
        //                    if (UCMiddlePanelSwitcher.View_UCCirclePanel != null && UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds != null)
        //                    {
        //                        //model = UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.CircleNewsFeeds.GetModel(id);
        //                        //UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.CircleNewsFeeds.RemoveModel(id, iDelete);
        //                        if (NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.CircleId))
        //                        {
        //                            NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME[UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.CircleId].RemoveData(id);
        //                        }
        //                        model = null;
        //                    }
        //                    lock (NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY)
        //                        NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY.Remove(id);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("ex in RemoveSingleFeedUI==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }
        //}

        //public void LoadAlbumList(long utid, int mediatype, List<MediaContentDTO> albumList)
        //{
        //    //Application.Current.Dispatcher.Invoke((Action)(() =>
        //    //{
        //    try
        //    {
        //        ObservableCollection<MediaContentModel> AudioOrVideoAlbums = null;
        //        if (utid == DefaultSettings.LOGIN_TABLE_ID)
        //        {
        //            if (DownloadOrAddToAlbumPopUpWrapper.ucDownloadOrAddToAlbumPopUp != null
        //                && DownloadOrAddToAlbumPopUpWrapper.ucDownloadOrAddToAlbumPopUp.IsVisible)
        //            {
        //                DownloadOrAddToAlbumPopUpWrapper.ucDownloadOrAddToAlbumPopUp.AlbumCount = (DownloadOrAddToAlbumPopUpWrapper.ucDownloadOrAddToAlbumPopUp.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO) ? DefaultSettings.MY_AUDIO_ALBUMS_COUNT : DefaultSettings.MY_VIDEO_ALBUMS_COUNT;
        //            }
        //            AudioOrVideoAlbums = (mediatype == SettingsConstants.MEDIA_TYPE_AUDIO) ? RingIDViewModel.Instance.MyAudioAlbums : RingIDViewModel.Instance.MyVideoAlbums;
        //        }
        //        else if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendBasicInfoModel != null
        //            && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendBasicInfoModel.ShortInfoModel.UserTableID == utid)
        //        {
        //            AudioOrVideoAlbums = (mediatype == SettingsConstants.MEDIA_TYPE_AUDIO) ? UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileMusicAlbum.FriendAudioAlbums : UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileVideoAlbum.FriendVideoAlbums;
        //        }
        //        if (AudioOrVideoAlbums != null)
        //            foreach (MediaContentDTO albumDto in albumList)
        //            {
        //                lock (AudioOrVideoAlbums)
        //                {
        //                    MediaContentModel model = AudioOrVideoAlbums.Where(P => P.AlbumId == albumDto.AlbumId).FirstOrDefault();
        //                    if (model == null)
        //                    {
        //                        model = new MediaContentModel();
        //                        model.LoadData(albumDto);
        //                        Application.Current.Dispatcher.Invoke((Action)(() =>
        //                        {
        //                            AudioOrVideoAlbums.Add(model);
        //                        }));
        //                    }
        //                    else
        //                    {
        //                        model.LoadData(albumDto);
        //                    }
        //                }
        //            }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: LoadAlbumList() => " + ex.Message + "\n" + ex.StackTrace);
        //    }
        //    //}));
        //}

        //public void LoadCommentList(List<CommentDTO> comments, long nfId, long imageId, long contentId)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //    {
        //        try
        //        {
        //            List<CommentDTO> sortedList = comments;
        //            //(from l in comments
        //            // orderby l.Time ascending
        //            // select l).ToList();
        //            CommentModel commentModel = null;
        //            if (nfId > 0)
        //            {
        //                FeedModel feedModel = (FeedModel)GetFeedModelByNfid(nfId);
        //                if (feedModel != null)
        //                {
        //                    foreach (CommentDTO comment in sortedList)
        //                    {
        //                        commentModel = feedModel.CommentList.Where(P => P.CommentId == comment.CommentId).FirstOrDefault();
        //                        if (commentModel == null) { commentModel = new CommentModel(); feedModel.CommentList.Insert(0, commentModel); }
        //                        commentModel.LoadData(comment);
        //                        commentModel.NewsfeedId = nfId;
        //                        commentModel.PostOwnerUid = feedModel.UserShortInfoModel.UserIdentity;
        //                    }
        //                }
        //            }
        //            else if (imageId > 0 && UCGuiRingID.Instance.ucBasicImageView != null && UCGuiRingID.Instance.ucBasicImageView.clickedImageID == imageId && UCGuiRingID.Instance.ucBasicImageView.ImageComments != null)
        //            {
        //                UCImageComments _UCImageComments = UCGuiRingID.Instance.ucBasicImageView.ImageComments;
        //                _UCImageComments.ShowWatch(false);
        //                _UCImageComments.ImageCommentList.Clear();
        //                foreach (CommentDTO comment in sortedList)
        //                {
        //                    commentModel = new CommentModel();
        //                    commentModel.LoadData(comment);
        //                    commentModel.ImageId = imageId;
        //                    commentModel.ContentId = contentId;
        //                    _UCImageComments.ImageCommentList.Insert(0, commentModel);
        //                }
        //                if (UCGuiRingID.Instance.ucBasicImageView.ImageFullNameProfileImage != null)
        //                    _UCImageComments.PreviousCommentsVisibility = (UCGuiRingID.Instance.ucBasicImageView.ImageFullNameProfileImage.NumberOfComments > _UCImageComments.ImageCommentList.Count) ? Visibility.Visible : Visibility.Collapsed;
        //            }
        //            else if (contentId > 0 && UCGuiRingID.Instance.ucBasicMediaView != null && UCGuiRingID.Instance.ucBasicMediaView.clickedMediaID == contentId && UCGuiRingID.Instance.ucBasicMediaView.ImageComments != null)
        //            {
        //                UCImageComments _UCImageComments = UCGuiRingID.Instance.ucBasicMediaView.ImageComments;
        //                _UCImageComments.ShowWatch(false);
        //                //_UCImageComments.ImageCommentList.Clear();
        //                foreach (CommentDTO comment in sortedList)
        //                {
        //                    commentModel = _UCImageComments.ImageCommentList.Where(P => P.CommentId == comment.CommentId).FirstOrDefault();
        //                    if (commentModel == null)
        //                    {
        //                        commentModel = new CommentModel();
        //                        commentModel.LoadData(comment);
        //                        commentModel.ImageId = imageId;
        //                        commentModel.ContentId = contentId;
        //                        _UCImageComments.ImageCommentList.Insert(0, commentModel);
        //                    }
        //                    else commentModel.LoadData(comment);
        //                }
        //                if (_UCImageComments.ImageCommentList.Count > 0)
        //                {
        //                    _UCImageComments.ImageCommentList = new ObservableCollection<CommentModel>(_UCImageComments.ImageCommentList.OrderByDescending(a => a.Time));
        //                }
        //                if (UCGuiRingID.Instance.ucBasicMediaView.ImageFullNameProfileImage != null)
        //                    _UCImageComments.PreviousCommentsVisibility = (UCGuiRingID.Instance.ucBasicMediaView.ImageFullNameProfileImage.NumberOfComments > _UCImageComments.ImageCommentList.Count) ? Visibility.Visible : Visibility.Collapsed;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            log.Error("Error: LoadCommentList() => " + ex.Message + "\n" + ex.StackTrace);
        //        }
        //    }));
        //}

        //public void LoadStatusComments(long nfId, long friendId, long circleId)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //  {
        //      try
        //      {
        //          Dictionary<long, CommentDTO> CommentsByNfid = null;
        //          FeedModel feedModel = (FeedModel)GetFeedModelByNfid(nfId);

        //          if (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST.TryGetValue(nfId, out CommentsByNfid) && feedModel != null)
        //          {
        //              lock (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST)
        //              {
        //                  List<CommentDTO> sortedList = (from l in CommentsByNfid.Values
        //                                                 orderby l.Time descending
        //                                                 select l).ToList();
        //                  foreach (CommentDTO comment in sortedList)
        //                  {
        //                      CommentModel model = feedModel.CommentList.Where(P => P.CommentId == comment.CommentId).FirstOrDefault();
        //                      if (model == null)
        //                      {
        //                          model = new CommentModel();
        //                          model.LoadData(comment);
        //                          model.NewsfeedId = nfId;
        //                          model.PostOwnerUid = feedModel.UserShortInfoModel.UserIdentity;
        //                          lock (RingIDViewModel.Instance.NewsFeeds)
        //                          {
        //                              feedModel.CommentList.Insert(0, model);
        //                          }
        //                      }
        //                  }
        //                  feedModel.OnPropertyChanged("CurrentInstance");
        //                  //if (UCLikeListPopup.CurrentInstance != null) UCLikeListPopup.CurrentInstance.SetVisibilities();
        //                  //NewsFeedDictionaries.Instance.FEED_LIKE_LIST.Clear();
        //              }
        //          }
        //      }
        //      catch (Exception ex)
        //      {
        //          log.Error("Error: LoadStatusComments() => " + ex.Message + "\n" + ex.StackTrace);
        //      }
        //  }));
        //}

        //public void EditFeed(FeedDTO feed)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //    {
        //        if (DownloadOrAddToAlbumPopUpWrapper.ucAddLocationView != null)
        //        {
        //            DownloadOrAddToAlbumPopUpWrapper.ucAddLocationView.EnableButtonsandLoader(true);
        //            DownloadOrAddToAlbumPopUpWrapper.ucAddLocationView.Hide();
        //        }
        //        if (feed != null)
        //        {
        //            if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.ContainsKey(feed.NewsfeedId)) NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
        //            FeedModel fm = (FeedModel)GetFeedModelByNfid(feed.NewsfeedId);
        //            if (fm != null)
        //            {
        //                long tm = fm.ActualTime;
        //                fm.LoadData(feed);
        //                fm.ActualTime = tm;
        //                fm.OnPropertyChanged("CurrentInstance");
        //                fm.IsEditMode = false;
        //            }
        //        }
        //    }));
        //}

        //public void EditFeedComment(string cmnt, long cmntId, long nfId, long friendId, long circleId)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //        {
        //            try
        //            {
        //                Dictionary<long, CommentDTO> CommentsByNfid = null;
        //                lock (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST)
        //                {
        //                    if (NewsFeedDictionaries.Instance.FEED_COMMENT_LIST.TryGetValue(nfId, out CommentsByNfid))
        //                    {
        //                        CommentDTO comment = null;
        //                        if (CommentsByNfid.TryGetValue(cmntId, out comment))
        //                        {
        //                            comment.Comment = cmnt;
        //                        }
        //                    }
        //                }

        //                FeedModel feedModel = (FeedModel)GetFeedModelByNfid(nfId);

        //                if (feedModel != null)
        //                {
        //                    CommentModel model = feedModel.CommentList.Where(P => P.CommentId == cmntId).FirstOrDefault();
        //                    if (model != null)
        //                    {
        //                        model.Comment = cmnt;
        //                        model.IsEditMode = false;
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                log.Error("Error: EditFeedComment() => " + ex.Message + "\n" + ex.StackTrace);
        //            }
        //        }));
        //}

        //public void UpdateShareStatus(FeedDTO feed)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //         {
        //             try
        //             {
        //                 if (feed.ParentFeed != null)
        //                 {
        //                     FeedModel parentFeedModel = (FeedModel)GetFeedModel(feed.ParentFeed);

        //                     if (parentFeedModel != null)
        //                     {
        //                         parentFeedModel.NumberOfShares++;
        //                         FeedModel whoShareModel = new FeedModel();
        //                         whoShareModel.LoadData(feed);
        //                         if (!parentFeedModel.WhoShareList.Any(P => P.NewsfeedId == whoShareModel.NewsfeedId))
        //                             parentFeedModel.WhoShareList.Add(whoShareModel);
        //                     }
        //                 }
        //             }
        //             catch (Exception ex)
        //             {
        //                 log.Error("Error: UpdateShareStatus() => " + ex.Message + "\n" + ex.StackTrace);
        //             }
        //         }));
        //}
        //public void UpdateEditStatus(FeedDTO feed)
        //{

        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //           {
        //               try
        //               {
        //                   FeedModel model = (FeedModel)GetFeedModel(feed);
        //                   if (model != null)
        //                   {
        //                       long at = model.ActualTime;
        //                       long t = model.Time;
        //                       string prevDsc = model.PreviewDesc;
        //                       string prevDom = model.PreviewDomain;
        //                       string prevDImg = model.PreviewImgUrl;
        //                       string prevUrl = model.PreviewUrl;
        //                       string prevTtl = model.PreviewTitle;
        //                       model.LoadData(feed);
        //                       model.Time = t;
        //                       model.ActualTime = at;
        //                       model.PreviewDesc = prevDsc;
        //                       model.PreviewDomain = prevDom;
        //                       model.PreviewImgUrl = prevDImg;
        //                       model.PreviewUrl = prevUrl;
        //                       model.PreviewTitle = prevTtl;
        //                       model.OnPropertyChanged("CurrentInstance");
        //                   }
        //               }
        //               catch (Exception ex)
        //               {
        //                   log.Error("Error: UpdateEditStatus() => " + ex.Message + "\n" + ex.StackTrace);
        //               }
        //           }));
        //}

        //public void UpdateStatusLike(bool MyLike, long nfId, long friendId, long circleId, long auId, UserBasicInfoDTO user, long nl)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //         {
        //             try
        //             {
        //                 FeedModel feedModel = null;
        //                 FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out feedModel);
        //                 if (feedModel != null)
        //                 {
        //                     feedModel.NumberOfLikes = nl;
        //                     if (MyLike) feedModel.ILike = 1;
        //                     if (feedModel.SingleImageFeedModel != null)
        //                     {
        //                         if (MyLike) feedModel.SingleImageFeedModel.ILike = 1;
        //                         feedModel.SingleImageFeedModel.NumberOfLikes = nl;
        //                     }
        //                     else if (feedModel.SingleMediaFeedModel != null)
        //                     {
        //                         if (MyLike) feedModel.SingleMediaFeedModel.ILike = 1;
        //                         feedModel.SingleMediaFeedModel.LikeCount = (short)nl;
        //                     }
        //                     if (user != null)
        //                     {
        //                         feedModel.ActivistShortInfoModel = RingIDViewModel.Instance.GetUserShortInfoModelFromServerNotNullable(auId, 0, user.FullName, user.ProfileImage);
        //                         feedModel.Activity = AppConstants.NEWSFEED_LIKED;
        //                         feedModel.OnPropertyChanged("CurrentInstance");
        //                         UserBasicInfoModel userModel = GetExistingorNewUserBasicModelFromDTO(user);
        //                         if (LikeListViewWrapper != null && LikeListViewWrapper.ucLikeListView != null &&
        //                             LikeListViewWrapper.ucLikeListView.NfId == nfId)
        //                         {
        //                             LikeListViewWrapper.ucLikeListView.LikeList.Add(userModel);
        //                         }
        //                     }
        //                 }
        //             }
        //             catch (Exception ex)
        //             {
        //                 log.Error("Error: UpdateStatusLike() => " + ex.Message + "\n" + ex.StackTrace);
        //             }
        //         }));
        //}

        //public void UpdateImageLikeUnlike(long imgId, int liked, UserBasicInfoDTO user, long nfid)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //         {
        //             long lkc = -1;
        //             if (basicImageViewWrapper().ucBasicImageView != null && basicImageViewWrapper().ucBasicImageView.clickedImageID == imgId && basicImageViewWrapper().ucBasicImageView.LikeNumbers != null)
        //             {
        //                 if (liked == 1)
        //                     basicImageViewWrapper().ucBasicImageView.LikeNumbers.NumberOfLikes = basicImageViewWrapper().ucBasicImageView.LikeNumbers.NumberOfLikes + 1;
        //                 else if (basicImageViewWrapper().ucBasicImageView.LikeNumbers.NumberOfLikes > 0) basicImageViewWrapper().ucBasicImageView.LikeNumbers.NumberOfLikes = basicImageViewWrapper().ucBasicImageView.LikeNumbers.NumberOfLikes - 1;
        //                 lkc = basicImageViewWrapper().ucBasicImageView.LikeNumbers.NumberOfLikes;
        //             }
        //             FeedModel feedModel = null;
        //             FeedDataContainer.Instance.FeedModels.TryGetValue(nfid, out feedModel);
        //             if (feedModel != null && feedModel.SingleImageFeedModel != null)
        //             {
        //                 if (lkc > -1)
        //                 {
        //                     feedModel.NumberOfLikes = lkc;
        //                     feedModel.SingleImageFeedModel.NumberOfLikes = feedModel.NumberOfLikes;
        //                 }
        //                 else if (liked == 1)
        //                 {
        //                     feedModel.NumberOfLikes = feedModel.NumberOfLikes + 1;
        //                     feedModel.SingleImageFeedModel.NumberOfLikes = feedModel.NumberOfLikes;
        //                 }
        //                 else
        //                 {
        //                     feedModel.NumberOfLikes = feedModel.NumberOfLikes - 1;
        //                     feedModel.SingleImageFeedModel.NumberOfLikes = feedModel.NumberOfLikes;
        //                 }
        //                 feedModel.OnPropertyChanged("CurrentInstance");
        //             }
        //         }));
        //}

        //public void UpdateStatusUnlike(bool MyLike, long nfId, long friendId, long circleId, long auId, long ut, long uId, long nl)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //         {
        //             try
        //             {
        //                 FeedModel feedModel = null;
        //                 FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out feedModel);
        //                 if (feedModel != null)
        //                 {
        //                     if (MyLike) feedModel.ILike = 0;
        //                     feedModel.NumberOfLikes = nl;
        //                     if (feedModel.SingleImageFeedModel != null)
        //                     {
        //                         if (MyLike) feedModel.SingleImageFeedModel.ILike = 0;
        //                         feedModel.SingleImageFeedModel.NumberOfLikes = nl;
        //                     }
        //                     else if (feedModel.SingleMediaFeedModel != null)
        //                     {
        //                         if (MyLike) feedModel.SingleMediaFeedModel.ILike = 0;
        //                         feedModel.SingleMediaFeedModel.LikeCount = (short)nl;
        //                     }
        //                     if (uId > 0)
        //                     {
        //                         feedModel.Time = ut;
        //                         if (feedModel.ActivistShortInfoModel != null && feedModel.ActivistShortInfoModel.UserTableID == auId)
        //                         {
        //                             feedModel.ActivistShortInfoModel = null;
        //                             feedModel.Activity = 0;
        //                             feedModel.OnPropertyChanged("CurrentInstance");
        //                         }
        //                         if (LikeListViewWrapper != null && LikeListViewWrapper.ucLikeListView != null &&
        //                             LikeListViewWrapper.ucLikeListView.NfId == nfId)
        //                         {
        //                             UserBasicInfoModel userModel = LikeListViewWrapper.ucLikeListView.LikeList.Where(P => P.ShortInfoModel.UserIdentity == uId).FirstOrDefault();
        //                             if (userModel != null)
        //                                 LikeListViewWrapper.ucLikeListView.LikeList.Remove(userModel);
        //                         }
        //                     }
        //                 }
        //             }
        //             catch (Exception ex)
        //             {
        //                 log.Error("Error: UpdateStatusUnlike() => " + ex.Message + "\n" + ex.StackTrace);
        //             }
        //         }));
        //}
        //public void UpdateCommentLike(long nfId, long cmntId, long friendId, long circleId)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //           {
        //               try
        //               {
        //                   FeedModel model = null;
        //                   FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out model);
        //                   if (model != null)
        //                   {
        //                       lock (model.CommentList)
        //                       {
        //                           CommentModel cmodel = model.CommentList.Where(P => P.CommentId == cmntId).FirstOrDefault();
        //                           if (cmodel != null)
        //                           {
        //                               cmodel.TotalLikeComment = cmodel.TotalLikeComment + 1;
        //                           }
        //                       }
        //                   }
        //               }
        //               catch (Exception ex)
        //               {
        //                   log.Error("Error: UpdateCommentLike() => " + ex.Message + "\n" + ex.StackTrace);
        //               }
        //           }));
        //}
        //public void UpdateCommentUnLike(long nfId, long cmntId, long friendId, long circleId)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //        {
        //            try
        //            {
        //                FeedModel model = null;
        //                FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out model);
        //                if (model != null)
        //                {
        //                    lock (model.CommentList)
        //                    {
        //                        CommentModel cmodel = model.CommentList.Where(P => P.CommentId == cmntId).FirstOrDefault();
        //                        if (cmodel != null)
        //                        {
        //                            cmodel.TotalLikeComment = cmodel.TotalLikeComment - 1;
        //                        }
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                log.Error("Error: UpdateCommentUnLike() => " + ex.Message + "\n" + ex.StackTrace);
        //            }
        //        }));
        //}

        //private void likeStatus_184_384(JObject _JobjFromResponse, bool MyLike = false)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.ActivistId] != null && _JobjFromResponse[JsonKeys.LikeOrComment] != null)
        //        {
        //            long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
        //            long friendTableId = (long)_JobjFromResponse[JsonKeys.ActivistId];
        //            int loc = (int)_JobjFromResponse[JsonKeys.LikeOrComment];
        //            //long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
        //            long friendId = (_JobjFromResponse[JsonKeys.UserIdentity] != null) ? (long)_JobjFromResponse[JsonKeys.UserIdentity] : 0;
        //            string friendName = (string)_JobjFromResponse[JsonKeys.FullName];

        //            UserShortInfoModel FriendShortInfoModel = RingIDViewModel.Instance.GetUserShortInfoModelFromServerNotNullable(friendTableId, friendId, friendName, "");
        //            FeedModel feedModel = null;
        //            //if (FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out feedModel))
        //            //{
        //            //    feedModel.NumberOfLikes = loc;
        //            //    if (MyLike) feedModel.ILike = 1;
        //            //    if (feedModel.SingleImageFeedModel != null)
        //            //    {
        //            //        if (MyLike) feedModel.SingleImageFeedModel.ILike = 1;
        //            //        feedModel.SingleImageFeedModel.NumberOfLikes = nl;//feedModel.SingleImageFeedModel.NumberOfLikes - 1;
        //            //    }
        //            //    else if (feedModel.SingleMediaFeedModel != null)
        //            //    {
        //            //        if (MyLike) feedModel.SingleMediaFeedModel.ILike = 1;
        //            //        feedModel.SingleMediaFeedModel.LikeCount = (short)nl;//(short)(feedModel.SingleMediaFeedModel.LikeCount - 1);
        //            //    }
        //            //    if (user != null)
        //            //    {
        //            //        feedModel.ActivistShortInfoModel = RingIDViewModel.Instance.GetUserShortInfoModelFromServerNotNullable(auId, 0, user.FullName, user.ProfileImage);
        //            //        feedModel.Activity = AppConstants.NEWSFEED_LIKED;
        //            //        feedModel.OnPropertyChanged("CurrentInstance");
        //            //        UserBasicInfoModel userModel = GetExistingorNewUserBasicModelFromDTO(user);
        //            //        if (LikeListViewWrapper != null && LikeListViewWrapper.ucLikeListView != null &&
        //            //            LikeListViewWrapper.ucLikeListView.NfId == nfId)
        //            //        {
        //            //            LikeListViewWrapper.ucLikeListView.LikeList.Add(userModel);
        //            //        }
        //            //    }
        //            //}
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //        log.Error("ProcessUpdateLikeStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

        //    }
        //}

        //private void unLikeStatus(JObject _JobjFromResponse, bool MyLike = false)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.NewsfeedId] != null && _JobjFromResponse[JsonKeys.ActivistId] != null && _JobjFromResponse[JsonKeys.LikeOrComment] != null)
        //        {
        //            long nfId = (long)_JobjFromResponse[JsonKeys.NewsfeedId];
        //            long auId = (long)_JobjFromResponse[JsonKeys.ActivistId];
        //            long nl = (long)_JobjFromResponse[JsonKeys.LikeOrComment];
        //            long ut = (_JobjFromResponse[JsonKeys.UpdateTime] != null) ? (long)_JobjFromResponse[JsonKeys.UpdateTime] : 0;
        //            long uId = (_JobjFromResponse[JsonKeys.UserIdentity] != null) ? (long)_JobjFromResponse[JsonKeys.UserIdentity] : 0;
        //            long circleId = (_JobjFromResponse[JsonKeys.GroupId] != null) ? (long)_JobjFromResponse[JsonKeys.GroupId] : 0;
        //            //long futId = (_JobjFromResponse[JsonKeys.FriendId] != null) ? (long)_JobjFromResponse[JsonKeys.FriendId] : 0;
        //            long futId = (_JobjFromResponse[JsonKeys.FutId] != null) ? (long)_JobjFromResponse[JsonKeys.FutId] : 0;
        //            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
        //            {
        //                FeedDTO feed = null;
        //                if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.TryGetValue(nfId, out feed))
        //                {
        //                    if (feed.NumberOfLikes > 0)
        //                    {
        //                        feed.NumberOfLikes--;
        //                        if (MyLike) feed.ILike = 0;
        //                    }
        //                }
        //            }
        //            UpdateStatusUnlike(MyLike, nfId, futId, circleId, auId, ut, uId, nl);
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //        log.Error("ProcessUpdateUnlikeStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

        //    }
        //}

        //public void ShareStatus(FeedDTO parentFeed)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //     {
        //         try
        //         {

        //             FeedModel parentModel = null;
        //             FeedDataContainer.Instance.FeedModels.TryGetValue(parentFeed.NewsfeedId, out parentModel);//(FeedModel)GetFeedModel(parentFeed);
        //             FeedModel fm = new FeedModel();
        //             fm.LoadData(parentFeed.WhoShare);
        //             //fm.OnPropertyChanged("CurrentInstance");
        //             if (parentModel != null)
        //             {
        //                 parentModel.IShare = parentFeed.IShare;
        //                 parentModel.NumberOfShares = parentFeed.NumberOfShares;
        //                 if (!parentModel.WhoShareList.Any(P => P.NewsfeedId == fm.NewsfeedId))
        //                     parentModel.WhoShareList.Add(fm);
        //                 fm.ParentFeed = parentModel;
        //                 parentModel.OnPropertyChanged("CurrentInstance");
        //             }

        //             if (UCSingleFeedDetails.Instance != null && UCSingleFeedDetails.Instance.FeedModel != null)
        //             {
        //                 FeedModel detailsModel = UCSingleFeedDetails.Instance.FeedModel;
        //                 if (detailsModel.NewsfeedId == parentFeed.NewsfeedId)
        //                 {
        //                     UCSingleFeedDetails.Instance.LoadFeedModel(fm);
        //                 }
        //             }

        //             if (UCShareSingleFeedView.Instance != null && UCShareSingleFeedView.Instance.Visibility == Visibility.Visible)
        //             {
        //                 UCShareSingleFeedView.Instance.Hide();
        //             }
        //             //InsertSingleShareFeedUI(fm, DefaultSettings.FEED_TYPE_ALL);
        //             //InsertSingleShareFeedUI(fm, DefaultSettings.FEED_TYPE_MY);
        //             RingIDViewModel.Instance.MyNewsFeeds.InsertModel(fm);
        //             if (UCMiddlePanelSwitcher.View_UCAllFeeds != null) UCMiddlePanelSwitcher.View_UCAllFeeds.InsertModel(fm, false);
        //             if (parentFeed.UserIdentity != DefaultSettings.LOGIN_USER_ID)
        //             {
        //                 //InsertSingleShareFeedUI(fm, DefaultSettings.FEED_TYPE_FRIEND, parentFeed.UserIdentity);
        //                 if (RingIDViewModel.Instance.FriendProfileFeeds.UserProfileId == parentFeed.UserIdentity)
        //                     RingIDViewModel.Instance.FriendProfileFeeds.InsertModel(fm);
        //             }

        //         }
        //         catch (Exception e)
        //         {
        //             log.Error("ex ShareStatus==>" + e.Message + "\n" + e.StackTrace);
        //         }

        //     }));


        //}

        //public void WhoShareList(long parentNfid, CustomSortedList whoShares)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //          {
        //              try
        //              {
        //                  FeedModel parentModel = null;// (FeedModel)GetFeedModelByNfid(parentNfid);
        //                  FeedDataContainer.Instance.FeedModels.TryGetValue(parentNfid, out parentModel);
        //                  if (parentModel != null)
        //                  {
        //                      parentModel.WhoShare = null;
        //                      //parentModel.WhoShareList.Clear();
        //                      if (whoShares == null || whoShares.Count == 0) { parentModel.ShowMoreSharesState = 0; return; }
        //                      else parentModel.IsShareListVisible = true;

        //                      foreach (CustomSortedList.Data data in whoShares)
        //                      {
        //                          FeedDTO singleShare = null;
        //                          NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.TryGetValue(data.NfId, out singleShare);
        //                          if (singleShare != null)
        //                          {
        //                              FeedModel singleShareModel = null;// (FeedModel)GetFeedModelByNfid(singleShare.NewsfeedId);
        //                              FeedDataContainer.Instance.FeedModels.TryGetValue(data.NfId, out singleShareModel);

        //                              if (singleShareModel == null)
        //                              {
        //                                  singleShareModel = parentModel.LoadWhoShare(singleShare, parentModel);
        //                              }

        //                              if (singleShareModel != null && !parentModel.WhoShareList.Any(P => P.NewsfeedId == singleShare.NewsfeedId))
        //                              {
        //                                  parentModel.WhoShareList.Add(singleShareModel);
        //                                  parentModel.OnPropertyChanged("WhoShareList");
        //                              }
        //                          }
        //                      }

        //                      if (parentModel.WhoShareList.Count == 0 || parentModel.NumberOfShares == 0) parentModel.ShowMoreSharesState = 0;
        //                      else if (parentModel.WhoShareList.Count == parentModel.NumberOfShares) parentModel.ShowMoreSharesState = 0;
        //                      else if (parentModel.WhoShareList.Count > 0 && parentModel.WhoShareList.Count < parentModel.NumberOfShares) parentModel.ShowMoreSharesState = 2;

        //                  }
        //              }
        //              catch (Exception ex)
        //              {
        //                  log.Error("Error: WhoShareList() => " + ex.Message + "\n" + ex.StackTrace);
        //              }
        //          }));
        //}

        //public void DeleteMediaComment(long contentId, long commentId, long nfid)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //    {
        //        bool deletedFromMedDtn = false;
        //        if (BasicMediaViewWrapper != null && BasicMediaViewWrapper.ucBasicMediaView != null &&
        //            RingPlayer.RingPlayerViewModel.Instance.ClickedMediaID == contentId && BasicMediaViewWrapper.ucBasicMediaView.ImageComments != null &&
        //            BasicMediaViewWrapper.ucBasicMediaView.ImageComments.ImageCommentList.Count > 0)
        //        {
        //            CommentModel model = BasicMediaViewWrapper.ucBasicMediaView.ImageComments.ImageCommentList.Where(P => P.CommentId == commentId).FirstOrDefault();
        //            if (model != null)
        //            {
        //                BasicMediaViewWrapper.ucBasicMediaView.ImageComments.ImageCommentList.Remove(model);
        //                if (BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage != null)
        //                    BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.NumberOfComments = BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.NumberOfComments - 1;
        //                if (!BasicMediaViewWrapper.ucBasicMediaView.ImageComments.ImageCommentList.Any(P => P.UserShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID) && BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage != null)
        //                {
        //                    BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.IComment = 0;
        //                }
        //                DeleteCommentFromMediaDictionary(contentId, BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.IComment);
        //                deletedFromMedDtn = true;
        //            }
        //        }
        //        FeedModel feedModel = null;//((FeedModel)GetFeedModelByNfid(nfid)) : null;
        //        FeedDataContainer.Instance.FeedModels.TryGetValue(nfid, out feedModel);
        //        if (feedModel != null && feedModel.SingleMediaFeedModel != null)
        //        {
        //            feedModel.NumberOfComments = feedModel.NumberOfComments - 1;
        //            feedModel.SingleMediaFeedModel.CommentCount = (feedModel.SingleMediaFeedModel.CommentCount - 1);
        //            CommentModel model = feedModel.CommentList.Where(P => P.CommentId == commentId).FirstOrDefault();
        //            if (model != null)
        //            {
        //                feedModel.CommentList.Remove(model);
        //                if (!feedModel.CommentList.Any(P => P.UserShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID))
        //                {
        //                    feedModel.IComment = 0;
        //                    feedModel.SingleMediaFeedModel.IComment = 0;
        //                }
        //                if (!deletedFromMedDtn) DeleteCommentFromMediaDictionary(contentId, feedModel.IComment);
        //            }
        //            //feedModel.OnPropertyChanged("CurrentInstance");
        //            //UpdateFeedModelsByContentId(feedModel.SingleMediaFeedModel.ContentId);
        //        }
        //    }));
        //}

        //public void LoadItemsOfAnAlbum(List<SingleMediaDTO> tmp_songs, long albumId, int mediaType, long utid, int totalSongInAlbum)
        //{
        //    try
        //    {
        //        //Application.Current.Dispatcher.BeginInvoke((Action)(() =>
        //        //{
        //        //    string AlbumNm = null;
        //        //    HideLoaderOfAblumContentList(utid, mediaType);
        //        //    if (tmp_songs.Count > 0)
        //        //    {
        //        //        ObservableCollection<SingleMediaModel> SearchSingleMediaModelCollection = null, ViewModelSingleMediaModelCollection = null;
        //        //        UCFriendProfile FriendProfile = null;

        //        //        if (utid == DefaultSettings.LOGIN_TABLE_ID) //user Profile
        //        //        {
        //        //            //ViewModelSingleMediaModelCollection = (mediaType == 1) ? RingIDViewModel.Instance.MyAudioAlbums : RingIDViewModel.Instance.MyVideoAlbums;
        //        //            if (mediaType == 1)// && UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyMediaAlbumDetailsForAudio != null)
        //        //            {
        //        //                if (RingIDViewModel.Instance.MyAudioAlbums.Count > 0)
        //        //                {
        //        //                    MediaContentModel albuminViewModel = RingIDViewModel.Instance.MyAudioAlbums.Where(P => P.AlbumId == albumId).FirstOrDefault();
        //        //                    if (albuminViewModel != null)
        //        //                    {
        //        //                        AlbumNm = albuminViewModel.AlbumName;
        //        //                        if (albuminViewModel.MediaList != null)
        //        //                        {
        //        //                            ViewModelSingleMediaModelCollection = albuminViewModel.MediaList;
        //        //                            if (UCMiddlePanelSwitcher.View_MusicsVideos != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Main != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Main._UCAlbumMediaListWrapper != null
        //        //                            && UCMiddlePanelSwitcher.View_MusicsVideos.View_Main._UCAlbumMediaListWrapper._UCSingleAlbumSong != null)
        //        //                            {
        //        //                                UCMiddlePanelSwitcher.View_MusicsVideos.View_Main._UCAlbumMediaListWrapper._UCSingleAlbumSong.VisibilityShowMore(((albuminViewModel.MediaList.Count + tmp_songs.Count) < albuminViewModel.TotalMediaCount));
        //        //                            }
        //        //                        }
        //        //                        else
        //        //                        {
        //        //                            albuminViewModel.MediaList = new ObservableCollection<SingleMediaModel>();
        //        //                            ViewModelSingleMediaModelCollection = albuminViewModel.MediaList;
        //        //                        }
        //        //                    }
        //        //                }
        //        //                if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyMediaAlbumDetailsForAudio != null)
        //        //                {
        //        //                    //UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyMediaAlbumDetailsForAudio.ShowLoader(false);
        //        //                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyMediaAlbumDetailsForAudio.IsPlayButtonVisible = Visibility.Visible;
        //        //                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyMediaAlbumDetailsForAudio.MySingleMediaAudios = ViewModelSingleMediaModelCollection;
        //        //                    if (ViewModelSingleMediaModelCollection != null && (tmp_songs.Count + ViewModelSingleMediaModelCollection.Count) < totalSongInAlbum)
        //        //                    {
        //        //                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyMediaAlbumDetailsForAudio.ShowMore();
        //        //                    }
        //        //                    else
        //        //                    {
        //        //                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyMediaAlbumDetailsForAudio.HideShowMoreLoading();
        //        //                    }
        //        //                }
        //        //                if (MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick != null
        //        //                    && MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.Visibility == Visibility.Visible)
        //        //                {
        //        //                    if ((tmp_songs.Count + ViewModelSingleMediaModelCollection.Count) < totalSongInAlbum)
        //        //                        MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.ShowMore();
        //        //                    else
        //        //                        MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.HideShowMoreLoading();
        //        //                }

        //        //            }
        //        //            else if (mediaType == 2)// && UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMediaAlbumdetailsForVideo != null)
        //        //            {
        //        //                if (RingIDViewModel.Instance.MyVideoAlbums.Count > 0)
        //        //                {
        //        //                    MediaContentModel albuminViewModel = RingIDViewModel.Instance.MyVideoAlbums.Where(P => P.AlbumId == albumId).FirstOrDefault();
        //        //                    if (albuminViewModel != null)
        //        //                    {
        //        //                        AlbumNm = albuminViewModel.AlbumName;
        //        //                        if (albuminViewModel.MediaList != null)
        //        //                        {
        //        //                            ViewModelSingleMediaModelCollection = albuminViewModel.MediaList;
        //        //                            if (UCMiddlePanelSwitcher.View_MusicsVideos != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Main != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Main._UCAlbumMediaListWrapper != null
        //        //                       && UCMiddlePanelSwitcher.View_MusicsVideos.View_Main._UCAlbumMediaListWrapper._UCSingleAlbumSong != null)
        //        //                            {
        //        //                                UCMiddlePanelSwitcher.View_MusicsVideos.View_Main._UCAlbumMediaListWrapper._UCSingleAlbumSong.VisibilityShowMore(((albuminViewModel.MediaList.Count + tmp_songs.Count) < albuminViewModel.TotalMediaCount));
        //        //                            }
        //        //                        }
        //        //                        else
        //        //                        {
        //        //                            albuminViewModel.MediaList = new ObservableCollection<SingleMediaModel>();
        //        //                            ViewModelSingleMediaModelCollection = albuminViewModel.MediaList;
        //        //                        }
        //        //                    }
        //        //                }
        //        //                if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMediaAlbumdetailsForVideo != null)
        //        //                {
        //        //                    //UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMediaAlbumdetailsForVideo.ShowLoader(false);
        //        //                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMediaAlbumdetailsForVideo.IsPlayButtonVisible = Visibility.Visible;
        //        //                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMediaAlbumdetailsForVideo.MySingleMediaVedios = ViewModelSingleMediaModelCollection;
        //        //                    if (ViewModelSingleMediaModelCollection != null && (tmp_songs.Count + ViewModelSingleMediaModelCollection.Count) < totalSongInAlbum)
        //        //                    {
        //        //                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMediaAlbumdetailsForVideo.ShowMore();
        //        //                    }
        //        //                    else
        //        //                    {
        //        //                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMediaAlbumdetailsForVideo.HideShowMoreLoading();
        //        //                    }
        //        //                }
        //        //                if (MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick != null
        //        //                    && MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.Visibility == Visibility.Visible)
        //        //                {
        //        //                    if ((tmp_songs.Count + ViewModelSingleMediaModelCollection.Count) < totalSongInAlbum)
        //        //                        MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.ShowMore();
        //        //                    else
        //        //                        MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.HideShowMoreLoading();
        //        //                }
        //        //            }
        //        //        }
        //        //        else if (FriendDictionaries.Instance.UTID_UID_DICTIONARY.ContainsKey(utid))   // Friend Profile
        //        //        {
        //        //            long uid = FriendDictionaries.Instance.UTID_UID_DICTIONARY[utid];
        //        //            if (UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.ContainsKey(uid))
        //        //            {
        //        //                FriendProfile = UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY[uid];
        //        //                if ((FriendProfile != null && mediaType == 1 && FriendProfile._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForAudio != null) || (mediaType == 2 && FriendProfile._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForVideo != null))
        //        //                {
        //        //                    ViewModelSingleMediaModelCollection = (mediaType == 1) ? FriendProfile._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForAudio.FriendSingleMediaAudios : FriendProfile._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForVideo.FriendSingleMediaVedios;
        //        //                    AlbumNm = (mediaType == 1) ? FriendProfile._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForAudio.AlbumName : FriendProfile._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForVideo.AlbumName;
        //        //                    if (ViewModelSingleMediaModelCollection != null)
        //        //                    {
        //        //                        if (mediaType == 1)
        //        //                        {
        //        //                            FriendProfile._UCFriendProfileMediaAlbum.UCFriendMediaDetailsForAudio.IsPlayButtonVisible = Visibility.Visible;
        //        //                            //FriendProfile._UCFriendProfileMediaAlbum.UCFriendMediaDetailsForAudio.ShowLoader(false);
        //        //                            if (ViewModelSingleMediaModelCollection != null && (ViewModelSingleMediaModelCollection.Count + tmp_songs.Count) < totalSongInAlbum)
        //        //                            {
        //        //                                FriendProfile._UCFriendProfileMediaAlbum.UCFriendMediaDetailsForAudio.ShowMore();
        //        //                            }
        //        //                            else
        //        //                            {
        //        //                                FriendProfile._UCFriendProfileMediaAlbum.UCFriendMediaDetailsForAudio.HideShowMoreLoading();
        //        //                            }
        //        //                        }
        //        //                        else
        //        //                        {
        //        //                            FriendProfile._UCFriendProfileMediaAlbum.UCFriendMediaDetailsForVideo.IsPlayButtonVisible = Visibility.Visible;
        //        //                            //FriendProfile._UCFriendProfileMediaAlbum.UCFriendMediaDetailsForVideo.ShowLoader(false);
        //        //                            if (ViewModelSingleMediaModelCollection != null && (ViewModelSingleMediaModelCollection.Count + tmp_songs.Count) < totalSongInAlbum)
        //        //                            {
        //        //                                FriendProfile._UCFriendProfileMediaAlbum.UCFriendMediaDetailsForVideo.ShowMore();
        //        //                            }
        //        //                            else
        //        //                            {
        //        //                                FriendProfile._UCFriendProfileMediaAlbum.UCFriendMediaDetailsForVideo.HideShowMoreLoading();
        //        //                            }
        //        //                        }
        //        //                    }
        //        //                }
        //        //            }
        //        //        }

        //        //        if (UCMiddlePanelSwitcher.View_MusicsVideos != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch != null
        //        //            && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.Tab_Albums_Wrapper != null
        //        //             && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.Tab_Albums_Wrapper.View_UCSearchAlbumPanel != null)
        //        //        {
        //        //            MediaContentModel albumModel = UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.Tab_Albums_Wrapper.View_UCSearchAlbumPanel.AlbumsFromSearch.Where(P => P.AlbumId == albumId).FirstOrDefault();
        //        //            if (albumModel != null && albumModel.MediaList != null)
        //        //            {
        //        //                AlbumNm = albumModel.AlbumName;
        //        //                SearchSingleMediaModelCollection = albumModel.MediaList;
        //        //                if (UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.View_Contents != null)
        //        //                    UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.View_Contents.VisibilityShowMore(((albumModel.MediaList.Count + tmp_songs.Count) < albumModel.TotalMediaCount));
        //        //            }
        //        //        }

        //        //        //if (MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick != null && MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.Visibility == Visibility.Visible)
        //        //        //{
        //        //        //    MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.HideOrShowMoreVisibility();
        //        //        //}

        //        //        foreach (SingleMediaDTO singleMedia in tmp_songs)
        //        //        {
        //        //            SingleMediaModel model = null;
        //        //            model = (ViewModelSingleMediaModelCollection != null && ViewModelSingleMediaModelCollection.Count > 0) ? ViewModelSingleMediaModelCollection.Where(P => P.ContentId == singleMedia.ContentId).FirstOrDefault() : null;
        //        //            if (model == null)
        //        //            {
        //        //                model = RingIDViewModel.Instance.GetExistingorNewSingleMediaModelFromDTO(singleMedia);////new SingleMediaModel();
        //        //                model.LoadData(singleMedia);
        //        //            }
        //        //            else
        //        //            {
        //        //                model.LoadData(singleMedia);
        //        //            }
        //        //            if (!string.IsNullOrEmpty(AlbumNm) && string.IsNullOrEmpty(model.AlbumName)) model.AlbumName = AlbumNm;
        //        //            if (SearchSingleMediaModelCollection != null && model != null && !SearchSingleMediaModelCollection.Any(P => P.ContentId == model.ContentId))
        //        //                SearchSingleMediaModelCollection.Add(model);
        //        //            if (ViewModelSingleMediaModelCollection != null && model != null && !ViewModelSingleMediaModelCollection.Any(P => P.ContentId == model.ContentId))
        //        //                ViewModelSingleMediaModelCollection.Add(model);
        //        //        }
        //        //    }
        //        //}));
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error => in LoadItemsOfAnAlbum" + ex.Message);
        //    }
        //}

        //public void NoMoreItemsInHashTagOrAlbum(long albumId, long hashTagId)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //    {
        //        if (albumId > 0 && UCMiddlePanelSwitcher.View_MusicsVideos != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch != null
        //              && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.View_Contents != null
        //            && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.View_Contents.albumModel != null
        //            && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.View_Contents.albumModel.AlbumId == albumId)
        //        {
        //            UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.View_Contents.VisibilityShowMore(false);
        //        }
        //        else if (hashTagId > 0 && UCMiddlePanelSwitcher.View_MusicsVideos != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch != null
        //              && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.View_Contents != null
        //            && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.View_Contents.hashTagModel != null
        //            && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.View_Contents.hashTagModel.HashTagId == hashTagId)
        //        {
        //            UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.View_Contents.VisibilityShowMore(false);
        //        }
        //        else if (albumId > 0 && MediaListFromAlbumClickWrapper != null
        //            && MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.IsVisible
        //            && MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.albumModel.AlbumId == albumId)
        //        {
        //            MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.HideOrShowMoreVisibility();
        //        }
        //    }));
        //}

        //public void NoItemFoundInUCSingleAlbumSong()
        //{
        //    if (UCMiddlePanelSwitcher.View_MusicsVideos != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Main != null
        //                           && UCMiddlePanelSwitcher.View_MusicsVideos.View_Main._UCAlbumMediaListWrapper != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Main._UCAlbumMediaListWrapper._UCSingleAlbumSong != null)
        //    {
        //        UCMiddlePanelSwitcher.View_MusicsVideos.View_Main._UCAlbumMediaListWrapper._UCSingleAlbumSong.NoItemsFound = true;
        //    }
        //}

        //public void UpdateCommentMedia(CommentDTO comment, long contentId, long nfid)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //    {
        //        if (BasicMediaViewWrapper != null && BasicMediaViewWrapper.ucBasicMediaView != null &&
        //            RingPlayer.RingPlayerViewModel.Instance.ClickedMediaID == contentId && BasicMediaViewWrapper.ucBasicMediaView.ImageComments != null)
        //        {
        //            CommentModel model = new CommentModel();
        //            model.LoadData(comment);
        //            model.ContentId = contentId;
        //            BasicMediaViewWrapper.ucBasicMediaView.ImageComments.ImageCommentList.Insert(0, model);
        //            if (BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage != null)
        //                BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.NumberOfComments = BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.NumberOfComments + 1;
        //        }
        //    }));
        //}

        //public void DeleteCommentFromMediaDictionary(long ContentId, int icomment)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //    {
        //        ObservableCollection<MediaContentModel> viewCollectionAudio = new ObservableCollection<MediaContentModel>();
        //        //ViewCollection = (mediatype == SettingsConstants.MEDIA_TYPE_AUDIO) ? RingIDViewModel.Instance.MyAudioAlbums : RingIDViewModel.Instance.MyVideoAlbums;
        //        viewCollectionAudio = RingIDViewModel.Instance.MyAudioAlbums;
        //        foreach (MediaContentModel contentModel in viewCollectionAudio)
        //        {
        //            if (contentModel.MediaList != null && contentModel.MediaList.Count > 0)
        //            {
        //                SingleMediaModel mediaModel = contentModel.MediaList.Where(P => P.ContentId == ContentId).FirstOrDefault();
        //                if (mediaModel != null)
        //                {
        //                    mediaModel.CommentCount--;
        //                    mediaModel.IComment = (short)icomment;
        //                    return;
        //                }
        //            }
        //        }
        //        //for (int i = 0; i < NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID.Count; i++)
        //        //{
        //        //    Dictionary<long, MediaContentDTO> mediaDictionaries = NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID.ElementAt(i).Value;
        //        //    for (int j = 0; j < mediaDictionaries.Count; j++)
        //        //    {
        //        //        MediaContentDTO album = mediaDictionaries.ElementAt(j).Value;
        //        //        if (album.MediaList != null && album.MediaList.Count > 0)
        //        //        {
        //        //            SingleMediaDTO dto = album.MediaList.Where(P => P.ContentId == ContentId).FirstOrDefault();
        //        //            if (dto != null)
        //        //            {
        //        //                dto.CommentCount--;
        //        //                dto.IComment = (short)icomment;
        //        //                return;
        //        //            }
        //        //        }
        //        //    }
        //        //}
        //        ObservableCollection<MediaContentModel> viewCollectionVideo = new ObservableCollection<MediaContentModel>();
        //        viewCollectionVideo = RingIDViewModel.Instance.MyVideoAlbums;
        //        //ViewCollection = (mediatype == SettingsConstants.MEDIA_TYPE_AUDIO) ? RingIDViewModel.Instance.MyAudioAlbums : RingIDViewModel.Instance.MyVideoAlbums;
        //        foreach (MediaContentModel contentModel in viewCollectionVideo)
        //        {
        //            if (contentModel.MediaList != null && contentModel.MediaList.Count > 0)
        //            {
        //                SingleMediaModel mediaModel = contentModel.MediaList.Where(P => P.ContentId == ContentId).FirstOrDefault();
        //                if (mediaModel != null)
        //                {
        //                    mediaModel.CommentCount--;
        //                    mediaModel.IComment = (short)icomment;
        //                    return;
        //                }
        //            }
        //        }
        //        //for (int i = 0; i < NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID.Count; i++)
        //        //{
        //        //    Dictionary<long, MediaContentDTO> mediaDictionaries = NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID.ElementAt(i).Value;
        //        //    for (int j = 0; j < mediaDictionaries.Count; j++)
        //        //    {
        //        //        MediaContentDTO album = mediaDictionaries.ElementAt(j).Value;
        //        //        if (album.MediaList != null && album.MediaList.Count > 0)
        //        //        {
        //        //            SingleMediaDTO dto = album.MediaList.Where(P => P.ContentId == ContentId).FirstOrDefault();
        //        //            if (dto != null)
        //        //            {
        //        //                dto.CommentCount--;
        //        //                dto.IComment = (short)icomment;
        //        //                return;
        //        //            }
        //        //        }
        //        //    }
        //        //}
        //    }));
        //}

        //public void UpdateFeedDetails(FeedDTO feed)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //    {
        //        FeedModel feedModel = (FeedModel)GetFeedModelByNfid(feed.NewsfeedId);
        //        if (feedModel != null)
        //        {

        //            feedModel.LoadData(feed, (feed.ImageList != null && feed.ImageList.Count > 0));
        //            feedModel.OnPropertyChanged("CurrentInstance");
        //            if (feedModel.TaggedFriendsList != null && feedModel.TaggedFriendsList.Count > 0)
        //                feedModel.OnPropertyChanged("TaggedFriendsList");
        //            //if (feedModel.ImageList != null)
        //            //{
        //            //    feedModel.OnPropertyChanged("ImageList");
        //            //    foreach (var item in feedModel.ImageList)
        //            //    {
        //            //        item.OnPropertyChanged("CurrentInstance");
        //            //    }
        //            //}
        //        }
        //    }));
        //}

        //public void Process_MEDIA_FEED_87(JObject _JobjFromResponse, string client_packet_id)
        //{
        //    try
        //    {
        //        //if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
        //        //{

        //        //    short mediaTrendingType = ((int)_JobjFromResponse[JsonKeys.MediaTrendingFeed] == SettingsConstants.MEDIA_FEED_TYPE_ALL) ? SettingsConstants.MEDIA_FEED_TYPE_ALL : SettingsConstants.MEDIA_FEED_TYPE_TRENDING;

        //        //    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
        //        //    {
        //        //        int first_feed_state = 0;
        //        //        if (mediaTrendingType == SettingsConstants.MEDIA_FEED_TYPE_ALL)
        //        //            first_feed_state = client_packet_id.Equals(DefaultSettings.START_MEDIAFEEDS_PACKETID) ? 1 : 0; //0=not addupdatestatus nor first time,2=addupdatestatus,1=firsttimefeed
        //        //        else if (mediaTrendingType == SettingsConstants.MEDIA_FEED_TYPE_TRENDING)
        //        //            first_feed_state = client_packet_id.Equals(DefaultSettings.START_MEDIAFEEDSTRENDING_PACKETID) ? 1 : 0; //0=not addupdatestatus nor first time,2=addupdatestatus,1=firsttimefeed

        //        //        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
        //        //        foreach (JObject singleObj in jarray)
        //        //        {
        //        //            FeedDTO feed = HelperMethodsModel.BindFeedDetails(singleObj);
        //        //            if (feed.MediaContent != null && feed.MediaContent.MediaList != null && feed.MediaContent.MediaList.Count == 1 && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.Count > 0)
        //        //            {
        //        //                HelperMethodsModel.UpdateFeedDTOsForSingleMediaFeed(feed.MediaContent.MediaList.ElementAt(0));
        //        //            }
        //        //            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
        //        //            {
        //        //                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
        //        //            }

        //        //            if (feed.WhoShare != null)
        //        //            {
        //        //                FeedDTO whoShareFeed = feed.WhoShare;
        //        //                whoShareFeed.ParentFeed = feed;

        //        //                if (!NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.UserIdentity)
        //        //                    && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.FriendId)
        //        //                    && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.UserIdentity)
        //        //                    && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.FriendId)
        //        //                    && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(whoShareFeed.NewsfeedId)
        //        //                    && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(whoShareFeed.ParentFeed.NewsfeedId))
        //        //                {

        //        //                    lock (NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY)
        //        //                    {
        //        //                        NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY[whoShareFeed.NewsfeedId] = feed.NewsfeedId;
        //        //                    }

        //        //                    lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
        //        //                    {
        //        //                        NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[whoShareFeed.NewsfeedId] = whoShareFeed;
        //        //                    }

        //        //                    if (mediaTrendingType == SettingsConstants.MEDIA_FEED_TYPE_ALL)
        //        //                    {
        //        //                        NewsFeedDictionaries.Instance.MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(whoShareFeed.NewsfeedId, whoShareFeed.Time, whoShareFeed.FeedCategory);
        //        //                        UpdateFeedModelWhoShareList(feed.NewsfeedId, feed);
        //        //                        InsertSingleFeedUI(first_feed_state, whoShareFeed, DefaultSettings.FEED_TYPE_MEDIA);
        //        //                    }
        //        //                    else
        //        //                    {
        //        //                        NewsFeedDictionaries.Instance.MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS.InsertDataFromServer(whoShareFeed.NewsfeedId);
        //        //                        UpdateFeedModelWhoShareList(feed.NewsfeedId, feed);
        //        //                        InsertSingleFeedUI(first_feed_state, whoShareFeed, DefaultSettings.FEED_TYPE_MEDIA_TRENDING);
        //        //                    }
        //        //                }
        //        //            }
        //        //            else
        //        //            {
        //        //                if (feed.FeedCategory == SettingsConstants.SPECIAL_FEED)
        //        //                {
        //        //                    if (mediaTrendingType == SettingsConstants.MEDIA_FEED_TYPE_ALL)
        //        //                    {
        //        //                        CustomSortedList.Data data = NewsFeedDictionaries.Instance.MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME.GetData(feed.NewsfeedId);
        //        //                        if (data != null)
        //        //                        {
        //        //                            if (data.Tm != feed.Time)
        //        //                            {
        //        //                                NewsFeedDictionaries.Instance.MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
        //        //                                InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_MEDIA);
        //        //                            }
        //        //                        }
        //        //                        else
        //        //                        {
        //        //                            NewsFeedDictionaries.Instance.MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
        //        //                            InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_MEDIA);
        //        //                        }
        //        //                    }
        //        //                    else
        //        //                    {
        //        //                        CustomSortedList.Data data = NewsFeedDictionaries.Instance.MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS.GetData(feed.NewsfeedId);
        //        //                        if (data != null)
        //        //                        {
        //        //                            if (data.Tm != feed.Time)
        //        //                            {
        //        //                                NewsFeedDictionaries.Instance.MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS.InsertDataFromServer(feed.NewsfeedId);
        //        //                                InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_MEDIA_TRENDING);
        //        //                            }
        //        //                        }
        //        //                        else
        //        //                        {
        //        //                            NewsFeedDictionaries.Instance.MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS.InsertDataFromServer(feed.NewsfeedId);
        //        //                            InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_MEDIA_TRENDING);
        //        //                        }
        //        //                    }
        //        //                }
        //        //                else
        //        //                {
        //        //                    if (!NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.UserIdentity)
        //        //                        && !NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.FriendId)
        //        //                        && !NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Contains(feed.NewsfeedId))
        //        //                    {
        //        //                        if (mediaTrendingType == SettingsConstants.MEDIA_FEED_TYPE_ALL)
        //        //                        {
        //        //                            NewsFeedDictionaries.Instance.MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
        //        //                            InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_MEDIA);
        //        //                        }
        //        //                        else
        //        //                        {
        //        //                            NewsFeedDictionaries.Instance.MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS.InsertDataFromServer(feed.NewsfeedId);
        //        //                            InsertSingleFeedUI(first_feed_state, feed, DefaultSettings.FEED_TYPE_MEDIA_TRENDING);
        //        //                        }
        //        //                    }
        //        //                }
        //        //            }
        //        //        }

        //        //    }
        //        //}
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("ProcessMediaFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }
        //}
        //public void HideLoaderOfAblumContentList(long userTableId, long mediaType)
        //{
        //    //if (userTableId == DefaultSettings.LOGIN_TABLE_ID)
        //    //{
        //    //    // Albums Content in Myprofile
        //    //    if (mediaType == 1)
        //    //    {
        //    //        if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum != null
        //    //            && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyMediaAlbumDetailsForAudio != null)
        //    //        {
        //    //            UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyMediaAlbumDetailsForAudio.ShowLoader(false);
        //    //        }
        //    //        else if ( MediaListFromAlbumClickWrapper != null
        //    //                && MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick != null
        //    //                && MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.IsVisible)
        //    //        {
        //    //            MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.HideOrShowMoreVisibility();
        //    //        }
        //    //    }
        //    //    else
        //    //    {
        //    //        if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum != null
        //    //            && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMediaAlbumdetailsForVideo != null)
        //    //        {
        //    //            UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMediaAlbumdetailsForVideo.ShowLoader(false);
        //    //        }
        //    //        else if ( MediaListFromAlbumClickWrapper != null
        //    //            && MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick != null
        //    //                && MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.IsVisible)
        //    //        {
        //    //            MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.HideOrShowMoreVisibility();
        //    //        }
        //    //    }
        //    //}
        //    //else
        //    //{
        //    //    // Albums Content in Friendprofile
        //    //    if (FriendDictionaries.Instance.UTID_UID_DICTIONARY.ContainsKey(userTableId))
        //    //    {
        //    //        long uid = FriendDictionaries.Instance.UTID_UID_DICTIONARY[userTableId];
        //    //        if (uid > 0)
        //    //        {
        //    //            UCFriendProfile _UCFriendProfile;
        //    //            if (UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.TryGetValue(uid, out _UCFriendProfile))
        //    //            {
        //    //                if (mediaType == 1)
        //    //                {
        //    //                    if (_UCFriendProfile._UCFriendProfileMediaAlbum != null && _UCFriendProfile._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForAudio != null)
        //    //                    {
        //    //                        _UCFriendProfile._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForAudio.ShowLoader(false);
        //    //                    }
        //    //                }
        //    //                else
        //    //                {
        //    //                    if (_UCFriendProfile._UCFriendProfileMediaAlbum != null && _UCFriendProfile._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForVideo != null)
        //    //                    {
        //    //                        _UCFriendProfile._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForVideo.ShowLoader(false);
        //    //                    }
        //    //                }
        //    //            }
        //    //        }
        //    //    }
        //    //}
        //    ////Albums Content in Music & Video, Search 
        //    //if (UCMiddlePanelSwitcher.View_MusicsVideos != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Main != null
        //    //       && UCMiddlePanelSwitcher.View_MusicsVideos.View_Main._UCAlbumMediaListWrapper != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Main._UCAlbumMediaListWrapper._UCSingleAlbumSong != null)
        //    //{
        //    //    UCMiddlePanelSwitcher.View_MusicsVideos.View_Main._UCAlbumMediaListWrapper._UCSingleAlbumSong.ShowLoader(false);
        //    //}

        //    //if (UCMiddlePanelSwitcher.View_MusicsVideos != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch != null
        //    //    && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.View_Contents != null)
        //    //{
        //    //    UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.View_Contents.ShowLoader(false);
        //    //}
        //}

        //public void ShowMorePanelVisiblityInMedia(long UtId, int mediaType, long userId, bool noData = false)
        //{
        //    //Application.Current.Dispatcher.Invoke((Action)(() =>
        //    //{
        //    //    if (UtId == DefaultSettings.LOGIN_TABLE_ID)
        //    //    {
        //    //        if (mediaType == 1)
        //    //        {
        //    //            if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum != null
        //    //                && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyMediaAlbumDetailsForAudio != null)
        //    //            {
        //    //                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyMediaAlbumDetailsForAudio.HideOrShowMoreVisibility();
        //    //            }
        //    //            else if ( MediaListFromAlbumClickWrapper != null
        //    //                && MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick != null
        //    //                && MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.IsVisible)
        //    //            {
        //    //                MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.HideOrShowMoreVisibility();
        //    //            }
        //    //        }
        //    //        else
        //    //        {
        //    //            if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum != null
        //    //                && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMediaAlbumdetailsForVideo != null)
        //    //            {
        //    //                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMediaAlbumdetailsForVideo.HideOrShowMoreVisibility();
        //    //            }
        //    //            else if ( MediaListFromAlbumClickWrapper != null
        //    //                && MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick != null
        //    //                && MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.IsVisible)
        //    //            {
        //    //                MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.HideOrShowMoreVisibility();
        //    //            }
        //    //        }
        //    //    }
        //    //    else // FriendProfile
        //    //    {
        //    //        if (userId > 0)
        //    //        {
        //    //            UCFriendProfile _UCFriendProfile;
        //    //            UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.TryGetValue(userId, out _UCFriendProfile);
        //    //            if (_UCFriendProfile != null)
        //    //            {
        //    //                if (mediaType == 1)
        //    //                {
        //    //                    if (_UCFriendProfile._UCFriendProfileMediaAlbum != null && _UCFriendProfile._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForAudio != null)
        //    //                    {
        //    //                        _UCFriendProfile._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForAudio.HideOrShowMoreVisibility();
        //    //                    }
        //    //                }
        //    //                else
        //    //                {
        //    //                    if (_UCFriendProfile._UCFriendProfileMediaAlbum != null && _UCFriendProfile._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForVideo != null)
        //    //                    {
        //    //                        _UCFriendProfile._UCFriendProfileMediaAlbum._UCFriendMediaAlbumDetailsForVideo.HideOrShowMoreVisibility();
        //    //                    }
        //    //                }
        //    //            }
        //    //        }
        //    //    }
        //    //    if (UCMiddlePanelSwitcher.View_MusicsVideos != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Main != null
        //    //        && UCMiddlePanelSwitcher.View_MusicsVideos.View_Main._UCAlbumMediaListWrapper != null
        //    //        && UCMiddlePanelSwitcher.View_MusicsVideos.View_Main._UCAlbumMediaListWrapper._UCSingleAlbumSong != null)
        //    //        UCMiddlePanelSwitcher.View_MusicsVideos.View_Main._UCAlbumMediaListWrapper._UCSingleAlbumSong.LoadMoreFailed(noData);

        //    //    if (UCMiddlePanelSwitcher.View_MusicsVideos != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search != null
        //    //        && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch != null
        //    //        && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.View_Contents != null)
        //    //        UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.View_Contents.LoadMoreFailed(noData);
        //    //}));
        //}

        //#region NewsPortalFeed
        //public void BottomLoadingNewsPortalFeed(int type, bool svd)
        //{
        //    if (!svd && UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel != null && UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel != null
        //        && UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCNewsContainerPanel != null)
        //    {
        //        //UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCNewsContainerPanel.BOTTOM_LOADING = type;
        //    }
        //    else if (UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel != null && UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel != null
        //        && UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCSavedContentPanel != null)
        //    {
        //        //UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCSavedContentPanel.BOTTOM_LOADING = type;
        //    }
        //}

        //public void BottomLoadingPagesFeed(int type, bool svd)
        //{
        //    if (!svd && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel != null && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel != null
        //        && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCNewsContainerPanel != null)
        //    {
        //        //UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCNewsContainerPanel.BOTTOM_LOADING = type;
        //    }
        //    else if (UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel != null && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel != null
        //        && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCSavedContentPanel != null)
        //    {
        //        // UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCSavedContentPanel.BOTTOM_LOADING = type;
        //    }
        //}
        //#endregion

        //public void Process_UPDATE_LIKEUNLIKECOMMENT_MEDIA_468(JObject _JobjFromResponse)
        //{
        //    //Received for action=468 from Auth JSON==> {"uId":"2110010086","sucs":true,"fn":"sirat samyoun","id":120,"cmnId":625,"tm":1449402328539,"cntntId":1875,"lkd":1,"loc":1,"mdaT":2}
        //    //try
        //    //{
        //    //    if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.CommentId] != null
        //    //        && _JobjFromResponse[JsonKeys.ContentId] != null && _JobjFromResponse[JsonKeys.Liked] != null)
        //    //    {
        //    //        long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
        //    //        long commentId = (long)_JobjFromResponse[JsonKeys.CommentId];
        //    //        long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
        //    //        int liked = (int)_JobjFromResponse[JsonKeys.Liked];
        //    //        UpdateLikeUnlikeCommentMedia(liked, contentId, commentId, nfid);
        //    //    }
        //    //}
        //    //catch (Exception e)
        //    //{

        //    //    log.Error("ProcessUpdateLikeUnlikeMedia ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

        //    //}
        //}

        //#region My Feeds

        //public void BottomMyLoading(Object obj, int type)
        //{
        //    //if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyNewsFeeds != null)
        //    //{
        //    //    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyNewsFeeds.BOTTOM_LOADING = type;
        //    //}
        //    //UCMyNewsFeeds _UCMyNewsFeeds = (UCMyNewsFeeds)obj;
        //    //_UCMyNewsFeeds.BOTTOM_LOADING = type;
        //}

        //#endregion

        //#region FriendFeeds
        //public void BottomFriendLoading(Object obj, int type)
        //{
        //    //UCFriendProfile _UCFriendProfile;
        //    //UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.TryGetValue(friendId, out _UCFriendProfile);

        //    //if (_UCFriendProfile != null && _UCFriendProfile._UCFriendNewsFeeds != null)
        //    //{
        //    //    _UCFriendProfile._UCFriendNewsFeeds.BOTTOM_LOADING = type;
        //    //}
        //    //if (obj is UCFriendNewsFeeds)
        //    //{
        //    //    UCFriendNewsFeeds _UCFriendNewsFeeds = (UCFriendNewsFeeds)obj;
        //    //    _UCFriendNewsFeeds.BOTTOM_LOADING = type;
        //    //}
        //    //else if (obj is UCNewsPortalProfile)
        //    //{
        //    //    UCNewsPortalProfile _UCNewsPortalProfile = (UCNewsPortalProfile)obj;
        //    //    _UCNewsPortalProfile.BOTTOM_LOADING = type;
        //    //}
        //    //else if (obj is UCPageProfile)
        //    //{
        //    //    UCPageProfile _UCPageProfile = (UCPageProfile)obj;
        //    //    _UCPageProfile.BOTTOM_LOADING = type;
        //    //}
        //    //else if (obj is UCMediaPageProfile)
        //    //{
        //    //    UCMediaPageProfile _UCPageProfile = (UCMediaPageProfile)obj;
        //    //    _UCPageProfile.BOTTOM_LOADING = type;
        //    //}
        //}

        //#endregion

        //#region CircleFeeds

        //public void BottomCircleLoading(Object obj, int type)
        //{
        //    /*UCCirclePanel _UCCirclePanel;
        //    UIDictionaries.Instance.CIRCLE_PANEL_DICTIONARY.TryGetValue(CircleId, out _UCCirclePanel);

        //    if (_UCCirclePanel != null && _UCCirclePanel._UCCircleNewsFeeds != null)
        //    {
        //        _UCCirclePanel._UCCircleNewsFeeds.BOTTOM_LOADING = type;
        //    }*/
        //    //UCCircleNewsFeeds _UCCircleNewsFeeds = (UCCircleNewsFeeds)obj;
        //    //_UCCircleNewsFeeds.BOTTOM_LOADING = type;
        //}

        //#endregion

        //public void BottomLoading(int type)
        //{
        //    if (UCAllFeeds.Instance != null)
        //    {
        //        //UCAllFeeds.Instance.BOTTOM_LOADING = type;
        //    }
        //}

        //#region "Media Feeds"

        //public void BottomLoadingMediaFeed(int type)
        //{
        //    if (UCMiddlePanelSwitcher.View_MediaFeed != null && UCMiddlePanelSwitcher.View_MediaFeed._UCMediaFeeds != null)
        //    {
        //        UCMiddlePanelSwitcher.View_MediaFeed._UCMediaFeeds.BOTTOM_LOADING = type;
        //    }
        //}
        //#endregion

        //#region "Media Feeds Trending"

        //public void BottomLoadingMediaFeedTrending(int type)
        //{
        //    if (UCMiddlePanelSwitcher.View_MediaFeed != null && UCMiddlePanelSwitcher.View_MediaFeed._UCMediaFeedsTrending != null)
        //    {
        //        UCMiddlePanelSwitcher.View_MediaFeed._UCMediaFeedsTrending.BOTTOM_LOADING = type;
        //    }
        //}
        //#endregion
        //public void UpdateLikeUnlikeCommentMedia(int liked, long contentId, long commentId, long nfid)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //    {
        //        ObservableCollection<CommentModel> CommentsINSingleMediaFeedModel = null;
        //        ObservableCollection<CommentModel> CommentsINMediaView = null;
        //        FeedModel feedModel = null;
        //        if (nfid > 0)
        //        {
        //            FeedDataContainer.Instance.FeedModels.TryGetValue(nfid, out feedModel);
        //            if (feedModel != null && feedModel.SingleMediaFeedModel != null)
        //            {
        //                CommentsINSingleMediaFeedModel = feedModel.CommentList;
        //            }
        //        }
        //        if (BasicMediaViewWrapper.ucBasicMediaView != null &&
        //            RingPlayer.RingPlayerViewModel.Instance.ClickedMediaID == contentId &&
        //            BasicMediaViewWrapper.ucBasicMediaView.ImageComments != null)
        //        {
        //            CommentsINMediaView = BasicMediaViewWrapper.ucBasicMediaView.ImageComments.ImageCommentList;
        //        }
        //        if (CommentsINMediaView != null || CommentsINSingleMediaFeedModel != null)
        //        {
        //            CommentModel commentFeed = null, commentMdaView = null;
        //            if (CommentsINSingleMediaFeedModel != null) commentFeed = CommentsINSingleMediaFeedModel.Where(p => p.CommentId == commentId).FirstOrDefault();
        //            if (CommentsINMediaView != null) commentMdaView = CommentsINMediaView.Where(p => p.CommentId == commentId).FirstOrDefault();
        //            if (commentFeed != null)
        //            {
        //                commentFeed.TotalLikeComment = (liked == 1) ? (commentFeed.TotalLikeComment + 1) : (commentFeed.TotalLikeComment - 1);
        //            }
        //            else if (commentMdaView != null)
        //            {
        //                if (commentFeed != null) commentMdaView.TotalLikeComment = commentFeed.TotalLikeComment;
        //                else commentMdaView.TotalLikeComment = (liked == 1) ? (commentMdaView.TotalLikeComment + 1) : (commentMdaView.TotalLikeComment - 1);
        //            }
        //        }
        //    }));
        //}

        //public void UpdateLikeUnlikeMedia(int loc, int liked, long contentId, long nfid)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //    {
        //        long lkc = -1;
        //        if (BasicMediaViewWrapper.ucBasicMediaView != null &&
        //            RingPlayer.RingPlayerViewModel.Instance.ClickedMediaID == contentId && BasicMediaViewWrapper.ucBasicMediaView.LikeNumbers != null)
        //        {
        //            if (liked == 1)
        //                BasicMediaViewWrapper.ucBasicMediaView.LikeNumbers.NumberOfLikes = BasicMediaViewWrapper.ucBasicMediaView.LikeNumbers.NumberOfLikes + 1;
        //            else if (BasicMediaViewWrapper.ucBasicMediaView.LikeNumbers.NumberOfLikes > 0) BasicMediaViewWrapper.ucBasicMediaView.LikeNumbers.NumberOfLikes = BasicMediaViewWrapper.ucBasicMediaView.LikeNumbers.NumberOfLikes - 1;
        //            lkc = BasicMediaViewWrapper.ucBasicMediaView.LikeNumbers.NumberOfLikes;
        //        }
        //        FeedModel feedModel = null;
        //        FeedDataContainer.Instance.FeedModels.TryGetValue(nfid, out feedModel);
        //        if (feedModel != null && feedModel.SingleMediaFeedModel != null)
        //        {
        //            if (lkc > -1)
        //            {
        //                feedModel.NumberOfLikes = lkc;
        //                feedModel.SingleMediaFeedModel.LikeCount = feedModel.NumberOfLikes;
        //            }
        //            else if (liked == 1)
        //            {
        //                feedModel.NumberOfLikes = feedModel.NumberOfLikes + 1;
        //                feedModel.SingleMediaFeedModel.LikeCount = feedModel.NumberOfLikes;
        //            }
        //            else
        //            {
        //                feedModel.NumberOfLikes = feedModel.NumberOfLikes - 1;
        //                feedModel.SingleMediaFeedModel.LikeCount = feedModel.NumberOfLikes;
        //            }
        //        }
        //    }));
        //}
        //public void AddFeedComment(CommentDTO comment, long nfId, long friendTableId, long circleId, long auId)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //     {
        //         try
        //         {
        //             FeedModel feedModel = null;
        //             FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out feedModel);
        //             if (feedModel != null)
        //             {
        //                 CommentModel model = new CommentModel();
        //                 model.NewsfeedId = nfId;
        //                 model.ShowContinue = false;
        //                 if (feedModel.UserShortInfoModel != null) model.PostOwnerUid = feedModel.UserShortInfoModel.UserTableID;
        //                 feedModel.CommentList.Add(model);
        //                 feedModel.NumberOfComments = feedModel.NumberOfComments + 1;
        //                 if (model.UserShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
        //                     feedModel.IComment = 1;
        //                 feedModel.ActivistShortInfoModel = RingIDViewModel.Instance.GetUserShortInfoModelFromServerNotNullable(auId, 0, comment.FullName, comment.ProfileImage);
        //                 feedModel.Activity = AppConstants.NEWSFEED_COMMENTED;
        //                 feedModel.OnPropertyChanged("CurrentInstance");
        //             }
        //         }
        //         catch (Exception ex)
        //         {
        //             log.Error("Error: AddFeedComment() => " + ex.Message + "\n" + ex.StackTrace);
        //         }
        //     }));
        //}

        //public void DeleteFeedComment(long nfid, long friendId, long circleId, long cmntId, long auId)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //        {
        //            try
        //            {
        //                FeedModel model = null;
        //                FeedDataContainer.Instance.FeedModels.TryGetValue(nfid, out model);
        //                if (model != null)
        //                {
        //                    CommentModel commentmodel = model.CommentList.Where(P => P.CommentId == cmntId).FirstOrDefault();
        //                    lock (model)
        //                    {
        //                        model.CommentList.Remove(commentmodel);
        //                    }
        //                    model.NumberOfComments = model.NumberOfComments - 1;
        //                    if (model.ActivistShortInfoModel != null && model.ActivistShortInfoModel.UserTableID == auId)
        //                    {
        //                        model.ActivistShortInfoModel = null;
        //                        model.Activity = 0;
        //                        model.OnPropertyChanged("ActivistShortInfoModel");
        //                    }
        //                    CommentModel anyMoreCommentbyMe = model.CommentList.Where(P => P.UserShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID).FirstOrDefault();
        //                    if (anyMoreCommentbyMe == null)
        //                        model.IComment = 0;

        //                    model.OnPropertyChanged("CurrentInstance");
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                log.Error("Error: DeleteFeedComment() => " + ex.Message + "\n" + ex.StackTrace);
        //            }
        //        }));
        //}
        //public void EnableSharePopup()
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //        {
        //            if (UCShareSingleFeedView.Instance != null)
        //            {
        //                UCShareSingleFeedView.Instance.EnableButtonsandLoader(true);
        //            }
        //        }));
        //}
        //public void SearchNewsPortals(string searchParam, Object ob)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //    {
        //        try
        //        {
        //            if (ob is List<NewsPortalDTO> && UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel != null && UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalSearchPanel != null
        //                && UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalSearchPanel.SearchTermTextBox.Text.Trim().Equals(searchParam))
        //            {
        //                List<NewsPortalDTO> list = (List<NewsPortalDTO>)ob;
        //                foreach (var item in list)
        //                {
        //                    NewsPortalModel model = null;
        //                    if (!UserProfilesContainer.Instance.NewsPortalModels.TryGetValue(item.PortalId, out model))
        //                    {
        //                        model = new NewsPortalModel();
        //                        UserProfilesContainer.Instance.NewsPortalModels[item.PortalId] = model;
        //                    }
        //                    model.LoadData(item);
        //                    //
        //                    if (!UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalSearchPanel.SearchedNewsPortals.Any(P => P.PortalId == model.PortalId))
        //                        UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalSearchPanel.SearchedNewsPortals.Add(model);
        //                }
        //            }
        //            else if (ob is List<PageInfoDTO> && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel != null && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesSearchPanel != null
        //                && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesSearchPanel.SearchTermTextBox.Text.Trim().Equals(searchParam))
        //            {
        //                List<PageInfoDTO> list = (List<PageInfoDTO>)ob;
        //                foreach (var item in list)
        //                {
        //                    PageInfoModel model = null;
        //                    if (!UserProfilesContainer.Instance.PageModels.TryGetValue(item.PageId, out model))
        //                    {
        //                        model = new PageInfoModel();
        //                        UserProfilesContainer.Instance.PageModels[item.PageId] = model;
        //                    }
        //                    model.LoadData(item);
        //                    //
        //                    if (!UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesSearchPanel.SearchedPages.Any(P => P.PageId == model.PageId))
        //                        UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesSearchPanel.SearchedPages.Add(model);
        //                }
        //            }
        //            else if (ob is List<CelebrityDTO> && UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel != null
        //                && UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesSearchPanel != null
        //                && UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesSearchPanel.SearchTermTextBox.Text.Trim().Equals(searchParam))
        //            {
        //                List<CelebrityDTO> list = (List<CelebrityDTO>)ob;
        //                foreach (var item in list)
        //                {
        //                    CelebrityModel model = null;
        //                    if (!UserProfilesContainer.Instance.CelebrityModels.TryGetValue(item.UserTableID, out model))
        //                    {
        //                        model = new CelebrityModel();
        //                        UserProfilesContainer.Instance.CelebrityModels[item.UserTableID] = model;
        //                    }
        //                    model.LoadData(item);
        //                    //
        //                    if (!UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesSearchPanel.SearchedCelebrities.Any(P => P.UserTableID == model.UserTableID))
        //                        UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesSearchPanel.SearchedCelebrities.Insert(UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesSearchPanel.SearchedCelebrities.Count - 1, model);
        //                    int index = UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesSearchPanel.SearchedCelebrities.IndexOf(model);
        //                    if ((index + 1) % 3 == 0)
        //                    {
        //                        model.IsRightMarginOff = true;
        //                    }
        //                    else
        //                    {
        //                        model.IsRightMarginOff = false;
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            log.Error("ex in SearchNewsPortals==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //        }
        //    }));
        //}

        //public void ShowAlbumItems(long albumId, List<SingleMediaDTO> list)
        //{
        //    if (UCMediaContentsView.Instance != null && UCMediaContentsView.Instance.albumModel != null && UCMediaContentsView.Instance.albumModel.AlbumId == albumId && UCMediaContentsView.Instance.albumModel.MediaList != null)
        //    {
        //        foreach (var item in list)
        //        {
        //            if (!UCMediaContentsView.Instance.albumModel.MediaList.Any(P => P.ContentId == item.ContentId))
        //            {
        //                SingleMediaModel model = RingIDViewModel.Instance.GetExistingorNewSingleMediaModelFromDTO(item);
        //                Application.Current.Dispatcher.Invoke((Action)(() =>
        //                {
        //                    UCMediaContentsView.Instance.albumModel.MediaList.Add(model);
        //                }));
        //            }
        //        }
        //    }
        //}
        //public void AddMediaComment(CommentDTO comment, long contentId, long nfid, bool update = false)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //    {
        //        ObservableCollection<CommentModel> CommentsINSingleMediaFeedModel = null;
        //        ObservableCollection<CommentModel> CommentsINMediaView = null;
        //        FeedModel feedModel = null;
        //        long postOwnerUid = 0;
        //        if (nfid > 0)
        //        {
        //            FeedDataContainer.Instance.FeedModels.TryGetValue(nfid, out feedModel);
        //            if (feedModel != null)
        //            {
        //                if (feedModel.SingleMediaFeedModel != null)
        //                {
        //                    CommentsINSingleMediaFeedModel = feedModel.CommentList;
        //                    feedModel.NumberOfComments = feedModel.NumberOfComments + 1;
        //                    feedModel.SingleMediaFeedModel.CommentCount = (feedModel.SingleMediaFeedModel.CommentCount + 1);
        //                    if (!update)
        //                    {
        //                        feedModel.IComment = 1;
        //                        feedModel.SingleMediaFeedModel.IComment = 1;
        //                    }
        //                }
        //                if (feedModel.UserShortInfoModel != null && feedModel.UserShortInfoModel.UserIdentity > 0)
        //                    postOwnerUid = feedModel.UserShortInfoModel.UserIdentity;
        //            }
        //        }

        //        if (BasicMediaViewWrapper.ucBasicMediaView != null &&
        //            RingPlayer.RingPlayerViewModel.Instance.ClickedMediaID == contentId)//BasicMediaViewWrapper.ucBasicMediaView.clickedMediaID == contentId)
        //        {
        //            if (BasicMediaViewWrapper.ucBasicMediaView.ImageComments != null)
        //                CommentsINMediaView = BasicMediaViewWrapper.ucBasicMediaView.ImageComments.ImageCommentList;
        //            if (BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage != null)
        //            {
        //                BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.NumberOfComments = BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.NumberOfComments + 1;
        //                if (!update) BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.IComment = 1;
        //            }
        //            if (RingPlayer.RingPlayerViewModel.Instance.MediaOwnerShortInfo != null && RingPlayer.RingPlayerViewModel.Instance.MediaOwnerShortInfo.UserIdentity > 0)
        //            {
        //                postOwnerUid = RingPlayer.RingPlayerViewModel.Instance.MediaOwnerShortInfo.UserIdentity;
        //            }
        //        }
        //        if (CommentsINMediaView != null || CommentsINSingleMediaFeedModel != null)
        //        {
        //            CommentModel commentFeed = null, commentMdaView = null, commentmodel = null;
        //            if (CommentsINSingleMediaFeedModel != null) commentFeed = CommentsINSingleMediaFeedModel.Where(p => p.CommentId == comment.CommentId).FirstOrDefault();
        //            if (CommentsINMediaView != null) commentMdaView = CommentsINMediaView.Where(p => p.CommentId == comment.CommentId).FirstOrDefault();
        //            if (commentFeed == null && commentMdaView == null)
        //            {
        //                commentmodel = new CommentModel();
        //                if (CommentsINSingleMediaFeedModel != null) CommentsINSingleMediaFeedModel.Add(commentmodel);
        //                if (CommentsINMediaView != null) CommentsINMediaView.Add(commentmodel);
        //            }
        //            else if (commentFeed != null && commentMdaView == null)
        //            {
        //                commentmodel = commentFeed;
        //                if (CommentsINMediaView != null) CommentsINMediaView.Insert(0, commentmodel);
        //            }
        //            else if (commentMdaView != null && commentFeed == null)
        //            {
        //                commentmodel = commentMdaView;
        //                if (CommentsINSingleMediaFeedModel != null) CommentsINSingleMediaFeedModel.Add(commentmodel);
        //            }
        //            else commentmodel = commentFeed;
        //            commentmodel.ContentId = contentId;
        //            if (nfid > 0) commentmodel.NewsfeedId = nfid;
        //            if (postOwnerUid > 0) commentmodel.PostOwnerUid = postOwnerUid;
        //            commentmodel.IsEditMode = false;
        //        }
        //    }));
        //}
        //public void AddOrUpdateMyAlbumModel(int mediaType, MediaContentModel newMediaContentModel)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //    {
        //        ObservableCollection<MediaContentModel> tmp_Coll = (mediaType == 1) ? RingIDViewModel.Instance.MyAudioAlbums : RingIDViewModel.Instance.MyVideoAlbums;
        //        MediaContentModel mediaContentModel = tmp_Coll.Where(P => P.AlbumId == newMediaContentModel.AlbumId).FirstOrDefault();
        //        if (mediaContentModel != null)
        //        {
        //            if (newMediaContentModel.MediaList != null)
        //            {
        //                mediaContentModel.TotalMediaCount += newMediaContentModel.MediaList.Count;
        //                mediaContentModel.AlbumImageUrl = newMediaContentModel.AlbumImageUrl;
        //            }
        //        }
        //        else
        //        {
        //            mediaContentModel = new MediaContentModel();
        //            if (newMediaContentModel.MediaList != null)
        //                mediaContentModel.TotalMediaCount = newMediaContentModel.MediaList.Count;
        //            tmp_Coll.Add(mediaContentModel);
        //        }
        //    }));
        //}

        //public void AddOrUpdateMyAlbumModel(int mediaType, MediaContentDTO newMediaContentDto)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //    {
        //        ObservableCollection<MediaContentModel> tmp_Coll = (mediaType == 1) ? RingIDViewModel.Instance.MyAudioAlbums : RingIDViewModel.Instance.MyVideoAlbums;
        //        MediaContentModel mediaContentModel = tmp_Coll.Where(P => P.AlbumId == newMediaContentDto.AlbumId).FirstOrDefault();
        //        if (mediaContentModel != null)
        //        {
        //            if (newMediaContentDto.MediaList != null)
        //            {
        //                mediaContentModel.TotalMediaCount += newMediaContentDto.MediaList.Count;
        //                mediaContentModel.AlbumImageUrl = newMediaContentDto.AlbumImageUrl;
        //            }
        //        }
        //        else
        //        {
        //            mediaContentModel = new MediaContentModel();
        //            mediaContentModel.LoadData(newMediaContentDto);
        //            if (newMediaContentDto.MediaList != null)
        //                mediaContentModel.TotalMediaCount = newMediaContentDto.MediaList.Count;
        //            tmp_Coll.Add(mediaContentModel);
        //        }
        //    }));
        //}
        //public void PreviousOrNextFeedComments(int scl, long tm, long nfid, long cntId = 0, long imgId = 0)
        //{
        //    new PreviousOrNextFeedComments().StartThread(scl, tm, nfid, cntId, imgId);
        //}
        //public void Process_MEDIA_SHARE_LIST_250(JObject _JobjFromResponse)
        //{
        //    processe_250_115(_JobjFromResponse);
        //}

        //public void Process_MEDIA_ALBUM_LIST_256(JObject _JobjFromResponse)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.UserTableID] != null && _JobjFromResponse[JsonKeys.MediaType] != null)
        //        {
        //            long utid = (long)_JobjFromResponse[JsonKeys.UserTableID];
        //            int mediatype = (int)_JobjFromResponse[JsonKeys.MediaType];
        //            ObservableCollection<MediaContentModel> ViewCollection = null;
        //            if (utid == DefaultSettings.LOGIN_TABLE_ID)
        //            {
        //                ViewCollection = (mediatype == SettingsConstants.MEDIA_TYPE_AUDIO) ? RingIDViewModel.Instance.MyAudioAlbums : RingIDViewModel.Instance.MyVideoAlbums;
        //                if (DownloadOrAddToAlbumPopUpWrapper.ucDownloadOrAddToAlbumPopUp != null
        //                    && DownloadOrAddToAlbumPopUpWrapper.ucDownloadOrAddToAlbumPopUp.IsVisible)
        //                {
        //                    DownloadOrAddToAlbumPopUpWrapper.ucDownloadOrAddToAlbumPopUp.AlbumCount = (DownloadOrAddToAlbumPopUpWrapper.ucDownloadOrAddToAlbumPopUp.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO) ? DefaultSettings.MY_AUDIO_ALBUMS_COUNT : DefaultSettings.MY_VIDEO_ALBUMS_COUNT;
        //                }
        //            }
        //            else if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendBasicInfoModel != null
        //                && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendBasicInfoModel.ShortInfoModel.UserTableID == utid)
        //            {
        //                ViewCollection = (mediatype == SettingsConstants.MEDIA_TYPE_AUDIO) ? UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileMusicAlbum.FriendAudioAlbums : UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileVideoAlbum.FriendVideoAlbums;
        //            }

        //            if (_JobjFromResponse[JsonKeys.MediaAlbumList] != null)
        //            {
        //                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaAlbumList];
        //                foreach (JObject obj in jarray)
        //                {
        //                    long albmId = (long)obj[JsonKeys.Id];
        //                    MediaContentModel mediaContentModel = ViewCollection.Where(P => P.AlbumId == albmId).FirstOrDefault();
        //                    if (mediaContentModel == null)
        //                    {
        //                        mediaContentModel = new MediaContentModel();
        //                        ViewCollection.InvokeAdd(mediaContentModel);
        //                    }
        //                    mediaContentModel.LoadData(obj);
        //                    mediaContentModel.AlbumId = albmId;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("ProcessMediaAlbumList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }
        //}

        //public void Process_MEDIA_ALBUM_CONTENT_LIST_261(JObject _JobjFromResponse)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaContentList] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.UserTableID] != null && _JobjFromResponse[JsonKeys.AlbumId] != null)
        //        {
        //            long utid = (long)_JobjFromResponse[JsonKeys.UserTableID];
        //            int mediaType = (int)_JobjFromResponse[JsonKeys.MediaType];
        //            long albumId = (long)_JobjFromResponse[JsonKeys.AlbumId];
        //            ObservableCollection<SingleMediaModel> ViewCollection = null;
        //            if (UCMediaContentsView.Instance != null && UCMediaContentsView.Instance.IsVisible && UCMediaContentsView.Instance.albumModel != null && UCMediaContentsView.Instance.albumModel.AlbumId == albumId && UCMediaContentsView.Instance.albumModel.MediaList != null)
        //            {
        //                ViewCollection = UCMediaContentsView.Instance.albumModel.MediaList;
        //            }
        //            else if (MainSwitcher.PopupController.MediaListFromAlbumClickWrapper != null
        //               && MainSwitcher.PopupController.MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick != null
        //               && MainSwitcher.PopupController.MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.albumModel != null
        //               && MainSwitcher.PopupController.MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.albumModel.AlbumId == albumId
        //               && MainSwitcher.PopupController.MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.IsVisible)
        //            {
        //                ViewCollection = MainSwitcher.PopupController.MediaListFromAlbumClickWrapper.ucMediaListFromAlbumClick.albumModel.MediaList;
        //            }
        //            if (_JobjFromResponse[JsonKeys.MediaContentList] != null)
        //            {
        //                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaContentList];
        //                foreach (JObject ob in jarray)
        //                {
        //                    long contentid = (long)ob[JsonKeys.ContentId];
        //                    SingleMediaModel contentModel = null;
        //                    if (!MediaDataContainer.Instance.ContentModels.TryGetValue(contentid, out contentModel))
        //                    {
        //                        contentModel = new SingleMediaModel();
        //                        MediaDataContainer.Instance.ContentModels[contentid] = contentModel;
        //                    }
        //                    contentModel.LoadData(ob);
        //                    contentModel.UserTableID = utid;
        //                    contentModel.AlbumId = albumId;
        //                    ViewCollection.InvokeAdd(contentModel);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("ProcessMediaAlbumContentList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
        //    }
        //}

        //public void Process_MEDIA_LIKE_LIST_269(JObject _JobjFromResponse)
        //{
        //    try
        //    {
        //        if ((bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ContentId] != null)
        //        {
        //            long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
        //            if (_JobjFromResponse[JsonKeys.Likes] != null && LikeListViewWrapper != null && LikeListViewWrapper.ucLikeListView != null && LikeListViewWrapper.ucLikeListView.ContentId == contentId)
        //            {
        //                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Likes];
        //                foreach (JObject singleObj in jarray)
        //                {
        //                    long utId = (singleObj[JsonKeys.UserTableID] != null) ? (long)singleObj[JsonKeys.UserTableID] : 0;
        //                    long uId = (singleObj[JsonKeys.UserIdentity] != null) ? (long)singleObj[JsonKeys.UserIdentity] : 0;
        //                    string fn = (singleObj[JsonKeys.FullName] != null) ? (string)singleObj[JsonKeys.FullName] : string.Empty;
        //                    string prIm = (singleObj[JsonKeys.ProfileImage] != null) ? (string)singleObj[JsonKeys.ProfileImage] : string.Empty;
        //                    UserBasicInfoModel userModel = RingIDViewModel.Instance.GetUserBasicInfoModelFromServerNotNullable(utId, uId, fn, prIm);
        //                    LikeListViewWrapper.ucLikeListView.LikeList.InvokeAdd(userModel);
        //                }
        //                Application.Current.Dispatcher.Invoke((Action)(() =>
        //                {
        //                    LikeListViewWrapper.ucLikeListView.SetVisibilities();
        //                }));
        //            }
        //        }
        //        else if ((bool)_JobjFromResponse[JsonKeys.Success] == false)
        //        {
        //            Application.Current.Dispatcher.Invoke((Action)(() =>
        //            {
        //                LikeListViewWrapper.ucLikeListView.HideLoaderandSeeMore();
        //            }));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("ProcessMediaLikeList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
        //    }
        //}

        //public void Process_MEDIA_COMMENT_LIST_270(JObject _JobjFromResponse)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && _JobjFromResponse[JsonKeys.ContentId] != null)
        //        {
        //            long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
        //            long nfId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
        //            long postOwnerId = 0;
        //            ObservableCollection<CommentModel> collectionMdaView = null; // collectionFeed = null, 
        //            bool success = (bool)_JobjFromResponse[JsonKeys.Success];
        //            //FeedModel feedModel = null;
        //            //if (nfId > 0 && FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out feedModel))
        //            //{
        //            //    if (feedModel.CommentList == null) feedModel.CommentList = new ObservableCollection<CommentModel>();
        //            //    collectionFeed = feedModel.CommentList;
        //            //    if (feedModel.UserShortInfoModel != null)
        //            //        postOwnerId = feedModel.UserShortInfoModel.UserIdentity;
        //            //    if (!success) feedModel.NoMoreComments = true;
        //            //}
        //            if (BasicMediaViewWrapper.ucBasicMediaView != null && RingPlayerViewModel.Instance.ClickedMediaID == contentId)
        //            {
        //                RingPlayerViewModel.Instance.OnStopAndRemoveAnimationRequested();
        //                if (BasicMediaViewWrapper.ucBasicMediaView.ImageComments != null)
        //                {
        //                    collectionMdaView = BasicMediaViewWrapper.ucBasicMediaView.ImageComments.ImageCommentList;
        //                    if (!success) BasicMediaViewWrapper.ucBasicMediaView.ImageComments.PreviousCommentsVisibility = Visibility.Collapsed;
        //                }
        //                if (BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage != null && BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.UserShortInfoModel != null)
        //                    postOwnerId = BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.UserShortInfoModel.UserTableID;
        //            }
        //            if (_JobjFromResponse[JsonKeys.Comments] != null)
        //            {
        //                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Comments];
        //                foreach (JObject singleObj in jarray)
        //                {
        //                    //if (collectionFeed != null)
        //                    //    HelperMethods.InsertIntoCommentListByAscTime(collectionFeed, singleObj, nfId, 0, contentId, postOwnerId);
        //                    if (collectionMdaView != null)
        //                        HelperMethods.InsertIntoCommentListByAscTime(collectionMdaView, singleObj, nfId, 0, contentId, postOwnerId);
        //                }
        //            }
        //            //if (feedModel != null && feedModel.CommentList != null && feedModel.CommentList.Count >= feedModel.NumberOfComments)
        //            //    feedModel.NoMoreComments = true;
        //            if (BasicMediaViewWrapper.ucBasicMediaView != null && RingPlayerViewModel.Instance.ClickedMediaID == contentId && BasicMediaViewWrapper.ucBasicMediaView.ImageComments != null && BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage != null)
        //            {
        //                BasicMediaViewWrapper.ucBasicMediaView.ImageComments.PreviousCommentsVisibility = (BasicMediaViewWrapper.ucBasicMediaView.ImageComments.ImageCommentList.Count >= BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.NumberOfComments) ? Visibility.Collapsed : Visibility.Visible;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("ProcessMediaCommentList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }
        //}

        //public void Process_MEDIACOMMENT_LIKE_LIST_271(JObject _JobjFromResponse)
        //{
        //    try
        //    {
        //        if ((bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ContentId] != null && _JobjFromResponse[JsonKeys.CommentId] != null)
        //        {
        //            long contentId = (long)_JobjFromResponse[JsonKeys.ContentId];
        //            long commentId = (long)_JobjFromResponse[JsonKeys.CommentId];
        //            if (_JobjFromResponse[JsonKeys.Likes] != null && LikeListViewWrapper != null && LikeListViewWrapper.ucLikeListView != null && LikeListViewWrapper.ucLikeListView.ContentId == contentId && LikeListViewWrapper.ucLikeListView.CommentId == commentId)
        //            {
        //                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Likes];
        //                foreach (JObject singleObj in jarray)
        //                {
        //                    long utId = (singleObj[JsonKeys.UserTableID] != null) ? (long)singleObj[JsonKeys.UserTableID] : 0;
        //                    long uId = (singleObj[JsonKeys.UserIdentity] != null) ? (long)singleObj[JsonKeys.UserIdentity] : 0;
        //                    string fn = (singleObj[JsonKeys.FullName] != null) ? (string)singleObj[JsonKeys.FullName] : string.Empty;
        //                    string prIm = (singleObj[JsonKeys.ProfileImage] != null) ? (string)singleObj[JsonKeys.ProfileImage] : string.Empty;
        //                    UserBasicInfoModel userModel = RingIDViewModel.Instance.GetUserBasicInfoModelFromServerNotNullable(utId, uId, fn, prIm);
        //                    LikeListViewWrapper.ucLikeListView.LikeList.InvokeAdd(userModel);
        //                }
        //                Application.Current.Dispatcher.Invoke((Action)(() =>
        //                {
        //                    LikeListViewWrapper.ucLikeListView.SetVisibilities();
        //                }));
        //            }
        //        }
        //        else if ((bool)_JobjFromResponse[JsonKeys.Success] == false)
        //        {
        //            Application.Current.Dispatcher.Invoke((Action)(() =>
        //            {
        //                LikeListViewWrapper.ucLikeListView.HideLoaderandSeeMore();
        //            }));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("ProcessMediaCommentLikeList ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
        //    }
        //}
        //public void Process_TYPE_CELEBRITY_FEED_286(JObject _JobjFromResponse)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] == true
        //            && _JobjFromResponse[JsonKeys.CelebrityList] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.CelebrityListType] != null)// && _JobjFromResponse[JsonKeys.SearchParam] != null && _JobjFromResponse[JsonKeys.Subscribe] != null)
        //        {
        //            int profileType = (_JobjFromResponse[JsonKeys.ProfileType] != null) ? ((int)_JobjFromResponse[JsonKeys.ProfileType]) : SettingsConstants.PROFILE_TYPE_CELEBRITY;
        //            int subscType = (int)_JobjFromResponse[JsonKeys.CelebrityListType];
        //            JArray jarray = (JArray)_JobjFromResponse[JsonKeys.CelebrityList];
        //            List<CelebrityDTO> list = new List<CelebrityDTO>();
        //            foreach (JObject singleObj in jarray)
        //            {
        //                CelebrityDTO dto = HelperMethodsModel.BindCelebrityDetails(singleObj);
        //                list.Add(dto);
        //            }
        //            ShowFollowingOrDiscoverCelebrities(subscType, list);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("Celebrity ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }
        //}

        //public void Process_TYPE_CELEBRITY_FEED_288(JObject _JobjFromResponse, string client_packet_id)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
        //        {
        //            if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
        //            {
        //                int feed_state = client_packet_id.Equals(DefaultSettings.START_CELEBRITYFEEDS_PACKETID) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
        //                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
        //                foreach (JObject singleObj in jarray)
        //                {
        //                    long nfId = (singleObj[JsonKeys.NewsfeedId] != null) ? (long)singleObj[JsonKeys.NewsfeedId] : 0;
        //                    FeedModel model = null;
        //                    if (!FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out model)) { model = new FeedModel(); FeedDataContainer.Instance.FeedModels[nfId] = model; }
        //                    model.LoadData(singleObj);
        //                    FeedDataContainer.Instance.InsertIntoSortedList(FeedDataContainer.Instance.AllCelebrityFeedsSortedIds, model.ActualTime, nfId);
        //                    if (feed_state == SettingsConstants.FEED_FIRSTTIME)
        //                    {
        //                        if (UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel != null
        //                            && UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesFeedPanel != null
        //                            && UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesFeedPanel.IsVisible)
        //                            FirstTimeFeedLoadWorker.Instance.LoadFirstTimeFeed(RingIDViewModel.Instance.AllCelebrityFeeds, model);
        //                        else if (!RingIDViewModel.Instance.AllCelebrityFeeds.Any(P => P.NewsfeedId == model.NewsfeedId))
        //                            RingIDViewModel.Instance.AllCelebrityFeeds.InsertModel(model);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("AllMediaPageFeeds ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }
        //}
        //public void Process_NEWSPORTAL_FEED_295(JObject _JobjFromResponse, string client_packet_id)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
        //        {
        //            if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
        //            {
        //                int feed_state = client_packet_id.Equals(DefaultSettings.START_NEWSPORTALFEEDS_PACKETID) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
        //                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
        //                foreach (JObject singleObj in jarray)
        //                {
        //                    long nfId = (singleObj[JsonKeys.NewsfeedId] != null) ? (long)singleObj[JsonKeys.NewsfeedId] : 0;
        //                    FeedModel model = null;
        //                    if (!FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out model)) { model = new FeedModel(); FeedDataContainer.Instance.FeedModels[nfId] = model; }
        //                    model.LoadData(singleObj);

        //                    FeedDataContainer.Instance.InsertIntoSortedList(FeedDataContainer.Instance.AllNewsPortalFeedsSortedIds, model.ActualTime, nfId);
        //                    if (feed_state == SettingsConstants.FEED_FIRSTTIME)
        //                    {
        //                        if (UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel != null
        //                            && UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel != null
        //                            && UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCNewsContainerPanel != null
        //                            && UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCNewsContainerPanel.IsVisible)
        //                            FirstTimeFeedLoadWorker.Instance.LoadFirstTimeFeed(RingIDViewModel.Instance.AllNewsPortalFeeds, model);
        //                        else if (!RingIDViewModel.Instance.AllNewsPortalFeeds.Any(P => P.NewsfeedId == model.NewsfeedId))
        //                            RingIDViewModel.Instance.AllNewsPortalFeeds.InsertModel(model);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("ProcessNewsPortalFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }
        //}
        //public void Process_NEWSPORTAL_BREAKING_FEED_302(JObject _JobjFromResponse)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.BreakingNewsFeeds] != null)
        //        {
        //            JArray jarray = (JArray)_JobjFromResponse[JsonKeys.BreakingNewsFeeds];
        //            List<FeedDTO> breakingNws = new List<FeedDTO>();
        //            foreach (JObject singleObj in jarray)
        //            {
        //                FeedDTO dto = new FeedDTO();
        //                dto.NewsInfo = new NewsDTO();
        //                dto.NewsfeedId = dto.NewsInfo.NewsId = (singleObj[JsonKeys.Id] != null) ? ((long)(singleObj[JsonKeys.Id])) : 0;
        //                dto.ProfileImage = (singleObj[JsonKeys.ProfileImage] != null) ? ((string)(singleObj[JsonKeys.ProfileImage])) : string.Empty;
        //                dto.NewsInfo.NewsShortDescription = (singleObj[JsonKeys.NewsShortDescription] != null) ? ((string)(singleObj[JsonKeys.NewsShortDescription])) : string.Empty;
        //                dto.NewsInfo.NewsTitle = (singleObj[JsonKeys.NewsPortalTitle] != null) ? ((string)(singleObj[JsonKeys.NewsPortalTitle])) : string.Empty;
        //                breakingNws.Add(dto);
        //            }
        //            BreakingNewsSliderUI(breakingNws, SettingsConstants.PROFILE_TYPE_NEWSPORTAL);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("ProcessBreakingNewsPortalFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }
        //}

        //public void Process_BUSINESSPAGE_BREAKING_FEED_303(JObject _JobjFromResponse)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.BreakingNewsFeeds] != null)
        //        {
        //            JArray jarray = (JArray)_JobjFromResponse[JsonKeys.BreakingNewsFeeds];
        //            List<FeedDTO> breakingNws = new List<FeedDTO>();
        //            foreach (JObject singleObj in jarray)
        //            {
        //                FeedDTO dto = new FeedDTO();
        //                dto.NewsInfo = new NewsDTO();
        //                dto.NewsfeedId = dto.NewsInfo.NewsId = (singleObj[JsonKeys.Id] != null) ? ((long)(singleObj[JsonKeys.Id])) : 0;
        //                dto.ProfileImage = (singleObj[JsonKeys.ProfileImage] != null) ? ((string)(singleObj[JsonKeys.ProfileImage])) : string.Empty;
        //                dto.NewsInfo.NewsShortDescription = (singleObj[JsonKeys.NewsShortDescription] != null) ? ((string)(singleObj[JsonKeys.NewsShortDescription])) : string.Empty;
        //                dto.NewsInfo.NewsTitle = (singleObj[JsonKeys.NewsPortalTitle] != null) ? ((string)(singleObj[JsonKeys.NewsPortalTitle])) : string.Empty;
        //                breakingNws.Add(dto);
        //            }
        //            BreakingNewsSliderUI(breakingNws, SettingsConstants.PROFILE_TYPE_PAGES);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("ProcessBreakingNewsPortalFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }
        //}

        //public void Process_BUSINESS_PAGE_FEED_306(JObject _JobjFromResponse, string client_packet_id)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
        //        {
        //            if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
        //            {
        //                int feed_state = client_packet_id.Equals(DefaultSettings.START_PAGESFEEDS_PACKETID) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
        //                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
        //                foreach (JObject singleObj in jarray)
        //                {
        //                    long nfId = (singleObj[JsonKeys.NewsfeedId] != null) ? (long)singleObj[JsonKeys.NewsfeedId] : 0;
        //                    FeedModel model = null;
        //                    if (!FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out model)) { model = new FeedModel(); FeedDataContainer.Instance.FeedModels[nfId] = model; }
        //                    model.LoadData(singleObj);
        //                    FeedDataContainer.Instance.InsertIntoSortedList(FeedDataContainer.Instance.AllPagesFeedsSortedIds, model.ActualTime, nfId);
        //                    if (feed_state == SettingsConstants.FEED_FIRSTTIME)
        //                    {
        //                        if (UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel != null
        //                            && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel != null
        //                            && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCNewsContainerPanel != null
        //                            && UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCNewsContainerPanel.IsVisible)
        //                            FirstTimeFeedLoadWorker.Instance.LoadFirstTimeFeed(RingIDViewModel.Instance.AllPagesFeeds, model);
        //                        else if (!RingIDViewModel.Instance.AllPagesFeeds.Any(P => P.NewsfeedId == model.NewsfeedId))
        //                            RingIDViewModel.Instance.AllPagesFeeds.InsertModel(model);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("ProcessPagesFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }
        //}

        //public void Process_MEDIA_PAGE_FEED_307(JObject _JobjFromResponse, string client_packet_id)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
        //        {
        //            if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
        //            {
        //                int feed_state = client_packet_id.Equals(DefaultSettings.START_MEDIACLOUDFEEDS_PACKETID) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
        //                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
        //                foreach (JObject singleObj in jarray)
        //                {
        //                    long nfId = (singleObj[JsonKeys.NewsfeedId] != null) ? (long)singleObj[JsonKeys.NewsfeedId] : 0;
        //                    FeedModel model = null;
        //                    if (!FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out model)) { model = new FeedModel(); FeedDataContainer.Instance.FeedModels[nfId] = model; }
        //                    model.LoadData(singleObj);
        //                    FeedDataContainer.Instance.InsertIntoSortedList(FeedDataContainer.Instance.AllMediaPageFeedsSortedIds, model.ActualTime, nfId);
        //                    if (feed_state == SettingsConstants.FEED_FIRSTTIME)
        //                    {
        //                        if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null
        //                            && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudFeedsPanel != null
        //                            && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudFeedsPanel.IsVisible)
        //                            FirstTimeFeedLoadWorker.Instance.LoadFirstTimeFeed(RingIDViewModel.Instance.AllMediaPageFeeds, model);
        //                        else if (!RingIDViewModel.Instance.AllMediaPageFeeds.Any(P => P.NewsfeedId == model.NewsfeedId))
        //                            RingIDViewModel.Instance.AllMediaPageFeeds.InsertModel(model);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("AllMediaPageFeeds ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }
        //}

        //public void Process_MEDIA_PAGE_TRENDING_FEED_308(JObject _JobjFromResponse)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
        //        {
        //            BreakingSequenceCount++;
        //            int seqTotal = Convert.ToInt32(((string)_JobjFromResponse[JsonKeys.Sequence]).Split('/')[1]);
        //            if (_JobjFromResponse[JsonKeys.NewsFeedList] != null
        //                && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null
        //                && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudFeedsPanel != null)
        //            {
        //                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
        //                foreach (JObject singleObj in jarray)
        //                {
        //                    FeedModel model = new FeedModel();
        //                    model.LoadData(singleObj);
        //                    Application.Current.Dispatcher.Invoke((Action)(() =>
        //                    {
        //                        if (model.MusicPageModel != null &&
        //                            !RingIDViewModel.Instance.BreakingMediaCloudFeeds.Any(P => P.NewsfeedId == model.NewsfeedId))
        //                            RingIDViewModel.Instance.BreakingMediaCloudFeeds.Add(model);
        //                    }));
        //                }
        //            }
        //            if (BreakingSequenceCount == seqTotal)
        //            {
        //                BreakingSequenceCount = 0;
        //                UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudFeedsPanel.LoadSliderData();
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("ProcessPagesFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }
        //}
        //public void ShowFollowingOrDiscoverCelebrities(int subscType, Object ob)
        //{
        //    try
        //    {
        //        Application.Current.Dispatcher.Invoke((Action)(() =>
        //        {
        //            if (ob is List<CelebrityDTO>)
        //            {
        //                List<CelebrityDTO> list = (List<CelebrityDTO>)ob;
        //                if (subscType == 1)//Discover
        //                {
        //                    foreach (var item in list)
        //                    {
        //                        CelebrityModel model = RingIDViewModel.Instance.CelebritiesDiscovered.Where(P => P.UserIdentity == item.RingID).FirstOrDefault();
        //                        if (model == null)
        //                        {
        //                            model = RingIDViewModel.Instance.CelebritiesFollowing.Where(P => P.UserIdentity == item.RingID).FirstOrDefault();
        //                            if (model == null)
        //                                model = RingIDViewModel.Instance.CelebritiesPopular.Where(P => P.UserIdentity == item.RingID).FirstOrDefault();
        //                            if (model == null) model = new CelebrityModel();
        //                            RingIDViewModel.Instance.CelebritiesDiscovered.InvokeAdd(model);
        //                        }
        //                        model.LoadData(item);
        //                    }
        //                }
        //                else if (subscType == 2)//Popular
        //                {
        //                    foreach (var item in list)
        //                    {
        //                        CelebrityModel model = RingIDViewModel.Instance.CelebritiesPopular.Where(P => P.UserIdentity == item.RingID).FirstOrDefault();
        //                        if (model == null)
        //                        {
        //                            model = RingIDViewModel.Instance.CelebritiesDiscovered.Where(P => P.UserIdentity == item.RingID).FirstOrDefault();
        //                            if (model == null)
        //                                model = RingIDViewModel.Instance.CelebritiesFollowing.Where(P => P.UserIdentity == item.RingID).FirstOrDefault();
        //                            if (model == null) model = new CelebrityModel();
        //                            RingIDViewModel.Instance.CelebritiesPopular.Add(model);
        //                        }
        //                        model.LoadData(item);
        //                    }
        //                }
        //                else if (subscType == 3)//Following
        //                {
        //                    foreach (var item in list)
        //                    {
        //                        CelebrityModel model = RingIDViewModel.Instance.CelebritiesFollowing.Where(P => P.UserIdentity == item.RingID).FirstOrDefault();
        //                        if (model == null)
        //                        {
        //                            model = RingIDViewModel.Instance.CelebritiesDiscovered.Where(P => P.UserIdentity == item.RingID).FirstOrDefault();
        //                            if (model == null)
        //                                model = RingIDViewModel.Instance.CelebritiesPopular.Where(P => P.UserIdentity == item.RingID).FirstOrDefault();
        //                            if (model == null) model = new CelebrityModel();
        //                            RingIDViewModel.Instance.CelebritiesFollowing.InvokeAdd(model);
        //                        }
        //                        model.LoadData(item);
        //                    }
        //                }
        //            }
        //        }));
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("ex in SearchedDiscoverCelebrities==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }
        //}
        //public void LoadStatusLikes(long nfId, List<UserBasicInfoDTO> users)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //   {
        //       try
        //       {
        //           if (LikeListViewWrapper != null && LikeListViewWrapper.ucLikeListView != null)
        //           {
        //               foreach (UserBasicInfoDTO user in users)
        //               {
        //                   if (!LikeListViewWrapper.ucLikeListView.LikeList.Any(P => P.ShortInfoModel.UserIdentity == user.RingID))
        //                   {
        //                       UserBasicInfoModel model = GetExistingorNewUserBasicModelFromDTO(user);
        //                       LikeListViewWrapper.ucLikeListView.LikeList.Add(model);
        //                   }
        //               }
        //               LikeListViewWrapper.ucLikeListView.SetVisibilities();
        //           }
        //       }
        //       catch (Exception ex)
        //       {
        //           log.Error("Error: LoadStatusLikes() => " + ex.Message + "\n" + ex.StackTrace);
        //       }
        //   }));
        //}
        //public void LoadLikeList(List<UserBasicInfoDTO> likers, long nfId, long imageId, long contentId, long commentId)
        //{
        //    Application.Current.Dispatcher.Invoke((Action)(() =>
        //    {
        //        try
        //        {
        //            if (LikeListViewWrapper != null && LikeListViewWrapper.ucLikeListView != null)
        //            {
        //                foreach (UserBasicInfoDTO user in likers)
        //                {
        //                    if (!LikeListViewWrapper.ucLikeListView.LikeList.Any(P => P.ShortInfoModel.UserIdentity == user.RingID))
        //                    {
        //                        UserBasicInfoModel model = GetExistingorNewUserBasicModelFromDTO(user);
        //                        LikeListViewWrapper.ucLikeListView.LikeList.Add(model);
        //                    }
        //                }
        //                LikeListViewWrapper.ucLikeListView.SetVisibilities();
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            log.Error("Error: LoadLikeList() => " + ex.Message + "\n" + ex.StackTrace);
        //        }
        //    }));
        //}

        //public void Process_MEDIA_SUGGESTION_277(JObject _JobjFromResponse)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.SearchParam] != null && _JobjFromResponse[JsonKeys.MediaSuggestion] != null)
        //        {
        //            string searchParam = (string)_JobjFromResponse[JsonKeys.SearchParam];
        //            List<SearchMediaDTO> searchListOfaSearchParam = null;
        //            lock (MediaDictionaries.Instance.TEMP_SEARCH_SGTNS)
        //            {
        //                if (!MediaDictionaries.Instance.TEMP_SEARCH_SGTNS.TryGetValue(searchParam, out searchListOfaSearchParam))
        //                {
        //                    searchListOfaSearchParam = new List<SearchMediaDTO>();
        //                    MediaDictionaries.Instance.TEMP_SEARCH_SGTNS.Add(searchParam, searchListOfaSearchParam);
        //                }
        //            }
        //            JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaSuggestion];
        //            List<SearchMediaDTO> lst = new List<SearchMediaDTO>();
        //            foreach (JObject singleObj in jarray)
        //            {
        //                SearchMediaDTO dto = new SearchMediaDTO();
        //                dto.SearchSuggestion = (string)singleObj[JsonKeys.MediaSearchKey];
        //                dto.SearchType = (int)singleObj[JsonKeys.MediaSuggestionType];
        //                lst.Add(dto);
        //                searchListOfaSearchParam.Add(dto);
        //            }
        //            if (lst.Count > 0)
        //                LoadMediaSearchs(searchParam, lst);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("ProcessMediaSearchSuggestions ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }
        //}

        //public void Process_MEDIA_CONTENTS_BASED_ON_KEYWORD_278(JObject _JobjFromResponse)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.SearchParam] != null && _JobjFromResponse[JsonKeys.MediaSuggestionType] != null)
        //        {
        //            int suggestionType = (int)_JobjFromResponse[JsonKeys.MediaSuggestionType];
        //            string searchParam = (string)_JobjFromResponse[JsonKeys.SearchParam];
        //            if ((bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaList] != null)
        //            {
        //                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaList];
        //                if (suggestionType == SettingsConstants.MEDIA_SEARCH_TYPE_SONGS)
        //                {
        //                    if (UCMediaContentsView.Instance != null && UCMediaContentsView.Instance.SearchParam != null && UCMediaContentsView.Instance.SearchParam.Equals(searchParam))
        //                    {
        //                        foreach (JObject singleObj in jarray)
        //                        {
        //                            long contentid = (long)singleObj[JsonKeys.ContentId];
        //                            SingleMediaModel contentModel = null;
        //                            if (!MediaDataContainer.Instance.ContentModels.TryGetValue(contentid, out contentModel))
        //                            {
        //                                contentModel = new SingleMediaModel();
        //                                MediaDataContainer.Instance.ContentModels[contentid] = contentModel;
        //                            }
        //                            contentModel.LoadData(singleObj);
        //                            UCMediaContentsView.Instance.SingleMediaItems.InvokeAdd(contentModel);
        //                        }
        //                    }
        //                }
        //                else if (suggestionType == SettingsConstants.MEDIA_SEARCH_TYPE_ALBUMS)
        //                {
        //                    foreach (JObject singleObj in jarray)
        //                    {
        //                        long albumtid = (long)singleObj[JsonKeys.AlbumId];

        //                        if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudSearch != null
        //                            && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudSearch.View_FullSearch != null
        //                            && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudSearch.SelectedSearchedParam.Equals(searchParam))
        //                        {
        //                            MediaContentModel contentModel = new MediaContentModel();
        //                            contentModel.LoadData(singleObj);
        //                            UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudSearch.View_FullSearch.AlbumsFromSearch.InvokeAdd(contentModel);
        //                        }
        //                    }
        //                }
        //                else if (suggestionType == SettingsConstants.MEDIA_SEARCH_TYPE_TAGS)
        //                {
        //                    foreach (JObject singleObj in jarray)
        //                    {
        //                        long hashTagId = (long)singleObj[JsonKeys.AlbumId];
        //                        if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudSearch != null
        //                            && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudSearch.View_FullSearch != null
        //                            && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudSearch.SelectedSearchedParam.Equals(searchParam))
        //                        {
        //                            Application.Current.Dispatcher.BeginInvoke(() =>
        //                            {
        //                                if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudSearch.View_FullSearch.hashGIFCtrl.IsRunning())
        //                                    UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudSearch.View_FullSearch.hashGIFCtrl.StopAnimate();
        //                            }, DispatcherPriority.Send);

        //                            HashTagModel model = new HashTagModel();
        //                            model.LoadData(singleObj);
        //                            UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudSearch.View_FullSearch.HashTagsFromSearch.InvokeAdd(model);
        //                        }
        //                    }
        //                }
        //                else if (suggestionType == 0)
        //                {
        //                    if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudSearch != null
        //                       && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudSearch.View_FullSearch != null
        //                       && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudSearch.SelectedSearchedParam.Equals(searchParam))
        //                    {
        //                        Application.Current.Dispatcher.BeginInvoke(() =>
        //                        {
        //                            if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudSearch.View_FullSearch.hashGIFCtrl.IsRunning())
        //                                UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudSearch.View_FullSearch.hashGIFCtrl.StopAnimate();
        //                        }, DispatcherPriority.Send);
        //                        foreach (JObject singleObj in jarray)
        //                        {
        //                            int sugType = (singleObj[JsonKeys.MediaSuggestionType] != null) ? (int)singleObj[JsonKeys.MediaSuggestionType] : 0;
        //                            switch (sugType)
        //                            {
        //                                case SettingsConstants.MEDIA_SEARCH_TYPE_SONGS:
        //                                    long contentId = (singleObj[JsonKeys.ContentId] != null) ? (long)singleObj[JsonKeys.ContentId] : 0;
        //                                    SingleMediaModel contentModel = null;
        //                                    if (!MediaDataContainer.Instance.ContentModels.TryGetValue(contentId, out contentModel))
        //                                    {
        //                                        contentModel = new SingleMediaModel();
        //                                        MediaDataContainer.Instance.ContentModels[contentId] = contentModel;
        //                                    }
        //                                    contentModel.LoadData(singleObj);
        //                                    UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudSearch.View_FullSearch.MediasAllTab.InvokeAdd(contentModel);
        //                                    break;
        //                                case SettingsConstants.MEDIA_SEARCH_TYPE_ALBUMS:
        //                                    long albumId = (singleObj[JsonKeys.AlbumId] != null) ? (long)singleObj[JsonKeys.AlbumId] : 0;
        //                                    MediaContentModel model = new MediaContentModel();
        //                                    model.LoadData(singleObj);
        //                                    UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudSearch.View_FullSearch.AlbumsAllTab.InvokeAdd(model);
        //                                    break;
        //                                case SettingsConstants.MEDIA_SEARCH_TYPE_TAGS:
        //                                    long hashTgId = (singleObj[JsonKeys.HashTagId] != null) ? (long)singleObj[JsonKeys.HashTagId] : 0;
        //                                    HashTagModel hashModel = new HashTagModel();
        //                                    hashModel.LoadData(singleObj);
        //                                    UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudSearch.View_FullSearch.HashTagsAllTab.InvokeAdd(hashModel);
        //                                    break;
        //                                default:
        //                                    break;
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("ProcessMediaSearchSuggestions ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }
        //}

        //public void Process_HASHTAG_MEDIA_CONTENTS_279(JObject _JobjFromResponse)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.MediaList] != null && _JobjFromResponse[JsonKeys.Sequence] != null && _JobjFromResponse[JsonKeys.HashTagId] != null)
        //        {
        //            long hashtagId = (long)_JobjFromResponse[JsonKeys.HashTagId];
        //            if (_JobjFromResponse[JsonKeys.MediaList] != null &&
        //                UCMediaContentsView.Instance != null && UCMediaContentsView.Instance.hashTagModel != null && UCMediaContentsView.Instance.hashTagModel.HashTagId == hashtagId && UCMediaContentsView.Instance.hashTagModel.MediaList != null)
        //            {
        //                JArray jarray = (JArray)_JobjFromResponse[JsonKeys.MediaList];
        //                foreach (JObject ob in jarray)
        //                {
        //                    long contentid = (long)ob[JsonKeys.ContentId];
        //                    SingleMediaModel contentModel = null;
        //                    if (!MediaDataContainer.Instance.ContentModels.TryGetValue(contentid, out contentModel))
        //                    {
        //                        contentModel = new SingleMediaModel();
        //                        MediaDataContainer.Instance.ContentModels[contentid] = contentModel;
        //                    }
        //                    contentModel.LoadData(ob);
        //                    if (!UCMediaContentsView.Instance.hashTagModel.MediaList.Any(P => P.ContentId == contentid))
        //                    {
        //                        UCMediaContentsView.Instance.hashTagModel.MediaList.InvokeAdd(contentModel);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("ProcessHashtagMediaContents ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
        //    }
        //}
    }
}
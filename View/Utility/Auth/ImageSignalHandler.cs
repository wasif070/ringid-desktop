﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Dictonary;
using View.UI;
using View.UI.PopUp;
using View.UI.Profile.FriendProfile;
using View.ViewModel;
using View.UI.Profile.CelebrityProfile;
using View.Utility.DataContainer;
using View.UI.Profile.MyProfile;
using View.Utility.Feed;

namespace View.Utility.Auth
{
    public class ImageSignalHandler
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(ImageSignalHandler).Name);
        //private int ImageCommentLikesSequeceCount = 0;
        #endregion"Fields"

        #region "Image Related"

        public void Process_COMMENTS_FOR_IMAGE_89(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && _JobjFromResponse[JsonKeys.ImageId] != null)
                {
                    Guid imageId = (Guid)_JobjFromResponse[JsonKeys.ImageId];
                    Guid newsfeedId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (Guid)_JobjFromResponse[JsonKeys.NewsfeedId] : Guid.Empty;
                    bool success = (bool)_JobjFromResponse[JsonKeys.Success];
                    long postOwnerId = 0;
                    ObservableCollection<CommentModel> collectionMdaView = RingIDViewModel.Instance.MediaComments;

                    if (success && ViewConstants.ImageID == imageId && _JobjFromResponse[JsonKeys.Comments] != null)
                    {
                        ImageModel imageModel = null;
                        if (ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(imageId, out imageModel))
                        {
                            postOwnerId = imageModel.UserTableID;
                        }
                        JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Comments];
                        foreach (JObject singleObj in jArray)
                        {
                            HelperMethods.InsertIntoCommentListByDscTime(collectionMdaView, singleObj, newsfeedId, Guid.Empty, imageId, postOwnerId);
                            System.Threading.Thread.Sleep(10);
                        }
                    }
                    if (_JobjFromResponse[JsonKeys.Comments] != null)
                    {
                        JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Comments];
                        foreach (JObject singleObj in jArray)
                        {
                            if (collectionMdaView != null)
                                HelperMethods.InsertIntoCommentListByAscTime(collectionMdaView, singleObj, newsfeedId, imageId, Guid.Empty, postOwnerId);
                        }
                    }
                }
            }
            catch (Exception e)
            {

                log.Error("ProcessImageComments ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

            }
        }

        public void Process_LIKES_FOR_IMAGE_93(JObject _JobjFromResponse)
        {
            try
            {
                if ((bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ImageId] != null)
                {
                    Guid imageId = (Guid)_JobjFromResponse[JsonKeys.ImageId];
                    JArray jArray = (JArray)_JobjFromResponse[JsonKeys.Likes];
                    if (imageId == ViewConstants.ImageID && jArray != null)
                    {
                        foreach (JObject singleObj in jArray)
                        {
                            long userTableID = (singleObj[JsonKeys.UserTableID] != null) ? (long)singleObj[JsonKeys.UserTableID] : 0;
                            long userIdentity = (singleObj[JsonKeys.UserIdentity] != null) ? (long)singleObj[JsonKeys.UserIdentity] : 0;
                            string fullName = (singleObj[JsonKeys.FullName] != null) ? (string)singleObj[JsonKeys.FullName] : string.Empty;
                            string profileImage = (singleObj[JsonKeys.ProfileImage] != null) ? (string)singleObj[JsonKeys.ProfileImage] : string.Empty;
                            UserBasicInfoModel userModel = RingIDViewModel.Instance.GetUserBasicInfoModelFromServerNotNullable(userTableID, userIdentity, fullName, profileImage);
                            RingIDViewModel.Instance.LikeList.InvokeAdd(userModel);
                        }
                    }
                }
            }
            catch (Exception e)
            {

                log.Error("ProcessLikersImage ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

            }
        }

        public void Process_IMAGE_COMMENT_LIKES_196(JObject _JobjFromResponse)
        {
            try
            {
                if ((bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ImageId] != null && _JobjFromResponse[JsonKeys.CommentId] != null)
                {
                    Guid imageId = (Guid)_JobjFromResponse[JsonKeys.ImageId];
                    Guid commentId = (Guid)_JobjFromResponse[JsonKeys.CommentId];
                    //if (_JobjFromResponse[JsonKeys.Likes] != null && LikeListViewWrapper != null && LikeListViewWrapper.ucLikeListView != null && LikeListViewWrapper.ucLikeListView.ImageId == imageId && LikeListViewWrapper.ucLikeListView.CommentId == commentId)
                    //{
                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.Likes];
                    foreach (JObject singleObj in jarray)
                    {
                        long userTableID = (singleObj[JsonKeys.UserTableID] != null) ? (long)singleObj[JsonKeys.UserTableID] : 0;
                        long userIdentity = (singleObj[JsonKeys.UserIdentity] != null) ? (long)singleObj[JsonKeys.UserIdentity] : 0;
                        string fullName = (singleObj[JsonKeys.FullName] != null) ? (string)singleObj[JsonKeys.FullName] : string.Empty;
                        string profileImage = (singleObj[JsonKeys.ProfileImage] != null) ? (string)singleObj[JsonKeys.ProfileImage] : string.Empty;
                        UserBasicInfoModel userModel = RingIDViewModel.Instance.GetUserBasicInfoModelFromServerNotNullable(userTableID, userIdentity, fullName, profileImage);
                        RingIDViewModel.Instance.LikeList.InvokeAdd(userModel);
                    }
                }
                else if ((bool)_JobjFromResponse[JsonKeys.Success] == false)
                {
                    //Application.Current.Dispatcher.Invoke((Action)(() =>
                    //{
                    //    LikeListViewWrapper.ucLikeListView.HideLoaderandSeeMore();
                    //}));
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessLikersImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_LIKE_UNLIKE_IMAGE_COMMENT_197(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse[JsonKeys.Success] != null)
                {
                    if ((bool)_JobjFromResponse[JsonKeys.Success])
                    {
                        //Guid newsfeedId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (Guid)_JobjFromResponse[JsonKeys.NewsfeedId] : Guid.Empty;
                        //Guid imageId = (_JobjFromResponse[JsonKeys.ImageId] != null) ? (Guid)_JobjFromResponse[JsonKeys.ImageId] : Guid.Empty;
                        //Guid commentId = (Guid)_JobjFromResponse[JsonKeys.CommentId];
                    }
                    else
                    {
                        string msg = "Failed to Like/Unlike!";
                        if (_JobjFromResponse[JsonKeys.Message] != null)
                            msg = (string)_JobjFromResponse[JsonKeys.Message];
                        ShowErrorMessageBox(msg, "Failed!");
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessLikeUnlikeImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_UPDATE_ADD_IMAGE_COMMENT_380(JObject _JobjFromResponse)
        {
            try
            {
                Guid newsfeedId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (Guid)_JobjFromResponse[JsonKeys.NewsfeedId] : Guid.Empty;
                Guid imageId = (_JobjFromResponse[JsonKeys.ImageId] != null) ? (Guid)_JobjFromResponse[JsonKeys.ImageId] : Guid.Empty;
                MainSwitcher.AuthSignalHandler().feedSignalHandler.AddOrUpdateAddAnyComment(_JobjFromResponse, newsfeedId, imageId, Guid.Empty, true);
            }
            catch (Exception e)
            {
                log.Error(" ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_UPDATE_DELETE_IMAGE_COMMENT_382(JObject _JobjFromResponse)
        {
            try
            {
                Guid newsfeedId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (Guid)_JobjFromResponse[JsonKeys.NewsfeedId] : Guid.Empty;
                Guid imageId = (_JobjFromResponse[JsonKeys.ImageId] != null) ? (Guid)_JobjFromResponse[JsonKeys.ImageId] : Guid.Empty;
                Guid commentId = (_JobjFromResponse[JsonKeys.CommentId] != null) ? (Guid)_JobjFromResponse[JsonKeys.CommentId] : Guid.Empty;
                MainSwitcher.AuthSignalHandler().feedSignalHandler.DeleteOrUpdateDeleteAnyComment(_JobjFromResponse, commentId, newsfeedId, imageId, Guid.Empty);
            }
            catch (Exception e)
            {
                log.Error(" ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_UPDATE_EDIT_IMAGE_COMMENT_394(JObject _JobjFromResponse)
        {
            try
            {
                Guid newsfeedId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (Guid)_JobjFromResponse[JsonKeys.NewsfeedId] : Guid.Empty;
                Guid imageId = (_JobjFromResponse[JsonKeys.ImageId] != null) ? (Guid)_JobjFromResponse[JsonKeys.ImageId] : Guid.Empty;
                Guid commentId = (_JobjFromResponse[JsonKeys.CommentId] != null) ? (Guid)_JobjFromResponse[JsonKeys.CommentId] : Guid.Empty;
                MainSwitcher.AuthSignalHandler().feedSignalHandler.EditOrUpdateEditAnyComment(_JobjFromResponse, commentId, newsfeedId, imageId, Guid.Empty);
            }
            catch (Exception e)
            {
                log.Error(" ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_UPDATE_LIKE_UNLIKE_IMAGE_COMMENT_397(JObject _JobjFromResponse)
        {
            //TODO
            //{"uId":"2110010304","sucs":true,"fn":"Sirat Test2","id":109,"cmnId":635,"tm":1442815321235,"imgId":1413,"lkd":1,"loc":1}
            //try
            //{
            //    if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ImageId] != null && _JobjFromResponse[JsonKeys.CommentId] != null && _JobjFromResponse[JsonKeys.Liked] != null)
            //    {
            //    }
            //}
            //catch (Exception e)
            //{

            //    log.Error("ProcessUpdateLikeUnlikeImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

            //}
        }

        #endregion

        #region "ImageAlbum"

        public void Process_IMAGE_ALBUM_LIST_96(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.Sequence] != null)
                {
                    long futid = (long)_JobjFromResponse[JsonKeys.FutId];
                    Guid npUUid = (_JobjFromResponse[JsonKeys.NpUUID] != null) ? (Guid)_JobjFromResponse[JsonKeys.NpUUID] : Guid.Empty;
                    int ImageAlbumCount = (_JobjFromResponse[JsonKeys.AlbumCount] != null) ? (int)_JobjFromResponse[JsonKeys.AlbumCount] : 0;
                    ObservableCollection<AlbumModel> ViewCollection = null;
                    if (futid == DefaultSettings.LOGIN_TABLE_ID)
                    {
                        DefaultSettings.MY_IMAGE_ALBUMS_COUNT = ImageAlbumCount;
                        ViewCollection = RingIDViewModel.Instance.MyImageAlubms;

                        RingIDViewModel.Instance.ImageAlbumNpUUId = npUUid;

                    }
                    else if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendBasicInfoModel != null
                        && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendBasicInfoModel.ShortInfoModel.UserTableID == futid)
                    {
                        UCMiddlePanelSwitcher.View_UCFriendProfilePanel.ImageAlbumCount = ImageAlbumCount;
                        ViewCollection = UCMiddlePanelSwitcher.View_UCFriendProfilePanel.FriendImageAlubms;
                        UCMiddlePanelSwitcher.View_UCFriendProfilePanel.AlbumNpUUID = npUUid;

                        if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendPhotos != null)
                            UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendPhotos.BOTTOM_LOADING = StatusConstants.LOADING_TEXT_VISIBLE;
                    }

                    if (_JobjFromResponse[JsonKeys.ImageAlbumList] != null)
                    {
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.ImageAlbumList];
                        foreach (JObject obj in jarray)
                        {
                            Guid albmId = (Guid)obj[JsonKeys.ImageAlbumId];
                            AlbumModel albumModel = ViewCollection.Where(P => P.AlbumId == albmId).FirstOrDefault();
                            if (albumModel == null)
                            {
                                albumModel = new AlbumModel(obj);
                                ViewCollection.InvokeInsert(ViewCollection.Count - 1, albumModel);
                            }
                            else
                                albumModel.LoadData(obj);
                            albumModel.UserTableID = futid;
                        }
                    }
                    if (futid == DefaultSettings.LOGIN_TABLE_ID)
                    {
                        if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos != null)
                            UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.ShowHideShowMoreLoading(ImageAlbumCount > (ViewCollection.Count - 1));
                        Application.Current.Dispatcher.BeginInvoke(() =>
                        {
                            if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile.MyProfilePhotosChange.IsVisible)
                            {
                                UCMiddlePanelSwitcher.View_UCMyProfile.MyProfilePhotosChange.ShowHideShowMoreLoading(ImageAlbumCount > (ViewCollection.Count - 1));
                            }
                            if (RingIDViewModel.Instance != null && RingIDViewModel.Instance.MyAlbums != null)
                            {
                                RingIDViewModel.Instance.MyAlbums.ShowHideShowMoreLoading(ImageAlbumCount > (ViewCollection.Count - 1));
                            }
                        });
                    }
                    else if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendBasicInfoModel != null
                       && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendBasicInfoModel.ShortInfoModel.UserTableID == futid && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendPhotos != null)
                    {
                        UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendPhotos.ShowHideShowMoreLoading(ImageAlbumCount > (ViewCollection.Count - 1));
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessMediaAlbumList ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_MY_ALBUM_IMAGES_97(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.Sequence] != null)
                {
                    long friend_Own_CelebTableId = (long)_JobjFromResponse[JsonKeys.FutId];
                    string sequence = ((string)_JobjFromResponse[JsonKeys.Sequence]);
                    int seqTotal = Convert.ToInt32(sequence.Split(new Char[] { '/' })[1]);
                    Guid albumId = (Guid)_JobjFromResponse[JsonKeys.AlbumId];

                    Dictionary<long, Dictionary<Guid, Dictionary<string, Dictionary<Guid, ImageModel>>>> TEMP_DICTIONARY = null;
                    TEMP_DICTIONARY = ImageDataContainer.Instance.TEMP_IMAGES;

                    if (_JobjFromResponse[JsonKeys.ImageList] != null && albumId != Guid.Empty)
                    {
                        JArray imageList = (JArray)_JobjFromResponse[JsonKeys.ImageList];

                        foreach (JObject singleImage in imageList)
                        {
                            //ImageDTO imageDTO = HelperMethodsModel.BindImageDetails(singleImage);
                            Guid imgId = singleImage[JsonKeys.ImageId] != null ? (Guid)singleImage[JsonKeys.ImageId] : Guid.Empty;
                            ImageModel model = ImageDataContainer.Instance.GetFromImageModelDictionary(imgId);
                            if (model == null)
                            {
                                model = new ImageModel(singleImage);
                                ImageDataContainer.Instance.AddOrReplaceImageModels(imgId, model);
                            }
                            else
                            {
                                model.LoadData(singleImage);
                            }

                            //handling missing uid and fullname
                            model.FriendUserTableID = friend_Own_CelebTableId;
                            if (model.UserShortInfoModel == null) model.UserShortInfoModel = RingIDViewModel.Instance.GetUserShortInfoModelNotNullable(model.FriendUserTableID, 0);
                            model.FullName = model.UserShortInfoModel.FullName;
                            //model.TotalImagesInAlbum = (int)_JobjFromResponse[JsonKeys.TotalImagesInAlbum];                            
                            model.AlbumId = albumId;

                            lock (TEMP_DICTIONARY)
                            {
                                if (TEMP_DICTIONARY.ContainsKey(friend_Own_CelebTableId))
                                {
                                    if (TEMP_DICTIONARY[friend_Own_CelebTableId].ContainsKey(albumId))
                                    {
                                        if (TEMP_DICTIONARY[friend_Own_CelebTableId][albumId].ContainsKey(sequence))
                                        {
                                            if (TEMP_DICTIONARY[friend_Own_CelebTableId][albumId][sequence].ContainsKey(model.ImageId))
                                            {
                                                TEMP_DICTIONARY[friend_Own_CelebTableId][albumId][sequence][model.ImageId] = model;
                                            }
                                            else
                                            {
                                                TEMP_DICTIONARY[friend_Own_CelebTableId][albumId][sequence].Add(model.ImageId, model);
                                            }
                                        }
                                        else
                                        {
                                            TEMP_DICTIONARY[friend_Own_CelebTableId][albumId].Add(sequence, new Dictionary<Guid, ImageModel>());
                                            TEMP_DICTIONARY[friend_Own_CelebTableId][albumId][sequence].Add(model.ImageId, model);
                                        }
                                    }
                                    else
                                    {
                                        TEMP_DICTIONARY[friend_Own_CelebTableId].Add(albumId, new Dictionary<string, Dictionary<Guid, ImageModel>>());
                                        TEMP_DICTIONARY[friend_Own_CelebTableId][albumId].Add(sequence, new Dictionary<Guid, ImageModel>());
                                        TEMP_DICTIONARY[friend_Own_CelebTableId][albumId][sequence].Add(model.ImageId, model);
                                    }
                                }
                                else
                                {
                                    TEMP_DICTIONARY.Add(friend_Own_CelebTableId, new Dictionary<Guid, Dictionary<string, Dictionary<Guid, ImageModel>>>());
                                    TEMP_DICTIONARY[friend_Own_CelebTableId].Add(albumId, new Dictionary<string, Dictionary<Guid, ImageModel>>());
                                    TEMP_DICTIONARY[friend_Own_CelebTableId][albumId].Add(sequence, new Dictionary<Guid, ImageModel>());
                                    TEMP_DICTIONARY[friend_Own_CelebTableId][albumId][sequence].Add(model.ImageId, model);
                                }
                            }
                        }
                    }

                    if (TEMP_DICTIONARY[friend_Own_CelebTableId][albumId].Count == seqTotal)
                    {
                        int albumType = HelperMethodsModel.GetImageTypeFromAlbum(albumId.ToString());
                        List<ImageModel> list = new List<ImageModel>();
                        foreach (string key in TEMP_DICTIONARY[friend_Own_CelebTableId][albumId].Keys)
                        {
                            list.AddRange(TEMP_DICTIONARY[friend_Own_CelebTableId][albumId][key].Values.ToList());
                        }

                        List<ImageModel> sortedList = list;//(from l in list orderby l.ImageId descending select l).ToList();
                        ObservableCollection<ImageModel> ViewCollection = null;
                        if (UCImageContentView.Instance != null && UCImageContentView.Instance.albumModel.AlbumId == albumId)
                        {
                            ViewCollection = UCImageContentView.Instance.albumModel.ImageList;
                            int pvc = UCImageContentView.Instance.albumModel.AlbumPrivacy;
                            foreach (var model in sortedList)
                            {
                                model.Privacy = pvc;
                                if (!ViewCollection.Any(P => P.ImageId == model.ImageId))
                                {
                                    ViewCollection.InvokeInsert(ViewCollection.Count - 1, model);
                                    Thread.Sleep(5);
                                }
                            }
                            UCImageContentView.Instance.ShowHideShowMoreLoading
                                    (UCImageContentView.Instance.albumModel.TotalImagesInAlbum > (ViewCollection.Count - 1));
                            //AddMyImagesFromServer(albumType, (int)_JobjFromResponse[JsonKeys.TotalImagesInAlbum], list);
                        }
                        if (UCImageContentViewForProfileCoverPhotoChange.Instance != null && UCImageContentViewForProfileCoverPhotoChange.Instance.albumModel.AlbumId == albumId)
                        {
                            ViewCollection = UCImageContentViewForProfileCoverPhotoChange.Instance.albumModel.ImageList;
                            int pvc = UCImageContentViewForProfileCoverPhotoChange.Instance.albumModel.AlbumPrivacy;
                            foreach (var model in sortedList)
                            {
                                model.Privacy = pvc;
                                if (!ViewCollection.Any(P => P.ImageId == model.ImageId))
                                {
                                    ViewCollection.InvokeInsert(ViewCollection.Count - 1, model);
                                    Thread.Sleep(5);
                                }
                            }
                            Application.Current.Dispatcher.BeginInvoke(() =>
                            {
                                if (UCImageContentViewForProfileCoverPhotoChange.Instance.albumModel.TotalImagesInAlbum > (ViewCollection.Count - 1))
                                {
                                    UCImageContentViewForProfileCoverPhotoChange.Instance.ShowMore();
                                }
                                else
                                {
                                    UCImageContentViewForProfileCoverPhotoChange.Instance.HideShowMoreLoading();
                                }
                            });

                            //AddMyImagesFromServer(albumType, (int)_JobjFromResponse[JsonKeys.TotalImagesInAlbum], list);
                        }

                        if (UCImageContentViewForFeedAlbum.Instance != null && UCImageContentViewForFeedAlbum.Instance.albumModel.AlbumId == albumId)
                        {
                            ViewCollection = UCImageContentViewForFeedAlbum.Instance.albumModel.ImageList;
                            int pvc = UCImageContentViewForFeedAlbum.Instance.albumModel.AlbumPrivacy;
                            foreach (var model in sortedList)
                            {
                                model.Privacy = pvc;
                                if (!ViewCollection.Any(P => P.ImageId == model.ImageId))
                                {
                                    ViewCollection.InvokeInsert(ViewCollection.Count - 1, model);
                                    Thread.Sleep(5);
                                }
                            }
                            UCImageContentViewForFeedAlbum.Instance.ShowHideShowMoreLoading
                                    (UCImageContentViewForFeedAlbum.Instance.albumModel.TotalImagesInAlbum > (ViewCollection.Count - 1));
                        }

                        //else
                        {
                            if (friend_Own_CelebTableId == DefaultSettings.LOGIN_TABLE_ID && UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile.PreviewPhotoID == Guid.Empty)
                            {
                                UCMiddlePanelSwitcher.View_UCMyProfile.PreviewPhotoID = sortedList[0].ImageId;
                            }
                            else if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel.PreviewPhotoID == Guid.Empty)
                            {
                                UCMiddlePanelSwitcher.View_UCFriendProfilePanel.PreviewPhotoID = sortedList[0].ImageId;
                            }
                        }
                        TEMP_DICTIONARY.Clear();
                    }
                }

                else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false)
                {
                    string albumId = (string)_JobjFromResponse[JsonKeys.AlbumId];
                    if (_JobjFromResponse[JsonKeys.FutId] == null || (long)_JobjFromResponse[JsonKeys.FutId] == 0)
                    {
                        NoImageFoundInMyAlbum(albumId);
                    }
                    else if (_JobjFromResponse[JsonKeys.FutId] != null && !String.IsNullOrEmpty(albumId))
                    {
                        long utID = (long)_JobjFromResponse[JsonKeys.FutId];
                        NoImageFoundInFriendsAlbum(utID, albumId);
                    }
                    else
                    {
                        long utID = (long)_JobjFromResponse[JsonKeys.FutId];
                        NoImageFoundInCelibrityAlbum(utID);
                    }
                }

            }
            catch (Exception e)
            {
                log.Error("Exception in ProcessMyAlbumImages ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_MORE_FEED_IMAGES_139(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
                {
                    Guid newsFeedId = (Guid)_JobjFromResponse[JsonKeys.NewsfeedId];
                    FeedModel fm = null;
                    if (!FeedDataContainer.Instance.FeedModels.TryGetValue(newsFeedId, out fm))
                    {
                        fm = RingIDViewModel.Instance.BreakingNewsPortalFeeds.Where(p => p.NewsfeedId == newsFeedId).FirstOrDefault();
                        if (fm == null)
                        {
                            fm = RingIDViewModel.Instance.BreakingNewsPortalProfileFeeds.Where(p => p.NewsfeedId == newsFeedId).FirstOrDefault();
                        }
                    }
                    if (_JobjFromResponse[JsonKeys.ImageList] != null)
                    {
                        JArray imageList = (JArray)_JobjFromResponse[JsonKeys.ImageList];
                        foreach (JObject singleImage in imageList)
                        {
                            //ImageDTO imageDTO = HelperMethodsModel.BindImageDetails(singleImage);
                            Guid imgId = singleImage[JsonKeys.ImageId] != null ? (Guid)singleImage[JsonKeys.ImageId] : Guid.Empty;
                            ImageModel model = ImageDataContainer.Instance.GetFromImageModelDictionary(imgId);
                            if (model == null)
                            {
                                model = new ImageModel(singleImage);
                                ImageDataContainer.Instance.AddOrReplaceImageModels(imgId, model);
                            }
                            else
                            {
                                model.LoadData(singleImage);
                            }
                            model.NewsFeedId = newsFeedId;
                            //handling missing uid and fullname
                            model.FriendUserTableID = model.UserTableID;
                            if (model.UserShortInfoModel == null) model.UserShortInfoModel = RingIDViewModel.Instance.GetUserShortInfoModelNotNullable(model.FriendUserTableID, 0);
                            model.FullName = model.UserShortInfoModel.FullName;
                            if (fm != null && fm.ImageList != null)
                            {
                                fm.ImageList.InvokeAddNoDuplicate(model);
                                model.FeedImageInfoModel = new FeedImageInfo();
                                model.FeedImageInfoModel.Width = model.ImageWidth;
                                model.FeedImageInfoModel.Height = model.ImageHeight;
                                fm.FeedImageList.InvokeAddNoDuplicate(model);
                                //if (!fm.ImageList.Any(p => p.ImageId == model.ImageId))
                                //{
                                //    fm.ImageList.Add(model);
                                //}
                            }
                        }
                        //fm.OnPropertyChanged("CurrentInstance");
                        if (fm != null && fm.ImageList.Count > 1 && fm.TotalImage == fm.ImageList.Count)
                        {
                            //NewsFeedViewModel.Instance.IsFirstTime = true;
                            NewsFeedViewModel.Instance.OnNewsPortalSliderLoaded(fm);
                        }
                    }
                    //Application.Current.Dispatcher.BeginInvoke(() =>
                    //{
                    //    if (basicImageViewWrapper().ucBasicImageView == null)
                    //    {
                    //        basicImageViewWrapper().AddUCBasicImageViewToWrapperPanel();
                    //    }
                    //    basicImageViewWrapper().ucBasicImageView.UIControlEnable();
                    //});
                }
            }
            catch (Exception e)
            {
                log.Error("Exception in Process_MORE_FEED_IMAGES_139 ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }
        //public void Process_MY_ALBUM_IMAGES_97(JObject _JobjFromResponse)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.Sequence] != null)
        //        {
        //            string sequence = ((string)_JobjFromResponse[JsonKeys.Sequence]);
        //            int seqTotal = Convert.ToInt32(sequence.Split(new Char[] { '/' })[1]);
        //            string albumId = (string)_JobjFromResponse[JsonKeys.AlbumId];
        //            if (_JobjFromResponse[JsonKeys.FutId] == null || (long)_JobjFromResponse[JsonKeys.FutId] == 0)
        //            {
        //                Dictionary<string, Dictionary<Guid, ImageModel>> TEMP_DICTIONARY = null;

        //                if (albumId.Equals(DefaultSettings.PROFILE_IMAGE_ALBUM_ID, StringComparison.Ordinal))
        //                {
        //                    TEMP_DICTIONARY = ImageDataContainer.Instance.TEMP_MY_PROFILE_IMAGES;
        //                }
        //                else if (albumId.Equals(DefaultSettings.COVER_IMAGE_ALBUM_ID, StringComparison.Ordinal))
        //                {
        //                    TEMP_DICTIONARY = ImageDataContainer.Instance.TEMP_MY_COVER_IMAGES;
        //                }
        //                else if (albumId.Equals(DefaultSettings.FEED_IMAGE_ALBUM_ID, StringComparison.Ordinal))
        //                {
        //                    TEMP_DICTIONARY = ImageDataContainer.Instance.TEMP_MY_FEED_IMAGES;
        //                }


        //                if (_JobjFromResponse[JsonKeys.ImageList] != null && !string.IsNullOrEmpty(albumId))
        //                {
        //                    JArray imageList = (JArray)_JobjFromResponse[JsonKeys.ImageList];

        //                    foreach (JObject singleImage in imageList)
        //                    {
        //                        //ImageDTO imageDTO = HelperMethodsModel.BindImageDetails(singleImage);
        //                        Guid imgId = singleImage[JsonKeys.ImageId] != null ? (Guid)singleImage[JsonKeys.ImageId] : Guid.Empty;
        //                        ImageModel model = ImageDataContainer.Instance.GetFromImageModelDictionary(imgId);
        //                        if (model == null)
        //                        {
        //                            model = new ImageModel(singleImage);
        //                            ImageDataContainer.Instance.AddOrReplaceImageModels(imgId, model);
        //                        }
        //                        else
        //                        {
        //                            model.LoadData(singleImage);
        //                        }

        //                        //handling missing uid and fullname
        //                        model.UserTableID = DefaultSettings.userProfile.UserTableID;
        //                        model.FullName = DefaultSettings.userProfile.FullName;

        //                        if (model.UserShortInfoModel == null) model.UserShortInfoModel = RingIDViewModel.Instance.GetUserShortInfoModelNotNullable(model.UserTableID, 0, model.FullName);

        //                        //model.TotalImagesInAlbum = (int)_JobjFromResponse[JsonKeys.TotalImagesInAlbum];
        //                        model.AlbumId = albumId;
        //                        lock (TEMP_DICTIONARY)
        //                        {
        //                            if (TEMP_DICTIONARY.ContainsKey(sequence))
        //                            {
        //                                if (TEMP_DICTIONARY[sequence].ContainsKey(model.ImageId))
        //                                {
        //                                    TEMP_DICTIONARY[sequence][model.ImageId] = model;
        //                                }
        //                                else
        //                                {
        //                                    TEMP_DICTIONARY[sequence].Add(model.ImageId, model);
        //                                }
        //                            }
        //                            else
        //                            {
        //                                TEMP_DICTIONARY.Add(sequence, new Dictionary<Guid, ImageModel>());
        //                                TEMP_DICTIONARY[sequence].Add(model.ImageId, model);
        //                            }
        //                        }

        //                        #region "Commented Code"
        //                        /*lock (NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES)
        //                        {
        //                            if (NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES.ContainsKey(albumId))
        //                            {
        //                                if (NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[albumId].ContainsKey(imageDTO.ImageId))
        //                                {
        //                                    NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[albumId][imageDTO.ImageId] = imageDTO;
        //                                }
        //                                else
        //                                {
        //                                    NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[albumId].Add(imageDTO.ImageId, imageDTO);
        //                                }
        //                            }
        //                            else
        //                            {
        //                                NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES.Add(albumId, new Dictionary<long, ImageDTO>());
        //                                NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[albumId].Add(imageDTO.ImageId, imageDTO);
        //                            }
        //                        }*/
        //                        #endregion
        //                    }
        //                }

        //                if (TEMP_DICTIONARY.Count == seqTotal)
        //                {
        //                    int albumType = HelperMethodsModel.GetImageTypeFromAlbum(albumId);
        //                    List<ImageModel> list = new List<ImageModel>();
        //                    foreach (string key in TEMP_DICTIONARY.Keys)
        //                    {
        //                        list.AddRange(TEMP_DICTIONARY[key].Values.ToList());
        //                    }

        //                    AddMyImagesFromServer(albumType, (int)_JobjFromResponse[JsonKeys.TotalImagesInAlbum], list);
        //                    TEMP_DICTIONARY.Clear();

        //                }
        //            }
        //            else if (_JobjFromResponse[JsonKeys.FutId] != null && !String.IsNullOrEmpty(albumId))
        //            {
        //                Process_FRIEND_ALBUM_IMAGES_109(_JobjFromResponse);
        //            }
        //            else
        //            {
        //                Process_CELEBRITY_ALBUM_IMAGES_97(_JobjFromResponse);
        //            }
        //        }

        //        else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false)
        //        {
        //            string albumId = (string)_JobjFromResponse[JsonKeys.AlbumId];
        //            if (_JobjFromResponse[JsonKeys.FutId] == null || (long)_JobjFromResponse[JsonKeys.FutId] == 0)
        //            {
        //                NoImageFoundInMyAlbum(albumId);
        //            }
        //            else if (_JobjFromResponse[JsonKeys.FutId] != null && !String.IsNullOrEmpty(albumId))
        //            {
        //                long utID = (long)_JobjFromResponse[JsonKeys.FutId];
        //                NoImageFoundInFriendsAlbum(utID, albumId);
        //            }
        //            else
        //            {
        //                long utID = (long)_JobjFromResponse[JsonKeys.FutId];
        //                NoImageFoundInCelibrityAlbum(utID);
        //            }
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("Exception in ProcessMyAlbumImages ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }
        //}

        //public void Process_FRIEND_ALBUM_IMAGES_109(JObject _JobjFromResponse) // 109
        //{
        //    try
        //    {
        //        if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.Sequence] != null)
        //        {
        //            long friendTableId = (long)_JobjFromResponse[JsonKeys.FutId];
        //            string sequence = (string)_JobjFromResponse[JsonKeys.Sequence];
        //            int seqTotal = Convert.ToInt32(sequence.Split(new char[] { '/' })[1]);
        //            string albumId = (string)_JobjFromResponse[JsonKeys.AlbumId];

        //            Dictionary<long, Dictionary<string, Dictionary<Guid, ImageModel>>> TEMP_DICTIONARY = null;
        //            if (albumId.Equals(DefaultSettings.PROFILE_IMAGE_ALBUM_ID, StringComparison.Ordinal))
        //            {
        //                TEMP_DICTIONARY = ImageDataContainer.Instance.TEMP_FRIEND_PROFILE_IMAGES;
        //            }
        //            else if (albumId.Equals(DefaultSettings.COVER_IMAGE_ALBUM_ID, StringComparison.Ordinal))
        //            {
        //                TEMP_DICTIONARY = ImageDataContainer.Instance.TEMP_FRIEND_COVER_IMAGES;
        //            }
        //            else if (albumId.Equals(DefaultSettings.FEED_IMAGE_ALBUM_ID, StringComparison.Ordinal))
        //            {
        //                TEMP_DICTIONARY = ImageDataContainer.Instance.TEMP_FRIEND_FEED_IMAGES;
        //            }

        //            if (_JobjFromResponse[JsonKeys.ImageList] != null && !string.IsNullOrEmpty(albumId))
        //            {
        //                JArray imageList = (JArray)_JobjFromResponse[JsonKeys.ImageList];

        //                foreach (JObject singleImage in imageList)
        //                {
        //                    //ImageDTO imageDTO = HelperMethodsModel.BindImageDetails(singleImage);

        //                    Guid imgId = singleImage[JsonKeys.ImageId] != null ? (Guid)singleImage[JsonKeys.ImageId] : Guid.Empty;
        //                    ImageModel model = ImageDataContainer.Instance.GetFromImageModelDictionary(imgId);
        //                    if (model == null)
        //                    {
        //                        model = new ImageModel(singleImage);
        //                        ImageDataContainer.Instance.AddOrReplaceImageModels(imgId, model);
        //                    }
        //                    else
        //                    {
        //                        model.LoadData(singleImage);
        //                    }
        //                    model.FriendUserTableID = friendTableId;
        //                    if (model.UserShortInfoModel == null) model.UserShortInfoModel = RingIDViewModel.Instance.GetUserShortInfoModelNotNullable(model.FriendUserTableID, 0);

        //                    model.FullName = model.UserShortInfoModel.FullName;
        //                    //model.TotalImagesInAlbum = (int)_JobjFromResponse[JsonKeys.TotalImagesInAlbum];

        //                    lock (TEMP_DICTIONARY)
        //                    {
        //                        if (TEMP_DICTIONARY.ContainsKey(friendTableId))
        //                        {
        //                            if (TEMP_DICTIONARY[friendTableId].ContainsKey(sequence))
        //                            {
        //                                if (TEMP_DICTIONARY[friendTableId][sequence].ContainsKey(model.ImageId))
        //                                {
        //                                    TEMP_DICTIONARY[friendTableId][sequence][model.ImageId] = model;
        //                                }
        //                                else
        //                                {
        //                                    TEMP_DICTIONARY[friendTableId][sequence].Add(model.ImageId, model);
        //                                }
        //                            }
        //                            else
        //                            {
        //                                TEMP_DICTIONARY[friendTableId].Add(sequence, new Dictionary<Guid, ImageModel>());
        //                                TEMP_DICTIONARY[friendTableId][sequence].Add(model.ImageId, model);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            TEMP_DICTIONARY.Add(friendTableId, new Dictionary<string, Dictionary<Guid, ImageModel>>());
        //                            TEMP_DICTIONARY[friendTableId].Add(sequence, new Dictionary<Guid, ImageModel>());
        //                            TEMP_DICTIONARY[friendTableId][sequence].Add(model.ImageId, model);
        //                        }
        //                    }
        //                    #region "Commented Code"
        //                    /*lock (NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES)
        //                    {
        //                        if (NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES.ContainsKey(friendTableId))
        //                        {
        //                            if (NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendTableId].ContainsKey(albumId))
        //                            {
        //                                if (NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendTableId][albumId].ContainsKey(imageDTO.ImageId))
        //                                    NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendTableId][albumId][imageDTO.ImageId] = imageDTO;
        //                                else
        //                                    NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendTableId][albumId].Add(imageDTO.ImageId, imageDTO);
        //                            }
        //                            else
        //                            {
        //                                NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendTableId].Add(albumId, new Dictionary<long, ImageDTO>());
        //                                NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendTableId][albumId].Add(imageDTO.ImageId, imageDTO);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES.Add(friendTableId, new Dictionary<string, Dictionary<long, ImageDTO>>());
        //                            NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendTableId].Add(albumId, new Dictionary<long, ImageDTO>());
        //                            NewsFeedDictionaries.Instance.FRIEND_ALBUM_IMAGES[friendTableId][albumId].Add(imageDTO.ImageId, imageDTO);
        //                        }
        //                    }*/
        //                    #endregion
        //                }
        //            }

        //            if (TEMP_DICTIONARY[friendTableId].Count == seqTotal)
        //            {
        //                int albumType = Models.Utility.HelperMethodsModel.GetImageTypeFromAlbum(albumId);
        //                List<ImageModel> list = new List<ImageModel>();
        //                foreach (string key in TEMP_DICTIONARY[friendTableId].Keys)
        //                {
        //                    list.AddRange(TEMP_DICTIONARY[friendTableId][key].Values.ToList());
        //                }

        //                AddFriendImagesFromServer(friendTableId, albumType, (int)_JobjFromResponse[JsonKeys.TotalImagesInAlbum], list);
        //                TEMP_DICTIONARY.Clear();
        //            }
        //        }
        //        else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false)
        //        {
        //            long utID = (long)_JobjFromResponse[JsonKeys.FutId];
        //            string albumId = (string)_JobjFromResponse[JsonKeys.AlbumId];
        //            NoImageFoundInFriendsAlbum(utID, albumId);
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //        log.Error("Exception in ProcessFriendAlbumImages ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

        //    }
        //}

        //public void Process_CELEBRITY_ALBUM_IMAGES_97(JObject _JobjFromResponse) // 97
        //{
        //    try
        //    {
        //        if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success] && _JobjFromResponse[JsonKeys.Sequence] != null)
        //        {
        //            long friendTableId = (long)_JobjFromResponse[JsonKeys.FutId];
        //            string sequence = (string)_JobjFromResponse[JsonKeys.Sequence];
        //            int seqTotal = Convert.ToInt32(sequence.Split(new char[] { '/' })[1]);
        //            string albumId = DefaultSettings.FEED_IMAGE_ALBUM_ID;

        //            Dictionary<long, Dictionary<string, Dictionary<Guid, ImageModel>>> TEMP_DICTIONARY = null;
        //            TEMP_DICTIONARY = ImageDataContainer.Instance.TEMP_CELEBRITY_IMAGES;

        //            if (_JobjFromResponse[JsonKeys.ImageList] != null)
        //            {
        //                JArray imageList = (JArray)_JobjFromResponse[JsonKeys.ImageList];

        //                foreach (JObject singleImage in imageList)
        //                {
        //                    //ImageDTO imageDTO = HelperMethodsModel.BindImageDetails(singleImage);
        //                    Guid imgId = singleImage[JsonKeys.ImageId] != null ? (Guid)singleImage[JsonKeys.ImageId] : Guid.Empty;
        //                    ImageModel model = ImageDataContainer.Instance.GetFromImageModelDictionary(imgId);
        //                    if (model == null)
        //                    {
        //                        model = new ImageModel(singleImage);
        //                        ImageDataContainer.Instance.AddOrReplaceImageModels(imgId, model);
        //                    }
        //                    else
        //                    {
        //                        model.LoadData(singleImage);
        //                    }

        //                    model.FriendUserTableID = friendTableId;
        //                    if (model.UserShortInfoModel == null) model.UserShortInfoModel = RingIDViewModel.Instance.GetUserShortInfoModelNotNullable(model.FriendUserTableID, 0);
        //                    model.FullName = model.UserShortInfoModel.FullName;
        //                    /*UserBasicInfoDTO tmp = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(friendTableId);
        //                    if (tmp != null)
        //                    {
        //                        model.FullName = tmp.FullName;
        //                    }*/

        //                    //model.TotalImagesInAlbum = (int)_JobjFromResponse[JsonKeys.TotalImagesInAlbum];

        //                    lock (TEMP_DICTIONARY)
        //                    {
        //                        if (TEMP_DICTIONARY.ContainsKey(friendTableId))
        //                        {
        //                            if (TEMP_DICTIONARY[friendTableId].ContainsKey(sequence))
        //                            {
        //                                if (TEMP_DICTIONARY[friendTableId][sequence].ContainsKey(model.ImageId))
        //                                {
        //                                    TEMP_DICTIONARY[friendTableId][sequence][model.ImageId] = model;
        //                                }
        //                                else
        //                                {
        //                                    TEMP_DICTIONARY[friendTableId][sequence].Add(model.ImageId, model);
        //                                }
        //                            }
        //                            else
        //                            {
        //                                TEMP_DICTIONARY[friendTableId].Add(sequence, new Dictionary<Guid, ImageModel>());
        //                                TEMP_DICTIONARY[friendTableId][sequence].Add(model.ImageId, model);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            TEMP_DICTIONARY.Add(friendTableId, new Dictionary<string, Dictionary<Guid, ImageModel>>());
        //                            TEMP_DICTIONARY[friendTableId].Add(sequence, new Dictionary<Guid, ImageModel>());
        //                            TEMP_DICTIONARY[friendTableId][sequence].Add(model.ImageId, model);
        //                        }
        //                    }
        //                    #region "Commented Code"
        //                    /*lock (NewsFeedDictionaries.Instance.CELEBRITY_ALBUM_IMAGES)
        //                    {
        //                        if (NewsFeedDictionaries.Instance.CELEBRITY_ALBUM_IMAGES.ContainsKey(friendTableId))
        //                        {

        //                            if (NewsFeedDictionaries.Instance.CELEBRITY_ALBUM_IMAGES[friendTableId][albumId].ContainsKey(imageDTO.ImageId))
        //                                NewsFeedDictionaries.Instance.CELEBRITY_ALBUM_IMAGES[friendTableId][albumId][imageDTO.ImageId] = imageDTO;
        //                            else
        //                                NewsFeedDictionaries.Instance.CELEBRITY_ALBUM_IMAGES[friendTableId][albumId].Add(imageDTO.ImageId, imageDTO);
        //                        }
        //                        else
        //                        {
        //                            NewsFeedDictionaries.Instance.CELEBRITY_ALBUM_IMAGES.Add(friendTableId, new Dictionary<string, Dictionary<long, ImageDTO>>());
        //                            NewsFeedDictionaries.Instance.CELEBRITY_ALBUM_IMAGES[friendTableId].Add(albumId, new Dictionary<long, ImageDTO>());
        //                            NewsFeedDictionaries.Instance.CELEBRITY_ALBUM_IMAGES[friendTableId][albumId].Add(imageDTO.ImageId, imageDTO);
        //                        }
        //                    }*/
        //                    #endregion
        //                }
        //            }

        //            if (TEMP_DICTIONARY[friendTableId].Count == seqTotal)
        //            {
        //                int albumType = Models.Utility.HelperMethodsModel.GetImageTypeFromAlbum(albumId);
        //                List<ImageModel> list = new List<ImageModel>();
        //                foreach (string key in TEMP_DICTIONARY[friendTableId].Keys)
        //                {
        //                    list.AddRange(TEMP_DICTIONARY[friendTableId][key].Values.ToList());
        //                }

        //                AddCelebrityImagesFromServer(friendTableId, albumType, (int)_JobjFromResponse[JsonKeys.TotalImagesInAlbum], list);
        //                TEMP_DICTIONARY.Clear();
        //            }
        //        }
        //        else if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == false)
        //        {
        //            //long uid = (long)_JobjFromResponse[JsonKeys.FutId];
        //            //NoImageFoundInCelebrityAlbum(uid);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("Exception in Process_CELEBRITY_ALBUM_IMAGES_97 ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }
        //}
        #endregion

        #region "Utility Methods"
        //private UCBasicImageViewWrapper basicImageViewWrapper()
        //{
        //    return MainSwitcher.PopupController.BasicImageViewWrapper;
        //}

        //public void AddFriendImagesFromServer(long userTableID, int friendImageType, int friendImageCount, List<ImageModel> friendImageList)
        //{
        //    try
        //    {
        //        UCFriendProfile ucFriendProfile = null;
        //        if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == userTableID)
        //        {
        //            ucFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
        //            List<ImageModel> sortedList = (from l in friendImageList orderby l.Time descending select l).ToList();

        //            #region "Image full view"
        //            //if (basicImageViewWrapper().ucBasicImageView != null)
        //            //    basicImageViewWrapper().ucBasicImageView.AddIntoImageList(friendImageType, userTableID, friendImageCount, sortedList);
        //            #endregion "Image full view"

        //            if (friendImageType == SettingsConstants.TYPE_PROFILE_IMAGE)
        //            {
        //                if (ucFriendProfile._UCFriendPhotos != null)
        //                {
        //                    ucFriendProfile._UCFriendPhotos.FriendProfileImageCount = friendImageCount;
        //                }
        //                ucFriendProfile.LoadFriendProfileImage(sortedList, friendImageCount);

        //            }
        //            else if (friendImageType == SettingsConstants.TYPE_COVER_IMAGE)
        //            {
        //                if (ucFriendProfile._UCFriendPhotos != null)
        //                {
        //                    ucFriendProfile._UCFriendPhotos.FriendCoverImageCount = friendImageCount;
        //                }
        //                ucFriendProfile.LoadFriendCoverImage(sortedList, friendImageCount);

        //            }
        //            else if (friendImageType == SettingsConstants.TYPE_NORMAL_BOOK_IMAGE)
        //            {
        //                if (ucFriendProfile._UCFriendPhotos != null)
        //                {
        //                    ucFriendProfile._UCFriendPhotos.FriendFeedImageCount = friendImageCount;
        //                }
        //                ucFriendProfile.LoadFriendFeedImage(sortedList, friendImageCount);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: AddFriendImagesFromServer() ==> " + ex.Message + "\n" + ex.StackTrace);
        //    }
        //}

        public void AddCelebrityImagesFromServer(long userTableID, int albumType, int celebrityImageCount, List<ImageModel> celibirtyImageList)
        {
            try
            {
                UCCelebrityProfile ucCelebrityProfile = UCMiddlePanelSwitcher.View_CelebrityProfile;
                if (ucCelebrityProfile != null && ucCelebrityProfile.CelebrityUserTableID == userTableID)
                {
                    List<ImageModel> sortedList = (from l in celibirtyImageList orderby l.Time descending select l).ToList();

                    #region "Image full view"
                    //if (basicImageViewWrapper().ucBasicImageView != null)
                    //    basicImageViewWrapper().ucBasicImageView.AddIntoImageList(albumType, userTableID, celebrityImageCount, sortedList);
                    #endregion "Image full view"

                    ucCelebrityProfile.FeedImageCount = celebrityImageCount;
                    ucCelebrityProfile.LoadCelebrityFeedImage(sortedList, celebrityImageCount);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: AddFriendImagesFromServer() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void NoImageFoundInCelibrityAlbum(long utid)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    UCCelebrityProfile ucCelebrityProfile = UCMiddlePanelSwitcher.View_CelebrityProfile;
                    if (ucCelebrityProfile != null && ucCelebrityProfile.CelebrityUserTableID == utid)
                    {
                        if (ucCelebrityProfile._UCCelebrityPhotos != null)
                            ucCelebrityProfile._UCCelebrityPhotos.ShowHideShowMoreLoading(ucCelebrityProfile.CelebFeedImageList.Count < ucCelebrityProfile.FeedImageCount);
                        else
                            ucCelebrityProfile.IsVisibleFeedPhotosLoader = Visibility.Collapsed;

                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: NoImageFoundInCelibrityAlbum() ==> " + ex.Message + "\n" + ex.StackTrace);
                }

            });
        }

        //public void GetSingleImageModelFromServer(ImageModel imageModel, Guid nfId)
        //{
        //    try
        //    {
        //        //if (basicImageViewWrapper().ucBasicImageView != null)
        //        //{
        //        //    basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage.ChangeAllValues(imageModel.ImageId);
        //        //}
        //        if (nfId != Guid.Empty)
        //        {
        //            FeedModel feedModel = GetFeedModelByNfid(nfId);
        //            if (feedModel != null && feedModel.SingleImageFeedModel != null && feedModel.SingleImageFeedModel.ImageId == imageModel.ImageId && feedModel.LikeCommentShare != null && imageModel.LikeCommentShare != null)
        //            {
        //                feedModel.SingleImageFeedModel = imageModel;
        //                feedModel.LikeCommentShare.ILike = imageModel.LikeCommentShare.ILike;
        //                feedModel.LikeCommentShare.NumberOfLikes = imageModel.LikeCommentShare.NumberOfLikes;
        //                feedModel.LikeCommentShare.IComment = imageModel.LikeCommentShare.IComment;
        //                feedModel.LikeCommentShare.NumberOfComments = imageModel.LikeCommentShare.NumberOfComments;
        //                //feedModel.OnPropertyChanged("CurrentInstance");
        //            }
        //            FeedModel model = new FeedModel();
        //            if (FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out model) && model.ImageList != null && model.ImageList.Count == 1 && feedModel.LikeCommentShare != null && imageModel.LikeCommentShare != null)
        //            {
        //                model.ImageList[0] = imageModel;
        //                model.LikeCommentShare.ILike = imageModel.LikeCommentShare.ILike;
        //                model.LikeCommentShare.NumberOfLikes = imageModel.LikeCommentShare.NumberOfLikes;
        //                model.LikeCommentShare.IComment = imageModel.LikeCommentShare.IComment;
        //                model.LikeCommentShare.NumberOfComments = imageModel.LikeCommentShare.NumberOfComments;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: GetSingleImageDTOFromServer() ==> " + ex.Message + "\n" + ex.StackTrace);
        //    }
        //}

        //public void GetSingleImageDTOFromServer(ImageDTO imageDTO, long nfId)
        //{
        //    try
        //    {
        //        if (basicImageViewWrapper().ucBasicImageView != null)
        //        {
        //            basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage.ChangeAllValues(imageDTO);
        //        }
        //        if (nfId > 0)
        //        {
        //            FeedModel feedModel = GetFeedModelByNfid(nfId);
        //            if (feedModel != null && feedModel.SingleImageFeedModel != null && feedModel.SingleImageFeedModel.ImageId == imageDTO.ImageId)
        //            {
        //                feedModel.SingleImageFeedModel.LoadData(imageDTO);
        //                feedModel.ILike = (short)imageDTO.iLike; feedModel.NumberOfLikes = imageDTO.NumberOfLikes;
        //                feedModel.IComment = (short)imageDTO.iComment; feedModel.NumberOfComments = imageDTO.NumberOfComments;
        //                feedModel.OnPropertyChanged("CurrentInstance");
        //            }
        //            FeedDTO dto = null;
        //            if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.TryGetValue(nfId, out dto) && dto.ImageList != null && dto.ImageList.Count == 1)
        //            {
        //                dto.ImageList[0] = imageDTO;
        //                dto.ILike = (short)imageDTO.iLike; dto.NumberOfLikes = imageDTO.NumberOfLikes;
        //                dto.IComment = (short)imageDTO.iComment; dto.NumberOfComments = imageDTO.NumberOfComments;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: GetSingleImageDTOFromServer() ==> " + ex.Message + "\n" + ex.StackTrace);
        //    }
        //}

        public void NoImageFoundInFriendsAlbum(long utId, string albumId)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    UCFriendProfile ucFriendProfile = null;
                    if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == utId)
                    {
                        ucFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                    }
                    if (ucFriendProfile != null && ucFriendProfile._UCFriendPhotos != null)
                    {
                        /*if (albumId == DefaultSettings.PROFILE_IMAGE_ALBUM_ID)
                        {
                            if (ucFriendProfile._UCFriendPhotos._UCFriendProfilePhotosAlbum != null)
                                ucFriendProfile._UCFriendPhotos._UCFriendProfilePhotosAlbum.ShowHideShowMoreLoading(ucFriendProfile.FriendProfileImageList.Count < ucFriendProfile._UCFriendPhotos.FriendProfileImageCount);
                            else
                                ucFriendProfile._UCFriendPhotos.IsVisibleProfilePhotosLoader = Visibility.Collapsed;
                        }
                        else if (albumId == DefaultSettings.COVER_IMAGE_ALBUM_ID)
                        {
                            if (ucFriendProfile._UCFriendPhotos._UCFriendCoverPhotosAlbum != null)
                                ucFriendProfile._UCFriendPhotos._UCFriendCoverPhotosAlbum.ShowHideShowMoreLoading(ucFriendProfile.FriendCoverImageList.Count < ucFriendProfile._UCFriendPhotos.FriendCoverImageCount);
                            else
                                ucFriendProfile._UCFriendPhotos.IsVisibleCoverPhotosLoader = Visibility.Collapsed;
                        }
                        else if (albumId == DefaultSettings.FEED_IMAGE_ALBUM_ID)
                        {
                            if (ucFriendProfile._UCFriendPhotos._UCFriendFeedPhotosAlbum != null)
                                ucFriendProfile._UCFriendPhotos._UCFriendFeedPhotosAlbum.ShowHideShowMoreLoading(ucFriendProfile.FriendFeedImageList.Count < ucFriendProfile._UCFriendPhotos.FriendFeedImageCount);
                            else
                                ucFriendProfile._UCFriendPhotos.IsVisibleFeedPhotosLoader = Visibility.Collapsed;
                        }*/
                        //to do
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: NoImageFoundInFriendsAlbum() ==> " + ex.Message + "\n" + ex.StackTrace);
                }

            });
        }

        private FeedModel GetFeedModelByNfid(Guid nfid)
        {
            FeedModel fm = null;
            FeedDataContainer.Instance.FeedModels.TryGetValue(nfid, out fm);
            return fm;
        }

        public void FetchImageCommentsFromServer(Guid imageId, List<CommentDTO> list, Guid nfid)
        {
            try
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    ObservableCollection<CommentModel> CommentsINSingleImageFeedModel = null;
                    ObservableCollection<CommentModel> CommentsINImageView = null;
                    FeedModel feedModel = null;
                    long postOwnerUtId = 0;
                    if (nfid != Guid.Empty)
                    {
                        feedModel = GetFeedModelByNfid(nfid);
                        if ((list == null || list.Count == 0) && feedModel != null)
                        {
                            feedModel.NoMoreComments = true;
                            //feedModel.OnPropertyChanged("CurrentInstance");
                            FetchImageCommentsUI();
                            //if (basicImageViewWrapper().ucBasicImageView != null && basicImageViewWrapper().ucBasicImageView.clickedImageID == imageId && basicImageViewWrapper().ucBasicImageView.ImageComments != null)
                            //{
                            //    basicImageViewWrapper().ucBasicImageView.ImageComments.PreviousCommentsVisibility = Visibility.Collapsed;
                            //}
                            return;
                        }
                        if (feedModel != null)
                        {
                            if (feedModel.SingleImageFeedModel != null)
                                CommentsINSingleImageFeedModel = feedModel.CommentList;
                            if (feedModel.PostOwner.UserTableID > 0)
                                postOwnerUtId = feedModel.PostOwner.UserTableID;
                        }
                    }
                    //if (basicImageViewWrapper().ucBasicImageView != null && basicImageViewWrapper().ucBasicImageView.clickedImageID == imageId)
                    //{
                    //    if (basicImageViewWrapper().ucBasicImageView.ImageComments != null)
                    //    {
                    //        basicImageViewWrapper().ucBasicImageView.ImageComments.ShowWatch(false);
                    //        CommentsINImageView = basicImageViewWrapper().ucBasicImageView.ImageComments.ImageCommentList;
                    //        basicImageViewWrapper().ucBasicImageView.ImageComments.PreviousCommentsVisibility = (basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage.NumberOfComments > CommentsINImageView.Count) ? Visibility.Visible : Visibility.Collapsed;
                    //    }
                    //    if (basicImageViewWrapper().ucBasicImageView.clickedUserTableID > 0)
                    //        postOwnerUtId = basicImageViewWrapper().ucBasicImageView.clickedUserTableID;
                    //}
                    if ((CommentsINImageView != null || CommentsINSingleImageFeedModel != null) && list != null)
                    {
                        //foreach (var comment in list)
                        //{
                        //    CommentModel commentFeed = null, commentImgView = null, commentmodel = null;
                        //    if (CommentsINSingleImageFeedModel != null) commentFeed = CommentsINSingleImageFeedModel.Where(p => p.CommentId == comment.CommentId).FirstOrDefault();
                        //    if (CommentsINImageView != null) commentImgView = CommentsINImageView.Where(p => p.CommentId == comment.CommentId).FirstOrDefault();
                        //    if (commentFeed == null && commentImgView == null)
                        //    {
                        //        commentmodel = new CommentModel();
                        //        GC.SuppressFinalize(commentmodel);
                        //        if (CommentsINSingleImageFeedModel != null) CommentsINSingleImageFeedModel.Add(commentmodel);
                        //        if (CommentsINImageView != null) CommentsINImageView.Add(commentmodel);
                        //    }
                        //    else if (commentFeed != null && commentImgView == null)
                        //    {
                        //        commentmodel = commentFeed;
                        //        if (CommentsINImageView != null) CommentsINImageView.Add(commentmodel);
                        //    }
                        //    else if (commentImgView != null && commentFeed == null)
                        //    {
                        //        commentmodel = commentImgView;
                        //        if (CommentsINSingleImageFeedModel != null) CommentsINSingleImageFeedModel.Add(commentmodel);
                        //    }
                        //    else commentmodel = commentFeed;

                        //    commentmodel.ImageId = imageId;
                        //    if (nfid != Guid.Empty) commentmodel.NewsfeedId = nfid;
                        //    if (postOwnerUtId > 0) commentmodel.PostOwnerUtId = postOwnerUtId;
                        //    commentmodel.IsEditMode = false;
                        //}
                        //    if (CommentsINSingleImageFeedModel != null) feedModel.CommentList = new ObservableCollection<CommentModel>(CommentsINSingleImageFeedModel.OrderBy(a => a.Time));
                        //    if (CommentsINImageView != null) basicImageViewWrapper().ucBasicImageView.ImageComments.ImageCommentList = new ObservableCollection<CommentModel>(CommentsINImageView.OrderBy(a => a.Time));
                        //
                    }
                    //if (basicImageViewWrapper().ucBasicImageView != null && basicImageViewWrapper().ucBasicImageView.clickedImageID == imageId)
                    //{
                    //    basicImageViewWrapper().ucBasicImageView.ImageComments.PreviousCommentsVisibility = (basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage.NumberOfComments > CommentsINImageView.Count) ? Visibility.Visible : Visibility.Collapsed;
                    //}
                    //if (feedModel != null) feedModel.OnPropertyChanged("CurrentInstance");
                    FetchImageCommentsUI();
                }
                );
            }
            catch (Exception ex)
            {
                log.Error("Error: FetchImageCommentsFromServer() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void FetchImageCommentsUI()
        {
            //if (basicImageViewWrapper().ucBasicImageView != null)
            //{
            //    basicImageViewWrapper().ucBasicImageView.StopandRemoveAnimation();
            //}
        }

        public void AddMyImagesFromServer(int albumImageType, int totalImage, List<ImageModel> imageList)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    List<ImageModel> sortedList = (from l in imageList orderby l.Time descending select l).ToList();

                    #region "Image full view"
                    //if (basicImageViewWrapper().ucBasicImageView == null)
                    //{
                    //    basicImageViewWrapper().AddUCBasicImageViewToWrapperPanel();
                    //}
                    //basicImageViewWrapper().ucBasicImageView.AddIntoImageList(albumImageType, DefaultSettings.LOGIN_TABLE_ID, totalImage, sortedList);
                    #endregion "Image full view"

                    #region ProfileImage
                    if (albumImageType == SettingsConstants.TYPE_PROFILE_IMAGE)
                    {
                        RingIDViewModel.Instance.ProfileImageCount = totalImage;
                        foreach (ImageModel model in sortedList)
                        {
                            lock (RingIDViewModel.Instance.ProfileImageList)
                            {
                                if (!RingIDViewModel.Instance.ProfileImageList.Any(p => p.ImageId == model.ImageId))
                                {
                                    //ImageModel model = new ImageModel(image);
                                    RingIDViewModel.Instance.ProfileImageList.Insert(RingIDViewModel.Instance.ProfileImageList.Count - 1, model);
                                    Thread.Sleep(5);
                                }
                            }
                        }
                        RingIDViewModel.Instance.IsVisibleProfilePhotosTopLoader = Visibility.Collapsed;

                        //if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos != null
                        //    && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyProfilePhotosAlbum != null)
                        //{
                        //    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyProfilePhotosAlbum.ShowHideShowMoreLoading
                        //        (RingIDViewModel.Instance.ProfileImageCount > (RingIDViewModel.Instance.ProfileImageList.Count - 1));
                        //} //to do

                        /*if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange != null
                            && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange._UCMyProfilePhotosChangeAlbum != null)
                        {
                            if (RingIDViewModel.Instance.ProfileImageCount > (RingIDViewModel.Instance.ProfileImageList.Count - 1))
                            {
                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange._UCMyProfilePhotosChangeAlbum.ShowMore();
                            }
                            else
                            {
                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange._UCMyProfilePhotosChangeAlbum.HideShowMoreLoading();
                            }
                        }*/

                        /*if (RingIDViewModel.Instance != null && RingIDViewModel.Instance.MyAlbums != null)
                        {
                            if (RingIDViewModel.Instance.ProfileImageCount > (RingIDViewModel.Instance.ProfileImageList.Count - 1))
                            {
                                RingIDViewModel.Instance.MyAlbums.AlbumProfileShowMore();
                            }
                            else
                            {
                                RingIDViewModel.Instance.MyAlbums.HideAlbumProfileShowMoreLoading();
                            }
                        }*/
                    }
                    #endregion

                    #region CoverImage
                    else if (albumImageType == SettingsConstants.TYPE_COVER_IMAGE)
                    {
                        RingIDViewModel.Instance.CoverImageCount = totalImage;
                        foreach (ImageModel model in sortedList)
                        {
                            lock (RingIDViewModel.Instance.CoverImageList)
                            {
                                if (!RingIDViewModel.Instance.CoverImageList.Any(p => p.ImageId == model.ImageId))
                                {
                                    //ImageModel model = new ImageModel(image);
                                    RingIDViewModel.Instance.CoverImageList.Insert(RingIDViewModel.Instance.CoverImageList.Count - 1, model);
                                    Thread.Sleep(5);
                                }
                            }
                        }
                        RingIDViewModel.Instance.IsVisibleCoverPhotosTopLoader = Visibility.Collapsed;

                        //if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos != null
                        //    && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyCoverPhotosAlbum != null)
                        //{
                        //    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyCoverPhotosAlbum.ShowHideShowMoreLoading
                        //        (RingIDViewModel.Instance.CoverImageCount > (RingIDViewModel.Instance.CoverImageList.Count - 1));
                        //} //to do

                        /*if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange != null
                            && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange._UCMyCoverPhotosChangeAlbum != null)
                        {
                            if (RingIDViewModel.Instance.CoverImageCount > (RingIDViewModel.Instance.CoverImageList.Count - 1))
                            {
                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange._UCMyCoverPhotosChangeAlbum.ShowMore();
                            }
                            else
                            {
                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange._UCMyCoverPhotosChangeAlbum.HideShowMoreLoading();
                            }
                        }*/

                        /*if (RingIDViewModel.Instance != null && RingIDViewModel.Instance.MyAlbums != null)
                        {
                            if (RingIDViewModel.Instance.CoverImageCount > (RingIDViewModel.Instance.CoverImageList.Count - 1))
                            {
                                RingIDViewModel.Instance.MyAlbums.AlbumCoverShowMore();
                            }
                            else
                            {
                                RingIDViewModel.Instance.MyAlbums.HideAlbumCoverShowMoreLoading();
                            }
                        }*/
                    }
                    #endregion

                    #region FeedImage
                    else if (albumImageType == SettingsConstants.TYPE_NORMAL_BOOK_IMAGE)
                    {
                        RingIDViewModel.Instance.FeedImageCount = totalImage;
                        foreach (ImageModel model in sortedList)
                        {
                            lock (RingIDViewModel.Instance.FeedImageList)
                            {
                                if (!RingIDViewModel.Instance.FeedImageList.Any(p => p.ImageId == model.ImageId))
                                {
                                    //ImageModel model = new ImageModel(image);
                                    RingIDViewModel.Instance.FeedImageList.Insert(RingIDViewModel.Instance.FeedImageList.Count - 1, model);
                                    Thread.Sleep(5);
                                }
                            }
                        }
                        RingIDViewModel.Instance.OnPropertyChanged("FeedImageList");
                        RingIDViewModel.Instance.IsVisibleFeedPhotosTopLoader = Visibility.Collapsed;

                        //if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos != null
                        //    && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyFeedPhotosAlbum != null)
                        //{
                        //    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyFeedPhotosAlbum.ShowHideShowMoreLoading
                        //        (RingIDViewModel.Instance.FeedImageCount > (RingIDViewModel.Instance.FeedImageList.Count - 1));
                        //} //to do

                        /*if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange != null
                            && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange._UCMyFeedPhotosChangeAlbum != null)
                        {
                            if (RingIDViewModel.Instance.FeedImageCount > (RingIDViewModel.Instance.FeedImageList.Count - 1))
                            {
                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange._UCMyFeedPhotosChangeAlbum.ShowMore();
                            }
                            else
                            {
                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange._UCMyFeedPhotosChangeAlbum.HideShowMoreLoading();
                            }
                        }*/

                        /*if (RingIDViewModel.Instance != null && RingIDViewModel.Instance.MyAlbums != null)
                        {
                            if (RingIDViewModel.Instance.FeedImageCount > (RingIDViewModel.Instance.FeedImageList.Count - 1))
                            {
                                RingIDViewModel.Instance.MyAlbums.AlbumFeedShowMore();
                            }
                            else
                            {
                                RingIDViewModel.Instance.MyAlbums.HideAlbumFeedShowMoreLoading();
                            }
                        }*/
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    log.Error("Error: AddMyImagesFromServer() => " + ex.Message + "\n" + ex.StackTrace);
                }
                finally
                {
                    //HelperMethods.ReleaseMemory(DefaultSettings.FEED_TYPE_ALL);
                }
            });
        }

        public void NoAlbumFoundInPhotos(long utId)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    if (utId == 0)
                    {
                        if(UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos != null)
                        {
                            UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.BOTTOM_LOADING = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
                        }
                        if (RingIDViewModel.Instance != null && RingIDViewModel.Instance.MyAlbums != null && RingIDViewModel.Instance.MyAlbums.IsVisible)
                        {
                            RingIDViewModel.Instance.MyAlbums.BOTTOM_LOADING = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
                        }
                    }
                    else
                    {
                        if(UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == utId 
                            && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendPhotos != null)
                        {
                            UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendPhotos.BOTTOM_LOADING = StatusConstants.LOADING_NO_MORE_FEED_VISIBLE;
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: NoAlbumFoundInPhotos() ==> " + ex.Message + "\n" + ex.StackTrace);
                }

            });
        }

        public void NoImageFoundInMyAlbum(string albumId)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos != null)
                    {
                        if (albumId == DefaultSettings.PROFILE_IMAGE_ALBUM_ID)
                        {
                            RingIDViewModel.Instance.IsVisibleProfilePhotosTopLoader = Visibility.Collapsed; //to do
                            //if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyProfilePhotosAlbum != null)
                            //    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyProfilePhotosAlbum.ShowHideShowMoreLoading
                            //        (RingIDViewModel.Instance.ProfileImageCount > (RingIDViewModel.Instance.ProfileImageList.Count - 1));
                        }
                        else if (albumId == DefaultSettings.COVER_IMAGE_ALBUM_ID)
                        {
                            RingIDViewModel.Instance.IsVisibleCoverPhotosTopLoader = Visibility.Collapsed; //to do
                            //if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyCoverPhotosAlbum != null)
                            //    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyCoverPhotosAlbum.ShowHideShowMoreLoading
                            //        (RingIDViewModel.Instance.CoverImageCount > (RingIDViewModel.Instance.CoverImageList.Count - 1));
                        }
                        else if (albumId == DefaultSettings.FEED_IMAGE_ALBUM_ID)
                        {
                            RingIDViewModel.Instance.IsVisibleFeedPhotosTopLoader = Visibility.Collapsed; //to do
                            //if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyFeedPhotosAlbum != null)
                            //    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyFeedPhotosAlbum.ShowHideShowMoreLoading
                            //        (RingIDViewModel.Instance.FeedImageCount > (RingIDViewModel.Instance.FeedImageList.Count - 1));
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: NoImageFoundInMyAlbum() ==> " + ex.Message + "\n" + ex.StackTrace);
                }

            });
        }

        //public void FetchImageCommentLikersFromServer(List<UserBasicInfoDTO> likers, long imgId, long commentId)
        //{
        //    try
        //    {
        //        Application.Current.Dispatcher.Invoke(() =>
        //        {
        //            if (LikeListViewWrapper != null && LikeListViewWrapper.ucLikeListView != null)
        //            {
        //                foreach (UserBasicInfoDTO liker in likers)
        //                {
        //                    UserBasicInfoModel userModel = MainSwitcher.AuthSignalHandler().GetExistingorNewUserBasicModelFromDTO(liker);
        //                    if (LikeListViewWrapper.ucLikeListView.ImageId == imgId &&
        //                        LikeListViewWrapper.ucLikeListView.type == 3 && LikeListViewWrapper.ucLikeListView.CommentId == commentId)
        //                    {
        //                        LikeListViewWrapper.ucLikeListView.LikeList.Add(userModel);
        //                    }
        //                }

        //                LikeListViewWrapper.ucLikeListView.SetVisibilities();
        //            }
        //        }
        //        );

        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: FetchImageCommentLikersFromServer() ==> " + ex.Message + "\n" + ex.StackTrace);
        //    }
        //}

        //private void UpdateImageCommentLikeUnlike(Guid imgId, Guid commentId, int liked, UserBasicInfoDTO user, Guid nfid)
        //{
        //    MainSwitcher.AuthSignalHandler().feedSignalHandler.UpdateImageCommentLikeUnlike(imgId, commentId, liked, user, nfid);
        //}

        private void ShowErrorMessageBox(string msg, string msg1)
        {
            UIHelperMethods.ShowErrorMessageBoxFromThread(msg, msg1);
        }

        #endregion "Utility Methods"

        //"Commented Functions"
        //#region "private Methods"
        //#endregion "private Methods"

        //#region "Public Methods"
        //#endregion "Public Methods"

        //public void Process_UPDATE_LIKE_UNLIKE_IMAGE_385(JObject _JobjFromResponse)
        //{
        //    try
        //    {
        //        if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.ImageId] != null && _JobjFromResponse[JsonKeys.Liked] != null)
        //        {
        //            long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
        //            long imgId = (long)_JobjFromResponse[JsonKeys.ImageId];
        //            int liked = (int)_JobjFromResponse[JsonKeys.Liked];
        //            UserBasicInfoDTO user = new UserBasicInfoDTO();
        //            if (_JobjFromResponse[JsonKeys.UserTableID] != null)
        //                user.UserTableID = (long)_JobjFromResponse[JsonKeys.UserTableID];
        //            if (_JobjFromResponse[JsonKeys.FullName] != null)
        //                user.FullName = (string)_JobjFromResponse[JsonKeys.FullName];
        //            UserBasicInfoDTO tmp = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(user.UserTableID);
        //            if (tmp != null)
        //            {
        //                user.FriendShipStatus = tmp.FriendShipStatus;
        //            }
        //            else if (user.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
        //            {
        //                user.FriendShipStatus = -1; //for myself
        //            }
        //            MainSwitcher.AuthSignalHandler().feedSignalHandler.UpdateImageLikeUnlike(imgId, liked, user, nfid);
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //        log.Error("ProcessUpdateLikeUnlikeImage ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

        //    }
        //}

        //public void Process_EDIT_IMAGE_COMMENT_194(JObject _JobjFromResponse) //194
        //{
        //    //{"sucs":true,"cmnId":547,"cmn":"chup","fn":"Smite.Bakasura.Desk","imgId":67,"sc":false}
        //    try
        //    {
        //        if (_JobjFromResponse[JsonKeys.Success] != null)
        //        {
        //            if ((bool)_JobjFromResponse[JsonKeys.Success])
        //            {
        //                long nfid = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
        //                long imageId = (long)_JobjFromResponse[JsonKeys.ImageId];
        //                long commentId = (long)_JobjFromResponse[JsonKeys.CommentId];
        //                string cmnt = (string)_JobjFromResponse[JsonKeys.Comment];
        //                //  CommentDTO comment = null;
        //                //lock (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST)
        //                //{
        //                //    if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].TryGetValue(commentId, out comment))
        //                //    {
        //                //        comment.Comment = cmnt;
        //                //    }
        //                //}
        //                EditImageComment(imageId, commentId, cmnt, nfid, (_JobjFromResponse[JsonKeys.ShowContinue] != null && (bool)_JobjFromResponse[JsonKeys.ShowContinue]));
        //            }
        //            else
        //            {
        //                string msg = "Failed to Comment!";
        //                if (_JobjFromResponse[JsonKeys.Message] != null)
        //                    msg = (string)_JobjFromResponse[JsonKeys.Message];
        //                ShowErrorMessageBox(msg, "Failed!");
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //        log.Error("ProcessEditImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

        //    }
        //}

        //public void Process_ADD_IMAGE_COMMENT_180(JObject _JobjFromResponse, bool update = false) //180
        //{
        //    // {"sucs":true,"cmnId":547,"tm":1442491058781,"cmn":"asddsa","fn":"Smite.Bakasura.Desk","imgId":67,"sc":false,"loc":4}
        //    try
        //    {
        //        if (_JobjFromResponse[JsonKeys.Success] != null)
        //        {
        //            if ((bool)_JobjFromResponse[JsonKeys.Success])
        //            {
        //                long loc = (long)_JobjFromResponse[JsonKeys.LikeOrComment];
        //                long imgId = (long)_JobjFromResponse[JsonKeys.ImageId];
        //                long nfId = (_JobjFromResponse[JsonKeys.NewsfeedId] != null) ? (long)_JobjFromResponse[JsonKeys.NewsfeedId] : 0;
        //                long postOwnerId = 0;
        //                ObservableCollection<CommentModel> collectionFeed = null, collectionImgView = null;
        //                //
        //                FeedModel feedModel = null;
        //                if (nfId > 0 && FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out feedModel))
        //                {
        //                    if (feedModel.CommentList == null) feedModel.CommentList = new ObservableCollection<CommentModel>();
        //                    collectionFeed = feedModel.CommentList;
        //                    if (feedModel.UserShortInfoModel != null)
        //                        postOwnerId = feedModel.UserShortInfoModel.UserIdentity;
        //                    if (feedModel.SingleImageFeedModel != null)
        //                    {
        //                        feedModel.NumberOfComments = loc;//feedModel.NumberOfComments + 1;
        //                        feedModel.SingleImageFeedModel.NumberOfComments = loc;//(feedModel.SingleImageFeedModel.NumberOfComments + 1);
        //                        if (!update)
        //                        {
        //                            feedModel.IComment = 1;
        //                            feedModel.SingleImageFeedModel.IComment = 1;
        //                        }
        //                    }
        //                }
        //                if (basicImageViewWrapper().ucBasicImageView != null && basicImageViewWrapper().ucBasicImageView.clickedImageID == imgId)
        //                {
        //                    if (basicImageViewWrapper().ucBasicImageView.ImageComments != null)
        //                        collectionImgView = basicImageViewWrapper().ucBasicImageView.ImageComments.ImageCommentList;
        //                    if (basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage != null)
        //                    {
        //                        if (basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage.UserShortInfoModel != null)
        //                            postOwnerId = basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage.UserShortInfoModel.UserIdentity;

        //                        basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage.NumberOfComments = loc;//basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage.NumberOfComments + 1;
        //                        if (!update) basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage.IComment = 1;
        //                    }
        //                }
        //                //
        //                if (collectionFeed != null)
        //                    HelperMethods.InsertIntoCommentListByAscTime(collectionFeed, _JobjFromResponse, nfId, imgId, 0, postOwnerId);
        //                if (collectionImgView != null)
        //                    HelperMethods.InsertIntoCommentListByAscTime(collectionImgView, _JobjFromResponse, nfId, imgId, 0, postOwnerId);
        //            }
        //            else
        //            {
        //                string msg = "Failed to Comment!";
        //                if (_JobjFromResponse[JsonKeys.Message] != null)
        //                    msg = (string)_JobjFromResponse[JsonKeys.Message];
        //                ShowErrorMessageBox(msg, "Failed!");
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //        log.Error("ProcessAddImageComment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

        //    }
        //}

        //public void AddImageComment(long imageId, CommentDTO comment, long nfid, bool update = false)
        //{
        //    try
        //    {
        //        Application.Current.Dispatcher.Invoke(() =>
        //        {
        //            ObservableCollection<CommentModel> CommentsINSingleImgFeedModel = null;
        //            ObservableCollection<CommentModel> CommentsINImgView = null;
        //            FeedModel feedModel = null;
        //            long postOwnerUid = 0;
        //            if (nfid > 0)
        //            {
        //                feedModel = GetFeedModelByNfid(nfid);
        //                if (feedModel != null)
        //                {
        //                    if (feedModel.SingleImageFeedModel != null)
        //                    {
        //                        CommentsINSingleImgFeedModel = feedModel.CommentList;
        //                        feedModel.NumberOfComments = feedModel.NumberOfComments + 1;
        //                        feedModel.SingleImageFeedModel.NumberOfComments = (feedModel.SingleImageFeedModel.NumberOfComments + 1);
        //                        if (!update)
        //                        {
        //                            feedModel.IComment = 1;
        //                            feedModel.SingleImageFeedModel.IComment = 1;
        //                        }
        //                    }
        //                    if (feedModel.UserShortInfoModel != null && feedModel.UserShortInfoModel.UserIdentity > 0)
        //                        postOwnerUid = feedModel.UserShortInfoModel.UserIdentity;
        //                }
        //            }
        //            if (basicImageViewWrapper().ucBasicImageView != null &&
        //                basicImageViewWrapper().ucBasicImageView.clickedImageID == imageId)
        //            {
        //                if (basicImageViewWrapper().ucBasicImageView.ImageComments != null)
        //                    CommentsINImgView = basicImageViewWrapper().ucBasicImageView.ImageComments.ImageCommentList;
        //                if (basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage != null)
        //                {
        //                    basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage.NumberOfComments = basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage.NumberOfComments + 1;
        //                    if (!update) basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage.IComment = 1;
        //                }
        //                if (basicImageViewWrapper().ucBasicImageView.clickedUserID > 0)
        //                    postOwnerUid = basicImageViewWrapper().ucBasicImageView.clickedUserID;
        //            }
        //            if (CommentsINImgView != null || CommentsINSingleImgFeedModel != null)
        //            {
        //                CommentModel commentFeed = null, commentMdaView = null, commentmodel = null;
        //                if (CommentsINSingleImgFeedModel != null) commentFeed = CommentsINSingleImgFeedModel.Where(p => p.CommentId == comment.CommentId).FirstOrDefault();
        //                if (CommentsINImgView != null) commentMdaView = CommentsINImgView.Where(p => p.CommentId == comment.CommentId).FirstOrDefault();
        //                if (commentFeed == null && commentMdaView == null)
        //                {
        //                    commentmodel = new CommentModel();
        //                    if (CommentsINSingleImgFeedModel != null) CommentsINSingleImgFeedModel.Add(commentmodel);
        //                    if (CommentsINImgView != null) CommentsINImgView.Add(commentmodel);
        //                }
        //                else if (commentFeed != null && commentMdaView == null)
        //                {
        //                    commentmodel = commentFeed;
        //                    if (CommentsINImgView != null) CommentsINImgView.Insert(0, commentmodel);
        //                }
        //                else if (commentMdaView != null && commentFeed == null)
        //                {
        //                    commentmodel = commentMdaView;
        //                    if (CommentsINSingleImgFeedModel != null) CommentsINSingleImgFeedModel.Add(commentmodel);
        //                }
        //                else commentmodel = commentFeed;

        //                //commentmodel.LoadData(comment);
        //                commentmodel.ImageId = imageId;
        //                if (nfid > 0) commentmodel.NewsfeedId = nfid;
        //                if (postOwnerUid > 0) commentmodel.PostOwnerUid = postOwnerUid;
        //                commentmodel.IsEditMode = false;
        //            }
        //            if (feedModel != null && feedModel.SingleImageFeedModel != null)
        //                feedModel.OnPropertyChanged("CurrentInstance");
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: AddImageComment() ==> " + ex.Message + "\n" + ex.StackTrace);
        //    }
        //}

        //public void DeleteImageComment(long imgId, long commentId, long nfid)
        //{
        //    try
        //    {
        //        Application.Current.Dispatcher.Invoke(() =>
        //        {
        //            if (basicImageViewWrapper().ucBasicImageView != null && basicImageViewWrapper().ucBasicImageView.ImageComments != null &&
        //                basicImageViewWrapper().ucBasicImageView.clickedImageID == imgId)
        //            {
        //                lock (basicImageViewWrapper().ucBasicImageView.ImageComments.ImageCommentList)
        //                {
        //                    CommentModel model = basicImageViewWrapper().ucBasicImageView.ImageComments.ImageCommentList.Where(P => P.CommentId == commentId).FirstOrDefault();
        //                    if (model != null)
        //                    {
        //                        basicImageViewWrapper().ucBasicImageView.ImageComments.ImageCommentList.Remove(model);
        //                        basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage.ChangeNumberOfComments(false);
        //                        CommentModel anyMoreCommentbyMe = basicImageViewWrapper().ucBasicImageView.ImageComments.ImageCommentList.Where(P => P.UserShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID).FirstOrDefault();
        //                        if (anyMoreCommentbyMe == null)
        //                            basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage.IComment = 0;
        //                    }
        //                }
        //            }
        //            FeedModel feedModel = (nfid > 0) ? (GetFeedModelByNfid(nfid)) : null;
        //            if (feedModel != null && feedModel.SingleImageFeedModel != null)
        //            {
        //                feedModel.NumberOfComments = feedModel.NumberOfComments - 1;
        //                feedModel.SingleImageFeedModel.NumberOfComments = feedModel.SingleImageFeedModel.NumberOfComments - 1;
        //                CommentModel model = feedModel.CommentList.Where(P => P.CommentId == commentId).FirstOrDefault();
        //                if (model != null)
        //                {
        //                    feedModel.CommentList.Remove(model);
        //                    if (!feedModel.CommentList.Any(P => P.UserShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID))
        //                    {
        //                        feedModel.IComment = 0;
        //                        feedModel.SingleImageFeedModel.IComment = 0;
        //                    }
        //                    //DeleteCommentFromMediaDictionary(contentId, UCGuiRingID.Instance.ucBasicMediaView.ImageFullNameProfileImage.IComment);
        //                }
        //                feedModel.OnPropertyChanged("CurrentInstance");
        //            }
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: DeleteImageComment() ==> " + ex.Message + "\n" + ex.StackTrace);

        //    }
        //}

        //public void EditImageComment(long imgId, long commentId, string cmnt, long nfid, bool sc, bool update = false)
        //{
        //    Application.Current.Dispatcher.Invoke(() =>
        //    {
        //        try
        //        {
        //            ObservableCollection<CommentModel> CommentsINSingleImageFeedModel = null;
        //            ObservableCollection<CommentModel> CommentsINImgView = null;
        //            FeedModel feedModel = null;
        //            if (nfid > 0)
        //            {
        //                feedModel = GetFeedModelByNfid(nfid);
        //                if (feedModel != null && feedModel.SingleImageFeedModel != null)
        //                {
        //                    CommentsINSingleImageFeedModel = feedModel.CommentList;
        //                }
        //            }
        //            if (basicImageViewWrapper().ucBasicImageView != null &&
        //                basicImageViewWrapper().ucBasicImageView.clickedImageID == imgId && basicImageViewWrapper().ucBasicImageView.ImageComments != null)
        //            {
        //                CommentsINImgView = basicImageViewWrapper().ucBasicImageView.ImageComments.ImageCommentList;
        //            }
        //            if (CommentsINImgView != null || CommentsINSingleImageFeedModel != null)
        //            {
        //                CommentModel commentFeed = null, commentImgView = null;
        //                if (CommentsINSingleImageFeedModel != null) commentFeed = CommentsINSingleImageFeedModel.Where(p => p.CommentId == commentId).FirstOrDefault();
        //                if (CommentsINImgView != null) commentImgView = CommentsINImgView.Where(p => p.CommentId == commentId).FirstOrDefault();
        //                if (commentFeed != null)
        //                {
        //                    commentFeed.Comment = cmnt;
        //                    commentFeed.ShowContinue = sc;
        //                    commentFeed.IsEditMode = false;
        //                }
        //                else if (commentImgView != null)
        //                {
        //                    commentImgView.Comment = cmnt;
        //                    commentImgView.ShowContinue = sc;
        //                    commentImgView.IsEditMode = false;
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            log.Error("Error: EditImageComment() ==> " + ex.Message + "\n" + ex.StackTrace);

        //        }
        //    });

        //}

        //public void LikeImageComment(long nfid, long imgId, CommentDTO commentDTO)
        //{
        //    try
        //    {
        //        if (basicImageViewWrapper().ucBasicImageView != null && basicImageViewWrapper().ucBasicImageView.ImageComments != null &&
        // basicImageViewWrapper().ucBasicImageView.clickedImageID == imgId)
        //        {
        //            lock (basicImageViewWrapper().ucBasicImageView.ImageComments.ImageCommentList)
        //            {
        //                CommentModel model = basicImageViewWrapper().ucBasicImageView.ImageComments.ImageCommentList.Where(P => P.CommentId == commentDTO.CommentId).FirstOrDefault();
        //                if (model != null)
        //                {
        //                    model.ILikeComment = commentDTO.ILikeComment;
        //                    model.TotalLikeComment = commentDTO.TotalLikeComment;
        //                }
        //            }
        //        }
        //        FeedModel feedModel = (nfid > 0) ? (GetFeedModelByNfid(nfid)) : null;
        //        if (feedModel != null && feedModel.SingleImageFeedModel != null && feedModel.CommentList.Count > 0)
        //        {
        //            CommentModel model = feedModel.CommentList.Where(P => P.CommentId == commentDTO.CommentId).FirstOrDefault();
        //            if (model != null)
        //            {
        //                model.ILikeComment = commentDTO.ILikeComment;
        //                model.TotalLikeComment = commentDTO.TotalLikeComment;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: LikeImageComment() ==> " + ex.Message + "\n" + ex.StackTrace);
        //    }
        //}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Dictonary;
using View.UI;
using View.UI.PopUp;
using View.UI.Profile.FriendProfile;
using View.ViewModel;
using View.UI.Profile.CelebrityProfile;
using View.Utility.DataContainer;

namespace View.Utility.Auth
{
    public class BusinessPageSignalHandler
    {
        //FeedSignalHandler feedSignalHandler = new FeedSignalHandler();
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(BusinessPageSignalHandler).Name);
        #endregion"Fields"

        public void Process_BUSINESSPAGE_BREAKING_FEED_303(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true && _JobjFromResponse[JsonKeys.BreakingNewsFeeds] != null)
                {
                    JArray jarray = (JArray)_JobjFromResponse[JsonKeys.BreakingNewsFeeds];
                    List<FeedModel> breakingNws = new List<FeedModel>();
                    foreach (JObject singleObj in jarray)
                    {
                        FeedModel model = new FeedModel();
                        model.NewsModel = new NewsModel();
                        model.NewsfeedId = model.NewsModel.NewsId = (singleObj[JsonKeys.Id] != null) ? ((Guid)(singleObj[JsonKeys.Id])) : Guid.Empty;
                        model.ImageList = new ObservableCollection<ImageModel>();
                        model.ImageList.Add(new ImageModel { ImageUrl = (singleObj[JsonKeys.ProfileImage] != null) ? ((string)(singleObj[JsonKeys.ProfileImage])) : string.Empty });
                        model.NewsModel.NewsShortDescription = (singleObj[JsonKeys.NewsShortDescription] != null) ? ((string)(singleObj[JsonKeys.NewsShortDescription])) : string.Empty;
                        model.NewsModel.NewsTitle = (singleObj[JsonKeys.NewsPortalTitle] != null) ? ((string)(singleObj[JsonKeys.NewsPortalTitle])) : string.Empty;
                        model.LoadData(singleObj);
                        breakingNws.Add(model);
                    }
                    MainSwitcher.AuthSignalHandler().feedSignalHandler.BreakingNewsSliderUI(breakingNws, SettingsConstants.PROFILE_TYPE_PAGES);
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessBreakingNewsPortalFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void Process_BUSINESS_PAGE_FEED_306(JObject _JobjFromResponse, string client_packet_id)
        {
            try
            {
                if (_JobjFromResponse != null && (bool)_JobjFromResponse[JsonKeys.Success] == true)
                {
                    if (_JobjFromResponse[JsonKeys.NewsFeedList] != null)
                    {
                        int feed_state = client_packet_id.Equals(DefaultSettings.ALLPAGE_STARTPKT) ? SettingsConstants.FEED_FIRSTTIME : ((_JobjFromResponse[JsonKeys.Scroll] != null && ((int)_JobjFromResponse[JsonKeys.Scroll]) == 1) ? SettingsConstants.FEED_ADDORUPDATE : SettingsConstants.FEED_REGULAR);
                        JArray jarray = (JArray)_JobjFromResponse[JsonKeys.NewsFeedList];
                        foreach (JObject singleObj in jarray)
                        {
                            Guid nfId = (singleObj[JsonKeys.NewsfeedId] != null) ? (Guid)singleObj[JsonKeys.NewsfeedId] : Guid.Empty;
                            FeedModel model = null;
                            if (!FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out model)) { model = new FeedModel(); FeedDataContainer.Instance.FeedModels[nfId] = model; }
                            model.LoadData(singleObj);
                            FeedDataContainer.Instance.AllPagesFeedsSortedIds.InsertIntoSortedList(model.ActualTime, nfId);
                            if (feed_state == SettingsConstants.FEED_FIRSTTIME)
                            {
                                //if (UCMiddlePanelSwitcher.View_UCPagesMainPanel != null
                                //    && UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCNewsContainerPanel != null
                                //    && UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCNewsContainerPanel.IsVisible)
                                //FirstTimeFeedLoadWorker.Instance.LoadFirstTimeFeed(RingIDViewModel.Instance.AllPagesFeeds, model);
                                //else if (!RingIDViewModel.Instance.AllPagesFeeds.Any(P => P.NewsfeedId == model.NewsfeedId))
                                //RingIDViewModel.Instance.AllPagesFeeds.InsertModel(model);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessPagesFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

    }
}

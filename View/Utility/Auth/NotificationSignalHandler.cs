﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.UI;
using View.UI.Notification;
using View.Utility.Notification;
using View.ViewModel;

namespace View.Utility.Auth
{
    public class NotificationSignalHandler
    {
        #region "Private Fields"
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(NotificationSignalHandler).Name);
        #endregion "Private Fields"

        #region "Private methods"
        #endregion "Private methods"

        #region "Signnal Handler Methods"

        public void Process_MY_NOTIFICATIONS_111(JObject _JobjFromResponse)
        {
            List<NotificationDTO> notificationList = new List<NotificationDTO>();
            try
            {
                if (_JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
                {
                    int scl = _JobjFromResponse[JsonKeys.Scroll] != null ? (int)_JobjFromResponse[JsonKeys.Scroll] : 0;
                    int totalNotificationCount = (_JobjFromResponse[JsonKeys.TotalNotificationCount] != null) ? (int)_JobjFromResponse[JsonKeys.TotalNotificationCount] : 0;
                    if (_JobjFromResponse[JsonKeys.NotificationList] != null)
                    {
                        JArray notificationListJSON = (JArray)_JobjFromResponse[JsonKeys.NotificationList];
                        foreach (JObject notificationJSON in notificationListJSON)
                        {
                            lock (notificationList)
                            {
                                NotificationDTO dto = HelperMethodsModel.BindNotificationInfo(notificationJSON);
                                
                                if (!(string.IsNullOrEmpty(dto.FriendName) || dto.MessageType == 0))                                
                                {
                                    NotificationDTO existingDTO = NotificationDTO.FindExistingDTO(dto);
                                    //existing and new notification comes
                                    if (existingDTO != null && existingDTO.UpdateTime < dto.UpdateTime)
                                    {
                                        if (!dto.PreviousIds.Contains(existingDTO.ID))
                                        {
                                            dto.PreviousIds.Add(existingDTO.ID);
                                        }

                                        dto.PreviousIds.AddRange(existingDTO.PreviousIds);
                                        RingDictionaries.Instance.NOTIFICATION_LISTS.Remove(existingDTO);
                                        RingDictionaries.Instance.NOTIFICATION_LISTS.Add(dto);
                                        notificationList.Add(dto);
                                        RingIDViewModel.Instance.AllNotificationCounter = RingIDViewModel.Instance.AllNotificationCounter > 0 ? RingIDViewModel.Instance.AllNotificationCounter-1 : 0;
                                        RingIDViewModel.Instance.AllNotificationCounter++;
                                        new DeleteFromNotificationHistoryTable(dto.PreviousIds);
                                    }
                                    //existing but old notification comes
                                    else if (existingDTO != null && existingDTO.UpdateTime > dto.UpdateTime)
                                    {
                                        int idx = RingDictionaries.Instance.NOTIFICATION_LISTS.IndexOf(existingDTO);
                                        if (!RingDictionaries.Instance.NOTIFICATION_LISTS[idx].PreviousIds.Contains(dto.ID))
                                        {
                                            RingDictionaries.Instance.NOTIFICATION_LISTS[idx].PreviousIds.Add(dto.ID);
                                        }
                                        new DeleteFromNotificationHistoryTable(RingDictionaries.Instance.NOTIFICATION_LISTS[idx].PreviousIds);
                                    }
                                    // fresh insert
                                    else
                                    {
                                        if (!RingDictionaries.Instance.NOTIFICATION_LISTS.Any(x => x.ID == dto.ID))
                                        {
                                            RingDictionaries.Instance.NOTIFICATION_LISTS.Add(dto);
                                            notificationList.Add(dto);
                                            RingIDViewModel.Instance.AllNotificationCounter++;
                                        }
                                    }
                                }
                               
                                if (dto.UpdateTime > AppConstants.NOTIFICATION_MAX_UT) { AppConstants.NOTIFICATION_MAX_UT = dto.UpdateTime; }
                                else if (dto.UpdateTime < AppConstants.NOTIFICATION_MIN_UT || AppConstants.NOTIFICATION_MIN_UT == 0) { AppConstants.NOTIFICATION_MIN_UT = dto.UpdateTime; }
                            }
                        }
                    
                        AddNotificationFromServer(notificationList, scl == 1 ? true : false);
                        NotificationListCount();
                    }
                }
                else
                {
                    if (_JobjFromResponse[JsonKeys.Scroll] != null && (short)_JobjFromResponse[JsonKeys.Scroll] == 2)
                    {
                        RemoveSeeMorePanel();
                    }
                    NotificationListCount();
                }
            }
            catch (Exception e)
            {

                log.Error("Exception in ProcessMyNotifications ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

            }
        }

        public void Process_SINGLE_NOTIFICATION_113(JObject _JobjFromResponse)
        {
            try
            {
                if (_JobjFromResponse[JsonKeys.NtUUID] != null)
                {
                    List<NotificationDTO> list = new List<NotificationDTO>();
                    NotificationDTO dto = HelperMethodsModel.BindNotificationInfo(_JobjFromResponse);

                    lock (RingDictionaries.Instance.NOTIFICATION_LISTS)
                    {
                        if (!(string.IsNullOrEmpty(dto.FriendName) || dto.MessageType == 0))                        
                        {
                            NotificationDTO existingDTO = NotificationDTO.FindExistingDTO(dto);
                            //existing and new notification comes
                            if (existingDTO != null && existingDTO.UpdateTime < dto.UpdateTime)
                            {
                                dto.PreviousIds.AddRange(existingDTO.PreviousIds);
                                dto.PreviousIds.Add(existingDTO.ID);

                                RingDictionaries.Instance.NOTIFICATION_LISTS.Remove(existingDTO);
                                RingDictionaries.Instance.NOTIFICATION_LISTS.Add(dto);

                                if (!list.Contains(dto))
                                {
                                    list.Add(dto);
                                    RingIDViewModel.Instance.AllNotificationCounter = RingIDViewModel.Instance.AllNotificationCounter > 0 ? RingIDViewModel.Instance.AllNotificationCounter-1 : 0;
                                    RingIDViewModel.Instance.AllNotificationCounter++;
                                }

                                new DeleteFromNotificationHistoryTable(dto.PreviousIds);
                            }
                            //existing but old notification comes
                            else if (existingDTO != null && existingDTO.UpdateTime > dto.UpdateTime)
                            {
                                int idx = RingDictionaries.Instance.NOTIFICATION_LISTS.IndexOf(existingDTO);

                                if (!existingDTO.PreviousIds.Contains(dto.ID))
                                {
                                    RingDictionaries.Instance.NOTIFICATION_LISTS[idx].PreviousIds.Add(dto.ID);
                                }
                                new DeleteFromNotificationHistoryTable(RingDictionaries.Instance.NOTIFICATION_LISTS[idx].PreviousIds);
                            }
                            else if (existingDTO != null && existingDTO.UpdateTime == dto.UpdateTime && existingDTO.ID == dto.ID)
                            {
                                //do nothing....omitting duplicate packets
                            }
                            // fresh insert
                            else
                            {
                                RingDictionaries.Instance.NOTIFICATION_LISTS.Add(dto);
                                if (!list.Contains(dto))
                                {
                                    list.Add(dto);
                                    RingIDViewModel.Instance.AllNotificationCounter++;
                                }
                            }
                        }                        
                    }

                    if (dto.UpdateTime > AppConstants.NOTIFICATION_MAX_UT) { AppConstants.NOTIFICATION_MAX_UT = dto.UpdateTime; }

                    else if (dto.UpdateTime < AppConstants.NOTIFICATION_MIN_UT || AppConstants.NOTIFICATION_MIN_UT == 0) { AppConstants.NOTIFICATION_MIN_UT = dto.UpdateTime; }
                    
                    pingNotificationSound();
                    AddNotificationFromServer(list, true);
                }
            }
            catch (Exception e)
            {

                log.Error("Exception in ProcessSingleNotification ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);

            }
        }

        #endregion "Signnal Handler Methods"

        #region "Utility Methods"

        public void AddNotificationFromServer(List<NotificationDTO> list, bool indexTop)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    list = (from l in list
                            orderby l.UpdateTime
                            select l).ToList();

                    lock (RingIDViewModel.Instance.NotificationList)
                    {
                        foreach (NotificationDTO dto in list)
                        {
                            foreach (var id in dto.PreviousIds)
                            {
                                if (RingIDViewModel.Instance.NotificationList.Any(x => x.NotificationID == id))
                                {
                                    NotificationModel model = RingIDViewModel.Instance.NotificationList.Where(x => x.NotificationID == id).First();
                                    RingIDViewModel.Instance.NotificationList.Remove(model);
                                }
                            }
                            NotificationModel insertModel = NotificationModel.LoadDataFromDTO(dto);
                            
                            if (!insertModel.NotificationID.Equals(Guid.Empty) && !RingIDViewModel.Instance.NotificationList.Any(x => x.NotificationID == insertModel.NotificationID))
                            {
                                RingIDViewModel.Instance.NotificationList.Add(insertModel);
                            }
                        }
                    }

                    if (UCAllNotification.Instance == null)
                    {
                        if (UCGuiRingID.Instance.ucFriendTabControlPanel._NotificationPanel.Children.Count > 0)
                        {
                            foreach (var item in UCGuiRingID.Instance.ucFriendTabControlPanel._NotificationPanel.Children)
                            {
                                UCGuiRingID.Instance.ucFriendTabControlPanel._NotificationPanel.Children.Remove(item as UIElement);
                            }
                        }
                        UCGuiRingID.Instance.ucFriendTabControlPanel._NotificationPanel.Children.Add(new UCAllNotification());
                    }                    
                    IncreaseAllnotificationCount();
                    VMNotification.Instance.NotificationModelCount = RingIDViewModel.Instance.NotificationList.Count;
                    VMNotification.Instance.IsLoading = false;
                    VMNotification.Instance.IsAllRead();
                    NotificationUtility.Instance.ShowMoreAction();

                    new InsertIntoNotificationHistoryTable(list);
                    VMNotification.Instance.NotificationLoaded = RingIDViewModel.Instance.NotificationList.Count > 0 ? 1 : 0;
                }
                catch (Exception ex)
                {
                    log.Error("Error: AddNotificationFromServerNew() ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            }, System.Windows.Threading.DispatcherPriority.Send);

        }

        public void IncreaseAllnotificationCount()
        {
            try
            {
                AppConstants.ALL_NOTIFICATION_COUNT = RingIDViewModel.Instance.AllNotificationCounter;
                new InsertIntoNotificationCountTable();                 
            }
            catch (Exception e)
            {
                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        private void pingNotificationSound()
        {
            Application.Current.Dispatcher.BeginInvoke(delegate
            {
                if (SettingsConstants.VALUE_RINGID_ALERT_SOUND)
                {
                    View.Utility.audio.AudioFilesAndSettings.Play(View.Utility.audio.AudioFilesAndSettings.NOTIFICATION_RECEIVED);
                }
            });
        }

        public void NotificationListCount()
        {
            try
            {
                if (UCAllNotification.Instance != null)
                {
                    VMNotification.Instance.NotificationModelCount = RingIDViewModel.Instance.NotificationList.Count;
                }
            }
            catch (Exception e)
            {
                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void AddSeemoreInNotificationList()
        {
            try
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    //NotificationUtility.Instance.AddSeeMorePanel();
                    NotificationListCount();
                    VMNotification.Instance.IsLoading = false;
                    NotificationUtility.Instance.ShowMoreAction();
                });

            }
            catch (Exception ex)
            {
                log.Error("Error: AddSeemoreInNotificationList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        //public void ShowAllNotificationCount(int count)
        //{
        //    Application.Current.Dispatcher.Invoke(() =>
        //        {
        //            try
        //            {
        //                if (AppConstants.ALL_NOTIFICATION_COUNT > 0)
        //                {
        //                    AppConstants.ALL_NOTIFICATION_COUNT = 0;
        //                }
        //                AppConstants.ALL_NOTIFICATION_COUNT = count;
        //                RingIDViewModel.Instance.AllNotificationCounter = AppConstants.ALL_NOTIFICATION_COUNT;
        //                RingIDViewModel.Instance.OnPropertyChanged("AllNotificationCounter");
        //                new InsertIntoNotificationCountTable();
        //            }
        //            catch (Exception e)
        //            {
        //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //            }
        //        });
        //}

        public void ClearAllNotificationCount()
        {
            try
            {
                if (AppConstants.ALL_NOTIFICATION_COUNT > 0)
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        AppConstants.ALL_NOTIFICATION_COUNT = 0;

                        RingIDViewModel.Instance.AllNotificationCounter = AppConstants.ALL_NOTIFICATION_COUNT;
                        RingIDViewModel.Instance.OnPropertyChanged("AllNotificationCounter");
                        new InsertIntoNotificationCountTable();
                        if (MainSwitcher.ThreadManager().ChangeNotificationStateRequest == null)
                        {
                            MainSwitcher.ThreadManager().ChangeNotificationStateRequest = new ThreadChangeNotificationStateRequest();
                        }
                        MainSwitcher.ThreadManager().ChangeNotificationStateRequest.StartThread();
                        // new ThreadChangeNotificationStateRequest();
                    });
                }
            }
            catch (Exception e)
            {
                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        //public bool CheckNotificationCount(NotificationDTO dto)
        //{
        //    bool increaseCounter = false;
        //    try
        //    {
        //        NotificationModel tmpNotificationModel = null;
        //        switch (dto.MessageType)
        //        {
        //            case StatusConstants.MESSAGE_LIKE_STATUS:
        //            case StatusConstants.MESSAGE_LIKE_COMMENT:
        //            case StatusConstants.MESSAGE_SHARE_STATUS:
        //            case StatusConstants.MESSAGE_NOTIFICATION_WITH_TAG:
        //            case StatusConstants.MESSAGE_ADD_STATUS_COMMENT:
        //                tmpNotificationModel = RingIDViewModel.Instance.NotificationList.Where(x => x.NewsfeedID == dto.NewsfeedID && x.MessageType == dto.MessageType).FirstOrDefault();
        //                if (tmpNotificationModel != null)
        //                {
        //                    if (tmpNotificationModel.IsRead == true)
        //                    { increaseCounter = true; }
        //                }
        //                else { increaseCounter = true; }
        //                break;
        //            case StatusConstants.MESSAGE_IMAGE_COMMENT:
        //            case StatusConstants.MESSAGE_AUDIO_MEDIA_COMMENT:
        //            case StatusConstants.MESSAGE_VIDEO_MEDIA_COMMENT:
        //            case StatusConstants.MESSAGE_LIKE_IMAGE:
        //            case StatusConstants.MESSAGE_LIKE_IMAGE_COMMENT:
        //            case StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA:
        //            case StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA_COMMENT:
        //            case StatusConstants.MESSAGE_AUDIO_MEDIA_VIEW:
        //            case StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA:
        //            case StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA_COMMENT:
        //            case StatusConstants.MESSAGE_VIDEO_MEDIA_VIEW:
        //                tmpNotificationModel = RingIDViewModel.Instance.NotificationList.Where(x => x.ImageID == dto.ImageID && x.MessageType == dto.MessageType).FirstOrDefault();
        //                if (tmpNotificationModel != null) { if (tmpNotificationModel.IsRead == true) { increaseCounter = true; } }
        //                else { increaseCounter = true; }
        //                break;
        //            case StatusConstants.MESSAGE_ADD_CIRCLE_MEMBER:
        //                tmpNotificationModel = RingIDViewModel.Instance.NotificationList.Where(x => x.ActivityID == dto.ActivityID && x.MessageType == dto.MessageType).FirstOrDefault();
        //                if (tmpNotificationModel != null) { if (tmpNotificationModel.IsRead == true) { increaseCounter = true; } }
        //                else { increaseCounter = true; }
        //                break;
        //            default:
        //                increaseCounter = true;
        //                break;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }
        //    return increaseCounter;
        //}

        public void RemoveSeeMorePanel()
        {
            try
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        if (UCAllNotification.Instance == null)
                        {
                            if (UCGuiRingID.Instance.ucFriendTabControlPanel._NotificationPanel.Children.Count > 0)
                            {
                                foreach (var item in UCGuiRingID.Instance.ucFriendTabControlPanel._NotificationPanel.Children)
                                {
                                    UCGuiRingID.Instance.ucFriendTabControlPanel._NotificationPanel.Children.Remove(item as UIElement);
                                }
                            }
                            UCGuiRingID.Instance.ucFriendTabControlPanel._NotificationPanel.Children.Add(new UCAllNotification());
                        }
                        VMNotification.Instance.IsLoading = false;
                        NotificationUtility.Instance.ShowMoreAction(SeeMoreText: NotificationMessages.NOTIFICATION_NO_MORE_NOTIFICATION);
                    });
            }
            catch (Exception e)
            {
                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        #endregion "Utility Methods"
    }
}

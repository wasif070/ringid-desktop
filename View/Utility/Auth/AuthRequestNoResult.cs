﻿using System;
using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;

namespace View.Utility.Auth
{
    public class AuthRequestNoResult
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(AuthRequestNoResult).Name);
        private bool running = false;
        JObject pakToSend;
        int paketType;
        int action;
        string message = null;
        int? limit = null;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        public AuthRequestNoResult() //"List Comments! "
        {

        }
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;

                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = action;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, this.paketType, packetId);
                if (feedbackfields != null)
                {
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                    MainSwitcher.AuthSignalHandler().StopLoaderAnimationInProfile(action, pakToSend);
                }
            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(JObject pakToSend2, int action1, int requestType, string message = null)
        {
            if (!running)
            {
                this.pakToSend = pakToSend2;
                this.paketType = requestType;
                this.action = action1;
                if (message != null) this.message = message;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

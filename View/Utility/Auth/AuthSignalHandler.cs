<<<<<<< HEAD
﻿using System;
using System.Windows;
using Auth.AppInterfaces;
using log4net;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.UI;
using View.UI.PopUp;
using View.Utility.FriendList;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.Utility.Auth
{
    public class AuthSignalHandler : IAuthSignalHandler
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(ImageSignalHandler).Name);
        public CallChatAuthSignalHandler callChatAuthSignalHandler;
        public CircleSignalHandler circleSignalHandler;
        public ContactListHandler contactListHandler;
        public FeedSignalHandler feedSignalHandler;
        public ImageSignalHandler imageSignalHandler;
        public NotificationSignalHandler notificationSignalHandler;
        public ProfileSignalHandler profileSignalHandler;
        public SignInSignUpSignOutHandler signInSignUpSignOutHandler;
        public WalletSignalHandler walletSignalHandler;
        public NewsPortalSignalHandler newsPortalSignalHandler;
        public BusinessPageSignalHandler businessPageSignalHandler;
        public MediaSignalHandler mediaSignalHandler;
        public CelebritySignalHandler celebritySignalHandler;
        public StreamSignalHandler liveStreamSignalHandler;
        public ChannelSignalHandler channelSignalHandler;

        #endregion "Fields"

        #region "Constructor"
        public AuthSignalHandler()
        {
            callChatAuthSignalHandler = new CallChatAuthSignalHandler();
            circleSignalHandler = new CircleSignalHandler();
            contactListHandler = new ContactListHandler();
            feedSignalHandler = new FeedSignalHandler();
            imageSignalHandler = new ImageSignalHandler();
            notificationSignalHandler = new NotificationSignalHandler();
            profileSignalHandler = new ProfileSignalHandler();
            signInSignUpSignOutHandler = new SignInSignUpSignOutHandler();
            walletSignalHandler = new WalletSignalHandler();
            newsPortalSignalHandler = new NewsPortalSignalHandler();
            businessPageSignalHandler = new BusinessPageSignalHandler();
            mediaSignalHandler = new MediaSignalHandler();
            celebritySignalHandler = new CelebritySignalHandler();
            liveStreamSignalHandler = new StreamSignalHandler();
            channelSignalHandler = new ChannelSignalHandler();
        }
        #endregion "Constructor"

        #region IAuthSignalHandler Members

        public void Process_INVALID_LOGIN_SESSION_19(Newtonsoft.Json.Linq.JObject jObject)
        {
            signInSignUpSignOutHandler.Process_INVALID_LOGIN_SESSION_19(jObject);
        }

        public void Process_CONTACT_LIST_23(global::Auth.Parser.CommonPacketAttributes commonPacketAttributes)
        {
            contactListHandler.Process_CONTACT_LIST_23(commonPacketAttributes);
        }

        public void Process_CONTACT_UTIDS_29(global::Auth.Parser.CommonPacketAttributes commonPacketAttributes)
        {
            contactListHandler.Process_CONTACT_UTIDS_29(commonPacketAttributes);
        }

        public void Process_SUGGESTION_IDS_31(Newtonsoft.Json.Linq.JObject jObject)
        {
            contactListHandler.Process_SUGGESTION_IDS_31(jObject);
        }

        public void Process_USERS_DETAILS_32(Newtonsoft.Json.Linq.JObject jObject)
        {
            contactListHandler.Process_USERS_DETAILS_32(jObject);
        }

        public void Process_CONTACT_SEARCH_34(Newtonsoft.Json.Linq.JObject jObject, string client_packet_id)
        {
            contactListHandler.Process_CONTACT_SEARCH_34(jObject, client_packet_id);
        }

        public void Process_NEW_CIRCLE_51(Newtonsoft.Json.Linq.JObject jObject)
        {
            circleSignalHandler.Process_NEW_CIRCLE_51(jObject);
        }

        public void Process_CIRCLE_DETAILS_52(Newtonsoft.Json.Linq.JObject jObject)
        {
            circleSignalHandler.Process_CIRCLE_DETAILS_52(jObject);
        }

        public void Process_LEAVE_CIRCLE_53(Newtonsoft.Json.Linq.JObject jObject)
        {
            circleSignalHandler.Process_LEAVE_CIRCLE_53(jObject);
        }

        public void Process_CIRCLE_LIST_70(Newtonsoft.Json.Linq.JObject jObject)
        {
            circleSignalHandler.Process_CIRCLE_LIST_70(jObject);
        }

        public void Process_MULTIPLE_SESSION_WARNING_75(global::Auth.Parser.CommonPacketAttributes commonPacketAttributes)
        {
            signInSignUpSignOutHandler.Process_MULTIPLE_SESSION_WARNING_75(commonPacketAttributes);
        }

        public void Process_PRESENCE_78(Newtonsoft.Json.Linq.JObject jObject)
        {
            profileSignalHandler.Process_PRESENCE_78(jObject);
        }

        public void Process_UNWANTED_LOGIN_79(global::Auth.Parser.CommonPacketAttributes commonPacketAttributes)
        {
            signInSignUpSignOutHandler.Process_UNWANTED_LOGIN_79(commonPacketAttributes);
        }

        public void Process_COMMENTS_FOR_STATUS_84(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_COMMENTS_FOR_STATUS_84(jObject);
        }

        public void Process_MEDIA_FEED_87(Newtonsoft.Json.Linq.JObject jObject, string client_packet_id)
        {
            mediaSignalHandler.Process_MEDIA_FEED_87(jObject, client_packet_id);
        }

        public void Process_NEWS_FEED_88(Newtonsoft.Json.Linq.JObject jObject, string client_packet_id)
        {
            feedSignalHandler.Process_NEWS_FEED_88(jObject, client_packet_id);
        }

        public void Process_COMMENTS_FOR_IMAGE_89(Newtonsoft.Json.Linq.JObject jObject)
        {
            imageSignalHandler.Process_COMMENTS_FOR_IMAGE_89(jObject);
        }

        public void Process_LIKES_FOR_STATUS_92(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_LIKES_FOR_STATUS_92(jObject);
        }

        public void Process_ACTION_MERGED_LIKES_LIST_OF_COMMENT_1116(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_ACTION_MERGED_LIKES_LIST_OF_COMMENT_1116(jObject);
        }

        public void Process_ACTION_MERGED_COMMENTS_LIST_1084(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_ACTION_MERGED_COMMENTS_LIST_1084(jObject);
        }

        public void Process_LIKES_FOR_IMAGE_93(Newtonsoft.Json.Linq.JObject jObject)
        {
            imageSignalHandler.Process_LIKES_FOR_IMAGE_93(jObject);
        }

        public void Process_MY_BOOK_94(Newtonsoft.Json.Linq.JObject jObject, string client_packet_id)
        {
            feedSignalHandler.Process_MY_BOOK_94(jObject, client_packet_id);
        }

        public void Process_IMAGE_ALBUM_LIST_96(Newtonsoft.Json.Linq.JObject jObject)
        {
            imageSignalHandler.Process_IMAGE_ALBUM_LIST_96(jObject);
        }

        public void Process_MY_ALBUM_IMAGES_97(Newtonsoft.Json.Linq.JObject jObject)
        {
            imageSignalHandler.Process_MY_ALBUM_IMAGES_97(jObject);
        }

        public void Process_CIRCLE_MEMBERS_LIST_99(Newtonsoft.Json.Linq.JObject jObject)
        {
            circleSignalHandler.Process_CIRCLE_MEMBERS_LIST_99(jObject);
        }

        public void Process_SEARCH_CIRCLE_MEMBER_101(Newtonsoft.Json.Linq.JObject jObject)
        {
            circleSignalHandler.Process_SEARCH_CIRCLE_MEMBER_101(jObject);
        }

        public void Process_FRIEND_CONTACT_LIST_107(Newtonsoft.Json.Linq.JObject jObject)
        {
            contactListHandler.Process_FRIEND_CONTACT_LIST_107(jObject);
        }

        public void Process_FRIEND_ALBUM_IMAGES_109(Newtonsoft.Json.Linq.JObject jObject)
        {
            //imageSignalHandler.Process_FRIEND_ALBUM_IMAGES_109(jObject);
        }

        public void Process_FRIEND_NEWSFEED_110(Newtonsoft.Json.Linq.JObject jObject, string clientpacketid)
        {
            feedSignalHandler.Process_FRIEND_NEWSFEED_110(jObject, clientpacketid);
        }

        public void Process_MY_NOTIFICATIONS_111(Newtonsoft.Json.Linq.JObject jObject)
        {
            notificationSignalHandler.Process_MY_NOTIFICATIONS_111(jObject);
        }

        public void Process_SINGLE_NOTIFICATION_113(Newtonsoft.Json.Linq.JObject jObject)
        {
            notificationSignalHandler.Process_SINGLE_NOTIFICATION_113(jObject);
        }

        public void Process_SINGLE_FEED_SHARE_LIST_115(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_SINGLE_FEED_SHARE_LIST_115(jObject);
        }

        public void Process_LIST_LIKES_OF_COMMENT_116(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_LIST_LIKES_OF_COMMENT_116(jObject);
        }

        public void Process_MULTIPLE_IMAGE_POST_117(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_MULTIPLE_IMAGE_POST_117(jObject);
        }

        public void Process_MUTUAL_FRIENDS_118(Newtonsoft.Json.Linq.JObject jObject)
        {
            contactListHandler.Process_MUTUAL_FRIENDS_118(jObject);
        }

        public void Process_ADD_FRIEND_127(Newtonsoft.Json.Linq.JObject jObject, long server_packet_id)
        {
            contactListHandler.Process_ADD_FRIEND_127(jObject, server_packet_id);
        }

        public void Process_DELETE_FRIEND_128(Newtonsoft.Json.Linq.JObject jObject, int action, long server_packet_id)
        {
            contactListHandler.Process_DELETE_FRIEND_128(jObject, action, server_packet_id);
        }

        public void Process_ACCEPT_FRIEND_129(Newtonsoft.Json.Linq.JObject jObject, long server_packet_id)
        {
            contactListHandler.Process_ACCEPT_FRIEND_129(jObject, server_packet_id);
        }

        public void Process_START_GROUP_CHAT_134(Newtonsoft.Json.Linq.JObject jObject, int action, long server_packet_id, string client_packet_id)
        {
            callChatAuthSignalHandler.Process_START_GROUP_CHAT_134(jObject, action, server_packet_id, client_packet_id);
        }

        public void Process_MORE_FEED_IMAGES_139(JObject jObject)
        {
            imageSignalHandler.Process_MORE_FEED_IMAGES_139(jObject);
        }

        public void Process_DELETE_CIRCLE_152(Newtonsoft.Json.Linq.JObject jObject)
        {
            circleSignalHandler.Process_DELETE_CIRCLE_152(jObject);
        }

        public void Process_REMOVE_CIRCLE_MEMBER_154(Newtonsoft.Json.Linq.JObject jObject)
        {
            circleSignalHandler.Process_REMOVE_CIRCLE_MEMBER_154(jObject);
        }

        public void Process_START_FRIEND_CHAT_175(Newtonsoft.Json.Linq.JObject jObject, int action, long server_packet_id, string client_packet_id)
        {
            callChatAuthSignalHandler.Process_START_FRIEND_CHAT_175(jObject, action, server_packet_id, client_packet_id);
        }

        public void Process_ADD_STATUS_177(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_ADD_STATUS_177(jObject);
        }

        //public void Process_ADD_IMAGE_COMMENT_180(Newtonsoft.Json.Linq.JObject jObject)
        //{
        //    imageSignalHandler.Process_ADD_IMAGE_COMMENT_180(jObject);
        //}

        //public void Process_LIKE_STATUS_184(Newtonsoft.Json.Linq.JObject jObject)
        //{
        //    feedSignalHandler.Process_LIKE_STATUS_184(jObject);
        //}

        //public void Process_UNLIKE_STATUS_186(Newtonsoft.Json.Linq.JObject jObject)
        //{
        //    feedSignalHandler.Process_UNLIKE_STATUS_186(jObject);
        //}

        //public void Process_EDIT_IMAGE_COMMENT_194(Newtonsoft.Json.Linq.JObject jObject)
        //{
        //    imageSignalHandler.Process_EDIT_IMAGE_COMMENT_194(jObject);
        //}

        public void Process_IMAGE_COMMENT_LIKES_196(Newtonsoft.Json.Linq.JObject jObject)
        {
            imageSignalHandler.Process_IMAGE_COMMENT_LIKES_196(jObject);
        }

        public void Process_LIKE_UNLIKE_IMAGE_COMMENT_197(Newtonsoft.Json.Linq.JObject jObject)
        {
            imageSignalHandler.Process_LIKE_UNLIKE_IMAGE_COMMENT_197(jObject);
        }

        public void Process_CIRCLE_NEWSFEED_198(Newtonsoft.Json.Linq.JObject jObject, string client_packet_id)
        {
            circleSignalHandler.Process_CIRCLE_NEWSFEED_198(jObject, client_packet_id);
        }

        public void Process_SINGLE_FRIEND_PRESENCE_INFO_199(Newtonsoft.Json.Linq.JObject jObject)
        {
            profileSignalHandler.Process_SINGLE_FRIEND_PRESENCE_INFO_199(jObject);
        }

        public void Process_UPDATE_DIGITS_VERIFY_209(Newtonsoft.Json.Linq.JObject jObject)
        {
            signInSignUpSignOutHandler.Process_UPDATE_DIGITS_VERIFY_209(jObject);
        }

        public void Process_FRIEND_CONTACT_LIST_211(Newtonsoft.Json.Linq.JObject jObject)
        {
            contactListHandler.Process_FRIEND_CONTACT_LIST_211(jObject);
        }

        public void Process_MISS_CALL_LIST_224(Newtonsoft.Json.Linq.JObject jObject)
        {
            contactListHandler.Process_MISS_CALL_LIST_224(jObject);
        }

        public void Process_ADD_WORK_227(Newtonsoft.Json.Linq.JObject jObject)
        {
            profileSignalHandler.Process_ADD_WORK_227(jObject);
        }

        public void Process_ADD_EDUCATION_231(Newtonsoft.Json.Linq.JObject jObject)
        {
            profileSignalHandler.Process_ADD_EDUCATION_231(jObject);
        }

        public void Process_LIST_WORK_234(Newtonsoft.Json.Linq.JObject jObject)
        {
            profileSignalHandler.Process_LIST_WORK_234(jObject);
        }

        public void Process_LIST_EDUCATION_235(Newtonsoft.Json.Linq.JObject jObject)
        {
            profileSignalHandler.Process_LIST_EDUCATION_235(jObject);
        }

        public void Process_LIST_SKILL_236(Newtonsoft.Json.Linq.JObject jObject)
        {
            profileSignalHandler.Process_LIST_SKILL_236(jObject);
        }

        public void Process_ADD_SKILL_237(Newtonsoft.Json.Linq.JObject jObject)
        {
            profileSignalHandler.Process_ADD_SKILL_237(jObject);
        }

        public void Process_WHO_SHARES_LIST_249(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_WHO_SHARES_LIST_249(jObject);
        }

        public void Process_MEDIA_SHARE_LIST_250(Newtonsoft.Json.Linq.JObject jObject)
        {
            mediaSignalHandler.Process_MEDIA_SHARE_LIST_250(jObject);
        }

        public void Process_MEDIA_ALBUM_LIST_256(Newtonsoft.Json.Linq.JObject jObject)
        {
            mediaSignalHandler.Process_MEDIA_ALBUM_LIST_256(jObject);
        }

        public void Process_MEDIA_ALBUM_CONTENT_LIST_261(Newtonsoft.Json.Linq.JObject jObject)
        {
            mediaSignalHandler.Process_MEDIA_ALBUM_CONTENT_LIST_261(jObject);
        }

        public void Process_MEDIA_LIKE_LIST_269(Newtonsoft.Json.Linq.JObject jObject)
        {
            mediaSignalHandler.Process_MEDIA_LIKE_LIST_269(jObject);
        }

        public void Process_MEDIA_COMMENT_LIST_270(Newtonsoft.Json.Linq.JObject jObject)
        {
            mediaSignalHandler.Process_MEDIA_COMMENT_LIST_270(jObject);
        }

        public void Process_MEDIACOMMENT_LIKE_LIST_271(Newtonsoft.Json.Linq.JObject jObject)
        {
            mediaSignalHandler.Process_MEDIACOMMENT_LIKE_LIST_271(jObject);
        }

        public void Process_VIEW_DOING_LIST_273(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_VIEW_DOING_LIST_273(jObject);
        }

        public void Process_VIEW_TAGGED_LIST_274(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_VIEW_TAGGED_LIST_274(jObject);
        }

        public void Process_MEDIA_SUGGESTION_277(Newtonsoft.Json.Linq.JObject jObject)
        {
            mediaSignalHandler.Process_MEDIA_SUGGESTION_277(jObject);
        }

        public void Process_MEDIA_CONTENTS_BASED_ON_KEYWORD_278(Newtonsoft.Json.Linq.JObject jObject)
        {
            mediaSignalHandler.Process_MEDIA_CONTENTS_BASED_ON_KEYWORD_278(jObject);
        }

        public void Process_HASHTAG_MEDIA_CONTENTS_279(Newtonsoft.Json.Linq.JObject jObject)
        {
            mediaSignalHandler.Process_HASHTAG_MEDIA_CONTENTS_279(jObject);
        }

        public void Process_HASHTAG_SUGGESTION_280(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_HASHTAG_SUGGESTION_280(jObject);
        }

        public void Process_SEARCH_TRENDS_281(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_SEARCH_TRENDS_281(jObject);
        }

        public void Process_STORE_CONTACT_LIST_284(Newtonsoft.Json.Linq.JObject jObject)
        {
            contactListHandler.Process_STORE_CONTACT_LIST_284(jObject);
        }

        public void Process_CELEBRITIES_CATEGORIES_LIST_285(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_CELEBRITIES_CATEGORIES_LIST_285(jObject);
        }

        public void Process_CELEBRITY_DISCOVER_LIST_286(JObject jObject)
        {
            //feedSignalHandler.Process_TYPE_CELEBRITY_FEED_286(jObject);
            celebritySignalHandler.Process_TYPE_CELEBRITY_FEED_286(jObject);
        }

        public void Process_TYPE_CELEBRITY_FEED_288(JObject jObject, string client_packet_id)
        {
            //feedSignalHandler.Process_TYPE_CELEBRITY_FEED_288(jObject, client_packet_id);
            celebritySignalHandler.Process_TYPE_CELEBRITY_FEED_288(jObject, client_packet_id);
        }

        public void Process_NEWSPORTAL_CATEGORIES_LIST_294(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_NEWSPORTAL_CATEGORIES_LIST_294(jObject);
        }

        public void Process_NEWSPORTAL_FEED_295(Newtonsoft.Json.Linq.JObject jObject, string client_packet_id)
        {
            //feedSignalHandler.Process_NEWSPORTAL_FEED_295(jObject, client_packet_id);
            newsPortalSignalHandler.Process_NEWSPORTAL_FEED_295(jObject, client_packet_id);
        }

        public void Process_NEWSPORTAL_LIST_299(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_NEWSPORTAL_LIST_299(jObject);
        }

        public void Process_NEWSPORTAL_BREAKING_FEED_302(Newtonsoft.Json.Linq.JObject jObject)
        {
            //feedSignalHandler.Process_NEWSPORTAL_BREAKING_FEED_302(jObject);
            newsPortalSignalHandler.Process_NEWSPORTAL_BREAKING_FEED_302(jObject);
        }
        public void Process_BUSINESSPAGE_BREAKING_FEED_303(Newtonsoft.Json.Linq.JObject jObject)
        {
            //feedSignalHandler.Process_BUSINESSPAGE_BREAKING_FEED_303(jObject);
            businessPageSignalHandler.Process_BUSINESSPAGE_BREAKING_FEED_303(jObject);
        }
        public void Process_BUSINESS_PAGE_FEED_306(Newtonsoft.Json.Linq.JObject jObject, string client_packet_id)
        {
            //feedSignalHandler.Process_BUSINESS_PAGE_FEED_306(jObject, client_packet_id);
            businessPageSignalHandler.Process_BUSINESS_PAGE_FEED_306(jObject, client_packet_id);
        }
        //public void Process_MEDIA_PAGE_FEED_307(JObject jObject, string client_packet_id)
        //{
        //    //feedSignalHandler.Process_MEDIA_PAGE_FEED_307(jObject, client_packet_id);
        //    mediaPageSignalHandler.Process_MEDIA_PAGE_FEED_307(jObject, client_packet_id);
        //}

        public void Process_MEDIA_PAGE_TRENDING_FEED_308(JObject jObject)
        {
            //feedSignalHandler.Process_MEDIA_PAGE_TRENDING_FEED_308(jObject);
            mediaSignalHandler.Process_MEDIA_PAGE_TRENDING_FEED_308(jObject);
        }

        public void Process_ALL_SAVED_FEEDS_309(JObject jObject, string client_packet_id)
        {
            feedSignalHandler.Process_ALL_SAVED_FEEDS_309(jObject, client_packet_id);
        }

        public void Process_ACTION_UPDATE_LIKE_UNLIKE_COMMENT_1323(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_ACTION_UPDATE_LIKE_UNLIKE_COMMENT_1323(jObject);
        }

        public void Process_UPDATE_LIKE_COMMENT_323(Newtonsoft.Json.Linq.JObject jObject)
        {
            //feedSignalHandler.Process_UPDATE_LIKE_COMMENT_323(jObject);
            feedSignalHandler.HandleLikeUnlikeUpdateComment(jObject, 1);
        }

        public void Process_UPDATE_UNLIKE_COMMENT_325(Newtonsoft.Json.Linq.JObject jObject)
        {
            //feedSignalHandler.Process_UPDATE_UNLIKE_COMMENT_325(jObject);
            feedSignalHandler.HandleLikeUnlikeUpdateComment(jObject, 0);
        }

        public void Process_UPDATE_ADD_FRIEND_327(Newtonsoft.Json.Linq.JObject jObject)
        {
            contactListHandler.Process_UPDATE_ADD_FRIEND_327(jObject);
        }

        public void Process_UPDATE_DELETE_FRIEND_328(Newtonsoft.Json.Linq.JObject jObject, int action, long server_packet_id)
        {
            contactListHandler.Process_UPDATE_DELETE_FRIEND_328(jObject, action, server_packet_id);
        }

        public void Process_UPDATE_ACCEPT_FRIEND_329(Newtonsoft.Json.Linq.JObject jObject)
        {
            contactListHandler.Process_UPDATE_ACCEPT_FRIEND_329(jObject);
        }

        public void Process_UPDATE_START_GROUP_CHAT_334(Newtonsoft.Json.Linq.JObject jObject, int action, long server_packet_id, string client_packet_id)
        {
            callChatAuthSignalHandler.Process_UPDATE_START_GROUP_CHAT_334(jObject, action, server_packet_id, client_packet_id);
        }

        public void Process_UPDATE_ADD_GROUP_MEMBER_335(Newtonsoft.Json.Linq.JObject jObject, int action, long server_packet_id, string client_packet_id)
        {
            callChatAuthSignalHandler.Process_UPDATE_ADD_GROUP_MEMBER_335(jObject, action, server_packet_id, client_packet_id);
        }

        public void Process_UPDATE_MULTIPLE_FRIEND_PRESENCE_INFO_336(Newtonsoft.Json.Linq.JObject jObject)
        {
            profileSignalHandler.Process_UPDATE_MULTIPLE_FRIEND_PRESENCE_INFO_336(jObject);
        }

        //public void Process_UPDATE_DELETE_CIRCLET_352(Newtonsoft.Json.Linq.JObject jObject)
        //{
        //    circleSignalHandler.Process_UPDATE_DELETE_CIRCLET_352(jObject);
        //}

        //public void Process_UPDATE_REMOVE_CIRCLE_MEMBER_354(Newtonsoft.Json.Linq.JObject jObject)
        //{
        //    circleSignalHandler.Process_UPDATE_REMOVE_CIRCLE_MEMBER_354(jObject);
        //}

        //public void Process_UPDATE_ADD_CIRCLE_MEMBER_356(Newtonsoft.Json.Linq.JObject jObject)
        //{
        //    circleSignalHandler.Process_UPDATE_ADD_CIRCLE_MEMBER_356(jObject);
        //}

        //public void Process_UPDATE_EDIT_CIRCLE_MEMBER_358(Newtonsoft.Json.Linq.JObject jObject)
        //{
        //    circleSignalHandler.Process_UPDATE_EDIT_CIRCLE_MEMBER_358(jObject);
        //}

        public void Process_UPDATE_SEND_REGISTER_374(Newtonsoft.Json.Linq.JObject jObject)
        {
            callChatAuthSignalHandler.Process_UPDATE_SEND_REGISTER_374(jObject);
        }

        public void Process_UPDATE_START_FRIEND_CHAT_375(Newtonsoft.Json.Linq.JObject jObject, int action, long server_packet_id, string client_packet_id)
        {
            callChatAuthSignalHandler.Process_UPDATE_START_FRIEND_CHAT_375(jObject, action, server_packet_id, client_packet_id);
        }

        public void Process_UPDATE_ADD_STATUS_377(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_ADD_STATUS_377(jObject);
        }

        public void Process_UPDATE_EDIT_STATUS_378(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_EDIT_STATUS_378(jObject);
        }

        public void Process_UPDATE_DELETE_STATUS_379(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_DELETE_STATUS_379(jObject);
        }

        public void Process_UPDATE_ADD_IMAGE_COMMENT_380(Newtonsoft.Json.Linq.JObject jObject)
        {
            imageSignalHandler.Process_UPDATE_ADD_IMAGE_COMMENT_380(jObject);
        }

        public void Process_UPDATE_ADD_STATUS_COMMENT_381(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_ADD_STATUS_COMMENT_381(jObject);
        }

        public void Process_ACTION_MERGED_UPDATE_ADD_COMMENT_1381(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_ACTION_MERGED_UPDATE_ADD_COMMENT_1381(jObject);
        }

        public void Process_UPDATE_DELETE_IMAGE_COMMENT_382(Newtonsoft.Json.Linq.JObject jObject)
        {
            imageSignalHandler.Process_UPDATE_DELETE_IMAGE_COMMENT_382(jObject);
        }

        public void Process_ACTION_MERGED_UPDATE_DELETE_COMMENT_1383(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_ACTION_MERGED_UPDATE_DELETE_COMMENT_1383(jObject);
        }

        public void Process_UPDATE_DELETE_STATUS_COMMENT_383(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_DELETE_STATUS_COMMENT_383(jObject);
        }

        public void Process_UPDATE_LIKE_STATUS_384(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_LIKE_STATUS_384(jObject);
        }

        public void Process_MERGED_UPDATE_LIKE_UNLIKE_1384(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_MERGED_UPDATE_LIKE_UNLIKE_1384(jObject);
        }

        public void Process_UPDATE_LIKE_UNLIKE_IMAGE_385(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.HandleLikeUnlikeUpdateFeed(jObject);
        }

        public void Process_UPDATE_UNLIKE_STATUS_386(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_UNLIKE_STATUS_386(jObject);
        }

        public void Process_UPDATE_EDIT_STATUS_COMMENT_389(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_EDIT_STATUS_COMMENT_389(jObject);
        }

        public void Process_UPDATE_SHARE_STATUS_391(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_SHARE_STATUS_391(jObject);
        }

        public void Process_UPDATE_EDIT_IMAGE_COMMENT_394(Newtonsoft.Json.Linq.JObject jObject)
        {
            imageSignalHandler.Process_UPDATE_EDIT_IMAGE_COMMENT_394(jObject);
        }

        public void Process_UPDATE_LIKE_UNLIKE_IMAGE_COMMENT_397(Newtonsoft.Json.Linq.JObject jObject)
        {
            imageSignalHandler.Process_UPDATE_LIKE_UNLIKE_IMAGE_COMMENT_397(jObject);
        }

        public void Process_UPDATE_LIKEUNLIKE_MEDIA_464(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.HandleLikeUnlikeUpdateFeed(jObject);
        }

        public void Process_UPDATE_COMMENT_MEDIA_465(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_COMMENT_MEDIA_465(jObject);
        }

        public void Process_UPDATE_EDITCOMMENT_MEDIA_466(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_EDITCOMMENT_MEDIA_466(jObject);
        }

        public void Process_UPDATE_DELETECOMMENT_MEDIA_467(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_DELETECOMMENT_MEDIA_467(jObject);
        }

        //public void Process_UPDATE_LIKEUNLIKECOMMENT_MEDIA_468(Newtonsoft.Json.Linq.JObject jObject)
        //{
        //    feedSignalHandler.Process_UPDATE_LIKEUNLIKECOMMENT_MEDIA_468(jObject);
        //}

        public void Process_UPDATE_STORE_CONTACT_LIST_484(Newtonsoft.Json.Linq.JObject jObject)
        {
            contactListHandler.Process_UPDATE_STORE_CONTACT_LIST_484(jObject);
        }

        public void Process_SPAM_REASON_LIST_1001(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_SPAM_REASON_LIST_1001(jObject);
        }
        public void Process_NEWSFEED_EDIT_HISTORY_LIST_1016(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_NEWSFEED_EDIT_HISTORY_LIST_1016(jObject);
        }
        public void Process_COMMENT_EDIT_HISTORY_LIST_1021(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_COMMENT_EDIT_HISTORY_LIST_1021(jObject);
        }
        public void Process_TYPE_GET_WALLET_INFORMATION_1026(JObject jObject)
        {
            walletSignalHandler.Process_TYPE_GET_WALLET_INFORMATION_1026(jObject);
        }

        public void Process_TYPE_GET_COIN_EXCHANGE_RATE_INFORMATION_1027(JObject jObject)
        {
            walletSignalHandler.Process_TYPE_GET_COIN_EXCHANGE_RATE_INFORMATION_1027(jObject);
        }

        public void PROCESS_TYPE_UPDATE_REFERRAL_REQUEST_1037(JObject jObject)
        {
            walletSignalHandler.PROCESS_TYPE_UPDATE_REFERRAL_REQUEST_1037(jObject);
        }

        //public void PROCESS_TYPE_UPDATE_ACCEPT_OR_DECLINE_REFERRAL_REQUEST_1039(JObject jObject)
        //{
        //    walletSignalHandler.PROCESS_TYPE_UPDATE_ACCEPT_OR_DECLINE_REFERRAL_REQUEST_1039(jObject);
        //}

        #endregion

        #region "Utility methods"
        public void LoadUserBasicInfoDto(UserBasicInfoModel model, UserBasicInfoDTO userDTO)
        {
            if (model != null)
            {
                model.LoadData(userDTO);
                model.ShortInfoModel.OnPropertyChanged("FriendShipStatus");
                model.OnPropertyChanged("Presence");
                model.OnPropertyChanged("CallAccess");
                model.OnPropertyChanged("ChatAccess");
                model.OnPropertyChanged("FeedAccess");
                model.OnPropertyChanged("CurrentInstance");
                model.ShortInfoModel.OnPropertyChanged("CurrentInstance");
            }

        }

        public void SessionInvalidAppRestart()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    //SignoutViewLoader.AppRestartAndShowSessionInvalidWarning();
                    if (UCGuiRingID.Instance != null)
                        UIHelperMethods.ShowSignoutLoader(false, UCGuiRingID.Instance.motherGridToShowPopup, StartUpConstatns.ARGUMENT_SESSIONINVALID);
                }
                catch (Exception e)
                {
                    log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            });
        }

        public void AppRestart()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    //if (UCGuiRingID.Instance != null)
                    //    SignoutViewLoader.AppRestart();
                    if (UCGuiRingID.Instance != null)
                        UIHelperMethods.ShowSignoutLoader(false, UCGuiRingID.Instance.motherGridToShowPopup, null);
                }
                catch (Exception e)
                {
                    log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            });
        }

        public void StopLoaderAnimationInProfile(int action, JObject pakToSend)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (action == AppConstants.TYPE_FRIEND_CONTACT_LIST_V_141)//if (action == AppConstants.TYPE_FRIEND_CONTACT_LIST)
                    contactListHandler.NoContactFoundInFriendsContactList((long)pakToSend[JsonKeys.UserTableID]);
                else if (action == AppConstants.TYPE_MUTUAL_FRIENDS)
                    contactListHandler.NoMutualFriendFoundInFriendsContactList((long)pakToSend[JsonKeys.FutId]);
                else if(action == AppConstants.TYPE_MY_ALBUMS)
                {
                    long utid = 0;
                    if (pakToSend[JsonKeys.FutId] != null) utid = (long)pakToSend[JsonKeys.FutId];
                    imageSignalHandler.NoAlbumFoundInPhotos(utid);
                }
                else if (action == AppConstants.TYPE_MY_ALBUM_IMAGES)
                {
                    string albumID = (string)pakToSend[JsonKeys.AlbumId];
                    long utid = 0;
                    if (pakToSend[JsonKeys.FutId] != null) utid = (long)pakToSend[JsonKeys.FutId];

                    if (utid == 0)
                    {
                        imageSignalHandler.NoImageFoundInMyAlbum(albumID);
                    }
                    else if (utid > 0 && !String.IsNullOrEmpty(albumID))
                    {
                        imageSignalHandler.NoImageFoundInFriendsAlbum(utid, albumID);
                    }
                    else
                    {
                        imageSignalHandler.NoImageFoundInCelibrityAlbum(utid);
                    }
                }
            });
        }
        #endregion


        public void Process_TYPE_GET_TRANSACTION_HISTORY_1031(JObject jObject)
        {
            walletSignalHandler.Process_TYPE_GET_TRANSACTION_HISTORY_1031(jObject);
        }

        public void PROCESS_TYPE_LIVE_STREAM_REQUEST_2001(JObject _JobjFromResponse)
        {
            callChatAuthSignalHandler.PROCESS_TYPE_START_LIVE_STREAM_REQUEST_2001(_JobjFromResponse);
        }

        public void PROCESS_TYPE_GET_FEATURED_LIVE_STREAMS_REQUEST_2004(JObject _JobjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_TYPE_GET_FEATURED_LIVE_STREAMS_REQUEST_2004(_JobjFromResponse);
        }

        public void PROCESS_TYPE_GET_RECENT_STREAMS_REQUEST_2005(JObject _JobjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_TYPE_GET_RECENT_STREAMS_REQUEST_2005(_JobjFromResponse);
        }

        public void PROCESS_UPDATE_LIVE_STREAM_2006(JObject _JobjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_UPDATE_LIVE_STREAM_2006(_JobjFromResponse);
        }

        public void PROCESS_TYPE_GET_STREAM_CATEGORY_LIST_REQUEST_2007(JObject _JobjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_TYPE_GET_STREAM_CATEGORY_LIST_REQUEST_2007(_JobjFromResponse);
        }

        public void PROCESS_TYPE_GET_MOST_VIEWED_STREAMS_2008(JObject _JobjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_TYPE_GET_MOST_VIEWED_STREAMS_2008(_JobjFromResponse);
        }

        public void PROCESS_TYPE_SEARCH_LIVE_STREAMS_REQUEST_2010(JObject _JobjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_TYPE_SEARCH_LIVE_STREAMS_REQUEST_2010(_JobjFromResponse);
        }

        public void PROCESS_UPDATE_STREAMING_LIKE_COUNT_2012(JObject _JobjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_UPDATE_STREAMING_LIKE_COUNT_2012(_JobjFromResponse);
        }

        public void PROCESS_GET_FOLLOWING_STREAMS_REQUEST_2013(JObject _JobjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_GET_FOLLOWING_STREAMS_REQUEST_2013(_JobjFromResponse);
        }

        public void PROCESS_GET_CATEGORY_WISE_STREAM_COUNT_REQUEST_2014(JObject _JobjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_GET_CATEGORY_WISE_STREAM_COUNT_REQUEST_2014(_JobjFromResponse);
        }

        public void PROCESS_TYPE_GET_NEAREST_STREAMS_2009(JObject _JobjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_TYPE_GET_NEAREST_STREAMS_2009(_JobjFromResponse);
        }

        public void PROCESS_TYPE_GET_WALLET_GIFT_1039(JObject jObject)
        {
            walletSignalHandler.PROCESS_TYPE_GET_WALLET_GIFT_1039(jObject);
        }

        public void PROCESS_TYPE_GET_WALLET_COIN_BUNDLE_LIST_1038(JObject jObject)
        {
            walletSignalHandler.PROCESS_TYPE_GET_WALLET_COIN_BUNDLE_LIST_1038(jObject);
        }

        public void PROCESS_TYPE_PAYMENT_RECEIVED_1046(JObject jObject)
        {
            walletSignalHandler.PROCESS_TYPE_PAYMENT_RECEIVED_1046(jObject);
        }

        public void PROCESS_TYPE_GET_COIN_EARNING_RULE_LIST_1045(JObject jObject)
        {
            walletSignalHandler.PROCESS_TYPE_GET_COIN_EARNING_RULE_LIST_1045(jObject);
        }

        public void PROCESS_TYPE_REFERRAL_NETWORK_SUMMARY_1041(JObject _jObjFromResponse)
        {
            walletSignalHandler.PROCESS_TYPE_REFERRAL_NETWORK_SUMMARY_1041(_jObjFromResponse);
        }

        public void PROCESS_TYPE_MY_REFERRAL_LIST_1042(JObject _jObjFromResponse)
        {
            walletSignalHandler.PROCESS_TYPE_MY_REFERRAL_LIST_1042(_jObjFromResponse);
        }


        public void PROCESS_TYPE_GET_DAILY_CHECKIN_HISTORY_1054(JObject _jObjFromResponse)
        {
            walletSignalHandler.PROCESS_TYPE_GET_DAILY_CHECKIN_HISTORY_1054(_jObjFromResponse);
        }


        public void PROCESS_TYPE_GIFT_RECEIVED_1052(JObject _jObjFromResponse)
        {
            walletSignalHandler.PROCESS_TYPE_GIFT_RECEIVED_1052(_jObjFromResponse);
        }

        public void PROCESS_UPDATE_FOLLOW_UNFOLLOW_USER(JObject _jObjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_UPDATE_FOLLOW_UNFOLLOW_USER(_jObjFromResponse);
        }

        public void PROCESS_CREATE_CHANNEL_REQUEST_2015(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_CREATE_CHANNEL_REQUEST_2015(_JobjFromResponse);
        }

        public void PROCESS_GET_CHANNEL_CATEGORY_LIST_REQUEST_2016(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_GET_CHANNEL_CATEGORY_LIST_REQUEST_2016(_JobjFromResponse);
        }

        public void PROCESS_UPDATE_CHANNEL_INFO_REQUEST_2017(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_UPDATE_CHANNEL_INFO_REQUEST_2017(_JobjFromResponse);
        }

        public void PROCESS_GET_FOLLOWING_CHANNEL_LIST_REQUEST_2019(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_GET_FOLLOWING_CHANNEL_LIST_REQUEST_2019(_JobjFromResponse);
        }

        public void PROCESS_GET_MOST_VIEWED_CHANNEL_LIST_REQUEST_2020(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_GET_MOST_VIEWED_CHANNEL_LIST_REQUEST_2020(_JobjFromResponse);
        }

        public void PROCESS_GET_OWN_CHANNEL_LIST_REQUEST_2021(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_GET_OWN_CHANNEL_LIST_REQUEST_2021(_JobjFromResponse);
        }

        public void PROCESS_ADD_CHANNEL_PROFILE_IMAGE_REQUEST_2024(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_ADD_CHANNEL_PROFILE_IMAGE_REQUEST_2024(_JobjFromResponse);
        }

        public void PROCESS_ADD_CHANNEL_COVER_IMAGE_REQUEST_2025(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_ADD_CHANNEL_COVER_IMAGE_REQUEST_2025(_JobjFromResponse);
        }

        public void PROCESS_ADD_MEDIA_TO_CHANNEL_REQUEST_2026(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_ADD_MEDIA_TO_CHANNEL_REQUEST_2026(_JobjFromResponse);
        }

        public void PROCESS_GET_CHANNEL_MEDIA_LIST_REQUEST_2028(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_GET_CHANNEL_MEDIA_LIST_REQUEST_2028(_JobjFromResponse);
        }

        public void PROCESS_UPDATE_CHANNEL_MEDIA_STATUS_2029(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_UPDATE_CHANNEL_MEDIA_STATUS_2029(_JobjFromResponse);
        }

        public void PROCESS_GET_FEATURED_CHANNEL_LIST_REQUEST_2030(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_GET_FEATURED_CHANNEL_LIST_REQUEST_2030(_JobjFromResponse);
        }

        public void PROCESS_UPDATE_CHANNEL_MEDIA_INFO_REQUEST_2031(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_UPDATE_CHANNEL_MEDIA_INFO_REQUEST_2031(_JobjFromResponse);
        }

        public void PROCESS_SEARCH_CHANNEL_LIST_REQUEST_2032(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_SEARCH_CHANNEL_LIST_REQUEST_2032(_JobjFromResponse);
        }

        public void PROCESS_GET_CHANNEL_PLAYLIST_REQUEST_2033(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_GET_CHANNEL_PLAYLIST_REQUEST_2033(_JobjFromResponse);
        }
    }
}
=======
﻿using System;
using System.Windows;
using Auth.AppInterfaces;
using log4net;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.UI;
using View.UI.PopUp;
using View.Utility.FriendList;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.Utility.Auth
{
    public class AuthSignalHandler : IAuthSignalHandler
    {
        #region "Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(ImageSignalHandler).Name);
        public CallChatAuthSignalHandler callChatAuthSignalHandler;
        public CircleSignalHandler circleSignalHandler;
        public ContactListHandler contactListHandler;
        public FeedSignalHandler feedSignalHandler;
        public ImageSignalHandler imageSignalHandler;
        public NotificationSignalHandler notificationSignalHandler;
        public ProfileSignalHandler profileSignalHandler;
        public SignInSignUpSignOutHandler signInSignUpSignOutHandler;
        public WalletSignalHandler walletSignalHandler;
        public NewsPortalSignalHandler newsPortalSignalHandler;
        public BusinessPageSignalHandler businessPageSignalHandler;
        public MediaSignalHandler mediaSignalHandler;
        public CelebritySignalHandler celebritySignalHandler;
        public StreamSignalHandler liveStreamSignalHandler;
        public ChannelSignalHandler channelSignalHandler;

        #endregion "Fields"

        #region "Constructor"
        public AuthSignalHandler()
        {
            callChatAuthSignalHandler = new CallChatAuthSignalHandler();
            circleSignalHandler = new CircleSignalHandler();
            contactListHandler = new ContactListHandler();
            feedSignalHandler = new FeedSignalHandler();
            imageSignalHandler = new ImageSignalHandler();
            notificationSignalHandler = new NotificationSignalHandler();
            profileSignalHandler = new ProfileSignalHandler();
            signInSignUpSignOutHandler = new SignInSignUpSignOutHandler();
            walletSignalHandler = new WalletSignalHandler();
            newsPortalSignalHandler = new NewsPortalSignalHandler();
            businessPageSignalHandler = new BusinessPageSignalHandler();
            mediaSignalHandler = new MediaSignalHandler();
            celebritySignalHandler = new CelebritySignalHandler();
            liveStreamSignalHandler = new StreamSignalHandler();
            channelSignalHandler = new ChannelSignalHandler();
        }
        #endregion "Constructor"

        #region IAuthSignalHandler Members

        public void Process_INVALID_LOGIN_SESSION_19(Newtonsoft.Json.Linq.JObject jObject)
        {
            signInSignUpSignOutHandler.Process_INVALID_LOGIN_SESSION_19(jObject);
        }

        public void Process_CONTACT_LIST_23(global::Auth.Parser.CommonPacketAttributes commonPacketAttributes)
        {
            contactListHandler.Process_CONTACT_LIST_23(commonPacketAttributes);
        }

        public void Process_CONTACT_UTIDS_29(global::Auth.Parser.CommonPacketAttributes commonPacketAttributes)
        {
            contactListHandler.Process_CONTACT_UTIDS_29(commonPacketAttributes);
        }

        public void Process_SUGGESTION_IDS_31(Newtonsoft.Json.Linq.JObject jObject)
        {
            contactListHandler.Process_SUGGESTION_IDS_31(jObject);
        }

        public void Process_USERS_DETAILS_32(Newtonsoft.Json.Linq.JObject jObject)
        {
            contactListHandler.Process_USERS_DETAILS_32(jObject);
        }

        public void Process_CONTACT_SEARCH_34(Newtonsoft.Json.Linq.JObject jObject, string client_packet_id)
        {
            contactListHandler.Process_CONTACT_SEARCH_34(jObject, client_packet_id);
        }

        public void Process_NEW_CIRCLE_51(Newtonsoft.Json.Linq.JObject jObject)
        {
            circleSignalHandler.Process_NEW_CIRCLE_51(jObject);
        }

        public void Process_CIRCLE_DETAILS_52(Newtonsoft.Json.Linq.JObject jObject)
        {
            circleSignalHandler.Process_CIRCLE_DETAILS_52(jObject);
        }

        public void Process_LEAVE_CIRCLE_53(Newtonsoft.Json.Linq.JObject jObject)
        {
            circleSignalHandler.Process_LEAVE_CIRCLE_53(jObject);
        }

        public void Process_CIRCLE_LIST_70(Newtonsoft.Json.Linq.JObject jObject)
        {
            circleSignalHandler.Process_CIRCLE_LIST_70(jObject);
        }

        public void Process_MULTIPLE_SESSION_WARNING_75(global::Auth.Parser.CommonPacketAttributes commonPacketAttributes)
        {
            signInSignUpSignOutHandler.Process_MULTIPLE_SESSION_WARNING_75(commonPacketAttributes);
        }

        public void Process_PRESENCE_78(Newtonsoft.Json.Linq.JObject jObject)
        {
            profileSignalHandler.Process_PRESENCE_78(jObject);
        }

        public void Process_UNWANTED_LOGIN_79(global::Auth.Parser.CommonPacketAttributes commonPacketAttributes)
        {
            signInSignUpSignOutHandler.Process_UNWANTED_LOGIN_79(commonPacketAttributes);
        }

        public void Process_COMMENTS_FOR_STATUS_84(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_COMMENTS_FOR_STATUS_84(jObject);
        }

        public void Process_MEDIA_FEED_87(Newtonsoft.Json.Linq.JObject jObject, string client_packet_id)
        {
            mediaSignalHandler.Process_MEDIA_FEED_87(jObject, client_packet_id);
        }

        public void Process_NEWS_FEED_88(Newtonsoft.Json.Linq.JObject jObject, string client_packet_id)
        {
            feedSignalHandler.Process_NEWS_FEED_88(jObject, client_packet_id);
        }

        public void Process_COMMENTS_FOR_IMAGE_89(Newtonsoft.Json.Linq.JObject jObject)
        {
            imageSignalHandler.Process_COMMENTS_FOR_IMAGE_89(jObject);
        }

        public void Process_LIKES_FOR_STATUS_92(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_LIKES_FOR_STATUS_92(jObject);
        }

        public void Process_ACTION_MERGED_LIKES_LIST_OF_COMMENT_1116(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_ACTION_MERGED_LIKES_LIST_OF_COMMENT_1116(jObject);
        }

        public void Process_ACTION_MERGED_COMMENTS_LIST_1084(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_ACTION_MERGED_COMMENTS_LIST_1084(jObject);
        }

        public void Process_LIKES_FOR_IMAGE_93(Newtonsoft.Json.Linq.JObject jObject)
        {
            imageSignalHandler.Process_LIKES_FOR_IMAGE_93(jObject);
        }

        public void Process_MY_BOOK_94(Newtonsoft.Json.Linq.JObject jObject, string client_packet_id)
        {
            feedSignalHandler.Process_MY_BOOK_94(jObject, client_packet_id);
        }

        public void Process_IMAGE_ALBUM_LIST_96(Newtonsoft.Json.Linq.JObject jObject)
        {
            imageSignalHandler.Process_IMAGE_ALBUM_LIST_96(jObject);
        }

        public void Process_MY_ALBUM_IMAGES_97(Newtonsoft.Json.Linq.JObject jObject)
        {
            imageSignalHandler.Process_MY_ALBUM_IMAGES_97(jObject);
        }

        public void Process_CIRCLE_MEMBERS_LIST_99(Newtonsoft.Json.Linq.JObject jObject)
        {
            circleSignalHandler.Process_CIRCLE_MEMBERS_LIST_99(jObject);
        }

        public void Process_SEARCH_CIRCLE_MEMBER_101(Newtonsoft.Json.Linq.JObject jObject)
        {
            circleSignalHandler.Process_SEARCH_CIRCLE_MEMBER_101(jObject);
        }

        public void Process_FRIEND_CONTACT_LIST_107(Newtonsoft.Json.Linq.JObject jObject)
        {
            contactListHandler.Process_FRIEND_CONTACT_LIST_107(jObject);
        }

        public void Process_FRIEND_ALBUM_IMAGES_109(Newtonsoft.Json.Linq.JObject jObject)
        {
            //imageSignalHandler.Process_FRIEND_ALBUM_IMAGES_109(jObject);
        }

        public void Process_FRIEND_NEWSFEED_110(Newtonsoft.Json.Linq.JObject jObject, string clientpacketid)
        {
            feedSignalHandler.Process_FRIEND_NEWSFEED_110(jObject, clientpacketid);
        }

        public void Process_MY_NOTIFICATIONS_111(Newtonsoft.Json.Linq.JObject jObject)
        {
            notificationSignalHandler.Process_MY_NOTIFICATIONS_111(jObject);
        }

        public void Process_SINGLE_NOTIFICATION_113(Newtonsoft.Json.Linq.JObject jObject)
        {
            notificationSignalHandler.Process_SINGLE_NOTIFICATION_113(jObject);
        }

        public void Process_SINGLE_FEED_SHARE_LIST_115(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_SINGLE_FEED_SHARE_LIST_115(jObject);
        }

        public void Process_LIST_LIKES_OF_COMMENT_116(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_LIST_LIKES_OF_COMMENT_116(jObject);
        }

        public void Process_MULTIPLE_IMAGE_POST_117(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_MULTIPLE_IMAGE_POST_117(jObject);
        }

        public void Process_MUTUAL_FRIENDS_118(Newtonsoft.Json.Linq.JObject jObject)
        {
            contactListHandler.Process_MUTUAL_FRIENDS_118(jObject);
        }

        public void Process_ADD_FRIEND_127(Newtonsoft.Json.Linq.JObject jObject, long server_packet_id)
        {
            contactListHandler.Process_ADD_FRIEND_127(jObject, server_packet_id);
        }

        public void Process_DELETE_FRIEND_128(Newtonsoft.Json.Linq.JObject jObject, int action, long server_packet_id)
        {
            contactListHandler.Process_DELETE_FRIEND_128(jObject, action, server_packet_id);
        }

        public void Process_ACCEPT_FRIEND_129(Newtonsoft.Json.Linq.JObject jObject, long server_packet_id)
        {
            contactListHandler.Process_ACCEPT_FRIEND_129(jObject, server_packet_id);
        }

        public void Process_START_GROUP_CHAT_134(Newtonsoft.Json.Linq.JObject jObject, int action, long server_packet_id, string client_packet_id)
        {
            callChatAuthSignalHandler.Process_START_GROUP_CHAT_134(jObject, action, server_packet_id, client_packet_id);
        }

        public void Process_MORE_FEED_IMAGES_139(JObject jObject)
        {
            imageSignalHandler.Process_MORE_FEED_IMAGES_139(jObject);
        }

        public void Process_DELETE_CIRCLE_152(Newtonsoft.Json.Linq.JObject jObject)
        {
            circleSignalHandler.Process_DELETE_CIRCLE_152(jObject);
        }

        public void Process_REMOVE_CIRCLE_MEMBER_154(Newtonsoft.Json.Linq.JObject jObject)
        {
            circleSignalHandler.Process_REMOVE_CIRCLE_MEMBER_154(jObject);
        }

        public void Process_START_FRIEND_CHAT_175(Newtonsoft.Json.Linq.JObject jObject, int action, long server_packet_id, string client_packet_id)
        {
            callChatAuthSignalHandler.Process_START_FRIEND_CHAT_175(jObject, action, server_packet_id, client_packet_id);
        }

        public void Process_ADD_STATUS_177(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_ADD_STATUS_177(jObject);
        }

        //public void Process_ADD_IMAGE_COMMENT_180(Newtonsoft.Json.Linq.JObject jObject)
        //{
        //    imageSignalHandler.Process_ADD_IMAGE_COMMENT_180(jObject);
        //}

        //public void Process_LIKE_STATUS_184(Newtonsoft.Json.Linq.JObject jObject)
        //{
        //    feedSignalHandler.Process_LIKE_STATUS_184(jObject);
        //}

        //public void Process_UNLIKE_STATUS_186(Newtonsoft.Json.Linq.JObject jObject)
        //{
        //    feedSignalHandler.Process_UNLIKE_STATUS_186(jObject);
        //}

        //public void Process_EDIT_IMAGE_COMMENT_194(Newtonsoft.Json.Linq.JObject jObject)
        //{
        //    imageSignalHandler.Process_EDIT_IMAGE_COMMENT_194(jObject);
        //}

        public void Process_IMAGE_COMMENT_LIKES_196(Newtonsoft.Json.Linq.JObject jObject)
        {
            imageSignalHandler.Process_IMAGE_COMMENT_LIKES_196(jObject);
        }

        public void Process_LIKE_UNLIKE_IMAGE_COMMENT_197(Newtonsoft.Json.Linq.JObject jObject)
        {
            imageSignalHandler.Process_LIKE_UNLIKE_IMAGE_COMMENT_197(jObject);
        }

        public void Process_CIRCLE_NEWSFEED_198(Newtonsoft.Json.Linq.JObject jObject, string client_packet_id)
        {
            circleSignalHandler.Process_CIRCLE_NEWSFEED_198(jObject, client_packet_id);
        }

        public void Process_SINGLE_FRIEND_PRESENCE_INFO_199(Newtonsoft.Json.Linq.JObject jObject)
        {
            profileSignalHandler.Process_SINGLE_FRIEND_PRESENCE_INFO_199(jObject);
        }

        public void Process_UPDATE_DIGITS_VERIFY_209(Newtonsoft.Json.Linq.JObject jObject)
        {
            signInSignUpSignOutHandler.Process_UPDATE_DIGITS_VERIFY_209(jObject);
        }

        public void Process_FRIEND_CONTACT_LIST_211(Newtonsoft.Json.Linq.JObject jObject)
        {
            contactListHandler.Process_FRIEND_CONTACT_LIST_211(jObject);
        }

        public void Process_MISS_CALL_LIST_224(Newtonsoft.Json.Linq.JObject jObject)
        {
            contactListHandler.Process_MISS_CALL_LIST_224(jObject);
        }

        public void Process_ADD_WORK_227(Newtonsoft.Json.Linq.JObject jObject)
        {
            profileSignalHandler.Process_ADD_WORK_227(jObject);
        }

        public void Process_ADD_EDUCATION_231(Newtonsoft.Json.Linq.JObject jObject)
        {
            profileSignalHandler.Process_ADD_EDUCATION_231(jObject);
        }

        public void Process_LIST_WORK_234(Newtonsoft.Json.Linq.JObject jObject)
        {
            profileSignalHandler.Process_LIST_WORK_234(jObject);
        }

        public void Process_LIST_EDUCATION_235(Newtonsoft.Json.Linq.JObject jObject)
        {
            profileSignalHandler.Process_LIST_EDUCATION_235(jObject);
        }

        public void Process_LIST_SKILL_236(Newtonsoft.Json.Linq.JObject jObject)
        {
            profileSignalHandler.Process_LIST_SKILL_236(jObject);
        }

        public void Process_ADD_SKILL_237(Newtonsoft.Json.Linq.JObject jObject)
        {
            profileSignalHandler.Process_ADD_SKILL_237(jObject);
        }

        public void Process_WHO_SHARES_LIST_249(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_WHO_SHARES_LIST_249(jObject);
        }

        public void Process_MEDIA_SHARE_LIST_250(Newtonsoft.Json.Linq.JObject jObject)
        {
            mediaSignalHandler.Process_MEDIA_SHARE_LIST_250(jObject);
        }

        public void Process_MEDIA_ALBUM_LIST_256(Newtonsoft.Json.Linq.JObject jObject)
        {
            mediaSignalHandler.Process_MEDIA_ALBUM_LIST_256(jObject);
        }

        public void Process_MEDIA_ALBUM_CONTENT_LIST_261(Newtonsoft.Json.Linq.JObject jObject)
        {
            mediaSignalHandler.Process_MEDIA_ALBUM_CONTENT_LIST_261(jObject);
        }

        public void Process_MEDIA_LIKE_LIST_269(Newtonsoft.Json.Linq.JObject jObject)
        {
            mediaSignalHandler.Process_MEDIA_LIKE_LIST_269(jObject);
        }

        public void Process_MEDIA_COMMENT_LIST_270(Newtonsoft.Json.Linq.JObject jObject)
        {
            mediaSignalHandler.Process_MEDIA_COMMENT_LIST_270(jObject);
        }

        public void Process_MEDIACOMMENT_LIKE_LIST_271(Newtonsoft.Json.Linq.JObject jObject)
        {
            mediaSignalHandler.Process_MEDIACOMMENT_LIKE_LIST_271(jObject);
        }

        public void Process_VIEW_DOING_LIST_273(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_VIEW_DOING_LIST_273(jObject);
        }

        public void Process_VIEW_TAGGED_LIST_274(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_VIEW_TAGGED_LIST_274(jObject);
        }

        public void Process_MEDIA_SUGGESTION_277(Newtonsoft.Json.Linq.JObject jObject)
        {
            mediaSignalHandler.Process_MEDIA_SUGGESTION_277(jObject);
        }

        public void Process_MEDIA_CONTENTS_BASED_ON_KEYWORD_278(Newtonsoft.Json.Linq.JObject jObject)
        {
            mediaSignalHandler.Process_MEDIA_CONTENTS_BASED_ON_KEYWORD_278(jObject);
        }

        public void Process_HASHTAG_MEDIA_CONTENTS_279(Newtonsoft.Json.Linq.JObject jObject)
        {
            mediaSignalHandler.Process_HASHTAG_MEDIA_CONTENTS_279(jObject);
        }

        public void Process_HASHTAG_SUGGESTION_280(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_HASHTAG_SUGGESTION_280(jObject);
        }

        public void Process_SEARCH_TRENDS_281(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_SEARCH_TRENDS_281(jObject);
        }

        public void Process_STORE_CONTACT_LIST_284(Newtonsoft.Json.Linq.JObject jObject)
        {
            contactListHandler.Process_STORE_CONTACT_LIST_284(jObject);
        }

        public void Process_CELEBRITIES_CATEGORIES_LIST_285(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_CELEBRITIES_CATEGORIES_LIST_285(jObject);
        }

        public void Process_CELEBRITY_DISCOVER_LIST_286(JObject jObject)
        {
            //feedSignalHandler.Process_TYPE_CELEBRITY_FEED_286(jObject);
            celebritySignalHandler.Process_TYPE_CELEBRITY_FEED_286(jObject);
        }

        public void Process_TYPE_CELEBRITY_FEED_288(JObject jObject, string client_packet_id)
        {
            //feedSignalHandler.Process_TYPE_CELEBRITY_FEED_288(jObject, client_packet_id);
            celebritySignalHandler.Process_TYPE_CELEBRITY_FEED_288(jObject, client_packet_id);
        }

        public void Process_NEWSPORTAL_CATEGORIES_LIST_294(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_NEWSPORTAL_CATEGORIES_LIST_294(jObject);
        }

        public void Process_NEWSPORTAL_FEED_295(Newtonsoft.Json.Linq.JObject jObject, string client_packet_id)
        {
            //feedSignalHandler.Process_NEWSPORTAL_FEED_295(jObject, client_packet_id);
            newsPortalSignalHandler.Process_NEWSPORTAL_FEED_295(jObject, client_packet_id);
        }

        public void Process_NEWSPORTAL_LIST_299(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_NEWSPORTAL_LIST_299(jObject);
        }

        public void Process_NEWSPORTAL_BREAKING_FEED_302(Newtonsoft.Json.Linq.JObject jObject)
        {
            //feedSignalHandler.Process_NEWSPORTAL_BREAKING_FEED_302(jObject);
            newsPortalSignalHandler.Process_NEWSPORTAL_BREAKING_FEED_302(jObject);
        }
        public void Process_BUSINESSPAGE_BREAKING_FEED_303(Newtonsoft.Json.Linq.JObject jObject)
        {
            //feedSignalHandler.Process_BUSINESSPAGE_BREAKING_FEED_303(jObject);
            businessPageSignalHandler.Process_BUSINESSPAGE_BREAKING_FEED_303(jObject);
        }
        public void Process_BUSINESS_PAGE_FEED_306(Newtonsoft.Json.Linq.JObject jObject, string client_packet_id)
        {
            //feedSignalHandler.Process_BUSINESS_PAGE_FEED_306(jObject, client_packet_id);
            businessPageSignalHandler.Process_BUSINESS_PAGE_FEED_306(jObject, client_packet_id);
        }
        //public void Process_MEDIA_PAGE_FEED_307(JObject jObject, string client_packet_id)
        //{
        //    //feedSignalHandler.Process_MEDIA_PAGE_FEED_307(jObject, client_packet_id);
        //    mediaPageSignalHandler.Process_MEDIA_PAGE_FEED_307(jObject, client_packet_id);
        //}

        public void Process_MEDIA_PAGE_TRENDING_FEED_308(JObject jObject)
        {
            //feedSignalHandler.Process_MEDIA_PAGE_TRENDING_FEED_308(jObject);
            mediaSignalHandler.Process_MEDIA_PAGE_TRENDING_FEED_308(jObject);
        }

        public void Process_ALL_SAVED_FEEDS_309(JObject jObject, string client_packet_id)
        {
            feedSignalHandler.Process_ALL_SAVED_FEEDS_309(jObject, client_packet_id);
        }

        public void Process_ACTION_UPDATE_LIKE_UNLIKE_COMMENT_1323(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_ACTION_UPDATE_LIKE_UNLIKE_COMMENT_1323(jObject);
        }

        public void Process_UPDATE_LIKE_COMMENT_323(Newtonsoft.Json.Linq.JObject jObject)
        {
            //feedSignalHandler.Process_UPDATE_LIKE_COMMENT_323(jObject);
            feedSignalHandler.HandleLikeUnlikeUpdateComment(jObject, 1);
        }

        public void Process_UPDATE_UNLIKE_COMMENT_325(Newtonsoft.Json.Linq.JObject jObject)
        {
            //feedSignalHandler.Process_UPDATE_UNLIKE_COMMENT_325(jObject);
            feedSignalHandler.HandleLikeUnlikeUpdateComment(jObject, 0);
        }

        public void Process_UPDATE_ADD_FRIEND_327(Newtonsoft.Json.Linq.JObject jObject)
        {
            contactListHandler.Process_UPDATE_ADD_FRIEND_327(jObject);
        }

        public void Process_UPDATE_DELETE_FRIEND_328(Newtonsoft.Json.Linq.JObject jObject, int action, long server_packet_id)
        {
            contactListHandler.Process_UPDATE_DELETE_FRIEND_328(jObject, action, server_packet_id);
        }

        public void Process_UPDATE_ACCEPT_FRIEND_329(Newtonsoft.Json.Linq.JObject jObject)
        {
            contactListHandler.Process_UPDATE_ACCEPT_FRIEND_329(jObject);
        }

        public void Process_UPDATE_START_GROUP_CHAT_334(Newtonsoft.Json.Linq.JObject jObject, int action, long server_packet_id, string client_packet_id)
        {
            callChatAuthSignalHandler.Process_UPDATE_START_GROUP_CHAT_334(jObject, action, server_packet_id, client_packet_id);
        }

        public void Process_UPDATE_ADD_GROUP_MEMBER_335(Newtonsoft.Json.Linq.JObject jObject, int action, long server_packet_id, string client_packet_id)
        {
            callChatAuthSignalHandler.Process_UPDATE_ADD_GROUP_MEMBER_335(jObject, action, server_packet_id, client_packet_id);
        }

        public void Process_UPDATE_MULTIPLE_FRIEND_PRESENCE_INFO_336(Newtonsoft.Json.Linq.JObject jObject)
        {
            profileSignalHandler.Process_UPDATE_MULTIPLE_FRIEND_PRESENCE_INFO_336(jObject);
        }

        //public void Process_UPDATE_DELETE_CIRCLET_352(Newtonsoft.Json.Linq.JObject jObject)
        //{
        //    circleSignalHandler.Process_UPDATE_DELETE_CIRCLET_352(jObject);
        //}

        //public void Process_UPDATE_REMOVE_CIRCLE_MEMBER_354(Newtonsoft.Json.Linq.JObject jObject)
        //{
        //    circleSignalHandler.Process_UPDATE_REMOVE_CIRCLE_MEMBER_354(jObject);
        //}

        //public void Process_UPDATE_ADD_CIRCLE_MEMBER_356(Newtonsoft.Json.Linq.JObject jObject)
        //{
        //    circleSignalHandler.Process_UPDATE_ADD_CIRCLE_MEMBER_356(jObject);
        //}

        //public void Process_UPDATE_EDIT_CIRCLE_MEMBER_358(Newtonsoft.Json.Linq.JObject jObject)
        //{
        //    circleSignalHandler.Process_UPDATE_EDIT_CIRCLE_MEMBER_358(jObject);
        //}

        public void Process_UPDATE_SEND_REGISTER_374(Newtonsoft.Json.Linq.JObject jObject)
        {
            callChatAuthSignalHandler.Process_UPDATE_SEND_REGISTER_374(jObject);
        }

        public void Process_UPDATE_START_FRIEND_CHAT_375(Newtonsoft.Json.Linq.JObject jObject, int action, long server_packet_id, string client_packet_id)
        {
            callChatAuthSignalHandler.Process_UPDATE_START_FRIEND_CHAT_375(jObject, action, server_packet_id, client_packet_id);
        }

        public void Process_UPDATE_ADD_STATUS_377(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_ADD_STATUS_377(jObject);
        }

        public void Process_UPDATE_EDIT_STATUS_378(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_EDIT_STATUS_378(jObject);
        }

        public void Process_UPDATE_DELETE_STATUS_379(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_DELETE_STATUS_379(jObject);
        }

        public void Process_UPDATE_ADD_IMAGE_COMMENT_380(Newtonsoft.Json.Linq.JObject jObject)
        {
            imageSignalHandler.Process_UPDATE_ADD_IMAGE_COMMENT_380(jObject);
        }

        public void Process_UPDATE_ADD_STATUS_COMMENT_381(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_ADD_STATUS_COMMENT_381(jObject);
        }

        public void Process_ACTION_MERGED_UPDATE_ADD_COMMENT_1381(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_ACTION_MERGED_UPDATE_ADD_COMMENT_1381(jObject);
        }

        public void Process_UPDATE_DELETE_IMAGE_COMMENT_382(Newtonsoft.Json.Linq.JObject jObject)
        {
            imageSignalHandler.Process_UPDATE_DELETE_IMAGE_COMMENT_382(jObject);
        }

        public void Process_ACTION_MERGED_UPDATE_DELETE_COMMENT_1383(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_ACTION_MERGED_UPDATE_DELETE_COMMENT_1383(jObject);
        }

        public void Process_UPDATE_DELETE_STATUS_COMMENT_383(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_DELETE_STATUS_COMMENT_383(jObject);
        }

        public void Process_UPDATE_LIKE_STATUS_384(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_LIKE_STATUS_384(jObject);
        }

        public void Process_MERGED_UPDATE_LIKE_UNLIKE_1384(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_MERGED_UPDATE_LIKE_UNLIKE_1384(jObject);
        }

        public void Process_UPDATE_LIKE_UNLIKE_IMAGE_385(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.HandleLikeUnlikeUpdateFeed(jObject);
        }

        public void Process_UPDATE_UNLIKE_STATUS_386(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_UNLIKE_STATUS_386(jObject);
        }

        public void Process_UPDATE_EDIT_STATUS_COMMENT_389(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_EDIT_STATUS_COMMENT_389(jObject);
        }

        public void Process_UPDATE_SHARE_STATUS_391(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_SHARE_STATUS_391(jObject);
        }

        public void Process_UPDATE_EDIT_IMAGE_COMMENT_394(Newtonsoft.Json.Linq.JObject jObject)
        {
            imageSignalHandler.Process_UPDATE_EDIT_IMAGE_COMMENT_394(jObject);
        }

        public void Process_UPDATE_LIKE_UNLIKE_IMAGE_COMMENT_397(Newtonsoft.Json.Linq.JObject jObject)
        {
            imageSignalHandler.Process_UPDATE_LIKE_UNLIKE_IMAGE_COMMENT_397(jObject);
        }

        public void Process_UPDATE_LIKEUNLIKE_MEDIA_464(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.HandleLikeUnlikeUpdateFeed(jObject);
        }

        public void Process_UPDATE_COMMENT_MEDIA_465(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_COMMENT_MEDIA_465(jObject);
        }

        public void Process_UPDATE_EDITCOMMENT_MEDIA_466(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_EDITCOMMENT_MEDIA_466(jObject);
        }

        public void Process_UPDATE_DELETECOMMENT_MEDIA_467(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_UPDATE_DELETECOMMENT_MEDIA_467(jObject);
        }

        //public void Process_UPDATE_LIKEUNLIKECOMMENT_MEDIA_468(Newtonsoft.Json.Linq.JObject jObject)
        //{
        //    feedSignalHandler.Process_UPDATE_LIKEUNLIKECOMMENT_MEDIA_468(jObject);
        //}

        public void Process_UPDATE_STORE_CONTACT_LIST_484(Newtonsoft.Json.Linq.JObject jObject)
        {
            contactListHandler.Process_UPDATE_STORE_CONTACT_LIST_484(jObject);
        }

        public void Process_SPAM_REASON_LIST_1001(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_SPAM_REASON_LIST_1001(jObject);
        }
        public void Process_NEWSFEED_EDIT_HISTORY_LIST_1016(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_NEWSFEED_EDIT_HISTORY_LIST_1016(jObject);
        }
        public void Process_COMMENT_EDIT_HISTORY_LIST_1021(Newtonsoft.Json.Linq.JObject jObject)
        {
            feedSignalHandler.Process_COMMENT_EDIT_HISTORY_LIST_1021(jObject);
        }
        public void Process_TYPE_GET_WALLET_INFORMATION_1026(JObject jObject)
        {
            walletSignalHandler.Process_TYPE_GET_WALLET_INFORMATION_1026(jObject);
        }

        public void Process_TYPE_GET_COIN_EXCHANGE_RATE_INFORMATION_1027(JObject jObject)
        {
            walletSignalHandler.Process_TYPE_GET_COIN_EXCHANGE_RATE_INFORMATION_1027(jObject);
        }

        public void PROCESS_TYPE_UPDATE_REFERRAL_REQUEST_1037(JObject jObject)
        {
            walletSignalHandler.PROCESS_TYPE_UPDATE_REFERRAL_REQUEST_1037(jObject);
        }

        //public void PROCESS_TYPE_UPDATE_ACCEPT_OR_DECLINE_REFERRAL_REQUEST_1039(JObject jObject)
        //{
        //    walletSignalHandler.PROCESS_TYPE_UPDATE_ACCEPT_OR_DECLINE_REFERRAL_REQUEST_1039(jObject);
        //}

        #endregion

        #region "Utility methods"
        public void LoadUserBasicInfoDto(UserBasicInfoModel model, UserBasicInfoDTO userDTO)
        {
            if (model != null)
            {
                model.LoadData(userDTO);
                model.ShortInfoModel.OnPropertyChanged("FriendShipStatus");
                model.OnPropertyChanged("Presence");
                model.OnPropertyChanged("CallAccess");
                model.OnPropertyChanged("ChatAccess");
                model.OnPropertyChanged("FeedAccess");
                model.OnPropertyChanged("CurrentInstance");
                model.ShortInfoModel.OnPropertyChanged("CurrentInstance");
            }

        }

        public void SessionInvalidAppRestart()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    //SignoutViewLoader.AppRestartAndShowSessionInvalidWarning();
                    if (UCGuiRingID.Instance != null)
                        UIHelperMethods.ShowSignoutLoader(false, UCGuiRingID.Instance.motherGridToShowPopup, StartUpConstatns.ARGUMENT_SESSIONINVALID);
                }
                catch (Exception e)
                {
                    log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            });
        }

        public void AppRestart()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    //if (UCGuiRingID.Instance != null)
                    //    SignoutViewLoader.AppRestart();
                    if (UCGuiRingID.Instance != null)
                        UIHelperMethods.ShowSignoutLoader(false, UCGuiRingID.Instance.motherGridToShowPopup, null);
                }
                catch (Exception e)
                {
                    log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            });
        }

        public void StopLoaderAnimationInProfile(int action, JObject pakToSend)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (action == AppConstants.TYPE_FRIEND_CONTACT_LIST_V_141)//if (action == AppConstants.TYPE_FRIEND_CONTACT_LIST)
                    contactListHandler.NoContactFoundInFriendsContactList((long)pakToSend[JsonKeys.UserTableID]);
                else if (action == AppConstants.TYPE_MUTUAL_FRIENDS)
                    contactListHandler.NoMutualFriendFoundInFriendsContactList((long)pakToSend[JsonKeys.FutId]);
                else if(action == AppConstants.TYPE_MY_ALBUMS)
                {
                    long utid = 0;
                    if (pakToSend[JsonKeys.FutId] != null) utid = (long)pakToSend[JsonKeys.FutId];
                    imageSignalHandler.NoAlbumFoundInPhotos(utid);
                }
                else if (action == AppConstants.TYPE_MY_ALBUM_IMAGES)
                {
                    string albumID = (string)pakToSend[JsonKeys.AlbumId];
                    long utid = 0;
                    if (pakToSend[JsonKeys.FutId] != null) utid = (long)pakToSend[JsonKeys.FutId];

                    if (utid == 0)
                    {
                        imageSignalHandler.NoImageFoundInMyAlbum(albumID);
                    }
                    else if (utid > 0 && !String.IsNullOrEmpty(albumID))
                    {
                        imageSignalHandler.NoImageFoundInFriendsAlbum(utid, albumID);
                    }
                    else
                    {
                        imageSignalHandler.NoImageFoundInCelibrityAlbum(utid);
                    }
                }
            });
        }
        #endregion


        public void Process_TYPE_GET_TRANSACTION_HISTORY_1031(JObject jObject)
        {
            walletSignalHandler.Process_TYPE_GET_TRANSACTION_HISTORY_1031(jObject);
        }

        public void PROCESS_TYPE_LIVE_STREAM_REQUEST_2001(JObject _JobjFromResponse)
        {
            callChatAuthSignalHandler.PROCESS_TYPE_START_LIVE_STREAM_REQUEST_2001(_JobjFromResponse);
        }

        public void PROCESS_TYPE_GET_FEATURED_LIVE_STREAMS_REQUEST_2004(JObject _JobjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_TYPE_GET_FEATURED_LIVE_STREAMS_REQUEST_2004(_JobjFromResponse);
        }

        public void PROCESS_TYPE_GET_RECENT_STREAMS_REQUEST_2005(JObject _JobjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_TYPE_GET_RECENT_STREAMS_REQUEST_2005(_JobjFromResponse);
        }

        public void PROCESS_UPDATE_LIVE_STREAM_2006(JObject _JobjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_UPDATE_LIVE_STREAM_2006(_JobjFromResponse);
        }

        public void PROCESS_TYPE_GET_STREAM_CATEGORY_LIST_REQUEST_2007(JObject _JobjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_TYPE_GET_STREAM_CATEGORY_LIST_REQUEST_2007(_JobjFromResponse);
        }

        public void PROCESS_TYPE_GET_MOST_VIEWED_STREAMS_2008(JObject _JobjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_TYPE_GET_MOST_VIEWED_STREAMS_2008(_JobjFromResponse);
        }

        public void PROCESS_TYPE_SEARCH_LIVE_STREAMS_REQUEST_2010(JObject _JobjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_TYPE_SEARCH_LIVE_STREAMS_REQUEST_2010(_JobjFromResponse);
        }

        public void PROCESS_UPDATE_STREAMING_LIKE_COUNT_2012(JObject _JobjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_UPDATE_STREAMING_LIKE_COUNT_2012(_JobjFromResponse);
        }

        public void PROCESS_GET_FOLLOWING_STREAMS_REQUEST_2013(JObject _JobjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_GET_FOLLOWING_STREAMS_REQUEST_2013(_JobjFromResponse);
        }

        public void PROCESS_GET_CATEGORY_WISE_STREAM_COUNT_REQUEST_2014(JObject _JobjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_GET_CATEGORY_WISE_STREAM_COUNT_REQUEST_2014(_JobjFromResponse);
        }

        public void PROCESS_TYPE_GET_NEAREST_STREAMS_2009(JObject _JobjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_TYPE_GET_NEAREST_STREAMS_2009(_JobjFromResponse);
        }

        public void PROCESS_TYPE_GET_WALLET_GIFT_1039(JObject jObject)
        {
            walletSignalHandler.PROCESS_TYPE_GET_WALLET_GIFT_1039(jObject);
        }

        public void PROCESS_TYPE_GET_WALLET_COIN_BUNDLE_LIST_1038(JObject jObject)
        {
            walletSignalHandler.PROCESS_TYPE_GET_WALLET_COIN_BUNDLE_LIST_1038(jObject);
        }

        public void PROCESS_TYPE_PAYMENT_RECEIVED_1046(JObject jObject)
        {
            walletSignalHandler.PROCESS_TYPE_PAYMENT_RECEIVED_1046(jObject);
        }

        public void PROCESS_TYPE_GET_COIN_EARNING_RULE_LIST_1045(JObject jObject)
        {
            walletSignalHandler.PROCESS_TYPE_GET_COIN_EARNING_RULE_LIST_1045(jObject);
        }

        public void PROCESS_TYPE_REFERRAL_NETWORK_SUMMARY_1041(JObject _jObjFromResponse)
        {
            walletSignalHandler.PROCESS_TYPE_REFERRAL_NETWORK_SUMMARY_1041(_jObjFromResponse);
        }

        public void PROCESS_TYPE_MY_REFERRAL_LIST_1042(JObject _jObjFromResponse)
        {
            walletSignalHandler.PROCESS_TYPE_MY_REFERRAL_LIST_1042(_jObjFromResponse);
        }


        public void PROCESS_TYPE_GET_DAILY_CHECKIN_HISTORY_1054(JObject _jObjFromResponse)
        {
            walletSignalHandler.PROCESS_TYPE_GET_DAILY_CHECKIN_HISTORY_1054(_jObjFromResponse);
        }


        public void PROCESS_TYPE_GIFT_RECEIVED_1052(JObject _jObjFromResponse)
        {
            walletSignalHandler.PROCESS_TYPE_GIFT_RECEIVED_1052(_jObjFromResponse);
        }

        public void PROCESS_UPDATE_FOLLOW_UNFOLLOW_USER(JObject _jObjFromResponse)
        {
            liveStreamSignalHandler.PROCESS_UPDATE_FOLLOW_UNFOLLOW_USER(_jObjFromResponse);
        }

        public void PROCESS_CREATE_CHANNEL_REQUEST_2015(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_CREATE_CHANNEL_REQUEST_2015(_JobjFromResponse);
        }

        public void PROCESS_GET_CHANNEL_CATEGORY_LIST_REQUEST_2016(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_GET_CHANNEL_CATEGORY_LIST_REQUEST_2016(_JobjFromResponse);
        }

        public void PROCESS_UPDATE_CHANNEL_INFO_REQUEST_2017(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_UPDATE_CHANNEL_INFO_REQUEST_2017(_JobjFromResponse);
        }

        public void PROCESS_GET_FOLLOWING_CHANNEL_LIST_REQUEST_2019(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_GET_FOLLOWING_CHANNEL_LIST_REQUEST_2019(_JobjFromResponse);
        }

        public void PROCESS_GET_MOST_VIEWED_CHANNEL_LIST_REQUEST_2020(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_GET_MOST_VIEWED_CHANNEL_LIST_REQUEST_2020(_JobjFromResponse);
        }

        public void PROCESS_GET_OWN_CHANNEL_LIST_REQUEST_2021(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_GET_OWN_CHANNEL_LIST_REQUEST_2021(_JobjFromResponse);
        }

        public void PROCESS_ADD_CHANNEL_PROFILE_IMAGE_REQUEST_2024(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_ADD_CHANNEL_PROFILE_IMAGE_REQUEST_2024(_JobjFromResponse);
        }

        public void PROCESS_ADD_CHANNEL_COVER_IMAGE_REQUEST_2025(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_ADD_CHANNEL_COVER_IMAGE_REQUEST_2025(_JobjFromResponse);
        }

        public void PROCESS_ADD_UPLOADED_CHANNEL_MEDIA_2026(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_ADD_UPLOADED_CHANNEL_MEDIA_2026(_JobjFromResponse);
        }

        public void PROCESS_GET_UPLOADED_CHANNEL_MEDIA_2027(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_GET_UPLOADED_CHANNEL_MEDIA_2027(_JobjFromResponse);
        }

        public void PROCESS_GET_CHANNEL_MEDIA_LIST_REQUEST_2028(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_GET_CHANNEL_MEDIA_LIST_REQUEST_2028(_JobjFromResponse);
        }

        public void PROCESS_UPDATE_CHANNEL_MEDIA_STATUS_2029(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_UPDATE_CHANNEL_MEDIA_STATUS_2029(_JobjFromResponse);
        }

        public void PROCESS_GET_FEATURED_CHANNEL_LIST_REQUEST_2030(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_GET_FEATURED_CHANNEL_LIST_REQUEST_2030(_JobjFromResponse);
        }

        public void PROCESS_UPDATE_CHANNEL_MEDIA_INFO_REQUEST_2031(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_UPDATE_CHANNEL_MEDIA_INFO_REQUEST_2031(_JobjFromResponse);
        }

        public void PROCESS_SEARCH_CHANNEL_LIST_REQUEST_2032(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_SEARCH_CHANNEL_LIST_REQUEST_2032(_JobjFromResponse);
        }

        public void PROCESS_GET_CHANNEL_PLAYLIST_REQUEST_2033(JObject _JobjFromResponse)
        {
            channelSignalHandler.PROCESS_GET_CHANNEL_PLAYLIST_REQUEST_2033(_JobjFromResponse);
        }
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

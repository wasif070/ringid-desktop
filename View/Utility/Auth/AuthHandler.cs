﻿

namespace View.Utility.Auth
{
    //    public class AuthHandler : IAuthResponse
    //    {
    //        private static readonly ILog log = LogManager.GetLogger(typeof(AuthHandler).Name);
    //        #region IAuthResponse Members

    //        public void AddFriendListFromServer(List<UserBasicInfoDTO> list)
    //        {
    //            try
    //            {
    //                FriendListLoadUtility.LoadDataFromServer(list);
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: AddFriendListFromServer() => " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }


    //        public bool CheckGuiRingInstance()
    //        {
    //            if (UCGuiRingID.Instance != null)
    //            {
    //                return true;
    //            }
    //            else
    //            {
    //                return false;
    //            }
    //        }
    //        public bool CheckGuiRingVisible()
    //        {
    //            if ( UCGuiRingID.Instance.IsVisible)
    //            {
    //                return true;
    //            }
    //            else
    //            {
    //                return false;
    //            }
    //        }

    //        public void ReloadMyProfile()
    //        {
    //            try
    //            {
    //                RingIDViewModel.Instance.MyBasicInfoModel.LoadData(DefaultSettings.userProfile);
    //                RingIDViewModel.Instance.MyBasicInfoModel.OnPropertyChanged("CurrentInstance");
    //                RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.OnPropertyChanged("CurrentInstance");
    //                if (RingIDViewModel.Instance.GetWindowRingIDSettings._AccountSettings != null && RingIDViewModel.Instance.GetWindowRingIDSettings._AccountSettings.ucRecoverySettings != null)
    //                {
    //                    RingIDViewModel.Instance.GetWindowRingIDSettings._AccountSettings.ucRecoverySettings.LoadData();
    //                }
    //                if (RingIDViewModel.Instance.GetWindowRingIDSettings._UCGeneralSettings != null)
    //                {
    //                    RingIDViewModel.Instance.GetWindowRingIDSettings._UCGeneralSettings.SetSelectedModelValuesForNotificationValidityPopup();
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: ReloadMyProfile() => " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }
    //        public void ReloadFriendsMutualFriendsProfile()
    //        {
    //            Application.Current.Dispatcher.Invoke(() =>
    //            {
    //                try
    //                {
    //                    foreach (UCFriendProfile ucFriendProfile in UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.Values)
    //                    {
    //                        if (ucFriendProfile._UCFriendContactList != null && ucFriendProfile._UCFriendContactList.MUTUAL_FRIENDS_LOADED)
    //                        {
    //                            long uid = ucFriendProfile._UCFriendContactList.uid;
    //                            /// new MutualFriendsRequest(uid);
    //                            SendDataToServer.SendMutualFreindRequest(uid);
    //                        }
    //                    }
    //                }
    //                catch (Exception ex)
    //                {
    //                    log.Error("Error: ReloadFriendsMutualFriendsProfile() => " + ex.Message + "\n" + ex.StackTrace);
    //                }
    //            });
    //        }
    //        public void ChangeMyStatusIcon(int mood)
    //        {
    //            try
    //            {
    //                RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.Mood = mood;
    //                RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.OnPropertyChanged("Mood");
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        //public void ChatUpdateIsInternetAvailable()
    //        //{
    //        //    ChatBaseHelpers.UpdateIsInternetAvailable(DefaultSettings.IsInternetAvailable);
    //        //    if (DefaultSettings.IsInternetAvailable == false)
    //        //    {
    //        //        ChatService.UnregisterAllChatConversation(StatusConstants.PRESENCE_ONLINE, StatusConstants.MOOD_ALIVE);
    //        //    }
    //        //}

    //        public void ShowFriendMutualContactList(long uid, List<long> listMFUtID)
    //        {
    //            Application.Current.Dispatcher.Invoke(() =>
    //            {
    //                try
    //                {
    //                    int tf = FriendDictionaries.Instance.FRIEND_MUTUAL_LIST_DICTIONARY[uid].TotalFriend;
    //                    UCFriendProfile ucFriendProfile = null;
    //                    UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.TryGetValue(uid, out ucFriendProfile);
    //                    if (ucFriendProfile != null)
    //                    {
    //                        ucFriendProfile.MUTUAL_FRIEND_COUNT_IN_CONTACTLIST = tf;
    //                        //ucFriendProfile.MutualList.Clear();
    //                    }
    //                    if (UCMutualFriendsPopup.CurrentInstance != null && UCMutualFriendsPopup.CurrentInstance.uid == uid)
    //                    {
    //                        //UCMutualFriendsPopup.CurrentInstance.MUTUAL_FRIEND_COUNT = "(" + tf + ")";
    //                        //UCMutualFriendsPopup.CurrentInstance.MutualList.Clear();
    //                        UCMutualFriendsPopup.CurrentInstance.Total = tf;
    //                    }
    //                    #region UPDATE UNKNOWN MODEL NMF COUNT
    //                    //  UserBasicInfoModel unknownModel = RingIDViewModel.Instance.UnknownFriendList.Where(P => P.ShortInfoModel.UserIdentity == uid).FirstOrDefault();

    //                    UserBasicInfoModel unknownModel = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(uid);
    //                    if (unknownModel != null && unknownModel.NumberOfMutualFriends != tf)
    //                    {
    //                        unknownModel.NumberOfMutualFriends = tf;// to update UnknownFriendList                       
    //                        UserBasicInfoDTO entity;
    //                        FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(uid, out entity);
    //                        if (entity != null && entity.NumberOfMutualFriends != tf)
    //                        {
    //                            entity.NumberOfMutualFriends = tf;//to update UID_USERBASICINFO_DICTIONARY
    //                            List<UserBasicInfoDTO> userDtoList = new List<UserBasicInfoDTO>();
    //                            userDtoList.Add(entity);
    //                            new InsertIntoUserBasicInfoTable(userDtoList).Start();
    //                        }
    //                    }
    //                    #endregion
    //                    int i = 0;
    //                    List<UserBasicInfoModel> tempUserList = new List<UserBasicInfoModel>();
    //                    foreach (long utid in listMFUtID)
    //                    {
    //                        UserBasicInfoModel model = null;
    //                        if (ucFriendProfile != null)
    //                            model = ucFriendProfile.FriendList.Where(P => P.ShortInfoModel.UserTableID == utid).FirstOrDefault();
    //                        if (model == null)
    //                            model = RingIDViewModel.Instance.GetFriendBasicInfoModelByUtID(utid);
    //                        if (model != null)
    //                        {
    //                            if (ucFriendProfile != null)
    //                            {
    //                                model.VisibilityModel.IsVisibleInFriendsFriendList = Visibility.Visible;
    //                                lock (ucFriendProfile.MutualList)
    //                                {
    //                                    if (!ucFriendProfile.MutualList.Any(P => P.ShortInfoModel.UserTableID == utid))
    //                                        ucFriendProfile.MutualList.Add(model);
    //                                }

    //                                if (ucFriendProfile._UCFriendContactList != null)
    //                                {
    //                                    ucFriendProfile._UCFriendContactList.SearchFromAuth(model);
    //                                }

    //                                if (!String.IsNullOrEmpty(model.ShortInfoModel.ProfileImage) && i < 3)
    //                                {
    //                                    AddMutualFriendForThreeCircle(i++, model, ucFriendProfile);
    //                                }
    //                                else if (!string.IsNullOrEmpty(model.ShortInfoModel.FullName))
    //                                {
    //                                    tempUserList.Add(model);
    //                                }
    //                            }
    //                            if (UCMutualFriendsPopup.CurrentInstance != null && UCMutualFriendsPopup.CurrentInstance.uid == uid)
    //                            {
    //                                UCMutualFriendsPopup.CurrentInstance.MutualList.Add(model);
    //                            }
    //                        }
    //                    }
    //                    foreach (UserBasicInfoModel user in tempUserList)
    //                    {
    //                        if (i >= 3)
    //                        {
    //                            tempUserList.Clear();
    //                            break;
    //                        }
    //                        AddMutualFriendForThreeCircle(i++, user, ucFriendProfile);
    //                    }
    //                    if (ucFriendProfile != null)
    //                    {
    //                        //ucFriendProfile.LoadData();
    //                        if (ucFriendProfile._UCFriendContactList != null)
    //                        {
    //                            ucFriendProfile._UCFriendContactList.MUTUAL_FRIEND_COUNT = ucFriendProfile.MUTUAL_FRIEND_COUNT_IN_CONTACTLIST;
    //                            //ucFriendProfile._UCFriendContactList.Check_Search();
    //                            ucFriendProfile._UCFriendContactList.MakeShowMoreVisible(ucFriendProfile.MutualList.Count < ucFriendProfile.MUTUAL_FRIEND_COUNT_IN_CONTACTLIST, true);
    //                        }
    //                    }
    //                    if (UCMutualFriendsPopup.CurrentInstance != null && UCMutualFriendsPopup.CurrentInstance.uid == uid)
    //                    {
    //                        UCMutualFriendsPopup.CurrentInstance.SetVisibilities();
    //                    }
    //                    FriendDictionaries.Instance.TMP_FRIEND_MUTUAL_LIST_DICTIONARY.Clear();
    //                }
    //                catch (Exception ex)
    //                {
    //                    log.Error("Error: ShowFriendMutualContactList() => " + ex.Message + "\n" + ex.StackTrace);
    //                }
    //                //finally
    //                //{
    //                //    HelperMethods.ReleaseMemory(DefaultSettings.FEED_TYPE_FRIEND, uid);
    //                //}
    //            });
    //        }
    //        public void AddMutualFriendForThreeCircle(int i, UserBasicInfoModel model, UCFriendProfile ucFriendProfile)
    //        {
    //            try
    //            {
    //                if (i == 0) ucFriendProfile.UserShortInfoModelOfMutualFriendCircle1 = model.ShortInfoModel;
    //                else if (i == 1) ucFriendProfile.UserShortInfoModelOfMutualFriendCircle2 = model.ShortInfoModel;
    //                else if (i == 2) ucFriendProfile.UserShortInfoModelOfMutualFriendCircle3 = model.ShortInfoModel;
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        public void NoMutualFriendFoundInFriendsContactList(long uid)
    //        {
    //            Application.Current.Dispatcher.Invoke(() =>
    //            {
    //                UCFriendProfile ucFriendProfile = null;
    //                if (UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.TryGetValue(uid, out ucFriendProfile))
    //                {
    //                    if (ucFriendProfile._UCFriendContactList != null)
    //                    {
    //                        ucFriendProfile._UCFriendContactList.MakeShowMoreVisible(ucFriendProfile.MutualList.Count < ucFriendProfile.MUTUAL_FRIEND_COUNT_IN_CONTACTLIST, true);
    //                    }
    //                }
    //            });
    //        }

    //        public void ShowFriendTotalContactList(long uid, List<UserBasicInfoDTO> listUserBasicInfoDTO)
    //        {
    //            Application.Current.Dispatcher.Invoke(() =>
    //                {
    //                    try
    //                    {
    //                        UCFriendProfile ucFriendProfile = null;
    //                        UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.TryGetValue(uid, out ucFriendProfile);
    //                        if (ucFriendProfile != null)
    //                        {
    //                            ucFriendProfile.TOTAL_FRIEND_IN_CONTACTLIST_COUNT = FriendDictionaries.Instance.FRIEND_CONTACT_LIST_DICTIONARY[uid].TotalFriend;
    //                            foreach (UserBasicInfoDTO user in listUserBasicInfoDTO)//FriendDictionaries.Instance.TMP_FRIEND_CONTACT_LIST_DICTIONARY.Values)
    //                            {
    //                                UserBasicInfoModel model = null;
    //                                if (ucFriendProfile.MutualList.Count > 0)
    //                                {
    //                                    model = ucFriendProfile.MutualList.Where(P => P.ShortInfoModel.UserIdentity == user.UserIdentity).FirstOrDefault();
    //                                }
    //                                if (model == null)
    //                                {
    //                                    model = RingIDViewModel.Instance.GetFriendBasicInfoModelByID(user.UserIdentity);
    //                                }
    //                                if (model == null)
    //                                    model = new UserBasicInfoModel(user);
    //                                else
    //                                {
    //                                    int blockedBy = model.BlockedBy;
    //                                    model.LoadData(user);
    //                                    model.BlockedBy = blockedBy;
    //                                }

    //                                if (ucFriendProfile._UCFriendContactList != null)
    //                                {
    //                                    ucFriendProfile._UCFriendContactList.SearchFromAuth(model);
    //                                }
    //                                lock (ucFriendProfile.FriendList)
    //                                {
    //                                    if (!ucFriendProfile.FriendList.Any(P => P.ShortInfoModel.UserIdentity == user.UserIdentity))
    //                                        ucFriendProfile.FriendList.Add(model);
    //                                }

    //                                if (!string.IsNullOrEmpty(model.ShortInfoModel.ProfileImage) && ucFriendProfile.TempFriendPreviewImageCount < 4) // ToShowFourImagesInFriendsProfileTab
    //                                {
    //                                    lock (ucFriendProfile.TempFriendList)
    //                                    {
    //                                        if (ucFriendProfile.TempFriendList.Count == 4 && !ucFriendProfile.TempFriendList.Any(P => P.ShortInfoModel.UserIdentity == user.UserIdentity))
    //                                        {
    //                                            ucFriendProfile.TempFriendList[ucFriendProfile.TempFriendPreviewImageCount++] = model;
    //                                        }
    //                                    }
    //                                }
    //                            }
    //                            ucFriendProfile.UnknownFriendPanel.Visibility = Visibility.Collapsed;
    //                            ucFriendProfile.DownLoadedFriendPanel.Visibility = Visibility.Visible;
    //                            ucFriendProfile.LoadData();
    //                            //ucFriendProfile._UCFriendContactList.MakeShowMoreVisible(ucFriendProfile.FriendList.Count < tf);
    //                            //ucFriendProfile._UCFriendContactList.Check_Search();
    //                            //FriendDictionaries.Instance.TMP_FRIEND_CONTACT_LIST_DICTIONARY.Clear();
    //                        }
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                        log.Error("Error: ShowFriendTotalContactList() => " + ex.Message + "\n" + ex.StackTrace);
    //                    }
    //                    //finally
    //                    //{
    //                    //    HelperMethods.ReleaseMemory(DefaultSettings.FEED_TYPE_FRIEND, uid);
    //                    //}
    //                });
    //        }

    //        public void NoContactFoundInFriendsContactList(long utid)
    //        {
    //            Application.Current.Dispatcher.Invoke(() =>
    //            {
    //                long uid;
    //                FriendDictionaries.Instance.UTID_UID_DICTIONARY.TryGetValue(utid, out uid);
    //                UCFriendProfile ucFriendProfile = null;
    //                if (UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.TryGetValue(uid, out ucFriendProfile))
    //                {
    //                    ucFriendProfile.LoadData();
    //                }
    //            });
    //        }

    //        private void ChangeFriendProfile(UserBasicInfoDTO userDTO)
    //        {
    //            UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelByID(userDTO.UserIdentity);
    //            if (userDTO.UserTableID == DefaultSettings.RINGID_OFFICIAL_UTID)
    //            {
    //                if (model == null)
    //                {
    //                    FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModelsByDto(userDTO);
    //                }
    //                else
    //                {
    //                    FriendListLoadUtility.AddInfoInModel(model, userDTO);
    //                }
    //            }
    //            else if (Models.Constants.DefaultSettings.FRIEND_LIST_LOADED && Models.Constants.DefaultSettings.FRIEND_LIST_LOADED_FROM_DB)
    //            {
    //                try
    //                {

    //                    if (model != null)
    //                    {
    //                        model.LoadData(userDTO);
    //                        model.ShortInfoModel.OnPropertyChanged("FriendShipStatus");
    //                        model.OnPropertyChanged("Presence");
    //                        model.OnPropertyChanged("CallAccess");
    //                        model.OnPropertyChanged("ChatAccess");
    //                        model.OnPropertyChanged("FeedAccess");
    //                        model.OnPropertyChanged("CurrentInstance");
    //                        model.ShortInfoModel.OnPropertyChanged("CurrentInstance");
    //                    }
    //                    else
    //                    {
    //                        FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModelsByDto(userDTO);
    //                        //     FriendListLoadUtility.InsertSingleFriendToUILists(userDTO);
    //                    }
    //                }
    //                catch (Exception ex)
    //                {
    //                    log.Error("Error: ChangeFriendProfile() => " + ex.Message + "\n" + ex.StackTrace);
    //                }
    //            }
    //        }

    //        public void MultipleSessionWarning()
    //        {
    //            Application.Current.Dispatcher.Invoke(() =>
    //            {
    //                try
    //                {
    //                    if (UCGuiRingID.Instance != null)
    //                        //UCGuiRingID.Instance.ucSignOutLoader.SetUserModel(false, false, true);
    //                        UCGuiRingID.Instance.ucSignOutLoader.AppRestartAndShowMultipleSessionWarning();
    //                }
    //                catch (Exception e)
    //                {
    //                    log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //                }
    //            });
    //        }

    //        public void SessionInvalidAppRestart()
    //        {
    //            Application.Current.Dispatcher.Invoke(() =>
    //            {
    //                try
    //                {
    //                    if (UCGuiRingID.Instance != null)
    //                        //UCGuiRingID.Instance.ucSignOutLoader.SetUserModel(false, true, false);
    //                        UCGuiRingID.Instance.ucSignOutLoader.AppRestartAndShowSessionInvalidWarning();
    //                }
    //                catch (Exception e)
    //                {
    //                    log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //                }
    //            });
    //        }

    //        public void AppRestart()
    //        {
    //            Application.Current.Dispatcher.Invoke(() =>
    //            {
    //                try
    //                {
    //                    if (UCGuiRingID.Instance != null)
    //                        UCGuiRingID.Instance.ucSignOutLoader.AppRestart();
    //                }
    //                catch (Exception e)
    //                {
    //                    log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //                }
    //            });
    //        }

    //        //public void StartStopSignOutLoader(bool start)
    //        //{
    //        //    if (UCGuiRingID.Instance != null)
    //        //        if (start)
    //        //            UCGuiRingID.Instance.OpenLoader();
    //        //        else
    //        //            UCGuiRingID.Instance.StopLoader();
    //        //}

    //        //public void ClearAllViewDictionariesAndOpenLoginUI()
    //        //{
    //        //    ChatAsyncCommunication.Instance.StopService();

    //        //    UIDictionaries.Instance.ClearUIDictionaries();
    //        //    ImageDictionaries.Instance.ClearImageDictionaries();
    //        //    GC.Collect();
    //        //    Application.Current.Dispatcher.Invoke(() =>
    //        //    {
    //        //        if (UCGuiRingID.Instance != null)
    //        //        {
    //        //            UCGuiRingID.Instance = null;
    //        //        }
    //        //        CancelRunningCall();
    //        //        if (RingIDViewModel.Instance._WNRingIDSettings != null)
    //        //        {
    //        //            RingIDViewModel.Instance._WNRingIDSettings.Close();
    //        //            RingIDViewModel.Instance._WNRingIDSettings = null;
    //        //        }
    //        //        DefaultSettings.CurrentDivertAdd = false;
    //        //        DefaultSettings.CurrentDivertCancel = false;
    //        //        RingIDViewModel.Instance._UCMyAlbums = null;
    //        //        AppConstants.ADD_FRIEND_NOTIFICATION_COUNT = 0;
    //        //        AppConstants.CALL_NOTIFICATION_COUNT = 0;
    //        //        AppConstants.CHAT_NOTIFICATION_COUNT = 0;
    //        //        AppConstants.ALL_NOTIFICATION_COUNT = 0;
    //        //        DefaultSettings.IS_MY_PROFILE_VALUES_LOADED = false;
    //        //        UCCallLog.Instance = null;
    //        //        UCChatLogPanel.Instance = null;
    //        //        UCAllNotification.Instance = null;
    //        //        UCAddFriendSearchPanel.Instance = null;
    //        //        UCSuggestions.Instance = null;
    //        //        UCAddFriendPending.Instance = null;
    //        //        RingIDViewModel.Instance.Reset();
    //        //        MiddlePanelSwitcher.pageSwitcher.ClearUIinstancesinMiddlePanelSwitcher();
    //        //        if (UCSignoutView.Instance != null)
    //        //            UCSignoutView.Instance.DoCancel();
    //        //        MainSwitcher.SwitchToLoginScreen();
    //        //        //  MainSwitcher._UCLoginMainSwitcher._UCLoginMainSwitcher.View_UCSignInPanel.LoadData();
    //        //        //UCLoginScreen.Instance.LoadData();
    //        //    });
    //        //    VoiceSignalReceiverSenderSocket.Instance.resetCallSocket();
    //        //}
    //        //public void CancelRunningCall()
    //        //{
    //        //    if (RingIDViewModel.Instance.callUI != null)
    //        //    {
    //        //        bool incomm = RingIDViewModel.Instance.callUI.callerDto.CallType == CallConstants.CALL_TYPE_INCOMING ? true : false;
    //        //        CallUIHelperMethods.cancelButtonAction(incomm);
    //        //    }
    //        //}

    //        public void ShowTimerMessageBox(string msg, string caption)
    //        {
    //            //Application.Current.Dispatcher.Invoke(() =>
    //            //{
    //            try
    //            {
    //                CustomMessageBox.ShowMessageWithTimerFromThread(msg);
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //            //});
    //            //System.Threading.Thread.Sleep(1500);
    //            //Application.Current.Dispatcher.Invoke(() =>
    //            //{
    //            //    CustomMessageBox.CloseWin();
    //            //});

    //        }

    //        public void ShowErrorMessageBox(string msg, string caption)
    //        {
    //            Application.Current.Dispatcher.Invoke(() =>
    //            {
    //                // MessageBox.Show(MainSwitcher.pageSwitcher, msg, caption, MessageBoxButton.OK, MessageBoxImage.Error);
    //                try
    //                {
    //                    if (UCCirclePopup.Instance != null && UCCirclePopup.Instance.popupCircle.IsOpen)
    //                    {
    //                        UCCirclePopup.Instance.ShowServerMessage(msg);
    //                    }
    //                    else
    //                        CustomMessageBox.ShowError(msg, caption);
    //                }
    //                catch (Exception e)
    //                {
    //                    log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //                }
    //            });
    //        }

    //        public void ShowSuccesMessageBox(string msg, string caption)
    //        {
    //            Application.Current.Dispatcher.Invoke(() =>
    //            {
    //                try
    //                {
    //                    CustomMessageBox.ShowInformation(msg, caption);
    //                }
    //                catch (Exception e)
    //                {
    //                    log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //                }
    //            });
    //        }


    //        //public void ShowSearchResultsInDialledFriendsContainer()
    //        //{
    //        //    new AddContactSearchResult().StartProcessing();
    //        //}

    //        public void DownloadMandatoryUpdater()
    //        {
    //            Application.Current.Dispatcher.Invoke(delegate
    //            {
    //                MessageBoxResult result = CustomMessageBox.ShowWarning("Update to the latest version of ringID mandatory!", "", true);
    //                if (result == MessageBoxResult.OK)
    //                {
    //                    HelperMethods.Runupdater(true);
    //                }
    //                else if (result == MessageBoxResult.Cancel)
    //                {
    //                    //MainSwitcher.pageSwitcher.HideTrayIcon();
    //                    MainSwitcher.MainController().MainUIController().MainWindow.HideTrayIcon();
    //                    Environment.Exit(0);
    //                }
    //            });
    //        }

    //        //public void AddOrAcceptSingleFriendUI(UserBasicInfoDTO user)
    //        //{
    //        //    // Application.Current.Dispatcher.Invoke(() =>
    //        //    //{
    //        //    //    try
    //        //    //    {
    //        //    //        FriendListLoadUtility.InsertSingleFriendToUILists(user);
    //        //    //    }
    //        //    //    catch (Exception ex)
    //        //    //    {
    //        //    //        log.Error("Error: AddOrAcceptSingleFriendUI() => " + ex.Message + "\n" + ex.StackTrace);
    //        //    //    }
    //        //    //});
    //        //}

    //        //public void RemoveSingleFriendFromUIList(UserBasicInfoDTO user)
    //        //{
    //        //    FriendListLoadUtility.RemoveAModelFromUI(user, null, true);
    //        //    //   Application.Current.Dispatcher.Invoke(() =>
    //        //    //{
    //        //    //    try
    //        //    //    {
    //        //    //        FriendListLoadUtility.RemoveSingleFriendFromUILists(user, true);
    //        //    //    }
    //        //    //    catch (Exception ex)
    //        //    //    {
    //        //    //        log.Error("Error: RemoveSingleFriendFromUIList() => " + ex.Message + "\n" + ex.StackTrace);
    //        //    //    }
    //        //    //});
    //        //}

    //        public void ShowCallSettingsInfo()
    //        {
    //            try
    //            {
    //                if (RingIDViewModel.Instance._WNRingIDSettings != null && RingIDViewModel.Instance._WNRingIDSettings._UCCallSettings != null)
    //                {
    //                    RingIDViewModel.Instance._WNRingIDSettings._UCCallSettings.LoadCallSettings();
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: ShowCallSettingsInfo() => " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }

    //        public void ShowPrivacySettingsInfo()
    //        {
    //            try
    //            {
    //                if (RingIDViewModel.Instance._WNRingIDSettings != null && RingIDViewModel.Instance._WNRingIDSettings._PrivacySettingsInstance != null)
    //                {
    //                    RingIDViewModel.Instance._WNRingIDSettings._PrivacySettingsInstance.LoadPrivacy();
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: ShowPrivacySettingsInfo() => " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }

    //        public void ShowBasicInfo(long utid)
    //        {
    //            try
    //            {
    //                if (utid == DefaultSettings.LOGIN_TABLE_ID)
    //                {
    //                    if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout != null)
    //                    {
    //                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ShowMyBasicInfos();
    //                    }
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: ShowBasicInfo() => " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }

    //        public void SetRequestsForFriendViewedandAdded(UserBasicInfoDTO userBasicInfo) ////someone accepted my request,or i accepted someone's req & his UI is already open
    //        {
    //            try
    //            {
    //                UCFriendProfile ucFriendProfile = null;
    //                if (UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.TryGetValue(userBasicInfo.UserIdentity, out ucFriendProfile))
    //                {
    //                    if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(userBasicInfo.UserIdentity))
    //                    {
    //                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Remove(userBasicInfo.UserIdentity);
    //                    }
    //                    Application.Current.Dispatcher.Invoke(() =>
    //                    {
    //                        if (ucFriendProfile._UCFriendNewsFeeds != null)
    //                        {
    //                            bool WasVisible = ucFriendProfile._UCFriendNewsFeeds.IsVisible;
    //                            ucFriendProfile._UCFriendNewsFeeds = new UCFriendNewsFeeds(userBasicInfo.UserIdentity);
    //                            //ucFriendProfile._UCFriendNewsFeeds.FriendShipStatus = StatusConstants.FRIENDSHIP_STATUS_ACCEPTED;
    //                            if (WasVisible) ucFriendProfile.ShowPost();
    //                        }
    //                    });
    //                    new FriendDetailsInfoRequest(userBasicInfo.UserTableID).Start();
    //                    //      new WorkEducationSkillRequest(userBasicInfo.UserTableID);
    //                    SendDataToServer.WorkEducationSkillRequest(userBasicInfo.UserTableID);
    //                   new ThreadFriendPresenceInfo(userBasicInfo.UserIdentity);
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: SetRequestsForFriendViewedandAdded() => " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }

    //        public void HideFriendAboutInfos(UserBasicInfoDTO userBasicInfo) ////someone deleted me from freindlist,or i deleted someone from freindlist & his UI is already open
    //        {
    //            try
    //            {
    //                UCFriendProfile ucFriendProfile = null;
    //                if (UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.TryGetValue(userBasicInfo.UserIdentity, out ucFriendProfile))
    //                {
    //                    if (ucFriendProfile._UCFriendAbout != null)
    //                    {
    //                        ucFriendProfile._UCFriendAbout.ucFriendBasicInfo.HideFriendAboutInfos();
    //                    }
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: HideFriendAboutInfos() => " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }

    //        public void HideFriendProfileTabForNF(UserBasicInfoDTO userBasicInfo)
    //        {
    //            try
    //            {
    //                UCFriendProfile ucFriendProfile = null;
    //                if (UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.TryGetValue(userBasicInfo.UserIdentity, out ucFriendProfile))
    //                {
    //                    if (NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(userBasicInfo.UserIdentity))
    //                    {
    //                        NewsFeedDictionaries.Instance.FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Remove(userBasicInfo.UserIdentity);
    //                    }
    //                    Application.Current.Dispatcher.Invoke(() =>
    //                    {
    //                        if (ucFriendProfile._UCFriendNewsFeeds != null)
    //                        {
    //                            bool WasVisible = ucFriendProfile._UCFriendNewsFeeds.IsVisible;
    //                            ucFriendProfile._UCFriendNewsFeeds = new UCFriendNewsFeeds(userBasicInfo.UserIdentity);
    //                            ucFriendProfile._UCFriendNewsFeeds.FriendShipStatus = 0;
    //                            if (WasVisible) ucFriendProfile.ShowPost();
    //                        }
    //                    });
    //                    if (ucFriendProfile.SelectedBorder > 0 && ucFriendProfile.SelectedBorder % 2 == 0)
    //                    {
    //                        ucFriendProfile.SelectedBorder = 1;
    //                        if (ucFriendProfile._UCFriendAbout == null)
    //                        {
    //                            ShowFriendBasicInfo(userBasicInfo);
    //                        }
    //                        Application.Current.Dispatcher.Invoke(() =>
    //                        {
    //                            ucFriendProfile.UCFPContentSwitcherInstance.Content = ucFriendProfile._UCFriendAbout;
    //                        });
    //                    }
    //                }
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        public void ShowFriendBasicInfo(UserBasicInfoDTO user)
    //        {
    //            Application.Current.Dispatcher.Invoke(() =>
    //            {
    //                try
    //                {
    //                    UCFriendProfile ucFriendProfile = null;
    //                    if (UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.TryGetValue(user.UserIdentity, out ucFriendProfile))
    //                    {
    //                        if (ucFriendProfile._UCFriendAbout == null)
    //                        {
    //                            //new WorkEducationSkillRequest(ucFriendProfile.FriendBasicInfoModel.UserTableID);
    //                            ucFriendProfile._UCFriendAbout = new UCFriendAbout(ucFriendProfile.FriendBasicInfoModel.ShortInfoModel.FullName);
    //                        }
    //                        UserBasicInfoModel _FriendModel = new UserBasicInfoModel(user);
    //                        ucFriendProfile._UCFriendAbout.ucFriendBasicInfo.LoadFriendBasicInfoFromModel(_FriendModel);
    //                    }
    //                }
    //                catch (Exception ex)
    //                {
    //                    log.Error("Error: ShowFriendBasicInfo() => " + ex.Message + "\n" + ex.StackTrace);
    //                }
    //            });
    //        }

    //        public void ShowWorkList(long utid)
    //        {
    //            try
    //            {
    //                if (utid == DefaultSettings.LOGIN_TABLE_ID)
    //                {
    //                    if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyProfessionalCareer != null)
    //                    {
    //                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyProfessionalCareer.ShowMyWorksList();
    //                    }
    //                }
    //                else
    //                {
    //                    Application.Current.Dispatcher.BeginInvoke(() =>
    //                    {
    //                        long uid = 0;
    //                        if (FriendDictionaries.Instance.UTID_UID_DICTIONARY.TryGetValue(utid, out uid))
    //                        {
    //                            UCFriendProfile ucFriendProfile = null;
    //                            if (UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.TryGetValue(uid, out ucFriendProfile))
    //                            {
    //                                if (ucFriendProfile._UCFriendAbout != null && ucFriendProfile._UCFriendAbout.ucFriendProfessionalCareer != null)
    //                                {
    //                                    ucFriendProfile._UCFriendAbout.ucFriendProfessionalCareer.LoadMyFriendsWorkToModels(uid);
    //                                }
    //                            }
    //                        }
    //                    });
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: ShowWorkList() => " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }

    //        public void ShowEducationList(long utid)
    //        {
    //            try
    //            {
    //                if (utid == DefaultSettings.LOGIN_TABLE_ID)
    //                {

    //                    if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyEducation != null)
    //                    {
    //                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyEducation.ShowMyEducationsList();
    //                    }
    //                }
    //                else
    //                {
    //                    Application.Current.Dispatcher.BeginInvoke(() =>
    //                    {
    //                        long uid = 0;
    //                        if (FriendDictionaries.Instance.UTID_UID_DICTIONARY.TryGetValue(utid, out uid))
    //                        {
    //                            UCFriendProfile ucFriendProfile = null;
    //                            if (UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.TryGetValue(uid, out ucFriendProfile))
    //                            {
    //                                if (ucFriendProfile._UCFriendAbout != null && ucFriendProfile._UCFriendAbout.ucFriendEducation != null)
    //                                {
    //                                    ucFriendProfile._UCFriendAbout.ucFriendEducation.LoadMyFriendsEduToModels(uid);
    //                                }
    //                            }
    //                        }
    //                    });
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: ShowEducationList() => " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }

    //        public void ShowSkillList(long utid)
    //        {
    //            try
    //            {
    //                if (utid == DefaultSettings.LOGIN_TABLE_ID)
    //                {
    //                    if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMySkill != null)
    //                    {
    //                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMySkill.ShowMySkillsList();
    //                    }
    //                }
    //                else
    //                {
    //                    Application.Current.Dispatcher.BeginInvoke(() =>
    //                    {
    //                        long uid = 0;
    //                        if (FriendDictionaries.Instance.UTID_UID_DICTIONARY.TryGetValue(utid, out uid))
    //                        {
    //                            UCFriendProfile ucFriendProfile = null;
    //                            if (UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.TryGetValue(uid, out ucFriendProfile) && ucFriendProfile._UCFriendAbout != null && ucFriendProfile._UCFriendAbout.ucFriendSkill != null)
    //                            {
    //                                ucFriendProfile._UCFriendAbout.ucFriendSkill.LoadMyFriendsSkillsToModels(uid);
    //                            }
    //                        }
    //                    });
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: ShowSkillList() => " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }

    //        public void AddtoMyWork()
    //        {
    //            try
    //            {
    //                SingleWorkModel workModel = new SingleWorkModel();
    //                workModel.LoadData(DefaultSettings.TEMP_WorkObject);
    //                if (!RingIDViewModel.Instance.MyWorkModelsList.Any(p => p.WId == workModel.WId))
    //                {
    //                    Application.Current.Dispatcher.Invoke(() =>
    //                    {
    //                        //RingIDViewModel.Instance.MyWorkModelsList.Add(workModel);
    //                        RingIDViewModel.Instance.MyWorkModelsList.Insert(0, workModel);
    //                    });
    //                }
    //                //workModel.WVisibilityEditMode = Visibility.Collapsed;
    //                workModel.WVisibilityViewMode = Visibility.Visible;
    //                //workModel.WErrorString = "";
    //                Application.Current.Dispatcher.Invoke(() =>
    //                {
    //                    if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyProfessionalCareer != null)
    //                    {
    //                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyProfessionalCareer.Absent = Visibility.Collapsed;
    //                    }
    //                });
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: AddtoMyWork() => " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }

    //        public void AddtoMyEducation()
    //        {
    //            try
    //            {
    //                SingleEducationModel EducationModel = new SingleEducationModel();
    //                EducationModel.LoadData(DefaultSettings.TEMP_EducationObject);

    //                if (!RingIDViewModel.Instance.MyEducationModelsList.Any(p => p.EId == EducationModel.EId))
    //                {
    //                    Application.Current.Dispatcher.Invoke(() =>
    //                    {
    //                        RingIDViewModel.Instance.MyEducationModelsList.Insert(0, EducationModel);
    //                    });
    //                }

    //                //EducationModel.EVisibilityEditMode = Visibility.Collapsed;
    //                EducationModel.EVisibilityViewMode = Visibility.Visible;
    //                //EducationModel.EErrorString = "";
    //                Application.Current.Dispatcher.Invoke(() =>
    //                {
    //                    if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyEducation != null)
    //                    {
    //                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyEducation.Absent = Visibility.Collapsed;
    //                    }
    //                });
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: AddtoMyEducation() => " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }

    //        public void AddtoMySkill()
    //        {
    //            try
    //            {
    //                SingleSkillModel SkillModel = new SingleSkillModel();
    //                SkillModel.LoadData(DefaultSettings.TEMP_SkillObject);
    //                if (!RingIDViewModel.Instance.MySkillModelsList.Any(p => p.SKId == SkillModel.SKId))
    //                {
    //                    Application.Current.Dispatcher.Invoke(() =>
    //                    {
    //                        //RingIDViewModel.Instance.MySkillModelsList.Add(SkillModel);
    //                        RingIDViewModel.Instance.MySkillModelsList.Insert(0, SkillModel);
    //                    });
    //                }
    //                //SkillModel.SVisibilityEditMode = Visibility.Collapsed;
    //                SkillModel.SVisibilityViewMode = Visibility.Visible;
    //                SkillModel.SErrorString = "";
    //                Application.Current.Dispatcher.Invoke(() =>
    //                {
    //                    if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMySkill != null)
    //                    {
    //                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMySkill.Absent = Visibility.Collapsed;
    //                    }
    //                });
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: AddtoMySkill() => " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }

    //        public void CheckFriendPresence(long uid)
    //        {
    //            try
    //            {
    //                if (UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.ContainsKey(uid) && UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY[uid].IsVisible)
    //                {
    //                   new ThreadFriendPresenceInfo(uid);
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: CheckFriendPresence() => " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }

    //        public void ChangeCallChatLogsOnFriendUpdate(long uid)
    //        {
    //            try
    //            {
    //                if (UCCallLog.Instance != null && RingIDViewModel.Instance.CallLogModelsList.Count > 0)
    //                {
    //                    List<RecentModel> recentModels = RingIDViewModel.Instance.CallLogModelsList.Where(item => item.CallLog.FriendIdentity == uid).ToList();
    //                    foreach (RecentModel r in recentModels)
    //                    {
    //                        r.CallLog.OnPropertyChanged("FriendIdentity");
    //                    }
    //                }
    //                if (UCChatLogPanel.Instance != null && RingIDViewModel.Instance.ChatLogModelsList.Count > 0)
    //                {
    //                    RecentModel recentModel = RingIDViewModel.Instance.ChatLogModelsList.Where(item => item.ContactID.Equals(uid.ToString())).FirstOrDefault();
    //                    if (recentModel != null)
    //                    {
    //                        //TRIGGER TO FULL NAME OR PROFILE IMAGE OR VISIBILITY BY SEARCH IN CHAT LOG PANEL
    //                        recentModel.OnPropertyChanged("Message");
    //                    }
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: ChangeCallChatLogsOnFriendUpdate() => " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }

    //        public void DownloadImage(string imageUrl, string imageName)
    //        {
    //            try
    //            {
    //                ImageUtility.DownloadImage(imageUrl, imageName);
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        public void  MainSwitcher.ThreadManager().StartNetworkThread();(string imageName)
    //        {
    //            try
    //            {
    //                if (File.Exists(imageName))
    //                {
    //                    File.Delete(imageName);
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error:  MainSwitcher.ThreadManager().StartNetworkThread();() => " + ex.Message + "\n" + ex.StackTrace);
    //            }

    //        }

    //        public void AddMyImagesFromServer(int albumImageType, int totalImage, List<ImageDTO> imageList)
    //        {
    //            Application.Current.Dispatcher.BeginInvoke(() =>
    //            {
    //                try
    //                {
    //                    List<ImageDTO> sortedList = (from l in imageList
    //                                                 orderby l.Time descending
    //                                                 select l).ToList();
    //                    #region "Image full view"
    //                    if (UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView == null)
    //                    {
    //                        UCGuiRingID.Instance.ucBasicImageViewWrapper.AddUCBasicImageViewToWrapperPanel();
    //                    }
    //                    UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.AddIntoImageList(albumImageType, DefaultSettings.LOGIN_USER_ID, sortedList);
    //                    #endregion "Image full view"
    //                    int timg = 0;
    //                    #region ProfileImage
    //                    if (albumImageType == SettingsConstants.TYPE_PROFILE_IMAGE)
    //                    {
    //                        foreach (ImageDTO image in sortedList)
    //                        {
    //                            lock (RingIDViewModel.Instance.ProfileImageList)
    //                            {
    //                                if (!RingIDViewModel.Instance.ProfileImageList.Any(p => p.ImageId == image.ImageId))
    //                                {
    //                                    ImageModel model = new ImageModel(image);
    //                                    RingIDViewModel.Instance.ProfileImageList.Add(model);
    //                                    Thread.Sleep(5);
    //                                    //if (UCGuiRingID.Instance.ucBasicImageView.clickedUserID == DefaultSettings.LOGIN_USER_ID && !UCGuiRingID.Instance.ucBasicImageView._FeedImageList.Any(p => p.ImageId == image.ImageId))
    //                                    //{
    //                                    //    UCGuiRingID.Instance.ucBasicImageView._FeedImageList.Add(model);
    //                                    //}
    //                                    if (timg == 0)
    //                                    {
    //                                        timg = image.TotalImagesInAlbum;
    //                                    }
    //                                }
    //                            }
    //                        }
    //                        if (timg > 0)
    //                        {
    //                            if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos != null) /* User profile Photos */
    //                            {
    //                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.ProfileImageCount = timg;
    //                            }
    //                            if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange != null) /* User Profile Change Photos */
    //                            {
    //                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange.ProfileImageCount = timg;
    //                            }
    //                            if (RingIDViewModel.Instance != null && RingIDViewModel.Instance.MyAlbums != null)
    //                            {
    //                                RingIDViewModel.Instance.MyAlbums.ProfileImageCount = timg;
    //                            }
    //                            //if (UCGuiRingID.Instance.ucBasicImageView.clickedUserID == DefaultSettings.LOGIN_USER_ID)
    //                            //{
    //                            //    UCGuiRingID.Instance.ucBasicImageView.setNavigationButtonsVisibility();
    //                            //    UCGuiRingID.Instance.ucBasicImageView.TotalImages = timg;
    //                            //    UCGuiRingID.Instance.ucBasicImageView.ChangeImageNumber();
    //                            //}
    //                        }


    //                        if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyProfilePhotosAlbum != null)
    //                        {
    //                            UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyProfilePhotosAlbum.ShowHideShowMoreLoading
    //                                (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.ProfileImageCount > RingIDViewModel.Instance.ProfileImageList.Count);
    //                        }
    //                        if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange._UCMyProfilePhotosChangeAlbum != null)
    //                        {

    //                            if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange.ProfileImageCount > RingIDViewModel.Instance.ProfileImageList.Count)
    //                            {
    //                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange._UCMyProfilePhotosChangeAlbum.ShowMore();
    //                            }
    //                            else
    //                            {
    //                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange._UCMyProfilePhotosChangeAlbum.HideShowMoreLoading();
    //                            }
    //                        }
    //                        if (RingIDViewModel.Instance != null && RingIDViewModel.Instance.MyAlbums != null)
    //                        {
    //                            if (RingIDViewModel.Instance.MyAlbums.ProfileImageCount > RingIDViewModel.Instance.ProfileImageList.Count)
    //                            {
    //                                RingIDViewModel.Instance.MyAlbums.AlbumProfileShowMore();
    //                            }
    //                            else
    //                            {
    //                                RingIDViewModel.Instance.MyAlbums.HideAlbumProfileShowMoreLoading();
    //                            }
    //                        }
    //                    }
    //                    #endregion
    //                    #region CoverImage
    //                    else if (albumImageType == SettingsConstants.TYPE_COVER_IMAGE)
    //                    {
    //                        foreach (ImageDTO image in sortedList)
    //                        {
    //                            lock (RingIDViewModel.Instance.CoverImageList)
    //                            {
    //                                if (!RingIDViewModel.Instance.CoverImageList.Any(p => p.ImageId == image.ImageId))
    //                                {
    //                                    ImageModel model = new ImageModel(image);
    //                                    RingIDViewModel.Instance.CoverImageList.Add(model);
    //                                    Thread.Sleep(5);
    //                                    //if (UCGuiRingID.Instance.ucBasicImageView.clickedUserID == DefaultSettings.LOGIN_USER_ID && !UCGuiRingID.Instance.ucBasicImageView._FeedImageList.Any(p => p.ImageId == image.ImageId))
    //                                    //{
    //                                    //    UCGuiRingID.Instance.ucBasicImageView._FeedImageList.Add(model);
    //                                    //}
    //                                    if (timg == 0)
    //                                    {
    //                                        timg = image.TotalImagesInAlbum;
    //                                    }
    //                                }
    //                            }

    //                        }
    //                        if (timg > 0)
    //                        {
    //                            //UCGuiRingID.Instance.ucBasicImageView.AddIntoImageList(DefaultSettings.LOGIN_USER_ID, sortedList);
    //                            if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos != null)
    //                            {
    //                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.CoverImageCount = timg;
    //                            }
    //                            if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange != null)
    //                            {
    //                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange.CoverImageCount = timg;
    //                            }
    //                            if (RingIDViewModel.Instance != null && RingIDViewModel.Instance.MyAlbums != null)
    //                            {
    //                                RingIDViewModel.Instance.MyAlbums.CoverImageCount = timg;
    //                            }
    //                        }
    //                        if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyCoverPhotosAlbum != null)
    //                        {
    //                            UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyCoverPhotosAlbum.ShowHideShowMoreLoading
    //                                (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.CoverImageCount > RingIDViewModel.Instance.CoverImageList.Count);
    //                        }
    //                        if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange._UCMyCoverPhotosChangeAlbum != null)
    //                        {

    //                            if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange.CoverImageCount > RingIDViewModel.Instance.CoverImageList.Count)
    //                            {
    //                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange._UCMyCoverPhotosChangeAlbum.ShowMore();
    //                            }
    //                            else
    //                            {
    //                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange._UCMyCoverPhotosChangeAlbum.HideShowMoreLoading();
    //                            }
    //                        }

    //                        if (RingIDViewModel.Instance != null && RingIDViewModel.Instance.MyAlbums != null)
    //                        {
    //                            if (RingIDViewModel.Instance.MyAlbums.CoverImageCount > RingIDViewModel.Instance.CoverImageList.Count)
    //                            {
    //                                RingIDViewModel.Instance.MyAlbums.AlbumCoverShowMore();
    //                            }
    //                            else
    //                            {
    //                                RingIDViewModel.Instance.MyAlbums.HideAlbumCoverShowMoreLoading();
    //                            }
    //                        }
    //                    }
    //                    #endregion
    //                    #region FeedImage
    //                    else if (albumImageType == SettingsConstants.TYPE_NORMAL_BOOK_IMAGE)
    //                    {
    //                        foreach (ImageDTO image in sortedList)
    //                        {
    //                            lock (RingIDViewModel.Instance.FeedImageList)
    //                            {
    //                                if (!RingIDViewModel.Instance.FeedImageList.Any(p => p.ImageId == image.ImageId))
    //                                {
    //                                    ImageModel model = new ImageModel(image);
    //                                    RingIDViewModel.Instance.FeedImageList.Add(model);
    //                                    Thread.Sleep(5);
    //                                }
    //                            }
    //                            if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos != null)
    //                            {
    //                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.FeedImageCount = image.TotalImagesInAlbum;
    //                            }
    //                            if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange != null)
    //                            {
    //                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange.FeedImageCount = image.TotalImagesInAlbum;
    //                            }
    //                            if (RingIDViewModel.Instance != null && RingIDViewModel.Instance.MyAlbums != null)
    //                            {
    //                                RingIDViewModel.Instance.MyAlbums.FeedImageCount = image.TotalImagesInAlbum;
    //                            }
    //                        }
    //                        RingIDViewModel.Instance.OnPropertyChanged("FeedImageList");

    //                        if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyFeedPhotosAlbum != null)
    //                        {
    //                            UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyFeedPhotosAlbum.ShowHideShowMoreLoading
    //                                (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.FeedImageCount > RingIDViewModel.Instance.FeedImageList.Count);
    //                        }
    //                        if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange._UCMyFeedPhotosChangeAlbum != null)
    //                        {

    //                            if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange.FeedImageCount > RingIDViewModel.Instance.FeedImageList.Count)
    //                            {
    //                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange._UCMyFeedPhotosChangeAlbum.ShowMore();
    //                            }
    //                            else
    //                            {
    //                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange._UCMyFeedPhotosChangeAlbum.HideShowMoreLoading();
    //                            }
    //                        }
    //                        if (RingIDViewModel.Instance != null && RingIDViewModel.Instance.MyAlbums != null)
    //                        {
    //                            if (RingIDViewModel.Instance.MyAlbums.FeedImageCount > RingIDViewModel.Instance.FeedImageList.Count)
    //                            {
    //                                RingIDViewModel.Instance.MyAlbums.AlbumFeedShowMore();
    //                            }
    //                            else
    //                            {
    //                                RingIDViewModel.Instance.MyAlbums.HideAlbumFeedShowMoreLoading();
    //                            }
    //                        }
    //                    }
    //                    #endregion
    //                }
    //                catch (Exception ex)
    //                {
    //                    log.Error("Error: AddMyImagesFromServer() => " + ex.Message + "\n" + ex.StackTrace);
    //                }
    //                finally
    //                {
    //                    //HelperMethods.ReleaseMemory(DefaultSettings.FEED_TYPE_ALL);
    //                }
    //            });
    //        }

    //        public void ProcessUpdateSendRegister(CallerDTO callerDto) //374
    //        {
    //            try
    //            {
    //                callerDto.CallIDLong = CallConstants.GetLong(callerDto.CallID);
    //                ConfigFile.USER_ID = DefaultSettings.LOGIN_USER_ID;
    //                if (CallConstants.CallSettings == null || CallConstants.CallSettings.FriendID == callerDto.FriendID)
    //                {
    //                    callerDto.IsIncomming = true;

    //                    int libLoaded = RingIDSDKWrapper.ipv_IsLoadRingIDSDK();
    //                    if (libLoaded == 0)
    //                    {
    //                        HelperMethods.InitRingIDSDK();
    //                    }

    //                    SessionStatus sessionStatus = RingIDSDKWrapper.ipv_CreateSession(callerDto.CallIDLong, ConfigFile.MediaType.IPV_MEDIA_AUDIO, callerDto.CallServerIP, callerDto.CallRegPort);
    //                    if (sessionStatus == SessionStatus.SESSION_CREATE_SUCCESSFULLY || sessionStatus == SessionStatus.ALREADY_SESSION_EXIST)
    //                    {
    //                        CallDictionaries.Instance.CALLERS_INFO_DICTIONARY.TryAdd(callerDto.CallID, callerDto);
    //                        CallSignals.SendRegButtonAction(callerDto.FriendID, callerDto.CallID, callerDto.CallServerIP, callerDto.CallRegPort, callerDto.calT);
    //                        Thread.Sleep(100);
    //                        for (int i = 1; i <= 120; i++)
    //                        {
    //                            if (!CallDictionaries.Instance.CALLERS_INFO_DICTIONARY.ContainsKey(callerDto.CallID))
    //                            {
    //                                break;
    //                            }
    //                            if (CallDictionaries.Instance.CALLERS_INFO_DICTIONARY.TryGetValue(callerDto.CallID).VoiceBindingPort > 0)
    //                            {
    //                                break;
    //                            }
    //                            Thread.Sleep(200);
    //                        }
    //                    }
    //                    else
    //                    {
    //                        log.Error("Error: ProcessUpdateSendRegister() : Session Creation failed");
    //                        //   P2PWrapper.ipv_CloseSession(callerDto.FriendID, ConfigFile.MediaType.IPV_MEDIA_AUDIO);
    //                        RingIDSDKWrapper.ipv_CloseSession(callerDto.CallIDLong, ConfigFile.MediaType.IPV_MEDIA_AUDIO);
    //                    }
    //                }
    //                else
    //                {
    //                    CallSignals.SendCallIn(callerDto.FriendID, callerDto.CallID, callerDto.CallServerIP, callerDto.CallRegPort);
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: ProcessUpdateSendRegister() ==> StartProcess update register ==>" + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }

    //        #region Notification



    //        public void NotificationListCount()
    //        {
    //            try
    //            {
    //                if (UCAllNotification.Instance != null)
    //                {
    //                    VMNotification.Instance.NotificationModelCount = RingIDViewModel.Instance.NotificationList.Count;
    //                }
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }
    //        public void AddSeemoreInNotificationList()
    //        {
    //            try
    //            {
    //                Application.Current.Dispatcher.Invoke(() =>
    //                {
    //                    //NotificationUtility.Instance.AddSeeMorePanel();
    //                    NotificationListCount();
    //                });

    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: AddSeemoreInNotificationList() ==> " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }

    //        public void ShowAllNotificationCount(int count)
    //        {
    //            Application.Current.Dispatcher.Invoke(() =>
    //                {
    //                    try
    //                    {
    //                        if (AppConstants.ALL_NOTIFICATION_COUNT > 0)
    //                        {
    //                            AppConstants.ALL_NOTIFICATION_COUNT = 0;
    //                        }
    //                        AppConstants.ALL_NOTIFICATION_COUNT = count;
    //                        RingIDViewModel.Instance.AllNotificationCounter = AppConstants.ALL_NOTIFICATION_COUNT;
    //                        RingIDViewModel.Instance.OnPropertyChanged("AllNotificationCounter");
    //                        new InsertIntoNotificationCountTable();
    //                    }
    //                    catch (Exception e)
    //                    {
    //                        log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //                    }
    //                });
    //        }

    //        public void ClearAllNotificationCount()
    //        {
    //            try
    //            {
    //                if (AppConstants.ALL_NOTIFICATION_COUNT > 0)
    //                {
    //                    Application.Current.Dispatcher.Invoke(() =>
    //                    {
    //                        AppConstants.ALL_NOTIFICATION_COUNT = 0;

    //                        RingIDViewModel.Instance.AllNotificationCounter = AppConstants.ALL_NOTIFICATION_COUNT;
    //                        RingIDViewModel.Instance.OnPropertyChanged("AllNotificationCounter");
    //                        new InsertIntoNotificationCountTable();
    //                        new ChangeNotificationStateRequest();
    //                    });
    //                }
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        #endregion

    //        public bool ClearChatNotificationCount()
    //        {
    //            try
    //            {
    //                if ( UCGuiRingID.Instance.ucFriendTabControlPanel.tabItem.Equals("ChatLogTab") && checkMainWindowIsActive())
    //                {
    //                    Application.Current.Dispatcher.Invoke(() =>
    //                    {
    //                        foreach (RecentDTO recent in ChatDictionaries.Instance.CHAT_UNREAD_MESSAGE.Values)
    //                        {
    //                            recent.isProcessed = true;
    //                        }
    //                        AppConstants.CHAT_NOTIFICATION_COUNT = 0;
    //                        RingIDViewModel.Instance.ChatNotificationCounter = AppConstants.CHAT_NOTIFICATION_COUNT;
    //                        RingIDViewModel.Instance.OnPropertyChanged("ChatNotificationCounter");
    //                        changeTaskOverlay();
    //                        new InsertIntoNotificationCountTable();
    //                    });
    //                    return true;
    //                }
    //                return false;
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //                return false;
    //            }
    //        }

    //        public bool SetandShowChatNotificationCount(bool ignoreChecking = false)
    //        {
    //            try
    //            {
    //                if (ignoreChecking || ( (!UCGuiRingID.Instance.ucFriendTabControlPanel.tabItem.Equals("ChatLogTab")) || checkMainWindowIsActive() == false))
    //                {
    //                    Application.Current.Dispatcher.Invoke(() =>
    //                    {
    //                        int count = 0;
    //                        foreach (RecentDTO recent in ChatDictionaries.Instance.CHAT_UNREAD_MESSAGE.Values)
    //                        {
    //                            if (recent.isProcessed == false)
    //                                count++;
    //                        }
    //                        AppConstants.CHAT_NOTIFICATION_COUNT = count;
    //                        RingIDViewModel.Instance.ChatNotificationCounter = AppConstants.CHAT_NOTIFICATION_COUNT;
    //                        RingIDViewModel.Instance.OnPropertyChanged("ChatNotificationCounter");
    //                        changeTaskOverlay();
    //                        new InsertIntoNotificationCountTable();
    //                    });
    //                    return true;
    //                }
    //                return false;
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //                return false;
    //            }
    //        }

    //        public void ClearCallNotificationCount(bool ignoreChecking = false)
    //        {
    //            try
    //            {
    //                if (ignoreChecking || ( UCGuiRingID.Instance.ucFriendTabControlPanel.tabItem.Equals("CallLogTab") && checkMainWindowIsActive()))
    //                {
    //                    Application.Current.Dispatcher.Invoke(() =>
    //                    {
    //                        AppConstants.CALL_NOTIFICATION_COUNT = 0;
    //                        RingIDViewModel.Instance.CallNotificationCounter = AppConstants.CALL_NOTIFICATION_COUNT;
    //                        RingIDViewModel.Instance.OnPropertyChanged("CallNotificationCounter");
    //                        changeTaskOverlay();
    //                        new InsertIntoNotificationCountTable();
    //                    });
    //                }
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        public void IncreaseCallNotificationCount()
    //        {
    //            try
    //            {
    //                if ( (!UCGuiRingID.Instance.ucFriendTabControlPanel.tabItem.Equals("CallLogTab") || checkMainWindowIsActive() == false))
    //                {
    //                    Application.Current.Dispatcher.Invoke(() =>
    //                    {
    //                        AppConstants.CALL_NOTIFICATION_COUNT++;
    //                        RingIDViewModel.Instance.CallNotificationCounter = AppConstants.CALL_NOTIFICATION_COUNT;
    //                        RingIDViewModel.Instance.OnPropertyChanged("AllNotificationCounter");
    //                        changeTaskOverlay();
    //                        new InsertIntoNotificationCountTable();
    //                    });
    //                }
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        public void AddFriendsNotificationForIncomingRequest()
    //        {
    //            FriendListLoadUtility.AddFriendsNotificationForIncomingRequest();
    //        }

    //        public void AddSuggestions(List<UserBasicInfoDTO> list)
    //        {
    //            try
    //            {
    //                SuggestionListLoadUtility.LoadDataFromServer(list);
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        #endregion

    //        public void AddFriendImagesFromServer(long userIdentity, int friendImageType, int friendImageCount, List<ImageDTO> friendImageList)
    //        {
    //            //  Application.Current.Dispatcher.Invoke(() =>
    //            //{
    //            try
    //            {
    //                UCFriendProfile ucFriendProfile = null;
    //                if (UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.TryGetValue(userIdentity, out ucFriendProfile) && ucFriendProfile != null)
    //                {
    //                    List<ImageDTO> sortedList = (from l in friendImageList
    //                                                 orderby l.Time descending
    //                                                 select l).ToList();
    //                    #region "Image full view"
    //                    if (UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView != null)
    //                        UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.AddIntoImageList(friendImageType, userIdentity, sortedList);
    //                    #endregion "Image full view"
    //                    if (friendImageType == SettingsConstants.TYPE_PROFILE_IMAGE)
    //                    {
    //                        if (ucFriendProfile._UCFriendPhotos != null)
    //                        {
    //                            ucFriendProfile._UCFriendPhotos.FriendProfileImageCount = friendImageCount;
    //                        }

    //                        ucFriendProfile.LoadFriendProfileImage(sortedList, friendImageCount);

    //                    }
    //                    else if (friendImageType == SettingsConstants.TYPE_COVER_IMAGE)
    //                    {
    //                        if (ucFriendProfile._UCFriendPhotos != null)
    //                        {
    //                            ucFriendProfile._UCFriendPhotos.FriendCoverImageCount = friendImageCount;
    //                        }
    //                        ucFriendProfile.LoadFriendCoverImage(sortedList, friendImageCount);

    //                    }
    //                    else if (friendImageType == SettingsConstants.TYPE_NORMAL_BOOK_IMAGE)
    //                    {
    //                        if (ucFriendProfile._UCFriendPhotos != null)
    //                        {
    //                            ucFriendProfile._UCFriendPhotos.FriendFeedImageCount = friendImageCount;
    //                        }

    //                        ucFriendProfile.LoadFriendFeedImage(sortedList, friendImageCount);

    //                    }
    //                }

    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: AddFriendImagesFromServer() ==> " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //            //finally
    //            //{
    //            //    HelperMethods.ReleaseMemory(DefaultSettings.FEED_TYPE_FRIEND, userIdentity);
    //            //}
    //            //});
    //        }

    //        public void GetSingleImageDTOFromServer(ImageDTO imageDTO, long nfId)
    //        {
    //            try
    //            {
    //                if ( UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView != null)
    //                {
    //                    UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageFullNameProfileImage.ChangeAllValues(imageDTO);
    //                }
    //                if (nfId > 0)
    //                {
    //                    FeedModel feedModel = (FeedModel)HelperMethodsAuth.NewsFeedHandlerInstance.GetFeedModelByNfid(nfId);
    //                    if (feedModel != null && feedModel.SingleImageFeedModel != null && feedModel.SingleImageFeedModel.ImageId == imageDTO.ImageId)
    //                    {
    //                        feedModel.SingleImageFeedModel.LoadData(imageDTO);
    //                        feedModel.ILike = (short)imageDTO.iLike; feedModel.NumberOfLikes = imageDTO.NumberOfLikes;
    //                        feedModel.IComment = (short)imageDTO.iComment; feedModel.NumberOfComments = imageDTO.NumberOfComments;
    //                        feedModel.OnPropertyChanged("CurrentInstance");
    //                    }
    //                    FeedDTO dto = null;
    //                    if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.TryGetValue(nfId, out dto) && dto.ImageList != null && dto.ImageList.Count == 1)
    //                    {
    //                        dto.ImageList[0] = imageDTO;
    //                        dto.ILike = (short)imageDTO.iLike; dto.NumberOfLikes = imageDTO.NumberOfLikes;
    //                        dto.IComment = (short)imageDTO.iComment; dto.NumberOfComments = imageDTO.NumberOfComments;
    //                    }
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: GetSingleImageDTOFromServer() ==> " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }

    //        public void NoImageFoundInMyAlbum(string albumId)
    //        {
    //            Application.Current.Dispatcher.Invoke(() =>
    //            {
    //                try
    //                {
    //                    if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos != null)
    //                    {
    //                        if (albumId == DefaultSettings.PROFILE_IMAGE_ALBUM_ID)
    //                        {
    //                            if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyProfilePhotosAlbum != null)
    //                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyProfilePhotosAlbum.ShowHideShowMoreLoading
    //                                    (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.ProfileImageCount > RingIDViewModel.Instance.ProfileImageList.Count);
    //                            else
    //                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.IsVisibleProfilePhotosLoader = Visibility.Collapsed;
    //                        }
    //                        else if (albumId == DefaultSettings.COVER_IMAGE_ALBUM_ID)
    //                        {
    //                            if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyCoverPhotosAlbum != null)
    //                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyCoverPhotosAlbum.ShowHideShowMoreLoading
    //                                    (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.CoverImageCount > RingIDViewModel.Instance.CoverImageList.Count);
    //                            else
    //                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.IsVisibleCoverPhotosLoader = Visibility.Collapsed;
    //                        }
    //                        else if (albumId == DefaultSettings.FEED_IMAGE_ALBUM_ID)
    //                        {
    //                            if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyFeedPhotosAlbum != null)
    //                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos._UCMyFeedPhotosAlbum.ShowHideShowMoreLoading
    //                                    (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.FeedImageCount > RingIDViewModel.Instance.FeedImageList.Count);
    //                            else
    //                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.IsVisibleFeedPhotosLoader = Visibility.Collapsed;
    //                        }
    //                    }
    //                }
    //                catch (Exception ex)
    //                {
    //                    log.Error("Error: NoImageFoundInMyAlbum() ==> " + ex.Message + "\n" + ex.StackTrace);
    //                }

    //            });
    //        }

    //        public void NoImageFoundInFriendsAlbum(long uid, string albumId)
    //        {
    //            Application.Current.Dispatcher.Invoke(() =>
    //            {
    //                try
    //                {
    //                    UCFriendProfile ucFriendProfile = null;
    //                    if (UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.TryGetValue(uid, out ucFriendProfile) && ucFriendProfile._UCFriendPhotos != null)
    //                    {
    //                        if (albumId == DefaultSettings.PROFILE_IMAGE_ALBUM_ID)
    //                        {
    //                            if (ucFriendProfile._UCFriendPhotos._UCFriendProfilePhotosAlbum != null)
    //                                ucFriendProfile._UCFriendPhotos._UCFriendProfilePhotosAlbum.ShowHideShowMoreLoading(ucFriendProfile.FriendProfileImageList.Count < ucFriendProfile._UCFriendPhotos.FriendProfileImageCount);
    //                            else
    //                                ucFriendProfile._UCFriendPhotos.IsVisibleProfilePhotosLoader = Visibility.Collapsed;
    //                        }
    //                        else if (albumId == DefaultSettings.COVER_IMAGE_ALBUM_ID)
    //                        {
    //                            if (ucFriendProfile._UCFriendPhotos._UCFriendCoverPhotosAlbum != null)
    //                                ucFriendProfile._UCFriendPhotos._UCFriendCoverPhotosAlbum.ShowHideShowMoreLoading(ucFriendProfile.FriendCoverImageList.Count < ucFriendProfile._UCFriendPhotos.FriendCoverImageCount);
    //                            else
    //                                ucFriendProfile._UCFriendPhotos.IsVisibleCoverPhotosLoader = Visibility.Collapsed;
    //                        }
    //                        else if (albumId == DefaultSettings.FEED_IMAGE_ALBUM_ID)
    //                        {
    //                            if (ucFriendProfile._UCFriendPhotos._UCFriendFeedPhotosAlbum != null)
    //                                ucFriendProfile._UCFriendPhotos._UCFriendFeedPhotosAlbum.ShowHideShowMoreLoading(ucFriendProfile.FriendFeedImageList.Count < ucFriendProfile._UCFriendPhotos.FriendFeedImageCount);
    //                            else
    //                                ucFriendProfile._UCFriendPhotos.IsVisibleFeedPhotosLoader = Visibility.Collapsed;
    //                        }
    //                    }
    //                }
    //                catch (Exception ex)
    //                {
    //                    log.Error("Error: NoImageFoundInFriendsAlbum() ==> " + ex.Message + "\n" + ex.StackTrace);
    //                }

    //            });
    //        }
    //        #region Unnecessary
    //        //private static void LoadFriendFeedImage(UCFriendProfile ucFriendProfile, List<ImageDTO> sortedList)
    //        //{
    //        //    foreach (var image in sortedList)
    //        //    {
    //        //        lock (ucFriendProfile._UCFriendPhotos.FriendFeedImageList)
    //        //        {
    //        //            if (!ucFriendProfile._UCFriendPhotos.FriendFeedImageList.Any(p => p.ImageId == image.ImageId))
    //        //                ucFriendProfile._UCFriendPhotos.FriendFeedImageList.Add(new ImageModel(image));
    //        //            ucFriendProfile._UCFriendPhotos.FriendFeedImageCount = image.TotalImagesInAlbum;
    //        //        }
    //        //    }

    //        //    ucFriendProfile._UCFriendPhotos._UCFriendFeedPhotosAlbum.FeedImageControl.ItemsSource = null;
    //        //    ucFriendProfile._UCFriendPhotos._UCFriendFeedPhotosAlbum.FeedImageControl.ItemsSource = ucFriendProfile._UCFriendPhotos.FriendFeedImageList;

    //        //    if (ucFriendProfile._UCFriendPhotos.FriendProfileImageCount > ucFriendProfile._UCFriendPhotos.FriendFeedImageList.Count)
    //        //    {
    //        //        ucFriendProfile._UCFriendPhotos._UCFriendFeedPhotosAlbum.ShowMore();
    //        //    }
    //        //    else
    //        //    {
    //        //        ucFriendProfile._UCFriendPhotos._UCFriendFeedPhotosAlbum.HideShowMoreLoading();
    //        //    }
    //        //}

    //        //private static void LoadFriendCoverImage(UCFriendProfile ucFriendProfile, List<ImageDTO> sortedList)
    //        //{
    //        //    foreach (var image in sortedList)
    //        //    {
    //        //        lock (ucFriendProfile._UCFriendPhotos.FriendCoverImageList)
    //        //        {
    //        //            if (!ucFriendProfile._UCFriendPhotos.FriendCoverImageList.Any(p => p.ImageId == image.ImageId))
    //        //                ucFriendProfile._UCFriendPhotos.FriendCoverImageList.Add(new ImageModel(image));
    //        //            ucFriendProfile._UCFriendPhotos.FriendCoverImageCount = image.TotalImagesInAlbum;
    //        //        }
    //        //    }

    //        //    ucFriendProfile._UCFriendPhotos._UCFriendCoverPhotosAlbum.CoverImageControl.ItemsSource = null;
    //        //    ucFriendProfile._UCFriendPhotos._UCFriendCoverPhotosAlbum.CoverImageControl.ItemsSource = ucFriendProfile._UCFriendPhotos.FriendCoverImageList;

    //        //    if (ucFriendProfile._UCFriendPhotos.FriendCoverImageCount > ucFriendProfile._UCFriendPhotos.FriendCoverImageList.Count)
    //        //    {
    //        //        ucFriendProfile._UCFriendPhotos._UCFriendCoverPhotosAlbum.ShowMore();
    //        //    }
    //        //    else
    //        //    {
    //        //        ucFriendProfile._UCFriendPhotos._UCFriendCoverPhotosAlbum.HideShowMoreLoading();
    //        //    }
    //        //} 
    //        #endregion


    //        #region Images

    //        public void LikeImageComment(long nfid, long imgId, CommentDTO commentDTO)
    //        {
    //            try
    //            {
    //                //if ( UCGuiRingID.Instance.ucBasicImageView.ImageComments != null && UCGuiRingID.Instance.ucBasicImageView.clickedImageID == imageId)
    //                //{
    //                //    Application.Current.Dispatcher.Invoke(() =>
    //                //        {
    //                //            if (UCGuiRingID.Instance.ucBasicImageView.ImageComments.ImageCommentList.Any(c => c.CommentId == commentDTO.CommentId))
    //                //            {
    //                //                CommentModel commentModel = UCGuiRingID.Instance.ucBasicImageView.ImageComments.ImageCommentList.Where(c => c.CommentId == commentDTO.CommentId).First();
    //                //                commentModel.ILikeComment = commentDTO.ILikeComment;
    //                //                commentModel.TotalLikeComment = commentDTO.TotalLikeComment;
    //                //                lock (UCGuiRingID.Instance.ucBasicImageView.ImageComments.ImageCommentList)
    //                //                {
    //                //                    int idx = UCGuiRingID.Instance.ucBasicImageView.ImageComments.ImageCommentList.IndexOf(UCGuiRingID.Instance.ucBasicImageView.ImageComments.ImageCommentList.Where(c => c.CommentId == commentDTO.CommentId).First());
    //                //                    UCGuiRingID.Instance.ucBasicImageView.ImageComments.ImageCommentList.RemoveAt(idx);
    //                //                    UCGuiRingID.Instance.ucBasicImageView.ImageComments.ImageCommentList.Insert(idx, commentModel);
    //                //                }
    //                //            }
    //                //            else
    //                //            {
    //                //              UIHelperMethods.ShowErrorMessageBox("Unable To Like/Unlike", "Failed!");
    //                //            }
    //                //        });
    //                //}
    //                if (UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView != null && UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments != null &&
    //                    UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.clickedImageID == imgId)
    //                {
    //                    lock (UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments.ImageCommentList)
    //                    {
    //                        CommentModel model = UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments.ImageCommentList.Where(P => P.CommentId == commentDTO.CommentId).FirstOrDefault();
    //                        if (model != null)
    //                        {
    //                            model.ILikeComment = commentDTO.ILikeComment;
    //                            model.TotalLikeComment = commentDTO.TotalLikeComment;
    //                        }
    //                    }
    //                }
    //                FeedModel feedModel = (nfid > 0) ? ((FeedModel)HelperMethodsAuth.NewsFeedHandlerInstance.GetFeedModelByNfid(nfid)) : null;
    //                if (feedModel != null && feedModel.SingleImageFeedModel != null && feedModel.CommentList.Count > 0)
    //                {
    //                    CommentModel model = feedModel.CommentList.Where(P => P.CommentId == commentDTO.CommentId).FirstOrDefault();
    //                    if (model != null)
    //                    {
    //                        model.ILikeComment = commentDTO.ILikeComment;
    //                        model.TotalLikeComment = commentDTO.TotalLikeComment;
    //                        //feedModel.OnPropertyChanged("CurrentInstance");
    //                        //DeleteCommentFromMediaDictionary(contentId, UCGuiRingID.Instance.ucBasicMediaView.ImageFullNameProfileImage.IComment);
    //                    }
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: LikeImageComment() ==> " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }
    //        public void FetchImageCommentsUI()
    //        {
    //            if (UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView != null)
    //            {
    //                UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.StopandRemoveAnimation();
    //            }
    //        }

    //        public void FetchImageCommentsFromServer(long imageId, List<CommentDTO> list, long nfid)
    //        {
    //            try
    //            {
    //                Application.Current.Dispatcher.BeginInvoke(() =>
    //                {
    //                    //System.Diagnostics.Debug.WriteLine("LoadComments==>" + list.Count);
    //                    ObservableCollection<CommentModel> CommentsINSingleImageFeedModel = null;
    //                    ObservableCollection<CommentModel> CommentsINImageView = null;
    //                    FeedModel feedModel = null;
    //                    long postOwnerUid = 0;
    //                    if (nfid > 0)
    //                    {
    //                        feedModel = (FeedModel)HelperMethodsAuth.NewsFeedHandlerInstance.GetFeedModelByNfid(nfid);
    //                        if ((list == null || list.Count == 0) && feedModel != null)
    //                        {
    //                            feedModel.NoMoreComments = true;
    //                            feedModel.OnPropertyChanged("CurrentInstance");
    //                            FetchImageCommentsUI();
    //                            if (UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView != null && UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.clickedImageID == imageId && UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments != null)
    //                            {
    //                                UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments.PreviousCommentsVisibility = Visibility.Collapsed;
    //                            }
    //                            return;
    //                        }
    //                        if (feedModel != null)
    //                        {
    //                            if (feedModel.SingleImageFeedModel != null)
    //                                CommentsINSingleImageFeedModel = feedModel.CommentList;
    //                            if (feedModel.UserShortInfoModel != null && feedModel.UserShortInfoModel.UserIdentity > 0)
    //                                postOwnerUid = feedModel.UserShortInfoModel.UserIdentity;
    //                        }
    //                    }
    //                    if (UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView != null && UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.clickedImageID == imageId)
    //                    {
    //                        if (UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments != null)
    //                        {
    //                            UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments.ShowWatch(false);
    //                            CommentsINImageView = UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments.ImageCommentList;
    //                            UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments.PreviousCommentsVisibility = (UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageFullNameProfileImage.NumberOfComments > CommentsINImageView.Count) ? Visibility.Visible : Visibility.Collapsed;
    //                        }
    //                        if (UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.clickedUserID > 0)
    //                            postOwnerUid = UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.clickedUserID;
    //                    }
    //                    if ((CommentsINImageView != null || CommentsINSingleImageFeedModel != null) && list != null)
    //                    {
    //                        //list = list.OrderByDescending(p => p.Time).ToList();
    //                        foreach (var comment in list)
    //                        {
    //                            CommentModel commentFeed = null, commentImgView = null, commentmodel = null;
    //                            if (CommentsINSingleImageFeedModel != null) commentFeed = CommentsINSingleImageFeedModel.Where(p => p.CommentId == comment.CommentId).FirstOrDefault();
    //                            if (CommentsINImageView != null) commentImgView = CommentsINImageView.Where(p => p.CommentId == comment.CommentId).FirstOrDefault();
    //                            if (commentFeed == null && commentImgView == null)
    //                            {
    //                                commentmodel = new CommentModel();
    //                                GC.SuppressFinalize(commentmodel);
    //                                if (CommentsINSingleImageFeedModel != null) CommentsINSingleImageFeedModel.Add(commentmodel);
    //                                if (CommentsINImageView != null) CommentsINImageView.Add(commentmodel);
    //                            }
    //                            else if (commentFeed != null && commentImgView == null)
    //                            {
    //                                commentmodel = commentFeed;
    //                                if (CommentsINImageView != null) CommentsINImageView.Add(commentmodel);
    //                            }
    //                            else if (commentImgView != null && commentFeed == null)
    //                            {
    //                                commentmodel = commentImgView;
    //                                if (CommentsINSingleImageFeedModel != null) CommentsINSingleImageFeedModel.Add(commentmodel);
    //                            }
    //                            else commentmodel = commentFeed;

    //                            commentmodel.LoadData(comment);
    //                            commentmodel.ImageId = imageId;
    //                            if (nfid > 0) commentmodel.NewsfeedId = nfid;
    //                            if (postOwnerUid > 0) commentmodel.PostOwnerUid = postOwnerUid;
    //                            commentmodel.IsEditMode = false;
    //                        }
    //                        if (CommentsINSingleImageFeedModel != null) feedModel.CommentList = new ObservableCollection<CommentModel>(CommentsINSingleImageFeedModel.OrderBy(a => a.Time));
    //                        if (CommentsINImageView != null) UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments.ImageCommentList = new ObservableCollection<CommentModel>(CommentsINImageView.OrderBy(a => a.Time));
    //                    }
    //                    if (UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView != null && UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.clickedImageID == imageId)
    //                    {
    //                        UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments.PreviousCommentsVisibility = (UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageFullNameProfileImage.NumberOfComments > CommentsINImageView.Count) ? Visibility.Visible : Visibility.Collapsed;
    //                    }
    //                    if (feedModel != null) feedModel.OnPropertyChanged("CurrentInstance");
    //                    FetchImageCommentsUI();
    //                }
    //                );
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: FetchImageCommentsFromServer() ==> " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }
    //        //public void FetchImageCommentsFromServer(long imageId, CommentDTO list)
    //        //{
    //        //    try
    //        //    {
    //        //        if (UCGuiRingID.Instance.ucBasicImageView.ImageComments != null)
    //        //        {
    //        //            UCGuiRingID.Instance.ucBasicImageView.ImageComments.LoadComments(imageId, list);
    //        //        }

    //        //    }
    //        //    catch (Exception ex)
    //        //    {
    //        //        logger.Error("FetchImageCommentsFromServer==>" + ex.Message + "\n" + ex.StackTrace);
    //        //    }
    //        //}
    //        public void FetchImageCommentLikersFromServer(List<UserBasicInfoDTO> likers, long imgId, long commentId)
    //        {
    //            try
    //            {
    //                Application.Current.Dispatcher.Invoke(() =>
    //                {
    //                    if (LikeListViewWrapper != null && LikeListViewWrapper.ucLikeListView != null)
    //                    {
    //                        foreach (UserBasicInfoDTO liker in likers)
    //                        {
    //                            UserBasicInfoModel userModel = GetExistingorNewUserBasicModelFromDTO(liker);
    //                            if (LikeListViewWrapper.ucLikeListView.ImageId == imgId &&
    //                                LikeListViewWrapper.ucLikeListView.type == 3 && LikeListViewWrapper.ucLikeListView.CommentId == commentId)
    //                            {
    //                                LikeListViewWrapper.ucLikeListView.LikeList.Add(userModel);
    //                            }
    //                        }

    //                        LikeListViewWrapper.ucLikeListView.SetVisibilities();
    //                    }
    //                }
    //                );

    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: FetchImageCommentLikersFromServer() ==> " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }

    //        public void OnLikerListLoadFailed()
    //        {
    //            try
    //            {
    //                Application.Current.Dispatcher.BeginInvoke(() =>
    //                {
    //                    if (LikeListViewWrapper != null && LikeListViewWrapper.ucLikeListView != null)
    //                    {
    //                        LikeListViewWrapper.ucLikeListView.ClosePopUp();
    //                    }
    //                });
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        public UserBasicInfoModel GetExistingorNewUserBasicModelFromDTO(UserBasicInfoDTO dto)
    //        {
    //            try
    //            {
    //                UserBasicInfoModel model = (dto.UserIdentity == DefaultSettings.LOGIN_USER_ID) ? RingIDViewModel.Instance.MyBasicInfoModel : RingIDViewModel.Instance.GetFriendBasicInfoModelByID(dto.UserIdentity);
    //                if (model == null)
    //                {
    //                    model = new UserBasicInfoModel(dto);
    //                    model.ShortInfoModel.FriendShipStatus = 0;
    //                    FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
    //                    //   AddRemoveInCollections.AddIntoUnknownFriendList(model);
    //                }
    //                return model;
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //                return new UserBasicInfoModel(dto);
    //            }
    //        }
    //        public void OnListLikesorCommentsFailed()
    //        {
    //            Application.Current.Dispatcher.Invoke(() =>
    //            {
    //                try
    //                {
    //                    if (LikeListViewWrapper != null && LikeListViewWrapper.ucLikeListView != null)
    //                    {
    //                        LikeListViewWrapper.ucLikeListView.HideLoaderandSeeMore();
    //                    }
    //                }
    //                catch (Exception ex)
    //                {
    //                    log.Error("Error: OnListLikesorCommentsFailed() ==> " + ex.Message + "\n" + ex.StackTrace);
    //                }
    //            });
    //        }


    //        public void AddImageComment(long imageId, CommentDTO comment, long nfid, bool update = false)
    //        {
    //            try
    //            {
    //                Application.Current.Dispatcher.Invoke(() =>
    //                {
    //                    //if ( UCGuiRingID.Instance.ucBasicImageView.ImageComments != null && UCGuiRingID.Instance.ucBasicImageView.clickedImageID == imageId)
    //                    //{
    //                    //    lock (UCGuiRingID.Instance.ucBasicImageView.ImageComments.ImageCommentList)
    //                    //    {
    //                    //        CommentModel model = new CommentModel();
    //                    //        model.LoadData(comment);
    //                    //        model.ImageId = imageId;
    //                    //        UCGuiRingID.Instance.ucBasicImageView.ImageComments.ImageCommentList.Insert(0, model);
    //                    //        UCGuiRingID.Instance.ucBasicImageView.ImageFullNameProfileImage.ChangeNumberOfComments(true);
    //                    //        if (comment.UserIdentity == DefaultSettings.LOGIN_USER_ID)
    //                    //            UCGuiRingID.Instance.ucBasicImageView.ImageFullNameProfileImage.IComment = 1;
    //                    //    }
    //                    //}
    //                    ObservableCollection<CommentModel> CommentsINSingleImgFeedModel = null;
    //                    ObservableCollection<CommentModel> CommentsINImgView = null;
    //                    FeedModel feedModel = null;
    //                    long postOwnerUid = 0;
    //                    if (nfid > 0)
    //                    {
    //                        feedModel = (FeedModel)HelperMethodsAuth.NewsFeedHandlerInstance.GetFeedModelByNfid(nfid);
    //                        if (feedModel != null)
    //                        {
    //                            if (feedModel.SingleImageFeedModel != null)
    //                            {
    //                                CommentsINSingleImgFeedModel = feedModel.CommentList;
    //                                feedModel.NumberOfComments = feedModel.NumberOfComments + 1;
    //                                feedModel.SingleImageFeedModel.NumberOfComments = (feedModel.SingleImageFeedModel.NumberOfComments + 1);
    //                                if (!update)
    //                                {
    //                                    feedModel.IComment = 1;
    //                                    feedModel.SingleImageFeedModel.IComment = 1;
    //                                }
    //                            }
    //                            if (feedModel.UserShortInfoModel != null && feedModel.UserShortInfoModel.UserIdentity > 0)
    //                                postOwnerUid = feedModel.UserShortInfoModel.UserIdentity;
    //                        }
    //                    }
    //                    if ( UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView != null &&
    //                        UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.clickedImageID == imageId)
    //                    {
    //                        if (UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments != null)
    //                            CommentsINImgView = UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments.ImageCommentList;
    //                        if (UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageFullNameProfileImage != null)
    //                        {
    //                            UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageFullNameProfileImage.NumberOfComments = UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageFullNameProfileImage.NumberOfComments + 1;
    //                            if (!update) UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageFullNameProfileImage.IComment = 1;
    //                        }
    //                        if (UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.clickedUserID > 0)
    //                            postOwnerUid = UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.clickedUserID;
    //                    }
    //                    if (CommentsINImgView != null || CommentsINSingleImgFeedModel != null)
    //                    {
    //                        CommentModel commentFeed = null, commentMdaView = null, commentmodel = null;
    //                        if (CommentsINSingleImgFeedModel != null) commentFeed = CommentsINSingleImgFeedModel.Where(p => p.CommentId == comment.CommentId).FirstOrDefault();
    //                        if (CommentsINImgView != null) commentMdaView = CommentsINImgView.Where(p => p.CommentId == comment.CommentId).FirstOrDefault();
    //                        if (commentFeed == null && commentMdaView == null)
    //                        {
    //                            commentmodel = new CommentModel();
    //                            if (CommentsINSingleImgFeedModel != null) CommentsINSingleImgFeedModel.Add(commentmodel);
    //                            if (CommentsINImgView != null) CommentsINImgView.Add(commentmodel);
    //                        }
    //                        else if (commentFeed != null && commentMdaView == null)
    //                        {
    //                            commentmodel = commentFeed;
    //                            if (CommentsINImgView != null) CommentsINImgView.Insert(0, commentmodel);
    //                        }
    //                        else if (commentMdaView != null && commentFeed == null)
    //                        {
    //                            commentmodel = commentMdaView;
    //                            if (CommentsINSingleImgFeedModel != null) CommentsINSingleImgFeedModel.Add(commentmodel);
    //                        }
    //                        else commentmodel = commentFeed;

    //                        commentmodel.LoadData(comment);
    //                        commentmodel.ImageId = imageId;
    //                        if (nfid > 0) commentmodel.NewsfeedId = nfid;
    //                        if (postOwnerUid > 0) commentmodel.PostOwnerUid = postOwnerUid;
    //                        commentmodel.IsEditMode = false;
    //                    }
    //                    if (feedModel != null && feedModel.SingleImageFeedModel != null)
    //                        feedModel.OnPropertyChanged("CurrentInstance");
    //                });

    //                //CommentDTO comntDto = null;
    //                //lock (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST)
    //                //{
    //                //    if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.ContainsKey(imageId))
    //                //    {
    //                //        if (NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].TryGetValue(comment.CommentId, out comntDto))
    //                //        {
    //                //            NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId][comntDto.CommentId] = comment;
    //                //        }
    //                //        else
    //                //        {
    //                //            NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Add(comment.CommentId, comment);
    //                //        }
    //                //    }
    //                //    else
    //                //    {
    //                //        NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST.Add(imageId, new Dictionary<long, CommentDTO>());
    //                //        NewsFeedDictionaries.Instance.TEMP_IMAGE_COMMENT_LIST[imageId].Add(comment.CommentId, comment);
    //                //    }
    //                //}
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: AddImageComment() ==> " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }


    //        public void DeleteImageComment(long imgId, long commentId, long nfid)
    //        {
    //            try
    //            {
    //                Application.Current.Dispatcher.Invoke(() =>
    //                {
    //                    if (UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView != null && UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments != null &&
    //                        UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.clickedImageID == imgId)
    //                    {
    //                        lock (UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments.ImageCommentList)
    //                        {
    //                            CommentModel model = UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments.ImageCommentList.Where(P => P.CommentId == commentId).FirstOrDefault();
    //                            if (model != null)
    //                            {
    //                                UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments.ImageCommentList.Remove(model);
    //                                UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageFullNameProfileImage.ChangeNumberOfComments(false);
    //                                CommentModel anyMoreCommentbyMe = UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments.ImageCommentList.Where(P => P.UserShortInfoModel.UserIdentity == DefaultSettings.LOGIN_USER_ID).FirstOrDefault();
    //                                if (anyMoreCommentbyMe == null)
    //                                    UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageFullNameProfileImage.IComment = 0;
    //                            }
    //                        }
    //                    }
    //                    FeedModel feedModel = (nfid > 0) ? ((FeedModel)HelperMethodsAuth.NewsFeedHandlerInstance.GetFeedModelByNfid(nfid)) : null;
    //                    if (feedModel != null && feedModel.SingleImageFeedModel != null)
    //                    {
    //                        feedModel.NumberOfComments = feedModel.NumberOfComments - 1;
    //                        feedModel.SingleImageFeedModel.NumberOfComments = feedModel.SingleImageFeedModel.NumberOfComments - 1;
    //                        CommentModel model = feedModel.CommentList.Where(P => P.CommentId == commentId).FirstOrDefault();
    //                        if (model != null)
    //                        {
    //                            feedModel.CommentList.Remove(model);
    //                            if (!feedModel.CommentList.Any(P => P.UserShortInfoModel.UserIdentity == DefaultSettings.LOGIN_USER_ID))
    //                            {
    //                                feedModel.IComment = 0;
    //                                feedModel.SingleImageFeedModel.IComment = 0;
    //                            }
    //                            //DeleteCommentFromMediaDictionary(contentId, UCGuiRingID.Instance.ucBasicMediaView.ImageFullNameProfileImage.IComment);
    //                        }
    //                        feedModel.OnPropertyChanged("CurrentInstance");
    //                    }
    //                });
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: DeleteImageComment() ==> " + ex.Message + "\n" + ex.StackTrace);

    //            }
    //        }

    //        public void EditImageComment(long imgId, long commentId, string cmnt, long nfid, bool sc, bool update = false)
    //        {
    //            Application.Current.Dispatcher.Invoke(() =>
    //            {
    //                try
    //                {
    //                    //if (UCGuiRingID.Instance.ucBasicImageView.ImageComments != null
    //                    //         && UCGuiRingID.Instance.ucBasicImageView.clickedImageID == imgId)
    //                    //{
    //                    //    lock (UCGuiRingID.Instance.ucBasicImageView.ImageComments.ImageCommentList)
    //                    //    {
    //                    //        CommentModel model = UCGuiRingID.Instance.ucBasicImageView.ImageComments.ImageCommentList.Where(P => P.CommentId == commentId).FirstOrDefault();
    //                    //        if (model != null)
    //                    //        {
    //                    //            model.Comment = cmnt;
    //                    //            model.IsEditMode = false;
    //                    //        }
    //                    //    }
    //                    //}
    //                    ObservableCollection<CommentModel> CommentsINSingleImageFeedModel = null;
    //                    ObservableCollection<CommentModel> CommentsINImgView = null;
    //                    FeedModel feedModel = null;
    //                    if (nfid > 0)
    //                    {
    //                        feedModel = (FeedModel)HelperMethodsAuth.NewsFeedHandlerInstance.GetFeedModelByNfid(nfid);
    //                        if (feedModel != null && feedModel.SingleImageFeedModel != null)
    //                        {
    //                            CommentsINSingleImageFeedModel = feedModel.CommentList;
    //                        }
    //                    }
    //                    if ( UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView != null &&
    //                        UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.clickedImageID == imgId && UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments != null)
    //                    {
    //                        CommentsINImgView = UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments.ImageCommentList;
    //                    }
    //                    if (CommentsINImgView != null || CommentsINSingleImageFeedModel != null)
    //                    {
    //                        CommentModel commentFeed = null, commentImgView = null;
    //                        if (CommentsINSingleImageFeedModel != null) commentFeed = CommentsINSingleImageFeedModel.Where(p => p.CommentId == commentId).FirstOrDefault();
    //                        if (CommentsINImgView != null) commentImgView = CommentsINImgView.Where(p => p.CommentId == commentId).FirstOrDefault();
    //                        if (commentFeed != null)
    //                        {
    //                            commentFeed.Comment = cmnt;
    //                            commentFeed.ShowContinue = sc;
    //                            commentFeed.IsEditMode = false;
    //                        }
    //                        else if (commentImgView != null)
    //                        {
    //                            commentImgView.Comment = cmnt;
    //                            commentImgView.ShowContinue = sc;
    //                            commentImgView.IsEditMode = false;
    //                        }
    //                    }
    //                }
    //                catch (Exception ex)
    //                {
    //                    log.Error("Error: EditImageComment() ==> " + ex.Message + "\n" + ex.StackTrace);

    //                }
    //            });

    //        }
    //        #endregion

    //        public void OnLikeUnlikeFailed(Object obj, string msg, long cmntId = 0)
    //        {
    //            try
    //            {
    //                CustomMessageBox.ShowError(msg, "Failed!");
    //                if (obj is long)
    //                {
    //                    long imgId = (long)obj;
    //                    if (cmntId > 0 &&  UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView != null &&
    //                        UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments != null && UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments.ImageCommentList.Count > 0)
    //                    {
    //                        CommentModel commentModel = UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments.ImageCommentList.Where(P => P.CommentId == cmntId).FirstOrDefault();
    //                        if (commentModel != null)
    //                        {
    //                            if (commentModel.ILikeComment == 0) { commentModel.ILikeComment = 1; commentModel.TotalLikeComment = commentModel.TotalLikeComment + 1; }
    //                            else if (commentModel.TotalLikeComment > 0) { commentModel.ILikeComment = 0; commentModel.TotalLikeComment = commentModel.TotalLikeComment - 1; }
    //                        }
    //                    }
    //                    else if ( UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView != null &&
    //                        UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.clickedImageID == imgId && UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView._UCLikeNumbers != null)
    //                    {
    //                        if (UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView._UCLikeNumbers.ILilke == 0)
    //                        {
    //                            UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView._UCLikeNumbers.ILilke = 1;
    //                            UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView._UCLikeNumbers.NumberOfLikes = UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView._UCLikeNumbers.NumberOfLikes + 1;
    //                        }
    //                        else
    //                        {
    //                            UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView._UCLikeNumbers.ILilke = 0;
    //                            UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView._UCLikeNumbers.NumberOfLikes = UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView._UCLikeNumbers.NumberOfLikes - 1;
    //                        }
    //                    }
    //                }
    //                else if (obj is FeedModel)
    //                {
    //                    FeedModel fm = (FeedModel)obj;
    //                    if (fm.ILike == 0)
    //                    {
    //                        fm.ILike = 1;
    //                        fm.NumberOfLikes = fm.NumberOfLikes + 1;
    //                    }
    //                    else
    //                    {
    //                        fm.ILike = 0;
    //                        fm.NumberOfLikes = fm.NumberOfLikes - 1;
    //                    }
    //                }
    //                else if (obj is CommentModel)
    //                {
    //                    CommentModel cm = (CommentModel)obj;
    //                    if (cm.ILikeComment == 0) { cm.ILikeComment = 1; cm.TotalLikeComment = cm.TotalLikeComment + 1; }
    //                    else { cm.ILikeComment = 0; cm.TotalLikeComment = cm.TotalLikeComment - 1; }
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: OnLikeUnlikeFailed() ==> " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }

    //        public void AddProfileCoverImageToUIList(List<ImageDTO> imageDTOs, int imageType, long friendId = 0)
    //        {
    //            Application.Current.Dispatcher.Invoke(() =>
    //          {
    //              try
    //              {
    //                  if (friendId > 0)
    //                  {

    //                  }
    //                  else
    //                  {

    //                      if (imageType == SettingsConstants.TYPE_PROFILE_IMAGE
    //                          && NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES.ContainsKey(DefaultSettings.PROFILE_IMAGE_ALBUM_ID)
    //                          && NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[DefaultSettings.PROFILE_IMAGE_ALBUM_ID].Count > 0)
    //                      {
    //                          if (!NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[DefaultSettings.PROFILE_IMAGE_ALBUM_ID].ContainsKey(imageDTOs[0].ImageId))
    //                          {

    //                              int totalCount = 0;
    //                              foreach (ImageDTO dto in NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[DefaultSettings.PROFILE_IMAGE_ALBUM_ID].Values)
    //                              {
    //                                  dto.TotalImagesInAlbum += 1;
    //                                  totalCount = dto.TotalImagesInAlbum;
    //                              }

    //                              imageDTOs[0].TotalImagesInAlbum = totalCount;
    //                              imageDTOs[0].Time = Models.Utility.ModelUtility.CurrentTimeMillisLocal();

    //                              NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[DefaultSettings.PROFILE_IMAGE_ALBUM_ID].Add(imageDTOs[0].ImageId, imageDTOs[0]);

    //                              if (RingIDViewModel.Instance.ProfileImageList.Count > 0)
    //                              {
    //                                  ImageModel img = new ImageModel();
    //                                  img.LoadData(imageDTOs[0]);

    //                                  lock (RingIDViewModel.Instance.ProfileImageList)
    //                                  {
    //                                      RingIDViewModel.Instance.ProfileImageList.Insert(0, img);

    //                                      foreach (ImageModel model in RingIDViewModel.Instance.ProfileImageList)
    //                                      {
    //                                          model.TotalImagesInAlbum = totalCount;
    //                                      }
    //                                  }
    //                                  if (UCMiddlePanelSwitcher.View_UCMyProfile != null
    //                                        && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos != null
    //                                        && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.ProfileImageCount > 0 && totalCount > 0)
    //                                  {
    //                                      UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.ProfileImageCount = totalCount;
    //                                  }
    //                                  if (UCMiddlePanelSwitcher.View_UCMyProfile != null
    //                                     && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange != null
    //                                     && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange.ProfileImageCount > 0 && totalCount > 0)
    //                                  {
    //                                      UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange.ProfileImageCount = totalCount;
    //                                  }
    //                              }
    //                          }
    //                      }
    //                      else if (imageType == SettingsConstants.TYPE_COVER_IMAGE
    //                          && NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES.ContainsKey(DefaultSettings.COVER_IMAGE_ALBUM_ID)
    //                          && NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[DefaultSettings.COVER_IMAGE_ALBUM_ID].Count > 0)
    //                      {
    //                          if (!NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[DefaultSettings.COVER_IMAGE_ALBUM_ID].ContainsKey(imageDTOs[0].ImageId))
    //                          {
    //                              int totalCount = 0;
    //                              foreach (ImageDTO dto in NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[DefaultSettings.COVER_IMAGE_ALBUM_ID].Values)
    //                              {
    //                                  dto.TotalImagesInAlbum += 1;
    //                                  totalCount = dto.TotalImagesInAlbum;

    //                              }

    //                              imageDTOs[0].TotalImagesInAlbum = totalCount;
    //                              imageDTOs[0].Time = Models.Utility.ModelUtility.CurrentTimeMillisLocal();

    //                              NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[DefaultSettings.COVER_IMAGE_ALBUM_ID].Add(imageDTOs[0].ImageId, imageDTOs[0]);

    //                              if (RingIDViewModel.Instance.CoverImageList.Count > 0)
    //                              {
    //                                  ImageModel img = new ImageModel();
    //                                  img.LoadData(imageDTOs[0]);

    //                                  lock (RingIDViewModel.Instance.CoverImageList)
    //                                  {
    //                                      RingIDViewModel.Instance.CoverImageList.Insert(0, img);

    //                                      foreach (ImageModel model in RingIDViewModel.Instance.CoverImageList)
    //                                      {
    //                                          model.TotalImagesInAlbum = totalCount;
    //                                      }
    //                                  }
    //                                  if (UCMiddlePanelSwitcher.View_UCMyProfile != null
    //                                        && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos != null
    //                                        && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.CoverImageCount > 0 && totalCount > 0)
    //                                  {
    //                                      UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.CoverImageCount = totalCount;
    //                                  }

    //                                  if (UCMiddlePanelSwitcher.View_UCMyProfile != null
    //                                     && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange != null
    //                                     && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange.CoverImageCount > 0 && totalCount > 0)
    //                                  {
    //                                      UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange.CoverImageCount = totalCount;
    //                                  }
    //                              }
    //                          }
    //                      }
    //                      else if (imageType == SettingsConstants.TYPE_NORMAL_BOOK_IMAGE
    //                              && NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES.ContainsKey(DefaultSettings.FEED_IMAGE_ALBUM_ID)
    //                              && NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[DefaultSettings.FEED_IMAGE_ALBUM_ID].Count > 0)
    //                      {
    //                          int totalCount = 0;
    //                          foreach (ImageDTO dto in NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[DefaultSettings.FEED_IMAGE_ALBUM_ID].Values)
    //                          {
    //                              dto.TotalImagesInAlbum += imageDTOs.Count;
    //                              totalCount = dto.TotalImagesInAlbum;

    //                          }

    //                          for (int index = 0; index < imageDTOs.Count; index++)
    //                          {
    //                              imageDTOs[index].TotalImagesInAlbum = totalCount;
    //                              NewsFeedDictionaries.Instance.MY_ALBUM_IMAGES[DefaultSettings.FEED_IMAGE_ALBUM_ID].Add(imageDTOs[index].ImageId, imageDTOs[index]);
    //                          }


    //                          if (RingIDViewModel.Instance.FeedImageList.Count > 0)
    //                          {
    //                              for (int index = 0; index < imageDTOs.Count; index++)
    //                              {
    //                                  ImageModel img = new ImageModel();
    //                                  img.LoadData(imageDTOs[index]);

    //                                  lock (RingIDViewModel.Instance.FeedImageList)
    //                                  {
    //                                      RingIDViewModel.Instance.FeedImageList.Insert(0, img);

    //                                  }
    //                                  if (UCMiddlePanelSwitcher.View_UCMyProfile != null
    //                                        && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos != null
    //                                        && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.FeedImageCount > 0 && totalCount > 0)
    //                                  {
    //                                      UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.FeedImageCount = totalCount;
    //                                  }

    //                                  if (UCMiddlePanelSwitcher.View_UCMyProfile != null
    //                                     && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange != null
    //                                     && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange.FeedImageCount > 0 && totalCount > 0)
    //                                  {
    //                                      UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange.FeedImageCount = totalCount;
    //                                  }
    //                              }
    //                              RingIDViewModel.Instance.OnPropertyChanged("FeedImageList");
    //                              lock (RingIDViewModel.Instance.FeedImageList)
    //                              {
    //                                  foreach (ImageModel model in RingIDViewModel.Instance.FeedImageList)
    //                                  {
    //                                      model.TotalImagesInAlbum = totalCount;
    //                                  }
    //                              }
    //                          }
    //                      }


    //                  }
    //              }
    //              catch (Exception ex)
    //              {
    //                  log.Error("Error: AddProfileCoverImageToUIList() ==> " + ex.Message + "\n" + ex.StackTrace);
    //              }
    //          });
    //        }

    //        public void StartNetworkThread()
    //        {

    //            try
    //            {
    //                MainSwitcher.ThreadManager().StartNetworkThread();
    //                //View.Utility.InternetOptions.CheckInternetThread.StartThread();
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        public void StopNetworkThread()
    //        {
    //            //MainSwitcher.ThreadManager().StopNetworkThread();
    //            try
    //            {
    //                MainSwitcher.ThreadManager().StopNetworkThread();
    //                // View.Utility.InternetOptions.CheckInternetThread.StopThread();
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        public void ResetNetworkThread()
    //        {
    //            //     MainSwitcher.ThreadManager().StopNetworkThread();
    //            try
    //            {
    //                MainSwitcher.ThreadManager().StopNetworkThread();
    //                //     View.Utility.InternetOptions.CheckInternetThread.ResetThread();
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        public void DisableLoaderAnimation()
    //        {
    //            try
    //            {
    //                Application.Current.Dispatcher.Invoke(() =>
    //                   {
    //                       if ( UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView != null
    //                           && UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.IsVisible) UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.ImageComments.ShowWatch(false, false);
    //                       else if (UCGuiRingID.Instance.ucBasicMediaViewWrapper != null && UCGuiRingID.Instance.ucBasicMediaViewWrapper.ucBasicMediaView != null && UCGuiRingID.Instance.ucBasicMediaViewWrapper.ucBasicMediaView.IsVisible)
    //                           UCGuiRingID.Instance.ucBasicMediaViewWrapper.ucBasicMediaView.ImageComments.ShowWatch(false, false);
    //                   });
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: DisableLoaderAnimation() ==> " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }

    //        public void AddNotificationFromServer(List<NotificationDTO> list, bool indexTop)
    //        {
    //            Application.Current.Dispatcher.BeginInvoke(() =>
    //            {
    //                try
    //                {
    //                    list = (from l in list
    //                            orderby l.UpdateTime
    //                            select l).ToList();

    //                    lock (RingIDViewModel.Instance.NotificationList)
    //                    {
    //                        foreach (NotificationDTO dto in list)
    //                        {
    //                            foreach (var id in dto.PreviousIds)
    //                            {
    //                                if (RingIDViewModel.Instance.NotificationList.Any(x => x.NotificationID == id))
    //                                {
    //                                    NotificationModel model = RingIDViewModel.Instance.NotificationList.Where(x => x.NotificationID == id).First();
    //                                    RingIDViewModel.Instance.NotificationList.Remove(model);
    //                                }
    //                            }
    //                            NotificationModel insertModel = new NotificationModel(dto);
    //                            if (!RingIDViewModel.Instance.NotificationList.Contains(insertModel))
    //                            {
    //                                RingIDViewModel.Instance.NotificationList.Add(insertModel);
    //                            }
    //                        }
    //                    }

    //                    if (UCAllNotification.Instance == null)
    //                    {
    //                        if (UCGuiRingID.Instance.ucFriendTabControlPanel._NotificationPanel.Children.Count > 0)
    //                        {
    //                            foreach (var item in UCGuiRingID.Instance.ucFriendTabControlPanel._NotificationPanel.Children)
    //                            {
    //                                UCGuiRingID.Instance.ucFriendTabControlPanel._NotificationPanel.Children.Remove(item as UIElement);
    //                            }
    //                        }
    //                        UCGuiRingID.Instance.ucFriendTabControlPanel._NotificationPanel.Children.Add(new UCAllNotification());
    //                    }

    //                    if (UCAllNotification.Instance.IsVisible)
    //                    {
    //                        var scrollViewer = (System.Windows.Controls.ScrollViewer)UCAllNotification.Instance.Scroll;
    //                        if (scrollViewer.VerticalOffset > 70)
    //                        {
    //                            VMNotification.Instance.NotificationModelCount = RingIDViewModel.Instance.NotificationList.Count;
    //                        }
    //                        else
    //                        {
    //                            ClearAllNotificationCount();
    //                        }
    //                    }
    //                    else
    //                    {
    //                        VMNotification.Instance.NotificationModelCount = RingIDViewModel.Instance.NotificationList.Count;
    //                        RingIDViewModel.Instance.AllNotificationCounter = list.Count;
    //                        RingIDViewModel.Instance.OnPropertyChanged("AllNotificationCounter");
    //                        AppConstants.ALL_NOTIFICATION_COUNT = RingIDViewModel.Instance.NotificationList.Count;
    //                    }

    //                    VMNotification.Instance.IsLoading = false;
    //                    VMNotification.Instance.IsAllRead();
    //                    NotificationUtility.Instance.ShowMoreAction();

    //                    new InsertIntoNotificationHistoryTable(list);
    //                    VMNotification.Instance.NotificationLoaded = RingIDViewModel.Instance.NotificationList.Count > 0 ? 1 : 0;
    //                }
    //                catch (Exception ex)
    //                {
    //                    log.Error("Error: AddNotificationFromServerNew() ==> " + ex.Message + "\n" + ex.StackTrace);
    //                }
    //            }, System.Windows.Threading.DispatcherPriority.Send);

    //        }

    //        public void RecoveryInformationAddition(string msg, string el, int iev, string mbl, string mblDc, int imv, bool isEmail)
    //        {
    //            try
    //            {
    //                Application.Current.Dispatcher.Invoke(() =>
    //                {
    //                    if (el != null)
    //                    {
    //                        DefaultSettings.userProfile.Email = el;
    //                        DefaultSettings.userProfile.IsEmailVerified = iev;
    //                        RingIDViewModel.Instance.MyBasicInfoModel.Email = el;
    //                        RingIDViewModel.Instance.MyBasicInfoModel.IsEmailVerified = iev;
    //                        // UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyRecoveryInfo.ucMyRecoveryEmailEdit.ResetViewMode();
    //                    }
    //                    else if (mbl != null && mblDc != null)
    //                    {
    //                        DefaultSettings.userProfile.MobileNumber = mbl;
    //                        DefaultSettings.userProfile.MobileDialingCode = mblDc;
    //                        DefaultSettings.userProfile.IsMobileNumberVerified = imv;
    //                        RingIDViewModel.Instance.MyBasicInfoModel.MobilePhone = mbl;
    //                        RingIDViewModel.Instance.MyBasicInfoModel.MobilePhoneDialingCode = mblDc;
    //                        RingIDViewModel.Instance.MyBasicInfoModel.IsMobileNumberVerified = imv;
    //                        //UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyRecoveryInfo.ucMyRecoveryInfoPhoneNumberEdit.ResetViewMode();
    //                    }
    //                    /*if (isEmail && UCMiddlePanelSwitcher.View_UCMyProfile != null
    //                        && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout != null
    //                        && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyRecoveryInfo != null
    //                        && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyRecoveryInfo.ucMyRecoveryEmailEdit != null)
    //                    {
    //                        if (el != null && iev == 1) UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyRecoveryInfo.ucMyRecoveryEmailEdit.ResetViewMode();
    //                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyRecoveryInfo.ucMyRecoveryEmailEdit.IsFieldsEnabled = true;
    //                    }
    //                    else if (UCMiddlePanelSwitcher.View_UCMyProfile != null
    //                        && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout != null
    //                        && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyRecoveryInfo != null
    //                        && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyRecoveryInfo.ucMyRecoveryInfoPhoneNumberEdit != null)
    //                    {
    //                        if (mbl != null && mblDc != null && imv == 1) UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyRecoveryInfo.ucMyRecoveryInfoPhoneNumberEdit.ResetViewMode();
    //                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyRecoveryInfo.ucMyRecoveryInfoPhoneNumberEdit.IsFieldsEnabled = true;
    //                    }*/
    //                    if (isEmail && RingIDViewModel.Instance._WNRingIDSettings._AccountSettings.ucRecoverySettings.ucRIEmailEdit != null)
    //                    {
    //                        if (el != null && iev == 1)
    //                        {
    //                            // RingIDViewModel.Instance._WNRingIDSettings._RecoverySettings.ucRIEmailEdit.ResetViewMode();
    //                            RingIDViewModel.Instance._WNRingIDSettings._AccountSettings.ucRecoverySettings.ucRIEmailEdit.ResetViewMode();
    //                        }
    //                        //RingIDViewModel.Instance._WNRingIDSettings._RecoverySettings.ucRIEmailEdit.IsFieldsEnabled = true;
    //                        RingIDViewModel.Instance._WNRingIDSettings._AccountSettings.ucRecoverySettings.ucRIEmailEdit.IsFieldsEnabled = true;
    //                    }
    //                    else if (RingIDViewModel.Instance._WNRingIDSettings._AccountSettings.ucRecoverySettings.ucRIPhoneNumberEdit != null)
    //                    {
    //                        if (mbl != null && mblDc != null && imv == 1)
    //                        {
    //                            RingIDViewModel.Instance._WNRingIDSettings._AccountSettings.ucRecoverySettings.ucRIPhoneNumberEdit.ResetViewMode();
    //                        }
    //                        RingIDViewModel.Instance._WNRingIDSettings._AccountSettings.ucRecoverySettings.ucRIPhoneNumberEdit.IsFieldsEnabled = true;
    //                    }
    //                    CustomMessageBox.ShowInformation(msg);
    //                });
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        public void IncreaseAllnotificationCount(bool isNew)
    //        {
    //            try
    //            {
    //                Application.Current.Dispatcher.Invoke(() =>
    //                {
    //                    if (isNew == true)
    //                        AppConstants.ALL_NOTIFICATION_COUNT++;

    //                    RingIDViewModel.Instance.AllNotificationCounter = AppConstants.ALL_NOTIFICATION_COUNT;
    //                    RingIDViewModel.Instance.OnPropertyChanged("AllNotificationCounter");
    //                    new InsertIntoNotificationCountTable();
    //                    if (SettingsConstants.VALUE_RINGID_ALERT_SOUND)
    //                    {
    //                        View.Utility.audio.AudioFilesAndSettings.Play(View.Utility.audio.AudioFilesAndSettings.NOTIFICATION_RECEIVED);
    //                    }
    //                });
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        public bool CheckNotificationCount(NotificationDTO dto)
    //        {
    //            bool increaseCounter = false;
    //            try
    //            {
    //                NotificationModel tmpNotificationModel = null;
    //                switch (dto.MessageType)
    //                {
    //                    case StatusConstants.MESSAGE_LIKE_STATUS:
    //                    case StatusConstants.MESSAGE_LIKE_COMMENT:
    //                    case StatusConstants.MESSAGE_SHARE_STATUS:
    //                    case StatusConstants.MESSAGE_YOU_HAVE_BEEN_TAGGED:
    //                    case StatusConstants.MESSAGE_ADD_STATUS_COMMENT:
    //                        tmpNotificationModel = RingIDViewModel.Instance.NotificationList.Where(x => x.NewsfeedID == dto.NewsfeedID && x.MessageType == dto.MessageType).FirstOrDefault();
    //                        if (tmpNotificationModel != null)
    //                        {
    //                            if (tmpNotificationModel.IsRead == true)
    //                            { increaseCounter = true; }
    //                        }
    //                        else { increaseCounter = true; }
    //                        break;
    //                    case StatusConstants.MESSAGE_IMAGE_COMMENT:
    //                    case StatusConstants.MESSAGE_AUDIO_MEDIA_COMMENT:
    //                    case StatusConstants.MESSAGE_VIDEO_MEDIA_COMMENT:
    //                    case StatusConstants.MESSAGE_LIKE_IMAGE:
    //                    case StatusConstants.MESSAGE_LIKE_IMAGE_COMMENT:
    //                    case StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA:
    //                    case StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA_COMMENT:
    //                    case StatusConstants.MESSAGE_AUDIO_MEDIA_VIEW:
    //                    case StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA:
    //                    case StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA_COMMENT:
    //                    case StatusConstants.MESSAGE_VIDEO_MEDIA_VIEW:
    //                        tmpNotificationModel = RingIDViewModel.Instance.NotificationList.Where(x => x.ImageID == dto.ImageID && x.MessageType == dto.MessageType).FirstOrDefault();
    //                        if (tmpNotificationModel != null) { if (tmpNotificationModel.IsRead == true) { increaseCounter = true; } }
    //                        else { increaseCounter = true; }
    //                        break;
    //                    case StatusConstants.MESSAGE_ADD_CIRCLE_MEMBER:
    //                        tmpNotificationModel = RingIDViewModel.Instance.NotificationList.Where(x => x.ActivityID == dto.ActivityID && x.MessageType == dto.MessageType).FirstOrDefault();
    //                        if (tmpNotificationModel != null) { if (tmpNotificationModel.IsRead == true) { increaseCounter = true; } }
    //                        else { increaseCounter = true; }
    //                        break;
    //                    default:
    //                        increaseCounter = true;
    //                        break;
    //                }
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //            return increaseCounter;
    //        }

    //        public void RemoveSeeMorePanel()
    //        {
    //            try
    //            {
    //                Application.Current.Dispatcher.BeginInvoke(() =>
    //                    {
    //                        if (UCAllNotification.Instance == null)
    //                        {
    //                            if ( UCGuiRingID.Instance.ucFriendTabControlPanel._NotificationPanel.Children.Count > 0)
    //                            {
    //                                foreach (var item in UCGuiRingID.Instance.ucFriendTabControlPanel._NotificationPanel.Children)
    //                                {
    //                                    UCGuiRingID.Instance.ucFriendTabControlPanel._NotificationPanel.Children.Remove(item as UIElement);
    //                                }
    //                            }
    //                            UCGuiRingID.Instance.ucFriendTabControlPanel._NotificationPanel.Children.Add(new UCAllNotification());
    //                        }
    //                        VMNotification.Instance.IsLoading = false;
    //                        NotificationUtility.Instance.ShowMoreAction(SeeMoreText: NotificationMessages.NOTIFICATION_NO_MORE_NOTIFICATION);
    //                    });
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        public void StartHashTagSuggestionThread()
    //        {
    //            try
    //            {
    //                Application.Current.Dispatcher.Invoke(() =>
    //                {
    //                    if (HashTagPopUp != null)
    //                    {
    //                        HashTagPopUp.StartThread();
    //                    }
    //                });
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        public void NoDataInFullMediaSearch(string searchParam, int suggestionType)
    //        {
    //            try
    //            {
    //                Application.Current.Dispatcher.Invoke(() =>
    //                {
    //                    if (UCMiddlePanelSwitcher.View_MusicsVideos != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search != null
    //                        && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch != null)
    //                    {
    //                        UCSearchingTabPanel ucFullSearch = UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch;
    //                        if (suggestionType == SettingsConstants.MEDIA_SEARCH_TYPE_SONGS && ucFullSearch.Tab_Songs != null)
    //                        {
    //                            //ucFullSearch.Tab_Songs.MediasFromSearch.Clear();
    //                            ucFullSearch.Tab_Songs.LoadStates(2);
    //                        }
    //                        else if (suggestionType == SettingsConstants.MEDIA_SEARCH_TYPE_ALBUMS && ucFullSearch.Tab_Albums != null)
    //                        {
    //                            //ucFullSearch.Tab_Albums.AlbumsFromSearch.Clear();
    //                            ucFullSearch.Tab_Albums.LoadStates(2);
    //                        }
    //                        else if (suggestionType == SettingsConstants.MEDIA_SEARCH_TYPE_TAGS && ucFullSearch.Tab_Tags != null)
    //                        {
    //                            //ucFullSearch.Tab_Tags.HashTagsFromSearch.Clear();
    //                            ucFullSearch.Tab_Tags.LoadStates(2);
    //                        }
    //                    }
    //                });
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        public void ShowHashTagResultsInFullSearch(string searchParam, List<HashTagDTO> list)
    //        {
    //            try
    //            {
    //                Application.Current.Dispatcher.Invoke(() =>
    //                {
    //                    if (UCMiddlePanelSwitcher.View_MusicsVideos != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search != null
    //                        && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch != null
    //                        && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.Tab_Tags != null
    //                        //&& UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.Tab_Tags_Wrapper.View_UCSearchTagsTabPanel != null
    //                        && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.SelectedSearchedParam.Equals(searchParam))
    //                    {
    //                        UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.Tab_Tags.LoadStates(1);
    //                        foreach (var item in list)
    //                        {
    //                            if (!UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.Tab_Tags.HashTagsFromSearch.Any(P => P.HashTagId == item.CategoryId))
    //                            {
    //                                HashTagModel model = new HashTagModel();
    //                                model.LoadData(item);
    //                                UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.Tab_Tags.HashTagsFromSearch.Add(model);
    //                            }
    //                        }
    //                    }
    //                });
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        public void ShowAlbumResultsInFullSearch(string searchParam, List<MediaContentDTO> list)
    //        {
    //            try
    //            {
    //                Application.Current.Dispatcher.Invoke(() =>
    //                {
    //                    if (UCMiddlePanelSwitcher.View_MusicsVideos != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search != null
    //                        && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch != null
    //                        && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.Tab_Albums != null
    //                        //&& UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.Tab_Albums_Wrapper.View_UCSearchAlbumPanel != null
    //                        && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.SelectedSearchedParam.Equals(searchParam))
    //                    {
    //                        UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.Tab_Albums.LoadStates(1);
    //                        foreach (var item in list)
    //                        {
    //                            if (!UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.Tab_Albums.AlbumsFromSearch.Any(P => P.AlbumId == item.AlbumId))
    //                            {
    //                                MediaContentModel model = new MediaContentModel();
    //                                model.LoadData(item);
    //                                UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.Tab_Albums.AlbumsFromSearch.Add(model);
    //                            }
    //                        }
    //                    }
    //                });
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        public void ShowMediaResultsInFullSearch(string searchParam, List<SingleMediaDTO> list)
    //        {
    //            try
    //            {
    //                Application.Current.Dispatcher.Invoke(() =>
    //                {
    //                    if (UCMiddlePanelSwitcher.View_MusicsVideos != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search != null
    //                        && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch != null
    //                        && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.Tab_Songs != null
    //                      && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.SelectedSearchedParam.Equals(searchParam))
    //                    {
    //                        UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.Tab_Songs.LoadStates(1);
    //                        foreach (var item in list)
    //                        {
    //                            if (!UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.Tab_Songs.MediasFromSearch.Any(P => P.ContentId == item.ContentId))
    //                            {
    //                                SingleMediaModel model = RingIDViewModel.Instance.GetExistingorNewSingleMediaModelFromDTO(item);//new SingleMediaModel();
    //                                //model.LoadData(item);
    //                                UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.Tab_Songs.MediasFromSearch.Add(model);
    //                            }
    //                        }
    //                    }
    //                });
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        public void ShowHashTagMediaContents(List<SingleMediaDTO> list, long hashtagId)
    //        {
    //            try
    //            {
    //                Application.Current.Dispatcher.Invoke(() =>
    //                {
    //                    if (UCMiddlePanelSwitcher.View_MusicsVideos != null && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search != null
    //                        && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch != null
    //                        && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.Tab_Tags_Wrapper != null
    //                        && UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.Tab_Tags_Wrapper.View_UCSearchTagsTabPanel != null)
    //                    {
    //                        HashTagModel hashtagModel = UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.Tab_Tags_Wrapper.View_UCSearchTagsTabPanel.HashTagsFromSearch.Where(P => P.HashTagId == hashtagId).FirstOrDefault();
    //                        if (hashtagModel != null && hashtagModel.MediaList != null)
    //                        {
    //                            foreach (var item in list)
    //                            {
    //                                SingleMediaModel model = RingIDViewModel.Instance.GetExistingorNewSingleMediaModelFromDTO(item);//new SingleMediaModel();
    //                                //model.LoadData(item);
    //                                hashtagModel.MediaList.Add(model);
    //                            }
    //                            if (UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.View_Contents != null)
    //                                UCMiddlePanelSwitcher.View_MusicsVideos.View_Search.View_FullSearch.View_Contents.VisibilityShowMore(hashtagModel.MediaList.Count < hashtagModel.NoOfItems);
    //                        }
    //                    }
    //                });
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        //public void RingSDKRestart()
    //        //{
    //        //    try
    //        //    {
    //        //        log.Info("****************Restarting => RingSDKRestart");
    //        //        //RingIDSDKWrapper.ipv_Release();
    //        //        log.Info("****************Stoped => RingSDKRestart");
    //        //        //  HelperMethods.InitRingIDSDK();
    //        //        log.Info("****************Restarted => RingSDKRestart");
    //        //    }
    //        //    catch (Exception e)
    //        //    {
    //        //        log.Error("********Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //        //    }
    //        //}

    //        public void StopLoaderAnimationInProfile(int action, JObject pakToSend)
    //        {
    //            Application.Current.Dispatcher.Invoke(() =>
    //            {
    //                if (action == AppConstants.TYPE_FRIEND_CONTACT_LIST_V_141)//if (action == AppConstants.TYPE_FRIEND_CONTACT_LIST)
    //                    NoContactFoundInFriendsContactList((long)pakToSend[JsonKeys.UserTableID]);
    //                else if (action == AppConstants.TYPE_MUTUAL_FRIENDS)
    //                    NoMutualFriendFoundInFriendsContactList((long)pakToSend[JsonKeys.UserIdentity]);
    //                else if (action == AppConstants.TYPE_FRIEND_ALBUM_IMAGES)
    //                {
    //                    long uid = (long)pakToSend[JsonKeys.FriendId];
    //                    string albumID = (string)pakToSend[JsonKeys.AlbumId];
    //                    NoImageFoundInFriendsAlbum(uid, albumID);
    //                }
    //                else if (action == AppConstants.TYPE_MY_ALBUM_IMAGES)
    //                {
    //                    string albumID = (string)pakToSend[JsonKeys.AlbumId];
    //                    NoImageFoundInMyAlbum(albumID);
    //                }
    //            });
    //        }

    //        public void SaveDeviceUniqueIDintoDB()
    //        {
    //            DatabaseActivityDAO.Instance.SaveDeviceUniqueIDintoDB();
    //        }
    //        public void ShowSingleFeedShareList(long nfId, List<UserBasicInfoDTO> list, long minIdOfSequence)
    //        {
    //            try
    //            {
    //                if (LikeListViewWrapper.ucLikeListView != null && LikeListViewWrapper.ucLikeListView.NfId == nfId)
    //                {
    //                    foreach (var item in list)
    //                    {
    //                        UserBasicInfoModel model = GetExistingorNewUserBasicModelFromDTO(item);
    //                        Application.Current.Dispatcher.Invoke(() =>
    //                        {
    //                            LikeListViewWrapper.ucLikeListView.LikeList.Add(model);
    //                        });
    //                    }
    //                    if (LikeListViewWrapper.ucLikeListView.PvtId == 0 || minIdOfSequence < LikeListViewWrapper.ucLikeListView.PvtId)
    //                        LikeListViewWrapper.ucLikeListView.PvtId = minIdOfSequence;
    //                }

    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("********Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }
    //        public void SubscribeUnsubscribeChannels(int profileType, int SubUnsubscrbType, long utId, string msg)
    //        {
    //            Application.Current.Dispatcher.Invoke(() =>
    //            {
    //                try
    //                {
    //                    if (profileType == SettingsConstants.PROFILE_TYPE_NEWSPORTAL)
    //                    {
    //                        NewsPortalModel model = null;
    //                        if (SubUnsubscrbType == 1)
    //                        {
    //                            Thread myThread = new System.Threading.Thread(delegate()
    //                            {
    //                                CustomMessageBox.ShowMessageWithTimerFromThread("Successfully unsubscribed from this Portal!");
    //                            });
    //                            myThread.Start();
    //                            model = NewsPortalDataContainer.Instance.SearchNewsPortalModelByUtId(utId);
    //                            if (model != null)
    //                            {
    //                                model.SubscriberCount--;
    //                                model.IsSubscribed = false;
    //                                //
    //                                if (UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel != null)
    //                                {
    //                                    UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCNewsContainerPanel = null;
    //                                    NewsFeedDictionaries.Instance.NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME.Clear();

    //                                    if (UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCFollowingListPanel != null)
    //                                    {
    //                                        NewsPortalModel model1 = UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCFollowingListPanel.FollowingNewsPortals.Where(P => P.PortalId == utId).FirstOrDefault();
    //                                        if (model1 != null)
    //                                        {
    //                                            UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCFollowingListPanel.FollowingNewsPortals.Remove(model1);
    //                                        }
    //                                    }

    //                                    if (UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCDiscoverPanel != null
    //                                        && !UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCDiscoverPanel.SearchedNewsPortals.Any(P => P.PortalId == utId))
    //                                        UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCDiscoverPanel.SearchedNewsPortals.Insert(0, model);
    //                                }
    //                                //
    //                            }
    //                            NewsPortalDTO dto = null;
    //                            if (NewsPortalDictionaries.Instance.UTID_NEWSPORTALINFO_DICTIONARY.TryGetValue(utId, out dto))
    //                            {
    //                                dto.SubscriberCount--;
    //                                dto.IsSubscribed = false;
    //                            }
    //                            if (UCMiddlePanelSwitcher.View_UCNewsPortalProfile != null && UCMiddlePanelSwitcher.View_UCNewsPortalProfile.NewsPortalModel != null && UCMiddlePanelSwitcher.View_UCNewsPortalProfile.NewsPortalModel.PortalId == utId)
    //                            {
    //                                UCMiddlePanelSwitcher.View_UCNewsPortalProfile.FollowUnfollowPlus = "+";
    //                                UCMiddlePanelSwitcher.View_UCNewsPortalProfile.FollowUnfollowTxt = "Follow";
    //                            }
    //                        }
    //                        else
    //                        {
    //                            if (SubUnsubscrbType == 0)
    //                            {
    //                                Thread myThread = new System.Threading.Thread(delegate()
    //                                {
    //                                    CustomMessageBox.ShowMessageWithTimerFromThread("Successfully subscribed to the chosen categories of this Portal!");
    //                                });
    //                                myThread.Start();
    //                            }
    //                            else if (SubUnsubscrbType == 2)
    //                            {
    //                                Thread myThread = new System.Threading.Thread(delegate()
    //                                {
    //                                    CustomMessageBox.ShowMessageWithTimerFromThread("Successfully subscribed to this Portal!");
    //                                });
    //                                myThread.Start();
    //                            }
    //                            if (DownloadOrAddToAlbumPopUpWrapper.ucFollowingUnfollowingView != null
    //                             && DownloadOrAddToAlbumPopUpWrapper.ucFollowingUnfollowingView.PortalModel != null
    //                             && DownloadOrAddToAlbumPopUpWrapper.ucFollowingUnfollowingView.PortalModel.PortalId == utId)
    //                            {
    //                                DownloadOrAddToAlbumPopUpWrapper.ucFollowingUnfollowingView.Hide();
    //                            }
    //                            model = NewsPortalDataContainer.Instance.SearchNewsPortalModelByUtId(utId);
    //                            if (model != null)
    //                            {
    //                                model.SubscriberCount++;
    //                                model.IsSubscribed = true;
    //                                //
    //                                if (UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel != null)
    //                                {
    //                                    UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCNewsContainerPanel = null;
    //                                    NewsFeedDictionaries.Instance.NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME.Clear();

    //                                    if (UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCFollowingListPanel != null
    //                                        && !UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCFollowingListPanel.FollowingNewsPortals.Any(P => P.PortalId == utId))
    //                                        UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCFollowingListPanel.FollowingNewsPortals.Insert(0, model);

    //                                    if (UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCDiscoverPanel != null)
    //                                    {
    //                                        NewsPortalModel model1 = UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCDiscoverPanel.SearchedNewsPortals.Where(P => P.PortalId == utId).FirstOrDefault();
    //                                        if (model1 != null)
    //                                        {
    //                                            UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCDiscoverPanel.SearchedNewsPortals.Remove(model1);
    //                                        }
    //                                    }
    //                                }
    //                                //
    //                            }
    //                            NewsPortalDTO dto = null;
    //                            if (NewsPortalDictionaries.Instance.UTID_NEWSPORTALINFO_DICTIONARY.TryGetValue(utId, out dto))
    //                            {
    //                                dto.SubscriberCount++;
    //                                dto.IsSubscribed = true;
    //                            }
    //                            if (UCMiddlePanelSwitcher.View_UCNewsPortalProfile != null && UCMiddlePanelSwitcher.View_UCNewsPortalProfile.NewsPortalModel != null && UCMiddlePanelSwitcher.View_UCNewsPortalProfile.NewsPortalModel.PortalId == utId)
    //                            {
    //                                UCMiddlePanelSwitcher.View_UCNewsPortalProfile.FollowUnfollowPlus = "-";
    //                                UCMiddlePanelSwitcher.View_UCNewsPortalProfile.FollowUnfollowTxt = "Unfollow";
    //                            }
    //                        }
    //                    }
    //                    else if (profileType == SettingsConstants.PROFILE_TYPE_PAGES)
    //                    {
    //                        PageInfoModel model = null;
    //                        if (SubUnsubscrbType == 1)
    //                        {
    //                            Thread myThread = new System.Threading.Thread(delegate()
    //                            {
    //                                CustomMessageBox.ShowMessageWithTimerFromThread("Successfully unsubscribed from this Page!");
    //                            });
    //                            myThread.Start();
    //                            model = NewsPortalDataContainer.Instance.SearchPageInfoModelByUtId(utId);
    //                            if (model != null)
    //                            {
    //                                model.SubscriberCount--;
    //                                model.IsSubscribed = false;
    //                                if (UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel != null)
    //                                {
    //                                    UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCNewsContainerPanel = null;
    //                                    NewsFeedDictionaries.Instance.PAGES_NEWS_FEEDS_ID_SORTED_BY_TIME.Clear();

    //                                    if (UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCFollowingListPanel != null)
    //                                    {
    //                                        PageInfoModel model1 = UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCFollowingListPanel.FollowingNewsPages.Where(P => P.PageId == utId).FirstOrDefault();
    //                                        if (model1 != null)
    //                                            UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCFollowingListPanel.FollowingNewsPages.Remove(model1);
    //                                    }

    //                                    if (UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCDiscoverPanel != null
    //                                         && !UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCDiscoverPanel.SearchedNewsPages.Any(P => P.PageId == utId))
    //                                        UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCDiscoverPanel.SearchedNewsPages.Insert(0, model);
    //                                }
    //                            }
    //                            PageInfoDTO dto = null;
    //                            if (NewsPortalDictionaries.Instance.UTID_PAGEINFO_DICTIONARY.TryGetValue(utId, out dto))
    //                            {
    //                                dto.SubscriberCount--;
    //                                dto.IsSubscribed = false;
    //                            }
    //                            if (UCMiddlePanelSwitcher.View_UCPageProfile != null && UCMiddlePanelSwitcher.View_UCPageProfile.PageModel != null && UCMiddlePanelSwitcher.View_UCPageProfile.PageModel.PageId == utId)
    //                            {
    //                                UCMiddlePanelSwitcher.View_UCPageProfile.FollowUnfollowPlus = "+";
    //                                UCMiddlePanelSwitcher.View_UCPageProfile.FollowUnfollowTxt = "Follow";
    //                            }
    //                        }
    //                        else
    //                        {
    //                            if (SubUnsubscrbType == 0)
    //                            {
    //                                Thread myThread = new System.Threading.Thread(delegate()
    //                                {
    //                                    CustomMessageBox.ShowMessageWithTimerFromThread("Successfully subscribed to the chosen categories of this Page!");
    //                                });
    //                                myThread.Start();
    //                            }
    //                            else if (SubUnsubscrbType == 2)
    //                            {
    //                                Thread myThread = new System.Threading.Thread(delegate()
    //                                {
    //                                    CustomMessageBox.ShowMessageWithTimerFromThread("Successfully subscribed to this Page!");
    //                                });
    //                                myThread.Start();
    //                            }
    //                            if (DownloadOrAddToAlbumPopUpWrapper.ucFollowingUnfollowingView != null
    //                             && DownloadOrAddToAlbumPopUpWrapper.ucFollowingUnfollowingView.PageModel != null
    //                             && DownloadOrAddToAlbumPopUpWrapper.ucFollowingUnfollowingView.PageModel.PageId == utId)
    //                            {
    //                                DownloadOrAddToAlbumPopUpWrapper.ucFollowingUnfollowingView.Hide();
    //                            }
    //                            model = NewsPortalDataContainer.Instance.SearchPageInfoModelByUtId(utId);
    //                            if (model != null)
    //                            {
    //                                model.SubscriberCount++;
    //                                model.IsSubscribed = true;
    //                                if (UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel != null)
    //                                {
    //                                    UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCNewsContainerPanel = null;
    //                                    NewsFeedDictionaries.Instance.PAGES_NEWS_FEEDS_ID_SORTED_BY_TIME.Clear();

    //                                    if (UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCDiscoverPanel != null)
    //                                    {
    //                                        PageInfoModel model1 = UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCDiscoverPanel.SearchedNewsPages.Where(P => P.PageId == utId).FirstOrDefault();
    //                                        if (model1 != null)
    //                                            UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCDiscoverPanel.SearchedNewsPages.Remove(model1);
    //                                    }

    //                                    if (UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCFollowingListPanel != null
    //                                        && !UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCFollowingListPanel.FollowingNewsPages.Any(P => P.PageId == utId))
    //                                        UCMiddlePanelSwitcher.View_UCPagesMainWrapperPanel.View_UCPagesMainPanel._UCFollowingListPanel.FollowingNewsPages.Insert(0, model);
    //                                }
    //                            }
    //                            PageInfoDTO dto = null;
    //                            if (NewsPortalDictionaries.Instance.UTID_PAGEINFO_DICTIONARY.TryGetValue(utId, out dto))
    //                            {
    //                                dto.SubscriberCount++;
    //                                dto.IsSubscribed = true;
    //                            }
    //                            if (UCMiddlePanelSwitcher.View_UCPageProfile != null && UCMiddlePanelSwitcher.View_UCPageProfile.PageModel != null && UCMiddlePanelSwitcher.View_UCPageProfile.PageModel.PageId == utId)
    //                            {
    //                                UCMiddlePanelSwitcher.View_UCPageProfile.FollowUnfollowPlus = "-";
    //                                UCMiddlePanelSwitcher.View_UCPageProfile.FollowUnfollowTxt = "Unfollow";
    //                            }
    //                        }
    //                    }
    //                }
    //                catch (Exception ex) { log.Error(ex.StackTrace); }
    //            });
    //        }

    //        public void AddRecoveryNumber(string mobile, string mblDc)
    //        {
    //            Application.Current.Dispatcher.Invoke(() =>
    //            {
    //                if (RingIDViewModel.Instance != null && RingIDViewModel.Instance._WNRingIDSettings != null
    //                      && RingIDViewModel.Instance._WNRingIDSettings._AccountSettings != null)
    //                //&& RingIDViewModel.Instance._WNRingIDSettings._RecoverySettings.ucDigitsCustomMsgPopup != null)
    //                //&& RingIDViewModel.Instance._WNRingIDSettings._RecoverySettings.IsVisible)
    //                {
    //                    CustomMessageBox.ShowInformation("Your Number has been Successfully Verified and Added!", "Success");
    //                    RingIDViewModel.Instance._WNRingIDSettings._AccountSettings.ucRecoverySettings.LoadVerifiedMobileNumberFromDigits(mobile, mblDc);
    //                }
    //            });
    //        }

    //        public void ChangeDigitsVerificationUI(bool SuccessfullyVerified, string mbl = "", string mbldc = "")
    //        {
    //            MainSwitcher.MainController().MainUIController().ChangeDigitVerificationUI(SuccessfullyVerified, mbl, mbldc);
    //            //Application.Current.Dispatcher.Invoke(() =>
    //            //{
    //            //    //bring to front
    //            //    MainSwitcher.pageSwitcher.Show();
    //            //    MainSwitcher.pageSwitcher.Activate();
    //            //    MainSwitcher.pageSwitcher.Topmost = true;
    //            //    MainSwitcher.pageSwitcher.Topmost = false;
    //            //    MainSwitcher.pageSwitcher.Focus();
    //            //    if (MainSwitcher._UCWelcomeMainSwitcher != null)
    //            //    {
    //            //        MainSwitcher._UCWelcomeMainSwitcher.ucDigitsCustomMsgPopup.Hide();
    //            //        if (SuccessfullyVerified)
    //            //        {
    //            //            if (MainSwitcher._UCWelcomeMainSwitcher.View_SignUpMain != null && MainSwitcher._UCWelcomeMainSwitcher.View_SignUpMain.IsVisible)
    //            //            {
    //            //                MainSwitcher._UCWelcomeMainSwitcher.LoadUI(WelcomePanelConstants.TypeSignupNamePassword);
    //            //                MainSwitcher._UCWelcomeMainSwitcher.View_SignUpNamePassword.LoadMobileData(null, null, null);
    //            //            }
    //            //            else if (MainSwitcher._UCWelcomeMainSwitcher.View_UCRecoverPasswordSuggestionPanel != null && MainSwitcher._UCWelcomeMainSwitcher.View_UCRecoverPasswordSuggestionPanel.IsVisible)
    //            //            { //MblSgn
    //            //                string mblwithDc = mbldc + "-" + mbl;
    //            //                if (MainSwitcher._UCWelcomeMainSwitcher.View_UCRecoverPasswordSuggestionPanel.MblSgn.Equals(mblwithDc))
    //            //                {
    //            //                    CustomMessageBox.ShowInformation("Your recovery number has been verified successfully!", "Success!");
    //            //                    MainSwitcher._UCWelcomeMainSwitcher.LoadUI(WelcomePanelConstants.TypePasswordReset);
    //            //                    MainSwitcher._UCWelcomeMainSwitcher.View_UCRecoverResetPassword.LoadMobileData(MainSwitcher._UCWelcomeMainSwitcher.View_UCRecoverPasswordSuggestionPanel.userIdentity, MainSwitcher._UCWelcomeMainSwitcher.View_UCRecoverPasswordSuggestionPanel.MblSgn);
    //            //                }
    //            //                else
    //            //                {
    //            //                    CustomMessageBox.ShowError("Sorry! You've verified the wrong number! Please  try again!", "Failed!");
    //            //                }
    //            //                if (MainSwitcher.ThreadManager().DigitsVerificationThread != null) MainSwitcher.ThreadManager().DigitsVerificationThread.StopThisThread();
    //            //                MainSwitcher.ThreadManager().DigitsVerificationThread = null;
    //            //            }
    //            //        }
    //            //    }
    //            //    else if (RingIDViewModel.Instance != null && RingIDViewModel.Instance._WNRingIDSettings != null
    //            //        && RingIDViewModel.Instance._WNRingIDSettings._AccountSettings != null
    //            //        && RingIDViewModel.Instance._WNRingIDSettings._AccountSettings.ucRecoverySettings.ucDigitsCustomMsgPopup != null)
    //            //    {
    //            //        RingIDViewModel.Instance._WNRingIDSettings._AccountSettings.ucRecoverySettings.ucDigitsCustomMsgPopup.Hide();
    //            //        if (SuccessfullyVerified && RingIDViewModel.Instance._WNRingIDSettings._AccountSettings.ucRecoverySettings.IsVisible && !string.IsNullOrEmpty(mbl) && !string.IsNullOrEmpty(mbldc))
    //            //        {
    //            //            new SendandVerifyAfterReg(mbl, mbldc, null, true);
    //            //            //RingIDViewModel.Instance._WNRingIDSettings._RecoverySettings.LoadVerifiedMobileNumberFromDigits(mbl, mbldc);
    //            //        }
    //            //    }
    //            //});
    //        }

    //        public void ProcessStartFriendChat(ChatInitiatorDTO initiatorDTO, Func<long, bool, int> onComplete = null)
    //        {
    //            try
    //            {
    //#if CHAT_LOG
    //                log.Debug("AUTH::FRIEND::RESPONSE".PadRight(65, ' ') + "==>   initiatorDTO = " + initiatorDTO.ToString());
    //#endif

    //                UserBasicInfoModel _FriendModel = RingIDViewModel.Instance.GetFriendBasicInfoModelByID(initiatorDTO.FriendIdentity);
    //                if (initiatorDTO.ReasonCode == ReasonCodeConstants.REASON_CODE_PERMISSION_DENIED)
    //                {
    //                    log.Debug("AUTH::FRIEND::REGISTER_DENIED_FOR_PERMISSION".PadRight(65, ' ') + "==>   friendId = " + initiatorDTO.FriendIdentity);

    //                    if (_FriendModel != null)
    //                    {
    //                        _FriendModel.ShortInfoModel.ChatRegDeniedForPermision = true;
    //                        _FriendModel.OnPropertyChanged("CurrentInstance");
    //                    }

    //                    UserBasicInfoDTO userDTO = null;
    //                    if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(initiatorDTO.FriendIdentity, out userDTO))
    //                    {
    //                        userDTO.ChatRegDeniedForPermission = true;
    //                    }

    //                    if (onComplete != null)
    //                    {
    //                        onComplete(initiatorDTO.FriendIdentity, false);
    //                    }


    //                    return;
    //                }
    //                else if (initiatorDTO.ReasonCode == ReasonCodeConstants.REASON_CODE_THIS_IS_SPECIAL_FRIEND)
    //                {
    //                    log.Debug("AUTH::FRIEND::REGISTER_DENIED_FOR_SPECIAL_FRIEND".PadRight(65, ' ') + "==>   friendId = " + initiatorDTO.FriendIdentity);

    //    return;
    //}

    //                    if (_FriendModel != null)
    //                    {
    //                        _FriendModel.ShortInfoModel.ChatRegDeniedForSpecial = true;
    //                    }

    //                    UserBasicInfoDTO userDTO = null;
    //                    if (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(initiatorDTO.FriendIdentity, out userDTO))
    //                    {
    //                        userDTO.ChatRegDeniedForSpecial = true;
    //                    }

    //                    if (onComplete != null)
    //                    {
    //                        onComplete(initiatorDTO.FriendIdentity, false);
    //                    }
    //                    return;
    //                }

    //                if (initiatorDTO.FriendIdentity > 0)
    //                {
    //                    if (_FriendModel != null)
    //                    {
    //                        ChatService.RequestFriendChatHistory(initiatorDTO.FriendIdentity, _FriendModel.ChatMaxPacketID, ChatConstants.HISTORY_DOWN, 100, null);

    //                    UserBasicInfoDTO userDTO = null;
    //                    if (!FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(initiatorDTO.FriendIdentity, out userDTO))
    //                    {
    //                        userDTO = new UserBasicInfoDTO();
    //                    }

    //                    if (userDTO.FriendShipStatus == 0)
    //                    {
    //                        List<UserBasicInfoDTO> list = new List<UserBasicInfoDTO>();
    //                        userDTO.UserIdentity = initiatorDTO.FriendIdentity;
    //                        userDTO.Presence = initiatorDTO.Presence;
    //                        userDTO.Mood = initiatorDTO.Mood;
    //                        userDTO.Device = initiatorDTO.Device;
    //                        userDTO.DeviceToken = initiatorDTO.DeviceToken;
    //                        userDTO.FullName = initiatorDTO.FullName;
    //                        list.Add(userDTO);
    //                        new InsertIntoUserBasicInfoTable(list).Start();
    //                        FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[initiatorDTO.FriendIdentity] = userDTO;
    //                    }
    //                    userDTO.ChatRegDeniedForPermission = false;
    //                    userDTO.ChatRegDeniedForSpecial = false;
    //                    if (_FriendModel == null)
    //                    {
    //                        //Application.Current.Dispatcher.Invoke(() =>
    //                        //{
    //                        //    AddRemoveInCollections.AddIntoUnknownFriendList(new UserBasicInfoModel(userDTO));
    //                        //});
    //                        userDTO.FriendShipStatus = 0;
    //                        FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModelsByDto(userDTO);
    //                    }

    //                    if (initiatorDTO.ActionType == AppConstants.TYPE_UPDATE_START_FRIEND_CHAT)
    //                    {
    //                        bool isHanlderCreated = false;
    //                        lock (ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT)
    //                        {
    //                            ChatEventHandler eventhandler = ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT.TryGetValue(initiatorDTO.FriendIdentity.ToString());
    //                            if (eventhandler == null)
    //                            {
    //                                eventhandler = new ChatEventHandler { FriendIdentity = initiatorDTO.FriendIdentity };
    //                                ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT[initiatorDTO.FriendIdentity.ToString()] = eventhandler;
    //                                isHanlderCreated = true;
    //                            }
    //                        }

    //                        if (onComplete != null)
    //                        {
    //                            onComplete(initiatorDTO.FriendIdentity, true);
    //                        }

    //                        if (isHanlderCreated)
    //                        {
    //#if CHAT_LOG
    //                            log.Debug("AUTH::FRIEND::NEW_REGISTRATION".PadRight(65, ' ') + "==>   friendId = " + initiatorDTO.FriendIdentity);
    //#endif

    //                            ChatService.RegisterFriendChat(initiatorDTO.FriendIdentity, initiatorDTO.ChatServerIP, initiatorDTO.ChatRegisterPort, initiatorDTO.Device, initiatorDTO.DeviceToken, initiatorDTO.ApplicationType, initiatorDTO.Presence, initiatorDTO.Mood);
    //                        }
    //                        else
    //                        {
    //#if CHAT_LOG
    //                            log.Debug("AUTH::FRIEND::REGISTRATION_ALREADY_RUNNING".PadRight(65, ' ') + "==>   friendId = " + initiatorDTO.FriendIdentity);
    //#endif
    //                        }
    //                    }
    //                    else
    //                    {
    //                        if (onComplete != null)
    //                        {
    //                            onComplete(initiatorDTO.FriendIdentity, true);
    //                        }

    //                        ChatService.RegisterFriendChat(initiatorDTO.FriendIdentity, initiatorDTO.ChatServerIP, initiatorDTO.ChatRegisterPort, initiatorDTO.Device, initiatorDTO.DeviceToken, initiatorDTO.ApplicationType, initiatorDTO.Presence, initiatorDTO.Mood);
    //                    }
    //                }
    //                else
    //                {
    //                    if (onComplete != null)
    //                    {
    //                        onComplete(initiatorDTO.FriendIdentity, false);
    //                    }
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: ProcessStartFriendChat() => " + ex.Message + "\n" + ex.StackTrace);
    //                if (onComplete != null)
    //                {
    //                    onComplete(initiatorDTO.FriendIdentity, false);
    //                }
    //            }
    //        }

    //        public void ProcessStartGroupChat(ChatInitiatorDTO initiatorDTO, Func<long, bool, int> onComplete = null)
    //        {
    //            try
    //            {
    //#if CHAT_LOG
    //                log.Debug("AUTH::GROUP::RESPONSE".PadRight(65, ' ') + "==>   initiatorDTO = " + initiatorDTO.ToString());
    //#endif

    //                GroupInfoModel groupInfoModel = RingIDViewModel.Instance.GetGroupInfoModelByGroupID(initiatorDTO.GroupID);

    //                if (initiatorDTO.ActionType == AppConstants.TYPE_UPDATE_START_GROUP_CHAT)
    //                {
    //                    if (groupInfoModel.IsPartial || !groupInfoModel.MemberInfoDictionary.ContainsKey(DefaultSettings.LOGIN_USER_ID))
    //                    {
    //                        ChatService.GetGroupInformationWithMembers(initiatorDTO.GroupID, null);
    //                    }
    //                }
    //                else if (initiatorDTO.ActionType == AppConstants.TYPE_UPDATE_ADD_GROUP_MEMBER)
    //                {
    //                    ChatService.GetGroupInformationWithMembers(initiatorDTO.GroupID, null);
    //                }

    //                ChatService.RequestGroupChatHistory(groupInfoModel.GroupID, groupInfoModel.ChatMaxPacketID, ChatConstants.HISTORY_DOWN, 100, null);

    //                if (initiatorDTO.ActionType == AppConstants.TYPE_UPDATE_START_GROUP_CHAT || initiatorDTO.ActionType == AppConstants.TYPE_UPDATE_ADD_GROUP_MEMBER)
    //                {
    //                    bool isHanlderCreated = false;
    //                    lock (ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT)
    //                    {
    //                        ChatEventHandler eventhandler = ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT.TryGetValue(initiatorDTO.GroupID.ToString());
    //                        if (eventhandler == null)
    //                        {
    //                            eventhandler = new ChatEventHandler { GroupID = initiatorDTO.GroupID };
    //                            ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT[initiatorDTO.GroupID.ToString()] = eventhandler;
    //                            isHanlderCreated = true;
    //                        }
    //                    }

    //                    if (onComplete != null)
    //                    {
    //                        onComplete(initiatorDTO.GroupID, true);
    //                    }

    //                    if (isHanlderCreated)
    //                    {
    //#if CHAT_LOG
    //                        log.Debug("AUTH::GROUP::NEW_REGISTRATION".PadRight(65, ' ') + "==>   groupId = " + initiatorDTO.GroupID);
    //#endif
    //                        ChatService.RegisterGroupChat(initiatorDTO.GroupID, initiatorDTO.ChatServerIP, initiatorDTO.ChatRegisterPort);
    //                    }
    //                    else
    //                    {
    //#if CHAT_LOG
    //                        log.Debug("AUTH::GROUP::REGISTRATION_ALREADY_RUNNING".PadRight(65, ' ') + "==>   groupId = " + initiatorDTO.GroupID);
    //#endif
    //                    }
    //                }
    //                else
    //                {
    //                    if (onComplete != null)
    //                    {
    //                        onComplete(initiatorDTO.GroupID, true);
    //                    }

    //                    ChatService.RegisterGroupChat(initiatorDTO.GroupID, initiatorDTO.ChatServerIP, initiatorDTO.ChatRegisterPort);
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: ProcessStartGroupChat() => " + ex.Message + "\n" + ex.StackTrace);
    //                if (onComplete != null)
    //                {
    //                    onComplete(initiatorDTO.GroupID, false);
    //                }
    //            }
    //        }

    //        public void StartKeepAliveThread()
    //        {
    //            MainSwitcher.ThreadManager().StartKeepAliveThread();
    //            //  (new KeepAlive()).StartMainKeepAlive();
    //        }

    //        #region "Friend Info Change or Edit/Add/Delete"
    //        //  UserBasicInfoDTO userBasicInfo 
    //        public void UI_ProcessByteContactList(List<UserBasicInfoDTO> basicInfos, bool success)//23
    //        {
    //            if (success)
    //            {
    //                if (basicInfos != null)
    //                {
    //                    FriendListLoadUtility.LoadDataFromServer(basicInfos);
    //                    //  HelperMethodsAuth.AuthHandlerInstance.AddFriendListFromServer(basicInfos);
    //                }
    //                if (FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Count == 0 && DefaultSettings.FRIEND_LIST_LOADED == false)
    //                {
    //                    //  AddFriendsNotificationForIncomingRequest();
    //                    //     FriendListLoadUtility.AddFriendsNotificationForIncomingRequest();
    //                    NotifyAllContactListLoaded();
    //                    new InsertIntoUserBasicInfoTable(FriendDictionaries.Instance.TEMP_CONTACT_LIST, true, () =>
    //                    {
    //                        FriendDictionaries.Instance.TEMP_CONTACT_LIST.Clear();
    //                        return 0;
    //                    }).Start();
    //                }
    //            }
    //            else if (FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Count == 0 && DefaultSettings.FRIEND_LIST_LOADED == false)
    //            {

    //                NotifyAllContactListLoaded();
    //            }

    //        }

    //        public void UI_ProcessDeleteFriendUpdate(UserBasicInfoDTO userBasicInfo)//328
    //        {
    //            UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelByID(userBasicInfo.UserIdentity);
    //            if (model != null)
    //            {
    //                FriendListLoadUtility.RemoveAModelFromUI(userBasicInfo, model, true);
    //                LoadUserBasicInfoDto(model, userBasicInfo);
    //                HideFriendProfileTabForNF(userBasicInfo);
    //                HideFriendAboutInfos(userBasicInfo);
    //                ReloadFriendsMutualFriendsProfile();
    //                //AddFriendsNotificationForIncomingRequest();
    //                FriendListController.Instance.FriendDataContainer.TotalFriends();
    //            }
    //        }

    //        public void UI_ProcessDeleteFriend(UserBasicInfoDTO userBasicInfo)//128
    //        {
    //            UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelByID(userBasicInfo.UserIdentity);
    //            if (model != null)
    //            {
    //                FriendListLoadUtility.RemoveAModelFromUI(userBasicInfo, model, true);
    //                LoadUserBasicInfoDto(model, userBasicInfo);
    //                HelperMethodsAuth.AuthHandlerInstance.HideFriendProfileTabForNF(userBasicInfo);
    //                HelperMethodsAuth.AuthHandlerInstance.HideFriendAboutInfos(userBasicInfo);
    //                FriendListController.Instance.FriendDataContainer.TotalFriends();
    //            }
    //        }

    //        public void UI_AcceptFriendRequest(UserBasicInfoDTO userBasicInfo)//129
    //        {
    //            UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelByID(userBasicInfo.UserIdentity);
    //            if (model != null)
    //            {
    //                FriendListLoadUtility.RemoveAModelFromUI(userBasicInfo, model, false);
    //                LoadUserBasicInfoDto(model, userBasicInfo);
    //                FriendListLoadUtility.AddAModelIntoUI(userBasicInfo, model, AddRemoveInCollections.Dictonary_NonFavouriteFriendList);
    //                SetRequestsForFriendViewedandAdded(userBasicInfo);
    //                ReloadFriendsMutualFriendsProfile();
    //                CheckFriendPresence(userBasicInfo.UserIdentity);
    //            }
    //            else
    //            {
    //                model = new UserBasicInfoModel(userBasicInfo);
    //                FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
    //                FriendListLoadUtility.AddAModelIntoUI(userBasicInfo, model, AddRemoveInCollections.Dictonary_NonFavouriteFriendList);
    //            }
    //            FriendListController.Instance.FriendDataContainer.TotalFriends();
    //        }

    //        public void UI_ProcessAcceptFriendUpdate(UserBasicInfoDTO userBasicInfo)//329
    //        {
    //            try
    //            {
    //                UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelByID(userBasicInfo.UserIdentity);
    //                if (model != null)
    //                {
    //                    FriendListLoadUtility.RemoveAModelFromUI(userBasicInfo, model, false);
    //                    LoadUserBasicInfoDto(model, userBasicInfo);
    //                    FriendListLoadUtility.AddAModelIntoUI(userBasicInfo, model, AddRemoveInCollections.Dictonary_NonFavouriteFriendList);
    //                    SetRequestsForFriendViewedandAdded(userBasicInfo);
    //                    ReloadFriendsMutualFriendsProfile();
    //                }
    //                else
    //                {
    //                    model = new UserBasicInfoModel(userBasicInfo);
    //                    FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
    //                    FriendListLoadUtility.AddAModelIntoUI(userBasicInfo, model, AddRemoveInCollections.Dictonary_NonFavouriteFriendList);
    //                }
    //                FriendListController.Instance.FriendDataContainer.TotalFriends();
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error(ex.StackTrace);
    //            }

    //        }

    //        public void UI_ProcessAddFriendUpdate(UserBasicInfoDTO userBasicInfo)//327
    //        {
    //            try
    //            {
    //                UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelByID(userBasicInfo.UserIdentity);
    //                if (model != null)
    //                {
    //                    FriendListLoadUtility.RemoveAModelFromUI(userBasicInfo, model, false);
    //                    LoadUserBasicInfoDto(model, userBasicInfo);
    //                    FriendListLoadUtility.AddAModelIntoUI(userBasicInfo, model, AddRemoveInCollections.Dictonary_IncomingRequestFriendList);
    //                    CheckFriendPresence(userBasicInfo.UserIdentity);
    //                }
    //                else
    //                {
    //                    model = new UserBasicInfoModel(userBasicInfo);
    //                    FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
    //                    FriendListLoadUtility.AddAModelIntoUI(userBasicInfo, model, AddRemoveInCollections.Dictonary_IncomingRequestFriendList);
    //                }
    //                if (userBasicInfo.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_INCOMING)
    //                {
    //                    AppConstants.ADD_FRIEND_NOTIFICATION_COUNT++;
    //                    FriendListLoadUtility.AddFriendsNotificationForIncomingRequest();
    //                }

    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error(ex.StackTrace);
    //            }
    //        }
    //        public void UI_ProcessPushAddFriend(UserBasicInfoDTO userBasicInfo)
    //        {
    //            UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelByID(userBasicInfo.UserIdentity);
    //            if (model != null)
    //            {
    //                FriendListLoadUtility.RemoveAModelFromUI(userBasicInfo, model, false);
    //                LoadUserBasicInfoDto(model, userBasicInfo);
    //                FriendListLoadUtility.AddAModelIntoUI(userBasicInfo, model, AddRemoveInCollections.Dictonary_OutgoingRequestFriendList);
    //                CheckFriendPresence(userBasicInfo.UserIdentity);
    //            }
    //            else
    //            {
    //                model = new UserBasicInfoModel(userBasicInfo);
    //                model.VisibilityModel.ShowActionButton = false;
    //                model.VisibilityModel.ShowLoading = Visibility.Visible;
    //                FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
    //                FriendListLoadUtility.AddAModelIntoUI(userBasicInfo, model, AddRemoveInCollections.Dictonary_OutgoingRequestFriendList);
    //            }
    //            if (UCAddFriendPending.Instance != null)
    //            {
    //                UCAddFriendPending.Instance.ShowIncommingAndPendingCounts();
    //            }
    //        }

    //        public void UI_ProcessTypeFriendPresenceInfo(UserBasicInfoDTO userBasicInfo)
    //        {
    //            AddIntoModelDictionary(userBasicInfo);
    //        }

    //        public void UI_AMPProfileAbout(UserBasicInfoDTO userBasicInfo)
    //        {
    //            AddIntoModelDictionary(userBasicInfo);
    //            ShowFriendBasicInfo(userBasicInfo);
    //            ChangeCallChatLogsOnFriendUpdate(userBasicInfo.UserIdentity);
    //        }

    //        public void UI_FriendDetailsInfoRequest(UserBasicInfoDTO userBasicInfo)
    //        {
    //            ChangeFriendProfile(userBasicInfo);
    //            //HelperMethodsAuth.AuthHandlerInstance.ShowFriendBasicInfo(_FriendDetailsInDictionary);/*No need to initialize about-28.01.16*/
    //        }

    //        public void UI_UnknownProfileInfoRequest(UserBasicInfoDTO unknownProfileDTO, bool alreadyFriendRequested)
    //        {
    //            AddIntoModelDictionary(unknownProfileDTO, null);
    //            ShowFriendBasicInfo(unknownProfileDTO);
    //            ChangeCallChatLogsOnFriendUpdate(unknownProfileDTO.UserIdentity);
    //            if (alreadyFriendRequested)
    //            {
    //                FriendListLoadUtility.MoveAFriendOneToAnotherList(unknownProfileDTO);
    //            }
    //        }

    //        public void UI_ProcessContactSearch(long searchCount, long matchCount, string packId, List<UserBasicInfoDTO> listuserBasicInfoDTO) //34
    //        {
    //            try
    //            {
    //                int ui = RingDictionaries.Instance.SEARCHING_UI_NAME_DICTIONARY.FirstOrDefault(x => x.Value == packId).Key;
    //                if (ui == StatusConstants.SEARCH_FRIENDLIST)
    //                {
    //                    new FriendListSearchResult().StartProcessing(searchCount, matchCount, listuserBasicInfoDTO);
    //                }
    //                else if (ui == StatusConstants.SEARCH_ADD_FRIEND_OR_DIALPAD)
    //                    new AddContactSearchResult().StartProcessing(searchCount, matchCount, listuserBasicInfoDTO);
    //            }
    //            catch (Exception e)
    //            {
    //                log.Error("Exception in AuthHandler ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
    //            }
    //        }

    //        #endregion "Friend Info Change or Edit/Add/Delete"

    //        #region "Private methods"

    //        private bool checkMainWindowIsActive()
    //        {
    //            return UIHelperMethods.IsViewActivate(MainSwitcher.MainController().MainUIController().MainWindow);
    //        }

    //        private void changeTaskOverlay()
    //        {
    //            MainSwitcher.MainController().MainUIController().ChangeTaskBarOverlay();
    //        }

    //        private void LoadUserBasicInfoDto(UserBasicInfoModel model, UserBasicInfoDTO userDTO)
    //        {
    //            if (model != null)
    //            {
    //                model.LoadData(userDTO);
    //                model.ShortInfoModel.OnPropertyChanged("FriendShipStatus");
    //                model.OnPropertyChanged("Presence");
    //                model.OnPropertyChanged("CallAccess");
    //                model.OnPropertyChanged("ChatAccess");
    //                model.OnPropertyChanged("FeedAccess");
    //                model.OnPropertyChanged("CurrentInstance");
    //                model.ShortInfoModel.OnPropertyChanged("CurrentInstance");
    //            }

    //        }

    //        private void NotifyAllContactListLoaded()
    //        {
    //            try
    //            {
    //                UCGuiRingID.Instance.HideAdvertiseSlider();
    //                Models.Constants.DefaultSettings.FRIEND_LIST_LOADED = true;
    //                ImageDictionaries.Instance.RING_IMAGE_QUEUE.GetItem();
    //            }
    //            catch (Exception ex)
    //            {
    //                log.Error("Error: NotifyAllContactListLoaded() => " + ex.Message + "\n" + ex.StackTrace);
    //            }
    //        }

    //        private UserBasicInfoModel AddIntoModelDictionary(UserBasicInfoDTO userDTO, UserBasicInfoModel model = null)
    //        {
    //            if (model == null)
    //                model = RingIDViewModel.Instance.GetFriendBasicInfoModelByID(userDTO.UserIdentity);
    //            if (model != null)
    //            {
    //                LoadUserBasicInfoDto(model, userDTO);
    //            }
    //            else
    //            {
    //                model = new UserBasicInfoModel(userDTO);
    //                FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
    //            }
    //            return model;
    //        }
    //        #endregion "Private Methods"

    //    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Media;
using View.Utility.Call;

namespace View.Utility
{
    public class PanelTransfromFromRB
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private Grid parentElement;
        //  private DockPanel targetDockpanel = null;
        public int position = 0;
        TranslateTransform translate = null;
        FrameworkElement element = null;

        public PanelTransfromFromRB(Grid panel)
        {
            this.parentElement = panel;
        }
        public void Translate()
        {
            if (translate != null && element != null)
            {
                if (position == 0)//00
                {
                    translate.X = 0;
                    translate.Y = 0;
                }
                else if (position == 1)//01
                {
                    translate.X = -(parentElement.ActualWidth - element.ActualWidth);
                    translate.Y = 0;
                }
                else if (position == 2)//10
                {
                    translate.X = 0;
                    translate.Y = -(parentElement.ActualHeight - element.ActualHeight);
                }
                else if (position == 3)//11
                {
                    translate.X = -(parentElement.ActualWidth - element.ActualWidth);
                    translate.Y = -(parentElement.ActualHeight - element.ActualHeight);
                }
            }
        }

        Point m_start;
        Vector m_startOffset;
        public void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            element = sender as DockPanel;
            translate = element.RenderTransform as TranslateTransform;

            m_start = e.GetPosition(parentElement);
            m_startOffset = new Vector(translate.X, translate.Y);
            element.CaptureMouse();
        }

        public void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            element = sender as DockPanel;
            translate = element.RenderTransform as TranslateTransform;

            if (element.IsMouseCaptured)
            {
                Vector offset = Point.Subtract(e.GetPosition(parentElement), m_start);

                if (offset.X < 0)
                {
                    if (m_startOffset.X + offset.X > -(parentElement.ActualWidth - element.ActualWidth))
                        translate.X = m_startOffset.X + offset.X;
                }
                else
                {
                    if (m_startOffset.X + offset.X < 0)
                        translate.X = m_startOffset.X + offset.X;
                }
                if (offset.Y < 0)
                {
                    if (m_startOffset.Y + offset.Y > -(parentElement.ActualHeight - element.ActualHeight))
                        translate.Y = m_startOffset.Y + offset.Y;
                }
                else
                {
                    if (m_startOffset.Y + offset.Y < 0)
                        translate.Y = m_startOffset.Y + offset.Y;
                }
                // translate.Y = m_startOffset.Y + offset.Y;
            }
            //}
        }

        public void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            element = sender as DockPanel;
            TranslateTransform translate = element.RenderTransform as TranslateTransform;

            int halfHeight = (int)parentElement.ActualHeight / 2;
            int halfWidth = (int)parentElement.ActualWidth / 2;
            double xValue = Math.Abs(translate.X);
            double yValue = Math.Abs(translate.Y);

            if (xValue <= halfWidth && yValue <= halfHeight)
            {
                position = 0;
            }
            else if (xValue >= halfWidth && yValue <= halfHeight)
            {
                position = 1;
            }
            else if (xValue <= halfWidth && yValue >= halfHeight)
            {
                position = 2;
            }
            else if (xValue >= halfWidth && yValue >= halfHeight)
            {
                position = 3;
            }
            Translate();
            element.ReleaseMouseCapture();
        }

    }
}

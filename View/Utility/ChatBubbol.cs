﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace View.Utility
{
    public class ChatBubbol : Shape
    {
        //test
        public static readonly DependencyProperty ArrowProperty;
        public static readonly DependencyProperty PaddingProperty;
        public static readonly DependencyProperty RadiusXProperty;
        public static readonly DependencyProperty RadiusYProperty;

        public Arrow Arrow
        {
            get { return (Arrow)GetValue(ArrowProperty); }
            set { SetValue(ArrowProperty, value); }
        }

        public Thickness Padding
        {
            get { return (Thickness)GetValue(PaddingProperty); }
            set { SetValue(PaddingProperty, value); }
        }

        public int RadiusX
        {
            get { return (int)GetValue(RadiusXProperty); }
            set { SetValue(RadiusXProperty, value); }
        }

        public int RadiusY
        {
            get { return (int)GetValue(RadiusYProperty); }
            set { SetValue(RadiusYProperty, value); }
        }

        static ChatBubbol()
        {
            ArrowProperty = DependencyProperty.Register("Arrow", typeof(Arrow), typeof(ChatBubbol), new FrameworkPropertyMetadata(Arrow.Left, (FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault)));
            PaddingProperty = DependencyProperty.Register("Padding", typeof(Thickness), typeof(ChatBubbol));
            RadiusXProperty = DependencyProperty.Register("RadiusX", typeof(int), typeof(ChatBubbol));
            RadiusYProperty = DependencyProperty.Register("RadiusY", typeof(int), typeof(ChatBubbol));
        }

        protected override Geometry DefiningGeometry
        {
            get
            {
                CombinedGeometry temp = null;

                Geometry result = new RectangleGeometry(new Rect(0 + Padding.Left, 0 + Padding.Top, Math.Max(base.ActualWidth - Padding.Left - Padding.Right, 0), Math.Max(base.ActualHeight - Padding.Top - Padding.Bottom, 0)), RadiusX, RadiusY);
                double halfWidth = (result.Bounds.Width) / 2;
                double halfHeight = (result.Bounds.Height) / 2;

                if (Arrow == Arrow.Left)
                {
                    if (Padding.Left > 0)
                    {
                        PathGeometry pathGeometry = new PathGeometry();
                        PathFigure figure = new PathFigure { StartPoint = new Point(0, 0 + Padding.Top - 2), IsClosed = true };
                        figure.Segments.Add(new ArcSegment { Point = new Point(0, Padding.Left * 2 - 2), Size = new Size(1, 1), SweepDirection = SweepDirection.Clockwise });
                        pathGeometry.Figures.Add(figure);

                        temp = new CombinedGeometry(GeometryCombineMode.Union, new RectangleGeometry(new Rect(0, Padding.Left - 2, Padding.Left, Padding.Left)), pathGeometry);
                        temp = new CombinedGeometry(GeometryCombineMode.Exclude, new RectangleGeometry(new Rect(0, 0 + Padding.Top, Padding.Left, Padding.Left * 2 - 2)), temp);
                        temp = new CombinedGeometry(GeometryCombineMode.Union, new RectangleGeometry(new Rect(0 + Padding.Left, 0 + Padding.Top, halfWidth, halfHeight)), temp);
                        result = new CombinedGeometry(GeometryCombineMode.Union, result, temp);
                    }
                }
                else if (Arrow == Arrow.Right)
                {
                    if (Padding.Right > 0)
                    {
                        PathGeometry pathGeometry = new PathGeometry();
                        PathFigure figure = new PathFigure { StartPoint = new Point(base.ActualWidth, 0 + Padding.Top - 2), IsClosed = true };
                        figure.Segments.Add(new ArcSegment { Point = new Point(base.ActualWidth, Padding.Right * 2 - 2), Size = new Size(1, 1), SweepDirection = SweepDirection.Counterclockwise });
                        pathGeometry.Figures.Add(figure);

                        temp = new CombinedGeometry(GeometryCombineMode.Union, new RectangleGeometry(new Rect(base.ActualWidth - Padding.Right, Padding.Right - 2, Padding.Right, Padding.Right)), pathGeometry);
                        temp = new CombinedGeometry(GeometryCombineMode.Exclude, new RectangleGeometry(new Rect(base.ActualWidth - Padding.Right, 0 + Padding.Top, Padding.Right, Math.Max(Padding.Right * 2 - 2, 0))), temp);
                        temp = new CombinedGeometry(GeometryCombineMode.Union, new RectangleGeometry(new Rect(halfWidth + Padding.Left, 0 + Padding.Top, halfWidth, halfHeight)), temp);
                        result = new CombinedGeometry(GeometryCombineMode.Union, result, temp);
                    }
                }
                return result;
            }
        }
    }

    public enum Arrow
    {
        None = 0,
        Left = 1,
        Right = 2,
    }
}

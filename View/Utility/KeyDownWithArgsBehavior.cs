﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace View.Utility
{
    public class KeyDownWithArgsBehavior : Behavior<UIElement>
    {
        public ICommand KeyDownCommand
        {
            get { return (ICommand)GetValue(KeyDownCommandProperty); }
            set { SetValue(KeyDownCommandProperty, value); }
        }

        public static readonly DependencyProperty KeyDownCommandProperty =  DependencyProperty.Register("KeyDownCommand", typeof(ICommand), typeof(KeyDownWithArgsBehavior), new UIPropertyMetadata(null));

        protected override void OnAttached()
        {
            AssociatedObject.PreviewKeyDown += new KeyEventHandler(AssociatedObjectKeyDown);
            base.OnAttached();
        }

        protected override void OnDetaching()
        {
            AssociatedObject.PreviewKeyDown -= new KeyEventHandler(AssociatedObjectKeyDown);
            base.OnDetaching();
        }

        private void AssociatedObjectKeyDown(object sender, KeyEventArgs e)
        {
            if (KeyDownCommand != null)
            {
                KeyDownCommand.Execute(e);
            }
        }
    }
}

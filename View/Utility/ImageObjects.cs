﻿using log4net;
using Models.Entity;
using Models.Stores;
using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using View.Constants;
using System.Drawing;

namespace View.Utility
{
    class ImageObjects
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ImageObjects).Name);

        private static ConcurrentDictionary<string, BitmapImage> _EMOTICON_ICON = null;
        private static BitmapFrame _APP_ICON = null;
        //private static BitmapImage _OFFLINE_OVERLAY = null;
        //      private static BitmapImage _SPLASH_SCREEN_BEFORE_LOGIN = null;
        private static BitmapImage _LOGIN_BG = null;
        private static BitmapImage _WELCOME_BG_IMAGE = null;
        private static BitmapImage _WELCOME_LOGO_IMAGE = null;
        private static BitmapImage _DEFAULT_MY_PROFILE_BG_IMAGE = null;
        private static BitmapImage _MY_PROFILE_BG_IMAGE_DEFAULT = null;
        private static BitmapImage _MY_PROFILE_BG_IMAGE_PRESSED = null;
        private static BitmapImage _DEFAULT_MY_PROFILE_BG_IMAGE_H = null;
        private static BitmapImage _GROUP_TOP = null;
        private static BitmapImage _GROUP_TOP_H = null;
        private static BitmapImage _GROUP_TOP_SELECTED = null;
        //private static BitmapImage _CIRCLE_TOP = null;
        //private static BitmapImage _CIRCLE_TOP_H = null;
        //private static BitmapImage _CIRCLE_TOP_SELECTED = null;
        private static BitmapImage _DIALPAD_TOP = null;
        private static BitmapImage _DIALPAD_TOP_H = null;
        private static BitmapImage _DIALPAD_TOP_SELECTED = null;
        private static BitmapImage _CALL_BOTTOM = null;
        //private static BitmapImage _CALL_BOTTOM_SELECTED = null;
        private static BitmapImage _UNKNOWN_GROUP_IMAGE = null;
        private static BitmapImage _UNKNOWN_GROUP_LARGE_IMAGE = null;
        private static BitmapImage _UNKNOWN_ROOM_IMAGE = null;
        private static BitmapImage _UNKNOWN_IMAGE_FOR_MUTUAL_FRIEND = null;
        private static BitmapImage _ALL_NOTIFICATION = null;
        private static BitmapImage _ALL_NOTIFICATION_H = null;
        private static BitmapImage _ALL_NOTIFICATION_SELECTED = null;
        private static BitmapImage _FRIEND_ICON = null;
        private static BitmapImage _FRIEND_ICON_H = null;
        private static BitmapImage _FRIEND_ICON_SELECTED = null;

        private static BitmapImage _MUSIC_VIDEO = null;
        private static BitmapImage _MUSIC_VIDEO_H = null;
        private static BitmapImage _MUSIC_VIDEO_SELECTED = null;
        private static BitmapImage _MEDIA_FEED = null;
        private static BitmapImage _MEDIA_FEED_H = null;
        private static BitmapImage _MEDIA_FEED_SELECTED = null;
        private static BitmapImage _CIRCLE_ICON = null;
        private static BitmapImage _CIRCLE_ICON_H = null;
        private static BitmapImage _CIRCLE_ICON_SELECTED = null;
        private static BitmapImage _STICKER_MARKET = null;
        private static BitmapImage _STICKER_MARKET_H = null;
        private static BitmapImage _STICKER_MARKET_SELECTED = null;

        private static BitmapImage _MSG_ICON = null;
        private static BitmapImage _MSG_ICON_H = null;
        private static BitmapImage _MSG_ICON_SELECTED = null;
        private static BitmapImage _CALLHISTORY_ICON = null;
        private static BitmapImage _CALLHISTORY_ICON_H = null;
        private static BitmapImage _CALLHISTORY_ICON_SELECTED = null;
        private static BitmapImage _ADD_FRIEND_STICKER = null;
        private static BitmapImage _ADD_FRIEND_STICKER_H = null;
        private static BitmapImage _ADD_FRIEND_STICKER_SELECTED = null;
        private static BitmapImage _ADD_FRIEND_STICKER_HOME_H = null;
        private static BitmapImage _CHAT_CALL_DEFAULT = null;
        private static BitmapImage _CHAT_CALL_SELECTED = null;
        private static BitmapImage _ID_DEFAULT = null;
        private static BitmapImage _ID_SELECTED = null;
        private static BitmapImage _BUTTON_FRIEND_ADD = null;
        private static BitmapImage _BUTTON_FRIEND_ADD_H = null;
        private static BitmapImage _BUTTON_INFO = null;
        private static BitmapImage _BUTTON_INFO_H = null;
        private static BitmapImage _BUTTON_CHAT = null;
        private static BitmapImage _BUTTON_CHAT_H = null;
        private static BitmapImage _BUTTON_VOICE_CALL = null;
        private static BitmapImage _BUTTON_VOICE_CALL_H = null;
        private static BitmapImage _BUTTON_VIDEO_CALL = null;
        private static BitmapImage _BUTTON_VIDEO_CALL_H = null;
        private static BitmapImage _CIRCLE_OUTSIDE_PROFILE_PIC = null;
        private static BitmapImage _CIRCLE_OUTSIDE_PROFILE_PIC_MYNAMEPANEL = null;
        private static BitmapImage _CIRCLE_OUTSIDE_PROFILE_PIC_PROFILE = null;
        private static BitmapImage _UNKNOWN_IMAGE = null;
        private static BitmapImage _LOADER_MEDIUM = null;
        private static BitmapImage _LOADER_SMALL = null;
        private static BitmapImage _UNKNOWN_IMAGE_LARGE = null;
        private static BitmapImage _DEFAULT_COVER_IMAGE = null;
        private static BitmapImage _NO_IMAGE_FOUND = null;
        private static BitmapImage _COVER_IMAGE_BACKGROUND = null;
        private static BitmapImage _PROFILE_IMAGE_UNKNOWN = null;
        private static BitmapImage _EDIT_INFO = null;
        private static BitmapImage _CELEBRITY_DEFAULT_IMAGE = null;
        private static BitmapImage _ROOM_DEFAULT_IMAGE = null;

        private static BitmapImage _MENU_MOOD = null;
        private static BitmapImage _MENU_SIGN_OUT = null;
        private static BitmapImage _MENU_CLOSE = null;
        private static BitmapImage _MENU_HELP_DESK = null;
        private static BitmapImage _MENU_CHECK_FOR_UPDATE = null;
        private static BitmapImage _MENU_SETTINGS = null;
        private static BitmapImage _MENU_INFO = null;
        private static BitmapImage _MENU_PRIVACY_POLICY = null;
        private static BitmapImage _MENU_CAONATCT_US_ACTIVE = null;
        private static BitmapImage _MENU_CAONATCT_US_INACTIVE = null;
        private static BitmapImage _MENU_ABOUT_RINGID = null;

        private static BitmapImage _STATUS_ONLINE = null;
        private static BitmapImage _STATUS_OFFLINE = null;
        private static BitmapImage _STATUS_HOVER = null;
        private static BitmapImage _STATUS_DONOT_DISTURB = null;
        private static BitmapImage _ONLINE_DESKTOP = null;
        private static BitmapImage _ONLINE_ANDROID = null;
        private static BitmapImage _ONLINE_IOS = null;
        private static BitmapImage _ONLINE_WEB = null;
        private static BitmapImage _ONLINE_WINDOWS = null;
        private static BitmapImage _AWAY = null;
        private static BitmapImage _OFFLINE = null;
        private static BitmapImage _SETTING_MINI = null;
        private static BitmapImage _SETTING_MINI_H = null;
        private static BitmapImage _STAR = null;
        private static BitmapImage _STAR_H = null;
        private static BitmapImage _CIRCLE_IMAGE_IN_LIST = null;
        private static BitmapImage _RIGHT_ARROW = null;
        private static BitmapImage _RIGHT_ARROW_H = null;
        private static BitmapImage _RIGHT_MINI_ARROW = null;
        private static BitmapImage _RIGHT_MINI_ARROW_H = null;
        private static BitmapImage _LEFT_ARROW = null;
        private static BitmapImage _LEFT_ARROW_H = null;

        private static BitmapImage _DOWN_ARROW = null;
        private static BitmapImage _DOWN_ARROW_H = null;
        private static BitmapImage _TICK_MARK = null;
        private static BitmapImage _LOADING_PROGRESS = null;
        private static BitmapImage _LOADING_RINGID_LOGO = null;
        private static BitmapImage _LOADING_WATCH = null;
        private static BitmapImage _LOADER_FEED = null;
        private static BitmapImage _LOADER_FEED_CYCLE = null;
        private static BitmapImage _LOADING_RINGID_FEED = null;
        //********CALL*******
        //private static BitmapImage _MESSAGE = null;
        //private static BitmapImage _MESSAGE_H = null;
        //private static BitmapImage _IGNORE_CALL = null;
        //private static BitmapImage _IGNORE_CALL_H = null;
        //private static BitmapImage _INCOMING_CALL_ANIMATION = null;
        //private static BitmapImage _OUTGOING_CALL_ANIMATION = null;
        //private static BitmapImage _CALL_ACCEPT = null;
        //private static BitmapImage _CALL_ACCEPT_H = null;
        //private static BitmapImage _CALL_HOLD = null;
        //private static BitmapImage _CALL_HOLD_H = null;
        //private static BitmapImage _CALL_UNHOLD = null;
        //private static BitmapImage _CALL_UNHOLD_H = null;
        //private static BitmapImage _CALL_MUTE = null;
        //private static BitmapImage _CALL_MUTE_H = null;
        //private static BitmapImage _CALL_UMMUTE = null;
        //private static BitmapImage _CALL_UMMUTE_H = null;
        //private static BitmapImage _BUTTON_VOLUME = null;
        //private static BitmapImage _BUTTON_VOLUME_H = null;
        //private static BitmapImage _CALL_END = null;
        //private static BitmapImage _CALL_END_H = null;

        //********CALL*******
        //CHAT

        private static BitmapImage _CHAT_EDITED = null;
        private static BitmapImage _CHAT_EMOTICON = null;
        private static BitmapImage _CHAT_EMOTICON_H = null;
        private static BitmapImage _CHAT_MORE_OPTION_BG = null;
        private static BitmapImage _CHAT_RECENT = null;
        private static BitmapImage _CHAT_RECENT_H = null;
        private static BitmapImage _CHAT_SEND = null;
        private static BitmapImage _CHAT_SEND_H = null;
        private static BitmapImage _MINUS = null;
        private static BitmapImage _MINUS_H = null;
        private static BitmapImage _OFF = null;
        private static BitmapImage _ON = null;
        private static BitmapImage _PLUS = null;
        private static BitmapImage _PLUS_H = null;
        private static BitmapImage _RECORD_PAUSE = null;
        private static BitmapImage _RECORD_PAUSE_H = null;
        private static BitmapImage _RECORD_RESUME = null;
        private static BitmapImage _RECORD_RESUME_H = null;
        private static BitmapImage _RECORD_SEND = null;
        private static BitmapImage _RECORD_SEND_H = null;
        private static BitmapImage _RECORD_START = null;
        private static BitmapImage _RECORD_START_H = null;
        private static BitmapImage _RECORD_STOP = null;
        private static BitmapImage _RECORD_STOP_H = null;
        private static BitmapImage _WATCH = null;
        private static BitmapImage _WATCH_H = null;
        private static BitmapImage _TAKE_PHOTO = null;
        private static BitmapImage _TAKE_PHOTO_H = null;
        private static BitmapImage _TAKE_PHOTO_SELECTED = null;
        private static BitmapImage _UPLOAD_FROM_COMPUTER = null;
        private static BitmapImage _UPLOAD_FROM_COMPUTER_H = null;
        private static BitmapImage _UPLOAD_FROM_COMPUTER_SELECTED = null;
        private static BitmapImage _EDIT_PHOTO = null;
        private static BitmapImage _EDIT_PHOTO_H = null;
        private static BitmapImage _EDIT_PHOTO_SELECTED = null;
        private static BitmapImage _BUTTON_NOTIFICATION_SELECT = null;
        private static BitmapImage _BUTTON_NOTIFICATION_SELECT_H = null;
        private static BitmapImage _BUTTON_NOTIFICATION_SELECTED = null;
        //   private static BitmapImage _POPUP_CHAT_RECENT = null;
        private static BitmapImage _CHAT_DEFAULT_AUDIO = null;
        private static BitmapImage _CHAT_DEFAULT_FILE = null;
        private static BitmapImage _CHAT_DEFAULT_FILE_DOWNLOAD = null;
        private static BitmapImage _CHAT_DEFAULT_FILE_UPLOAD = null;
        private static BitmapImage _CHAT_DEFAULT_VIDEO = null;
        private static BitmapImage _CHAT_DEFAULT_IMAGE = null;
        private static BitmapImage _RECORDER_CLOSE = null;
        private static BitmapImage _RECORDER_CLOSE_H = null;
        private static BitmapImage _INCLUDE_VOICE = null;
        private static BitmapImage _INCLUDE_VOICE_H = null;
        private static BitmapImage _INCLUDE_VOICE_SELECTED = null;
        private static BitmapImage _SEND_CHAT_IMAGE = null;
        private static BitmapImage _SEND_CHAT_IMAGE_H = null;
        private static BitmapImage _SEND_CHAT_IMAGE_SELECTED = null;
        private static BitmapImage _ADD_FROM_WEBCAM = null;
        private static BitmapImage _ADD_FROM_DIRECTORY = null;
        private static BitmapImage _ADD_FROM_WEBCAM_H = null;
        private static BitmapImage _ADD_FROM_DIRECTORY_H = null;
        private static BitmapImage _CHAT_MULTIMEDIA = null;
        private static BitmapImage _CHAT_MULTIMEDIA_H = null;
        private static BitmapImage _EMOTICON_POPUP_DOWN_ARROW = null;
        private static BitmapImage _EMOTICON_POPUP_UP_ARROW = null;
        private static BitmapImage _EMOTICON_POPUP_NO_ARROW = null;
        private static BitmapImage _CHAT_MEDIA_PLAY = null;
        private static BitmapImage _CHAT_MEDIA_PLAY_H = null;
        private static BitmapImage _CHAT_MEDIA_PLAY_DISABLE = null;
        private static BitmapImage _CHAT_MEDIA_PAUSE = null;
        private static BitmapImage _CHAT_MEDIA_PAUSE_H = null;
        private static BitmapImage _CHAT_MEDIA_PAUSE_DISABLE = null;
        private static Cursor _HAND_GRAB = null;
        private static Cursor _HAND_RELEASE = null;
        private static BitmapImage _IMAGE_ORIGINAL_SIZE = null;
        private static BitmapImage _IMAGE_ORIGINAL_SIZE_H = null;
        private static BitmapImage _IMAGE_ZOOM = null;
        private static BitmapImage _IMAGE_ZOOM_H = null;
        private static BitmapImage _RECORD_EXPAND = null;
        private static BitmapImage _RECORD_EXPAND_H = null;
        private static BitmapImage _RECORD_NORMAL = null;
        private static BitmapImage _RECORD_NORMAL_H = null;
        private static BitmapImage _DEFAULT_RING_MEDIA_IMAGE = null;

        private static BitmapImage _UNKNOWN_70 = null;
        private static BitmapImage _UNKNOWN_25 = null;
        private static BitmapImage _GROUP_DELETE = null;
        private static BitmapImage _GROUP_LEAVE = null;
        private static BitmapImage _GROUP_JOIN = null;
        private static BitmapImage _CLOSE = null;
        private static BitmapImage _CLOSE_H = null;
        private static BitmapImage _STICKER_REMOVE = null;
        private static BitmapImage _STICKER_REMOVE_H = null;
        private static BitmapImage _CLOSE_TRANSPARENT = null;
        private static BitmapImage _BUTTON_CLOSE_BOLD = null;
        private static BitmapImage _BUTTON_CLOSE_BOLD_H = null;
        private static BitmapImage _BUTTON_CLOSE_BOLD_WHITE = null;
        private static BitmapImage _BUTTON_CLOSE_SMALL_H = null;
        private static BitmapImage _BUTTON_CLOSE_SMALL_WHITE = null;


        private static BitmapImage _BUTTON_SETTING_MINI = null;
        private static BitmapImage _BUTTON_SETTING_MINI_H = null;
        private static BitmapImage _BUTTON_SETTING_MINI_SELECTED = null;
        private static BitmapImage _BUTTON_ABOUT_SETTING_MINI = null;
        private static BitmapImage _BUTTON_ABOUT_SETTING_MINI_H = null;
        private static BitmapImage _BUTTON_ABOUT_SETTING_MINI_SELECTED = null;
        private static BitmapImage _GROUP_INFO = null;
        private static BitmapImage _BUTTON_CLOSE_MINI = null;
        private static BitmapImage _BUTTON_CLOSE_MINI_H = null;
        private static BitmapImage _BUTTON_OK_MINI = null;
        private static BitmapImage _BUTTON_OK_MINI_H = null;
        private static BitmapImage _BUTTON_UPLOAD_FROM_DIRECTORY = null;
        private static BitmapImage _BUTTON_UPLOAD_FROM_DIRECTORY_H = null;
        private static BitmapImage _BUTTON_CAMERA = null;
        private static BitmapImage _BUTTON_CAMERA_H = null;
        private static BitmapImage _BLOCK_USER = null;
        private static BitmapImage _CHANGE_ACCESS = null;
        private static BitmapImage _INFO_ICON = null;
        private static BitmapImage _WORK_ICON = null;
        private static BitmapImage _EDUCATION_ICON = null;
        private static BitmapImage _SKILL_ICON = null;
        private static BitmapImage _BUTTON_EDIT = null;
        private static BitmapImage _BUTTON_EDIT_H = null;
        private static BitmapImage _BUTTON_MARK_AS_READ = null;
        private static BitmapImage _BUTTON_MARK_AS_READ_H = null;
        private static BitmapImage _ADD_FRIEND_SEARCH_ICON = null;
        private static BitmapImage _ADD_FRIEND_SEARCH_ICON_H = null;
        private static BitmapImage _ADD_FRIEND_SEARCH_ICON_SELECTED = null;
        private static BitmapImage _ADD_FRIEND_ICON = null;
        private static BitmapImage _ADD_FRIEND_ICON_H = null;

        private static BitmapImage _ADD_FRIEND_INVITE_ICON = null;
        private static BitmapImage _ADD_FRIEND_INVITE_ICON_H = null;
        private static BitmapImage _ADD_FRIEND_INVITE_ICON_SELECTED = null;

        private static BitmapImage _ADD_FRIEND_PENDING_ICON = null;
        private static BitmapImage _ADD_FRIEND_ADD_ICON = null;
        private static BitmapImage _ADD_FRIEND_ADD_H_ICON = null;
        private static BitmapImage _ADD_FRIEND_ADD_SELECTED_ICON = null;
        private static BitmapImage _FRIEND_ACCESS_ICON_H = null;

        private static BitmapImage _ADD_FRIEND_PENDING_ICON_H = null;
        private static BitmapImage _ADD_FRIEND_PENDING_ICON_SELECTED = null;
        private static BitmapImage _ADD_FRIEND_BLOCK_LIST = null;
        private static BitmapImage _ADD_FRIEND_BLOCK_LIST_H = null;
        private static BitmapImage _ADD_FRIEND_BLOCK_LIST_SELECTED = null;
        private static BitmapImage _ADD_FRIEND_INCOMING_ICON = null;
        private static BitmapImage _ADD_FRIEND_INCOMING_ICON_H = null;
        private static BitmapImage _ADD_FRIEND_OUTGOING_ICON = null;
        private static BitmapImage _ADD_FRIEND_OUTGOING_ICON_H = null;
        private static BitmapImage _FEED = null;
        private static BitmapImage _FEED_H = null;
        private static BitmapImage _FEED_SELECTED = null;
        private static BitmapImage _NEWS_PORTAL = null;
        private static BitmapImage _NEWS_PORTAL_H = null;
        private static BitmapImage _NEWS_PORTAL_SELECTED = null;
        private static BitmapImage _PAGES = null;
        private static BitmapImage _PAGES_H = null;
        private static BitmapImage _PAGES_SELECTED = null;
        private static BitmapImage _CELEBRITY = null;
        private static BitmapImage _CELEBRITY_H = null;
        private static BitmapImage _CELEBRITY_SELECTED = null;
        private static BitmapImage _SAVED_FEED = null;
        private static BitmapImage _SAVED_FEED_H = null;
        private static BitmapImage _SAVED_FEED_SELECTED = null;
        private static BitmapImage _FOLLOWING_LIST = null;
        private static BitmapImage _FOLLOWING_LIST_H = null;
        private static BitmapImage _FOLLOWING_LIST_SELECTED = null;
        private static BitmapImage _POPUP_SIGN_IN = null;
        private static BitmapImage _POPUP_STATUS = null;
        private static BitmapImage _DEFAULT_ID_ICON = null;
        private static BitmapImage _ADD_FRIEND_ICON_CONTACT_LIST = null;
        private static BitmapImage _FRIEND_ICON_CONTACT_LIST = null;
        private static BitmapImage _PENDING_IN_FRIEND_ICON_CONTACT_LIST = null;
        private static BitmapImage _PENDING_OUT_FRIEND_ICON_CONTACT_LIST = null;
        private static BitmapImage _POPUP_MUTUAL_FRIENDS = null;
        private static BitmapImage _POPUP_ABOUT_RINGID = null;
        private static BitmapImage _ADD_ICON_CONTACT_LIST = null;
        private static BitmapImage _ADD_ICON_CONTACT_LIST_H = null;
        private static BitmapImage _INCOMING_FRIEND_ICON_CONTACT_LIST = null;
        private static BitmapImage _INCOMING_FRIEND_ICON_CONTACT_LIST_H = null;
        private static BitmapImage _OUTGOING_FRIEND_ICON_CONTACT_LIST = null;
        private static BitmapImage _OUTGOING_FRIEND_ICON_CONTACT_LIST_H = null;
        private static BitmapImage _FRIEND_CONTACT_LIST = null;
        private static BitmapImage _FRIEND_CONTACT_LIST_H = null;
        private static BitmapImage _FRIEND_CONTACT_ID = null;
        private static BitmapImage _REJECT_ICON_CONTACT_LIST = null;
        private static BitmapImage _REJECT_ICON_CONTACT_LIST_H = null;
        private static BitmapImage _REMOVE_ICON_PEOPLE_UMAY_KNOW_LIST = null;
        private static BitmapImage _REMOVE_ICON_PEOPLE_UMAY_KNOW_LIST_H = null;
        private static BitmapImage _POPUP_EDIT_DELETE = null;
        private static BitmapImage _POPUP_THREE_ITEMS = null;
        private static BitmapImage _SEQUENCE_NUMBER = null;
        private static BitmapImage _NEW_STATUS_OPTIONS = null;
        private static BitmapImage _NEW_STATUS_OPTIONS_H = null;
        private static BitmapImage _POPUP_DELETE = null;
        private static BitmapImage _POPUP_UPLOAD_PHOTO = null;
        private static BitmapImage _BUTTON_TAKE_PHOTOS = null;
        private static BitmapImage _BUTTON_UPLOAD_PHOTO = null;
        private static BitmapImage _BUTTON_ALBUM_PHOTO = null;
        private static BitmapImage _ADD_GROUP_MEMBER = null;
        private static BitmapImage _ADD_GROUP_MEMBER_H = null;
        private static BitmapImage _GROUP_SETTINGS = null;
        private static BitmapImage _GROUP_SETTINGS_H = null;
        private static BitmapImage _VIEW_PREVIOUS_COMMENT_ICON = null;
        private static BitmapImage _NEW_STATUS_IMAGE_UPLOAD_CROSS = null;
        private static BitmapImage _NEW_STATUS_IMAGE_UPLOAD_CROSS_H = null;
        private static BitmapImage _POPUP_SHARE = null;
        private static BitmapImage _BOOK_ARROW = null;
        private static BitmapImage _NEW_STATUS_EMOTICONS = null;
        private static BitmapImage _NEW_STATUS_EMOTICONS_H = null;
        private static BitmapImage _VIDEO_SETTINGS = null;
        private static BitmapImage _VIDEO_SETTINGS_H = null;

        private static BitmapImage _BUTTON_BACK_IMAGE;
        private static BitmapImage _BUTTON_BACK_IMAGE_H;
        private static BitmapImage _BUTTON_BACK_IMAGE_P;
        private static BitmapImage _BUTTON_NEXT_IMAGE;
        private static BitmapImage _BUTTON_NEXT_IMAGE_H;
        private static BitmapImage _BUTTON_NEXT_IMAGE_P;

        private static BitmapImage _NEW_STATUS_TAG = null;
        private static BitmapImage _NEW_STATUS_TAG_H = null;
        private static BitmapImage _NEW_STATUS_MUSIC = null;
        private static BitmapImage _NEW_STATUS_MUSIC_H = null;
        private static BitmapImage _NEW_STATUS_LOCATION = null;
        private static BitmapImage _NEW_STATUS_LOCATION_H = null;
        private static BitmapImage _POPUP_UPLOAD_VIDEO = null;

        private static BitmapImage _BLOCK_FRIEND_ICON = null;
        private static BitmapImage _BLOCK_FRIEND_ICON_H = null;
        private static BitmapImage _CIRCLE_SMALL_ICON = null;
        private static BitmapImage _CIRCLE_SMALL_ICON_H = null;
        private static BitmapImage _LOCATION_USER = null;
        private static BitmapImage _INFO_BIG = null;
        private static BitmapImage _PHOTOS = null;
        private static BitmapImage _MV = null;
        private static BitmapImage _MEDIA_ALBUM_DEFAULT = null;
        private static BitmapImage _VIDEO_ALBUM_DEFAULT = null;
        private static BitmapImage _LOADER_PLAYER_BUFFER = null;
        private static BitmapImage _LOADER_PLAYER_MUSIC = null;
        private static BitmapImage _LOADER_OFFLINE = null;
        private static BitmapImage _PLAYER_PAUSED_ICON = null;
        private static BitmapImage _DEFAULT_AUDIO_SMALL = null;
        private static BitmapImage _DEFAULT_VIDEO_SMALL = null;
        private static BitmapImage _SPLASH_SCREEN_LOADER = null;

        private static BitmapImage _ADD_MORE = null;
        private static BitmapImage _ADD_MORE_H = null;
        private static BitmapImage _RECORDING = null;
        private static BitmapImage _PLAY_RECORD_VIDEO = null;
        private static BitmapImage _PLAY_RECORD_VIDEO_H = null;
        private static BitmapImage _LINK_VIDEO_ICON = null;
        private static BitmapImage _LINK_VIDEO_ICON_H = null;

        private static Bitmap _LOADER_100_BMP = null;
        private static BitmapImage _LOADER_LIKE_ANIMATION = null;


        private static BitmapImage _ADD_FRIEND_ICON_CONTACT_LIST_MEDIUM = null;
        private static BitmapImage _FRIEND_ICON_CONTACT_LIST_MEDIUM = null;
        private static BitmapImage _INCOMING_FRIEND_ICON_CONTACT_LIST_MEDIUM = null;
        private static BitmapImage _OUTGOING_FRIEND_ICON_CONTACT_LIST_MEDIUM = null;
        private static BitmapImage _CANCEL_REQUEST_ICON_CONTACT_LIST = null;
        private static BitmapImage _CANCEL_REQUEST_ICON_CONTACT_LIST_H = null;
        private static BitmapImage _UP_ARROW = null;
        private static BitmapImage _UP_ARROW_H = null;

        private static BitmapImage _UPDATE_VERSION_ICON = null;
        private static BitmapImage _PHOTO_LOADING = null;

        private static BitmapImage _UPLOAD = null;
        private static BitmapImage _UPLOAD_H = null;
        private static BitmapImage _UPLOAD_POPUP_UP_ARROW = null;
        private static BitmapImage _DOWNLOAD = null;

        private static BitmapImage _HDOT = null;
        private static BitmapImage _VDOT = null;
        private static BitmapImage _HDOT_H = null;
        private static BitmapImage _VDOT_H = null;
        private static BitmapImage _HDOT_SELECTED = null;
        private static BitmapImage _VDOT_SELECTED = null;

        private static BitmapImage _RECENT_LIVE = null;
        private static Cursor _STREAM_CURSOR = null;
        private static BitmapImage _DEFAULT_APP_ICON = null;
        private static BitmapImage _CHANNEL_PROFILE_UNKNOWN = null;

        #region CONTEXT MENU
        private static BitmapImage _ADD_FRIEND_ICON_CM = null;
        private static BitmapImage _ADD_FRIEND_ICON_CM_H = null;
        private static BitmapImage _BLOCK_USER_ICON_CM = null;
        private static BitmapImage _BLOCK_USER_ICON_CM_H = null;
        private static BitmapImage _CANCEL_REQUEST_ICON_CM = null;
        private static BitmapImage _CANCEL_REQUEST_ICON_CM_H = null;
        private static BitmapImage _CIRCLE_SMALL_ICON_CM = null;
        private static BitmapImage _CIRCLE_SMALL_ICON_CM_H = null;
        private static BitmapImage _FRIEND_ICON_CM = null;
        private static BitmapImage _FRIEND_ICON_CM_H = null;
        private static BitmapImage _REJECT_ICON_CM = null;
        private static BitmapImage _REJECT_ICON_CM_H = null;
        private static BitmapImage _GROUP_DELETE_CM = null;
        private static BitmapImage _GROUP_DELETE_CM_H = null;
        private static BitmapImage _EDIT_INFO_CM = null;
        private static BitmapImage _EDIT_INFO_CM_H = null;
        private static BitmapImage _GROUP_LEAVE_CM = null;
        private static BitmapImage _GROUP_LEAVE_CM_H = null;
        #endregion

        //public static BitmapImage OFFLINE_OVERLAY
        //{
        //    get
        //    {
        //        if (ImageObjects._OFFLINE_OVERLAY == null)
        //        {
        //            ImageObjects._OFFLINE_OVERLAY = ImageUtility.GetBitmapImage(ImageLocation.OFFLINE_TASKBAR_ICON);
        //        }
        //        return ImageObjects._OFFLINE_OVERLAY;
        //    }
        //}

        public static BitmapImage HDOT
        {
            get
            {
                if (ImageObjects._HDOT == null)
                {
                    ImageObjects._HDOT = ImageUtility.GetBitmapImage(ImageLocation.HDOT);
                }
                return ImageObjects._HDOT;
            }
        }

        public static BitmapImage VDOT
        {
            get
            {
                if (ImageObjects._VDOT == null)
                {
                    ImageObjects._VDOT = ImageUtility.GetBitmapImage(ImageLocation.VDOT);
                }
                return ImageObjects._VDOT;
            }
        }

        public static BitmapImage HDOT_H
        {
            get
            {
                if (ImageObjects._HDOT_H == null)
                {
                    ImageObjects._HDOT_H = ImageUtility.GetBitmapImage(ImageLocation.HDOT_H);
                }
                return ImageObjects._HDOT_H;
            }
        }

        public static BitmapImage VDOT_H
        {
            get
            {
                if (ImageObjects._VDOT_H == null)
                {
                    ImageObjects._VDOT_H = ImageUtility.GetBitmapImage(ImageLocation.VDOT_H);
                }
                return ImageObjects._VDOT_H;
            }
        }

        public static BitmapImage HDOT_SELECTED
        {
            get
            {
                if (ImageObjects._HDOT_SELECTED == null)
                {
                    ImageObjects._HDOT_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.HDOT_SELECTED);
                }
                return ImageObjects._HDOT_SELECTED;
            }
        }

        public static BitmapImage VDOT_SELECTED
        {
            get
            {
                if (ImageObjects._VDOT_SELECTED == null)
                {
                    ImageObjects._VDOT_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.VDOT_SELECTED);
                }
                return ImageObjects._VDOT_SELECTED;
            }
        }

        public static BitmapImage RECORD_START
        {
            get
            {
                if (ImageObjects._RECORD_START == null)
                {
                    ImageObjects._RECORD_START = ImageUtility.GetBitmapImage(ImageLocation.RECORD_START);
                }
                return ImageObjects._RECORD_START;
            }
        }

        public static BitmapImage RECORD_START_H
        {
            get
            {
                if (ImageObjects._RECORD_START_H == null)
                {
                    ImageObjects._RECORD_START_H = ImageUtility.GetBitmapImage(ImageLocation.RECORD_START_H);
                }
                return ImageObjects._RECORD_START_H;
            }
        }

        public static BitmapImage RECORD_STOP
        {
            get
            {
                if (ImageObjects._RECORD_STOP == null)
                {
                    ImageObjects._RECORD_STOP = ImageUtility.GetBitmapImage(ImageLocation.RECORD_STOP);
                }
                return ImageObjects._RECORD_STOP;
            }
        }

        public static BitmapImage RECORD_STOP_H
        {
            get
            {
                if (ImageObjects._RECORD_STOP_H == null)
                {
                    ImageObjects._RECORD_STOP_H = ImageUtility.GetBitmapImage(ImageLocation.RECORD_STOP_H);
                }
                return ImageObjects._RECORD_STOP_H;
            }
        }

        public static BitmapImage PLUS
        {
            get
            {
                if (ImageObjects._PLUS == null)
                {
                    ImageObjects._PLUS = ImageUtility.GetBitmapImage(ImageLocation.PLUS);
                }
                return ImageObjects._PLUS;
            }
        }

        public static BitmapImage PLUS_H
        {
            get
            {
                if (ImageObjects._PLUS_H == null)
                {
                    ImageObjects._PLUS_H = ImageUtility.GetBitmapImage(ImageLocation.PLUS_H);
                }
                return ImageObjects._PLUS_H;
            }
        }

        public static BitmapImage RECORD_PAUSE
        {
            get
            {
                if (ImageObjects._RECORD_PAUSE == null)
                {
                    ImageObjects._RECORD_PAUSE = ImageUtility.GetBitmapImage(ImageLocation.RECORD_PAUSE);
                }
                return ImageObjects._RECORD_PAUSE;
            }
        }

        public static BitmapImage RECORD_PAUSE_H
        {
            get
            {
                if (ImageObjects._RECORD_PAUSE_H == null)
                {
                    ImageObjects._RECORD_PAUSE_H = ImageUtility.GetBitmapImage(ImageLocation.RECORD_PAUSE_H);
                }
                return ImageObjects._RECORD_PAUSE_H;
            }
        }

        public static BitmapImage RECORD_RESUME
        {
            get
            {
                if (ImageObjects._RECORD_RESUME == null)
                {
                    ImageObjects._RECORD_RESUME = ImageUtility.GetBitmapImage(ImageLocation.RECORD_RESUME);
                }
                return ImageObjects._RECORD_RESUME;
            }
        }

        public static BitmapImage RECORD_RESUME_H
        {
            get
            {
                if (ImageObjects._RECORD_RESUME_H == null)
                {
                    ImageObjects._RECORD_RESUME_H = ImageUtility.GetBitmapImage(ImageLocation.RECORD_RESUME_H);
                }
                return ImageObjects._RECORD_RESUME_H;
            }
        }

        public static BitmapImage RECORD_SEND
        {
            get
            {
                if (ImageObjects._RECORD_SEND == null)
                {
                    ImageObjects._RECORD_SEND = ImageUtility.GetBitmapImage(ImageLocation.RECORD_SEND);
                }
                return ImageObjects._RECORD_SEND;
            }
        }

        public static BitmapImage RECORD_SEND_H
        {
            get
            {
                if (ImageObjects._RECORD_SEND_H == null)
                {
                    ImageObjects._RECORD_SEND_H = ImageUtility.GetBitmapImage(ImageLocation.RECORD_SEND_H);
                }
                return ImageObjects._RECORD_SEND_H;
            }
        }

        public static BitmapImage CHAT_EDITED
        {
            get
            {
                if (ImageObjects._CHAT_EDITED == null)
                {
                    ImageObjects._CHAT_EDITED = ImageUtility.GetBitmapImage(ImageLocation.CHAT_EDITED);
                }
                return ImageObjects._CHAT_EDITED;
            }
        }

        public static BitmapImage CHAT_EMOTICON
        {
            get
            {
                if (ImageObjects._CHAT_EMOTICON == null)
                {
                    ImageObjects._CHAT_EMOTICON = ImageUtility.GetBitmapImage(ImageLocation.CHAT_EMOTICON);
                }
                return ImageObjects._CHAT_EMOTICON;
            }
        }

        public static BitmapImage CHAT_EMOTICON_H
        {
            get
            {
                if (ImageObjects._CHAT_EMOTICON_H == null)
                {
                    ImageObjects._CHAT_EMOTICON_H = ImageUtility.GetBitmapImage(ImageLocation.CHAT_EMOTICON_H);
                }
                return ImageObjects._CHAT_EMOTICON_H;
            }
        }

        public static BitmapImage CHAT_RECENT
        {
            get
            {
                if (ImageObjects._CHAT_RECENT == null)
                {
                    ImageObjects._CHAT_RECENT = ImageUtility.GetBitmapImage(ImageLocation.CHAT_RECENT);
                }
                return ImageObjects._CHAT_RECENT;
            }
        }

        public static BitmapImage CHAT_RECENT_H
        {
            get
            {
                if (ImageObjects._CHAT_RECENT_H == null)
                {
                    ImageObjects._CHAT_RECENT_H = ImageUtility.GetBitmapImage(ImageLocation.CHAT_RECENT_H);
                }
                return ImageObjects._CHAT_RECENT_H;
            }
        }

        public static BitmapImage CHAT_SEND
        {
            get
            {
                if (ImageObjects._CHAT_SEND == null)
                {
                    ImageObjects._CHAT_SEND = ImageUtility.GetBitmapImage(ImageLocation.CHAT_SEND);
                }
                return ImageObjects._CHAT_SEND;
            }
        }

        public static BitmapImage CHAT_SEND_H
        {
            get
            {
                if (ImageObjects._CHAT_SEND_H == null)
                {
                    ImageObjects._CHAT_SEND_H = ImageUtility.GetBitmapImage(ImageLocation.CHAT_SEND_H);
                }
                return ImageObjects._CHAT_SEND_H;
            }
        }

        public static BitmapImage GROUP_DELETE
        {
            get
            {
                if (ImageObjects._GROUP_DELETE == null)
                {
                    ImageObjects._GROUP_DELETE = ImageUtility.GetBitmapImage(ImageLocation.GROUP_DELETE);
                }
                return ImageObjects._GROUP_DELETE;
            }
        }

        public static BitmapImage GROUP_LEAVE
        {
            get
            {
                if (ImageObjects._GROUP_LEAVE == null)
                {
                    ImageObjects._GROUP_LEAVE = ImageUtility.GetBitmapImage(ImageLocation.GROUP_LEAVE);
                }
                return ImageObjects._GROUP_LEAVE;
            }
        }

        public static BitmapImage GROUP_JOIN
        {
            get
            {
                if (ImageObjects._GROUP_JOIN == null)
                {
                    ImageObjects._GROUP_JOIN = ImageUtility.GetBitmapImage(ImageLocation.GROUP_JOIN);
                }
                return ImageObjects._GROUP_JOIN;
            }
        }

        public static BitmapImage MINUS
        {
            get
            {
                if (ImageObjects._MINUS == null)
                {
                    ImageObjects._MINUS = ImageUtility.GetBitmapImage(ImageLocation.MINUS);
                }
                return ImageObjects._MINUS;
            }

        }

        public static BitmapImage MINUS_H
        {
            get
            {
                if (ImageObjects._MINUS_H == null)
                {
                    ImageObjects._MINUS_H = ImageUtility.GetBitmapImage(ImageLocation.MINUS_H);
                }
                return ImageObjects._MINUS_H;
            }
        }

        public static BitmapImage OFF
        {
            get
            {
                if (ImageObjects._OFF == null)
                {
                    ImageObjects._OFF = ImageUtility.GetBitmapImage(ImageLocation.OFF);
                }
                return ImageObjects._OFF;
            }
        }

        public static BitmapImage ON
        {
            get
            {
                if (ImageObjects._ON == null)
                {
                    ImageObjects._ON = ImageUtility.GetBitmapImage(ImageLocation.ON);
                }
                return ImageObjects._ON;
            }
        }

        public static BitmapFrame APP_ICON
        {
            get
            {
                if (ImageObjects._APP_ICON == null)
                {
                    ImageObjects._APP_ICON = ImageUtility.GetBitmapFrame(ImageLocation.APP_ICON);
                }
                return ImageObjects._APP_ICON;
            }
        }

        public static BitmapImage DEFAULT_MY_PROFILE_BG_IMAGE
        {
            get
            {
                if (ImageObjects._DEFAULT_MY_PROFILE_BG_IMAGE == null)
                {
                    ImageObjects._DEFAULT_MY_PROFILE_BG_IMAGE = ImageUtility.GetBitmapImage(ImageLocation.DEFAULT_MY_PROFILE_BG_IMAGE);
                }
                return ImageObjects._DEFAULT_MY_PROFILE_BG_IMAGE;
            }
        }
        public static BitmapImage MY_PROFILE_BG_IMAGE_DEFAULT
        {
            get
            {
                if (ImageObjects._MY_PROFILE_BG_IMAGE_DEFAULT == null)
                {
                    ImageObjects._MY_PROFILE_BG_IMAGE_DEFAULT = ImageUtility.GetBitmapImage(ImageLocation.MY_PROFILE_BG_IMAGE_DEFAULT);
                }
                return ImageObjects._MY_PROFILE_BG_IMAGE_DEFAULT;
            }
        }
        public static BitmapImage MY_PROFILE_BG_IMAGE_PRESSED
        {
            get
            {
                if (ImageObjects._MY_PROFILE_BG_IMAGE_PRESSED == null)
                {
                    ImageObjects._MY_PROFILE_BG_IMAGE_PRESSED = ImageUtility.GetBitmapImage(ImageLocation.MY_PROFILE_BG_IMAGE_PRESSED);
                }
                return ImageObjects._MY_PROFILE_BG_IMAGE_PRESSED;
            }
        }

        public static BitmapImage LOGIN_BG_IMAGE
        {
            get
            {
                if (ImageObjects._LOGIN_BG == null)
                {
                    ImageObjects._LOGIN_BG = ImageUtility.GetBitmapImage(ImageLocation.LOGIN_BG);
                }
                return ImageObjects._LOGIN_BG;
            }
        }

        public static BitmapImage WELCOME_BG_IMAGE
        {
            get
            {
                if (ImageObjects._WELCOME_BG_IMAGE == null)
                {
                    ImageObjects._WELCOME_BG_IMAGE = ImageUtility.GetBitmapImage(ImageLocation.WELCOME_BG_IMAGE);
                }
                return ImageObjects._WELCOME_BG_IMAGE;
            }
        }

        public static BitmapImage WELCOME_LOGO_IMAGE
        {
            get
            {
                if (ImageObjects._WELCOME_LOGO_IMAGE == null)
                {
                    ImageObjects._WELCOME_LOGO_IMAGE = ImageUtility.GetBitmapImage(ImageLocation.WELCOME_LOGO_IMAGE);
                }
                return ImageObjects._WELCOME_LOGO_IMAGE;
            }
        }

        public static BitmapImage DEFAULT_MY_PROFILE_BG_IMAGE_H
        {
            get
            {
                if (ImageObjects._DEFAULT_MY_PROFILE_BG_IMAGE_H == null)
                {
                    ImageObjects._DEFAULT_MY_PROFILE_BG_IMAGE_H = ImageUtility.GetBitmapImage(ImageLocation.DEFAULT_MY_PROFILE_BG_IMAGE_H);
                }
                return ImageObjects._DEFAULT_MY_PROFILE_BG_IMAGE_H;
            }
        }

        public static BitmapImage GROUP_TOP
        {
            get
            {
                if (ImageObjects._GROUP_TOP == null)
                {
                    ImageObjects._GROUP_TOP = ImageUtility.GetBitmapImage(ImageLocation.GROUP_TOP);
                }
                return ImageObjects._GROUP_TOP;
            }
        }

        public static BitmapImage GROUP_TOP_H
        {
            get
            {
                if (ImageObjects._GROUP_TOP_H == null)
                {
                    ImageObjects._GROUP_TOP_H = ImageUtility.GetBitmapImage(ImageLocation.GROUP_TOP_H);
                }
                return ImageObjects._GROUP_TOP_H;
            }
        }

        public static BitmapImage GROUP_TOP_SELECTED
        {
            get
            {
                if (ImageObjects._GROUP_TOP_SELECTED == null)
                {
                    ImageObjects._GROUP_TOP_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.GROUP_TOP_SELECTED);
                }
                return ImageObjects._GROUP_TOP_SELECTED;
            }
        }
        //public static BitmapImage CIRCLE_TOP
        //{
        //    get
        //    {
        //        if (ImageObjects._CIRCLE_TOP == null)
        //        {
        //            ImageObjects._CIRCLE_TOP = ImageUtility.GetBitmapImage(ImageLocation.CIRCLE_TOP);
        //        }
        //        return ImageObjects._CIRCLE_TOP;
        //    }
        //}

        //public static BitmapImage CIRCLE_TOP_H
        //{
        //    get
        //    {
        //        if (ImageObjects._CIRCLE_TOP_H == null)
        //        {
        //            ImageObjects._CIRCLE_TOP_H = ImageUtility.GetBitmapImage(ImageLocation.CIRCLE_TOP_H);
        //        }
        //        return ImageObjects._CIRCLE_TOP_H;
        //    }
        //}

        //public static BitmapImage CIRCLE_TOP_SELECTED
        //{
        //    get
        //    {
        //        if (ImageObjects._CIRCLE_TOP_SELECTED == null)
        //        {
        //            ImageObjects._CIRCLE_TOP_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.CIRCLE_TOP_SELECTED);
        //        }
        //        return ImageObjects._CIRCLE_TOP_SELECTED;
        //    }
        //}

        public static BitmapImage DIALPAD_TOP
        {
            get
            {
                if (ImageObjects._DIALPAD_TOP == null)
                {
                    ImageObjects._DIALPAD_TOP = ImageUtility.GetBitmapImage(ImageLocation.DIALPAD);
                }
                return ImageObjects._DIALPAD_TOP;
            }
        }

        public static BitmapImage DIALPAD_TOP_H
        {
            get
            {
                if (ImageObjects._DIALPAD_TOP_H == null)
                {
                    ImageObjects._DIALPAD_TOP_H = ImageUtility.GetBitmapImage(ImageLocation.DIALPAD_H);
                }
                return ImageObjects._DIALPAD_TOP_H;
            }
        }

        public static BitmapImage DIALPAD_TOP_SELECTED
        {
            get
            {
                if (ImageObjects._DIALPAD_TOP_SELECTED == null)
                {
                    ImageObjects._DIALPAD_TOP_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.DIALPAD_SELECTED);
                }
                return ImageObjects._DIALPAD_TOP_SELECTED;
            }
        }

        public static BitmapImage FEED
        {
            get
            {
                if (ImageObjects._FEED == null)
                {
                    ImageObjects._FEED = ImageUtility.GetBitmapImage(ImageLocation.FEED);
                }
                return ImageObjects._FEED;
            }
        }

        public static BitmapImage FEED_H
        {
            get
            {
                if (ImageObjects._FEED_H == null)
                {
                    ImageObjects._FEED_H = ImageUtility.GetBitmapImage(ImageLocation.FEED_H);
                }
                return ImageObjects._FEED_H;
            }
        }

        public static BitmapImage FEED_SELECTED
        {
            get
            {
                if (ImageObjects._FEED_SELECTED == null)
                {
                    ImageObjects._FEED_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.FEED_SELECTED);
                }
                return ImageObjects._FEED_SELECTED;
            }
        }

        public static BitmapImage NEWS_PORTAL
        {
            get
            {
                if (ImageObjects._NEWS_PORTAL == null)
                {
                    ImageObjects._NEWS_PORTAL = ImageUtility.GetBitmapImage(ImageLocation.NEWS_PORTAL);
                }
                return ImageObjects._NEWS_PORTAL;
            }
        }

        public static BitmapImage NEWS_PORTAL_H
        {
            get
            {
                if (ImageObjects._NEWS_PORTAL_H == null)
                {
                    ImageObjects._NEWS_PORTAL_H = ImageUtility.GetBitmapImage(ImageLocation.NEWS_PORTAL_H);
                }
                return ImageObjects._NEWS_PORTAL_H;
            }
        }

        public static BitmapImage NEWS_PORTAL_SELECTED
        {
            get
            {
                if (ImageObjects._NEWS_PORTAL_SELECTED == null)
                {
                    ImageObjects._NEWS_PORTAL_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.NEWS_PORTAL_SELECTED);
                }
                return ImageObjects._NEWS_PORTAL_SELECTED;
            }
        }

        public static BitmapImage PAGES
        {
            get
            {
                if (ImageObjects._PAGES == null)
                {
                    ImageObjects._PAGES = ImageUtility.GetBitmapImage(ImageLocation.PAGES);
                }
                return ImageObjects._PAGES;
            }
        }

        public static BitmapImage PAGES_H
        {
            get
            {
                if (ImageObjects._PAGES_H == null)
                {
                    ImageObjects._PAGES_H = ImageUtility.GetBitmapImage(ImageLocation.PAGES_H);
                }
                return ImageObjects._PAGES_H;
            }
        }

        public static BitmapImage PAGES_SELECTED
        {
            get
            {
                if (ImageObjects._PAGES_SELECTED == null)
                {
                    ImageObjects._PAGES_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.PAGES_SELECTED);
                }
                return ImageObjects._PAGES_SELECTED;
            }
        }

        public static BitmapImage CELEBRITY
        {
            get
            {
                if (ImageObjects._CELEBRITY == null)
                {
                    ImageObjects._CELEBRITY = ImageUtility.GetBitmapImage(ImageLocation.CELEBRITY);
                }
                return ImageObjects._CELEBRITY;
            }
        }

        public static BitmapImage CELEBRITY_H
        {
            get
            {
                if (ImageObjects._CELEBRITY_H == null)
                {
                    ImageObjects._CELEBRITY_H = ImageUtility.GetBitmapImage(ImageLocation.CELEBRITY_H);
                }
                return ImageObjects._CELEBRITY_H;
            }
        }

        public static BitmapImage CELEBRITY_SELECTED
        {
            get
            {
                if (ImageObjects._CELEBRITY_SELECTED == null)
                {
                    ImageObjects._CELEBRITY_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.CELEBRITY_SELECTED);
                }
                return ImageObjects._CELEBRITY_SELECTED;
            }
        }

        public static BitmapImage SAVED_FEED
        {
            get
            {
                if (ImageObjects._SAVED_FEED == null)
                {
                    ImageObjects._SAVED_FEED = ImageUtility.GetBitmapImage(ImageLocation.SAVED_FEED);
                }
                return ImageObjects._SAVED_FEED;
            }
        }

        public static BitmapImage SAVED_FEED_H
        {
            get
            {
                if (ImageObjects._SAVED_FEED_H == null)
                {
                    ImageObjects._SAVED_FEED_H = ImageUtility.GetBitmapImage(ImageLocation.SAVED_FEED_H);
                }
                return ImageObjects._SAVED_FEED_H;
            }
        }

        public static BitmapImage SAVED_FEED_SELECTED
        {
            get
            {
                if (ImageObjects._SAVED_FEED_SELECTED == null)
                {
                    ImageObjects._SAVED_FEED_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.SAVED_FEED_SELECTED);
                }
                return ImageObjects._SAVED_FEED_SELECTED;
            }
        }

        public static BitmapImage FOLLOWING_LIST
        {
            get
            {
                if (ImageObjects._FOLLOWING_LIST == null)
                {
                    ImageObjects._FOLLOWING_LIST = ImageUtility.GetBitmapImage(ImageLocation.FOLLOWING_LIST);
                }
                return ImageObjects._FOLLOWING_LIST;
            }
        }

        public static BitmapImage FOLLOWING_LIST_H
        {
            get
            {
                if (ImageObjects._FOLLOWING_LIST_H == null)
                {
                    ImageObjects._FOLLOWING_LIST_H = ImageUtility.GetBitmapImage(ImageLocation.FOLLOWING_LIST_H);
                }
                return ImageObjects._FOLLOWING_LIST_H;
            }
        }

        public static BitmapImage FOLLOWING_LIST_SELECTED
        {
            get
            {
                if (ImageObjects._FOLLOWING_LIST_SELECTED == null)
                {
                    ImageObjects._FOLLOWING_LIST_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.FOLLOWING_LIST_SELECTED);
                }
                return ImageObjects._FOLLOWING_LIST_SELECTED;
            }
        }

        public static BitmapImage CALL_BOTTOM
        {
            get
            {
                if (ImageObjects._CALL_BOTTOM == null)
                {
                    ImageObjects._CALL_BOTTOM = ImageUtility.GetBitmapImage(ImageLocation.CALL_BOTTOM);
                }
                return ImageObjects._CALL_BOTTOM;
            }
        }

        //public static BitmapImage CALL_BOTTOM_SELECTED
        //{
        //    get
        //    {
        //        if (ImageObjects._CALL_BOTTOM_SELECTED == null)
        //        {
        //            ImageObjects._CALL_BOTTOM_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.CALL_BOTTOM_SELECTED);
        //        }
        //        return ImageObjects._CALL_BOTTOM_SELECTED;
        //    }
        //}

        public static BitmapImage ALL_NOTIFICATION
        {
            get
            {
                if (ImageObjects._ALL_NOTIFICATION == null)
                {
                    ImageObjects._ALL_NOTIFICATION = ImageUtility.GetBitmapImage(ImageLocation.ALL_NOTIFICATION);
                }
                return ImageObjects._ALL_NOTIFICATION;
            }
        }

        public static BitmapImage ALL_NOTIFICATION_H
        {
            get
            {
                if (ImageObjects._ALL_NOTIFICATION_H == null)
                {
                    ImageObjects._ALL_NOTIFICATION_H = ImageUtility.GetBitmapImage(ImageLocation.ALL_NOTIFICATION_H);
                }
                return ImageObjects._ALL_NOTIFICATION_H;
            }
        }

        public static BitmapImage ALL_NOTIFICATION_SELECTED
        {
            get
            {
                if (ImageObjects._ALL_NOTIFICATION_SELECTED == null)
                {
                    ImageObjects._ALL_NOTIFICATION_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.ALL_NOTIFICATION_SELECTED);
                }
                return ImageObjects._ALL_NOTIFICATION_SELECTED;
            }
        }

        public static BitmapImage BUTTON_CLOSE_BOLD
        {
            get
            {
                if (ImageObjects._BUTTON_CLOSE_BOLD == null)
                {
                    ImageObjects._BUTTON_CLOSE_BOLD = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_CLOSE_BOLD);
                }
                return ImageObjects._BUTTON_CLOSE_BOLD;
            }
        }

        public static BitmapImage BUTTON_CLOSE_BOLD_H
        {
            get
            {
                if (ImageObjects._BUTTON_CLOSE_BOLD_H == null)
                {
                    ImageObjects._BUTTON_CLOSE_BOLD_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_CLOSE_BOLD_H);
                }
                return ImageObjects._BUTTON_CLOSE_BOLD_H;
            }
        }

        public static BitmapImage BUTTON_CLOSE_BOLD_WHITE
        {
            get
            {
                if (ImageObjects._BUTTON_CLOSE_BOLD_WHITE == null)
                {
                    ImageObjects._BUTTON_CLOSE_BOLD_WHITE = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_CLOSE_BOLD_WHITE);
                }
                return ImageObjects._BUTTON_CLOSE_BOLD_WHITE;
            }
        }


        public static BitmapImage BUTTON_MARK_AS_READ
        {
            get
            {
                if (ImageObjects._BUTTON_MARK_AS_READ == null)
                {
                    ImageObjects._BUTTON_MARK_AS_READ = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_MARK_AS_READ);
                }
                return ImageObjects._BUTTON_MARK_AS_READ;
            }
        }

        public static BitmapImage BUTTON_MARK_AS_READ_H
        {
            get
            {
                if (ImageObjects._BUTTON_MARK_AS_READ_H == null)
                {
                    ImageObjects._BUTTON_MARK_AS_READ_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_MARK_AS_READ_H);
                }
                return ImageObjects._BUTTON_MARK_AS_READ_H;
            }
        }

        public static BitmapImage BUTTON_SETTING_MINI
        {
            get
            {
                if (ImageObjects._BUTTON_SETTING_MINI == null)
                {
                    ImageObjects._BUTTON_SETTING_MINI = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_SETTING_MINI);
                }
                return ImageObjects._BUTTON_SETTING_MINI;
            }
        }

        public static BitmapImage BUTTON_SETTING_MINI_H
        {
            get
            {
                if (ImageObjects._BUTTON_SETTING_MINI_H == null)
                {
                    ImageObjects._BUTTON_SETTING_MINI_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_SETTING_MINI_H);
                }
                return ImageObjects._BUTTON_SETTING_MINI_H;
            }
        }

        public static BitmapImage BUTTON_SETTING_MINI_SELECTED
        {
            get
            {
                if (ImageObjects._BUTTON_SETTING_MINI_SELECTED == null)
                {
                    ImageObjects._BUTTON_SETTING_MINI_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_SETTING_MINI_SELECTED);
                }
                return ImageObjects._BUTTON_SETTING_MINI_SELECTED;
            }
        }

        public static BitmapImage BUTTON_ABOUT_SETTING_MINI
        {
            get
            {
                if (ImageObjects._BUTTON_ABOUT_SETTING_MINI == null)
                {
                    ImageObjects._BUTTON_ABOUT_SETTING_MINI = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_ABOUT_SETTING_MINI);
                }
                return ImageObjects._BUTTON_ABOUT_SETTING_MINI;
            }
        }

        public static BitmapImage BUTTON_ABOUT_SETTING_MINI_H
        {
            get
            {
                if (ImageObjects._BUTTON_ABOUT_SETTING_MINI_H == null)
                {
                    ImageObjects._BUTTON_ABOUT_SETTING_MINI_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_ABOUT_SETTING_MINI_H);
                }
                return ImageObjects._BUTTON_ABOUT_SETTING_MINI_H;
            }
        }

        public static BitmapImage BUTTON_ABOUT_SETTING_MINI_SELECTED
        {
            get
            {
                if (ImageObjects._BUTTON_ABOUT_SETTING_MINI_SELECTED == null)
                {
                    ImageObjects._BUTTON_ABOUT_SETTING_MINI_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_ABOUT_SETTING_MINI_SELECTED);
                }
                return ImageObjects._BUTTON_ABOUT_SETTING_MINI_SELECTED;
            }
        }

        public static BitmapImage BLOCK_USER
        {
            get
            {
                if (ImageObjects._BLOCK_USER == null)
                {
                    ImageObjects._BLOCK_USER = ImageUtility.GetBitmapImage(ImageLocation.BLOCK_USER);
                }
                return ImageObjects._BLOCK_USER;
            }
        }

        public static BitmapImage CHANGE_ACCESS
        {
            get
            {
                if (ImageObjects._CHANGE_ACCESS == null)
                {
                    ImageObjects._CHANGE_ACCESS = ImageUtility.GetBitmapImage(ImageLocation.CHANGE_ACCESS);
                }
                return ImageObjects._CHANGE_ACCESS;
            }
        }

        public static BitmapImage GROUP_INFO
        {
            get
            {
                if (ImageObjects._GROUP_INFO == null)
                {
                    ImageObjects._GROUP_INFO = ImageUtility.GetBitmapImage(ImageLocation.GROUP_INFO);
                }
                return ImageObjects._GROUP_INFO;
            }
        }
        public static BitmapImage INFO_ICON
        {
            get
            {
                if (ImageObjects._INFO_ICON == null)
                {
                    ImageObjects._INFO_ICON = ImageUtility.GetBitmapImage(ImageLocation.INFO_ICON);
                }
                return ImageObjects._INFO_ICON;
            }
        }
        public static BitmapImage WORK_ICON
        {
            get
            {
                if (ImageObjects._WORK_ICON == null)
                {
                    ImageObjects._WORK_ICON = ImageUtility.GetBitmapImage(ImageLocation.WORK_ICON);
                }
                return ImageObjects._WORK_ICON;
            }
        }

        public static BitmapImage EDUCATION_ICON
        {
            get
            {
                if (ImageObjects._EDUCATION_ICON == null)
                {
                    ImageObjects._EDUCATION_ICON = ImageUtility.GetBitmapImage(ImageLocation.EDUCATION_ICON);
                }
                return ImageObjects._EDUCATION_ICON;
            }
        }

        public static BitmapImage BUTTON_EDIT
        {
            get
            {
                if (ImageObjects._BUTTON_EDIT == null)
                {
                    ImageObjects._BUTTON_EDIT = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_EDIT);
                }
                return ImageObjects._BUTTON_EDIT;
            }
        }

        public static BitmapImage BUTTON_EDIT_H
        {
            get
            {
                if (ImageObjects._BUTTON_EDIT_H == null)
                {
                    ImageObjects._BUTTON_EDIT_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_EDIT_H);
                }
                return ImageObjects._BUTTON_EDIT_H;
            }
        }

        public static BitmapImage SKILL_ICON
        {
            get
            {
                if (ImageObjects._SKILL_ICON == null)
                {
                    ImageObjects._SKILL_ICON = ImageUtility.GetBitmapImage(ImageLocation.SKILL_ICON);
                }
                return ImageObjects._SKILL_ICON;
            }
        }

        public static BitmapImage FRIEND_ICON
        {
            get
            {
                if (ImageObjects._FRIEND_ICON == null)
                {
                    ImageObjects._FRIEND_ICON = ImageUtility.GetBitmapImage(ImageLocation.FRIEND_ICON);
                }
                return ImageObjects._FRIEND_ICON;
            }
        }

        public static BitmapImage FRIEND_ICON_H
        {
            get
            {
                if (ImageObjects._FRIEND_ICON_H == null)
                {
                    ImageObjects._FRIEND_ICON_H = ImageUtility.GetBitmapImage(ImageLocation.FRIEND_ICON_H);
                }
                return ImageObjects._FRIEND_ICON_H;
            }
        }

        public static BitmapImage FRIEND_ICON_SELECTED
        {
            get
            {
                if (ImageObjects._FRIEND_ICON_SELECTED == null)
                {
                    ImageObjects._FRIEND_ICON_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.FRIEND_ICON_SELECTED);
                }
                return ImageObjects._FRIEND_ICON_SELECTED;
            }
        }

        public static BitmapImage MUSIC_VIDEO
        {
            get
            {
                if (ImageObjects._MUSIC_VIDEO == null)
                {
                    ImageObjects._MUSIC_VIDEO = ImageUtility.GetBitmapImage(ImageLocation.MEDIA_CLOUD);
                }
                return ImageObjects._MUSIC_VIDEO;
            }
        }
        public static BitmapImage MUSIC_VIDEO_H
        {
            get
            {
                if (ImageObjects._MUSIC_VIDEO_H == null)
                {
                    ImageObjects._MUSIC_VIDEO_H = ImageUtility.GetBitmapImage(ImageLocation.MEDIA_CLOUD_H);
                }
                return ImageObjects._MUSIC_VIDEO_H;
            }
        }
        public static BitmapImage MUSIC_VIDEO_SELECTED
        {
            get
            {
                if (ImageObjects._MUSIC_VIDEO_SELECTED == null)
                {
                    ImageObjects._MUSIC_VIDEO_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.MEDIA_CLOUD_SELECTED);
                }
                return ImageObjects._MUSIC_VIDEO_SELECTED;
            }
        }

        public static BitmapImage MEDIA_FEED
        {
            get
            {
                if (ImageObjects._MEDIA_FEED == null)
                {
                    ImageObjects._MEDIA_FEED = ImageUtility.GetBitmapImage(ImageLocation.MEDIA_FEED);
                }
                return ImageObjects._MEDIA_FEED;
            }
        }
        public static BitmapImage MEDIA_FEED_H
        {
            get
            {
                if (ImageObjects._MEDIA_FEED_H == null)
                {
                    ImageObjects._MEDIA_FEED_H = ImageUtility.GetBitmapImage(ImageLocation.MEDIA_FEED_H);
                }
                return ImageObjects._MEDIA_FEED_H;
            }
        }
        public static BitmapImage MEDIA_FEED_SELECTED
        {
            get
            {
                if (ImageObjects._MEDIA_FEED_SELECTED == null)
                {
                    ImageObjects._MEDIA_FEED_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.MEDIA_FEED_SELECTED);
                }
                return ImageObjects._MEDIA_FEED_SELECTED;
            }
        }


        public static BitmapImage CIRCLE_ICON
        {
            get
            {
                if (ImageObjects._CIRCLE_ICON == null)
                {
                    ImageObjects._CIRCLE_ICON = ImageUtility.GetBitmapImage(ImageLocation.CIRCLE_ICON);
                }
                return ImageObjects._CIRCLE_ICON;
            }
        }

        public static BitmapImage CIRCLE_ICON_H
        {
            get
            {
                if (ImageObjects._CIRCLE_ICON_H == null)
                {
                    ImageObjects._CIRCLE_ICON_H = ImageUtility.GetBitmapImage(ImageLocation.CIRCLE_ICON_H);
                }
                return ImageObjects._CIRCLE_ICON_H;
            }
        }

        public static BitmapImage CIRCLE_ICON_SELECTED
        {
            get
            {
                if (ImageObjects._CIRCLE_ICON_SELECTED == null)
                {
                    ImageObjects._CIRCLE_ICON_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.CIRCLE_ICON_SELECTED);
                }
                return ImageObjects._CIRCLE_ICON_SELECTED;
            }
        }

        public static BitmapImage STICKER_MARKET
        {
            get
            {
                if (ImageObjects._STICKER_MARKET == null)
                {
                    ImageObjects._STICKER_MARKET = ImageUtility.GetBitmapImage(ImageLocation.STICKER_MARKET);
                }
                return ImageObjects._STICKER_MARKET;
            }
        }

        public static BitmapImage STICKER_MARKET_H
        {
            get
            {
                if (ImageObjects._STICKER_MARKET_H == null)
                {
                    ImageObjects._STICKER_MARKET_H = ImageUtility.GetBitmapImage(ImageLocation.STICKER_MARKET_H);
                }
                return ImageObjects._STICKER_MARKET_H;
            }
        }

        public static BitmapImage STICKER_MARKET_SELECTED
        {
            get
            {
                if (ImageObjects._STICKER_MARKET_SELECTED == null)
                {
                    ImageObjects._STICKER_MARKET_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.STICKER_MARKET_SELECTED);
                }
                return ImageObjects._STICKER_MARKET_SELECTED;
            }
        }

        public static BitmapImage MSG_ICON
        {
            get
            {
                if (ImageObjects._MSG_ICON == null)
                {
                    ImageObjects._MSG_ICON = ImageUtility.GetBitmapImage(ImageLocation.MSG_ICON);
                }
                return ImageObjects._MSG_ICON;
            }
        }

        public static BitmapImage MSG_ICON_H
        {
            get
            {
                if (ImageObjects._MSG_ICON_H == null)
                {
                    ImageObjects._MSG_ICON_H = ImageUtility.GetBitmapImage(ImageLocation.MSG_ICON_H);
                }
                return ImageObjects._MSG_ICON_H;
            }
        }

        public static BitmapImage MSG_ICON_SELECTED
        {
            get
            {
                if (ImageObjects._MSG_ICON_SELECTED == null)
                {
                    ImageObjects._MSG_ICON_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.MSG_ICON_SELECTED);
                }
                return ImageObjects._MSG_ICON_SELECTED;
            }
        }

        public static BitmapImage CALLHISTORY_ICON
        {
            get
            {
                if (ImageObjects._CALLHISTORY_ICON == null)
                {
                    ImageObjects._CALLHISTORY_ICON = ImageUtility.GetBitmapImage(ImageLocation.CALLHISTORY_ICON);
                }
                return ImageObjects._CALLHISTORY_ICON;
            }
        }

        public static BitmapImage CALLHISTORY_ICON_H
        {
            get
            {
                if (ImageObjects._CALLHISTORY_ICON_H == null)
                {
                    ImageObjects._CALLHISTORY_ICON_H = ImageUtility.GetBitmapImage(ImageLocation.CALLHISTORY_ICON_H);
                }
                return ImageObjects._CALLHISTORY_ICON_H;
            }
        }

        public static BitmapImage CALLHISTORY_ICON_SELECTED
        {
            get
            {
                if (ImageObjects._CALLHISTORY_ICON_SELECTED == null)
                {
                    ImageObjects._CALLHISTORY_ICON_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.CALLHISTORY_ICON_SELECTED);
                }
                return ImageObjects._CALLHISTORY_ICON_SELECTED;
            }
        }

        public static BitmapImage ADD_FRIEND_STICKER
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_STICKER == null)
                {
                    ImageObjects._ADD_FRIEND_STICKER = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_STICKER);
                }
                return ImageObjects._ADD_FRIEND_STICKER;
            }
        }

        public static BitmapImage ADD_FRIEND_STICKER_H
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_STICKER_H == null)
                {
                    ImageObjects._ADD_FRIEND_STICKER_H = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_STICKER_H);
                }
                return ImageObjects._ADD_FRIEND_STICKER_H;
            }
        }

        public static BitmapImage ADD_FRIEND_STICKER_SELECTED
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_STICKER_SELECTED == null)
                {
                    ImageObjects._ADD_FRIEND_STICKER_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_STICKER_SELECTED);
                }
                return ImageObjects._ADD_FRIEND_STICKER_SELECTED;
            }
        }

        public static BitmapImage ADD_FRIEND_STICKER_HOME_H
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_STICKER_HOME_H == null)
                {
                    ImageObjects._ADD_FRIEND_STICKER_HOME_H = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_STICKER_HOME_H);
                }
                return ImageObjects._ADD_FRIEND_STICKER_HOME_H;
            }
        }

        public static BitmapImage CHAT_CALL_DEFAULT
        {
            get
            {
                if (ImageObjects._CHAT_CALL_DEFAULT == null)
                {
                    ImageObjects._CHAT_CALL_DEFAULT = ImageUtility.GetBitmapImage(ImageLocation.CHAT_CALL_DEFAULT);
                }
                return ImageObjects._CHAT_CALL_DEFAULT;
            }
        }

        public static BitmapImage CHAT_CALL_SELECTED
        {
            get
            {
                if (ImageObjects._CHAT_CALL_SELECTED == null)
                {
                    ImageObjects._CHAT_CALL_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.CHAT_CALL_SELECTED);
                }
                return ImageObjects._CHAT_CALL_SELECTED;
            }
        }

        public static BitmapImage ID_DEFAULT
        {
            get
            {
                if (ImageObjects._ID_DEFAULT == null)
                {
                    ImageObjects._ID_DEFAULT = ImageUtility.GetBitmapImage(ImageLocation.ID_DEFAULT);
                }
                return ImageObjects._ID_DEFAULT;
            }
        }

        public static BitmapImage ID_SELECTED
        {
            get
            {
                if (ImageObjects._ID_SELECTED == null)
                {
                    ImageObjects._ID_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.ID_SELECTED);
                }
                return ImageObjects._ID_SELECTED;
            }
        }

        public static BitmapImage BUTTON_FRIEND_ADD
        {
            get
            {
                if (ImageObjects._BUTTON_FRIEND_ADD == null)
                {
                    ImageObjects._BUTTON_FRIEND_ADD = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_FRIEND_ADD);
                }
                return ImageObjects._BUTTON_FRIEND_ADD;
            }
        }
        public static BitmapImage BUTTON_FRIEND_ADD_H
        {
            get
            {
                if (ImageObjects._BUTTON_FRIEND_ADD_H == null)
                {
                    ImageObjects._BUTTON_FRIEND_ADD_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_FRIEND_ADD_H);
                }
                return ImageObjects._BUTTON_FRIEND_ADD_H;
            }
        }

        public static BitmapImage INFO_BIG
        {
            get
            {
                if (ImageObjects._INFO_BIG == null)
                {
                    ImageObjects._INFO_BIG = ImageUtility.GetBitmapImage(ImageLocation.INFO_BIG);
                }
                return ImageObjects._INFO_BIG;
            }
        }
        public static BitmapImage PHOTOS
        {
            get
            {
                if (ImageObjects._PHOTOS == null)
                {
                    ImageObjects._PHOTOS = ImageUtility.GetBitmapImage(ImageLocation.PHOTOS);
                }
                return ImageObjects._PHOTOS;
            }
        }
        public static BitmapImage MV
        {
            get
            {
                if (ImageObjects._MV == null)
                {
                    ImageObjects._MV = ImageUtility.GetBitmapImage(ImageLocation.MV);
                }
                return ImageObjects._MV;
            }
        }
        public static BitmapImage DEFAULT_MEDIA_ALBUM_IMAGE
        {
            get
            {
                if (ImageObjects._MEDIA_ALBUM_DEFAULT == null)
                {
                    ImageObjects._MEDIA_ALBUM_DEFAULT = ImageUtility.GetBitmapImage(ImageLocation.DEFAULT_AUDIO_IMAGE);
                }
                return ImageObjects._MEDIA_ALBUM_DEFAULT;
            }
        }
        public static BitmapImage DEFAULT_VIDEO_ALBUM_IMAGE
        {
            get
            {
                if (ImageObjects._VIDEO_ALBUM_DEFAULT == null)
                {
                    ImageObjects._VIDEO_ALBUM_DEFAULT = ImageUtility.GetBitmapImage(ImageLocation.DEFAULT_VIDEO_IMAGE);
                }
                return ImageObjects._VIDEO_ALBUM_DEFAULT;
            }
        }

        public static BitmapImage DEFAULT_AUDIO_SMALL
        {
            get
            {
                if (ImageObjects._DEFAULT_AUDIO_SMALL == null)
                {
                    ImageObjects._DEFAULT_AUDIO_SMALL = ImageUtility.GetBitmapImage(ImageLocation.DEFAULT_AUDIO_SMALL);
                }
                return ImageObjects._DEFAULT_AUDIO_SMALL;
            }
        }

        public static BitmapImage DEFAULT_VIDEO_SMALL
        {
            get
            {
                if (ImageObjects._DEFAULT_VIDEO_SMALL == null)
                {
                    ImageObjects._DEFAULT_VIDEO_SMALL = ImageUtility.GetBitmapImage(ImageLocation.DEFAULT_VIDEO_SMALL);
                }
                return ImageObjects._DEFAULT_VIDEO_SMALL;
            }
        }
        public static BitmapImage BUTTON_INFO
        {
            get
            {
                if (ImageObjects._BUTTON_INFO == null)
                {
                    ImageObjects._BUTTON_INFO = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_INFO);
                }
                return ImageObjects._BUTTON_INFO;
            }
        }

        public static BitmapImage BUTTON_INFO_H
        {
            get
            {
                if (ImageObjects._BUTTON_INFO_H == null)
                {
                    ImageObjects._BUTTON_INFO_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_INFO_H);
                }
                return ImageObjects._BUTTON_INFO_H;
            }
        }

        public static BitmapImage BUTTON_CHAT
        {
            get
            {
                if (ImageObjects._BUTTON_CHAT == null)
                {
                    ImageObjects._BUTTON_CHAT = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_CHAT);
                }
                return ImageObjects._BUTTON_CHAT;
            }
        }

        public static BitmapImage BUTTON_CHAT_H
        {
            get
            {
                if (ImageObjects._BUTTON_CHAT_H == null)
                {
                    ImageObjects._BUTTON_CHAT_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_CHAT_H);
                }
                return ImageObjects._BUTTON_CHAT_H;
            }
        }

        public static BitmapImage BUTTON_VOICE_CALL
        {
            get
            {
                if (ImageObjects._BUTTON_VOICE_CALL == null)
                {
                    ImageObjects._BUTTON_VOICE_CALL = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_VOICE_CALL);
                }
                return ImageObjects._BUTTON_VOICE_CALL;
            }
        }

        public static BitmapImage BUTTON_VOICE_CALL_H
        {
            get
            {
                if (ImageObjects._BUTTON_VOICE_CALL_H == null)
                {
                    ImageObjects._BUTTON_VOICE_CALL_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_VOICE_CALL_H);
                }
                return ImageObjects._BUTTON_VOICE_CALL_H;
            }
        }

        public static BitmapImage BUTTON_VIDEO_CALL
        {
            get
            {
                if (ImageObjects._BUTTON_VIDEO_CALL == null)
                {
                    ImageObjects._BUTTON_VIDEO_CALL = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_VIDEO_CALL);
                }
                return ImageObjects._BUTTON_VIDEO_CALL;
            }
        }

        public static BitmapImage BUTTON_VIDEO_CALL_H
        {
            get
            {
                if (ImageObjects._BUTTON_VIDEO_CALL_H == null)
                {
                    ImageObjects._BUTTON_VIDEO_CALL_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_VIDEO_CALL_H);
                }
                return ImageObjects._BUTTON_VIDEO_CALL_H;
            }
        }

        //public static BitmapImage MESSAGE
        //{
        //    get
        //    {
        //        if (ImageObjects._MESSAGE == null)
        //        {
        //            ImageObjects._MESSAGE = ImageUtility.GetBitmapImage(ImageLocation.MESSAGE);
        //        }
        //        return ImageObjects._MESSAGE;
        //    }
        //}

        //public static BitmapImage MESSAGE_H
        //{
        //    get
        //    {
        //        if (ImageObjects._MESSAGE_H == null)
        //        {
        //            ImageObjects._MESSAGE_H = ImageUtility.GetBitmapImage(ImageLocation.MESSAGE_H);
        //        }
        //        return ImageObjects._MESSAGE_H;
        //    }
        //}

        //public static BitmapImage IGNORE_CALL
        //{
        //    get
        //    {
        //        if (ImageObjects._IGNORE_CALL == null)
        //        {
        //            ImageObjects._IGNORE_CALL = ImageUtility.GetBitmapImage(ImageLocation.IGNORE_CALL);
        //        }
        //        return ImageObjects._IGNORE_CALL;
        //    }
        //}

        //public static BitmapImage IGNORE_CALL_H
        //{
        //    get
        //    {
        //        if (ImageObjects._IGNORE_CALL_H == null)
        //        {
        //            ImageObjects._IGNORE_CALL_H = ImageUtility.GetBitmapImage(ImageLocation.IGNORE_CALL_H);
        //        }
        //        return ImageObjects._IGNORE_CALL_H;
        //    }
        //}

        //public static BitmapImage INCOMING_CALL_ANIMATION
        //{
        //    get
        //    {
        //        if (ImageObjects._INCOMING_CALL_ANIMATION == null)
        //        {
        //            ImageObjects._INCOMING_CALL_ANIMATION = ImageUtility.GetBitmapImage(ImageLocation.INCOMING_CALL_ANIMATION);
        //        }
        //        return ImageObjects._INCOMING_CALL_ANIMATION;
        //    }
        //}

        //public static BitmapImage OUTGOING_CALL_ANIMATION
        //{
        //    get
        //    {
        //        if (ImageObjects._OUTGOING_CALL_ANIMATION == null)
        //        {
        //            ImageObjects._OUTGOING_CALL_ANIMATION = ImageUtility.GetBitmapImage(ImageLocation.OUTGOING_CALL_ANIMATION);
        //        }
        //        return ImageObjects._OUTGOING_CALL_ANIMATION;
        //    }
        //}
        public static BitmapImage LOADING_PROGRESS
        {
            get
            {
                if (ImageObjects._LOADING_PROGRESS == null)
                {
                    ImageObjects._LOADING_PROGRESS = ImageUtility.GetBitmapImage(ImageLocation.LOADER_PRGOGRESS);
                }
                return ImageObjects._LOADING_PROGRESS;
            }
        }
        public static BitmapImage LOADING_RINGID_LOGO
        {
            get
            {
                if (ImageObjects._LOADING_RINGID_LOGO == null)
                {
                    ImageObjects._LOADING_RINGID_LOGO = ImageUtility.GetBitmapImage(ImageLocation.LOADER_100);
                }
                return ImageObjects._LOADING_RINGID_LOGO;
            }
            set
            {
                ImageObjects._LOADING_RINGID_LOGO = value;
            }
        }

        public static BitmapImage LOADING_RINGID_FEED
        {
            get
            {
                if (ImageObjects._LOADING_RINGID_FEED == null)
                {
                    ImageObjects._LOADING_RINGID_FEED = ImageUtility.GetBitmapImage(ImageLocation.LOADING_RINGID_FEED);
                }
                return ImageObjects._LOADING_RINGID_FEED;
            }
            set
            {
                ImageObjects._LOADING_RINGID_FEED = value;
            }
        }
        public static BitmapImage LOADING_WATCH
        {
            get
            {
                if (ImageObjects._LOADING_WATCH == null)
                {
                    ImageObjects._LOADING_WATCH = ImageUtility.GetBitmapImage(ImageLocation.LOADER_CLOCK);
                }
                return ImageObjects._LOADING_WATCH;
            }
        }
        public static BitmapImage LOADER_PLAYER_BUFFER
        {
            get
            {
                if (ImageObjects._LOADER_PLAYER_BUFFER == null)
                {
                    ImageObjects._LOADER_PLAYER_BUFFER = ImageUtility.GetBitmapImage(ImageLocation.LOADER_PLAYER_BUFFER);
                }
                return ImageObjects._LOADER_PLAYER_BUFFER;
            }
            set
            {
                ImageObjects._LOADER_PLAYER_BUFFER = value;
            }
        }

        public static BitmapImage LOADER_OFFLINE
        {
            get
            {
                if (ImageObjects._LOADER_OFFLINE == null)
                {
                    ImageObjects._LOADER_OFFLINE = ImageUtility.GetBitmapImage(ImageLocation.LOADER_OFFLINE);
                }
                return ImageObjects._LOADER_OFFLINE;
            }
            set
            {
                ImageObjects._LOADER_OFFLINE = value;
            }
        }
        public static BitmapImage LOADER_PLAYER_MUSIC
        {
            get
            {
                if (ImageObjects._LOADER_PLAYER_MUSIC == null)
                {
                    ImageObjects._LOADER_PLAYER_MUSIC = ImageUtility.GetBitmapImage(ImageLocation.LOADER_PLAYER_MUSIC);
                }
                return ImageObjects._LOADER_PLAYER_MUSIC;
            }
            set
            {
                ImageObjects._LOADER_PLAYER_MUSIC = value;
            }
        }
        public static BitmapImage PLAYER_PAUSED_ICON
        {
            get
            {
                if (ImageObjects._PLAYER_PAUSED_ICON == null)
                {
                    ImageObjects._PLAYER_PAUSED_ICON = ImageUtility.GetBitmapImage(ImageLocation.MEDIA_VIEW_PLAY_ICON);
                }
                return ImageObjects._PLAYER_PAUSED_ICON;
            }
            set
            {
                ImageObjects._PLAYER_PAUSED_ICON = value;
            }
        }

        public static BitmapImage PLAYER_PLAY_LARGE_ICON { get { return ImageUtility.GetBitmapImage(ImageLocation.MEDIA_VIEW_PLAY_LARGE); } }

        public static BitmapImage PLAYER_PLAY_LARGE_ICON_H { get { return ImageUtility.GetBitmapImage(ImageLocation.MEDIA_VIEW_PLAY_LARGE_H); } }

        public static BitmapImage SPLASH_SCREEN_LOADER
        {
            get
            {
                if (ImageObjects._SPLASH_SCREEN_LOADER == null)
                {
                    ImageObjects._SPLASH_SCREEN_LOADER = ImageUtility.GetBitmapImage(ImageLocation.SPLASH_SCREEN_LOADER);
                }
                return ImageObjects._SPLASH_SCREEN_LOADER;
            }
        }
        public static BitmapImage LOADER_FEED
        {
            get
            {
                if (ImageObjects._LOADER_FEED == null)
                {
                    ImageObjects._LOADER_FEED = ImageUtility.GetBitmapImage(ImageLocation.LOADER_FEED);
                }
                return ImageObjects._LOADER_FEED;
            }
            set
            {
                ImageObjects._LOADER_FEED = value;
            }
        }
        public static BitmapImage LOADER_FEED_CYCLE
        {
            get
            {
                if (ImageObjects._LOADER_FEED_CYCLE == null)
                {
                    ImageObjects._LOADER_FEED_CYCLE = ImageUtility.GetBitmapImage(ImageLocation.LOADER_FEED_CYCLE);
                }
                return ImageObjects._LOADER_FEED_CYCLE;
            }
            set
            {
                ImageObjects._LOADER_FEED_CYCLE = value;
            }
        }

        public static BitmapImage CIRCLE_OUTSIDE_PROFILE_PIC
        {
            get
            {
                if (ImageObjects._CIRCLE_OUTSIDE_PROFILE_PIC == null)
                {
                    ImageObjects._CIRCLE_OUTSIDE_PROFILE_PIC = ImageUtility.GetBitmapImage(ImageLocation.CIRCLE_OUTSIDE_PROFILE_PIC);
                }
                return ImageObjects._CIRCLE_OUTSIDE_PROFILE_PIC;
            }
        }

        public static BitmapImage CIRCLE_OUTSIDE_PROFILE_PIC_MYNAMEPANEL
        {
            get
            {
                if (ImageObjects._CIRCLE_OUTSIDE_PROFILE_PIC_MYNAMEPANEL == null)
                {
                    ImageObjects._CIRCLE_OUTSIDE_PROFILE_PIC_MYNAMEPANEL = ImageUtility.GetBitmapImage(ImageLocation.CIRCLE_OUTSIDE_PROFILE_PIC_MYNAMEPANEL);
                }
                return ImageObjects._CIRCLE_OUTSIDE_PROFILE_PIC_MYNAMEPANEL;
            }
        }

        public static BitmapImage CIRCLE_OUTSIDE_PROFILE_PIC_PROFILE
        {
            get
            {
                if (ImageObjects._CIRCLE_OUTSIDE_PROFILE_PIC_PROFILE == null)
                {
                    ImageObjects._CIRCLE_OUTSIDE_PROFILE_PIC_PROFILE = ImageUtility.GetBitmapImage(ImageLocation.CIRCLE_OUTSIDE_PROFILE_PIC_PROFILE);
                }
                return ImageObjects._CIRCLE_OUTSIDE_PROFILE_PIC_PROFILE;
            }
        }

        public static BitmapImage UNKNOWN_IMAGE
        {
            get
            {
                if (ImageObjects._UNKNOWN_IMAGE == null)
                {
                    ImageObjects._UNKNOWN_IMAGE = ImageUtility.GetBitmapImage(ImageLocation.UNKNOWN_IMAGE);
                }
                return ImageObjects._UNKNOWN_IMAGE;
            }
        }

        public static BitmapImage UNKNOWN_IMAGE_LARGE
        {
            get
            {
                if (ImageObjects._UNKNOWN_IMAGE_LARGE == null)
                {
                    ImageObjects._UNKNOWN_IMAGE_LARGE = ImageUtility.GetBitmapImage(ImageLocation.UNKNOWN_IMAGE_LARGE);
                }
                return ImageObjects._UNKNOWN_IMAGE_LARGE;
            }
        }

        public static BitmapImage CELEBRITY_DEFAULT_IMAGE
        {
            get
            {
                if (ImageObjects._CELEBRITY_DEFAULT_IMAGE == null)
                {
                    ImageObjects._CELEBRITY_DEFAULT_IMAGE = ImageUtility.GetBitmapImage(ImageLocation.CELEBRITY_DEFAULT_IMAGE);
                }
                return ImageObjects._CELEBRITY_DEFAULT_IMAGE;
            }
        }

        public static BitmapImage ROOM_DEFAULT_IMAGE
        {
            get
            {
                if (ImageObjects._ROOM_DEFAULT_IMAGE == null)
                {
                    ImageObjects._ROOM_DEFAULT_IMAGE = ImageUtility.GetBitmapImage(ImageLocation.ROOM_DEFAULT_IMAGE);
                }
                return ImageObjects._ROOM_DEFAULT_IMAGE;
            }
        }

        public static BitmapImage LOADER_MEDIUM
        {
            get
            {
                if (ImageObjects._LOADER_MEDIUM == null)
                {
                    ImageObjects._LOADER_MEDIUM = ImageUtility.GetBitmapImage(ImageLocation.LOADER_MEDIUM);
                }
                return ImageObjects._LOADER_MEDIUM;
            }
            set
            {
                ImageObjects._LOADER_MEDIUM = value;
            }
        }

        public static BitmapImage LOADER_SMALL
        {
            get
            {
                if (ImageObjects._LOADER_SMALL == null)
                {
                    ImageObjects._LOADER_SMALL = ImageUtility.GetBitmapImage(ImageLocation.LOADER_SMALL);
                }
                return ImageObjects._LOADER_SMALL;
            }
            set
            {
                ImageObjects._LOADER_SMALL = value;
            }
        }

        public static BitmapImage LOADER_LIKE_ANIMATION
        {
            get
            {
                if (ImageObjects._LOADER_LIKE_ANIMATION == null)
                {
                    ImageObjects._LOADER_LIKE_ANIMATION = ImageUtility.GetBitmapImage(ImageLocation.LOADER_LIKE_ANIMATION);
                }
                return ImageObjects._LOADER_LIKE_ANIMATION;
            }
        }

        public static BitmapImage NO_IMAGE_FOUND
        {
            get
            {
                if (ImageObjects._NO_IMAGE_FOUND == null)
                {
                    ImageObjects._NO_IMAGE_FOUND = ImageUtility.GetBitmapImage(ImageLocation.NO_IMAGE_FOUND);
                }
                return ImageObjects._NO_IMAGE_FOUND;
            }
        }

        public static BitmapImage DEFAULT_COVER_IMAGE
        {
            get
            {
                if (ImageObjects._DEFAULT_COVER_IMAGE == null)
                {
                    ImageObjects._DEFAULT_COVER_IMAGE = ImageUtility.GetBitmapImage(ImageLocation.DEFAULT_COVER_IMAGE);
                }
                return ImageObjects._DEFAULT_COVER_IMAGE;
            }
        }

        public static BitmapImage COVER_IMAGE_BACKGROUND
        {
            get
            {
                if (ImageObjects._COVER_IMAGE_BACKGROUND == null)
                {
                    ImageObjects._COVER_IMAGE_BACKGROUND = ImageUtility.GetBitmapImage(ImageLocation.COVER_IMAGE_BACKGROUND);
                }
                return ImageObjects._COVER_IMAGE_BACKGROUND;
            }
        }

        public static BitmapImage PROFILE_IMAGE_BOX_UNKNOWN
        {
            get
            {
                if (ImageObjects._PROFILE_IMAGE_UNKNOWN == null)
                {
                    ImageObjects._PROFILE_IMAGE_UNKNOWN = ImageUtility.GetBitmapImage(ImageLocation.PROFILE_IMAGE_BOX_UNKNOWN);
                }
                return ImageObjects._PROFILE_IMAGE_UNKNOWN;
            }
        }
        public static BitmapImage MENU_MOOD
        {
            get
            {
                if (ImageObjects._MENU_MOOD == null)
                {
                    ImageObjects._MENU_MOOD = ImageUtility.GetBitmapImage(ImageLocation.MENU_MOOD);
                }
                return ImageObjects._MENU_MOOD;
            }
        }
        public static BitmapImage MENU_SIGN_OUT
        {
            get
            {
                if (ImageObjects._MENU_SIGN_OUT == null)
                {
                    ImageObjects._MENU_SIGN_OUT = ImageUtility.GetBitmapImage(ImageLocation.MENU_SIGN_OUT);
                }
                return ImageObjects._MENU_SIGN_OUT;
            }
        }
        public static BitmapImage MENU_CLOSE
        {
            get
            {
                if (ImageObjects._MENU_CLOSE == null)
                {
                    ImageObjects._MENU_CLOSE = ImageUtility.GetBitmapImage(ImageLocation.MENU_CLOSE);
                }
                return ImageObjects._MENU_CLOSE;
            }
        }


        public static BitmapImage MENU_CHECK_FOR_UPDATES
        {
            get
            {
                if (ImageObjects._MENU_CHECK_FOR_UPDATE == null)
                {
                    ImageObjects._MENU_CHECK_FOR_UPDATE = ImageUtility.GetBitmapImage(ImageLocation.MENU_CHECK_FOR_UPDATE);
                }
                return ImageObjects._MENU_CHECK_FOR_UPDATE;
            }
        }
        public static BitmapImage MENU_SETTINGS
        {
            get
            {
                if (ImageObjects._MENU_SETTINGS == null)
                {
                    ImageObjects._MENU_SETTINGS = ImageUtility.GetBitmapImage(ImageLocation.MENU_SETTINGS);
                }
                return ImageObjects._MENU_SETTINGS;
            }
        }

        public static BitmapImage MENU_HELP_DESK
        {
            get
            {
                if (ImageObjects._MENU_HELP_DESK == null)
                {
                    ImageObjects._MENU_HELP_DESK = ImageUtility.GetBitmapImage(ImageLocation.MENU_HELP_DESK);
                }
                return ImageObjects._MENU_HELP_DESK;
            }
        }
        public static BitmapImage MENU_INFO
        {
            get
            {
                if (ImageObjects._MENU_INFO == null)
                {
                    ImageObjects._MENU_INFO = ImageUtility.GetBitmapImage(ImageLocation.MENU_INFO);
                }
                return ImageObjects._MENU_INFO;
            }
        }
        public static BitmapImage PRIVACE_POLICY
        {
            get
            {
                if (ImageObjects._MENU_PRIVACY_POLICY == null)
                {
                    ImageObjects._MENU_PRIVACY_POLICY = ImageUtility.GetBitmapImage(ImageLocation.PRIVACE_POLICY);
                }
                return ImageObjects._MENU_PRIVACY_POLICY;
            }
        }
        public static BitmapImage CONTACT_US_ACTIVE
        {
            get
            {
                if (ImageObjects._MENU_CAONATCT_US_ACTIVE == null)
                {
                    ImageObjects._MENU_CAONATCT_US_ACTIVE = ImageUtility.GetBitmapImage(ImageLocation.CONTACT_US_ACTIVE);
                }
                return ImageObjects._MENU_CAONATCT_US_ACTIVE;
            }
        }
        public static BitmapImage CONTACT_US_INACTIVE
        {
            get
            {
                if (ImageObjects._MENU_CAONATCT_US_INACTIVE == null)
                {
                    ImageObjects._MENU_CAONATCT_US_INACTIVE = ImageUtility.GetBitmapImage(ImageLocation.CONTACT_US_INACTIVE);
                }
                return ImageObjects._MENU_CAONATCT_US_INACTIVE;
            }
        }
        public static BitmapImage ABOUIT_RINGID
        {
            get
            {
                if (ImageObjects._MENU_ABOUT_RINGID == null)
                {
                    ImageObjects._MENU_ABOUT_RINGID = ImageUtility.GetBitmapImage(ImageLocation.ABOUIT_RINGID);
                }
                return ImageObjects._MENU_ABOUT_RINGID;
            }
        }

        public static BitmapImage STATUS_ONLINE
        {
            get
            {
                if (ImageObjects._STATUS_ONLINE == null)
                {
                    ImageObjects._STATUS_ONLINE = ImageUtility.GetBitmapImage(ImageLocation.STATUS_ONLINE);
                }
                return ImageObjects._STATUS_ONLINE;
            }
        }

        public static BitmapImage STATUS_OFFLINE
        {
            get
            {
                if (ImageObjects._STATUS_OFFLINE == null)
                {
                    ImageObjects._STATUS_OFFLINE = ImageUtility.GetBitmapImage(ImageLocation.LOADER_OFFLINE); //ImageLocation.STATUS_OFFLINE
                }
                return ImageObjects._STATUS_OFFLINE;
            }
        }

        public static BitmapImage STATUS_HOVER
        {
            get
            {
                if (ImageObjects._STATUS_HOVER == null)
                {
                    ImageObjects._STATUS_HOVER = ImageUtility.GetBitmapImage(ImageLocation.STATUS_HOVER);
                }
                return ImageObjects._STATUS_HOVER;
            }
        }

        public static BitmapImage SETTING_MINI
        {
            get
            {
                if (ImageObjects._SETTING_MINI == null)
                {
                    ImageObjects._SETTING_MINI = ImageUtility.GetBitmapImage(ImageLocation.SETTING_MINI);
                }
                return ImageObjects._SETTING_MINI;
            }
        }

        public static BitmapImage SETTING_MINI_H
        {
            get
            {
                if (ImageObjects._SETTING_MINI_H == null)
                {
                    ImageObjects._SETTING_MINI_H = ImageUtility.GetBitmapImage(ImageLocation.SETTING_MINI_H);
                }
                return ImageObjects._SETTING_MINI_H;
            }
        }

        public static BitmapImage BUTTON_CLOSE_MINI
        {
            get
            {
                if (ImageObjects._BUTTON_CLOSE_MINI == null)
                {
                    ImageObjects._BUTTON_CLOSE_MINI = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_CANCEL_EDIT);
                }
                return ImageObjects._BUTTON_CLOSE_MINI;
            }
        }

        public static BitmapImage BUTTON_CLOSE_MINI_H
        {
            get
            {
                if (ImageObjects._BUTTON_CLOSE_MINI_H == null)
                {
                    ImageObjects._BUTTON_CLOSE_MINI_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_CANCEL_EDIT_H);
                }
                return ImageObjects._BUTTON_CLOSE_MINI_H;
            }
        }

        public static BitmapImage BUTTON_OK_MINI
        {
            get
            {
                if (ImageObjects._BUTTON_OK_MINI == null)
                {
                    ImageObjects._BUTTON_OK_MINI = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_OK_MINI);
                }
                return ImageObjects._BUTTON_OK_MINI;
            }
        }

        public static BitmapImage BUTTON_OK_MINI_H
        {
            get
            {
                if (ImageObjects._BUTTON_OK_MINI_H == null)
                {
                    ImageObjects._BUTTON_OK_MINI_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_OK_MINI_H);
                }
                return ImageObjects._BUTTON_OK_MINI_H;
            }
        }

        public static BitmapImage BUTTON_UPLOAD_FROM_DIRECTORY
        {
            get
            {
                if (ImageObjects._BUTTON_UPLOAD_FROM_DIRECTORY == null)
                {
                    ImageObjects._BUTTON_UPLOAD_FROM_DIRECTORY = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_UPLOAD_FROM_DIRECTORY);
                }
                return ImageObjects._BUTTON_UPLOAD_FROM_DIRECTORY;
            }
        }

        public static BitmapImage BUTTON_UPLOAD_FROM_DIRECTORY_H
        {
            get
            {
                if (ImageObjects._BUTTON_UPLOAD_FROM_DIRECTORY_H == null)
                {
                    ImageObjects._BUTTON_UPLOAD_FROM_DIRECTORY_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_UPLOAD_FROM_DIRECTORY_H);
                }
                return ImageObjects._BUTTON_UPLOAD_FROM_DIRECTORY_H;
            }
        }

        public static BitmapImage BUTTON_CAMERA
        {
            get
            {
                if (ImageObjects._BUTTON_CAMERA == null)
                {
                    ImageObjects._BUTTON_CAMERA = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_CAMERA);
                }
                return ImageObjects._BUTTON_CAMERA;
            }
        }

        public static BitmapImage BUTTON_CAMERA_H
        {
            get
            {
                if (ImageObjects._BUTTON_CAMERA_H == null)
                {
                    ImageObjects._BUTTON_CAMERA_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_CAMERA_H);
                }
                return ImageObjects._BUTTON_CAMERA_H;
            }
        }

        public static BitmapImage AWAY
        {
            get
            {
                if (ImageObjects._AWAY == null)
                {
                    ImageObjects._AWAY = ImageUtility.GetBitmapImage(ImageLocation.AWAY);
                }
                return ImageObjects._AWAY;
            }
        }

        public static BitmapImage OFFLINE
        {
            get
            {
                if (ImageObjects._OFFLINE == null)
                {
                    ImageObjects._OFFLINE = ImageUtility.GetBitmapImage(ImageLocation.OFFLINE);
                }
                return ImageObjects._OFFLINE;
            }
        }

        public static BitmapImage STATUS_DONOT_DISTURB
        {
            get
            {
                if (ImageObjects._STATUS_DONOT_DISTURB == null)
                {
                    ImageObjects._STATUS_DONOT_DISTURB = ImageUtility.GetBitmapImage(ImageLocation.STATUS_DONOT_DISTURB);
                }
                return ImageObjects._STATUS_DONOT_DISTURB;
            }
        }

        public static BitmapImage ONLINE_DESKTOP
        {
            get
            {
                if (ImageObjects._ONLINE_DESKTOP == null)
                {
                    ImageObjects._ONLINE_DESKTOP = ImageUtility.GetBitmapImage(ImageLocation.ONLINE_DESKTOP);
                }
                return ImageObjects._ONLINE_DESKTOP;
            }
        }

        public static BitmapImage ONLINE_ANDROID
        {
            get
            {
                if (ImageObjects._ONLINE_ANDROID == null)
                {
                    ImageObjects._ONLINE_ANDROID = ImageUtility.GetBitmapImage(ImageLocation.ONLINE_ANDROID);
                }
                return ImageObjects._ONLINE_ANDROID;
            }
        }

        public static BitmapImage ONLINE_IOS
        {
            get
            {
                if (ImageObjects._ONLINE_IOS == null)
                {
                    ImageObjects._ONLINE_IOS = ImageUtility.GetBitmapImage(ImageLocation.ONLINE_IOS);
                }
                return ImageObjects._ONLINE_IOS;
            }
        }

        public static BitmapImage ONLINE_WEB
        {
            get
            {
                if (ImageObjects._ONLINE_WEB == null)
                {
                    ImageObjects._ONLINE_WEB = ImageUtility.GetBitmapImage(ImageLocation.ONLINE_WEB);
                }
                return ImageObjects._ONLINE_WEB;
            }
        }

        public static BitmapImage ONLINE_WINDOWS
        {
            get
            {
                if (ImageObjects._ONLINE_WINDOWS == null)
                {
                    ImageObjects._ONLINE_WINDOWS = ImageUtility.GetBitmapImage(ImageLocation.ONLINE_WINDOWS);
                }
                return ImageObjects._ONLINE_WINDOWS;
            }
        }

        public static BitmapImage STAR
        {
            get
            {
                if (ImageObjects._STAR == null)
                {
                    ImageObjects._STAR = ImageUtility.GetBitmapImage(ImageLocation.STAR);
                }
                return ImageObjects._STAR;
            }
        }

        public static BitmapImage STAR_H
        {
            get
            {
                if (ImageObjects._STAR_H == null)
                {
                    ImageObjects._STAR_H = ImageUtility.GetBitmapImage(ImageLocation.STAR_H);
                }
                return ImageObjects._STAR_H;
            }
        }

        public static BitmapImage UNKNOWN_70
        {
            get
            {
                if (ImageObjects._UNKNOWN_70 == null)
                {
                    ImageObjects._UNKNOWN_70 = ImageUtility.GetBitmapImage(ImageLocation.UNKNOWN_70);
                }
                return ImageObjects._UNKNOWN_70;
            }
        }

        public static BitmapImage UNKNOWN_25
        {
            get
            {
                if (ImageObjects._UNKNOWN_25 == null)
                {
                    ImageObjects._UNKNOWN_25 = ImageUtility.GetBitmapImage(ImageLocation.UNKNOWN_25);
                }
                return ImageObjects._UNKNOWN_25;
            }
        }

        public static BitmapImage CIRCLE_IMAGE_IN_LIST
        {
            get
            {
                if (ImageObjects._CIRCLE_IMAGE_IN_LIST == null)
                {
                    ImageObjects._CIRCLE_IMAGE_IN_LIST = ImageUtility.GetBitmapImage(ImageLocation.CIRCLE_IMAGE_IN_LIST);
                }
                return ImageObjects._CIRCLE_IMAGE_IN_LIST;
            }
        }

        public static BitmapImage DOWN_ARROW
        {
            get
            {
                if (ImageObjects._DOWN_ARROW == null)
                {
                    ImageObjects._DOWN_ARROW = ImageUtility.GetBitmapImage(ImageLocation.DOWN_ARROW);
                }
                return ImageObjects._DOWN_ARROW;
            }
        }

        public static BitmapImage DOWN_ARROW_H
        {
            get
            {
                if (ImageObjects._DOWN_ARROW_H == null)
                {
                    ImageObjects._DOWN_ARROW_H = ImageUtility.GetBitmapImage(ImageLocation.DOWN_ARROW_H);
                }
                return ImageObjects._DOWN_ARROW_H;
            }
        }

        public static BitmapImage TICK_MARK
        {
            get
            {
                if (ImageObjects._TICK_MARK == null)
                {
                    ImageObjects._TICK_MARK = ImageUtility.GetBitmapImage(ImageLocation.TICK_MARK);
                }
                return ImageObjects._TICK_MARK;
            }
        }

        public static BitmapImage WATCH
        {
            get
            {
                if (ImageObjects._WATCH == null)
                {
                    ImageObjects._WATCH = ImageUtility.GetBitmapImage(ImageLocation.WATCH);
                }
                return ImageObjects._WATCH;
            }
        }

        public static BitmapImage WATCH_H
        {
            get
            {
                if (ImageObjects._WATCH_H == null)
                {
                    ImageObjects._WATCH_H = ImageUtility.GetBitmapImage(ImageLocation.WATCH_H);
                }
                return ImageObjects._WATCH_H;
            }
        }

        public static ConcurrentDictionary<string, BitmapImage> EMOTICON_ICON
        {
            get
            {
                return ImageObjects._EMOTICON_ICON;
            }
        }

        public static void InitEmoticonIcon()
        {
            if (ImageObjects._EMOTICON_ICON == null)
            {
                try
                {
                    _EMOTICON_ICON = new ConcurrentDictionary<string, BitmapImage>();
                    //List<EmoticonDTO> list = new List<EmoticonDTO>();

                    foreach (EmoticonDTO entry in DefaultDictionaries.Instance.EMOTICON_DICTIONARY.Values)
                    {
                        //list.Add(entry);
                        EMOTICON_ICON[entry.Symbol] = null;
                    }
                    // list = list.OrderByDescending(P => P.Symbol).ToList();

                    //foreach (EmoticonDTO entry in list)
                    //{

                    //    BitmapImage icon = ImageUtility.GetBitmapImageOfDynamicResource(RingIDSettings.EMOTICON_FOLDER + Path.DirectorySeparatorChar + "dmid" + Path.DirectorySeparatorChar + entry.Url);
                    //    if (icon != null)
                    //    {
                    //        EMOTICON_ICON[entry.Symbol] = icon;
                    //    }
                    //}
                }
                catch (Exception e)
                {
                    log.Error("Error In Getting Emoticon Icon : " + e.Message + "\n" + e.StackTrace);
                }
            }
        }

        public static BitmapImage POPUP_SIGN_IN
        {
            get
            {
                if (ImageObjects._POPUP_SIGN_IN == null)
                {
                    ImageObjects._POPUP_SIGN_IN = ImageUtility.GetBitmapImage(ImageLocation.POPUP_SIGN_IN);
                }
                return ImageObjects._POPUP_SIGN_IN;
            }
        }

        public static BitmapImage POPUP_MUTUAL_FRIENDS
        {
            get
            {
                if (ImageObjects._POPUP_MUTUAL_FRIENDS == null)
                {
                    ImageObjects._POPUP_MUTUAL_FRIENDS = ImageUtility.GetBitmapImage(ImageLocation.POPUP_MUTUAL_FRIENDS);
                }
                return ImageObjects._POPUP_MUTUAL_FRIENDS;
            }
        }

        public static BitmapImage POPUP_ABOUT_RINGID
        {
            get
            {
                if (ImageObjects._POPUP_ABOUT_RINGID == null)
                {
                    ImageObjects._POPUP_ABOUT_RINGID = ImageUtility.GetBitmapImage(ImageLocation.POPUP_ABOUT_RINGID);
                }
                return ImageObjects._POPUP_ABOUT_RINGID;
            }
        }

        public static BitmapImage POPUP_STATUS
        {
            get
            {
                if (ImageObjects._POPUP_STATUS == null)
                {
                    ImageObjects._POPUP_STATUS = ImageUtility.GetBitmapImage(ImageLocation.POPUP_STATUS);
                }
                return ImageObjects._POPUP_STATUS;
            }
        }

        public static BitmapImage POPUP_EDIT_DELETE
        {
            get
            {
                if (ImageObjects._POPUP_EDIT_DELETE == null)
                {
                    ImageObjects._POPUP_EDIT_DELETE = ImageUtility.GetBitmapImage(ImageLocation.POPUP_EDIT_DELETE);
                }
                return ImageObjects._POPUP_EDIT_DELETE;
            }
        }
        public static BitmapImage POPUP_THREE_ITEMS
        {
            get
            {
                if (ImageObjects._POPUP_THREE_ITEMS == null)
                {
                    ImageObjects._POPUP_THREE_ITEMS = ImageUtility.GetBitmapImage(ImageLocation.POPUP_THREE_ITEMS);
                }
                return ImageObjects._POPUP_THREE_ITEMS;
            }
        }
        public static BitmapImage POPUP_DELETE
        {
            get
            {
                if (ImageObjects._POPUP_DELETE == null)
                {
                    ImageObjects._POPUP_DELETE = ImageUtility.GetBitmapImage(ImageLocation.POPUP_DELETE);
                }
                return ImageObjects._POPUP_DELETE;
            }
        }

        public static BitmapImage TAKE_PROFILE_PHOTO
        {
            get
            {
                if (ImageObjects._TAKE_PHOTO == null)
                {
                    ImageObjects._TAKE_PHOTO = ImageUtility.GetBitmapImage(ImageLocation.TAKE_PROFILE_PHOTO);
                }
                return ImageObjects._TAKE_PHOTO;
            }
        }
        public static BitmapImage TAKE_PROFILE_PHOTO_H
        {
            get
            {
                if (ImageObjects._TAKE_PHOTO_H == null)
                {
                    ImageObjects._TAKE_PHOTO_H = ImageUtility.GetBitmapImage(ImageLocation.TAKE_PROFILE_PHOTO_H);
                }
                return ImageObjects._TAKE_PHOTO_H;
            }
        }
        public static BitmapImage TAKE_PROFILE_PHOTO_SELECTED
        {
            get
            {
                if (ImageObjects._TAKE_PHOTO_SELECTED == null)
                {
                    ImageObjects._TAKE_PHOTO_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.TAKE_PROFILE_PHOTO_SELECTED);
                }
                return ImageObjects._TAKE_PHOTO_SELECTED;
            }
        }

        public static BitmapImage UPLOAD_PROFILE_PHOTO
        {
            get
            {
                if (ImageObjects._UPLOAD_FROM_COMPUTER == null)
                {
                    ImageObjects._UPLOAD_FROM_COMPUTER = ImageUtility.GetBitmapImage(ImageLocation.UPLOAD_PROFILE_PHOTO);
                }
                return ImageObjects._UPLOAD_FROM_COMPUTER;
            }
        }
        public static BitmapImage UPLOAD_PROFILE_PHOTO_H
        {
            get
            {
                if (ImageObjects._UPLOAD_FROM_COMPUTER_H == null)
                {
                    ImageObjects._UPLOAD_FROM_COMPUTER_H = ImageUtility.GetBitmapImage(ImageLocation.UPLOAD_PROFILE_PHOTO_H);
                }
                return ImageObjects._UPLOAD_FROM_COMPUTER_H;
            }
        }
        public static BitmapImage UPLOAD_PROFILE_PHOTO_SELECTED
        {
            get
            {
                if (ImageObjects._UPLOAD_FROM_COMPUTER_SELECTED == null)
                {
                    ImageObjects._UPLOAD_FROM_COMPUTER_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.UPLOAD_PROFILE_PHOTO_SELECTED);
                }
                return ImageObjects._UPLOAD_FROM_COMPUTER_SELECTED;
            }
        }
        public static BitmapImage EDIT_PHOTO
        {
            get
            {
                if (ImageObjects._EDIT_PHOTO == null)
                {
                    ImageObjects._EDIT_PHOTO = ImageUtility.GetBitmapImage(ImageLocation.EDIT_PHOTO);
                }
                return ImageObjects._EDIT_PHOTO;
            }
        }
        public static BitmapImage EDIT_PHOTO_H
        {
            get
            {
                if (ImageObjects._EDIT_PHOTO_H == null)
                {
                    ImageObjects._EDIT_PHOTO_H = ImageUtility.GetBitmapImage(ImageLocation.EDIT_PHOTO_H);
                }
                return ImageObjects._EDIT_PHOTO_H;
            }
        }
        public static BitmapImage EDIT_PHOTO_SELECTED
        {
            get
            {
                if (ImageObjects._EDIT_PHOTO_SELECTED == null)
                {
                    ImageObjects._EDIT_PHOTO_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.EDIT_PHOTO_SELECTED);
                }
                return ImageObjects._EDIT_PHOTO_SELECTED;
            }
        }

        public static BitmapImage CHAT_DEFAULT_AUDIO
        {
            get
            {
                if (ImageObjects._CHAT_DEFAULT_AUDIO == null)
                {
                    ImageObjects._CHAT_DEFAULT_AUDIO = ImageUtility.GetBitmapImage(ImageLocation.CHAT_DEFAULT_AUDIO);
                }
                return ImageObjects._CHAT_DEFAULT_AUDIO;
            }
        }

        public static BitmapImage CHAT_DEFAULT_FILE
        {
            get
            {
                if (ImageObjects._CHAT_DEFAULT_FILE == null)
                {
                    ImageObjects._CHAT_DEFAULT_FILE = ImageUtility.GetBitmapImage(ImageLocation.CHAT_DEFAULT_FILE);
                }
                return ImageObjects._CHAT_DEFAULT_FILE;
            }
        }

        public static BitmapImage CHAT_DEFAULT_FILE_DOWNLOAD
        {
            get
            {
                if (ImageObjects._CHAT_DEFAULT_FILE_DOWNLOAD == null)
                {
                    ImageObjects._CHAT_DEFAULT_FILE_DOWNLOAD = ImageUtility.GetBitmapImage(ImageLocation.CHAT_DEFAULT_FILE_DOWNLOAD);
                }
                return ImageObjects._CHAT_DEFAULT_FILE_DOWNLOAD;
            }
        }

        public static BitmapImage CHAT_DEFAULT_FILE_UPLOAD
        {
            get
            {
                if (ImageObjects._CHAT_DEFAULT_FILE_UPLOAD == null)
                {
                    ImageObjects._CHAT_DEFAULT_FILE_UPLOAD = ImageUtility.GetBitmapImage(ImageLocation.CHAT_DEFAULT_FILE_UPLOAD);
                }
                return ImageObjects._CHAT_DEFAULT_FILE_UPLOAD;
            }
        }

        public static BitmapImage CHAT_DEFAULT_VIDEO
        {
            get
            {
                if (ImageObjects._CHAT_DEFAULT_VIDEO == null)
                {
                    ImageObjects._CHAT_DEFAULT_VIDEO = ImageUtility.GetBitmapImage(ImageLocation.CHAT_DEFAULT_VIDEO);
                }
                return ImageObjects._CHAT_DEFAULT_VIDEO;
            }
        }

        public static BitmapImage CHAT_DEFAULT_IMAGE
        {
            get
            {
                if (ImageObjects._CHAT_DEFAULT_IMAGE == null)
                {
                    ImageObjects._CHAT_DEFAULT_IMAGE = ImageUtility.GetBitmapImage(ImageLocation.CHAT_DEFAULT_IMAGE);
                }
                return ImageObjects._CHAT_DEFAULT_IMAGE;
            }
        }

        public static BitmapImage RECORDER_CLOSE
        {
            get
            {
                if (ImageObjects._RECORDER_CLOSE == null)
                {
                    ImageObjects._RECORDER_CLOSE = ImageUtility.GetBitmapImage(ImageLocation.RECORDER_CLOSE);
                }
                return ImageObjects._RECORDER_CLOSE;
            }
        }

        public static BitmapImage RECORDER_CLOSE_H
        {
            get
            {
                if (ImageObjects._RECORDER_CLOSE_H == null)
                {
                    ImageObjects._RECORDER_CLOSE_H = ImageUtility.GetBitmapImage(ImageLocation.RECORDER_CLOSE_H);
                }
                return ImageObjects._RECORDER_CLOSE_H;
            }
        }

        public static BitmapImage INCLUDE_VOICE
        {
            get
            {
                if (ImageObjects._INCLUDE_VOICE == null)
                {
                    ImageObjects._INCLUDE_VOICE = ImageUtility.GetBitmapImage(ImageLocation.INCLUDE_VOICE);
                }
                return ImageObjects._INCLUDE_VOICE;
            }
        }

        public static BitmapImage INCLUDE_VOICE_H
        {
            get
            {
                if (ImageObjects._INCLUDE_VOICE_H == null)
                {
                    ImageObjects._INCLUDE_VOICE_H = ImageUtility.GetBitmapImage(ImageLocation.INCLUDE_VOICE_H);
                }
                return ImageObjects._INCLUDE_VOICE_H;
            }
        }

        public static BitmapImage CHAT_MULTIMEDIA
        {
            get
            {
                if (ImageObjects._CHAT_MULTIMEDIA == null)
                {
                    ImageObjects._CHAT_MULTIMEDIA = ImageUtility.GetBitmapImage(ImageLocation.CHAT_MULTIMEDIA);
                }
                return ImageObjects._CHAT_MULTIMEDIA;
            }
        }

        public static BitmapImage CHAT_MULTIMEDIA_H
        {
            get
            {
                if (ImageObjects._CHAT_MULTIMEDIA_H == null)
                {
                    ImageObjects._CHAT_MULTIMEDIA_H = ImageUtility.GetBitmapImage(ImageLocation.CHAT_MULTIMEDIA_H);
                }
                return ImageObjects._CHAT_MULTIMEDIA_H;
            }
        }

        public static BitmapImage INCLUDE_VOICE_SELECTED
        {
            get
            {
                if (ImageObjects._INCLUDE_VOICE_SELECTED == null)
                {
                    ImageObjects._INCLUDE_VOICE_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.INCLUDE_VOICE_SELECTED);
                }
                return ImageObjects._INCLUDE_VOICE_SELECTED;
            }
        }

        public static BitmapImage EMOTICON_POPUP_DOWN_ARROW
        {
            get
            {
                if (ImageObjects._EMOTICON_POPUP_DOWN_ARROW == null)
                {
                    ImageObjects._EMOTICON_POPUP_DOWN_ARROW = ImageUtility.GetBitmapImage(ImageLocation.EMOTICON_POPUP_DOWN_ARROW);
                }
                return ImageObjects._EMOTICON_POPUP_DOWN_ARROW;
            }
        }

        public static BitmapImage EMOTICON_POPUP_UP_ARROW
        {
            get
            {
                if (ImageObjects._EMOTICON_POPUP_UP_ARROW == null)
                {
                    ImageObjects._EMOTICON_POPUP_UP_ARROW = ImageUtility.GetBitmapImage(ImageLocation.EMOTICON_POPUP_UP_ARROW);
                }
                return ImageObjects._EMOTICON_POPUP_UP_ARROW;
            }
        }

        public static BitmapImage EMOTICON_POPUP_NO_ARROW
        {
            get
            {
                if (ImageObjects._EMOTICON_POPUP_NO_ARROW == null)
                {
                    ImageObjects._EMOTICON_POPUP_NO_ARROW = ImageUtility.GetBitmapImage(ImageLocation.EMOTICON_POPUP_NO_ARROW);
                }
                return ImageObjects._EMOTICON_POPUP_NO_ARROW;
            }
        }

        public static BitmapImage ADD_FRIEND_SEARCH_ICON
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_SEARCH_ICON == null)
                {
                    ImageObjects._ADD_FRIEND_SEARCH_ICON = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_SEARCH);
                }
                return ImageObjects._ADD_FRIEND_SEARCH_ICON;
            }
        }

        public static BitmapImage ADD_FRIEND_SEARCH_ICON_H
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_SEARCH_ICON_H == null)
                {
                    ImageObjects._ADD_FRIEND_SEARCH_ICON_H = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_SEARCH_H);
                }
                return ImageObjects._ADD_FRIEND_SEARCH_ICON_H;
            }
        }

        public static BitmapImage ADD_FRIEND_SEARCH_ICON_SELECTED
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_SEARCH_ICON_SELECTED == null)
                {
                    ImageObjects._ADD_FRIEND_SEARCH_ICON_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_SEARCH_SELECTED);
                }
                return ImageObjects._ADD_FRIEND_SEARCH_ICON_SELECTED;
            }
        }

        public static BitmapImage ADD_FRIEND_ICON
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_ICON == null)
                {
                    ImageObjects._ADD_FRIEND_ICON = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND);
                }
                return ImageObjects._ADD_FRIEND_ICON;
            }
        }

        public static BitmapImage ADD_FRIEND_ICON_H
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_ICON_H == null)
                {
                    ImageObjects._ADD_FRIEND_ICON_H = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_H);
                }
                return ImageObjects._ADD_FRIEND_ICON_H;
            }
        }

        public static BitmapImage ADD_FRIEND_SUGGESTION_ICON
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_ADD_ICON == null)
                {
                    ImageObjects._ADD_FRIEND_ADD_ICON = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_SUGGESTION);
                }
                return ImageObjects._ADD_FRIEND_ADD_ICON;
            }
        }

        public static BitmapImage ADD_FRIEND_SUGGESTION_ICON_H
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_ADD_H_ICON == null)
                {
                    ImageObjects._ADD_FRIEND_ADD_H_ICON = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_SUGGESTION_H);
                }
                return ImageObjects._ADD_FRIEND_ADD_H_ICON;
            }
        }

        public static BitmapImage ADD_FRIEND_SUGGESTION_ICON_SELECTED
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_ADD_SELECTED_ICON == null)
                {
                    ImageObjects._ADD_FRIEND_ADD_SELECTED_ICON = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_SUGGESTION_SELECTED);
                }
                return ImageObjects._ADD_FRIEND_ADD_SELECTED_ICON;
            }
        }

        public static BitmapImage ADD_FRIEND_INVITE
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_INVITE_ICON == null)
                {
                    ImageObjects._ADD_FRIEND_INVITE_ICON = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_INVITE);
                }
                return ImageObjects._ADD_FRIEND_INVITE_ICON;
            }
        }

        public static BitmapImage ADD_FRIEND_INVITE_H
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_INVITE_ICON_H == null)
                {
                    ImageObjects._ADD_FRIEND_INVITE_ICON_H = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_INVITE_H);
                }
                return ImageObjects._ADD_FRIEND_INVITE_ICON_H;
            }
        }

        public static BitmapImage ADD_FRIEND_INVITE_SELECTED
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_INVITE_ICON_SELECTED == null)
                {
                    ImageObjects._ADD_FRIEND_INVITE_ICON_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_INVITE_SELECTED);
                }
                return ImageObjects._ADD_FRIEND_INVITE_ICON_SELECTED;
            }
        }

        public static BitmapImage ADD_FRIEND_PENDING_ICON
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_PENDING_ICON == null)
                {
                    ImageObjects._ADD_FRIEND_PENDING_ICON = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_PENDING);
                }
                return ImageObjects._ADD_FRIEND_PENDING_ICON;
            }
        }

        public static BitmapImage ADD_FRIEND_PENDING_ICON_H
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_PENDING_ICON_H == null)
                {
                    ImageObjects._ADD_FRIEND_PENDING_ICON_H = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_PENDING_H);
                }
                return ImageObjects._ADD_FRIEND_PENDING_ICON_H;
            }
        }

        public static BitmapImage ADD_FRIEND_PENDING_ICON_SELECTED
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_PENDING_ICON_SELECTED == null)
                {
                    ImageObjects._ADD_FRIEND_PENDING_ICON_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_PENDING_SELECTED);
                }
                return ImageObjects._ADD_FRIEND_PENDING_ICON_SELECTED;
            }
        }

        public static BitmapImage ADD_FRIEND_BLOCK_LIST_ICON
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_BLOCK_LIST == null)
                {
                    ImageObjects._ADD_FRIEND_BLOCK_LIST = ImageUtility.GetBitmapImage(ImageLocation._ADD_FRIEND_BLOCK_LIST);
                }
                return ImageObjects._ADD_FRIEND_BLOCK_LIST;
            }
        }

        public static BitmapImage ADD_FRIEND_BLOCK_LIST_ICON_H
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_BLOCK_LIST_H == null)
                {
                    ImageObjects._ADD_FRIEND_BLOCK_LIST_H = ImageUtility.GetBitmapImage(ImageLocation._ADD_FRIEND_BLOCK_LIST_H);
                }
                return ImageObjects._ADD_FRIEND_BLOCK_LIST_H;
            }
        }

        public static BitmapImage ADD_FRIEND_BLOCK_LIST_SELECTED
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_BLOCK_LIST_SELECTED == null)
                {
                    ImageObjects._ADD_FRIEND_BLOCK_LIST_SELECTED = ImageUtility.GetBitmapImage(ImageLocation._ADD_FRIEND_BLOCK_LIST_SELECTED);
                }
                return ImageObjects._ADD_FRIEND_BLOCK_LIST_SELECTED;
            }
        }

        public static BitmapImage ADD_FRIEND_INCOMING_ICON
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_INCOMING_ICON == null)
                {
                    ImageObjects._ADD_FRIEND_INCOMING_ICON = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_ADD_FRIEND_INCOMING);
                }
                return ImageObjects._ADD_FRIEND_INCOMING_ICON;
            }
        }

        public static BitmapImage ADD_FRIEND_INCOMING_ICON_H
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_INCOMING_ICON_H == null)
                {
                    ImageObjects._ADD_FRIEND_INCOMING_ICON_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_ADD_FRIEND_INCOMING_H);
                }
                return ImageObjects._ADD_FRIEND_INCOMING_ICON_H;
            }
        }

        public static BitmapImage ADD_FRIEND_OUTGOING_ICON
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_OUTGOING_ICON == null)
                {
                    ImageObjects._ADD_FRIEND_OUTGOING_ICON = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_ADD_FRIEND_OUTGOING);
                }
                return ImageObjects._ADD_FRIEND_OUTGOING_ICON;
            }
        }

        public static BitmapImage ADD_FRIEND_OUTGOING_ICON_H
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_OUTGOING_ICON_H == null)
                {
                    ImageObjects._ADD_FRIEND_OUTGOING_ICON_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_ADD_FRIEND_OUTGOING_H);
                }
                return ImageObjects._ADD_FRIEND_OUTGOING_ICON_H;
            }
        }

        public static BitmapImage FRIEND_ACCESS_ICON_H
        {
            get
            {
                if (ImageObjects._FRIEND_ACCESS_ICON_H == null)
                {
                    ImageObjects._FRIEND_ACCESS_ICON_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_FRIEND_ACCESS_H);
                }
                return ImageObjects._FRIEND_ACCESS_ICON_H;
            }
        }

        public static BitmapImage LEFT_ARROW
        {
            get
            {
                if (ImageObjects._LEFT_ARROW == null)
                {
                    ImageObjects._LEFT_ARROW = ImageUtility.GetBitmapImage(ImageLocation.LEFT_ARROW);
                }
                return ImageObjects._LEFT_ARROW;
            }
        }

        public static BitmapImage LEFT_ARROW_H
        {
            get
            {
                if (ImageObjects._LEFT_ARROW_H == null)
                {
                    ImageObjects._LEFT_ARROW_H = ImageUtility.GetBitmapImage(ImageLocation.LEFT_ARROW_H);
                }
                return ImageObjects._LEFT_ARROW_H;
            }
        }

        public static BitmapImage RIGHT_ARROW
        {
            get
            {
                if (ImageObjects._RIGHT_ARROW == null)
                {
                    ImageObjects._RIGHT_ARROW = ImageUtility.GetBitmapImage(ImageLocation.RIGHT_ARROW);
                }
                return ImageObjects._RIGHT_ARROW;
            }
        }

        public static BitmapImage RIGHT_ARROW_H
        {
            get
            {
                if (ImageObjects._RIGHT_ARROW_H == null)
                {
                    ImageObjects._RIGHT_ARROW_H = ImageUtility.GetBitmapImage(ImageLocation.RIGHT_ARROW_H);
                }
                return ImageObjects._RIGHT_ARROW_H;
            }
        }

        public static BitmapImage RIGHT_MINI_ARROW
        {
            get
            {
                if (ImageObjects._RIGHT_MINI_ARROW == null)
                {
                    ImageObjects._RIGHT_MINI_ARROW = ImageUtility.GetBitmapImage(ImageLocation.RIGHT_MINI_ARROW);
                }
                return ImageObjects._RIGHT_MINI_ARROW;
            }
        }

        public static BitmapImage RIGHT_MINI_ARROW_H
        {
            get
            {
                if (ImageObjects._RIGHT_MINI_ARROW_H == null)
                {
                    ImageObjects._RIGHT_MINI_ARROW_H = ImageUtility.GetBitmapImage(ImageLocation.RIGHT_MINI_ARROW_H);
                }
                return ImageObjects._RIGHT_MINI_ARROW_H;
            }
        }

        public static BitmapImage CLOSE
        {
            get
            {
                if (ImageObjects._CLOSE == null)
                {
                    ImageObjects._CLOSE = ImageUtility.GetBitmapImage(ImageLocation.CLOSE);
                }
                return ImageObjects._CLOSE;
            }
        }

        public static BitmapImage CLOSE_H
        {
            get
            {
                if (ImageObjects._CLOSE_H == null)
                {
                    ImageObjects._CLOSE_H = ImageUtility.GetBitmapImage(ImageLocation.CLOSE_H);
                }
                return ImageObjects._CLOSE_H;
            }
        }

        public static BitmapImage CLOSE_TRANSPARENT
        {
            get
            {
                if (ImageObjects._CLOSE_TRANSPARENT == null)
                {
                    ImageObjects._CLOSE_TRANSPARENT = ImageUtility.GetBitmapImage(ImageLocation.CLOSE_TRANSPARENT);
                }
                return ImageObjects._CLOSE_TRANSPARENT;
            }
        }

        public static BitmapImage STICKER_REMOVE
        {
            get
            {
                if (ImageObjects._STICKER_REMOVE == null)
                {
                    ImageObjects._STICKER_REMOVE = ImageUtility.GetBitmapImage(ImageLocation.STICKER_REMOVE);
                }
                return ImageObjects._STICKER_REMOVE;
            }
        }

        public static BitmapImage STICKER_REMOVE_H
        {
            get
            {
                if (ImageObjects._STICKER_REMOVE_H == null)
                {
                    ImageObjects._STICKER_REMOVE_H = ImageUtility.GetBitmapImage(ImageLocation.STICKER_REMOVE_H);
                }
                return ImageObjects._STICKER_REMOVE_H;
            }
        }


        public static BitmapImage ADD_FROM_DIRECTORY
        {
            get
            {
                if (ImageObjects._ADD_FROM_DIRECTORY == null)
                {
                    ImageObjects._ADD_FROM_DIRECTORY = ImageUtility.GetBitmapImage(ImageLocation.ADD_FROM_DIRECTORY);
                }
                return ImageObjects._ADD_FROM_DIRECTORY;
            }
        }

        public static BitmapImage ADD_FROM_DIRECTORY_H
        {
            get
            {
                if (ImageObjects._ADD_FROM_DIRECTORY_H == null)
                {
                    ImageObjects._ADD_FROM_DIRECTORY_H = ImageUtility.GetBitmapImage(ImageLocation.ADD_FROM_DIRECTORY_H);
                }
                return ImageObjects._ADD_FROM_DIRECTORY_H;
            }
        }

        public static BitmapImage ADD_FROM_WEBCAM
        {
            get
            {
                if (ImageObjects._ADD_FROM_WEBCAM == null)
                {
                    ImageObjects._ADD_FROM_WEBCAM = ImageUtility.GetBitmapImage(ImageLocation.ADD_FROM_WEBCAM);
                }
                return ImageObjects._ADD_FROM_WEBCAM;
            }
        }

        public static BitmapImage ADD_FROM_WEBCAM_H
        {
            get
            {
                if (ImageObjects._ADD_FROM_WEBCAM_H == null)
                {
                    ImageObjects._ADD_FROM_WEBCAM_H = ImageUtility.GetBitmapImage(ImageLocation.ADD_FROM_WEBCAM_H);
                }
                return ImageObjects._ADD_FROM_WEBCAM_H;
            }
        }

        public static BitmapImage SEND_CHAT_IMAGE
        {
            get
            {
                if (ImageObjects._SEND_CHAT_IMAGE == null)
                {
                    ImageObjects._SEND_CHAT_IMAGE = ImageUtility.GetBitmapImage(ImageLocation.SEND_CHAT_IMAGE);
                }
                return ImageObjects._SEND_CHAT_IMAGE;
            }
        }

        public static BitmapImage SEND_CHAT_IMAGE_H
        {
            get
            {
                if (ImageObjects._SEND_CHAT_IMAGE_H == null)
                {
                    ImageObjects._SEND_CHAT_IMAGE_H = ImageUtility.GetBitmapImage(ImageLocation.SEND_CHAT_IMAGE_H);
                }
                return ImageObjects._SEND_CHAT_IMAGE_H;
            }

        }

        public static BitmapImage SEND_CHAT_IMAGE_SELECTED
        {
            get
            {
                if (ImageObjects._SEND_CHAT_IMAGE_SELECTED == null)
                {
                    ImageObjects._SEND_CHAT_IMAGE_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.SEND_CHAT_IMAGE_SELECTED);
                }
                return ImageObjects._SEND_CHAT_IMAGE_SELECTED;
            }
        }

        public static BitmapImage DEFAULT_ID_ICON
        {
            get
            {
                if (ImageObjects._DEFAULT_ID_ICON == null)
                {
                    ImageObjects._DEFAULT_ID_ICON = ImageUtility.GetBitmapImage(ImageLocation.DEFAULT_ID_ICON);
                }
                return ImageObjects._DEFAULT_ID_ICON;
            }
        }

        //public static BitmapImage FRIEND_BLOCK_ICON
        //{
        //    get
        //    {
        //        if (ImageObjects._FRIEND_BLOCK_ICON == null)
        //        {
        //            ImageObjects._FRIEND_BLOCK_ICON = ImageUtility.GetBitmapImage(ImageLocation.FRIEND_BLOCK_ICON);
        //        }
        //        return ImageObjects._FRIEND_BLOCK_ICON;
        //    }
        //}

        public static BitmapImage ADD_FRIEND_ICON_CONTACT_LIST
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_ICON_CONTACT_LIST == null)
                {
                    ImageObjects._ADD_FRIEND_ICON_CONTACT_LIST = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_ICON_CONTACT_LIST);
                }
                return ImageObjects._ADD_FRIEND_ICON_CONTACT_LIST;
            }
        }

        public static BitmapImage FRIEND_ICON_CONTACT_LIST
        {
            get
            {
                if (ImageObjects._FRIEND_ICON_CONTACT_LIST == null)
                {
                    ImageObjects._FRIEND_ICON_CONTACT_LIST = ImageUtility.GetBitmapImage(ImageLocation.FRIEND_ICON_CONTACT_LIST);
                }
                return ImageObjects._FRIEND_ICON_CONTACT_LIST;
            }
        }

        public static BitmapImage PENDING_IN_FRIEND_ICON_CONTACT_LIST
        {
            get
            {
                if (ImageObjects._PENDING_IN_FRIEND_ICON_CONTACT_LIST == null)
                {
                    ImageObjects._PENDING_IN_FRIEND_ICON_CONTACT_LIST = ImageUtility.GetBitmapImage(ImageLocation.PENDING_IN_FRIEND_ICON_CONTACT_LIST);
                }
                return ImageObjects._PENDING_IN_FRIEND_ICON_CONTACT_LIST;
            }
        }

        public static BitmapImage PENDING_OUT_FRIEND_ICON_CONTACT_LIST
        {
            get
            {
                if (ImageObjects._PENDING_OUT_FRIEND_ICON_CONTACT_LIST == null)
                {
                    ImageObjects._PENDING_OUT_FRIEND_ICON_CONTACT_LIST = ImageUtility.GetBitmapImage(ImageLocation.PENDING_OUT_FRIEND_ICON_CONTACT_LIST);
                }
                return ImageObjects._PENDING_OUT_FRIEND_ICON_CONTACT_LIST;
            }
        }

        public static BitmapImage ADD_ICON_CONTACT_LIST
        {
            get
            {
                if (ImageObjects._ADD_ICON_CONTACT_LIST == null)
                {
                    ImageObjects._ADD_ICON_CONTACT_LIST = ImageUtility.GetBitmapImage(ImageLocation.ADD_ICON_CONTACT_LIST);
                }
                return ImageObjects._ADD_ICON_CONTACT_LIST;
            }
        }

        public static BitmapImage ADD_ICON_CONTACT_LIST_H
        {
            get
            {
                if (ImageObjects._ADD_ICON_CONTACT_LIST_H == null)
                {
                    ImageObjects._ADD_ICON_CONTACT_LIST_H = ImageUtility.GetBitmapImage(ImageLocation.ADD_ICON_CONTACT_LIST_H);
                }
                return ImageObjects._ADD_ICON_CONTACT_LIST_H;
            }
        }

        #region CONTEXT MENU
        public static BitmapImage ADD_FRIEND_ICON_CM
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_ICON_CM == null)
                {
                    ImageObjects._ADD_FRIEND_ICON_CM = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_ICON_CM);
                }
                return ImageObjects._ADD_FRIEND_ICON_CM;
            }
        }

        public static BitmapImage ADD_FRIEND_ICON_CM_H
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_ICON_CM_H == null)
                {
                    ImageObjects._ADD_FRIEND_ICON_CM_H = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_ICON_CM_H);
                }
                return ImageObjects._ADD_FRIEND_ICON_CM_H;
            }
        }

        public static BitmapImage BLOCK_USER_ICON_CM
        {
            get
            {
                if (ImageObjects._BLOCK_USER_ICON_CM == null)
                {
                    ImageObjects._BLOCK_USER_ICON_CM = ImageUtility.GetBitmapImage(ImageLocation.BLOCK_USER_ICON_CM);
                }
                return ImageObjects._BLOCK_USER_ICON_CM;
            }
        }

        public static BitmapImage BLOCK_USER_ICON_CM_H
        {
            get
            {
                if (ImageObjects._BLOCK_USER_ICON_CM_H == null)
                {
                    ImageObjects._BLOCK_USER_ICON_CM_H = ImageUtility.GetBitmapImage(ImageLocation.BLOCK_USER_ICON_CM_H);
                }
                return ImageObjects._BLOCK_USER_ICON_CM_H;
            }
        }

        public static BitmapImage CANCEL_REQUEST_ICON_CM
        {
            get
            {
                if (ImageObjects._CANCEL_REQUEST_ICON_CM == null)
                {
                    ImageObjects._CANCEL_REQUEST_ICON_CM = ImageUtility.GetBitmapImage(ImageLocation.CANCEL_REQUEST_ICON_CM);
                }
                return ImageObjects._CANCEL_REQUEST_ICON_CM;
            }
        }

        public static BitmapImage CANCEL_REQUEST_ICON_CM_H
        {
            get
            {
                if (ImageObjects._CANCEL_REQUEST_ICON_CM_H == null)
                {
                    ImageObjects._CANCEL_REQUEST_ICON_CM_H = ImageUtility.GetBitmapImage(ImageLocation.CANCEL_REQUEST_ICON_CM_H);
                }
                return ImageObjects._CANCEL_REQUEST_ICON_CM_H;
            }
        }

        public static BitmapImage CIRCLE_SMALL_ICON_CM
        {
            get
            {
                if (ImageObjects._CIRCLE_SMALL_ICON_CM == null)
                {
                    ImageObjects._CIRCLE_SMALL_ICON_CM = ImageUtility.GetBitmapImage(ImageLocation.CIRCLE_SMALL_ICON_CM);
                }
                return ImageObjects._CIRCLE_SMALL_ICON_CM;
            }
        }

        public static BitmapImage CIRCLE_SMALL_ICON_CM_H
        {
            get
            {
                if (ImageObjects._CIRCLE_SMALL_ICON_CM_H == null)
                {
                    ImageObjects._CIRCLE_SMALL_ICON_CM_H = ImageUtility.GetBitmapImage(ImageLocation.CIRCLE_SMALL_ICON_CM_H);
                }
                return ImageObjects._CIRCLE_SMALL_ICON_CM_H;
            }
        }

        public static BitmapImage FRIEND_ICON_CM
        {
            get
            {
                if (ImageObjects._FRIEND_ICON_CM == null)
                {
                    ImageObjects._FRIEND_ICON_CM = ImageUtility.GetBitmapImage(ImageLocation.FRIEND_ICON_CM);
                }
                return ImageObjects._FRIEND_ICON_CM;
            }
        }

        public static BitmapImage FRIEND_ICON_CM_H
        {
            get
            {
                if (ImageObjects._FRIEND_ICON_CM_H == null)
                {
                    ImageObjects._FRIEND_ICON_CM_H = ImageUtility.GetBitmapImage(ImageLocation.FRIEND_ICON_CM_H);
                }
                return ImageObjects._FRIEND_ICON_CM_H;
            }
        }

        public static BitmapImage REJECT_ICON_CM
        {
            get
            {
                if (ImageObjects._REJECT_ICON_CM == null)
                {
                    ImageObjects._REJECT_ICON_CM = ImageUtility.GetBitmapImage(ImageLocation.REJECT_ICON_CM);
                }
                return ImageObjects._REJECT_ICON_CM;
            }
        }

        public static BitmapImage REJECT_ICON_CM_H
        {
            get
            {
                if (ImageObjects._REJECT_ICON_CM_H == null)
                {
                    ImageObjects._REJECT_ICON_CM_H = ImageUtility.GetBitmapImage(ImageLocation.REJECT_ICON_CM_H);
                }
                return ImageObjects._REJECT_ICON_CM_H;
            }
        }

        public static BitmapImage EDIT_INFO_CM
        {
            get
            {
                if (ImageObjects._EDIT_INFO_CM == null)
                {
                    ImageObjects._EDIT_INFO_CM = ImageUtility.GetBitmapImage(ImageLocation.EDIT_INFO_CM);
                }
                return ImageObjects._EDIT_INFO_CM;
            }
        }

        public static BitmapImage EDIT_INFO_CM_H
        {
            get
            {
                if (ImageObjects._EDIT_INFO_CM_H == null)
                {
                    ImageObjects._EDIT_INFO_CM_H = ImageUtility.GetBitmapImage(ImageLocation.EDIT_INFO_CM_H);
                }
                return ImageObjects._EDIT_INFO_CM_H;
            }
        }

        public static BitmapImage GROUP_DELETE_CM
        {
            get
            {
                if (ImageObjects._GROUP_DELETE_CM == null)
                {
                    ImageObjects._GROUP_DELETE_CM = ImageUtility.GetBitmapImage(ImageLocation.GROUP_DELETE_CM);
                }
                return ImageObjects._GROUP_DELETE_CM;
            }
        }

        public static BitmapImage GROUP_DELETE_CM_H
        {
            get
            {
                if (ImageObjects._GROUP_DELETE_CM_H == null)
                {
                    ImageObjects._GROUP_DELETE_CM_H = ImageUtility.GetBitmapImage(ImageLocation.GROUP_DELETE_CM_H);
                }
                return ImageObjects._GROUP_DELETE_CM_H;
            }
        }

        public static BitmapImage GROUP_LEAVE_CM
        {
            get
            {
                if (ImageObjects._GROUP_LEAVE_CM == null)
                {
                    ImageObjects._GROUP_LEAVE_CM = ImageUtility.GetBitmapImage(ImageLocation.GROUP_LEAVE_CM);
                }
                return ImageObjects._GROUP_LEAVE_CM;
            }
        }

        public static BitmapImage GROUP_LEAVE_CM_H
        {
            get
            {
                if (ImageObjects._GROUP_LEAVE_CM_H == null)
                {
                    ImageObjects._GROUP_LEAVE_CM_H = ImageUtility.GetBitmapImage(ImageLocation.GROUP_LEAVE_CM_H);
                }
                return ImageObjects._GROUP_LEAVE_CM_H;
            }
        }
        #endregion

        public static BitmapImage INCOMING_FRIEND_ICON_CONTACT_LIST
        {
            get
            {
                if (ImageObjects._INCOMING_FRIEND_ICON_CONTACT_LIST == null)
                {
                    ImageObjects._INCOMING_FRIEND_ICON_CONTACT_LIST = ImageUtility.GetBitmapImage(ImageLocation.INCOMING_FRIEND_ICON_CONTACT_LIST);
                }
                return ImageObjects._INCOMING_FRIEND_ICON_CONTACT_LIST;
            }
        }

        public static BitmapImage INCOMING_FRIEND_ICON_CONTACT_LIST_H
        {
            get
            {
                if (ImageObjects._INCOMING_FRIEND_ICON_CONTACT_LIST_H == null)
                {
                    ImageObjects._INCOMING_FRIEND_ICON_CONTACT_LIST_H = ImageUtility.GetBitmapImage(ImageLocation.INCOMING_FRIEND_ICON_CONTACT_LIST_H);
                }
                return ImageObjects._INCOMING_FRIEND_ICON_CONTACT_LIST_H;
            }
        }

        public static BitmapImage OUTGOING_FRIEND_ICON_CONTACT_LIST
        {
            get
            {
                if (ImageObjects._OUTGOING_FRIEND_ICON_CONTACT_LIST == null)
                {
                    ImageObjects._OUTGOING_FRIEND_ICON_CONTACT_LIST = ImageUtility.GetBitmapImage(ImageLocation.OUTGOING_FRIEND_ICON_CONTACT_LIST);
                }
                return ImageObjects._OUTGOING_FRIEND_ICON_CONTACT_LIST;
            }
        }

        public static BitmapImage OUTGOING_FRIEND_ICON_CONTACT_LIST_H
        {
            get
            {
                if (ImageObjects._OUTGOING_FRIEND_ICON_CONTACT_LIST_H == null)
                {
                    ImageObjects._OUTGOING_FRIEND_ICON_CONTACT_LIST_H = ImageUtility.GetBitmapImage(ImageLocation.OUTGOING_FRIEND_ICON_CONTACT_LIST_H);
                }
                return ImageObjects._OUTGOING_FRIEND_ICON_CONTACT_LIST_H;
            }
        }

        public static BitmapImage FRIEND_CONTACT_LIST
        {
            get
            {
                if (ImageObjects._FRIEND_CONTACT_LIST == null)
                {
                    ImageObjects._FRIEND_CONTACT_LIST = ImageUtility.GetBitmapImage(ImageLocation.FRIEND_CONTACT_LIST);
                }
                return ImageObjects._FRIEND_CONTACT_LIST;
            }
        }

        public static BitmapImage FRIEND_CONTACT_LIST_H
        {
            get
            {
                if (ImageObjects._FRIEND_CONTACT_LIST_H == null)
                {
                    ImageObjects._FRIEND_CONTACT_LIST_H = ImageUtility.GetBitmapImage(ImageLocation.FRIEND_CONTACT_LIST_H);
                }
                return ImageObjects._FRIEND_CONTACT_LIST_H;
            }
        }

        public static BitmapImage FRIEND_CONTACT_ID
        {
            get
            {
                if (ImageObjects._FRIEND_CONTACT_ID == null)
                {
                    ImageObjects._FRIEND_CONTACT_ID = ImageUtility.GetBitmapImage(ImageLocation.FRIEND_CONTACT_ID);
                }
                return ImageObjects._FRIEND_CONTACT_ID;
            }
        }

        public static BitmapImage REJECT_ICON_CONTACT_LIST
        {
            get
            {
                if (ImageObjects._REJECT_ICON_CONTACT_LIST == null)
                {
                    ImageObjects._REJECT_ICON_CONTACT_LIST = ImageUtility.GetBitmapImage(ImageLocation.REJECT_ICON_CONTACT_LIST);
                }
                return ImageObjects._REJECT_ICON_CONTACT_LIST;
            }
        }

        public static BitmapImage REJECT_ICON_CONTACT_LIST_H
        {
            get
            {
                if (ImageObjects._REJECT_ICON_CONTACT_LIST_H == null)
                {
                    ImageObjects._REJECT_ICON_CONTACT_LIST_H = ImageUtility.GetBitmapImage(ImageLocation.REJECT_ICON_CONTACT_LIST_H);
                }
                return ImageObjects._REJECT_ICON_CONTACT_LIST_H;
            }
        }

        public static BitmapImage CANCEL_REQUEST_ICON_CONTACT_LIST
        {
            get
            {
                if (ImageObjects._CANCEL_REQUEST_ICON_CONTACT_LIST == null)
                {
                    ImageObjects._CANCEL_REQUEST_ICON_CONTACT_LIST = ImageUtility.GetBitmapImage(ImageLocation.CANCEL_REQUEST_ICON_CONTACT_LIST);
                }
                return ImageObjects._CANCEL_REQUEST_ICON_CONTACT_LIST;
            }
        }

        public static BitmapImage CANCEL_REQUEST_ICON_CONTACT_LIST_H
        {
            get
            {
                if (ImageObjects._CANCEL_REQUEST_ICON_CONTACT_LIST_H == null)
                {
                    ImageObjects._CANCEL_REQUEST_ICON_CONTACT_LIST_H = ImageUtility.GetBitmapImage(ImageLocation.CANCEL_REQUEST_ICON_CONTACT_LIST_H);
                }
                return ImageObjects._CANCEL_REQUEST_ICON_CONTACT_LIST_H;
            }
        }

        public static BitmapImage REMOVE_ICON_PEOPLE_UMAY_KNOW_LIST
        {
            get
            {
                if (ImageObjects._REMOVE_ICON_PEOPLE_UMAY_KNOW_LIST == null)
                {
                    ImageObjects._REMOVE_ICON_PEOPLE_UMAY_KNOW_LIST = ImageUtility.GetBitmapImage(ImageLocation.REMOVE_ICON_PUMAYKNOW_LIST);
                }
                return ImageObjects._REMOVE_ICON_PEOPLE_UMAY_KNOW_LIST;
            }
        }

        public static BitmapImage REMOVE_ICON_PEOPLE_UMAY_KNOW_LIST_H
        {
            get
            {
                if (ImageObjects._REMOVE_ICON_PEOPLE_UMAY_KNOW_LIST_H == null)
                {
                    ImageObjects._REMOVE_ICON_PEOPLE_UMAY_KNOW_LIST_H = ImageUtility.GetBitmapImage(ImageLocation.REMOVE_ICON_PUMAYKNOW_LIST_H);
                }
                return ImageObjects._REMOVE_ICON_PEOPLE_UMAY_KNOW_LIST_H;
            }
        }

        public static BitmapImage CHAT_MORE_OPTION_BG
        {
            get
            {
                if (ImageObjects._CHAT_MORE_OPTION_BG == null)
                {
                    ImageObjects._CHAT_MORE_OPTION_BG = ImageUtility.GetBitmapImage(ImageLocation.CHAT_MORE_OPTION_BG);
                }
                return ImageObjects._CHAT_MORE_OPTION_BG;
            }
        }

        public static BitmapImage SEQUENCE_NUMBER
        {
            get
            {
                if (ImageObjects._SEQUENCE_NUMBER == null)
                {
                    ImageObjects._SEQUENCE_NUMBER = ImageUtility.GetBitmapImage(ImageLocation.SEQUENCE_NUMBER);
                }
                return ImageObjects._SEQUENCE_NUMBER;
            }
        }

        public static BitmapImage NEW_STATUS_OPTIONS
        {
            get
            {
                if (ImageObjects._NEW_STATUS_OPTIONS == null)
                {
                    ImageObjects._NEW_STATUS_OPTIONS = ImageUtility.GetBitmapImage(ImageLocation.NEW_STATUS_OPTIONS);
                }
                return ImageObjects._NEW_STATUS_OPTIONS;
            }
        }

        public static BitmapImage NEW_STATUS_OPTIONS_H
        {
            get
            {
                if (ImageObjects._NEW_STATUS_OPTIONS_H == null)
                {
                    ImageObjects._NEW_STATUS_OPTIONS_H = ImageUtility.GetBitmapImage(ImageLocation.NEW_STATUS_OPTIONS_H);
                }
                return ImageObjects._NEW_STATUS_OPTIONS_H;
            }
        }

        public static BitmapImage POPUP_UPLOAD_PHOTO
        {
            get
            {
                if (ImageObjects._POPUP_UPLOAD_PHOTO == null)
                {
                    ImageObjects._POPUP_UPLOAD_PHOTO = ImageUtility.GetBitmapImage(ImageLocation.POPUP_UPLOAD_PHOTO);
                }
                return ImageObjects._POPUP_UPLOAD_PHOTO;
            }
        }

        public static BitmapImage POPUP_UPLOAD_VIDEO
        {
            get
            {
                if (ImageObjects._POPUP_UPLOAD_VIDEO == null)
                {
                    ImageObjects._POPUP_UPLOAD_VIDEO = ImageUtility.GetBitmapImage(ImageLocation.POPUP_UPLOAD_VIDEO);
                }
                return ImageObjects._POPUP_UPLOAD_VIDEO;
            }
        }

        public static BitmapImage BUTTON_TAKE_PHOTOS
        {
            get
            {
                if (ImageObjects._BUTTON_TAKE_PHOTOS == null)
                {
                    ImageObjects._BUTTON_TAKE_PHOTOS = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_TAKE_PHOTOS);
                }
                return ImageObjects._BUTTON_TAKE_PHOTOS;
            }
        }

        public static BitmapImage BUTTON_UPLOAD_PHOTO
        {
            get
            {
                if (ImageObjects._BUTTON_UPLOAD_PHOTO == null)
                {
                    ImageObjects._BUTTON_UPLOAD_PHOTO = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_UPLOAD_PHOTO);
                }
                return ImageObjects._BUTTON_UPLOAD_PHOTO;
            }
        }

        public static BitmapImage BUTTON_ALBUM_PHOTO
        {
            get
            {
                if (ImageObjects._BUTTON_ALBUM_PHOTO == null)
                {
                    ImageObjects._BUTTON_ALBUM_PHOTO = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_ALBUM_PHOTO);
                }
                return ImageObjects._BUTTON_ALBUM_PHOTO;
            }
        }

        public static BitmapImage UNKNOWN_GROUP_IMAGE
        {
            get
            {
                if (ImageObjects._UNKNOWN_GROUP_IMAGE == null)
                {
                    ImageObjects._UNKNOWN_GROUP_IMAGE = ImageUtility.GetBitmapImage(ImageLocation.UNKNOWN_GROUP_IMAGE);
                }
                return ImageObjects._UNKNOWN_GROUP_IMAGE;
            }
        }

        public static BitmapImage UNKNOWN_GROUP_LARGE_IMAGE
        {
            get
            {
                if (ImageObjects._UNKNOWN_GROUP_LARGE_IMAGE == null)
                {
                    ImageObjects._UNKNOWN_GROUP_LARGE_IMAGE = ImageUtility.GetBitmapImage(ImageLocation.UNKNOWN_GROUP_LARGE_IMAGE);
                }
                return ImageObjects._UNKNOWN_GROUP_LARGE_IMAGE;
            }
        }

        public static BitmapImage UNKNOWN_ROOM_IMAGE
        {
            get
            {
                if (ImageObjects._UNKNOWN_ROOM_IMAGE == null)
                {
                    ImageObjects._UNKNOWN_ROOM_IMAGE = ImageUtility.GetBitmapImage(ImageLocation.UNKNOWN_ROOM_IMAGE);
                }
                return ImageObjects._UNKNOWN_ROOM_IMAGE;
            }
        }

        public static BitmapImage UNKNOWN_IMAGE_FOR_MUTUAL_FRIEND
        {
            get
            {
                if (ImageObjects._UNKNOWN_IMAGE_FOR_MUTUAL_FRIEND == null)
                {
                    ImageObjects._UNKNOWN_IMAGE_FOR_MUTUAL_FRIEND = ImageUtility.GetBitmapImage(ImageLocation.UNKNOWN_IMAGE_FOR_MUTUAL_FRIEND);
                }
                return ImageObjects._UNKNOWN_IMAGE_FOR_MUTUAL_FRIEND;
            }
        }

        public static BitmapImage ADD_GROUP_MEMBER
        {
            get
            {
                if (ImageObjects._ADD_GROUP_MEMBER == null)
                {
                    ImageObjects._ADD_GROUP_MEMBER = ImageUtility.GetBitmapImage(ImageLocation.ADD_GROUP_MEMBER);
                }
                return ImageObjects._ADD_GROUP_MEMBER;
            }
        }

        public static BitmapImage ADD_GROUP_MEMBER_H
        {
            get
            {
                if (ImageObjects._ADD_GROUP_MEMBER_H == null)
                {
                    ImageObjects._ADD_GROUP_MEMBER_H = ImageUtility.GetBitmapImage(ImageLocation.ADD_GROUP_MEMBER_H);
                }
                return ImageObjects._ADD_GROUP_MEMBER_H;
            }
        }

        public static BitmapImage GROUP_SETTINGS
        {
            get
            {
                if (ImageObjects._GROUP_SETTINGS == null)
                {
                    ImageObjects._GROUP_SETTINGS = ImageUtility.GetBitmapImage(ImageLocation.GROUP_SETTINGS);
                }
                return ImageObjects._GROUP_SETTINGS;
            }
        }

        public static BitmapImage GROUP_SETTINGS_H
        {
            get
            {
                if (ImageObjects._GROUP_SETTINGS_H == null)
                {
                    ImageObjects._GROUP_SETTINGS_H = ImageUtility.GetBitmapImage(ImageLocation.GROUP_SETTINGS_H);
                }
                return ImageObjects._GROUP_SETTINGS_H;
            }
        }

        public static BitmapImage VIEW_PREVIOUS_COMMENT_ICON
        {
            get
            {
                if (ImageObjects._VIEW_PREVIOUS_COMMENT_ICON == null)
                {
                    ImageObjects._VIEW_PREVIOUS_COMMENT_ICON = ImageUtility.GetBitmapImage(ImageLocation.VIEW_PREVIOUS_COMMENT_ICON);
                }
                return ImageObjects._VIEW_PREVIOUS_COMMENT_ICON;
            }
        }

        public static BitmapImage NEW_STATUS_IMAGE_UPLOAD_CROSS
        {
            get
            {
                if (ImageObjects._NEW_STATUS_IMAGE_UPLOAD_CROSS == null)
                {
                    ImageObjects._NEW_STATUS_IMAGE_UPLOAD_CROSS = ImageUtility.GetBitmapImage(ImageLocation.NEW_STATUS_IMAGE_UPLOAD_CROSS);
                }
                return ImageObjects._NEW_STATUS_IMAGE_UPLOAD_CROSS;
            }
        }

        public static BitmapImage NEW_STATUS_IMAGE_UPLOAD_CROSS_H
        {
            get
            {
                if (ImageObjects._NEW_STATUS_IMAGE_UPLOAD_CROSS_H == null)
                {
                    ImageObjects._NEW_STATUS_IMAGE_UPLOAD_CROSS_H = ImageUtility.GetBitmapImage(ImageLocation.NEW_STATUS_IMAGE_UPLOAD_CROSS_H);
                }
                return ImageObjects._NEW_STATUS_IMAGE_UPLOAD_CROSS_H;
            }
        }
        public static BitmapImage POPUP_SHARE
        {
            get
            {
                if (ImageObjects._POPUP_SHARE == null)
                {
                    ImageObjects._POPUP_SHARE = ImageUtility.GetBitmapImage(ImageLocation.POPUP_SHARE);
                }
                return ImageObjects._POPUP_SHARE;
            }
        }
        public static BitmapImage BOOK_ARROW
        {
            get
            {
                if (ImageObjects._BOOK_ARROW == null)
                {
                    ImageObjects._BOOK_ARROW = ImageUtility.GetBitmapImage(ImageLocation.BOOK_ARROW);
                }
                return ImageObjects._BOOK_ARROW;
            }
        }
        public static BitmapImage EDIT_INFO
        {
            get
            {
                if (ImageObjects._EDIT_INFO == null)
                {
                    ImageObjects._EDIT_INFO = ImageUtility.GetBitmapImage(ImageLocation.EDIT_INFO);
                }
                return ImageObjects._EDIT_INFO;
            }
        }

        public static BitmapImage NEW_STATUS_EMOTICONS
        {
            get
            {
                if (ImageObjects._NEW_STATUS_EMOTICONS == null)
                {
                    ImageObjects._NEW_STATUS_EMOTICONS = ImageUtility.GetBitmapImage(ImageLocation.NEW_STATUS_EMOTICONS);
                }
                return ImageObjects._NEW_STATUS_EMOTICONS;
            }
        }

        public static BitmapImage NEW_STATUS_EMOTICONS_H
        {
            get
            {
                if (ImageObjects._NEW_STATUS_EMOTICONS_H == null)
                {
                    ImageObjects._NEW_STATUS_EMOTICONS_H = ImageUtility.GetBitmapImage(ImageLocation.NEW_STATUS_EMOTICONS_H);
                }
                return ImageObjects._NEW_STATUS_EMOTICONS_H;
            }
        }

        public static BitmapImage VIDEO_SETTINGS
        {
            get
            {
                if (ImageObjects._VIDEO_SETTINGS == null)
                {
                    ImageObjects._VIDEO_SETTINGS = ImageUtility.GetBitmapImage(ImageLocation.VIDEO_SETTINGS);
                }
                return ImageObjects._VIDEO_SETTINGS;
            }
        }

        public static BitmapImage VIDEO_SETTINGS_H
        {
            get
            {
                if (ImageObjects._VIDEO_SETTINGS_H == null)
                {
                    ImageObjects._VIDEO_SETTINGS_H = ImageUtility.GetBitmapImage(ImageLocation.VIDEO_SETTINGS_H);
                }
                return ImageObjects._VIDEO_SETTINGS_H;
            }
        }

        public static BitmapImage CHAT_MEDIA_PLAY
        {
            get
            {
                if (ImageObjects._CHAT_MEDIA_PLAY == null)
                {
                    ImageObjects._CHAT_MEDIA_PLAY = ImageUtility.GetBitmapImage(ImageLocation.CHAT_MEDIA_PLAY);
                }
                return ImageObjects._CHAT_MEDIA_PLAY;
            }
        }

        public static BitmapImage CHAT_MEDIA_PLAY_H
        {
            get
            {
                if (ImageObjects._CHAT_MEDIA_PLAY_H == null)
                {
                    ImageObjects._CHAT_MEDIA_PLAY_H = ImageUtility.GetBitmapImage(ImageLocation.CHAT_MEDIA_PLAY_H);
                }
                return ImageObjects._CHAT_MEDIA_PLAY_H;
            }
        }

        public static BitmapImage CHAT_MEDIA_PLAY_DISABLE
        {
            get
            {
                if (ImageObjects._CHAT_MEDIA_PLAY_DISABLE == null)
                {
                    ImageObjects._CHAT_MEDIA_PLAY_DISABLE = ImageUtility.GetBitmapImage(ImageLocation.CHAT_MEDIA_PLAY_DISABLE);
                }
                return ImageObjects._CHAT_MEDIA_PLAY_DISABLE;
            }
        }

        public static BitmapImage CHAT_MEDIA_PAUSE
        {
            get
            {
                if (ImageObjects._CHAT_MEDIA_PAUSE == null)
                {
                    ImageObjects._CHAT_MEDIA_PAUSE = ImageUtility.GetBitmapImage(ImageLocation.CHAT_MEDIA_PAUSE);
                }
                return ImageObjects._CHAT_MEDIA_PAUSE;
            }
        }

        public static BitmapImage CHAT_MEDIA_PAUSE_H
        {
            get
            {
                if (ImageObjects._CHAT_MEDIA_PAUSE_H == null)
                {
                    ImageObjects._CHAT_MEDIA_PAUSE_H = ImageUtility.GetBitmapImage(ImageLocation.CHAT_MEDIA_PAUSE_H);
                }
                return ImageObjects._CHAT_MEDIA_PAUSE_H;
            }
        }

        public static BitmapImage CHAT_MEDIA_PAUSE_DISABLE
        {
            get
            {
                if (ImageObjects._CHAT_MEDIA_PAUSE_DISABLE == null)
                {
                    ImageObjects._CHAT_MEDIA_PAUSE_DISABLE = ImageUtility.GetBitmapImage(ImageLocation.CHAT_MEDIA_PAUSE_DISABLE);
                }
                return ImageObjects._CHAT_MEDIA_PAUSE_DISABLE;
            }
        }

        public static Cursor HAND_GRAB
        {
            get
            {
                if (ImageObjects._HAND_GRAB == null)
                {
                    ImageObjects._HAND_GRAB = ImageUtility.GetCursor(ImageLocation.HAND_GRAB, 10, 0);
                }
                return ImageObjects._HAND_GRAB;
            }
        }

        public static Cursor HAND_RELEASE
        {
            get
            {
                if (ImageObjects._HAND_RELEASE == null)
                {
                    ImageObjects._HAND_RELEASE = ImageUtility.GetCursor(ImageLocation.HAND_RELEASE, 10, 0);
                }
                return ImageObjects._HAND_RELEASE;
            }
        }

        public static BitmapImage IMAGE_ORIGINAL_SIZE
        {
            get
            {
                if (ImageObjects._IMAGE_ORIGINAL_SIZE == null)
                {
                    ImageObjects._IMAGE_ORIGINAL_SIZE = ImageUtility.GetBitmapImage(ImageLocation.IMAGE_ORIGINAL_SIZE);
                }
                return ImageObjects._IMAGE_ORIGINAL_SIZE;
            }
        }

        public static BitmapImage IMAGE_ORIGINAL_SIZE_H
        {
            get
            {
                if (ImageObjects._IMAGE_ORIGINAL_SIZE_H == null)
                {
                    ImageObjects._IMAGE_ORIGINAL_SIZE_H = ImageUtility.GetBitmapImage(ImageLocation.IMAGE_ORIGINAL_SIZE_H);
                }
                return ImageObjects._IMAGE_ORIGINAL_SIZE_H;
            }
        }

        public static BitmapImage RECORD_EXPAND
        {
            get
            {
                if (ImageObjects._RECORD_EXPAND == null)
                {
                    ImageObjects._RECORD_EXPAND = ImageUtility.GetBitmapImage(ImageLocation.RECORD_EXPAND);
                }
                return ImageObjects._RECORD_EXPAND;
            }
        }

        public static BitmapImage RECORD_EXPAND_H
        {
            get
            {
                if (ImageObjects._RECORD_EXPAND_H == null)
                {
                    ImageObjects._RECORD_EXPAND_H = ImageUtility.GetBitmapImage(ImageLocation.RECORD_EXPAND_H);
                }
                return ImageObjects._RECORD_EXPAND_H;
            }
        }

        public static BitmapImage RECORD_NORMAL
        {
            get
            {
                if (ImageObjects._RECORD_NORMAL == null)
                {
                    ImageObjects._RECORD_NORMAL = ImageUtility.GetBitmapImage(ImageLocation.RECORD_NORMAL);
                }
                return ImageObjects._RECORD_NORMAL;
            }
        }

        public static BitmapImage RECORD_NORMAL_H
        {
            get
            {
                if (ImageObjects._RECORD_NORMAL_H == null)
                {
                    ImageObjects._RECORD_NORMAL_H = ImageUtility.GetBitmapImage(ImageLocation.RECORD_NORMAL_H);
                }
                return ImageObjects._RECORD_NORMAL_H;
            }
        }

        public static BitmapImage DEFAULT_RING_MEDIA_IMAGE
        {
            get
            {
                if (ImageObjects._DEFAULT_RING_MEDIA_IMAGE == null)
                {
                    ImageObjects._DEFAULT_RING_MEDIA_IMAGE = ImageUtility.GetBitmapImage(ImageLocation.DEFAULT_RING_MEDIA_IMAGE);
                }
                return ImageObjects._DEFAULT_RING_MEDIA_IMAGE;
            }
        }

        public static BitmapImage BUTTON_BACK_IMAGE
        {
            get
            {
                if (ImageObjects._BUTTON_BACK_IMAGE == null)
                {
                    ImageObjects._BUTTON_BACK_IMAGE = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_BACK_IMAGE);
                }
                return ImageObjects._BUTTON_BACK_IMAGE;
            }
        }

        public static BitmapImage BUTTON_BACK_IMAGE_H
        {
            get
            {
                if (ImageObjects._BUTTON_BACK_IMAGE_H == null)
                {
                    ImageObjects._BUTTON_BACK_IMAGE_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_BACK_IMAGE_H);
                }
                return ImageObjects._BUTTON_BACK_IMAGE_H;
            }
        }

        public static BitmapImage BUTTON_BACK_IMAGE_P
        {
            get
            {
                if (ImageObjects._BUTTON_BACK_IMAGE_P == null)
                {
                    ImageObjects._BUTTON_BACK_IMAGE_P = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_BACK_IMAGE_P);
                }
                return ImageObjects._BUTTON_BACK_IMAGE_P;
            }
        }

        public static BitmapImage BUTTON_NEXT_IMAGE
        {
            get
            {
                if (ImageObjects._BUTTON_NEXT_IMAGE == null)
                {
                    ImageObjects._BUTTON_NEXT_IMAGE = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_NEXT_IMAGE);
                }
                return ImageObjects._BUTTON_NEXT_IMAGE;
            }
        }

        public static BitmapImage BUTTON_NEXT_IMAGE_H
        {
            get
            {
                if (ImageObjects._BUTTON_NEXT_IMAGE_H == null)
                {
                    ImageObjects._BUTTON_NEXT_IMAGE_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_NEXT_IMAGE_H);
                }
                return ImageObjects._BUTTON_NEXT_IMAGE_H;
            }
        }

        public static BitmapImage BUTTON_NEXT_IMAGE_P
        {
            get
            {
                if (ImageObjects._BUTTON_NEXT_IMAGE_P == null)
                {
                    ImageObjects._BUTTON_NEXT_IMAGE_P = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_NEXT_IMAGE_P);
                }
                return ImageObjects._BUTTON_NEXT_IMAGE_P;
            }
        }

        public static BitmapImage BUTTON_CLOSE_SMALL_H
        {
            get
            {
                if (ImageObjects._BUTTON_CLOSE_SMALL_H == null)
                {
                    ImageObjects._BUTTON_CLOSE_SMALL_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_CLOSE_SMALL_H);
                }
                return ImageObjects._BUTTON_CLOSE_SMALL_H;
            }
        }

        public static BitmapImage BUTTON_CLOSE_SMALL_WHITE
        {
            get
            {
                if (ImageObjects._BUTTON_CLOSE_SMALL_WHITE == null)
                {
                    ImageObjects._BUTTON_CLOSE_SMALL_WHITE = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_CLOSE_SMALL_WHITE);
                }
                return ImageObjects._BUTTON_CLOSE_SMALL_WHITE;
            }
        }

        public static BitmapImage IMAGE_ZOOM
        {
            get
            {
                if (ImageObjects._IMAGE_ZOOM == null)
                {
                    ImageObjects._IMAGE_ZOOM = ImageUtility.GetBitmapImage(ImageLocation.IMAGE_ZOOM);
                }
                return ImageObjects._IMAGE_ZOOM;
            }
        }

        public static BitmapImage IMAGE_ZOOM_H
        {
            get
            {
                if (ImageObjects._IMAGE_ZOOM_H == null)
                {
                    ImageObjects._IMAGE_ZOOM_H = ImageUtility.GetBitmapImage(ImageLocation.IMAGE_ZOOM_H);
                }
                return ImageObjects._IMAGE_ZOOM_H;
            }
        }

        public static BitmapImage BUTTON_NOTIFICATION_SELECT
        {
            get
            {
                if (ImageObjects._BUTTON_NOTIFICATION_SELECT == null)
                {
                    ImageObjects._BUTTON_NOTIFICATION_SELECT = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_NOTIFICATION_SELECT);
                }
                return ImageObjects._BUTTON_NOTIFICATION_SELECT;
            }
        }

        public static BitmapImage BUTTON_NOTIFICATION_SELECT_H
        {
            get
            {
                if (ImageObjects._BUTTON_NOTIFICATION_SELECT_H == null)
                {
                    ImageObjects._BUTTON_NOTIFICATION_SELECT_H = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_NOTIFICATION_SELECT_H);
                }
                return ImageObjects._BUTTON_NOTIFICATION_SELECT_H;
            }
        }

        public static BitmapImage BUTTON_NOTIFICATION_SELECTED
        {
            get
            {
                if (ImageObjects._BUTTON_NOTIFICATION_SELECTED == null)
                {
                    ImageObjects._BUTTON_NOTIFICATION_SELECTED = ImageUtility.GetBitmapImage(ImageLocation.BUTTON_NOTIFICATION_SELECTED);
                }
                return ImageObjects._BUTTON_NOTIFICATION_SELECTED;
            }
        }

        public static BitmapImage NEW_STATUS_TAG
        {
            get
            {
                if (ImageObjects._NEW_STATUS_TAG == null)
                {
                    ImageObjects._NEW_STATUS_TAG = ImageUtility.GetBitmapImage(ImageLocation.NEW_STATUS_TAG);
                }
                return ImageObjects._NEW_STATUS_TAG;
            }
        }

        public static BitmapImage NEW_STATUS_TAG_H
        {
            get
            {
                if (ImageObjects._NEW_STATUS_TAG_H == null)
                {
                    ImageObjects._NEW_STATUS_TAG_H = ImageUtility.GetBitmapImage(ImageLocation.NEW_STATUS_TAG_H);
                }
                return ImageObjects._NEW_STATUS_TAG_H;
            }
        }

        public static BitmapImage NEW_STATUS_MUSIC
        {
            get
            {
                if (ImageObjects._NEW_STATUS_MUSIC == null)
                {
                    ImageObjects._NEW_STATUS_MUSIC = ImageUtility.GetBitmapImage(ImageLocation.NEW_STATUS_MUSIC);
                }
                return ImageObjects._NEW_STATUS_MUSIC;
            }
        }
        public static BitmapImage NEW_STATUS_MUSIC_H
        {
            get
            {
                if (ImageObjects._NEW_STATUS_MUSIC_H == null)
                {
                    ImageObjects._NEW_STATUS_MUSIC_H = ImageUtility.GetBitmapImage(ImageLocation.NEW_STATUS_MUSIC_H);
                }
                return ImageObjects._NEW_STATUS_MUSIC_H;
            }
        }

        public static BitmapImage NEW_STATUS_LOCATION
        {
            get
            {
                if (ImageObjects._NEW_STATUS_LOCATION == null)
                {
                    ImageObjects._NEW_STATUS_LOCATION = ImageUtility.GetBitmapImage(ImageLocation.NEW_STATUS_LOCATION);
                }
                return ImageObjects._NEW_STATUS_LOCATION;
            }
        }
        public static BitmapImage NEW_STATUS_LOCATION_H
        {
            get
            {
                if (ImageObjects._NEW_STATUS_LOCATION_H == null)
                {
                    ImageObjects._NEW_STATUS_LOCATION_H = ImageUtility.GetBitmapImage(ImageLocation.NEW_STATUS_LOCATION_H);
                }
                return ImageObjects._NEW_STATUS_LOCATION_H;
            }
        }
        public static BitmapImage BLOCK_FRIEND_ICON
        {
            get
            {
                if (ImageObjects._BLOCK_FRIEND_ICON == null)
                {
                    ImageObjects._BLOCK_FRIEND_ICON = ImageUtility.GetBitmapImage(ImageLocation.BLOCK_FRIEND_ICON);
                }
                return ImageObjects._BLOCK_FRIEND_ICON;
            }
        }
        public static BitmapImage BLOCK_FRIEND_ICON_H
        {
            get
            {
                if (ImageObjects._BLOCK_FRIEND_ICON_H == null)
                {
                    ImageObjects._BLOCK_FRIEND_ICON_H = ImageUtility.GetBitmapImage(ImageLocation.BLOCK_FRIEND_ICON_H);
                }
                return ImageObjects._BLOCK_FRIEND_ICON_H;
            }
        }

        public static BitmapImage CIRCLE_SMALL_ICON
        {
            get
            {
                if (ImageObjects._CIRCLE_SMALL_ICON == null)
                {
                    ImageObjects._CIRCLE_SMALL_ICON = ImageUtility.GetBitmapImage(ImageLocation.CIRCLE_SMALL_ICON);
                }
                return ImageObjects._CIRCLE_SMALL_ICON;
            }
        }
        public static BitmapImage CIRCLE_SMALL_ICON_H
        {
            get
            {
                if (ImageObjects._CIRCLE_SMALL_ICON_H == null)
                {
                    ImageObjects._CIRCLE_SMALL_ICON_H = ImageUtility.GetBitmapImage(ImageLocation.CIRCLE_SMALL_ICON_H);
                }
                return ImageObjects._CIRCLE_SMALL_ICON_H;
            }
        }

        public static BitmapImage LOCATION_USER
        {
            get
            {
                if (ImageObjects._LOCATION_USER == null)
                {
                    ImageObjects._LOCATION_USER = ImageUtility.GetBitmapImage(ImageLocation.LOCATION_USER);
                }
                return ImageObjects._LOCATION_USER;
            }
        }

        private static BitmapImage _BUTTON_VIDEO = null;
        private static BitmapImage _BUTTON_VIDEO_H = null;

        public static BitmapImage BUTTON_VIDEO
        {
            get
            {
                ImageObjects._BUTTON_VIDEO = ImageObjects._BUTTON_VIDEO ?? ImageUtility.GetBitmapImage(ImageLocation.BUTTON_VIDEO_FEED);
                return ImageObjects._BUTTON_VIDEO;
            }
        }
        public static BitmapImage BUTTON_VIDEO_H
        {
            get
            {
                ImageObjects._BUTTON_VIDEO_H = ImageObjects._BUTTON_VIDEO_H ?? ImageUtility.GetBitmapImage(ImageLocation.BUTTON_VIDEO_FEED_H);
                return ImageObjects._BUTTON_VIDEO_H;
            }
        }

        public static BitmapImage ADD_MORE
        {
            get
            {
                if (ImageObjects._ADD_MORE == null)
                {
                    ImageObjects._ADD_MORE = ImageUtility.GetBitmapImage(ImageLocation.ADD_MORE);
                }
                return ImageObjects._ADD_MORE;
            }
        }
        public static BitmapImage ADD_MORE_H
        {
            get
            {
                if (ImageObjects._ADD_MORE_H == null)
                {
                    ImageObjects._ADD_MORE_H = ImageUtility.GetBitmapImage(ImageLocation.ADD_MORE_H);
                }
                return ImageObjects._ADD_MORE_H;
            }
        }

        public static BitmapImage RECORDING
        {
            get
            {
                if (ImageObjects._RECORDING == null)
                {
                    ImageObjects._RECORDING = ImageUtility.GetBitmapImage(ImageLocation.RECORD_ANIMATION);
                }
                return ImageObjects._RECORDING;
            }
        }

        public static BitmapImage PLAY_RECORD_VIDEO
        {
            get
            {
                if (ImageObjects._PLAY_RECORD_VIDEO == null)
                {
                    ImageObjects._PLAY_RECORD_VIDEO = ImageUtility.GetBitmapImage(ImageLocation.PLAY_RECORD_VIDEO);
                }
                return ImageObjects._PLAY_RECORD_VIDEO;
            }
        }
        public static BitmapImage PLAY_RECORD_VIDEO_H
        {
            get
            {
                if (ImageObjects._PLAY_RECORD_VIDEO_H == null)
                {
                    ImageObjects._PLAY_RECORD_VIDEO_H = ImageUtility.GetBitmapImage(ImageLocation.PLAY_RECORD_VIDEO_H);
                }
                return ImageObjects._PLAY_RECORD_VIDEO_H;
            }
        }

        public static BitmapImage LINK_VIDEO_ICON
        {
            get
            {
                if (ImageObjects._LINK_VIDEO_ICON == null)
                {
                    ImageObjects._LINK_VIDEO_ICON = ImageUtility.GetBitmapImage(ImageLocation.LINK_VIDEO_ICON);
                }
                return ImageObjects._LINK_VIDEO_ICON;
            }
        }

        public static BitmapImage LINK_VIDEO_ICON_H
        {
            get
            {
                if (ImageObjects._LINK_VIDEO_ICON_H == null)
                {
                    ImageObjects._LINK_VIDEO_ICON_H = ImageUtility.GetBitmapImage(ImageLocation.LINK_VIDEO_ICON_H);
                }
                return ImageObjects._LINK_VIDEO_ICON_H;
            }
        }

        public static Bitmap LOADER_100_BMP
        {
            get
            {
                if (ImageObjects._LOADER_100_BMP == null)
                {
                    ImageObjects._LOADER_100_BMP = ImageUtility.GetBitmap(ImageLocation.LOADER_100);
                }
                return ImageObjects._LOADER_100_BMP;
            }
        }

        public static BitmapImage ADD_FRIEND_ICON_CONTACT_LIST_MEDIUM
        {
            get
            {
                if (ImageObjects._ADD_FRIEND_ICON_CONTACT_LIST_MEDIUM == null)
                {
                    ImageObjects._ADD_FRIEND_ICON_CONTACT_LIST_MEDIUM = ImageUtility.GetBitmapImage(ImageLocation.ADD_FRIEND_ICON_CONTACT_LIST_MEDIUM);
                }
                return ImageObjects._ADD_FRIEND_ICON_CONTACT_LIST_MEDIUM;
            }
        }
        public static BitmapImage FRIEND_ICON_CONTACT_LIST_MEDIUM
        {
            get
            {
                if (ImageObjects._FRIEND_ICON_CONTACT_LIST_MEDIUM == null)
                {
                    ImageObjects._FRIEND_ICON_CONTACT_LIST_MEDIUM = ImageUtility.GetBitmapImage(ImageLocation.FRIEND_ICON_CONTACT_LIST_MEDIUM);
                }
                return ImageObjects._FRIEND_ICON_CONTACT_LIST_MEDIUM;
            }
        }
        public static BitmapImage INCOMING_FRIEND_ICON_CONTACT_LIST_MEDIUM
        {
            get
            {
                if (ImageObjects._INCOMING_FRIEND_ICON_CONTACT_LIST_MEDIUM == null)
                {
                    ImageObjects._INCOMING_FRIEND_ICON_CONTACT_LIST_MEDIUM = ImageUtility.GetBitmapImage(ImageLocation.INCOMING_FRIEND_ICON_CONTACT_LIST_MEDIUM);
                }
                return ImageObjects._INCOMING_FRIEND_ICON_CONTACT_LIST_MEDIUM;
            }
        }
        public static BitmapImage OUTGOING_FRIEND_ICON_CONTACT_LIST_MEDIUM
        {
            get
            {
                if (ImageObjects._OUTGOING_FRIEND_ICON_CONTACT_LIST_MEDIUM == null)
                {
                    ImageObjects._OUTGOING_FRIEND_ICON_CONTACT_LIST_MEDIUM = ImageUtility.GetBitmapImage(ImageLocation.OUTGOING_FRIEND_ICON_CONTACT_LIST_MEDIUM);
                }
                return ImageObjects._OUTGOING_FRIEND_ICON_CONTACT_LIST_MEDIUM;
            }
        }
        public static BitmapImage UP_ARROW
        {
            get
            {
                if (ImageObjects._UP_ARROW == null)
                {
                    ImageObjects._UP_ARROW = ImageUtility.GetBitmapImage(ImageLocation.UP_ARROW);
                }
                return ImageObjects._UP_ARROW;
            }
        }
        public static BitmapImage UP_ARROW_H
        {
            get
            {
                if (ImageObjects._UP_ARROW_H == null)
                {
                    ImageObjects._UP_ARROW_H = ImageUtility.GetBitmapImage(ImageLocation.UP_ARROW_H);
                }
                return ImageObjects._UP_ARROW_H;
            }
        }

        public static BitmapImage UPDATE_VERSION_ICON
        {
            get
            {
                if (ImageObjects._UPDATE_VERSION_ICON == null)
                {
                    ImageObjects._UPDATE_VERSION_ICON = ImageUtility.GetBitmapImage(ImageLocation.UPDATE_VERSION_ICON);
                }
                return ImageObjects._UPDATE_VERSION_ICON;
            }
        }
        // public static string UPDATE_VERSION_ICON { get { return BASE + "update.png"; } }

        public static BitmapImage PHOTO_LOADING
        {
            get
            {
                if (ImageObjects._PHOTO_LOADING == null)
                {
                    ImageObjects._PHOTO_LOADING = ImageUtility.GetBitmapImage(ImageLocation.PHOTO_LOADING);
                }
                return ImageObjects._PHOTO_LOADING;
            }
        }

        private static BitmapImage _PHOTO_ALBUM_COVER_IMAGE = null;
        public static BitmapImage PHOTO_ALBUM_COVER_IMAGE
        {
            get
            {
                if (ImageObjects._PHOTO_ALBUM_COVER_IMAGE == null)
                {
                    ImageObjects._PHOTO_ALBUM_COVER_IMAGE = ImageUtility.GetBitmapImage(ImageLocation.PHOTO_ALBUM_COVER_IMAGE);
                }
                return ImageObjects._PHOTO_ALBUM_COVER_IMAGE;
            }
        }

        public static BitmapImage UPLOAD
        {
            get
            {
                if (ImageObjects._UPLOAD == null)
                {
                    ImageObjects._UPLOAD = ImageUtility.GetBitmapImage(ImageLocation.UPLOAD);
                }
                return ImageObjects._UPLOAD;
            }
        }
        public static BitmapImage UPLOAD_H
        {
            get
            {
                if (ImageObjects._UPLOAD_H == null)
                {
                    ImageObjects._UPLOAD_H = ImageUtility.GetBitmapImage(ImageLocation.UPLOAD_H);
                }
                return ImageObjects._UPLOAD_H;
            }
        }
        public static BitmapImage UPLOAD_POPUP_UP_ARROW
        {
            get
            {
                if (ImageObjects._UPLOAD_POPUP_UP_ARROW == null)
                {
                    ImageObjects._UPLOAD_POPUP_UP_ARROW = ImageUtility.GetBitmapImage(ImageLocation.UPLOAD_POPUP_UP_ARROW);
                }
                return ImageObjects._UPLOAD_POPUP_UP_ARROW;
            }
        }

        public static BitmapImage HD
        {
            get { return ImageUtility.GetBitmapImage(ImageLocation.BUTTON_HD); }
        }

        public static BitmapImage HD_H
        {
            get { return ImageUtility.GetBitmapImage(ImageLocation.BUTTON_HD_H); }
        }

        public static BitmapImage TICK_MARK_WHITE
        {
            get { return ImageUtility.GetBitmapImage(ImageLocation.TICK_MARK_WHITE); }
        }

        public static BitmapImage VERIFY_ICON
        {
            get { return ImageUtility.GetBitmapImage(ImageLocation.VERIFY); }
        }
        public static BitmapImage DOWNLOAD
        {
            get
            {
                if (ImageObjects._DOWNLOAD == null)
                {
                    ImageObjects._DOWNLOAD = ImageUtility.GetBitmapImage(ImageLocation.DOWNLOAD);
                }
                return ImageObjects._DOWNLOAD;
            }
        }

        public static BitmapImage RECENT_LIVE
        {
            get
            {
                if (ImageObjects._RECENT_LIVE == null)
                {
                    ImageObjects._RECENT_LIVE = ImageUtility.GetBitmapImage(ImageLocation.RECENT_LIVE);
                }
                return ImageObjects._RECENT_LIVE;
            }
        }

        public static Cursor STREAM_CURSOR
        {
            get
            {
                if (ImageObjects._STREAM_CURSOR == null)
                {
                    ImageObjects._STREAM_CURSOR = ImageUtility.GetCursor(ImageLocation.STREAM_CURSOR, 25, 0);
                }
                return ImageObjects._STREAM_CURSOR;
            }
        }

        public static BitmapImage DEFAULT_APP_ICON
        {
            get
            {
                if (ImageObjects._DEFAULT_APP_ICON == null)
                {
                    ImageObjects._DEFAULT_APP_ICON = ImageUtility.GetBitmapImage(ImageLocation.DEFAULT_APP_ICON);
                }
                return ImageObjects._DEFAULT_APP_ICON;
            }
        }

        public static BitmapImage CHANNEL_PROFILE_UNKNOWN
        {
            get
            {
                if (ImageObjects._CHANNEL_PROFILE_UNKNOWN == null)
                {
                    ImageObjects._CHANNEL_PROFILE_UNKNOWN = ImageUtility.GetBitmapImage(ImageLocation.CHANNEL_PROFILE_UNKNOWN);
                }
                return ImageObjects._CHANNEL_PROFILE_UNKNOWN;
            }
        }

        public static BitmapImage WALLET { get { return ImageUtility.GetBitmapImage(ImageLocation.WALLET); } }
        public static BitmapImage WALLET_H { get { return ImageUtility.GetBitmapImage(ImageLocation.WALLET_H); } }
        public static BitmapImage WALLET_SELECTED { get { return ImageUtility.GetBitmapImage(ImageLocation.WALLET_SELECTED); } }
        public static BitmapImage GOLD { get { return ImageUtility.GetBitmapImage(ImageLocation.GOLD); } }
        public static BitmapImage SILVER { get { return ImageUtility.GetBitmapImage(ImageLocation.SILVER); } }
        public static BitmapImage BRONZE { get { return ImageUtility.GetBitmapImage(ImageLocation.BRONZE); } }
        public static BitmapImage COIN_EXCHANGE { get { return ImageUtility.GetBitmapImage(ImageLocation.COIN_EXCHANGE); } }
        public static BitmapImage COIN_EXCHANGE_H { get { return ImageUtility.GetBitmapImage(ImageLocation.COIN_EXCHANGE_H); } }
        public static BitmapImage WALLET_COIN { get { return ImageUtility.GetBitmapImage(ImageLocation.WALLET_COIN); } }
        public static BitmapImage WALLET_DOUBLE_COIN { get { return ImageUtility.GetBitmapImage(ImageLocation.WALLET_DOUBLE_COIN); } }
        public static BitmapImage WALLET_CHECK_IN { get { return ImageUtility.GetBitmapImage(ImageLocation.WALLET_CHECK_IN); } }
        public static BitmapImage WALLET_CHECK_IN_GIFT { get { return ImageUtility.GetBitmapImage(ImageLocation.WALLET_CHECK_IN_GIFT); } }
        public static BitmapImage WALLET_CHECK_IN_SUCCESS { get { return ImageUtility.GetBitmapImage(ImageLocation.WALLET_CHECK_IN_SUCCESS); } }
        public static BitmapImage WALLET_INVITE { get { return ImageUtility.GetBitmapImage(ImageLocation.WALLET_INVITE); } }
        public static BitmapImage WALLET_INVITE_H { get { return ImageUtility.GetBitmapImage(ImageLocation.WALLET_INVITE_H); } }
        public static BitmapImage WALLET_MORE_SHARE { get { return ImageUtility.GetBitmapImage(ImageLocation.WALLET_MORE_SHARE); } }
        public static BitmapImage WALLET_MORE_SHARE_H { get { return ImageUtility.GetBitmapImage(ImageLocation.WALLET_MORE_SHARE_H); } }
        public static BitmapImage WALLET_ALREADY_CHECKED_IN { get { return ImageUtility.GetBitmapImage(ImageLocation.WALLET_ALREADY_CHECKED_IN); } }
        public static BitmapImage WALLET_MOBILE_BANKING { get { return ImageUtility.GetBitmapImage(ImageLocation.WALLET_MOBILE_BANKING); } }
        public static BitmapImage WALLET_INTERNET_BANKING { get { return ImageUtility.GetBitmapImage(ImageLocation.WALLET_INTERNET_BANKING); } }
        public static BitmapImage WALLET_BANK_CARDS { get { return ImageUtility.GetBitmapImage(ImageLocation.WALLET_BANK_CARDS); } }
        public static BitmapImage WALLET_RIGHT_ARROW { get { return ImageUtility.GetBitmapImage(ImageLocation.WALLET_RIGHT_ARROW); } }
        public static BitmapImage WALLET_SSL { get { return ImageUtility.GetBitmapImage(ImageLocation.WALLET_SSL); } }
        public static BitmapImage WALLET_PAYPAL { get { return ImageUtility.GetBitmapImage(ImageLocation.WALLET_PAYPAL); } }
    }
}

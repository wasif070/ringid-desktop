﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.ViewModel;

namespace View.Utility.Myprofile
{
    public class ThradChangeAccessSettings
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradChangeAccessSettings).Name);
        private bool running = false;
        private int settingsName, settingsValue;

        #endregion "Private Fields"

        #region "Public Fields"
        public delegate void CompleteHandler(bool status, string errorMsg);
        public event CompleteHandler OnComplete;
        #endregion "Public Fields"

        #region "Constructors"
        public ThradChangeAccessSettings(int settingsName, int settingsValue)
        {
            this.settingsName = settingsName;
            this.settingsValue = settingsValue;
        }
        #endregion "Constructors"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_UPDATE_TOGGLE_SETTINGS;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.SettingsName] = this.settingsName;
                pakToSend[JsonKeys.SettingsValue] = this.settingsValue;

                JObject FeedBackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (FeedBackFields != null)
                {
                    if ((bool)FeedBackFields[JsonKeys.Success] && FeedBackFields[JsonKeys.SettingsName] != null && FeedBackFields[JsonKeys.SettingsValue] != null)
                    {
                        if ((int)FeedBackFields[JsonKeys.SettingsName] == StatusConstants.CALL_ACCESS)
                        {
                            DefaultSettings.userProfile.CallAccess = (int)FeedBackFields[JsonKeys.SettingsValue];
                        }
                        else if ((int)FeedBackFields[JsonKeys.SettingsName] == StatusConstants.CHAT_ACCESS)
                        {
                            DefaultSettings.userProfile.ChatAccess = (int)FeedBackFields[JsonKeys.SettingsValue];
                        }
                        else if ((int)FeedBackFields[JsonKeys.SettingsName] == StatusConstants.FEED_ACCESS)
                        {
                            DefaultSettings.userProfile.FeedAccess = (int)FeedBackFields[JsonKeys.SettingsValue];
                        }
                        else if ((int)FeedBackFields[JsonKeys.SettingsName] == StatusConstants.ANONYMOUS_CALL)
                        {
                            DefaultSettings.userProfile.AnonymousCallAccess = (int)FeedBackFields[JsonKeys.SettingsValue];
                        }
                        else if ((int)FeedBackFields[JsonKeys.SettingsName] == StatusConstants.ANONYMOUS_CHAT)
                        {
                            DefaultSettings.userProfile.AnonymousChatAccess = (int)FeedBackFields[JsonKeys.SettingsValue];
                        }
                        else if ((int)FeedBackFields[JsonKeys.SettingsName] == StatusConstants.ALLOW_INCOMING_FRIEND_REQUEST)
                        {
                            DefaultSettings.userProfile.AllowIncomingFriendRequest = (int)FeedBackFields[JsonKeys.SettingsValue];
                        }
                        else if ((int)FeedBackFields[JsonKeys.SettingsName] == StatusConstants.ALLOW_FRIENDS_TO_ADD_ME)
                        {
                            DefaultSettings.userProfile.AllowFriendsToAddMe = (int)FeedBackFields[JsonKeys.SettingsValue];
                        }
                        else if ((int)FeedBackFields[JsonKeys.SettingsName] == StatusConstants.AUTO_ADD_FRIENDS)
                        {
                            DefaultSettings.userProfile.AutoAddFriends = (int)FeedBackFields[JsonKeys.SettingsValue];
                        }
                        //   HelperMethods.ReloadMyProfile();
                        RingIDViewModel.Instance.ReloadMyProfile();

                        Callback(true, String.Empty);
                    }
                    else
                    {
                        RingIDViewModel.Instance.ReloadMyProfile();

                        if (OnComplete == null)
                        {
                            if (FeedBackFields[JsonKeys.Message] != null)
                            {
                                UIHelperMethods.ShowErrorMessageBoxFromThread((string)FeedBackFields[JsonKeys.Message], "Failed!");
                            }
                            else if (FeedBackFields[JsonKeys.ReasonCode] != null)
                            {
                                UIHelperMethods.ShowErrorMessageBoxFromThread(ReasonCodeConstants.GetReason((int)FeedBackFields[JsonKeys.ReasonCode]), "Failed!");
                            }
                        }
                        else
                        {
                            if (FeedBackFields[JsonKeys.Message] != null)
                            {
                                Callback(false, (string)FeedBackFields[JsonKeys.Message]);
                            }
                            else if (FeedBackFields[JsonKeys.ReasonCode] != null)
                            {
                                Callback(false, ReasonCodeConstants.GetReason((int)FeedBackFields[JsonKeys.ReasonCode]));
                            }
                        }
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out FeedBackFields);
                }
                else
                {
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                    Callback(false, "Failed to process request!");
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }

        private void Callback(bool status, string errorMsg)
        {
            try
            {
                if (OnComplete != null)
                {
                    OnComplete(status, errorMsg);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in Callback() => " + ex.Message + "\n" + ex.StackTrace);
            }
            OnComplete = null;
        }

        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}


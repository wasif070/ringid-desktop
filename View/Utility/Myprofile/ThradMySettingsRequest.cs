﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.ViewModel;

namespace View.Utility.Myprofile
{
    class ThradMySettingsRequest
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradMySettingsRequest).Name);
        private bool running = false;
        #endregion "Private Fields"

        #region "Constructors"
        #endregion "Constructors"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_MY_SETTINGS_VALUES;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                JObject FeedBackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
                if (FeedBackFields != null)
                {
                    if (FeedBackFields != null && (bool)FeedBackFields[JsonKeys.Success])
                    {
                        if (FeedBackFields[JsonKeys.CallAccess] != null) DefaultSettings.userProfile.CallAccess = (int)FeedBackFields[JsonKeys.CallAccess];
                        if (FeedBackFields[JsonKeys.ChatAccess] != null) DefaultSettings.userProfile.ChatAccess = (int)FeedBackFields[JsonKeys.ChatAccess];
                        if (FeedBackFields[JsonKeys.FeedAccess] != null) DefaultSettings.userProfile.FeedAccess = (int)FeedBackFields[JsonKeys.FeedAccess];
                        if (FeedBackFields[JsonKeys.FacebookValidated] != null) DefaultSettings.userProfile.FacebookValidated = (int)FeedBackFields[JsonKeys.FacebookValidated];
                        if (FeedBackFields[JsonKeys.TwitterValidated] != null) DefaultSettings.userProfile.TwitterValidated = (int)FeedBackFields[JsonKeys.TwitterValidated];
                        if (FeedBackFields[JsonKeys.AnonymousCallAccess] != null) DefaultSettings.userProfile.AnonymousCallAccess = (int)FeedBackFields[JsonKeys.AnonymousCallAccess];
                        if (FeedBackFields[JsonKeys.AnonymousChatAccess] != null) DefaultSettings.userProfile.AnonymousChatAccess = (int)FeedBackFields[JsonKeys.AnonymousChatAccess];
                        if (FeedBackFields[JsonKeys.AllowIncomingFriendRequest] != null) DefaultSettings.userProfile.AllowIncomingFriendRequest = (int)FeedBackFields[JsonKeys.AllowIncomingFriendRequest];
                        if (FeedBackFields[JsonKeys.AllowFriendsToAddMe] != null) DefaultSettings.userProfile.AllowFriendsToAddMe = (int)FeedBackFields[JsonKeys.AllowFriendsToAddMe];
                        if (FeedBackFields[JsonKeys.AutoAddFriends] != null) DefaultSettings.userProfile.AutoAddFriends = (int)FeedBackFields[JsonKeys.AutoAddFriends];
                        if (FeedBackFields[JsonKeys.NotificationValidity] != null) DefaultSettings.userProfile.NotificationValidity = (int)FeedBackFields[JsonKeys.NotificationValidity];
                        if (FeedBackFields[JsonKeys.IsMyNumberVerified] != null) DefaultSettings.userProfile.IsMobileNumberVerified = (int)FeedBackFields[JsonKeys.IsMyNumberVerified];
                        if (FeedBackFields[JsonKeys.IsEmailVerified] != null) DefaultSettings.userProfile.IsEmailVerified = (int)FeedBackFields[JsonKeys.IsEmailVerified];
                        RingIDViewModel.Instance.ReloadMyProfile();
                    }
                    else
                    {
                        if (FeedBackFields[JsonKeys.Message] != null) UIHelperMethods.ShowErrorMessageBoxFromThread((string)FeedBackFields[JsonKeys.Message], ""/*"Failed! MySettingsRequest"*/);
                        else if (FeedBackFields[JsonKeys.ReasonCode] != null) UIHelperMethods.ShowErrorMessageBoxFromThread(ReasonCodeConstants.GetReason((int)FeedBackFields[JsonKeys.ReasonCode]), ""/*"Failed! MySettingsRequest"*/);
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out FeedBackFields);
                }
                else
                {
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception) { }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
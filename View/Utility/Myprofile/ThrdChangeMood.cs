﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.ViewModel;

namespace View.Utility.Myprofile
{
    public class ThrdChangeMood
    {
        #region "Private Fields"
        private int mood;
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThrdChangeMood).Name);
        private bool running = false;
        #endregion "Private Fields"

        #region "Constructors"
        public ThrdChangeMood()
        {
        }
        #endregion "Constructors"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_ACTION_CHANGE_MOOD;
                pakToSend[JsonKeys.Mood] = mood;
                JObject FeedBackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_AUTH, packetId);
                if (FeedBackFields != null)
                {
                    if ((bool)FeedBackFields[JsonKeys.Success])
                    {
                        if (FeedBackFields[JsonKeys.Mood] != null)
                        {
                            DefaultSettings.userProfile.Mood = (int)FeedBackFields[JsonKeys.Mood];
                            RingIDViewModel.Instance.ChangeMyStatusIcon(DefaultSettings.userProfile.Mood);
                            //new InsertIntoAllRingUsersSettings();
                        }
                        if (FeedBackFields[JsonKeys.LastStatus] != null)
                        {
                            DefaultSettings.userProfile.Presence = (int)FeedBackFields[JsonKeys.LastStatus];
                        }

                    }
                    else
                    {
                        UIHelperMethods.ShowErrorMessageBoxFromThread((string)FeedBackFields[JsonKeys.Message], "Failed!");
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out FeedBackFields);
                }
                else
                {
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(int mood)
        {
            if (!running)
            {
                this.mood = mood;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.ViewModel;

namespace View.Utility.Myprofile
{
    public class ThradMyProfileDetails
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradMyProfileDetails).Name);
        private bool running = false;
        #endregion "Private Fields"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_MY_PROFILE_DETAILS;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                    {
                        //DefaultSettings.CALL_FORWARDIN_TXT = (feedbackfields[JsonKeys.CallForwardText])(long)feedbackfields[JsonKeys.CallForwardText];
                        HelperMethodsModel.BindMyProfileDetails(feedbackfields);
                        DefaultSettings.IS_MY_PROFILE_VALUES_LOADED = true;
                        RingIDViewModel.Instance.ReloadMyProfile();
                        MainSwitcher.AuthSignalHandler().profileSignalHandler.ShowBasicInfo(DefaultSettings.LOGIN_TABLE_ID);
                        MainSwitcher.AuthSignalHandler().profileSignalHandler.ShowPrivacySettingsInfo();
                        MainSwitcher.AuthSignalHandler().profileSignalHandler.ShowCallSettingsInfo();
                        //Album preview Photo
                        View.Utility.Auth.SendDataToServer.SendAlbumRequest(0, DefaultSettings.userProfile.AlbumId, Guid.Empty, 10);
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {
            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

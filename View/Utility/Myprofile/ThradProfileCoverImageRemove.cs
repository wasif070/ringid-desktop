﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.ViewModel;

namespace View.Utility.Myprofile
{
    class ThradProfileCoverImageRemove
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradProfileCoverImageRemove).Name);
        private bool running = false;
        private int imageType;
        #endregion "Private Fields"


        #region "Constructors"
        public ThradProfileCoverImageRemove(int type)
        {
            this.imageType = type;
        }
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                String packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                if (imageType == SettingsConstants.TYPE_PROFILE_IMAGE)
                {
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_REMOVE_PROFILE_IMAGE;
                    pakToSend[JsonKeys.ProfileImage] = DefaultSettings.userProfile.ProfileImage;
                }
                else if (imageType == SettingsConstants.TYPE_COVER_IMAGE)
                {
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_REMOVE_COVER_IMAGE;
                    pakToSend[JsonKeys.CoverImage] = DefaultSettings.userProfile.CoverImage;
                }

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                    {

                        if (imageType == SettingsConstants.TYPE_PROFILE_IMAGE)
                        {
                            DefaultSettings.userProfile.ProfileImageId = Guid.Empty;
                            DefaultSettings.userProfile.ProfileImage = null;
                        }
                        else if (imageType == SettingsConstants.TYPE_COVER_IMAGE)
                        {
                            DefaultSettings.userProfile.CoverImageId = Guid.Empty;
                            DefaultSettings.userProfile.CoverImage = null;
                        }
                        RingIDViewModel.Instance.ReloadMyProfile();
                        //    HelperMethods.ReloadMyProfile();
                    }
                    //else
                    //{
                    //    if (feedbackfields[JsonKeys.Message] != null)
                    //    {
                    //        msg = (string)feedbackfields[JsonKeys.Message];
                    //    }
                    //}
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        //public void StartThread()
        //{
        //    if (!running)
        //    {
        //        System.Threading.Thread thrd = new System.Threading.Thread(Run);
        //        thrd.Start();
        //    }
        //}

        public void Run(out bool isSuccess, out string msg)
        {
            isSuccess = false;
            msg = string.Empty;

            if (!String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                try
                {
                    if (DefaultSettings.userProfile != null)
                    {
                        JObject pakToSend = new JObject();
                        String packetId = SendToServer.GetRanDomPacketID();
                        pakToSend[JsonKeys.PacketId] = packetId;
                        pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                        if (imageType == SettingsConstants.TYPE_PROFILE_IMAGE)
                        {
                            pakToSend[JsonKeys.Action] = AppConstants.TYPE_REMOVE_PROFILE_IMAGE;
                            pakToSend[JsonKeys.ProfileImage] = DefaultSettings.userProfile.ProfileImage;
                        }
                        else if (imageType == SettingsConstants.TYPE_COVER_IMAGE)
                        {
                            pakToSend[JsonKeys.Action] = AppConstants.TYPE_REMOVE_COVER_IMAGE;
                            pakToSend[JsonKeys.CoverImage] = DefaultSettings.userProfile.CoverImage;
                        }

                        JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                        if (feedbackfields != null)
                        {
                            if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                            {
                                isSuccess = true;

                                if (imageType == SettingsConstants.TYPE_PROFILE_IMAGE)
                                {
                                    DefaultSettings.userProfile.ProfileImageId = Guid.Empty;
                                    DefaultSettings.userProfile.ProfileImage = null;
                                }
                                else if (imageType == SettingsConstants.TYPE_COVER_IMAGE)
                                {
                                    DefaultSettings.userProfile.CoverImageId = Guid.Empty;
                                    DefaultSettings.userProfile.CoverImage = null;
                                }

                                RingIDViewModel.Instance.ReloadMyProfile();
                            }
                            else
                            {
                                if (feedbackfields[JsonKeys.Message] != null)
                                {
                                    msg = (string)feedbackfields[JsonKeys.Message];
                                }
                            }
                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);

                        }
                        else
                        {
                            if (!MainSwitcher.ThreadManager().PingNow())
                            {
                                msg = NotificationMessages.INTERNET_UNAVAILABLE;
                            }
                        }

                    }

                }
                catch (Exception e)
                {
                    log.Error("ProfileCoverImageRemove ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            }
            else
            {
                log.Error("ProfileCoverImageRemove Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                //msg = NotificationMessages.INTERNET_UNAVAILABLE;
            }
        }
        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
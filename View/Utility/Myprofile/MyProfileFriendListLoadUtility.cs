﻿using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using View.BindingModels;
using View.UI;
using View.ViewModel;

namespace View.Utility.Myprofile
{
    public class MyProfileFriendListLoadUtility
    {
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(MyProfileFriendListLoadUtility).Name);
        public MyProfileFriendListLoadUtility()
        {
        }

        public void AddIntoFriendlist(int startRange)
        {
            try
            {
                int start = 0;
                int friendCount = 0;
                int maxCount = 10;
                
                    if (RingIDViewModel.Instance.FavouriteFriendList.Count > 0 && RingIDViewModel.Instance.FavouriteFriendList.Count > startRange)
                    {
                        start = 0;
                        lock (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.FriendListInUserProfile)
                        {
                            for (int i = start; i < (startRange + maxCount); i++)
                            {
                                if (RingIDViewModel.Instance.FavouriteFriendList.Count <= UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.FriendListInUserProfile.Count)
                                {
                                    break;
                                }
                                UserBasicInfoModel model = RingIDViewModel.Instance.FavouriteFriendList.ElementAt(i);
                                if (!checkIfAnyFriendListAlreadyExists(model))
                                {
                                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.FriendListInUserProfile.Add(model);
                                    friendCount++;
                                }
                            }
                        }
                    }

                    if (friendCount < maxCount && RingIDViewModel.Instance.TopFriendList.Count > 0 && (RingIDViewModel.Instance.TopFriendList.Count + RingIDViewModel.Instance.FavouriteFriendList.Count) > startRange)
                    {
                        if (friendCount > 0)
                        {
                            start = 0;
                        }
                        else if (startRange > 0)
                        {
                            start = startRange - RingIDViewModel.Instance.FavouriteFriendList.Count;
                        }
                       
                        lock (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.FriendListInUserProfile)
                        {
                            for (int i = start; i < (startRange + maxCount); i++)
                            {
                                if (friendCount == maxCount || (RingIDViewModel.Instance.FavouriteFriendList.Count + RingIDViewModel.Instance.TopFriendList.Count) <= UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.FriendListInUserProfile.Count)
                                {
                                    break;
                                }
                                UserBasicInfoModel model = RingIDViewModel.Instance.TopFriendList.ElementAt(i);
                                if (!checkIfAnyFriendListAlreadyExists(model))
                                {
                                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.FriendListInUserProfile.Add(model);
                                    friendCount++;
                                }
                            }
                        }
                    }

                int totalCount = RingIDViewModel.Instance.FavouriteFriendList.Count + RingIDViewModel.Instance.TopFriendList.Count + RingIDViewModel.Instance.NonFavouriteFriendList.Count;
                if (friendCount < maxCount && RingIDViewModel.Instance.NonFavouriteFriendList.Count > 0)
                {
                    if (friendCount > 0)
                    {
                        start = 0;
                    }
                    else if (startRange > 0)
                    {
                        start = startRange - (RingIDViewModel.Instance.FavouriteFriendList.Count + RingIDViewModel.Instance.TopFriendList.Count);
                    }
                    lock (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.FriendListInUserProfile)
                    {
                        for (int i = start; i < (startRange + maxCount); i++)
                        {
                            if (friendCount == maxCount || totalCount <= UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.FriendListInUserProfile.Count)
                            {
                                break;
                            }
                            UserBasicInfoModel model = RingIDViewModel.Instance.NonFavouriteFriendList.ElementAt(i);
                            if (!checkIfAnyFriendListAlreadyExists(model))
                            {
                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.FriendListInUserProfile.Add(model);
                                friendCount++;
                            }
                        }
                    }
                }

                if (totalCount > 10 && totalCount > UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.FriendListInUserProfile.Count)
                {
                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.ShowLoader(false);
                }
                else
                {
                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.HideShowMorePanel();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in Loading  MyProfile FriendList==> " + ex.Message + " ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private bool checkIfAnyFriendListAlreadyExists(UserBasicInfoModel model)
        {
            return UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.FriendListInUserProfile.Any(P => P.ShortInfoModel.UserTableID == model.ShortInfoModel.UserTableID);
        }
    }
}

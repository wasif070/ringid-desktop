﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using log4net;
using Models.Constants;
using View.UI;
using View.UI.MiddleUpperViews;
using View.Utility.WPFMessageBox;

namespace View.Utility
{
    public class ThrdCheckForUpdates
    {
        #region "Private Fields"
        private readonly ILog log = LogManager.GetLogger(typeof(ThrdCheckForUpdates).Name);
        private bool CheckingForUpdate = false;
        private bool needToWaringMessage = true;
        #endregion "Private Fields"

        #region "Public Fields"
        public void StartChecking(bool needToShowMessage2)
        {
            if (!CheckingForUpdate)
            {
                Thread trd = new Thread(Run);
                trd.Start();
                this.needToWaringMessage = needToShowMessage2;
            }
        }
        #endregion "Public Fields"

        #region "Constructors"
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Public Methods"
        #endregion "Public methods"

        #region "Private methods"
        private void Run()
        {
            CheckingForUpdate = true;
            try
            {
                WebClient request = new WebClient();
                //    string url = "https://images.ringid.com/official/desktop/ringidUpdate.txt";
                request.Credentials = new NetworkCredential("anonymous", "www.ringid.com");

                try
                {
                    byte[] newFileData = request.DownloadData(AppConfig.UPDATE_URL);
                    string fileString = System.Text.Encoding.UTF8.GetString(newFileData);
                    string versionLine = null;
                    using (StringReader reader = new StringReader(fileString))
                    {
                        string line = string.Empty;
                        do
                        {
                            line = reader.ReadLine();
                            if (line != null && line.StartsWith("ProductVersion"))
                            {
                                versionLine = line;
                                break;
                            }
                        } while (line != null);
                    }
                    System.Windows.Application.Current.Dispatcher.Invoke(delegate
                      {
                          if (versionLine != null)
                          {
                              versionLine = versionLine.Replace("ProductVersion = ", "");
                              int value = string.Compare(versionLine.Trim(), AppConfig.DESKTOP_REALEASE_VERSION);
                              if (value > 0)
                              {
                                  if (UCGuiRingID.Instance != null) UCGuiRingID.Instance.AddIntoUpperMiddlePanel(new UCUpdateRingID(versionLine));
                                  else HelperMethods.Runupdater(needToWaringMessage);
                              }
                              else if (needToWaringMessage)
                              {
                                  UIHelperMethods.ShowInformation(NotificationMessages.NO_NEED_UPGRADE_MESSAGE, "ringID update");
                                  // CustomMessageBox.ShowInformation(NotificationMessages.NO_NEED_UPGRADE_MESSAGE);
                              }
                          }
                          else if (needToWaringMessage)
                          {
                              UIHelperMethods.ShowInformation("Something went wrong. Try again later.", "ringID update");
                              //CustomMessageBox.ShowInformation("Failed!!");
                          }
                      });
                }
                catch (WebException)
                {
                    // Do something such as log error, but this is based on OP's original code
                    // so for now we do nothing.
                }
            }
            catch (Exception) { }
            finally
            {
                CheckingForUpdate = false;
            }
        }
        #endregion "Private methods"
    }
}

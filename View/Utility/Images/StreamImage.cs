﻿using log4net;
using Models.Constants;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using View.BindingModels;
using View.Constants;
using View.Utility.Stream;
using View.ViewModel;

namespace View.Utility.Images
{
    class StreamImage : IRingImage
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(StreamImage).Name);

        private string _ImageUrl;
        private string _ImageName;
        private StreamModel _StreamModel;
        private StreamUserModel _StreamUserModel;
        private WalletGiftProductModel _WalletGiftProductModel;
        private BackgroundWorker bgworker = null;

        public StreamImage(string imageName, string imageUrl, StreamModel streamModel)
        {
            this._StreamModel = streamModel;
            this._ImageName = imageName;
            this._ImageUrl = imageUrl;

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName, ServerAndPortSettings.IMAGE_SERVER_RESOURCE + Path.AltDirectorySeparatorChar);
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeImage();
                }
                ImageDictionaries.Instance.RING_IMAGE_QUEUE.onComplete();
            };

        }

        public StreamImage(string imageName, string imageUrl, StreamUserModel streamUserModel)
        {
            this._StreamUserModel = streamUserModel;
            this._ImageName = imageName;
            this._ImageUrl = imageUrl;

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName, ServerAndPortSettings.IMAGE_SERVER_RESOURCE + Path.AltDirectorySeparatorChar);
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeImage();
                }
                ImageDictionaries.Instance.RING_IMAGE_QUEUE.onComplete();
            };

        }

        public StreamImage(string imageName, string imageUrl, WalletGiftProductModel walletGiftProductModel)
        {
            this._WalletGiftProductModel = walletGiftProductModel;
            this._ImageName = imageName;
            this._ImageUrl = imageUrl;

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName, ServerAndPortSettings.IMAGE_SERVER_RESOURCE + Path.AltDirectorySeparatorChar);
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeImage();
                }
                ImageDictionaries.Instance.RING_IMAGE_QUEUE.onComplete();
            };

        }

        public void Start()
        {
            bgworker.RunWorkerAsync();
        }

        public void ChangeImage()
        {
            try
            {
                if (_StreamModel != null)
                {
                    _StreamModel.OnPropertyChanged("ProfileImage");
                }
                else if (_StreamUserModel != null)
                {
                    _StreamUserModel.OnPropertyChanged("ProfileImage");
                }
                else if (_WalletGiftProductModel != null)
                {
                    _WalletGiftProductModel.OnPropertyChanged("GiftSenderProfileImage");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeImage()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

    }
}

﻿using log4net;
using Models.Constants;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using View.BindingModels;
using View.Constants;
using View.ViewModel;

namespace View.Utility.Images
{
    class RoomImage : IRingImage
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(RoomImage).Name);

        private string _RoomID;
        private string _ImageUrl;
        private string _ImageName;
        private BackgroundWorker bgworker = null;

        public RoomImage(string roomId, string imageName, string imageUrl)
        {
            this._RoomID = roomId;
            this._ImageName = imageName;
            this._ImageUrl = imageUrl;

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName, ServerAndPortSettings.IMAGE_SERVER_RESOURCE + Path.AltDirectorySeparatorChar);
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeImage();
                }
                ImageDictionaries.Instance.RING_IMAGE_QUEUE.onComplete();
            };

        }

        public void Start()
        {
            bgworker.RunWorkerAsync();
        }

        public void ChangeImage()
        {
            try
            {
                RoomModel model = RingIDViewModel.Instance.RoomList.TryGetValue(_RoomID);
                if (model != null)
                {
                    model.OnPropertyChanged("RoomProfileImage");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeImage()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

    }
}

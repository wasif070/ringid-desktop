﻿using log4net;
using Models.Constants;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using View.BindingModels;
using View.Constants;

namespace View.Utility.Images
{
    public class MediaImage : IRingImage
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(MediaImage).Name);
        private string _filePath;
        //private Bitmap _BitmapImage;
        private MediaContentModel mediaContentModel;
        private LinkInformationModel linkModel;
        private SingleMediaModel singleMediaModel;
        private NewsPortalModel newsPortalModel;
        private PageInfoModel pageInfoModel;
        private MusicPageModel musicPageModel;
        private CelebrityModel celebrityModel;
        private CommentModel commentModel;
        private ChannelMediaModel channelMediaModel;
        private BackgroundWorker bgworker = null;


        public MediaImage(SingleMediaModel singleMediaModel, string filePath)
        {
            this.singleMediaModel = singleMediaModel;
            this._filePath = filePath;
            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                //_BitmapImage = ImageUtility.DownloadDoingImage(ServerAndPortSettings.GetVODThumbImageUpResourceURL + singleMediaModel.ThumbUrl, _filePath);
                //e.Result = _BitmapImage;
                if (!(_filePath.EndsWith(".jpg") || _filePath.EndsWith(".png"))) _filePath = _filePath + ".jpg";
                if (ServerAndPortSettings.GetVODThumbImageUpResourceURL != null)
                    e.Result = ImageUtility.SaveImageLocallyFromURL(ServerAndPortSettings.GetVODThumbImageUpResourceURL + singleMediaModel.ThumbUrl, _filePath);
                else e.Result = true;
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeMediaImage(false, 1);
                }
                else
                {
                    ChangeMediaImage(true, 1);
                }
            };
        }

        public MediaImage(MediaContentModel mediaContentModel, string filePath)
        {
            this.mediaContentModel = mediaContentModel;
            this._filePath = filePath;
            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                //_BitmapImage = ImageUtility.DownloadDoingImage(ServerAndPortSettings.GetVODThumbImageUpResourceURL + mediaContentModel.AlbumImageUrl, _filePath);
                //e.Result = _BitmapImage;
                e.Result = ImageUtility.SaveImageLocallyFromURL(ServerAndPortSettings.GetVODThumbImageUpResourceURL + mediaContentModel.AlbumImageUrl, _filePath);
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeMediaImage(false, 2);
                }
                else
                {
                    ChangeMediaImage(true, 2);
                }
            };
        }

        public MediaImage(LinkInformationModel linkModel, string filePath)
        {
            this.linkModel = linkModel;
            this._filePath = filePath;
            if (!string.IsNullOrEmpty(this.linkModel.PreviewImgUrl))
            {
                bgworker = new BackgroundWorker();
                bgworker.DoWork += (s, e) =>
                {
                    string st = (linkModel.PreviewImgUrl.StartsWith("http")) ? linkModel.PreviewImgUrl : "http://" + linkModel.PreviewImgUrl;
                    //_BitmapImage = ImageUtility.DownloadDoingImage(st, _filePath);
                    //e.Result = _BitmapImage;
                    e.Result = ImageUtility.SaveImageLocallyFromURL(st, _filePath);
                };
                bgworker.RunWorkerCompleted += (s, e) =>
                {
                    if (e.Result != null && e.Result is bool && (bool)e.Result)
                    {
                        ChangeMediaImage(false, 3);
                    }
                    else
                    {
                        ChangeMediaImage(true, 3);
                    }
                };
            }
        }

        public MediaImage(NewsPortalModel newsPortalModel, string filePath)
        {
            this.newsPortalModel = newsPortalModel;
            this._filePath = filePath;
            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                //_BitmapImage = ImageUtility.DownloadDoingImage(ServerAndPortSettings.GetVODThumbImageUpResourceURL + mediaContentModel.AlbumImageUrl, _filePath);
                //e.Result = _BitmapImage;
                e.Result = ImageUtility.SaveImageLocallyFromURL(ServerAndPortSettings.GetImageServerResourceURL + newsPortalModel.ProfileImage, _filePath);
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeMediaImage(false, 4);
                }
                else
                {
                    ChangeMediaImage(true, 4);
                }
            };
        }

        public MediaImage(MusicPageModel musicPageModel, string filePath)
        {
            this.musicPageModel = musicPageModel;
            this._filePath = filePath;
            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                //_BitmapImage = ImageUtility.DownloadDoingImage(ServerAndPortSettings.GetVODThumbImageUpResourceURL + mediaContentModel.AlbumImageUrl, _filePath);
                //e.Result = _BitmapImage;
                e.Result = ImageUtility.SaveImageLocallyFromURL(ServerAndPortSettings.GetImageServerResourceURL + musicPageModel.ProfileImage, _filePath);
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeMediaImage(false, 5);
                }
                else
                {
                    ChangeMediaImage(true, 5);
                }
            };
        }

        public MediaImage(PageInfoModel pageInfoModel, string filePath)
        {
            this.pageInfoModel = pageInfoModel;
            this._filePath = filePath;
            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                //_BitmapImage = ImageUtility.DownloadDoingImage(ServerAndPortSettings.GetVODThumbImageUpResourceURL + mediaContentModel.AlbumImageUrl, _filePath);
                //e.Result = _BitmapImage;
                e.Result = ImageUtility.SaveImageLocallyFromURL(ServerAndPortSettings.GetImageServerResourceURL + pageInfoModel.ProfileImage, _filePath);
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeMediaImage(false, 6);
                }
                else
                {
                    ChangeMediaImage(true, 6);
                }
            };
        }

        public MediaImage(CelebrityModel celebrityModel, string filePath)
        {
            this.celebrityModel = celebrityModel;
            this._filePath = filePath;
            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                e.Result = ImageUtility.SaveImageLocallyFromURL(ServerAndPortSettings.GetImageServerResourceURL + celebrityModel.ProfileImage, _filePath);
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeMediaImage(false, 7);
                }
                else
                {
                    ChangeMediaImage(true, 7);
                }
            };
        }

        public MediaImage(CommentModel commentModel, string filePath)
        {
            this.commentModel = commentModel;
            this._filePath = filePath;
            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                if (!(_filePath.EndsWith(".jpg") || _filePath.EndsWith(".png"))) _filePath = _filePath + ".jpg";
                e.Result = ImageUtility.SaveImageLocallyFromURL(ServerAndPortSettings.GetVODThumbImageUpResourceURL + commentModel.ThumbUrl, _filePath);
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeMediaImage(false, 8);
                }
                else
                {
                    ChangeMediaImage(true, 8);
                }
            };
        }

        public MediaImage(ChannelMediaModel chnlMediaModel, string filePath)
        {
            this.channelMediaModel = chnlMediaModel;
            this._filePath = filePath;
            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                //_BitmapImage = ImageUtility.DownloadDoingImage(ServerAndPortSettings.GetVODThumbImageUpResourceURL + mediaContentModel.AlbumImageUrl, _filePath);
                //e.Result = _BitmapImage;
                e.Result = ImageUtility.SaveImageLocallyFromURL(ServerAndPortSettings.GetVODThumbImageUpResourceURL + channelMediaModel.ThumbImageUrl, _filePath);
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeMediaImage(false, 9);
                }
                else
                {
                    ChangeMediaImage(true, 9);
                }
            };
        }

        public void Start()
        {
            bgworker.RunWorkerAsync();
        }

        private void ChangeMediaImage(bool _NoImageFound, int type)
        {
            try
            {
                if (singleMediaModel != null && type == 1)
                {
                    if (_NoImageFound)
                    {
                        singleMediaModel.NoImageFound = true;
                    }
                    singleMediaModel.OnPropertyChanged("CurrentInstance");
                }
                else if (mediaContentModel != null && type == 2)
                {
                    if (_NoImageFound)
                    {
                        mediaContentModel.DownloadFailed = true;
                    }
                    mediaContentModel.OnPropertyChanged("CurrentInstance");
                }
                else if (linkModel != null && !_NoImageFound && type == 3)
                {
                    linkModel.OnPropertyChanged("CurrentInstance");
                }
                else if (newsPortalModel != null && !_NoImageFound && type == 4)
                {
                    newsPortalModel.OnPropertyChanged("CurrentInstance");
                }
                else if (musicPageModel != null && !_NoImageFound && type == 5)
                {
                    musicPageModel.OnPropertyChanged("CurrentInstance");
                }
                else if (pageInfoModel != null && !_NoImageFound && type == 6)
                {
                    pageInfoModel.OnPropertyChanged("CurrentInstance");
                }
                else if (celebrityModel != null && !_NoImageFound && type == 7)
                {
                    celebrityModel.OnPropertyChanged("CurrentInstance");
                }
                else if (commentModel != null && !_NoImageFound && type == 8)
                {
                    commentModel.OnPropertyChanged("CurrentInstance");
                }
                else if (channelMediaModel != null && !_NoImageFound && type == 9)
                {
                    channelMediaModel.OnPropertyChanged("ThumbImageUrl");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: MediaImage() ==>" + ex.StackTrace);
            }
        }
    }
}

﻿using log4net;
using Models.Constants;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using View.BindingModels;
using View.Constants;
using View.Utility.Stream;
using View.ViewModel;

namespace View.Utility.Images
{
    class ChannelImage : IRingImage
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(ChannelImage).Name);

        private string _ImageUrl;
        private string _ImageName;
        private bool _IsProfileImage;
        private ChannelModel _ChannelModel;
        private BackgroundWorker bgworker = null;

        public ChannelImage(string imageName, string imageUrl, ChannelModel channelModel, bool isProfileImage)
        {
            this._ChannelModel = channelModel;
            this._ImageName = imageName;
            this._ImageUrl = imageUrl;
            this._IsProfileImage = isProfileImage;

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName, ServerAndPortSettings.IMAGE_SERVER_RESOURCE + Path.AltDirectorySeparatorChar);
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeImage();
                }
                ImageDictionaries.Instance.RING_IMAGE_QUEUE.onComplete();
            };

        }

        public void Start()
        {
            bgworker.RunWorkerAsync();
        }

        public void ChangeImage()
        {
            try
            {
                if (_ChannelModel != null)
                {
                    if (this._IsProfileImage)
                    {
                        _ChannelModel.OnPropertyChanged("ProfileImage");
                    }
                    else
                    {
                        _ChannelModel.OnPropertyChanged("CoverImage");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeImage()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

    }
}

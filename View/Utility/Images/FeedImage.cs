﻿using Models.Constants;
using View.BindingModels;
using View.Constants;
using View.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using View.Utility.Feed;
using View.Dictonary;
using View.UI.Profile.FriendProfile;
using log4net;
using Auth.utility;
using Models.Entity;
using View.Utility.DataContainer;

namespace View.Utility.Images
{
    class FeedImage : IRingImage
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(FeedImage).Name);

        private Guid _imageId, nfid;
        private string _ImageUrl;
        private string _ImageName;
        private int _ImageType;
        //private Bitmap _BitmapImage;
        private BackgroundWorker bgworker = null;


        public FeedImage(Guid imageId, string imageUrl, Guid nfid, int imageType)
        {

            this.nfid = nfid;
            this._imageId = imageId;
            this._ImageName = imageUrl;
            this._ImageType = imageType;
            this._ImageUrl = imageUrl.Replace("_", "/");

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                //if (this._ImageType == SettingsConstants.TYPE_NORMAL_BOOK_IMAGE)
                //{
                //    e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_BOOK_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
                //}
                if (this._ImageType == SettingsConstants.TYPE_PROFILE_IMAGE)
                {
                    e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
                }
                else if (this._ImageType == SettingsConstants.TYPE_COVER_IMAGE)
                {
                    e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_COVER_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
                }
                else
                {
                    e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_BOOK_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
                }
                //e.Result = _BitmapImage;
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeFeedImage(false);
                }
                else
                {
                    ChangeFeedImage(true);
                }
            };

        }

        public FeedImage(FeedModel model, string imageUrl, string filePath, bool firstImageInList = false)
        {
            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                e.Result = ImageUtility.DownloadImage(imageUrl, filePath);
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeFeedImage(false, model, firstImageInList);
                }
                else
                {
                    ChangeFeedImage(true, model, firstImageInList);
                }
            };

        }
        public void Start()
        {
            bgworker.RunWorkerAsync();
        }

        private void ChangeFeedImage(bool _NoImageFound, FeedModel mdl = null, bool firstImageInList = false)
        {
            try
            {
                if (mdl == null)
                {
                    ImageModel imgModel = null;
                    FeedModel feedModel = null;
                    if (!FeedDataContainer.Instance.FeedModels.TryGetValue(nfid, out feedModel))
                    {
                        feedModel = RingIDViewModel.Instance.BreakingNewsPortalFeeds.Where(p => p.NewsfeedId == nfid).FirstOrDefault();
                        if (feedModel == null)
                        {
                            feedModel = RingIDViewModel.Instance.BreakingNewsPortalProfileFeeds.Where(p => p.NewsfeedId == nfid).FirstOrDefault();
                        }
                    }
                    if (feedModel != null && feedModel.ImageList != null)
                    {
                        imgModel = feedModel.ImageList.Where(P => P.ImageId == _imageId).FirstOrDefault();
                    }
                    if (imgModel != null)
                    {
                        if (_NoImageFound)
                        {
                            imgModel.NoImageFound = _NoImageFound;
                        }
                        //imgModel.OnPropertyChanged("CurrentInstance");
                        imgModel.OnPropertyChanged("IsOpenedInFeed");
                    }
                }
                else
                {
                    mdl.OnPropertyChanged("CurrentInstance");
                    if (firstImageInList && mdl.ImageList != null && mdl.ImageList.Count > 0)
                    {
                        ImageModel imgModel = mdl.ImageList[0];
                        if (imgModel != null)
                        {
                            if (_NoImageFound)
                            {
                                imgModel.NoImageFound = _NoImageFound;
                            }
                            //imgModel.OnPropertyChanged("CurrentInstance");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeFeedImage()." + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #region "Utility Methods"

        #endregion "Utility Methods"

    }
}

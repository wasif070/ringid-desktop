﻿using log4net;
using Models.Constants;
using Models.Stores;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using View.BindingModels;
using View.Constants;
using View.ViewModel;

namespace View.Utility.Images
{
    public class GroupImage : IRingImage
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(GroupImage).Name);

        private long _GroupID;
        private string _ImageUrl;
        private string _ImageName;
        private BackgroundWorker bgworker = null;

        public GroupImage(long groupID, string imageUrl)
        {
            this._GroupID = groupID;
            this._ImageName = imageUrl;
            this._ImageUrl = imageUrl.Replace("_", "/");

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName, ServerAndPortSettings.IMAGE_SERVER_RESOURCE + Path.AltDirectorySeparatorChar);
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeImage();
                }
            };
        }

        public void Start()
        {
            bgworker.RunWorkerAsync();
        }

        public void ChangeImage()
        {
            try
            {
                GroupInfoModel model = RingIDViewModel.Instance.GroupList.TryGetValue(_GroupID);
                if (model != null)
                {
                    model.OnPropertyChanged("CurrentInstance");
                }

                RecentModel recentModel = RingIDViewModel.Instance.ChatLogModelsList.Where(P => P.ContactID.Equals(_GroupID.ToString())).FirstOrDefault();
                if (recentModel != null)
                {
                    //TRIGGER TO FULL NAME OR PROFILE IMAGE OR VISIBILITY BY SEARCH IN CHAT LOG PANEL
                    recentModel.OnPropertyChanged("Message");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeImage()." + ex.Message + "\n" + ex.StackTrace);
            }
        }
    }
}

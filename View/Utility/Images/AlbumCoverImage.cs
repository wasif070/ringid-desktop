﻿using Models.Constants;
using View.BindingModels;
using View.Constants;
using View.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using View.UI.Profile.FriendProfile;
using View.Dictonary;
using log4net;
using View.UI;
using View.Utility.DataContainer;

namespace View.Utility.Images
{
    public class AlbumCoverImage
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AlbumCoverImage).Name);

        private long _friendTableId;
        private Guid _albumId;
        private string _ImageUrl;
        private string _ImageName;
       
        private BackgroundWorker bgworker = null;

        public AlbumCoverImage(long friendTableId, Guid albumId, string imageUrl)
        {
            this._friendTableId = friendTableId;
            this._albumId = albumId;
            this._ImageName = imageUrl;            
            this._ImageUrl = imageUrl.Replace("_", "/");

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_BOOK_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeAlbumCoverImage(false);
                }
                else
                {
                    ChangeAlbumCoverImage(true);
                }
            };
        }        

        public void Start()
        {
            bgworker.RunWorkerAsync();
        }

        private void ChangeAlbumCoverImage(bool _NoImageFound)
        {
            try
            {
                if (this._friendTableId > 0)
                {
                    AlbumModel model = null;
                    UCFriendProfile ucFriendProfile = null;
                    if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == this._friendTableId)
                    {
                        ucFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                        model = UCMiddlePanelSwitcher.View_UCFriendProfilePanel.FriendImageAlubms.Where(P => P.AlbumId == _albumId).FirstOrDefault();
                    }
                    
                    if (model != null)
                    {
                        if (_NoImageFound)
                        {
                            model.NoImageFound = _NoImageFound;
                        }                        
                        model.OnPropertyChanged("CurrentInstance");                        
                    }
                }
                else
                {
                    AlbumModel model = RingIDViewModel.Instance.MyImageAlubms.Where(P => P.AlbumId == _albumId).FirstOrDefault();
                    if (model != null)
                    {
                        if (_NoImageFound)
                        {
                            model.NoImageFound = _NoImageFound;
                        }
                        
                        model.OnPropertyChanged("CurrentInstance");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeAlbumCoverImage()." + ex.Message + "\n" + ex.StackTrace);
            }
        }        
    }
}

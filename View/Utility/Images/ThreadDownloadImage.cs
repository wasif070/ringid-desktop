﻿using System;
using System.ComponentModel;
using System.IO;
using log4net;
using Models.Constants;
using Models.Stores;
using View.BindingModels;
using View.Constants;
using View.Dictonary;
using View.UI;
using View.UI.Profile.FriendProfile;
using View.ViewModel;

namespace View.Utility.Images
{
    public class ThreadDownloadImage
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ThreadDownloadImage).Name);

        public ThreadDownloadImage(string imageUrl, int imageType, BaseUserProfileModel shortInfoModel)
        {
            string imageFolder = null;
            if (imageType == SettingsConstants.IMAGE_TYPE_PROFILE)
            {
                imageFolder = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER;
            }
            else if (imageType == SettingsConstants.IMAGE_TYPE_COVER)
            {
                imageFolder = RingIDSettings.TEMP_COVER_IMAGE_FOLDER;
            }
            else
            {
                imageFolder = RingIDSettings.TEMP_BOOK_IMAGE_FOLDER;
            }
            BackgroundWorker bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                e.Result = ImageUtility.DownloadImage(imageUrl.Replace("_", "/"), imageFolder + Path.DirectorySeparatorChar + imageUrl);
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeProfileImage(shortInfoModel);
                }
                ImageDictionaries.Instance.RING_IMAGE_QUEUE.onComplete();
            };
            bgworker.RunWorkerAsync();
        }

        public void ChangeProfileImage(BaseUserProfileModel shortInfoModel)
        {
            try
            {
                if (shortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID && shortInfoModel.ProfileType == SettingsConstants.PROFILE_TYPE_GENERAL)
                {
                    RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.OnPropertyChanged("CurrentInstance");
                }
                if (shortInfoModel != null)
                {
                    shortInfoModel.OnPropertyChanged("CurrentInstance");
                }
                //else
                //{
                //    if (shortInfoModel != null)
                //    {
                //        shortInfoModel.OnPropertyChanged("CurrentInstance");
                //    }
                //    //if (_FrinedTableId > 0)
                //    //{
                //    //    UCFriendProfile _UCFriendProfile = null;
                //    //    if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == this._FrinedTableId)
                //    //    {
                //    //        _UCFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                //    //    }

                //    //    if (_UCFriendProfile != null)
                //    //    {
                //    //        _UCFriendProfile.ChangeProfileImageForThreeCircle(shortInfoModel);
                //    //    }
                //    //}
                //}

                //if (basicImageViewWrapper() != null && basicImageViewWrapper().ucBasicImageView != null && basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage != null
                //    && basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage.UserShortInfoModel != null
                //    && basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage.UserShortInfoModel.UserTableID == _UserTableID)
                //{
                //    basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage.OnPropertyChanged("UserShortInfoModel");
                //}

                //if (BasicMediaViewWrapper != null && BasicMediaViewWrapper.ucBasicMediaView != null &&
                //    BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage != null
                //     && BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.UserShortInfoModel != null
                //    && BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.UserShortInfoModel.UserTableID == _UserTableID)
                //{
                //    BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.OnPropertyChanged("UserShortInfoModel");
                //}
            }
            catch (Exception ex)
            {
                log.Error("Error: ThreadDownloadImage." + ex.Message + "\n" + ex.StackTrace);
            }

        }

        //private UCBasicImageViewWrapper basicImageViewWrapper()
        //{
        //    return MainSwitcher.PopupController.basicImageViewWrapper;
        //}

        //public UCBasicMediaViewWrapper BasicMediaViewWrapper
        //{
        //    get
        //    {
        //        return MainSwitcher.PopupController.basicMediaViewWrapper;
        //    }
        //}
    }
}

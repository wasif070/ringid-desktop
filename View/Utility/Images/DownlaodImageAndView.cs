﻿using System;
using System.ComponentModel;
using System.IO;
using System.Threading;
using log4net;
using Models.Constants;
using View.BindingModels;
using View.Constants;
using View.Utility.WPFMessageBox;

namespace View.Utility.Images
{
    public class DownlaodImageAndView : BackgroundWorker
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(DownlaodImageAndView).Name);
        ImageModel _selectedFeedImageInfo;
        string imageDirectory = null;
        //  private Bitmap bitmapImage = null;
        public DownlaodImageAndView(ImageModel _selectedFeedImageInfo2)
        {
            this._selectedFeedImageInfo = _selectedFeedImageInfo2;
            WorkerReportsProgress = true;
            WorkerSupportsCancellation = true;
            DoWork += new DoWorkEventHandler(bw_DoWork);
            ProgressChanged += new ProgressChangedEventHandler(bw_ProgressChanged);
            RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);

            ImageDrectorySet();
        }
        private void ImageDrectorySet()
        {
            if (_selectedFeedImageInfo.ImageType == SettingsConstants.TYPE_COVER_IMAGE)
            {
                imageDirectory = RingIDSettings.TEMP_COVER_IMAGE_FOLDER;
            }
            else if (_selectedFeedImageInfo.ImageType == SettingsConstants.TYPE_PROFILE_IMAGE)
            {
                imageDirectory = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER;
            }
            else
            {
                imageDirectory = RingIDSettings.TEMP_BOOK_IMAGE_FOLDER;
            }
        }
        public void Start()
        {
            if (IsBusy != true)
            {
                RunWorkerAsync();
            }
        }
        public void Cancel()
        {
            if (WorkerSupportsCancellation == true)
            {
                CancelAsync();
            }
        }

        string dir = null;
        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                BackgroundWorker worker = sender as BackgroundWorker;
                worker.ReportProgress(0);
                worker.ReportProgress(1);
                if (_selectedFeedImageInfo.ImageUrl == null || _selectedFeedImageInfo.ImageUrl.Trim().Length < 4)
                {
                    for (int wait = 1; wait <= 30; wait++)
                    {
                        Thread.Sleep(300);
                        //if (basicImageViewWrapper().ucBasicImageView.clickedImageID == _selectedFeedImageInfo.ImageId)
                        //{
                        //    if (basicImageViewWrapper().ucBasicImageView._FeedImageList.Count > 0 && basicImageViewWrapper().ucBasicImageView._FeedImageList[basicImageViewWrapper().ucBasicImageView.selectedIndex].ImageUrl != null && basicImageViewWrapper().ucBasicImageView._FeedImageList[basicImageViewWrapper().ucBasicImageView.selectedIndex].ImageUrl.Length > 4)
                        //    {
                        //        if (basicImageViewWrapper().ucBasicImageView._FeedImageList[basicImageViewWrapper().ucBasicImageView.selectedIndex].ImageUrl == NotificationMessages.CONTENT_DOES_NOT_EXIST)
                        //        {
                        //        }
                        //        else
                        //        {
                        //            _selectedFeedImageInfo = basicImageViewWrapper().ucBasicImageView._FeedImageList[basicImageViewWrapper().ucBasicImageView.selectedIndex];
                        //            ImageDrectorySet();
                        //            basicImageViewWrapper().ucBasicImageView.LikeNumbers.CahgeAlbumName();
                        //        }
                        //        break;
                        //    }
                        //}
                        //else
                        //{
                        //    break;
                        //}
                        //if (wait == 30)
                        //{
                        //    break;
                        //}
                    }
                }
                if (_selectedFeedImageInfo.ImageUrl != null && _selectedFeedImageInfo.ImageUrl.Length > 4 && _selectedFeedImageInfo.ImageUrl != NotificationMessages.CONTENT_DOES_NOT_EXIST)
                {
                    String url = ServerAndPortSettings.GetImageServerResourceURL + _selectedFeedImageInfo.ImageUrl;
                    worker.ReportProgress(2);
                    if (!String.IsNullOrEmpty(url) && url.Contains("/"))
                    {
                        string convertedUrl = HelperMethods.ConvertUrlToName(_selectedFeedImageInfo.ImageUrl, ImageUtility.IMG_FULL);
                        dir = imageDirectory + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                        if (!File.Exists(dir))
                        {
                            try
                            {
                                if (DefaultSettings.IsInternetAvailable)
                                {
                                    bool flag = ImageUtility.DownloadRemoteImageFile(url, dir);
                                    if (flag == false)
                                    {
                                        e.Result = "Image file not valid";
                                    }
                                }
                                else
                                {
                                    e.Result = NotificationMessages.INTERNET_UNAVAILABLE;
                                }
                            }
                            catch (Exception ex)
                            {
                                e.Result = "Error";
                                log.Error("DownloadfullImage => " + ex.Message + "\n" + ex.StackTrace);
                            }
                        }
                    }
                }
                else
                {
                    //if (basicImageViewWrapper().ucBasicImageView != null && basicImageViewWrapper().ucBasicImageView.clickedImageID == _selectedFeedImageInfo.ImageId)
                    //{
                    //    worker.ReportProgress(90);
                    //}
                }
            }
            catch (Exception ex)
            {
                log.Error("bw_DoWork" + ex.Message + "\n" + ex.StackTrace);
            }
        }
        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //if ((e.Cancelled == true))
            //{
            //}
            //else if (!(e.Error == null))
            //{
            //}
            //else
            //{
            //    if (e.Result == null)
            //    {
            //        if (basicImageViewWrapper().ucBasicImageView.clickedImageID == _selectedFeedImageInfo.ImageId)
            //        {
            //            if (File.Exists(dir))
            //            {
            //                basicImageViewWrapper().ucBasicImageView.ViewAnImageFrobDirectory(dir);
            //            }
            //        }
            //    }
            //    else
            //    {
            //        if (basicImageViewWrapper().ucBasicImageView.clickedImageID == _selectedFeedImageInfo.ImageId)
            //        {
            //            basicImageViewWrapper().ucBasicImageView.ViewAnImageFrobDirectory(null);
            //        }

            //        log.Error("bw_RunWorkerCompleted() ==>" + e.Result + " ImageId==>" + _selectedFeedImageInfo.ImageId);
            //    }
            //}
        }
        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

            try
            {
                //if (e.ProgressPercentage == 0)
                //{

                //    if (basicImageViewWrapper().ucBasicImageView.clickedImageID == _selectedFeedImageInfo.ImageId)
                //    {
                //        basicImageViewWrapper().ucBasicImageView.ViewBitMap(ImageObjects.LOADER_FEED);
                //    }

                //}
                //else if (e.ProgressPercentage == 1)
                //{
                //    if (_selectedFeedImageInfo.ImageUrl != null && _selectedFeedImageInfo.ImageUrl.Trim().Length > 4)
                //    {
                //        string convertedUrl = null;
                //        convertedUrl = imageDirectory + System.IO.Path.DirectorySeparatorChar + HelperMethods.ConvertUrlToName(_selectedFeedImageInfo.ImageUrl, ImageUtility.IMG_600);
                //        if (!File.Exists(convertedUrl))
                //        {
                //            convertedUrl = null;
                //            convertedUrl = imageDirectory + System.IO.Path.DirectorySeparatorChar + HelperMethods.ConvertUrlToName(_selectedFeedImageInfo.ImageUrl, ImageUtility.IMG_300);
                //            if (!File.Exists(convertedUrl))
                //            {
                //                convertedUrl = null;
                //                convertedUrl = imageDirectory + System.IO.Path.DirectorySeparatorChar + HelperMethods.ConvertUrlToName(_selectedFeedImageInfo.ImageUrl, ImageUtility.IMG_THUMB);
                //                if (!File.Exists(convertedUrl))
                //                {
                //                    convertedUrl = null;
                //                }
                //            }
                //        }
                //        if (convertedUrl != null && basicImageViewWrapper().ucBasicImageView.clickedImageID == _selectedFeedImageInfo.ImageId && File.Exists(convertedUrl))
                //        {
                //            basicImageViewWrapper().ucBasicImageView.ViewAnImageFrobDirectory(convertedUrl);
                //        }
                //    }

                //}
                //else if (e.ProgressPercentage == 2)
                //{
                //    basicImageViewWrapper().ucBasicImageView.LoadMoreAlbumImages();
                //}
                //else if (e.ProgressPercentage == 90)
                //{
                //    basicImageViewWrapper().ucBasicImageView.ViewAnImageFrobDirectory(null);
                //    CustomMessageBox.ShowError(NotificationMessages.CONTENT_DOES_NOT_EXIST);
                //    if (basicImageViewWrapper().ucBasicImageView.TotalImages == 0 || basicImageViewWrapper().ucBasicImageView.TotalImages == 1)
                //    {

                //        basicImageViewWrapper().ucBasicImageView.DoCancel();
                //    }
                //}
            }
            catch (Exception ex)
            {
                log.Error("bw_ProgressChanged() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }
    }
}

﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Net;
using System.Threading;
using Auth.utility;
using log4net;
using Models.Constants;
using View.BindingModels;
using View.Constants;
using View.UI;
using View.Utility.WPFMessageBox;
/*
 * DownLoadFullImageAndView will show 
 * 600 image  
 * or loading image first then will download
 * main image and will show in view from downlaoded directory 
 */
namespace View.Utility.Images
{
    //public class DownLoadFullImageAndView : BackgroundWorker
    //{
    //    private static readonly ILog log = LogManager.GetLogger(typeof(DownLoadFullImageAndView).Name);
    //    ImageModel _selectedFeedImageInfo;
    //    string imageDirectory = null;
    //    public DownLoadFullImageAndView(ImageModel _selectedFeedImageInfo2)
    //    {
    //        this._selectedFeedImageInfo = _selectedFeedImageInfo2;
    //        WorkerReportsProgress = true;
    //        WorkerSupportsCancellation = true;
    //        DoWork += new DoWorkEventHandler(bw_DoWork);
    //        ProgressChanged += new ProgressChangedEventHandler(bw_ProgressChanged);
    //        RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);

    //        ImageDrectorySet();
    //    }
    //    private void ImageDrectorySet()
    //    {
    //        if (_selectedFeedImageInfo.ImageType == SettingsConstants.TYPE_COVER_IMAGE)
    //        {
    //            imageDirectory = RingIDSettings.TEMP_COVER_IMAGE_FOLDER;
    //        }
    //        else if (_selectedFeedImageInfo.ImageType == SettingsConstants.TYPE_PROFILE_IMAGE)
    //        {
    //            imageDirectory = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER;
    //        }
    //        else
    //        {
    //            imageDirectory = RingIDSettings.TEMP_BOOK_IMAGE_FOLDER;
    //        }
    //    }
    //    public void Start()
    //    {
    //        if (IsBusy != true)
    //        {
    //            RunWorkerAsync();
    //        }
    //    }
    //    public void Cancel()
    //    {
    //        if (WorkerSupportsCancellation == true)
    //        {
    //            CancelAsync();
    //        }
    //    }

    //    string dir = null;
    //    private void bw_DoWork(object sender, DoWorkEventArgs e)
    //    {
    //        try
    //        {
    //            BackgroundWorker worker = sender as BackgroundWorker;
    //            worker.ReportProgress(0);
    //            worker.ReportProgress(1);
    //            if (_selectedFeedImageInfo.ImageUrl == null || _selectedFeedImageInfo.ImageUrl.Trim().Length < 4)
    //            {
    //                for (int wait = 1; wait <= 30; wait++)
    //                {
    //                    Thread.Sleep(300);
    //                    if (UCGuiRingID.Instance.ucBasicImageView.clickedImageID == _selectedFeedImageInfo.ImageId)
    //                    {
    //                        if (UCGuiRingID.Instance.ucBasicImageView._FeedImageList.Count > 0 && UCGuiRingID.Instance.ucBasicImageView._FeedImageList[UCGuiRingID.Instance.ucBasicImageView.SelectedIndex1].ImageUrl != null && UCGuiRingID.Instance.ucBasicImageView._FeedImageList[UCGuiRingID.Instance.ucBasicImageView.SelectedIndex1].ImageUrl.Length > 4)
    //                        {
    //                            if (UCGuiRingID.Instance.ucBasicImageView._FeedImageList[UCGuiRingID.Instance.ucBasicImageView.SelectedIndex1].ImageUrl == NotificationMessages.CONTENT_DOES_NOT_EXIST)
    //                            {
    //                            }
    //                            else
    //                            {
    //                                _selectedFeedImageInfo = UCGuiRingID.Instance.ucBasicImageView._FeedImageList[UCGuiRingID.Instance.ucBasicImageView.SelectedIndex1];
    //                                ImageDrectorySet();
    //                                UCGuiRingID.Instance.ucBasicImageView.LikeNumbers.CahgeAlbumName();
    //                            }
    //                            break;
    //                        }
    //                    }
    //                    else
    //                    {
    //                        break;
    //                    }
    //                    if (wait == 30)
    //                    {
    //                        break;
    //                    }
    //                }
    //            }
    //            if (_selectedFeedImageInfo.ImageUrl != null && _selectedFeedImageInfo.ImageUrl.Length > 4 && _selectedFeedImageInfo.ImageUrl != NotificationMessages.CONTENT_DOES_NOT_EXIST)
    //            {
    //                String url = ServerAndPortSettings.GetImageServerResourceURL + _selectedFeedImageInfo.ImageUrl;
    //                worker.ReportProgress(2);
    //                if (!String.IsNullOrEmpty(url) && url.Contains("/"))
    //                {
    //                    string convertedUrl = HelperMethods.ConvertUrlTo(_selectedFeedImageInfo.ImageUrl, ImageUtility.IMG_FULL);
    //                    dir = imageDirectory + System.IO.Path.DirectorySeparatorChar + convertedUrl;
    //                    if (!File.Exists(dir))
    //                    {
    //                        try
    //                        {
    //                            //DefaultSettings.IsInternetAvailable = PingInServer.CheckPingServer();
    //                            if (DefaultSettings.IsInternetAvailable)
    //                            {
    //                                Uri uriAddress = new Uri(url, UriKind.RelativeOrAbsolute);
    //                                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uriAddress);
    //                                request.Headers.Add("x-app-version", DefaultSettings.VERSION_PC);
    //                                request.Proxy = null;
    //                                HttpWebResponse response = null;
    //                                String contentType = String.Empty;
    //                                bool flag = false;

    //                                try
    //                                {
    //                                    response = (HttpWebResponse)request.GetResponse();
    //                                    if ((response.StatusCode == HttpStatusCode.OK ||
    //                                        response.StatusCode == HttpStatusCode.Moved ||
    //                                        response.StatusCode == HttpStatusCode.Redirect) &&
    //                                        response.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
    //                                    {
    //                                        System.Net.WebRequest request2 = System.Net.WebRequest.Create(uriAddress);
    //                                        System.Net.WebResponse response2 = request.GetResponse();
    //                                        System.IO.Stream responseStream = response.GetResponseStream();
    //                                        Bitmap img = new Bitmap(responseStream);
    //                                        img.Save(dir, System.Drawing.Imaging.ImageFormat.Jpeg);
    //                                        img.Dispose();
    //                                        flag = true;
    //                                    }
    //                                }
    //                                catch (Exception ex)
    //                                {
    //                                    log.Error("Exception in getting response  => " + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message + "==>" + url);
    //                                }
    //                                finally
    //                                {
    //                                    if (response != null)
    //                                    {
    //                                        response.Close();
    //                                        response = null;
    //                                    }
    //                                    request.Abort();
    //                                    request = null;
    //                                }

    //                                if (flag == false)
    //                                {
    //                                    e.Result = "Image file not valid";
    //                                }
    //                            }
    //                            else
    //                            {
    //                                e.Result = NotificationMessages.INTERNET_UNAVAILABLE;
    //                            }
    //                        }
    //                        catch (Exception ex)
    //                        {
    //                            e.Result = "Error";
    //                            log.Error("DownloadfullImage => " + ex.Message + "\n" + ex.StackTrace);
    //                        }
    //                    }
    //                }
    //            }
    //            else
    //            {
    //                if (UCGuiRingID.Instance.ucBasicImageView != null && UCGuiRingID.Instance.ucBasicImageView.clickedImageID == _selectedFeedImageInfo.ImageId)
    //                {
    //                    worker.ReportProgress(90);
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            log.Error("bw_DoWork" + ex.Message + "\n" + ex.StackTrace);
    //        }
    //    }
    //    private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    //    {
    //        if ((e.Cancelled == true))
    //        {
    //        }
    //        else if (!(e.Error == null))
    //        {
    //        }
    //        else
    //        {
    //            if (e.Result == null)
    //            {
    //                if (UCGuiRingID.Instance.ucBasicImageView.clickedImageID == _selectedFeedImageInfo.ImageId)
    //                {
    //                    if (File.Exists(dir))
    //                    {

    //                        UCGuiRingID.Instance.ucBasicImageView.ViewAnImageFrobDirectory(dir);
    //                    }
    //                    else
    //                    {
    //                        log.Error("bw_RunWorkerCompleted() ==>ID ==> " + _selectedFeedImageInfo.ImageId + ", Url ==> " + _selectedFeedImageInfo.ImageUrl);

    //                        if (UCGuiRingID.Instance.ucBasicImageView.clickedImageID == _selectedFeedImageInfo.ImageId)
    //                        {
    //                            UCGuiRingID.Instance.ucBasicImageView.ViewAnImageFrobDirectory(null);
    //                        }
    //                    }
    //                }

    //            }
    //            else
    //            {

    //                if (UCGuiRingID.Instance.ucBasicImageView.clickedImageID == _selectedFeedImageInfo.ImageId)
    //                {
    //                    UCGuiRingID.Instance.ucBasicImageView.ViewAnImageFrobDirectory(null);
    //                }

    //                log.Error("bw_RunWorkerCompleted() ==>" + e.Result + " ImageId==>" + _selectedFeedImageInfo.ImageId);
    //            }
    //        }
    //    }
    //    private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
    //    {

    //        try
    //        {
    //            if (e.ProgressPercentage == 0)
    //            {

    //                if (UCGuiRingID.Instance.ucBasicImageView.clickedImageID == _selectedFeedImageInfo.ImageId)
    //                {
    //                    // UCGuiRingID.Instance.ucBasicImageView.ViewBitMap(ImageObjects.LOADING_RINGID_LOGO);
    //                    UCGuiRingID.Instance.ucBasicImageView.ViewBitMap(ImageObjects.LOADER_FEED);
    //                }

    //            }
    //            else if (e.ProgressPercentage == 1)
    //            {
    //                if (_selectedFeedImageInfo.ImageUrl != null && _selectedFeedImageInfo.ImageUrl.Trim().Length > 4)
    //                {
    //                    string convertedUrl = null;
    //                    /*
    //                    if (UCGuiRingID.Instance.ucBasicImageView.clickedNewsFeedID > 0)
    //                    {
    //                        convertedUrl = HelperMethods.ConvertUrlTo(_selectedFeedImageInfo.ImageUrl, ImageUtility.IMG_600);
    //                    }
    //                    else if (_selectedFeedImageInfo.ImageType == SettingsConstants.TYPE_PROFILE_IMAGE)
    //                    {
    //                        convertedUrl = HelperMethods.ConvertUrlTo(_selectedFeedImageInfo.ImageUrl, ImageUtility.IMG_THUMB);
    //                    }
    //                    else if (_selectedFeedImageInfo.ImageType == SettingsConstants.TYPE_COVER_IMAGE)
    //                    {
    //                        convertedUrl = HelperMethods.ConvertUrlTo(_selectedFeedImageInfo.ImageUrl, ImageUtility.IMG_300);
    //                    }
    //                    */

    //                    convertedUrl = imageDirectory + System.IO.Path.DirectorySeparatorChar + HelperMethods.ConvertUrlTo(_selectedFeedImageInfo.ImageUrl, ImageUtility.IMG_600);
    //                    if (!File.Exists(convertedUrl))
    //                    {
    //                        convertedUrl = null;
    //                        convertedUrl = imageDirectory + System.IO.Path.DirectorySeparatorChar + HelperMethods.ConvertUrlTo(_selectedFeedImageInfo.ImageUrl, ImageUtility.IMG_300);
    //                        if (!File.Exists(convertedUrl))
    //                        {
    //                            convertedUrl = null;
    //                            convertedUrl = imageDirectory + System.IO.Path.DirectorySeparatorChar + HelperMethods.ConvertUrlTo(_selectedFeedImageInfo.ImageUrl, ImageUtility.IMG_THUMB);
    //                            if (!File.Exists(convertedUrl))
    //                            {
    //                                convertedUrl = null;
    //                            }
    //                        }
    //                    }


    //                    if (convertedUrl != null && UCGuiRingID.Instance.ucBasicImageView.clickedImageID == _selectedFeedImageInfo.ImageId && File.Exists(convertedUrl))
    //                    {
    //                        // convertedUrl = convertedUrl.Replace("/", "_");
    //                        // convertedUrl = imageDirectory + System.IO.Path.DirectorySeparatorChar + convertedUrl;
    //                        // if (File.Exists(convertedUrl))
    //                        // {
    //                        UCGuiRingID.Instance.ucBasicImageView.ViewAnImageFrobDirectory(convertedUrl);
    //                        //  }

    //                    }
    //                }

    //            }
    //            else if (e.ProgressPercentage == 2)
    //            {
    //                UCGuiRingID.Instance.ucBasicImageView.LoadMoreAlbumImages();
    //            }
    //            else if (e.ProgressPercentage == 90)
    //            {
    //                UCGuiRingID.Instance.ucBasicImageView.ViewAnImageFrobDirectory(null);
    //                CustomMessageBox.ShowError(NotificationMessages.CONTENT_DOES_NOT_EXIST);
    //                if (UCGuiRingID.Instance.ucBasicImageView.TotalImages == 0 || UCGuiRingID.Instance.ucBasicImageView.TotalImages == 1)
    //                {

    //                    UCGuiRingID.Instance.ucBasicImageView.DoCancel();
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            log.Error("bw_ProgressChanged() ==>" + ex.Message + "\n" + ex.StackTrace);
    //        }
    //    }
    //}
}

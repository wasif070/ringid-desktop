﻿using System;
using System.ComponentModel;
using System.IO;
using log4net;
using Models.Constants;
using Models.Stores;
using View.BindingModels;
using View.Constants;
using View.UI;
using View.UI.Profile.FriendProfile;
using View.ViewModel;
using View.Utility.Stream;
using System.Linq;

namespace View.Utility.Images
{
    public class ProfileImage : IRingImage
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ProfileImage).Name);

        private long _UserTableID;
        private string _ImageUrl;
        private string _ImageName;
        //private Bitmap _BitmapImage;
        private BackgroundWorker bgworker = null;
        private long _FrinedTableId;
        //private CommentModel commentModel = null;
        //private FeedModel feedModel = null;
        private string _StreamId;

        public ProfileImage(long userTableID, string imageUrl, UserShortInfoModel shortInfoModel, long _FrinedProfileUtId = 0)
        {

            this._UserTableID = userTableID;
            this._ImageName = imageUrl;
            this._FrinedTableId = _FrinedProfileUtId;
            this._ImageUrl = imageUrl.Replace("_", "/");

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
                //e.Result = _BitmapImage;
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeProfileImage(shortInfoModel);
                }
                ImageDictionaries.Instance.RING_IMAGE_QUEUE.onComplete();
            };

        }
        public ProfileImage(long userIdentity, string imageUrl, NewsPortalModel shortInfoModel)
        {

            this._UserTableID = userIdentity;
            this._ImageName = imageUrl;
            //this._FrinedTableId = _FrinedProfileIdentity;
            this._ImageUrl = imageUrl.Replace("_", "/");

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
                //e.Result = _BitmapImage;
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeProfileImage(shortInfoModel);
                }
                ImageDictionaries.Instance.RING_IMAGE_QUEUE.onComplete();
            };

        }
        public ProfileImage(long userIdentity, string imageUrl, PageInfoModel shortInfoModel)
        {

            this._UserTableID = userIdentity;
            this._ImageName = imageUrl;
            //this._FrinedTableId = _FrinedProfileIdentity;
            this._ImageUrl = imageUrl.Replace("_", "/");

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
                //e.Result = _BitmapImage;
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeProfileImage(shortInfoModel);
                }
                ImageDictionaries.Instance.RING_IMAGE_QUEUE.onComplete();
            };

        }

        public ProfileImage(long userIdentity, string imageUrl, CelebrityModel shortInfoModel)
        {

            this._UserTableID = userIdentity;
            this._ImageName = imageUrl;
            //this._FrinedTableId = _FrinedProfileIdentity;
            this._ImageUrl = imageUrl.Replace("_", "/");

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
                //e.Result = _BitmapImage;
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeProfileImage(shortInfoModel);
                }
                ImageDictionaries.Instance.RING_IMAGE_QUEUE.onComplete();
            };

        }

        public ProfileImage(long userIdentity, string imageUrl, MusicPageModel shortInfoModel)
        {

            this._UserTableID = userIdentity;
            this._ImageName = imageUrl;
            //this._FrinedTableId = _FrinedProfileIdentity;
            this._ImageUrl = imageUrl.Replace("_", "/");

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
                //e.Result = _BitmapImage;
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeProfileImage(shortInfoModel);
                }
                ImageDictionaries.Instance.RING_IMAGE_QUEUE.onComplete();
            };

        }
        //Base Model
        public ProfileImage(long userTableID, string imageUrl, BaseUserProfileModel shortInfoModel)
        {

            this._UserTableID = userTableID;
            this._ImageName = imageUrl;
            //this._FrinedTableId = _FrinedProfileUtId;
            this._ImageUrl = imageUrl.Replace("_", "/");

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
                //e.Result = _BitmapImage;
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeProfileImage(shortInfoModel);
                }
                ImageDictionaries.Instance.RING_IMAGE_QUEUE.onComplete();
            };

        }

        public void Start()
        {
            bgworker.RunWorkerAsync();
        }

        public void ChangeProfileImage(BaseUserProfileModel shortInfoModel)
        {
            try
            {
                if (_UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                {
                    RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.OnPropertyChanged("CurrentInstance");
                }
                else
                {
                    if (shortInfoModel != null)
                    {
                        shortInfoModel.OnPropertyChanged("CurrentInstance");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeProfileImage()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ChangeProfileImage(UserShortInfoModel shortInfoModel)
        {
            try
            {
                if (_UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                {
                    RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.OnPropertyChanged("CurrentInstance");
                }
                else
                {
                    /*UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelByID(_UserIdentity);
                    if (model != null)
                    {
                        model.ShortInfoModel.OnPropertyChanged("CurrentInstance");
                    }*/
                    if (shortInfoModel != null)
                    {
                        shortInfoModel.OnPropertyChanged("CurrentInstance");
                    }
                    if (_FrinedTableId > 0)
                    {
                        UCFriendProfile _UCFriendProfile = null;
                        if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == this._FrinedTableId)
                        {
                            _UCFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                        }

                        if (_UCFriendProfile != null)
                        {
                            _UCFriendProfile.ChangeProfileImageForThreeCircle(shortInfoModel);
                        }
                    }
                }

                //if (basicImageViewWrapper() != null && basicImageViewWrapper().ucBasicImageView != null && basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage != null
                //    && basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage.UserShortInfoModel != null
                //    && basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage.UserShortInfoModel.UserTableID == _UserTableID)
                //{
                //    basicImageViewWrapper().ucBasicImageView.ImageFullNameProfileImage.OnPropertyChanged("UserShortInfoModel");
                //}

                //if (BasicMediaViewWrapper != null && BasicMediaViewWrapper.ucBasicMediaView != null &&
                //    BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage != null
                //     && BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.UserShortInfoModel != null
                //    && BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.UserShortInfoModel.UserTableID == _UserTableID)
                //{
                //    BasicMediaViewWrapper.ucBasicMediaView.ImageFullNameProfileImage.OnPropertyChanged("UserShortInfoModel");
                //}
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeProfileImage()." + ex.Message + "\n" + ex.StackTrace);
            }

        }

        public void ChangeProfileImage(PageInfoModel shortInfoModel)
        {
            try
            {
                if (shortInfoModel != null)
                {
                    shortInfoModel.OnPropertyChanged("CurrentInstance");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeProfileImage()." + ex.Message + "\n" + ex.StackTrace);
            }

        }
        public void ChangeProfileImage(NewsPortalModel shortInfoModel)
        {
            try
            {
                if (shortInfoModel != null)
                {
                    shortInfoModel.OnPropertyChanged("CurrentInstance");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeProfileImage()." + ex.Message + "\n" + ex.StackTrace);
            }

        }

        public void ChangeProfileImage(CelebrityModel shortInfoModel)
        {
            try
            {
                if (shortInfoModel != null)
                {
                    shortInfoModel.OnPropertyChanged("CurrentInstance");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeProfileImage()." + ex.Message + "\n" + ex.StackTrace);
            }

        }

        public void ChangeProfileImage(MusicPageModel shortInfoModel)
        {
            try
            {
                if (shortInfoModel != null)
                {
                    shortInfoModel.OnPropertyChanged("CurrentInstance");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeProfileImage()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        //private UCBasicImageViewWrapper basicImageViewWrapper()
        //{
        //    return MainSwitcher.PopupController.basicImageViewWrapper;
        //}

        //public UCBasicMediaViewWrapper BasicMediaViewWrapper
        //{
        //    get
        //    {
        //        return MainSwitcher.PopupController.basicMediaViewWrapper;
        //    }
        //}
    }
}

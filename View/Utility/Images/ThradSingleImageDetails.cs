﻿using System;
using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Utility.DataContainer;

namespace View.Utility.Images
{
    public class ThradSingleImageDetails
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradSingleImageDetails).Name);
        private bool running = false;
        Guid nfId, imgId;
        #endregion "Private Fields"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.ImageId] = imgId;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_IMAGE_DETAILS;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
                if (feedbackfields != null)
                {
                    try
                    {
                        ImageModel imageModel = ImageDataContainer.Instance.GetFromImageModelDictionary(imgId);

                        //{"imgId":192,"iurl":"2110063361/1442288972579.jpg","cptn":"","ih":360,"iw":640,"imT":1,"albId":"default","albn":"Feed Photos","tm":1442288974849,"nl":0,"il":0,"ic":1,"nc":7,"sucs":true,"uId":"2110063361","fn":"smite.bakasura.desktop.2"}
                        if (feedbackfields != null && (bool)feedbackfields[JsonKeys.Success] == true)
                        {
                            imageModel.LoadData(feedbackfields);
                        }
                        else
                        {
                            imageModel.ImageUrl = NotificationMessages.CONTENT_DOES_NOT_EXIST;
                        }
                        //MainSwitcher.AuthSignalHandler().imageSignalHandler.GetSingleImageModelFromServer(imageModel, nfId);
                    }
                    finally
                    {
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    MainSwitcher.AuthSignalHandler().imageSignalHandler.FetchImageCommentsUI();
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(Guid imgId, Guid nfId)
        {
            if (!running)
            {
                this.nfId = nfId;
                this.imgId = imgId;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

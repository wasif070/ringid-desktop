﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace View.Utility.Images
{
    public class ImageUrlHelper
    {
        public static string GetImageNameFromUrl(String imageUrl)
        {
            String fullImageName = String.Empty;
            if (imageUrl != null)
            {
                fullImageName = imageUrl.Replace('/', '_');
            }
            return fullImageName;
        }

        public static string GetThumbUrl(String imageUrl)
        {
            String fullImageName = String.Empty;

            if (imageUrl != null && imageUrl.Trim().Length > 0 && imageUrl.Contains("/"))
            {
                int index = imageUrl.LastIndexOf('/');
                StringBuilder bulider = new StringBuilder(imageUrl);
                bulider.Insert(imageUrl.LastIndexOf('/') + 1, "thumb");
                fullImageName = bulider.ToString()/*.Replace('/', '_')*/;
            }

            return fullImageName;
        }

        public static string GetCropUrl(String imageUrl)
        {
            String fullImageName = String.Empty;

            if (imageUrl != null && imageUrl.Trim().Length > 0 && imageUrl.Contains("/"))
            {
                int index = imageUrl.LastIndexOf('/');
                StringBuilder bulider = new StringBuilder(imageUrl);
                bulider.Insert(imageUrl.LastIndexOf('/') + 1, "crp");
                fullImageName = bulider.ToString()/*.Replace('/', '_')*/;
            }

            return fullImageName;
        }
    }
}

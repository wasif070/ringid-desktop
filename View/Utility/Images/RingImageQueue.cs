﻿using Models.Constants;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using View.BindingModels;

namespace View.Utility.Images
{
    public class RingImageQueue : ConcurrentQueue<DownloadImageShortInfo>
    {
        public static int _STATUS_OPEN = 0;
        public  int _STATUS_PENDING = 1;
        public static int _CURRENT_STATUS = _STATUS_OPEN;

        public void AddItem(DownloadImageShortInfo obj)
        {
            this.Enqueue(obj);
            GetItem();
        }

        public void GetItem()
        {
            if (!IsEmpty && _CURRENT_STATUS == _STATUS_OPEN && DefaultSettings.FRIEND_LIST_LOADED)
            {
                DownloadImageShortInfo img = null;
                this.TryDequeue(out img);
                if (img != null)
                {
                    _CURRENT_STATUS = _STATUS_PENDING;
                    if(img.DownloadImageType == SettingsConstants.IMAGE_TYPE_PROFILE && img.DownloadImageClass != null)
                    {
                        if (img.DownloadImageClass is BaseUserProfileModel)
                            new ProfileImage(img.UserTableID, img.ImageUrl, (BaseUserProfileModel)img.DownloadImageClass).Start();                       
                        else if (img.DownloadImageClass is UserShortInfoModel)
                            new ProfileImage(img.UserTableID, img.ImageUrl, (UserShortInfoModel)img.DownloadImageClass).Start();
                        else if (img.DownloadImageClass is NewsPortalModel)
                            new ProfileImage(img.UserTableID, img.ImageUrl, (NewsPortalModel)img.DownloadImageClass).Start();
                        else if (img.DownloadImageClass is PageInfoModel)
                            new ProfileImage(img.UserTableID, img.ImageUrl, (PageInfoModel)img.DownloadImageClass).Start();
                        else if (img.DownloadImageClass is CelebrityModel)
                            new ProfileImage(img.UserTableID, img.ImageUrl, (CelebrityModel)img.DownloadImageClass).Start();
                        else if (img.DownloadImageClass is MusicPageModel)
                            new ProfileImage(img.UserTableID, img.ImageUrl, (MusicPageModel)img.DownloadImageClass).Start();
                    }
                    else if(img.DownloadImageType == SettingsConstants.IMAGE_TYPE_CHAT_PROFILE && img.DownloadImageClass != null)
                    {
                        if (img.DownloadImageClass is MessageModel)
                            new ChatProfileImage(img.UserTableID, img.ImageName, img.ImageUrl, (MessageModel)img.DownloadImageClass).Start();
                    }
                    else if(img.DownloadImageType == SettingsConstants.IMAGE_TYPE_ROOM)
                    {
                        new RoomImage(img.RoomId, img.ImageName, img.ImageUrl).Start();
                    }
                    else if (img.DownloadImageType == SettingsConstants.IMAGE_TYPE_STREAM)
                    {
                        if (img.DownloadImageClass is StreamModel)
                        {
                            new StreamImage(img.ImageName, img.ImageUrl, (StreamModel)img.DownloadImageClass).Start();
                        }
                        else if (img.DownloadImageClass is StreamUserModel)
                        {
                            new StreamImage(img.ImageName, img.ImageUrl, (StreamUserModel)img.DownloadImageClass).Start();
                        }
                        else if (img.DownloadImageClass is WalletGiftProductModel)
                        {
                            new StreamImage(img.ImageName, img.ImageUrl, (WalletGiftProductModel)img.DownloadImageClass).Start();
                        }
                    }
                    else if (img.DownloadImageType == SettingsConstants.IMAGE_TYPE_CHANNEL)
                    {
                        new ChannelImage(img.ImageName, img.ImageUrl, (ChannelModel)img.DownloadImageClass, img.IsProfileImage).Start();
                    }
                    else if (img.DownloadImageType == SettingsConstants.IMAGE_TYPE_ROOM_MEMBER && img.DownloadImageClass != null)
                    {
                        if (img.DownloadImageClass is RoomMemberModel)
                            new RoomMemberImage(img.ImageName, img.ImageUrl, (RoomMemberModel)img.DownloadImageClass).Start();
                    }
                }
            }
        }


       // private class ProfileImageLoadHandler : ILoadImageListener
       // {

            public void onComplete()
            {
                _CURRENT_STATUS = _STATUS_OPEN;
                GetItem();
            }
        //}
    }

}

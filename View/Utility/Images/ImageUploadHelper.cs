﻿using System;
using System.Collections.Generic;
using System.IO;
using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Utility.DataContainer;
using View.ViewModel;
using System.Collections.ObjectModel;
using System.Threading;

namespace View.Utility.Images
{
    public class ImageUploadHelper
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ImageUploadHelper).Name);

        #endregion "Private Fields"

        #region "Utility methods"
        public static int SendProfileCoverImagePostRequestToAuth(string imageServerResponse, int cropW, int cropH, int imageType, string folderLocation, double cropX, double cropY, Guid imgId, int issa)
        {
            int returnCode = ReasonCodeConstants.REASON_CODE_NONE;
            if (!String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                try
                {
                    JObject responseObj = JObject.Parse(imageServerResponse);
                    if (responseObj != null && (bool)responseObj[JsonKeys.Success])
                    {
                        JObject pakToSend = new JObject();
                        pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                        string packetId = SendToServer.GetRanDomPacketID();
                        pakToSend[JsonKeys.PacketId] = packetId;
                        
                        JObject albumDetailsObj = new JObject();
                        JArray imgListArray = new JArray();
                        JObject imgObject = new JObject();
                        if (imgId != Guid.Empty) imgObject[JsonKeys.ImageId] = imgId;                        
                        imgObject[JsonKeys.ImageWidth] = cropW;
                        imgObject[JsonKeys.ImageHeight] = cropH;
                        imgObject[JsonKeys.ImageUrl] = (string)responseObj[JsonKeys.ImageUrl];
                        imgListArray.Add(imgObject);
                        albumDetailsObj[JsonKeys.ImageList] = imgListArray;
                        pakToSend[JsonKeys.AlbumDetails] = albumDetailsObj;
                        
                        if (imageType == SettingsConstants.TYPE_COVER_IMAGE)
                        {
                            pakToSend[JsonKeys.CropImageX] = (int)(cropX);
                            pakToSend[JsonKeys.CropImageY] = (int)cropY;
                        }
                        pakToSend[JsonKeys.Action] = (imageType == SettingsConstants.TYPE_PROFILE_IMAGE) ? AppConstants.TYPE_UPDATE_PROFILE_IMAGE : AppConstants.TYPE_UPDATE_COVER_IMAGE;
                        pakToSend[JsonKeys.WallOwnerType] = SettingsConstants.PROFILE_TYPE_GENERAL;
                        pakToSend[JsonKeys.WallOwnerId] = DefaultSettings.LOGIN_TABLE_ID;
                        pakToSend[JsonKeys.isSameAlbum] = issa;
                        try
                        {
                            JObject feedBackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                            if (feedBackFields != null)
                            {
                                if ((bool)feedBackFields[JsonKeys.Success])
                                {
                                    imgId = (Guid)feedBackFields[JsonKeys.ImageId];
                                    if (imageType == SettingsConstants.TYPE_PROFILE_IMAGE)
                                    {
                                        string previous_image = ImageUrlHelper.GetImageNameFromUrl(DefaultSettings.userProfile.ProfileImage);
                                        string previous_thumb_image = ImageUrlHelper.GetImageNameFromUrl(ImageUrlHelper.GetThumbUrl(DefaultSettings.userProfile.ProfileImage));
                                        string previous_crop_image = ImageUrlHelper.GetImageNameFromUrl(ImageUrlHelper.GetCropUrl(DefaultSettings.userProfile.ProfileImage));
                                        string new_image = ImageUrlHelper.GetImageNameFromUrl((string)responseObj[JsonKeys.ImageUrl]);
                                        string new_thumb_image = ImageUrlHelper.GetImageNameFromUrl(ImageUrlHelper.GetThumbUrl((string)responseObj[JsonKeys.ImageUrl]));
                                        string new_crop_image = ImageUrlHelper.GetImageNameFromUrl(ImageUrlHelper.GetCropUrl((string)responseObj[JsonKeys.ImageUrl]));

                                        DefaultSettings.userProfile.ProfileImage = (string)responseObj[JsonKeys.ImageUrl];
                                        DefaultSettings.userProfile.ProfileImageId = imgId;

                                        ImageUtility.DownloadImage(ImageUrlHelper.GetCropUrl((string)responseObj[JsonKeys.ImageUrl]), folderLocation + Path.DirectorySeparatorChar + new_crop_image);
                                        ImageUtility.DownloadImage(ImageUrlHelper.GetThumbUrl((string)responseObj[JsonKeys.ImageUrl]), folderLocation + Path.DirectorySeparatorChar + new_thumb_image);
                                        RingIDViewModel.Instance.ReloadMyProfile();
                                        ImageUtility.DeleteImageFromLocalDirectory(folderLocation + Path.DirectorySeparatorChar + previous_image);
                                        ImageUtility.DeleteImageFromLocalDirectory(folderLocation + Path.DirectorySeparatorChar + previous_thumb_image);
                                        ImageUtility.DeleteImageFromLocalDirectory(folderLocation + Path.DirectorySeparatorChar + previous_crop_image);


                                    }
                                    else if (imageType == SettingsConstants.TYPE_COVER_IMAGE)
                                    {
                                        string previous_image = ImageUrlHelper.GetImageNameFromUrl(DefaultSettings.userProfile.CoverImage);
                                        string new_image = ImageUrlHelper.GetImageNameFromUrl((string)responseObj[JsonKeys.ImageUrl]);
                                        DefaultSettings.userProfile.CoverImage = (string)responseObj[JsonKeys.ImageUrl];
                                        DefaultSettings.userProfile.CoverImageId = imgId;
                                        DefaultSettings.userProfile.CropImageX = (int)cropX;
                                        DefaultSettings.userProfile.CropImageY = (int)cropY;
                                        if(issa == 1)
                                        {
                                            string crop_image = HelperMethods.ConvertUrlToName(DefaultSettings.userProfile.CoverImage, ImageUtility.IMG_CROP);
                                            ImageUtility.DeleteImageFromLocalDirectory(folderLocation + Path.DirectorySeparatorChar + crop_image);
                                            RingIDViewModel.Instance.ReloadMyProfile();                                            
                                        }
                                        else
                                        {
                                            RingIDViewModel.Instance.ReloadMyProfile();
                                            ImageUtility.DeleteImageFromLocalDirectory(folderLocation + Path.DirectorySeparatorChar + previous_image);
                                        }
                                    }

                                    ImageModel model = ImageDataContainer.Instance.GetFromImageModelDictionary(imgId);
                                    if (model == null)
                                    {
                                        model = new ImageModel(feedBackFields);
                                        ImageDataContainer.Instance.AddOrReplaceImageModels(imgId, model);
                                    }
                                    else
                                    {
                                        model.LoadData(feedBackFields);
                                    }

                                    ObservableCollection<ImageModel> list = new ObservableCollection<ImageModel>();
                                    list.Add(model);
                                    ImageUtility.AddProfileCoverImageToUIList(list, imageType);
                                }
                                else
                                {
                                    returnCode = (int)feedBackFields[JsonKeys.ReasonCode];
                                }
                                RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedBackFields);
                            }
                            else
                            {
                                if (!MainSwitcher.ThreadManager().PingNow())
                                {
                                    returnCode = ReasonCodeConstants.REASON_CODE_INTERNET_UNAVAILABLE;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            returnCode = ReasonCodeConstants.REASON_CODE_INTERNET_UNAVAILABLE;
                        }
                        finally { }
                    }
                    else
                    {
                        returnCode = ReasonCodeConstants.REASON_CODE_IMAGE_SERVER_UPLOADING_FAILED;
                    }
                }
                catch (Exception e)
                {
                    log.Error("Exception in SendProfileCoverImagePostRequestToAuth==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            }
            else
            {
                log.Error("SendProfileCoverImagePostRequestToAuth() Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                returnCode = ReasonCodeConstants.REASON_CODE_INTERNET_UNAVAILABLE;
            }
            return returnCode;
        }

        public static void GetRepositionCropImageUpdate(string responseUrl, string folderLocation, int type)
        {
            if (type == SettingsConstants.TYPE_PROFILE_IMAGE)
            {
                string crop_image = HelperMethods.ConvertUrlToName(responseUrl, ImageUtility.IMG_CROP);
                string thumb_image = HelperMethods.ConvertUrlToName(responseUrl, ImageUtility.IMG_THUMB);

                ImageUtility.DeleteImageFromLocalDirectory(folderLocation + Path.DirectorySeparatorChar + crop_image);
                ImageUtility.DeleteImageFromLocalDirectory(folderLocation + Path.DirectorySeparatorChar + thumb_image);

                ImageUtility.DownloadImage(crop_image.Replace("_", "/"), folderLocation + Path.DirectorySeparatorChar + crop_image);
                ImageUtility.DownloadImage(thumb_image.Replace("_", "/"), folderLocation + Path.DirectorySeparatorChar + thumb_image);
                if (RingIDViewModel.Instance.MyBasicInfoModel != null)
                {
                    RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.OnPropertyChanged("CurrentInstance");
                }
            }
            else if (type == SettingsConstants.TYPE_COVER_IMAGE)
            {
                string crop_image = HelperMethods.ConvertUrlToName(responseUrl, ImageUtility.IMG_CROP);
                ImageUtility.DeleteImageFromLocalDirectory(folderLocation + Path.DirectorySeparatorChar + crop_image);
                ImageUtility.DownloadImage(crop_image.Replace("_", "/"), folderLocation + Path.DirectorySeparatorChar + crop_image);
                if (RingIDViewModel.Instance.MyBasicInfoModel != null) RingIDViewModel.Instance.MyBasicInfoModel.OnPropertyChanged("CurrentInstance");
            }

        }
        #endregion "Utility Methods"


    }
}

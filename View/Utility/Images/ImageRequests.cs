﻿using System;
using System.Threading;
using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Utility.DataContainer;

namespace View.Utility.Images
{
    public class ImageRequests
    {
        #region "Fields"
        #endregion " Fields"

        /// <summary>
        /// >> Send packet to104.193.36.76:9009==>{"pckId":"2110100958171479123945618","sId":"34345269433983462110100958","imgId":823153,"actn":121}
        /// Received for action=121 from Auth JSON==> {"imgId":823152,"iurl":"cloud/uploaded-144/2110044433/1099221479119648295.jpg","cptn":"","ih":720,"iw":1084,"imT":1,"albId":"default","albn":"Feed Photos","tm":1479119661071,"nl":0,"il":0,"ic":0,"nc":0,"sucs":true,"uId":"2110044433","fn":"Faiz 044433","mpvc":3,"rc":0,"utId":37407,"pId":0}
        /// </summary>
        /// <param name="imageID"></param>
        /// This will Fetch ImageDetials From Server
        public bool SingleImageDetailsRequest(Guid imageID)
        {
            JObject pakToSend = new JObject();
            string packetId = SendToServer.GetRanDomPacketID();
            pakToSend[JsonKeys.PacketId] = packetId;
            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            pakToSend[JsonKeys.ImageId] = imageID;
            pakToSend[JsonKeys.Action] = AppConstants.TYPE_IMAGE_DETAILS;

            JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId, true);
            if (feedbackfields != null)
            {
                ImageModel imageModel = ImageDataContainer.Instance.GetFromImageModelDictionary(imageID);
                if (feedbackfields != null && (bool)feedbackfields[JsonKeys.Success] == true)
                {
                    imageModel.LoadData(feedbackfields);
                    if (imageModel.LikeCommentShare == null)
                    {
                        if (imageModel.NewsFeedId != Guid.Empty)
                        {
                            FeedModel fm = null;
                            if (FeedDataContainer.Instance.FeedModels.TryGetValue(imageModel.NewsFeedId, out fm) && fm.SingleMediaFeedModel != null && fm.LikeCommentShare != null)
                            {
                                imageModel.LikeCommentShare = fm.LikeCommentShare;
                            }
                        }
                        if (imageModel.LikeCommentShare == null) imageModel.LikeCommentShare = new LikeCommentShareModel();
                    }
                    if (feedbackfields[JsonKeys.ImageDetails] != null)
                        imageModel.LikeCommentShare.LoadData((JObject)feedbackfields[JsonKeys.ImageDetails]);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// For Feed Details
        /// Send packet to104.193.36.76:9009==>{"pckId":"2110100958181479123945640","sId":"34345269433983462110100958","nfId":920807,"actn":114}
        /// 
        ///Received for action=114 from Auth JSON==> {"seq":"1/1","newsFeedList":[{"nfId":920807,"uId":"2110044433","futId":0,"utId":37407,"sts":"","tm":1479119661071,"at":1479119661071,"type":1,"spType":0,"stp":0,"nc":0,"nl":2,"ns":0,"ac":0,"il":1,"ic":0,"is":0,"fn":"Faiz 044433","afn":"null","prIm":"cloud/uploaded-136/2110044433/484421455356790029.jpg","prImPr":1,"fprImPr":1,"imageList":[{"imgId":823153,"iurl":"cloud/uploaded-144/2110044433/3645061479119655240.jpg","cptn":"","ih":720,"iw":1084,"imT":1,"albId":"default","albn":"Feed Photos","tm":1479119661071,"nl":0,"il":0,"ic":0,"nc":0,"sucs":false,"mpvc":3,"rc":0,"utId":37407,"pId":0},{"imgId":823146,"iurl":"cloud/uploaded-144/2110044433/6857401479119603149.jpg","cptn":"","ih":720,"iw":1084,"imT":1,"albId":"default","albn":"Feed Photos","tm":1479119661071,"nl":0,"il":0,"ic":0,"nc":0,"sucs":false,"mpvc":3,"rc":0,"utId":37407,"pId":0}],"sfId":0,"tim":8,"grpId":0,"actvt":1,"auId":75276,"imc":8,"sc":false,"rc":0,"lnkT":0,"lat":9999.0,"lng":9999.0,"fc":0,"fpvc":3,"svd":false,"ishdn":false,"isuhdn":false,"subCount":0,"edtd":false,"vldt":-1,"stm":0}],"sucs":true,"futId":0,"grpId":0,"scl":0,"lmt":10,"nfId":920807,"rc":18,"mtf":0}
        ///
        /// For Album
        /// </summary>
        /// <param name="currentImageCount"></param>
        /// <param name="totalImages"></param>
        /// <param name="model"></param>
        /// <param name="navigateFrom"></param>
        /// <returns></returns>
        public JArray FetchMoreImagesFromServer(int currentImageCount, int totalImages, ImageModel model, int navigateFrom)
        {
            JArray jarray = null;
            View.Utility.Auth.SendDataToServer.SendMoreFeedImagesRequest(model.NewsFeedId, currentImageCount - 1, totalImages - currentImageCount);
            //if (currentImageCount < totalImages)
            //{
            //    try
            //    {
            //        JObject pakToSend = new JObject();
            //        string pakId = SendToServer.GetRanDomPacketID();
            //        pakToSend[JsonKeys.PacketId] = pakId;
            //        pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;

            //        if (navigateFrom == StatusConstants.NAVIGATE_FROM_FEED)
            //        {
            //            pakToSend[JsonKeys.NewsfeedId] = model.NewsFeedId;
            //            pakToSend[JsonKeys.Action] = AppConstants.TYPE_ACTION_GET_MORE_FEED_IMAGE;
            //            pakToSend[JsonKeys.Limit] = totalImages - currentImageCount;
            //            pakToSend[JsonKeys.Scroll] = 2;
            //        }
            //        else
            //        {
            //            pakToSend[JsonKeys.Action] = AppConstants.TYPE_MY_ALBUM_IMAGES;
            //            if (model.UserTableID > 0 && model.UserTableID != DefaultSettings.LOGIN_TABLE_ID)
            //            {
            //                pakToSend[JsonKeys.FutId] = model.UserTableID;
            //            }
            //            string albumID = Models.Utility.HelperMethodsModel.GetAlbumIDFromType(model.ImageType);
            //            if (!string.IsNullOrEmpty(albumID))
            //            {
            //                pakToSend[JsonKeys.AlbumId] = albumID;
            //            }
            //        }
            //        pakToSend[JsonKeys.StartLimit] = currentImageCount - 1;
            //        JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakId, true);
            //        if (navigateFrom == StatusConstants.NAVIGATE_FROM_FEED)
            //        {
            //            int waitingTime = 0;
            //            if (feedbackfields[JsonKeys.NewsFeedList] == null)
            //            {
            //                while (feedbackfields[JsonKeys.NewsFeedList] == null)
            //                {
            //                    if (waitingTime == DefaultSettings.TRYING_TIME)
            //                    {
            //                        break;
            //                    }
            //                    Thread.Sleep(DefaultSettings.WAITING_TIME);
            //                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out feedbackfields);
            //                    waitingTime++;
            //                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success] && feedbackfields[JsonKeys.NewsFeedList] != null)
            //                    {
            //                        jarray = (JArray)feedbackfields[JsonKeys.NewsFeedList];
            //                        break;
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                jarray = (JArray)feedbackfields[JsonKeys.NewsFeedList];
            //            }
            //        }
            //    }
            //    finally { }
            //}
            return jarray;
        }

        public bool CommentReqeust(Guid newsFeedId, Guid imageID, Guid pivotId, long time, int scroll)
        {
            bool value = false;
            try
            {
                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                if (scroll > 0)
                {
                    pakToSend[JsonKeys.Scroll] = scroll;
                    pakToSend[JsonKeys.Time] = time;
                }
                if (pivotId != Guid.Empty)
                    pakToSend[JsonKeys.PivotUUID] = pivotId;
                //else pakToSend[JsonKeys.LogStartLimit] = startLimit;
                if (newsFeedId != Guid.Empty)
                    pakToSend[JsonKeys.NewsfeedId] = newsFeedId;
                pakToSend[JsonKeys.Action] = AppConstants.ACTION_MERGED_COMMENTS_LIST;
                if (imageID != Guid.Empty)
                {
                    pakToSend[JsonKeys.ContentId] = imageID;
                    pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_IMAGE;
                }
                else
                    pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_STATUS;


                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId, true);
                if (feedbackfields != null)
                {
                    value = true;
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
            }
            finally { }
            return value;
        }
    }
}

﻿using log4net;
using Models.Constants;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using View.BindingModels;
using View.Constants;

namespace View.Utility.Images
{
    class ChatProfileImage : IRingImage
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(ChatProfileImage).Name);

        private long _UserID;
        private string _ImageUrl;
        private string _ImageName;
        private MessageModel messageModel;
        private BackgroundWorker bgworker = null;

        public ChatProfileImage(long userId, string imageName, string imageUrl, MessageModel messageModel)
        {
            this._UserID = userId;
            this._ImageName = imageName;
            this._ImageUrl = imageUrl;
            this.messageModel = messageModel;


            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName, ServerAndPortSettings.IMAGE_SERVER_RESOURCE + Path.AltDirectorySeparatorChar);
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeImage();
                }
                ImageDictionaries.Instance.RING_IMAGE_QUEUE.onComplete();
            };
        }

        public void Start()
        {
            bgworker.RunWorkerAsync();
        }

        public void ChangeImage()
        {
            try
            {
                if (messageModel != null)
                {
                    messageModel.OnPropertyChanged("CurrentInstance");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeImage()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

    }
}

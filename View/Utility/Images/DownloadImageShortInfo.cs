﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace View.Utility.Images
{
    public class DownloadImageShortInfo
    {
        #region property
        public string ImageName { get; set; }
        public string ImageUrl { get; set; }
        public long UserTableID { get; set; }        
        public object DownloadImageClass { get; set; }
        public int DownloadImageType { get; set; }
        public string RoomId { get; set; }
        public string StreamId { get; set; }
        public bool IsProfileImage { get; set; }
        #endregion
        
        #region Constructors
        public DownloadImageShortInfo(long utID, string imgUrl, object downloadImgClass, int downloadImgType)
        {
            this.UserTableID = utID;            
            this.ImageUrl = imgUrl;
            this.DownloadImageClass = downloadImgClass;
            this.DownloadImageType = downloadImgType;
        }
        public DownloadImageShortInfo(long utID, string imgName, string imgUrl, object downloadImgClass, int downloadImgType)
        {
            this.UserTableID = utID;
            this.ImageName = imgName;
            this.ImageUrl = imgUrl;
            this.DownloadImageClass = downloadImgClass;
            this.DownloadImageType = downloadImgType;
        }
        public DownloadImageShortInfo(string roomId, string imgName, string imgUrl, int downloadImgType)
        {
            this.RoomId = roomId;
            this.ImageName = imgName;
            this.ImageUrl = imgUrl;            
            this.DownloadImageType = downloadImgType;
        }
        public DownloadImageShortInfo(string imgName, string imgUrl, object downloadImgClass, int downloadImgType)
        {            
            this.ImageName = imgName;
            this.ImageUrl = imgUrl;
            this.DownloadImageClass = downloadImgClass;
            this.DownloadImageType = downloadImgType;
        }
        public DownloadImageShortInfo(string imgName, string imgUrl, object downloadImgClass, int downloadImgType, bool isProfileImage)
        {
            this.ImageName = imgName;
            this.ImageUrl = imgUrl;
            this.DownloadImageClass = downloadImgClass;
            this.DownloadImageType = downloadImgType;
            this.IsProfileImage = isProfileImage;
        }
        #endregion
    }
}

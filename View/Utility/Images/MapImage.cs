﻿using log4net;
using System;
using System.ComponentModel;
using System.Drawing;
using View.BindingModels;

namespace View.Utility.Images
{
    public class MapImage : IRingImage
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(MapImage).Name);
        private string _filePath;
        //private Bitmap _BitmapImage;
        private LocationModel locationModel;
        private BackgroundWorker bgworker = null;


        public MapImage(LocationModel locationModel, string filePath)
        {
            this.locationModel = locationModel;
            this._filePath = filePath;

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                //_BitmapImage = ImageUtility.DownloadDoingImage(locationModel.imgUrl, _filePath);
                //e.Result = _BitmapImage;
                e.Result = ImageUtility.SaveImageLocallyFromURL(locationModel.ImageUrl, _filePath);
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeLocationMap(false);
                }
                else
                {
                    ChangeLocationMap(true);
                }
            };

        }

        public void Start()
        {
            bgworker.RunWorkerAsync();
        }

        private void ChangeLocationMap(bool _NoImageFound)
        {
            try
            {
                if (locationModel != null && !_NoImageFound)
                {
                    locationModel.OnPropertyChanged("CurrentInstance");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeFeedImage()." + ex.Message + "\n" + ex.StackTrace);
            }
        }
    }
}

﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using View.BindingModels;
using View.Constants;

namespace View.Utility.Images
{
    class CommentImage
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CommentImage).Name);

        CommentModel commentModel;
        private string _ImageUrl;
        private string _ImageName;

        private BackgroundWorker bgworker = null;


        public CommentImage(CommentModel commentModel, string convertedUrl)
        {
            this.commentModel = commentModel;
            this._ImageName = convertedUrl;

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                if (this.commentModel.UrlType == SettingsConstants.UPLOADTYPE_STICKER)
                {
                    this._ImageUrl = commentModel.MediaOrImageUrl;
                    e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.STICKER_FOLDER + Path.DirectorySeparatorChar + _ImageUrl.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar), ServerAndPortSettings.StickerMarketDownloadUrl + "/dfull/");
                }
                else
                {
                    this._ImageUrl = commentModel.MediaOrImageUrl.Replace("_", "/");
                    e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_COMMENT_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
                }
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeFeedImage(false);
                }
                else
                {
                    ChangeFeedImage(true);
                }
            };
        }

        public void Start()
        {
            bgworker.RunWorkerAsync();
        }

        private void ChangeFeedImage(bool _NoImageFound)
        {
            try
            {
                if (commentModel != null && !_NoImageFound)
                {
                    commentModel.OnPropertyChanged("CurrentInstance");
                }

            }

            catch (Exception ex)
            {
                log.Error("Error: ChangeFeedImage()." + ex.Message + "\n" + ex.StackTrace);
            }
        }
    }
}

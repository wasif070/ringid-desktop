﻿using Models.Constants;
using View.BindingModels;
using View.Constants;
using View.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using View.UI.Profile.FriendProfile;
using View.Dictonary;
using log4net;
using View.UI;
using View.Utility.DataContainer;

namespace View.Utility.Images
{
    class AlbumImage : IRingImage
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AlbumImage).Name);

        private long _friendTableId;
        private Guid _imageId;
        private string _ImageUrl;
        private string _ImageName;
        private int _ImageType;
        //private Bitmap _BitmapImage;
        private BackgroundWorker bgworker = null;

        public AlbumImage(long friendTableId, Guid imageId, string imageUrl, int imageType)
        {
            this._friendTableId = friendTableId;
            this._imageId = imageId;
            this._ImageName = imageUrl;
            this._ImageType = imageType;
            this._ImageUrl = imageUrl.Replace("_", "/");

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {
                //if (this._ImageType == SettingsConstants.TYPE_NORMAL_BOOK_IMAGE)
                //{
                //    e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_BOOK_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
                //}
                if (this._ImageType == SettingsConstants.TYPE_PROFILE_IMAGE)
                {
                    e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
                }
                else if (this._ImageType == SettingsConstants.TYPE_COVER_IMAGE)
                {
                    e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_COVER_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
                }
                else
                {
                    e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_BOOK_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
                }

                //e.Result = _BitmapImage;
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeAlbumImage(false);
                }
                else
                {
                    ChangeAlbumImage(true);
                }
            };
        }

        /*public AlbumImage(long imageId, string imageUrl)
        {

            this._imageId = imageId;
            this._ImageName = imageUrl;
            this._ImageUrl = imageUrl.Replace("_", "/");

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {

                e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_BOOK_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
                //e.Result = _BitmapImage;
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeAlbumImage(false);
                }
                else
                {
                    ChangeAlbumImage(true);
                }
            };

        }*/

        public void Start()
        {
            bgworker.RunWorkerAsync();
        }

        private void ChangeAlbumImage(bool _NoImageFound)
        {
            try
            {
                if (this._friendTableId > 0)
                {
                    UCFriendProfile ucFriendProfile = null;
                    if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._FriendTableID == this._friendTableId)
                    {
                        ucFriendProfile = UCMiddlePanelSwitcher.View_UCFriendProfilePanel;
                    }

                    ImageModel model = ImageDataContainer.Instance.GetFromImageModelDictionary(_imageId);
                    if (model != null)
                    {
                        if (_NoImageFound)
                        {
                            model.NoImageFound = _NoImageFound;
                        }
                        model.IsIMG300Downloading = false;
                        model.OnPropertyChanged("CurrentInstance");
                        if (ucFriendProfile != null && ucFriendProfile.PreviewPhotoID == model.ImageId)
                        {
                            ucFriendProfile.OnPropertyChanged("PreviewPhotoID");
                        }
                    }
                }
                else
                {
                    ImageModel model = ImageDataContainer.Instance.GetFromImageModelDictionary(_imageId);
                    if (model != null)
                    {
                        if (_NoImageFound)
                        {
                            model.NoImageFound = _NoImageFound;
                        }
                        model.IsIMG300Downloading = false;
                        model.OnPropertyChanged("CurrentInstance");

                        if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile.PreviewPhotoID == model.ImageId)
                        {
                            UCMiddlePanelSwitcher.View_UCMyProfile.OnPropertyChanged("PreviewPhotoID");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeAlbumImage()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        //private void ChangeAlbumImage(bool _NoImageFound)
        //{
        //    try
        //    {
        //        if (this._friendTableId > 0)
        //        {
        //            UCFriendProfile ucFriendProfile = null;
        //            UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.TryGetValue(this._friendTableId, out ucFriendProfile);

        //            ImageModel model;

        //            if (ucFriendProfile != null)
        //            {
        //                lock (ucFriendProfile.FriendProfileImageList)
        //                {
        //                    model = ucFriendProfile.FriendProfileImageList.Where(P => P.ImageId == _imageId).FirstOrDefault();
        //                }

        //                if (model == null && ucFriendProfile != null)
        //                {
        //                    lock (ucFriendProfile.FriendCoverImageList)
        //                    {
        //                        model = ucFriendProfile.FriendCoverImageList.Where(P => P.ImageId == _imageId).FirstOrDefault();
        //                    }
        //                }

        //                if (model == null && ucFriendProfile != null)
        //                {
        //                    lock (ucFriendProfile.FriendFeedImageList)
        //                    {
        //                        model = ucFriendProfile.FriendFeedImageList.Where(P => P.ImageId == _imageId).FirstOrDefault();
        //                    }
        //                }

        //                if (model != null)
        //                {
        //                    if (_NoImageFound)
        //                    {
        //                        model.NoImageFound = _NoImageFound;
        //                    }
        //                    model.OnPropertyChanged("CurrentInstance");
        //                    if (ucFriendProfile != null && ucFriendProfile.FriendFeedImageList != null)
        //                    {
        //                        ucFriendProfile.OnPropertyChanged("FriendFeedImageList");
        //                    }
        //                }
        //            }
        //            if (UCMiddlePanelSwitcher.View_CelebrityProfile != null && UCMiddlePanelSwitcher.View_CelebrityProfile.CelebrityUserTableID == this._friendTableId)
        //            {
        //                lock (UCMiddlePanelSwitcher.View_CelebrityProfile._UCCelebrityPhotos.CelebFeedImageList)
        //                {
        //                    model = UCMiddlePanelSwitcher.View_CelebrityProfile._UCCelebrityPhotos.CelebFeedImageList.Where(P => P.ImageId == _imageId).FirstOrDefault();
        //                }
        //                if (model != null)
        //                {
        //                    if (_NoImageFound)
        //                    {
        //                        model.NoImageFound = _NoImageFound;
        //                    }
        //                    model.OnPropertyChanged("CurrentInstance");
        //                }
        //            }
        //        }
        //        else
        //        {
        //            ImageModel model = RingIDViewModel.Instance.GetImageModelByID(_imageId);
        //            if (model != null)
        //            {
        //                if (_NoImageFound)
        //                {
        //                    model.NoImageFound = _NoImageFound;
        //                }
        //                model.OnPropertyChanged("CurrentInstance");

        //                //UCFriendProfile ucFriendProfile = null;
        //                // UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.TryGetValue(this._friendIdentity, out ucFriendProfile);

        //                bool myFeedImageFound = true;
        //                foreach (UCFriendProfile ucFriendProfile in UIDictionaries.Instance.FRIEND_PROFILE_DICTIONARY.Values)
        //                {
        //                    if (ucFriendProfile.FriendFeedImageList != null && ucFriendProfile.FriendFeedImageList.Any(P => P.ImageId == model.ImageId))
        //                    {
        //                        ucFriendProfile.OnPropertyChanged("FriendFeedImageList");
        //                        myFeedImageFound = false;
        //                    }
        //                }
        //                if (myFeedImageFound && UCMiddlePanelSwitcher.View_UCMyProfile != null)
        //                {
        //                    RingIDViewModel.Instance.OnPropertyChanged("FeedImageList");
        //                }

        //                //if (UCFriendProfile.Instance != null && UCFriendProfile.Instance.FriendFeedImageList != null)
        //                //{
        //                //    UCFriendProfile.Instance.OnPropertyChanged("FriendFeedImageList");
        //                //}
        //                //else if (UCMiddlePanelSwitcher.View_UCMyProfile != null)
        //                //{
        //                //    RingIDViewModel.Instance.OnPropertyChanged("FeedImageList");
        //                //}
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("Error: ChangeAlbumImage()." + ex.Message + "\n" + ex.StackTrace);
        //    }
        //}
    }
}

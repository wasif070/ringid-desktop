﻿using Models.Constants;
using View.BindingModels;
using View.Constants;
using View.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace View.Utility.Images
{
    class CoverImage : IRingImage
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CoverImage).Name);

        private long _UserTableId;
        private string _ImageUrl;
        private string _ImageName;
        //private Bitmap _BitmapImage;
        private BackgroundWorker bgworker = null;
        private NewsPortalModel newsPortal;
        private PageInfoModel pageInfoModel;
        private MusicPageModel musicPageModel;
        public CoverImage(long userTableId, string imageUrl)
        {

            this._UserTableId = userTableId;
            this._ImageName = imageUrl;
            this._ImageUrl = imageUrl.Replace("_", "/");

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {

                e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_COVER_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
                //e.Result = _BitmapImage;
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeCoverImage();
                }
            };

        }
        public CoverImage(NewsPortalModel newsPortal, string imageUrl)
        {

            this.newsPortal = newsPortal;
            this._ImageName = imageUrl;
            this._ImageUrl = imageUrl.Replace("_", "/");

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {

                e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_COVER_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
                //e.Result = _BitmapImage;
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeCoverImage();
                }
            };

        }
        public CoverImage(MusicPageModel musicPageModel, string imageUrl)
        {

            this.musicPageModel = musicPageModel;
            this._ImageName = imageUrl;
            this._ImageUrl = imageUrl.Replace("_", "/");

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {

                e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_COVER_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
                //e.Result = _BitmapImage;
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeCoverImage();
                }
            };

        }
        public CoverImage(PageInfoModel pageInfoModel, string imageUrl)
        {

            this.pageInfoModel = pageInfoModel;
            this._ImageName = imageUrl;
            this._ImageUrl = imageUrl.Replace("_", "/");

            bgworker = new BackgroundWorker();
            bgworker.DoWork += (s, e) =>
            {

                e.Result = ImageUtility.DownloadImage(_ImageUrl, RingIDSettings.TEMP_COVER_IMAGE_FOLDER + Path.DirectorySeparatorChar + _ImageName);
                //e.Result = _BitmapImage;
            };
            bgworker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is bool && (bool)e.Result)
                {
                    ChangeCoverImage();
                }
            };

        }
        public void Start()
        {
            bgworker.RunWorkerAsync();
        }

        public void ChangeCoverImage()
        {
            try
            {
                if (newsPortal != null)
                {
                    newsPortal.OnPropertyChanged("CurrentInstance");
                }
                else if (pageInfoModel != null)
                {
                    pageInfoModel.OnPropertyChanged("CurrentInstance");
                }
                else if (musicPageModel != null)
                {
                    musicPageModel.OnPropertyChanged("CurrentInstance");
                }
                else
                {
                    if (DefaultSettings.LOGIN_TABLE_ID.Equals(this._UserTableId))
                    {
                        RingIDViewModel.Instance.MyBasicInfoModel.OnPropertyChanged("CurrentInstance");
                    }
                    else
                    {
                        UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(_UserTableId);
                        if (model != null)
                        {
                            model.OnPropertyChanged("CurrentInstance");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeCoverImage()." + ex.Message + "\n" + ex.StackTrace);
            }

        }

    }
}

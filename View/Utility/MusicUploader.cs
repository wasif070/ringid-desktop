<<<<<<< HEAD
﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Auth.Service.Images;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.ViewModel.NewStatus;

namespace View.UI.Feed
{
    class MusicUploader
    {
        private readonly ILog log = LogManager.GetLogger(typeof(MusicUploader).Name);
        private NewStatusViewModel newStatusViewModel;
        private WebClient _WebClient = null;
        private bool _SingleMusicUploadFailed = false;
        private List<MusicUploaderModel> _MusicUploaderList;
        private JObject _MediaJobject = null;
        private UploadingModel _uploadingModel = new UploadingModel();
        private long TotalFileSize = 0;
        private long TotalUploaded = 0;

        public MusicUploader(NewStatusViewModel newStatusViewModel)
        {
            this.newStatusViewModel = newStatusViewModel;
            this._SingleMusicUploadFailed = false;
            this._MusicUploaderList = new List<MusicUploaderModel>();
            this.newStatusViewModel.EnablePostbutton(false);
            this._WebClient = new WebClient();
            this._WebClient.UploadProgressChanged += new UploadProgressChangedEventHandler(UploadProgressChanged);
            this._WebClient.UploadDataCompleted += new UploadDataCompletedEventHandler(UploadDataCompleted);
            this._MusicUploaderList.AddRange(newStatusViewModel.NewStatusMusicUpload);

            RingIDViewModel.Instance.UploadingModelList.InvokeAdd(_uploadingModel);
            _uploadingModel.UploadingContent = _MusicUploaderList.Count > 1 ? _MusicUploaderList.Count + " audios are uploading..." : _MusicUploaderList.Count + " audio is uploading...";
            for (int i = 0; i < _MusicUploaderList.Count; i++)
            {
                TotalFileSize += _MusicUploaderList[i].FileSize;
            }

            new Thread(new ThreadStart(BeginToMusicUpload)).Start();
        }

        private void BeginToMusicUpload()
        {
            try
            {
                if (_MusicUploaderList.Count > 0)
                {
                    if (!_SingleMusicUploadFailed)
                    {
                        MusicUploaderModel model = _MusicUploaderList.FirstOrDefault();
                        if (string.IsNullOrEmpty(model.StreamUrl))
                        {
                            UploadToAudioServer(model);
                        }
                        else
                        {
                            _MusicUploaderList.Remove(model);
                            model.IsUploadedInAudioServer = true;
                            BeginToMusicUpload();
                        }
                    }
                    else
                    {
                        newStatusViewModel.EnablePostbutton(true);
                        UIHelperMethods.ShowFailed("Failed to upload audio!", "Audio upload");
                        if (newStatusViewModel.ucMediaCloudUploadPopup != null) newStatusViewModel.ucMediaCloudUploadPopup.LoadStatesInUI(1);
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                        });
                        HideUploadingPopup();
                    }
                }
                else
                {
                    if (!_SingleMusicUploadFailed)
                    {
                        HideUploadingPopup();
                        _MediaJobject = new JObject();
                        LoadMusicUploaderModeltoMediaContentDTO();
                        LoadAlbumIdNameinMediaContentDTO();
                        new ThreadAddFeed().StartThread(newStatusViewModel, SettingsConstants.MEDIA_TYPE_AUDIO, _MediaJobject);
                        if (this._WebClient != null)
                        {
                            this._WebClient.Dispose();
                        }
                    }
                }
            }
            catch (Exception)
            {
                newStatusViewModel.EnablePostbutton(true);
                UIHelperMethods.ShowFailed("Failed to upload Music!", "Music upload");
                if (newStatusViewModel.ucMediaCloudUploadPopup != null) newStatusViewModel.ucMediaCloudUploadPopup.LoadStatesInUI(1);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                });
                HideUploadingPopup();
                if (this._WebClient != null)
                {
                    this._WebClient.Dispose();
                }
            }
        }

        private void HideUploadingPopup()
        {
            RingIDViewModel.Instance.UploadingModelList.InvokeRemove(_uploadingModel);
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (MainSwitcher.PopupController.ucUploadingPopup != null && RingIDViewModel.Instance.UploadingModelList.Count == 0)
                {
                    MainSwitcher.PopupController.ucUploadingPopup.popupUploading.IsOpen = false;
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        private void LoadMusicUploaderModeltoMediaContentDTO()
        {
            JArray hashtagsToSend = null;
            if (newStatusViewModel.NewStatusHashTag.Count > 0)
            {
                hashtagsToSend = new JArray();
                foreach (HashTagModel hashTagModel in newStatusViewModel.NewStatusHashTag)
                {
                    JObject obj = new JObject();
                    if (hashTagModel.HashTagSearchKey.Length == 0) continue;
                    hashtagsToSend.Add(hashTagModel.HashTagSearchKey);
                }
                _MediaJobject[JsonKeys.HashTagList] = hashtagsToSend;
            }
            _MediaJobject[JsonKeys.MediaType] = 1;
            JArray mediasToSend = new JArray();
            bool IsAlbumUrlPicked = false;
            foreach (var singleMediaDTO in newStatusViewModel.NewStatusMusicUpload)
            {
                JObject obj = new JObject();
                obj[JsonKeys.StreamUrl] = singleMediaDTO.StreamUrl;
                if (!string.IsNullOrEmpty(singleMediaDTO.ThumbUrl))
                {
                    obj[JsonKeys.ThumbUrl] = singleMediaDTO.ThumbUrl;
                    obj[JsonKeys.ThumbImageWidth] = singleMediaDTO.ImageWidth;
                    obj[JsonKeys.ThumbImageHeight] = singleMediaDTO.ImageHeight;
                    if (!IsAlbumUrlPicked) { _MediaJobject[JsonKeys.MediaAlbumImageURL] = singleMediaDTO.ThumbUrl; IsAlbumUrlPicked = true; }
                }
                obj[JsonKeys.Title] = singleMediaDTO.AudioTitle;
                obj[JsonKeys.MediaPrivacy] = newStatusViewModel.PrivacyValue;
                if (!string.IsNullOrEmpty(singleMediaDTO.AudioArtist)) obj[JsonKeys.Artist] = singleMediaDTO.AudioArtist;
                obj[JsonKeys.MediaDuration] = singleMediaDTO.AudioDuration;
                if (hashtagsToSend != null) obj[JsonKeys.HashTagList] = hashtagsToSend;
                mediasToSend.Add(obj);
            }
            _MediaJobject[JsonKeys.MediaList] = mediasToSend;
        }

        private void LoadAlbumIdNameinMediaContentDTO()
        {
            string albumName = "";
            albumName = newStatusViewModel.AudioAlbumTitleTextBoxText.Trim();

            if (!string.IsNullOrEmpty(albumName))
            {
                MediaContentModel mediaContentModel = RingIDViewModel.Instance.MyAudioAlbums.Where(x => x.AlbumName == albumName).FirstOrDefault();
                _MediaJobject[JsonKeys.AlbumId] = mediaContentModel != null ? mediaContentModel.AlbumId : Guid.Empty;
                _MediaJobject[JsonKeys.AlbumName] = albumName;
            }
        }
        private byte[] wholebytes;
        private void UploadToAudioServer(MusicUploaderModel model)
        {
            try
            {
                string _lineEnd = "\r\n";
                string _twoHyphens = "--";
                string _boundary = "*****";

                _WebClient.Headers["Content-Type"] = "multipart/form-data; boundary=" + _boundary;
                _WebClient.Headers["User-Agent"] = "ringID" + "_" + DefaultSettings.VALUE_APP_INSTALLED_VERSION;
                _WebClient.Headers["access-control-allow-origin"] = "*";

                var firstPart = "";
                string fileheaderTemplate = "Content-Disposition: form-data; name=\"{0}\"" + _lineEnd + _lineEnd + "{1}" + _lineEnd;

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "sId", DefaultSettings.LOGIN_SESSIONID);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "uId", DefaultSettings.LOGIN_RING_ID);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "authServer", ServerAndPortSettings.AUTH_SERVER_IP);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "comPort", ServerAndPortSettings.COMMUNICATION_PORT);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "x-app-version", AppConfig.VERSION_PC);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += "Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + Path.GetFileName(model.FilePath) + "\"" + _lineEnd;
                firstPart += _lineEnd;

                byte[] firstBytes = _WebClient.Encoding.GetBytes(firstPart);

                var lastPart = "";
                lastPart += _lineEnd;
                lastPart += _twoHyphens + _boundary + _twoHyphens + _lineEnd;

                byte[] lastBytes = _WebClient.Encoding.GetBytes(lastPart);

                FileStream oFileStream = new FileStream(model.FilePath, FileMode.Open, FileAccess.Read);
                int fileLength = (int)oFileStream.Length;
                long length = firstBytes.Length + fileLength + lastBytes.Length;
                wholebytes = new Byte[length];
                int currentbytes = 0;

                /*New Length including header*/
                TotalFileSize -= model.FileSize;
                model.FileSize = length;
                TotalFileSize += model.FileSize;

                Buffer.BlockCopy(firstBytes, 0, wholebytes, currentbytes, firstBytes.Length);
                currentbytes += firstBytes.Length;

                oFileStream.Read(wholebytes, currentbytes, fileLength);
                currentbytes += fileLength;

                oFileStream.Close();

                Buffer.BlockCopy(lastBytes, 0, wholebytes, currentbytes, lastBytes.Length);
                currentbytes += lastBytes.Length;

                model.IsUploading = true;
                _WebClient.UploadDataAsync(new Uri(ServerAndPortSettings.GetAudioUploadingURL), "POST", wholebytes, model);
                if (wholebytes != null) GC.SuppressFinalize(wholebytes);
                wholebytes = null;
            }
            catch (Exception ex)
            {
                if (wholebytes != null) GC.SuppressFinalize(wholebytes);
                wholebytes = null;
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
                if (_WebClient != null) GC.SuppressFinalize(_WebClient);
                _WebClient = null;
                _SingleMusicUploadFailed = true;
                model.IsUploading = false;
                newStatusViewModel.EnablePostbutton(true);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                });
                UIHelperMethods.ShowFailed("Failed to upload audio!", "Audio upload");
                HideUploadingPopup();
            }
        }

        private void UploadDataCompleted(Object sender, UploadDataCompletedEventArgs e)
        {
            try
            {
                MusicUploaderModel model = (MusicUploaderModel)e.UserState;

                if (e.Result != null && e.Result.Length > 0 && (int)e.Result[0] == 1)
                {
                    TotalUploaded += model.FileSize;
                    int urlLength = (int)e.Result[1];
                    model.StreamUrl = System.Text.Encoding.UTF8.GetString(e.Result, 2, urlLength);

                    _MusicUploaderList.Remove(model);

                    if (!string.IsNullOrEmpty(model.StreamUrl) && model.IsImageFound && model.ImageWidth > 0 && model.ImageHeight > 0)
                    {
                        ThumbImageUpload(model);
                    }
                    else
                    {
                        model.IsUploading = false;
                        model.IsUploadedInAudioServer = true;
                        BeginToMusicUpload();
                    }
                }
                else
                {
                    _SingleMusicUploadFailed = true;
                    newStatusViewModel.EnablePostbutton(true);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                    });
                    UIHelperMethods.ShowFailed("Failed to upload audio!", "Audio upload");
                    HideUploadingPopup();
                    if (newStatusViewModel.ucMediaCloudUploadPopup != null) newStatusViewModel.ucMediaCloudUploadPopup.LoadStatesInUI(1);
                }
            }
            catch (Exception)
            {
                _SingleMusicUploadFailed = true;
                newStatusViewModel.EnablePostbutton(true);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                });
                if (newStatusViewModel.ucMediaCloudUploadPopup != null) newStatusViewModel.ucMediaCloudUploadPopup.LoadStatesInUI(1);
            }
        }

        private void UploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {
            try
            {
                MusicUploaderModel obj = (MusicUploaderModel)e.UserState;
                obj.Pecentage = (int)(((double)e.BytesSent / (double)e.TotalBytesToSend) * 100);
                _uploadingModel.TotalUploadedInPercentage = (int)(((double)(e.BytesSent + TotalUploaded) / (double)TotalFileSize) * 100);
            }
            catch (Exception)
            {
                _SingleMusicUploadFailed = true;
                newStatusViewModel.EnablePostbutton(true);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                });
                if (newStatusViewModel.ucMediaCloudUploadPopup != null) newStatusViewModel.ucMediaCloudUploadPopup.LoadStatesInUI(1);
            }
        }

        private async void ThumbImageUpload(MusicUploaderModel model)
        {
            try
            {
                TagLib.File file = TagLib.File.Create(model.FilePath);

                byte[] response = await FeedImagesUpload.Instance.UploadThumbImageToImageServerWebRequest(file.Tag.Pictures[0].Data.Data, model.ImageWidth, model.ImageHeight);

                if (response != null)
                {
                    if (response[0] == 1)
                    {
                        int len = response[1];
                        string ArtImageUrl = Encoding.UTF8.GetString(response, 2, len);
                        model.ThumbUrl = ArtImageUrl;
                    }
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                model.IsUploading = false;
                model.IsUploadedInAudioServer = true;
                BeginToMusicUpload();
            }

        }
    }
}
=======
﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Auth.Service.Images;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.UI.PopUp;
using View.Utility;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.ViewModel.NewStatus;

namespace View.UI.Feed
{
    class MusicUploader
    {
        private readonly ILog log = LogManager.GetLogger(typeof(MusicUploader).Name);
        private NewStatusViewModel newStatusViewModel;
        private WebClient webClient = null;
        private bool singleMusicUploadFailed = false;
        private List<MusicUploaderModel> musicUploaderList;
        private JObject mediaJobject = null;
        private UploadingModel uploadingModel = new UploadingModel();
        private long totalFileSize = 0;
        private long totalUploaded = 0;
        private byte[] wholebytes;

        public MusicUploader(NewStatusViewModel newStatusViewModel)
        {
            this.newStatusViewModel = newStatusViewModel;
            this.singleMusicUploadFailed = false;
            this.musicUploaderList = new List<MusicUploaderModel>();
            this.newStatusViewModel.EnablePostbutton(false);
            this.webClient = new WebClient();
            this.webClient.UploadProgressChanged += new UploadProgressChangedEventHandler(UploadProgressChanged);
            this.webClient.UploadDataCompleted += new UploadDataCompletedEventHandler(UploadDataCompleted);
            this.musicUploaderList.AddRange(newStatusViewModel.NewStatusMusicUpload);

            RingIDViewModel.Instance.UploadingModelList.InvokeAdd(uploadingModel);
            uploadingModel.UploadingContent = musicUploaderList.Count > 1 ? musicUploaderList.Count + " audios are uploading..." : musicUploaderList.Count + " audio is uploading...";
            for (int i = 0; i < musicUploaderList.Count; i++)
            {
                totalFileSize += musicUploaderList[i].FileSize;
            }
            new Thread(new ThreadStart(BeginToMusicUpload)).Start();
        }

        private void BeginToMusicUpload()
        {
            try
            {
                if (musicUploaderList.Count > 0)
                {
                    if (!singleMusicUploadFailed)
                    {
                        MusicUploaderModel model = musicUploaderList.FirstOrDefault();
                        if (string.IsNullOrEmpty(model.StreamUrl))
                        {
                            UploadToAudioServer(model);
                        }
                        else
                        {
                            musicUploaderList.Remove(model);
                            model.IsUploadedInAudioServer = true;
                            BeginToMusicUpload();
                        }
                    }
                    else
                    {
                        newStatusViewModel.EnablePostbutton(true);
                        UIHelperMethods.ShowFailed("Failed to upload audio!", "Audio upload");
                        if (newStatusViewModel.ucMediaCloudUploadPopup != null) newStatusViewModel.ucMediaCloudUploadPopup.LoadStatesInUI(1);
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                        });
                        HideUploadingPopup();
                    }
                }
                else
                {
                    if (!singleMusicUploadFailed)
                    {
                        HideUploadingPopup();
                        mediaJobject = new JObject();
                        LoadMusicUploaderModeltoMediaContentDTO();
                        LoadAlbumIdNameinMediaContentDTO();
                        new ThreadAddFeed().StartThread(newStatusViewModel, SettingsConstants.MEDIA_TYPE_AUDIO, mediaJobject);
                        if (this.webClient != null)
                        {
                            this.webClient.Dispose();
                        }
                    }
                }
            }
            catch (Exception)
            {
                newStatusViewModel.EnablePostbutton(true);
                UIHelperMethods.ShowFailed("Failed to upload Music!", "Music upload");
                if (newStatusViewModel.ucMediaCloudUploadPopup != null) newStatusViewModel.ucMediaCloudUploadPopup.LoadStatesInUI(1);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                });
                HideUploadingPopup();
                if (this.webClient != null)
                {
                    this.webClient.Dispose();
                }
            }
        }

        private void HideUploadingPopup()
        {
            RingIDViewModel.Instance.UploadingModelList.InvokeRemove(uploadingModel);
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (MainSwitcher.PopupController.ucUploadingPopup != null && RingIDViewModel.Instance.UploadingModelList.Count == 0)
                {
                    MainSwitcher.PopupController.ucUploadingPopup.popupUploading.IsOpen = false;
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        private void LoadMusicUploaderModeltoMediaContentDTO()
        {
            JArray hashtagsToSend = null;
            if (newStatusViewModel.NewStatusHashTag.Count > 0)
            {
                hashtagsToSend = new JArray();
                foreach (HashTagModel hashTagModel in newStatusViewModel.NewStatusHashTag)
                {
                    JObject obj = new JObject();
                    if (hashTagModel.HashTagSearchKey.Length == 0) continue;
                    hashtagsToSend.Add(hashTagModel.HashTagSearchKey);
                }
                mediaJobject[JsonKeys.HashTagList] = hashtagsToSend;
            }
            mediaJobject[JsonKeys.MediaType] = 1;
            JArray mediasToSend = new JArray();
            bool isAlbumUrlPicked = false;
            foreach (var singleMediaDTO in newStatusViewModel.NewStatusMusicUpload)
            {
                JObject obj = new JObject();
                obj[JsonKeys.StreamUrl] = singleMediaDTO.StreamUrl;
                if (!string.IsNullOrEmpty(singleMediaDTO.ThumbUrl))
                {
                    obj[JsonKeys.ThumbUrl] = singleMediaDTO.ThumbUrl;
                    obj[JsonKeys.ThumbImageWidth] = singleMediaDTO.ImageWidth;
                    obj[JsonKeys.ThumbImageHeight] = singleMediaDTO.ImageHeight;
                    if (!isAlbumUrlPicked) 
                    { 
                        mediaJobject[JsonKeys.MediaAlbumImageURL] = singleMediaDTO.ThumbUrl; 
                        isAlbumUrlPicked = true; 
                    }
                }
                obj[JsonKeys.Title] = singleMediaDTO.AudioTitle;
                obj[JsonKeys.MediaPrivacy] = newStatusViewModel.PrivacyValue;
                if (!string.IsNullOrEmpty(singleMediaDTO.AudioArtist))
                {
                    obj[JsonKeys.Artist] = singleMediaDTO.AudioArtist;
                }
                obj[JsonKeys.MediaDuration] = singleMediaDTO.AudioDuration;
                if (hashtagsToSend != null)
                {
                    obj[JsonKeys.HashTagList] = hashtagsToSend;
                }
                mediasToSend.Add(obj);
            }
            mediaJobject[JsonKeys.MediaList] = mediasToSend;
        }

        private void LoadAlbumIdNameinMediaContentDTO()
        {
            string albumName = "";
            albumName = newStatusViewModel.AudioAlbumTitleTextBoxText.Trim();

            if (!string.IsNullOrEmpty(albumName))
            {
                MediaContentModel mediaContentModel = RingIDViewModel.Instance.MyAudioAlbums.Where(x => x.AlbumName == albumName).FirstOrDefault();
                mediaJobject[JsonKeys.AlbumId] = mediaContentModel != null ? mediaContentModel.AlbumId : Guid.Empty;
                mediaJobject[JsonKeys.AlbumName] = albumName;
            }
        }
        private void UploadToAudioServer(MusicUploaderModel model)
        {
            try
            {
                string _lineEnd = "\r\n";
                string _twoHyphens = "--";
                string _boundary = "*****";

                webClient.Headers["Content-Type"] = "multipart/form-data; boundary=" + _boundary;
                webClient.Headers["User-Agent"] = "ringID" + "_" + DefaultSettings.VALUE_APP_INSTALLED_VERSION;
                webClient.Headers["access-control-allow-origin"] = "*";

                var firstPart = "";
                string fileheaderTemplate = "Content-Disposition: form-data; name=\"{0}\"" + _lineEnd + _lineEnd + "{1}" + _lineEnd;

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "sId", DefaultSettings.LOGIN_SESSIONID);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "uId", DefaultSettings.LOGIN_RING_ID);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "authServer", ServerAndPortSettings.AUTH_SERVER_IP);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "comPort", ServerAndPortSettings.COMMUNICATION_PORT);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "x-app-version", AppConfig.VERSION_PC);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += "Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + Path.GetFileName(model.FilePath) + "\"" + _lineEnd;
                firstPart += _lineEnd;

                byte[] firstBytes = webClient.Encoding.GetBytes(firstPart);

                var lastPart = "";
                lastPart += _lineEnd;
                lastPart += _twoHyphens + _boundary + _twoHyphens + _lineEnd;

                byte[] lastBytes = webClient.Encoding.GetBytes(lastPart);

                FileStream oFileStream = new FileStream(model.FilePath, FileMode.Open, FileAccess.Read);
                int fileLength = (int)oFileStream.Length;
                long length = firstBytes.Length + fileLength + lastBytes.Length;
                wholebytes = new Byte[length];
                int currentbytes = 0;

                /*New Length including header*/
                totalFileSize -= model.FileSize;
                model.FileSize = length;
                totalFileSize += model.FileSize;

                Buffer.BlockCopy(firstBytes, 0, wholebytes, currentbytes, firstBytes.Length);
                currentbytes += firstBytes.Length;

                oFileStream.Read(wholebytes, currentbytes, fileLength);
                currentbytes += fileLength;

                oFileStream.Close();

                Buffer.BlockCopy(lastBytes, 0, wholebytes, currentbytes, lastBytes.Length);
                currentbytes += lastBytes.Length;

                model.IsUploading = true;
                webClient.UploadDataAsync(new Uri(ServerAndPortSettings.GetAudioUploadingURL), "POST", wholebytes, model);
                if (wholebytes != null) GC.SuppressFinalize(wholebytes);
                wholebytes = null;
            }
            catch (Exception ex)
            {
                if (wholebytes != null)
                {
                    GC.SuppressFinalize(wholebytes);
                }
                wholebytes = null;
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
                if (webClient != null)
                {
                    GC.SuppressFinalize(webClient);
                }
                webClient = null;
                singleMusicUploadFailed = true;
                model.IsUploading = false;
                newStatusViewModel.EnablePostbutton(true);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                });
                UIHelperMethods.ShowFailed("Failed to upload audio!", "Audio upload");
                HideUploadingPopup();
            }
        }

        private void UploadDataCompleted(Object sender, UploadDataCompletedEventArgs e)
        {
            try
            {
                MusicUploaderModel model = (MusicUploaderModel)e.UserState;

                if (e.Result != null && e.Result.Length > 0 && (int)e.Result[0] == 1)
                {
                    totalUploaded += model.FileSize;
                    int urlLength = (int)e.Result[1];
                    model.StreamUrl = System.Text.Encoding.UTF8.GetString(e.Result, 2, urlLength);
                    musicUploaderList.Remove(model);
                    if (!string.IsNullOrEmpty(model.StreamUrl) && model.IsImageFound && model.ImageWidth > 0 && model.ImageHeight > 0)
                    {
                        ThumbImageUpload(model);
                    }
                    else
                    {
                        model.IsUploading = false;
                        model.IsUploadedInAudioServer = true;
                        BeginToMusicUpload();
                    }
                }
                else
                {
                    singleMusicUploadFailed = true;
                    newStatusViewModel.EnablePostbutton(true);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                    });
                    UIHelperMethods.ShowFailed("Failed to upload audio!", "Audio upload");
                    HideUploadingPopup();
                    if (newStatusViewModel.ucMediaCloudUploadPopup != null)
                    {
                        newStatusViewModel.ucMediaCloudUploadPopup.LoadStatesInUI(1);
                    }
                }
            }
            catch (Exception)
            {
                singleMusicUploadFailed = true;
                HideUploadingPopup();
                this.webClient.UploadProgressChanged -= new UploadProgressChangedEventHandler(UploadProgressChanged);
                newStatusViewModel.EnablePostbutton(true);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (DefaultSettings.IsInternetAvailable)
                    {
                        UIHelperMethods.ShowFailed(NotificationMessages.INTERNET_UNAVAILABLE, "Post failed!");
                    }
                    else
                    {
                        UIHelperMethods.ShowFailed("Failed to upload audio!", "Audio upload");
                    }
                    newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                });
                if (newStatusViewModel.ucMediaCloudUploadPopup != null)
                {
                    newStatusViewModel.ucMediaCloudUploadPopup.LoadStatesInUI(1);
                }
            }
        }

        private void UploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {
            try
            {
                MusicUploaderModel obj = (MusicUploaderModel)e.UserState;
                obj.Pecentage = (int)(((double)e.BytesSent / (double)e.TotalBytesToSend) * 100);
                uploadingModel.TotalUploadedInPercentage = (int)(((double)(e.BytesSent + totalUploaded) / (double)totalFileSize) * 100);
            }
            catch (Exception)
            {
                singleMusicUploadFailed = true;
                newStatusViewModel.EnablePostbutton(true);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                });
                if (newStatusViewModel.ucMediaCloudUploadPopup != null)
                {
                    newStatusViewModel.ucMediaCloudUploadPopup.LoadStatesInUI(1);
                }
            }
        }

        private async void ThumbImageUpload(MusicUploaderModel model)
        {
            try
            {
                TagLib.File file = TagLib.File.Create(model.FilePath);

                byte[] response = await FeedImagesUpload.Instance.UploadThumbImageToImageServerWebRequest(file.Tag.Pictures[0].Data.Data, model.ImageWidth, model.ImageHeight);

                if (response != null)
                {
                    if (response[0] == 1)
                    {
                        int len = response[1];
                        string ArtImageUrl = Encoding.UTF8.GetString(response, 2, len);
                        model.ThumbUrl = ArtImageUrl;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("ThumbImageUpload() => Error Message : " + ex.Message + " Stack Trace : " + ex.StackTrace);
            }
            finally
            {
                model.IsUploading = false;
                model.IsUploadedInAudioServer = true;
                BeginToMusicUpload();
            }
        }
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

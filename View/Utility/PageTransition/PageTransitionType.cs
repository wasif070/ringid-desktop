﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace View.Utility.PageTransition
{
	public enum PageTransitionType
	{
		Fade,
		Slide,
		SlideAndFade,
		Grow,
		GrowAndFade,
		Flip,
		FlipAndFade,
		Spin,
		SpinAndFade
	}
}

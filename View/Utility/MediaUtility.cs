﻿using System;
using System.Collections.ObjectModel;
using log4net;
using Models.Constants;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.UI;
using View.UI.MediaPlayer;
using View.Utility.RingPlayer;
using View.ViewModel;
using View.UI.PopUp;
using View.UI.ImageViewer;

namespace View.Utility
{
    public class MediaUtility
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(MediaUtility).Name);

        #region "Properties"
        private static UCMediaPlayerInMain MediaPlayerInMain { get; set; }
        #endregion "Properties"

        public static string MakeEncriptedURL(long utId, Guid mediaId)
        {
            Cryptographer cryptographer = new Cryptographer();
            if (ServerAndPortSettings.AUTH_SERVER_IP != null && ServerAndPortSettings.COMMUNICATION_PORT > 0)
                return ServerAndPortSettings.MediaShareBase + cryptographer.encrypt(utId, mediaId.ToString(), ServerAndPortSettings.AUTH_SERVER_IP, ServerAndPortSettings.COMMUNICATION_PORT);
            else
            {
                UIHelperMethods.ShowTimerMessageBox("Not Signed In or Internet Unavailable!", "Failed!");
                return string.Empty;
            }
        }

        public static void RunPlayList(bool fromPortalOrNotUploaded, Guid nfid, ObservableCollection<SingleMediaModel> playList, BaseUserProfileModel OwnerShortInfoForAlbum = null, int idx = 0, bool autoNext = true, System.Windows.Controls.Grid ParentGrid = null)
        {
            try
            {
                if (MediaPlayerInMain != null)
                {
                    MediaPlayerInMain.RemovePlayerFromGuiRingID();
                    MediaPlayerInMain.CloseMediaPlayer();
                }
                RingIDViewModel.Instance.MediaList.Clear();
                VMPlayer DataModel = new VMPlayer();
                foreach (SingleMediaModel model in playList)
                {
                    model.NewsFeedId = nfid;
                    RingIDViewModel.Instance.MediaList.InvokeAdd(model);
                }
                DataModel.MediaList = RingIDViewModel.Instance.MediaList;
                if (fromPortalOrNotUploaded) DataModel.PlayingState = ViewConstants.MEDIA_PAUSED;
                DataModel.PlayingState = ViewConstants.MEDIA_PLAYING;
                MediaPlayerInMain = new UCMediaPlayerInMain(DataModel);
                MediaPlayerInMain.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1, "#FFd8e1e6", 1.0, null, false);
                MediaPlayerInMain.SetOwinServer();
                if (ParentGrid != null)
                {
                    if (ParentGrid == UCGuiRingID.Instance.MotherPanel)
                        MediaPlayerInMain.SetParentGrid(UCGuiRingID.Instance.MotherPanel);
                    else
                    {
                        var obj = ((System.Windows.Controls.Border)(ParentGrid.Parent)).Parent;
                        if (obj != null && obj is UCImageViewInMain)
                        {
                            MediaPlayerInMain.SetParentGrid(ParentGrid);
                        }
                        else if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                            MediaPlayerInMain.SetParentGrid(UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid);
                    }                  
                }
                else if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                {
                    MediaPlayerInMain.SetParentGrid(UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid);
                }
                else MediaPlayerInMain.SetParentGrid(UCGuiRingID.Instance.MotherPanel);
                MediaPlayerInMain.ShowPlayerInMainView(true);
                MediaPlayerInMain.OnClosedMediaPlayer += () =>
                {
                    if (ParentGrid != null && !(ParentGrid == UCGuiRingID.Instance.MotherPanel))
                    {
                        var obj = ((System.Windows.Controls.Border)(ParentGrid.Parent)).Parent;
                        if (obj != null && obj is UCImageViewInMain)
                        {
                            UCImageViewInMain imageViewInMain1 = (UCImageViewInMain)obj;
                            imageViewInMain1.GrabKeyboardFocus();
                        }
                        else if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                            UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                    }
                    else if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                    if (MediaPlayerInMain != null)
                    {
                        MediaPlayerInMain.DataModel = null;
                        MediaPlayerInMain = null;
                    }
                };
                MediaPlayerInMain.ChangeSingleMedia(idx, true);
                ViewConstants.AlbumID = DataModel.SingleMediaModel.AlbumId;
            }
            finally { }
        }

        public static void PlaySingleMediaNoContentID(SingleMediaModel singleMediaModel, bool isFromLocalDirectory, System.Windows.Controls.Grid ParentGrid = null)//playlist!=null &>0& idx<count,playList items must have utid
        {
            try
            {
                ObservableCollection<SingleMediaModel> playList = new ObservableCollection<SingleMediaModel>();
                singleMediaModel.IsFromLocalDirectory = isFromLocalDirectory;
                if (string.IsNullOrEmpty(singleMediaModel.Title)) singleMediaModel.Title = "<Unknown>";
                playList.Add(singleMediaModel);
                RunPlayList(false, Guid.Empty, playList, singleMediaModel.MediaOwner, 0, false, ParentGrid);
            }
            finally { }
        }

        public static void RunPlayListFromNotification(Guid nfid,
            Guid cmntID,
            ObservableCollection<SingleMediaModel> playList,
            UserShortInfoModel OwnerShortInfoForAlbum = null,
            int idx = 0,
            bool autoNext = true)//playlist!=null &>0& idx<count,playList items must have utid
        {
            RunPlayList(true, nfid, playList, OwnerShortInfoForAlbum, idx, autoNext);
        }

        public static void RunMediaFromChat(JObject mediaObj)
        {
            SingleMediaModel singleMediaModel = new SingleMediaModel();
            singleMediaModel.LoadData(mediaObj, (int)mediaObj[JsonKeys.MediaType]);
            if (mediaObj[JsonKeys.NewsfeedId] != null) singleMediaModel.NewsFeedId = (Guid)mediaObj[JsonKeys.NewsfeedId];
            ObservableCollection<SingleMediaModel> playList = new ObservableCollection<SingleMediaModel>();
            playList.Add(singleMediaModel);
            RunPlayList(false, singleMediaModel.NewsFeedId, playList);
        }
    }
}
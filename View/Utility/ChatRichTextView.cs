﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using View.Constants;

namespace View.Utility
{
    public class ChatRichTextView : RichTextBox
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChatRichTextView).Name);

        public ChatRichTextView()
        {
            this.IsReadOnly = true;
            this.Margin = new Thickness(0, 0, 0, 0);
            this.BorderThickness = new Thickness(0, 0, 0, 0);
            this.Background = Brushes.Transparent;
            this.UndoLimit = 0;
            this.IsUndoEnabled = false;
        }

        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                SetValue(TextProperty, value);
            }
        }

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(ChatRichTextView), new PropertyMetadata(default(string), TextPropertyChanged));

        public double ActualTextWidth
        {
            get { return (double)GetValue(ActualTextWidthProperty); }
            private set { SetValue(ActualTextWidthProperty, value); }
        }

        public static readonly DependencyProperty ActualTextWidthProperty = DependencyProperty.Register("ActualTextWidth", typeof(double), typeof(ChatRichTextView), new UIPropertyMetadata(0.00));

        public int EmoSize
        {
            get { return (int)GetValue(EmoSizeProperty); }
            set { SetValue(EmoSizeProperty, value); }
        }

        public static readonly DependencyProperty EmoSizeProperty = DependencyProperty.Register("EmoSize", typeof(int), typeof(ChatRichTextView), new UIPropertyMetadata(25));

        public double WideTextLimit
        {
            get { return (double)GetValue(WideTextLimitProperty); }
            set { SetValue(WideTextLimitProperty, value); }
        }

        public static readonly DependencyProperty WideTextLimitProperty = DependencyProperty.Register("WideTextLimit", typeof(double), typeof(ChatRichTextView), new UIPropertyMetadata(308.00, WideTextLimitChanged));

        public bool HasWideText
        {
            get { return (bool)GetValue(HasWideTextProperty); }
            set { SetValue(HasWideTextProperty, value); }
        }

        public static readonly DependencyProperty HasWideTextProperty = DependencyProperty.Register("HasWideText", typeof(bool), typeof(ChatRichTextView), new UIPropertyMetadata(true));

        private static void TextPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            try
            {
                var rtb = (ChatRichTextView)dependencyObject;
                var text = (string)dependencyPropertyChangedEventArgs.NewValue;
                if (text == null) return;

                int textPosition = 0;
                var paragraph = new Paragraph();
                var matches = Regex.Matches(text, DefaultSettings.EMOTION_REGULAR_EXPRESSION + DefaultSettings.HYPER_LINK_EXPRESSION);

                foreach (Match match in matches)
                {
                    string value = match.Value;
                    int urlOccurrenceIndex = text.IndexOf(value, textPosition, StringComparison.Ordinal);
                    if (urlOccurrenceIndex >= 0)
                    {
                        paragraph.Inlines.Add(text.Substring(textPosition, urlOccurrenceIndex - textPosition));
                        textPosition += urlOccurrenceIndex - textPosition;
                    }

                    if (Regex.IsMatch(value, DefaultSettings.HYPER_LINK_EXPRESSION))
                    {
                        Hyperlink hyperlink = GetHyperlink(match);
                        paragraph.Inlines.Add(hyperlink);
                    }
                    else if (DefaultDictionaries.Instance.EMOTICON_DICTIONARY.ContainsKey(value))
                    {
                        Image image = CreateEmoticonImage(value, rtb.EmoSize);
                        paragraph.Inlines.Add(new InlineUIContainer { Child = image, BaselineAlignment = BaselineAlignment.Center });
                    }
                    textPosition += value.Length;
                }

                if (matches.Count == 0)
                {
                    Run run = new Run { Text = text };
                    paragraph.Inlines.Add(run);
                }
                else if (textPosition <= text.Length - 1)
                {
                    Run run = new Run { Text = text };
                    paragraph.Inlines.Add(text.Substring(textPosition, text.Length - textPosition));
                }
                rtb.Document.PagePadding = new Thickness(0, 1, 0, 0);
                rtb.Document.Blocks.Clear();
                rtb.Document.Blocks.Add(paragraph);
                rtb.ActualTextWidth = GetTotalWidth(rtb);
                rtb.HasWideText = rtb.ActualTextWidth > rtb.WideTextLimit;
                rtb.Width = rtb.HasWideText ? rtb.MaxWidth : rtb.ActualTextWidth;
            }
            catch (Exception ex)
            {
                log.Error("Error: TextPropertyChanged()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private static void WideTextLimitChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            try
            {
                var rtb = (ChatRichTextView)dependencyObject;
                if (rtb.ActualTextWidth == 0) return;

                rtb.HasWideText = rtb.ActualTextWidth > rtb.WideTextLimit;
                rtb.Width = rtb.HasWideText ? rtb.MaxWidth : rtb.ActualTextWidth;
            }
            catch (Exception ex)
            {
                log.Error("Error: WideTextLimitChanged()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static Hyperlink GetHyperlink(Match match)
        {
            Uri uri;
            if (!(Uri.TryCreate(match.Value, UriKind.Absolute, out uri) && (uri.Scheme == Uri.UriSchemeHttp || uri.Scheme == Uri.UriSchemeHttps || uri.Scheme == Uri.UriSchemeFtp)))
            {
                uri = new Uri("http://" + match.Value);
            }

            Hyperlink hyperlink = new Hyperlink
            {
                NavigateUri = uri,
                TargetName = "_blank",
                Cursor = Cursors.Hand,
                Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF06a8e6"))
            };
            hyperlink.Inlines.Add(match.Value);
            hyperlink.MouseLeftButtonDown += new MouseButtonEventHandler(link_click);
            hyperlink.MouseEnter += new MouseEventHandler(link_hover);
            hyperlink.MouseLeave += new MouseEventHandler(link_leave);
            return hyperlink;
        }

        private static void link_leave(object sender, MouseEventArgs e)
        {
            Hyperlink link = (Hyperlink)sender;
            link.TextDecorations = null;
        }

        private static void link_hover(object sender, MouseEventArgs e)
        {
            Hyperlink link = (Hyperlink)sender;
            link.TextDecorations = TextDecorations.Underline;
        }

        private static void link_click(object sender, RoutedEventArgs e)
        {
            try
            {
                Hyperlink link = (Hyperlink)sender;
                System.Diagnostics.Process.Start(link.NavigateUri.ToString());
            }
            catch (Exception ex)
            {
                log.Error("Error: link_click()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static Image CreateEmoticonImage(string value, int emoSize)
        {
            Image image = new Image();
            image.ToolTip = value;
            BitmapImage icon = ImageObjects.EMOTICON_ICON[value];
            if (icon == null)
            {
                EmoticonDTO entry = DefaultDictionaries.Instance.EMOTICON_DICTIONARY[value];
                if (entry != null)
                {
                    icon = ImageUtility.GetBitmapImageOfDynamicResource(RingIDSettings.EMOTICON_FOLDER + Path.DirectorySeparatorChar + "dmid" + Path.DirectorySeparatorChar + entry.Url);
                    if (icon != null)
                    {
                        ImageObjects.EMOTICON_ICON[entry.Symbol] = icon;
                    }
                }
            }

            image.Source = icon;
            image.Height = emoSize;
            image.Width = emoSize;
            image.Margin = new Thickness(0, 0, 0, 0);//new Thickness(0, 0, 0, -3);
            return image;
        }

        public static double GetTotalWidth(ChatRichTextView rtb)
        {
            List<double> widthList = new List<double>();
            try
            {
                double width = 1.0;


                string value = String.Empty;
                BlockCollection bc = rtb.Document.Blocks;
                int count = bc.Count;

                foreach (Paragraph b in bc)
                {
                    foreach (Inline ic in b.Inlines)
                    {
                        if (ic is Run)
                        {
                            Run run = ((Run)ic);
                            value = new TextRange(run.ContentStart, run.ContentEnd).Text;
                            width += GetTextWith(value, rtb);
                        }
                        else if (ic is InlineUIContainer)
                        {
                            width += 25.0;
                        }
                        else if (ic is Hyperlink)
                        {
                            Hyperlink hyperlink = ((Hyperlink)ic);
                            value = new TextRange(hyperlink.ContentStart, hyperlink.ContentEnd).Text;
                            width += GetTextWith(value, rtb);
                        }
                        else if (ic is LineBreak)
                        {
                            value += "\r\n";
                            widthList.Add(width);
                            width = 1.0;
                        }
                    }

                    if (--count > 0)
                    {
                        value += "\r\n";
                    }

                    widthList.Add(width);
                    width = 1.0;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: GetTotalWidth()." + ex.Message + "\n" + ex.StackTrace);
            }

            return widthList.Max();

        }

        private static double GetTextWith(string text, ChatRichTextView rtb)
        {
            FormattedText formattedText = new FormattedText(text,
                                                CultureInfo.CurrentUICulture,
                                                FlowDirection.LeftToRight,
                                                new Typeface(rtb.FontFamily, rtb.FontStyle, rtb.FontWeight, rtb.FontStretch),
                                                rtb.FontSize,
                                                Brushes.Black);
            return formattedText.WidthIncludingTrailingWhitespace;
        }

    }

}

﻿using Auth.Service;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using View.BindingModels;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Utility.Auth;
using View.UI;

namespace View.Utility
{
    public class MyEducationUtility
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(MyEducationUtility).Name);

        public static void DeleteEducation(object sender, Dictionary<Guid, EducationDTO> myEducationsbyId)
        {
            try
            {
                if (ConfirmResultMsgBox(NotificationMessages.ED_ASK_DELETE_EDUCATION))
                {
                    if (myEducationsbyId == null) RingDictionaries.Instance.EDUCATION_DICTIONARY.TryGetValue(DefaultSettings.LOGIN_TABLE_ID, out myEducationsbyId);
                    Control control = (Control)sender;
                    SingleEducationModel educationModel = (SingleEducationModel)control.DataContext;
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        bool _Success = false;
                        string _Msg = string.Empty;
                        //  DeleteEducationInfo deleteEducationInfo = new DeleteEducationInfo(educationModel.EId);
                        //   deleteEducationInfo.Run(out _Success, out _Msg);

                        JObject pakToSend = new JObject();
                        pakToSend[JsonKeys.UUID] = educationModel.EId;
                        (new AuthRequstWithMessage(pakToSend, AppConstants.TYPE_REMOVE_EDUCATION, AppConstants.REQUEST_TYPE_UPDATE)).Run(out _Success, out _Msg);
                        if (_Success)
                        {
                            if (myEducationsbyId.Remove(educationModel.EId))
                            {
                                if (RingIDViewModel.Instance.MyEducationModelsList.Where(P => P.EId == educationModel.EId).FirstOrDefault() != null)
                                {
                                    RingIDViewModel.Instance.MyEducationModelsList.Remove(educationModel);
                                }
                            }
                            else
                            {
                                // MessageBox.Show(MainSwitcher.pageSwitcher, "Failed! ", "Failed!", MessageBoxButton.OK, MessageBoxImage.Error);
                                //CustomMessageBox.ShowError("Failed! ");
                                UIHelperMethods.ShowFailed("Failed to delete!", "Delete education");
                            }
                            //educationModel.EVisibilityEditMode = Visibility.Collapsed;
                            educationModel.EVisibilityViewMode = Visibility.Visible;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(_Msg))
                                MessageBox.Show(null, "Failed! " + _Msg, "Failed!", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: DeleteEducationClick() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void CurrentEducation_Click(object sender)
        {
            try
            {
                Control control = (Control)sender;
                SingleEducationModel educationModel = (SingleEducationModel)control.DataContext;
                educationModel.EToTime_EditMode = DateTime.Now.ToShortDateString();
                educationModel.EToTime_ViewMode = "Present";
                educationModel.EToTime = 0;
                educationModel.EVisibilityDate = Visibility.Visible;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void EducationFromTime_Selected(object sender)
        {
            try
            {
                Control control = (Control)sender;
                SingleEducationModel educationModel = (SingleEducationModel)control.DataContext;
                if (educationModel.EVisibilityViewMode == Visibility.Collapsed)
                {
                    var picker = sender as DatePicker;
                    DateTime? ft = picker.SelectedDate;
                    educationModel.EFromTime = ft != null ? ModelUtility.MillisFromDateTimeSince1970(ft) : 1;
                    educationModel.EFromTime_ViewMode = ft != null ? ft.Value.ToString("dd-MMM-yyyy") : "";
                    educationModel.EVisibilityDate = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        public static void EducationToTime_Selected(object sender)
        {
            try
            {
                Control control = (Control)sender;
                SingleEducationModel educationModel = (SingleEducationModel)control.DataContext;
                if (educationModel.EVisibilityViewMode == Visibility.Collapsed)
                {
                    var picker = sender as DatePicker;
                    DateTime? tt = picker.SelectedDate;
                    educationModel.EToTime = tt != null ? ModelUtility.MillisFromDateTimeSince1970(tt) : 1;
                    educationModel.EToTime_ViewMode = tt != null ? tt.Value.ToString("dd-MMM-yyyy") : "";
                    educationModel.EVisibilityDate = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        public static void CancelEditEducationClick(object param, Dictionary<Guid, EducationDTO> myEducationsbyId)
        {
            try
            {
                //Control control = (Control)sender;
                SingleEducationModel educationModel = (SingleEducationModel)param;//SingleEducationModel educationModel = (SingleEducationModel)control.DataContext;
                if (myEducationsbyId == null) RingDictionaries.Instance.EDUCATION_DICTIONARY.TryGetValue(DefaultSettings.LOGIN_TABLE_ID, out myEducationsbyId);
                EducationDTO education = myEducationsbyId[educationModel.EId];
                educationModel.LoadData(education);
                //educationModel.EVisibilityEditMode = Visibility.Collapsed;
                educationModel.EVisibilityViewMode = Visibility.Visible;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        public static void EditEducationClick(object sender)
        {
            try
            {
                Control control = (Control)sender;
                SingleEducationModel educationModel = (SingleEducationModel)control.DataContext;
                //educationModel.EVisibilityEditMode = Visibility.Visible;
                educationModel.EVisibilityViewMode = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        public static string TimesCompare(string ft, string tt)
        {
            if (string.IsNullOrEmpty(ft) && string.IsNullOrEmpty(tt)) return string.Empty;
            else
            {
                if (!string.IsNullOrEmpty(ft) && !string.IsNullOrEmpty(tt))
                {
                    DateTime dt1 = Convert.ToDateTime(ft);
                    DateTime dt2 = Convert.ToDateTime(tt);
                    if (dt1.CompareTo(dt2) < 0) return "";
                    else return NotificationMessages.ABOUT_END_DATE_MUST_GREATER_THAN_START_DATE;
                }
                else
                {
                    return NotificationMessages.ABOUT_START_DATE_END_DATE_REQUIRED_OR_NONE;
                }
            }
        }
        private static bool ConfirmResultMsgBox(string toAsk)
        {
            WNConfirmationView cv = new WNConfirmationView(toAsk, String.Format(NotificationMessages.SURE_WANT_TO, toAsk), CustomConfirmationDialogButtonOptions.YesNo);
            var result = cv.ShowCustomDialog();
            return (result == ConfirmationDialogResult.Yes);
        }
        public static void educationContextMenu_IsVisibleChanged(object sender)
        {
            Control control = (Control)sender;
            if (sender != null && control is ContextMenu)
            {
                SingleEducationModel edModel = (SingleEducationModel)control.DataContext;
                if (control.IsVisible) edModel.SettingAboutButtonVisibilty = Visibility.Visible;
                else edModel.SettingAboutButtonVisibilty = Visibility.Collapsed;
            }
        }
        public static bool VerifyEducation(SingleEducationModel EModel, bool IsCollege = false)
        {
            bool NoError = true;
            if (string.IsNullOrEmpty(EModel.EInstName))
            {
                EModel.EInstituteNameErrorString = NotificationMessages.ED_SCHOOL_NAME_REQUIRED;
                NoError = false;
            }
            else EModel.EInstituteNameErrorString = string.Empty;

            if (string.IsNullOrEmpty(EModel.EDegree))
            {
                EModel.EDegreeErrorString = NotificationMessages.ED_DEGREE_REQUIRED;
                NoError = false;
            }
            else EModel.EDegreeErrorString = string.Empty;
            if (string.IsNullOrEmpty(EModel.EConcentration))
            {
                EModel.EConcentrationErrorString = NotificationMessages.ED_CENCENTRATION_REQUIRED;
                NoError = false;
            }
            else EModel.EConcentrationErrorString = string.Empty;

            //FromDate
            if (EModel.EFromTime == 1)
            {
                EModel.EFromDateErrorString = NotificationMessages.ABOUT_START_DATE_REQUIRED;
                NoError = false;
            }
            else EModel.EFromDateErrorString = string.Empty;

            //ToDate
            if (EModel.EToTime == 1)
            {
                EModel.EToDateErrorString = NotificationMessages.ABOUT_END_DATE_REQUIRED;
                NoError = false;
            }
            else EModel.EToDateErrorString = string.Empty;

            if (EModel.EFromTime != 1)
            {
                DateTime t2 = DateTime.Today;
                if (EModel.EToTime > 1)
                {
                    t2 = ModelUtility.DateTimeFromMillisSince1970(EModel.EToTime).Date;
                }
                if (ModelUtility.DateTimeFromMillisSince1970(EModel.EFromTime).Date.CompareTo(t2) > 0)
                {
                    EModel.EFromDateErrorString = NotificationMessages.ABOUT_START_DAY_MUST_EARLY;
                    NoError = false;
                }
                else EModel.EFromDateErrorString = string.Empty;
            }
            return NoError;
        }
    }
}

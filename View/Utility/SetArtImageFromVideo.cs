﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using View.BindingModels;

namespace View.Utility
{
    public class SetArtImageFromVideo
    {

        private MediaPlayer player = null;
        private FeedVideoUploaderModel model;

        public SetArtImageFromVideo(FeedVideoUploaderModel model)
        {
            this.model = model;
            try
            {
                this.player = new MediaPlayer { Volume = 0, ScrubbingEnabled = true };
                this.player.MediaOpened += new EventHandler(HandleMediaPlayerMediaOpened);
                this.player.Open(new Uri(model.FilePath));
                System.Threading.Thread.Sleep(500);
            }
            catch (Exception)
            {
                model.VideoThumbnail = null;
                if (player != null)
                {
                    player.Close();
                    player = null;
                }
            }
        }


        private void HandleMediaPlayerMediaOpened(object sender, EventArgs e)
        {
            uint[] framePixels = new uint[player.NaturalVideoWidth * player.NaturalVideoHeight];
            uint[] previousFramePixels = new uint[framePixels.Length];
            player.Pause();
            if (player.NaturalDuration.HasTimeSpan && player.NaturalDuration.TimeSpan.TotalSeconds > 5)
            {
                player.Position = TimeSpan.FromSeconds(5);
            }
            else
            {
                player.Position = TimeSpan.FromSeconds(1);
            }

            // Render the current frame into a bitmap
            var drawingVisual = new DrawingVisual();
            var renderTargetBitmap = new RenderTargetBitmap(player.NaturalVideoWidth, player.NaturalVideoHeight, 96, 96, PixelFormats.Default);
            using (var drawingContext = drawingVisual.RenderOpen())
            {
                drawingContext.DrawVideo(player, new Rect(0, 0, player.NaturalVideoWidth, player.NaturalVideoHeight));
            }
            renderTargetBitmap.Render(drawingVisual);

            // Copy the pixels to the specified location
            renderTargetBitmap.CopyPixels(previousFramePixels, player.NaturalVideoWidth * 4, 0);

            // Return the bitmap
            var bitmapImage = new BitmapImage();
            GC.SuppressFinalize(bitmapImage);
            var bitmapEncoder = new PngBitmapEncoder();
            bitmapEncoder.Frames.Add(BitmapFrame.Create(renderTargetBitmap));

            using (var stream = new MemoryStream())
            {
                bitmapEncoder.Save(stream);
                stream.Seek(0, SeekOrigin.Begin);

                bitmapImage.BeginInit();
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.StreamSource = stream;
                bitmapImage.EndInit();
                bitmapImage.Freeze();
            }

            model.VideoThumbnail = bitmapImage;
            bitmapImage = null;
            GC.SuppressFinalize(model.VideoThumbnail);
            player.Close();
        }




        /* public static BitmapImage GetArtImageFromVideo(string filename)
         {
             MediaPlayer player = new MediaPlayer { Volume = 0, ScrubbingEnabled = true };
             player.Open(new Uri(filename));
             player.Pause();
             System.Threading.Thread.Sleep(300);
             if (player.NaturalDuration.HasTimeSpan)
             {
                 Duration = (long)player.NaturalDuration.TimeSpan.TotalSeconds;
             }
             RenderTargetBitmap rtb = new RenderTargetBitmap(120, 90, 96, 96, PixelFormats.Pbgra32);
             DrawingVisual dv = new DrawingVisual();
             using (DrawingContext dc = dv.RenderOpen())
             {
                 dc.DrawVideo(player, new Rect(0, 0, 120, 90));
             }
             rtb.Render(dv);
             BitmapFrame frame = BitmapFrame.Create(rtb).GetCurrentValueAsFrozen() as BitmapFrame;
             BitmapEncoder encoder = new JpegBitmapEncoder();
             encoder.Frames.Add(frame as BitmapFrame);
             MemoryStream memoryStream = new MemoryStream();
             encoder.Save(memoryStream);
             memoryStream.Position = 0;
             BitmapImage bitmapImage = new BitmapImage();
             bitmapImage.BeginInit();
             bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
             bitmapImage.StreamSource = memoryStream;
             bitmapImage.EndInit();
             memoryStream.Close();
             player.Close();
             return bitmapImage;
         }
         public static byte[] GetArtImageByteArrayFromVideo(string filename)
         {
             MediaPlayer player = new MediaPlayer { Volume = 0, ScrubbingEnabled = true };
             player.Open(new Uri(filename));
             player.Pause();
             System.Threading.Thread.Sleep(300);
             if (player.NaturalDuration.HasTimeSpan)
             {
                 Duration = (long)player.NaturalDuration.TimeSpan.TotalSeconds;
             }
             RenderTargetBitmap rtb = new RenderTargetBitmap(120, 90, 96, 96, PixelFormats.Pbgra32);
             DrawingVisual dv = new DrawingVisual();
             using (DrawingContext dc = dv.RenderOpen())
             {
                 dc.DrawVideo(player, new Rect(0, 0, 120, 90));
             }
             rtb.Render(dv);
             BitmapFrame frame = BitmapFrame.Create(rtb).GetCurrentValueAsFrozen() as BitmapFrame;
             BitmapEncoder encoder = new JpegBitmapEncoder();
             encoder.Frames.Add(frame as BitmapFrame);
             MemoryStream memoryStream = new MemoryStream();
             encoder.Save(memoryStream);
             //memoryStream.Position = 0;
             byte[] arr = memoryStream.ToArray();
             memoryStream.Close();
             player.Close();
             return arr;
         }*/
    }
}

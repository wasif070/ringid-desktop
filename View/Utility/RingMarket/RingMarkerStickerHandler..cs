<<<<<<< HEAD
﻿using Auth.Service.RingMarket;
using log4net;
using Models.DAO;
using Models.Entity;
using Models.Utility;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using View.BindingModels;
using View.ViewModel;
using View.UI;
using View.Utility.WPFMessageBox;
using Models.Constants;
using View.UI.Chat;
using View.UI.RingMarket;
using View.Utility.Chat.Service;

namespace View.Utility.RingMarket
{
    public class RingMarkerStickerHandler : IRingMarkerSticker
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(RingMarkerStickerHandler).Name);

        public void OnInit()
        {
            if (UCRingChatStickerLowerPanel.Instance != null)
            {
                UCRingChatStickerLowerPanel.Instance.ShowInitLoader(true);
                RingIDViewModel.Instance.IsFetchingSticker = true;
                RingIDViewModel.Instance.IsFetchingStickerCompleted = false;
            }
        }

        public void OnComplete()
        {
            if (UCRingChatStickerLowerPanel.Instance != null)
            {
                UCRingChatStickerLowerPanel.Instance.ShowInitLoader(false);
                RingIDViewModel.Instance.IsFetchingSticker = false;
                RingIDViewModel.Instance.IsFetchingStickerCompleted = true;
            }
        }

        public void OnMarketStickerCategoryInfo(List<MarketStickerCategoryDTO> categoryDTOs, bool isRecent)
        {
            try
            {
                if (categoryDTOs != null && categoryDTOs.Count > 0)
                {
                    new InsertIntoRingMarketStickerTable(categoryDTOs.Where(P => P.IsDefault).ToList());

                    if (isRecent)
                    {
                        RingMarketStickerLoadUtility.RunRecentCat(categoryDTOs, true);
                    }
                    else
                    {
                        RingMarketStickerLoadUtility.LoadMarketStickerCategorys(categoryDTOs);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("OnMarketStickerCategoryInfo ==> " + ex);
            }
        }

        public void OnMarketStickerCollectionInfo(List<MarketStickerCollectionDTO> collectionDTos)
        {
            try
            {
                RingMarketStickerLoadUtility.LoadMarketStickerCollections(collectionDTos);
            }
            catch (Exception ex)
            {
                log.Error("OnMarketStickerCollectionInfo ==> " + ex);
            }
        }

        public void OnMarketLaguageInfo(List<MarketStickerLanguageyDTO> langDTOs)
        {
            RingMarketStickerLoadUtility.LoadMarketStickerLanguage(langDTOs);
        }

        public void OnMarketStickerIconDownloaded(int categoryID, int imageType, bool status)
        {
            try
            {
                MarketStickerCategoryModel ctgModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == categoryID).FirstOrDefault();
                if (ctgModel != null)
                {
                    if (imageType == RingMarketStickerService._Sticker_Icon)
                    {
                        ctgModel.IsIconDownloading = false;
                        if (status)
                        {
                            ctgModel.OnPropertyChanged("Icon");
                        }
                    }
                    else
                    {
                        ctgModel.IsMarketIconDownloading = false;
                        if (status)
                        {
                            ctgModel.OnPropertyChanged("Icon");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("OnMarketStickerIconDownloaded ==> " + ex);
            }
        }

        public void OnMarketStickerBannerImageDownloaded(int categoryID)
        {
            try
            {
                MarketStickerCategoryModel ctgModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == categoryID).FirstOrDefault();
                if (ctgModel != null)
                {
                    ctgModel.OnPropertyChanged("CategoryBannerImage");
                }
            }
            catch (Exception ex)
            {
                log.Error("OnMarketStickerDetailImageDownloaded ==> " + ex);
            }
        }

        public void OnMarketStickerImageInfo(int categoryId, List<MarkertStickerImagesDTO> imgDTos)
        {
            try
            {
                MarketStickerCategoryModel ctgModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == categoryId).FirstOrDefault();
                if (ctgModel != null)
                {
                    List<MarkertStickerImagesDTO> imagesList = new List<MarkertStickerImagesDTO>();
                    if (ctgModel.ImageList.Count > 0)
                    {
                        foreach (MarkertStickerImagesDTO imageDTO in imgDTos)
                        {
                            MarkertStickerImagesModel tempImageModel = ctgModel.ImageList.Where(P => P.ImageID == imageDTO.imId).FirstOrDefault();
                            if (tempImageModel != null)
                            {
                                if (tempImageModel.StickerCollectionID <= 0)
                                {
                                    tempImageModel.StickerCollectionID = imageDTO.sClId;
                                }
                            }
                            else
                            {
                                imagesList.Add(imageDTO);
                            }
                        }
                    }
                    else
                    {
                        imagesList = imgDTos;
                    }

                    imagesList.ForEach(i =>
                    {
                        ctgModel.ImageList.InvokeAdd(new MarkertStickerImagesModel(i));
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error("OnMarketStickerImageInfo ==> " + ex);
            }
        }

        public void OnMarketStickerDetailImageDownloaded(int categoryID, int imageType, bool isSucces)
        {
            try
            {
                MarketStickerCategoryModel ctgModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == categoryID).FirstOrDefault();
                if (ctgModel != null)
                {
                    if (imageType == RingMarketStickerService._MarketDetailImage)
                    {
                        ctgModel.IsDetailImageDownloading = false;
                        if (isSucces)
                        {
                            ctgModel.OnPropertyChanged("DetailImage");
                        }
                    }
                    else
                    {
                        ctgModel.IsThumbDetailImageDownloading = false;
                        if (isSucces)
                        {
                            ctgModel.OnPropertyChanged("ThumbDetailImage");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("OnMarketStickerDetailImageDownloaded ==> " + ex.Message + ex.StackTrace);
            }
        }

        public void OnMarketStickerImageDownloaded(int categoryID, int imageID)
        {
            try
            {
                MarketStickerCategoryModel ctgModel = categoryID > 0 ? RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == categoryID).FirstOrDefault()
                                                                     : RingIDViewModel.Instance.RecentStickerCategoryList.FirstOrDefault();
                if (ctgModel != null)
                {
                    if (ctgModel.Downloaded == false)
                    {
                        ctgModel.IsDownloadRunning = true;
                    }
                    MarkertStickerImagesModel imgModel = ctgModel.ImageList.Where(P => P.ImageID == imageID).FirstOrDefault();
                    if (imgModel != null)
                    {
                        imgModel.OnPropertyChanged("ImageUrl");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("OnMarketStickerImageDownloaded ==> " + ex);
            }
        }

        public void OnMarketStickerImageListDownloaded(int categoryID, bool status)
        {
            try
            {
                if (status)
                {
                    MarketStickerCategoryDTO marketCtgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(categoryID);
                    if (marketCtgDTO != null && marketCtgDTO.Downloaded == false)
                    {
                        marketCtgDTO.Downloaded = true;
                        marketCtgDTO.DownloadTime = ChatService.GetServerTime();
                        MarketStickerCategoryModel ctgModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == categoryID).FirstOrDefault();
                        if (ctgModel != null)
                        {
                            ctgModel.Downloaded = true;
                            ctgModel.IsDownloadRunning = false;
                            ctgModel.DownloadTime = marketCtgDTO.DownloadTime;
                        }

                        List<MarketStickerCategoryDTO> marketCtgDTOs = new List<MarketStickerCategoryDTO>();
                        marketCtgDTOs.Add(marketCtgDTO);
                        RingMarketStickerLoadUtility.LoadMarketStickerRecentCategorys(marketCtgDTOs);
                        new InsertIntoRingMarketStickerTable(marketCtgDTOs);
                        new InsertIntoRingMarketStickerTable(marketCtgDTO.ImagesList);

                        // RingMarkerStickerDAO.UpdateStickerCategoryDownloadStatus(marketCtgDTO.SCtId, marketCtgDTO.Downloaded, marketCtgDTO.DownloadTime);
                    }
                }
                else
                {
                    string categoryName = String.Empty;

                    MarketStickerCategoryModel ctgModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == categoryID).FirstOrDefault();
                    if (ctgModel != null)
                    {
                        categoryName = ctgModel.StickerCategoryName;
                        if (ctgModel.IsDetailImageDownloading)
                        {
                            ctgModel.IsDetailImageDownloading = false;
                        }
                        if (ctgModel.IsThumbDetailImageDownloading)
                        {
                            ctgModel.IsThumbDetailImageDownloading = false;
                        }

                        ctgModel.IsDownloadRunning = false;
                        ctgModel.DownloadPercentage = 0;
                    }

                    if (UCGuiRingID.Instance.IsVisible)
                    {
                        Application.Current.Dispatcher.BeginInvoke(() =>
                        {
                            UIHelperMethods.ShowFailed(String.Format(NotificationMessages.STICKER_DOWNLOAD_FAILED, categoryName), "Sticker download");
                            //CustomMessageBox.ShowError(String.Format(NotificationMessages.STICKER_DOWNLOAD_FAILED, categoryName));
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("OnMarketStickerImageListDownloaded ==> " + ex.Message + ex.StackTrace);
            }
        }

        public void OnOnMarketStickerNewCount(int totalNewCount, int newStickerUnseen)
        {
            RingIDViewModel.Instance.TotalNewStickers = totalNewCount;
            RingIDViewModel.Instance.NewStickerNotificationCounter = newStickerUnseen;
        }
    }
}
=======
﻿using Auth.Service.RingMarket;
using log4net;
using Models.DAO;
using Models.Entity;
using Models.Utility;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using View.BindingModels;
using View.ViewModel;
using View.UI;
using View.Utility.WPFMessageBox;
using Models.Constants;
using View.UI.Chat;
using View.UI.RingMarket;
using View.Utility.Chat.Service;

namespace View.Utility.RingMarket
{
    public class RingMarkerStickerHandler : IRingMarkerSticker
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(RingMarkerStickerHandler).Name);

        public void OnInit()
        {
            if (UCRingChatStickerLowerPanel.Instance != null)
            {
                UCRingChatStickerLowerPanel.Instance.ShowInitLoader(true);
            }

            RingIDViewModel.Instance.IsFetchingSticker = true;
            RingIDViewModel.Instance.IsFetchingStickerCompleted = false;
        }

        public void OnComplete()
        {
            if (UCRingChatStickerLowerPanel.Instance != null)
            {
                UCRingChatStickerLowerPanel.Instance.ShowInitLoader(false);
            }

            RingIDViewModel.Instance.IsFetchingSticker = false;
            RingIDViewModel.Instance.IsFetchingStickerCompleted = true;
        }

        public void OnMarketStickerCategoryInfo(List<MarketStickerCategoryDTO> categoryDTOs, bool isRecent)
        {
            try
            {
                if (categoryDTOs != null && categoryDTOs.Count > 0)
                {
                    new InsertIntoRingMarketStickerTable(categoryDTOs.Where(P => P.IsDefault).ToList());

                    if (isRecent)
                    {
                        RingMarketStickerLoadUtility.RunRecentCat(categoryDTOs, true);
                    }
                    else
                    {
                        RingMarketStickerLoadUtility.LoadMarketStickerCategorys(categoryDTOs);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("OnMarketStickerCategoryInfo ==> " + ex);
            }
        }

        public void OnMarketStickerCollectionInfo(List<MarketStickerCollectionDTO> collectionDTos)
        {
            try
            {
                RingMarketStickerLoadUtility.LoadMarketStickerCollections(collectionDTos);
            }
            catch (Exception ex)
            {
                log.Error("OnMarketStickerCollectionInfo ==> " + ex);
            }
        }

        public void OnMarketLaguageInfo(List<MarketStickerLanguageyDTO> langDTOs)
        {
            RingMarketStickerLoadUtility.LoadMarketStickerLanguage(langDTOs);
        }

        public void OnMarketStickerIconDownloaded(int categoryID, int imageType, bool status)
        {
            try
            {
                MarketStickerCategoryModel ctgModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == categoryID).FirstOrDefault();
                if (ctgModel != null)
                {
                    if (imageType == RingMarketStickerService._Sticker_Icon)
                    {
                        ctgModel.IsIconDownloading = false;
                        if (status)
                        {
                            ctgModel.OnPropertyChanged("Icon");
                        }
                    }
                    else
                    {
                        ctgModel.IsMarketIconDownloading = false;
                        if (status)
                        {
                            ctgModel.OnPropertyChanged("Icon");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("OnMarketStickerIconDownloaded ==> " + ex);
            }
        }

        public void OnMarketStickerBannerImageDownloaded(int categoryID)
        {
            try
            {
                MarketStickerCategoryModel ctgModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == categoryID).FirstOrDefault();
                if (ctgModel != null)
                {
                    ctgModel.OnPropertyChanged("CategoryBannerImage");
                }
            }
            catch (Exception ex)
            {
                log.Error("OnMarketStickerDetailImageDownloaded ==> " + ex);
            }
        }

        public void OnMarketStickerImageInfo(int categoryId, List<MarkertStickerImagesDTO> imgDTos)
        {
            try
            {
                MarketStickerCategoryModel ctgModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == categoryId).FirstOrDefault();
                if (ctgModel != null)
                {
                    List<MarkertStickerImagesDTO> imagesList = new List<MarkertStickerImagesDTO>();
                    if (ctgModel.ImageList.Count > 0)
                    {
                        foreach (MarkertStickerImagesDTO imageDTO in imgDTos)
                        {
                            MarkertStickerImagesModel tempImageModel = ctgModel.ImageList.Where(P => P.ImageID == imageDTO.imId).FirstOrDefault();
                            if (tempImageModel != null)
                            {
                                if (tempImageModel.StickerCollectionID <= 0)
                                {
                                    tempImageModel.StickerCollectionID = imageDTO.sClId;
                                }
                            }
                            else
                            {
                                imagesList.Add(imageDTO);
                            }
                        }
                    }
                    else
                    {
                        imagesList = imgDTos;
                    }

                    imagesList.ForEach(i =>
                    {
                        ctgModel.ImageList.InvokeAdd(new MarkertStickerImagesModel(i));
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error("OnMarketStickerImageInfo ==> " + ex);
            }
        }

        public void OnMarketStickerDetailImageDownloaded(int categoryID, int imageType, bool isSucces)
        {
            try
            {
                MarketStickerCategoryModel ctgModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == categoryID).FirstOrDefault();
                if (ctgModel != null)
                {
                    if (imageType == RingMarketStickerService._MarketDetailImage)
                    {
                        ctgModel.IsDetailImageDownloading = false;
                        if (isSucces)
                        {
                            ctgModel.OnPropertyChanged("DetailImage");
                        }
                    }
                    else
                    {
                        ctgModel.IsThumbDetailImageDownloading = false;
                        if (isSucces)
                        {
                            ctgModel.OnPropertyChanged("ThumbDetailImage");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("OnMarketStickerDetailImageDownloaded ==> " + ex.Message + ex.StackTrace);
            }
        }

        public void OnMarketStickerImageDownloaded(int categoryID, int imageID)
        {
            try
            {
                MarketStickerCategoryModel ctgModel = categoryID > 0 ? RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == categoryID).FirstOrDefault()
                                                                     : RingIDViewModel.Instance.RecentStickerCategoryList.FirstOrDefault();
                if (ctgModel != null)
                {
                    if (ctgModel.Downloaded == false)
                    {
                        ctgModel.IsDownloadRunning = true;
                    }
                    MarkertStickerImagesModel imgModel = ctgModel.ImageList.Where(P => P.ImageID == imageID).FirstOrDefault();
                    if (imgModel != null)
                    {
                        imgModel.OnPropertyChanged("ImageUrl");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("OnMarketStickerImageDownloaded ==> " + ex);
            }
        }

        public void OnMarketStickerImageListDownloaded(int categoryID, bool status)
        {
            try
            {
                if (status)
                {
                    MarketStickerCategoryDTO marketCtgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(categoryID);
                    if (marketCtgDTO != null && marketCtgDTO.Downloaded == false)
                    {
                        marketCtgDTO.Downloaded = true;
                        marketCtgDTO.DownloadTime = ChatService.GetServerTime();
                        MarketStickerCategoryModel ctgModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == categoryID).FirstOrDefault();
                        if (ctgModel != null)
                        {
                            ctgModel.Downloaded = true;
                            ctgModel.IsDownloadRunning = false;
                            ctgModel.DownloadTime = marketCtgDTO.DownloadTime;
                        }

                        List<MarketStickerCategoryDTO> marketCtgDTOs = new List<MarketStickerCategoryDTO>();
                        marketCtgDTOs.Add(marketCtgDTO);
                        RingMarketStickerLoadUtility.LoadMarketStickerRecentCategorys(marketCtgDTOs);
                        new InsertIntoRingMarketStickerTable(marketCtgDTOs);
                        new InsertIntoRingMarketStickerTable(marketCtgDTO.ImagesList);

                        // RingMarkerStickerDAO.UpdateStickerCategoryDownloadStatus(marketCtgDTO.SCtId, marketCtgDTO.Downloaded, marketCtgDTO.DownloadTime);
                    }
                }
                else
                {
                    string categoryName = String.Empty;

                    MarketStickerCategoryModel ctgModel = RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == categoryID).FirstOrDefault();
                    if (ctgModel != null)
                    {
                        categoryName = ctgModel.StickerCategoryName;
                        if (ctgModel.IsDetailImageDownloading)
                        {
                            ctgModel.IsDetailImageDownloading = false;
                        }
                        if (ctgModel.IsThumbDetailImageDownloading)
                        {
                            ctgModel.IsThumbDetailImageDownloading = false;
                        }

                        ctgModel.IsDownloadRunning = false;
                        ctgModel.DownloadPercentage = 0;
                    }

                    if (UCGuiRingID.Instance.IsVisible)
                    {
                        Application.Current.Dispatcher.BeginInvoke(() =>
                        {
                            UIHelperMethods.ShowFailed(String.Format(NotificationMessages.STICKER_DOWNLOAD_FAILED, categoryName), "Sticker download");
                            //CustomMessageBox.ShowError(String.Format(NotificationMessages.STICKER_DOWNLOAD_FAILED, categoryName));
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("OnMarketStickerImageListDownloaded ==> " + ex.Message + ex.StackTrace);
            }
        }

        public void OnOnMarketStickerNewCount(int totalNewCount, int newStickerUnseen)
        {
            RingIDViewModel.Instance.TotalNewStickers = totalNewCount;
            RingIDViewModel.Instance.NewStickerNotificationCounter = newStickerUnseen;
        }
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

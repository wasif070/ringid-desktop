<<<<<<< HEAD
﻿using Auth.Service.RingMarket;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using View.BindingModels;
using View.Constants;
using View.Dictonary;
using View.UI;
using View.UI.RingMarket;
using View.ViewModel;

namespace View.Utility.RingMarket
{
    class RingMarketStickerLoadUtility
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(RingMarketStickerLoadUtility).Name);

        private static readonly object _LoadSyncRoot = new object();
        private static RingMarketStickerLoadUtility _Instance = null;

        public RingMarketStickerLoadUtility()
        {
            _Instance = this;
        }

        public static void LoadMarketStickerRecentCategorys(List<MarketStickerCategoryDTO> list)
        {
            if (list != null && list.Count > 0)
            {
                Thread _CategoryThread = new Thread(() => RunRecentCat(list));
                _CategoryThread.Start();
            }
        }

        public static void RunRecentCat(object state, bool IsFromHandler = false)
        {
            List<MarketStickerCategoryDTO> ctgList = (List<MarketStickerCategoryDTO>)state;
            if (ctgList.Count > 0)
            {
                foreach (MarketStickerCategoryDTO ctgDTo in ctgList)
                {
                    AddStickerCategoryModel(ctgDTo);
                    if (!IsFromHandler)
                    {
                        Thread.Sleep(10);
                    }
                }

                if (RingIDViewModel.Instance.RecentStickerCategoryList.Count > 0 && UCRingChatStickerLowerPanel.Instance != null && UIDictionaries.Instance.RING_MARKET_STICKER_DICTIONARY.Count == 0)
                {
                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        UCRingChatStickerLowerPanel.Instance.OnNavigate(RingIDViewModel.Instance.RecentStickerCategoryList[0]);
                    });
                }
            }
        }


        public static void LoadMarketStickerCategorys(List<MarketStickerCategoryDTO> list)
        {
            if (list != null && list.Count > 0)
            {
                Thread _CategoryThread = new Thread(() => RunAllCat(list));
                _CategoryThread.Start();
            }
        }

        public static void RunAllCat(object state)
        {
            List<MarketStickerCategoryDTO> allCtgDTOList = ((List<MarketStickerCategoryDTO>)state).ToList();
            if (allCtgDTOList.Count > 0)
            {
                if (UCMiddlePanelSwitcher.View_MenuStickerWrapper != null && UCStickerMarketInitPanel.ViewStickerHome != null && UCStickerMarketInitPanel.ViewStickerHome._StickerMarketHomeLoaded == true)
                {
                    foreach (int prlCtgId in StickerDictionaries.Instance.MARKET_STICKER_POPULAR_LIST)
                    {
                        MarketStickerCategoryDTO ctgDTO = allCtgDTOList.Where(P => P.sCtId == prlCtgId).FirstOrDefault();
                        if (ctgDTO != null)
                        {
                            MarketStickerCategoryModel categoryModel = AddStickerCategoryModel(ctgDTO);
                            if (UCStickerMarketInitPanel.ViewStickerHome.StickerPopularCategoryList.Where(P => P.StickerCategoryID == categoryModel.StickerCategoryID).FirstOrDefault() == null)
                            {
                                UCStickerMarketInitPanel.ViewStickerHome.StickerPopularCategoryList.InvokeAdd(categoryModel);
                                if (UCStickerMarketInitPanel.ViewStickerHome.StickerPopularCategoryList.Count >= 2)
                                {
                                    break;
                                }
                                Thread.Sleep(5);
                            }
                        }
                    }

                    foreach (int newCtgId in StickerDictionaries.Instance.MARKET_STICKER_NEW_LIST)
                    {
                        MarketStickerCategoryDTO ctgDTO = allCtgDTOList.Where(P => P.sCtId == newCtgId).FirstOrDefault();
                        if (ctgDTO != null)
                        {
                            MarketStickerCategoryModel categoryModel = AddStickerCategoryModel(ctgDTO);
                            if (UCStickerMarketInitPanel.ViewStickerHome.StickerNewCategoryList.Where(P => P.StickerCategoryID == categoryModel.StickerCategoryID).FirstOrDefault() == null)
                            {
                                UCStickerMarketInitPanel.ViewStickerHome.StickerNewCategoryList.InvokeAdd(categoryModel);
                                if (UCStickerMarketInitPanel.ViewStickerHome.StickerNewCategoryList.Count >= 2)
                                {
                                    break;
                                }
                                Thread.Sleep(5);
                            }
                        }
                    }

                    foreach (int allCtgId in StickerDictionaries.Instance.MARKET_STICKER_ALL_LIST)
                    {
                        MarketStickerCategoryDTO ctgDTO = allCtgDTOList.Where(P => P.sCtId == allCtgId).FirstOrDefault();
                        if (ctgDTO != null)
                        {
                            MarketStickerCategoryModel categoryModel = AddStickerCategoryModel(ctgDTO);
                            if (UCStickerMarketInitPanel.ViewStickerHome.StickerAllCategoryList.Where(P => P.StickerCategoryID == categoryModel.StickerCategoryID).FirstOrDefault() == null)
                            {
                                UCStickerMarketInitPanel.ViewStickerHome.StickerAllCategoryList.InvokeAdd(categoryModel);
                                if (UCStickerMarketInitPanel.ViewStickerHome.StickerAllCategoryList.Count >= 4)
                                {
                                    break;
                                }
                                Thread.Sleep(5);
                            }
                        }
                    }
                }
            }
        }

        public static void LoadMarketStickerCollections(List<MarketStickerCollectionDTO> list)
        {
            if (list != null && list.Count > 0)
            {
                Thread _CollectionThread = new Thread(RunAllCollections);
                _CollectionThread.Start(list);
            }
        }

        private static void RunAllCollections(object state)
        {
            List<MarketStickerCollectionDTO> ctgList = (List<MarketStickerCollectionDTO>)state;

            if (UCMiddlePanelSwitcher.View_MenuStickerWrapper != null)
            {
                if (UCStickerMarketInitPanel.ViewStickerCollection != null && UCStickerMarketInitPanel.ViewStickerCollection._StickerMarketCollectionLoaded == true)
                {
                    List<MarketStickerCategoryModel> tempCtgModelList = RingIDViewModel.Instance.MarketStickerCategoryList.ToList();

                    foreach (MarketStickerCollectionDTO collectionDto in StickerDictionaries.Instance.MARKET_STICKER_COLLECTION_DICTIONARY.Values)
                    {
                        if (UCStickerMarketInitPanel.ViewStickerCollection.CollectionModelList.Where(P => P.StickerCollectionID == collectionDto.sClId).FirstOrDefault() == null)
                        {
                            MarketStickerCollectionModel collectionModel = new MarketStickerCollectionModel(collectionDto);
                            UCStickerMarketInitPanel.ViewStickerCollection.CollectionModelList.InvokeAdd(collectionModel);
                            System.Threading.Thread.Sleep(10);

                            foreach (int categoryId in collectionDto.catIds)
                            {
                                MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == categoryId).FirstOrDefault();
                                if (ctgModel == null)
                                {
                                    MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == categoryId).FirstOrDefault();
                                    if (ctgDTO != null)
                                    {
                                        ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                                    }
                                }
                                if (ctgModel != null)
                                {
                                    if (collectionModel.CategoryList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                                    {
                                        collectionModel.CategoryList.InvokeAdd(ctgModel);
                                        System.Threading.Thread.Sleep(5);
                                    }
                                }
                            }
                        }

                        System.Threading.Thread.Sleep(10);
                    }

                    UCStickerMarketInitPanel.ViewStickerCollection.LoadMoreInitDataIfNeeds();
                }
            }
        }

        public static void LoadMarketStickerLanguage(List<MarketStickerLanguageyDTO> list)
        {
            if (list != null && list.Count > 0)
            {
                Thread _LanguageThread = new Thread(RunLanguage);
                _LanguageThread.Start(list);
            }
        }

        private static void RunLanguage(object state)
        {
            List<MarketStickerLanguageyDTO> laguageList = (List<MarketStickerLanguageyDTO>)state;
            foreach (MarketStickerLanguageyDTO langDto in laguageList)
            {
                if (UCMiddlePanelSwitcher.View_MenuStickerWrapper != null)
                {
                    if (UCStickerMarketInitPanel.ViewStickerLanguage != null && UCStickerMarketInitPanel.ViewStickerLanguage._StickerMarketLanguageLoaded == true)
                    {
                        List<MarketStickerCategoryModel> tempCtgModelList = RingIDViewModel.Instance.MarketStickerCategoryList.ToList();

                        foreach (MarketStickerLanguageyDTO langDTO in StickerDictionaries.Instance.STICKER_COUNTRYLIST)
                        {
                            if (UCStickerMarketInitPanel.ViewStickerLanguage.LanguageModelList.Where(P => P.CountryName == langDTO.name).FirstOrDefault() == null)
                            {
                                MarketStickerLanguageModel langModel = new MarketStickerLanguageModel(langDTO);
                                UCStickerMarketInitPanel.ViewStickerLanguage.LanguageModelList.InvokeAdd(langModel);
                                System.Threading.Thread.Sleep(10);

                                foreach (int categoryId in langDto.catIds)
                                {
                                    MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == categoryId).FirstOrDefault();
                                    if (ctgModel == null)
                                    {
                                        MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == categoryId).FirstOrDefault();
                                        if (ctgDTO != null)
                                        {
                                            ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                                        }
                                    }
                                    if (ctgModel != null)
                                    {
                                        if (langModel.CategoryList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                                        {
                                            langModel.CategoryList.InvokeAdd(ctgModel);
                                            System.Threading.Thread.Sleep(5);
                                        }
                                    }
                                }
                            }
                        }

                        UCStickerMarketInitPanel.ViewStickerLanguage.LoadMoreInitDataIfNeeds();
                    }
                }
            }
        }

        public static MarketStickerCategoryModel AddStickerCategoryModel(MarketStickerCategoryDTO newDTO)
        {
            MarketStickerCategoryModel categoryModel = null;
            try
            {
                categoryModel = newDTO.sCtId > 0 ? RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == newDTO.sCtId).FirstOrDefault() : RingIDViewModel.Instance.RecentStickerCategoryList.Where(P => P.StickerCategoryID == newDTO.sCtId).FirstOrDefault();
                if (categoryModel == null)
                {
                    categoryModel = new MarketStickerCategoryModel(newDTO);
                    newDTO.ImagesList.ForEach(i =>
                    {
                        categoryModel.ImageList.InvokeAdd(new MarkertStickerImagesModel(i));
                    });

                    int idx = GetStickerCategorySortPosition(categoryModel);

                    if (idx > -1)
                    {
                        RingIDViewModel.Instance.MarketStickerCategoryList.InvokeInsert(idx, categoryModel);
                    }

                    if (idx < 4)
                    {
                        int rIdx = GetRecentStickerCategorySortPosition(categoryModel); // recent sticker
                        if (rIdx < 4)
                        {
                            RingIDViewModel.Instance.RecentStickerCategoryList.InvokeInsert(rIdx, categoryModel);
                            if (RingIDViewModel.Instance.RecentStickerCategoryList.Count > 4)
                            {
                                RingIDViewModel.Instance.RecentStickerCategoryList.InvokeRemoveAt(4);
                            }
                        }
                    }
                }
                else
                {
                    //categoryModel.UpdateData(newDTO);
                    List<MarkertStickerImagesDTO> imagesList = categoryModel.ImageList.Count > 0 ? newDTO.ImagesList.Where(P => !categoryModel.ImageList.Any(Q => Q.ImageID == P.imId)).ToList() : newDTO.ImagesList;
                    imagesList.ForEach(i =>
                    {
                        categoryModel.ImageList.InvokeAdd(new MarkertStickerImagesModel(i));
                    });

                    int oldIdx = RingIDViewModel.Instance.RecentStickerCategoryList.IndexOf(categoryModel);
                    int newIdx = GetRecentStickerCategorySortPosition(categoryModel);

                    if (newIdx < 4)
                    {
                        if (oldIdx < 4 && oldIdx >= 0)
                        {
                            if (oldIdx != newIdx)
                            {
                                RingIDViewModel.Instance.RecentStickerCategoryList.InvokeMove(oldIdx, newIdx);
                            }
                        }
                        else
                        {
                            RingIDViewModel.Instance.RecentStickerCategoryList.InvokeInsert(newIdx, categoryModel);
                            if (RingIDViewModel.Instance.RecentStickerCategoryList.Count > 4)
                            {
                                RingIDViewModel.Instance.RecentStickerCategoryList.InvokeRemoveAt(4);
                            }
                        }
                    }

                    if (categoryModel.StickerCategoryID > 0)
                    {
                        oldIdx = RingIDViewModel.Instance.MarketStickerCategoryList.IndexOf(categoryModel);
                        newIdx = GetStickerCategorySortPosition(categoryModel);

                        if (oldIdx != newIdx)
                        {
                            RingIDViewModel.Instance.MarketStickerCategoryList.InvokeMove(oldIdx, newIdx);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddStickerCategoryModel() ==> " + ex.Message + "\n" + ex.StackTrace);
            }

            return categoryModel;
        }

        public static int GetStickerCategorySortPosition(MarketStickerCategoryModel marketCtgModel)
        {
            int min = 0;
            try
            {
                double calValue = 0;

                if (marketCtgModel.StickerCategoryID == 0)
                {
                    return -1;
                }
                else if (marketCtgModel.IsDefault)
                {
                    calValue = 30000 + (double)5000 / marketCtgModel.StickerCategoryID;
                }
                //else if (marketCtgModel.LastUsedDate > 0)
                //{
                //    calValue = 20000 + (double)marketCtgModel.LastUsedDate / 2592000000;
                //}
                else if (marketCtgModel.Downloaded && marketCtgModel.DownloadTime > 0)
                {
                    calValue = 20000 + (double)marketCtgModel.DownloadTime / 2592000000;
                }
                else if (marketCtgModel.DownloadTime > 0) // at least once downloaded but removed by user
                {
                    calValue = 10000 + (double)marketCtgModel.DownloadTime / 2592000000;
                }
                else
                {
                    calValue = (double)5000 / marketCtgModel.StickerCategoryID;
                }

                int max = RingIDViewModel.Instance.MarketStickerCategoryList.Count;
                int pivot;

                while (max > min)
                {
                    pivot = (min + max) / 2;
                    MarketStickerCategoryModel pivotModel = RingIDViewModel.Instance.MarketStickerCategoryList.ElementAt(pivot);
                    double pivotValue = pivotModel.CalculationValue;
                    if (calValue < pivotValue)
                    {
                        min = pivot + 1;
                    }
                    else
                    {
                        max = pivot;
                    }
                }

                marketCtgModel.CalculationValue = calValue;

                //log.Error(" min = " + min + " , CalculationValue = " + calValue + " , StickerCategoryID = " + categoryModel.StickerCategoryID + " , Rank = " + categoryModel.Rank);
            }
            catch (Exception ex)
            {
                log.Error("Error: StickerCategorySortPosition()." + ex.Message + "\n" + ex.StackTrace);
            }
            return min;
        }

        public static int GetRecentStickerCategorySortPosition(MarketStickerCategoryModel marketCtgModel)
        {
            int min = 0;
            try
            {
                double calValue = 0;

                if (marketCtgModel.StickerCategoryID == 0)
                {
                    calValue = 40000;
                }
                else if (marketCtgModel.IsDefault)
                {
                    calValue = 30000 + (double)5000 / marketCtgModel.StickerCategoryID;
                }
                //else if (categoryModel.LastUsedDate > 0)
                //{
                //    calValue = 20000 + (double)categoryModel.LastUsedDate / 2592000000;
                //}
                else if (marketCtgModel.Downloaded && marketCtgModel.DownloadTime > 0)
                {
                    calValue = 20000 + (double)marketCtgModel.DownloadTime / 2592000000;
                }
                else if (marketCtgModel.DownloadTime > 0) // at least once downloaded but removed by user
                {
                    calValue = 10000 + (double)marketCtgModel.DownloadTime / 2592000000;
                }
                else
                {
                    calValue = (double)5000 / marketCtgModel.StickerCategoryID;
                }

                int max = RingIDViewModel.Instance.RecentStickerCategoryList.Count;
                int pivot;

                while (max > min)
                {
                    pivot = (min + max) / 2;
                    MarketStickerCategoryModel pivotModel = RingIDViewModel.Instance.RecentStickerCategoryList.ElementAt(pivot);
                    double pivotValue = pivotModel.CalculationValue;
                    if (calValue < pivotValue)
                    {
                        min = pivot + 1;
                    }
                    else
                    {
                        max = pivot;
                    }
                }

                marketCtgModel.CalculationValue = calValue;

                //log.Error(" min = " + min + " , RecentCalculationValue = " + calValue + " , StickerCategoryID = " + categoryModel.StickerCategoryID + " , Rank = " + categoryModel.Rank);
            }
            catch (Exception ex)
            {
                log.Error("Error: StickerCategorySortPosition()." + ex.Message + "\n" + ex.StackTrace);
            }
            return min;
        }

        public static void AddRecentSticker(MarkertStickerImagesModel imageModel)
        {
            MarketStickerCategoryDTO recentCtgDTO;
            if (!StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(0, out recentCtgDTO))
            {
                recentCtgDTO = new MarketStickerCategoryDTO();
                recentCtgDTO.sCtId = 0;
                recentCtgDTO.sClId = 0;
                recentCtgDTO.sctName = "Recent";
                recentCtgDTO.Downloaded = true;
                recentCtgDTO.DownloadTime = 0;
                recentCtgDTO.SortOrder = 0;
                recentCtgDTO.IsVisible = true;
                recentCtgDTO.IsDefault = false;
                recentCtgDTO.LastUsedDate = 0;
                StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY[0] = recentCtgDTO;
            }

            MarkertStickerImagesDTO imageDTO = recentCtgDTO.ImagesList.Where(P => P.imId == imageModel.ImageID).FirstOrDefault();
            if (imageDTO == null)
            {
                imageModel.IsRecent = true;
                imageDTO = new MarkertStickerImagesDTO();
                imageDTO.imId = imageModel.ImageID;
                imageDTO.sClId = imageModel.StickerCollectionID;
                imageDTO.sCtId = imageModel.StickerCategoryID;
                imageDTO.imUrl = imageModel.ImageUrl;
                imageDTO.IsRecent = imageModel.IsRecent;
                recentCtgDTO.ImagesList.Add(imageDTO);

                RingMarkerStickerDAO.UpdateStickerRecent(imageModel.StickerCategoryID, imageModel.ImageID);
                List<MarketStickerCategoryDTO> tempList = new List<MarketStickerCategoryDTO>();
                tempList.Add(recentCtgDTO);
                LoadMarketStickerRecentCategorys(tempList);
            }
        }

        public static void LaodAndDownLoadStickerImages(MarketStickerCategoryModel model, MarketStickerCategoryDTO ctgDTO, Func<bool, int> isNoInternetAvailable = null)
        {
            if (model != null && model.StickerCategoryID >= 0 && ctgDTO != null)// those are in db but model is not constructed.
            {
                if (model.ImageList.Count < ctgDTO.ImagesList.Count)
                {
                    ctgDTO.ImagesList.ForEach(i =>
                    {
                        MarkertStickerImagesModel imgModel = model.ImageList.Where(P => P.ImageID == i.imId).FirstOrDefault();
                        if (imgModel == null)
                        {
                            imgModel = new MarkertStickerImagesModel(i);
                            model.ImageList.InvokeAdd(imgModel);
                        }
                    });
                }

                new RingMarketStickerService(model.StickerCategoryID, RingIDSettings.STICKER_FOLDER, new RingMarkerStickerHandler(), 0, (isNoInternet)=>
                {
                    if (isNoInternet && isNoInternetAvailable !=null)
                    {
                        isNoInternetAvailable(true);                       
                    }
                    return 0;
                });
            }
        }

        public static void DownLoadStickerImages(MarketStickerCategoryDTO ctgDTO, Func<bool, int> isInternetAvailable = null)
        {
            if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
            {
                new System.Threading.Thread(() =>
                {
                    RingMarketStickerService.Instance.GetMarketStickerImagesOfACategory(ctgDTO, new RingMarkerStickerHandler());
                    new RingMarketStickerService(ctgDTO.sCtId, RingIDSettings.STICKER_FOLDER, new RingMarkerStickerHandler());
                }).Start();
            }
            else if (isInternetAvailable != null)
            {
                isInternetAvailable(true);
            }
        }
    }
}
=======
﻿using Auth.Service.RingMarket;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using View.BindingModels;
using View.Constants;
using View.Dictonary;
using View.UI;
using View.UI.RingMarket;
using View.ViewModel;

namespace View.Utility.RingMarket
{
    class RingMarketStickerLoadUtility
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(RingMarketStickerLoadUtility).Name);

        private static readonly object _LoadSyncRoot = new object();
        private static RingMarketStickerLoadUtility _Instance = null;

        public RingMarketStickerLoadUtility()
        {
            _Instance = this;
        }

        public static void LoadMarketStickerRecentCategorys(List<MarketStickerCategoryDTO> list)
        {
            if (list != null && list.Count > 0)
            {
                Thread _CategoryThread = new Thread(() => RunRecentCat(list));
                _CategoryThread.Start();
            }
        }

        public static void RunRecentCat(object state, bool IsFromHandler = false)
        {
            List<MarketStickerCategoryDTO> ctgList = (List<MarketStickerCategoryDTO>)state;
            if (ctgList.Count > 0)
            {
                foreach (MarketStickerCategoryDTO ctgDTo in ctgList)
                {
                    AddStickerCategoryModel(ctgDTo);
                    if (!IsFromHandler)
                    {
                        Thread.Sleep(10);
                    }
                }

                if (RingIDViewModel.Instance.RecentStickerCategoryList.Count > 0 && UCRingChatStickerLowerPanel.Instance != null && UIDictionaries.Instance.RING_MARKET_STICKER_DICTIONARY.Count == 0)
                {
                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        UCRingChatStickerLowerPanel.Instance.OnNavigate(RingIDViewModel.Instance.RecentStickerCategoryList[0]);
                    });
                }
            }
        }


        public static void LoadMarketStickerCategorys(List<MarketStickerCategoryDTO> list)
        {
            if (list != null && list.Count > 0)
            {
                Thread _CategoryThread = new Thread(() => RunAllCat(list));
                _CategoryThread.Start();
            }
        }

        public static void RunAllCat(object state)
        {
            List<MarketStickerCategoryDTO> allCtgDTOList = ((List<MarketStickerCategoryDTO>)state).ToList();
            if (allCtgDTOList.Count > 0)
            {
                if (UCMiddlePanelSwitcher.View_MenuStickerWrapper != null && UCStickerMarketInitPanel.ViewStickerHome != null && UCStickerMarketInitPanel.ViewStickerHome._StickerMarketHomeLoaded == true)
                {
                    UCStickerMarketInitPanel.ViewStickerHome.RunHomeThread();
                }
            }
        }

        public static void LoadMarketStickerCollections(List<MarketStickerCollectionDTO> list)
        {
            if (list != null && list.Count > 0)
            {
                Thread _CollectionThread = new Thread(RunAllCollections);
                _CollectionThread.Start(list);
            }
        }

        private static void RunAllCollections(object state)
        {
            List<MarketStickerCollectionDTO> ctgList = (List<MarketStickerCollectionDTO>)state;

            if (UCMiddlePanelSwitcher.View_MenuStickerWrapper != null)
            {
                if (UCStickerMarketInitPanel.ViewStickerCollection != null && UCStickerMarketInitPanel.ViewStickerCollection._StickerMarketCollectionLoaded == true)
                {
                    List<MarketStickerCategoryModel> tempCtgModelList = RingIDViewModel.Instance.MarketStickerCategoryList.ToList();

                    foreach (MarketStickerCollectionDTO collectionDto in StickerDictionaries.Instance.MARKET_STICKER_COLLECTION_DICTIONARY.Values)
                    {
                        if (UCStickerMarketInitPanel.ViewStickerCollection.CollectionModelList.Where(P => P.StickerCollectionID == collectionDto.sClId).FirstOrDefault() == null)
                        {
                            MarketStickerCollectionModel collectionModel = new MarketStickerCollectionModel(collectionDto);
                            UCStickerMarketInitPanel.ViewStickerCollection.CollectionModelList.InvokeAdd(collectionModel);
                            System.Threading.Thread.Sleep(10);

                            foreach (int categoryId in collectionDto.catIds)
                            {
                                MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == categoryId).FirstOrDefault();
                                if (ctgModel == null)
                                {
                                    MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == categoryId).FirstOrDefault();
                                    if (ctgDTO != null)
                                    {
                                        ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                                    }
                                }
                                if (ctgModel != null)
                                {
                                    if (collectionModel.CategoryList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                                    {
                                        collectionModel.CategoryList.InvokeAdd(ctgModel);
                                        System.Threading.Thread.Sleep(5);
                                    }
                                }
                            }
                        }

                        System.Threading.Thread.Sleep(10);
                    }

                    UCStickerMarketInitPanel.ViewStickerCollection.LoadMoreInitDataIfNeeds();
                }
            }
        }

        public static void LoadMarketStickerLanguage(List<MarketStickerLanguageyDTO> list)
        {
            if (list != null && list.Count > 0)
            {
                Thread _LanguageThread = new Thread(RunLanguage);
                _LanguageThread.Start(list);
            }
        }

        private static void RunLanguage(object state)
        {
            List<MarketStickerLanguageyDTO> laguageList = (List<MarketStickerLanguageyDTO>)state;
            foreach (MarketStickerLanguageyDTO langDto in laguageList)
            {
                if (UCMiddlePanelSwitcher.View_MenuStickerWrapper != null)
                {
                    if (UCStickerMarketInitPanel.ViewStickerLanguage != null && UCStickerMarketInitPanel.ViewStickerLanguage._StickerMarketLanguageLoaded == true)
                    {
                        List<MarketStickerCategoryModel> tempCtgModelList = RingIDViewModel.Instance.MarketStickerCategoryList.ToList();

                        foreach (MarketStickerLanguageyDTO langDTO in StickerDictionaries.Instance.STICKER_COUNTRYLIST)
                        {
                            if (UCStickerMarketInitPanel.ViewStickerLanguage.LanguageModelList.Where(P => P.CountryName == langDTO.name).FirstOrDefault() == null)
                            {
                                MarketStickerLanguageModel langModel = new MarketStickerLanguageModel(langDTO);
                                UCStickerMarketInitPanel.ViewStickerLanguage.LanguageModelList.InvokeAdd(langModel);
                                System.Threading.Thread.Sleep(10);

                                foreach (int categoryId in langDto.catIds)
                                {
                                    MarketStickerCategoryModel ctgModel = tempCtgModelList.Where(P => P.StickerCategoryID == categoryId).FirstOrDefault();
                                    if (ctgModel == null)
                                    {
                                        MarketStickerCategoryDTO ctgDTO = StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.Values.Where(P => P.sCtId == categoryId).FirstOrDefault();
                                        if (ctgDTO != null)
                                        {
                                            ctgModel = RingMarketStickerLoadUtility.AddStickerCategoryModel(ctgDTO);
                                        }
                                    }
                                    if (ctgModel != null)
                                    {
                                        if (langModel.CategoryList.Where(P => P.StickerCategoryID == ctgModel.StickerCategoryID).FirstOrDefault() == null)
                                        {
                                            langModel.CategoryList.InvokeAdd(ctgModel);
                                            System.Threading.Thread.Sleep(5);
                                        }
                                    }
                                }
                            }
                        }

                        UCStickerMarketInitPanel.ViewStickerLanguage.LoadMoreInitDataIfNeeds();
                    }
                }
            }
        }

        public static MarketStickerCategoryModel AddStickerCategoryModel(MarketStickerCategoryDTO newDTO)
        {
            MarketStickerCategoryModel categoryModel = null;
            try
            {
                categoryModel = newDTO.sCtId > 0 ? RingIDViewModel.Instance.MarketStickerCategoryList.Where(P => P.StickerCategoryID == newDTO.sCtId).FirstOrDefault() : RingIDViewModel.Instance.RecentStickerCategoryList.Where(P => P.StickerCategoryID == newDTO.sCtId).FirstOrDefault();
                if (categoryModel == null)
                {
                    categoryModel = new MarketStickerCategoryModel(newDTO);
                    newDTO.ImagesList.ForEach(i =>
                    {
                        categoryModel.ImageList.InvokeAdd(new MarkertStickerImagesModel(i));
                    });

                    int idx = GetStickerCategorySortPosition(categoryModel);

                    if (idx > -1)
                    {
                        RingIDViewModel.Instance.MarketStickerCategoryList.InvokeInsert(idx, categoryModel);
                    }

                    if (idx < 4)
                    {
                        int rIdx = GetRecentStickerCategorySortPosition(categoryModel); // recent sticker
                        if (rIdx < 4)
                        {
                            RingIDViewModel.Instance.RecentStickerCategoryList.InvokeInsert(rIdx, categoryModel);
                            if (RingIDViewModel.Instance.RecentStickerCategoryList.Count > 4)
                            {
                                RingIDViewModel.Instance.RecentStickerCategoryList.InvokeRemoveAt(4);
                            }
                        }
                    }
                }
                else
                {
                    //categoryModel.UpdateData(newDTO);
                    List<MarkertStickerImagesDTO> imagesList = categoryModel.ImageList.Count > 0 ? newDTO.ImagesList.Where(P => !categoryModel.ImageList.Any(Q => Q.ImageID == P.imId)).ToList() : newDTO.ImagesList;
                    imagesList.ForEach(i =>
                    {
                        categoryModel.ImageList.InvokeAdd(new MarkertStickerImagesModel(i));
                    });

                    int oldIdx = RingIDViewModel.Instance.RecentStickerCategoryList.IndexOf(categoryModel);
                    int newIdx = GetRecentStickerCategorySortPosition(categoryModel);

                    if (newIdx < 4)
                    {
                        if (oldIdx < 4 && oldIdx >= 0)
                        {
                            if (oldIdx != newIdx)
                            {
                                RingIDViewModel.Instance.RecentStickerCategoryList.InvokeMove(oldIdx, newIdx);
                            }
                        }
                        else
                        {
                            RingIDViewModel.Instance.RecentStickerCategoryList.InvokeInsert(newIdx, categoryModel);
                            if (RingIDViewModel.Instance.RecentStickerCategoryList.Count > 4)
                            {
                                RingIDViewModel.Instance.RecentStickerCategoryList.InvokeRemoveAt(4);
                            }
                        }
                    }

                    if (categoryModel.StickerCategoryID > 0)
                    {
                        oldIdx = RingIDViewModel.Instance.MarketStickerCategoryList.IndexOf(categoryModel);
                        newIdx = GetStickerCategorySortPosition(categoryModel);

                        if (oldIdx != newIdx)
                        {
                            RingIDViewModel.Instance.MarketStickerCategoryList.InvokeMove(oldIdx, newIdx);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("AddStickerCategoryModel() ==> " + ex.Message + "\n" + ex.StackTrace);
            }

            return categoryModel;
        }

        public static int GetStickerCategorySortPosition(MarketStickerCategoryModel marketCtgModel)
        {
            int min = 0;
            try
            {
                double calValue = 0;

                if (marketCtgModel.StickerCategoryID == 0)
                {
                    return -1;
                }
                else if (marketCtgModel.IsDefault)
                {
                    calValue = 30000 + (double)5000 / marketCtgModel.StickerCategoryID;
                }
                //else if (marketCtgModel.LastUsedDate > 0)
                //{
                //    calValue = 20000 + (double)marketCtgModel.LastUsedDate / 2592000000;
                //}
                else if (marketCtgModel.Downloaded && marketCtgModel.DownloadTime > 0)
                {
                    calValue = 20000 + (double)marketCtgModel.DownloadTime / 2592000000;
                }
                else if (marketCtgModel.DownloadTime > 0) // at least once downloaded but removed by user
                {
                    calValue = 10000 + (double)marketCtgModel.DownloadTime / 2592000000;
                }
                else
                {
                    calValue = (double)5000 / marketCtgModel.StickerCategoryID;
                }

                int max = RingIDViewModel.Instance.MarketStickerCategoryList.Count;
                int pivot;

                while (max > min)
                {
                    pivot = (min + max) / 2;
                    MarketStickerCategoryModel pivotModel = RingIDViewModel.Instance.MarketStickerCategoryList.ElementAt(pivot);
                    double pivotValue = pivotModel.CalculationValue;
                    if (calValue < pivotValue)
                    {
                        min = pivot + 1;
                    }
                    else
                    {
                        max = pivot;
                    }
                }

                marketCtgModel.CalculationValue = calValue;

                //log.Error(" min = " + min + " , CalculationValue = " + calValue + " , StickerCategoryID = " + categoryModel.StickerCategoryID + " , Rank = " + categoryModel.Rank);
            }
            catch (Exception ex)
            {
                log.Error("Error: StickerCategorySortPosition()." + ex.Message + "\n" + ex.StackTrace);
            }
            return min;
        }

        public static int GetRecentStickerCategorySortPosition(MarketStickerCategoryModel marketCtgModel)
        {
            int min = 0;
            try
            {
                double calValue = 0;

                if (marketCtgModel.StickerCategoryID == 0)
                {
                    calValue = 40000;
                }
                else if (marketCtgModel.IsDefault)
                {
                    calValue = 30000 + (double)5000 / marketCtgModel.StickerCategoryID;
                }
                //else if (categoryModel.LastUsedDate > 0)
                //{
                //    calValue = 20000 + (double)categoryModel.LastUsedDate / 2592000000;
                //}
                else if (marketCtgModel.Downloaded && marketCtgModel.DownloadTime > 0)
                {
                    calValue = 20000 + (double)marketCtgModel.DownloadTime / 2592000000;
                }
                else if (marketCtgModel.DownloadTime > 0) // at least once downloaded but removed by user
                {
                    calValue = 10000 + (double)marketCtgModel.DownloadTime / 2592000000;
                }
                else
                {
                    calValue = (double)5000 / marketCtgModel.StickerCategoryID;
                }

                int max = RingIDViewModel.Instance.RecentStickerCategoryList.Count;
                int pivot;

                while (max > min)
                {
                    pivot = (min + max) / 2;
                    MarketStickerCategoryModel pivotModel = RingIDViewModel.Instance.RecentStickerCategoryList.ElementAt(pivot);
                    double pivotValue = pivotModel.CalculationValue;
                    if (calValue < pivotValue)
                    {
                        min = pivot + 1;
                    }
                    else
                    {
                        max = pivot;
                    }
                }

                marketCtgModel.CalculationValue = calValue;

                //log.Error(" min = " + min + " , RecentCalculationValue = " + calValue + " , StickerCategoryID = " + categoryModel.StickerCategoryID + " , Rank = " + categoryModel.Rank);
            }
            catch (Exception ex)
            {
                log.Error("Error: StickerCategorySortPosition()." + ex.Message + "\n" + ex.StackTrace);
            }
            return min;
        }

        public static void AddRecentSticker(MarkertStickerImagesModel imageModel)
        {
            MarketStickerCategoryDTO recentCtgDTO;
            if (!StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY.TryGetValue(0, out recentCtgDTO))
            {
                recentCtgDTO = new MarketStickerCategoryDTO();
                recentCtgDTO.sCtId = 0;
                recentCtgDTO.sClId = 0;
                recentCtgDTO.sctName = "Recent";
                recentCtgDTO.Downloaded = true;
                recentCtgDTO.DownloadTime = 0;
                recentCtgDTO.SortOrder = 0;
                recentCtgDTO.IsVisible = true;
                recentCtgDTO.IsDefault = false;
                recentCtgDTO.LastUsedDate = 0;
                StickerDictionaries.Instance.MARKET_STICKER_CATEGORY_DICTIONARY[0] = recentCtgDTO;
            }

            MarkertStickerImagesDTO imageDTO = recentCtgDTO.ImagesList.Where(P => P.imId == imageModel.ImageID).FirstOrDefault();
            if (imageDTO == null)
            {
                imageModel.IsRecent = true;
                imageDTO = new MarkertStickerImagesDTO();
                imageDTO.imId = imageModel.ImageID;
                imageDTO.sClId = imageModel.StickerCollectionID;
                imageDTO.sCtId = imageModel.StickerCategoryID;
                imageDTO.imUrl = imageModel.ImageUrl;
                imageDTO.IsRecent = imageModel.IsRecent;
                recentCtgDTO.ImagesList.Add(imageDTO);

                RingMarkerStickerDAO.UpdateStickerRecent(imageModel.StickerCategoryID, imageModel.ImageID);
                List<MarketStickerCategoryDTO> tempList = new List<MarketStickerCategoryDTO>();
                tempList.Add(recentCtgDTO);
                LoadMarketStickerRecentCategorys(tempList);
            }
        }

        public static void DownLoadStickerImages(MarketStickerCategoryModel model, MarketStickerCategoryDTO ctgDTO, Func<bool, int> isNoInternetAvailable = null)
        {
            if (model != null && model.StickerCategoryID >= 0 && ctgDTO != null)// those are in db but model is not constructed.
            {
                if (DefaultSettings.IsInternetAvailable && DefaultSettings.LOGIN_SESSIONID != null)
                {
                    new System.Threading.Thread(() =>
                    {
                        if (model.ImageList.Count < ctgDTO.ImagesList.Count)
                        {
                            ctgDTO.ImagesList.ForEach(i =>
                            {
                                MarkertStickerImagesModel imgModel = model.ImageList.Where(P => P.ImageID == i.imId).FirstOrDefault();
                                if (imgModel == null)
                                {
                                    imgModel = new MarkertStickerImagesModel(i);
                                    model.ImageList.InvokeAdd(imgModel);
                                }
                            });
                        }
                        else
                        {
                            RingMarketStickerService.Instance.GetMarketStickerImagesOfACategory(ctgDTO, new RingMarkerStickerHandler());
                        }

                        new RingMarketStickerService(model.StickerCategoryID, RingIDSettings.STICKER_FOLDER, new RingMarkerStickerHandler(), 0, (isNoInternet) =>
                        {
                            if (isNoInternet && isNoInternetAvailable != null)
                            {
                                isNoInternetAvailable(true);
                            }
                            return 0;
                        });
                    }).Start();
                }
                else if (isNoInternetAvailable != null)
                {
                    isNoInternetAvailable(true);
                }

            }
        }
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

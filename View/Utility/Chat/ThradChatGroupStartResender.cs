﻿using System;
using System.Collections.Generic;
using System.Threading;
using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace View.Utility.Chat
{
    public class ThradChatGroupStartResender
    {

        private readonly ILog log = LogManager.GetLogger(typeof(ThradChatGroupStartResender).Name);
        private long _GroupID;
        private List<long> _Members;
        private Func<long, bool, int> _OnComplete;

        public ThradChatGroupStartResender(long groupID, List<long> members, Func<long, bool, int> onComplete)
        {
            this._GroupID = groupID;
            this._Members = members;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Run));
            t.Name = this.GetType().Name;
            t.Start();
        }

        public void Run()
        {
            if (!String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                try
                {
                    JObject pakToSend = new JObject();
                    string pakId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.TagId] = _GroupID;
                    pakToSend[JsonKeys.FutIds] = _Members != null ? new JArray(_Members.ToArray()) : new JArray();
                    pakToSend[JsonKeys.PacketId] = pakId;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_START_GROUP_CHAT;

                    string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
                    Dictionary<String, byte[]> packets = SendToServer.BuildBrokenPacket(data, AppConstants.TYPE_START_GROUP_CHAT, pakId);

                    List<string> packetIds = new List<string>(packets.Keys);
                    if (packetIds.Count > 1)
                    {
                        SendToServer.SendBrokenPacket(AppConstants.BROKEN_PACKET, packets, AppConstants.REQUEST_TYPE_CHAT);
                    }
                    else
                    {
                        SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_CHAT, data);
                    }
                    string makeResponsePacket = pakId + "_" + AppConstants.TYPE_START_GROUP_CHAT;

                    for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
                    {
                        if (string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
                        {
                            break;
                        }
                        Thread.Sleep(DefaultSettings.WAITING_TIME);
                        if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(makeResponsePacket))
                        {
                            if (SendToServer.CheckAllBrokenPacketConfirmation(packets))
                            {
                                if (packetIds.Count > 1)
                                {
                                    SendToServer.SendBrokenPacket(AppConstants.BROKEN_PACKET, packets, AppConstants.REQUEST_TYPE_CHAT);
                                }
                                else
                                {
                                    SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_CHAT, data);
                                }
                            }
                        }
                        else
                        {
                            JObject _JobjFromResponse = RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(makeResponsePacket);
                            if (_JobjFromResponse != null && _JobjFromResponse[JsonKeys.Success] != null && (bool)_JobjFromResponse[JsonKeys.Success])
                            {
                                Callback(_GroupID, true);
                            }
                            else
                            {
                                Callback(_GroupID, false);
                            }
                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(makeResponsePacket);
                            return;
                        }
                        MainSwitcher.ThreadManager().PingNow();
                        //PingInServer.StartThread(i, DefaultSettings.TRYING_TIME);
                    }
                    SendToServer.RemoveAllBrokenPacketConfirmation(packetIds);
                }
                catch (Exception ex)
                {
                    log.Error("ChatGroupStartResender ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
                }
            }
            else
            {
                log.Error("ChatGroupStartResender Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            }

            Callback(_GroupID, false);
        }

        private void Callback(long groupID, bool status)
        {
            try
            {
                if (_OnComplete != null)
                {
                    _OnComplete(groupID, status);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in Callback() => " + ex.Message + "\n" + ex.StackTrace);
            }
            _OnComplete = null;
        }

    }
}

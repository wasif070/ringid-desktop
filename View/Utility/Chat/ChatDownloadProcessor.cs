﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows;
using Auth.Service.RingMarket;
using imsdkwrapper;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using View.BindingModels;
using View.Constants;
using View.Utility.Chat.Service;
using View.Utility.FriendProfile;
using View.Utility.WPFMessageBox;

namespace View.Utility.Chat
{

    public class ChatDownloadProcessor
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChatDownloadProcessor).Name);
        public MessageModel Model { get; set; }
        private bool IsPreview = false;
        private Func<bool, string, int> _OnComplete = null;

        public ChatDownloadProcessor(MessageModel model, bool isPreview = false, Func<bool, string, int> onComplete = null)
        {
            this.Model = model;
            this.IsPreview = isPreview;
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            new Thread(() =>
            {
                if (IsPreview)
                {
                    DownloadPreview();
                }
                else
                {
                    DownloadFile();
                }
            }).Start();
        }

        private void CheckFileAutoDownload()
        {
            if (Model.ViewModel.IsMediaMessage)
            {
                string tempPath = ChatHelpers.GetChatFilePath(Model.Message, Model.MessageType, Model.PacketID, Model.LinkImageUrl, Model.SenderTableID);
                if (File.Exists(tempPath))
                {
#if MESSAGE_STATUS_LOG
                    log.Debug("*** ALREADY FILE DOWNLOADED => " + Model.Message);
#endif
                    Model.IsFileOpened = true;
                    Model.ViewModel.IsForceDownload = false;
                    Model.IsDownloadRunning = false;
                    Model.ViewModel.DownloadPercentage = 0;
                }
                else if (SettingsConstants.VALUE_RINGID_IM_AUTO_DOWNLOAD)
                {
#if MESSAGE_STATUS_LOG
                    log.Debug("*** START FILE AUTO DOWNLOAD => " + Model.Message);
#endif
                    Model.IsFileOpened = false;
                    Model.ViewModel.IsForceDownload = false;
                    Model.IsDownloadRunning = true;
                    Model.ViewModel.DownloadPercentage = 0;
                    ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.AddItem(new ChatDownloadProcessor(Model));
                }
            }
        }

        public void DownloadFile()
        {
            try
            {
                switch ((MessageType)Model.MessageType)
                {
                    case MessageType.FILE_STREAM:
                        {
                            string filePath = ChatHelpers.GetChatFilePath(Model.Message, Model.MessageType, Model.PacketID, Model.LinkImageUrl, Model.SenderTableID);
                            if (!File.Exists(filePath))
                            {
                                Model.ViewModel.DownloadPercentage = 1;
                                ChatHelpers.CreateSessionOfFileStreamRequest(filePath, Model, _OnComplete);
                            }
                            else
                            {
                                Model.FileStatus = ChatConstants.FILE_TRANSFER_COMPLETED;
                                Model.ViewModel.IsForceDownload = false;
                                Model.IsDownloadRunning = false;
                                RecentChatCallActivityDAO.UpdateChatMessageFileStatus(Model.FriendTableID, Model.GroupID, Model.PacketID, Model.FileStatus);

                                if (_OnComplete != null)
                                {
                                    _OnComplete(true, Model.PacketID);
                                }
                            }
                        }
                        break;
                    default:
                        {
                            string filePath = ChatHelpers.GetChatFilePath(Model.Message, Model.MessageType, Model.PacketID, Model.LinkImageUrl, Model.SenderTableID);
                            if (File.Exists(filePath))
                            {
#if MESSAGE_STATUS_LOG
                                log.Debug("*** ALREADY FILE DOWNLOADED => " + Model.Message);
#endif
                                Model.IsFileOpened = true;
                                Model.ViewModel.IsForceDownload = false;
                                Model.IsDownloadRunning = false;
                                Model.ViewModel.DownloadPercentage = 0;
                                ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();

                                if (Model.MessageType == MessageType.IMAGE_FILE_FROM_GALLERY || Model.MessageType == MessageType.IMAGE_FILE_FROM_CAMERA)
                                {
                                    Model.IsPreviewOpened = true;
                                    Model.OnPropertyChanged("IsPreviewOpened");
                                }

                                if (_OnComplete != null)
                                {
                                    _OnComplete(true, Model.PacketID);
                                }
                                return;
                            }
#if MESSAGE_STATUS_LOG
                            log.Debug("*** FILE DOWNLOADING ........ => " + Model.Message);
#endif
                            string baseUrl = ServerAndPortSettings.IMAGE_SERVER_RESOURCE;
                            string msg = String.Empty;
                            string returnMsg = String.Empty;

                            switch ((MessageType)Model.MessageType)
                            {
                                case MessageType.IMAGE_FILE_FROM_CAMERA:
                                    msg = Model.Message;
                                    break;
                                case MessageType.IMAGE_FILE_FROM_GALLERY:
                                    msg = Model.Message;
                                    break;
                                case MessageType.AUDIO_FILE:
                                    msg = Model.Message;
                                    break;
                                case MessageType.VIDEO_FILE:
                                    msg = Model.Message;
                                    break;
                            }

                            if (Model.ViewModel.IsRoomChat)
                            {
                                ChatService.DownloadChatFile(
                                    Model.PacketType,
                                    Model.MessageType,
                                    msg,
                                    baseUrl,
                                    filePath,
                                    (s) =>
                                    {
                                        if (s)
                                        {
#if MESSAGE_STATUS_LOG
                                            log.Debug("*** FILE DOWNLOADED COMPLETED => " + Model.Message);
#endif
                                            Model.IsFileOpened = true;
                                            Model.ViewModel.IsForceDownload = false;
                                            Model.IsDownloadRunning = false;
                                            Model.ViewModel.DownloadPercentage = 0;
                                            ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();

                                            if (Model.MessageType == MessageType.IMAGE_FILE_FROM_GALLERY || Model.MessageType == MessageType.IMAGE_FILE_FROM_CAMERA)
                                            {
                                                Model.IsPreviewOpened = true;
                                                Model.OnPropertyChanged("IsPreviewOpened");
                                            }

                                            if (_OnComplete != null)
                                            {
                                                _OnComplete(true, Model.PacketID);
                                            }
                                        }
                                        else
                                        {
#if MESSAGE_STATUS_LOG
                                            log.Debug("*** FILE DOWNLOADED FAILED => " + Model.Message);
#endif
                                            try
                                            {
                                                if (File.Exists(filePath)) { File.Delete(filePath); }
                                            }
                                            catch (Exception) { }

                                            Model.IsFileOpened = false;
                                            Model.ViewModel.IsForceDownload = false;
                                            Model.IsDownloadRunning = false;
                                            Model.ViewModel.DownloadPercentage = 0;
                                            ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();

                                            if (_OnComplete != null)
                                            {
                                                _OnComplete(false, Model.PacketID);
                                            }
                                        }
                                        return 1;
                                    },
                                    (p, s) =>
                                    {
                                        Model.ViewModel.DownloadPercentage = p;
                                        return 1;
                                    }
                                );
                            }
                            else
                            {
                                bool status = false;
                                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                                {
                                    if (!String.IsNullOrWhiteSpace(msg))
                                    {
                                        if (Model.ViewModel.IsFriendChat)
                                        {
                                            status = ChatService.DownloadFriendChatMedia(Model.FriendTableID, Model.PacketID, msg, (int)Model.MessageType, Model.Timeout);
                                        }
                                        else
                                        {
                                            status = ChatService.DownloadGroupChatMedia(Model.GroupID, Model.SenderTableID, Model.PacketID, msg, (int)Model.MessageType);
                                        }
                                    }
                                    else
                                    {
                                        status = ChatService.AcceptChatMediaTransfer(Model.PacketID);
                                    }
                                }

                                if (!status)
                                {
                                    Model.IsFileOpened = false;
                                    Model.ViewModel.IsForceDownload = false;
                                    Model.IsDownloadRunning = false;
                                    Model.ViewModel.DownloadPercentage = 0;
                                    ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                    log.Error("Downloading Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + " IsMessageNull = " + string.IsNullOrEmpty(msg) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable + ", errorMsg = " + returnMsg);
                                }
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: MessageViewModel_OnDoMediaDownload() => + => " + ex.Message + "\n" + ex.StackTrace);
                ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();

                if (_OnComplete != null)
                {
                    _OnComplete(false, Model.PacketID);
                }
            }
        }

        public void DownloadPreview()
        {
            try
            {
                string filePath = String.Empty;
                if (Model.ViewModel.IsPreviewMessage)
                {
                    filePath = ChatHelpers.GetChatPreviewPath(Model.Message, Model.MessageType, Model.PacketID, Model.LinkImageUrl, Model.Latitude, Model.Longitude, ChatHelpers.GetShareContactUrl(Model.SharedContact));
                    if (File.Exists(filePath))
                    {
#if MESSAGE_STATUS_LOG
                        log.Debug("*** ALREADY PREVIEW DOWNLOADED => " + Model.Message);
#endif
                        Model.IsPreviewOpened = true;
                        Model.OnChatOpened();
                        CheckFileAutoDownload();
                        ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                        return;
                    }
                }
                else if (Model.MessageType == MessageType.AUDIO_FILE)
                {
                    Model.OnChatOpened();
                    CheckFileAutoDownload();
                    ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                    return;
                }
                else
                {
                    Model.OnChatOpened();
                    ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                    return;
                }
#if MESSAGE_STATUS_LOG
                log.Debug("*** PREVIEW DOWNLOADING ........ => " + Model.Message);
#endif
                switch ((MessageType)Model.MessageType)
                {
                    case MessageType.DOWNLOAD_STICKER_MESSAGE:
                        {
                            DownloadSingleStickerImage image = new DownloadSingleStickerImage(filePath, Model.Message);
                            image.OnComplete += (status) =>
                            {
#if MESSAGE_STATUS_LOG
                                log.Debug("*** PREVIEW DOWNLOADED COMPLETED => " + Model.Message);
#endif
                                Model.IsPreviewOpened = true;
                                Model.OnChatOpened();
                                ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                            };
                            image.Start();
                        }
                        break;
                    case MessageType.LINK_MESSAGE:
                        {

                            using (WebClient wc = new WebClient())
                            {
                                wc.DownloadFileCompleted += (s, args) =>
                                {
#if MESSAGE_STATUS_LOG
                                    log.Debug("*** PREVIEW DOWNLOADED COMPLETED => " + Model.Message);
#endif
                                    if (args.Cancelled || args.Error != null)
                                    {
                                        try
                                        {
                                            if (File.Exists(filePath)) { File.Delete(filePath); }
                                        }
                                        catch (Exception) { }
                                    }
                                    Model.IsPreviewOpened = true;
                                    Model.OnChatOpened();
                                    ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                };
                                wc.DownloadFileAsync(new System.Uri(Model.LinkImageUrl), filePath);
                            }
                        }
                        break;
                    case MessageType.LOCATION_MESSAGE:
                        {
                            string mapURL = "http://maps.googleapis.com/maps/api/staticmap?"
                                + "center=" + Model.Latitude + "," + Model.Longitude
                                + "&key=" + SocialMediaConstants.GOOGLE_MAP_API_KEY
                                + "&size=340x160"
                                + "&markers=size:mid%7Ccolor:red%7C" + Model.Latitude.ToString() + "," + Model.Longitude.ToString()
                                + "&zoom=15"
                                + "&maptype=roadmap"
                                + "&sensor=false";

                            using (WebClient wc = new WebClient())
                            {
                                wc.DownloadFileCompleted += (s, args) =>
                                {
#if MESSAGE_STATUS_LOG
                                    log.Debug("*** PREVIEW DOWNLOADED COMPLETED => " + Model.Message);
#endif
                                    if (args.Cancelled || args.Error != null)
                                    {
                                        try
                                        {
                                            if (File.Exists(filePath)) { File.Delete(filePath); }
                                        }
                                        catch (Exception) { }
                                    }
                                    Model.IsPreviewOpened = true;
                                    Model.OnChatOpened();
                                    ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                };
                                wc.DownloadFileAsync(new System.Uri(mapURL), filePath);
                            }
                        }
                        break;
                    case MessageType.RING_MEDIA_MESSAGE:
                        {
                            ChatService.DownloadChatFile(
                                Model.PacketType,
                                Model.MessageType,
                                Model.LinkImageUrl,
                                ServerAndPortSettings.GetVODThumbImageUpResourceURL,
                                filePath,
                                (s) =>
                                {
                                    if (!s)
                                    {
                                        try
                                        {
                                            if (File.Exists(filePath)) { File.Delete(filePath); }
                                        }
                                        catch (Exception) { }
                                    }
#if MESSAGE_STATUS_LOG
                                    log.Debug("*** PREVIEW DOWNLOADED COMPLETED => " + Model.Message);
#endif
                                    Model.IsPreviewOpened = true;
                                    Model.OnChatOpened();
                                    ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                    return 1;
                                },
                                null
                            );
                            break;
                        }
                    case MessageType.CONTACT_SHARE:
                        {
                            string msg = Model.SharedContact.ShortInfoModel.ProfileImage;
                            msg = HelperMethods.ConvertUrlToPurl(msg, ImageUtility.IMG_THUMB);
                            ChatService.DownloadChatFile(
                                Model.PacketType,
                                Model.MessageType,
                                msg,
                                ServerAndPortSettings.IMAGE_SERVER_RESOURCE,
                                filePath,
                                (s) =>
                                {
                                    if (!s)
                                    {
                                        try
                                        {
                                            if (File.Exists(filePath)) { File.Delete(filePath); }
                                        }
                                        catch (Exception) { }
                                    }
#if MESSAGE_STATUS_LOG
                                    log.Debug("*** PREVIEW DOWNLOADED COMPLETED => " + Model.Message);
#endif
                                    Model.IsPreviewOpened = true;
                                    Model.OnChatOpened();
                                    ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                    return 1;
                                },
                                null
                            );
                            break;
                        }
                    default:
                        {
                            string baseUrl = ServerAndPortSettings.IMAGE_SERVER_RESOURCE;
                            string msg = String.Empty;

                            switch ((MessageType)Model.MessageType)
                            {
                                case MessageType.IMAGE_FILE_FROM_CAMERA:
                                    msg = Model.Message;
                                    msg = ChatHelpers.ConvertToThumbUrl(msg);
                                    break;
                                case MessageType.IMAGE_FILE_FROM_GALLERY:
                                    msg = Model.Message;
                                    msg = ChatHelpers.ConvertToThumbUrl(msg);
                                    break;
                                case MessageType.VIDEO_FILE:
                                    msg = Model.Message;
                                    msg = !String.IsNullOrWhiteSpace(msg) ? Path.ChangeExtension(msg, ".jpg") : msg;
                                    break;
                            }

                            if (!String.IsNullOrWhiteSpace(msg))
                            {
                                ChatService.DownloadChatFile(
                                    Model.PacketType,
                                    Model.MessageType,
                                    msg,
                                    baseUrl,
                                    filePath,
                                    (s) =>
                                    {
                                        if (!s)
                                        {
                                            try
                                            {
                                                if (File.Exists(filePath)) { File.Delete(filePath); }
                                            }
                                            catch (Exception) { }
                                        }
#if MESSAGE_STATUS_LOG
                                        log.Debug("*** PREVIEW DOWNLOADED COMPLETED => " + Model.Message);
#endif
                                        Model.IsPreviewOpened = true;
                                        Model.OnChatOpened();
                                        CheckFileAutoDownload();
                                        ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                        return 1;
                                    },
                                    null
                                );
                            }
                            else
                            {
                                Model.IsPreviewOpened = true;
                                Model.OnChatOpened();
                                CheckFileAutoDownload();
                                ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: MessageViewModel_OnDoMediaDownload() => + => " + ex.Message + "\n" + ex.StackTrace);
                ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
            }
        }

    }

}

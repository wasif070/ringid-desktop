﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Input;
using View.BindingModels;
using Models.Utility;
using View.Utility;
using View.Dictonary;
using View.UI.Group;
using View.UI.Profile.FriendProfile;
using View.UI.Room;
using imsdkwrapper;
using System.Windows.Threading;
using Models.DAO;
using View.Utility.Recent;
using System.IO;
using Microsoft.Win32;
using Models.Constants;
using View.Utility.WPFMessageBox;
using View.UI.Chat;
using Models.Stores;
using System.Windows;
using View.Utility.RingPlayer;
using Newtonsoft.Json.Linq;
using System.Dynamic;
using View.ViewModel;
using View.Utility.Chat.Service;
using Models.Entity;
using Models.Utility.Chat;
using System.Collections.Concurrent;
using View.UI;
using System.Collections.ObjectModel;
using View.UI.PopUp.MediaSendViaChat;

namespace View.Utility.Chat
{
    public class ChatViewModel : INotifyPropertyChanged
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(ChatViewModel).Name);
        public event PropertyChangedEventHandler PropertyChanged;

        private ConcurrentDictionary<string, ObservableDictionary<string, long>> _UnreadChatList = new ConcurrentDictionary<string, ObservableDictionary<string, long>>();
        private ConcurrentDictionary<string, ObservableDictionary<string, long>> _UnreadCallList = new ConcurrentDictionary<string, ObservableDictionary<string, long>>();
        private ObservableDictionary<string, int> _UnreadChatContactList = new ObservableDictionary<string, int>();
        private ObservableDictionary<string, int> _UnreadCallContactList = new ObservableDictionary<string, int>();
        private ConcurrentDictionary<long, LastStatusModel> _LastStatusList = new ConcurrentDictionary<long, LastStatusModel>();

        private ICommand _ShowProfileClickCommand;
        private ICommand _OnLikeUnlikeCommand;
        private ICommand _OnLikeListCommand;
        private ICommand _CopySelectionCommand;
        private ICommand _CopyMessageCommand;
        private ICommand _OnEditCommand;
        private ICommand _OnResendCommand;
        private ICommand _OnInformationCommand;
        private ICommand _OnForwardCommand;
        private ICommand _OnReportCommand;
        private ICommand _OnSaveAsCommand;
        private ICommand _OnDeleteCommand;
        private ICommand _OnDeleteBothCommand;
        private ICommand _DeleteConversationCommand;
        private ICommand _MarkAsReadCommand;

        private ICommand _OnImageClickCommand;
        private ICommand _OnVideoClickCommand;
        private ICommand _OnAudioClickCommand;
        private ICommand _OnAudioPlayerActionClickCommand;
        private ICommand _OnRingMediaClickCommand;
        private ICommand _OnLocationClickCommand;
        private ICommand _OnFileClickCommand;
        private ICommand _OnFileActionClickCommand;
        private ICommand _OnMediaDownloadClickCommand;

        #region Property

        public static ChatViewModel Instance
        {
            get;
            set;
        }

        public ConcurrentDictionary<string, ObservableDictionary<string, long>> UnreadChatList
        {
            get
            {
                return _UnreadChatList;
            }
            set
            {
                _UnreadChatList = value;
            }
        }

        public ConcurrentDictionary<string, ObservableDictionary<string, long>> UnreadCallList
        {
            get
            {
                return _UnreadCallList;
            }
            set
            {
                _UnreadCallList = value;
            }
        }

        public ObservableDictionary<string, int> UnreadChatContactList
        {
            get
            {
                return _UnreadChatContactList;
            }
            set
            {
                _UnreadChatContactList = value;
            }
        }

        public ObservableDictionary<string, int> UnreadCallContactList
        {
            get
            {
                return _UnreadCallContactList;
            }
            set
            {
                _UnreadCallContactList = value;
            }
        }

        public ConcurrentDictionary<long, LastStatusModel> LastStatusList
        {
            get
            {
                return _LastStatusList;
            }
            set
            {
                _LastStatusList = value;
            }
        }

        public ICommand ShowProfileClickCommand
        {
            get
            {
                if (_ShowProfileClickCommand == null)
                {
                    _ShowProfileClickCommand = new RelayCommand((param) => ShowProfileClicked(param));
                }
                return _ShowProfileClickCommand;
            }
        }

        public ICommand OnLikeUnlikeCommand
        {
            get
            {
                if (_OnLikeUnlikeCommand == null)
                {
                    _OnLikeUnlikeCommand = new RelayCommand((param) => OnLikeUnlikeClicked(param));
                }
                return _OnLikeUnlikeCommand;
            }
        }

        public ICommand OnLikeListCommand
        {
            get
            {
                if (_OnLikeListCommand == null)
                {
                    _OnLikeListCommand = new RelayCommand((param) => OnLikeListClicked(param));
                }
                return _OnLikeListCommand;
            }
        }

        public ICommand OnCopySelectionCommand
        {
            get
            {
                if (_CopySelectionCommand == null)
                {
                    _CopySelectionCommand = new RelayCommand((param) => OnCopySelectionClicked(param));
                }
                return _CopySelectionCommand;
            }
        }

        public ICommand CopyMessageCommand
        {
            get
            {
                if (_CopyMessageCommand == null)
                {
                    _CopyMessageCommand = new RelayCommand((param) => OnCopyMessageClicked(param));
                }
                return _CopyMessageCommand;
            }
        }

        public ICommand OnEditCommand
        {
            get
            {
                if (_OnEditCommand == null)
                {
                    _OnEditCommand = new RelayCommand((param) => OnEditClicked(param));
                }
                return _OnEditCommand;
            }
        }

        public ICommand OnResendCommand
        {
            get
            {
                if (_OnResendCommand == null)
                {
                    _OnResendCommand = new RelayCommand((param) => OnResendClicked(param));
                }
                return _OnResendCommand;
            }
        }

        public ICommand OnInformationCommand
        {
            get
            {
                if (_OnInformationCommand == null)
                {
                    _OnInformationCommand = new RelayCommand((param) => OnInformationClicked(param));
                }
                return _OnInformationCommand;
            }
        }

        public ICommand OnForwardCommand
        {
            get
            {
                if (_OnForwardCommand == null)
                {
                    _OnForwardCommand = new RelayCommand((param) => OnForwardClicked(param));
                }
                return _OnForwardCommand;
            }
        }

        public ICommand OnReportCommand
        {
            get
            {
                if (_OnReportCommand == null)
                {
                    _OnReportCommand = new RelayCommand((param) => OnReportClicked(param));
                }
                return _OnReportCommand;
            }
        }

        public ICommand OnSaveAsCommand
        {
            get
            {
                if (_OnSaveAsCommand == null)
                {
                    _OnSaveAsCommand = new RelayCommand((param) => SaveAsClicked(param));
                }
                return _OnSaveAsCommand;
            }
        }

        public ICommand OnDeleteCommand
        {
            get
            {
                if (_OnDeleteCommand == null)
                {
                    _OnDeleteCommand = new RelayCommand((param) => OnDeleteClicked(param));
                }
                return _OnDeleteCommand;
            }
        }

        public ICommand OnDeleteBothCommand
        {
            get
            {
                if (_OnDeleteBothCommand == null)
                {
                    _OnDeleteBothCommand = new RelayCommand((param) => OnDeleteBothClicked(param));
                }
                return _OnDeleteBothCommand;
            }
        }

        public ICommand DeleteConversationCommand
        {
            get
            {
                if (_DeleteConversationCommand == null)
                {
                    _DeleteConversationCommand = new RelayCommand(param => OnDeleteConversationClicked(param));
                }
                return _DeleteConversationCommand;
            }
        }

        public ICommand MarkAsReadCommand
        {
            get
            {
                if (_MarkAsReadCommand == null)
                {
                    _MarkAsReadCommand = new RelayCommand(param => OnMarkAsReadClicked(param));
                }
                return _MarkAsReadCommand;
            }
        }

        public ICommand OnImageClickCommand
        {
            get
            {
                if (_OnImageClickCommand == null)
                {
                    _OnImageClickCommand = new RelayCommand(param => OnImageClicked(param));
                }
                return _OnImageClickCommand;
            }
        }

        public ICommand OnVideoClickCommand
        {
            get
            {
                if (_OnVideoClickCommand == null)
                {
                    _OnVideoClickCommand = new RelayCommand(param => OnVideoClicked(param), (param) => { return param != null && ((MessageModel)param).ViewModel.IsUploadRunning == false; });
                }
                return _OnVideoClickCommand;
            }
        }

        public ICommand OnAudioClickCommand
        {
            get
            {
                if (_OnAudioClickCommand == null)
                {
                    _OnAudioClickCommand = new RelayCommand(param => OnAudioClicked(param));
                }
                return _OnAudioClickCommand;
            }
        }

        public ICommand OnAudioPlayerActionClickCommand
        {
            get
            {
                if (_OnAudioPlayerActionClickCommand == null)
                {
                    _OnAudioPlayerActionClickCommand = new RelayCommand(param => OnAudioPlayerActionClicked(param), param => CanAudioPlayerActionClicked(param));
                }
                return _OnAudioPlayerActionClickCommand;
            }
        }

        public ICommand OnRingMediaClickCommand
        {
            get
            {
                if (_OnRingMediaClickCommand == null)
                {
                    _OnRingMediaClickCommand = new RelayCommand(param => OnRingMediaClicked(param));
                }
                return _OnRingMediaClickCommand;
            }
        }

        public ICommand OnLocationClickCommand
        {
            get
            {
                if (_OnLocationClickCommand == null)
                {
                    _OnLocationClickCommand = new RelayCommand(param => OnLocationClicked(param));
                }
                return _OnLocationClickCommand;
            }
        }

        public ICommand OnFileClickCommand
        {
            get
            {
                if (_OnFileClickCommand == null)
                {
                    _OnFileClickCommand = new RelayCommand(param => OnFileClicked(param), param => CanFileClicked(param));
                }
                return _OnFileClickCommand;
            }
        }

        public ICommand OnFileActionClickCommand
        {
            get
            {
                if (_OnFileActionClickCommand == null)
                {
                    _OnFileActionClickCommand = new RelayCommand(param => OnFileActionClicked(param), param => { return param != null && ((MessageModel)param).FileStatus == ChatConstants.FILE_DEFAULT; });
                }
                return _OnFileActionClickCommand;
            }
        }

        public ICommand OnMediaDownloadClickCommand
        {
            get
            {
                if (_OnMediaDownloadClickCommand == null)
                {
                    _OnMediaDownloadClickCommand = new RelayCommand(param => OnMediaDownloadClicked(param));
                }
                return _OnMediaDownloadClickCommand;
            }
        }

        #endregion Property

        #region Utility Method

        public ObservableDictionary<string, long> GetUnreadChatByID(object id)
        {
            ObservableDictionary<string, long> unreadList = UnreadChatList.TryGetValue(id.ToString());
            if (unreadList == null)
            {
                unreadList = new ObservableDictionary<string, long>();
                UnreadChatList[id.ToString()] = unreadList;
            }
            return unreadList;
        }

        public ObservableDictionary<string, long> GetUnreadCallByID(object id)
        {
            ObservableDictionary<string, long> unreadList = UnreadCallList.TryGetValue(id.ToString());
            if (unreadList == null)
            {
                unreadList = new ObservableDictionary<string, long>();
                UnreadCallList[id.ToString()] = unreadList;
            }
            return unreadList;
        }

        public LastStatusModel GetLastStatusModel(long id)
        {
            LastStatusModel lastStatusModel = LastStatusList.TryGetValue(id);
            if (lastStatusModel == null)
            {
                lastStatusModel = new LastStatusModel();
                LastStatusList[id] = lastStatusModel;
            }
            return lastStatusModel;
        }

        private void ShowProfileClicked(object param)
        {
            if (param == null) return;
            try
            {
                MessageModel model = (MessageModel)param;
                if (model.SenderTableID != DefaultSettings.LOGIN_TABLE_ID)
                {
                    RingIDViewModel.Instance.OnFriendCallChatButtonClicked(model.SenderTableID);
                }
                else
                {
                    RingIDViewModel.Instance.OnMyProfileClicked(null);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ShowProfileClicked() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnEditClicked(object param)
        {
            if (param == null) return;
            try
            {
                RecentModel model = (RecentModel)param;
                new Thread(() =>
                {
                    Thread.Sleep(200);
                    if (model.Message.MessageType == MessageType.LINK_MESSAGE)
                    {
                        model.IsRecentOpened = false;
                        model.Message.ViewModel.IsEditMode = true;
                        model.Message.ViewModel.LoadViewStructure(model.Message);
                        model.Message.ViewModel.LoadViewType(model.Message);
                        model.IsRecentOpened = true;
                    }
                    else
                    {
                        model.Message.ViewModel.IsEditMode = true;
                    }

                    if (model.FriendTableID > 0)
                    {
                        UCFriendChatCallPanel ucFriendChatCallPanel = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(model.FriendTableID);
                        if (ucFriendChatCallPanel != null)
                        {
                            ucFriendChatCallPanel.OnChatEdit(model);
                        }
                    }
                    else if (model.GroupID > 0)
                    {
                        UCGroupChatCallPanel ucGroupChatCallPanel = UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY.TryGetValue(model.GroupID);
                        if (ucGroupChatCallPanel != null)
                        {
                            ucGroupChatCallPanel.OnChatEdit(model);
                        }
                    }
                    else if (!String.IsNullOrWhiteSpace(model.RoomID))
                    {
                        UCRoomChatCallPanel ucRoomChatCallPanel = UIDictionaries.Instance.ROOM_CHAT_CALL_DICTIONARY.TryGetValue(model.RoomID);
                        if (ucRoomChatCallPanel != null)
                        {
                            ucRoomChatCallPanel.OnChatEdit(model);
                        }
                    }
                }).Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: OnEditClicked() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnResendClicked(object param)
        {
            if (param == null) return;

            try
            {
                RecentModel model = (RecentModel)param;
                ChatFileDTO chatFileDTO = null;

                if (model.Message.MessageType == MessageType.FILE_STREAM)
                {
                    FileInfo fi = new FileInfo(model.Message.Message);
                    if (!(fi.Exists && fi.Length == model.Message.FileSize))
                    {
                        UIHelperMethods.ShowFailed("Failed to resend message!", "Resend message");
                        //CustomMessageBox.ShowError("Failed to resend message!");
                        return;
                    }
                }
                else if (model.Message.MessageType == MessageType.IMAGE_FILE_FROM_GALLERY
                    || model.Message.MessageType == MessageType.IMAGE_FILE_FROM_CAMERA
                    || model.Message.MessageType == MessageType.AUDIO_FILE
                    || model.Message.MessageType == MessageType.VIDEO_FILE)
                {
                    if (Path.GetFileNameWithoutExtension(model.Message.Message).Equals(model.Message.PacketID))
                    {
                        string filePath = ChatHelpers.GetChatFilePath(model.Message.Message, model.Message.MessageType, model.Message.PacketID, model.Message.LinkImageUrl, model.Message.SenderTableID);
                        if (File.Exists(filePath))
                        {
                            MessageDTO messageDTO = new MessageDTO();
                            messageDTO.FriendTableID = model.Message.FriendTableID;
                            messageDTO.GroupID = model.Message.GroupID;
                            messageDTO.RoomID = model.Message.RoomID;
                            messageDTO.MessageType = (int)model.Message.MessageType;
                            messageDTO.OriginalMessage = model.Message.OriginalMessage;
                            messageDTO.Timeout = model.Message.Timeout;
                            messageDTO.IsSecretVisible = model.Message.IsSecretVisible;
                            messageDTO.PacketID = model.Message.PacketID;
                            messageDTO.MessageDate = model.Message.MessageDate;
                            messageDTO.Status = ChatConstants.STATUS_SENDING;
                            messageDTO.SenderTableID = model.Message.SenderTableID;
                            messageDTO.PacketType = model.Message.PacketType;
                            messageDTO.FullName = model.Message.FullName;
                            messageDTO.ProfileImage = model.Message.ProfileImage;
                            ChatJSONParser.ParseMessage(messageDTO);

                            chatFileDTO = new ChatFileDTO();
                            chatFileDTO.FriendTableID = messageDTO.FriendTableID;
                            chatFileDTO.GroupID = messageDTO.GroupID;
                            chatFileDTO.RoomID = messageDTO.RoomID;
                            chatFileDTO.Message = messageDTO.Message;
                            chatFileDTO.MessageType = messageDTO.MessageType;
                            chatFileDTO.PacketID = messageDTO.PacketID;
                            chatFileDTO.MessageDTO = messageDTO;
                            chatFileDTO.File = filePath;

                            if (chatFileDTO.MessageType == (int)MessageType.IMAGE_FILE_FROM_GALLERY || chatFileDTO.MessageType == (int)MessageType.IMAGE_FILE_FROM_CAMERA)
                            {
                                chatFileDTO.UploadUrl = ServerAndPortSettings.ChatImageUploadingUrl;
                            }
                            else if (chatFileDTO.MessageType == (int)MessageType.AUDIO_FILE)
                            {
                                chatFileDTO.UploadUrl = ServerAndPortSettings.ChatMP3UploadingUrl;
                            }
                            else if (chatFileDTO.MessageType == (int)MessageType.VIDEO_FILE)
                            {
                                chatFileDTO.UploadUrl = ServerAndPortSettings.ChatMP4UploadingUrl;
                            }

                            chatFileDTO.RecentModel = model;
                        }
                        else
                        {
                            UIHelperMethods.ShowFailed("Failed to resend message!", "Resend message");
                            //CustomMessageBox.ShowError("Failed to resend message!");
                            return;
                        }
                    }
                }

                if (model.Message.ViewModel.IsFriendChat)
                {
                    UCFriendChatCallPanel ucFriendChatCall = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(model.Message.FriendTableID);
                    if (ucFriendChatCall != null)
                    {
                        int anonymousSettingName = -1;
                        if (!HelperMethods.HasAnonymousChatPermission(ucFriendChatCall.FriendBasicInfoModel))
                        {
                            if (HelperMethods.ShowAnonymousWarning() == MessageBoxResult.Yes)
                            {
                                return;
                            }
                            else
                            {
                                anonymousSettingName = StatusConstants.ANONYMOUS_CHAT;
                            }
                        }

                        HelperMethods.ChangeAnonymousSettingsWrapper((result) =>
                        {
                            if (result == false) return 0;

                            int accessType = -1;
                            if (!HelperMethods.HasFriendChatPermission(ucFriendChatCall.FriendBasicInfoModel, ucFriendChatCall.BlockedNonFriendModel, out accessType))
                            {
                                if (HelperMethods.ShowBlockWarning(ucFriendChatCall.FriendBasicInfoModel, accessType))
                                {
                                    return 0;
                                }
                            }

                            HelperMethods.ChangeBlockUnblockSettingsWrapper(accessType, ucFriendChatCall.FriendBasicInfoModel, ucFriendChatCall.BlockedNonFriendModel, (status) =>
                            {
                                if (status == false) return 0;

                                model.Message.Status = ChatConstants.STATUS_SENDING;
                                RecentChatCallActivityDAO.UpdateChatMessageStatus(model.Message.FriendTableID, 0, model.Message.PacketID, model.Message.Status, 0, 0, 0);

                                if (chatFileDTO != null)
                                {
                                    model.Message.ViewModel.IsUploadRunning = true;
                                    ChatService.UploadChatFile(chatFileDTO, new UploadProgressHandler());
                                }
                                else if (model.Message.PacketType == ChatConstants.PACKET_TYPE_FRIEND_MESSAGE_EDIT)
                                {
                                    ChatService.EditFriendChat(model.Message.PacketID, model.Message.FriendTableID, (int)model.Message.MessageType, model.Message.IsSecretChat ? 1 : 0, model.Message.OriginalMessage, model.Message.MessageDate, model.Message.IsSecretVisible, null, null);
                                }
                                else
                                {
                                    ChatService.SendFriendChat(model.Message.PacketID, model.Message.FriendTableID, (int)model.Message.MessageType, model.Message.IsSecretChat ? 1 : 0, model.Message.OriginalMessage, model.Message.MessageDate, model.Message.IsSecretVisible, null, null);
                                }
                                return 1;
                            });

                            return 1;
                        }, anonymousSettingName);
                    }
                }
                else if (model.Message.ViewModel.IsGroupChat)
                {
                    model.Message.Status = ChatConstants.STATUS_SENDING;
                    RecentChatCallActivityDAO.UpdateChatMessageStatus(0, model.Message.GroupID, model.Message.PacketID, model.Message.Status, 0, 0, 0);
                    if (chatFileDTO != null)
                    {
                        model.Message.ViewModel.IsUploadRunning = true;
                        ChatService.UploadChatFile(chatFileDTO, new UploadProgressHandler());
                    }
                    else if (model.Message.PacketType == ChatConstants.PACKET_TYPE_GROUP_MESSAGE_EDIT)
                    {
                        ChatService.EditGroupChat(model.Message.PacketID, model.Message.GroupID, (int)model.Message.MessageType, model.Message.OriginalMessage, model.Message.MessageDate);
                    }
                    else
                    {
                        ChatService.SendGroupChat(model.Message.PacketID, model.Message.GroupID, (int)model.Message.MessageType, model.Message.OriginalMessage, model.Message.MessageDate);
                    }
                }
                else if (model.Message.ViewModel.IsRoomChat)
                {
                    model.Message.Status = ChatConstants.STATUS_SENDING;

                    if (chatFileDTO != null)
                    {
                        model.Message.ViewModel.IsUploadRunning = true;
                        ChatService.UploadChatFile(chatFileDTO, new UploadProgressHandler());
                    }
                    else if (model.Message.PacketType == ChatConstants.PACKET_TYPE_ROOM_MESSAGE_EDIT)
                    {
                        ChatService.EditPublicRoomChat(model.Message.PacketID, model.Message.RoomID, (int)model.Message.MessageType, model.Message.OriginalMessage, model.Message.MessageDate, model.Message.FullName, model.Message.ProfileImage);
                    }
                    else
                    {
                        ChatService.SendPublicRoomChat(model.Message.PacketID, model.Message.RoomID, (int)model.Message.MessageType, model.Message.OriginalMessage, model.Message.MessageDate, model.Message.FullName, model.Message.ProfileImage);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnResendClicked() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnDeleteClicked(object param)
        {
            if (param == null) return;
            try
            {
                RecentModel model = (RecentModel)param;
                if (model.Message != null)
                {
                    Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        model.Message.MessageType = (int)MessageType.DELETE_MESSAGE;
                        ChatHelpers.DeleteChatMessage(model.FriendTableID, model.GroupID, model.RoomID, model.UniqueKey, model.Message.SenderTableID, false);
                    }));
                }
                else if (model.CallLog != null)
                {
                    List<string> list = new List<string>();
                    list.Add(model.UniqueKey);
                    RecentChatCallActivityDAO.DeleteCallHistory(model.FriendTableID, list);
                    RecentLoadUtility.Remove(model.FriendTableID, model.UniqueKey);
                }
                else if (model.Activity != null)
                {
                    List<string> list = new List<string>();
                    list.Add(model.UniqueKey);
                    RecentChatCallActivityDAO.DeleteActivityHistory(model.GroupID, list);
                    RecentLoadUtility.Remove(model.GroupID, model.UniqueKey);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnDeleteClick() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnDeleteBothClicked(object param)
        {
            if (param == null) return;
            try
            {
                MessageModel model = (MessageModel)param;
                Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    model.MessageType = (int)MessageType.DELETE_MESSAGE;
                    ChatHelpers.DeleteChatMessage(model.FriendTableID, model.GroupID, model.RoomID, model.PacketID, model.SenderTableID, true);
                }));
            }
            catch (Exception ex)
            {
                log.Error("Error: OnDeleteBothClick() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnDeleteConversationClicked(object param)
        {
            if (param == null) return;
            try
            {
                RecentModel model = (RecentModel)param;
                if (model.FriendTableID > 0)
                {
                    UCFriendChatCallPanel ucFriendChatCallPanel = null;
                    if (UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(model.FriendTableID, out ucFriendChatCallPanel) && !ucFriendChatCallPanel.IsDeletePanelVisible)
                    {
                        ucFriendChatCallPanel.OpenCloseChatDeletePanel();
                    }
                }
                if (model.GroupID > 0)
                {
                    UCGroupChatCallPanel ucGroupChatCallPanel = null;
                    if (UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY.TryGetValue(model.GroupID, out ucGroupChatCallPanel) && !ucGroupChatCallPanel.IsDeletePanelVisible)
                    {
                        ucGroupChatCallPanel.OpenCloseChatDeletePanel();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnDeleteConversationClicked() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnMarkAsReadClicked(object param)
        {
            if (param == null) return;
            try
            {
                RecentModel model = (RecentModel)param;
                if (model.Message != null)
                {
                    List<string> packetIds = model.UnreadList.Keys.ToList();
                    ChatHelpers.RemoveUnreadChat(model.ContactID, packetIds);
                    RecentChatCallActivityDAO.MakeChatAsRead(model.FriendTableID, model.GroupID, packetIds);

                    if (model.FriendTableID > 0)
                    {
                        UCFriendChatCallPanel ucFriendChatCallPanel = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(model.FriendTableID);
                        if (ucFriendChatCallPanel != null)
                        {
                            List<RecentModel> chatModelList = ucFriendChatCallPanel.ChatScrollViewer.RecentModelList.Where(P => packetIds.Any(Q => P.Message != null && P.UniqueKey.Equals(Q))).ToList();
                            chatModelList.ForEach(i => i.Message.IsUnread = false);
                        }
                    }
                    else if (model.GroupID > 0)
                    {
                        UCGroupChatCallPanel ucGroupChatCallPanel = UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY.TryGetValue(model.GroupID);
                        if (ucGroupChatCallPanel != null)
                        {
                            List<RecentModel> chatModelList = ucGroupChatCallPanel.ChatScrollViewer.RecentModelList.Where(P => packetIds.Any(Q => P.Message != null && P.UniqueKey.Equals(Q))).ToList();
                            chatModelList.ForEach(i => i.Message.IsUnread = false);
                        }
                    }
                }
                else if (model.CallLog != null)
                {
                    List<string> callIds = model.CallLog.CallIDs.Keys.ToList().Where(P => model.UnreadList.Keys.ToList().Any(Q => Q.Equals(P))).ToList();
                    ChatHelpers.RemoveUnreadCall(model.ContactID, callIds);
                    RecentChatCallActivityDAO.MakeCallAsRead(model.FriendTableID, callIds);

                    UCFriendChatCallPanel ucFriendChatCallPanel = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(model.FriendTableID);
                    if (ucFriendChatCallPanel != null)
                    {
                        List<RecentModel> chatModelList = ucFriendChatCallPanel.ChatScrollViewer.RecentModelList.Where(P => callIds.Any(Q => P.CallLog != null && P.UniqueKey.Equals(Q))).ToList();
                        chatModelList.ForEach(i => i.CallLog.IsUnread = false);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnMarkAsReadClicked() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnCopySelectionClicked(object param)
        {
            if (param == null) return;
            try
            {
                Clipboard.Clear();
                MessageModel model = (MessageModel)param;
                if (model.ViewModel.SelectedCharacters != null)
                {
                    Clipboard.SetText(model.ViewModel.SelectedCharacters);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnCopySelectionClick() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnCopyMessageClicked(object param)
        {
            if (param == null) return;
            try
            {
                MessageModel model = (MessageModel)param;
                Clipboard.Clear();
                Clipboard.SetText(model.Message ?? String.Empty);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnCopyMessageClick() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnInformationClicked(object param)
        {
            if (param == null) return;
            try
            {
                RecentModel model = (RecentModel)param;
                if (model.FriendTableID > 0)
                {
                    UCFriendChatCallPanel ucFriendChatCallPanel = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(model.FriendTableID);
                    if (ucFriendChatCallPanel != null)
                    {
                        ucFriendChatCallPanel.ShowChatInformationView(model);
                    }
                }
                else if (model.GroupID > 0)
                {
                    UCGroupChatCallPanel ucGroupChatCallPanel = UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY.TryGetValue(model.GroupID);
                    if (ucGroupChatCallPanel != null)
                    {
                        ucGroupChatCallPanel.ShowChatInformationView(model);
                    }
                }
                else if (!String.IsNullOrWhiteSpace(model.RoomID))
                {
                    UCRoomChatCallPanel ucRoomChatCallPanel = UIDictionaries.Instance.ROOM_CHAT_CALL_DICTIONARY.TryGetValue(model.RoomID);
                    if (ucRoomChatCallPanel != null)
                    {
                        ucRoomChatCallPanel.ShowChatInformationView(model);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnInformationClick() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnForwardClicked(object param)
        {
            if (param == null) return;
            try
            {
                MessageModel model = (MessageModel)param;
                UCMediaShareToFriendPopup ucMediaShareToFriendPopup = new UCMediaShareToFriendPopup(UCGuiRingID.Instance.MotherPanel);
                ucMediaShareToFriendPopup.Show();
                ucMediaShareToFriendPopup.SetMessage(model);
                //MainSwitcher.PopupController.DownloadOrAddToAlbumPopUpWrapper.Show(model);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnForwardClick() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnReportClicked(object param)
        {
            if (param == null) return;
            try
            {
                MessageModel model = (MessageModel)param;
                if (!String.IsNullOrWhiteSpace(model.RoomID))
                {
                    UCRoomChatCallPanel ucRoomChatCallPanel = UIDictionaries.Instance.ROOM_CHAT_CALL_DICTIONARY.TryGetValue(model.RoomID);
                    if (ucRoomChatCallPanel != null)
                    {
                        ucRoomChatCallPanel.ShowChatReportView(model);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnReportClick() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnLikeUnlikeClicked(object param)
        {
            if (param == null) return;
            try
            {
                MessageModel model = (MessageModel)param;
                ChatHelpers.SendLikeUnlike(model);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnLikeUnlikeClick() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnLikeListClicked(object param)
        {
            if (param == null) return;
            try
            {
                MessageModel model = (MessageModel)param;
                if (!String.IsNullOrWhiteSpace(model.RoomID))
                {
                    UCRoomChatCallPanel ucRoomChatCallPanel = UIDictionaries.Instance.ROOM_CHAT_CALL_DICTIONARY.TryGetValue(model.RoomID);
                    if (ucRoomChatCallPanel != null)
                    {
                        ucRoomChatCallPanel.ShowChatLikeMemberList(model);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnLikeListClick() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void SaveAsClicked(object param)
        {
            if (param == null) return;
            try
            {
                MessageModel model = (MessageModel)param;
                string filePath = ChatHelpers.GetChatFilePath(model.Message, model.MessageType, model.PacketID, model.LinkImageUrl, model.SenderTableID);
                FileInfo fi = new FileInfo(filePath);
                if (fi.Exists)
                {
                    SaveFileDialog dialog = new SaveFileDialog();
                    dialog.Filter = model.MessageType == MessageType.FILE_STREAM
                        ? "All files (*.*)|*.*"
                        : "All supported graphics|*.jpg;*.jpeg;*.png|" +
                          "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                          "Portable Network Graphic (*.png)|*.png";
                    dialog.FileName = Path.GetFileName(model.Message);
                    if (dialog.ShowDialog() == true)
                    {
                        fi = new FileInfo(filePath);
                        if (fi.Exists)
                        {
                            File.Copy(filePath, dialog.FileName, true);
                        }
                        else
                        {
                            UIHelperMethods.ShowWarning(NotificationMessages.FILE_NOT_FOUND, "File not found");
                            //  CustomMessageBox.ShowError(NotificationMessages.FILE_NOT_FOUND);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: SaveAs() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnImageClicked(object param)
        {
            if (param == null) return;
            try
            {
                MessageModel model = (MessageModel)param;
                if (model.FriendTableID > 0)
                {
                    UCFriendChatCallPanel ucFriendChatCallPanel = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(model.FriendTableID);
                    if (ucFriendChatCallPanel != null)
                    {
                        if (ucFriendChatCallPanel.Parent is WNChatView)
                        {
                            ucFriendChatCallPanel.ShowMediaPreview(model);
                        }
                        else
                        {
                            UCChatMediaPreviewWrapper.Instance.Show(model);
                        }
                    }
                }
                else if (model.GroupID > 0)
                {
                    UCGroupChatCallPanel ucGroupChatCallPanel = UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY.TryGetValue(model.GroupID);
                    if (ucGroupChatCallPanel != null)
                    {
                        if (ucGroupChatCallPanel.Parent is WNChatView)
                        {
                            ucGroupChatCallPanel.ShowMediaPreview(model);
                        }
                        else
                        {
                            UCChatMediaPreviewWrapper.Instance.Show(model);
                        }
                    }
                }
                else if (!String.IsNullOrWhiteSpace(model.RoomID))
                {
                    UCRoomChatCallPanel ucRoomChatCallPanel = UIDictionaries.Instance.ROOM_CHAT_CALL_DICTIONARY.TryGetValue(model.RoomID);
                    if (ucRoomChatCallPanel != null)
                    {
                        if (ucRoomChatCallPanel.Parent is WNChatView)
                        {
                            ucRoomChatCallPanel.ShowMediaPreview(model);
                        }
                        else
                        {
                            UCChatMediaPreviewWrapper.Instance.Show(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnImageClicked() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnVideoClicked(object param)
        {
            if (param == null) return;
            try
            {
                MessageModel model = (MessageModel)param;
                if (model.FriendTableID > 0)
                {
                    UCFriendChatCallPanel ucFriendChatCallPanel = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(model.FriendTableID);
                    if (ucFriendChatCallPanel != null)
                    {
                        if (ucFriendChatCallPanel.Parent is WNChatView)
                        {
                            ucFriendChatCallPanel.ShowMediaPreview(model);
                        }
                        else
                        {
                            UCChatMediaPreviewWrapper.Instance.Show(model);
                        }
                    }
                }
                else if (model.GroupID > 0)
                {
                    UCGroupChatCallPanel ucGroupChatCallPanel = UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY.TryGetValue(model.GroupID);
                    if (ucGroupChatCallPanel != null)
                    {
                        if (ucGroupChatCallPanel.Parent is WNChatView)
                        {
                            ucGroupChatCallPanel.ShowMediaPreview(model);
                        }
                        else
                        {
                            UCChatMediaPreviewWrapper.Instance.Show(model);
                        }
                    }
                }

                else if (!String.IsNullOrWhiteSpace(model.RoomID))
                {
                    UCRoomChatCallPanel ucRoomChatCallPanel = UIDictionaries.Instance.ROOM_CHAT_CALL_DICTIONARY.TryGetValue(model.RoomID);
                    if (ucRoomChatCallPanel != null)
                    {
                        if (ucRoomChatCallPanel.Parent is WNChatView)
                        {
                            ucRoomChatCallPanel.ShowMediaPreview(model);
                        }
                        else
                        {
                            UCChatMediaPreviewWrapper.Instance.Show(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnVideoClicked() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnAudioClicked(object param)
        {
            if (param == null) return;
            try
            {
                MessageModel model = (MessageModel)param;
                model.ViewModel.IsAudioOpened = true;

                dynamic obj = new ExpandoObject();
                obj.Model = model;
                obj.State = StatusConstants.MEDIA_PLAY_STATE;
                OnAudioPlayerActionClicked(obj);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnAudioClicked() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnAudioPlayerActionClicked(object param)
        {
            if (param == null) return;
            try
            {
                MessageModel model = ((dynamic)param).Model;
                int state = ((dynamic)param).State;

                string filePath = ChatHelpers.GetChatFilePath(model.Message, model.MessageType, model.PacketID, model.LinkImageUrl, model.SenderTableID);
                if (File.Exists(filePath))
                {
                    if (ChatAudioPlayer._Instance != null)
                    {
                        ChatAudioPlayer.Instance.Stop();
                    }
                    if (state == StatusConstants.MEDIA_PLAY_STATE)
                    {
                        ChatAudioPlayer.Instance.Play(model, filePath);
                    }
                }
                else
                {
                    OnMediaDownloadClicked(model);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnAudioClicked() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private bool CanAudioPlayerActionClicked(object param)
        {
            if (param == null) return false;
            MessageModel model = ((dynamic)param).Model;
            int state = ((dynamic)param).State;
            return !(state == StatusConstants.MEDIA_PAUSE_STATE && model.FromFriend && model.IsSecretChat); ;
        }

        private void OnRingMediaClicked(object param)
        {
            if (param == null) return;
            try
            {
                MessageModel model = (MessageModel)param;
                JObject feedObj = JObject.Parse(model.OriginalMessage);
                if (feedObj != null) MediaUtility.RunMediaFromChat(feedObj);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnRingMediaClicked() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnLocationClicked(object param)
        {
            if (param == null) return;
            try
            {
                MessageModel model = (MessageModel)param;
                WNLocationViewer.Instance.ShowWindow(model.Latitude, model.Longitude);
            }
            catch (Exception ex)
            {
                log.Error("Error: OnLocationClicked() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnFileClicked(object param)
        {
            if (param == null) return;
            try
            {
                MessageModel model = (MessageModel)param;
                string filePath = ChatHelpers.GetChatFilePath(model.Message, model.MessageType, model.PacketID, model.LinkImageUrl, model.SenderTableID);
                FileInfo fi = new FileInfo(filePath);
                if (fi.Exists && fi.Length == model.FileSize)
                {
                    try
                    {
                        System.Diagnostics.Process.Start(filePath);
                    }
                    catch
                    {

                    }
                }
                else
                {
                    model.FileStatus = ChatConstants.FILE_MOVED;
                    if (model.FromFriend)
                    {
                        model.ViewModel.IsForceDownload = false;
                        model.IsDownloadRunning = false;
                        model.ViewModel.DownloadPercentage = 0;
                    }
                    else
                    {
                        model.ViewModel.IsUploadRunning = false;
                        model.ViewModel.UploadPercentage = 0;
                    }
                    RecentChatCallActivityDAO.UpdateChatMessageFileStatus(model.FriendTableID, model.GroupID, model.PacketID, model.FileStatus);
                    ChatHelpers.SendFileStreamCancel(model.FriendTableID, model.GroupID, model.FileID, null);
                    log.Debug("FILE TRANSFER => OnFileClicked() => File Already Moved. ContactID = " + (model.FriendTableID > 0 ? model.FriendTableID : model.GroupID) + ", FileID = " + model.FileID + "");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnFileClicked() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private bool CanFileClicked(object param)
        {
            if (param == null) return false; MessageModel model = (MessageModel)param;
            return (model.FromFriend == false && model.FileStatus != ChatConstants.FILE_MOVED) || (model.FromFriend && model.FileStatus == ChatConstants.FILE_TRANSFER_COMPLETED);
        }

        private void OnFileActionClicked(object param)
        {
            if (param == null) return;
            try
            {
                MessageModel model = (MessageModel)param;
                string filePath = ChatHelpers.GetChatFilePath(model.Message, model.MessageType, model.PacketID, model.LinkImageUrl, model.SenderTableID);
                if (model.FromFriend && model.IsDownloadRunning == false)
                {
                    FileInfo fi = new FileInfo(filePath);
                    if (fi.Exists && fi.Length == model.FileSize)
                    {
                        model.FileStatus = ChatConstants.FILE_TRANSFER_COMPLETED;
                        RecentChatCallActivityDAO.UpdateChatMessageFileStatus(model.FriendTableID, model.GroupID, model.PacketID, model.FileStatus);
                    }
                    else
                    {
                        model.ViewModel.IsForceDownload = true;
                        model.IsDownloadRunning = true;
                        model.ViewModel.DownloadPercentage = 0;

                        ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.AddItem(new ChatDownloadProcessor(model, false, (status, packetId) =>
                        {
                            if (!status)
                            {
                                Application.Current.Dispatcher.BeginInvoke(() =>
                                {
                                    UIHelperMethods.ShowWarning(String.Format(NotificationMessages.CHAT_CONTENT_DOWNLOAD_FAILED, "file"), "Chat file");
                                    //CustomMessageBox.ShowError(String.Format(NotificationMessages.CHAT_CONTENT_DOWNLOAD_FAILED, "file"));
                                });
                            }
                            return 0;
                        }));
                    }
                }
                else
                {
                    model.FileStatus = ChatConstants.FILE_TRANSFER_CANCELLED;
                    if (model.FromFriend)
                    {
                        model.ViewModel.IsForceDownload = false;
                        model.IsDownloadRunning = false;
                        model.ViewModel.UploadPercentage = 0;
                    }
                    else
                    {
                        model.ViewModel.IsUploadRunning = false;
                        model.ViewModel.UploadPercentage = 0;
                    }
                    RecentChatCallActivityDAO.UpdateChatMessageFileStatus(model.FriendTableID, model.GroupID, model.PacketID, model.FileStatus);
                    ChatHelpers.SendFileStreamCancel(model.FriendTableID, model.GroupID, model.FileID, null);
                    log.Debug("FILE TRANSFER => OnFileClicked() => Cancel File Download. ContactID = " + (model.FriendTableID > 0 ? model.FriendTableID : model.GroupID) + ", FileID = " + model.FileID + "");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnFileClicked() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void OnMediaDownloadClicked(object param)
        {
            if (param == null) return;
            try
            {
                MessageModel model = (MessageModel)param;
                model.IsFileOpened = false;
                model.ViewModel.IsForceDownload = true;
                model.IsDownloadRunning = true;
                model.ViewModel.DownloadPercentage = 0;

                ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.AddItem(new ChatDownloadProcessor(model, false, (status, packetId) =>
                {
                    if (!status)
                    {
                        Application.Current.Dispatcher.BeginInvoke(() =>
                        {
                            UIHelperMethods.ShowWarning("Failed to download! Try again.", "Chat file");
                            //  CustomMessageBox.ShowError("Failed to download! Try again.");
                        });
                    }
                    return 0;
                }));
            }
            catch (Exception ex)
            {
                log.Error("Error: OnImageDownloadClick() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion Utility Method

    }
}

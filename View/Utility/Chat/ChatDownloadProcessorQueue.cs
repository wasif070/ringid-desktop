﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace View.Utility.Chat
{
    public class ChatDownloadProcessorQueue : ConcurrentQueue<ChatDownloadProcessor>
    {
        private static object _Lock = new object();
        private static int _STATUS_OPEN = 0;
        private static int _STATUS_PENDING = 1;
        private int _CURRENT_STATUS = _STATUS_OPEN;

        public void AddItem(ChatDownloadProcessor obj)
        {
            if (obj.Model.ViewModel.IsForceDownload)
            {
                obj.Start();
            }
            else
            {
                this.Enqueue(obj);
                GetItem();
            }
        }

        private void GetItem()
        {
            lock (_Lock)
            {
                if (!IsEmpty && _CURRENT_STATUS == _STATUS_OPEN)
                {
                    _CURRENT_STATUS = _STATUS_PENDING;
                    ChatDownloadProcessor obj = null;
                    if (this.TryDequeue(out obj))
                    {
                        obj.Start();
                    }
                    else
                    {
                        _CURRENT_STATUS = _STATUS_OPEN;
                    }
                }
            }
        }

        public void OnComplete()
        {
            _CURRENT_STATUS = _STATUS_OPEN;
            GetItem();
        }
    }
}

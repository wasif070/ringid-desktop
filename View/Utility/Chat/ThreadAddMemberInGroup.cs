﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using log4net;
using Models.Constants;
using Newtonsoft.Json.Linq;
using View.Utility.Auth;
using Auth.utility;

namespace View.Utility.Chat
{

    public class ThreadAddMemberInGroup
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ThreadAddMemberInGroup).Name);
        private long _GroupID;
        private List<long> _Members;
        private string _ChatServerIP;
        private int _ChatRegisterPort;

        public ThreadAddMemberInGroup(long groupId, List<long> members, string chatServerIP, int chatRegisterPort)
        {
            this._GroupID = groupId;
            this._Members = members;
            this._ChatServerIP = chatServerIP;
            this._ChatRegisterPort = chatRegisterPort;
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Run));
            t.Name = this.GetType().Name;
            t.Start();
        }

        public void Run()
        {
            try
            {
                JArray jArray = new JArray();
                if (_Members != null && _Members.Count > 0)
                {
                    foreach (long utId in _Members)
                    {
                        jArray.Add(utId.ToString());
                    }
                }

                JObject pakToSend = new JObject();
                pakToSend[JsonKeys.TagId] = _GroupID;
                pakToSend[JsonKeys.FutIds] = jArray;
                pakToSend[JsonKeys.ChatServerIp] = _ChatServerIP;
                pakToSend[JsonKeys.ChatRegistrationPort] = _ChatRegisterPort;
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_GROUP_MEMBER;

                JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_CHAT, packetId);
                if (_JobjFromResponse != null)
                {

                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                log.Error("AddMemberInGroup ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
        }
    }
}

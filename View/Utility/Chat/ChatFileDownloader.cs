﻿using imsdkwrapper;
using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using View.Utility.Call;

namespace View.Utility.Chat
{
    public class ChatFileDownloader
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChatFileDownloader).Name);

        private int _PacketType;
        private MessageType _MessageType;
        private string _Message;
        private string _DownloadBaseUrl;
        private string _DirectoryPath;
        private Func<bool, int> _OnComplete = null;
        private Func<int, object, int> _Progress = null;
        private WebClient _Downloader = null;

        public ChatFileDownloader(
            int packetType,
            MessageType messageType,
            string message,
            string downloadBaseUrl,
            string directoryPath,
            Func<bool, int> onComplete = null,
            Func<int, object, int> onProgress = null)
        {
            this._PacketType = packetType;
            this._MessageType = messageType;
            this._Message = message;
            this._DownloadBaseUrl = downloadBaseUrl;
            this._DirectoryPath = directoryPath;
            this._OnComplete = onComplete;
            this._Progress = onProgress;

            _Downloader = new WebClient();
            _Downloader.Headers.Set("x-app-version", AppConfig.VERSION_PC);
            _Downloader.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
            _Downloader.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);

        }

        public void Start()
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID) && !String.IsNullOrWhiteSpace(_Message))
                {
                    string downloadUrl = _DownloadBaseUrl + Path.AltDirectorySeparatorChar + _Message;

                    if (!System.IO.File.Exists(_DirectoryPath))
                    {
                        if (_MessageType == MessageType.AUDIO_FILE)
                        {
                            _DirectoryPath = Path.ChangeExtension(_DirectoryPath, ".g729");
                        }

                        _Downloader.DownloadFileAsync(new Uri(downloadUrl), _DirectoryPath);
                    }
                }
                else
                {
                    log.Error("Downloading Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + " IsMessageNull = " + string.IsNullOrEmpty(_Message) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);

                    if (_OnComplete != null)
                    {
                        _OnComplete(false);
                    }
                    _Downloader.Dispose();
                }
            }
            catch (Exception ex)
            {
                log.Error("Downloading Falied ==> " + ex.Message + "\n" + ex.StackTrace);
                if (_OnComplete != null)
                {
                    _OnComplete(false);
                }
                _Downloader.Dispose();
            }
        }

        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            if (_Progress != null)
            {
                _Progress(e.ProgressPercentage, null);
            }
        }

        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            try
            {
                _Downloader.Dispose();
                if (e.Cancelled == false && e.Error == null)
                {
                    if (_MessageType == MessageType.AUDIO_FILE)
                    {
                        if (DecodeAudio())
                        {
                            if (_OnComplete != null)
                            {
                                _OnComplete(true);
                            }
                        }
                        else
                        {
                            if (_OnComplete != null)
                            {
                                _OnComplete(false);
                            }
                        }
                    }
                    else
                    {
                        if (_OnComplete != null)
                        {
                            _OnComplete(true);
                        }
                    }
                }
                else
                {
                    if (_MessageType == MessageType.AUDIO_FILE)
                    {
                        if (System.IO.File.Exists(_DirectoryPath))
                        {
                            System.IO.File.Delete(_DirectoryPath);
                        }
                    }

                    if (_OnComplete != null)
                    {
                        _OnComplete(false);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Completed() Failed ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private bool DecodeAudio()
        {
            if (System.IO.File.Exists(_DirectoryPath))
            {
                try
                {
                    CallHelperMethods.StartAudioEncodeDecodeSession();
                    byte[] rawData = System.IO.File.ReadAllBytes(_DirectoryPath);
                    rawData = CallHelperMethods.DecodeAudioFrame(rawData);

                    WaveWriter writer = new WaveWriter();
                    writer.Write(Path.ChangeExtension(_DirectoryPath, ".mp3"), rawData);
                    if (System.IO.File.Exists(_DirectoryPath))
                    {
                        System.IO.File.Delete(_DirectoryPath);
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    log.Error("DecodeAudio() Failed ==> " + ex.Message + "\n" + ex.StackTrace);
                }
                finally
                {
                    CallHelperMethods.StopAudioEncodeDecodeSession();
                }
            }
            else
            {
                log.Error("DecodeAudio() Failed ==> File not Exist.");
            }
            return false;
        }
    }
}

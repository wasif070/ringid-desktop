﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Models.Entity;
using Models.Utility;
using log4net;
using View.BindingModels;
using View.ViewModel;

namespace View.Utility.Chat
{
    public class ChatUserTyping : BackgroundWorker
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChatUserTyping).Name);

        private ConcurrentDictionary<long, Data> map = new ConcurrentDictionary<long, Data>();

        private const long MAX_VALUE = 1500;
        private const string IS_TYPING = " is typing...";
        private const string ARE_TYPING = " are typing...";
        private const string CONCATE = ", ";

        public ChatUserTyping()
        {
            WorkerSupportsCancellation = true;
            WorkerReportsProgress = true;
            DoWork += ChatUserTyping_DowWork;
            RunWorkerCompleted += ChatUserTyping_RunWorkerCompleted;
        }

        public void AddUserInfo(long userTableID, string fullName = "")
        {
            Data data = map.TryGetValue(userTableID);
            if (data == null)
            {
                if (String.IsNullOrWhiteSpace(fullName))
                {
                    fullName = userTableID.ToString();
                    UserBasicInfoModel friendModel = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(userTableID);
                    if (friendModel != null && !String.IsNullOrWhiteSpace(friendModel.ShortInfoModel.FullName))
                    {
                        fullName = friendModel.ShortInfoModel.FullName;
                    }
                }

                data = new Data();
                data.UserId = userTableID;
                data.FullName = fullName;
                data.Time = MAX_VALUE;
                map[userTableID] = data;
            }
            else
            {
                data.Time = MAX_VALUE;
            }
        }

        private void ChatUserTyping_DowWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                SetText(map.Values);

                while (true)
                {
                    if (map.Count <= 0)
                    {
                        e.Result = true;
                        break;
                    }
                    else if (this.CancellationPending)
                    {
                        e.Cancel = true;
                        break;
                    }

                    Thread.Sleep(500);
                    List<Data> deletedList = new List<Data>();

                    ICollection<Data> mapArray = map.Values;
                    foreach (Data data in mapArray)
                    {
                        data.Time = data.Time - 500;
                        if (data.Time <= 0)
                        {
                            map.TryRemove(data.UserId);
                        }
                    }

                    SetText(map.Values);

                    if (map.Count <= 0)
                    {
                        e.Result = true;
                        break;
                    }
                    else if (this.CancellationPending)
                    {
                        e.Cancel = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("ChatUserTyping_DowWork() ==> " + ex.Message + "\n" + ex.StackTrace);
                e.Cancel = true;
            }
        }

        private void ChatUserTyping_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        public void StartThread()
        {
            if (!this.IsBusy)
            {
                this.RunWorkerAsync();
            }
        }

        private void SetText(ICollection<Data> mapArray)
        {
            try
            {
                string text = String.Empty;

                if (mapArray.Count == 1)
                {
                    text = mapArray.ElementAt(0).FullName + IS_TYPING;
                }
                else if (mapArray.Count > 1)
                {
                    int idx = 0;
                    StringBuilder sb = new StringBuilder();
                    foreach (Data data in mapArray)
                    {
                        if (idx == 0)
                        {
                            sb.Append(data.FullName);
                        }
                        else if (idx == mapArray.Count - 1)
                        {
                            sb.Append(" & ");
                            sb.Append(data.FullName);
                        }
                        else 
                        {
                            sb.Append(CONCATE);
                            sb.Append(data.FullName);
                        }
                        idx++;
                    }
                    sb.Append(ARE_TYPING);
                    text = sb.ToString();
                }

                ReportProgress(0, text);
            }
            catch (Exception ex)
            {
                log.Error("SetText() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        class Data
        {
            public long UserId { get; set; }
            public string FullName { get; set; }
            public long Time { get; set; }
        }
    }
}

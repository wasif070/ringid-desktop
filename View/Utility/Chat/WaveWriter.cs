﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace View.Utility.Chat
{
    public class WaveWriter
    {

        private int Length;
        private int DataLength;
        public short Channels = 1;
        public int SampleRate = 8000;
        public short BitsPerSample = 16;

        public void Write(string outputPath, byte[] data) 
        {
            if (data != null && data.Length > 0)
            {
                DataLength = data.Length;
                Length = DataLength + 36;
                
                using (FileStream fs = new FileStream(outputPath, FileMode.Create, FileAccess.Write))
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    bw.Write(new char[4] { 'R', 'I', 'F', 'F' });
                    bw.Write(Length);//Length
                    bw.Write(new char[8] { 'W', 'A', 'V', 'E', 'f', 'm', 't', ' ' });
                    bw.Write((int)16);
                    bw.Write((short)1);
                    bw.Write(Channels);//Channel
                    bw.Write(SampleRate);//Sample Rate
                    bw.Write((int)(SampleRate * ((BitsPerSample * Channels) / 8)));//bytes per sec 
                    bw.Write((short)((BitsPerSample * Channels) / 8));//alignment 
                    bw.Write(BitsPerSample);//BitsPerSample
                    bw.Write(new char[4] { 'd', 'a', 't', 'a' });
                    bw.Write(DataLength);//DataLength
                    bw.Write(data);
                }
            }
        }

        public void Read(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            using (BinaryReader br = new BinaryReader(fs))
            {
                char[] startInfo = br.ReadChars(4);
                int length = br.ReadInt32();
                char[] format = br.ReadChars(8);
                int a = br.ReadInt32();
                short b = br.ReadInt16();
                short channels = br.ReadInt16();
                int samplerate = br.ReadInt32();
                int bytesPerSec = br.ReadInt32();
                short alignment = br.ReadInt16();
                short bitsPerSample = br.ReadInt16();
                char[] body = br.ReadChars(4);
                int dataLength = br.ReadInt32();
                byte[] data = br.ReadBytes(dataLength);
            }
        }

    }

}

﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using View.BindingModels;

namespace View.Utility.Chat
{
    class RecentTemplateSelector : DataTemplateSelector
    {
        private static DataTemplate TYPE_DATE_TITLE = null;
        private static DataTemplate TYPE_CHAT = null;
        private static DataTemplate TYPE_CALL = null;
        private static DataTemplate TYPE_ACTIVITY = null;

        static RecentTemplateSelector()
        {
            if (Application.Current != null && Application.Current.MainWindow != null)
            {
                TYPE_DATE_TITLE = Application.Current.MainWindow.FindResource("TYPE_DATE_TITLE") as DataTemplate;
                TYPE_CHAT = Application.Current.MainWindow.FindResource("TYPE_CHAT") as DataTemplate;
                TYPE_CALL = Application.Current.MainWindow.FindResource("TYPE_CALL") as DataTemplate;
                TYPE_ACTIVITY = Application.Current.MainWindow.FindResource("TYPE_ACTIVITY") as DataTemplate;
            }
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            RecentModel recentModel = item as RecentModel;

            switch (recentModel.Type)
            {
                case ChatConstants.SUBTYPE_DATE_TITLE:
                    return TYPE_DATE_TITLE;
                case ChatConstants.SUBTYPE_FRIEND_CHAT:
                case ChatConstants.SUBTYPE_GROUP_CHAT:
                case ChatConstants.SUBTYPE_ROOM_CHAT:
                    return TYPE_CHAT;
                case ChatConstants.SUBTYPE_CALL_LOG:
                    return TYPE_CALL;
                case ChatConstants.SUBTYPE_GROUP_ACTIVITY:
                    return TYPE_ACTIVITY;
                default:
                    return null;
            }
        }
    }
}

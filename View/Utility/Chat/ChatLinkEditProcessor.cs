﻿using HtmlAgilityPack;
using imsdkwrapper;
using log4net;
using Models.DAO;
using Models.Entity;
using Models.Utility;
using Models.Utility.Chat;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using View.BindingModels;
using View.Utility.Chat.Service;
using View.Utility.Recent;
using View.ViewModel;

namespace View.Utility.Chat
{
    public class ChatLinkEditProcessor
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChatLinkProcessor).Name);

        private long _FriendTableID;
        private long _GroupID;
        private string _RoomID;
        private string _ContactID = String.Empty;
        private string _Link;
        private MessageDTO _MessageDTO;

        public ChatLinkEditProcessor(long friendTableID, long groupID, string roomID, string link, MessageDTO messageDTO)
        {
            this._FriendTableID = friendTableID;
            this._GroupID = groupID;
            this._RoomID = roomID;
            this._Link = link;
            this._MessageDTO = messageDTO;

            if (_FriendTableID > 0)
            {
                _ContactID = _FriendTableID.ToString();
            }
            else if (_GroupID > 0)
            {
                _ContactID = _GroupID.ToString();
            }
            else
            {
                _ContactID = _RoomID;
            }

            Uri uri;
            if (!(Uri.TryCreate(_Link, UriKind.Absolute, out uri) && (uri.Scheme == Uri.UriSchemeHttp || uri.Scheme == Uri.UriSchemeHttps || uri.Scheme == Uri.UriSchemeFtp)))
            {
                _Link = "http://" + _Link;
            }
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Run));
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            LinkInformationDTO linkInformation = HelperMethods.FetchDynamicObject(_Link);
            if (!String.IsNullOrWhiteSpace(linkInformation.Title))
            {
                _MessageDTO.OriginalMessage = ChatJSONParser.MakeLinkMessage(_MessageDTO.OriginalMessage, linkInformation.Url, linkInformation.Title, (linkInformation.ImageUrls != null && linkInformation.ImageUrls.Count > 0 ? linkInformation.ImageUrls.FirstOrDefault() : String.Empty), linkInformation.Description);
                _MessageDTO.MessageType = (int)MessageType.LINK_MESSAGE;
                ChatJSONParser.ParseMessage(_MessageDTO);
                RecentChatCallActivityDAO.InsertMessageDTO(_MessageDTO);

                ObservableCollection<RecentModel> chatModelList = RingIDViewModel.Instance.GetRecentModelListByID(_ContactID);
                RecentModel chatModel = chatModelList.Where(P => P.UniqueKey == _MessageDTO.PacketID).FirstOrDefault();
                if (chatModel != null)
                {
                    chatModel.IsRecentOpened = false;
                    chatModel.Message.IsMessageOpened = false;
                    chatModel.Message.IsPreviewOpened = false;
                    chatModel.Message.LoadData(_MessageDTO);
                    chatModel.IsRecentOpened = true;
                    chatModel.Message.IsMessageOpened = true;
                }

                RecentModel model = RingIDViewModel.Instance.ChatLogModelsList.Where(P => P.ContactID == _ContactID && P.UniqueKey == _MessageDTO.PacketID).FirstOrDefault();
                if (model != null)
                {
                    model.Message.MessageType = (MessageType)_MessageDTO.MessageType;
                    model.Message.Message = _MessageDTO.Message;
                }
            }
            else
            {
                RecentChatCallActivityDAO.InsertMessageDTO(_MessageDTO);
            }

            SendMessage();
        }

        private void SendMessage()
        {
            if(_FriendTableID > 0)
            {
                ChatService.EditFriendChat(_MessageDTO.PacketID, _MessageDTO.FriendTableID, _MessageDTO.MessageType, _MessageDTO.Timeout, _MessageDTO.OriginalMessage, _MessageDTO.MessageDate, _MessageDTO.IsSecretVisible, null, null);
            }
            else if (_GroupID > 0)
            {
                ChatService.EditGroupChat(_MessageDTO.PacketID, _MessageDTO.GroupID, _MessageDTO.MessageType, _MessageDTO.OriginalMessage, _MessageDTO.MessageDate);
            }
            else
            {
                ChatService.EditPublicRoomChat(_MessageDTO.PacketID, _MessageDTO.RoomID, _MessageDTO.MessageType, _MessageDTO.OriginalMessage, _MessageDTO.MessageDate, _MessageDTO.FullName, _MessageDTO.ProfileImage);
            }
            
        }

    }
}

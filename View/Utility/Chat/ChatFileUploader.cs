﻿using imsdkwrapper;
using log4net;
using MediaToolkit;
using MediaToolkit.Model;
using MediaToolkit.Options;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using View.BindingModels;
using View.Constants;
using View.Utility.Call;

namespace View.Utility.Chat
{
    public class ChatFileUploader
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChatFileUploader).Name);

        private ChatFileDTO _ChatFileDTO;
        private IUploadProgress _Listener;
        private int _CurrPregressPrecentage;
        object _Locker = new object();

        string _lineEnd = "\r\n";
        string _twoHyphens = "--";
        string _boundary = "*****";

        string _FileName = string.Empty;
        byte[] _BodyBytes = null;

        public ChatFileUploader(ChatFileDTO chatFileDTO, IUploadProgress listener)
        {
            this._ChatFileDTO = chatFileDTO;
            this._Listener = listener;
        }

        public void Start()
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
                    _FileName = Path.GetFileName(_ChatFileDTO.File);
                    _BodyBytes = ChatHelpers.ReadAllBytes(_ChatFileDTO.File);

                    Thread t = new Thread(new ThreadStart(UploadProcess));
                    t.Name = this.GetType().Name;
                    t.Start();
                }
                else
                {
                    log.Error("ChatFileUploader Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    _Listener.OnFailed(_ChatFileDTO);
                }
            }
            catch (Exception ex)
            {
                log.Error("ChatFileUploader Run() Failed ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void UploadProcess()
        {
            CustomWebClient webClient = null;
            try
            {
                webClient = new CustomWebClient(new Uri(_ChatFileDTO.UploadUrl));
                webClient.Headers["Content-Type"] = "multipart/form-data; boundary=" + _boundary;
                webClient.Headers["User-Agent"] = RingIDSettings.APP_NAME + "_" + DefaultSettings.VALUE_APP_INSTALLED_VERSION;
                webClient.Headers["access-control-allow-origin"] = "*";
                var headerData = String.Empty;

                headerData += _twoHyphens + _boundary + _lineEnd;
                headerData += "Content-Disposition: form-data; name=\"sId\"" + _lineEnd + _lineEnd + DefaultSettings.LOGIN_SESSIONID + _lineEnd;

                headerData += _twoHyphens + _boundary + _lineEnd;
                headerData += "Content-Disposition: form-data; name=\"uId\"" + _lineEnd + _lineEnd + DefaultSettings.LOGIN_RING_ID + _lineEnd;

                headerData += _twoHyphens + _boundary + _lineEnd;
                headerData += "Content-Disposition: form-data; name=\"authServer\"" + _lineEnd + _lineEnd + ServerAndPortSettings.AUTH_SERVER_IP + _lineEnd;

                headerData += _twoHyphens + _boundary + _lineEnd;
                headerData += "Content-Disposition: form-data; name=\"comPort\"" + _lineEnd + _lineEnd + ServerAndPortSettings.COMMUNICATION_PORT + _lineEnd;

                headerData += _twoHyphens + _boundary + _lineEnd;
                headerData += "Content-Disposition: form-data; name=\"x-app-version\"" + _lineEnd + _lineEnd + AppConfig.VERSION_PC + _lineEnd;

                headerData += _twoHyphens + _boundary + _lineEnd;
                headerData += "Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + _FileName + "\"" + _lineEnd;
                headerData += _lineEnd;

                byte[] headerBytes = webClient.Encoding.GetBytes(headerData);
                byte[] footerBytes = webClient.Encoding.GetBytes(_lineEnd + _twoHyphens + _boundary + _twoHyphens + _lineEnd);

                long length = headerBytes.Length + _BodyBytes.Length + footerBytes.Length;
                Byte[] byteArray = new Byte[length];
                int count = 0;

                Buffer.BlockCopy(headerBytes, 0, byteArray, count, headerBytes.Length);
                count += headerBytes.Length;

                Buffer.BlockCopy(_BodyBytes, 0, byteArray, count, _BodyBytes.Length);
                count += _BodyBytes.Length;

                Buffer.BlockCopy(footerBytes, 0, byteArray, count, footerBytes.Length);
                count += footerBytes.Length;

                webClient.UploadProgressChanged += (s, e) =>
                {
                    lock (_Locker)
                    {
                        int prs = (int)((double)(e.BytesSent * 100) / e.TotalBytesToSend);
                        if (prs > _CurrPregressPrecentage)
                        {
                            _CurrPregressPrecentage = prs;
                            _Listener.OnProgress(prs, _ChatFileDTO);
                        }
                    }
                };
                webClient.UploadDataCompleted += (s, e) =>
                {
                    try
                    {
                        if (e.Error == null && e.Result != null)
                        {
                            ChatFileDTO responseDTO = ChatHelpers.GetChatFileUploadResponse(e.Result);
                            _ChatFileDTO.Message = responseDTO.UploadUrl ?? _ChatFileDTO.Message;
                            _ChatFileDTO.ErrorMsg = responseDTO.ErrorMsg;
                            if (responseDTO.Status == 1)
                            {
                                _Listener.OnSuccess(_ChatFileDTO);
                            }
                            else
                            {
                                _Listener.OnFailed(_ChatFileDTO);
                            }
                        }
                        else
                        {
                            _Listener.OnFailed(_ChatFileDTO);
                        }
                    }
                    catch (Exception ex)
                    {
                        _Listener.OnFailed(_ChatFileDTO);
                        log.Error("Error at UploadDataCompleted() in ChatFileUploader class ==>" + ex.Message + "\n" + ex.StackTrace);
                    }
                };
                webClient.UploadDataAsync(new Uri(_ChatFileDTO.UploadUrl), "POST", byteArray);
                GC.SuppressFinalize(byteArray);
                byteArray = null;
            }
            catch (Exception ex)
            {
                log.Error("Error at UploadProcess() in ChatFileUploader class ==>" + ex.Message + "\n" + ex.StackTrace);
                if (webClient != null)
                {
                    webClient.Dispose();
                    GC.SuppressFinalize(webClient);
                    webClient = null;
                }
                _Listener.OnFailed(_ChatFileDTO);
            }
            finally
            {
                if (webClient != null)
                {
                    webClient.Dispose();
                    GC.SuppressFinalize(webClient);
                    webClient = null;
                }
            }
        }

        private static void WriteToStream(System.IO.Stream s, string txt)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(txt);
            s.Write(bytes, 0, bytes.Length);
        }

        private static void WriteToStream(System.IO.Stream s, byte[] bytes)
        {
            s.Write(bytes, 0, bytes.Length);
        }
    }

    public class CustomWebClient : WebClient
    {
        public CustomWebClient(Uri address)
        {
            var webRequest = base.GetWebRequest(address);
            ((HttpWebRequest)webRequest).AllowWriteStreamBuffering = false;
            ((HttpWebRequest)webRequest).Timeout = 600000;
        }
    }

}

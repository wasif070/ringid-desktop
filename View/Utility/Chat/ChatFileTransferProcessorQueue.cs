﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using View.BindingModels;

namespace View.Utility.Chat
{
    public class ChatFileTransferProcessorQueue : LinkedList<ChatFileDTO>
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChatFileTransferProcessorQueue).Name);
        private object _Lock = new object();
        private int _FILE_INDEX = 1;
        private Queue _RunningQueque = new Queue();

        public void AddItem(ChatFileDTO obj)
        {
            try
            {
                lock (this._Lock)
                {
                    this.AddLast(obj);
                    if (this.GetItem())
                    {
                        if (obj.RecentModel != null && obj.RecentModel is MessageModel)
                        {
                            ((MessageModel)obj.RecentModel).ViewModel.IsFileQueued = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: AddItem() => + => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void AddItemToFront(ChatFileDTO obj)
        {
            try
            {
                lock (this._Lock)
                {
                    this.AddFirst(obj);
                    this.GetItem();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: AddItemToFront() => + => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private bool GetItem()
        {
            bool isQueued = true;
            if (this.Count > 0 && DefaultSettings.IsInternetAvailable && this._RunningQueque.Count < ChatConstants.NO_OF_CONCURRENT_FILE_TRANFER)
            {
                ChatFileDTO obj = this.FirstOrDefault();
                this.RemoveFirst();
                if (obj != null)
                {
                    isQueued = false;
                    this._RunningQueque.Enqueue(this._FILE_INDEX++);
                    if (obj.RecentModel != null && obj.RecentModel is MessageModel)
                    {
                        ((MessageModel)obj.RecentModel).ViewModel.IsFileQueued = false;
                    }
                    new Thread(() =>
                    {
                        Thread.Sleep(50);
                        ChatHelpers.TransferFile(obj);
                    }).Start();
                }
            }
            return isQueued;
        }

        public void OnComplete()
        {
            try
            {
                lock (this._Lock)
                {
                    if (this._RunningQueque.Count > 0)
                    {
                        this._RunningQueque.Dequeue();
                    }
                    this.GetItem();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: OnComplete() => + => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public new List<ChatFileDTO> Clear()
        {
            List<ChatFileDTO> tempList = new List<ChatFileDTO>();
            try
            {
                lock (this._Lock)
                {
                    ChatFileDTO obj = null;
                    while (this.Count > 0 && (obj = this.FirstOrDefault()) != null)
                    {
                        this.RemoveFirst();
                        if (obj.RecentModel != null && obj.RecentModel is MessageModel)
                        {
                            ((MessageModel)obj.RecentModel).ViewModel.IsFileQueued = false;
                            tempList.Add(obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Clear() => + => " + ex.Message + "\n" + ex.StackTrace);
            }
            return tempList;
        }

    }
}

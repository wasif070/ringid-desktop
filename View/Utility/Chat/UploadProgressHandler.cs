﻿using Auth.utility;
using Models.DAO;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using View.BindingModels;
using View.Constants;
using View.ViewModel;
using System.Collections.ObjectModel;
using log4net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using imsdkwrapper;
using View.Utility.Chat.Service;
using Models.Utility.Chat;
using Models.Constants;

namespace View.Utility.Chat
{
    public class UploadProgressHandler : IUploadProgress
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UploadProgressHandler).Name);

        public void OnProgress(int i, ChatFileDTO chatFileDTO)
        {
            try
            {
                RecentModel chatModel = (RecentModel)chatFileDTO.RecentModel;
                if (chatModel != null)
                {
                    chatModel.Message.ViewModel.UploadPercentage = i;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at OnProgress() in UploadProgressHandler class ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }


        public void OnSuccess(ChatFileDTO chatFileDTO)
        {
            try
            {
                RecentModel chatModel = (RecentModel)chatFileDTO.RecentModel;
                MessageDTO messageDTO = chatFileDTO.MessageDTO;

                if (messageDTO != null)
                {
                    switch ((MessageType)messageDTO.MessageType)
                    {
                        case MessageType.IMAGE_FILE_FROM_GALLERY:
                        case MessageType.IMAGE_FILE_FROM_CAMERA:
                            messageDTO.OriginalMessage = ChatJSONParser.MakeImageMessage(chatFileDTO.Message, messageDTO.Caption, messageDTO.Width, messageDTO.Height);
                            break;
                        case MessageType.AUDIO_FILE:
                        case MessageType.VIDEO_FILE:
                            messageDTO.OriginalMessage = ChatJSONParser.MakeMultimediaMessage(chatFileDTO.Message, messageDTO.FileSize, messageDTO.Duration);
                            break;
                    }

                    ChatJSONParser.ParseMessage(messageDTO);
                    RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);
                    
                    if (chatModel != null)
                    {
                        chatModel.Message.IsFileOpened = true;
                        chatModel.Message.ViewModel.IsUploadRunning = false;
                        chatModel.Message.ViewModel.UploadPercentage = 0;
                        chatModel.Message.FileSize = messageDTO.FileSize;
                        chatModel.Message.Width = messageDTO.Width;
                        chatModel.Message.Height = messageDTO.Height;
                        chatModel.Message.OriginalMessage = messageDTO.OriginalMessage;
                        chatModel.Message.Message = messageDTO.Message;
                        chatModel.Message.Status = messageDTO.Status;
                    }

                    if (messageDTO.FriendTableID > 0)
                    {
                        ChatService.SendFriendChat(messageDTO.PacketID, messageDTO.FriendTableID, messageDTO.MessageType, messageDTO.Timeout, messageDTO.OriginalMessage, messageDTO.MessageDate, messageDTO.IsSecretVisible, null, null);
                    }
                    else if (messageDTO.GroupID > 0)
                    {
                        ChatService.SendGroupChat(messageDTO.PacketID, messageDTO.GroupID, messageDTO.MessageType, messageDTO.OriginalMessage, messageDTO.MessageDate);
                    }
                    else
                    {
                        ChatService.SendPublicRoomChat(messageDTO.PacketID, messageDTO.RoomID, messageDTO.MessageType, messageDTO.OriginalMessage, messageDTO.MessageDate, messageDTO.FullName, messageDTO.ProfileImage);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at OnSuccess() in UploadProgressHandler class ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnFailed(ChatFileDTO chatFileDTO)
        {
            try
            {
                RecentModel chatModel = (RecentModel)chatFileDTO.RecentModel;
                MessageDTO messageDTO = chatFileDTO.MessageDTO;

                if (messageDTO != null)
                {
                    messageDTO.Status = ChatConstants.STATUS_FAILED;

                    switch ((MessageType)messageDTO.MessageType)
                    {
                        case MessageType.IMAGE_FILE_FROM_GALLERY:
                        case MessageType.IMAGE_FILE_FROM_CAMERA:
                            messageDTO.OriginalMessage = ChatJSONParser.MakeImageMessage(chatFileDTO.Message, messageDTO.Caption, messageDTO.Width, messageDTO.Height);
                            break;
                        case MessageType.AUDIO_FILE:
                        case MessageType.VIDEO_FILE:
                            messageDTO.OriginalMessage = ChatJSONParser.MakeMultimediaMessage(chatFileDTO.Message, messageDTO.FileSize, messageDTO.Duration);
                            break;
                    }

                    ChatJSONParser.ParseMessage(messageDTO);
                    RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                    if (chatModel != null)
                    {
                        chatModel.Message.IsFileOpened = true;
                        chatModel.Message.ViewModel.IsUploadRunning = false;
                        chatModel.Message.ViewModel.UploadPercentage = 0;
                        chatModel.Message.FileSize = messageDTO.FileSize;
                        chatModel.Message.Width = messageDTO.Width;
                        chatModel.Message.Height = messageDTO.Height;
                        chatModel.Message.Message = messageDTO.Message;
                        chatModel.Message.Status = messageDTO.Status;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at OnFailed() in UploadProgressHandler class ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace View.Utility.Chat
{
    public class PacketTimeID
    {
        public String PacketID { get; set; }
        public long Time { get; set; }
        public long NanoTime { get; set; }
    }
}

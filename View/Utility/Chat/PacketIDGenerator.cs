﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace View.Utility.Chat
{
    public static class PacketIDGenerator
    {

        private static long lastTimestamp = 0;
        private static DateTime utc1582 = new DateTime(1582, 10, 15, 0, 0, 0, DateTimeKind.Utc);
        private static long START_EPOCH = (long)(utc1582 - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;

        private static long FromUnixTimestamp(long tstamp)
        {
            return (tstamp - START_EPOCH) * 10000;
        }

        private static long MillisOf(long timestamp)
        {
            return timestamp / 10000;
        }

        private static long MakeMSB(long timestamp)
        {
            long msb = 0L;
            msb |= (0x00000000ffffffffL & timestamp) << 32;
            msb |= (0x0000ffff00000000L & timestamp) >> 16;
            msb |= (0x0fff000000000000L & timestamp) >> 48;
            msb |= 0x0000000000001000L;
            return msb;
        }

        private static String PadHex(long l, int n)
        {
            String s = l.ToString("X4").ToLower();
            while (s.Length < n)
            {
                s = "0" + s;
            }
            return s;
        }

        private static long GetCurrentTimestamp(long time)
        {
            while (true)
            {
                long now = FromUnixTimestamp(time);
                long last = Interlocked.Read(ref lastTimestamp);
                if (now > last)
                {
                    Interlocked.CompareExchange(ref lastTimestamp, now, last);
                    if (Interlocked.Read(ref lastTimestamp) == now)
                    {
                        return now;
                    }
                }
                else
                {
                    long lastMillis = MillisOf(last);
                    if (MillisOf(now) < MillisOf(last))
                    {
                        return Interlocked.Increment(ref lastTimestamp);
                    }

                    long candidate = last + 1;
                    if (MillisOf(candidate) == lastMillis)
                    {
                        Interlocked.CompareExchange(ref lastTimestamp, candidate, last);
                        if (Interlocked.Read(ref lastTimestamp) == candidate)
                        {
                            return candidate;
                        }
                    }
                }
            }
        }

        public static long GetUnixTimestamp(string name)
        {
            if (String.IsNullOrWhiteSpace(name))
            {
                return -1;
            }

            string[] components = name.Split('-');
            if (components.Length != 5)
            {
                return -1;
            }

            long mostSigBits = Int64.Parse(components[0], NumberStyles.HexNumber);
            mostSigBits <<= 16;
            mostSigBits |= Int64.Parse(components[1], NumberStyles.HexNumber);
            mostSigBits <<= 16;
            mostSigBits |= Int64.Parse(components[2], NumberStyles.HexNumber);

            long timestamp = (mostSigBits & 0x0FFFL) << 48
              | ((mostSigBits >> 16) & 0x0FFFFL) << 32
              | (long)((ulong)mostSigBits >> 32);

            return timestamp;
        }

        public static long GetTime(string name)
        {
            return (GetUnixTimestamp(name) / 10000) + START_EPOCH;
        }

        private static String GetUUID(long time, long identity)
        {
            long mostSigBits = MakeMSB(time);
            return PadHex((long)(((ulong)mostSigBits & 0xFFFFFFFF00000000L) >> 32) & 0xFFFFFFFFL, 8)
                   + "-" +
                   PadHex(((mostSigBits & 0xFFFF0000L) >> 16), 4)
                   + "-" +
                   PadHex((mostSigBits & 0x0000000000000000FFFFL), 4)
                   + "-" +
                   PadHex((long)((((ulong)identity & 0xFFFF000000000000L) >> 48) & 0xFFFF), 4)
                   + "-" +
                   PadHex(identity & 0xFFFFFFFFFFFFL, 12);
        }

        public static String GeneratedPacketIDByFixTimeAndIdentity(long time, long identity)
        {
            return GetUUID(FromUnixTimestamp(time), identity);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using imsdkwrapper;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Models.Utility.Chat;
using View.BindingModels;
using View.Constants;
using View.Dictonary;
using View.UI;
using View.UI.Chat;
using View.UI.Group;
using View.UI.Profile.FriendProfile;
using View.UI.Room;
using View.Utility.audio;
using View.Utility.Chat.Service;
using View.Utility.Recent;
using View.Utility.Recorder;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.Utility.Call.Settings;
using System.Diagnostics;
using System.Drawing.Drawing2D;
using View.UI.Call;
using System.Collections.Concurrent;
using View.Utility.FriendList;
using View.Utility.Call;
using MediaToolkit.Options;
using MediaToolkit.Model;
using MediaToolkit;

namespace View.Utility.Chat
{
    public static class ChatHelpers
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChatHelpers).Name);

        #region Member Variable

        private static object FEED_MESSAGE_LOCK = new object();
        private const long ONE_MB = 1048576;
        public static int[] VERTICAL_DIMENSION = { 720, 1480 };
        public static int[] HORIZONTAL_DIMENSION = { 1480, 720 };

        public static int[][] CHAT_VIEW_HEIGHT;
        public static int CALL_VIEW_HEIGHT = 45;
        public static int ACTIVITY_VIEW_HEIGHT = 45;
        public static int TIME_VIEW_HEIGHT = 35;

        static ChatHelpers()
        {
            CHAT_VIEW_HEIGHT = new int[21][];
            CHAT_VIEW_HEIGHT[(int)MessageType.DELETE_MESSAGE] = new int[] { 42, 4 };
            CHAT_VIEW_HEIGHT[(int)MessageType.BLANK_MESSAGE] = new int[] { 0, 4 };
            CHAT_VIEW_HEIGHT[(int)MessageType.PLAIN_MESSAGE] = new int[] { 42, 4 };
            CHAT_VIEW_HEIGHT[(int)MessageType.EMOTICON_MESSAGE] = new int[] { 42, 4 };
            CHAT_VIEW_HEIGHT[(int)MessageType.LOCATION_MESSAGE] = new int[] { 160, 4 };
            CHAT_VIEW_HEIGHT[(int)MessageType.LINK_MESSAGE] = new int[] { 150, 4 };
            CHAT_VIEW_HEIGHT[(int)MessageType.DOWNLOAD_STICKER_MESSAGE] = new int[] { 135, 4 };
            CHAT_VIEW_HEIGHT[(int)MessageType.IMAGE_FILE_FROM_GALLERY] = new int[] { 160, 4 };
            CHAT_VIEW_HEIGHT[(int)MessageType.AUDIO_FILE] = new int[] { 60, 4 };
            CHAT_VIEW_HEIGHT[(int)MessageType.VIDEO_FILE] = new int[] { 170, 4 };
            CHAT_VIEW_HEIGHT[(int)MessageType.IMAGE_FILE_FROM_CAMERA] = new int[] { 160, 4 };
            CHAT_VIEW_HEIGHT[(int)MessageType.FILE_STREAM] = new int[] { 86, 4 };
            CHAT_VIEW_HEIGHT[(int)MessageType.CONTACT_SHARE] = new int[] { 86, 4 };
            CHAT_VIEW_HEIGHT[(int)MessageType.RING_MEDIA_MESSAGE] = new int[] { 180, 4 };
        }

        public const double UI_UNIT_SIZE = 45;
        public const double UP_SCROLL_LIMIT = 700;
        public const int DEFAULT_SECRET_INDEX = 5;
        public static string[] SECONDS_ARRAY = {"", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
        "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40",
        "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60",
        "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80",
        "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "100",
        "101", "102", "103", "104", "105", "106", "107", "108", "109", "110", "111", "112", "113", "114", "115", "116", "117", "118", "119", "120", ""};
        public static string[] EXT_IMAGE = { ".jpg", ".jpeg", ".png" };
        public static string[] FILE_SIZE_SUFFIX = { "B", "KB", "MB", "GB", "TB", "PB", "EB" };

        public static string CHAT_DATE_FORMAT = "{0:D}";
        public static string CHAT_TIME_FORMAT = "{0:t}";

        public static string STATUS_TXT_EDITED = "Edited";
        public static string STATUS_TXT_SENDING = "Sending";
        public static string STATUS_TXT_SENT = "Delivered";
        public static string STATUS_TXT_DELIVERED = "Delivered";
        public static string STATUS_TXT_SEEN = "Seen";
        public static string STATUS_TXT_FAILED = "Failed";
        public static string STATUS_TXT_VIEWED = "Seen";
        public static string STATUS_TXT_PENDING = "Failed";
        public static string STATUS_TXT_DISAPPEARED_IMAGE_VIDEO = "Viewed";
        public static string STATUS_TXT_DISAPPEARED_VOICE = "Played";
        public static string STATUS_TXT_DISAPPEARED_DEFAULT = "Deleted";

        public static string STATUS_TXT_FILE_SENT = "File Sent";
        public static string STATUS_TXT_FILE_RECEIVED = "File Received";
        public static string STATUS_TXT_FILE_SENDING_CANCELLED = "Sending Cancelled";
        public static string STATUS_TXT_FILE_RECEIVING_CANCELLED = "Receiving Cancelled";
        public static string STATUS_TXT_FILE_MOVED = "File Moved";
        public static string STATUS_TXT_FILE_QUEUED = "Queued...";

        #endregion Member Variable

        #region Common Method

        public static void DeleteChatMessage(long friendTableID, long groupID, string roomId, string packetID, long senderTableID, bool bothDevice = true)
        {
            //new Thread(() =>
            //{
            //    Thread.Sleep(200);
            MessageDTO messageDTO = new MessageDTO();
            messageDTO.FriendTableID = friendTableID;
            messageDTO.GroupID = groupID;
            messageDTO.RoomID = roomId;
            messageDTO.PacketID = packetID;
            messageDTO.SenderTableID = senderTableID;
            ChatHelpers.DeleteChatMessage(messageDTO, bothDevice);
            //}).Start();
        }

        public static void DeleteChatMessage(MessageDTO messageDTO, bool bothDevice = true)
        {
            try
            {
                if (messageDTO.FriendTableID > 0)
                {
                    List<string> list = new List<string>();
                    list.Add(messageDTO.PacketID);
                    RecentChatCallActivityDAO.DeleteChatHistory(messageDTO.FriendTableID, 0, list);
                    RecentLoadUtility.Remove(messageDTO.FriendTableID, messageDTO.PacketID);
                    FileTransferSession.CancelTransferFileByPacketIDs(messageDTO.FriendTableID, list, false);

                    if (bothDevice && messageDTO.SenderTableID == DefaultSettings.LOGIN_TABLE_ID)
                    {
                        List<String> packetIds = new List<String>();
                        packetIds.Add(messageDTO.PacketID);
                        ChatService.DeleteFriendChat(messageDTO.FriendTableID, packetIds);
                    }
                }
                else if (messageDTO.GroupID > 0)
                {
                    List<string> list = new List<string>();
                    list.Add(messageDTO.PacketID);
                    RecentChatCallActivityDAO.DeleteChatHistory(0, messageDTO.GroupID, list);
                    RecentLoadUtility.Remove(messageDTO.GroupID, messageDTO.PacketID);
                    FileTransferSession.CancelTransferFileByPacketIDs(messageDTO.GroupID, list, false);

                    if (bothDevice && messageDTO.SenderTableID == DefaultSettings.LOGIN_TABLE_ID)
                    {
                        List<string> packetIds = new List<string>();
                        packetIds.Add(messageDTO.PacketID);
                        ChatService.DeleteGroupChat(messageDTO.GroupID, packetIds);
                    }
                }
                else if (!String.IsNullOrWhiteSpace(messageDTO.RoomID))
                {
                    List<string> list = new List<string>();
                    list.Add(messageDTO.PacketID);
                    RecentLoadUtility.Remove(messageDTO.RoomID, messageDTO.PacketID);

                    if (bothDevice && messageDTO.SenderTableID == DefaultSettings.LOGIN_TABLE_ID)
                    {
                        List<string> packetIds = new List<string>();
                        packetIds.Add(messageDTO.PacketID);
                        ChatService.DeletePublicRoomChat(messageDTO.RoomID, packetIds);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: DeleteChatMessage() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddCallMessage(long friendTableId, long userTableId, string message, string packetID)
        {
            try
            {
                UCFriendChatCallPanel ucFriendChatCall = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(friendTableId);
                UserBasicInfoModel friendInfoModel = ucFriendChatCall != null ? ucFriendChatCall.FriendBasicInfoModel : RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(friendTableId);

                MessageDTO messageDTO = new MessageDTO();
                messageDTO.MessageType = (int)MessageType.PLAIN_MESSAGE;
                messageDTO.SenderTableID = userTableId;
                messageDTO.OriginalMessage = message;
                messageDTO.FriendTableID = friendTableId;
                messageDTO.PacketID = packetID;
                messageDTO.MessageDate = PacketIDGenerator.GetTime(messageDTO.PacketID);

                if (messageDTO.SenderTableID != DefaultSettings.LOGIN_TABLE_ID)
                {
                    messageDTO.PacketType = ChatConstants.PACKET_TYPE_CALL_BUSY_MESSAGE;
                    messageDTO.Status = ChatConstants.STATUS_DELIVERED;
                    ChatService.SendFriendCallBusyMessage(messageDTO.PacketID, messageDTO.FriendTableID, messageDTO.OriginalMessage, messageDTO.MessageDate);
                }
                else
                {
                    messageDTO.PacketType = ChatConstants.PACKET_TYPE_FRIEND_MESSAGE;
                    messageDTO.Status = ChatConstants.STATUS_DELIVERED;
                }

                ChatJSONParser.ParseMessage(messageDTO);
                RecentDTO recentDTO = new RecentDTO();
                recentDTO.FriendTableID = friendTableId;
                recentDTO.ContactID = friendTableId.ToString();
                recentDTO.Time = messageDTO.MessageDate;
                recentDTO.Message = messageDTO;

                if (messageDTO.SenderTableID == DefaultSettings.LOGIN_TABLE_ID)
                {
                    RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                    if (friendInfoModel.ShortInfoModel.ImSoundEnabled)
                    {
                        AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_DELIEVERED_TUNE);
                    }

                    if (ucFriendChatCall != null)
                    {
                        RecentLoadUtility.LoadRecentData(recentDTO);
                    }
                }
                else
                {
                    bool isDeleted = messageDTO.MessageType == (int)MessageType.DELETE_MESSAGE;
                    bool isNotifiable = !(isDeleted || ChatHelpers.IsDontDisturbMood());
                    bool isChatViewInactive = UIHelperMethods.IsViewActivate(ucFriendChatCall) == false;
                    bool isChatViewHiddenInactive = isChatViewInactive || HelperMethods.IsViewVisible(ucFriendChatCall) == false;
                    bool isViewAtTop = HelperMethods.IsViewAtTop(ucFriendChatCall);

                    bool isSound = friendInfoModel.ShortInfoModel.ImSoundEnabled && isNotifiable;
                    bool isPopup = friendInfoModel.ShortInfoModel.ImPopupEnabled && isNotifiable && isChatViewInactive;
                    bool isUnread = friendInfoModel.ShortInfoModel.ImNotificationEnabled && isNotifiable && (isChatViewHiddenInactive || isViewAtTop);

                    messageDTO.IsUnread = isUnread;
                    RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                    recentDTO.IsMoveToButtom = !isViewAtTop;

                    if (isSound)
                    {
                        if (isChatViewHiddenInactive)
                        {
                            AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_RECEIVED_ALERT_TUNE);
                        }
                        else
                        {
                            AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_RECEIVED_TUNE);
                        }
                    }

                    if (isPopup)
                    {
                        WNChatPopupMessage.ShowNotification(recentDTO);
                    }

                    if (isUnread)
                    {
                        ChatHelpers.AddUnreadChat(recentDTO.ContactID, recentDTO.Message.PacketID, recentDTO.Message.MessageDate);
                    }

                    if (ucFriendChatCall != null && ucFriendChatCall.IsVisible)
                    {
                        RecentLoadUtility.LoadRecentData(recentDTO);
                    }
                }

                ChatLogLoadUtility.LoadRecentData(recentDTO);
            }
            catch (Exception ex)
            {
                log.Error("Error: AddInstanceMessage() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void AddForwardOrShareMessage(long friendTableId, long groupId, string roomId, MessageType messageType, object message, bool isSecretChatApplicable, MessageModel srcModel)
        {
            if (friendTableId > 0)
            {
                RingIDViewModel.Instance.OnFriendCallChatButtonClicked(friendTableId);
            }
            else if (groupId > 0)
            {
                RingIDViewModel.Instance.OnGroupCallChatButtonClicked(groupId);
            }

            else if (!String.IsNullOrWhiteSpace(roomId))
            {
                RingIDViewModel.Instance.OnRoomCallChatButtonClicked(roomId);
            }

            new Thread(() =>
            {
                try
                {
                    Thread.Sleep(1000);

                    if (friendTableId > 0)
                    {
                        UCFriendChatCallPanel ucFriendChatCall = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(friendTableId);
                        if (ucFriendChatCall != null)
                        {
                            int anonymousSettingName = -1;
                            if (!HelperMethods.HasAnonymousChatPermission(ucFriendChatCall.FriendBasicInfoModel))
                            {
                                if (HelperMethods.ShowAnonymousWarning() == MessageBoxResult.Yes)
                                {
                                    return;
                                }
                                else
                                {
                                    anonymousSettingName = StatusConstants.ANONYMOUS_CHAT;
                                }
                            }

                            HelperMethods.ChangeAnonymousSettingsWrapper((result) =>
                            {
                                if (result == false) return 0;

                                int accessType = -1;
                                if (!HelperMethods.HasFriendChatPermission(ucFriendChatCall.FriendBasicInfoModel, ucFriendChatCall.BlockedNonFriendModel, out accessType))
                                {
                                    if (HelperMethods.ShowBlockWarning(ucFriendChatCall.FriendBasicInfoModel, accessType))
                                    {
                                        return 0;
                                    }
                                }

                                HelperMethods.ChangeBlockUnblockSettingsWrapper(accessType, ucFriendChatCall.FriendBasicInfoModel, ucFriendChatCall.BlockedNonFriendModel, (status) =>
                                {
                                    if (status == false) return 0;

                                    MessageDTO messageDTO = new MessageDTO();
                                    messageDTO.MessageType = (int)messageType;
                                    PacketTimeID packet = ChatService.GeneratePacketID(ModelUtility.CurrentTimeMillis());
                                    messageDTO.PacketID = packet.PacketID;
                                    messageDTO.MessageDate = packet.Time;
                                    messageDTO.OriginalMessage = message.ToString();
                                    messageDTO.PacketType = ChatConstants.PACKET_TYPE_FRIEND_MESSAGE;
                                    messageDTO.Status = ChatConstants.STATUS_SENDING;
                                    messageDTO.FriendTableID = friendTableId;
                                    messageDTO.SenderTableID = DefaultSettings.LOGIN_TABLE_ID;

                                    if (isSecretChatApplicable)
                                    {
                                        if (srcModel != null)
                                        {
                                            messageDTO.Timeout = srcModel.Timeout;
                                            messageDTO.IsSecretVisible = srcModel.IsSecretVisible;
                                        }
                                        else if (ucFriendChatCall.IsSecretCheckOn)
                                        {
                                            messageDTO.Timeout = ucFriendChatCall.SecretTimerValue;
                                            messageDTO.IsSecretVisible = ucFriendChatCall.FriendBasicInfoModel.ShortInfoModel.IsSecretVisible;
                                        }
                                    }

                                    if (messageType == MessageType.DOWNLOAD_STICKER_MESSAGE)
                                    {
                                        if (ucFriendChatCall.FriendBasicInfoModel.ShortInfoModel.ImSoundEnabled)
                                        {
                                            AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_STICKER_SEND_TUNE);
                                        }
                                        if (message is MarkertStickerImagesModel)
                                        {
                                            HelperMethods.UpdateRingMarketStickerLastUsedDate((MarkertStickerImagesModel)message);
                                        }
                                    }

                                    ChatJSONParser.ParseMessage(messageDTO);
                                    RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);
                                    ChatHelpers.SaveFeedMediaImageToChatDirectory(messageDTO, srcModel);

                                    RecentLoadUtility.LoadRecentData(new RecentDTO { ContactID = friendTableId.ToString(), FriendTableID = friendTableId, Message = messageDTO, Time = messageDTO.MessageDate, IsMoveToButtom = true });
                                    ChatLogLoadUtility.LoadRecentData(new RecentDTO { ContactID = friendTableId.ToString(), FriendTableID = friendTableId, Message = messageDTO, Time = messageDTO.MessageDate });
                                    ChatService.SendFriendChat(messageDTO.PacketID, messageDTO.FriendTableID, messageDTO.MessageType, messageDTO.Timeout, messageDTO.OriginalMessage, messageDTO.MessageDate, messageDTO.IsSecretVisible, ucFriendChatCall.FriendBasicInfoModel, ucFriendChatCall.BlockedNonFriendModel);
                                    return 1;
                                });

                                return 1;
                            }, anonymousSettingName);
                        }
                    }
                    else if (groupId > 0)
                    {
                        MessageDTO messageDTO = new MessageDTO();
                        messageDTO.MessageType = (int)messageType;
                        PacketTimeID packet = ChatService.GeneratePacketID(ModelUtility.CurrentTimeMillis());
                        messageDTO.PacketID = packet.PacketID;
                        messageDTO.MessageDate = packet.Time;
                        messageDTO.OriginalMessage = message.ToString();
                        messageDTO.PacketType = ChatConstants.PACKET_TYPE_GROUP_MESSAGE;
                        messageDTO.Status = ChatConstants.STATUS_SENDING;
                        messageDTO.GroupID = groupId;
                        messageDTO.SenderTableID = DefaultSettings.LOGIN_TABLE_ID;

                        if (messageType == MessageType.DOWNLOAD_STICKER_MESSAGE)
                        {
                            GroupInfoModel groupInfoModel = RingIDViewModel.Instance.GetGroupInfoModelByGroupID(groupId);
                            if (groupInfoModel.ImSoundEnabled)
                            {
                                AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_STICKER_SEND_TUNE);
                            }
                            if (message is MarkertStickerImagesModel)
                            {
                                HelperMethods.UpdateRingMarketStickerLastUsedDate((MarkertStickerImagesModel)message);
                            }
                        }

                        ChatJSONParser.ParseMessage(messageDTO);
                        RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);
                        ChatHelpers.SaveFeedMediaImageToChatDirectory(messageDTO, srcModel);

                        RecentLoadUtility.LoadRecentData(new RecentDTO { ContactID = groupId.ToString(), GroupID = groupId, Message = messageDTO, Time = messageDTO.MessageDate, IsMoveToButtom = true });
                        ChatLogLoadUtility.LoadRecentData(new RecentDTO { ContactID = groupId.ToString(), GroupID = groupId, Message = messageDTO, Time = messageDTO.MessageDate });
                        ChatService.SendGroupChat(messageDTO.PacketID, messageDTO.GroupID, messageDTO.MessageType, messageDTO.OriginalMessage, messageDTO.MessageDate);
                    }
                    else if (!String.IsNullOrWhiteSpace(roomId))
                    {
                        MessageDTO messageDTO = new MessageDTO();
                        messageDTO.MessageType = (int)messageType;
                        PacketTimeID packet = ChatService.GeneratePacketID(ModelUtility.CurrentTimeMillis());
                        messageDTO.PacketID = packet.PacketID;
                        messageDTO.MessageDate = packet.Time;
                        messageDTO.OriginalMessage = message.ToString();
                        messageDTO.PacketType = ChatConstants.PACKET_TYPE_ROOM_MESSAGE;
                        messageDTO.Status = ChatConstants.STATUS_SENDING;
                        messageDTO.RoomID = roomId;
                        messageDTO.SenderTableID = DefaultSettings.LOGIN_TABLE_ID;
                        messageDTO.FullName = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName;
                        messageDTO.ProfileImage = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.ProfileImage;

                        if (messageType == MessageType.DOWNLOAD_STICKER_MESSAGE)
                        {
                            RoomModel roomModel = RingIDViewModel.Instance.GetRoomModelByRoomID(roomId);
                            if (roomModel.ImSoundEnabled)
                            {
                                AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_STICKER_SEND_TUNE);
                            }
                            if (message is MarkertStickerImagesModel)
                            {
                                HelperMethods.UpdateRingMarketStickerLastUsedDate((MarkertStickerImagesModel)message);
                            }
                        }

                        ChatJSONParser.ParseMessage(messageDTO);
                        RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);
                        ChatHelpers.SaveFeedMediaImageToChatDirectory(messageDTO, srcModel);

                        RecentLoadUtility.LoadRecentData(new RecentDTO { ContactID = roomId, RoomID = roomId, Message = messageDTO, Time = messageDTO.MessageDate, IsMoveToButtom = true });
                        ChatLogLoadUtility.LoadRecentData(new RecentDTO { ContactID = roomId, RoomID = roomId, Message = messageDTO, Time = messageDTO.MessageDate });
                        ChatService.SendPublicRoomChat(messageDTO.PacketID, messageDTO.RoomID, messageDTO.MessageType, messageDTO.OriginalMessage, messageDTO.MessageDate, messageDTO.FullName, messageDTO.ProfileImage);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: AddMediaMessage() ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            }).Start();
        }

        public static void AddFeedMessage(FeedDTO feedDTO)
        {
            //if (feedDTO.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
            //    return;

            //List<ImageDTO> imageDTOs = feedDTO.ImageList != null ? feedDTO.ImageList.ToList() : new List<ImageDTO>();
            //List<SingleMediaDTO> mediaDTOs = feedDTO.MediaContent != null && feedDTO.MediaContent.MediaList != null ? feedDTO.MediaContent.MediaList.ToList() : new List<SingleMediaDTO>();

            //new Thread(() =>
            //{
            //    try
            //    {
            //        lock (ChatHelpers.FEED_MESSAGE_LOCK)
            //        {
            //            List<string> param = new List<string>();
            //            List<RecentDTO> rList = new List<RecentDTO>();
            //            List<MessageDTO> mList = new List<MessageDTO>();

            //            if (!String.IsNullOrWhiteSpace(feedDTO.Status))
            //            {
            //                MessageDTO messageDTO = new MessageDTO();
            //                messageDTO.MessageType = ChatConstants.TYPE_PLAIN_MSG;
            //                messageDTO.PacketID = PacketIDGenerator.GeneratedPacketIDByFixTimeAndIdentity(feedDTO.ActualTime, feedDTO.NewsfeedId);
            //                messageDTO.OriginalMessage = feedDTO.Status;
            //                mList.Add(messageDTO);
            //                param.Add(messageDTO.PacketID);
            //            }

            //            foreach (ImageDTO imageDTO in imageDTOs)
            //            {
            //                MessageDTO messageDTO = new MessageDTO();
            //                messageDTO.MessageType = ChatConstants.TYPE_IMAGE_FILE_DIRECTORY;
            //                messageDTO.PacketID = PacketIDGenerator.GeneratedPacketIDByFixTimeAndIdentity(feedDTO.ActualTime, long.Parse(feedDTO.NewsfeedId.ToString() + imageDTO.ImageId.ToString()));
            //                messageDTO.OriginalMessage = ChatJSONParser.MakeImageMessage(imageDTO.ImageUrl, imageDTO.Caption, imageDTO.ImageWidth, imageDTO.ImageHeight);
            //                mList.Add(messageDTO);
            //                param.Add(messageDTO.PacketID);
            //            }

            //            foreach (SingleMediaDTO mediaDTO in mediaDTOs)
            //            {
            //                MessageDTO messageDTO = new MessageDTO();
            //                messageDTO.MessageType = ChatConstants.TYPE_RING_MEDIA_MSG;
            //                messageDTO.PacketID = PacketIDGenerator.GeneratedPacketIDByFixTimeAndIdentity(feedDTO.ActualTime, long.Parse(feedDTO.NewsfeedId.ToString() + mediaDTO.ContentId.ToString()));
            //                messageDTO.OriginalMessage = ChatJSONParser.MakeRingMediaMessage(mediaDTO);
            //                mList.Add(messageDTO);
            //                param.Add(messageDTO.PacketID);
            //            }

            //            if (mList.Count > 0)
            //            {
            //                param = RecentChatCallActivityDAO.GetFeedMessagePacketIDs(feedDTO.RingID, param);
            //                for (int idx = mList.Count - 1; idx >= 0; idx--)
            //                {
            //                    MessageDTO messageDTO = mList[idx];
            //                    if (param.Contains(messageDTO.PacketID))
            //                    {
            //                        mList.RemoveAt(idx);
            //                    }
            //                    else
            //                    {
            //                        messageDTO.MessageDate = feedDTO.ActualTime + idx;
            //                        messageDTO.PacketType = ChatConstants.PACKET_TYPE_FEED_MESSAGE;
            //                        messageDTO.SenderTableID = feedDTO.UserTableID;
            //                        messageDTO.FriendTableID = feedDTO.UserTableID;
            //                        messageDTO.Status = ChatConstants.STATUS_DELIVERED;
            //                        ChatJSONParser.ParseMessage(messageDTO);
            //                        rList.Add(new RecentDTO { ContactID = feedDTO.RingID.ToString(), FriendTableID = feedDTO.RingID, Message = messageDTO });
            //                    }
            //                }
            //            }

            //            if (rList.Count > 0)
            //            {
            //                new InsertIntoFriendMessageTable(mList).Run();
            //                UCFriendChatCallPanel ucFriendChatCall = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(feedDTO.UserTableID);
            //                UserBasicInfoModel friendInfoModel = ucFriendChatCall != null ? ucFriendChatCall.FriendBasicInfoModel : RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(feedDTO.UserTableID, feedDTO.RingID, feedDTO.FullName, feedDTO.ProfileImage);

            //                bool isChatViewInactive = UIHelperMethods.IsViewActivate(ucFriendChatCall) == false;
            //                bool isChatViewHiddenInactive = isChatViewInactive || HelperMethods.IsViewVisible(ucFriendChatCall) == false;
            //                bool isDontDisturbMood = ChatHelpers.IsDontDisturbMood();
            //                bool isSoundEnable = friendInfoModel.ShortInfoModel.ImSoundEnabled && isDontDisturbMood == false;
            //                bool isNotificationEnable = friendInfoModel.ShortInfoModel.ImNotificationEnabled && isChatViewHiddenInactive && isDontDisturbMood == false;
            //                bool isPopupEnable = friendInfoModel.ShortInfoModel.ImPopupEnabled && isChatViewInactive && isDontDisturbMood == false;

            //                if (isSoundEnable)
            //                {
            //                    if (isChatViewHiddenInactive)
            //                    {
            //                        AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_RECEIVED_ALERT_TUNE);
            //                    }
            //                    else
            //                    {
            //                        AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_RECEIVED_TUNE);
            //                    }
            //                }

            //                RecentDTO maxRecentDTO = rList.OrderByDescending(P => P.Message.MessageDate).FirstOrDefault();
            //                if (isPopupEnable)
            //                {
            //                    WNChatPopupMessage.ShowNotification(maxRecentDTO);
            //                }
            //                if (ucFriendChatCall != null && ucFriendChatCall.IsVisible)
            //                {
            //                    RecentLoadUtility.LoadRecentData(rList);
            //                }
            //                if (isNotificationEnable)
            //                {
            //                    RecentDTO prevRecentDTO = ChatDictionaries.Instance.CHAT_UNREAD_MESSAGE.TryGetValue(feedDTO.RingID.ToString());
            //                    if (prevRecentDTO == null)
            //                    {
            //                        ChatDictionaries.Instance.CHAT_UNREAD_MESSAGE[feedDTO.RingID.ToString()] = maxRecentDTO;
            //                        MainSwitcher.AuthSignalHandler().callChatAuthSignalHandler.SetandShowChatNotificationCount();
            //                        FlashWindowHelper.StartFlashing();
            //                    }
            //                    else if (maxRecentDTO.Message.MessageDate >= prevRecentDTO.Message.MessageDate)
            //                    {
            //                        ChatDictionaries.Instance.CHAT_UNREAD_MESSAGE[feedDTO.RingID.ToString()] = maxRecentDTO;
            //                        MainSwitcher.AuthSignalHandler().callChatAuthSignalHandler.SetandShowChatNotificationCount();
            //                        FlashWindowHelper.StartFlashing();
            //                    }
            //                }

            //                ChatLogLoadUtility.LoadRecentData(maxRecentDTO);
            //            }
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        log.Error("Error: AddFeedMessage() ==> " + ex.Message + "\n" + ex.StackTrace);
            //    }
            //}).Start();
        }

        private static void SaveFeedMediaImageToChatDirectory(MessageDTO messageDTO, MessageModel srcModel)
        {
            try
            {
                if (messageDTO.MessageType == (int)MessageType.IMAGE_FILE_FROM_GALLERY || messageDTO.MessageType == (int)MessageType.IMAGE_FILE_FROM_CAMERA)
                {
                    string srcFilePath = String.Empty;
                    string srcPreviewPath = String.Empty;
                    if (srcModel != null)
                    {
                        srcFilePath = ChatHelpers.GetChatFilePath(srcModel.Message, srcModel.MessageType, srcModel.PacketID, srcModel.LinkImageUrl, srcModel.SenderTableID);
                        srcPreviewPath = ChatHelpers.GetChatPreviewPath(srcModel.Message, srcModel.MessageType, srcModel.PacketID, srcModel.LinkImageUrl, srcModel.Latitude, srcModel.Longitude, ChatHelpers.GetShareContactUrl(srcModel.SharedContact));
                    }
                    else
                    {
                        srcFilePath = @RingIDSettings.TEMP_BOOK_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + HelperMethods.ConvertUrlToName(messageDTO.Message, ImageUtility.IMG_FULL);
                        srcPreviewPath = @RingIDSettings.TEMP_BOOK_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + HelperMethods.ConvertUrlToName(messageDTO.Message, ImageUtility.IMG_600);
                    }

                    string dstFilePath = ChatHelpers.GetChatFilePath(messageDTO.Message, (MessageType)messageDTO.MessageType, messageDTO.PacketID, messageDTO.LinkImageUrl, messageDTO.SenderTableID);
                    string dstPreviewPath = ChatHelpers.GetChatPreviewPath(messageDTO.Message, (MessageType)messageDTO.MessageType, messageDTO.PacketID, messageDTO.LinkImageUrl, messageDTO.Latitude, messageDTO.Longitude, messageDTO.SharedProfileImage);

                    if (!String.IsNullOrWhiteSpace(srcFilePath) && File.Exists(srcFilePath))
                    {
                        try { File.Copy(srcFilePath, dstPreviewPath); }
                        catch { }
                        try { File.Copy(srcFilePath, dstFilePath); }
                        catch { }
                    }
                    else if (!String.IsNullOrWhiteSpace(srcPreviewPath) && File.Exists(srcPreviewPath))
                    {
                        try { File.Copy(srcPreviewPath, dstPreviewPath); }
                        catch { }
                    }
                }
                else if (messageDTO.MessageType == (int)MessageType.VIDEO_FILE)
                {
                    string srcFilePath = ChatHelpers.GetChatFilePath(srcModel.Message, srcModel.MessageType, srcModel.PacketID, srcModel.LinkImageUrl, srcModel.SenderTableID);
                    string srcPreviewPath = ChatHelpers.GetChatPreviewPath(srcModel.Message, srcModel.MessageType, srcModel.PacketID, srcModel.LinkImageUrl, srcModel.Latitude, srcModel.Longitude, ChatHelpers.GetShareContactUrl(srcModel.SharedContact));

                    string dstFilePath = ChatHelpers.GetChatFilePath(messageDTO.Message, (MessageType)messageDTO.MessageType, messageDTO.PacketID, messageDTO.LinkImageUrl, messageDTO.SenderTableID);
                    string dstPreviewPath = ChatHelpers.GetChatPreviewPath(messageDTO.Message, (MessageType)messageDTO.MessageType, messageDTO.PacketID, messageDTO.LinkImageUrl, messageDTO.Latitude, messageDTO.Longitude, messageDTO.SharedProfileImage);

                    if (!String.IsNullOrWhiteSpace(srcFilePath) && File.Exists(srcFilePath))
                    {
                        try { File.Copy(srcFilePath, dstFilePath); }
                        catch { }
                    }
                    if (!String.IsNullOrWhiteSpace(srcPreviewPath) && File.Exists(srcPreviewPath))
                    {
                        try { File.Copy(srcPreviewPath, dstPreviewPath); }
                        catch { }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: SaveFeedMediaImageToChatDirectory() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static bool HideAlreadyRunningRecorder(bool forcefullyHide = false)
        {
            try
            {
                if (forcefullyHide == false && UCChatAudioRecorder.Instance != null && UCChatAudioRecorder.Instance.State != RecorderConstants.STATE_READY)
                {
                    UIHelperMethods.ShowWarning(NotificationMessages.ANOTHER_RECORDING_RUNNING);
                    // CustomMessageBox.ShowError(NotificationMessages.ANOTHER_RECORDING_RUNNING);
                    return false;
                }
                else
                {
                    if (UCChatAudioRecorder.Instance != null && UCChatAudioRecorder.Instance.FriendBasicInfoModel != null)
                    {
                        UCFriendChatCallPanel ucFriendChatCall = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(UCChatAudioRecorder.Instance.FriendBasicInfoModel.ShortInfoModel.UserTableID);
                        if (ucFriendChatCall != null)
                        {
                            ucFriendChatCall.HideAudioRecord();
                        }
                    }
                    if (UCChatAudioRecorder.Instance != null && UCChatAudioRecorder.Instance.GroupInfoModel != null)
                    {
                        UCGroupChatCallPanel ucGroupChatCall = UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY.TryGetValue(UCChatAudioRecorder.Instance.GroupInfoModel.GroupID);
                        if (ucGroupChatCall != null)
                        {
                            ucGroupChatCall.HideAudioRecord();
                        }
                    }
                    if (UCChatAudioRecorder.Instance != null && UCChatAudioRecorder.Instance.RoomModel != null)
                    {
                        UCRoomChatCallPanel ucRoomChatCall = UIDictionaries.Instance.ROOM_CHAT_CALL_DICTIONARY.TryGetValue(UCChatAudioRecorder.Instance.RoomModel.RoomID);
                        if (ucRoomChatCall != null)
                        {
                            ucRoomChatCall.HideAudioRecord();
                        }
                    }
                }

                if (forcefullyHide == false && UCChatVideoRecorder.Instance != null && UCChatVideoRecorder.Instance.State != RecorderConstants.STATE_READY)
                {
                    //CustomMessageBox.ShowError(NotificationMessages.ANOTHER_RECORDING_RUNNING);
                    UIHelperMethods.ShowWarning(NotificationMessages.ANOTHER_RECORDING_RUNNING);
                    return false;
                }
                else
                {
                    if (UCChatVideoRecorder.Instance != null && UCChatVideoRecorder.Instance.FriendBasicInfoModel != null)
                    {
                        UCFriendChatCallPanel ucFriendChatCall = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(UCChatVideoRecorder.Instance.FriendBasicInfoModel.ShortInfoModel.UserTableID);
                        if (ucFriendChatCall != null)
                        {
                            ucFriendChatCall.HideVideoRecord();
                        }
                    }
                    if (UCChatVideoRecorder.Instance != null && UCChatVideoRecorder.Instance.GroupInfoModel != null)
                    {
                        UCGroupChatCallPanel ucGroupChatCall = UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY.TryGetValue(UCChatVideoRecorder.Instance.GroupInfoModel.GroupID);
                        if (ucGroupChatCall != null)
                        {
                            ucGroupChatCall.HideVideoRecord();
                        }
                    }
                    if (UCChatAudioRecorder.Instance != null && UCChatAudioRecorder.Instance.RoomModel != null)
                    {
                        UCRoomChatCallPanel ucRoomChatCall = UIDictionaries.Instance.ROOM_CHAT_CALL_DICTIONARY.TryGetValue(UCChatAudioRecorder.Instance.RoomModel.RoomID);
                        if (ucRoomChatCall != null)
                        {
                            ucRoomChatCall.HideVideoRecord();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: HideAlreadyRunningRecorder() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            return true;
        }

        public static bool ForcefullyStopSecretTimer()
        {
            bool flag = false;
            try
            {
                List<MessageModel> modelList = new List<MessageModel>();
                lock (RingIDViewModel.Instance.RecentModelList)
                {
                    foreach (KeyValuePair<string, ObservableCollection<RecentModel>> pair in RingIDViewModel.Instance.RecentModelList.ToArray())
                    {
                        List<MessageModel> tempList = pair.Value.Where(P =>
                            P.Message != null
                            && (P.Message.ViewModel.IsMediaMessage || P.Message.IsSecretChat)
                            ).Select(P => P.Message).ToList();
                        modelList.AddRange(tempList);
                    }
                }

                if (modelList.Count > 0)
                {
                    flag = true;
                }

                foreach (MessageModel model in modelList)
                {
                    model.ViewModel.ForceStopSecretTimer = true;
                }
                modelList.Clear();
            }
            catch (Exception ex)
            {
                log.Error("Error: ForcefullyStopSecretTimer() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            return flag;
        }

        public static void ForcePauseMediaPlayer(string escapePacketID = null)
        {
            try
            {
                List<MessageModel> modelList = new List<MessageModel>();
                lock (RingIDViewModel.Instance.RecentModelList)
                {
                    foreach (KeyValuePair<string, ObservableCollection<RecentModel>> pair in RingIDViewModel.Instance.RecentModelList.ToArray())
                    {
                        List<MessageModel> tempList = pair.Value.Where(P =>
                            P.Message != null
                            && P.Message.ViewModel.IsMediaMessage
                            && P.Message.IsSecretChat == false
                            && P.Message.MultiMediaState == StatusConstants.MEDIA_PLAY_STATE
                            ).Select(P => P.Message).ToList();
                        modelList.AddRange(tempList);
                    }
                }

                foreach (MessageModel model in modelList)
                {
                    model.MultiMediaState = StatusConstants.MEDIA_PAUSE_STATE;
                }
                modelList.Clear();
            }
            catch (Exception ex)
            {
                log.Error("Error: ForcePauseMediaPlayer() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static bool IsDontDisturbMood()
        {
            return RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.Mood == StatusConstants.MOOD_DONT_DISTURB;
        }

        public static List<long> GetGroupMemberIDList(long groupId, GroupInfoModel groupInfoModel)
        {
            groupInfoModel = groupInfoModel == null ? RingIDViewModel.Instance.GetGroupInfoModelByGroupID(groupId) : groupInfoModel;
            List<long> memberList = groupInfoModel.MemberInfoDictionary.Values.Where(P => P.MemberAccessType != ChatConstants.MEMBER_TYPE_NOT_MEMBER).Select(P => P.UserTableID).ToList();
            return memberList;
        }

        public static int GetGroupMemberCount(GroupInfoModel groupInfoModel)
        {
            return groupInfoModel.MemberInfoDictionary.Values.Where(P => P.MemberAccessType != ChatConstants.MEMBER_TYPE_NOT_MEMBER).ToList().Count;
        }

        public static string GetShareContactUrl(UserBasicInfoModel model)
        {
            return (model != null ? model.ShortInfoModel.ProfileImage : "");
        }

        public static string GetChatFilePath(string message, MessageType messageType, string packetID, string imageUrl, long userTableId)
        {
            string filePath = String.Empty;
            switch (messageType)
            {
                case MessageType.IMAGE_FILE_FROM_GALLERY:
                case MessageType.IMAGE_FILE_FROM_CAMERA:
                    filePath = @RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER
                        + System.IO.Path.DirectorySeparatorChar
                        + packetID + ".jpg";
                    break;
                case MessageType.VIDEO_FILE:
                    filePath = @RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER
                        + System.IO.Path.DirectorySeparatorChar
                        + packetID + ".mp4";
                    break;
                case MessageType.AUDIO_FILE:
                    filePath = @RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER
                        + System.IO.Path.DirectorySeparatorChar
                        + packetID + ".mp3";
                    break;
                case MessageType.FILE_STREAM:
                    if (userTableId != DefaultSettings.LOGIN_TABLE_ID)
                    {
                        string fn = Path.GetFileNameWithoutExtension(message);
                        filePath = @RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER
                            + System.IO.Path.DirectorySeparatorChar
                            + (fn.Length > 100 ? fn.Substring(0, 100) : fn)
                            + "_"
                            + packetID
                            + "_"
                            + DefaultSettings.LOGIN_RING_ID
                            + Path.GetExtension(message);
                    }
                    else
                    {
                        filePath = message;
                    }
                    break;
            }
            return filePath;
        }

        public static string GetChatPreviewPath(string message, MessageType messageType, string packetID, string imageUrl, float lat, float lng, string profileImage)
        {
            string filePath = String.Empty;
            switch (messageType)
            {
                case MessageType.DOWNLOAD_STICKER_MESSAGE:
                    filePath = @RingIDSettings.STICKER_FOLDER
                        + Path.DirectorySeparatorChar
                        + message.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
                    break;
                case MessageType.IMAGE_FILE_FROM_GALLERY:
                case MessageType.IMAGE_FILE_FROM_CAMERA:
                    filePath = @RingIDSettings.TEMP_CHAT_FILES_FOLDER
                        + System.IO.Path.DirectorySeparatorChar
                        + packetID + ".jpg";
                    break;
                case MessageType.VIDEO_FILE:
                    filePath = @RingIDSettings.TEMP_CHAT_FILES_FOLDER
                        + System.IO.Path.DirectorySeparatorChar
                        + packetID + ".jpg";
                    break;
                case MessageType.LINK_MESSAGE:
                    int idx = imageUrl.LastIndexOf('?');
                    if (idx > 0)
                    {
                        imageUrl = imageUrl.Substring(0, idx);
                    }
                    filePath = @RingIDSettings.TEMP_CHAT_FILES_FOLDER
                        + System.IO.Path.DirectorySeparatorChar
                        + Regex.Replace(imageUrl, "[^a-zA-Z0-9.-]", "_");
                    break;
                case MessageType.LOCATION_MESSAGE:
                    filePath = @RingIDSettings.TEMP_CHAT_FILES_FOLDER
                        + System.IO.Path.DirectorySeparatorChar
                        + lat.ToString() + "_" + lng.ToString() + ".jpg";
                    break;
                case MessageType.RING_MEDIA_MESSAGE:
                    if (!String.IsNullOrWhiteSpace(imageUrl))
                    {
                        if (!(filePath.EndsWith(".jpg") || filePath.EndsWith(".png")))
                            filePath = filePath + ".jpg";
                        filePath = @RingIDSettings.TEMP_MEDIA_IMAGES_FOLDER
                            + System.IO.Path.DirectorySeparatorChar
                            + imageUrl.Replace('/', '_');
                    }
                    break;
                case MessageType.CONTACT_SHARE:
                    filePath = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER
                        + Path.DirectorySeparatorChar
                        + HelperMethods.ConvertUrlToName(profileImage, ImageUtility.IMG_THUMB);
                    break;
            }
            return filePath;
        }

        public static string GetChatFileDeleteFile(string message, MessageType messageType, string packetID, long userTableID)
        {
            string filePath = String.Empty;
            switch (messageType)
            {
                case MessageType.IMAGE_FILE_FROM_GALLERY:
                case MessageType.IMAGE_FILE_FROM_CAMERA:
                case MessageType.VIDEO_FILE:
                    filePath = @RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER
                        + System.IO.Path.DirectorySeparatorChar
                        + packetID + Path.GetExtension(message);
                    break;
                case MessageType.AUDIO_FILE:
                    filePath = @RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER
                        + System.IO.Path.DirectorySeparatorChar
                        + packetID + ".mp3";
                    break;
                case MessageType.FILE_STREAM:
                    if (userTableID != DefaultSettings.LOGIN_TABLE_ID)
                    {
                        string fn = Path.GetFileNameWithoutExtension(message);
                        filePath = @RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER
                            + System.IO.Path.DirectorySeparatorChar
                            + (fn.Length > 100 ? fn.Substring(0, 100) : fn)
                            + "_"
                            + packetID
                            + "_"
                            + DefaultSettings.LOGIN_RING_ID
                            + Path.GetExtension(message);
                    }
                    break;
            }
            return filePath;
        }

        public static string GetChatPreviewDeletePath(string message, MessageType messageType, string packetID)
        {
            string filePath = String.Empty;
            switch (messageType)
            {
                case MessageType.IMAGE_FILE_FROM_GALLERY:
                case MessageType.IMAGE_FILE_FROM_CAMERA:
                    filePath = @RingIDSettings.TEMP_CHAT_FILES_FOLDER
                        + System.IO.Path.DirectorySeparatorChar
                        + packetID + Path.GetExtension(message);
                    break;
                case MessageType.VIDEO_FILE:
                    filePath = @RingIDSettings.TEMP_CHAT_FILES_FOLDER
                        + System.IO.Path.DirectorySeparatorChar
                        + packetID + ".jpg";
                    break;
            }
            return filePath;
        }

        public static void UpdateFriendHistoryTime(long friendTableId, string minPacketID, string maxPacketID, UserBasicInfoModel model = null)
        {
            bool hasMin = !String.IsNullOrWhiteSpace(minPacketID);
            bool hasMax = !String.IsNullOrWhiteSpace(maxPacketID);

            if (hasMin || hasMax)
            {
                model = model == null ? RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(friendTableId) : model;
                if (model == null) return;

                //if (hasMin)
                //{
                //    long currMinTime = PacketIDGenerator.GetUnixTimestamp(minPacketID);
                //    bool validCurrMinTime = (currMinTime / 100000000000000000) < 3;

                //    if (String.IsNullOrWhiteSpace(model.ChatMinPacketID))
                //    {
                //        if (validCurrMinTime)
                //        {
                //            model.ChatMinPacketID = minPacketID;
                //        }
                //    }
                //    else
                //    {
                //        long prevMinTime = PacketIDGenerator.GetUnixTimestamp(model.ChatMinPacketID);
                //        bool validPrevMinTime = (prevMinTime / 100000000000000000) < 3;

                //        if (validCurrMinTime && (validPrevMinTime == false || currMinTime <= prevMinTime))
                //        {
                //            model.ChatMinPacketID = minPacketID;
                //        }
                //    }
                //}

                if (hasMin && (String.IsNullOrWhiteSpace(model.ChatMinPacketID) || PacketIDGenerator.GetUnixTimestamp(minPacketID) <= PacketIDGenerator.GetUnixTimestamp(model.ChatMinPacketID)))
                {
                    model.ChatMinPacketID = minPacketID;
                }

                //if (hasMax)
                //{
                //    long currMaxTime = PacketIDGenerator.GetUnixTimestamp(maxPacketID);
                //    bool validCurrMaxTime = (currMaxTime / 100000000000000000) < 3;

                //    if (String.IsNullOrWhiteSpace(model.ChatMaxPacketID))
                //    {
                //        if (validCurrMaxTime)
                //        {
                //            model.ChatMaxPacketID = maxPacketID;
                //        }
                //    }
                //    else
                //    {
                //        long prevMaxTime = PacketIDGenerator.GetUnixTimestamp(model.ChatMaxPacketID);
                //        bool validPrevMaxTime = (prevMaxTime / 100000000000000000) < 3;

                //        if (validCurrMaxTime && (validPrevMaxTime == false || currMaxTime >= prevMaxTime))
                //        {
                //            model.ChatMaxPacketID = maxPacketID;
                //        }
                //    }
                //}

                if (hasMax && (String.IsNullOrWhiteSpace(model.ChatMaxPacketID) || PacketIDGenerator.GetUnixTimestamp(maxPacketID) >= PacketIDGenerator.GetUnixTimestamp(model.ChatMaxPacketID)))
                {
                    model.ChatMaxPacketID = maxPacketID;
                }

                UserBasicInfoDTO dto = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(friendTableId);
                if (dto != null)
                {
                    dto.ChatMinPacketID = model.ChatMinPacketID;
                    dto.ChatMaxPacketID = model.ChatMaxPacketID;
                }

                ContactListDAO.UpdateChatHistoryTime(friendTableId, dto.ChatMinPacketID, dto.ChatMaxPacketID);

            }
        }

        public static void UpdateGroupHistoryTime(long groupID, string minPacketID, string maxPacketID, GroupInfoModel model = null)
        {
            bool hasMin = !String.IsNullOrWhiteSpace(minPacketID);
            bool hasMax = !String.IsNullOrWhiteSpace(maxPacketID);

            if (hasMin || hasMax)
            {
                model = model == null ? RingIDViewModel.Instance.GroupList.TryGetValue(groupID) : model;
                if (model == null) return;

                //if (hasMin)
                //{
                //    long currMinTime = PacketIDGenerator.GetUnixTimestamp(minPacketID);
                //    bool validCurrMinTime = (currMinTime / 100000000000000000) < 3;

                //    if (String.IsNullOrWhiteSpace(model.ChatMinPacketID))
                //    {
                //        if (validCurrMinTime)
                //        {
                //            model.ChatMinPacketID = minPacketID;
                //        }
                //    }
                //    else
                //    {
                //        long prevMinTime = PacketIDGenerator.GetUnixTimestamp(model.ChatMinPacketID);
                //        bool validPrevMinTime = (prevMinTime / 100000000000000000) < 3;

                //        if (validCurrMinTime && (validPrevMinTime == false || currMinTime <= prevMinTime))
                //        {
                //            model.ChatMinPacketID = minPacketID;
                //        }
                //    }
                //}

                if (hasMin && (String.IsNullOrWhiteSpace(model.ChatMinPacketID) || PacketIDGenerator.GetUnixTimestamp(minPacketID) <= PacketIDGenerator.GetUnixTimestamp(model.ChatMinPacketID)))
                {
                    model.ChatMinPacketID = minPacketID;
                }

                //if (hasMax)
                //{
                //    long currMaxTime = PacketIDGenerator.GetUnixTimestamp(maxPacketID);
                //    bool validCurrMaxTime = (currMaxTime / 100000000000000000) < 3;

                //    if (String.IsNullOrWhiteSpace(model.ChatMaxPacketID))
                //    {
                //        if (validCurrMaxTime)
                //        {
                //            model.ChatMaxPacketID = maxPacketID;
                //        }
                //    }
                //    else
                //    {
                //        long prevMaxTime = PacketIDGenerator.GetUnixTimestamp(model.ChatMaxPacketID);
                //        bool validPrevMaxTime = (prevMaxTime / 100000000000000000) < 3;

                //        if (validCurrMaxTime && (validPrevMaxTime == false || currMaxTime >= prevMaxTime))
                //        {
                //            model.ChatMaxPacketID = maxPacketID;
                //        }
                //    }
                //}

                if (hasMax && (String.IsNullOrWhiteSpace(model.ChatMaxPacketID) || PacketIDGenerator.GetUnixTimestamp(maxPacketID) >= PacketIDGenerator.GetUnixTimestamp(model.ChatMaxPacketID)))
                {
                    model.ChatMaxPacketID = maxPacketID;
                }

                GroupInfoDAO.UpdateChatHistoryTime(groupID, model.ChatMinPacketID, model.ChatMaxPacketID);
            }
        }

        public static void UpdateRoomHistoryTime(string roomID, string minPacketID, string maxPacketID, RoomModel model = null)
        {
            bool hasMin = !String.IsNullOrWhiteSpace(minPacketID);
            bool hasMax = !String.IsNullOrWhiteSpace(maxPacketID);

            if (hasMin || hasMax)
            {
                model = RingIDViewModel.Instance.RoomList.TryGetValue(roomID);
                if (model == null) return;

                //if (hasMin)
                //{
                //    long currMinTime = PacketIDGenerator.GetUnixTimestamp(minPacketID);
                //    bool validCurrMinTime = (currMinTime / 100000000000000000) < 3;

                //    if (String.IsNullOrWhiteSpace(model.ChatMinPacketID))
                //    {
                //        if (validCurrMinTime)
                //        {
                //            model.ChatMinPacketID = minPacketID;
                //        }
                //    }
                //    else
                //    {
                //        long prevMinTime = PacketIDGenerator.GetUnixTimestamp(model.ChatMinPacketID);
                //        bool validPrevMinTime = (prevMinTime / 100000000000000000) < 3;

                //        if (validCurrMinTime && (validPrevMinTime == false || currMinTime <= prevMinTime))
                //        {
                //            model.ChatMinPacketID = minPacketID;
                //        }
                //    }
                //}

                if (hasMin && (String.IsNullOrWhiteSpace(model.ChatMinPacketID) || PacketIDGenerator.GetUnixTimestamp(minPacketID) <= PacketIDGenerator.GetUnixTimestamp(model.ChatMinPacketID)))
                {
                    model.ChatMinPacketID = minPacketID;
                }

                //if (hasMax)
                //{
                //    long currMaxTime = PacketIDGenerator.GetUnixTimestamp(maxPacketID);
                //    bool validCurrMaxTime = (currMaxTime / 100000000000000000) < 3;

                //    if (String.IsNullOrWhiteSpace(model.ChatMaxPacketID))
                //    {
                //        if (validCurrMaxTime)
                //        {
                //            model.ChatMaxPacketID = maxPacketID;
                //        }
                //    }
                //    else
                //    {
                //        long prevMaxTime = PacketIDGenerator.GetUnixTimestamp(model.ChatMaxPacketID);
                //        bool validPrevMaxTime = (prevMaxTime / 100000000000000000) < 3;

                //        if (validCurrMaxTime && (validPrevMaxTime == false || currMaxTime >= prevMaxTime))
                //        {
                //            model.ChatMaxPacketID = maxPacketID;
                //        }
                //    }
                //}

                if (hasMax && (String.IsNullOrWhiteSpace(model.ChatMaxPacketID) || PacketIDGenerator.GetUnixTimestamp(maxPacketID) >= PacketIDGenerator.GetUnixTimestamp(model.ChatMaxPacketID)))
                {
                    model.ChatMaxPacketID = maxPacketID;
                }
            }
        }

        public static string SaveChatBackgroundFromDirectory(Bitmap originalBitmap)
        {
            string imageName = string.Empty;
            try
            {
                imageName = DefaultSettings.LOGIN_RING_ID + "_" + ChatService.GetServerTime() + ".jpg";
                Bitmap thumbBitmap = ImageUtility.ResizeBitmap(originalBitmap, 100, (int)((double)100 / ((double)originalBitmap.Width / originalBitmap.Height)));
                string originalPath = RingIDSettings.TEMP_CHAT_BACKGROUND_FOLDER + System.IO.Path.DirectorySeparatorChar + imageName;
                string thumbPath = RingIDSettings.TEMP_CHAT_BACKGROUND_FOLDER + System.IO.Path.DirectorySeparatorChar + "thumb" + imageName;
                originalBitmap.Save(originalPath, ImageFormat.Jpeg);
                thumbBitmap.Save(thumbPath, ImageFormat.Jpeg);
            }
            catch (Exception ex)
            {
                log.Error("Error: SaveChatBackgroundFromDirectory() => " + ex.Message + "\n" + ex.StackTrace);
                return null;
            }
            return imageName;
        }

        public static void FriendChatMessageEdit(MessageModel messageModel, string msg, int msgType)
        {

            UserBasicInfoModel friendInfoModel = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(messageModel.FriendTableID);

            int anonymousSettingName = -1;
            if (!HelperMethods.HasAnonymousChatPermission(friendInfoModel))
            {
                if (HelperMethods.ShowAnonymousWarning() == MessageBoxResult.Yes)
                {
                    return;
                }
                else
                {
                    anonymousSettingName = StatusConstants.ANONYMOUS_CHAT;
                }
            }

            HelperMethods.ChangeAnonymousSettingsWrapper((result) =>
            {
                if (result == false) return 0;

                int accessType = -1;
                BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(friendInfoModel.ShortInfoModel.UserTableID);

                if (!HelperMethods.HasFriendChatPermission(friendInfoModel, blockModel, out accessType))
                {
                    if (HelperMethods.ShowBlockWarning(friendInfoModel, accessType))
                    {
                        messageModel.ViewModel.IsEditMode = false;
                        messageModel.ViewModel.LoadViewType(messageModel);
                        return 0;
                    }
                }

                HelperMethods.ChangeBlockUnblockSettingsWrapper(accessType, friendInfoModel, blockModel, (status) =>
                {
                    if (status == false)
                    {
                        messageModel.ViewModel.IsEditMode = false;
                        messageModel.ViewModel.LoadViewType(messageModel);
                        return 0;
                    }

                    ChatHelpers.ChatMessageEdit(messageModel, msg, msgType, friendInfoModel, blockModel);

                    return 1;
                });

                return 0;
            }, anonymousSettingName);
        }

        public static void ChatMessageEdit(MessageModel messageModel, string msg, int msgType, UserBasicInfoModel friendInfoModel = null, BlockedNonFriendModel blockModel = null)
        {
            try
            {
                string contactID = string.Empty;
                MessageDTO messageDTO = new MessageDTO();
                messageDTO.FriendTableID = messageModel.FriendTableID;
                messageDTO.GroupID = messageModel.GroupID;
                messageDTO.RoomID = messageModel.RoomID;
                messageDTO.SenderTableID = messageModel.SenderTableID;
                messageDTO.PacketID = messageModel.PacketID;
                messageDTO.MessageType = msgType;
                messageDTO.OriginalMessage = msg;
                messageDTO.MessageDate = messageModel.MessageDate;
                messageDTO.FullName = messageModel.FullName;
                messageDTO.ProfileImage = messageModel.ProfileImage;
                messageDTO.Status = ChatConstants.STATUS_SENDING;

                if (messageDTO.FriendTableID > 0)
                {
                    contactID = messageDTO.FriendTableID.ToString();
                    messageDTO.PacketType = ChatConstants.PACKET_TYPE_FRIEND_MESSAGE_EDIT;

                    UCFriendChatCallPanel ucFriendChatCall = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(messageDTO.FriendTableID);
                    if (ucFriendChatCall.IsSecretCheckOn && ucFriendChatCall.SecretTimerValue > 0)
                    {
                        messageDTO.Timeout = ucFriendChatCall.SecretTimerValue;
                        messageDTO.IsSecretVisible = ucFriendChatCall.FriendBasicInfoModel.ShortInfoModel.IsSecretVisible;
                    }
                }
                else if (messageDTO.GroupID > 0)
                {
                    contactID = messageDTO.GroupID.ToString();
                    messageDTO.PacketType = ChatConstants.PACKET_TYPE_GROUP_MESSAGE_EDIT;
                }
                else if (!String.IsNullOrWhiteSpace(messageDTO.RoomID))
                {
                    contactID = messageDTO.RoomID;
                    messageDTO.PacketType = ChatConstants.PACKET_TYPE_ROOM_MESSAGE_EDIT;
                }
                else
                {
                    return;
                }

                ChatJSONParser.ParseMessage(messageDTO);
                messageModel.ViewModel.IsEditMode = false;
                messageModel.LoadData(messageDTO);

                RecentModel model = RingIDViewModel.Instance.ChatLogModelsList.Where(P => P.ContactID.Equals(contactID) && P.UniqueKey == messageDTO.PacketID).FirstOrDefault();
                if (model != null)
                {
                    model.Message.MessageType = (MessageType)messageDTO.MessageType;
                    model.Message.Message = messageDTO.Message;
                }

                string link = String.Empty;
                var matches = Regex.Matches(messageDTO.OriginalMessage, DefaultSettings.ONLY_HTTPS_HYPER_LINK_EXPRESSION);
                foreach (Match match in matches)
                {
                    if (!String.IsNullOrWhiteSpace(match.Value))
                    {
                        link = match.Value.ToString();
                        break;
                    }
                }

                if (!String.IsNullOrWhiteSpace(link))
                {
                    new ChatLinkEditProcessor(messageDTO.FriendTableID, messageDTO.GroupID, messageDTO.RoomID, link, messageDTO).Start();
                }
                else
                {
                    RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                    if (messageDTO.FriendTableID > 0)
                    {
                        ChatService.EditFriendChat(messageDTO.PacketID, messageDTO.FriendTableID, messageDTO.MessageType, messageDTO.Timeout, messageDTO.OriginalMessage, messageDTO.MessageDate, messageDTO.IsSecretVisible, friendInfoModel, blockModel);
                    }
                    else if (messageDTO.GroupID > 0)
                    {
                        ChatService.EditGroupChat(messageDTO.PacketID, messageDTO.GroupID, messageDTO.MessageType, messageDTO.OriginalMessage, messageDTO.MessageDate);
                    }
                    else
                    {
                        ChatService.EditPublicRoomChat(messageDTO.PacketID, messageDTO.RoomID, messageDTO.MessageType, messageDTO.OriginalMessage, messageDTO.MessageDate, messageDTO.FullName, messageDTO.ProfileImage);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChatMessageEdit() => " + ex.Message + "\n" + ex.StackTrace);
            }

        }

        public static void SendLikeUnlike(MessageModel model)
        {
            try
            {
                if (model.ViewModel.IsProcessing)
                {
                    return;
                }

                model.ViewModel.IsProcessing = true;
                model.ILike = !model.ILike;
                if (model.ILike)
                {
                    model.LikeCount++;
                }
                else
                {
                    model.LikeCount--;
                }


                ChatService.LikeUnlikePublicRoomChat(model.PacketID, model.RoomID, model.SenderTableID, model.ILike, (args) =>
                {
                    if (!args.Status)
                    {
                        model.ILike = !model.ILike;
                        if (model.ILike)
                        {
                            model.LikeCount++;
                        }
                        else
                        {
                            model.LikeCount--;
                        }
                    }
                    model.ViewModel.IsProcessing = false;
                });
            }
            catch (Exception ex)
            {
                log.Error("Error at SendLikeUnlike() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static string ConvertLatLongToDMS(float lat, float lon)
        {
            string latDir = (lat >= 0 ? "N" : "S");
            lat = Math.Abs(lat);
            double latMinPart = ((lat - Math.Truncate(lat) / 1) * 60);
            double latSecPart = ((latMinPart - Math.Truncate(latMinPart) / 1) * 60);

            string lonDir = (lon >= 0 ? "E" : "W");
            lon = Math.Abs(lon);
            double lonMinPart = ((lon - Math.Truncate(lon) / 1) * 60);
            double lonSecPart = ((lonMinPart - Math.Truncate(lonMinPart) / 1) * 60);
            return (Math.Truncate(lat) + "° " + Math.Truncate(latMinPart) + "' " + Math.Truncate(latSecPart) + "'' " + latDir).ToString()
                    + "  " + (Math.Truncate(lon) + "° " + Math.Truncate(lonMinPart) + "' " + Math.Truncate(lonSecPart) + "'' " + lonDir).ToString();
        }

        public static void UpdateAllHistory(long maxTime)
        {
            try
            {
                ConcurrentDictionary<string, ConcurrentDictionary<string, int>> uniqueKeysGroupByContactIDs = RecentChatCallActivityDAO.GetUniqueKeysGroupByContactIDs(maxTime);
                RecentChatCallActivityDAO.DeleteCallChatHistoryByDate(maxTime);

                foreach (UCFriendChatCallPanel friendPanel in UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.Values)
                {
                    List<RecentModel> modelList = friendPanel.ChatScrollViewer.RecentModelList.Where(P => P.Time < maxTime && !String.IsNullOrWhiteSpace(P.UniqueKey)).ToList();
                    List<string> packetList = modelList.Select(P => P.UniqueKey).ToList();

                    foreach (RecentModel temp in modelList)
                    {
                        if (temp.Message != null)
                        {
                            string filePath = ChatHelpers.GetChatFileDeleteFile(temp.Message.Message, temp.Message.PrevMessageType, temp.Message.PacketID, temp.Message.SenderTableID);
                            if (!String.IsNullOrWhiteSpace(filePath) && System.IO.File.Exists(filePath))
                            {
                                try { System.IO.File.Delete(filePath); }
                                catch { }
                            }

                            filePath = ChatHelpers.GetChatPreviewDeletePath(temp.Message.Message, temp.Message.PrevMessageType, temp.Message.PacketID);
                            if (!String.IsNullOrWhiteSpace(filePath) && System.IO.File.Exists(filePath))
                            {
                                try { System.IO.File.Delete(filePath); }
                                catch { }
                            }
                            temp.Message.MessageType = ChatConstants.TYPE_DELETE_MSG;
                        }
                    }

                    FileTransferSession.CancelTransferFileByPacketIDs(friendPanel._FriendTableID, null, false);
                }

                foreach (UCGroupChatCallPanel groupPanel in UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY.Values)
                {
                    List<RecentModel> modelList = groupPanel.ChatScrollViewer.RecentModelList.Where(P => P.Time < maxTime && !String.IsNullOrWhiteSpace(P.UniqueKey)).ToList();
                    List<string> packetList = modelList.Select(P => P.UniqueKey).ToList();

                    foreach (RecentModel temp in modelList)
                    {
                        if (temp.Message != null)
                        {
                            string filePath = ChatHelpers.GetChatFileDeleteFile(temp.Message.Message, temp.Message.PrevMessageType, temp.Message.PacketID, temp.Message.SenderTableID);
                            if (!String.IsNullOrWhiteSpace(filePath) && System.IO.File.Exists(filePath))
                            {
                                try { System.IO.File.Delete(filePath); }
                                catch { }
                            }

                            filePath = ChatHelpers.GetChatPreviewDeletePath(temp.Message.Message, temp.Message.PrevMessageType, temp.Message.PacketID);
                            if (!String.IsNullOrWhiteSpace(filePath) && System.IO.File.Exists(filePath))
                            {
                                try { System.IO.File.Delete(filePath); }
                                catch { }
                            }
                            temp.Message.MessageType = (int)MessageType.DELETE_MESSAGE;
                        }
                    }

                    FileTransferSession.CancelTransferFileByPacketIDs(groupPanel._GroupID, null, false);
                }

                List<string> chatKeys = ChatViewModel.Instance.UnreadChatList.Keys.ToList();
                foreach (string contactId in chatKeys)
                {
                    ObservableDictionary<string, long> unreadList = ChatViewModel.Instance.UnreadChatList.TryGetValue(contactId);
                    if (unreadList != null && unreadList.Count > 0)
                    {
                        List<string> packetIds = new List<string>();
                        foreach (KeyValuePair<string, long> pair in unreadList.ToArray())
                        {
                            if (pair.Value < maxTime)
                            {
                                packetIds.Add(pair.Key);
                            }
                        }
                        if (packetIds.Count > 0)
                        {
                            ChatHelpers.RemoveUnreadChat(contactId, packetIds, unreadList);
                        }
                    }
                }

                List<string> callKeys = ChatViewModel.Instance.UnreadCallList.Keys.ToList();
                foreach (string contactId in callKeys)
                {
                    ObservableDictionary<string, long> unreadList = ChatViewModel.Instance.UnreadCallList.TryGetValue(contactId);
                    if (unreadList != null && unreadList.Count > 0)
                    {
                        List<string> callIds = new List<string>();
                        foreach (KeyValuePair<string, long> pair in unreadList.ToArray())
                        {
                            if (pair.Value < maxTime)
                            {
                                callIds.Add(pair.Key);
                            }
                        }
                        if (callIds.Count > 0)
                        {
                            ChatHelpers.RemoveUnreadCall(contactId, callIds, unreadList);
                        }
                    }
                }

                bool hasChatLogUpdated = false;
                bool hasCallLogUpdated = false;

                foreach (KeyValuePair<string, ConcurrentDictionary<string, int>> uniqueKeys in uniqueKeysGroupByContactIDs)
                {
                    List<string> callIds = new List<string>();
                    bool hasChatHistory = false;
                    bool hasCallHistory = false;

                    foreach (KeyValuePair<string, int> tempKeys in uniqueKeys.Value)
                    {
                        switch (tempKeys.Value)
                        {
                            case ChatConstants.SUBTYPE_FRIEND_CHAT:
                            case ChatConstants.SUBTYPE_GROUP_CHAT:
                            case ChatConstants.SUBTYPE_ROOM_CHAT:
                                hasChatHistory = true;
                                break;
                            case ChatConstants.SUBTYPE_CALL_LOG:
                                callIds.Add(tempKeys.Key);
                                hasCallHistory = true;
                                break;
                            default:
                                break;
                        }
                    }

                    RecentLoadUtility.RemoveRange(uniqueKeys.Key, uniqueKeys.Value.Keys.ToList(), true);

                    if (hasChatHistory && UCChatLogPanel.Instance != null && UCChatLogPanel.Instance.LoadChatLogByContactID(uniqueKeys.Key))
                    {
                        hasChatLogUpdated = true;
                    }
                    if (hasCallHistory && UCCallLog.Instance != null)
                    {
                        hasCallLogUpdated = true;
                        CallLogLoadUtility.RemoveRange(uniqueKeys.Key, callIds);
                    }
                }

                if (hasChatLogUpdated)
                {
                    UCChatLogPanel.Instance._DB_OFFSET = RingIDViewModel.Instance.ChatLogModelsList.Count;
                    UCChatLogPanel.Instance.LoadNextChatLogs();
                }
                if (hasCallLogUpdated)
                {
                    UCCallLog.Instance.LoadNextCallLogs();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at UpdateAllHistory() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void UpdateStatusByInsertOrChange(long id, string packetId, MessageType messageType, long messageDate, long senderTableID, int status, LastStatusModel statusModel = null)
        {
            statusModel = statusModel == null ? ChatViewModel.Instance.GetLastStatusModel(id) : statusModel;

            if (senderTableID != DefaultSettings.LOGIN_TABLE_ID || messageType == MessageType.DELETE_MESSAGE)
                return;

            switch (status)
            {
                case ChatConstants.STATUS_DELIVERED:
                case ChatConstants.STATUS_SENT:
                    {
                        if (messageDate > statusModel.LastDeliveredDate && messageDate > statusModel.LastSeenDate)
                        {
                            statusModel.LastDeliveredDate = messageDate;
                            statusModel.DeliveredPacketID = packetId;
                        }
                    }
                    break;
                case ChatConstants.STATUS_SEEN:
                case ChatConstants.STATUS_VIEWED_PLAYED:
                case ChatConstants.STATUS_DISAPPEARED:
                    {
                        if (messageDate > statusModel.LastDeliveredDate)
                        {
                            statusModel.LastDeliveredDate = 0;
                            statusModel.DeliveredPacketID = null;
                        }
                        if (messageDate > statusModel.LastSeenDate)
                        {
                            statusModel.LastSeenDate = messageDate;
                            statusModel.SeenPacketID = packetId;
                        }
                    }
                    break;
            }
        }

        public static void UpdateStatusByRemove(object id, List<string> packetIds)
        {
            long contactId = id is long ? (long)id : 0;
            if (contactId > 0 || long.TryParse(id.ToString(), out contactId))
            {
                LastStatusModel statusModel = ChatViewModel.Instance.GetLastStatusModel(contactId);
                if ((!String.IsNullOrWhiteSpace(statusModel.DeliveredPacketID) && packetIds.Contains(statusModel.DeliveredPacketID)) || (!String.IsNullOrWhiteSpace(statusModel.SeenPacketID) && packetIds.Contains(statusModel.SeenPacketID)))
                {
                    UpdateStatusByRemove(contactId, statusModel);
                }
            }
        }

        public static void UpdateStatusByRemove(object id, string packetId)
        {
            long contactId = id is long ? (long)id : 0;
            if (contactId > 0 || long.TryParse(id.ToString(), out contactId))
            {
                LastStatusModel statusModel = ChatViewModel.Instance.GetLastStatusModel(contactId);
                if ((!String.IsNullOrWhiteSpace(statusModel.DeliveredPacketID) && packetId.Equals(statusModel.DeliveredPacketID)) || (!String.IsNullOrWhiteSpace(statusModel.SeenPacketID) && packetId.Equals(statusModel.SeenPacketID)))
                {
                    UpdateStatusByRemove(contactId, statusModel);
                }
            }
        }

        private static void UpdateStatusByRemove(long id, LastStatusModel statusModel)
        {
            List<RecentModel> recentList = RingIDViewModel.Instance.GetRecentModelListByID(id).Where(P => P.Message != null && !P.Message.FromFriend & P.Message.MessageType != MessageType.DELETE_MESSAGE).ToList();
            statusModel.Reset();

            for (int idx = recentList.Count - 1; idx >= 0; idx--)
            {

                RecentModel model = recentList[idx];
                switch (model.Message.Status)
                {
                    case ChatConstants.STATUS_DELIVERED:
                    case ChatConstants.STATUS_SENT:
                        {
                            if (model.Message.MessageDate > statusModel.LastDeliveredDate)
                            {
                                statusModel.LastDeliveredDate = model.Message.MessageDate;
                                statusModel.DeliveredPacketID = model.Message.PacketID;
                            }
                        }
                        break;
                    case ChatConstants.STATUS_SEEN:
                    case ChatConstants.STATUS_VIEWED_PLAYED:
                    case ChatConstants.STATUS_DISAPPEARED:
                        {
                            if (model.Message.MessageDate > statusModel.LastSeenDate)
                            {
                                statusModel.LastSeenDate = model.Message.MessageDate;
                                statusModel.SeenPacketID = model.Message.PacketID;
                                return;
                            }
                        }
                        break;
                }
            }
        }

        #endregion Common Method

        #region Chat Notification

        public static void AddUnreadChat(string id, string packetId, long time)
        {
            ObservableDictionary<string, long> list = ChatViewModel.Instance.GetUnreadChatByID(id);
            list[packetId] = time;

            int prevCount = ChatViewModel.Instance.UnreadChatContactList.Count;
            ChatViewModel.Instance.UnreadChatContactList[id.ToString()] = list.Count;
            int newCount = ChatViewModel.Instance.UnreadChatContactList.Count;

            if (prevCount != newCount)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    UIHelperMethods.ChangeTaskBarOverlay();
                });
            }
            FlashWindowHelper.StartFlashing();
        }

        public static void AddUnreadChat(string id, List<MessageDTO> msgList)
        {
            ObservableDictionary<string, long> list = ChatViewModel.Instance.GetUnreadChatByID(id);
            foreach (MessageDTO msg in msgList)
            {
                list[msg.PacketID] = msg.MessageDate;
            }

            int prevCount = ChatViewModel.Instance.UnreadChatContactList.Count;
            ChatViewModel.Instance.UnreadChatContactList[id.ToString()] = list.Count;
            int newCount = ChatViewModel.Instance.UnreadChatContactList.Count;

            if (prevCount != newCount)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    UIHelperMethods.ChangeTaskBarOverlay();
                });
            }
            FlashWindowHelper.StartFlashing();
        }

        public static bool RemoveUnreadChat(string id, string packetId, ObservableDictionary<string, long> unreadList = null)
        {
            bool status = false;
            unreadList = unreadList == null ? ChatViewModel.Instance.GetUnreadChatByID(id) : unreadList;

            if (unreadList.Count > 0)
            {
                status = unreadList.TryRemove(packetId);

                if (status)
                {
                    int prevCount = ChatViewModel.Instance.UnreadChatContactList.Count;
                    if (unreadList.Count > 0)
                    {
                        ChatViewModel.Instance.UnreadChatContactList[id.ToString()] = unreadList.Count;
                    }
                    else
                    {
                        ChatViewModel.Instance.UnreadChatContactList.TryRemove(id.ToString());
                    }
                    int newCount = ChatViewModel.Instance.UnreadChatContactList.Count;

                    if (prevCount != newCount)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            UIHelperMethods.ChangeTaskBarOverlay();
                        });
                    }
                }
            }
            return status;
        }

        public static bool RemoveUnreadChat(string id, List<string> packetIds, ObservableDictionary<string, long> unreadList = null)
        {
            bool status = false;
            unreadList = unreadList == null ? ChatViewModel.Instance.GetUnreadChatByID(id) : unreadList;

            if (unreadList.Count > 0)
            {
                foreach (string packetId in packetIds)
                {
                    status = unreadList.TryRemove(packetId) ? true : false;
                }

                if (status)
                {
                    int prevCount = ChatViewModel.Instance.UnreadChatContactList.Count;
                    if (unreadList.Count > 0)
                    {
                        ChatViewModel.Instance.UnreadChatContactList[id.ToString()] = unreadList.Count;
                    }
                    else
                    {
                        ChatViewModel.Instance.UnreadChatContactList.TryRemove(id.ToString());
                    }
                    int newCount = ChatViewModel.Instance.UnreadChatContactList.Count;

                    if (prevCount != newCount)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            UIHelperMethods.ChangeTaskBarOverlay();
                        });
                    }
                }
            }
            return status;
        }

        #endregion Chat Notification

        #region Call Notification

        public static void AddUnreadCall(string id, string callId, long time)
        {
            ObservableDictionary<string, long> list = ChatViewModel.Instance.GetUnreadCallByID(id);
            list[callId] = time;

            int prevCount = ChatViewModel.Instance.UnreadCallContactList.Count;
            ChatViewModel.Instance.UnreadCallContactList[id.ToString()] = list.Count;
            int newCount = ChatViewModel.Instance.UnreadCallContactList.Count;

            if (prevCount != newCount)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    UIHelperMethods.ChangeTaskBarOverlay();
                });
            }
            FlashWindowHelper.StartFlashing();
        }

        public static bool RemoveUnreadCall(string id, string callId, ObservableDictionary<string, long> unreadList = null)
        {
            bool status = false;
            unreadList = unreadList == null ? ChatViewModel.Instance.GetUnreadCallByID(id) : unreadList;

            if (unreadList.Count > 0)
            {
                status = unreadList.TryRemove(callId);

                if (status)
                {
                    int prevCount = ChatViewModel.Instance.UnreadCallContactList.Count;
                    if (unreadList.Count > 0)
                    {
                        ChatViewModel.Instance.UnreadCallContactList[id.ToString()] = unreadList.Count;
                    }
                    else
                    {
                        ChatViewModel.Instance.UnreadCallContactList.TryRemove(id.ToString());
                    }
                    int newCount = ChatViewModel.Instance.UnreadCallContactList.Count;

                    if (prevCount != newCount)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            UIHelperMethods.ChangeTaskBarOverlay();
                        });
                    }
                }
            }
            return status;
        }

        public static bool RemoveUnreadCall(string id, List<string> callIds, ObservableDictionary<string, long> unreadList = null)
        {
            bool status = false;
            unreadList = unreadList == null ? ChatViewModel.Instance.GetUnreadCallByID(id) : unreadList;

            if (unreadList.Count > 0)
            {
                foreach (string callId in callIds)
                {
                    status = unreadList.TryRemove(callId) ? true : false;
                }

                if (status)
                {
                    int prevCount = ChatViewModel.Instance.UnreadCallContactList.Count;
                    if (unreadList.Count > 0)
                    {
                        ChatViewModel.Instance.UnreadCallContactList[id.ToString()] = unreadList.Count;
                    }
                    else
                    {
                        ChatViewModel.Instance.UnreadCallContactList.TryRemove(id.ToString());
                    }
                    int newCount = ChatViewModel.Instance.UnreadCallContactList.Count;

                    if (prevCount != newCount)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            UIHelperMethods.ChangeTaskBarOverlay();
                        });
                    }
                }
            }
            return status;
        }

        #endregion Call Notification

        #region File Utility Method

        public static ChatFileDTO GetChatFileUploadResponse(byte[] recievedBuffer)
        {
            ChatFileDTO chatFileDTO = new ChatFileDTO();
            int totalRead = 0;
            chatFileDTO.Status = recievedBuffer[0];
            totalRead++;

            int msgLength = recievedBuffer[totalRead];
            totalRead++;
            if (chatFileDTO.Status == 1)
            {
                chatFileDTO.UploadUrl = Encoding.UTF8.GetString(recievedBuffer, totalRead, msgLength);
            }
            else
            {
                chatFileDTO.ErrorMsg = Encoding.UTF8.GetString(recievedBuffer, totalRead, msgLength);
            }
            return chatFileDTO;
        }

        public static byte[] ReadAllBytes(string fileName)
        {
            byte[] buffer = null;
            using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                buffer = new byte[fs.Length];
                fs.Read(buffer, 0, (int)fs.Length);
            }
            return buffer;
        }

        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return false;
        }

        public static string GetFileSizeInText(long fileSize)
        {
            string statusText = "0" + " " + FILE_SIZE_SUFFIX[0];
            try
            {
                if (fileSize == 0)
                {
                    return statusText;
                }

                long bytes = Math.Abs(fileSize);
                int place = System.Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
                double num = Math.Round(bytes / Math.Pow(1024, place), 1);
                statusText = (Math.Sign(fileSize) * num).ToString() + " " + FILE_SIZE_SUFFIX[place];
            }
            catch (Exception ex)
            {
                log.Error("Error: GetFileSizeInText() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            return statusText;
        }

        public static string GetShortStreamFileName(string message)
        {
            string extension = String.Empty;
            string fileName = String.Empty;

            try
            {
                if (!String.IsNullOrWhiteSpace(message))
                {
                    extension = Path.GetExtension(message);
                    fileName = Path.GetFileNameWithoutExtension(message);

                    if (fileName.Length > 25)
                    {
                        fileName = fileName.Substring(0, 15) + "..." + fileName.Substring(fileName.Length - 10, 10);
                    }
                }
                else
                {
                    return String.Empty;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: GetShortStreamFileName() ==> " + ex.Message + "\n" + ex.StackTrace);
                return String.Empty;
            }

            return fileName + extension;
        }

        public static string ConvertToThumbUrl(string imageUrl)
        {
            String url = String.Empty;

            if (imageUrl != null && imageUrl.Trim().Length > 0 && imageUrl.Contains("/"))
            {
                int index = imageUrl.LastIndexOf('/');
                StringBuilder bulider = new StringBuilder(imageUrl);
                bulider.Insert(imageUrl.LastIndexOf('/') + 1, "thumb");
                return bulider.ToString();
            }

            return url;
        }

        public static void UploadFileStream(MessageModel model)
        {
            try
            {
                string filePath = ChatHelpers.GetChatFilePath(model.Message, model.MessageType, model.PacketID, model.LinkImageUrl, model.SenderTableID);
                FileInfo fi = new FileInfo(filePath);
                if (fi.Exists)
                {
                    model.FileStatus = ChatConstants.FILE_DEFAULT;
                    model.ViewModel.IsUploadRunning = true;
                    model.ViewModel.UploadPercentage = 1;
                    model.ViewModel.FileProgressInSize = 0;

                    RecentChatCallActivityDAO.UpdateChatMessageFileStatusInSameProcess(model.FriendTableID, model.GroupID, model.PacketID, ChatConstants.FILE_DEFAULT);

                    ChatFileDTO fileDTO = new ChatFileDTO();
                    fileDTO.PacketID = model.PacketID;
                    fileDTO.ContactID = model.GroupID > 0 ? model.GroupID : model.FriendTableID;
                    fileDTO.SenderTableID = model.SenderTableID;
                    fileDTO.FriendTableID = model.FriendTableID;
                    fileDTO.GroupID = model.GroupID;
                    fileDTO.FileID = model.FileID;
                    fileDTO.File = filePath;
                    fileDTO.FileSize = model.FileSize;
                    fileDTO.RecentModel = model;
                    fileDTO.IsDownstream = false;

#if CHAT_LOG
                    log.Debug("FILE TRANSFER => ChatFileTransferProcessorQueue->AddItem. ContactID = " + fileDTO.ContactID + ", FileID = " + model.FileID + ", IsDownstream = " + false);
#endif
                    ImageDictionaries.Instance.FILE_UPLOAD_PROCESSOR_QUEUE.AddItem(fileDTO);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: UploadFileStream() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void CreateSessionOfFileStreamRequest(string filePath, MessageModel model, Func<bool, string, int> onComplete)
        {
            try
            {
                ChatFileDTO fileDTO = new ChatFileDTO();
                fileDTO.PacketID = model.PacketID;
                fileDTO.ContactID = model.GroupID > 0 ? model.GroupID : model.FriendTableID;
                fileDTO.SenderTableID = model.SenderTableID;
                fileDTO.FriendTableID = model.FriendTableID;
                fileDTO.GroupID = model.GroupID;
                fileDTO.FileID = model.FileID;
                fileDTO.FileMenifest = model.FileMenifest;
                fileDTO.File = filePath;
                fileDTO.FileSize = model.FileSize;
                fileDTO.RecentModel = model;
                fileDTO.IsDownstream = true;

                if (onComplete != null)
                {
                    onComplete(true, model.PacketID);
                }
#if CHAT_LOG
                log.Debug("FILE TRANSFER => ChatFileTransferProcessorQueue->AddItem. ContactID = " + fileDTO.ContactID + ", FileID = " + model.FileID + ", IsDownstream = " + true);
#endif
                ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.AddItem(fileDTO);
            }
            catch (Exception ex)
            {
                log.Error("Error: CreateSessionAndSendFileStreamRequest() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void TransferFile(ChatFileDTO fileDTO)
        {
            MessageModel model = (MessageModel)fileDTO.RecentModel;

            if (model != null && (model.FileStatus == ChatConstants.FILE_MOVED || model.FileStatus == ChatConstants.FILE_TRANSFER_CANCELLED))
            {
                if (fileDTO.IsDownstream)
                {
                    ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                }
                else
                {
                    ImageDictionaries.Instance.FILE_UPLOAD_PROCESSOR_QUEUE.OnComplete();
                }
                return;
            }

            if (fileDTO.GroupID > 0)
            {
                TransferGroupFile(fileDTO);
            }
            else
            {
                TransferFriendFile(fileDTO);
            }
        }

        public static void TransferFriendFile(ChatFileDTO fileDTO)
        {
            try
            {
                MessageModel model = (MessageModel)fileDTO.RecentModel;

                if (fileDTO.IsDownstream)
                {
                    ChatHelpers.CheckForFileMenifest(model, () =>
                    {
                        if (model.FileStatus == ChatConstants.FILE_TRANSFER_CANCELLED)
                        {
#if CHAT_LOG
                            log.Debug("FILE TRANSFER => TransferFriendFile() => File Cancelled By Owner. ContactID = " + fileDTO.FriendTableID + ", FileID = " + fileDTO.FileID + "");
#endif
                            model.IsDownloadRunning = false;
                            model.ViewModel.DownloadPercentage = 0;
                            ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                            return 0;
                        }

                        fileDTO.FileMenifest = model.FileMenifest;
                        if (String.IsNullOrWhiteSpace(fileDTO.FileMenifest))
                        {
                            UserBasicInfoModel friendInfoModel = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(fileDTO.FriendTableID);
                            BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(fileDTO.FriendTableID);
                            ChatService.StartFriendChat(fileDTO.FriendTableID, friendInfoModel, blockModel, (rArgs) =>
                            {
                                if (rArgs.Status && ChatService.HasFriendRegistration(fileDTO.FriendTableID))
                                {
                                    ChatService.SendFriendFileStreamRequest(fileDTO.FriendTableID, fileDTO.FileID, (args) =>
                                    {
                                        if (args.Status)
                                        {
                                            ChatService.SendFriendFileSessionRequest(fileDTO.FriendTableID, fileDTO.FileID, (args1) =>
                                            {
                                                if (args1.Status && !String.IsNullOrWhiteSpace(args1.ServerIP) && args1.ServerPort > 0)
                                                {
                                                    if (FileTransferSession.CreateChannel(fileDTO.FriendTableID, args1.ServerIP, args1.ServerPort, false))
                                                    {
                                                        if (!FileTransferSession.TransferFile(fileDTO, false))
                                                        {
                                                            model.IsDownloadRunning = false;
                                                            model.ViewModel.DownloadPercentage = 0;
#if CHAT_LOG
                                                            log.Debug("FILE TRANSFER => TransferFriendFile() => Transfer File Failed. ContactID = " + fileDTO.FriendTableID + ", FileID = " + fileDTO.FileID + "");
#endif
                                                            ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        model.IsDownloadRunning = false;
                                                        model.ViewModel.DownloadPercentage = 0;
#if CHAT_LOG
                                                        log.Debug("FILE TRANSFER => TransferFriendFile() => Create Channel Failed. ContactID = " + fileDTO.FriendTableID + ", FileID = " + fileDTO.FileID + "");
#endif
                                                        ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                                    }
                                                }
                                                else
                                                {
                                                    model.IsDownloadRunning = false;
                                                    model.ViewModel.DownloadPercentage = 0;

                                                    log.Debug("FILE TRANSFER => TransferFriendFile() => Session Request Failed. ContactID = " + fileDTO.FriendTableID + ", FileID = " + fileDTO.FileID + "");

                                                    ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                                }
                                            });
                                        }
                                        else
                                        {
                                            model.IsDownloadRunning = false;
                                            model.ViewModel.DownloadPercentage = 0;

                                            log.Debug("FILE TRANSFER => TransferFriendFile() => Stream Request Failed. ContactID = " + fileDTO.FriendTableID + ", FileID = " + fileDTO.FileID + "");

                                            ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                        }
                                    });
                                }
                                else
                                {
                                    model.IsDownloadRunning = false;
                                    model.ViewModel.DownloadPercentage = 0;
#if CHAT_LOG
                                    log.Debug("FILE TRANSFER => TransferFriendFile() => Online Registration Check Failed. ContactID = " + fileDTO.FriendTableID + ", FileID = " + fileDTO.FileID + "");
#endif
                                    ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                }
                            });
                        }
                        else
                        {
                            UserBasicInfoModel friendInfoModel = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(fileDTO.FriendTableID);
                            BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(fileDTO.FriendTableID);
                            ChatService.StartFriendChat(fileDTO.FriendTableID, friendInfoModel, blockModel, (rArgs) =>
                            {
                                ChatService.SendFriendFileSessionRequest(fileDTO.FriendTableID, fileDTO.FileID, (args) =>
                                {
                                    if (FileTransferSession.CreateChannel(fileDTO.FriendTableID, args.ServerIP, args.ServerPort, false))
                                    {
                                        if (!FileTransferSession.TransferFile(fileDTO, false))
                                        {
                                            model.IsDownloadRunning = false;
                                            model.ViewModel.DownloadPercentage = 0;
#if CHAT_LOG
                                            log.Debug("FILE TRANSFER => TransferFriendFile() => Transfer File Failed. ContactID = " + fileDTO.FriendTableID + ", FileID = " + fileDTO.FileID + "");
#endif
                                            ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                        }
                                    }
                                    else
                                    {
                                        model.IsDownloadRunning = false;
                                        model.ViewModel.DownloadPercentage = 0;
#if CHAT_LOG
                                        log.Debug("FILE TRANSFER => TransferFriendFile() => Session Request Failed. ContactID = " + fileDTO.FriendTableID + ", FileID = " + fileDTO.FileID + "");
#endif
                                        ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                    }
                                });
                            });
                        }
                        return 0;
                    });
                }
                else
                {
                    UserBasicInfoModel friendInfoModel = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(fileDTO.FriendTableID);
                    BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(fileDTO.FriendTableID);
                    ChatService.StartFriendChat(fileDTO.FriendTableID, friendInfoModel, blockModel, (rArgs) =>
                    {
                        ChatService.SendFriendFileSessionRequest(fileDTO.FriendTableID, fileDTO.FileID, (args) =>
                        {
                            if (FileTransferSession.CreateChannel(fileDTO.FriendTableID, args.ServerIP, args.ServerPort, false))
                            {
                                if (!FileTransferSession.TransferFile(fileDTO, true))
                                {
                                    if (model != null)
                                    {
                                        model.ViewModel.IsUploadRunning = false;
                                        model.ViewModel.UploadPercentage = 0;
                                    }
#if CHAT_LOG
                                    log.Debug("FILE TRANSFER => TransferFriendFile() => Transfer File Failed. ContactID = " + fileDTO.FriendTableID + ", FileID = " + fileDTO.FileID + "");
#endif
                                    ImageDictionaries.Instance.FILE_UPLOAD_PROCESSOR_QUEUE.OnComplete();
                                }
                            }
                            else
                            {
                                if (model != null)
                                {
                                    model.ViewModel.IsUploadRunning = false;
                                    model.ViewModel.UploadPercentage = 0;
                                }

#if CHAT_LOG
                                log.Debug("FILE TRANSFER => TransferFriendFile() => Session Request Failed. ContactID = " + fileDTO.FriendTableID + ", FileID = " + fileDTO.FileID + "");
#endif
                                ImageDictionaries.Instance.FILE_UPLOAD_PROCESSOR_QUEUE.OnComplete();
                            }
                        });
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: TransferFriendFile() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void TransferGroupFile(ChatFileDTO fileDTO)
        {
            try
            {
                MessageModel model = (MessageModel)fileDTO.RecentModel;

                if (fileDTO.IsDownstream)
                {
                    ChatHelpers.CheckForFileMenifest(model, () =>
                    {
                        if (model.FileStatus == ChatConstants.FILE_TRANSFER_CANCELLED)
                        {
#if CHAT_LOG
                            log.Debug("FILE TRANSFER => TransferGroupFile() => File Cancelled By Owner. ContactID = " + fileDTO.GroupID + ", FileID = " + fileDTO.FileID + "");
#endif
                            model.IsDownloadRunning = false;
                            model.ViewModel.DownloadPercentage = 0;
                            ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                            return 0;
                        }

                        fileDTO.FileMenifest = model.FileMenifest;
                        if (String.IsNullOrWhiteSpace(fileDTO.FileMenifest))
                        {
                            ChatService.StartGroupChat(fileDTO.GroupID, null, (rArgs) =>
                            {
                                if (rArgs.Status)
                                {
                                    ChatService.SendGroupFileStreamRequest(fileDTO.GroupID, fileDTO.SenderTableID, fileDTO.FileID, (args) =>
                                    {
                                        if (args.Status)
                                        {
                                            ChatService.SendGroupFileSessionRequest(fileDTO.GroupID, fileDTO.FileID, (args1) =>
                                            {
                                                if (args1.Status && !String.IsNullOrWhiteSpace(args1.ServerIP) && args1.ServerPort > 0)
                                                {
                                                    if (FileTransferSession.CreateChannel(fileDTO.GroupID, args1.ServerIP, args1.ServerPort, true))
                                                    {
                                                        if (!FileTransferSession.TransferFile(fileDTO, false))
                                                        {
                                                            model.IsDownloadRunning = false;
                                                            model.ViewModel.DownloadPercentage = 0;
#if CHAT_LOG
                                                            log.Debug("FILE TRANSFER => TransferGroupFile() => Transfer File Failed. ContactID = " + fileDTO.GroupID + ", FileID = " + fileDTO.FileID + "");
#endif
                                                            ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        model.IsDownloadRunning = false;
                                                        model.ViewModel.DownloadPercentage = 0;
#if CHAT_LOG
                                                        log.Debug("FILE TRANSFER => TransferGroupFile() => Create Channel Failed. ContactID = " + fileDTO.GroupID + ", FileID = " + fileDTO.FileID + "");
#endif
                                                        ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                                    }
                                                }
                                                else
                                                {
                                                    model.IsDownloadRunning = false;
                                                    model.ViewModel.DownloadPercentage = 0;

                                                    log.Debug("FILE TRANSFER => TransferGroupFile() => Session Request Failed. ContactID = " + fileDTO.GroupID + ", FileID = " + fileDTO.FileID + "");

                                                    ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                                }
                                            });
                                        }
                                        else
                                        {
                                            model.IsDownloadRunning = false;
                                            model.ViewModel.DownloadPercentage = 0;

                                            log.Debug("FILE TRANSFER => TransferGroupFile() => Stream Request Failed. ContactID = " + fileDTO.GroupID + ", FileID = " + fileDTO.FileID + "");

                                            ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                        }
                                    });
                                }
                                else
                                {
                                    model.IsDownloadRunning = false;
                                    model.ViewModel.DownloadPercentage = 0;

                                    log.Debug("FILE TRANSFER => TransferGroupFile() => Registration Check Failed. ContactID = " + fileDTO.GroupID + ", FileID = " + fileDTO.FileID + "");

                                    ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                }
                            });
                        }
                        else
                        {
                            ChatService.StartGroupChat(fileDTO.GroupID, null, (rArgs) =>
                            {
                                ChatService.SendGroupFileSessionRequest(fileDTO.GroupID, fileDTO.FileID, (args) =>
                                {
                                    if (FileTransferSession.CreateChannel(fileDTO.GroupID, args.ServerIP, args.ServerPort, true))
                                    {
                                        if (!FileTransferSession.TransferFile(fileDTO, false))
                                        {
                                            model.IsDownloadRunning = false;
                                            model.ViewModel.DownloadPercentage = 0;
#if CHAT_LOG
                                            log.Debug("FILE TRANSFER => TransferGroupFile() => Transfer File Failed. ContactID = " + fileDTO.GroupID + ", FileID = " + fileDTO.FileID + "");
#endif
                                            ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                        }
                                    }
                                    else
                                    {
                                        model.IsDownloadRunning = false;
                                        model.ViewModel.DownloadPercentage = 0;
#if CHAT_LOG
                                        log.Debug("FILE TRANSFER => TransferGroupFile() => Create Channel Failed. ContactID = " + fileDTO.GroupID + ", FileID = " + fileDTO.FileID + "");
#endif
                                        ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                    }
                                });
                            });
                        }
                        return 0;
                    });
                }
                else
                {
                    ChatService.StartGroupChat(fileDTO.GroupID, null, (rArgs) =>
                    {
                        ChatService.SendGroupFileSessionRequest(fileDTO.GroupID, fileDTO.FileID, (args) =>
                        {
                            if (FileTransferSession.CreateChannel(fileDTO.GroupID, args.ServerIP, args.ServerPort, true))
                            {
                                if (!FileTransferSession.TransferFile(fileDTO, true))
                                {
                                    if (model != null)
                                    {
                                        model.ViewModel.IsUploadRunning = false;
                                        model.ViewModel.UploadPercentage = 0;
                                    }
#if CHAT_LOG
                                    log.Debug("FILE TRANSFER => TransferGroupFile() => Transfer File Failed. ContactID = " + fileDTO.GroupID + ", FileID = " + fileDTO.FileID + "");
#endif
                                    ImageDictionaries.Instance.FILE_UPLOAD_PROCESSOR_QUEUE.OnComplete();
                                }
                            }
                            else
                            {
                                if (model != null)
                                {
                                    model.ViewModel.IsUploadRunning = false;
                                    model.ViewModel.UploadPercentage = 0;
                                }
#if CHAT_LOG
                                log.Debug("FILE TRANSFER => TransferGroupFile() => Create Channel Failed. ContactID = " + fileDTO.GroupID + ", FileID = " + fileDTO.FileID + "");
#endif
                                ImageDictionaries.Instance.FILE_UPLOAD_PROCESSOR_QUEUE.OnComplete();
                            }
                        });
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: TransferGroupFile() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void CheckForFileMenifest(MessageModel model, Func<int> onCheck)
        {

            if (String.IsNullOrWhiteSpace(model.FileMenifest))
            {
                if (model.FriendTableID > 0)
                {
                    ChatService.GetFriendMessageByPacketID(model.FriendTableID, model.PacketID, (args) =>
                    {
                        onCheck();
                    });
                }
                else if (model.GroupID > 0)
                {
                    ChatService.GetGroupMessageByPacketID(model.GroupID, model.PacketID, (args) =>
                    {
                        onCheck();
                    });
                }
            }
            else
            {
                onCheck();
            }
        }

        public static void SendFileStreamCancel(long friendTableID, long groupId, long fileId, MessageDTO messageDTO)
        {
            try
            {
                FileTransferSession.CancelTransferFile(friendTableID > 0 ? friendTableID : groupId, fileId, true);
                messageDTO = messageDTO == null ? RecentChatCallActivityDAO.GetMessageDTOByPacketID(friendTableID, groupId, null, fileId) : messageDTO;

                if (messageDTO != null && messageDTO.SenderTableID == DefaultSettings.LOGIN_TABLE_ID && String.IsNullOrWhiteSpace(messageDTO.FileMenifest))
                {
                    messageDTO.OriginalMessage = ChatJSONParser.MakeStreamMessage(messageDTO.Message, messageDTO.FileSize, messageDTO.FileID, messageDTO.FileMenifest, true);
                    if (messageDTO.FriendTableID > 0)
                    {
                        ChatService.EditFriendChat(messageDTO.PacketID, messageDTO.FriendTableID, messageDTO.MessageType, messageDTO.Timeout, messageDTO.OriginalMessage, messageDTO.MessageDate, messageDTO.IsSecretVisible, null, null);
                    }
                    else
                    {
                        ChatService.EditGroupChat(messageDTO.PacketID, messageDTO.GroupID, messageDTO.MessageType, messageDTO.OriginalMessage, messageDTO.MessageDate);
                    }

                    RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                    ObservableCollection<RecentModel> chatModelList = RingIDViewModel.Instance.GetRecentModelListByID(messageDTO.FriendTableID > 0 ? messageDTO.FriendTableID : messageDTO.GroupID);
                    RecentModel chatModel = chatModelList.Where(P => P.UniqueKey == messageDTO.PacketID).FirstOrDefault();
                    if (chatModel != null && chatModel.Message != null)
                    {
                        chatModel.Message.OriginalMessage = messageDTO.OriginalMessage;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: SendFileStreamCancel() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void SendFileStreamMenifest(long friendTableID, long groupId, long fileId, string manifestCloudUrl)
        {
            try
            {
                MessageDTO messageDTO = RecentChatCallActivityDAO.GetMessageDTOByPacketID(friendTableID, groupId, null, fileId);
                if (messageDTO != null)
                {
                    messageDTO.OriginalMessage = ChatJSONParser.MakeStreamMessage(messageDTO.Message, messageDTO.FileSize, messageDTO.FileID, manifestCloudUrl, false);
                    if (messageDTO.FriendTableID > 0)
                    {
                        ChatService.EditFriendChat(messageDTO.PacketID, messageDTO.FriendTableID, messageDTO.MessageType, messageDTO.Timeout, messageDTO.OriginalMessage, messageDTO.MessageDate, messageDTO.IsSecretVisible, null, null);
                    }
                    else
                    {
                        ChatService.EditGroupChat(messageDTO.PacketID, messageDTO.GroupID, messageDTO.MessageType, messageDTO.OriginalMessage, messageDTO.MessageDate);
                    }
                    RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                    ObservableCollection<RecentModel> chatModelList = RingIDViewModel.Instance.GetRecentModelListByID(messageDTO.FriendTableID > 0 ? messageDTO.FriendTableID : messageDTO.GroupID);
                    RecentModel chatModel = chatModelList.Where(P => P.UniqueKey == messageDTO.PacketID).FirstOrDefault();
                    if (chatModel != null && chatModel.Message != null)
                    {
                        chatModel.Message.OriginalMessage = messageDTO.OriginalMessage;
                        chatModel.Message.FileMenifest = manifestCloudUrl;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at SendFileStreamMenifest() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static int[] SaveChatImageFile(string filePath, string savingLocation)
        {
            int w = 0;
            int h = 0;
            try
            {
                byte[] imageData = File.ReadAllBytes(filePath);
                using (MemoryStream ms = new MemoryStream(imageData))
                {
                    using (System.Drawing.Image img = System.Drawing.Image.FromStream(ms))
                    {
                        ImageFormat format = GetImageFormat(img);
                        if (format == ImageFormat.Jpeg || format == ImageFormat.Png || format == ImageFormat.Bmp)
                        {
                            bool isResized = false;
                            int targetW = 0;
                            int targetH = 0;
                            w = img.Width;
                            h = img.Height;
                            double ratio = (double)w / (double)h;

                            if (ratio >= 1)
                            {
                                targetW = HORIZONTAL_DIMENSION[0];
                                targetH = HORIZONTAL_DIMENSION[1];
                            }
                            else if (ratio < 1)
                            {
                                targetW = VERTICAL_DIMENSION[0];
                                targetH = VERTICAL_DIMENSION[1];
                            }

                            if (w > targetW || h > targetH)
                            {
                                int newW = 0;
                                int newH = 0;
                                int oldW = w;
                                int oldH = h;

                                if (oldW > targetW && oldH > targetH)
                                {
                                    double diffW = (double)((oldW - targetW) * 100) / oldW;
                                    double diffH = (double)((oldH - targetH) * 100) / oldH;
                                    if (diffW > diffH)
                                    {
                                        newW = targetW;
                                        newH = (oldH * newW) / oldW;
                                    }
                                    else
                                    {
                                        newH = targetH;
                                        newW = (oldW * newH) / oldH;
                                    }
                                }
                                else if (oldW > targetW)
                                {
                                    newW = targetW;
                                    newH = (oldH * newW) / oldW;
                                }
                                else if (oldH > targetH)
                                {
                                    newH = targetH;
                                    newW = (oldW * newH) / oldH;
                                }

                                w = newW;
                                h = newH;
                                isResized = true;
                            }

                            if (isResized || imageData.Length > ONE_MB)
                            {
                                img.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
                                img.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

                                var newImage = new Bitmap(w, h);
                                using (var g = Graphics.FromImage(newImage))
                                {
                                    g.CompositingMode = CompositingMode.SourceCopy;
                                    g.CompositingQuality = CompositingQuality.HighQuality;
                                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                    g.SmoothingMode = SmoothingMode.HighQuality;
                                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                                    g.DrawImage(img, new Rectangle(new System.Drawing.Point(0, 0), new System.Drawing.Size(w, h)));
                                }

                                long fileSize = 0;
                                using (var memoryStream = new MemoryStream())
                                {
                                    newImage.Save(memoryStream, format);
                                    fileSize = memoryStream.Length;
                                }

                                if (fileSize > ONE_MB)
                                {
                                    if (format == ImageFormat.Png)
                                    {
                                        bool isTransparent = false;
                                        if ((img.Flags & 0x2) != 0)
                                        {
                                            for (int y = 0; y < newImage.Height; ++y)
                                            {
                                                for (int x = 0; x < newImage.Width; ++x)
                                                {
                                                    if (newImage.GetPixel(x, y).A != 255)
                                                    {
                                                        isTransparent = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }

                                        if (isTransparent == false)
                                        {
                                            format = ImageFormat.Jpeg;
                                        }
                                    }
                                    else if (format == ImageFormat.Bmp)
                                    {
                                        format = ImageFormat.Jpeg;
                                    }
                                }

                                if (format == ImageFormat.Jpeg)
                                {
                                    ImageCodecInfo encoder = GetEncoder(format);
                                    EncoderParameters parameters = new EncoderParameters(1);
                                    parameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 70L);
                                    newImage.Save(savingLocation, encoder, parameters);
                                }
                                else
                                {
                                    newImage.Save(savingLocation, format);
                                }
                            }
                            else
                            {
                                File.Copy(filePath, savingLocation, true);
                            }
                        }
                        else
                        {
                            File.Copy(filePath, savingLocation, true);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error at SaveChatImageFile() in ChatFileUploader class ==>" + ex.Message + "\n" + ex.StackTrace);
                //throw ex;
            }
            return new int[2] { w, h };
        }

        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {

            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        public static ImageFormat GetImageFormat(System.Drawing.Image img)
        {
            ImageFormat format = ImageFormat.Jpeg;

            try
            {
                Guid id = img.RawFormat.Guid;

                if (id == ImageFormat.Png.Guid)
                {
                    format = ImageFormat.Png;
                }
                else if (id == ImageFormat.Bmp.Guid)
                {
                    format = ImageFormat.Bmp;
                }
                else if (id == ImageFormat.Emf.Guid)
                {
                    format = ImageFormat.Emf;
                }
                else if (id == ImageFormat.Exif.Guid)
                {
                    format = ImageFormat.Exif;
                }
                else if (id == ImageFormat.Gif.Guid)
                {
                    format = ImageFormat.Gif;
                }
                else if (id == ImageFormat.Icon.Guid)
                {
                    format = ImageFormat.Icon;
                }
                else if (id == ImageFormat.Jpeg.Guid)
                {
                    format = ImageFormat.Jpeg;
                }
                else if (id == ImageFormat.MemoryBmp.Guid)
                {
                    format = ImageFormat.MemoryBmp;
                }
                else if (id == ImageFormat.Tiff.Guid)
                {
                    format = ImageFormat.Tiff;
                }
                else if (id == ImageFormat.Wmf.Guid)
                {
                    format = ImageFormat.Wmf;
                }
            }
            catch
            {
            }

            return format;
        }

        public static bool EncodeChatMediaAudioUsingOpus(string inputPath, string outputFile)
        {
            bool status = false;
            try
            {
                CallHelperMethods.StartAudioEncodeDecodeSession();
                byte[] byteArray = System.IO.File.ReadAllBytes(inputPath);
                byteArray = CallHelperMethods.EncodeAudioFrame(byteArray);
                File.WriteAllBytes(outputFile, byteArray);
                status = true;
            }
            catch (Exception ex)
            {
                log.Error("Error at EncodeAudioUsingOpus() in ChatHelpers class ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            finally
            {
                CallHelperMethods.StopAudioEncodeDecodeSession();
            }

            return status;
        }

        public static bool DecodeChatMediaAudioUsingOpus(string inputPath, string outputFile)
        {
            if (System.IO.File.Exists(inputPath))
            {
                try
                {
                    CallHelperMethods.StartAudioEncodeDecodeSession();
                    byte[] rawData = System.IO.File.ReadAllBytes(inputPath);
                    rawData = CallHelperMethods.DecodeAudioFrame(rawData);

                    WaveWriter writer = new WaveWriter();
                    writer.Write(outputFile, rawData);
                    if (System.IO.File.Exists(inputPath))
                    {
                        System.IO.File.Delete(inputPath);
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    log.Error("DecodeAudio() Failed ==> " + ex.Message + "\n" + ex.StackTrace);
                }
                finally
                {
                    CallHelperMethods.StopAudioEncodeDecodeSession();
                }
            }
            else
            {
                log.Error("DecodeAudio() Failed ==> File not Exist.");
            }
            return false;
        }

        public static bool EncodeChatMediaVideoUsingFFMPEG(string path, int duration)
        {
            bool status = false;
            try
            {
                string tempFile = System.IO.Path.GetDirectoryName(path) + System.IO.Path.DirectorySeparatorChar + System.IO.Path.GetFileNameWithoutExtension(path) + "_trim" + System.IO.Path.GetExtension(path);

                ConversionOptions convertOptions = new ConversionOptions
                {
                    VideoEncoder = VideoEncoder.MPEG4,
                    VideoBitRate = 1200,
                    AudioEncoder = AudioEncoder.AAC,
                    AudioSampleRate = AudioSampleRate.Hz44100,
                    MaxDuration = TimeSpan.FromSeconds(duration)
                };
                Metadata md1 = MediaService.ConvertMedia(RingIDSettings.TEMP_MEDIA_TOOLKIT, path, tempFile, convertOptions);
                if (md1 != null && md1.VideoData != null)
                {
                    File.Delete(path);
                    File.Move(tempFile, path);
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return status;
        }

        #endregion File Utility Method

        #region Group Profile Utility

        public static long CreateGroupID()
        {
            long time = ChatService.GetServerTime();
            long groupID = (time / 1000) * 1000000 + DefaultSettings.LOGIN_RING_ID % 1000000;
            return groupID;
        }

        public static void CreateGroup(List<UserBasicInfoModel> modelList, string param, bool showNameAlert = true)
        {
            try
            {
                string groupName = "Group";
                if (showNameAlert)
                {
                    RingIDViewModel.Instance.GroupButtonSelection = true;
                    CustomInputBox result = CustomInputBox.ShowInputBox(param, "Enter Group Name", MessageBoxButton.YesNoCancel, MessageBoxImage.Information, MessageBoxResult.No, MessageBoxOptions.None, new String[] { "Save", "Skip", "Cancel" });

                    if (result.MessageBoxResult == MessageBoxResult.Cancel)
                    {
                        RingIDViewModel.Instance.GroupButtonSelection = false;
                        return;
                    }

                    if (result.MessageBoxResult == MessageBoxResult.Yes)
                    {
                        groupName = result.InputText;
                    }
                }
                else
                {
                    groupName = param;
                }

                long groupID = ChatHelpers.CreateGroupID();

                List<BaseMemberDTO> memberMessageDTOs = new List<BaseMemberDTO>();
                BaseMemberDTO memberMessageDTO = null;
                if (modelList != null && modelList.Count > 0)
                {
                    foreach (UserBasicInfoModel model in modelList)
                    {
                        memberMessageDTO = new BaseMemberDTO();
                        memberMessageDTO.MemberIdentity = model.ShortInfoModel.UserTableID;
                        memberMessageDTO.RingID = model.ShortInfoModel.UserIdentity;
                        memberMessageDTO.FullName = model.ShortInfoModel.FullName;
                        memberMessageDTO.Status = ChatConstants.MEMBER_TYPE_MEMBER;
                        memberMessageDTO.AddedBy = DefaultSettings.LOGIN_TABLE_ID;
                        memberMessageDTOs.Add(memberMessageDTO);
                    }
                    modelList.Clear();
                }

                memberMessageDTO = new BaseMemberDTO();
                memberMessageDTO.MemberIdentity = DefaultSettings.LOGIN_TABLE_ID;
                memberMessageDTO.RingID = DefaultSettings.LOGIN_RING_ID;
                memberMessageDTO.FullName = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName;
                memberMessageDTO.Status = ChatConstants.MEMBER_TYPE_OWNER;
                memberMessageDTO.AddedBy = DefaultSettings.LOGIN_TABLE_ID;
                memberMessageDTOs.Add(memberMessageDTO);

                ChatService.CreateGroup(groupID, groupName, String.Empty, memberMessageDTOs, (args) =>
                {
                    if (args.Status)
                    {
                        //List<ActivityDTO> activityList = new List<ActivityDTO>();
                        //List<RecentDTO> recentList = new List<RecentDTO>();
                        List<GroupMemberInfoDTO> memberList = new List<GroupMemberInfoDTO>();
                        GroupMemberInfoDTO member = null;

                        //ActivityDTO activityDTO = new ActivityDTO();
                        //activityDTO.UpdateTime = HelperMethods.GroupCreationTime(messageBaseDTO.GroupID) - 1;
                        //activityDTO.PacketID = PacketIDGenerator.GeneratedPacketIDByTime(activityDTO.UpdateTime);
                        //activityDTO.GroupID = messageBaseDTO.GroupID;
                        //activityDTO.ActivityBy = DefaultSettings.LOGIN_USER_ID;
                        //activityDTO.ActivityType = ChatConstants.ACTIVITY_GROUP_CREATED;
                        //activityDTO.MessageType = ActivityConstants.MSG_GROUP_CREATE;
                        //activityList.Add(activityDTO);
                        //recentList.Add(new RecentDTO { Activity = activityDTO });

                        if (memberMessageDTOs != null && memberMessageDTOs.Count > 0)
                        {
                            //activityDTO = new ActivityDTO();
                            //activityDTO.UpdateTime = HelperMethods.GroupCreationTime(messageBaseDTO.GroupID);
                            //activityDTO.PacketID = PacketIDGenerator.GeneratedPacketIDByTime(activityDTO.UpdateTime);
                            //activityDTO.GroupID = messageBaseDTO.GroupID;
                            //activityDTO.ActivityBy = DefaultSettings.LOGIN_USER_ID;
                            //activityDTO.ActivityType = ChatConstants.ACTIVITY_MEMBER_ADDED;
                            //activityDTO.MessageType = ActivityConstants.MSG_GROUP_MEMBER_ADDED_INTO_GROUP;

                            foreach (BaseMemberDTO memberDTO in memberMessageDTOs)
                            {
                                member = new GroupMemberInfoDTO();
                                member.UserTableID = memberDTO.MemberIdentity;
                                member.RingID = memberDTO.RingID;
                                member.GroupID = groupID;
                                member.FullName = memberDTO.FullName;
                                member.MemberAccessType = (short)memberDTO.Status;
                                member.MemberAddedBy = memberDTO.AddedBy;
                                memberList.Add(member);
                                //activityDTO.MemberList.Add(new ActivityMemberDTO { UID = member.UserIdentity });
                            }
                            //activityList.Add(activityDTO);
                            //recentList.Add(new RecentDTO { Activity = activityDTO });
                        }

                        List<GroupInfoDTO> groupList = new List<GroupInfoDTO>();
                        GroupInfoDTO groupDTO = new GroupInfoDTO();
                        groupDTO.GroupID = groupID;
                        groupDTO.GroupName = groupName;
                        groupDTO.GroupProfileImage = String.Empty;
                        groupDTO.NumberOfMembers = memberList.Count;
                        groupList.Add(groupDTO);

                        GroupInfoModel groupModel = new GroupInfoModel(groupDTO);
                        foreach (GroupMemberInfoDTO dto in memberList)
                        {
                            groupModel.LoadMemberData(dto);
                        }
                        Application.Current.Dispatcher.BeginInvoke(delegate
                        {
                            RingIDViewModel.Instance.GroupList[groupID] = groupModel;
                        });


                        //List<long> groupIDs = new List<long>();
                        //groupIDs.Add(groupID);
                        //ChatHelpers.AddGroupCreationMessage(groupIDs);

                        InsertIntoGroupOrMemberTable groupTable = new InsertIntoGroupOrMemberTable(groupList, memberList);
                        groupTable.OnComplete += () =>
                        {
                            Application.Current.Dispatcher.BeginInvoke(() =>
                            {
                                if (UCGuiRingID.Instance != null)
                                {
                                    UCGuiRingID.Instance.ucFriendTabControlPanel.SelectLeftSideTabControl(2);
                                    //if (MainSwitcher.PopupController.DownloadOrAddToAlbumPopUpWrapper.ucCreateGroupPopUp != null)
                                    //{
                                    //    MainSwitcher.PopupController.DownloadOrAddToAlbumPopUpWrapper.ucCreateGroupPopUp.Hide();
                                    //}
                                }

                                RingIDViewModel.Instance.OnGroupCallChatButtonClicked(groupID);

                                //InsertIntoRingActivityTable activityTable = new InsertIntoRingActivityTable(activityList);
                                //activityTable.OnComplete += () =>
                                //{
                                //    RecentLoadUtility.LoadRecentData(recentList);
                                //};
                                //activityTable.Start();
                            }, DispatcherPriority.ApplicationIdle);
                        };
                        groupTable.Start();
                    }
                    else
                    {
                        UIHelperMethods.ShowFailed("Sorry! Create group failed. Try again.", "Create group");
                        // CustomMessageBox.ShowError("Sorry! Create group failed. Try again.");
                    }
                });


            }
            catch (Exception ex)
            {
                log.Error("Error: CreateGroup() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void SendGroupMemberAdd(GroupInfoModel groupInfoModel, List<BaseMemberDTO> memberMsgList, List<GroupMemberInfoDTO> dbList, Func<bool, int> onComplete)
        {
            ChatService.AddGroupMembers(groupInfoModel.GroupID, memberMsgList, (args) =>
            {
                try
                {
                    if (args.Status)
                    {
                        new InsertIntoGroupOrMemberTable(dbList).Start();

                        groupInfoModel.NumberOfMembers = ChatHelpers.GetGroupMemberCount(groupInfoModel);
                        GroupInfoDAO.UpdateNumberOfMembers(groupInfoModel.GroupID, groupInfoModel.NumberOfMembers);

                        BaseGroupInformation registerInfo = ChatService.GetGroupRegisterInfo(groupInfoModel.GroupID);
                        if (registerInfo != null && !String.IsNullOrWhiteSpace(registerInfo.RegisterServerAddress))
                        {
                            new ThreadAddMemberInGroup(groupInfoModel.GroupID, dbList.Select(P => P.UserTableID).ToList(), registerInfo.RegisterServerAddress, registerInfo.RegisterServerPort).Start();
                        }

                        ChatService.RequestGroupChatHistory(groupInfoModel.GroupID, ChatService.GeneratePacketID().PacketID, ChatConstants.HISTORY_UP, memberMsgList.Count, null);

                        onComplete(true);
                    }
                    else
                    {
                        foreach (BaseMemberDTO baseDTO in memberMsgList)
                        {
                            groupInfoModel.RemoveMemberData(baseDTO.MemberIdentity);
                        }
                        onComplete(false);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: SendGroupMemberAdd() ==> " + ex.Message + "\n" + ex.StackTrace);
                    foreach (BaseMemberDTO baseDTO in memberMsgList)
                    {
                        groupInfoModel.RemoveMemberData(baseDTO.MemberIdentity);
                    }
                    onComplete(false);
                }
            });
        }

        public static void SendGroupName(GroupInfoModel groupInfoModel, string oldGroupName, Func<bool, int> onComplete)
        {
            ChatService.ChangeGroupName(groupInfoModel.GroupID, groupInfoModel.GroupName, (args) =>
            {
                try
                {
                    if (args.Status)
                    {
                        GroupInfoDAO.UpdateGroupName(groupInfoModel.GroupID, groupInfoModel.GroupName);
                        ChatService.RequestGroupChatHistory(groupInfoModel.GroupID, ChatService.GeneratePacketID().PacketID, ChatConstants.HISTORY_UP, 1, null);

                        onComplete(true);
                    }
                    else
                    {
                        groupInfoModel.GroupName = oldGroupName;
                        onComplete(false);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: SendGroupName() ==> " + ex.Message + "\n" + ex.StackTrace);
                    groupInfoModel.GroupName = oldGroupName;
                    onComplete(false);
                }
            });
        }

        public static void SendGroupImage(GroupInfoModel groupInfoModel, string oldImageUrl, Func<bool, int> onComplete)
        {
            ChatService.ChangeGroupUrl(groupInfoModel.GroupID, groupInfoModel.GroupProfileImage, (args) =>
            {
                try
                {
                    if (args.Status)
                    {
                        GroupInfoDAO.UpdateGroupProfileImage(groupInfoModel.GroupID, groupInfoModel.GroupProfileImage);
                        ChatService.RequestGroupChatHistory(groupInfoModel.GroupID, ChatService.GeneratePacketID().PacketID, ChatConstants.HISTORY_UP, 1, null);

                        onComplete(true);
                    }
                    else
                    {
                        groupInfoModel.GroupProfileImage = oldImageUrl;
                        //TRIGGER TO GROUP IMAGE IN GROUP CHAT & LOG PANEL
                        groupInfoModel.OnPropertyChanged("CurrentInstance");
                        onComplete(false);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: SendGroupImage() ==> " + ex.Message + "\n" + ex.StackTrace);
                    groupInfoModel.GroupProfileImage = oldImageUrl;
                    //TRIGGER TO GROUP IMAGE IN GROUP CHAT & CALL PANEL
                    groupInfoModel.OnPropertyChanged("CurrentInstance");
                    onComplete(false);
                }
            });
        }

        public static void SendRemoveGroupMember(GroupInfoModel groupInfoModel, GroupMemberInfoModel memberModel, List<GroupMemberInfoDTO> dbList, Func<bool, int> onComplete)
        {
            List<long> memberIds = new List<long>();
            memberIds.Add(memberModel.UserTableID);

            ChatService.RemoveGroupMembers(groupInfoModel.GroupID, memberIds, (args) =>
            {
                try
                {
                    if (args.Status)
                    {
                        groupInfoModel.RemoveMemberData(memberModel);
                        new InsertIntoGroupOrMemberTable(dbList).Start();

                        groupInfoModel.NumberOfMembers = ChatHelpers.GetGroupMemberCount(groupInfoModel);
                        GroupInfoDAO.UpdateNumberOfMembers(groupInfoModel.GroupID, groupInfoModel.NumberOfMembers);

                        ChatService.RequestGroupChatHistory(groupInfoModel.GroupID, ChatService.GeneratePacketID().PacketID, ChatConstants.HISTORY_UP, 1, null);

                        memberModel.IsReadyForAction = true;
                        onComplete(true);
                    }
                    else
                    {
                        memberModel.IsReadyForAction = true;
                        onComplete(false);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: SendRemoveGroupMember() ==> " + ex.Message + "\n" + ex.StackTrace);
                    memberModel.IsReadyForAction = true;
                    onComplete(false);
                }
            });
        }

        public static void LeaveFromGroup(GroupInfoModel groupInfoModel, GroupMemberInfoModel memberModel, List<GroupMemberInfoDTO> dbList, Func<bool, int> onComplete)
        {
            ChatService.LeaveGroup(groupInfoModel.GroupID, (args) =>
            {
                try
                {
                    if (args.Status)
                    {
                        groupInfoModel.RemoveMemberData(DefaultSettings.LOGIN_TABLE_ID);
                        new InsertIntoGroupOrMemberTable(dbList).Start();

                        groupInfoModel.NumberOfMembers = ChatHelpers.GetGroupMemberCount(groupInfoModel);
                        GroupInfoDAO.UpdateNumberOfMembers(groupInfoModel.GroupID, groupInfoModel.NumberOfMembers);

                        ChatService.UnRegisterGroupChat(groupInfoModel.GroupID, StatusConstants.PRESENCE_ONLINE, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.Mood);
                        ChatService.RequestGroupChatHistory(groupInfoModel.GroupID, ChatService.GeneratePacketID().PacketID, ChatConstants.HISTORY_UP, 1, null);

                        memberModel.IsReadyForAction = true;
                        onComplete(true);
                    }
                    else
                    {
                        memberModel.IsReadyForAction = true;
                        onComplete(false);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: SendRemoveGroupMember() ==> " + ex.Message + "\n" + ex.StackTrace);
                    memberModel.IsReadyForAction = true;
                    onComplete(false);
                }
            });
        }

        public static void SendMemberAccessType(GroupInfoModel groupInfoModel, List<BaseMemberDTO> memberMsgList, List<GroupMemberInfoDTO> oldMemberDTOs, List<GroupMemberInfoModel> memberModelList, Func<bool, int> onComplete)
        {
            ChatService.ChangeGroupMemberStatus(groupInfoModel.GroupID, memberMsgList, (args) =>
            {
                try
                {
                    if (args.Status)
                    {
                        foreach (BaseMemberDTO memberMsgDTO in memberMsgList)
                        {
                            GroupInfoDAO.UpdateGroupMemberAccessType(groupInfoModel.GroupID, memberMsgDTO.MemberIdentity, memberMsgDTO.Status);
                        }
                        ChatService.RequestGroupChatHistory(groupInfoModel.GroupID, ChatService.GeneratePacketID().PacketID, ChatConstants.HISTORY_UP, memberMsgList.Count, null);

                        foreach (GroupMemberInfoModel memberModel in memberModelList)
                        {
                            memberModel.IsReadyForAction = true;
                        }

                        onComplete(true);
                    }
                    else
                    {
                        foreach (GroupMemberInfoDTO memberDTO in oldMemberDTOs)
                        {
                            GroupMemberInfoModel memberModel = memberModelList.Where(P => P.UserTableID == memberDTO.UserTableID).FirstOrDefault();
                            if (memberModel != null)
                            {
                                memberModel.MemberAccessType = memberDTO.MemberAccessType;
                                memberModel.IsReadyForAction = true;
                            }
                        }

                        onComplete(false);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error: SendMemberAccessType() ==> " + ex.Message + "\n" + ex.StackTrace);
                    foreach (GroupMemberInfoDTO memberDTO in oldMemberDTOs)
                    {
                        GroupMemberInfoModel memberModel = memberModelList.Where(P => P.UserTableID == memberDTO.UserTableID).FirstOrDefault();
                        if (memberModel != null)
                        {
                            memberModel.MemberAccessType = memberDTO.MemberAccessType;
                            memberModel.IsReadyForAction = true;
                        }
                    }
                    onComplete(false);
                }
            });
        }

        #endregion Group Profile Utility

        #region CHAT UI

        public static void TestChatLoad()
        {
            //List<UserBasicInfoModel> friendModelList = FriendListController.Instance.FriendDataContainer.UserBasicInfoModels.Values.ToList().Where(P => P.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED).ToList();
            //List<UserBasicInfoModel> friendModelList = new List<UserBasicInfoModel>();
            //UserBasicInfoModel _FriendModel = new UserBasicInfoModel();
            //_FriendModel.ShortInfoModel.UserTableID = 25;
            //friendModelList.Add(_FriendModel);
            //TestChatLoad(friendModelList, 0, 0);
            //TestCallLoad(friendModelList, 0, 0);

            //List<GroupInfoModel> groupModelList = RingIDViewModel.Instance.GroupList.Values.ToList();
            //List<GroupInfoModel> groupModelList = new List<GroupInfoModel>();
            //GroupInfoModel groupModel = new GroupInfoModel();
            //groupModel.GroupID = 1475299941079192;
            //groupModelList.Add(groupModel);
            //TestActivityLoad(groupModelList, 0, 0);
        }

        private static void TestChatLoad(List<UserBasicInfoModel> modelList, int modelIndex, int totalMessage)
        {
            UserBasicInfoModel model = modelIndex < modelList.Count ? modelList.ElementAt(modelIndex) : null;
            if (model == null)
            {
                return;
            }

            List<MessageDTO> mList = new List<MessageDTO>();

            int messageIndex = 0;
            long startTime = ModelUtility.GetStartTimeMillisLocal(DateTime.Now) - (ChatConstants.DAY_365_DAYS * SettingsConstants.MILISECONDS_IN_DAY);
            long currTime = ModelUtility.CurrentTimeMillis();

            while (startTime <= currTime)
            {
                MessageDTO messageDTO = new MessageDTO();
                messageDTO.MessageDate = startTime + 1000;
                messageDTO.PacketID = ChatService.GeneratePacketID(messageDTO.MessageDate).PacketID;

                if (messageIndex % 11 == 0)
                {
                    messageDTO.MessageType = (int)MessageType.PLAIN_MESSAGE;
                    messageDTO.OriginalMessage = messageIndex.ToString() + " => Test Messsage For Load Testing";
                }
                else if (messageIndex % 11 == 1)
                {
                    messageDTO.MessageType = ChatConstants.TYPE_EMOTICON_MSG;
                    messageDTO.OriginalMessage = messageIndex.ToString() + " => Test Messsage For Load Testing :) :( hi";
                }
                else if (messageIndex % 11 == 2)
                {
                    messageDTO.MessageType = ChatConstants.TYPE_DOWNLOADED_STICKER_MSG;
                    messageDTO.OriginalMessage = "1/1/ringidfun5.png";
                }
                else if (messageIndex % 11 == 3)
                {
                    messageDTO.MessageType = ChatConstants.TYPE_IMAGE_FILE_DIRECTORY;
                    messageDTO.OriginalMessage = "{\"u\":\"cloud/chatContens-140/2110000298/676861474871665583.jpg\",\"c\":\"I am working in c# :)\",\"w\":471,\"h\":200}";
                }
                else if (messageIndex % 11 == 4)
                {
                    messageDTO.MessageType = ChatConstants.TYPE_VIDEO_FILE;
                    messageDTO.OriginalMessage = "{\"u\":\"cloud/chatContens-140/2110000298/288671474871967115.mp4\",\"w\":1235,\"h\":6}";
                }
                else if (messageIndex % 11 == 5)
                {
                    messageDTO.MessageType = ChatConstants.TYPE_AUDIO_FILE;
                    messageDTO.OriginalMessage = "{\"u\":\"cloud/chatContens-140/2110000298/10129131474871681231.g729\",\"w\":60,\"h\":3}";
                }
                else if (messageIndex % 11 == 6)
                {
                    messageDTO.MessageType = ChatConstants.TYPE_LOCATION_MSG;
                    messageDTO.OriginalMessage = "{\"la\":23.7916,\"lo\":90.4152,\"loc\":\"Test Client 1's Location\"}";
                }
                else if (messageIndex % 11 == 7)
                {
                    messageDTO.MessageType = ChatConstants.TYPE_LINK_MSG;
                    messageDTO.OriginalMessage = "{\"m\":\"www.google.com\",\"u\":\"http://www.google.com\",\"t\":\"Google\",\"i\":\"http://www.google.com/images/branding/googleg/1x/googleg_standard_color_128dp.png\",\"d\":\"\"}";
                }
                else if (messageIndex % 11 == 8)
                {
                    messageDTO.MessageType = ChatConstants.TYPE_STREAM_FILE;
                    messageDTO.OriginalMessage = "{\"n\":\"C:\\\\Users\\\\shahadat.hossain\\\\Desktop\\\\CallSdkDesktop.zip\",\"s\":967952,\"i\":" + messageDTO.MessageDate + ",\"m\":\"\",\"c\":false}";
                    messageDTO.FileID = messageDTO.MessageDate;
                    messageDTO.FileStatus = ChatConstants.FILE_DEFAULT;
                }
                else if (messageIndex % 11 == 9)
                {
                    messageDTO.MessageType = ChatConstants.TYPE_RING_MEDIA_MSG;
                    //messageDTO.OriginalMessage = "{\"cntntId\":131698,\"ttl\":\"r-270\",\"artst\":null,\"mdaT\":2,\"strmURL\":\"cloud/media-140/2110000298/6230131474871855894.mp4\",\"thmbURL\":\"cloud/media-140/2110000298/6230131474871855894.jpg\",\"drtn\":6,\"tiw\":480,\"tih\":360,\"mpvc\":3,\"albn\":\"Video Feeds\",\"albId\":57065,\"ac\":0,\"lc\":0,\"cc\":0,\"utId\":1068970,\"il\":0,\"ic\":0,\"is\":0,\"ns\":0,\"fn\":\"Test Client 1\",\"prIm\":\"cloud/uploaded-140/2110000298/9354901474447751522.jpg\",\"nfId\":0}";
                    messageDTO.OriginalMessage = "{\"cntntId\":129675,\"ttl\":\"Tomar_Jonnya_-_Anjan_Dutta[www.MP3Fiber.com].mp3\",\"artst\":\"dfsdfsdf\",\"mdaT\":1,\"strmURL\":\"cloud/media-144/2110000235/4858381474452968903.mp3\",\"thmbURL\":\"cloud/media-144/2110000235/3682571474452991885.jpg\",\"drtn\":289,\"tiw\":252,\"tih\":200,\"mpvc\":3,\"albn\":\"Some Album\",\"albId\":56454,\"ac\":1,\"lc\":0,\"cc\":0,\"utId\":1068907,\"il\":0,\"ic\":0,\"is\":0,\"ns\":0,\"fn\":\"_RING_SPECIALD_235\",\"prIm\":\"cloud/uploaded-144/2110000235/5898421474372676637.jpg\",\"nfId\":0}";
                }
                else if (messageIndex % 11 == 10)
                {
                    messageDTO.MessageType = ChatConstants.TYPE_CONTACT_SHARE;
                    messageDTO.OriginalMessage = "{\"u\":1068971,\"r\":2110000299,\"n\":\"Test Client 2\",\"p\":\"cloud/uploaded-140/2110000299/10031931474447808463.jpg\"}";
                }

                messageDTO.FriendTableID = model.ShortInfoModel.UserTableID;
                messageDTO.SenderTableID = messageIndex % 2 == 0 ? messageDTO.FriendTableID : DefaultSettings.LOGIN_TABLE_ID;
                messageDTO.Status = ChatConstants.STATUS_SEEN;
                mList.Add(messageDTO);

                messageIndex += 1;
                startTime += (2 * 3600000);//60 min interval
            }

            InsertIntoFriendMessageTable table = new InsertIntoFriendMessageTable(mList);
            table.OnComplete += () =>
            {
                modelIndex += 1;
                totalMessage += mList.Count;
                log.Info(modelIndex + ". Count => " + totalMessage);
                TestChatLoad(modelList, modelIndex, totalMessage);
            };
            table.Start();
        }

        private static void TestCallLoad(List<UserBasicInfoModel> modelList, int modelIndex, int totalMessage)
        {
            UserBasicInfoModel model = modelIndex < modelList.Count ? modelList.ElementAt(modelIndex) : null;
            if (model == null)
            {
                return;
            }

            List<CallLogDTO> mList = new List<CallLogDTO>();

            int messageIndex = 0;
            long startTime = ModelUtility.GetStartTimeMillisLocal(DateTime.Now) - (ChatConstants.DAY_365_DAYS * SettingsConstants.MILISECONDS_IN_DAY);
            long currTime = ModelUtility.CurrentTimeMillis();

            while (startTime <= currTime)
            {
                CallLogDTO callDTO = new CallLogDTO();
                callDTO.CallingTime = startTime + 1000;
                callDTO.CallID = ChatService.GeneratePacketID(callDTO.CallingTime).PacketID;
                callDTO.FriendTableID = model.ShortInfoModel.UserTableID;

                if (messageIndex % 16 == 0 || messageIndex % 16 == 1)
                {
                    callDTO.CallType = XamlStaticValues.CALL_TYPE_INCOMING;
                    callDTO.CallCategory = XamlStaticValues.CALL_TYPE_VOICE;
                    callDTO.CallDuration = 5000;
                }
                else if (messageIndex % 16 == 2 || messageIndex % 16 == 3)
                {
                    callDTO.CallType = XamlStaticValues.CALL_TYPE_OUTGOING;
                    callDTO.CallCategory = XamlStaticValues.CALL_TYPE_VOICE;
                    callDTO.CallDuration = 6000;
                }
                else if (messageIndex % 16 == 4 || messageIndex % 16 == 5)
                {
                    callDTO.CallType = XamlStaticValues.CALL_TYPE_MISS_CALL;
                    callDTO.CallCategory = XamlStaticValues.CALL_TYPE_VOICE;
                    callDTO.CallDuration = 0;
                }
                else if (messageIndex % 16 == 6 || messageIndex % 16 == 7)
                {
                    callDTO.CallType = XamlStaticValues.CALL_TYPE_OUTGOING;
                    callDTO.CallCategory = XamlStaticValues.CALL_TYPE_VOICE;
                    callDTO.CallDuration = 0;
                    callDTO.Message = "User Busy";
                }
                else if (messageIndex % 16 == 8 || messageIndex % 16 == 9)
                {
                    callDTO.CallType = XamlStaticValues.CALL_TYPE_INCOMING;
                    callDTO.CallCategory = XamlStaticValues.CALL_TYPE_VIDEO;
                    callDTO.CallDuration = 5000;
                }
                else if (messageIndex % 16 == 10 || messageIndex % 16 == 11)
                {
                    callDTO.CallType = XamlStaticValues.CALL_TYPE_OUTGOING;
                    callDTO.CallCategory = XamlStaticValues.CALL_TYPE_VIDEO;
                    callDTO.CallDuration = 6000;
                }
                else if (messageIndex % 16 == 12 || messageIndex % 16 == 13)
                {
                    callDTO.CallType = XamlStaticValues.CALL_TYPE_MISS_CALL;
                    callDTO.CallCategory = XamlStaticValues.CALL_TYPE_VIDEO;
                    callDTO.CallDuration = 0;
                }
                else if (messageIndex % 16 == 14 || messageIndex % 16 == 15)
                {
                    callDTO.CallType = XamlStaticValues.CALL_TYPE_OUTGOING;
                    callDTO.CallCategory = XamlStaticValues.CALL_TYPE_VIDEO;
                    callDTO.CallDuration = 0;
                    callDTO.Message = "User Busy";
                }
                mList.Add(callDTO);

                messageIndex += 1;
                startTime += (2 * 3600000);//60 min interval
            }

            InsertIntoCallLogTable table = new InsertIntoCallLogTable(mList);
            table.OnComplete += () =>
            {
                modelIndex += 1;
                totalMessage += mList.Count;
                log.Info(modelIndex + ". Count => " + totalMessage);
                TestCallLoad(modelList, modelIndex, totalMessage);
            };
            table.Start();
        }

        private static void TestActivityLoad(List<GroupInfoModel> modelList, int modelIndex, int totalMessage)
        {
            GroupInfoModel model = modelIndex < modelList.Count ? modelList.ElementAt(modelIndex) : null;
            if (model == null)
            {
                return;
            }

            List<ActivityDTO> mList = new List<ActivityDTO>();

            int activityIndex = 0;
            long startTime = ModelUtility.GetStartTimeMillisLocal(DateTime.Now) - (ChatConstants.DAY_365_DAYS * SettingsConstants.MILISECONDS_IN_DAY);
            long currTime = ModelUtility.CurrentTimeMillis();

            while (startTime <= currTime)
            {
                ActivityDTO activityDTO = new ActivityDTO();
                activityDTO.UpdateTime = startTime + 1000;
                activityDTO.PacketID = ChatService.GeneratePacketID(activityDTO.UpdateTime).PacketID;

                if (activityIndex % 5 == 0)
                {
                    activityDTO.MessageType = ActivityConstants.MSG_GROUP_NAME_CHANGE;
                    activityDTO.ActivityType = (int)GroupChangeActivity.GROUP_RENAME;
                    activityDTO.GroupName = "Test 123456";
                }
                else if (activityIndex % 5 == 1)
                {
                    activityDTO.MessageType = ActivityConstants.MSG_GROUP_PROFILE_IMAGE_CHANGE;
                    activityDTO.ActivityType = (int)GroupChangeActivity.GROUP_URL_RENAME;
                    activityDTO.GroupProfileImage = "cloud/chatContens-140/2110000298/676861474871665583.jpg";
                }
                else if (activityIndex % 5 == 2)
                {
                    activityDTO.MessageType = ActivityConstants.MSG_GROUP_MEMBER_ADDED_INTO_GROUP;
                    activityDTO.ActivityType = (int)GroupChangeActivity.ADDED;
                    activityDTO.MemberList = new List<ActivityMemberDTO>();
                    activityDTO.MemberList.Add(new ActivityMemberDTO { UID = 57096, RID = 2110079193, ACCESS = 0 });
                }
                else if (activityIndex % 5 == 3)
                {
                    activityDTO.MessageType = ActivityConstants.MSG_GROUP_MEMBER_REMOVED_FROM_GROUP;
                    activityDTO.ActivityType = (int)GroupChangeActivity.DELETED;
                    activityDTO.MemberList = new List<ActivityMemberDTO>();
                    activityDTO.MemberList.Add(new ActivityMemberDTO { UID = 57096, RID = 2110079193, ACCESS = 0 });
                }
                else if (activityIndex % 5 == 4)
                {
                    activityDTO.MessageType = ActivityConstants.MSG_GROUP_MEMBER_MADE_ADMIN;
                    activityDTO.ActivityType = (int)GroupChangeActivity.MEMBER_TYPE_CHANGE;
                    activityDTO.MemberList = new List<ActivityMemberDTO>();
                    activityDTO.MemberList.Add(new ActivityMemberDTO { UID = 57096, RID = 2110079193, ACCESS = ChatConstants.MEMBER_TYPE_ADMIN });
                }

                activityDTO.GroupID = model.GroupID;
                activityDTO.ActivityBy = DefaultSettings.LOGIN_TABLE_ID;
                mList.Add(activityDTO);

                activityIndex += 1;
                startTime += (2 * 3600000);//60 min interval
            }

            InsertIntoRingActivityTable table = new InsertIntoRingActivityTable(mList);
            table.OnComplete += () =>
            {
                modelIndex += 1;
                totalMessage += mList.Count;
                log.Info(modelIndex + ". Count => " + totalMessage);
                TestActivityLoad(modelList, modelIndex, totalMessage);
            };
            table.Start();
        }

        public static void SecretDeleteTimer_Tick(int counter, bool initTick = false, object state = null)
        {
            try
            {
                MessageModel model = state as MessageModel;
                if (initTick == false)
                {
                    model.ViewModel.SecretCount--;
                }

                if (model.ViewModel.ForceStopSecretTimer == true || model.ViewModel.SecretCount <= 0)
                {
#if MESSAGE_STATUS_LOG
                    log.Debug("*** SecretDeleteTimer_Tick(Time Out) => " + model.Message);
#endif
                    model.ViewModel.SecretDeleteTimer.Stop();
                    ChatHelpers.DeleteSecretChat(model);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: SecretDeleteTimer_Tick() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void SecretCounterTimer_Tick(int counter, bool initTick = false, object state = null)
        {
            try
            {
                MessageModel model = state as MessageModel;
                if (initTick == false)
                {
                    model.ViewModel.SecretCount--;
                }

                if (model.ViewModel.ForceStopSecretTimer == true || model.ViewModel.SecretCount <= 0)
                {
#if MESSAGE_STATUS_LOG
                    log.Debug("*** SecretCounterTimer_Tick(Time Out) => " + model.Message);
#endif
                    model.ViewModel.SecretCounterTimer.Stop();
                    RecentChatCallActivityDAO.UpdateChatMessageStatus(model.FriendTableID, model.GroupID, model.PacketID, ChatConstants.STATUS_DISAPPEARED, 0, 0, 0);
                    model.Status = ChatConstants.STATUS_DISAPPEARED;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: SecretCounterTimer_Tick() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void SendViewPlayedSatatus(MessageModel model)
        {
            try
            {
                if (model.IsSecretChat && model.FromFriend)
                {

                    if (model.Status == ChatConstants.STATUS_VIEWED_PLAYED)
                    {
                        return;
                    }

                    if (model.ViewModel.IsMediaMessage == false)
                    {
                        model.IsMessageOpened = true;
                        return;
                    }

#if MESSAGE_STATUS_LOG
                    log.Debug("*** SendViewPlayedSatatus => " + model.Message);
#endif
                    model.MessageViewDate = ChatService.GetServerTime();
                    RecentChatCallActivityDAO.UpdateChatMessageStatus(model.FriendTableID, 0, model.PacketID, ChatConstants.STATUS_VIEWED_PLAYED, 0, 0, model.MessageViewDate);

                    if (model.ViewModel.IsMediaMessage)
                    {
#if MESSAGE_STATUS_LOG
                        log.Debug("*** SENT STATUS_VIEWED_PLAYED To Server => " + model.Message);
#endif
                        List<BaseSeenPacketDTO> list = new List<BaseSeenPacketDTO>();
                        BaseSeenPacketDTO msgDTO = new BaseSeenPacketDTO();
                        msgDTO.PacketID = model.PacketID;
                        msgDTO.MessageStatus = ChatConstants.STATUS_VIEWED_PLAYED;
                        msgDTO.MessageDate = model.MessageViewDate;
                        list.Add(msgDTO);
                        ChatService.SeenFriendChat(model.FriendTableID, list);
                    }

                    if (model.ViewModel.SecretDeleteTimer != null && model.ViewModel.SecretDeleteTimer.IsAlive == false)
                    {
#if MESSAGE_STATUS_LOG
                        log.Debug("*** START DELETE TIMER");
#endif
                        model.ViewModel.SecretCount = model.Timeout;
                        model.ViewModel.SecretDeleteTimer.Start();
                    }

                    model.MessageViewDate = model.MessageViewDate;
                    model.Status = ChatConstants.STATUS_VIEWED_PLAYED;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: SendViewPlayedSatatus() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void DeleteSecretChat(MessageModel model)
        {
            try
            {
#if MESSAGE_STATUS_LOG
                log.Debug("*** DeleteSecretChat => " + model.Message);
#endif
                if (model.FriendTableID > 0)
                {
                    RecentLoadUtility.Remove(model.FriendTableID, model.PacketID);
                }

                List<string> list = new List<string>();
                list.Add(model.PacketID);
                RecentChatCallActivityDAO.DeleteChatHistory(model.FriendTableID, 0, list);

                string filePath = ChatHelpers.GetChatFileDeleteFile(model.Message, model.PrevMessageType, model.PacketID, model.SenderTableID);
                if (!String.IsNullOrWhiteSpace(filePath) && File.Exists(filePath))
                {
                    try { System.IO.File.Delete(filePath); }
                    catch { }
                }

                filePath = ChatHelpers.GetChatPreviewDeletePath(model.Message, model.PrevMessageType, model.PacketID);
                if (!String.IsNullOrWhiteSpace(filePath) && File.Exists(filePath))
                {
                    try { System.IO.File.Delete(filePath); }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: DeleteSecretChat() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void MessageViewModel_OnChatOpened(object sender, AddingNewEventArgs e)
        {
            try
            {
                MessageModel model = e.NewObject as MessageModel;
#if MESSAGE_STATUS_LOG
                log.Debug("######## => " + model.Message + " => STATUS - " + model.Status);
#endif

                if (model.FromFriend)
                {
#if MESSAGE_STATUS_LOG
                    log.Debug("*** IS FROM FRIEND => True");
#endif
                    if (model.FriendTableID > 0)
                    {
                        if (model.Status == ChatConstants.STATUS_DELIVERED)
                        {
#if MESSAGE_STATUS_LOG
                            log.Debug("*** IS CHAT_FRIEND_DELIVERED => TRUE");
#endif
                            model.MessageSeenDate = ChatService.GetServerTime();
                            RecentChatCallActivityDAO.UpdateChatMessageStatus(model.FriendTableID, 0, model.PacketID, ChatConstants.STATUS_SEEN, 0, model.MessageSeenDate, 0);

                            if (model.PacketType == ChatConstants.PACKET_TYPE_FRIEND_MESSAGE || model.PacketType == ChatConstants.PACKET_TYPE_FRIEND_MESSAGE_EDIT || model.PacketType == ChatConstants.PACKET_TYPE_CALL_BUSY_MESSAGE)
                            {
                                List<BaseSeenPacketDTO> list = new List<BaseSeenPacketDTO>();
                                BaseSeenPacketDTO msgDTO = new BaseSeenPacketDTO();
                                msgDTO.PacketID = model.PacketID;
                                msgDTO.MessageStatus = ChatConstants.STATUS_SEEN;
                                msgDTO.MessageDate = model.MessageSeenDate;
                                list.Add(msgDTO);
                                ChatService.SeenFriendChat(model.FriendTableID, list);
                            }

                            model.Status = ChatConstants.STATUS_SEEN;
                        }
                        else if (model.Status == ChatConstants.STATUS_SEEN)
                        {
#if MESSAGE_STATUS_LOG
                            log.Debug("*** IS CHAT_FRIEND_SEEN => True");
#endif
                            if (model.IsSecretChat == false || model.ViewModel.IsMediaMessage == true)
                                return;
#if MESSAGE_STATUS_LOG
                            log.Debug("*** IS MEDIA MESSAGE => FALSE & IS SECRET CHAT = TRUE");
#endif
                            if (model.ViewModel.SecretDeleteTimer != null && model.ViewModel.SecretDeleteTimer.IsAlive == false)
                            {
#if MESSAGE_STATUS_LOG
                                log.Debug("*** START DELETE TIMER");
#endif
                                model.ViewModel.SecretCount = model.Timeout;
                                model.ViewModel.SecretDeleteTimer.Start();
                            }
                        }
                    }
                    else if (model.GroupID > 0)
                    {
                        if (model.Status == ChatConstants.STATUS_DELIVERED)
                        {
#if MESSAGE_STATUS_LOG
                            log.Debug("*** IS CHAT_GROUP_DELIVERED => TRUE");
#endif
                            model.MessageSeenDate = ChatService.GetServerTime();
                            RecentChatCallActivityDAO.UpdateChatMessageStatus(0, model.GroupID, model.PacketID, ChatConstants.STATUS_SEEN, 0, model.MessageSeenDate, 0);

                            List<string> list = new List<string>();
                            list.Add(model.PacketID);
                            ChatService.SeenGroupChat(model.GroupID, list);

                            model.Status = ChatConstants.STATUS_SEEN;
                        }
                    }
                }
                else
                {
#if MESSAGE_STATUS_LOG
                    log.Debug("*** IS FROM FRIEND => FALSE");
#endif
                    if (model.IsSecretChat == false)
                        return;

                    long time = 0;
                    if (model.ViewModel.IsMediaMessage == true && model.Status == ChatConstants.STATUS_VIEWED_PLAYED)
                    {
#if MESSAGE_STATUS_LOG
                        log.Debug("*** IS MEDIA MESSAGE => TRUE, IS CHAT_STATUS_VIEWED => TRUE");
#endif
                        time = model.MessageViewDate;
                    }
                    else if (model.ViewModel.IsMediaMessage == false && model.Status == ChatConstants.STATUS_SEEN)
                    {
#if MESSAGE_STATUS_LOG
                        log.Debug("*** IS MEDIA MESSAGE => FALSE, IS CHAT_FRIEND_SEEN => TRUE");
#endif
                        time = model.MessageSeenDate;
                    }
                    else
                    {
                        return;
                    }

                    long diff = (ChatService.GetServerTime() - time) / 1000;
                    if (diff < model.Timeout && model.ViewModel.SecretCounterTimer != null && model.ViewModel.SecretCounterTimer.IsAlive == false)
                    {
                        model.ViewModel.SecretCount = model.Timeout - (int)(diff < 0 ? 0 : diff);
#if MESSAGE_STATUS_LOG
                        log.Debug("*** START SECRET TIMER => Count = " + model.ViewModel.SecretCount);
#endif
                        model.ViewModel.SecretCounterTimer.Start();
                    }
                    else
                    {
                        RecentChatCallActivityDAO.UpdateChatMessageStatus(model.FriendTableID, model.GroupID, model.PacketID, ChatConstants.STATUS_DISAPPEARED, 0, 0, 0);
                        model.Status = ChatConstants.STATUS_DISAPPEARED;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: MessageViewModel_OnMessageOpened() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void MediaPlayer_AudioPlayStateChange(object sender, AddingNewEventArgs e)
        {
            try
            {
                MessageModel model = e.NewObject as MessageModel;
                switch (model.MultiMediaState)
                {
                    case StatusConstants.MEDIA_INIT_STATE:
#if MESSAGE_STATUS_LOG
                        log.Info("Play Init");
#endif
                        if (model.IsSecretChat && model.FromFriend)
                        {
                            ChatHelpers.DeleteSecretChat(model);
                        }
                        break;
                    case StatusConstants.MEDIA_PLAY_STATE:
#if MESSAGE_STATUS_LOG
                        log.Info("Play Started");
#endif
                        if (model.OffsetPosition >= model.Duration)
                        {
                            model.OffsetPosition = 0;
                        }
                        break;
                    case StatusConstants.MEDIA_PAUSE_STATE:
#if MESSAGE_STATUS_LOG
                        log.Info("Play Paused");
#endif
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: MediaPlayer_MediaPlayStateChange() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void TriggerChatActivateEvent(UserControl userControl)
        {
            try
            {
                if (userControl != null && userControl.Parent != null && userControl.IsVisible)
                {
                    if (userControl is UCFriendChatCallPanel)
                    {
                        UCFriendChatScrollViewer scrollViewer = ((UCFriendChatCallPanel)userControl).ChatScrollViewer;
                        ChatHelpers.ChangeChatViewPortOpenedProperty(scrollViewer.srvRecentList, scrollViewer.tvRecentList, scrollViewer._StackPanel, true, scrollViewer._UnreadChatList, scrollViewer._UnreadCallList);
                    }
                    else if (userControl is UCGroupChatCallPanel)
                    {
                        UCGroupChatScrollViewer scrollViewer = ((UCGroupChatCallPanel)userControl).ChatScrollViewer;
                        ChatHelpers.ChangeChatViewPortOpenedProperty(scrollViewer.srvRecentList, scrollViewer.tvRecentList, scrollViewer._StackPanel, true, scrollViewer._UnreadChatList, null);
                    }
                    else if (userControl is UCRoomChatCallPanel)
                    {
                        UCRoomChatScrollViewer scrollViewer = ((UCRoomChatCallPanel)userControl).ChatScrollViewer;
                        ChatHelpers.ChangeChatViewPortOpenedProperty(scrollViewer.srvRecentList, scrollViewer.tvRecentList, scrollViewer._StackPanel, true, null, null);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: TriggerChatActivateEvent()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void ChangeChatViewPortOpenedProperty(ScrollViewer scrollViewer, ItemsControl itemsControl, FrameworkElement container, bool isScrollBarCaptured, ObservableDictionary<string, long> unreadChatList, ObservableDictionary<string, long> unreadCallList)
        {
            try
            {
                if (container == null) return;

                double offset = scrollViewer.VerticalOffset - 200;
                double height = container.ActualHeight - scrollViewer.ActualHeight > 0 ? scrollViewer.ActualHeight : container.ActualHeight;
                double x = (scrollViewer.ActualWidth - 615) / 2;
                double y = (offset > 0 ? offset : 0) + height;

                HitTestResult hitTest = VisualTreeHelper.HitTest(itemsControl, new System.Windows.Point(x, y));
                if (hitTest != null)
                {
                    int maxIndex = itemsControl.Items.Count - 1;
                    int startIndex = maxIndex;
                    ContentPresenter presenter = ChatHelpers.GetContentPresenterFromEvent(hitTest.VisualHit, itemsControl) as ContentPresenter;
                    if (presenter != null)
                    {
                        startIndex = itemsControl.ItemContainerGenerator.IndexFromContainer(presenter);
                        //Debug.WriteLine("===============================> startIndex = " + startIndex);
                        if (!(presenter.Content is RecentModel && startIndex >= 0)) return;
                    }

                    long friendId = 0;
                    long groupId = 0;
                    List<string> packetIds = new List<string>();
                    List<string> callIds = new List<string>();

                    int index = startIndex;
                    int overLimit = 1;

                    if (!isScrollBarCaptured)
                    {
                        index = (startIndex + 5 > maxIndex ? maxIndex : startIndex + 5);
                        overLimit = 5;

                        int i = maxIndex - index > 10 ? index + 10 : maxIndex;
                        for (; i > index; i--)
                        {
                            var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(i);
                            if (temp == null) break;

                            presenter = temp as ContentPresenter;
                            RecentModel model = (RecentModel)presenter.Content;
                            model.IsRecentOpened = false;
                            //Debug.WriteLine("HIDE => " + i + ",     Message => " + (model.Message != null ? model.Message.OriginalMessage : ""));
                        }
                    }

                    for (int overflow = 0; index >= 0; index--)
                    {
                        if (height <= 0 && (++overflow) >= overLimit) break;

                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(index);
                        if (temp == null) break;

                        presenter = temp as ContentPresenter;
                        RecentModel model = (RecentModel)presenter.Content;
                        model.IsRecentOpened = true;
                        //Debug.WriteLine("Show => " + index + ",     Message => " + (model.Message != null ? model.Message.OriginalMessage : ""));

                        if (index <= startIndex)
                        {
                            if (height > -200)
                            {
                                if (model.Message != null)
                                {
                                    model.Message.IsMessageOpened = true;
                                    if (model.Message.IsUnread)
                                    {
                                        model.Message.IsUnread = false;
                                        if (ChatHelpers.RemoveUnreadChat(model.ContactID, model.Message.PacketID, unreadChatList))
                                        {
                                            friendId = model.Message.FriendTableID;
                                            groupId = model.Message.GroupID;
                                            packetIds.Add(model.Message.PacketID);
                                        }
                                    }
                                    //Debug.WriteLine("Preview => " + index + ",     Message => " + model.Message.OriginalMessage);
                                }
                                else if (model.CallLog != null)
                                {
                                    if (model.CallLog.IsUnread)
                                    {
                                        model.CallLog.IsUnread = false;
                                        if (ChatHelpers.RemoveUnreadCall(model.ContactID, model.CallLog.CallID, unreadCallList))
                                        {
                                            friendId = model.CallLog.FriendTableID;
                                            callIds.Add(model.CallLog.CallID);
                                        }
                                    }
                                }
                            }
                            if (index == startIndex)
                            {
                                double visibleHeight = scrollViewer.ActualHeight - presenter.TransformToAncestor(scrollViewer).TransformBounds(new System.Windows.Rect(new System.Windows.Point(0, 0), presenter.RenderSize)).Top;
                                height -= visibleHeight;
                                //Debug.WriteLine("VisibleHeight => " + visibleHeight);
                            }
                            else
                            {
                                height -= presenter.ActualHeight;
                            }
                        }
                    }

                    if (!isScrollBarCaptured)
                    {
                        int count = index > 10 ? index - 10 : 0;
                        for (int j = index; j >= count; j--)
                        {
                            var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(j);
                            if (temp == null) break;

                            presenter = temp as ContentPresenter;
                            RecentModel model = (RecentModel)presenter.Content;
                            model.IsRecentOpened = false;
                            //Debug.WriteLine("Hide => " + index + ",     Message => " + (model.Message != null ? model.Message.OriginalMessage : ""));
                        }
                    }

                    RecentChatCallActivityDAO.MakeChatAsRead(friendId, groupId, packetIds);
                    RecentChatCallActivityDAO.MakeCallAsRead(friendId, callIds);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeChatViewPortOpenedProperty() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void ChangeRoomViewPortOpenedProperty(ScrollViewer scrollViewer, UserControl userControl, ItemsControl itemsControl, FrameworkElement container)
        {
            try
            {
                double height = container.ActualHeight > scrollViewer.ActualHeight ? scrollViewer.ActualHeight : container.ActualHeight;
                double x = (scrollViewer.ActualWidth - 630) / 2;
                double y = scrollViewer.VerticalOffset + height;//(height - 50/* in case loading visible*/);

                HitTestResult hitTest = VisualTreeHelper.HitTest(itemsControl, new System.Windows.Point(x, y));
                if (hitTest != null)
                {
                    int maxIndex = itemsControl.Items.Count - 1;
                    int startIndex = maxIndex;

                    ContentPresenter presenter = ChatHelpers.GetContentPresenterFromEvent(hitTest.VisualHit, itemsControl) as ContentPresenter;
                    if (presenter != null)
                    {
                        startIndex = itemsControl.ItemContainerGenerator.IndexFromContainer(presenter);
                        if (startIndex < 0) return;
                    }

                    int index = (startIndex + 2 > maxIndex ? maxIndex : startIndex + 2);
                    for (; index >= 0; index--)
                    {
                        if (height <= 0)
                            break;

                        var temp = itemsControl.ItemContainerGenerator.ContainerFromIndex(index);
                        if (temp == null)
                            break;

                        presenter = temp as ContentPresenter;
                        if (index <= startIndex)
                        {
                            height -= presenter.ActualHeight;
                        }

                        CustomBorder child = HelperMethods.FindVisualChild<CustomBorder>(presenter);
                        if (child == null)
                            return;

                        if (child.IsVisible)
                        {
                            child.IsOpened = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ChangeChatViewPortOpenedProperty() => " + ex.Message + "\n" + ex.StackTrace);
            }

        }

        public static ContentPresenter GetContentPresenterFromEvent(object source, ItemsControl itemsControl)
        {
            DependencyObject depObj = source as DependencyObject;
            if (depObj != null)
            {
                DependencyObject current = depObj;
                while (current != null && current != itemsControl)
                {
                    ContentPresenter presenter = current as ContentPresenter;
                    if (presenter != null)
                    {
                        return presenter;
                    }
                    current = VisualTreeHelper.GetParent(current);
                }
            }

            return null;
        }

        #endregion CHAT UI

    }
}

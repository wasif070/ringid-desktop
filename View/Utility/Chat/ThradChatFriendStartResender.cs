﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;

namespace View.Utility.Chat
{
    class ThradChatFriendStartResender
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradChatFriendStartResender).Name);
        private bool running = false;
        private long _FriendTableID;
        private Func<long, bool, int> _OnComplete;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        public ThradChatFriendStartResender(long friendTableID, Func<long, bool, int> onComplete)
        {
            this._FriendTableID = friendTableID;
            this._OnComplete = onComplete;
        }
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.FutId] = _FriendTableID.ToString();
                pakToSend[JsonKeys.UserTableID] = DefaultSettings.LOGIN_TABLE_ID.ToString();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_START_FRIEND_CHAT;
                JObject _JobjFromResponse = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_CHAT, packetId);
                if (_JobjFromResponse != null)
                {
                    long friendTableId = _JobjFromResponse[JsonKeys.FutId] != null ? ModelUtility.ConvertToLong(_JobjFromResponse[JsonKeys.FutId]) : 0;
                    int reasonCode = _JobjFromResponse[JsonKeys.ReasonCode] != null ? (int)_JobjFromResponse[JsonKeys.ReasonCode] : 0;
                    Callback(_FriendTableID, (reasonCode != ReasonCodeConstants.REASON_CODE_PERMISSION_DENIED && friendTableId != 0));
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out _JobjFromResponse);
                }
                else
                {
                    Callback(_FriendTableID, false);
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                running = false;
            }
        }

        private void Callback(long friendTableId, bool status)
        {
            try
            {
                if (_OnComplete != null)
                {
                    _OnComplete(friendTableId, status);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in Callback() => " + ex.Message + "\n" + ex.StackTrace);
            }
            _OnComplete = null;
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
﻿using imsdkwrapper;
using log4net;
using MediaToolkit;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Utility;
using Models.Utility.Chat;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using View.BindingModels;
using View.Constants;
using View.Utility.Chat.Service;
using View.Utility.Recent;
using View.ViewModel;

namespace View.Utility.Chat
{
    public class ChatFileShareProcessor
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChatFileShareProcessor).Name);

        private string[] _EXT_IMAGE = { "jpg", "jpeg", "png" };
        private string[] _EXT_AUDIO = { "mp3" };
        private string[] _EXT_VIDEO = { "mp4" };

        private long _FriendTableID;
        private long _GroupID;
        private string _RoomID;
        private string _ContactID;
        private List<FileShareData> _FileShareDatas;

        public ChatFileShareProcessor(long friendTableID, long grouID, string roomID, List<FileShareData> fileShareDatas)
        {
            this._FriendTableID = friendTableID;
            this._GroupID = grouID;
            this._RoomID = roomID;

            if (_FriendTableID > 0)
            {
                _ContactID = _FriendTableID.ToString();
            }
            else if (_GroupID > 0)
            {
                _ContactID = _GroupID.ToString();
            }
            else
            {
                _ContactID = _RoomID;
            }

            this._FileShareDatas = fileShareDatas;
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Run));
            t.Name = this.GetType().Name;
            t.Start();
        }

        public void Run()
        {
            List<RecentDTO> list = new List<RecentDTO>();
            try
            {
                if (_FileShareDatas != null && _FileShareDatas.Count > 0)
                {
                    ObservableCollection<RecentModel> chatModelList = RingIDViewModel.Instance.GetRecentModelListByID(_ContactID);
                    foreach (FileShareData fileShareData in _FileShareDatas)
                    {
                        RecentDTO recentDTO = ShowFileChatMessage(fileShareData);
                        list.Add(recentDTO);

                        List<RecentDTO> tempList = new List<RecentDTO>();
                        tempList.Add(recentDTO);
                        int i = 0;
                        Thread.Sleep(10);
                        while (!RecentLoadUtility.IsProcessed(tempList) && i < 1000)
                        {
                            Thread.Sleep(5);
                            i++;
                        }
                    }
                }

                UploadChatFile(list);
            }
            catch (Exception ex)
            {
                log.Error("Error: StartProcessing() ==>" + ex.Message + "\n" + ex.StackTrace);
                UploadChatFile(list);
            }
            finally
            {
            }
        }

        private void Dispatcher_Tick(int counter, bool initTick = false, object state = null)
        {
            try
            {
                if (state != null)
                {
                    RecentModel model = (RecentModel)state;
                    int temp = counter * 3;
                    model.Message.ViewModel.UploadPercentage = temp > 30 ? 30 : temp;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Dispatcher_Tick() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void UploadChatFile(List<RecentDTO> recentList)
        {
            try
            {
                ObservableCollection<RecentModel> chatModelList = RingIDViewModel.Instance.GetRecentModelListByID(_ContactID);
                for (int idx = 0; idx < recentList.Count; idx++)
                {
                    RecentDTO recentDTO = recentList.ElementAt(idx);
                    FileShareData fileShareData = _FileShareDatas[idx];

                    RecentModel model = chatModelList.Where(P => P.UniqueKey == recentDTO.Message.PacketID).FirstOrDefault();
                    if (model != null)
                    {
                        model.Message.ViewModel.IsUploadRunning = true;
                        model.Message.ViewModel.UploadPercentage = 1;
                    }

                    int widthOrFileSize = 0;
                    int heightOrDuration = 0;

                    if (recentDTO.Message.MessageType == (int)MessageType.IMAGE_FILE_FROM_GALLERY || recentDTO.Message.MessageType == (int)MessageType.IMAGE_FILE_FROM_CAMERA)
                    {
                        widthOrFileSize = recentDTO.Message.Width;
                        heightOrDuration = recentDTO.Message.Height;
                    }
                    else if (recentDTO.Message.MessageType == (int)MessageType.AUDIO_FILE)
                    {
                        string prevPath = fileShareData.FilePath;
                        fileShareData.FilePath = Path.ChangeExtension(fileShareData.FilePath, ".g729");
                        ChatHelpers.EncodeChatMediaAudioUsingOpus(prevPath, fileShareData.FilePath);

                        widthOrFileSize = (int)recentDTO.Message.FileSize;
                        heightOrDuration = recentDTO.Message.Duration;
                    }
                    else if (recentDTO.Message.MessageType == (int)MessageType.VIDEO_FILE)
                    {
                        ChatHelpers.EncodeChatMediaVideoUsingFFMPEG(fileShareData.FilePath, fileShareData.Duration);

                        FileInfo fi = new FileInfo(fileShareData.FilePath);
                        widthOrFileSize = fi.Exists ? (int)(fi.Length / 1024) : 0;
                        heightOrDuration = recentDTO.Message.Duration;

                        recentDTO.Message.FileSize = widthOrFileSize;
                        if (model != null) model.Message.FileSize = widthOrFileSize;
                    }

                    RecentChatCallActivityDAO.InsertMessageDTO(recentDTO.Message);

                    if (recentDTO.FriendTableID > 0)
                    {
                        ChatService.UploadFriendChatMedia(recentDTO.Message.FriendTableID, recentDTO.Message.PacketID, recentDTO.Message.MessageType, fileShareData.FilePath, recentDTO.Message.Caption, widthOrFileSize, heightOrDuration, recentDTO.Message.Timeout, recentDTO.Message.MessageDate, recentDTO.Message.IsSecretVisible == 1);
                    }
                    else if (recentDTO.GroupID > 0)
                    {
                        ChatService.UploadGroupChatMedia(recentDTO.Message.GroupID, recentDTO.Message.PacketID, recentDTO.Message.MessageType, fileShareData.FilePath, recentDTO.Message.Caption, widthOrFileSize, heightOrDuration, recentDTO.Message.MessageDate);
                    }
                    else if (!String.IsNullOrWhiteSpace(recentDTO.RoomID))
                    {
                        ChatFileDTO chatFileDTO = new ChatFileDTO();
                        chatFileDTO.FriendTableID = recentDTO.Message.FriendTableID;
                        chatFileDTO.GroupID = recentDTO.Message.GroupID;
                        chatFileDTO.RoomID = recentDTO.Message.RoomID;
                        chatFileDTO.Message = recentDTO.Message.Message;
                        chatFileDTO.MessageType = recentDTO.Message.MessageType;
                        chatFileDTO.PacketID = recentDTO.Message.PacketID;
                        chatFileDTO.MessageDTO = recentDTO.Message;
                        chatFileDTO.File = fileShareData.FilePath;
                        chatFileDTO.RecentModel = model;

                        if (chatFileDTO.MessageType == (int)MessageType.IMAGE_FILE_FROM_GALLERY || chatFileDTO.MessageType == (int)MessageType.IMAGE_FILE_FROM_CAMERA)
                        {
                            chatFileDTO.UploadUrl = ServerAndPortSettings.ChatImageUploadingUrl;
                        }
                        else if (chatFileDTO.MessageType == (int)MessageType.AUDIO_FILE)
                        {
                            chatFileDTO.UploadUrl = ServerAndPortSettings.ChatMP3UploadingUrl;
                        }
                        else if (chatFileDTO.MessageType == (int)MessageType.VIDEO_FILE)
                        {
                            chatFileDTO.UploadUrl = ServerAndPortSettings.ChatMP4UploadingUrl;
                        }

                        ChatService.UploadChatFile(chatFileDTO, new UploadProgressHandler());
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: UploadChatFile() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private RecentDTO ShowFileChatMessage(FileShareData fileShareData)
        {
            RecentDTO recentDTO = new RecentDTO();
            PacketTimeID packet = ChatService.GeneratePacketID();

            if (fileShareData.MessageType == (int)MessageType.IMAGE_FILE_FROM_GALLERY || fileShareData.MessageType == (int)MessageType.IMAGE_FILE_FROM_CAMERA)
            {
                string extension = Path.GetExtension(fileShareData.FilePath);

                string newPath = RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER + Path.DirectorySeparatorChar + packet.PacketID + ".jpg";
                int[] size = ChatHelpers.SaveChatImageFile(fileShareData.FilePath, newPath);

                string thumbPath = RingIDSettings.TEMP_CHAT_FILES_FOLDER + Path.DirectorySeparatorChar + packet.PacketID + ".jpg";
                File.Copy(newPath, thumbPath, true);

                fileShareData.FilePath = newPath;
                fileShareData.Width = size[0];
                fileShareData.Height = size[1];
            }
            else if (fileShareData.MessageType == (int)MessageType.AUDIO_FILE)
            {
                string path = RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER + Path.DirectorySeparatorChar + packet.PacketID + ".mp3";
                File.Move(fileShareData.FilePath, path);

                fileShareData.FilePath = path;
            }
            else if (fileShareData.MessageType == (int)MessageType.VIDEO_FILE)
            {
                string path = RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER + Path.DirectorySeparatorChar + packet.PacketID + ".mp4";
                File.Move(fileShareData.FilePath, path);

                string thumbPath = RingIDSettings.TEMP_CHAT_FILES_FOLDER + Path.DirectorySeparatorChar + packet.PacketID + ".jpg";
                MediaService.GetThumbnail(RingIDSettings.TEMP_MEDIA_TOOLKIT, path, thumbPath, 1);

                fileShareData.FilePath = path;
            }

            MessageDTO messageDTO = new MessageDTO();
            messageDTO.FriendTableID = _FriendTableID;
            messageDTO.GroupID = _GroupID;
            messageDTO.RoomID = _RoomID;
            messageDTO.MessageType = fileShareData.MessageType;
            if (messageDTO.MessageType == (int)MessageType.IMAGE_FILE_FROM_GALLERY || messageDTO.MessageType == (int)MessageType.IMAGE_FILE_FROM_CAMERA)
            {
                messageDTO.OriginalMessage = ChatJSONParser.MakeImageMessage(String.Empty, fileShareData.Caption, fileShareData.Width, fileShareData.Height);
            }
            else if (messageDTO.MessageType == (int)MessageType.AUDIO_FILE || messageDTO.MessageType == (int)MessageType.VIDEO_FILE)
            {
                messageDTO.OriginalMessage = ChatJSONParser.MakeMultimediaMessage(String.Empty, fileShareData.FileSize, fileShareData.Duration);
            }
            messageDTO.Timeout = fileShareData.TimeOut;
            messageDTO.IsSecretVisible = fileShareData.IsSecretVisible;
            messageDTO.PacketID = packet.PacketID;
            messageDTO.MessageDate = packet.Time;
            messageDTO.Status = ChatConstants.STATUS_SENDING;
            messageDTO.SenderTableID = DefaultSettings.LOGIN_TABLE_ID;

            if (_FriendTableID > 0)
            {
                messageDTO.PacketType = ChatConstants.PACKET_TYPE_FRIEND_MESSAGE;
                ChatJSONParser.ParseMessage(messageDTO);
                RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                recentDTO = new RecentDTO { ContactID = _FriendTableID.ToString(), FriendTableID = _FriendTableID, Message = messageDTO, Time = messageDTO.MessageDate, IsMoveToButtom = true };
                RecentLoadUtility.LoadRecentData(recentDTO);
                ChatLogLoadUtility.LoadRecentData(new RecentDTO { ContactID = _FriendTableID.ToString(), FriendTableID = _FriendTableID, Message = messageDTO, Time = messageDTO.MessageDate });
            }
            else if (_GroupID > 0)
            {
                messageDTO.PacketType = ChatConstants.PACKET_TYPE_GROUP_MESSAGE;
                ChatJSONParser.ParseMessage(messageDTO);
                RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                recentDTO = new RecentDTO { ContactID = _GroupID.ToString(), GroupID = _GroupID, Message = messageDTO, Time = messageDTO.MessageDate, IsMoveToButtom = true };
                RecentLoadUtility.LoadRecentData(recentDTO);
                ChatLogLoadUtility.LoadRecentData(new RecentDTO { ContactID = _GroupID.ToString(), GroupID = _GroupID, Message = messageDTO, Time = messageDTO.MessageDate });
            }
            else
            {
                messageDTO.PacketType = ChatConstants.PACKET_TYPE_ROOM_MESSAGE;
                messageDTO.FullName = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName;
                messageDTO.ProfileImage = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.ProfileImage;
                ChatJSONParser.ParseMessage(messageDTO);
                RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                recentDTO = new RecentDTO { ContactID = _RoomID, RoomID = _RoomID, Message = messageDTO, Time = messageDTO.MessageDate, IsMoveToButtom = true };
                RecentLoadUtility.LoadRecentData(recentDTO);
                ChatLogLoadUtility.LoadRecentData(new RecentDTO { ContactID = _RoomID, RoomID = _RoomID, Message = messageDTO, Time = messageDTO.MessageDate });
            }

            return recentDTO;
        }
    }

    public class FileShareData
    {
        public string FilePath { get; set; }
        public string Caption { get; set; }
        public int TimeOut { get; set; }
        public int IsSecretVisible { get; set; }
        public int MessageType { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Duration { get; set; }
        public long FileSize { get; set; }
        public CustomizeTimer Timer { get; set; }
    }

}

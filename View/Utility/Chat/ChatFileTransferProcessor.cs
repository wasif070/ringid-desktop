﻿using Auth.utility;
using imsdkwrapper;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Utility;
using Models.Utility.Chat;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using View.BindingModels;
using View.Constants;
using View.Utility.Chat.Service;
using View.Utility.Recent;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.Utility.Chat
{
    class ChatFileTransferProcessor
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(ChatFileTransferProcessor).Name);

        private long _FriendTableID;
        private long _GroupID;
        private string _RoomID;
        private List<string> _FileToUploadList;

        public ChatFileTransferProcessor(long friendIdentity, long groupID, string roomID, List<string> fileToUploadList)
        {
            this._FriendTableID = friendIdentity;
            this._GroupID = groupID;
            this._RoomID = roomID;
            this._FileToUploadList = fileToUploadList;
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Run));
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            try
            {
                BuildChatMessage();
            }
            catch (Exception ex)
            {
                log.Error("Error: Run() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void BuildChatMessage()
        {
            long messageDate = ModelUtility.CurrentTimeMillis();

            if (_FileToUploadList != null && _FileToUploadList.Count > 0)
            {
                foreach (string filePath in _FileToUploadList)
                {
                    MessageDTO messageDTO = new MessageDTO();
                    messageDTO.MessageType = (int)MessageType.FILE_STREAM;
                    PacketTimeID packet = ChatService.GeneratePacketID(messageDate++);
                    messageDTO.PacketID = packet.PacketID;
                    messageDTO.MessageDate = packet.Time;
                    messageDTO.GroupID = _GroupID;
                    messageDTO.FriendTableID = _FriendTableID;
                    messageDTO.RoomID = _RoomID;
                    messageDTO.FileID = PacketIDGenerator.GetUnixTimestamp(packet.PacketID);
                    messageDTO.Status = ChatConstants.STATUS_SENDING;
                    messageDTO.SenderTableID = DefaultSettings.LOGIN_TABLE_ID;
                    FileInfo fi = new FileInfo(filePath);
                    if (!fi.Exists) continue;
                    messageDTO.OriginalMessage = ChatJSONParser.MakeStreamMessage(filePath, fi.Length, messageDTO.FileID);

                    if (_FriendTableID > 0)
                    {
                        messageDTO.PacketType = ChatConstants.PACKET_TYPE_FRIEND_MESSAGE;

                        ChatJSONParser.ParseMessage(messageDTO);
                        RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);
                        RecentLoadUtility.LoadRecentData(new RecentDTO { ContactID = _FriendTableID.ToString(), FriendTableID = _FriendTableID, Message = messageDTO, Time = messageDTO.MessageDate, IsMoveToButtom = true });
                        ChatLogLoadUtility.LoadRecentData(new RecentDTO { ContactID = _FriendTableID.ToString(), FriendTableID = _FriendTableID, Message = messageDTO, Time = messageDTO.MessageDate });
                        ChatService.SendFriendChat(messageDTO.PacketID, messageDTO.FriendTableID, messageDTO.MessageType, messageDTO.Timeout, messageDTO.OriginalMessage, messageDTO.MessageDate, messageDTO.IsSecretVisible, null, null);
                    }
                    else if (_GroupID > 0)
                    {
                        messageDTO.PacketType = ChatConstants.PACKET_TYPE_GROUP_MESSAGE;

                        ChatJSONParser.ParseMessage(messageDTO);
                        RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);
                        RecentLoadUtility.LoadRecentData(new RecentDTO { ContactID = _GroupID.ToString(), GroupID = _GroupID, Message = messageDTO, Time = messageDTO.MessageDate, IsMoveToButtom = true });
                        ChatLogLoadUtility.LoadRecentData(new RecentDTO { ContactID = _GroupID.ToString(), GroupID = _GroupID, Message = messageDTO, Time = messageDTO.MessageDate });
                        ChatService.SendGroupChat(messageDTO.PacketID, messageDTO.GroupID, messageDTO.MessageType, messageDTO.OriginalMessage, messageDTO.MessageDate);
                    }
                    else if (!String.IsNullOrWhiteSpace(_RoomID))
                    {
                        messageDTO.PacketType = ChatConstants.PACKET_TYPE_ROOM_MESSAGE;
                        messageDTO.FullName = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName;
                        messageDTO.ProfileImage = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.ProfileImage;

                        ChatJSONParser.ParseMessage(messageDTO);
                        RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);
                        RecentLoadUtility.LoadRecentData(new RecentDTO { ContactID = _RoomID, RoomID = _RoomID, Message = messageDTO, Time = messageDTO.MessageDate, IsMoveToButtom = true });
                        ChatLogLoadUtility.LoadRecentData(new RecentDTO { ContactID = _RoomID, RoomID = _RoomID, Message = messageDTO, Time = messageDTO.MessageDate });
                        ChatService.SendPublicRoomChat(messageDTO.PacketID, messageDTO.RoomID, messageDTO.MessageType, messageDTO.OriginalMessage, messageDTO.MessageDate, messageDTO.FullName, messageDTO.ProfileImage);
                    }
                }
            }
        }

    }
}

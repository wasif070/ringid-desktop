﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using View.BindingModels;
using View.Constants;

namespace View.Utility.Chat
{
    public class DateEventProcessor
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(DateEventProcessor).Name);
        private DateEventDTO _EventDTO;
        private UserBasicInfoModel _FriendBasicInfoModel;
        private GroupInfoModel _GroupInfoModel;
        private RoomModel _RoomModel;
        private WebClient _Downloader;

        public DateEventProcessor(UserBasicInfoModel friendBasicInfoModel, DateEventDTO eventDTO)
        {
            this._FriendBasicInfoModel = friendBasicInfoModel;
            this._EventDTO = eventDTO;
        }

        public DateEventProcessor(GroupInfoModel groupInfoModel, DateEventDTO eventDTO)
        {
            this._GroupInfoModel = groupInfoModel;
            this._EventDTO = eventDTO;
        }

        public DateEventProcessor(RoomModel roomModel, DateEventDTO eventDTO)
        {
            this._RoomModel = roomModel;
            this._EventDTO = eventDTO;
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Run));
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            try
            {
                DateEventDTO dateEventDTO = ChatDictionaries.Instance.DATE_EVENT_DICTIONARY.TryGetValue(_EventDTO.Day + "-" + _EventDTO.Month);
                if (dateEventDTO != null && !String.IsNullOrWhiteSpace(dateEventDTO.Url))
                {
                    string eventChatBgPath = RingIDSettings.TEMP_CHAT_BACKGROUND_FOLDER + Path.DirectorySeparatorChar + dateEventDTO.Url;
                    if (File.Exists(eventChatBgPath))
                    {
                        if (_FriendBasicInfoModel != null)
                        {
                            _FriendBasicInfoModel.EventChatBgUrl = dateEventDTO.Url;
                        }
                        else if (_GroupInfoModel != null)
                        {
                            _GroupInfoModel.EventChatBgUrl = dateEventDTO.Url;
                        }
                        else if (_RoomModel != null)
                        {
                            _RoomModel.EventChatBgUrl = dateEventDTO.Url;
                        }
                    }
                    else
                    {
                        _Downloader = new WebClient();
                        _Downloader.Headers.Set("x-app-version", AppConfig.VERSION_PC);
                        _Downloader.DownloadFileCompleted += (s, e) => 
                        {
                            if (e.Cancelled == false && e.Error == null)
                            {
                                if (_FriendBasicInfoModel != null)
                                {
                                    _FriendBasicInfoModel.EventChatBgUrl = dateEventDTO.Url;
                                }
                                else if (_GroupInfoModel != null)
                                {
                                    _GroupInfoModel.EventChatBgUrl = dateEventDTO.Url;
                                }
                                else if (_RoomModel != null)
                                {
                                    _RoomModel.EventChatBgUrl = dateEventDTO.Url;
                                }
                            }

                            if (_Downloader != null)
                            {
                                _Downloader.Dispose();
                            }
                        };
                        string downloadUrl = ServerAndPortSettings.ChatBackgroundImageDownloadUrl + Path.AltDirectorySeparatorChar + ServerAndPortSettings.D_FULL + Path.AltDirectorySeparatorChar + dateEventDTO.Url;
                        _Downloader.DownloadFileAsync(new Uri(downloadUrl), eventChatBgPath);
                    }
                }
                else
                {
                    if (_FriendBasicInfoModel != null)
                    {
                        _FriendBasicInfoModel.EventChatBgUrl = null;
                    }
                    else if (_GroupInfoModel != null)
                    {
                        _GroupInfoModel.EventChatBgUrl = null;
                    }
                    else if (_RoomModel != null)
                    {
                        _RoomModel.EventChatBgUrl = null;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: Run() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }
    }

}

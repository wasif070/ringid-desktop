﻿using HtmlAgilityPack;
using imsdkwrapper;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Utility;
using Models.Utility.Chat;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using View.BindingModels;
using  View.Utility.Chat.Service;
using View.Utility.Recent;
using View.ViewModel;

namespace View.Utility.Chat
{
    public class ChatLinkProcessor
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChatLinkProcessor).Name);

        private long _FriendTableID;
        private long _GroupID;
        private string _Link;
        private string _RoomID;
        private string _ContactID = String.Empty;
        private MessageDTO _MessageDTO;

        public ChatLinkProcessor(long friendTableID, long groupID, string roomID, string link, MessageDTO messageDTO)
        {
            this._FriendTableID = friendTableID;
            this._GroupID = groupID;
            this._RoomID = roomID;
           
            if (_FriendTableID > 0)
            {
                this._ContactID = _FriendTableID.ToString();
            }
            else if (_GroupID > 0)
            {
                this._ContactID = _GroupID.ToString();
            }
            else
            {
                this._ContactID = _RoomID.ToString();
            }

            this._Link = link;
            this._MessageDTO = messageDTO;

            Uri uri;
            if (!(Uri.TryCreate(_Link, UriKind.Absolute, out uri) && (uri.Scheme == Uri.UriSchemeHttp || uri.Scheme == Uri.UriSchemeHttps || uri.Scheme == Uri.UriSchemeFtp)))
            {
                _Link = "http://" + _Link;
            }
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Run));
            t.Name = this.GetType().Name;
            t.Start();
        }

        private void Run()
        {
            ChatJSONParser.ParseMessage(_MessageDTO);
            RecentDTO recentDTO = new RecentDTO();
            recentDTO.FriendTableID = _FriendTableID;
            recentDTO.GroupID = _GroupID;
            recentDTO.RoomID = _RoomID;            
            recentDTO.ContactID = _ContactID;
            recentDTO.IsMoveToButtom = true;
            recentDTO.Time = _MessageDTO.MessageDate;
            recentDTO.Message = _MessageDTO;
            RecentLoadUtility.LoadRecentData(recentDTO);

            LinkInformationDTO linkInformation = HelperMethods.FetchDynamicObject(_Link);
            if (!String.IsNullOrWhiteSpace(linkInformation.Title))
            {
                List<RecentDTO> tempList = new List<RecentDTO>();
                tempList.Add(recentDTO);
                while (!RecentLoadUtility.IsProcessed(tempList))
                {
                    Thread.Sleep(10);
                }

                _MessageDTO.OriginalMessage = ChatJSONParser.MakeLinkMessage(_MessageDTO.OriginalMessage, linkInformation.Url, linkInformation.Title, (linkInformation.ImageUrls != null && linkInformation.ImageUrls.Count > 0 ? linkInformation.ImageUrls.FirstOrDefault() : String.Empty), linkInformation.Description);
                _MessageDTO.MessageType = (int)MessageType.LINK_MESSAGE;
                ChatJSONParser.ParseMessage(_MessageDTO);
                RecentChatCallActivityDAO.InsertMessageDTO(_MessageDTO);

                recentDTO.Message = _MessageDTO;
                ChatLogLoadUtility.LoadRecentData(recentDTO);
                ObservableCollection<RecentModel> chatModelList = RingIDViewModel.Instance.GetRecentModelListByID(_ContactID);
                RecentModel chatModel = chatModelList.Where(P => P.UniqueKey == _MessageDTO.PacketID).FirstOrDefault();
                if (chatModel != null)
                {
                    chatModel.IsRecentOpened = false;
                    chatModel.Message.IsMessageOpened = false;
                    chatModel.Message.IsPreviewOpened = false;
                    chatModel.Message.LoadData(_MessageDTO);
                    chatModel.IsRecentOpened = true;
                    chatModel.Message.IsMessageOpened = true;
                }
            }
            else
            {
                RecentChatCallActivityDAO.InsertMessageDTO(_MessageDTO);
                ChatLogLoadUtility.LoadRecentData(recentDTO);
            }

            SendMessage();
        }

        private void SendMessage()
        {
            if (_FriendTableID > 0)
            {
                ChatService.SendFriendChat(_MessageDTO.PacketID, _MessageDTO.FriendTableID, _MessageDTO.MessageType, _MessageDTO.Timeout, _MessageDTO.OriginalMessage, _MessageDTO.MessageDate, _MessageDTO.IsSecretVisible, null, null);
            }
            else if (_GroupID > 0)
            {
                ChatService.SendGroupChat(_MessageDTO.PacketID, _MessageDTO.GroupID, _MessageDTO.MessageType, _MessageDTO.OriginalMessage, _MessageDTO.MessageDate);
            }
            else
            {
                ChatService.SendPublicRoomChat(_MessageDTO.PacketID, _MessageDTO.RoomID, _MessageDTO.MessageType, _MessageDTO.OriginalMessage, _MessageDTO.MessageDate, _MessageDTO.FullName, _MessageDTO.ProfileImage);
            }
        }

    }
}

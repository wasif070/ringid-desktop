﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace View.Utility.Chat
{
    public interface IUploadProgress
    {
        void OnSuccess(ChatFileDTO chatFileDTO);

        void OnProgress(int percentage, ChatFileDTO chatFileDTO);

        void OnFailed(ChatFileDTO chatFileDTO);
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using imsdkwrapper;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Models.Utility.Chat;
using View.BindingModels;
using View.ViewModel;
using View.Constants;
using FileTrasnferSDKCLI;
using View.Utility.Stream;
using System.Diagnostics;
using View.Utility.Call;
using System.Windows;
using View.UI;

namespace View.Utility.Chat.Service
{
    public static class ChatService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChatService).Name);

        private static BaseChatService baseChatService = null;
        private static FTSDKService fileTransferService = null;
        private static InternetCheckService internetCheckService = null;
        public static ChatHandler CHAT_HANDLER = new ChatHandler();
        public static FileTrasnferHandler FILE_TRANSFER_HANDLER = new FileTrasnferHandler();

        #region Common

        public static void Initialize(long userId, string userName, string offlineServerAddress, int offlineServerPort, string authenticationServerAddress, int authenticationServerPort, int deviceType, string appSessionId, int appVersion, List<string> directoryList, string baseUploadUrl, string baseDownloadUrl, string key)
        {
            try
            {
#if CHAT_LOG
                log.Debug("SERVICE::COMMON::INITIALIZE".PadRight(72, ' ') + "==>   packetId = " + userId + ", userName = " + userName + ", offlineServerAddress = " + offlineServerAddress + ", offlineServerPort = " + offlineServerPort + ", authServerAddress = " + authenticationServerAddress + ", authServerPort = " + authenticationServerPort + ", deviceType = " + deviceType + ", appSessionId = " + appSessionId + ", appVersion = " + appVersion + ", directoryList = " + directoryList.ToString(",") + ", baseUploadUrl = " + baseUploadUrl + ", baseDownloadUrl = " + baseDownloadUrl + ", key = " + key);
#endif
                log.Debug("*********  Initialize 1 *******");
                if (!String.IsNullOrWhiteSpace(key))
                {
                    log.Debug("*********  Initialize 2 *******");
                    if (baseChatService == null)
                    {
                        log.Debug("*********  Initialize 3 *******");
                        baseChatService = new BaseChatService(userId, userName ?? String.Empty, offlineServerAddress ?? String.Empty, offlineServerPort, authenticationServerAddress ?? String.Empty, authenticationServerPort, deviceType, appSessionId ?? String.Empty, appVersion, 0, directoryList ?? new List<String>(), baseUploadUrl ?? String.Empty, baseDownloadUrl ?? String.Empty, false, key ?? String.Empty);
                        log.Debug("*********  Initialize 4 *******");
                        baseChatService.registerEventHandler(CHAT_HANDLER);
                        log.Debug("*********  Initialize 5 *******");
                        //ChatService.SetLogfile(RingIDSettings.APP_FOLDER + Path.DirectorySeparatorChar + "logs" + Path.DirectorySeparatorChar + "ChatWrapper.log");
                    }
                    else
                    {
                        log.Debug("*********  Initialize 6 *******");
                        baseChatService.reinitialize(userId, userName ?? String.Empty, offlineServerAddress ?? String.Empty, offlineServerPort, authenticationServerAddress ?? String.Empty, authenticationServerPort, deviceType, appSessionId ?? String.Empty, appVersion, 0, directoryList ?? new List<String>(), false, key ?? String.Empty);
                    }
                    log.Debug("*********  Initialize 7 *******");
                }

                log.Debug("*********  Initialize 8 *******");
                if (fileTransferService == null)
                {
                    log.Debug("*********  Initialize 9 *******");
                    fileTransferService = new FTSDKService(FILE_TRANSFER_HANDLER);
                    log.Debug("*********  Initialize 10 *******");
                    fileTransferService.Init(DefaultSettings.LOGIN_TABLE_ID, RingIDSettings.APP_FOLDER + Path.DirectorySeparatorChar + "logs" + Path.DirectorySeparatorChar + "FileTransferSDK.log", 5);
                }
                log.Debug("*********  Initialize 11 *******");
                fileTransferService.SetCloudInformation(ServerAndPortSettings.ChatSharedFileUploadingUrl, ServerAndPortSettings.ChatSharedFileDownloadingUrl, appSessionId ?? String.Empty, userId.ToString(), authenticationServerAddress ?? String.Empty, authenticationServerPort.ToString(), appVersion.ToString(), RingIDSettings.TEMP_CHAT_FILES_FOLDER);
                log.Debug("*********  Initialize 12 *******");
            }
            catch (Exception ex)
            {
                log.Error("Error at Initialize() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void Close()
        {
            try
            {
#if CHAT_LOG
                log.Debug("SERVICE::COMMON::CLOSE".PadRight(72, ' ') + "==>   (void)");
#endif
                if (baseChatService != null)
                {
                    baseChatService.unregisterEventHandler();
                    baseChatService.cleanUp(StatusConstants.MOOD_ALIVE);
                    baseChatService.Dispose();
                    baseChatService = null;
                }

                if (fileTransferService != null)
                {
                    fileTransferService.Dispose();
                    fileTransferService = null;
                }

                if (internetCheckService != null)
                {
                    internetCheckService.Dispose();
                    internetCheckService = null;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at UnRegisterEventHandler() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void SetLogfile(string fileName)
        {
            try
            {
                if (baseChatService != null)
                {
                    string dirName = Path.GetDirectoryName(fileName);
                    var dir = new DirectoryInfo(dirName);
                    if (!dir.Exists)
                    {
                        dir.Create();
                    }
                    baseChatService.setLogEnabled(true);
                    baseChatService.setLogfile(fileName);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at SetLogfile() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void UpdateOfflineIPAndPort(string offlineServerAddress, int offlineServerPort)
        {
            try
            {
#if CHAT_LOG
                log.Debug("SERVICE::COMMON::UPDATE_OFFLINE_IP_PORT".PadRight(72, ' ') + "==>   offlineServerAddress = " + offlineServerAddress + ", offlineServerPort = " + offlineServerPort);
#endif
                if (baseChatService != null)
                {
                    baseChatService.setOfflineServer(offlineServerAddress ?? String.Empty, offlineServerPort);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at UpdateOfflineIPAndPort() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void UpdateAuthenticationIPAndPort(string authenticationServerAddress, int authenticationServerPort)
        {
            try
            {
#if CHAT_LOG
                log.Debug("SERVICE::COMMON::UPDATE_AUTH_IP_PORT".PadRight(72, ' ') + "==>   authServerAddress = " + authenticationServerAddress + ", authServerPort = " + authenticationServerPort);
#endif
                if (baseChatService != null)
                {
                    baseChatService.setAuthenticationServer(authenticationServerAddress ?? String.Empty, authenticationServerPort);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at UpdateAuthenticationIPAndPort() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static PacketTimeID GeneratePacketID()
        {
            PacketTimeID packet = new PacketTimeID();
            try
            {
                if (baseChatService != null)
                {
                    BasePacketTimeID data = baseChatService.generatePacketId();
                    packet.PacketID = data.PacketID;
                    packet.Time = data.PacketTime1970;
                }
                else
                {
                    packet.Time = ModelUtility.CurrentTimeMillis();
                    packet.PacketID = PacketIDGenerator.GeneratedPacketIDByFixTimeAndIdentity(packet.Time, DefaultSettings.LOGIN_TABLE_ID);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GeneratePacketID() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return packet;
        }

        public static PacketTimeID GeneratePacketID(long timeMili)
        {
            PacketTimeID packet = new PacketTimeID();
            try
            {
                if (baseChatService != null)
                {
                    BasePacketTimeID data = baseChatService.generatePacketId(timeMili, DefaultSettings.LOGIN_TABLE_ID);
                    packet.PacketID = data.PacketID;
                    packet.Time = data.PacketTime1970;
                }
                else
                {
                    packet.Time = timeMili;
                    packet.PacketID = PacketIDGenerator.GeneratedPacketIDByFixTimeAndIdentity(packet.Time, DefaultSettings.LOGIN_TABLE_ID);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GeneratePacketID(timeMili, userId) ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return packet;
        }

        public static BaseApiStatus RequestOffline(long messageDate, long blockUpdateDate)
        {
            BaseApiStatus data = null;
            try
            {
#if CHAT_LOG
                log.Debug("SERVICE::COMMON::REQUEST_OFFLINE".PadRight(72, ' ') + "==>   messageDate = " + messageDate + ", blockUpdateDate = " + blockUpdateDate);
#endif
                if (baseChatService != null)
                {
                    data = baseChatService.requestForOffline(messageDate, blockUpdateDate);
                }
                else
                {
                    log.Debug("RequestOffline Failed => baseChatService = NULL");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at RequestOffline() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return data;
        }

        public static void GetConversationList(ConversationType conversationType, int startIndex, int limit, long friendOrGroupId, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::COMMON::GET_CONVERSATION_LIST".PadRight(72, ' ') + "==>   conversationType = " + conversationType + ", startIndex = " + startIndex + ", limit = " + limit + ", friendOrGroupId = " + friendOrGroupId);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.getConversationList(conversationType, startIndex, limit, friendOrGroupId);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID });
                        }
                        else
                        {
                            log.Error("GetConversationList Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("GetConversationList Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                    }
                }
                else
                {
                    log.Error("GetConversationList Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetConversationList() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
            }
        }

        public static void DeleteConversation(List<BaseConversationDTO> conversationList, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::COMMON::DELETE_CONVERSATION".PadRight(72, ' ') + "==>   conversationList = " + conversationList.ToString("\n"));
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.deleteConversation(conversationList);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID });
                        }
                        else
                        {
                            log.Error("DeleteConversation Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("DeleteConversation Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                    }
                }
                else
                {
                    log.Error("DeleteConversation Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at DeleteConversation() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
            }
        }

        public static long GetServerTime()
        {
            try
            {
                if (baseChatService != null)
                {
                    return baseChatService.getCurrentServerSyncedTime();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetServerTime() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return ModelUtility.CurrentTimeMillis();
        }

        public static void ClearAllRegistrations()
        {
            try
            {
                log.Debug("SERVICE::COMMON::CLEAR_ALL_REGISTRATIONS".PadRight(72, ' ') + "==>   IsChatServiceExist = " + (baseChatService != null));

                if (baseChatService != null)
                {
                    baseChatService.clearRegisteredSessions(StatusConstants.MOOD_ALIVE);
                }
                else
                {
                    log.Debug("ClearAllRegistrations Failed => baseChatService = NULL");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at ClearAllRegistrations() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static bool CheckInternetConnection()
        {
            try
            {
                if (internetCheckService == null)
                {
                    internetCheckService = new InternetCheckService(RingIDSettings.APP_FOLDER);
                }
                return internetCheckService.CheckInternetConnection();
            }
            catch (Exception ex)
            {
                log.Error("Error at CheckInternetConnection() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return false;
        }

        public static void UpdateUserPassword(string password)
        {
            try
            {
                if (baseChatService != null)
                {
#if CHAT_LOG
                    log.Debug("SERVICE::STREAM::CHAT::UPDATE_KEY".PadRight(72, ' ') + "==>   key = " + password);
#endif
                    baseChatService.updateUserPassword(password);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at UpdateUserPassword() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Common

        #region Register Friend

        public static bool StartFriendChat(long friendID, UserBasicInfoModel friendInfoModel, BlockedNonFriendModel blockModel, ChatEventDelegate onComplete = null, bool forceFully = false, bool checkPermission = true)
        {
            if (checkPermission && !HelperMethods.HasAnonymousAndFriendChatPermission(friendInfoModel, blockModel))
            {
#if CHAT_LOG
                log.Debug("SERVICE::FRIEND::ANONYMOUS_AND_FRIEND_CHAT_PERMISSION".PadRight(72, ' ') + "==>   FALSE");
#endif
                ChatService.DontSendPendingMessages(friendID);
                ChatService.UnRegisterFriendChat(friendID, StatusConstants.PRESENCE_ONLINE, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.Mood);

                InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendID, Status = false });
                return false;
            }

            if (forceFully == false && ChatService.HasFriendRegistration(friendID))
            {
                InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendID, Status = true });
                return false;
            }

#if CHAT_LOG
            log.Debug("SERVICE::FRIEND::LOCK_REG_EVENT".PadRight(51, ' ') + "==>   ChatEventHandler.CHAT_REG_EVENT_LOCKER");
#endif
            lock (ChatEventHandler.CHAT_REG_EVENT_LOCKER)
            {
                ChatEventHandler eventhandler = ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT.TryGetValue(friendID.ToString());
                if (eventhandler == null)
                {
                    eventhandler = new ChatEventHandler { FriendTableID = friendID };
                    ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT[friendID.ToString()] = eventhandler;
#if CHAT_LOG
                    log.Debug("SERVICE::FRIEND::NEW_REGISTRATION".PadRight(72, ' ') + "==>   friendId = " + friendID);
#endif
                    new ThradChatFriendStartResender(friendID, (fndId, status) => { if (!status) { CHAT_HANDLER.onFriendChatRegisterFailure(fndId, null, false); } return 0; }).StartThread();
                }
                else
                {
#if CHAT_LOG
                    log.Debug("SERVICE::FRIEND::REGISTRATION_ALREADY_RUNNING".PadRight(72, ' ') + "==>   friendId = " + friendID);
#endif
                }

                if (onComplete != null)
                {
                    eventhandler.OnCompleted += onComplete;
                }
            }
#if CHAT_LOG
            log.Debug("SERVICE::FRIEND::UNLOCK_REG_EVENT".PadRight(51, ' ') + "==>   ChatEventHandler.CHAT_REG_EVENT_LOCKER");
#endif
            return true;
        }

        public static BaseApiStatus RegisterFriendChat(long friendId, string friendName, string friendUrl, string registerServerAddress, int registerServerPort, int deviceType, string deviceToken, int appType, int presence, int mood, long authRegistrationTime, int iosVoipPush)
        {
            BaseApiStatus data = null;
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
                    //registerServerAddress = "192.168.1.251";
                    //registerServerPort = 1500;
#if CHAT_LOG
                    log.Debug("SERVICE::FRIEND::REGISTER_SENDING".PadRight(65, ' ') + "==>   friendId = " + friendId + ", registerIP = " + registerServerAddress + ", registerPort = " + registerServerPort + ", presence = " + presence + ", deviceType = " + deviceType + ", iosVoipPush = " + iosVoipPush + ", registrationTime = " + authRegistrationTime);

#endif
                    if (baseChatService != null)
                    {
                        data = baseChatService.registerFriendChat(friendId, friendName ?? "", friendUrl ?? "", registerServerAddress ?? "", registerServerPort, deviceType, deviceToken ?? "", appType, presence, mood, iosVoipPush, authRegistrationTime);
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("RegisterFriendChat Failed ==> ChatError = " + data.ErrorCode);
                            CHAT_HANDLER.onFriendChatRegisterFailure(friendId, null, false);
                        }
                        else if (presence != (int)OnlineStatus.ONLINE)
                        {
                            CHAT_HANDLER.onFriendChatRegisterSuccess(friendId, 0, null, false);
                        }
                    }
                    else
                    {
                        log.Debug("RegisterFriendChat Failed => baseChatService = NULL");
                        CHAT_HANDLER.onFriendChatRegisterFailure(friendId, null, false);
                    }
                }
                else
                {
                    log.Error("RegisterFriendChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    CHAT_HANDLER.onFriendChatRegisterFailure(friendId, null, false);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at RegisterFriendChat() ==>" + ex.Message + "\n" + ex.StackTrace);
                CHAT_HANDLER.onFriendChatRegisterFailure(friendId, null, false);
            }
            return data;
        }

        public static BaseApiStatus UnRegisterFriendChat(long friendId, int presence, int mood)
        {
            BaseApiStatus data = null;
            try
            {
#if CHAT_LOG
                log.Debug("SERVICE::FRIEND::UNREGISTER_SENDING".PadRight(72, ' ') + "==>   friendId = " + friendId + ", presence = " + presence + ", mood = " + mood);
#endif
                if (baseChatService != null)
                {
                    data = baseChatService.unregisterFriendChat(friendId, presence, mood);
                    if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                    {
                        log.Error("UnRegisterFriendChat Failed ==> ChatError = " + data.ErrorCode);
                    }
                }
                else
                {
                    log.Debug("UnRegisterFriendChat Failed => baseChatService = NULL");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at UnRegisterFriendChat() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return data;
        }

        public static BaseFriendInformation GetFriendRegisterInfo(long friendId)
        {
            BaseFriendInformation data = null;
            try
            {
                if (baseChatService != null)
                {
                    data = baseChatService.getFriendInformation(friendId);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetFriendRegisterInfo() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return data;
        }

        public static List<long> GetFriendRegisterList()
        {
            List<long> data = null;
            try
            {
                if (baseChatService != null)
                {
                    data = baseChatService.getRegisteredFriendsList();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetFriendRegisterInfo() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return data;
        }

        public static bool HasFriendRegistration(long friendId)
        {
            bool data = false;
            try
            {
                if (baseChatService != null)
                {
                    data = baseChatService.isExistsFriendConnection(friendId);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at HasFriendRegistration() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return data;
        }

        public static BaseApiStatus UpdateFriendRegisterServer(long friendId, string registerServerAddress, int registerServerPort)
        {
            BaseApiStatus data = null;
            try
            {
#if CHAT_LOG
                log.Debug("SERVICE::FRIEND::UPDATE_REGISTER_SERVER".PadRight(72, ' ') + "==>   friendId = " + friendId + ", registerServerAddress = " + registerServerAddress + ", registerServerPort = " + registerServerPort);
#endif
                if (baseChatService != null)
                {
                    data = baseChatService.updateFriendRegisterServer(friendId, registerServerAddress ?? String.Empty, registerServerPort);
                }
                else
                {
                    log.Debug("UpdateFriendRegisterServer Failed => baseChatService = NULL");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at UpdateFriendRegisterServer() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return data;
        }

        public static BaseApiStatus UpdateFriendPresenceAndMood(long friendId, int presence, int mood)
        {
            BaseApiStatus data = null;
            try
            {
#if CHAT_LOG
                log.Debug("SERVICE::FRIEND::UPDATE_PRESENCE_MOOD".PadRight(72, ' ') + "==>   friendId = " + friendId + ", presence = " + presence + ", mood = " + mood);
#endif
                if (baseChatService != null)
                {
                    data = baseChatService.updateFriendStatusAndMood(friendId, presence, mood);
                }
                else
                {
                    log.Debug("UpdateFriendPresenceAndMood Failed => baseChatService = NULL");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at UpdateFriendPresenceAndMood() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return data;
        }

        #endregion Register Friend

        #region Register Group

        public static bool StartGroupChat(long groupId, GroupInfoModel groupInfoModel = null, ChatEventDelegate onComplete = null, bool forceFully = false)
        {
            if (forceFully == false && ChatService.HasGroupRegistration(groupId))
            {
                InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = true });
                return false;
            }

            List<long> memberList = ChatHelpers.GetGroupMemberIDList(groupId, groupInfoModel);
            if (!memberList.Contains(DefaultSettings.LOGIN_TABLE_ID))
            {
                log.Debug("SERVICE::GROUP::NOT_GROUP_MEMBER".PadRight(72, ' ') + "==>   groupId = " + groupId);
                InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                return false;
            }

#if CHAT_LOG
            log.Debug("SERVICE::GROUP::LOCK_REG_EVENT".PadRight(51, ' ') + "==>   ChatEventHandler.CHAT_REG_EVENT_LOCKER");
#endif
            lock (ChatEventHandler.CHAT_REG_EVENT_LOCKER)
            {
                ChatEventHandler eventhandler = ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT.TryGetValue(groupId.ToString());
                if (eventhandler == null)
                {
                    eventhandler = new ChatEventHandler { GroupID = groupId };
                    ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT[groupId.ToString()] = eventhandler;
                    memberList.Remove(DefaultSettings.LOGIN_TABLE_ID);
#if CHAT_LOG
                    log.Debug("SERVICE::GROUP::NEW_REGISTRATION".PadRight(72, ' ') + "==>   groupId = " + groupId);
#endif
                    new ThradChatGroupStartResender(groupId, memberList, (grpId, status) => { if (!status) { CHAT_HANDLER.onGroupChatRegisterFailure(groupId, null); } return 0; }).Start();
                }
                else
                {
#if CHAT_LOG
                    log.Debug("SERVICE::GROUP::REGISTRATION_ALREADY_RUNNING".PadRight(72, ' ') + "==>   groupId = " + groupId);
#endif
                }

                if (onComplete != null)
                {
                    eventhandler.OnCompleted += onComplete;
                }
            }
#if CHAT_LOG
            log.Debug("SERVICE::GROUP::UNLOCK_REG_EVENT".PadRight(51, ' ') + "==>   ChatEventHandler.CHAT_REG_EVENT_LOCKER");
#endif

            return true;
        }

        public static BaseApiStatus RegisterGroupChat(long groupId, string registerServerAddress, int registerServerPort, long authRegistrationTime)
        {
            BaseApiStatus data = null;
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
                    //registerServerAddress = "192.168.1.251";
                    //registerServerPort = 1500;
#if CHAT_LOG
                    log.Debug("SERVICE::GROUP::REGISTER_SENDING".PadRight(72, ' ') + "==>   groupId = " + groupId + ", registerIP = " + registerServerAddress + ", registerPort = " + registerServerPort + ", registrationTime = " + authRegistrationTime);
#endif
                    if (baseChatService != null)
                    {
                        data = baseChatService.registerGroupChat(groupId, registerServerAddress ?? String.Empty, registerServerPort, authRegistrationTime);
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("RegisterGroupChat Failed ==> ChatError = " + data.ErrorCode);
                            CHAT_HANDLER.onGroupChatRegisterFailure(groupId, null);
                        }
                    }
                    else
                    {
                        log.Debug("RegisterGroupChat Failed => baseChatService = NULL");
                        CHAT_HANDLER.onGroupChatRegisterFailure(groupId, null);
                    }
                }
                else
                {
                    log.Error("RegisterGroupChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    CHAT_HANDLER.onGroupChatRegisterFailure(groupId, null);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at RegisterGroupChat() ==>" + ex.Message + "\n" + ex.StackTrace);
                CHAT_HANDLER.onGroupChatRegisterFailure(groupId, null);
            }
            return data;
        }

        public static BaseApiStatus UnRegisterGroupChat(long groupId, int status, int mood)
        {
            BaseApiStatus data = null;
            try
            {
#if CHAT_LOG
                log.Debug("SERVICE::GROUP::UNREGISTER_SENDING".PadRight(72, ' ') + "==>   groupId = " + groupId);
#endif
                if (baseChatService != null)
                {
                    data = baseChatService.unregisterGroupChat(groupId, status, mood);
                    if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                    {
                        log.Error("UnRegisterGroupChat Failed ==> ChatError = " + data.ErrorCode);
                    }
                }
                else
                {
                    log.Debug("UnRegisterGroupChat Failed => baseChatService = NULL");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at UnRegisterGroupChat() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return data;
        }

        public static BaseGroupInformation GetGroupRegisterInfo(long groupId)
        {
            BaseGroupInformation data = null;
            try
            {
                if (baseChatService != null)
                {
                    data = baseChatService.getGroupInformation(groupId);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetFriendRegisterInfo() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return data;
        }

        public static List<BaseGroupInformation> GetGroupRegisterList()
        {
            List<BaseGroupInformation> data = null;
            try
            {
                if (baseChatService != null)
                {
                    data = baseChatService.getRegisteredGroupList();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetGroupRegisterList() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return data;
        }

        public static bool HasGroupRegistration(long groupId)
        {
            bool data = false;
            try
            {
                if (baseChatService != null)
                {
                    data = baseChatService.isExistsGroupConnection(groupId);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at HasGroupRegistration() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return data;
        }

        #endregion Register Group

        #region Register Room

        public static bool StartPublicRoomChat(string roomId, RoomModel roomModel = null, ChatEventDelegate onComplete = null, bool forceFully = false)
        {
            if (forceFully == false && ChatService.HasRoomRegistration(roomId))
            {
                InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = true });
                return false;
            }

            roomModel = RingIDViewModel.Instance.RoomList.TryGetValue(roomId);
            if (roomModel == null)
            {
                log.Debug("SERVICE::ROOM::ROOM_NOT_FOUND".PadRight(72, ' ') + "==>   roomId = " + roomId);
                InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
                return false;
            }

            bool isHanlderCreated = false;
            ChatEventHandler eventhandler = null;

#if CHAT_LOG
            log.Debug("SERVICE::ROOM::LOCK_REG_EVENT".PadRight(51, ' ') + "==>   ChatEventHandler.CHAT_REG_EVENT_LOCKER");
#endif
            lock (ChatEventHandler.CHAT_REG_EVENT_LOCKER)
            {
                eventhandler = ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT.TryGetValue(roomId);
                if (eventhandler == null)
                {
                    eventhandler = new ChatEventHandler { RoomID = roomId };
                    ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT[roomId.ToString()] = eventhandler;
                    isHanlderCreated = true;
                }
            }
#if CHAT_LOG
            log.Debug("SERVICE::ROOM::UNLOCK_REG_EVENT".PadRight(51, ' ') + "==>   ChatEventHandler.CHAT_REG_EVENT_LOCKER");
#endif

            if (onComplete != null)
            {
                eventhandler.OnCompleted += onComplete;
            }

            if (isHanlderCreated)
            {
#if CHAT_LOG
                log.Debug("SERVICE::ROOM::NEW_REGISTRATION".PadRight(72, ' ') + "==>   roomId = " + roomId);
#endif

                ChatService.RegisterPublicRoomChat(roomId, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.UserIdentity, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.ProfileImage);
            }
            else
            {
#if CHAT_LOG
                log.Debug("SERVICE::ROOM::REGISTRATION_ALREADY_RUNNING".PadRight(72, ' ') + "==>   roomId = " + roomId);
#endif
            }

            return true;
        }

        public static BaseApiStatus RegisterPublicRoomChat(string roomId, long ringId, string fullName, string imageUrl)
        {
            BaseApiStatus data = null;
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
                    //registerServerAddress = "192.168.1.251";
                    //registerServerPort = 1500;
#if CHAT_LOG
                    log.Debug("SERVICE::ROOM::REGISTER_SENDING".PadRight(72, ' ') + "==>   roomId = " + roomId + ", ringId = " + ringId + ", fullName = " + fullName + ", imageUrl = " + imageUrl);
#endif
                    if (baseChatService != null)
                    {
                        data = baseChatService.registerPublicRoomChat(roomId, fullName ?? String.Empty, imageUrl ?? String.Empty, ringId);
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("RegisterPublicRoomChat Failed ==> ChatError = " + data.ErrorCode);
                            CHAT_HANDLER.onPublicRoomChatRegisterFailure(roomId, null);
                        }
                    }
                    else
                    {
                        log.Debug("RegisterPublicRoomChat Failed => baseChatService = NULL");
                        CHAT_HANDLER.onPublicRoomChatRegisterFailure(roomId, null);
                    }
                }
                else
                {
                    log.Error("RegisterPublicRoomChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    CHAT_HANDLER.onPublicRoomChatRegisterFailure(roomId, null);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at RegisterPublicRoomChat() ==>" + ex.Message + "\n" + ex.StackTrace);
                CHAT_HANDLER.onPublicRoomChatRegisterFailure(roomId, null);
            }
            return data;
        }

        public static BaseApiStatus UnRegisterPublicRoomChat(string roomId)
        {
            BaseApiStatus data = null;
            try
            {
#if CHAT_LOG
                log.Debug("SERVICE::ROOM::UNREGISTER_SENDING".PadRight(72, ' ') + "==>   roomId = " + roomId);
#endif
                if (baseChatService != null)
                {
                    data = baseChatService.unregisterPublicRoomChat(roomId);
                    if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                    {
                        log.Error("UnRegisterPublicRoomChat Failed ==> ChatError = " + data.ErrorCode);
                    }
                }
                else
                {
                    log.Debug("UnRegisterPublicRoomChat Failed => baseChatService = NULL");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at UnRegisterPublicRoomChat() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return data;
        }

        public static bool HasRoomRegistration(string roomId)
        {
            bool data = false;
            try
            {
                if (baseChatService != null)
                {
                    data = baseChatService.isExistsRoomConnection(roomId);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at HasRoomRegistration() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return data;
        }

        #endregion Register Room

        #region Register Stream

        public static void StartLiveStream(StreamModel model, Func<bool, string, int, int> onComplete)
        {
#if CHAT_LOG
            log.Debug("SERVICE::STREAM::CHAT::START_LIVE_STREAM".PadRight(72, ' ') + "==>   streamId = " + model.StreamID + ", chatOn = " + model.ChatOn + ", giftOn = " + model.GiftOn);
#endif
            try
            {
                List<StreamCategoryDTO> tagList = new List<StreamCategoryDTO>();
                if (model.CategoryList != null && model.CategoryList.Count > 0)
                {
                    foreach (StreamCategoryModel i in model.CategoryList)
                    {
                        tagList.Add(new StreamCategoryDTO { CategoryID = i.CategoryID, CategoryName = i.CategoryName });
                    }
                }

                if (String.IsNullOrWhiteSpace(model.PublisherServerIP) || model.PublisherServerPort == 0)
                {
                    new ThrdInitializeLiveStream(model.StreamID, (serverDTO) =>
                    {
                        if (serverDTO == null || String.IsNullOrWhiteSpace(serverDTO.ServerIP) || serverDTO.RegisterPort == 0)
                        {
                            if (onComplete != null) onComplete(false, "Failed! Couldn't connect with server.", 0);
                            return 0;
                        }

                        model.StartTime = ChatService.GetServerTime();
                        model.PublisherServerIP = serverDTO.ServerIP;
                        model.PublisherServerPort = serverDTO.RegisterPort;
#if CHAT_LOG
                        log.Debug("AUTH::STREAM::RESPONSE".PadRight(58, ' ') + "==>   publisherID = " + model.UserTableID + ", streamID = " + model.StreamID + ", userName = " + model.UserName + ", startTime = " + model.StartTime + ", endTime = " + model.EndTime + ", publisherServerIP = " + model.PublisherServerIP + ", publisherServerPort = " + model.PublisherServerPort);
#endif
                        CallHelperMethods.PublisherRegister(
                            model.StreamID.ToString(),
                            model.PublisherServerIP,
                            model.PublisherServerPort,
                            callsdkwrapper.RegType.LiveStream);

                        Thread.Sleep(3000);
                        new ThrdStartLiveStream(model.UserTableID, model.StreamID, model.Title, model.ChatOn, model.GiftOn, tagList, onComplete).Start();
                        return 0;
                    }).Start();
                }
                else
                {
                    new ThrdStartLiveStream(model.UserTableID, model.StreamID, model.Title, model.ChatOn, model.GiftOn, tagList, (status, errormsg, reasonCode) =>
                    {
                        if (status)
                        {
                            if (onComplete != null) onComplete(status, errormsg, 0);
                        }
                        else if (reasonCode == ReasonCodeConstants.REASON_CODE_STREAM_SERVER_NOT_READY)
                        {
                            Thread.Sleep(3000);
                            new ThrdStartLiveStream(model.UserTableID, model.StreamID, model.Title, model.ChatOn, model.GiftOn, tagList, onComplete).Start();
                        }
                        else if (reasonCode == ReasonCodeConstants.REASON_CODE_RE_INITIALIZE_STREAM)
                        {
                            CallHelperMethods.PublisherUnregister(model.StreamID.ToString(), 0, 0, 0, args =>
                            {
                                new ThrdInitializeLiveStream(model.StreamID, (serverDTO) =>
                                {
                                    if (serverDTO == null || String.IsNullOrWhiteSpace(serverDTO.ServerIP) || serverDTO.RegisterPort == 0)
                                    {
                                        if (onComplete != null) onComplete(false, "Failed! Couldn't connect with server.", 0);
                                        return 0;
                                    }

                                    model.StartTime = ChatService.GetServerTime();
                                    model.PublisherServerIP = serverDTO.ServerIP;
                                    model.PublisherServerPort = serverDTO.RegisterPort;
#if CHAT_LOG
                                    log.Debug("AUTH::STREAM::RESPONSE".PadRight(58, ' ') + "==>   publisherID = " + model.UserTableID + ", streamID = " + model.StreamID + ", userName = " + model.UserName + ", startTime = " + model.StartTime + ", endTime = " + model.EndTime + ", publisherServerIP = " + model.PublisherServerIP + ", publisherServerPort = " + model.PublisherServerPort);
#endif
                                    CallHelperMethods.PublisherRegister(
                                        model.StreamID.ToString(),
                                        model.PublisherServerIP,
                                        model.PublisherServerPort,
                                        callsdkwrapper.RegType.LiveStream);

                                    Thread.Sleep(3000);
                                    new ThrdStartLiveStream(model.UserTableID, model.StreamID, model.Title, model.ChatOn, model.GiftOn, tagList, onComplete).Start();
                                    return 0;
                                }).Start();
                            });
                        }
                        else
                        {
                            if (onComplete != null) onComplete(status, errormsg, 0);
                        }
                        return 0;
                    }).Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at StartLiveStream() ==>" + ex.Message + "\n" + ex.StackTrace);
                if (onComplete != null) { onComplete(false, "Failed! Couldn't connect with server.", 0); }
            }
        }

        public static BaseApiStatus RegisterStreamChat(long publisherId, string streamId, string registerServerAddress, int registerServerPort)
        {
            BaseApiStatus data = null;
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
                    //registerServerAddress = "192.168.1.251";
                    //registerServerPort = 1500;
#if CHAT_LOG
                    log.Debug("SERVICE::STREAM::CHAT::REGISTER_SENDING".PadRight(72, ' ') + "==>   publisherId = " + publisherId + ", streamId = " + streamId + ", registerIP = " + registerServerAddress + ", registerPort = " + registerServerPort);
#endif
                    if (baseChatService != null)
                    {
                        data = baseChatService.registerLiveStreamChat(publisherId, registerServerAddress ?? String.Empty, registerServerPort, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.FullName);
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("RegisterStreamChat Failed ==> ChatError = " + data.ErrorCode);
                            CHAT_HANDLER.onLiveStreamChatRegisterFailure(publisherId, null);
                        }
                    }
                    else
                    {
                        log.Debug("RegisterStreamChat Failed => baseChatService = NULL");
                        CHAT_HANDLER.onLiveStreamChatRegisterFailure(publisherId, null);
                    }
                }
                else
                {
                    log.Error("RegisterStreamChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    CHAT_HANDLER.onLiveStreamChatRegisterFailure(publisherId, null);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at RegisterStreamChat() ==>" + ex.Message + "\n" + ex.StackTrace);
                CHAT_HANDLER.onLiveStreamChatRegisterFailure(publisherId, null);
            }
            return data;
        }

        public static BaseApiStatus UnRegisterStreamChat(long publisherId, int status, int mood)
        {
            BaseApiStatus data = null;
            try
            {
#if CHAT_LOG
                log.Debug("SERVICE::STREAM::CHAT::UNREGISTER_SENDING".PadRight(72, ' ') + "==>   publisherId = " + publisherId);
#endif
                if (baseChatService != null)
                {
                    data = baseChatService.unregisterLiveStreamChat(publisherId, status, mood);
                    if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                    {
                        log.Error("UnRegisterStreamChat Failed ==> ChatError = " + data.ErrorCode);
                    }
                }
                else
                {
                    log.Debug("UnRegisterStreamChat Failed => baseChatService = NULL");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at UnRegisterStreamChat() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return data;
        }

        #endregion Register Stream

        #region Friend Chat

        public static void SendFriendChat(string packetId, long friendId, int messageType, int secretTimeout, string message, long messageDate, int isSecretVisible, UserBasicInfoModel friendInfoModel, BlockedNonFriendModel blockModel)
        {
            try
            {
                bool isBlocked = HelperMethods.IsChatBlockedByFriend(friendId, friendInfoModel, blockModel);

                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID) && !isBlocked)
                {
#if CHAT_LOG
                    log.Debug("SERVICE::FRIEND::SEND_CHAT".PadRight(72, ' ') + "==>   packetId = " + packetId + ", friendId = " + friendId + ", messageType = " + messageType + ", messageDate = " + messageDate + ", timeout = " + secretTimeout + ", isSecretVisible = " + isSecretVisible + ", message = " + message);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.sendFriendChat(packetId, friendId, messageType, secretTimeout, message ?? String.Empty, messageDate, (isSecretVisible == 1 ? true : false));
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("SendFriendChat Failed ==> ChatError = " + data.ErrorCode);
                            MakeFriendChatFailed(packetId, friendId, false);
                        }
                    }
                    else
                    {
                        log.Debug("SendFriendChat Failed => baseChatService = NULL");
                        MakeFriendChatFailed(packetId, friendId, false);
                    }
                }
                else
                {
                    log.Error("SendFriendChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable + ", isBlocked = " + isBlocked);
                    MakeFriendChatFailed(packetId, friendId, false);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at SendFriendChat() ==>" + ex.Message + "\n" + ex.StackTrace);
                MakeFriendChatFailed(packetId, friendId, false);
            }
        }

        public static void EditFriendChat(string packetId, long friendId, int messageType, int secretTimeout, string message, long messageDate, int isSecretVisible, UserBasicInfoModel friendInfoModel, BlockedNonFriendModel blockModel)
        {
            try
            {
                bool isBlocked = HelperMethods.IsChatBlockedByFriend(friendId, friendInfoModel, blockModel);

                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID) && !isBlocked)
                {
#if CHAT_LOG
                    log.Debug("SERVICE::FRIEND::EDIT_CHAT".PadRight(72, ' ') + "==>   packetId = " + packetId + ", friendId = " + friendId + ", messageType = " + messageType + ", messageDate = " + messageDate + ", timeout = " + secretTimeout + ", isSecretVisible = " + isSecretVisible + ", message = " + message);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.editFriendChat(packetId, friendId, messageType, secretTimeout, message ?? String.Empty, messageDate, (isSecretVisible == 1 ? true : false));
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("EditFriendChat Failed ==> ChatError = " + data.ErrorCode);
                            MakeFriendChatFailed(packetId, friendId, false);
                        }
                    }
                    else
                    {
                        log.Debug("EditFriendChat Failed => baseChatService = NULL");
                        MakeFriendChatFailed(packetId, friendId, false);
                    }
                }
                else
                {
                    log.Error("EditFriendChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable + ", isBlocked = " + isBlocked);
                    MakeFriendChatFailed(packetId, friendId, false);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at EditFriendChat() ==>" + ex.Message + "\n" + ex.StackTrace);
                MakeFriendChatFailed(packetId, friendId, false);
            }
        }

        public static void SeenFriendChat(long friendId, List<BaseSeenPacketDTO> seenPacketList)
        {
            try
            {
                bool isBlockedByAnySide = HelperMethods.IsChatBlockedByAnySide(friendId);

                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID) && !isBlockedByAnySide)
                {
#if CHAT_LOG
                    log.Debug("SERVICE::FRIEND::CHAT_SEEN".PadRight(72, ' ') + "==>   friendId = " + friendId + ", seenPacketList = " + seenPacketList.ToString("\n"));
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.seenFriendChat(friendId, seenPacketList);
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("SeenFriendChat Failed ==> ChatError = " + data.ErrorCode);
                        }
                    }
                    else
                    {
                        log.Debug("SeenFriendChat Failed => baseChatService = NULL");
                    }
                }
                else
                {
                    log.Error("SeenFriendChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable + ", isBlockedByAnySide = " + isBlockedByAnySide);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at SeenFriendChat() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void DeleteFriendChat(long friendId, List<string> packetIds)
        {
            try
            {
                bool isBlockedByAnySide = HelperMethods.IsChatBlockedByAnySide(friendId);

                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID) && !isBlockedByAnySide)
                {
#if CHAT_LOG
                    log.Debug("SERVICE::FRIEND::CHAT_DELETE".PadRight(72, ' ') + "==>   friendId = " + friendId + ", packetIds = " + packetIds.ToString("\n"));
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.deleteFriendChatMessage(friendId, packetIds);
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("DeleteFriendChat Failed ==> ChatError = " + data.ErrorCode);
                        }
                    }
                    else
                    {
                        log.Debug("DeleteFriendChat Failed => baseChatService = NULL");
                    }
                }
                else
                {
                    log.Error("DeleteFriendChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable + ", isBlockedByAnySide = " + isBlockedByAnySide);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at DeleteFriendChat() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void TypingFriendChat(long friendId, UserBasicInfoModel friendInfoModel, BlockedNonFriendModel blockModel)
        {
            try
            {
                bool isBlockedByAnySide = HelperMethods.IsChatBlockedByAnySide(friendId, friendInfoModel, blockModel);

                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID) && !isBlockedByAnySide)
                {
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.typingFriendChat(friendId);
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("TypingFriendChat Failed ==> ChatError = " + data.ErrorCode);
                        }
                    }
                    else
                    {
                        log.Debug("TypingFriendChat Failed => baseChatService = NULL");
                    }
                }
                else
                {
                    log.Error("TypingFriendChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable + ", isBlockedByAnySide = " + isBlockedByAnySide);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at TypingFriendChat() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void GetFriendChatStatus(long friendId, List<string> packetIds)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::FRIEND::GET_CHAT_STATUS".PadRight(72, ' ') + "==>   friendId = " + friendId + ", packetIds = " + packetIds.ToString("\n"));
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.getFriendMessageStatus(friendId, packetIds);
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("GetFriendChatStatus Failed ==> ChatError = " + data.ErrorCode);
                        }
                    }
                    else
                    {
                        log.Debug("GetFriendChatStatus Failed => baseChatService = NULL");
                    }
                }
                else
                {
                    log.Error("GetFriendChatStatus Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetFriendChatStatus() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void RequestFriendChatHistory(long friendId, string packetId, int direction, int limit, ChatEventDelegate onComplete)
        {
            try
            {
                if (baseChatService == null)
                {
                    log.Debug("RequestFriendChatHistory Failed => baseChatService = NULL");
                    InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
                    return;
                }

                if (String.IsNullOrWhiteSpace(packetId))
                {
                    if (direction == ChatConstants.HISTORY_DOWN)
                        return;
                    packetId = ChatService.GeneratePacketID(ModelUtility.CurrentTimeMillis()).PacketID;
                }


                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::FRIEND::REQUEST_CHAT_HISTORY".PadRight(72, ' ') + "==>   friendId = " + friendId + ", packetId = " + packetId + ", packetTime = " + PacketIDGenerator.GetUnixTimestamp(packetId) + ", direction = " + direction + ", limit = " + limit);
#endif

                    BaseApiStatus data = baseChatService.requestFriendChatHistory(friendId, packetId, direction, limit);
                    if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                    {
                        InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, FriendTableID = friendId });
                    }
                    else
                    {
                        log.Error("RequestFriendChatHistory Failed ==> ChatError = " + data.ErrorCode);
                        InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
                    }
                }
                else
                {
                    log.Error("RequestFriendChatHistory Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at RequestFriendChatHistory() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
            }
        }

        public static List<string> GetPendingFriendMessages()
        {
            List<string> dataList = null;
            try
            {
                if (baseChatService != null)
                {
                    dataList = baseChatService.getPendingFriendMessages();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetPendingFriendMessages() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return dataList;
        }

        public static void SendFriendCallBusyMessage(string packetId, long friendId, string message, long messageDate)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::FRIEND::CALL_BUSY_MESSAGE".PadRight(72, ' ') + "==>   friendId = " + friendId + ", messageDate = " + messageDate + ", message = " + message);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.sendFriendCallBusyMessage(friendId, message ?? String.Empty, messageDate, packetId);
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("SendFriendCallBusyMessage Failed ==> ChatError = " + data.ErrorCode);
                        }
                    }
                    else
                    {
                        log.Debug("SendFriendCallBusyMessage Failed => baseChatService = NULL");
                    }

                }
                else
                {
                    log.Error("SendFriendCallBusyMessage Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at SendFriendCallBusyMessage() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static BaseApiStatus DontSendPendingMessages(long friendId)
        {
            BaseApiStatus data = null;
            try
            {
                if (baseChatService != null)
                {
                    data = baseChatService.dontSendPendingMessages(friendId);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at DontSendPendingMessages() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return data;
        }

        public static byte[] GetFriendFileTransferIdlePacket(long friendId)
        {
            byte[] array = null;
            try
            {
                if (baseChatService != null)
                {
                    array = baseChatService.getFriendFileIdelPacket(friendId);
                }
                else
                {
                    log.Debug("GetFriendFileTransferIdlePacket Failed => baseChatService = NULL");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetFriendFileTransferIdlePacket() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return array;
        }

        public static void GetFriendMessageByPacketID(long friendId, string packetId, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::FRIEND::GET_MESSAAGE_PACKETID".PadRight(72, ' ') + "==>   friendId = " + friendId + ", packetId = " + packetId);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.getFriendChatMessage(friendId, packetId);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, FriendTableID = friendId });
                        }
                        else
                        {
                            log.Error("GetFriendMessageByPacketID Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("GetFriendMessageByPacketID Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
                    }

                }
                else
                {
                    log.Error("GetFriendMessageByPacketID Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetFriendMessageByPacketID() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
            }
        }

        #endregion Friend Chat

        #region Friend Manipulation

        public static void BlockUnblockFriend(bool doBlock, long friendId, ChatEventDelegate onComplete)
        {
            PacketTimeID packet = ChatService.GeneratePacketID();

            if (doBlock)
            {
                ChatService.BlockFriend(packet.PacketID, friendId, packet.Time, onComplete);
            }
            else
            {
                ChatService.UnblockFriend(packet.PacketID, friendId, packet.Time, onComplete);
            }
        }

        private static void BlockFriend(string packetId, long friendId, long blockUnblockDate, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::FRIEND::BLOCK".PadRight(72, ' ') + "==>   friendId = " + friendId + ", blockUnblockDate = " + blockUnblockDate);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.blockFriend(packetId, friendId, blockUnblockDate, false);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, FriendTableID = friendId });
                        }
                        else
                        {
                            log.Error("BlockFriend Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("BlockFriend Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
                    }

                }
                else
                {
                    log.Error("BlockFriend Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at BlockFriend() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
            }
        }

        private static void UnblockFriend(string packetId, long friendId, long blockUnblockDate, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::FRIEND::UNBLOCK".PadRight(72, ' ') + "==>   friendId = " + friendId + ", blockUnblockDate = " + blockUnblockDate);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.unblockFriend(packetId, friendId, blockUnblockDate);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, FriendTableID = friendId });
                        }
                        else
                        {
                            log.Error("UnblockFriend Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("UnblockFriend Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
                    }
                }
                else
                {
                    log.Error("UnblockFriend Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at UnblockFriend() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
            }
        }

        #endregion Friend Manipulation

        #region Group Chat

        public static void SendGroupChat(string packetId, long groupId, int messageType, string message, long messageDate)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::GROUP::SEND_CHAT".PadRight(72, ' ') + "==>   packetId = " + packetId + ", groupId = " + groupId + ", messageType = " + messageType + ", messageDate = " + messageDate + ", message = " + message);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.sendGroupChat(packetId, groupId, messageType, message ?? String.Empty, messageDate);
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("SendGroupChat Failed ==> ChatError = " + data.ErrorCode);
                            MakeGroupChatFailed(packetId, groupId);
                        }
                    }
                    else
                    {
                        log.Debug("SendGroupChat Failed => baseChatService = NULL");
                        MakeGroupChatFailed(packetId, groupId);
                    }

                }
                else
                {
                    log.Error("SendGroupChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    MakeGroupChatFailed(packetId, groupId);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at SendGroupChat() ==>" + ex.Message + "\n" + ex.StackTrace);
                MakeGroupChatFailed(packetId, groupId);
            }
        }

        public static void EditGroupChat(string packetId, long groupId, int messageType, string message, long messageDate)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::GROUP::EDIT_CHAT".PadRight(72, ' ') + "==>   packetId = " + packetId + ", groupId = " + groupId + ", messageType = " + messageType + ", messageDate = " + messageDate + ", message = " + message);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.editGroupChat(packetId, groupId, messageType, message ?? String.Empty, messageDate);
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("EditGroupChat Failed ==> ChatError = " + data.ErrorCode);
                            MakeGroupChatFailed(packetId, groupId);
                        }
                    }
                    else
                    {
                        log.Debug("EditGroupChat Failed => baseChatService = NULL");
                        MakeGroupChatFailed(packetId, groupId);
                    }
                }
                else
                {
                    log.Error("EditGroupChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    MakeGroupChatFailed(packetId, groupId);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at EditGroupChat() ==>" + ex.Message + "\n" + ex.StackTrace);
                MakeGroupChatFailed(packetId, groupId);
            }
        }

        public static void SeenGroupChat(long groupId, List<string> packetIds)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::GROUP::CHAT_SEEN".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetIds = " + packetIds.ToString("\n"));
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.seenGroupChat(groupId, packetIds);
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("SeenGroupChat Failed ==> ChatError = " + data.ErrorCode);
                        }
                    }
                    else
                    {
                        log.Debug("SeenGroupChat Failed => baseChatService = NULL");
                    }
                }
                else
                {
                    log.Error("SeenGroupChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at SeenGroupChat() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void DeleteGroupChat(long groupId, List<string> packetIds)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::GROUP::CHAT_DELETE".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetIds = " + packetIds.ToString("\n"));
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.deleteGroupChatMessage(groupId, packetIds);
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("DeleteGroupChat Failed ==> ChatError = " + data.ErrorCode);
                        }
                    }
                    else
                    {
                        log.Debug("DeleteGroupChat Failed => baseChatService = NULL");
                    }
                }
                else
                {
                    log.Error("DeleteGroupChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at DeleteGroupChat() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void TypingGroupChat(long groupId)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.typingGroupChat(groupId);
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("TypingGroupChat Failed ==> ChatError = " + data.ErrorCode);
                        }
                    }
                    else
                    {
                        log.Debug("TypingGroupChat Failed => baseChatService = NULL");
                    }

                }
                else
                {
                    log.Error("TypingGroupChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at TypingGroupChat() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void RequestGroupChatHistory(long groupId, string packetId, int direction, int limit, ChatEventDelegate onComplete)
        {
            try
            {
                if (baseChatService == null)
                {
                    log.Debug("RequestGroupChatHistory Failed => baseChatService = NULL");
                    InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                    return;
                }

                if (String.IsNullOrWhiteSpace(packetId))
                {
                    if (direction == ChatConstants.HISTORY_DOWN)
                        return;
                    packetId = ChatService.GeneratePacketID(ModelUtility.CurrentTimeMillis()).PacketID;
                }

                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::GROUP::REQUEST_CHAT_HISTORY".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetId + ", packetTime = " + PacketIDGenerator.GetUnixTimestamp(packetId) + ", direction = " + direction + ", limit = " + limit);
#endif

                    BaseApiStatus data = baseChatService.requestGroupChatHistory(groupId, packetId, direction, limit);
                    if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                    {
                        InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, GroupID = groupId });
                    }
                    else
                    {
                        log.Error("RequestGroupChatHistory Failed ==> ChatError = " + data.ErrorCode);
                        InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                    }
                }
                else
                {
                    log.Error("RequestFriendChatHistory Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at RequestGroupChatHistory() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
            }
        }

        public static List<string> GetPendingGroupMessages()
        {
            List<string> dataList = null;
            try
            {
                if (baseChatService != null)
                {
                    dataList = baseChatService.getPendingGroupMessages();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetPendingGroupMessages() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return dataList;
        }

        public static byte[] GetGroupFileTransferIdlePacket(long groupId)
        {
            byte[] array = null;
            try
            {
                if (baseChatService != null)
                {
                    array = baseChatService.getGroupFileIdlePacket(groupId);
                }
                else
                {
                    log.Debug("GetGroupFileTransferIdlePacket Failed => baseChatService = NULL");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetGroupFileTransferIdlePacket() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return array;
        }

        public static void GetGroupMessageByPacketID(long groupId, string packetId, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::GROUP::GET_MESSAAGE_BY_PACKETID".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetId);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.getGroupChatMessage(groupId, packetId);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, GroupID = groupId });
                        }
                        else
                        {
                            log.Error("GetGroupMessageByPacketID Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("GetGroupMessageByPacketID Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                    }
                }
                else
                {
                    log.Error("GetGroupMessageByPacketID Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetGroupMessageByPacketID() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
            }
        }

        public static void GetGroupMessageSeenMemberListByPacketID(long groupId, string packetId, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::GROUP::GET_MESSAGE_SEEN_MEMBER_LIST_BY_PACKETID".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetId);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.getGroupMessageSeenList(groupId, packetId);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, GroupID = groupId });
                        }
                        else
                        {
                            log.Error("GetGroupMessageSeenMembersByPacketID Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("GetGroupMessageSeenMembersByPacketID Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                    }
                }
                else
                {
                    log.Error("GetGroupMessageSeenMembersByPacketID Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetGroupMessageSeenMembersByPacketID() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
            }
        }

        #endregion Group Chat

        #region Group Manipulation

        public static void ChangeGroupName(long groupId, string groupName, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::GROUP::CHANGE_NAME".PadRight(72, ' ') + "==>   groupId = " + groupId + ", groupName = " + groupName);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.changeGroupName(groupId, groupName ?? String.Empty);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, GroupID = groupId });
                        }
                        else
                        {
                            log.Error("ChangeGroupName Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("ChangeGroupName Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                    }
                }
                else
                {
                    log.Error("ChangeGroupName Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at ChangeGroupName() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
            }
        }

        public static void ChangeGroupUrl(long groupId, string groupUrl, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::GROUP::CHANGE_IMAGE_URL".PadRight(72, ' ') + "==>   groupId = " + groupId + ", groupUrl = " + groupUrl);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.changeGroupUrl(groupId, groupUrl ?? String.Empty);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, GroupID = groupId });
                        }
                        else
                        {
                            log.Error("ChangeGroupUrl Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("ChangeGroupUrl Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                    }
                }
                else
                {
                    log.Error("ChangeGroupUrl Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at ChangeGroupUrl() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
            }
        }

        public static void AddGroupMembers(long groupId, List<BaseMemberDTO> memberList, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::GROUP::ADD_MEMBERS".PadRight(72, ' ') + "==>   groupId = " + groupId + ", memberList = " + memberList.ToString("\n"));
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.addGroupMembers(groupId, memberList);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, GroupID = groupId });
                        }
                        else
                        {
                            log.Error("AddGroupMembers Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("AddGroupMembers Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                    }
                }
                else
                {
                    log.Error("AddGroupMembers Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at AddGroupMembers() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
            }
        }

        public static void RemoveGroupMembers(long groupId, List<long> memberIds, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::GROUP::REMOVE_MEMBERS".PadRight(72, ' ') + "==>   groupId = " + groupId + ", memberIds = " + memberIds.ToString("\n"));
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.removeGroupMembers(groupId, memberIds);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, GroupID = groupId });
                        }
                        else
                        {
                            log.Error("RemoveGroupMembers Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("RemoveGroupMembers Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                    }
                }
                else
                {
                    log.Error("RemoveGroupMembers Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at RemoveGroupMembers() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
            }
        }

        public static void LeaveGroup(long groupId, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::GROUP::LEAVE".PadRight(72, ' ') + "==>   groupId = " + groupId);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.leaveGroup(groupId);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, GroupID = groupId });
                        }
                        else
                        {
                            log.Error("LeaveGroup Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("LeaveGroup Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                    }
                }
                else
                {
                    log.Error("LeaveGroup Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at LeaveGroup() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
            }
        }

        public static void ChangeGroupMemberStatus(long groupId, List<BaseMemberDTO> memberList, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::GROUP::CHANGE_MEMBER_STATUS".PadRight(72, ' ') + "==>   groupId = " + groupId + ", memberList = " + memberList.ToString("\n"));
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.changeGroupMemberStatus(groupId, memberList);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, GroupID = groupId });
                        }
                        else
                        {
                            log.Error("ChangeGroupMemberStatus Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("ChangeGroupMemberStatus Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                    }
                }
                else
                {
                    log.Error("ChangeGroupMemberStatus Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at ChangeGroupMemberStatus() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
            }
        }

        public static void CreateGroup(long groupId, string groupName, string groupUrl, List<BaseMemberDTO> memberList, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::GROUP::CREATE".PadRight(72, ' ') + "==>   groupId = " + groupId + ", groupName = " + groupName + ", groupUrl = " + groupUrl + ", memberList = " + memberList.ToString("\n"));
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.createGroup(groupId, groupName ?? String.Empty, groupUrl ?? String.Empty, memberList);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, GroupID = groupId });
                        }
                        else
                        {
                            log.Error("CreateGroup Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("CreateGroup Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                    }
                }
                else
                {
                    log.Error("CreateGroup Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at CreateGroup() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
            }
        }

        public static void GetGroupInformationWithMembers(long groupId, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::GROUP::GET_INFORMATION_WITH_MEMBERS".PadRight(72, ' ') + "==>   ========> groupId = " + groupId);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.getGroupInformationWithMembers(groupId);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, GroupID = groupId });
                        }
                        else
                        {
                            log.Error("GetGroupInformationWithMembers Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("GetGroupInformationWithMembers Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                    }
                }
                else
                {
                    log.Error("GetGroupInformationWithMembers Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetGroupInformationWithMembers() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
            }
        }

        #endregion Group Manipulation

        #region Room Chat

        public static void SendPublicRoomChat(string packetId, string roomId, int messageType, string message, long messageDate, string memberFullName, string memberProfileUrl)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::ROOM::SEND_CHAT".PadRight(72, ' ') + "==>   packetId = " + packetId + ", roomId = " + roomId + ", messageType = " + messageType + ", messageDate = " + messageDate + ", message = " + message + ", fullName = " + memberFullName + ", profileUrl = " + memberProfileUrl);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.sendPublicRoomChat(packetId, roomId, messageType, message ?? String.Empty, messageDate, memberFullName ?? String.Empty, memberProfileUrl ?? String.Empty);
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("SendPublicRoomChat Failed ==> ChatError = " + data.ErrorCode);
                            MakePublicRoomChatFailed(packetId, roomId);
                        }
                    }
                    else
                    {
                        log.Debug("SendPublicRoomChat Failed => baseChatService = NULL");
                        MakePublicRoomChatFailed(packetId, roomId);
                    }
                }
                else
                {
                    log.Error("SendPublicRoomChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    MakePublicRoomChatFailed(packetId, roomId);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at SendPublicRoomChat() ==>" + ex.Message + "\n" + ex.StackTrace);
                MakePublicRoomChatFailed(packetId, roomId);
            }
        }

        public static void EditPublicRoomChat(string packetId, string roomId, int messageType, string message, long messageDate, string memberFullName, string memberProfileUrl)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::ROOM::EDIT_CHAT".PadRight(72, ' ') + "==>   packetId = " + packetId + ", roomId = " + roomId + ", messageType = " + messageType + ", messageDate = " + messageDate + ", message = " + message + ", fullName = " + memberFullName + ", profileUrl = " + memberProfileUrl);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.editPublicRoomChat(packetId, roomId, messageType, message ?? String.Empty, messageDate, memberFullName ?? String.Empty, memberProfileUrl ?? String.Empty);
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("EditPublicRoomChat Failed ==> ChatError = " + data.ErrorCode);
                            MakePublicRoomChatFailed(packetId, roomId);
                        }
                    }
                    else
                    {
                        log.Debug("EditPublicRoomChat Failed => baseChatService = NULL");
                        MakePublicRoomChatFailed(packetId, roomId);
                    }
                }
                else
                {
                    log.Error("EditPublicRoomChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    MakePublicRoomChatFailed(packetId, roomId);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at EditPublicRoomChat() ==>" + ex.Message + "\n" + ex.StackTrace);
                MakePublicRoomChatFailed(packetId, roomId);
            }
        }

        public static void TypingPublicRoomChat(string roomId)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.typingPublicRoomChat(roomId);
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("TypingPublicRoomChat Failed ==> ChatError = " + data.ErrorCode);
                        }
                    }
                    else
                    {
                        log.Debug("TypingPublicRoomChat Failed => baseChatService = NULL");
                    }
                }
                else
                {
                    log.Error("TypingPublicRoomChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at TypingPublicRoomChat() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void DeletePublicRoomChat(string roomId, List<string> packetIds)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::ROOM::CHAT_DELETE".PadRight(72, ' ') + "==>   roomId = " + roomId + ", packetIds = " + packetIds.ToString("\n"));
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.deletePublicChatMessage(roomId, packetIds);
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("DeletePublicRoomChat Failed ==> ChatError = " + data.ErrorCode);
                        }
                    }
                    else
                    {
                        log.Debug("DeletePublicRoomChat Failed => baseChatService = NULL");
                    }
                }
                else
                {
                    log.Error("DeletePublicRoomChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at DeletePublicRoomChat() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void RequestRoomChatHistory(string roomId, string packetId, int direction, int limit, ChatEventDelegate onComplete)
        {
            try
            {
                if (baseChatService == null)
                {
                    log.Debug("RequestRoomChatHistory Failed => baseChatService = NULL");
                    InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
                    return;
                }

                int month = 0;
                int year = 0;
                if (String.IsNullOrWhiteSpace(packetId))
                {
                    if (direction == ChatConstants.HISTORY_DOWN)
                        return;
                    PacketTimeID packet = ChatService.GeneratePacketID(ModelUtility.CurrentTimeMillis());
                    DateTime dateTime = ModelUtility.GetLocalDateTime(packet.Time);
                    packetId = packet.PacketID;
                    month = dateTime.Month - 1;
                    year = dateTime.Year;
                }
                else
                {
                    DateTime dateTime = ModelUtility.GetLocalDateTime(PacketIDGenerator.GetTime(packetId));
                    month = dateTime.Month - 1;
                    year = dateTime.Year;
                }

                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::ROOM::REQUEST_CHAT_HISTORY".PadRight(72, ' ') + "==>   ========> roomId = " + roomId + ", packetId = " + packetId + ", packetTime = " + PacketIDGenerator.GetUnixTimestamp(packetId) + ", direction = " + direction + ", limit = " + limit + ", year = " + year + ", month = " + month);
#endif

                    BaseApiStatus data = baseChatService.requestRoomChatHistory(roomId, packetId, year, month, direction, limit);
                    if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                    {
                        InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, RoomID = roomId });
                    }
                    else
                    {
                        log.Error("RequestRoomChatHistory Failed ==> ChatError = " + data.ErrorCode);
                        InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
                    }
                }
                else
                {
                    log.Error("RequestRoomChatHistory Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at RequestRoomChatHistory() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
            }
        }

        public static void LikeUnlikePublicRoomChat(string packetId, string roomId, long messageSenderId, bool isLike, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::ROOM::LIKE_UNLIKE_CHAT".PadRight(72, ' ') + "==>    isLike = " + isLike + ", packetId = " + packetId + ", roomId = " + roomId + ", messageSenderId = " + messageSenderId);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = isLike ? baseChatService.likePublicChatMessage(packetId, roomId, messageSenderId) : baseChatService.unlikePublicChatMessage(packetId, roomId, messageSenderId);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, RoomID = roomId });
                        }
                        else
                        {
                            log.Error("LikeUnlikePublicRoomChat Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("LikeUnlikePublicRoomChat Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
                    }
                }
                else
                {
                    log.Error("LikeUnlikePublicRoomChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at LikeUnlikePublicRoomChat() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
            }
        }

        public static void GetPublicChatLikeMemberList(string roomId, string packetId, long lastLikerId, int limit, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::ROOM::GET_LIKE_MEMBER_LIST_BY_PACKETID".PadRight(72, ' ') + "==>   roomId = " + roomId + ", packetId = " + packetId + ", lastLikerId = " + lastLikerId + ", limit = " + limit);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.getPublicChatLikeMemberList(roomId, packetId, lastLikerId, limit);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, RoomID = roomId });
                        }
                        else
                        {
                            log.Error("GetPublicChatLikeMemberList Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("GetPublicChatLikeMemberList Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
                    }
                }
                else
                {
                    log.Error("GetPublicChatLikeMemberList Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetPublicChatLikeMemberList() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
            }
        }

        public static void ReportPublicRoomChat(string packetId, string roomId, long messageSenderId, string reportMessage, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::ROOM::REPORT_CHAT".PadRight(72, ' ') + "==>   packetId = " + packetId + ", roomId = " + roomId + ", messageSenderId = " + messageSenderId);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.reportPublicChatMessage(packetId, roomId, messageSenderId, reportMessage);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, RoomID = roomId });
                        }
                        else
                        {
                            log.Error("ReportPublicRoomChat Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("ReportPublicRoomChat Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
                    }
                }
                else
                {
                    log.Error("ReportPublicRoomChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at ReportPublicRoomChat() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
            }
        }

        #endregion Room Chat

        #region Room Manipulation

        public static void EnterIntoRoom(string roomId)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::ROOM::ENTER_ROOM".PadRight(72, ' ') + "==>   roomId = " + roomId);
#endif
                    if (baseChatService != null)
                    {
                        baseChatService.enterPublicChatScreen(roomId);
                    }
                    else
                    {
                        log.Debug("EnterIntoRoom Failed => baseChatService = NULL");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at EnterIntoRoom() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void ExitFromRoom(string roomId)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::ROOM::EXIT_ROOM".PadRight(72, ' ') + "==>   roomId = " + roomId);
#endif
                    if (baseChatService != null)
                    {
                        baseChatService.exitPublicChatScreen(roomId);
                    }
                    else
                    {
                        log.Debug("ExitFromRoom Failed => baseChatService = NULL");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at ExitFromRoom() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void GetRoomInformation(string roomId, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::ROOM::GET_ROOM_INFORMATION".PadRight(72, ' ') + "==>   roomId = " + roomId);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.getRoomInformation(roomId);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, RoomID = roomId });
                        }
                        else
                        {
                            log.Error("GetRoomInformation Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("GetRoomInformation Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
                    }
                }
                else
                {
                    log.Error("GetRoomInformation Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetRoomInformation() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
            }
        }

        public static void GetRoomListWithHistory(int startIndex, int limit, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::ROOM::GET_ROOM_LIST_WITH_HISTORY".PadRight(72, ' ') + "==>   startIndex = " + startIndex + ", limit = " + limit);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.getRoomListWithHistory(startIndex, limit);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID });
                        }
                        else
                        {
                            log.Error("GetRoomListWithHistory Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("GetRoomListWithHistory Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                    }
                }
                else
                {
                    log.Error("GetRoomListWithHistory Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetRoomListWithHistory() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
            }
        }

        public static void GetRoomMemberList(string roomId, string pagingState, int limit, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::ROOM::GET_ROOM_MEMBER_LIST".PadRight(72, ' ') + "==>   ========> roomId = " + roomId + ", pagingState = " + pagingState + ", limit = " + limit);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.getRoomMemberList(roomId, pagingState ?? String.Empty, limit);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, RoomID = roomId });
                        }
                        else
                        {
                            log.Error("GetRoomMemberList Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("GetRoomMemberList Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
                    }
                }
                else
                {
                    log.Error("GetRoomMemberList Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetRoomMemberList() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { RoomID = roomId, Status = false });
            }
        }

        public static void SearchRoomList(string searchQuery, string country, string category, int startIndex, int limit, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::ROOM::SEARCH_ROOM_LIST_REQUEST".PadRight(72, ' ') + "==>   searchQuery = " + searchQuery + ", country = " + country + ", category = " + category + ", startIndex = " + startIndex + ", limit = " + limit);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.searchRoomList(searchQuery, country, category, startIndex, limit);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID });
                        }
                        else
                        {
                            log.Error("SearchRoomList Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("SearchRoomList Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                    }
                }
                else
                {
                    log.Error("SearchRoomList Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at SearchRoomList() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
            }
        }

        public static void GetPublicChatRoomCategoryList(string lastCategory, int limit, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::ROOM::GET_CATEGORY_LIST".PadRight(72, ' ') + "==>   lastCategory = " + lastCategory + ", limit = " + limit);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.getPublicChatRoomCategoryList(lastCategory, limit);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID });
                        }
                        else
                        {
                            log.Error("GetPublicChatRoomCategoryList Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("GetPublicChatRoomCategoryList Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                    }
                }
                else
                {
                    log.Error("GetPublicChatRoomCategoryList Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetPublicChatRoomCategoryList() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { Status = false });
            }
        }

        #endregion Room Manipulation

        #region Stream Chat

        public static void SendStreamChat(string packetId, long publisherId, int messageType, string message, long messageDate)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::STREAM::CHAT::SEND_CHAT".PadRight(72, ' ') + "==>   packetId = " + packetId + ", publisherId = " + publisherId + ", messageType = " + messageType + ", messageDate = " + messageDate + ", message = " + message);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.sendLiveStreamChat(packetId, publisherId, messageType, message ?? String.Empty, messageDate);
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("SendStreamChat Failed ==> ChatError = " + data.ErrorCode);
                            MakeStreamChatFailed(packetId, publisherId);
                        }
                    }
                    else
                    {
                        log.Debug("SendStreamChat Failed => baseChatService = NULL");
                        MakeStreamChatFailed(packetId, publisherId);
                    }
                }
                else
                {
                    log.Error("SendStreamChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    MakeStreamChatFailed(packetId, publisherId);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at SendStreamChat() ==>" + ex.Message + "\n" + ex.StackTrace);
                MakeStreamChatFailed(packetId, publisherId);
            }
        }

        public static void TypingStreamChat(long publisherId)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.typingLiveStreamChat(publisherId);
                        if (data.ErrorCode != ChatError.NO_ERROR_IN_SDK)
                        {
                            log.Error("TypingStreamChat Failed ==> ChatError = " + data.ErrorCode);
                        }
                    }
                    else
                    {
                        log.Debug("TypingStreamChat Failed => baseChatService = NULL");
                    }
                }
                else
                {
                    log.Error("TypingStreamChat Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at TypingStreamChat() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Stream Chat

        #region Friend File Transfer

        public static void SendFriendFileStreamRequest(long friendId, long fileId, ChatEventDelegate onComplete)
        {
            try
            {
                bool isBlockedByAnySide = HelperMethods.IsChatBlockedByAnySide(friendId);

                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID) && !isBlockedByAnySide)
                {
#if CHAT_LOG
                    log.Debug("SERVICE::FRIEND::FILE_STREAM_REQUEST".PadRight(72, ' ') + "==>   friendId = " + friendId + ", fileId = " + fileId);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.friendFileTransferStreamRequest(friendId, fileId);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, FriendTableID = friendId });
                        }
                        else
                        {
                            log.Error("SendFriendFileStreamRequest Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("SendFriendFileStreamRequest Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
                    }
                }
                else
                {
                    log.Error("SendFriendFileStreamRequest Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable + ", isBlockedByAnySide = " + isBlockedByAnySide);
                    InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at SendFriendFileStreamRequest() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
            }
        }

        public static void SendFriendFileSessionRequest(long friendId, long fileId, ChatEventDelegate onComplete)
        {
            try
            {
                bool isBlockedByAnySide = HelperMethods.IsChatBlockedByAnySide(friendId);

                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID) && !isBlockedByAnySide)
                {
#if CHAT_LOG
                    log.Debug("SERVICE::FRIEND::FILE_SESSION_REQUEST".PadRight(72, ' ') + "==>   friendId = " + friendId + ", fileId = " + fileId);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.friendFileTransferSessionRequest(friendId, fileId);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, FriendTableID = friendId });
                        }
                        else
                        {
                            log.Error("SendFriendFileSessionRequest Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("SendFriendFileSessionRequest Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
                    }
                }
                else
                {
                    log.Error("SendFriendFileSessionRequest Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable + ", isBlocked = " + isBlockedByAnySide);
                    InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at SendFriendFileSessionRequest() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { FriendTableID = friendId, Status = false });
            }
        }

        public static bool UploadFriendChatMedia(long friendId, String packetId, int messageType, String filePath, String caption, int widthOrFileSize, int heightOrDuration, int secretTimeout, long messageDate, bool isSecretVisible)
        {
            try
            {
#if CHAT_LOG
                log.Debug("SERVICE::FRIEND::UPLOAD_CHAT_MEDIA".PadRight(72, ' ') + "==>   friendId = " + friendId + ", packetId = " + packetId + ", messageType = " + messageType + ", widthOrFileSize = " + widthOrFileSize + ", heightOrDuration = " + heightOrDuration + ", messageDate = " + messageDate + ", secretTimeout = " + secretTimeout + ", isSecretVisible = " + isSecretVisible + ", caption = " + caption + ", filePath = " + filePath);
#endif
                if (baseChatService != null)
                {
                    return baseChatService.sendFriendChatMedia(friendId, packetId, messageType, filePath, caption, widthOrFileSize, heightOrDuration, secretTimeout, messageDate, isSecretVisible);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at UploadFriendChatMedia() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return false;
        }

        public static bool DownloadFriendChatMedia(long friendId, String packetId, String manifestUrl, int messageType, int secretTimeout)
        {
            try
            {
#if CHAT_LOG
                log.Debug("SERVICE::FRIEND::DOWNLOAD_CHAT_MEDIA".PadRight(72, ' ') + "==>   friendId = " + friendId + ", packetId = " + packetId + ", messageType = " + messageType + ", secretTimeout = " + secretTimeout + ", manifestUrl = " + manifestUrl);
#endif
                if (baseChatService != null)
                {
                    return baseChatService.downloadFriendChatMedia(friendId, packetId, manifestUrl, messageType, secretTimeout);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at DownloadFriendChatMedia() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return false;
        }

        #endregion

        #region Group File Transfer

        public static void SendGroupFileStreamRequest(long groupId, long friendId, long fileId, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::GROUP::FILE_TRANSFER_REQUEST".PadRight(72, ' ') + "==>   groupId = " + groupId + ", friendId = " + friendId + ", fileId = " + fileId);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.groupFileTransferStreamRequest(friendId, groupId, fileId);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, GroupID = groupId });
                        }
                        else
                        {
                            log.Error("SendGroupFileStreamRequest Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("SendGroupFileStreamRequest Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                    }
                }
                else
                {
                    log.Error("SendGroupFileStreamRequest Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at SendGroupFileStreamRequest() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
            }
        }

        public static void SendGroupFileSessionRequest(long groupId, long fileId, ChatEventDelegate onComplete)
        {
            try
            {
                if (DefaultSettings.IsInternetAvailable && !String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID))
                {
#if CHAT_LOG
                    log.Debug("SERVICE::GROUP::FILE_SESSION_REQUEST".PadRight(72, ' ') + "==>   groupId = " + groupId + ", fileId = " + fileId);
#endif
                    if (baseChatService != null)
                    {
                        BaseApiStatus data = baseChatService.groupFileTransferSessionRequest(groupId, fileId);
                        if (data.ErrorCode == ChatError.NO_ERROR_IN_SDK)
                        {
                            InsertDelegate(data.ErrorCode, onComplete, new ChatEventArgs { PacketID = data.PacketID, GroupID = groupId });
                        }
                        else
                        {
                            log.Error("SendGroupFileSessionRequest Failed ==> ChatError = " + data.ErrorCode);
                            InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                        }
                    }
                    else
                    {
                        log.Debug("SendGroupFileSessionRequest Failed => baseChatService = NULL");
                        InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                    }
                }
                else
                {
                    log.Error("SendGroupFileSessionRequest Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                    InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at SendGroupFileSessionRequest() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(onComplete, new ChatEventArgs { GroupID = groupId, Status = false });
            }
        }

        public static bool UploadGroupChatMedia(long groupId, String packetId, int messageType, String filePath, String caption, int widthOrFileSize, int heightOrDuration, long messageDate)
        {
            try
            {
#if CHAT_LOG
                log.Debug("SERVICE::GROUP::UPLOAD_CHAT_MEDIA".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetId + ", messageType = " + messageType + ", widthOrFileSize = " + widthOrFileSize + ", heightOrDuration = " + heightOrDuration + ", messageDate = " + messageDate + ", caption = " + caption + ", filePath = " + filePath);
#endif
                if (baseChatService != null)
                {
                    return baseChatService.sendGroupChatMedia(groupId, packetId, messageType, filePath, caption, widthOrFileSize, heightOrDuration, messageDate);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at UploadGroupChatMedia() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return false;
        }

        public static bool DownloadGroupChatMedia(long groupId, long friendId, String packetId, String manifestUrl, int messageType)
        {
            try
            {
#if CHAT_LOG
                log.Debug("SERVICE::GROUP::DOWNLOAD_CHAT_MEDIA".PadRight(72, ' ') + "==>   groupId = " + groupId + ", friendId = " + friendId + ", packetId = " + packetId + ", messageType = " + messageType + ", manifestUrl = " + manifestUrl);
#endif
                if (baseChatService != null)
                {
                    return baseChatService.downloadGroupChatMedia(groupId, friendId, packetId, manifestUrl, messageType);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at DownloadGroupChatMedia() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return false;
        }

        #endregion

        #region Common File Transfer

        public static void UploadChatFile(ChatFileDTO chatFileDTO, IUploadProgress listener)
        {
            new ChatFileUploader(chatFileDTO, listener).Start();
        }

        public static void DownloadChatFile(int packetType, MessageType messageType, string message, string downloadBaseUrl, string directoryBasePath, Func<bool, int> onComplete = null, Func<int, object, int> onProgress = null)
        {
            new ChatFileDownloader(packetType, messageType, message, downloadBaseUrl, directoryBasePath, onComplete, onProgress).Start();
        }

        public static BaseSessionStatus CreateChannel(long contactID, BaseMediaType mediaType, bool isGroupFileTransfer)
        {
            if (fileTransferService != null)
            {
                return (BaseSessionStatus)fileTransferService.CreateChannel(contactID, mediaType, isGroupFileTransfer);
            }
            return BaseSessionStatus.FAILED_TO_CREATE_SESSION;
        }

        public static void SetChannelChatServerInfo(long contactID, BaseMediaType mediaType, string cRelayServerIP, int iRelayServerPort)
        {
            if (fileTransferService != null)
            {
                fileTransferService.SetChannelChatServerInfo(contactID, mediaType, cRelayServerIP, iRelayServerPort);
            }
        }

        public static int TransferFile(long contactID, long fileID, bool isSender, long senderID, string fileDirectory, string fileName, string manifestCloudUrl, long fileOffset = -1)
        {
            if (fileTransferService != null)
            {
                return fileTransferService.TransferFile(contactID, fileID, isSender, senderID, fileDirectory, fileName, manifestCloudUrl, fileOffset);
            }
            return 0;
        }

        public static void CancelFileTransfer(long contactID, long fileID, bool deleteFile)
        {
            if (fileTransferService != null)
            {
                fileTransferService.CancelFileTransfer(contactID, fileID, deleteFile);
            }
        }

        public static int SendTo(long contactID, BaseMediaType mediaType, byte[] data, int iLen, string cDestinationIP, int iDestinationPort)
        {
            if (fileTransferService != null)
            {
                return fileTransferService.SendTo(contactID, mediaType, data, iLen, cDestinationIP, iDestinationPort);
            }
            return 0;
        }

        public static int CloseChannel(long contactID, BaseMediaType mediaType)
        {
            if (fileTransferService != null)
            {
                return fileTransferService.CloseChannel(contactID, mediaType);
            }
            return 0;
        }

        public static void SendNetworkStatus(BaseNetworkStatus status)
        {
            if (fileTransferService != null)
            {
                fileTransferService.SendNetworkStatus(status);
            }
        }

        public static bool AcceptChatMediaTransfer(String packetId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("SERVICE::COMMON::ACCEPT_CHAT_MEDIA_TRANSFER".PadRight(72, ' ') + "==>   packetId = " + packetId);
#endif
                if (baseChatService != null)
                {
                    return baseChatService.acceptChatMedia(packetId);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at AcceptChatMediaTransfer() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return false;
        }

        public static void CancelChatMediaTransfer(String packetId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("SERVICE::COMMON::ACCEPT_CHAT_MEDIA_TRANSFER".PadRight(72, ' ') + "==>   packetId = " + packetId);
#endif
                if (baseChatService != null)
                {
                    baseChatService.cancelChatMediaTransfer(packetId);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at CancelChatMediaTransfer() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static List<String> GetPendingChatMediaList(ConversationType conversationType, bool isUpload)
        {
            try
            {
#if CHAT_LOG
                log.Debug("SERVICE::COMMON::GET_PENDING_CHAT_MEDIA_LIST".PadRight(72, ' ') + "==>   conversationType = " + conversationType + ", isUpload = " + isUpload);
#endif
                if (baseChatService != null)
                {
                    return baseChatService.getPendingChatMediaList(conversationType, isUpload);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at GetPendingChatMediaList() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return null;
        }

        #endregion

        #region Additional

        private static void InsertDelegate(ChatError errorCode, ChatEventDelegate onComplete, ChatEventArgs args)
        {
            if (onComplete != null)
            {
                if (errorCode == ChatError.NO_ERROR_IN_SDK)
                {
                    ChatDictionaries.Instance.CHAT_EVENT_DELEGATE[args.PacketID] = onComplete;
                }
                else
                {
                    new Thread(() =>
                    {
                        onComplete.Invoke(args);
                    }).Start();
                }
            }
        }

        private static void InvokeDelegate(ChatEventDelegate onComplete, ChatEventArgs args)
        {
            new Thread(() =>
            {
                try
                {
                    if (onComplete != null)
                    {
                        onComplete.Invoke(args);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error at InsertDelegate() ==>" + ex.Message + "\n" + ex.StackTrace);
                }
            }).Start();
        }

        private static void MakeFriendChatFailed(string packetId, long friendId, bool fromAnonymousUser)
        {
            new Thread(() =>
            {
                Thread.Sleep(2000);
                CHAT_HANDLER.onFriendChatFailedToSend(friendId, packetId, fromAnonymousUser);
            }).Start();
        }

        private static void MakeGroupChatFailed(string packetId, long groupId)
        {
            new Thread(() =>
            {
                Thread.Sleep(2000);
                CHAT_HANDLER.onGroupChatFailedToSend(groupId, packetId);
            }).Start();
        }

        private static void MakePublicRoomChatFailed(string packetId, string roomId)
        {
            new Thread(() =>
            {
                Thread.Sleep(2000);
                CHAT_HANDLER.onPublicRoomChatFailedToSend(packetId, roomId);
            }).Start();
        }

        private static void MakeStreamChatFailed(string packetId, long publisherId)
        {
            new Thread(() =>
            {
                Thread.Sleep(2000);
                CHAT_HANDLER.onLiveStreamChatFailedToSend(publisherId, packetId);
            }).Start();
        }

        #endregion Additional

    }
}

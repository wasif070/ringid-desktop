﻿using FileTrasnferSDKCLI;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Models.Utility.Chat;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using View.Constants;

namespace View.Utility.Chat.Service
{
    public class FileTransferSession
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(FileTransferSession).Name);
        public static object _Lock = new object();
        private const int KEEP_ALIVE_LIFE_TIME = 3 * 60000;
        private const int KEEP_ALIVE_INTERVAL = 5000;
        private static ConcurrentDictionary<long, FileTransferSession> _FILE_TRANSFER_SESSION = new ConcurrentDictionary<long, FileTransferSession>();

        private ManualResetEvent _ClientDone;
        private bool _IsKeepAlive = false;
        private long _ContactID = 0;
        private string _SessionIP = String.Empty;
        private int _SessionPort = 0;
        private long _LastActivityTime = 0;
        private ConcurrentDictionary<long, ChatFileDTO> _FILE_INFO = new ConcurrentDictionary<long, ChatFileDTO>();

        #region Constructor

        public FileTransferSession(long contactID, string sessionIP, int sessionPort, bool isGroupSession)
        {
            this._ContactID = contactID;
            this._SessionIP = sessionIP;
            this._SessionPort = sessionPort;
            this.IsGroupSession = isGroupSession;
            this._LastActivityTime = ChatService.GetServerTime();
            this._ClientDone = new ManualResetEvent(false);
        }

        #endregion Constructor

        #region Methods

        private void StartKeepAlive()
        {
            Thread t = new Thread(KeepAlive);
            t.Name = this.GetType().Name;
            t.Start();
        }

        public void StopKeepAlive()
        {
            _IsKeepAlive = false;
            _ClientDone.Close();
            _ClientDone.Dispose();
            _ClientDone = null;
#if CHAT_LOG
            log.Debug("FILE TRANSFER => Stop Keep Alive");
#endif
        }

        private void KeepAlive()
        {
            try
            {
#if CHAT_LOG
                log.Debug("FILE TRANSFER => Start Keep Alive");
#endif
                _IsKeepAlive = true;

                byte[] packet = IsGroupSession ? ChatService.GetGroupFileTransferIdlePacket(_ContactID) : ChatService.GetFriendFileTransferIdlePacket(_ContactID);
                ChatService.SendTo(_ContactID, BaseMediaType.IPV_MEDIA_FILE_TRANSFER, packet, packet.Length, SessionIP, SessionPort);
                ChatService.SendTo(_ContactID, BaseMediaType.IPV_MEDIA_FILE_TRANSFER, packet, packet.Length, SessionIP, SessionPort);
                ChatService.SendTo(_ContactID, BaseMediaType.IPV_MEDIA_FILE_TRANSFER, packet, packet.Length, SessionIP, SessionPort);
                ChatService.SendTo(_ContactID, BaseMediaType.IPV_MEDIA_FILE_TRANSFER, packet, packet.Length, SessionIP, SessionPort);
                ChatService.SendTo(_ContactID, BaseMediaType.IPV_MEDIA_FILE_TRANSFER, packet, packet.Length, SessionIP, SessionPort);

                while (_IsKeepAlive)
                {
                    if (_ClientDone == null) break;
                    _ClientDone.Reset();

                    long diff = ChatService.GetServerTime() - LastActivityTime;
#if CHAT_LOG
                    log.Debug("FILE TRANSFER => Keep Alive => ContactID = " + _ContactID + ", IP = " + _SessionIP + ", Port = " + _SessionPort + ", Diff = " + diff);
#endif
                    if (diff >= KEEP_ALIVE_LIFE_TIME)
                    {
                        _IsKeepAlive = false;
#if CHAT_LOG
                        log.Debug("FILE TRANSFER => Keep Alive => ContactID = " + _ContactID + ". Closing Beacuse Of Time Limit Exit");
#endif
                        CloseChannel(_ContactID);
                        break;
                    }

                    ChatService.SendTo(_ContactID, BaseMediaType.IPV_MEDIA_FILE_TRANSFER, packet, packet.Length, _SessionIP, _SessionPort);

                    if (_ClientDone == null) break;
                    _ClientDone.WaitOne(KEEP_ALIVE_INTERVAL);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in ChatService class in KeepAlive method ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Methods

        #region Property

        internal ConcurrentDictionary<long, ChatFileDTO> FILE_INFO
        {
            get { return _FILE_INFO; }
        }
        internal long ContactID
        {
            get { return _ContactID; }
        }
        internal string SessionIP
        {
            get { return _SessionIP; }
            set { _SessionIP = value; }
        }
        internal int SessionPort
        {
            get { return _SessionPort; }
            set { _SessionPort = value; }
        }
        internal long LastActivityTime
        {
            get { return _LastActivityTime; }
            set { _LastActivityTime = value; }
        }
        internal static ConcurrentDictionary<long, FileTransferSession> FILE_TRANSFER_SESSION
        {
            get { return _FILE_TRANSFER_SESSION; }
        }
        public bool IsGroupSession { get; private set; }

        #endregion Property

        #region Static

        public static bool CreateChannel(long contactID, string sessionIP, int sessionPort, bool isGroupFileTransfer)
        {
            try
            {
                lock (_Lock)
                {
                    FileTransferSession session = FILE_TRANSFER_SESSION.TryGetValue(contactID);
                    if (session == null)
                    {
                        BaseSessionStatus status = ChatService.CreateChannel(contactID, BaseMediaType.IPV_MEDIA_FILE_TRANSFER, isGroupFileTransfer);

                        if (status == BaseSessionStatus.SESSION_CREATED_SUCCESSFULLY)
                        {
                            ChatService.SetChannelChatServerInfo(contactID, BaseMediaType.IPV_MEDIA_FILE_TRANSFER, sessionIP, sessionPort);

                            session = new FileTransferSession(contactID, sessionIP, sessionPort, isGroupFileTransfer);
                            session.StartKeepAlive();
                            FILE_TRANSFER_SESSION[contactID] = session;
#if CHAT_LOG
                            log.Debug("FILE TRANSFER => CreateSession Successfully Done. ContactID = " + contactID + ", Reason = " + status.ToString());
#endif
                            return true;
                        }
                        else
                        {
#if CHAT_LOG
                            log.Debug("FILE TRANSFER => CreateSession Create Failed. ContactID = " + contactID + ", Reason = " + status.ToString());
#endif
                        }
                    }
                    else
                    {
#if CHAT_LOG
                        log.Debug("FILE TRANSFER => CreateSession Create Cancelled. ContactID = " + contactID + ", Session Already Exist, PrevIP = " + session.SessionIP + ", PrePort = " + session.SessionPort + ", IP = " + sessionIP + ", Port = " + sessionPort);
#endif
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in ChatService class in HasValidSession method ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return false;
        }

        public static bool CloseChannel(long contactID)
        {
            try
            {
                lock (_Lock)
                {
                    FileTransferSession session = null;
                    if (FILE_TRANSFER_SESSION.TryRemove(contactID, out session))
                    {
                        int r = ChatService.CloseChannel(contactID, BaseMediaType.IPV_MEDIA_FILE_TRANSFER);
#if CHAT_LOG
                        log.Debug("FILE TRANSFER => CloseChannel. ContactID =" + contactID + " IsSucceed = " + (r == 1));
#endif

                        session.StopKeepAlive();
                        List<ChatFileDTO> fileDTOs = session.FILE_INFO.Values.ToList();
                        session.FILE_INFO.Clear();

                        foreach (ChatFileDTO fileDTO in fileDTOs)
                        {
#if CHAT_LOG
                            log.Debug("FILE TRANSFER => Force Changing Status. ContactID = " + contactID + ", FileID = " + fileDTO.FileID);
#endif
                            ChatService.FILE_TRANSFER_HANDLER.OnFileStreamCancelled(0, fileDTO);

#if CHAT_LOG
                            log.Debug("FILE TRANSFER => ChatFileTransferProcessorQueue->OnComplete. ContactID = " + contactID + ", FileID = " + fileDTO.FileID + ", IsDownstream = " + fileDTO.IsDownstream);
#endif
                            if (fileDTO.IsDownstream)
                            {
                                ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                            }
                            else
                            {
                                ImageDictionaries.Instance.FILE_UPLOAD_PROCESSOR_QUEUE.OnComplete();
                            }
                        }
                        session = null;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in ChatService class in HasValidSession method ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return false;
        }

        public static bool TransferFile(ChatFileDTO fileDTO, bool isSender)
        {
            lock (_Lock)
            {
                FileTransferSession session = FILE_TRANSFER_SESSION.TryGetValue(fileDTO.ContactID);
                if (session != null)
                {
                    session.LastActivityTime = ChatService.GetServerTime();
                    if (!session.FILE_INFO.ContainsKey(fileDTO.FileID))
                    {
                        session.FILE_INFO[fileDTO.FileID] = new ChatFileDTO(fileDTO);

                        string fileDirectory = Path.GetDirectoryName(fileDTO.File);
                        string fileName = Path.GetFileName(fileDTO.File);
                        string manifestCloudUrl = (!isSender && !String.IsNullOrWhiteSpace(fileDTO.FileMenifest) ? fileDTO.FileMenifest : String.Empty);

                        if (ChatService.TransferFile(fileDTO.ContactID, fileDTO.FileID, isSender, fileDTO.SenderTableID, fileDirectory, fileName, manifestCloudUrl, 0) == 1)
                        {
                            return true;
                        }
                        else
                        {
#if CHAT_LOG
                            log.Debug("FILE TRANSFER => Transfer File Failed. ContactID = " + fileDTO.ContactID + "");
#endif
                            return false;
                        }
                    }
                }
                else
                {
#if CHAT_LOG
                    log.Debug("FILE TRANSFER => Transfer File Failed. ContactID = " + fileDTO.ContactID + ", Reason = SESSION NOT FOUND");
#endif
                }
            }
            return false;
        }

        public static bool CancelTransferFile(long contactID, long fileID, bool deleteFile = false)
        {
            try
            {
                lock (_Lock)
                {
                    FileTransferSession session = FILE_TRANSFER_SESSION.TryGetValue(contactID);
                    if (session != null)
                    {
                        session.LastActivityTime = ChatService.GetServerTime();
                        ChatFileDTO tempDTO = null;

                        if (session.FILE_INFO.TryRemove(fileID, out tempDTO))
                        {
                            ChatService.CancelFileTransfer(contactID, fileID, deleteFile);

                            if (session.FILE_INFO.Count <= 0)
                            {
#if CHAT_LOG
                                log.Debug("FILE TRANSFER => ALL FILE COMPLETED. ContactID = " + contactID);
#endif
                                session.StopKeepAlive();
                                FILE_TRANSFER_SESSION.TryRemove(contactID);
                                new Thread(() =>
                                {
                                    int r = ChatService.CloseChannel(contactID, BaseMediaType.IPV_MEDIA_FILE_TRANSFER);
#if CHAT_LOG
                                    log.Debug("FILE TRANSFER => CloseChannel. ContactID =" + contactID + " IsSucceed = " + (r == 1));
#endif
                                }).Start();
                            }

#if CHAT_LOG
                            log.Debug("FILE TRANSFER => ChatFileTransferProcessorQueue->OnComplete. ContactID = " + contactID + ", FileID = " + fileID + ", IsDownstream = " + tempDTO.IsDownstream);
#endif
                            if (tempDTO.IsDownstream)
                            {
                                ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                            }
                            else
                            {
                                ImageDictionaries.Instance.FILE_UPLOAD_PROCESSOR_QUEUE.OnComplete();
                            }
                            return true;
                        }
                        else
                        {
#if CHAT_LOG
                            log.Debug("FILE TRANSFER => Cancel Transfer File Failed. ContactID = " + contactID + ", Reason = FILE INFO NOT FOUND");
#endif
                        }
                    }
                    else
                    {
#if CHAT_LOG
                        log.Debug("FILE TRANSFER => Cancel Transfer File Failed. ContactID = " + contactID + ", Reason = SESSION NOT FOUND");
#endif
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in ChatService class in Cancel Transfer File method ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return false;
        }

        public static void CancelTransferFileByPacketIDs(long contactID, List<string> packetIDs, bool deleteFile = false)
        {
            new Thread(() =>
            {
                try
                {
                    lock (_Lock)
                    {
                        FileTransferSession session = FILE_TRANSFER_SESSION.TryGetValue(contactID);
                        if (session != null)
                        {
                            session.LastActivityTime = ChatService.GetServerTime();
                            ChatFileDTO tempDTO = null;

                            List<long> fileIDs = packetIDs == null
                                ? session.FILE_INFO.Values.ToList().Select(P => P.FileID).ToList()
                                : session.FILE_INFO.Values.ToList().Where(P => packetIDs.Any(Q => Q.Equals(P.PacketID))).ToList().Select(P => P.FileID).ToList();
                            foreach (long fileID in fileIDs)
                            {
                                if (session.FILE_INFO.TryRemove(fileID, out tempDTO))
                                {
                                    ChatService.CancelFileTransfer(contactID, fileID, deleteFile);

                                    if (session.FILE_INFO.Count <= 0)
                                    {
#if CHAT_LOG
                                        log.Debug("FILE TRANSFER => ALL FILE COMPLETED. ContactID = " + contactID);
#endif
                                        session.StopKeepAlive();
                                        FILE_TRANSFER_SESSION.TryRemove(contactID);

                                        int r = ChatService.CloseChannel(contactID, BaseMediaType.IPV_MEDIA_FILE_TRANSFER);
#if CHAT_LOG
                                        log.Debug("FILE TRANSFER => CloseChannel. ContactID =" + contactID + " IsSucceed = " + (r == 1));
#endif
                                    }

#if CHAT_LOG
                                    log.Debug("FILE TRANSFER => ChatFileTransferProcessorQueue->OnComplete. ContactID = " + contactID + ", FileID = " + fileID + ", IsDownstream = " + tempDTO.IsDownstream);
#endif
                                    if (tempDTO.IsDownstream)
                                    {
                                        ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                    }
                                    else
                                    {
                                        ImageDictionaries.Instance.FILE_UPLOAD_PROCESSOR_QUEUE.OnComplete();
                                    }
                                }
                                else
                                {
#if CHAT_LOG
                                    log.Debug("FILE TRANSFER => Cancel Transfer File Failed. ContactID = " + contactID + ", Reason = FILE INFO NOT FOUND");
#endif
                                }
                            }
                        }
                        else
                        {
#if CHAT_LOG
                            log.Debug("FILE TRANSFER => Cancel Transfer File Failed. ContactID = " + contactID + ", Reason = SESSION NOT FOUND");
#endif
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error in ChatService class in Cancel Transfer File by packetIDs method ==>" + ex.Message + "\n" + ex.StackTrace);
                }
            }).Start();
        }

        public static List<ChatFileDTO> CancelAllTransferFile(bool deleteFile = false)
        {
            List<ChatFileDTO> tempList = new List<ChatFileDTO>();
            try
            {
                lock (_Lock)
                {
                    List<long> contactIds = FILE_TRANSFER_SESSION.Keys.ToList();
                    foreach (long contactID in contactIds)
                    {
                        FileTransferSession session = null;
                        if (FILE_TRANSFER_SESSION.TryRemove(contactID, out session))
                        {
                            List<ChatFileDTO> fileDTOs = session.FILE_INFO.Values.ToList();
                            foreach (ChatFileDTO fileDTO in fileDTOs)
                            {
#if CHAT_LOG
                                log.Debug("FILE TRANSFER => Cancel Transfer File. FileID = " + fileDTO.FileID + "");
#endif
                                ChatService.CancelFileTransfer(contactID, fileDTO.FileID, deleteFile);
                                if (ChatService.CHAT_HANDLER != null)
                                {
                                    ChatService.FILE_TRANSFER_HANDLER.OnFileStreamCancelled((deleteFile ? -1 : 0), fileDTO);
                                }
                            }
#if CHAT_LOG
                            log.Debug("FILE TRANSFER => Clear Session. ContactID = " + contactID + "");
#endif
                            session.FILE_INFO.Clear();
                            session.StopKeepAlive();

                            int r = ChatService.CloseChannel(contactID, BaseMediaType.IPV_MEDIA_FILE_TRANSFER);
#if CHAT_LOG
                            log.Debug("FILE TRANSFER => CloseChannel. ContactID =" + contactID + " IsSucceed = " + (r == 1));
#endif
                            tempList.AddRange(ImageDictionaries.Instance.FILE_UPLOAD_PROCESSOR_QUEUE.Clear());
                            tempList.AddRange(ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.Clear());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in ChatService class in Cancel All Transfer File method ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return tempList;
        }

        public static bool IsFileTransferRunning(long contactID, long fileID)
        {
            bool status = false;
            try
            {
                lock (_Lock)
                {
                    FileTransferSession session = FILE_TRANSFER_SESSION.TryGetValue(contactID);
                    if (session != null && session.FILE_INFO.ContainsKey(fileID))
                    {
                        status = true;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in ChatService class in IsFileTransferRunning() method ==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return status;
        }


        #endregion Static

    }
}

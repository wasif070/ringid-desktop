﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using imsdkwrapper;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Models.Utility.Chat;
using View.BindingModels;
using View.Dictonary;
using View.UI;
using View.UI.Chat;
using View.UI.Group;
using View.UI.Profile.FriendProfile;
using View.UI.Room;
using View.Utility.audio;
using View.Utility.FriendProfile;
using View.Utility.Recent;
using View.ViewModel;
using FileTrasnferSDKCLI;
using View.Utility.FriendList;
using View.Utility.Stream;
using View.Constants;
using MediaToolkit;

namespace View.Utility.Chat.Service
{

    public class ChatHandler : IChatListener
    {

        public static readonly ILog log = LogManager.GetLogger(typeof(ChatHandler).Name);

        private static object _GROUP_STATUS_LOCK = new object();
        private static object _FRIEND_STATUS_LOCK = new object();
        private static object _ROOM_STATUS_LOCK = new object();

        #region Register

        public override void onFriendChatRegisterSuccess(long friendId, long serverDate, string packetId, bool fromAnonymousUser)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::REGISTER_SUCCESS".PadRight(72, ' ') + "==>   friendId = " + friendId + ", packetId = " + packetId + ", serverDate = " + serverDate);
#endif

                ChatEventHandler eventhandler = null;
                if (ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT.TryRemove(friendId.ToString(), out eventhandler))
                {
                    Array delegates = eventhandler.RemoveAllEvents();
                    foreach (ChatEventDelegate ced in delegates)
                    {
                        InvokeDelegate(ced, new ChatEventArgs { FriendTableID = friendId, Status = true });
                    }
                }

                FileTransferSession session = FileTransferSession.FILE_TRANSFER_SESSION.TryGetValue(friendId);
                if (session != null)
                {
                    session.LastActivityTime = ChatService.GetServerTime();
                    BaseFriendInformation friendRegInfo = ChatService.GetFriendRegisterInfo(friendId);
                    string fileTransferIP = friendRegInfo != null ? friendRegInfo.RegisterServerAddress : String.Empty;

                    if (!String.IsNullOrWhiteSpace(fileTransferIP) && !String.IsNullOrWhiteSpace(session.SessionIP) && !session.SessionIP.Equals(fileTransferIP))
                    {
                        ChatService.SendFriendFileSessionRequest(friendId, 0, (args) =>
                        {
                            if (args.Status && !String.IsNullOrWhiteSpace(args.ServerIP) && args.ServerPort > 0)
                            {
                                session.SessionIP = args.ServerIP;
                                session.SessionPort = args.ServerPort;
                                ChatService.SetChannelChatServerInfo(friendId, BaseMediaType.IPV_MEDIA_FILE_TRANSFER, session.SessionIP, session.SessionPort);
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendChatRegisterSuccess() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendChatRegisterFailure(long friendId, string packetId, bool fromAnonymousUser)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::REGISTER_FAILURE".PadRight(72, ' ') + "==>   friendId = " + friendId + ", packetId = " + packetId);
#endif

                ChatEventHandler eventhandler = null;
                if (ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT.TryRemove(friendId.ToString(), out eventhandler))
                {
                    Array delegates = eventhandler.RemoveAllEvents();
                    foreach (ChatEventDelegate ced in delegates)
                    {
                        InvokeDelegate(ced, new ChatEventArgs { FriendTableID = friendId, Status = false });
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendChatRegisterFailure() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendRegistrationExpired(long friendId, bool fromAnonymousUser)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::REGISTER_EXPIRED".PadRight(72, ' ') + "==>   friendId = " + friendId);
#endif
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendRegistrationExpired() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendUnregistered(long friendId, int presence, int mood, bool fromAnonymousUser)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::UNREGISTER".PadRight(72, ' ') + "==>   friendId = " + friendId + ", presence = " + presence + ", mood = " + mood);
#endif

                UserBasicInfoModel friendModel = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(friendId);
                if (friendModel != null && friendModel.ShortInfoModel.Presence != presence)
                {
                    if (friendModel.ShortInfoModel.Presence != (int)OnlineStatus.OFFLINE)
                    {
                        friendModel.ShortInfoModel.LastOnlineTime = ChatService.GetServerTime();
                    }
                    friendModel.ShortInfoModel.Presence = (int)OnlineStatus.OFFLINE;
                    friendModel.ShortInfoModel.OnPropertyChanged("CurrentInstance");
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendUnregistered() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupChatRegisterSuccess(long groupId, long serverDate, string packetId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::REGISTER_SUCCESS".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetId + ", serverDate = " + serverDate);
#endif

                ChatEventHandler eventhandler = null;
                if (ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT.TryRemove(groupId.ToString(), out eventhandler))
                {
                    Array delegates = eventhandler.RemoveAllEvents();
                    foreach (ChatEventDelegate ced in delegates)
                    {
                        InvokeDelegate(ced, new ChatEventArgs { GroupID = groupId, Status = true });
                    }
                }

                FileTransferSession session = FileTransferSession.FILE_TRANSFER_SESSION.TryGetValue(groupId);
                if (session != null)
                {
                    session.LastActivityTime = ChatService.GetServerTime();
                    BaseGroupInformation groupRegInfo = ChatService.GetGroupRegisterInfo(groupId);
                    string fileTransferIP = groupRegInfo != null ? groupRegInfo.RegisterServerAddress : String.Empty;

                    if (!String.IsNullOrWhiteSpace(fileTransferIP) && !String.IsNullOrWhiteSpace(session.SessionIP) && !session.SessionIP.Equals(fileTransferIP))
                    {
                        ChatService.SendGroupFileSessionRequest(groupId, 0, (args) =>
                        {
                            if (args.Status && !String.IsNullOrWhiteSpace(args.ServerIP) && args.ServerPort > 0)
                            {
                                session.SessionIP = args.ServerIP;
                                session.SessionPort = args.ServerPort;
                                ChatService.SetChannelChatServerInfo(groupId, BaseMediaType.IPV_MEDIA_FILE_TRANSFER, session.SessionIP, session.SessionPort);
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupChatRegisterSuccess() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupChatRegisterFailure(long groupId, string packetId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::REGISTER_FAILURE".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetId);
#endif

                ChatEventHandler eventhandler = null;
                if (ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT.TryRemove(groupId.ToString(), out eventhandler))
                {
                    Array delegates = eventhandler.RemoveAllEvents();
                    foreach (ChatEventDelegate ced in delegates)
                    {
                        InvokeDelegate(ced, new ChatEventArgs { GroupID = groupId, Status = false });
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupChatRegisterFailure() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupRegistrationExpired(long groupId, long lastActivityTime)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::REGISTER_EXPIRED".PadRight(72, ' ') + "==>   groupId = " + groupId + ", lastActivityTime = " + lastActivityTime);
#endif
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupRegistrationExpired() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicRoomChatRegisterSuccess(string roomId, string packetId, int numberOfMembers, long anonymousId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::REGISTER_SUCCESS".PadRight(72, ' ') + "==>   roomId = " + roomId + ", packetId = " + packetId + ", numberOfmembers = " + numberOfMembers + ", anonymousId = " + anonymousId);
#endif

                ChatEventHandler eventhandler = null;
                if (ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT.TryRemove(roomId, out eventhandler))
                {
                    Array delegates = eventhandler.RemoveAllEvents();
                    foreach (ChatEventDelegate ced in delegates)
                    {
                        InvokeDelegate(ced, new ChatEventArgs { RoomID = roomId, AnonymousID = anonymousId, NumberOfMembers = numberOfMembers, Status = true });
                    }
                }

                onPublicChatMemberCountChanged(roomId, numberOfMembers);
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicRoomChatRegisterSuccess() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicRoomChatRegisterFailure(string roomId, string packetId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::REGISTER_FAILURE".PadRight(72, ' ') + "==>   roomId = " + roomId + ", packetId = " + packetId);
#endif

                if (!String.IsNullOrWhiteSpace(packetId))
                {
                    InvokeDelegate(new ChatEventArgs { PacketID = packetId, RoomID = roomId, Status = true });
                }

                ChatEventHandler eventhandler = null;
                if (ChatDictionaries.Instance.CHAT_REG_PROGRESS_EVENT.TryRemove(roomId, out eventhandler))
                {
                    Array delegates = eventhandler.RemoveAllEvents();
                    foreach (ChatEventDelegate ced in delegates)
                    {
                        InvokeDelegate(ced, new ChatEventArgs { RoomID = roomId, Status = false });
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicRoomChatRegisterFailure() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicRoomChatRegistrationExpired(string roomId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::REGISTER_EXPIRED".PadRight(72, ' ') + "==>   roomId = " + roomId);
#endif
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicRoomChatRegistrationExpired() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onLiveStreamChatRegisterSuccess(long publisherId, string packetId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CHAT::REGISTER_SUCCESS".PadRight(72, ' ') + "==>   publisherId = " + publisherId + ", packetId = " + packetId);
#endif
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveStreamChatRegisterSuccess() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onLiveStreamChatRegisterFailure(long publisherId, string packetId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CHAT::REGISTER_FAILURE".PadRight(72, ' ') + "==>   publisherId = " + publisherId + ", packetId = " + packetId);
#endif
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveStreamChatRegisterFailure() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onLiveStreamChatRegistrationExpired(long publisherId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CHAT::REGISTER_EXPIRED".PadRight(72, ' ') + "==>   publisherId = " + publisherId);
#endif
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveStreamChatRegistrationExpired() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Register

        #region Friend Chat

        public override void onFriendChatReceived(long senderId, long receiverId, string packetId, int messageType, int timeout, string message, long messageDate, bool isSecretVisible, bool fromOnline, bool fromAnonymousUser)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::CHAT_RECEIVED".PadRight(72, ' ') + "==>   packetId = " + packetId + ", receiverId = " + receiverId + ", senderId = " + senderId + ", messageType = " + messageType + ", messageDate = " + messageDate + ", timeout = " + timeout + ", isSecretVisible = " + isSecretVisible + ", fromOnline = " + fromOnline + ", message = " + message);
#endif
                long friendId = senderId == DefaultSettings.LOGIN_TABLE_ID ? receiverId : senderId;
                bool isProcessed = false;

                MessageDTO messageDTO = RecentChatCallActivityDAO.GetMessageDTOByPacketID(friendId, null, packetId);
                if (messageDTO == null)
                {
                    messageDTO = new MessageDTO();
                }
                else
                {
                    isProcessed = true;
                }

                messageDTO.FriendTableID = friendId;
                messageDTO.SenderTableID = senderId;
                messageDTO.PacketID = packetId;
                messageDTO.MessageType = messageType;
                messageDTO.Timeout = timeout;
                messageDTO.OriginalMessage = message;
                messageDTO.MessageDate = messageDate;
                messageDTO.MessageDelieverDate = ChatService.GetServerTime();
                messageDTO.IsSecretVisible = isSecretVisible ? 1 : 0;
                messageDTO.Status = ChatConstants.STATUS_DELIVERED;
                messageDTO.PacketType = ChatConstants.PACKET_TYPE_FRIEND_MESSAGE;
                ChatJSONParser.ParseMessage(messageDTO);

                UCFriendChatCallPanel ucFriendChatCall = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(messageDTO.FriendTableID);
                if (isProcessed)
                {
                    if (messageDTO.IsChatMessage)
                    {
                        RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                        RecentDTO recentDTO = new RecentDTO();
                        recentDTO.ContactID = messageDTO.FriendTableID.ToString();
                        recentDTO.FriendTableID = messageDTO.FriendTableID;
                        recentDTO.Time = messageDTO.MessageDate;
                        recentDTO.Message = messageDTO;

                        if (ucFriendChatCall != null && (ucFriendChatCall.IsVisible || messageDTO.MessageType == (int)MessageType.DELETE_MESSAGE))
                        {
                            RecentLoadUtility.LoadRecentData(recentDTO);
                        }
                    }
                }
                else
                {
                    UserBasicInfoModel friendInfoModel = ucFriendChatCall != null ? ucFriendChatCall.FriendBasicInfoModel : RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(messageDTO.FriendTableID);
                    ChatHelpers.UpdateFriendHistoryTime(messageDTO.FriendTableID, messageDTO.PacketID, messageDTO.PacketID, friendInfoModel);

                    if (fromOnline && friendInfoModel.ShortInfoModel.FriendShipStatus != StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                    {
                        BlockedNonFriendModel blockModel = ucFriendChatCall != null ? ucFriendChatCall.BlockedNonFriendModel : RingIDViewModel.Instance.GetBlockedNonFriendModelByID(messageDTO.FriendTableID);
                        if (blockModel.IsBlockedByMe)
                        {
                            ChatService.BlockUnblockFriend(blockModel.IsBlockedByMe, messageDTO.FriendTableID, null);
                            return;
                        }
                    }

                    if (messageDTO.IsChatMessage)
                    {
                        bool isDeleted = messageDTO.MessageType == (int)MessageType.DELETE_MESSAGE;
                        bool isNotifiable = !(isDeleted || ChatHelpers.IsDontDisturbMood() || senderId == DefaultSettings.LOGIN_TABLE_ID);
                        bool isChatViewInactive = UIHelperMethods.IsViewActivate(ucFriendChatCall) == false;
                        bool isChatViewHiddenInactive = isChatViewInactive || HelperMethods.IsViewVisible(ucFriendChatCall) == false;
                        bool isViewAtTop = HelperMethods.IsViewAtTop(ucFriendChatCall);

                        bool isSound = friendInfoModel.ShortInfoModel.ImSoundEnabled && isNotifiable;
                        bool isPopup = friendInfoModel.ShortInfoModel.ImPopupEnabled && isNotifiable && isChatViewInactive;
                        bool isUnread = friendInfoModel.ShortInfoModel.ImNotificationEnabled && isNotifiable && (isChatViewHiddenInactive || isViewAtTop);

                        messageDTO.IsUnread = isUnread;
                        RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                        RecentDTO recentDTO = new RecentDTO();
                        recentDTO.ContactID = messageDTO.FriendTableID.ToString();
                        recentDTO.FriendTableID = messageDTO.FriendTableID;
                        recentDTO.Time = messageDTO.MessageDate;
                        recentDTO.IsMoveToButtom = !isViewAtTop;
                        recentDTO.Message = messageDTO;

                        if (isSound)
                        {
                            if (isChatViewHiddenInactive)
                            {
                                AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_RECEIVED_ALERT_TUNE);
                            }
                            else
                            {
                                AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_RECEIVED_TUNE);
                            }
                        }

                        if (isPopup)
                        {
                            WNChatPopupMessage.ShowNotification(recentDTO);
                        }

                        if (isUnread)
                        {
                            ChatHelpers.AddUnreadChat(recentDTO.ContactID, recentDTO.Message.PacketID, recentDTO.Message.MessageDate);
                        }

                        if (ucFriendChatCall != null && (ucFriendChatCall.IsVisible || isDeleted))
                        {
                            RecentLoadUtility.LoadRecentData(recentDTO);
                        }

                        ChatLogLoadUtility.LoadRecentData(recentDTO);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendChatReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendChatEdited(long senderId, long receiverId, string packetId, int messageType, int timeout, string message, long messageDate, bool isSecretVisible, bool fromOnline, bool fromAnonymousUser)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::CHAT_EDITED".PadRight(72, ' ') + "==>   packetId = " + packetId + ", receiverId = " + receiverId + ", senderId = " + senderId + ", messageType = " + messageType + ", messageDate = " + messageDate + ", timeout = " + timeout + ", isSecretVisible = " + isSecretVisible + ", fromOnline = " + fromOnline + ", message = " + message);
#endif

                MessageDTO messageDTO = new MessageDTO();
                messageDTO.FriendTableID = senderId == DefaultSettings.LOGIN_TABLE_ID ? receiverId : senderId;
                messageDTO.SenderTableID = senderId;
                messageDTO.PacketID = packetId;
                messageDTO.MessageType = messageType;
                messageDTO.Timeout = timeout;
                messageDTO.OriginalMessage = message;
                messageDTO.MessageDate = messageDate;
                messageDTO.MessageDelieverDate = ChatService.GetServerTime();
                messageDTO.IsSecretVisible = isSecretVisible ? 1 : 0;
                messageDTO.Status = ChatConstants.STATUS_DELIVERED;
                messageDTO.PacketType = ChatConstants.PACKET_TYPE_FRIEND_MESSAGE_EDIT;
                ChatJSONParser.ParseMessage(messageDTO);

                UCFriendChatCallPanel ucFriendChatCall = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(messageDTO.FriendTableID);
                UserBasicInfoModel friendInfoModel = ucFriendChatCall != null ? ucFriendChatCall.FriendBasicInfoModel : RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(messageDTO.FriendTableID);
                ChatHelpers.UpdateFriendHistoryTime(messageDTO.FriendTableID, messageDTO.PacketID, messageDTO.PacketID, friendInfoModel);

                if (fromOnline && friendInfoModel.ShortInfoModel.FriendShipStatus != StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                {
                    BlockedNonFriendModel blockModel = ucFriendChatCall != null ? ucFriendChatCall.BlockedNonFriendModel : RingIDViewModel.Instance.GetBlockedNonFriendModelByID(messageDTO.FriendTableID);
                    if (blockModel.IsBlockedByMe)
                    {
                        ChatService.BlockUnblockFriend(blockModel.IsBlockedByMe, messageDTO.FriendTableID, null);
                        return;
                    }
                }

                if (messageDTO.IsChatMessage)
                {

                    MessageDTO prevDTO = RecentChatCallActivityDAO.GetMessageDTOByPacketID(messageDTO.FriendTableID, null, messageDTO.PacketID);
                    if (prevDTO != null)
                    {
                        prevDTO.PacketType = messageDTO.PacketType;
                        prevDTO.OriginalMessage = messageDTO.OriginalMessage;
                        prevDTO.Message = messageDTO.Message;
                        prevDTO.LinkUrl = messageDTO.LinkUrl;
                        prevDTO.LinkTitle = messageDTO.LinkTitle;
                        prevDTO.LinkImageUrl = messageDTO.LinkImageUrl;
                        prevDTO.LinkDescription = messageDTO.LinkDescription;
                        prevDTO.FileMenifest = messageDTO.FileMenifest;
                        prevDTO.MessageType = messageDTO.MessageType;
                        prevDTO.Timeout = messageDTO.Timeout;
                        prevDTO.IsSecretVisible = messageDTO.IsSecretVisible;
                        prevDTO.Status = messageDTO.Status;
                    }
                    else
                    {
                        prevDTO = messageDTO;
                    }
                    prevDTO.FileStatus = messageDTO.IsFileCancelled ? ChatConstants.FILE_TRANSFER_CANCELLED : prevDTO.FileStatus;

                    RecentChatCallActivityDAO.InsertMessageDTO(prevDTO);
                    RecentDTO recentDTO = new RecentDTO();
                    recentDTO.ContactID = messageDTO.FriendTableID.ToString();
                    recentDTO.FriendTableID = messageDTO.FriendTableID;
                    recentDTO.Time = messageDate;
                    recentDTO.Message = prevDTO;

                    ObservableCollection<RecentModel> chatModelList = RingIDViewModel.Instance.GetRecentModelListByID(messageDTO.FriendTableID);
                    RecentModel chatModel = chatModelList.Where(P => P.UniqueKey == messageDTO.PacketID).FirstOrDefault();
                    if (chatModel != null)
                    {
                        chatModel.Message.IsPreviewOpened = false;
                        chatModel.Message.LoadData(prevDTO);

                        if (chatModel.Message.MessageType == MessageType.LINK_MESSAGE)
                        {
                            ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.AddItem(new ChatDownloadProcessor(chatModel.Message, true));
                        }
                    }

                    if (ucFriendChatCall != null && ucFriendChatCall.IsVisible)
                    {
                        if (chatModel == null)
                        {
                            RecentLoadUtility.LoadRecentData(recentDTO);
                        }
                    }

                    ChatLogLoadUtility.LoadRecentData(recentDTO);

                    if (messageDTO.IsFileCancelled && fromOnline)
                    {
                        ChatHelpers.SendFileStreamCancel(prevDTO.FriendTableID, 0, prevDTO.FileID, prevDTO);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendChatEdited() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendChatFailedToSend(long friendId, string packetId, bool fromAnonymousUser)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::CHAT_FAILED_TO_SEND".PadRight(72, ' ') + "==>   friendId = " + friendId + ", packetId = " + packetId);
#endif

                lock (_FRIEND_STATUS_LOCK)
                {
                    RecentChatCallActivityDAO.UpdateChatMessageStatusInSameProcess(friendId, 0, packetId, ChatConstants.STATUS_FAILED, 0, 0, 0);
                    RecentModel chatModel = GetRecentModel(friendId, 0, null, packetId);
                    if (chatModel != null)
                    {
                        chatModel.Message.Status = ChatConstants.STATUS_FAILED;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendChatFailedToSend() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendChatDelivered(long friendId, string packetId, bool fromAnonymousUser, bool isEdited, bool fromOnline)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::CHAT_DELIVERED".PadRight(72, ' ') + "==>   friendId = " + friendId + ", packetId = " + packetId + ", isEdited = " + isEdited + ", fromOnline = " + fromOnline);
#endif

                lock (_FRIEND_STATUS_LOCK)
                {
                    List<string> packetIds = new List<string>();
                    packetIds.Add(packetId);
                    MessageDTO prevDTO = RecentChatCallActivityDAO.GetMessageStatusByPacketIDs(friendId, 0, packetIds).FirstOrDefault();

                    if (prevDTO == null || prevDTO.Status >= ChatConstants.STATUS_DELIVERED)
                        return;

                    long messageDelieveDate = ChatService.GetServerTime();
                    RecentChatCallActivityDAO.UpdateChatMessageStatusInSameProcess(friendId, 0, packetId, ChatConstants.STATUS_DELIVERED, messageDelieveDate, 0, 0);

                    RecentModel chatModel = GetRecentModel(friendId, 0, null, packetId);
                    if (chatModel != null)
                    {
                        chatModel.Message.Status = ChatConstants.STATUS_DELIVERED;
                        chatModel.Message.MessageDelieverDate = messageDelieveDate;
                        ChatHelpers.UpdateStatusByInsertOrChange(chatModel.Message.FriendTableID, chatModel.Message.PacketID, chatModel.Message.MessageType, chatModel.Message.MessageDate, chatModel.Message.SenderTableID, chatModel.Message.Status, chatModel.Message.LastStatus);
                    }
                    else
                    {
                        ChatHelpers.UpdateStatusByInsertOrChange(prevDTO.FriendTableID, prevDTO.PacketID, (MessageType)prevDTO.MessageType, prevDTO.MessageDate, prevDTO.SenderTableID, ChatConstants.STATUS_DELIVERED, null);
                    }

                    if (prevDTO.Status == ChatConstants.STATUS_SENDING)
                    {
                        UserBasicInfoModel friendInfoModel = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(friendId);
                        if (friendInfoModel.ShortInfoModel.ImSoundEnabled)
                        {
                            AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_DELIEVERED_TUNE);
                        }

                        ChatHelpers.UpdateFriendHistoryTime(prevDTO.FriendTableID, prevDTO.PacketID, prevDTO.PacketID, friendInfoModel);

                        if (chatModel != null && chatModel.Message.MessageType == MessageType.FILE_STREAM)
                        {
                            ChatHelpers.UploadFileStream(chatModel.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendChatDelivered() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendChatSent(long friendId, string packetId, bool fromAnonymousUser)
        {

        }

        public override void onFriendChatSeen(long friendId, List<BaseSeenPacketDTO> seenPacketList, bool fromAnonymousUser)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::CHAT_SEEN".PadRight(72, ' ') + "==>    friendid = " + friendId + ", seenPacketList = " + seenPacketList.ToString("\n"));
#endif

                if (seenPacketList == null || seenPacketList.Count <= 0)
                    return;

                lock (_FRIEND_STATUS_LOCK)
                {
                    List<MessageDTO> prevDTOs = RecentChatCallActivityDAO.GetMessageStatusByPacketIDs(friendId, 0, seenPacketList.Select(P => P.PacketID).ToList());
                    ObservableCollection<RecentModel> chatModelList = RingIDViewModel.Instance.GetRecentModelListByID(friendId);

                    foreach (BaseSeenPacketDTO seenPacketDTO in seenPacketList)
                    {
                        MessageDTO prevDTO = prevDTOs.Where(P => P.PacketID == seenPacketDTO.PacketID).FirstOrDefault();

                        if (prevDTO == null || prevDTO.Status >= seenPacketDTO.MessageStatus)
                            continue;

                        prevDTO.MessageSeenDate = seenPacketDTO.MessageStatus == ChatConstants.STATUS_SEEN || prevDTO.MessageSeenDate == 0 ? seenPacketDTO.MessageDate : prevDTO.MessageSeenDate;
                        prevDTO.MessageViewDate = seenPacketDTO.MessageStatus == ChatConstants.STATUS_VIEWED_PLAYED ? seenPacketDTO.MessageDate : prevDTO.MessageViewDate;

                        RecentChatCallActivityDAO.UpdateChatMessageStatusInSameProcess(friendId, 0, seenPacketDTO.PacketID, seenPacketDTO.MessageStatus, 0, prevDTO.MessageSeenDate, prevDTO.MessageViewDate);

                        RecentModel chatModel = chatModelList.Where(P => P.UniqueKey == seenPacketDTO.PacketID).FirstOrDefault();
                        if (chatModel != null)
                        {
                            chatModel.Message.MessageSeenDate = prevDTO.MessageSeenDate;
                            chatModel.Message.MessageViewDate = prevDTO.MessageViewDate;
                            chatModel.Message.Status = seenPacketDTO.MessageStatus;
                            ChatHelpers.UpdateStatusByInsertOrChange(chatModel.Message.FriendTableID, chatModel.Message.PacketID, chatModel.Message.MessageType, chatModel.Message.MessageDate, chatModel.Message.SenderTableID, chatModel.Message.Status, chatModel.Message.LastStatus);
                        }
                        else
                        {
                            ChatHelpers.UpdateStatusByInsertOrChange(prevDTO.FriendTableID, prevDTO.PacketID, (MessageType)prevDTO.MessageType, prevDTO.MessageDate, prevDTO.SenderTableID, ChatConstants.STATUS_SEEN, null);
                        }

                        if (prevDTO.Status == ChatConstants.STATUS_SENDING)
                        {
                            ChatHelpers.UpdateFriendHistoryTime(prevDTO.FriendTableID, prevDTO.PacketID, prevDTO.PacketID, null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendChatSeen() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendChatSeenConfirmed(long friendId, List<BaseSeenPacketDTO> packetIds, bool fromAnonymousUser)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::CHAT_SEEN_CONFIRMATION".PadRight(72, ' ') + "==>   friendId = " + friendId + ", packetIds = " + packetIds.ToString("\n"));
#endif
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendChatSeenConfirmed() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendChatTyping(long friendTableId, bool fromAnonymousUser)
        {
            try
            {
                UCFriendChatCallPanel ucFriendChatCall = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(friendTableId);
                if (ucFriendChatCall != null && ucFriendChatCall.IsVisible)
                {
                    ucFriendChatCall.AddTyping();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendChatTyping() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendChatIdle(long friendId, bool fromAnonymousUser)
        {
            try
            {

            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendChatIdle() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendChatDeleted(long senderId, long friendId, List<string> packetIds, bool fromAnonymousUser)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::CHAT_DELETED".PadRight(72, ' ') + "==>   senderId = " + senderId + ", friendId = " + friendId + ", packetIds = " + packetIds.ToString("\n"));
#endif
                if (senderId == DefaultSettings.LOGIN_TABLE_ID)
                {
                    RecentChatCallActivityDAO.DeleteChatHistory(friendId, 0, packetIds);
                    RecentLoadUtility.RemoveRange(friendId, packetIds);
                    FileTransferSession.CancelTransferFileByPacketIDs(friendId, packetIds, false);
                }
                else
                {
                    RecentChatCallActivityDAO.DeleteChatHistoryByMessageType(friendId, 0, packetIds);
                    RecentLoadUtility.RemoveRangeByMessageType(friendId, packetIds);
                    FileTransferSession.CancelTransferFileByPacketIDs(friendId, packetIds, false);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendChatDeleted() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendChatDeleteConfirmed(long friendId, string packetId, bool fromAnonymousUser)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::CHAT_DELETED_CONFIRMED".PadRight(72, ' ') + "==>   friendId = " + friendId + ", packetId = " + packetId);
#endif
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendChatDeleteConfirmed() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendMessagesStatusReceived(long friendId, List<BaseFriendMessageStatusDTO> messageStatusList)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::CHAT_STATUS_RECEIVED".PadRight(72, ' ') + "==>   friendId = " + friendId + ", messageStatusList = " + messageStatusList.ToString("\n"));
#endif
                if (messageStatusList == null || messageStatusList.Count <= 0)
                    return;

                lock (_FRIEND_STATUS_LOCK)
                {
                    List<MessageDTO> prevDTOs = RecentChatCallActivityDAO.GetMessageStatusByPacketIDs(friendId, 0, messageStatusList.Select(P => P.PacketID).ToList());
                    ObservableCollection<RecentModel> chatModelList = RingIDViewModel.Instance.GetRecentModelListByID(friendId);

                    foreach (BaseFriendMessageStatusDTO messageStatusDTO in messageStatusList)
                    {
                        MessageDTO prevDTO = prevDTOs.Where(P => P.PacketID == messageStatusDTO.PacketID).FirstOrDefault();
                        if (prevDTO == null || prevDTO.Status >= messageStatusDTO.MessageStatus)
                            continue;

                        prevDTO.MessageSeenDate = messageStatusDTO.MessageStatus == ChatConstants.STATUS_SEEN || prevDTO.MessageSeenDate == 0 ? messageStatusDTO.UpdateDate : prevDTO.MessageSeenDate;
                        prevDTO.MessageViewDate = messageStatusDTO.MessageStatus == ChatConstants.STATUS_VIEWED_PLAYED ? messageStatusDTO.UpdateDate : prevDTO.MessageViewDate;

                        RecentChatCallActivityDAO.UpdateChatMessageStatusInSameProcess(friendId, 0, messageStatusDTO.PacketID, messageStatusDTO.MessageStatus, 0, prevDTO.MessageSeenDate, prevDTO.MessageViewDate);

                        RecentModel chatModel = chatModelList.Where(P => P.UniqueKey == messageStatusDTO.PacketID).FirstOrDefault();
                        if (chatModel != null)
                        {
                            chatModel.Message.MessageSeenDate = prevDTO.MessageSeenDate;
                            chatModel.Message.MessageViewDate = prevDTO.MessageViewDate;
                            chatModel.Message.Status = messageStatusDTO.MessageStatus;
                            ChatHelpers.UpdateStatusByInsertOrChange(chatModel.Message.FriendTableID, chatModel.Message.PacketID, chatModel.Message.MessageType, chatModel.Message.MessageDate, chatModel.Message.SenderTableID, chatModel.Message.Status, chatModel.Message.LastStatus);
                        }
                        else
                        {
                            ChatHelpers.UpdateStatusByInsertOrChange(prevDTO.FriendTableID, prevDTO.PacketID, (MessageType)prevDTO.MessageType, prevDTO.MessageDate, prevDTO.SenderTableID, messageStatusDTO.MessageStatus, null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendMessagesStatusReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendHistoryMessageRequestStatus(long friendId, string packetId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::CHAT_HISTORY_REQUEST_SENT".PadRight(72, ' ') + "==>   friendId = " + friendId + ", packetId = " + packetId + ", status = " + status + ", Time = " + PacketIDGenerator.GetUnixTimestamp(packetId));
#endif
                InvokeDelegate(new ChatEventArgs { PacketID = packetId, FriendTableID = friendId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendHistoryMessageRequestSent() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendHistoryMessageReceived(long friendId, List<BaseMessageDTO> friendHistoryMessageList, int direction)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::CHAT_HISTORY_RECEIVED".PadRight(72, ' ') + "==>   friendId = " + friendId + ", direction = " + direction + ", friendHistoryMessageList = " + friendHistoryMessageList.ToString("\n"));
#endif

                if (friendHistoryMessageList == null || friendHistoryMessageList.Count <= 0 || friendId == 0)
                    return;

                string minPacketID = String.Empty;
                string maxPacketID = String.Empty;
                ObservableDictionary<string, long> unreadList = null;

                if (direction == ChatConstants.HISTORY_UP)
                {
                    minPacketID = friendHistoryMessageList.Last().PacketID;
                    maxPacketID = friendHistoryMessageList.First().PacketID;
                }
                else
                {
                    minPacketID = friendHistoryMessageList.First().PacketID;
                    maxPacketID = friendHistoryMessageList.Last().PacketID;
                    unreadList = ChatViewModel.Instance.GetUnreadChatByID(friendId);
                }
                ChatHelpers.UpdateFriendHistoryTime(friendId, minPacketID, maxPacketID, null);

#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::MIN_MAX_PACKETID".PadRight(72, ' ') + "==>   maxPacketID = " + maxPacketID + ", maxPacketTime = " + PacketIDGenerator.GetUnixTimestamp(maxPacketID) + ", minPacketID = " + minPacketID + ", minPacketTime = " + PacketIDGenerator.GetUnixTimestamp(minPacketID));
#endif

                List<RecentDTO> recentDTOs = new List<RecentDTO>();
                List<MessageDTO> messageDTOs = new List<MessageDTO>();

                foreach (BaseMessageDTO entity in friendHistoryMessageList)
                {
                    if (entity.MessageDate <= 0) continue;

                    MessageDTO msgDTO = new MessageDTO();
                    msgDTO.FriendTableID = friendId;
                    msgDTO.SenderTableID = entity.SenderID;
                    msgDTO.PacketID = entity.PacketID;
                    msgDTO.MessageType = entity.MessageType;
                    msgDTO.PacketType = entity.IsEdited ? ChatConstants.PACKET_TYPE_FRIEND_MESSAGE_EDIT : ChatConstants.PACKET_TYPE_FRIEND_MESSAGE;
                    msgDTO.Timeout = entity.TimeOut;
                    msgDTO.OriginalMessage = entity.Message;
                    msgDTO.MessageDate = entity.MessageDate;
                    msgDTO.MessageDelieverDate = entity.MessageDate;
                    msgDTO.MessageSeenDate = entity.MessageStatus > ChatConstants.STATUS_DELIVERED ? entity.MessageDate : 0;
                    msgDTO.IsSecretVisible = entity.IsSecretVisible ? 1 : 0;
                    msgDTO.Status = entity.MessageStatus;
                    msgDTO.IsUnread = unreadList != null && unreadList.Count > 0 && unreadList.ContainsKey(msgDTO.PacketID);
                    ChatJSONParser.ParseMessage(msgDTO);
                    msgDTO.FileStatus = msgDTO.IsFileCancelled ? ChatConstants.FILE_TRANSFER_CANCELLED : msgDTO.FileStatus;

                    if (msgDTO.IsChatMessage)
                    {
                        recentDTOs.Add(new RecentDTO { ContactID = friendId.ToString(), FriendTableID = friendId, Message = msgDTO, Time = msgDTO.MessageDate });
                        messageDTOs.Add(msgDTO);
                    }
                }

                new InsertIntoFriendMessageTable(messageDTOs).Start();
                RecentLoadUtility.LoadRecentData(recentDTOs);

                MessageDTO maxMessageDTO = messageDTOs.Where(P => P.MessageType != (int)MessageType.DELETE_MESSAGE).OrderByDescending(P => P.MessageDate).FirstOrDefault();
                if (maxMessageDTO != null)
                {
                    ChatLogLoadUtility.LoadRecentData(new RecentDTO { ContactID = friendId.ToString(), FriendTableID = friendId, Message = maxMessageDTO, Time = maxMessageDTO.MessageDate });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendHistoryMessageReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendHistoryCompleted(long friendId, int direction)
        {
            try
            {
                if (direction == ChatConstants.HISTORY_UP)
                {
#if CHAT_LOG
                    log.Debug("HANDLER::FRIEND::CHAT_HISTORY_COMPLETED".PadRight(72, ' ') + "==>   friendId = " + friendId);
#endif

                    UserBasicInfoModel model = RingIDViewModel.Instance.GetFriendBasicInfoModelNullable(friendId);
                    if (model != null)
                    {
                        model.ChatHistoryNotFound = true;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendHistoryCompleted() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendGetChatMessageReceived(long friendId, BaseMessageDTO friendChatMessageDTO)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::GET_MESSAAGE_PACKETID_SUCCESS".PadRight(72, ' ') + "==>   friendId = " + friendId + ", MessageDTO = " + friendChatMessageDTO.ToString());
#endif
                MessageDTO messageDTO = RecentChatCallActivityDAO.GetMessageDTOByPacketID(friendId, null, friendChatMessageDTO.PacketID);
                if (messageDTO != null)
                {
                    messageDTO.OriginalMessage = friendChatMessageDTO.Message;
                    ChatJSONParser.ParseMessage(messageDTO);
                    messageDTO.FileStatus = messageDTO.IsFileCancelled ? ChatConstants.FILE_TRANSFER_CANCELLED : messageDTO.FileStatus;
                    RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                    if (messageDTO.FileID > 0)
                    {
                        RecentModel chatModel = GetRecentModel(messageDTO.FriendTableID, 0, null, messageDTO.PacketID);
                        if (chatModel != null && chatModel.Message != null)
                        {
                            chatModel.Message.FileMenifest = messageDTO.FileMenifest;
                            chatModel.Message.FileStatus = messageDTO.FileStatus;
                        }
                    }
                }

                InvokeDelegate(new ChatEventArgs { PacketID = friendChatMessageDTO.PacketID, FriendTableID = friendId, Status = true });
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendGetChatMessageReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendGetChatMessageFailedToSend(long friendId, string packetId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::GET_MESSAAGE_PACKETID_FAILED".PadRight(72, ' ') + "==>   friendId = " + friendId + ", packetId = " + packetId);
#endif

                InvokeDelegate(new ChatEventArgs { PacketID = packetId, FriendTableID = friendId, Status = false });
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendGetChatMessageFailedToSend() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendChatConversationListReceived(List<BaseMessageDTO> conversationList)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::CONVERSATION_LIST".PadRight(72, ' ') + "==>   conversationList = " + conversationList.ToString("\n"));
#endif
                if (conversationList == null || conversationList.Count <= 0)
                    return;

                List<MessageDTO> messageDTOs = new List<MessageDTO>();
                List<long> frinedTableIDs = FriendListController.Instance.FriendDataContainer.UserBasicInfoModels.Keys.ToList();

                foreach (BaseMessageDTO entity in conversationList)
                {
                    if (entity.MessageDate <= 0) continue;

                    MessageDTO msgDTO = new MessageDTO();
                    msgDTO.FriendTableID = entity.SenderID == DefaultSettings.LOGIN_TABLE_ID ? entity.ReceiverID : entity.SenderID;
                    msgDTO.SenderTableID = entity.SenderID;
                    //msgDTO.PacketID = entity.PacketID;
                    msgDTO.MessageType = entity.MessageType;
                    msgDTO.PacketType = entity.IsEdited ? ChatConstants.PACKET_TYPE_FRIEND_MESSAGE_EDIT : ChatConstants.PACKET_TYPE_FRIEND_MESSAGE;
                    msgDTO.Timeout = entity.TimeOut;
                    switch ((MessageType)msgDTO.MessageType)
                    {
                        case MessageType.PLAIN_MESSAGE:
                        case MessageType.EMOTICON_MESSAGE:
                            msgDTO.OriginalMessage = entity.Message;
                            msgDTO.Message = entity.Message;
                            break;
                    }
                    msgDTO.MessageDate = entity.MessageDate;
                    msgDTO.MessageDelieverDate = entity.MessageDate;
                    msgDTO.MessageSeenDate = entity.MessageStatus > ChatConstants.STATUS_DELIVERED ? entity.MessageDate : 0;
                    msgDTO.IsSecretVisible = entity.IsSecretVisible ? 1 : 0;
                    msgDTO.Status = entity.MessageStatus;
                    msgDTO.FileStatus = msgDTO.IsFileCancelled ? ChatConstants.FILE_TRANSFER_CANCELLED : msgDTO.FileStatus;
                    messageDTOs.Add(msgDTO);

                    if (!frinedTableIDs.Contains(msgDTO.FriendTableID)) new ThradUnknownProfileInfoRequest().StartThread(msgDTO.FriendTableID, true);
                    ChatLogLoadUtility.LoadRecentData(new RecentDTO { ContactID = msgDTO.FriendTableID.ToString(), FriendTableID = msgDTO.FriendTableID, Message = msgDTO, Time = msgDTO.MessageDate });
                }
                new InsertIntoFriendMessageTable(messageDTOs).Start();
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendChatConversationListReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Friend Chat

        #region Friend Manipulation

        public override void onBlocked(long blockerId, long blockedId, long blockUnblockDate, bool addToBlock)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::BLOCK".PadRight(72, ' ') + "==>   blockerId = " + blockerId + ", blockedId = " + blockedId + ", blockUnblockDate = " + blockUnblockDate + ", addToBlock = " + addToBlock);
#endif
                bool blockedByMe = blockerId == DefaultSettings.LOGIN_TABLE_ID;
                long friendId = blockedByMe ? blockedId : blockerId;

                UserBasicInfoModel friendInfoModel = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(friendId);
                BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(friendId);

                if (friendInfoModel.ShortInfoModel.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                {
                    if (!blockedByMe)
                    {
                        blockModel.IsFriendRegDenied = true;
                    }
                }
                else
                {
                    if (blockedByMe)
                    {
                        blockModel.IsBlockedByMe = true;
                    }
                    else
                    {
                        blockModel.IsBlockedByFriend = true;
                        blockModel.IsFriendRegDenied = false;
                    }
                    BlockedNonFriendDAO.SaveBlockedNonFriend(friendId, blockModel.IsBlockedByMe, blockModel.IsBlockedByFriend);
                }

                SettingsConstants.VALUE_RINGID_BLOCK_UNBLOCK_UT = ChatService.GetServerTime();
                new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_BLOCK_UNBLOCK_UT, SettingsConstants.VALUE_RINGID_BLOCK_UNBLOCK_UT.ToString());
            }
            catch (Exception ex)
            {
                log.Error("Error at onBlocked() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onUnblocked(long blockerId, long blockedId, long blockUnblockDate)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::UNBLOCK".PadRight(72, ' ') + "==>   blockerId = " + blockerId + ", blockedId = " + blockedId + ", blockUnblockDate = " + blockUnblockDate);
#endif
                bool unblockedByMe = blockerId == DefaultSettings.LOGIN_TABLE_ID;
                long friendId = unblockedByMe ? blockedId : blockerId;

                BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(friendId);
                if (unblockedByMe)
                {
                    blockModel.IsBlockedByMe = false;
                }
                else
                {
                    blockModel.IsBlockedByFriend = false;
                    blockModel.IsFriendRegDenied = false;
                }
                BlockedNonFriendDAO.SaveBlockedNonFriend(friendId, blockModel.IsBlockedByMe, blockModel.IsBlockedByFriend);
                SettingsConstants.VALUE_RINGID_BLOCK_UNBLOCK_UT = ChatService.GetServerTime();
                new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_BLOCK_UNBLOCK_UT, SettingsConstants.VALUE_RINGID_BLOCK_UNBLOCK_UT.ToString());
            }
            catch (Exception ex)
            {
                log.Error("Error at onUnblocked() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onBlockUnblockRequestStatus(long friendId, string packetId, long blockUnblockDate, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::BLOCK_UNBLOCK_CONFIRMATION".PadRight(72, ' ') + "==>   friendId = " + friendId + ", packetId = " + packetId + ", status = " + status);
#endif

                InvokeDelegate(new ChatEventArgs { PacketID = packetId, FriendTableID = friendId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onBlockUnblockConfirmed() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Friend Manipulation

        #region Group Chat

        public override void onGroupChatReceived(long senderId, long groupId, string packetId, int messageType, string message, long messageDate, bool fromOnline)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::CHAT_RECEIVED".PadRight(72, ' ') + "==>   packetId = " + packetId + ", groupId = " + groupId + ", senderId = " + senderId + ", messageType = " + messageType + ", messageDate = " + messageDate + ", fromOnline = " + fromOnline + ", message = " + message);
#endif
                bool isProcessed = false;

                MessageDTO messageDTO = RecentChatCallActivityDAO.GetMessageDTOByPacketID(null, groupId, packetId);
                if (messageDTO == null)
                {
                    messageDTO = new MessageDTO();
                }
                else
                {
                    isProcessed = true;
                }

                messageDTO.GroupID = groupId;
                messageDTO.SenderTableID = senderId;
                messageDTO.PacketID = packetId;
                messageDTO.MessageType = messageType;
                messageDTO.OriginalMessage = message;
                messageDTO.MessageDate = messageDate;
                messageDTO.MessageDelieverDate = ChatService.GetServerTime();
                messageDTO.Status = ChatConstants.STATUS_DELIVERED;
                messageDTO.PacketType = ChatConstants.PACKET_TYPE_GROUP_MESSAGE;
                ChatJSONParser.ParseMessage(messageDTO);

                UCGroupChatCallPanel ucGroupChatCall = UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY.TryGetValue(messageDTO.GroupID);

                if (isProcessed)
                {
                    if (messageDTO.IsChatMessage)
                    {
                        RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                        RecentDTO recentDTO = new RecentDTO();
                        recentDTO.ContactID = groupId.ToString();
                        recentDTO.GroupID = groupId;
                        recentDTO.Time = messageDTO.MessageDate;
                        recentDTO.Message = messageDTO;

                        if (ucGroupChatCall != null && (ucGroupChatCall.IsVisible || messageDTO.MessageType == (int)MessageType.DELETE_MESSAGE))
                        {
                            RecentLoadUtility.LoadRecentData(recentDTO);
                        }
                    }
                }
                else
                {
                    GroupInfoModel groupInfoModel = ucGroupChatCall != null ? ucGroupChatCall.GroupInfoModel : RingIDViewModel.Instance.GetGroupInfoModelByGroupID(messageDTO.GroupID);
                    ChatHelpers.UpdateGroupHistoryTime(messageDTO.GroupID, messageDTO.PacketID, messageDTO.PacketID, groupInfoModel);

                    if (messageDTO.IsChatMessage)
                    {
                        bool isDeleted = messageDTO.MessageType == (int)MessageType.DELETE_MESSAGE;
                        bool isNotifiable = !(isDeleted || ChatHelpers.IsDontDisturbMood() || senderId == DefaultSettings.LOGIN_TABLE_ID);
                        bool isChatViewInactive = UIHelperMethods.IsViewActivate(ucGroupChatCall) == false;
                        bool isChatViewHiddenInactive = isChatViewInactive || HelperMethods.IsViewVisible(ucGroupChatCall) == false;
                        bool isViewAtTop = HelperMethods.IsViewAtTop(ucGroupChatCall);

                        bool isSound = groupInfoModel.ImSoundEnabled && isNotifiable;
                        bool isPopup = groupInfoModel.ImPopupEnabled && isNotifiable && isChatViewInactive;
                        bool isUnread = groupInfoModel.ImNotificationEnabled && isNotifiable && (isChatViewHiddenInactive || isViewAtTop);

                        messageDTO.IsUnread = isUnread;
                        RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                        RecentDTO recentDTO = new RecentDTO();
                        recentDTO.ContactID = groupId.ToString();
                        recentDTO.GroupID = groupId;
                        recentDTO.Time = messageDTO.MessageDate;
                        recentDTO.IsMoveToButtom = !isViewAtTop;
                        recentDTO.Message = messageDTO;

                        if (isSound)
                        {
                            if (isChatViewHiddenInactive)
                            {
                                AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_RECEIVED_ALERT_TUNE);
                            }
                            else
                            {
                                AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_RECEIVED_TUNE);
                            }
                        }

                        if (isPopup)
                        {
                            WNChatPopupMessage.ShowNotification(recentDTO);
                        }

                        if (isUnread)
                        {
                            ChatHelpers.AddUnreadChat(recentDTO.ContactID, recentDTO.Message.PacketID, recentDTO.Message.MessageDate);
                        }

                        if (ucGroupChatCall != null && (ucGroupChatCall.IsVisible || isDeleted))
                        {
                            RecentLoadUtility.LoadRecentData(recentDTO);
                        }

                        ChatLogLoadUtility.LoadRecentData(recentDTO);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupChatReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupChatEdited(long senderId, long groupId, string packetId, int messageType, string message, long messageDate, bool fromOnline)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::CHAT_EDITED".PadRight(72, ' ') + "==>   packetId = " + packetId + ", groupId = " + groupId + ", senderId = " + senderId + ", messageType = " + messageType + ", messageDate = " + messageDate + ", fromOnline = " + fromOnline + ", message = " + message);
#endif

                MessageDTO messageDTO = new MessageDTO();
                messageDTO.GroupID = groupId;
                messageDTO.SenderTableID = senderId;
                messageDTO.PacketID = packetId;
                messageDTO.MessageType = messageType;
                messageDTO.OriginalMessage = message;
                messageDTO.MessageDate = messageDate;
                messageDTO.MessageDelieverDate = ChatService.GetServerTime();
                messageDTO.Status = ChatConstants.STATUS_DELIVERED;
                messageDTO.PacketType = ChatConstants.PACKET_TYPE_GROUP_MESSAGE_EDIT;
                ChatJSONParser.ParseMessage(messageDTO);

                UCGroupChatCallPanel ucGroupChatCall = UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY.TryGetValue(messageDTO.GroupID);
                ChatHelpers.UpdateGroupHistoryTime(messageDTO.GroupID, messageDTO.PacketID, messageDTO.PacketID, ucGroupChatCall != null ? ucGroupChatCall.GroupInfoModel : null);

                if (messageDTO.IsChatMessage)
                {
                    MessageDTO prevDTO = RecentChatCallActivityDAO.GetMessageDTOByPacketID(null, messageDTO.GroupID, messageDTO.PacketID);
                    if (prevDTO != null)
                    {
                        prevDTO.PacketType = messageDTO.PacketType;
                        prevDTO.OriginalMessage = messageDTO.OriginalMessage;
                        prevDTO.Message = messageDTO.Message;
                        prevDTO.LinkUrl = messageDTO.LinkUrl;
                        prevDTO.LinkTitle = messageDTO.LinkTitle;
                        prevDTO.LinkImageUrl = messageDTO.LinkImageUrl;
                        prevDTO.LinkDescription = messageDTO.LinkDescription;
                        prevDTO.FileMenifest = messageDTO.FileMenifest;
                        prevDTO.MessageType = messageDTO.MessageType;
                        prevDTO.Status = messageDTO.Status;
                    }
                    else
                    {
                        prevDTO = messageDTO;
                    }
                    prevDTO.FileStatus = messageDTO.IsFileCancelled ? ChatConstants.FILE_TRANSFER_CANCELLED : prevDTO.FileStatus;

                    RecentChatCallActivityDAO.InsertMessageDTO(prevDTO);
                    RecentDTO recentDTO = new RecentDTO();
                    recentDTO.ContactID = groupId.ToString();
                    recentDTO.GroupID = groupId;
                    recentDTO.Time = messageDate;
                    recentDTO.Message = prevDTO;

                    ObservableCollection<RecentModel> chatModelList = RingIDViewModel.Instance.GetRecentModelListByID(messageDTO.GroupID);
                    RecentModel chatModel = chatModelList.Where(P => P.UniqueKey == messageDTO.PacketID).FirstOrDefault();
                    if (chatModel != null)
                    {
                        chatModel.Message.IsPreviewOpened = false;
                        chatModel.Message.LoadData(prevDTO);

                        if (chatModel.Message.MessageType == MessageType.LINK_MESSAGE)
                        {
                            ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.AddItem(new ChatDownloadProcessor(chatModel.Message, true));
                        }
                    }

                    if (ucGroupChatCall != null && ucGroupChatCall.IsVisible)
                    {
                        if (chatModel == null)
                        {
                            RecentLoadUtility.LoadRecentData(recentDTO);
                        }
                    }

                    ChatLogLoadUtility.LoadRecentData(recentDTO);

                    if (messageDTO.IsFileCancelled && fromOnline)
                    {
                        ChatHelpers.SendFileStreamCancel(0, prevDTO.GroupID, prevDTO.FileID, prevDTO);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupChatEdited() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupChatFailedToSend(long groupId, string packetId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::CHAT_FAILED_TO_SEND".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetId);
#endif

                lock (_GROUP_STATUS_LOCK)
                {
                    RecentChatCallActivityDAO.UpdateChatMessageStatusInSameProcess(0, groupId, packetId, ChatConstants.STATUS_FAILED, 0, 0, 0);
                    RecentModel chatModel = GetRecentModel(0, groupId, null, packetId);
                    if (chatModel != null)
                    {
                        chatModel.Message.Status = ChatConstants.STATUS_FAILED;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupChatFailedToSend() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupChatDelivered(long senderId, long groupId, string packetId)
        {

        }

        public override void onGroupChatSent(long groupId, string packetId, bool isEdited)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::CHAT_SENT".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetId);
#endif

                lock (_GROUP_STATUS_LOCK)
                {
                    List<string> packetIds = new List<string>();
                    packetIds.Add(packetId);
                    MessageDTO prevDTO = RecentChatCallActivityDAO.GetMessageStatusByPacketIDs(0, groupId, packetIds).FirstOrDefault();

                    if (prevDTO == null || prevDTO.Status >= ChatConstants.STATUS_DELIVERED)
                        return;

                    long messageDelieveDate = ChatService.GetServerTime();
                    RecentChatCallActivityDAO.UpdateChatMessageStatusInSameProcess(0, groupId, packetId, ChatConstants.STATUS_DELIVERED, messageDelieveDate, 0, 0);

                    RecentModel chatModel = GetRecentModel(0, groupId, null, packetId);
                    if (chatModel != null)
                    {
                        chatModel.Message.Status = ChatConstants.STATUS_DELIVERED;
                        chatModel.Message.MessageDelieverDate = messageDelieveDate;
                        ChatHelpers.UpdateStatusByInsertOrChange(chatModel.Message.GroupID, chatModel.Message.PacketID, chatModel.Message.MessageType, chatModel.Message.MessageDate, chatModel.Message.SenderTableID, chatModel.Message.Status, chatModel.Message.LastStatus);
                    }
                    else
                    {
                        ChatHelpers.UpdateStatusByInsertOrChange(prevDTO.GroupID, prevDTO.PacketID, (MessageType)prevDTO.MessageType, prevDTO.MessageDate, prevDTO.SenderTableID, ChatConstants.STATUS_DELIVERED, null);
                    }

                    if (prevDTO.Status == ChatConstants.STATUS_SENDING)
                    {
                        GroupInfoModel groupInfoModel = RingIDViewModel.Instance.GetGroupInfoModelByGroupID(groupId);
                        if (groupInfoModel.ImSoundEnabled)
                        {
                            AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_DELIEVERED_TUNE);
                        }

                        ChatHelpers.UpdateGroupHistoryTime(prevDTO.GroupID, prevDTO.PacketID, prevDTO.PacketID, groupInfoModel);

                        if (chatModel != null && chatModel.Message.MessageType == MessageType.FILE_STREAM)
                        {
                            ChatHelpers.UploadFileStream(chatModel.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupChatSent() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupChatSeen(long groupId, List<string> packetIds)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::CHAT_SEEN".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetIds.ToString("\n"));
#endif
                if (packetIds == null || packetIds.Count <= 0)
                    return;

                lock (_GROUP_STATUS_LOCK)
                {
                    List<MessageDTO> prevDTOs = RecentChatCallActivityDAO.GetMessageStatusByPacketIDs(0, groupId, packetIds);
                    ObservableCollection<RecentModel> chatModelList = RingIDViewModel.Instance.GetRecentModelListByID(groupId);

                    foreach (string packetId in packetIds)
                    {
                        MessageDTO prevDTO = prevDTOs.Where(P => P.PacketID == packetId).FirstOrDefault();

                        if (prevDTO == null || prevDTO.SenderTableID != DefaultSettings.LOGIN_TABLE_ID || prevDTO.Status >= ChatConstants.STATUS_SEEN)
                            continue;

                        long messageSeenDate = ChatService.GetServerTime();
                        RecentChatCallActivityDAO.UpdateChatMessageStatusInSameProcess(0, groupId, packetId, ChatConstants.STATUS_SEEN, 0, messageSeenDate, 0);

                        RecentModel chatModel = chatModelList.Where(P => P.UniqueKey == packetId).FirstOrDefault();
                        if (chatModel != null)
                        {
                            chatModel.Message.Status = ChatConstants.STATUS_SEEN;
                            chatModel.Message.MessageSeenDate = messageSeenDate;
                            ChatHelpers.UpdateStatusByInsertOrChange(chatModel.Message.GroupID, chatModel.Message.PacketID, chatModel.Message.MessageType, chatModel.Message.MessageDate, chatModel.Message.SenderTableID, chatModel.Message.Status, chatModel.Message.LastStatus);
                        }
                        else
                        {
                            ChatHelpers.UpdateStatusByInsertOrChange(prevDTO.GroupID, prevDTO.PacketID, (MessageType)prevDTO.MessageType, prevDTO.MessageDate, prevDTO.SenderTableID, ChatConstants.STATUS_SEEN, null);
                        }

                        if (prevDTO.Status == ChatConstants.STATUS_SENDING)
                        {
                            ChatHelpers.UpdateGroupHistoryTime(prevDTO.GroupID, prevDTO.PacketID, prevDTO.PacketID, null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupChatSeen() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupChatSeenRequestStatus(long groupId, List<string> packetIds, bool status)
        {
#if CHAT_LOG
            log.Debug("HANDLER::GROUP::CHAT_SEEN_CONFIRMATION".PadRight(72, ' ') + "==>   groupId = " + groupId + ", status = " + status + ", packetId = " + packetIds.ToString("\n"));
#endif
        }

        public override void onGroupChatTyping(long friendid, long groupid)
        {
            try
            {
                UCGroupChatCallPanel ucGroupChatCall = UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY.TryGetValue(groupid);
                if (ucGroupChatCall != null && ucGroupChatCall.IsVisible)
                {
                    ucGroupChatCall.AddTyping(friendid);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupChatTyping() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupChatIdle(long friendid, long groupid)
        {
            try
            {

            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupChatIdle() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupChatDeleted(long senderId, long groupId, List<string> packetIds)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::CHAT_DELETED".PadRight(72, ' ') + "==>   groupId = " + groupId + ", senderId = " + senderId + ", packetIds = " + packetIds.ToString("\n"));
#endif

                if (senderId == DefaultSettings.LOGIN_TABLE_ID)
                {
                    RecentChatCallActivityDAO.DeleteChatHistory(0, groupId, packetIds);
                    RecentLoadUtility.RemoveRange(groupId, packetIds);
                    FileTransferSession.CancelTransferFileByPacketIDs(groupId, packetIds, false);
                }
                else
                {
                    RecentChatCallActivityDAO.DeleteChatHistoryByMessageType(0, groupId, packetIds);
                    RecentLoadUtility.RemoveRangeByMessageType(groupId, packetIds);
                    FileTransferSession.CancelTransferFileByPacketIDs(groupId, packetIds, false);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupChatDeleted() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupChatDeleteRequestStatus(long groupId, string packetId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::CHAT_DELETED_CONFIRMED".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetId + ", status = " + status);
#endif
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupChatDeleteConfirmed() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupHistoryMessageRequestStatus(long groupId, string packetId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::CHAT_HISTORY_REQUEST_SENT".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetId + ", status = " + status + ", Time = " + PacketIDGenerator.GetUnixTimestamp(packetId));
#endif

                InvokeDelegate(new ChatEventArgs { PacketID = packetId, GroupID = groupId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupHistoryMessageRequestSent() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupHistoryMessageReceived(long groupId, List<BaseMessageDTO> groupHistoryMessageList, int direction)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::CHAT_HISTORY_RECEIVED".PadRight(72, ' ') + "==>   groupId = " + groupId + ", direction = " + direction + ", groupHistoryMessageList = " + groupHistoryMessageList.ToString("\n"));
#endif

                if (groupHistoryMessageList == null || groupHistoryMessageList.Count <= 0 || groupId == 0)
                    return;

                string minPacketID = String.Empty;
                string maxPacketID = String.Empty;
                ObservableDictionary<string, long> unreadList = null;
                int status = ChatConstants.STATUS_SEEN;

                if (direction == ChatConstants.HISTORY_UP)
                {
                    minPacketID = groupHistoryMessageList.Last().PacketID;
                    maxPacketID = groupHistoryMessageList.First().PacketID;
                }
                else
                {
                    minPacketID = groupHistoryMessageList.First().PacketID;
                    maxPacketID = groupHistoryMessageList.Last().PacketID;
                    unreadList = ChatViewModel.Instance.GetUnreadChatByID(groupId);
                    status = ChatConstants.STATUS_DELIVERED;
                }
                ChatHelpers.UpdateGroupHistoryTime(groupId, minPacketID, maxPacketID, null);

#if CHAT_LOG
                log.Debug("HANDLER::GROUP::MIN_MAX_PACKETID".PadRight(72, ' ') + "==>   maxPacketID = " + maxPacketID + ", maxPacketTime = " + PacketIDGenerator.GetUnixTimestamp(maxPacketID) + ", minPacketID = " + minPacketID + ", minPacketTime = " + PacketIDGenerator.GetUnixTimestamp(minPacketID));
#endif

                List<RecentDTO> recentDTOs = new List<RecentDTO>();
                List<MessageDTO> messageDTOs = new List<MessageDTO>();

                foreach (BaseMessageDTO entity in groupHistoryMessageList)
                {
                    if (entity.MessageDate <= 0) continue;

                    MessageDTO msgDTO = new MessageDTO();
                    msgDTO.GroupID = groupId;
                    msgDTO.SenderTableID = entity.SenderID;
                    msgDTO.PacketID = entity.PacketID;
                    msgDTO.MessageType = entity.MessageType;
                    msgDTO.PacketType = entity.IsEdited ? ChatConstants.PACKET_TYPE_GROUP_MESSAGE_EDIT : ChatConstants.PACKET_TYPE_GROUP_MESSAGE;
                    msgDTO.OriginalMessage = entity.Message;
                    msgDTO.MessageDate = entity.MessageDate;
                    msgDTO.MessageDelieverDate = entity.MessageDate;
                    msgDTO.MessageSeenDate = entity.MessageDate;
                    msgDTO.Status = status;
                    msgDTO.IsUnread = unreadList != null && unreadList.Count > 0 && unreadList.ContainsKey(msgDTO.PacketID);
                    ChatJSONParser.ParseMessage(msgDTO);
                    msgDTO.FileStatus = msgDTO.IsFileCancelled ? ChatConstants.FILE_TRANSFER_CANCELLED : msgDTO.FileStatus;

                    if (msgDTO.IsChatMessage)
                    {
                        recentDTOs.Add(new RecentDTO { ContactID = msgDTO.GroupID.ToString(), GroupID = msgDTO.GroupID, Message = msgDTO, Time = msgDTO.MessageDate });
                        messageDTOs.Add(msgDTO);
                    }
                }

                new InsertIntoGroupMessageTable(messageDTOs).Start();
                RecentLoadUtility.LoadRecentData(recentDTOs);

                MessageDTO maxMessageDTO = messageDTOs.Where(P => P.MessageType != (int)MessageType.DELETE_MESSAGE).OrderByDescending(P => P.MessageDate).FirstOrDefault();
                if (maxMessageDTO != null)
                {
                    ChatLogLoadUtility.LoadRecentData(new RecentDTO { ContactID = groupId.ToString(), GroupID = groupId, Message = maxMessageDTO, Time = maxMessageDTO.MessageDate });
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupHistoryMessageReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupHistoryCompleted(long groupId, int direction)
        {
            try
            {
                if (direction == ChatConstants.HISTORY_UP)
                {
#if CHAT_LOG
                    log.Debug("HANDLER::GROUP::CHAT_HISTORY_COMPLETED".PadRight(72, ' ') + "==>   groupId = " + groupId);
#endif

                    GroupInfoModel model = RingIDViewModel.Instance.GroupList.TryGetValue(groupId);
                    if (model != null)
                    {
                        model.ChatHistoryNotFound = true;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupHistoryCompleted() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupGetChatMessageReceived(long groupId, BaseMessageDTO groupChatMessageDTO)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::GET_MESSAAGE_PACKETID_SUCCESS".PadRight(72, ' ') + "==>   groupId = " + groupId + ", MessageDTO = " + groupChatMessageDTO.ToString());
#endif

                MessageDTO messageDTO = RecentChatCallActivityDAO.GetMessageDTOByPacketID(null, groupId, groupChatMessageDTO.PacketID);
                if (messageDTO != null)
                {
                    messageDTO.OriginalMessage = groupChatMessageDTO.Message;
                    ChatJSONParser.ParseMessage(messageDTO);
                    messageDTO.FileStatus = messageDTO.IsFileCancelled ? ChatConstants.FILE_TRANSFER_CANCELLED : messageDTO.FileStatus;
                    RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                    if (messageDTO.FileID > 0)
                    {
                        RecentModel chatModel = GetRecentModel(0, messageDTO.GroupID, null, messageDTO.PacketID);
                        if (chatModel != null && chatModel.Message != null)
                        {
                            chatModel.Message.FileMenifest = messageDTO.FileMenifest;
                            chatModel.Message.FileStatus = messageDTO.FileStatus;
                        }
                    }
                }

                InvokeDelegate(new ChatEventArgs { PacketID = groupChatMessageDTO.PacketID, GroupID = groupId, Status = true });
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupGetChatMessageReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupGetChatMessageFailedToSend(long groupId, string packetId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::GET_MESSAAGE_PACKETID_FAILED".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetId);
#endif

                InvokeDelegate(new ChatEventArgs { PacketID = packetId, GroupID = groupId, Status = false });
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupGetChatMessageFailedToSend() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupChatMessageSeenListReceived(long groupId, string packetId, List<BaseMemberDTO> memberList)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::MESSAGE_SEEN_MEMBER_LIST_BY_PACKETID".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetId + ", memberList = " + memberList.ToString("\n"));
#endif
                if (memberList == null || memberList.Count == 0)
                    return;

                RecentModel chatModel = GetRecentModel(0, groupId, null, packetId);
                if (chatModel.Message == null)
                    return;

                foreach (BaseMemberDTO memberDTO in memberList)
                {
                    GroupMemberInfoModel memberModel = chatModel.Message.SeenByList.TryGetValue(memberDTO.MemberIdentity);
                    if (memberModel == null)
                    {
                        memberModel = new GroupMemberInfoModel();
                        Application.Current.Dispatcher.BeginInvoke(() => { chatModel.Message.SeenByList[memberDTO.MemberIdentity] = memberModel; }, DispatcherPriority.Send);
                    }
                    memberModel.UserTableID = memberDTO.MemberIdentity;
                    memberModel.FullName = memberDTO.FullName;
                    memberModel.UpdateTime = memberDTO.SeenTime;
                    if (memberModel.BasicInfoModel == null)
                    {
                        memberModel.BasicInfoModel = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(memberModel.UserTableID, memberModel.RingID, memberModel.FullName);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupChatMessageSeenListReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupChatMessageSeenListRequestStatus(long groupId, string requestPacketId, string messagePacketId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::MESSAGE_SEEN_LIST_BY_PACKETID_CONFIRMATION".PadRight(72, ' ') + "==>   groupId = " + groupId + ", requestPacketId = " + requestPacketId + ", messagePacketId = " + messagePacketId + ", status = " + status);
#endif
                InvokeDelegate(new ChatEventArgs { PacketID = requestPacketId, GroupID = groupId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupChatMessageSeenListRequestStatus() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupChatConversationListReceived(List<BaseMessageDTO> conversationList)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::CONVERSATION_LIST".PadRight(72, ' ') + "==>   conversationList = " + conversationList.ToString("\n"));
#endif
                if (conversationList == null || conversationList.Count <= 0)
                    return;

                List<MessageDTO> messageDTOs = new List<MessageDTO>();
                List<long> groupIds = RingIDViewModel.Instance.GroupList.Keys.ToList();

                foreach (BaseMessageDTO entity in conversationList)
                {
                    if (entity.MessageDate <= 0) continue;

                    MessageDTO msgDTO = new MessageDTO();
                    msgDTO.GroupID = entity.GroupID;
                    msgDTO.SenderTableID = entity.SenderID;
                    //msgDTO.PacketID = entity.PacketID;
                    msgDTO.MessageType = entity.MessageType;
                    msgDTO.PacketType = entity.IsEdited ? ChatConstants.PACKET_TYPE_GROUP_MESSAGE_EDIT : ChatConstants.PACKET_TYPE_GROUP_MESSAGE;
                    switch ((MessageType)msgDTO.MessageType)
                    {
                        case MessageType.PLAIN_MESSAGE:
                        case MessageType.EMOTICON_MESSAGE:
                            msgDTO.OriginalMessage = entity.Message;
                            msgDTO.Message = entity.Message;
                            break;
                    }
                    msgDTO.MessageDate = entity.MessageDate;
                    msgDTO.MessageDelieverDate = entity.MessageDate;
                    msgDTO.MessageSeenDate = entity.MessageDate;
                    msgDTO.Status = ChatConstants.STATUS_SEEN;
                    msgDTO.FileStatus = msgDTO.IsFileCancelled ? ChatConstants.FILE_TRANSFER_CANCELLED : msgDTO.FileStatus;
                    messageDTOs.Add(msgDTO);

                    if (!groupIds.Contains(msgDTO.GroupID)) ChatService.GetGroupInformationWithMembers(msgDTO.GroupID, null);
                    ChatLogLoadUtility.LoadRecentData(new RecentDTO { ContactID = msgDTO.GroupID.ToString(), GroupID = msgDTO.GroupID, Message = msgDTO, Time = msgDTO.MessageDate });
                }
                new InsertIntoGroupMessageTable(messageDTOs).Start();
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupChatConversationListReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Group Chat

        #region Group Manipulation

        public override void onGroupCreated(long groupId, string packetId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::CREATED".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetId + ", status = " + status);
#endif

                InvokeDelegate(new ChatEventArgs { PacketID = packetId, GroupID = groupId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupCreated() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onMyGroupListReceived(List<BaseGroupDTO> myGroupList)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::GROUP_LIST_RECEIVED".PadRight(72, ' ') + "==>   myGroupList = " + myGroupList.ToString("\n"));
#endif

                List<GroupInfoDTO> groupInfoDTOs = GroupInfoDAO.GetGroupInfoDTOsByGroupIDs(myGroupList.Select(P => P.GroupID).ToList());

                foreach (BaseGroupDTO gDTO in myGroupList)
                {
                    GroupInfoDTO groupInfoDTO = groupInfoDTOs.Where(P => P.GroupID == gDTO.GroupID).FirstOrDefault();
                    if (groupInfoDTO == null)
                    {
                        groupInfoDTO = new GroupInfoDTO { GroupID = gDTO.GroupID };
                        groupInfoDTOs.Add(groupInfoDTO);
                    }
                    groupInfoDTO.IsPartial = true;
                    groupInfoDTO.GroupName = gDTO.GroupName;
                    groupInfoDTO.NumberOfMembers = gDTO.NumberOfMembers;
                    groupInfoDTO.GroupProfileImage = gDTO.GroupUrl;

                    GroupInfoModel model = RingIDViewModel.Instance.GroupList.TryGetValue(gDTO.GroupID);
                    if (model != null)
                    {
                        model.LoadData(groupInfoDTO);
                        //TRIGGER TO GROUP IMAGE IN GROUP CHAT & LOG PANEL
                        model.OnPropertyChanged("CurrentInstance");
                    }
                    else
                    {
                        model = new GroupInfoModel(groupInfoDTO);
                        Application.Current.Dispatcher.BeginInvoke(delegate
                        {
                            RingIDViewModel.Instance.GroupList[groupInfoDTO.GroupID] = model;
                        });
                    }
                }

                new InsertIntoGroupOrMemberTable(groupInfoDTOs).Start();
                SettingsConstants.VALUE_RINGID_GROUP_UT = ChatService.GetServerTime();
                new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_GROUP_UT, SettingsConstants.VALUE_RINGID_GROUP_UT.ToString());
            }
            catch (Exception ex)
            {
                log.Error("Error at onMyGroupListReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupNameChangeRequestStatus(long groupId, string packetId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::NAME_CHANGE_SENT".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetId + ", status = " + status);
#endif

                InvokeDelegate(new ChatEventArgs { PacketID = packetId, GroupID = groupId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupNameChangeRequestStatus() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupUrlChangeRequestStatus(long groupId, string packetId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::IMAGE_URL_CHANGE_SENT".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetId + ", status = " + status);
#endif

                InvokeDelegate(new ChatEventArgs { PacketID = packetId, GroupID = groupId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupUrlChangeRequestStatus() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupNameChanged(long senderId, long groupId, string groupName)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::NAME_CHANGED".PadRight(72, ' ') + "==>   groupId = " + groupId + ", senderId = " + senderId + ", groupName = " + groupName);
#endif

                GroupInfoDTO groupInfoDTO = GroupInfoDAO.GetGroupInfoDTOByGroupID(groupId);
                if (groupInfoDTO == null)
                {
                    groupInfoDTO = new GroupInfoDTO { GroupID = groupId, IsPartial = true };
                }
                groupInfoDTO.GroupName = groupName;

                List<GroupInfoDTO> list = new List<GroupInfoDTO>();
                list.Add(groupInfoDTO);
                new InsertIntoGroupOrMemberTable(list).Start();

                GroupInfoModel model = RingIDViewModel.Instance.GroupList.TryGetValue(groupId);
                if (model != null)
                {
                    model.LoadData(groupInfoDTO);

                    ActivityDTO activityDTO = new ActivityDTO();
                    PacketTimeID packet = ChatService.GeneratePacketID();
                    activityDTO.UpdateTime = packet.Time;
                    activityDTO.PacketID = packet.PacketID;
                    activityDTO.GroupID = groupId;
                    activityDTO.ActivityBy = senderId;
                    activityDTO.ActivityType = (int)GroupChangeActivity.GROUP_RENAME;
                    activityDTO.GroupName = groupInfoDTO.GroupName;
                    activityDTO.MessageType = ActivityConstants.MSG_GROUP_NAME_CHANGE;

                    List<ActivityDTO> activityList = new List<ActivityDTO>();
                    activityList.Add(activityDTO);
                    new InsertIntoRingActivityTable(activityList).Start();
                    RecentLoadUtility.LoadRecentData(new RecentDTO { ContactID = groupId.ToString(), GroupID = groupId, Activity = activityDTO, Time = activityDTO.UpdateTime });
                }
                else
                {
                    model = new GroupInfoModel(groupInfoDTO);
                    Application.Current.Dispatcher.BeginInvoke(delegate
                    {
                        RingIDViewModel.Instance.GroupList[groupInfoDTO.GroupID] = model;
                    });
                }

                SettingsConstants.VALUE_RINGID_GROUP_UT = ChatService.GetServerTime();
                new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_GROUP_UT, SettingsConstants.VALUE_RINGID_GROUP_UT.ToString());
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupNameChanged() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupUrlChanged(long senderId, long groupId, string groupUrl)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::IMAGE_URL_CHANGED".PadRight(72, ' ') + "==>   groupId = " + groupId + ", senderId = " + senderId + ", groupUrl = " + groupUrl);
#endif

                GroupInfoDTO groupInfoDTO = GroupInfoDAO.GetGroupInfoDTOByGroupID(groupId);
                if (groupInfoDTO == null)
                {
                    groupInfoDTO = new GroupInfoDTO { GroupID = groupId, GroupName = groupId.ToString(), IsPartial = true };
                }
                groupInfoDTO.GroupProfileImage = groupUrl;

                List<GroupInfoDTO> list = new List<GroupInfoDTO>();
                list.Add(groupInfoDTO);
                new InsertIntoGroupOrMemberTable(list).Start();

                GroupInfoModel model = RingIDViewModel.Instance.GroupList.TryGetValue(groupId);
                if (model != null)
                {
                    model.LoadData(groupInfoDTO);

                    ActivityDTO activityDTO = new ActivityDTO();
                    PacketTimeID packet = ChatService.GeneratePacketID();
                    activityDTO.UpdateTime = packet.Time;
                    activityDTO.PacketID = packet.PacketID;
                    activityDTO.GroupID = groupId;
                    activityDTO.ActivityBy = senderId;
                    activityDTO.ActivityType = (int)GroupChangeActivity.GROUP_URL_RENAME;
                    activityDTO.GroupProfileImage = groupInfoDTO.GroupProfileImage;
                    activityDTO.MessageType = ActivityConstants.MSG_GROUP_PROFILE_IMAGE_CHANGE;
                    //TRIGGER TO GROUP IMAGE IN GROUP CHAT & LOG PANEL
                    model.OnPropertyChanged("CurrentInstance");

                    List<ActivityDTO> activityList = new List<ActivityDTO>();
                    activityList.Add(activityDTO);
                    new InsertIntoRingActivityTable(activityList).Start();
                    RecentLoadUtility.LoadRecentData(new RecentDTO { ContactID = groupId.ToString(), GroupID = groupId, Activity = activityDTO, Time = activityDTO.UpdateTime });
                }
                else
                {
                    model = new GroupInfoModel(groupInfoDTO);
                    Application.Current.Dispatcher.BeginInvoke(delegate
                    {
                        RingIDViewModel.Instance.GroupList[groupId] = model;
                    });
                }

                SettingsConstants.VALUE_RINGID_GROUP_UT = ChatService.GetServerTime();
                new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_GROUP_UT, SettingsConstants.VALUE_RINGID_GROUP_UT.ToString());
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupUrlChanged() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupInformationWithMembersReceived(long groupId, string groupName, string groupUrl, List<BaseMemberDTO> memberList)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::INFORMATION_WITH_MEMBERS_RECEIVED".PadRight(72, ' ') + "==>   groupId = " + groupId + ", groupName = " + groupName + ", groupUrl = " + groupUrl + ", memberList = " + memberList.ToString("\n"));
#endif

                List<GroupInfoDTO> groupInfoDTOs = new List<GroupInfoDTO>();

                GroupInfoDTO groupInfoDTO = GroupInfoDAO.GetGroupInfoDTOByGroupID(groupId);
                List<GroupMemberInfoDTO> groupMemberInfoDTOs = GroupInfoDAO.GetAllGroupMemberListByGroupID(groupId);

                if (groupInfoDTO == null)
                {
                    groupInfoDTO = new GroupInfoDTO { GroupID = groupId };
                }
                if (memberList == null)
                {
                    memberList = new List<BaseMemberDTO>();
                }
                groupInfoDTO.GroupName = groupName;
                groupInfoDTO.GroupProfileImage = groupUrl;
                groupInfoDTO.NumberOfMembers = memberList.Count;
                groupInfoDTOs.Add(groupInfoDTO);

                GroupInfoModel model = RingIDViewModel.Instance.GroupList.TryGetValue(groupId);
                if (model != null)
                {
                    model.LoadData(groupInfoDTO);
                    //TRIGGER TO GROUP IMAGE IN GROUP CHAT & LOG PANEL
                    model.OnPropertyChanged("CurrentInstance");
                }
                else
                {
                    model = new GroupInfoModel(groupInfoDTO);
                    Application.Current.Dispatcher.BeginInvoke(delegate
                    {
                        RingIDViewModel.Instance.GroupList[groupId] = model;
                    });
                }

                int numberOfMembers = 0;
                bool isGroupMember = true;

                foreach (BaseMemberDTO i in memberList)
                {
                    GroupMemberInfoDTO memberInfoDTO = groupMemberInfoDTOs.Where(P => P.UserTableID == i.MemberIdentity).FirstOrDefault();
                    if (memberInfoDTO == null)
                    {
                        memberInfoDTO = new GroupMemberInfoDTO();
                        groupMemberInfoDTOs.Add(memberInfoDTO);
                    }
                    memberInfoDTO.GroupID = groupId;
                    memberInfoDTO.UserTableID = i.MemberIdentity;
                    memberInfoDTO.RingID = i.RingID;
                    memberInfoDTO.FullName = i.FullName;
                    memberInfoDTO.MemberAddedBy = i.AddedBy;

                    switch (i.Status)
                    {
                        case ChatConstants.MEMBER_TYPE_MEMBER:
                        case ChatConstants.MEMBER_TYPE_ADMIN:
                        case ChatConstants.MEMBER_TYPE_OWNER:
                            {
                                memberInfoDTO.MemberAccessType = i.Status;
                                numberOfMembers++;
                            }
                            break;
                        default:
                            {
                                memberInfoDTO.MemberAccessType = ChatConstants.MEMBER_TYPE_NOT_MEMBER;
                                isGroupMember = memberInfoDTO.UserTableID == DefaultSettings.LOGIN_TABLE_ID ? false : isGroupMember;
                            }
                            break;
                    }

                    if (model != null)
                    {
                        model.LoadMemberData(memberInfoDTO);
                    }
                }

                if (isGroupMember == false)
                {
                    UCGroupChatSettings ucGroupChatSettings = UIDictionaries.Instance.GROUP_CHAT_SETTINGS_DICTIONARY.TryGetValue(groupId);
                    if (ucGroupChatSettings != null)
                    {
                        Application.Current.Dispatcher.BeginInvoke(() =>
                        {
                            ucGroupChatSettings.CancelGroupNameChange();
                            ucGroupChatSettings.CancelGroupImageChange();
                        });
                    }

                    if (ChatService.HasGroupRegistration(groupId))
                    {
                        ChatService.UnRegisterGroupChat(groupId, StatusConstants.PRESENCE_ONLINE, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.Mood);
                    }

                    //List<string> tempIds = new List<string>();
                    //tempIds.Add(groupId.ToString());
                    //RecentChatCallActivityDAO.DeleteChatHistory(0, groupId, tempIds);

                    //if (UCChatLogPanel.Instance != null)
                    //{
                    //    UCChatLogPanel.Instance.LoadChatLogByContactID(groupId.ToString());
                    //}
                }

                groupInfoDTO.NumberOfMembers = numberOfMembers;
                model.NumberOfMembers = numberOfMembers;
                groupInfoDTO.IsPartial = false;
                model.IsPartial = false;

                new InsertIntoGroupOrMemberTable(groupInfoDTOs, groupMemberInfoDTOs).Start();
                SettingsConstants.VALUE_RINGID_GROUP_UT = ChatService.GetServerTime();
                new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_GROUP_UT, SettingsConstants.VALUE_RINGID_GROUP_UT.ToString());
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupInformationWithMembersReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupInformationWithMembersRequestStatus(long groupId, string packetId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::INFORMATION_RECEIVED_STATUS".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetId + ", status = " + status);
#endif
                InvokeDelegate(new ChatEventArgs { PacketID = packetId, GroupID = groupId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupInformationWithMembersReceivedFailed() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupMemberAdded(long senderId, long groupId, List<BaseMemberDTO> memberList, string packetId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::MEMBER_ADDED".PadRight(72, ' ') + "==>   groupId = " + groupId + ", senderId = " + senderId + ", packetId = " + packetId + ", memberList = " + memberList.ToString("\n"));
#endif

                List<GroupMemberInfoDTO> groupMemberInfoDTOs = new List<GroupMemberInfoDTO>();
                GroupInfoModel groupInfoModel = RingIDViewModel.Instance.GroupList.TryGetValue(groupId);

                foreach (BaseMemberDTO mDTO in memberList)
                {
                    GroupMemberInfoDTO memberInfoDTO = new GroupMemberInfoDTO();
                    memberInfoDTO.GroupID = groupId;
                    memberInfoDTO.UserTableID = mDTO.MemberIdentity;
                    memberInfoDTO.RingID = mDTO.RingID;
                    memberInfoDTO.FullName = mDTO.FullName;
                    memberInfoDTO.MemberAccessType = mDTO.Status;
                    memberInfoDTO.MemberAddedBy = senderId;
                    groupMemberInfoDTOs.Add(memberInfoDTO);

                    if (groupInfoModel != null)
                    {
                        groupInfoModel.LoadMemberData(memberInfoDTO);
                    }
                }

                if (groupInfoModel != null)
                {
                    groupInfoModel.NumberOfMembers = ChatHelpers.GetGroupMemberCount(groupInfoModel);
                    GroupInfoDAO.UpdateNumberOfMembers(groupInfoModel.GroupID, groupInfoModel.NumberOfMembers);
                }

                new InsertIntoGroupOrMemberTable(groupMemberInfoDTOs).Start();
                SettingsConstants.VALUE_RINGID_GROUP_UT = ChatService.GetServerTime();
                new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_GROUP_UT, SettingsConstants.VALUE_RINGID_GROUP_UT.ToString());

                ChatService.RequestGroupChatHistory(groupId, ChatService.GeneratePacketID().PacketID, ChatConstants.HISTORY_UP, memberList.Count, null);
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupMemberAdded() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupMemberAddRequestStatus(long groupId, string packetId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::MEMBER_ADD_SENT".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetId + ", status = " + status);
#endif

                InvokeDelegate(new ChatEventArgs { PacketID = packetId, GroupID = groupId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupMemberAddSent() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupMemberRemovedOrLeft(long senderId, long groupId, List<long> memberList, string packetId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::MEMBER_REMOVED_LEFT".PadRight(72, ' ') + "==>   groupId = " + groupId + ", senderId = " + senderId + ", packetId = " + packetId + ", memberList = " + memberList.ToString("\n"));
#endif

                List<GroupMemberInfoDTO> groupMemberInfoDTOs = new List<GroupMemberInfoDTO>();
                GroupInfoModel groupInfoModel = RingIDViewModel.Instance.GroupList.TryGetValue(groupId);

                foreach (long memberIdentity in memberList)
                {
                    GroupMemberInfoDTO memberInfoDTO = new GroupMemberInfoDTO();
                    memberInfoDTO.GroupID = groupId;
                    memberInfoDTO.UserTableID = memberIdentity;
                    memberInfoDTO.IntegerStatus = StatusConstants.STATUS_DELETED;
                    groupMemberInfoDTOs.Add(memberInfoDTO);

                    if (groupInfoModel != null)
                    {
                        groupInfoModel.RemoveMemberData(memberInfoDTO.UserTableID);
                    }
                }

                if (groupInfoModel != null)
                {
                    groupInfoModel.NumberOfMembers = ChatHelpers.GetGroupMemberCount(groupInfoModel);
                    GroupInfoDAO.UpdateNumberOfMembers(groupInfoModel.GroupID, groupInfoModel.NumberOfMembers);
                }

                if (memberList.Contains(DefaultSettings.LOGIN_TABLE_ID))
                {
                    UCGroupChatSettings ucGroupChatSettings = UIDictionaries.Instance.GROUP_CHAT_SETTINGS_DICTIONARY.TryGetValue(groupId);
                    if (ucGroupChatSettings != null)
                    {
                        Application.Current.Dispatcher.BeginInvoke(() =>
                        {
                            ucGroupChatSettings.CancelGroupNameChange();
                            ucGroupChatSettings.CancelGroupImageChange();
                        });
                    }

                    ChatService.UnRegisterGroupChat(groupId, StatusConstants.PRESENCE_ONLINE, RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel.Mood);
                }

                new InsertIntoGroupOrMemberTable(groupMemberInfoDTOs).Start();
                SettingsConstants.VALUE_RINGID_GROUP_UT = ChatService.GetServerTime();
                new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_GROUP_UT, SettingsConstants.VALUE_RINGID_GROUP_UT.ToString());

                ChatService.RequestGroupChatHistory(groupId, ChatService.GeneratePacketID().PacketID, ChatConstants.HISTORY_UP, memberList.Count, null);
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupMemberRemovedOrLeft() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupMemberRemoveLeaveRequestStatus(long groupId, string packetId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::MEMBER_LEAVE_REMOVE_SENT".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetId + ", status = " + status);
#endif

                InvokeDelegate(new ChatEventArgs { PacketID = packetId, GroupID = groupId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupMemberRemoveLeaveSent() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupMemberStatusChanged(long senderId, long groupId, List<BaseMemberDTO> memberList, string packetId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::MEMBER_STATUS_CHANGED".PadRight(72, ' ') + "==>   groupId = " + groupId + ", senderId = " + senderId + ", packetId = " + packetId + ", memberList = " + memberList.ToString("\n"));
#endif

                List<GroupMemberInfoDTO> groupMemberInfoDTOs = new List<GroupMemberInfoDTO>();
                GroupInfoModel groupModel = RingIDViewModel.Instance.GroupList.TryGetValue(groupId);

                foreach (BaseMemberDTO mDTO in memberList)
                {
                    GroupMemberInfoDTO memberInfoDTO = new GroupMemberInfoDTO();
                    memberInfoDTO.GroupID = groupId;
                    memberInfoDTO.UserTableID = mDTO.MemberIdentity;
                    memberInfoDTO.MemberAccessType = mDTO.Status;
                    memberInfoDTO.IntegerStatus = StatusConstants.STATUS_UPDATED;
                    groupMemberInfoDTOs.Add(memberInfoDTO);

                    GroupMemberInfoModel memberModel = groupModel != null ? groupModel.MemberInfoDictionary.TryGetValue(mDTO.MemberIdentity) : null;
                    if (memberModel != null)
                    {
                        memberModel.MemberAccessType = mDTO.Status;
                    }
                }

                if (memberList.FirstOrDefault(P => P.MemberIdentity == DefaultSettings.LOGIN_TABLE_ID) != null)
                {
                    UCGroupChatSettings ucGroupChatSettings = UIDictionaries.Instance.GROUP_CHAT_SETTINGS_DICTIONARY.TryGetValue(groupId);
                    if (ucGroupChatSettings != null)
                    {
                        Application.Current.Dispatcher.BeginInvoke(() =>
                        {
                            ucGroupChatSettings.CancelGroupNameChange();
                            ucGroupChatSettings.CancelGroupImageChange();
                        });
                    }
                }

                new InsertIntoGroupOrMemberTable(groupMemberInfoDTOs).Start();
                SettingsConstants.VALUE_RINGID_GROUP_UT = ChatService.GetServerTime();
                new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_GROUP_UT, SettingsConstants.VALUE_RINGID_GROUP_UT.ToString());

                ChatService.RequestGroupChatHistory(groupId, ChatService.GeneratePacketID().PacketID, ChatConstants.HISTORY_UP, memberList.Count, null);
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupMemberStatusChanged() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupMemberStatusChangeRequestStatus(long groupId, string packetId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::MEMBER_STATUS_CHANGE_SENT".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetId + ", status = " + status);
#endif

                InvokeDelegate(new ChatEventArgs { PacketID = packetId, GroupID = groupId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupMemberStatusChangeSent() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupInformationActivityReceived(List<BaseGroupActivityDTO> groupActivityList)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::ACTIVITY_RECEIVED".PadRight(72, ' ') + "==>   groupActivityList = " + groupActivityList.ToString("\n"));
#endif
                if (groupActivityList == null || groupActivityList.Count == 0)
                    return;

                ConcurrentDictionary<long, List<RecentDTO>> recentListByGroupID = new ConcurrentDictionary<long, List<RecentDTO>>();
                List<ActivityDTO> activityDTOs = new List<ActivityDTO>();

                var groups = groupActivityList.GroupBy(item => item.GroupID);
                foreach (var group in groups)
                {
                    long groupId = group.Key;
                    string groupName = String.Empty;
                    string groupImage = String.Empty;
                    List<RecentDTO> recentDTOs = new List<RecentDTO>();
                    ConcurrentDictionary<long, GroupMemberInfoDTO> groupMemberInfoDTOs = new ConcurrentDictionary<long, GroupMemberInfoDTO>();
                    recentListByGroupID[groupId] = recentDTOs;

                    foreach (BaseGroupActivityDTO baseDTO in group)
                    {
                        ActivityDTO activityDTO = new ActivityDTO();
                        activityDTO.GroupID = groupId;
                        activityDTO.UpdateTime = baseDTO.UpdateTime;
                        activityDTO.ActivityBy = baseDTO.ChangedByUserID;
                        activityDTO.PacketID = baseDTO.PacketID;
                        activityDTO.ActivityType = baseDTO.ActivityType;

                        switch ((GroupChangeActivity)activityDTO.ActivityType)
                        {
                            case GroupChangeActivity.GROUP_CREATED:
                                {
                                    activityDTO.UpdateTime = activityDTO.UpdateTime - 1;
                                    activityDTO.MessageType = ActivityConstants.MSG_GROUP_CREATE;
                                    activityDTOs.Add(activityDTO);
                                }
                                break;
                            case GroupChangeActivity.GROUP_RENAME:
                                {
                                    activityDTO.GroupName = baseDTO.GroupNameOrUrlString;
                                    activityDTO.MessageType = ActivityConstants.MSG_GROUP_NAME_CHANGE;
                                    activityDTOs.Add(activityDTO);
                                    recentDTOs.Add(new RecentDTO { ContactID = groupId.ToString(), GroupID = groupId, Activity = activityDTO, UniqueKey = activityDTO.PacketID, Time = activityDTO.UpdateTime });

                                    groupName = activityDTO.GroupName;
                                }
                                break;
                            case GroupChangeActivity.GROUP_URL_RENAME:
                                {
                                    activityDTO.GroupProfileImage = baseDTO.GroupNameOrUrlString;
                                    activityDTO.MessageType = ActivityConstants.MSG_GROUP_PROFILE_IMAGE_CHANGE;
                                    activityDTOs.Add(activityDTO);
                                    recentDTOs.Add(new RecentDTO { ContactID = groupId.ToString(), GroupID = groupId, Activity = activityDTO, UniqueKey = activityDTO.PacketID, Time = activityDTO.UpdateTime });

                                    groupImage = activityDTO.GroupProfileImage;
                                }
                                break;
                            case GroupChangeActivity.ADDED:
                                {
                                    activityDTO.MessageType = ActivityConstants.MSG_GROUP_MEMBER_ADDED_INTO_GROUP;
                                    activityDTO.MemberList.Add(new ActivityMemberDTO { UID = baseDTO.MemberIdentity, RID = baseDTO.RingID });
                                    activityDTOs.Add(activityDTO);
                                    recentDTOs.Add(new RecentDTO { ContactID = groupId.ToString(), GroupID = groupId, Activity = activityDTO, UniqueKey = activityDTO.PacketID, Time = activityDTO.UpdateTime });

                                    GroupMemberInfoDTO memberInfoDTO = new GroupMemberInfoDTO();
                                    memberInfoDTO.GroupID = groupId;
                                    memberInfoDTO.UserTableID = baseDTO.MemberIdentity;
                                    memberInfoDTO.RingID = baseDTO.RingID;
                                    memberInfoDTO.FullName = baseDTO.GroupNameOrUrlString;
                                    memberInfoDTO.MemberAccessType = baseDTO.MemberType;
                                    memberInfoDTO.MemberAddedBy = baseDTO.ChangedByUserID;
                                    memberInfoDTO.IntegerStatus = StatusConstants.STATUS_INSERTED;
                                    groupMemberInfoDTOs[memberInfoDTO.UserTableID] = memberInfoDTO;
                                }
                                break;
                            case GroupChangeActivity.DELETED:
                                {
                                    activityDTO.MessageType = ActivityConstants.MSG_GROUP_MEMBER_REMOVED_FROM_GROUP;
                                    activityDTO.MemberList.Add(new ActivityMemberDTO { UID = baseDTO.MemberIdentity, RID = baseDTO.RingID });
                                    activityDTOs.Add(activityDTO);
                                    recentDTOs.Add(new RecentDTO { ContactID = groupId.ToString(), GroupID = groupId, Activity = activityDTO, UniqueKey = activityDTO.PacketID, Time = activityDTO.UpdateTime });

                                    GroupMemberInfoDTO memberInfoDTO = new GroupMemberInfoDTO();
                                    memberInfoDTO.GroupID = groupId;
                                    memberInfoDTO.UserTableID = baseDTO.MemberIdentity;
                                    memberInfoDTO.RingID = baseDTO.RingID;
                                    memberInfoDTO.FullName = baseDTO.GroupNameOrUrlString;
                                    memberInfoDTO.MemberAccessType = ChatConstants.MEMBER_TYPE_NOT_MEMBER;
                                    memberInfoDTO.MemberAddedBy = baseDTO.ChangedByUserID;
                                    memberInfoDTO.IntegerStatus = StatusConstants.STATUS_INSERTED;
                                    groupMemberInfoDTOs[memberInfoDTO.UserTableID] = memberInfoDTO;
                                }
                                break;
                            case GroupChangeActivity.LEAVE:
                                {
                                    activityDTO.MessageType = ActivityConstants.MSG_GROUP_MEMBER_LEFT_CONVERSATION;
                                    activityDTOs.Add(activityDTO);
                                    recentDTOs.Add(new RecentDTO { ContactID = groupId.ToString(), GroupID = groupId, Activity = activityDTO, UniqueKey = activityDTO.PacketID, Time = activityDTO.UpdateTime });

                                    GroupMemberInfoDTO memberInfoDTO = new GroupMemberInfoDTO();
                                    memberInfoDTO.GroupID = groupId;
                                    memberInfoDTO.UserTableID = baseDTO.MemberIdentity;
                                    memberInfoDTO.RingID = baseDTO.RingID;
                                    memberInfoDTO.FullName = baseDTO.GroupNameOrUrlString;
                                    memberInfoDTO.MemberAccessType = ChatConstants.MEMBER_TYPE_NOT_MEMBER;
                                    memberInfoDTO.MemberAddedBy = baseDTO.ChangedByUserID;
                                    memberInfoDTO.IntegerStatus = StatusConstants.STATUS_INSERTED;
                                    groupMemberInfoDTOs[memberInfoDTO.UserTableID] = memberInfoDTO;
                                }
                                break;
                            case GroupChangeActivity.MEMBER_TYPE_CHANGE:
                                {
                                    switch (baseDTO.MemberType)
                                    {
                                        case ChatConstants.MEMBER_TYPE_ADMIN:
                                            activityDTO.MessageType = ActivityConstants.MSG_GROUP_MEMBER_MADE_ADMIN;
                                            break;
                                        case ChatConstants.MEMBER_TYPE_OWNER:
                                            activityDTO.MessageType = ActivityConstants.MSG_GROUP_MEMBER_MADE_OWNER;
                                            break;
                                        case ChatConstants.MEMBER_TYPE_MEMBER:
                                            activityDTO.MessageType = ActivityConstants.MSG_GROUP_MEMBER_REMOVE_FROM_ADMIN;
                                            break;
                                    }
                                    activityDTO.MemberList.Add(new ActivityMemberDTO { UID = baseDTO.MemberIdentity, RID = baseDTO.RingID, ACCESS = baseDTO.MemberType });
                                    activityDTOs.Add(activityDTO);
                                    recentDTOs.Add(new RecentDTO { ContactID = groupId.ToString(), GroupID = groupId, Activity = activityDTO, UniqueKey = activityDTO.PacketID, Time = activityDTO.UpdateTime });

                                    GroupMemberInfoDTO memberInfoDTO = new GroupMemberInfoDTO();
                                    memberInfoDTO.GroupID = groupId;
                                    memberInfoDTO.UserTableID = baseDTO.MemberIdentity;
                                    memberInfoDTO.RingID = baseDTO.RingID;
                                    memberInfoDTO.FullName = baseDTO.GroupNameOrUrlString;
                                    memberInfoDTO.MemberAccessType = baseDTO.MemberType;
                                    memberInfoDTO.MemberAddedBy = baseDTO.ChangedByUserID;
                                    memberInfoDTO.IntegerStatus = StatusConstants.STATUS_INSERTED;
                                    groupMemberInfoDTOs[memberInfoDTO.UserTableID] = memberInfoDTO;
                                }
                                break;
                            //END SWITCH
                        }
                    }

                    int? numberOfMembers = null;
                    List<GroupMemberInfoDTO> memberList = groupMemberInfoDTOs.Values.ToList();

                    GroupInfoModel groupInfoModel = RingIDViewModel.Instance.GroupList.TryGetValue(groupId);
                    if (groupInfoModel != null)
                    {
                        int prevCount = groupInfoModel.NumberOfMembers;
                        groupInfoModel.GroupName = !String.IsNullOrWhiteSpace(groupName) ? groupName : groupInfoModel.GroupName;
                        groupInfoModel.GroupProfileImage = !String.IsNullOrWhiteSpace(groupImage) ? groupImage : groupInfoModel.GroupProfileImage;

                        foreach (GroupMemberInfoDTO memberDTO in memberList)
                        {
                            groupInfoModel.LoadMemberData(memberDTO);
                        }

                        groupInfoModel.NumberOfMembers = ChatHelpers.GetGroupMemberCount(groupInfoModel);
                        numberOfMembers = prevCount != groupInfoModel.NumberOfMembers ? (int?)groupInfoModel.NumberOfMembers : null;
                    }

                    GroupInfoDAO.UpdateGroupNameProfileImageAndNumberOfMember(groupId, groupName, groupImage, numberOfMembers);
                    new InsertIntoGroupOrMemberTable(memberList).Start();

                    if (groupMemberInfoDTOs.ContainsKey(DefaultSettings.LOGIN_TABLE_ID))
                    {
                        UCGroupChatSettings ucGroupChatSettings = UIDictionaries.Instance.GROUP_CHAT_SETTINGS_DICTIONARY.TryGetValue(groupId);
                        if (ucGroupChatSettings != null)
                        {
                            Application.Current.Dispatcher.BeginInvoke(() =>
                            {
                                ucGroupChatSettings.CancelGroupNameChange();
                                ucGroupChatSettings.CancelGroupImageChange();
                            });
                        }
                    }
                }

                new InsertIntoRingActivityTable(activityDTOs).Start();

                SettingsConstants.VALUE_RINGID_GROUP_UT = ChatService.GetServerTime();
                new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_GROUP_UT, SettingsConstants.VALUE_RINGID_GROUP_UT.ToString());

                foreach (KeyValuePair<long, List<RecentDTO>> entrySet in recentListByGroupID)
                {
                    long groupID = entrySet.Key;
                    List<RecentDTO> recentDTOs = entrySet.Value;

                    if (recentDTOs.Count > 0)
                    {
                        RecentLoadUtility.LoadRecentData(recentDTOs);
                        ChatHelpers.UpdateGroupHistoryTime(groupID, recentDTOs.First().UniqueKey, recentDTOs.Last().UniqueKey, null);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupInformationActivityReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupInformationActivityFromHistoryMessageReceived(long groupId, List<BaseGroupActivityDTO> groupActivityList, int direction)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::ACTIVITY_HISTORY_RECEIVED".PadRight(72, ' ') + "==>   groupId = " + groupId + ", direction = " + direction + ", groupActivityList = " + groupActivityList.ToString("\n"));
#endif

                if (groupActivityList == null || groupActivityList.Count <= 0)
                    return;

                string minPacketID = String.Empty;
                string maxPacketID = String.Empty;
                if (direction == ChatConstants.HISTORY_UP)
                {
                    minPacketID = groupActivityList.Last().PacketID;
                    maxPacketID = groupActivityList.First().PacketID;
                }
                else
                {
                    minPacketID = groupActivityList.First().PacketID;
                    maxPacketID = groupActivityList.Last().PacketID;
                }
                GroupInfoModel groupInfoModel = RingIDViewModel.Instance.GroupList.TryGetValue(groupId);
                ChatHelpers.UpdateGroupHistoryTime(groupId, minPacketID, maxPacketID, groupInfoModel);

                List<RecentDTO> recentDTOs = new List<RecentDTO>();
                List<ActivityDTO> activityDTOs = new List<ActivityDTO>();

                List<string> groupNameList = new List<string>();
                List<string> groupImageList = new List<string>();
                ConcurrentDictionary<long, GroupMemberInfoDTO> groupMemberInfoDTOs = new ConcurrentDictionary<long, GroupMemberInfoDTO>();

                foreach (BaseGroupActivityDTO baseDTO in groupActivityList)
                {
                    ActivityDTO activityDTO = new ActivityDTO();
                    activityDTO.GroupID = groupId;
                    activityDTO.UpdateTime = baseDTO.UpdateTime;
                    activityDTO.ActivityBy = baseDTO.ChangedByUserID;
                    activityDTO.PacketID = baseDTO.PacketID;
                    activityDTO.ActivityType = baseDTO.ActivityType;

                    switch ((GroupChangeActivity)baseDTO.ActivityType)
                    {
                        case GroupChangeActivity.GROUP_CREATED:
                            {
                                activityDTO.UpdateTime = activityDTO.UpdateTime - 1;
                                activityDTO.MessageType = ActivityConstants.MSG_GROUP_CREATE;
                                activityDTOs.Add(activityDTO);
                                recentDTOs.Add(new RecentDTO { ContactID = groupId.ToString(), GroupID = groupId, Activity = activityDTO, Time = activityDTO.UpdateTime });
                            }
                            break;
                        case GroupChangeActivity.GROUP_RENAME:
                            {
                                activityDTO.GroupName = baseDTO.GroupNameOrUrlString;
                                activityDTO.MessageType = ActivityConstants.MSG_GROUP_NAME_CHANGE;
                                activityDTOs.Add(activityDTO);
                                recentDTOs.Add(new RecentDTO { ContactID = groupId.ToString(), GroupID = groupId, Activity = activityDTO, Time = activityDTO.UpdateTime });

                                if (direction == ChatConstants.HISTORY_DOWN)
                                {
                                    groupNameList.Add(activityDTO.GroupName);
                                }
                            }
                            break;
                        case GroupChangeActivity.GROUP_URL_RENAME:
                            {
                                activityDTO.GroupProfileImage = baseDTO.GroupNameOrUrlString;
                                activityDTO.MessageType = ActivityConstants.MSG_GROUP_PROFILE_IMAGE_CHANGE;
                                activityDTOs.Add(activityDTO);
                                recentDTOs.Add(new RecentDTO { ContactID = groupId.ToString(), GroupID = groupId, Activity = activityDTO, Time = activityDTO.UpdateTime });

                                if (direction == ChatConstants.HISTORY_DOWN)
                                {
                                    groupImageList.Add(activityDTO.GroupProfileImage);
                                }
                            }
                            break;
                        case GroupChangeActivity.ADDED:
                            {
                                activityDTO.MessageType = ActivityConstants.MSG_GROUP_MEMBER_ADDED_INTO_GROUP;
                                activityDTO.MemberList.Add(new ActivityMemberDTO { UID = baseDTO.MemberIdentity, RID = baseDTO.RingID });
                                activityDTOs.Add(activityDTO);
                                recentDTOs.Add(new RecentDTO { ContactID = groupId.ToString(), GroupID = groupId, Activity = activityDTO, Time = activityDTO.UpdateTime });

                                if (direction == ChatConstants.HISTORY_DOWN && !groupMemberInfoDTOs.ContainsKey(baseDTO.MemberIdentity))
                                {
                                    GroupMemberInfoDTO memberInfoDTO = new GroupMemberInfoDTO();
                                    memberInfoDTO.GroupID = groupId;
                                    memberInfoDTO.UserTableID = baseDTO.MemberIdentity;
                                    memberInfoDTO.RingID = baseDTO.RingID;
                                    memberInfoDTO.FullName = baseDTO.GroupNameOrUrlString;
                                    memberInfoDTO.MemberAccessType = baseDTO.MemberType;
                                    memberInfoDTO.MemberAddedBy = baseDTO.ChangedByUserID;
                                    memberInfoDTO.IntegerStatus = StatusConstants.STATUS_INSERTED;
                                    groupMemberInfoDTOs[baseDTO.MemberIdentity] = memberInfoDTO;
                                }
                            }
                            break;
                        case GroupChangeActivity.DELETED:
                            {
                                activityDTO.MessageType = ActivityConstants.MSG_GROUP_MEMBER_REMOVED_FROM_GROUP;
                                activityDTO.MemberList.Add(new ActivityMemberDTO { UID = baseDTO.MemberIdentity, RID = baseDTO.RingID });
                                activityDTOs.Add(activityDTO);
                                recentDTOs.Add(new RecentDTO { ContactID = groupId.ToString(), GroupID = groupId, Activity = activityDTO, Time = activityDTO.UpdateTime });

                                if (direction == ChatConstants.HISTORY_DOWN && !groupMemberInfoDTOs.ContainsKey(baseDTO.MemberIdentity))
                                {
                                    GroupMemberInfoDTO memberInfoDTO = new GroupMemberInfoDTO();
                                    memberInfoDTO.GroupID = groupId;
                                    memberInfoDTO.UserTableID = baseDTO.MemberIdentity;
                                    memberInfoDTO.RingID = baseDTO.RingID;
                                    memberInfoDTO.FullName = baseDTO.GroupNameOrUrlString;
                                    memberInfoDTO.MemberAccessType = ChatConstants.MEMBER_TYPE_NOT_MEMBER;
                                    memberInfoDTO.MemberAddedBy = baseDTO.ChangedByUserID;
                                    memberInfoDTO.IntegerStatus = StatusConstants.STATUS_INSERTED;
                                    groupMemberInfoDTOs[baseDTO.MemberIdentity] = memberInfoDTO;
                                }
                            }
                            break;
                        case GroupChangeActivity.LEAVE:
                            {
                                activityDTO.MessageType = ActivityConstants.MSG_GROUP_MEMBER_LEFT_CONVERSATION;
                                activityDTOs.Add(activityDTO);
                                recentDTOs.Add(new RecentDTO { ContactID = groupId.ToString(), GroupID = groupId, Activity = activityDTO, Time = activityDTO.UpdateTime });

                                if (direction == ChatConstants.HISTORY_DOWN && !groupMemberInfoDTOs.ContainsKey(baseDTO.MemberIdentity))
                                {
                                    GroupMemberInfoDTO memberInfoDTO = new GroupMemberInfoDTO();
                                    memberInfoDTO.GroupID = groupId;
                                    memberInfoDTO.UserTableID = baseDTO.MemberIdentity;
                                    memberInfoDTO.RingID = baseDTO.RingID;
                                    memberInfoDTO.FullName = baseDTO.GroupNameOrUrlString;
                                    memberInfoDTO.MemberAccessType = ChatConstants.MEMBER_TYPE_NOT_MEMBER;
                                    memberInfoDTO.MemberAddedBy = baseDTO.ChangedByUserID;
                                    memberInfoDTO.IntegerStatus = StatusConstants.STATUS_INSERTED;
                                    groupMemberInfoDTOs[baseDTO.MemberIdentity] = memberInfoDTO;
                                }
                            }
                            break;
                        case GroupChangeActivity.MEMBER_TYPE_CHANGE:
                            {
                                switch (baseDTO.MemberType)
                                {
                                    case ChatConstants.MEMBER_TYPE_ADMIN:
                                        activityDTO.MessageType = ActivityConstants.MSG_GROUP_MEMBER_MADE_ADMIN;
                                        break;
                                    case ChatConstants.MEMBER_TYPE_OWNER:
                                        activityDTO.MessageType = ActivityConstants.MSG_GROUP_MEMBER_MADE_OWNER;
                                        break;
                                    case ChatConstants.MEMBER_TYPE_MEMBER:
                                        activityDTO.MessageType = ActivityConstants.MSG_GROUP_MEMBER_REMOVE_FROM_ADMIN;
                                        break;
                                }
                                activityDTO.MemberList.Add(new ActivityMemberDTO { UID = baseDTO.MemberIdentity, RID = baseDTO.RingID, ACCESS = baseDTO.MemberType });
                                activityDTOs.Add(activityDTO);
                                recentDTOs.Add(new RecentDTO { ContactID = groupId.ToString(), GroupID = groupId, Activity = activityDTO, Time = activityDTO.UpdateTime });

                                if (direction == ChatConstants.HISTORY_DOWN && !groupMemberInfoDTOs.ContainsKey(baseDTO.MemberIdentity))
                                {
                                    GroupMemberInfoDTO memberInfoDTO = new GroupMemberInfoDTO();
                                    memberInfoDTO.GroupID = groupId;
                                    memberInfoDTO.UserTableID = baseDTO.MemberIdentity;
                                    memberInfoDTO.RingID = baseDTO.RingID;
                                    memberInfoDTO.FullName = baseDTO.GroupNameOrUrlString;
                                    memberInfoDTO.MemberAccessType = baseDTO.MemberType;
                                    memberInfoDTO.MemberAddedBy = baseDTO.ChangedByUserID;
                                    memberInfoDTO.IntegerStatus = StatusConstants.STATUS_INSERTED;
                                    groupMemberInfoDTOs[baseDTO.MemberIdentity] = memberInfoDTO;
                                }
                            }
                            break;
                    }
                }

                if (direction == ChatConstants.HISTORY_DOWN)
                {
                    int? numberOfMembers = null;
                    String groupName = groupNameList.FirstOrDefault();
                    String groupImage = groupImageList.FirstOrDefault();
                    List<GroupMemberInfoDTO> memberList = groupMemberInfoDTOs.Values.ToList();

                    if (groupInfoModel != null)
                    {
                        int prevCount = groupInfoModel.NumberOfMembers;
                        groupInfoModel.GroupName = !String.IsNullOrWhiteSpace(groupName) ? groupName : groupInfoModel.GroupName;
                        groupInfoModel.GroupProfileImage = !String.IsNullOrWhiteSpace(groupImage) ? groupImage : groupInfoModel.GroupProfileImage;

                        foreach (GroupMemberInfoDTO memberDTO in memberList)
                        {
                            groupInfoModel.LoadMemberData(memberDTO);
                        }

                        groupInfoModel.NumberOfMembers = ChatHelpers.GetGroupMemberCount(groupInfoModel);
                        numberOfMembers = prevCount != groupInfoModel.NumberOfMembers ? (int?)groupInfoModel.NumberOfMembers : null;
                    }

                    GroupInfoDAO.UpdateGroupNameProfileImageAndNumberOfMember(groupId, groupName, groupImage, numberOfMembers);
                    new InsertIntoGroupOrMemberTable(memberList).Start();

                    if (groupMemberInfoDTOs.ContainsKey(DefaultSettings.LOGIN_TABLE_ID))
                    {
                        UCGroupChatSettings ucGroupChatSettings = UIDictionaries.Instance.GROUP_CHAT_SETTINGS_DICTIONARY.TryGetValue(groupId);
                        if (ucGroupChatSettings != null)
                        {
                            Application.Current.Dispatcher.BeginInvoke(() =>
                            {
                                ucGroupChatSettings.CancelGroupNameChange();
                                ucGroupChatSettings.CancelGroupImageChange();
                            });
                        }
                    }
                }

                new InsertIntoRingActivityTable(activityDTOs).Start();
                RecentLoadUtility.LoadRecentData(recentDTOs);
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupInformationActivityFromHistoryMessageReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Group Manipulation

        #region Room Chat

        public override void onPublicRoomChatReceived(string roomId, long senderId, string packetId, int messageType, string message, long messageDate, string memberFullName, string memberProfileUrl)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::CHAT_RECEIVED".PadRight(72, ' ') + "==>   packetId = " + packetId + ", roomId = " + roomId + ", senderId = " + senderId + ", messageType = " + messageType + ", messageDate = " + messageDate + ", message = " + message + ", memberFullName = " + memberFullName + ", memberProfileUrl = " + memberProfileUrl);
#endif

                MessageDTO messageDTO = new MessageDTO();
                messageDTO.RoomID = roomId;
                messageDTO.SenderTableID = senderId;
                messageDTO.PacketID = packetId;
                messageDTO.MessageType = messageType;
                messageDTO.OriginalMessage = message;
                messageDTO.MessageDate = messageDate;
                messageDTO.MessageDelieverDate = ChatService.GetServerTime();
                messageDTO.FullName = memberFullName;
                messageDTO.ProfileImage = memberProfileUrl;
                messageDTO.Status = ChatConstants.STATUS_DELIVERED;
                messageDTO.PacketType = ChatConstants.PACKET_TYPE_ROOM_MESSAGE;
                ChatJSONParser.ParseMessage(messageDTO);

                UCRoomChatCallPanel ucRoomChatCall = UIDictionaries.Instance.ROOM_CHAT_CALL_DICTIONARY.TryGetValue(messageDTO.RoomID);
                RoomModel roomModel = ucRoomChatCall != null ? ucRoomChatCall.RoomModel : RingIDViewModel.Instance.GetRoomModelByRoomID(messageDTO.RoomID);
                ChatHelpers.UpdateRoomHistoryTime(messageDTO.RoomID, messageDTO.PacketID, messageDTO.PacketID, ucRoomChatCall != null ? ucRoomChatCall.RoomModel : null);

                if (messageDTO.IsChatMessage)
                {
                    bool isChatViewHiddenInactive = UIHelperMethods.IsViewActivateAndVisible(ucRoomChatCall) == false;
                    bool isViewAtTop = HelperMethods.IsViewAtTop(ucRoomChatCall);
                    bool isSound = roomModel.ImSoundEnabled && ChatHelpers.IsDontDisturbMood() == false;

                    RecentDTO recentDTO = new RecentDTO();
                    recentDTO.ContactID = roomId;
                    recentDTO.RoomID = roomId;
                    recentDTO.Time = messageDate;
                    recentDTO.IsMoveToButtom = !isViewAtTop;
                    recentDTO.Message = messageDTO;

                    if (isSound)
                    {
                        if (!isChatViewHiddenInactive)
                        {
                            AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_RECEIVED_TUNE);
                        }
                    }

                    if (ucRoomChatCall != null && ucRoomChatCall.IsVisible)
                    {
                        RecentLoadUtility.LoadRecentData(recentDTO);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicRoomChatReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicRoomChatEdited(string roomId, long senderId, string packetId, int messageType, string message, long messageDate, string memberFullName, string memberProfileUrl)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::CHAT_EDITED".PadRight(72, ' ') + "==>   packetId = " + packetId + ", roomId = " + roomId + ", senderId = " + senderId + ", messageType = " + messageType + ", messageDate = " + messageDate + ", message = " + message + ", memberFullName = " + memberFullName + ", memberProfileUrl = " + memberProfileUrl);
#endif

                MessageDTO messageDTO = new MessageDTO();
                messageDTO.RoomID = roomId;
                messageDTO.SenderTableID = senderId;
                messageDTO.PacketID = packetId;
                messageDTO.MessageType = messageType;
                messageDTO.OriginalMessage = message;
                messageDTO.MessageDate = messageDate;
                messageDTO.MessageDelieverDate = ChatService.GetServerTime();
                messageDTO.FullName = memberFullName;
                messageDTO.ProfileImage = memberProfileUrl;
                messageDTO.Status = ChatConstants.STATUS_DELIVERED;
                messageDTO.PacketType = ChatConstants.PACKET_TYPE_ROOM_MESSAGE_EDIT;
                ChatJSONParser.ParseMessage(messageDTO);

                UCRoomChatCallPanel ucRoomChatCall = UIDictionaries.Instance.ROOM_CHAT_CALL_DICTIONARY.TryGetValue(messageDTO.RoomID);
                ChatHelpers.UpdateRoomHistoryTime(messageDTO.RoomID, messageDTO.PacketID, messageDTO.PacketID, ucRoomChatCall != null ? ucRoomChatCall.RoomModel : null);

                if (messageDTO.IsChatMessage)
                {
                    RecentDTO recentDTO = new RecentDTO();
                    recentDTO.ContactID = roomId;
                    recentDTO.RoomID = roomId;
                    recentDTO.Time = messageDate;
                    recentDTO.Message = messageDTO;

                    ObservableCollection<RecentModel> chatModelList = RingIDViewModel.Instance.GetRecentModelListByID(messageDTO.RoomID);
                    RecentModel chatModel = chatModelList.Where(P => P.UniqueKey == messageDTO.PacketID).FirstOrDefault();
                    if (chatModel != null)
                    {
                        chatModel.Message.IsPreviewOpened = false;
                        chatModel.Message.LoadData(messageDTO);

                        if (chatModel.Message.MessageType == MessageType.LINK_MESSAGE)
                        {
                            ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.AddItem(new ChatDownloadProcessor(chatModel.Message, true));
                        }
                    }

                    if (ucRoomChatCall != null && ucRoomChatCall.IsVisible)
                    {
                        if (chatModel == null)
                        {
                            RecentLoadUtility.LoadRecentData(recentDTO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicRoomChatEdited() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicRoomChatDelivered(string roomId, string packetId, bool isEdited)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::CHAT_DELIVERED".PadRight(72, ' ') + "==>   roomId = " + roomId + ", packetId = " + packetId);
#endif

                lock (_ROOM_STATUS_LOCK)
                {
                    RecentModel chatModel = GetRecentModel(0, 0, roomId, packetId);
                    if (chatModel != null)
                    {
                        int prevStatus = chatModel.Message.Status;
                        chatModel.Message.Status = ChatConstants.STATUS_DELIVERED;
                        chatModel.Message.MessageDelieverDate = ChatService.GetServerTime();

                        if (prevStatus == ChatConstants.STATUS_SENDING)
                        {
                            RoomModel roomModel = RingIDViewModel.Instance.GetRoomModelByRoomID(roomId);
                            if (roomModel.ImSoundEnabled)
                            {
                                AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_DELIEVERED_TUNE);
                            }

                            ChatHelpers.UpdateRoomHistoryTime(chatModel.Message.RoomID, chatModel.Message.PacketID, chatModel.Message.PacketID, roomModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicRoomChatDelivered() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicRoomChatFailedToSend(string roomId, string packetId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::CHAT_FAILED_TO_SEND".PadRight(72, ' ') + "==>   roomId = " + roomId + ", packetId = " + packetId);
#endif

                lock (_ROOM_STATUS_LOCK)
                {
                    RecentModel chatModel = GetRecentModel(0, 0, roomId, packetId);
                    if (chatModel != null)
                    {
                        chatModel.Message.Status = ChatConstants.STATUS_FAILED;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicRoomChatFailedToSend() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicRoomChatTyping(string roomId, long senderId, string memberName)
        {
            try
            {
                UCRoomChatCallPanel ucRoomChatCall = UIDictionaries.Instance.ROOM_CHAT_CALL_DICTIONARY.TryGetValue(roomId);
                if (ucRoomChatCall != null && ucRoomChatCall.IsVisible)
                {
                    ucRoomChatCall.AddTyping(senderId, memberName);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicRoomChatTyping() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicChatMessageDeleted(string roomId, List<string> deletedPacketIds)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::CHAT_DELETED".PadRight(72, ' ') + "==>   roomId = " + roomId + ", packetIds = " + deletedPacketIds.ToString("\n"));
#endif
                RecentLoadUtility.RemoveRange(roomId, deletedPacketIds);
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicChatMessageDeleted() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicChatDeleteRequestStatus(string roomId, string packetId, bool status)
        {
#if CHAT_LOG
            log.Debug("HANDLER::ROOM::CHAT_DELETED_CONFIRMED".PadRight(72, ' ') + "==>   roomId = " + roomId + ", packetId = " + packetId + ", status = " + status);
#endif
        }

        public override void onPublicRoomChatHistoryReceived(string roomId, List<BaseMessageDTO> roomHistoryMessageList)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::CHAT_HISTORY_RECEIVED".PadRight(72, ' ') + "==>   roomId = " + roomId + ", roomHistoryMessageList = " + roomHistoryMessageList.ToString("\n"));
#endif

                if (roomHistoryMessageList == null || roomHistoryMessageList.Count <= 0)
                    return;

                string minPacketID = minPacketID = roomHistoryMessageList.Last().PacketID;
                string maxPacketID = maxPacketID = roomHistoryMessageList.First().PacketID;
                ChatHelpers.UpdateRoomHistoryTime(roomId, minPacketID, maxPacketID, null);

#if CHAT_LOG
                log.Debug("HANDLER::ROOM::MIN_MAX_PACKETID".PadRight(72, ' ') + "==>   maxPacketID = " + maxPacketID + ", maxPacketTime = " + PacketIDGenerator.GetUnixTimestamp(maxPacketID) + ", minPacketID = " + minPacketID + ", minPacketTime = " + PacketIDGenerator.GetUnixTimestamp(minPacketID));
#endif

                List<RecentDTO> recentDTOs = new List<RecentDTO>();

                foreach (BaseMessageDTO entity in roomHistoryMessageList)
                {
                    if (entity.MessageDate <= 0) continue;

                    MessageDTO msgDTO = new MessageDTO();
                    msgDTO.RoomID = roomId;
                    msgDTO.SenderTableID = entity.SenderID;
                    msgDTO.PacketID = entity.PacketID;
                    msgDTO.MessageType = entity.IReport ? (int)MessageType.DELETE_MESSAGE : entity.MessageType;
                    msgDTO.PacketType = entity.IsEdited ? ChatConstants.PACKET_TYPE_ROOM_MESSAGE_EDIT : ChatConstants.PACKET_TYPE_ROOM_MESSAGE;
                    msgDTO.OriginalMessage = entity.Message;
                    msgDTO.MessageDate = entity.MessageDate;
                    msgDTO.MessageDelieverDate = entity.SenderID == DefaultSettings.LOGIN_TABLE_ID ? msgDTO.MessageDate : ChatService.GetServerTime();
                    msgDTO.FullName = entity.MemberFullName;
                    msgDTO.ProfileImage = entity.MemberProfileUrl;
                    msgDTO.LikeCount = entity.LikeCount;
                    msgDTO.ILike = entity.ILike;
                    msgDTO.IReport = entity.IReport;
                    msgDTO.Status = ChatConstants.STATUS_SEEN;
                    ChatJSONParser.ParseMessage(msgDTO);

                    if (msgDTO.IsChatMessage)
                    {
                        recentDTOs.Add(new RecentDTO { ContactID = msgDTO.RoomID, RoomID = msgDTO.RoomID, Message = msgDTO, Time = msgDTO.MessageDate });
                    }
                }

                RecentLoadUtility.LoadRecentData(recentDTOs);
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicRoomChatHistoryReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicRoomChatHistoryCompleted(string roomId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::CHAT_HISTORY_COMPLETED".PadRight(72, ' ') + "==>   roomId = " + roomId);
                RoomModel model = RingIDViewModel.Instance.RoomList.TryGetValue(roomId);
                if (model != null)
                {
                    model.ChatHistoryNotFound = true;
                }
#endif
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicRoomChatHistoryCompleted() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicRoomChatHistoryRequestStatus(string roomId, string packetId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::CHAT_HISTORY_REQUEST_SENT".PadRight(72, ' ') + "==>   roomId = " + roomId + ", packetId = " + packetId + ", status = " + status);
#endif

                InvokeDelegate(new ChatEventArgs { PacketID = packetId, RoomID = roomId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicRoomChatHistoryRequestStatus() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicChatMessageLiked(string roomId, string packetId, long messageSenderId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::CHAT_LIKE".PadRight(72, ' ') + "==>   roomId = " + roomId + ", packetId = " + packetId + ", messageSenderId = " + messageSenderId);
#endif
                ObservableCollection<RecentModel> chatModelList = RingIDViewModel.Instance.GetRecentModelListByID(roomId);
                RecentModel chatModel = chatModelList.Where(P => P.UniqueKey == packetId).FirstOrDefault();
                if (chatModel != null)
                {
                    chatModel.Message.LikeCount++;
                    if (messageSenderId == DefaultSettings.LOGIN_TABLE_ID)
                    {
                        chatModel.Message.ILike = true;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicChatMessageLiked() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicChatMessageUnliked(string roomId, string packetId, long messageSenderId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::CHAT_UNLIKE".PadRight(72, ' ') + "==>   roomId = " + roomId + ", packetId = " + packetId + ", messageSenderId = " + messageSenderId);
#endif
                ObservableCollection<RecentModel> chatModelList = RingIDViewModel.Instance.GetRecentModelListByID(roomId);
                RecentModel chatModel = chatModelList.Where(P => P.UniqueKey == packetId).FirstOrDefault();
                if (chatModel != null)
                {
                    chatModel.Message.LikeCount = chatModel.Message.LikeCount > 0 ? chatModel.Message.LikeCount - 1 : 0;
                    if (messageSenderId == DefaultSettings.LOGIN_TABLE_ID)
                    {
                        chatModel.Message.ILike = false;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicChatMessageUnliked() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicChatLikeUnlikeRequestStatus(string roomId, string requestPacketId, string messagePacketId, bool status, bool fromLikeRequest)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::CHAT_LIKE_UNLIKE_CONFIRMATION".PadRight(72, ' ') + "==>   roomId = " + roomId + ", requestPacketId = " + requestPacketId + ", messagePacketId = " + messagePacketId + ", status = " + status + ", fromLikeRequest = " + fromLikeRequest);
#endif
                InvokeDelegate(new ChatEventArgs { PacketID = requestPacketId, RoomID = roomId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicChatLikeUnlikeRequestStatus() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicChatLikeMemberListReceived(string roomId, string packetId, List<BasePublicChatMemberDTO> publicChatMemberList)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::LIKE_MEMBER_LIST".PadRight(72, ' ') + "==>   roomId = " + roomId + ", packetId = " + packetId + ", publicChatMemberList = " + publicChatMemberList.ToString("\n"));
#endif
                RecentModel chatModel = GetRecentModel(0, 0, roomId, packetId);
                if (chatModel.Message == null) return;

                if (publicChatMemberList == null || publicChatMemberList.Count == 0)
                {
                    chatModel.Message.ViewModel.LikeMemberNotFound = true;
                    return;
                }

                foreach (BasePublicChatMemberDTO memberDTO in publicChatMemberList)
                {
                    RoomMemberModel memberModel = chatModel.Message.LikeMemberList.FirstOrDefault(P => P.MemberID == memberDTO.MemberID);
                    if (memberModel == null)
                    {
                        memberModel = new RoomMemberModel();
                        memberModel.MemberID = memberDTO.MemberID;
                        memberModel.FullName = memberDTO.FullName;
                        memberModel.ProfileImageUrl = memberDTO.ProfileUrl;
                        chatModel.Message.LikeMemberList.InvokeAdd(memberModel);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicChatLikeMemberListReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicChatLikeMemberListRequestStatus(string roomId, string packetId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::LIKE_MEMBER_LIST_CONFIRMATION".PadRight(72, ' ') + "==>   roomId = " + roomId + ", packetId = " + packetId + ", status = " + status);
#endif
                InvokeDelegate(new ChatEventArgs { PacketID = packetId, RoomID = roomId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicChatLikeMemberListRequestStatus() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicChatMessageReported(string roomId, string packetId, long recipientId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::CHAT_REPORT".PadRight(72, ' ') + "==>   roomId = " + roomId + ", packetId = " + packetId + ", recipientId = " + recipientId);
#endif

            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicChatMessageReported() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicChatReportRequestStatus(string roomId, string requestPacketId, string messagePacketId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::CHAT_REPORT_CONFIRMATION".PadRight(72, ' ') + "==>   roomId = " + roomId + ", requestPacketId = " + requestPacketId + ", messagePacketId = " + messagePacketId + ", status = " + status);
#endif
                InvokeDelegate(new ChatEventArgs { PacketID = requestPacketId, RoomID = roomId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicChatReportRequestStatus() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Room Chat

        #region Room Manipulation

        public override void onPublicRoomInformationReceived(string roomId, string roomName, string roomUrl, string packetId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::ROOM_INFORMATION_RECEIVED".PadRight(72, ' ') + "==>   roomId = " + roomId + ", packetId = " + packetId);
#endif

                RoomModel roomModel = RingIDViewModel.Instance.RoomList.TryGetValue(roomId);
                if (roomModel == null)
                {
                    roomModel = new RoomModel();
                    roomModel.RoomID = roomId;
                    roomModel.RoomName = roomName;
                    roomModel.RoomProfileImage = roomUrl;
                    roomModel.ImSoundEnabled = SettingsConstants.VALUE_RINGID_IM_SOUND;
                    roomModel.ImNotificationEnabled = SettingsConstants.VALUE_RINGID_IM_NOTIFICATION;
                    roomModel.ImPopupEnabled = SettingsConstants.VALUE_RINGID_IM_POPUP;
                    RingIDViewModel.Instance.RoomList[roomId] = roomModel;
                }
                else
                {
                    roomModel.RoomName = roomName;
                    roomModel.RoomProfileImage = roomUrl;
                    roomModel.IsPartial = false;
                }

                List<RoomDTO> roomList = new List<RoomDTO>();
                RoomDTO roomDTO = new RoomDTO();
                roomDTO.RoomID = roomModel.RoomID;
                roomDTO.RoomName = roomModel.RoomName;
                roomDTO.RoomProfileImage = roomModel.RoomProfileImage;
                roomDTO.NumberOfMembers = roomModel.NumberOfMembers;
                roomDTO.IsPartial = roomModel.IsPartial;
                roomDTO.ChatBgUrl = roomModel.ChatBgUrl;
                roomDTO.ChatMinPacketID = roomModel.ChatMinPacketID;
                roomDTO.ChatMaxPacketID = roomModel.ChatMaxPacketID;
                roomDTO.ImSoundEnabled = roomModel.ImSoundEnabled;
                roomDTO.ImNotificationEnabled = roomModel.ImNotificationEnabled;
                roomDTO.ImPopupEnabled = roomModel.ImPopupEnabled;
                roomList.Add(roomDTO);
                new InsertIntoRoomListTable(roomList).Start();
                roomModel.FromDB = true;

                InvokeDelegate(new ChatEventArgs { PacketID = packetId, RoomID = roomId, Status = true });
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicRoomInformationReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
                InvokeDelegate(new ChatEventArgs { PacketID = packetId, RoomID = roomId, Status = true });
            }
        }

        public override void onPublicRoomInformationRequestFailed(string roomId, string packetId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::ROOM_INFORMATION_REQUEST_FAILED".PadRight(72, ' ') + "==>   roomId = " + roomId + ", packetId = " + packetId);
#endif

                InvokeDelegate(new ChatEventArgs { PacketID = packetId, RoomID = roomId, Status = false });
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicRoomInformationRequestFailed() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicRoomListReceived(List<BaseRoomDTO> roomList, bool roomListFromHistory)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::ROOM_LIST_RECEIVED".PadRight(72, ' ') + "==>   roomListFromHistory = " + roomListFromHistory + ", roomList = " + roomList.ToString("\n"));
#endif
                bool fromSearch = false;
                if (UCMiddlePanelSwitcher.View_UCRoomListPanel != null && UCMiddlePanelSwitcher.View_UCRoomListPanel._RoomListAllPanel != null && UCMiddlePanelSwitcher.View_UCRoomListPanel._RoomListAllPanel.Parent != null)
                {
                    if (roomList == null || roomList.Count <= 0)
                    {
                        RingIDViewModel.Instance.RoomListFetchQuery.NoMoreRoom = true;
                    }
                    RingIDViewModel.Instance.RoomListFetchQuery.StartIndex += roomList.Count;


                    if (roomList == null || roomList.Count <= 0)
                    {
                        return;
                    }
                }
                else if (UCMiddlePanelSwitcher.View_UCRoomListPanel != null && UCMiddlePanelSwitcher.View_UCRoomListPanel._RoomListSearchPanel != null && UCMiddlePanelSwitcher.View_UCRoomListPanel._RoomListSearchPanel.Parent != null)
                {
                    if (!RingIDViewModel.Instance.RoomListSearchQuery.IsActive)
                    {
                        return;
                    }

                    if (roomList == null || roomList.Count <= 0)
                    {
                        RingIDViewModel.Instance.RoomListSearchQuery.NoMoreRoom = true;
                    }
                    RingIDViewModel.Instance.RoomListSearchQuery.StartIndex += roomList.Count;

                    if (roomList == null || roomList.Count <= 0)
                    {
                        return;
                    }

                    fromSearch = true;
                }

                List<RoomDTO> roomDTOs = new List<RoomDTO>();

                foreach (BaseRoomDTO baseRoomDTO in roomList)
                {
                    bool insertIntoDB = false;
                    RoomModel roomModel = RingIDViewModel.Instance.RoomList.TryGetValue(baseRoomDTO.RoomID);

                    if (roomModel == null)
                    {
                        roomModel = new RoomModel(baseRoomDTO);
                        roomModel.ImSoundEnabled = SettingsConstants.VALUE_RINGID_IM_SOUND;
                        roomModel.ImNotificationEnabled = SettingsConstants.VALUE_RINGID_IM_NOTIFICATION;
                        roomModel.ImPopupEnabled = SettingsConstants.VALUE_RINGID_IM_POPUP;

                        if (fromSearch)
                        {
                            if (!RingIDViewModel.Instance.RoomSearchList.ContainsKey(baseRoomDTO.RoomID))
                            {
                                Application.Current.Dispatcher.BeginInvoke(() => { RingIDViewModel.Instance.RoomSearchList[baseRoomDTO.RoomID] = roomModel; }, DispatcherPriority.Send);
                            }
                        }
                        else
                        {
                            Application.Current.Dispatcher.BeginInvoke(() => { RingIDViewModel.Instance.RoomList[baseRoomDTO.RoomID] = roomModel; }, DispatcherPriority.Send);
                        }
                    }
                    else
                    {
                        if (fromSearch)
                        {
                            if (!RingIDViewModel.Instance.RoomSearchList.ContainsKey(baseRoomDTO.RoomID))
                            {
                                Application.Current.Dispatcher.BeginInvoke(() => { RingIDViewModel.Instance.RoomSearchList[baseRoomDTO.RoomID] = roomModel; }, DispatcherPriority.Send);
                            }
                        }

                        baseRoomDTO.NumberOfMember = fromSearch ? roomModel.NumberOfMembers : baseRoomDTO.NumberOfMember;
                        insertIntoDB = roomModel.FromDB && (roomModel.RoomName != baseRoomDTO.RoomName || roomModel.RoomProfileImage != baseRoomDTO.RoomImageUrl || roomModel.NumberOfMembers != baseRoomDTO.NumberOfMember);
                        roomModel.RoomName = baseRoomDTO.RoomName;
                        roomModel.RoomProfileImage = baseRoomDTO.RoomImageUrl;
                        roomModel.NumberOfMembers = baseRoomDTO.NumberOfMember;
                        roomModel.IsPartial = false;

                        if (insertIntoDB)
                        {
                            RoomDTO roomDTO = new RoomDTO();
                            roomDTO.RoomID = roomModel.RoomID;
                            roomDTO.RoomName = roomModel.RoomName;
                            roomDTO.RoomProfileImage = roomModel.RoomProfileImage;
                            roomDTO.NumberOfMembers = roomModel.NumberOfMembers;
                            roomDTO.IsPartial = roomModel.IsPartial;
                            roomDTO.ChatBgUrl = roomModel.ChatBgUrl;
                            roomDTO.ChatMinPacketID = roomModel.ChatMinPacketID;
                            roomDTO.ChatMaxPacketID = roomModel.ChatMaxPacketID;
                            roomDTO.ImSoundEnabled = roomModel.ImSoundEnabled;
                            roomDTO.ImNotificationEnabled = roomModel.ImNotificationEnabled;
                            roomDTO.ImPopupEnabled = roomModel.ImPopupEnabled;
                            roomDTOs.Add(roomDTO);
                        }
                    }

                    if (baseRoomDTO.RoomMessageList == null || baseRoomDTO.RoomMessageList.Count <= 0)
                        continue;

                    for (int i = 0; i < baseRoomDTO.RoomMessageList.Count; i++)
                    {
                        BaseMessageDTO msgDTO = baseRoomDTO.RoomMessageList[i];
                        MessageDTO messageDTO = new MessageDTO();
                        messageDTO.RoomID = baseRoomDTO.RoomID;
                        messageDTO.SenderTableID = msgDTO.SenderID;
                        messageDTO.Message = (msgDTO.MessageType == (int)MessageType.PLAIN_MESSAGE || msgDTO.MessageType == (int)MessageType.EMOTICON_MESSAGE) ? msgDTO.Message : "Message";
                        messageDTO.MessageDate = msgDTO.MessageDate;
                        messageDTO.MessageType = msgDTO.MessageType;
                        messageDTO.Status = ChatConstants.STATUS_DELIVERED;
                        messageDTO.PacketID = msgDTO.PacketID;
                        messageDTO.FullName = msgDTO.MemberFullName;
                        messageDTO.ProfileImage = msgDTO.MemberProfileUrl;
                        messageDTO.PacketType = msgDTO.IsEdited ? ChatConstants.PACKET_TYPE_ROOM_MESSAGE_EDIT : ChatConstants.PACKET_TYPE_ROOM_MESSAGE;
                        if (roomModel.MessageList.Count > i)
                        {
                            roomModel.MessageList[i].LoadData(messageDTO);
                            roomModel.MessageList[i].Index = i;
                        }
                        else
                        {
                            MessageModel model = new MessageModel(messageDTO);
                            model.Index = i;
                            roomModel.MessageList.InvokeAdd(model);
                        }
                    }
                }

                new InsertIntoRoomListTable(roomDTOs).Start();
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicRoomListReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicRoomListRequestStatus(string packetId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::ROOM_LIST_REQUEST_STATUS".PadRight(72, ' ') + "==>   packetId = " + packetId + ", status = " + status);
#endif

                InvokeDelegate(new ChatEventArgs { PacketID = packetId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicRoomListRequestStatus() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicChatMemberListReceived(string roomId, string pagingState, List<BasePublicChatMemberDTO> publicChatMemberList)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::ROOM_MEMBER_LIST_RECEIVED".PadRight(72, ' ') + "==>   roomId = " + roomId + ", pagingState = " + pagingState + ", publicChatMemberList = " + publicChatMemberList.ToString("\n"));
#endif

                RoomModel roomModel = RingIDViewModel.Instance.RoomList.TryGetValue(roomId);
                if (roomModel != null)
                {
                    roomModel.PagingState = pagingState;
                    roomModel.ChatMemberNotFound = publicChatMemberList == null || publicChatMemberList.Count <= 0 || String.IsNullOrWhiteSpace(pagingState);
                }
                else
                {
                    return;
                }

                if (publicChatMemberList == null || publicChatMemberList.Count <= 0)
                    return;

                foreach (BasePublicChatMemberDTO memberDTO in publicChatMemberList)
                {
                    RoomMemberModel memberModel = roomModel.MemberInfoList.Where(P => P.MemberID == memberDTO.MemberID).FirstOrDefault();
                    if (memberModel == null)
                    {
                        memberModel = new RoomMemberModel(memberDTO);
                        roomModel.MemberInfoList.InvokeAdd(memberModel);
                    }
                    else
                    {
                        memberModel.FakeID = memberDTO.FakeID;
                        memberModel.MemberID = memberDTO.MemberID;
                        memberModel.RingID = memberDTO.RingID;
                        memberModel.FullName = memberDTO.FullName;
                        memberModel.ProfileImageUrl = memberDTO.ProfileUrl;
                        memberModel.AddedTime = memberDTO.AddedTime;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicChatMemberListReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicRoomMemberListRequestStatus(string roomId, string packetId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::ROOM_MEMBER_LIST_REQUEST_STATUS".PadRight(72, ' ') + "==>   roomId = " + roomId + ", packetId = " + packetId + ", status = " + status);
#endif

                InvokeDelegate(new ChatEventArgs { PacketID = packetId, RoomID = roomId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicRoomMemberListRequestStatus() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicChatMemberCountChanged(string roomId, int numberOfMembers)
        {
#if CHAT_LOG
            log.Debug("HANDLER::ROOM::ROOM_MEMBER_COUNT".PadRight(72, ' ') + "==>   roomId = " + roomId + ", numberOfMembers = " + numberOfMembers);
#endif

            RoomModel roomModel = RingIDViewModel.Instance.RoomList.TryGetValue(roomId);
            if (roomModel != null)
            {
                roomModel.NumberOfMembers = numberOfMembers;
                if (roomModel.FromDB)
                {
                    RoomDAO.UpdateNumberOfMembers(roomId, numberOfMembers);
                }
            }
        }

        public override void onPublicChatCategoryListReceived(List<string> publicChatCategoryList)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::CATEGORY_LIST".PadRight(72, ' ') + "==>   publicChatCategoryList = " + publicChatCategoryList.ToString("\n"));
#endif
                if (publicChatCategoryList == null || publicChatCategoryList.Count == 0)
                    return;

                foreach (string categoryName in publicChatCategoryList)
                {
                    if (!RingIDViewModel.Instance.RoomCategoryList.Any(P => P.CategoryName == categoryName))
                    {
                        RoomCategoryModel model = new RoomCategoryModel();
                        model.CategoryName = categoryName;
                        RingIDViewModel.Instance.RoomCategoryList.InvokeAdd(model);
                    }
                }

                RoomDAO.SaveRoomCategory(publicChatCategoryList);
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicChatCategoryListReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onPublicChatCategoryListRequestStatus(string packetId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::ROOM::CATEGORY_LIST_CONFIRMATION".PadRight(72, ' ') + "==>   packetId = " + packetId + ", status = " + status);
#endif
                InvokeDelegate(new ChatEventArgs { PacketID = packetId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onPublicChatCategoryListRequestStatus() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Room Manipulation

        #region Stream Chat

        public override void onLiveStreamChatReceived(long publisherId, long senderId, string packetId, int messageType, string message, long messageDate, string senderFullName)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CHAT::CHAT_RECEIVED".PadRight(72, ' ') + "==>   publisherId = " + publisherId + ", senderId = " + senderId + ", packetId = " + packetId + ", messageType = " + messageType + ", messageDate = " + messageDate + ", message = " + message + ", fullName = " + senderFullName);
#endif
                MessageDTO messageDTO = new MessageDTO();
                messageDTO.PublisherID = publisherId;
                messageDTO.SenderTableID = senderId;
                messageDTO.PacketID = packetId;
                messageDTO.StreamMsgDTO = new StreamMessageDTO { MessageType = StreamConstants.PLAIN_MESSAGE };
                messageDTO.Message = message;
                messageDTO.MessageDate = messageDate;
                messageDTO.FullName = senderFullName;
                messageDTO.Status = ChatConstants.STATUS_DELIVERED;
                StreamMsgLoadUtility.LoadMessageData(messageDTO);
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveStreamChatReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onLiveStreamChatFailedToSend(long publisherId, string packetId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CHAT::CHAT_FAILED_TO_SEND".PadRight(72, ' ') + "==>   publisherId = " + publisherId + ", packetId = " + packetId);
#endif
                MessageModel messageModel = GetStreamMessageModel(publisherId, packetId);
                if (messageModel != null)
                {
                    messageModel.Status = ChatConstants.STATUS_FAILED;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveStreamChatFailedToSend() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onLiveStreamChatDelivered(long publisherId, string packetId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::STREAM::CHAT::CHAT_DELIVERED".PadRight(72, ' ') + "==>   publisherId = " + publisherId + ", packetId = " + packetId);
#endif
                MessageModel messageModel = GetStreamMessageModel(publisherId, packetId);
                if (messageModel != null)
                {
                    messageModel.Status = ChatConstants.STATUS_DELIVERED;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveStreamChatDelivered() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onLiveStreamChatTyping(long publisherId, long senderId)
        {
            try
            {

            }
            catch (Exception ex)
            {
                log.Error("Error at onLiveStreamChatTyping() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Stream Chat

        #region Friend File Transfer

        public override void onFileStreamRequestReceived(long friendId, long fileId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::FILE_STREAM_REQUEST_RECEIVED".PadRight(72, ' ') + "==>   friendId = " + friendId + ", fileId = " + fileId);
#endif

                if (FileTransferSession.IsFileTransferRunning(friendId, fileId))
                {
#if CHAT_LOG
                    log.Debug("FILE TRANSFER => onFileStreamRequestReceived() => File Upload Already Running. FriendID = " + friendId + ", FileID = " + fileId + "");
#endif
                    return;
                }

                MessageDTO messageDTO = RecentChatCallActivityDAO.GetMessageDTOByPacketID(friendId, null, null, fileId);
                if (messageDTO != null)
                {
                    if (messageDTO.FileStatus == ChatConstants.FILE_TRANSFER_CANCELLED || messageDTO.FileStatus == ChatConstants.FILE_MOVED)
                    {
#if CHAT_LOG
                        log.Debug("FILE TRANSFER => onFileStreamRequestReceived() => File Already Cancelled Or Moved. FriendID = " + friendId + ", FileID = " + messageDTO.FileID + "");
#endif
                        ChatHelpers.SendFileStreamCancel(friendId, 0, fileId, messageDTO);
                        return;
                    }
                    else if (messageDTO.Status == ChatConstants.TYPE_DELETE_MSG)
                    {
#if CHAT_LOG
                        log.Debug("FILE TRANSFER => onFileStreamRequestReceived() => Message Deleted. FriendID = " + friendId + ", FileID = " + messageDTO.FileID + "");
#endif
                        ChatHelpers.SendFileStreamCancel(friendId, 0, fileId, messageDTO);
                        return;
                    }
                    else if (!String.IsNullOrWhiteSpace(messageDTO.FileMenifest))
                    {
                        return;
                    }

                    ObservableCollection<RecentModel> chatModelList = RingIDViewModel.Instance.GetRecentModelListByID(friendId);
                    RecentModel chatModel = chatModelList.Where(P => P.UniqueKey == messageDTO.PacketID).FirstOrDefault();
                    MessageModel model = chatModel != null ? chatModel.Message : null;

                    string filePath = ChatHelpers.GetChatFilePath(messageDTO.Message, (MessageType)messageDTO.MessageType, messageDTO.PacketID, messageDTO.LinkImageUrl, messageDTO.SenderTableID);
                    FileInfo fi = new FileInfo(filePath);
                    if (fi.Exists)
                    {
                        if (model != null)
                        {
                            model.FileStatus = ChatConstants.FILE_DEFAULT;
                            model.ViewModel.IsUploadRunning = true;
                            model.ViewModel.UploadPercentage = 0;
                            model.ViewModel.FileProgressInSize = 0;
                        }

                        RecentChatCallActivityDAO.UpdateChatMessageFileStatusInSameProcess(friendId, 0, messageDTO.PacketID, ChatConstants.FILE_DEFAULT);

                        ChatFileDTO fileDTO = new ChatFileDTO();
                        fileDTO.PacketID = messageDTO.PacketID;
                        fileDTO.ContactID = messageDTO.FriendTableID;
                        fileDTO.SenderTableID = messageDTO.SenderTableID;
                        fileDTO.FriendTableID = messageDTO.FriendTableID;
                        fileDTO.FileID = messageDTO.FileID;
                        fileDTO.File = filePath;
                        fileDTO.FileSize = messageDTO.FileSize;
                        fileDTO.RecentModel = model;
                        fileDTO.IsDownstream = false;

#if CHAT_LOG
                        log.Debug("FILE TRANSFER => ChatFileTransferProcessorQueue->AddItem. FriendID = " + friendId + ", FileID = " + messageDTO.FileID + ", IsDownstream = " + false);
#endif
                        ImageDictionaries.Instance.FILE_UPLOAD_PROCESSOR_QUEUE.AddItem(fileDTO);
                    }
                    else
                    {
                        if (model != null)
                        {
                            model.FileStatus = ChatConstants.FILE_MOVED;
                            model.ViewModel.IsUploadRunning = false;
                            model.ViewModel.UploadPercentage = 0;
                        }
#if CHAT_LOG
                        log.Debug("FILE TRANSFER => onFileStreamRequestReceived() => File Already Moved. FriendID = " + friendId + ", FileID = " + messageDTO.FileID + "");
#endif

                        RecentChatCallActivityDAO.UpdateChatMessageFileStatusInSameProcess(friendId, 0, messageDTO.PacketID, ChatConstants.FILE_MOVED);
                        ChatHelpers.SendFileStreamCancel(friendId, 0, fileId, messageDTO);
                    }
                }
                else
                {
#if CHAT_LOG
                    log.Debug("FILE TRANSFER => onFileStreamRequestReceived() => Message Not Found. FriendID = " + friendId + ", FileID = " + fileId + "");
#endif
                    ChatHelpers.SendFileStreamCancel(friendId, 0, fileId, null);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onFileStreamRequestReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendFileSessionRequestSuccess(string packetId, long friendId, long fileId, int fileTransferPort)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::FILE_SESSION_REQUEST_SENT".PadRight(72, ' ') + "==>   friendId = " + friendId + ", fileId = " + fileId + ", fileTransferPort = " + fileTransferPort + ", packetId = " + packetId);
#endif

                BaseFriendInformation info = ChatService.GetFriendRegisterInfo(friendId);
                string serverIP = info != null ? info.RegisterServerAddress : null;

                InvokeDelegate(new ChatEventArgs { PacketID = packetId, FriendTableID = friendId, ServerIP = serverIP, ServerPort = fileTransferPort, Status = true });
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendFileSessionRequestSuccess() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendFileTransferSessionRequestFailed(string packetId, long friendId, long fileId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::FILE_SESSION_REQUEST_FAILED".PadRight(72, ' ') + "==>   friendId = " + friendId + ", fileId = " + fileId + ", packetId = " + packetId);
#endif
                InvokeDelegate(new ChatEventArgs { PacketID = packetId, FriendTableID = friendId, Status = false });
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendFileTransferSessionRequestFailed() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendFileStreamRequestSuccess(string packetId, long friendId, long fileId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::FILE_STREAM_REQUEST_SENT".PadRight(72, ' ') + "==>   friendId = " + friendId + ", fileId = " + fileId + ", packetId = " + packetId);
#endif

                InvokeDelegate(new ChatEventArgs { PacketID = packetId, FriendTableID = friendId, Status = true });
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendFileStreamRequestSuccess() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendFileTransferStreamRequestFailed(string packetId, long friendId, long fileId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::FILE_STREAM_REQUEST_FAILED".PadRight(72, ' ') + "==>   friendId = " + friendId + ", fileId = " + fileId + ", packetId = " + packetId);
#endif
                InvokeDelegate(new ChatEventArgs { PacketID = packetId, FriendTableID = friendId, Status = false });
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendFileTransferStreamRequestFailed() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendIncomingChatMedia(long friendId, string fileName, string packetId, int messageType, int timeout, long messageDate, bool isSecretVisible, string caption, int widthOrFileSize, int heightOrDuration)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::INCOMING_CHAT_MEDIA".PadRight(72, ' ') + "==>   friendId = " + friendId + ", fileName = " + fileName + ", packetId = " + packetId + ", messageType = " + messageType + ", messageDate = " + messageDate + ", timeout = " + timeout + ", isSecretVisible = " + isSecretVisible);
#endif
                MessageDTO messageDTO = new MessageDTO();
                messageDTO.FriendTableID = friendId;
                messageDTO.SenderTableID = friendId;
                messageDTO.PacketID = packetId;
                messageDTO.MessageType = messageType;
                messageDTO.Timeout = timeout;
                switch ((MessageType)messageDTO.MessageType)
                {
                    case MessageType.IMAGE_FILE_FROM_GALLERY:
                    case MessageType.IMAGE_FILE_FROM_CAMERA:
                        messageDTO.OriginalMessage = ChatJSONParser.MakeImageMessage(String.Empty, caption, widthOrFileSize, heightOrDuration);
                        break;
                    case MessageType.AUDIO_FILE:
                    case MessageType.VIDEO_FILE:
                        messageDTO.OriginalMessage = ChatJSONParser.MakeMultimediaMessage(String.Empty, widthOrFileSize, heightOrDuration);
                        break;
                }
                messageDTO.MessageDate = messageDate;
                messageDTO.IsSecretVisible = isSecretVisible ? 1 : 0;
                messageDTO.Status = ChatConstants.STATUS_PENDING;
                messageDTO.PacketType = ChatConstants.PACKET_TYPE_FRIEND_MESSAGE;
                ChatJSONParser.ParseMessage(messageDTO);

                UCFriendChatCallPanel ucFriendChatCall = UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.TryGetValue(messageDTO.FriendTableID);
                UserBasicInfoModel friendInfoModel = ucFriendChatCall != null ? ucFriendChatCall.FriendBasicInfoModel : RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(messageDTO.FriendTableID);
                ChatHelpers.UpdateFriendHistoryTime(messageDTO.FriendTableID, messageDTO.PacketID, messageDTO.PacketID, friendInfoModel);

                if (friendInfoModel.ShortInfoModel.FriendShipStatus != StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                {
                    BlockedNonFriendModel blockModel = ucFriendChatCall != null ? ucFriendChatCall.BlockedNonFriendModel : RingIDViewModel.Instance.GetBlockedNonFriendModelByID(messageDTO.FriendTableID);
                    if (blockModel.IsBlockedByMe)
                    {
                        ChatService.BlockUnblockFriend(blockModel.IsBlockedByMe, messageDTO.FriendTableID, null);
                        return;
                    }
                }

                bool isDeleted = messageDTO.MessageType == (int)MessageType.DELETE_MESSAGE;
                bool isNotifiable = !(isDeleted || ChatHelpers.IsDontDisturbMood());
                bool isChatViewInactive = UIHelperMethods.IsViewActivate(ucFriendChatCall) == false;
                bool isChatViewHiddenInactive = isChatViewInactive || HelperMethods.IsViewVisible(ucFriendChatCall) == false;
                bool isViewAtTop = HelperMethods.IsViewAtTop(ucFriendChatCall);

                bool isSound = friendInfoModel.ShortInfoModel.ImSoundEnabled && isNotifiable;
                bool isPopup = friendInfoModel.ShortInfoModel.ImPopupEnabled && isNotifiable && isChatViewInactive;
                bool isUnread = friendInfoModel.ShortInfoModel.ImNotificationEnabled && isNotifiable && (isChatViewHiddenInactive || isViewAtTop);

                messageDTO.IsUnread = isUnread;
                RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                RecentDTO recentDTO = new RecentDTO();
                recentDTO.ContactID = messageDTO.FriendTableID.ToString();
                recentDTO.FriendTableID = messageDTO.FriendTableID;
                recentDTO.Time = messageDTO.MessageDate;
                recentDTO.IsMoveToButtom = !isViewAtTop;
                recentDTO.Message = messageDTO;

                if (isSound)
                {
                    if (isChatViewHiddenInactive)
                    {
                        AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_RECEIVED_ALERT_TUNE);
                    }
                    else
                    {
                        AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_RECEIVED_TUNE);
                    }
                }

                if (isPopup)
                {
                    WNChatPopupMessage.ShowNotification(recentDTO);
                }

                if (isUnread)
                {
                    ChatHelpers.AddUnreadChat(recentDTO.ContactID, recentDTO.Message.PacketID, recentDTO.Message.MessageDate);
                }

                if (ucFriendChatCall != null && (ucFriendChatCall.IsVisible || isDeleted))
                {
                    RecentLoadUtility.LoadRecentData(recentDTO);
                }

                ChatLogLoadUtility.LoadRecentData(recentDTO);
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendIncomingChatMedia() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendChatMediaTransferProgress(long friendId, string fileName, double percentage, bool isUpload, string packetId)
        {
            try
            {
                //#if CHAT_LOG
                //                log.Debug("HANDLER::FRIEND::CHAT_MEDIA_TRANSFER_PROGRESS".PadRight(72, ' ') + "==>   friendId = " + friendId + ", fileName = " + fileName + ", percentage = " + percentage + ", isUpload = " + isUpload + ", packetId = " + packetId);
                //#endif
                RecentModel chatModel = GetRecentModel(friendId, 0, null, packetId);
                if (chatModel != null)
                {
                    if (isUpload)
                    {
                        chatModel.Message.ViewModel.UploadPercentage = (int)percentage;
                    }
                    else
                    {
                        chatModel.Message.ViewModel.DownloadPercentage = (int)percentage;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendChatMediaTransferProgress() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendFileManifestUploaded(long friendId, string packetId, string cloudManifestUrl, int messageType, string caption, int widthOrFileSize, int heightOrDuration)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::CHAT_MEDIA_MENIFEST_UPLOADED".PadRight(72, ' ') + "==>   friendId = " + friendId + ", packetId = " + packetId + ", cloudManifestUrl = " + cloudManifestUrl + ", messageType = " + messageType + ", caption = " + caption + ", widthOrFileSize = " + widthOrFileSize + ", heightOrDuration = " + heightOrDuration);
#endif
                if (messageType == (int)MessageType.AUDIO_FILE)
                {
                    string filePath = RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER + Path.DirectorySeparatorChar + packetId + ".g729";
                    if (File.Exists(filePath))
                    {
                        try { File.Delete(filePath); }
                        catch { }
                    }
                }

                MessageDTO messageDTO = RecentChatCallActivityDAO.GetMessageDTOByPacketID(friendId, null, packetId);
                if (messageDTO != null)
                {
                    switch ((MessageType)messageDTO.MessageType)
                    {
                        case MessageType.IMAGE_FILE_FROM_GALLERY:
                        case MessageType.IMAGE_FILE_FROM_CAMERA:
                            messageDTO.OriginalMessage = ChatJSONParser.MakeImageMessage(cloudManifestUrl, caption, widthOrFileSize, heightOrDuration);
                            break;
                        case MessageType.AUDIO_FILE:
                        case MessageType.VIDEO_FILE:
                            messageDTO.OriginalMessage = ChatJSONParser.MakeMultimediaMessage(cloudManifestUrl, widthOrFileSize, heightOrDuration);
                            break;
                    }

                    ChatJSONParser.ParseMessage(messageDTO);
                    RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                    RecentModel chatModel = GetRecentModel(friendId, 0, null, messageDTO.PacketID);
                    if (chatModel != null)
                    {
                        chatModel.Message.IsFileOpened = true;
                        chatModel.Message.ViewModel.IsUploadRunning = false;
                        chatModel.Message.ViewModel.UploadPercentage = 0;
                        chatModel.Message.FileSize = messageDTO.FileSize;
                        chatModel.Message.Width = messageDTO.Width;
                        chatModel.Message.Height = messageDTO.Height;
                        chatModel.Message.OriginalMessage = messageDTO.OriginalMessage;
                        chatModel.Message.Message = messageDTO.Message;
                        chatModel.Message.Status = messageDTO.Status;
                    }

                    ChatService.SendFriendChat(messageDTO.PacketID, messageDTO.FriendTableID, messageDTO.MessageType, messageDTO.Timeout, messageDTO.OriginalMessage, messageDTO.MessageDate, messageDTO.IsSecretVisible, null, null);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendFileManifestUploaded() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendChatMediaDownloadCompleted(long friendId, string fileNameWithPath, string packetId, int messageType)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::CHAT_MEDIA_DOWNLOAD_COMPLETED".PadRight(72, ' ') + "==>   friendId = " + friendId + ", messageType = " + messageType + ", packetId = " + packetId + ", fileNameWithPath = " + fileNameWithPath);
#endif
                if (messageType == (int)MessageType.IMAGE_FILE_FROM_GALLERY || messageType == (int)MessageType.IMAGE_FILE_FROM_CAMERA)
                {
                    string filePath = RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER + Path.DirectorySeparatorChar + packetId + ".jpg";
                    if (File.Exists(fileNameWithPath))
                    {
                        File.Move(fileNameWithPath, filePath);
                    }
                }
                else if (messageType == (int)MessageType.VIDEO_FILE)
                {
                    string filePath = RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER + Path.DirectorySeparatorChar + packetId + ".mp4";
                    if (File.Exists(fileNameWithPath))
                    {
                        File.Move(fileNameWithPath, filePath);
                        string thumbPath = RingIDSettings.TEMP_CHAT_FILES_FOLDER + Path.DirectorySeparatorChar + packetId + ".jpg";
                        if (!File.Exists(thumbPath))
                        {
                            try { MediaService.GetThumbnail(RingIDSettings.TEMP_MEDIA_TOOLKIT, filePath, thumbPath, 1); }
                            catch { }
                        }
                    }

                }
                else if (messageType == (int)MessageType.AUDIO_FILE)
                {
                    string filePath = RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER + Path.DirectorySeparatorChar + packetId + ".mp3";
                    ChatHelpers.DecodeChatMediaAudioUsingOpus(fileNameWithPath, filePath);
                }

                RecentModel chatModel = GetRecentModel(friendId, 0, null, packetId);
                if (chatModel != null)
                {
                    chatModel.Message.IsFileOpened = true;
                    chatModel.Message.ViewModel.IsForceDownload = false;
                    chatModel.Message.IsDownloadRunning = false;
                    chatModel.Message.ViewModel.DownloadPercentage = 0;
                    ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();

                    if (chatModel.Message.MessageType == MessageType.IMAGE_FILE_FROM_GALLERY
                        || chatModel.Message.MessageType == MessageType.IMAGE_FILE_FROM_CAMERA
                        || chatModel.Message.MessageType == MessageType.VIDEO_FILE)
                    {
                        chatModel.Message.IsPreviewOpened = true;
                        chatModel.Message.OnPropertyChanged("IsPreviewOpened");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendChatMediaDownloadCompleted() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendChatMediaTransferCanceled(long friendId, string fileName, string packetId, bool isUpload, bool isChunkedTransfer)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::CHAT_MEDIA_TRANSFER_CANCELED".PadRight(72, ' ') + "==>   friendId = " + friendId + ", fileName = " + fileName + ", packetId = " + packetId + ", isUpload = " + isUpload + ", isChunkedTransfer = " + isChunkedTransfer);
#endif
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendChatMediaTransferCanceled() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onFriendChatMediaTransferFailed(long friendId, string fileName, string packetId, bool isUpload, bool isChunkedTransfer)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::FRIEND::CHAT_MEDIA_TRANSFER_FAILED".PadRight(72, ' ') + "==>   friendId = " + friendId + ", fileName = " + fileName + ", isUpload = " + isUpload + ", isChunkedTransfer = " + isChunkedTransfer);
#endif
                if (isUpload)
                {
                    //if (!isChunkedTransfer)
                    //{
                    MessageDTO messageDTO = RecentChatCallActivityDAO.GetMessageDTOByPacketID(friendId, null, packetId);
                    if (messageDTO != null)
                    {
                        messageDTO.Status = ChatConstants.STATUS_FAILED;
                        RecentChatCallActivityDAO.UpdateChatMessageStatus(friendId, 0, packetId, messageDTO.Status, 0, 0, 0);
                        RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                        RecentModel chatModel = GetRecentModel(friendId, 0, null, messageDTO.PacketID);
                        if (chatModel != null)
                        {
                            chatModel.Message.IsFileOpened = true;
                            chatModel.Message.ViewModel.IsUploadRunning = false;
                            chatModel.Message.ViewModel.UploadPercentage = 0;
                            chatModel.Message.Status = messageDTO.Status;
                        }
                    }
                    //}
                }
                else
                {
                    //if (!isChunkedTransfer)
                    //{
                    RecentModel chatModel = GetRecentModel(friendId, 0, null, packetId);
                    if (chatModel != null)
                    {
                        chatModel.Message.IsFileOpened = false;
                        chatModel.Message.ViewModel.IsForceDownload = false;
                        chatModel.Message.IsDownloadRunning = false;
                        chatModel.Message.ViewModel.DownloadPercentage = 0;
                    }
                    ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                    //}
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onFriendChatMediaTransferFailed() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Friend File Transfer

        #region Group File Transfer

        public override void onGroupFileStreamRequestReceived(long friendId, long groupId, long fileId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::FILE_STREAM_REQUEST_RECEIVED".PadRight(72, ' ') + "==>   groupId = " + groupId + ", friendId = " + friendId + ", fileId = " + fileId);
#endif

                if (FileTransferSession.IsFileTransferRunning(groupId, fileId))
                {
#if CHAT_LOG
                    log.Debug("FILE TRANSFER => onGroupFileStreamRequestReceived() => File Upload Already Running. GroupID = " + groupId + ", FileID = " + fileId + "");
#endif
                    return;
                }

                MessageDTO messageDTO = RecentChatCallActivityDAO.GetMessageDTOByPacketID(null, groupId, null, fileId);
                if (messageDTO != null)
                {
                    if (messageDTO.FileStatus == ChatConstants.FILE_TRANSFER_CANCELLED || messageDTO.FileStatus == ChatConstants.FILE_MOVED)
                    {
#if CHAT_LOG
                        log.Debug("FILE TRANSFER => onGroupFileStreamRequestReceived() => File Already Cancelled Or Moved. GroupID = " + messageDTO.GroupID + ", FileID = " + messageDTO.FileID + "");
#endif
                        ChatHelpers.SendFileStreamCancel(0, groupId, fileId, messageDTO);
                        return;
                    }
                    else if (messageDTO.Status == ChatConstants.TYPE_DELETE_MSG)
                    {
#if CHAT_LOG
                        log.Debug("FILE TRANSFER => onGroupFileStreamRequestReceived() => Message Deleted. GroupID = " + messageDTO.GroupID + ", FileID = " + messageDTO.FileID + "");
#endif
                        ChatHelpers.SendFileStreamCancel(0, groupId, fileId, messageDTO);
                        return;
                    }
                    else if (!String.IsNullOrWhiteSpace(messageDTO.FileMenifest))
                    {
                        return;
                    }

                    ObservableCollection<RecentModel> chatModelList = RingIDViewModel.Instance.GetRecentModelListByID(messageDTO.GroupID);
                    RecentModel chatModel = chatModelList.Where(P => P.UniqueKey == messageDTO.PacketID).FirstOrDefault();
                    MessageModel model = chatModel != null ? chatModel.Message : null;

                    string filePath = ChatHelpers.GetChatFilePath(messageDTO.Message, (MessageType)messageDTO.MessageType, messageDTO.PacketID, messageDTO.LinkImageUrl, messageDTO.SenderTableID);
                    FileInfo fi = new FileInfo(filePath);
                    if (fi.Exists)
                    {
                        if (model != null)
                        {
                            model.FileStatus = ChatConstants.FILE_DEFAULT;
                            model.ViewModel.IsUploadRunning = true;
                            model.ViewModel.UploadPercentage = 0;
                            model.ViewModel.FileProgressInSize = 0;
                        }

                        RecentChatCallActivityDAO.UpdateChatMessageFileStatusInSameProcess(0, messageDTO.GroupID, messageDTO.PacketID, ChatConstants.FILE_DEFAULT);

                        ChatFileDTO fileDTO = new ChatFileDTO();
                        fileDTO.PacketID = messageDTO.PacketID;
                        fileDTO.ContactID = messageDTO.GroupID;
                        fileDTO.SenderTableID = messageDTO.SenderTableID;
                        fileDTO.GroupID = messageDTO.GroupID;
                        fileDTO.FileID = messageDTO.FileID;
                        fileDTO.File = filePath;
                        fileDTO.FileSize = messageDTO.FileSize;
                        fileDTO.RecentModel = model;
                        fileDTO.IsDownstream = false;

#if CHAT_LOG
                        log.Debug("FILE TRANSFER => ChatFileTransferProcessorQueue->AddItem. ContactID = " + messageDTO.GroupID + ", FileID = " + messageDTO.FileID + ", IsDownstream = " + false);
#endif
                        ImageDictionaries.Instance.FILE_UPLOAD_PROCESSOR_QUEUE.AddItem(fileDTO);
                    }
                    else
                    {
                        if (model != null)
                        {
                            model.FileStatus = ChatConstants.FILE_MOVED;
                            model.ViewModel.IsUploadRunning = false;
                            model.ViewModel.UploadPercentage = 0;
                        }
#if CHAT_LOG
                        log.Debug("FILE TRANSFER => onGroupFileStreamRequestReceived() => File Already Moved. GroupID = " + messageDTO.GroupID + ", FileID = " + messageDTO.FileID + "");
#endif

                        RecentChatCallActivityDAO.UpdateChatMessageFileStatusInSameProcess(0, messageDTO.GroupID, messageDTO.PacketID, ChatConstants.FILE_MOVED);
                        ChatHelpers.SendFileStreamCancel(0, groupId, fileId, messageDTO);
                    }
                }
                else
                {
#if CHAT_LOG
                    log.Debug("FILE TRANSFER => onGroupFileStreamRequestReceived() => Message Not Found. GroupID = " + groupId + ", FileID = " + fileId + "");
#endif
                    ChatHelpers.SendFileStreamCancel(0, groupId, fileId, null);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupFileStreamRequestReceived() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupFileSessionRequestSuccess(string packetId, long groupId, long fileId, int fileTransferPort)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::FILE_SESSION_REQUEST_SENT".PadRight(72, ' ') + "==>   groupId = " + groupId + ", fileId = " + fileId + ", fileTransferPort = " + fileTransferPort + ", packetId = " + packetId);
#endif

                BaseGroupInformation groupRegInfo = ChatService.GetGroupRegisterInfo(groupId);
                string fileTransferIP = groupRegInfo != null ? groupRegInfo.RegisterServerAddress : null;

                InvokeDelegate(new ChatEventArgs { PacketID = packetId, GroupID = groupId, ServerIP = fileTransferIP, ServerPort = fileTransferPort, Status = true });
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupFileSessionRequestSuccess() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupFileTransferSessionRequestFailed(string packetId, long groupId, long fileId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::FILE_SESSION_REQUEST_FAILED".PadRight(72, ' ') + "==>   groupId = " + groupId + ", fileId = " + fileId + ", packetId = " + packetId);
#endif
                InvokeDelegate(new ChatEventArgs { PacketID = packetId, GroupID = groupId, Status = false });
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupFileTransferSessionRequestFailed() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupFileStreamRequestSuccess(string packetId, long groupId, long fileId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::FILE_STREAM_REQUEST_SENT".PadRight(72, ' ') + "==>   groupId = " + groupId + ", fileId = " + fileId + ", packetId = " + packetId);
#endif
                InvokeDelegate(new ChatEventArgs { PacketID = packetId, GroupID = groupId, Status = true });
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupFileStreamRequestSuccess() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupFileTransferStreamRequestFailed(string packetId, long groupId, long fileId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::FILE_STREAM_REQUEST_FAILED".PadRight(72, ' ') + "==>   groupId = " + groupId + ", fileId = " + fileId + ", packetId = " + packetId);
#endif
                InvokeDelegate(new ChatEventArgs { PacketID = packetId, GroupID = groupId, Status = false });
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupFileTransferStreamRequestFailed() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupIncomingChatMedia(long friendId, long groupId, string fileName, string packetId, int messageType, long messageDate, string caption, int widthOrFileSize, int heightOrDuration)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::INCOMING_CHAT_MEDIA".PadRight(72, ' ') + "==>   friendId = " + friendId + ", groupId = " + groupId + ", fileName = " + fileName + ", packetId = " + packetId + ", messageType = " + messageType + ", messageDate = " + messageDate);
#endif
                MessageDTO messageDTO = new MessageDTO();
                messageDTO.GroupID = groupId;
                messageDTO.SenderTableID = friendId;
                messageDTO.PacketID = packetId;
                messageDTO.MessageType = messageType;
                switch ((MessageType)messageDTO.MessageType)
                {
                    case MessageType.IMAGE_FILE_FROM_GALLERY:
                    case MessageType.IMAGE_FILE_FROM_CAMERA:
                        messageDTO.OriginalMessage = ChatJSONParser.MakeImageMessage(String.Empty, caption, widthOrFileSize, heightOrDuration);
                        break;
                    case MessageType.AUDIO_FILE:
                    case MessageType.VIDEO_FILE:
                        messageDTO.OriginalMessage = ChatJSONParser.MakeMultimediaMessage(String.Empty, widthOrFileSize, heightOrDuration);
                        break;
                }
                messageDTO.MessageDate = messageDate;
                messageDTO.Status = ChatConstants.STATUS_PENDING;
                messageDTO.PacketType = ChatConstants.PACKET_TYPE_GROUP_MESSAGE;
                ChatJSONParser.ParseMessage(messageDTO);

                UCGroupChatCallPanel ucGroupChatCall = UIDictionaries.Instance.GROUP_CHAT_CALL_DICTIONARY.TryGetValue(messageDTO.GroupID);
                GroupInfoModel groupInfoModel = ucGroupChatCall != null ? ucGroupChatCall.GroupInfoModel : RingIDViewModel.Instance.GetGroupInfoModelByGroupID(messageDTO.GroupID);

                ChatHelpers.UpdateGroupHistoryTime(messageDTO.GroupID, messageDTO.PacketID, messageDTO.PacketID, groupInfoModel);

                bool isDeleted = messageDTO.MessageType == (int)MessageType.DELETE_MESSAGE;
                bool isNotifiable = !(isDeleted || ChatHelpers.IsDontDisturbMood());
                bool isChatViewInactive = UIHelperMethods.IsViewActivate(ucGroupChatCall) == false;
                bool isChatViewHiddenInactive = isChatViewInactive || HelperMethods.IsViewVisible(ucGroupChatCall) == false;
                bool isViewAtTop = HelperMethods.IsViewAtTop(ucGroupChatCall);

                bool isSound = groupInfoModel.ImSoundEnabled && isNotifiable;
                bool isPopup = groupInfoModel.ImPopupEnabled && isNotifiable && isChatViewInactive;
                bool isUnread = groupInfoModel.ImNotificationEnabled && isNotifiable && (isChatViewHiddenInactive || isViewAtTop);

                messageDTO.IsUnread = isUnread;
                RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                RecentDTO recentDTO = new RecentDTO();
                recentDTO.ContactID = groupId.ToString();
                recentDTO.GroupID = groupId;
                recentDTO.Time = messageDTO.MessageDate;
                recentDTO.IsMoveToButtom = !isViewAtTop;
                recentDTO.Message = messageDTO;

                if (isSound)
                {
                    if (isChatViewHiddenInactive)
                    {
                        AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_RECEIVED_ALERT_TUNE);
                    }
                    else
                    {
                        AudioFilesAndSettings.Play(AudioFilesAndSettings.IM_RECEIVED_TUNE);
                    }
                }

                if (isPopup)
                {
                    WNChatPopupMessage.ShowNotification(recentDTO);
                }

                if (isUnread)
                {
                    ChatHelpers.AddUnreadChat(recentDTO.ContactID, recentDTO.Message.PacketID, recentDTO.Message.MessageDate);
                }

                if (ucGroupChatCall != null && (ucGroupChatCall.IsVisible || isDeleted))
                {
                    RecentLoadUtility.LoadRecentData(recentDTO);
                }

                ChatLogLoadUtility.LoadRecentData(recentDTO);
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupIncomingChatMedia() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupChatMediaTransferProgress(long friendId, long groupId, string fileName, double percentage, bool isUpload, string packetId)
        {
            try
            {
                //#if CHAT_LOG
                //                log.Debug("HANDLER::GROUP::CHAT_MEDIA_TRANSFER_PROGRESS".PadRight(72, ' ') + "==>   friendId = " + friendId + ", groupId = " + groupId + ", fileName = " + fileName + ", percentage = " + percentage + ", isUpload = " + isUpload + ", packetId = " + packetId);
                //#endif
                RecentModel chatModel = GetRecentModel(0, groupId, null, packetId);
                if (chatModel != null)
                {
                    if (isUpload)
                    {
                        chatModel.Message.ViewModel.UploadPercentage = (int)percentage;
                    }
                    else
                    {
                        chatModel.Message.ViewModel.DownloadPercentage = (int)percentage;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupChatMediaTransferProgress() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupFileManifestUploaded(long groupId, string packetId, string cloudManifestUrl, int messageType, string caption, int widthOrFileSize, int heightOrDuration)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::CHAT_MEDIA_MENIFEST_UPLOADED".PadRight(72, ' ') + "==>   groupId = " + groupId + ", packetId = " + packetId + ", cloudManifestUrl = " + cloudManifestUrl + ", messageType = " + messageType + ", caption = " + caption + ", widthOrFileSize = " + widthOrFileSize + ", heightOrDuration = " + heightOrDuration);
#endif
                if (messageType == (int)MessageType.AUDIO_FILE)
                {
                    string filePath = RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER + Path.DirectorySeparatorChar + packetId + ".g729";
                    if (File.Exists(filePath))
                    {
                        try { File.Delete(filePath); }
                        catch { }
                    }
                }

                MessageDTO messageDTO = RecentChatCallActivityDAO.GetMessageDTOByPacketID(0, groupId, packetId);
                if (messageDTO != null)
                {
                    switch ((MessageType)messageDTO.MessageType)
                    {
                        case MessageType.IMAGE_FILE_FROM_GALLERY:
                        case MessageType.IMAGE_FILE_FROM_CAMERA:
                            messageDTO.OriginalMessage = ChatJSONParser.MakeImageMessage(cloudManifestUrl, caption, widthOrFileSize, heightOrDuration);
                            break;
                        case MessageType.AUDIO_FILE:
                        case MessageType.VIDEO_FILE:
                            messageDTO.OriginalMessage = ChatJSONParser.MakeMultimediaMessage(cloudManifestUrl, widthOrFileSize, heightOrDuration);
                            break;
                    }

                    ChatJSONParser.ParseMessage(messageDTO);
                    RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                    RecentModel chatModel = GetRecentModel(0, groupId, null, messageDTO.PacketID);
                    if (chatModel != null)
                    {
                        chatModel.Message.IsFileOpened = true;
                        chatModel.Message.ViewModel.IsUploadRunning = false;
                        chatModel.Message.ViewModel.UploadPercentage = 0;
                        chatModel.Message.FileSize = messageDTO.FileSize;
                        chatModel.Message.Width = messageDTO.Width;
                        chatModel.Message.Height = messageDTO.Height;
                        chatModel.Message.OriginalMessage = messageDTO.OriginalMessage;
                        chatModel.Message.Message = messageDTO.Message;
                        chatModel.Message.Status = messageDTO.Status;
                    }

                    ChatService.SendGroupChat(messageDTO.PacketID, messageDTO.GroupID, messageDTO.MessageType, messageDTO.OriginalMessage, messageDTO.MessageDate);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupFileManifestUploaded() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupChatMediaDownloadCompleted(long friendId, long groupId, string fileNameWithPath, string packetId, int messageType)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::CHAT_MEDIA_DOWNLOAD_COMPLETED".PadRight(72, ' ') + "==>   friendId = " + friendId + ", groupId = " + groupId + ", messageType = " + messageType + ", packetId = " + packetId + ", fileNameWithPath = " + fileNameWithPath);
#endif
                if (messageType == (int)MessageType.IMAGE_FILE_FROM_GALLERY || messageType == (int)MessageType.IMAGE_FILE_FROM_CAMERA)
                {
                    string filePath = RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER + Path.DirectorySeparatorChar + packetId + ".jpg";
                    if (File.Exists(fileNameWithPath))
                    {
                        File.Move(fileNameWithPath, filePath);
                    }
                }
                else if (messageType == (int)MessageType.VIDEO_FILE)
                {
                    string filePath = RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER + Path.DirectorySeparatorChar + packetId + ".mp4";
                    if (File.Exists(fileNameWithPath))
                    {
                        File.Move(fileNameWithPath, filePath);
                        string thumbPath = RingIDSettings.TEMP_CHAT_FILES_FOLDER + Path.DirectorySeparatorChar + packetId + ".jpg";
                        if (!File.Exists(thumbPath))
                        {
                            try { MediaService.GetThumbnail(RingIDSettings.TEMP_MEDIA_TOOLKIT, filePath, thumbPath, 1); }
                            catch { }
                        }
                    }

                }
                else if (messageType == (int)MessageType.AUDIO_FILE)
                {
                    string filePath = RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER + Path.DirectorySeparatorChar + packetId + ".mp3";
                    ChatHelpers.DecodeChatMediaAudioUsingOpus(fileNameWithPath, filePath);
                }

                RecentModel chatModel = GetRecentModel(0, groupId, null, packetId);
                if (chatModel != null)
                {
                    chatModel.Message.IsFileOpened = true;
                    chatModel.Message.ViewModel.IsForceDownload = false;
                    chatModel.Message.IsDownloadRunning = false;
                    chatModel.Message.ViewModel.DownloadPercentage = 0;
                    ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();

                    if (chatModel.Message.MessageType == MessageType.IMAGE_FILE_FROM_GALLERY
                        || chatModel.Message.MessageType == MessageType.IMAGE_FILE_FROM_CAMERA
                        || chatModel.Message.MessageType == MessageType.VIDEO_FILE)
                    {
                        chatModel.Message.IsPreviewOpened = true;
                        chatModel.Message.OnPropertyChanged("IsPreviewOpened");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupChatMediaDownloadCompleted() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupChatMediaTransferCanceled(long friendId, long groupId, string fileName, string packetId, bool isUpload, bool isChunkedTransfer)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::CHAT_MEDIA_TRANSFER_CANCELED".PadRight(72, ' ') + "==>   friendId = " + friendId + ", groupId = " + groupId + ", fileName = " + fileName + ", packetId = " + packetId + ", isUpload = " + isUpload + ", isChunkedTransfer = " + isChunkedTransfer);
#endif
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupChatMediaTransferCanceled() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onGroupChatMediaTransferFailed(long friendId, long groupId, string fileName, string packetId, bool isUpload, bool isChunkedTransfer)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::GROUP::CHAT_MEDIA_TRANSFER_FAILED".PadRight(72, ' ') + "==>   friendId = " + friendId + ", groupId = " + groupId + ", fileName = " + fileName + ", isUpload = " + isUpload + ", isChunkedTransfer = " + isChunkedTransfer);
#endif
                if (isUpload)
                {
                    //if (!isChunkedTransfer)
                    //{
                    MessageDTO messageDTO = RecentChatCallActivityDAO.GetMessageDTOByPacketID(null, groupId, packetId);
                    if (messageDTO != null)
                    {
                        messageDTO.Status = ChatConstants.STATUS_FAILED;
                        RecentChatCallActivityDAO.UpdateChatMessageStatus(0, groupId, packetId, messageDTO.Status, 0, 0, 0);
                        RecentChatCallActivityDAO.InsertMessageDTO(messageDTO);

                        RecentModel chatModel = GetRecentModel(0, groupId, null, messageDTO.PacketID);
                        if (chatModel != null)
                        {
                            chatModel.Message.IsFileOpened = true;
                            chatModel.Message.ViewModel.IsUploadRunning = false;
                            chatModel.Message.ViewModel.UploadPercentage = 0;
                            chatModel.Message.Status = messageDTO.Status;
                        }
                    }
                    //}
                }
                else
                {
                    //if (!isChunkedTransfer)
                    //{
                    RecentModel chatModel = GetRecentModel(0, groupId, null, packetId);
                    if (chatModel != null)
                    {
                        chatModel.Message.IsFileOpened = false;
                        chatModel.Message.ViewModel.IsForceDownload = false;
                        chatModel.Message.IsDownloadRunning = false;
                        chatModel.Message.ViewModel.DownloadPercentage = 0;
                    }
                    ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                    //}
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at onGroupChatMediaTransferFailed() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Group File Transfer

        #region Common

        public override void onChatConversationListRequestStatus(long friendOrGroupId, string packetId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::COMMON::GET_CONVERSATION_LIST_CONFIRMATION".PadRight(72, ' ') + "==>   friendOrGroupId = " + friendOrGroupId + ", packetId = " + packetId + ", status = " + status);
#endif
                InvokeDelegate(new ChatEventArgs { PacketID = packetId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onChatConversationListRequestStatus() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onChatConversationListCompleted()
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::COMMON::CONVERSATION_LIST_COMPLETED".PadRight(72, ' ') + "==>   void(*)");
#endif
                SettingsConstants.VALUE_RINGID_IS_NO_MORE_CHAT_LOG = true;
                new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_RINGID_IS_NO_MORE_CHAT_LOG, SettingsConstants.VALUE_RINGID_IS_NO_MORE_CHAT_LOG.ToString());
            }
            catch (Exception ex)
            {
                log.Error("Error at onChatConversationListCompleted() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onConversationListDeleteRequestStatus(string packetId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::COMMON::CONVERSATION_DELETE_CONFIRMATION".PadRight(72, ' ') + "==>   packetId = " + packetId + ", status = " + status);
#endif
                InvokeDelegate(new ChatEventArgs { PacketID = packetId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onConversationListDeleteRequestStatus() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onConversationListMarkAsSeenRequestStatus(string packetId, bool status)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::COMMON::CONVERSATION_MARK_AS_SEEN_CONFIRMATION".PadRight(72, ' ') + "==>   packetId = " + packetId + ", status = " + status);
#endif
                InvokeDelegate(new ChatEventArgs { PacketID = packetId, Status = status });
            }
            catch (Exception ex)
            {
                log.Error("Error at onConversationListMarkAsSeenRequestStatus() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onServerTimeSynced(long timeDifference)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::COMMON::SERVER_TIME_SYNCED".PadRight(72, ' ') + "==>   timeDifference = " + timeDifference);
#endif

            }
            catch (Exception ex)
            {
                log.Error("Error at onServerTimeSynced() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void shouldCheckFriendPresence(long friendId, bool fromAnonymousUser)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::COMMON::UPDATE_FRIEND_PRESENCE".PadRight(72, ' ') + "==>   friendId = " + friendId);
#endif
                new ThreadFriendPresenceInfo(friendId, (presence, mood, status) =>
                 {
                     ChatService.UpdateFriendPresenceAndMood(friendId, presence, mood);
                     return true;
                 });
            }
            catch (Exception ex)
            {
                log.Error("Error at shouldCheckFriendPresence() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void shouldCheckOfflineServerAddress()
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::COMMON::UPDATE_OFFLINE_SERVER_ADDRESS".PadRight(72, ' ') + "==>   (void)");
#endif

                ChatGetOfflineIPAndPort requester = new ChatGetOfflineIPAndPort((ip, port, status) =>
                {
                    ChatService.UpdateOfflineIPAndPort(ip, port);
                    return true;
                });
                requester.Start();
            }
            catch (Exception ex)
            {
                log.Error("Error at shouldCheckOfflineServerAddress() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void shouldCheckServerAddressForFriend(long friendId, bool fromAnonymousUser)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::COMMON::UPDATE_FRIEND_REGISTER".PadRight(72, ' ') + "==>   friendId = " + friendId);
#endif
                UserBasicInfoModel friendInfoModel = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(friendId);
                BlockedNonFriendModel blockModel = RingIDViewModel.Instance.GetBlockedNonFriendModelByID(friendId);
                ChatService.StartFriendChat(friendId, friendInfoModel, blockModel, null, true, false);
            }
            catch (Exception ex)
            {
                log.Error("Error at shouldCheckServerAddressForFriend() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void shouldCheckServerAddressForGroup(long groupId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::COMMON::UPDATE_GROUP_REGISTER".PadRight(72, ' ') + "==>   groupId = " + groupId);
#endif

                ChatService.StartGroupChat(groupId, null, null, true);
            }
            catch (Exception ex)
            {
                log.Error("Error at shouldCheckServerAddressForGroup() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void shouldCheckServerAddressForRoom(string roomId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::COMMON::UPDATE_ROOM_REGISTER".PadRight(72, ' ') + "==>   roomId = " + roomId);
#endif
            }
            catch (Exception ex)
            {
                log.Error("Error at shouldCheckServerAddressForRoom() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void shouldCheckServerAddressForLiveStreamChat(long publisherId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::COMMON::UPDATE_STREAM_REGISTER".PadRight(72, ' ') + "==>   publisherId = " + publisherId);
#endif
            }
            catch (Exception ex)
            {
                log.Error("Error at shouldCheckServerAddressForLiveStreamChat() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void shouldUpdateUserPassword()
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::COMMON::UPDATE_USER_PASSWORD".PadRight(72, ' ') + "==>   (void)");
#endif
                ChatService.UpdateUserPassword(DefaultSettings.VALUE_LOGIN_USER_PASSWORD);
            }
            catch (Exception ex)
            {
                log.Error("Error at shouldUpdateUserPassword() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onEventHandlerAttached()
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::COMMON::EVENT_HANDLER_ATTACHED".PadRight(72, ' ') + "==>   (void)");
#endif
            }
            catch (Exception ex)
            {
                log.Error("Error at onEventHandlerAttached() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onEventHandlerDetached()
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::COMMON::EVENT_HANDLER_DETACHED".PadRight(72, ' ') + "==>   (void)");
#endif
            }
            catch (Exception ex)
            {
                log.Error("Error at onEventHandlerDetached() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onSDKError(int errorCode, string packetId)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::COMMON::SDK_ERROR".PadRight(72, ' ') + "==>   errorCode = " + errorCode + ", packetId = " + packetId);
#endif
            }
            catch (Exception ex)
            {
                log.Error("Error at onSDKError() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void onShadowIdsInfoRequestFailed(string packetId) { }

        public override void onShadowIdsInfoResponseReceived(List<BasePublicChatMemberDTO> shadowIdsInfo) { }

        public override void onNetworkDataCounted(BaseDataCounter dataCounter)
        {
            try
            {
#if CHAT_LOG
                log.Debug("HANDLER::COMMON::NETWORK_DATA_COUNTER".PadRight(72, ' ') + "==>   dataCounter = " + dataCounter.ToString());
#endif
            }
            catch (Exception ex)
            {
                log.Error("Error at onNetworkDataCounted() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Common

        #region Additional

        private RecentModel GetRecentModel(long friendId, long groupId, string roomId, string packetId)
        {
            string id = String.Empty;
            if (friendId > 0)
            {
                id = friendId.ToString();
            }
            else if (groupId > 0)
            {
                id = groupId.ToString();
            }
            else
            {
                id = roomId;
            }
            ObservableCollection<RecentModel> chatModelList = RingIDViewModel.Instance.GetRecentModelListByID(id);
            RecentModel chatModel = chatModelList.Where(P => P.UniqueKey == packetId).FirstOrDefault();
            return chatModel;
        }

        private MessageModel GetStreamMessageModel(long publisherId, string packetId)
        {
            ObservableCollection<MessageModel> messageList = StreamViewModel.Instance.GetMessageListByPublisherID(publisherId);
            MessageModel msgModel = messageList.Where(P => P.PacketID == packetId).FirstOrDefault();
            return msgModel;
        }

        private void InvokeDelegate(ChatEventArgs args)
        {
            new Thread(() =>
            {
                try
                {
                    ChatEventDelegate onComplete = null;
                    if (ChatDictionaries.Instance.CHAT_EVENT_DELEGATE.TryRemove(args.PacketID, out onComplete))
                    {
                        onComplete.Invoke(args);
                        onComplete = null;
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error at InsertDelegate() ==>" + ex.Message + "\n" + ex.StackTrace);
                }
            }).Start();
        }

        private void InvokeDelegate(ChatEventDelegate onComplete, ChatEventArgs args)
        {
            new Thread(() =>
            {
                try
                {
                    if (onComplete != null)
                    {
                        onComplete.Invoke(args);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error at InsertDelegate() ==>" + ex.Message + "\n" + ex.StackTrace);
                }
            }).Start();
        }

        #endregion Additional

    }

    public class FileTrasnferHandler : IFileTrasnferListener
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(FileTrasnferHandler).Name);

        public override void OnNotifyForFileTransfer(int eventType, long fileId, long friendId, long offset, long receiveLength, double speed)
        {

        }

        public override void OnNotifyForFileTransferProgress(int eventType, long contactId, long fileId, long receiveLength, bool isGroupFileTransfer)
        {
            try
            {
                ChatFileDTO fileDTO = null;
                lock (FileTransferSession._Lock)
                {
                    //#if CHAT_LOG
                    //                    log.Debug("FILE TRANSFER => NOFICATION TYPE = " + eventType + ", ContactID = " + contactId + ", FileID = " + fileId + ", ReceiveLength = " + receiveLength + ", isGroupFileTransfer = " + isGroupFileTransfer);
                    //#endif
                    FileTransferSession session = FileTransferSession.FILE_TRANSFER_SESSION.TryGetValue(contactId);
                    if (session != null)
                    {
                        session.LastActivityTime = ChatService.GetServerTime();
                        session.FILE_INFO.TryGetValue(fileId, out fileDTO);
                        if (eventType != ChatConstants.SDK_FILE_EVENT_PROGRESS && fileDTO != null)
                        {
                            bool status = session.FILE_INFO.TryRemove(fileId);

                            if (session.FILE_INFO.Count <= 0)
                            {
#if CHAT_LOG
                                log.Debug("FILE TRANSFER => NOFICATION TYPE = " + eventType + ". ALL FILE COMPLETED. GroupID = " + contactId);
#endif
                                session.StopKeepAlive();
                                FileTransferSession.FILE_TRANSFER_SESSION.TryRemove(contactId);
                                new Thread(() =>
                                {
                                    int r = ChatService.CloseChannel(contactId, BaseMediaType.IPV_MEDIA_FILE_TRANSFER);
#if CHAT_LOG
                                    log.Debug("FILE TRANSFER => CloseChannel. ContactID =" + contactId + " IsSucceed = " + (r == 1));
#endif
                                }).Start();
                            }

                            if (status)
                            {
#if CHAT_LOG
                                log.Debug("FILE TRANSFER => ChatFileTransferProcessorQueue->OnComplete. ContactID = " + contactId + ", FileID = " + fileId + ", IsDownstream = " + fileDTO.IsDownstream);
#endif
                                if (fileDTO.IsDownstream)
                                {
                                    ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                                }
                                else
                                {
                                    ImageDictionaries.Instance.FILE_UPLOAD_PROCESSOR_QUEUE.OnComplete();
                                }
                            }
                        }
                    }
                    else
                    {
#if CHAT_LOG
                        log.Debug("FILE TRANSFER => NOFICATION TYPE = " + eventType + ". FAILED => SESSION NOT FOUND");
#endif
                    }
                }

                if (fileDTO != null)
                {
                    switch (eventType)
                    {
                        case ChatConstants.SDK_FILE_EVENT_PROGRESS:
                            OnFileStreamProgress(receiveLength, fileDTO);
                            break;
                        case ChatConstants.SDK_FILE_EVENT_COMPLETE:
                            OnFileStreamCompleted(receiveLength, fileDTO);
                            break;
                        case ChatConstants.SDK_FILE_EVENT_CANCELED:
                            OnFileStreamCancelled(receiveLength, fileDTO);
                            break;
                        case ChatConstants.SDK_FILE_EVENT_FAILED:
                            OnFileStreamFailed(receiveLength, fileDTO);
                            break;
                        case ChatConstants.SDK_FILE_EVENT_ERROR:
                            OnFileStreamCancelled(fileDTO.IsDownstream ? 0 : -1, fileDTO);
                            break;
                        case ChatConstants.SDK_FILE_EVENT_PAUSED:
                            OnFileStreamPaused(fileDTO);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in ChatService class in OnNotifyForFileTransferProgress method ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void OnNotifyForFileTransferManifest(int eventType, long contactId, long fileId, int length, string manifestCloudUrl, bool isGroupFileTransfer)
        {
            try
            {
#if CHAT_LOG
                log.Debug("FILE TRANSFER => NOFICATION TYPE = " + eventType + ", contactID = " + contactId + ", FileID = " + fileId + ", manifestCloudUrl = " + manifestCloudUrl + ", isGroupFileTransfer = " + isGroupFileTransfer);
#endif
                if (eventType == ChatConstants.SDK_FILE_EVENT_MENIFEST)
                {
                    if (isGroupFileTransfer)
                    {
                        ChatHelpers.SendFileStreamMenifest(0, contactId, fileId, manifestCloudUrl);
                    }
                    else
                    {
                        ChatHelpers.SendFileStreamMenifest(contactId, 0, fileId, manifestCloudUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in ChatService class in OnNotifyForFileTransferManifest method ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public override void OnNotifyForFileTransferErrorEvent(int eventType, long contactId, long fileId, int length, string errorMessage, int errorCode, bool isGroupFileTransfer)
        {
#if CHAT_LOG
            log.Debug("FILE TRANSFER => NOFICATION TYPE = " + eventType + ", contactId = " + contactId + ", FileID = " + fileId + ", errorCode = " + errorCode + ", errorMessage = " + errorMessage + ", isGroupFileTransfer = " + isGroupFileTransfer);
#endif
            if (!DefaultSettings.IsInternetAvailable && (errorCode == ChatConstants.ERROR_INTERNET_UNAVAILABLE || errorCode == ChatConstants.ERROR_CONNECTION_TIMEOOUT))
            {
                OnNotifyForFileTransferProgress(ChatConstants.SDK_FILE_EVENT_PAUSED, contactId, fileId, 0, isGroupFileTransfer);
            }
            else
            {
                OnNotifyForFileTransferProgress(eventType, contactId, fileId, 0, isGroupFileTransfer);
            }
        }

        #region Additional

        private RecentModel GetRecentModel(long friendId, long groupId, string roomId, string packetId)
        {
            string id = String.Empty;
            if (friendId > 0)
            {
                id = friendId.ToString();
            }
            else if (groupId > 0)
            {
                id = groupId.ToString();
            }
            else
            {
                id = roomId;
            }
            ObservableCollection<RecentModel> chatModelList = RingIDViewModel.Instance.GetRecentModelListByID(id);
            RecentModel chatModel = chatModelList.Where(P => P.UniqueKey == packetId).FirstOrDefault();
            return chatModel;
        }

        public void OnFileStreamProgress(long receivedLength, ChatFileDTO chatFileDTO)
        {
            try
            {
                MessageModel model = (MessageModel)chatFileDTO.RecentModel;
                if (model == null)
                {
                    RecentModel chatModel = GetRecentModel(chatFileDTO.FriendTableID, chatFileDTO.GroupID, null, chatFileDTO.PacketID);
                    if (chatModel != null && chatModel.Message != null)
                    {
                        model = chatModel.Message;
                        chatFileDTO.RecentModel = model;
                    }
                }
                if (model != null)
                {
                    int percentage = (int)((receivedLength * 100) / chatFileDTO.FileSize);
                    if (chatFileDTO.IsDownstream)
                    {
                        model.ViewModel.DownloadPercentage = percentage == 0 ? 1 : percentage > 100 ? 100 : percentage;
                    }
                    else
                    {
                        model.ViewModel.UploadPercentage = percentage == 0 ? 1 : percentage > 100 ? 100 : percentage;
                    }
                    model.ViewModel.FileProgressInSize = receivedLength;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at OnFileStreamProgress() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnFileStreamCompleted(long receivedLength, ChatFileDTO chatFileDTO)
        {
            try
            {
                MessageModel model = (MessageModel)chatFileDTO.RecentModel;
                if (model == null)
                {
                    RecentModel chatModel = GetRecentModel(chatFileDTO.FriendTableID, chatFileDTO.GroupID, null, chatFileDTO.PacketID);
                    if (chatModel != null && chatModel.Message != null)
                    {
                        model = chatModel.Message;
                        chatFileDTO.RecentModel = model;
                    }
                }
                if (model != null)
                {
                    if (chatFileDTO.IsDownstream)
                    {
                        model.FileStatus = ChatConstants.FILE_TRANSFER_COMPLETED;
                        model.ViewModel.IsForceDownload = false;
                        model.IsDownloadRunning = false;
                    }
                    else
                    {
                        model.FileStatus = ChatConstants.FILE_TRANSFER_COMPLETED;
                        model.ViewModel.IsUploadRunning = false;
                    }
                    model.ViewModel.FileProgressInSize = 0;
                }
                RecentChatCallActivityDAO.UpdateChatMessageFileStatusInSameProcess(chatFileDTO.FriendTableID, chatFileDTO.GroupID, chatFileDTO.PacketID, ChatConstants.FILE_TRANSFER_COMPLETED);
            }
            catch (Exception ex)
            {
                log.Error("Error at OnFileStreamCompleted() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnFileStreamCancelled(long receivedLength, ChatFileDTO chatFileDTO)
        {
            try
            {
                MessageDTO messageDTO = RecentChatCallActivityDAO.GetMessageDTOByPacketID(chatFileDTO.FriendTableID, chatFileDTO.GroupID, chatFileDTO.PacketID);
                if (messageDTO != null)
                {
                    if (messageDTO.FileStatus == ChatConstants.FILE_MOVED || messageDTO.MessageType == (int)MessageType.DELETE_MESSAGE)
                    {
#if CHAT_LOG
                        log.Debug("FILE TRANSFER => OnFileStreamCancelled() => File Already Moved Or Message Deleted. GroupID = " + chatFileDTO.GroupID + ", FileID = " + chatFileDTO.FileID + "");
#endif
                        return;
                    }

                    MessageModel model = (MessageModel)chatFileDTO.RecentModel;
                    if (model == null)
                    {
                        RecentModel chatModel = GetRecentModel(chatFileDTO.FriendTableID, chatFileDTO.GroupID, null, chatFileDTO.PacketID);
                        if (chatModel != null && chatModel.Message != null)
                        {
                            model = chatModel.Message;
                            chatFileDTO.RecentModel = model;
                        }
                    }
                    if (model != null)
                    {
                        if (chatFileDTO.IsDownstream)
                        {
                            if (receivedLength == -1)
                            {
                                model.FileStatus = ChatConstants.FILE_TRANSFER_CANCELLED;
                            }
                            model.ViewModel.IsForceDownload = false;
                            model.IsDownloadRunning = false;
                            model.ViewModel.DownloadPercentage = 0;
                        }
                        else
                        {
                            if (receivedLength == -1)
                            {
                                model.FileStatus = ChatConstants.FILE_TRANSFER_CANCELLED;
                            }
                            model.ViewModel.IsUploadRunning = false;
                            model.ViewModel.UploadPercentage = 0;
                        }
                        model.ViewModel.FileProgressInSize = 0;
                    }

                    if (receivedLength == -1)
                    {
                        RecentChatCallActivityDAO.UpdateChatMessageFileStatusInSameProcess(chatFileDTO.FriendTableID, chatFileDTO.GroupID, chatFileDTO.PacketID, ChatConstants.FILE_TRANSFER_CANCELLED);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at OnFileStreamCancelled() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnFileStreamFailed(long receivedLength, ChatFileDTO chatFileDTO)
        {
            try
            {
                MessageModel model = (MessageModel)chatFileDTO.RecentModel;
                if (model == null)
                {
                    RecentModel chatModel = GetRecentModel(chatFileDTO.FriendTableID, chatFileDTO.GroupID, null, chatFileDTO.PacketID);
                    if (chatModel != null && chatModel.Message != null)
                    {
                        model = chatModel.Message;
                        chatFileDTO.RecentModel = model;
                    }
                }

                if (model != null)
                {
                    if (chatFileDTO.IsDownstream)
                    {
                        model.ViewModel.IsForceDownload = false;
                        model.IsDownloadRunning = false;
                        model.ViewModel.DownloadPercentage = 0;
                    }
                    else
                    {
                        model.ViewModel.IsUploadRunning = false;
                        model.ViewModel.UploadPercentage = 0;
                    }
                    model.ViewModel.FileProgressInSize = 0;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at OnFileStreamFailed() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void OnFileStreamPaused(ChatFileDTO chatFileDTO)
        {
            try
            {
                MessageModel model = (MessageModel)chatFileDTO.RecentModel;
                if (model == null)
                {
                    RecentModel chatModel = GetRecentModel(chatFileDTO.FriendTableID, chatFileDTO.GroupID, null, chatFileDTO.PacketID);
                    if (chatModel != null && chatModel.Message != null)
                    {
                        model = chatModel.Message;
                        chatFileDTO.RecentModel = model;
                    }
                }

                if (chatFileDTO.IsDownstream)
                {
                    ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.AddItemToFront(chatFileDTO);
                }
                else
                {
                    ImageDictionaries.Instance.FILE_UPLOAD_PROCESSOR_QUEUE.AddItemToFront(chatFileDTO);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error at OnFileStreamFailed() ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Additional
    }

}

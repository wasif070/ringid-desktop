﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.Threading;
namespace View.Utility.Chat
{
    public class ChatGetOfflineIPAndPort
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ChatGetOfflineIPAndPort).Name);
        private Func<string, int, bool, bool> _OnComplete = null;

        public ChatGetOfflineIPAndPort(Func<string, int, bool, bool> onComplete = null)
        {
            this._OnComplete = onComplete;
        }

        public void Start()
        {
            Thread t = new Thread(new ThreadStart(Run));
            t.Name = this.GetType().Name;
            t.Start();
        }

        public void Run()
        {
            //Callback("192.168.1.125", 1200, true);
            //return;

            if (!String.IsNullOrWhiteSpace(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                try
                {
                    JObject pakToSend = new JObject();
                    string pakId = SendToServer.GetRanDomPacketID();
                    //pakToSend[JsonKeys.RID] = DefaultSettings.LOGIN_USER_ID.ToString();
                    pakToSend[JsonKeys.PacketId] = pakId;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_IM_OFFLINE_SERVER;


                    JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakId);
                    if (feedbackfields != null)
                    {
                        if (feedbackfields != null && (bool)feedbackfields[JsonKeys.Success])
                        {
                            string ip = string.Empty;
                            int port = 0;
                            if (feedbackfields[JsonKeys.OfflineServerIp] != null)
                            {
                                ip = (string)feedbackfields[JsonKeys.OfflineServerIp];
                            }
                            if (feedbackfields[JsonKeys.OfflineServerPrt] != null)
                            {
                                port = (int)feedbackfields[JsonKeys.OfflineServerPrt];
                            }

                            DefaultSettings.RING_OFFLINE_IM_IP = ip;
                            DefaultSettings.RING_OFFLINE_IM_PORT = port;

                            if (!string.IsNullOrWhiteSpace(ip))
                            {
                                Callback(ip, port, true);
                            }
                            else
                            {
                                Callback(String.Empty, 0, false);
                            }
                        }
                        else
                        {
                            Callback(String.Empty, 0, false);
                        }
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId);
                    }
                    else
                    {
                        Callback(String.Empty, 0, false);
                        if (!MainSwitcher.ThreadManager().PingNow())
                        {
                        }
                    }

                    //string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
                    //SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
                    //for (int i = 0; i < 5; i++)
                    //{
                    //    Thread.Sleep(500);
                    //    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(pakId))
                    //    {
                    //        SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
                    //    }
                    //    else
                    //    {
                    //        JObject FeedBackFields = RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId);
                    //        if (FeedBackFields != null && (bool)FeedBackFields[JsonKeys.Success])
                    //        {
                    //            string ip = string.Empty;
                    //            int port = 0;
                    //            if (FeedBackFields[JsonKeys.OfflineServerIp] != null)
                    //            {
                    //                ip = (string)FeedBackFields[JsonKeys.OfflineServerIp];
                    //            }
                    //            if (FeedBackFields[JsonKeys.OfflineServerPrt] != null)
                    //            {
                    //                port = (int)FeedBackFields[JsonKeys.OfflineServerPrt];
                    //            }

                    //            if (!string.IsNullOrWhiteSpace(ip))
                    //            {
                    //                Callback(ip, port, true);
                    //            }
                    //            else
                    //            {
                    //                Callback(String.Empty, 0, false);
                    //            }
                    //        }
                    //        else
                    //        {
                    //            Callback(String.Empty, 0, false);
                    //        }
                    //        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId);
                    //        return;
                    //    }
                    //    PingInServer.StartThread(i, 5);
                    //}
                }
                catch (Exception ex)
                {
                    log.Error("ChatGetOfflineIPAndPort ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
                }
            }
            else
            {
                log.Error("ChatGetOfflineIPAndPort Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            }
            //Callback(String.Empty, 0, false);
        }

        private void Callback(string ip, int port, bool status)
        {
            try
            {
                if (_OnComplete != null)
                {
                    _OnComplete(ip, port, status);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in Callback() => " + ex.Message + "\n" + ex.StackTrace);
            }
            _OnComplete = null;
        }

    }
}

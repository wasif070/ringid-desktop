﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using System.Threading;
using View.UI;

namespace View.Utility.Feed
{
    public class ThreadHideSpecificUser
    {
        #region "Private Fields"

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThreadHideSpecificUser).Name);
        private bool running = false;
        long utIdorPid;
        bool hide = true;

        BaseUserProfileModel userProfileModel;
        private Func<bool, int> _Oncomplete;
        private bool _IsFromLiveStream;

        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        public ThreadHideSpecificUser()
        {

        }
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.UserTableID] = utIdorPid;
                pakToSend[JsonKeys.Hide] = hide;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_HIDE_UNHIDE_USER_FEED;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                    {
                        if (_IsFromLiveStream == false)
                        {
                            UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, "Successfully " + (hide ? "Hidden" : "Unhidden") + " this Profile Feeds!");
                            MainSwitcher.AuthSignalHandler().feedSignalHandler.HideSpecificProfile(utIdorPid, userProfileModel, hide);
                        }

                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                        if (this._Oncomplete != null)
                        {
                            this._Oncomplete(true);
                        }
                    }
                    else
                    {
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                        if (_IsFromLiveStream)
                        {
                            UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, "Failed to " + (hide == false ? "Follow" : "UnFollow") + " this user!");
                        }
                        else
                        {
                           UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to hide this profile's feeds! Please try later!", "Failed");
                        }

                    }
                }
                else
                {
                    if (_IsFromLiveStream)
                    {
                        UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, "Failed to " + (hide == false ? "Follow" : "UnFollow") + " this user!");
                    }
                    else
                    {
                       UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to hide this profile's feeds! Please try later!", "Failed");
                    }

                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {
               UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to hide this profile's feeds! Please Try later!", "Failed");
            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"

        public void StartThread(long utIdorPid, BaseUserProfileModel userProfileModel, bool hide = true, bool isFromLiveStream = false, Func<bool, int> onComplete = null)
        {
            this._Oncomplete = onComplete;
            this._IsFromLiveStream = isFromLiveStream;
            if (!running)
            {
                this.utIdorPid = utIdorPid;
                this.userProfileModel = userProfileModel;
                this.hide = hide;
                Thread thrd = new Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

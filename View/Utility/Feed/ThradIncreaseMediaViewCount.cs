﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using Newtonsoft.Json;

namespace View.Utility.Feed
{
    public class ThradIncreaseMediaViewCount
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradIncreaseMediaViewCount).Name);
        private bool running = false;
        private int mediaType;
        private long utId; 
        Guid albumId;
        private Guid contentId;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_INCREASE_MEDIA_CONTENT_VIEW_COUNT;
                pakToSend[JsonKeys.ContentId] = contentId;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success] && feedbackfields[JsonKeys.LikeOrComment] != null)
                    {
                        short NewAccessCount = (short)feedbackfields[JsonKeys.LikeOrComment];
                        MainSwitcher.AuthSignalHandler().feedSignalHandler.IncreaseMediaViewCount(contentId, utId, albumId, mediaType, NewAccessCount);
                    }
                    else
                    {
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(long utId, int mediaType, Guid albumId, Guid contentId)
        {
            if (!running)
            {
                this.utId = utId;
                this.mediaType = mediaType;
                this.albumId = albumId;
                this.contentId = contentId;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
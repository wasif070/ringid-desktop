﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Threading;

namespace View.Utility.Feed
{
    public class ThreadGetSingleFullComment
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ThreadGetSingleFullComment).Name);
        private Guid NfId, CmntId, ImgId, contentId;
        private bool success = false;
        public delegate void CallBack(JObject commentObject);
        public event CallBack callBackEvent;
        public ThreadGetSingleFullComment(Guid NfId, Guid CmntId, Guid ImgId, Guid contentId)
        {
            this.NfId = NfId;
            this.CmntId = CmntId;
            this.ImgId = ImgId;
            this.contentId = contentId;
        }
        public void StartThread()
        {
            new Thread(new ThreadStart(Run)).Start();
        }
        public void Run()
        {
            if (!String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                try
                {
                    JObject pakToSend = new JObject();
                    String pakId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = pakId;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_ACTION_GET_FULL_COMMENT;
                    if (ImgId != Guid.Empty)
                    {
                        pakToSend[JsonKeys.ImageId] = ImgId;
                        pakToSend[JsonKeys.CommentType] = SettingsConstants.TYPE_IMAGE_COMMENT;
                    }
                    else if (contentId != Guid.Empty)
                    {
                        pakToSend[JsonKeys.ContentId] = contentId;
                        pakToSend[JsonKeys.CommentType] = SettingsConstants.TYPE_MEDIA_COMMENT;
                    }
                    else if (NfId != Guid.Empty)
                    {
                        pakToSend[JsonKeys.NewsfeedId] = NfId;
                        pakToSend[JsonKeys.CommentType] = SettingsConstants.TYPE_FEED_COMMENT;
                    }
                    pakToSend[JsonKeys.CommentId] = CmntId;

                    JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakId);
                    if (feedbackfields != null)
                    {
                        if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                        {
                            if (feedbackfields[JsonKeys.CommentDTO] != null)
                            {
                                JObject jObj = (JObject)feedbackfields[JsonKeys.CommentDTO];
                                success = true;
                                if (callBackEvent != null)
                                {
                                    callBackEvent(jObj);
                                    callBackEvent = null;
                                }
                            }
                        }
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
                    }
                }
                catch (Exception e)
                {
                    log.Error("ShowContinueCmmntex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            }
            else
            {
                log.Error("ShowContinueCmmntex Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                //msg = NotificationMessages.INTERNET_UNAVAILABLE;
            }
            if (!success && callBackEvent != null)
            {
                callBackEvent(null);
                callBackEvent = null;
            }
        }
    }
}

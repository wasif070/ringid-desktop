﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;


namespace View.Utility.Feed
{
    public class ThradSaveUnsaveNewsPortalFeed
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradSaveUnsaveNewsPortalFeed).Name);
        private bool running = false;
        private List<Guid> nfIdLst;
        private string sb;
        private int option; //fieldName - option value - 1/2/3 1 => Save ids 2 => Remove selected Ids3 => Remove All
        private int ProfileType;
        private FeedModel Model;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        public ThradSaveUnsaveNewsPortalFeed(int option, List<Guid> nfIdLst, int ProfileType, FeedModel model)
        {
            this.nfIdLst = nfIdLst;
            this.option = option;
            this.sb = (option == 1) ? "save" : "remove";
            this.ProfileType = ProfileType;
            this.Model = model;
        }

        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_SAVE_UNSAVE_NEWSFEED;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Option] = option;

                if (option != 3)
                {
                    JArray Arr = new JArray();
                    foreach (var item in nfIdLst)
                    {
                        Arr.Add(item);
                    }
                    pakToSend[JsonKeys.FeedIdsList] = Arr;
                }
                JObject FeedBackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (FeedBackFields != null)
                {
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out FeedBackFields);
                    if ((bool)FeedBackFields[JsonKeys.Success])
                    {
                        MainSwitcher.AuthSignalHandler().feedSignalHandler.SaveUnsaveFeed(option, Model);//SaveUnsaveNewsPortalFeed(option, nfIdLst);
                    }
                    else
                    {
                       UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to " + sb + " this feed! Please Try later!", "Failed");
                    }
                }
                else
                {
                   UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to " + sb + " this feed! Please Check your Inernet Connection or Try later!", "Failed");
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Threading;

namespace View.Utility.Feed
{
    public class DiscoverOrFollowingChannels
    {
        private readonly ILog log = LogManager.GetLogger(typeof(DiscoverOrFollowingChannels).Name);
        private int st, profileType, subScType; //1=dis,2=fol,0-both 
        public delegate void CallBack(int success);
        public event CallBack callBackEvent;

        public DiscoverOrFollowingChannels(int subScType, int profileType, int st = 0)
        {
            this.subScType = subScType;
            this.st = st;
            this.profileType = profileType;
        }

        public void StartThread()
        {
            new Thread(new ThreadStart(Run)).Start();
        }

        public void Run()
        {
            if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                try
                {
                    JObject pakToSend = new JObject();
                    string pakId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = pakId;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_NEWSPORTAL_LIST;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.SubscribeType] = subScType;
                    pakToSend[JsonKeys.Limit] = 10;
                    if (st > 0) pakToSend[JsonKeys.StartLimit] = st;
                    pakToSend[JsonKeys.ProfileType] = profileType;

                    JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakId);
                    if (feedbackfields != null)
                    {
                        if (feedbackfields[JsonKeys.Success] != null)
                        {
                            callBackEvent(((bool)feedbackfields[JsonKeys.Success]) ? SettingsConstants.RESPONSE_SUCCESS : SettingsConstants.RESPONSE_NOTSUCCESS);
                            callBackEvent = null;
                            return;
                        }
                    }
                    else
                    {
                        callBackEvent(SettingsConstants.NO_RESPONSE);
                        callBackEvent = null;
                        MainSwitcher.ThreadManager().PingNow();
                    }
                    //string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
                    //SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
                    //Thread.Sleep(25);
                    //for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
                    //{
                    //    if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
                    //    {
                    //        break;
                    //    }
                    //    Thread.Sleep(DefaultSettings.WAITING_TIME);
                    //    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out FeedBackFields))
                    //    {
                    //        if (i % DefaultSettings.SEND_INTERVAL == 0)
                    //            SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
                    //    }
                    //    else if (FeedBackFields[JsonKeys.Success] != null)
                    //    {
                    //        callBackEvent((bool)FeedBackFields[JsonKeys.Success]);
                    //        callBackEvent = null;
                    //        return;
                    //    }
                    //    //  PingInServer.StartThread(1, DefaultSettings.TRYING_TIME);
                    //}

                }
                catch (Exception e)
                {
                    callBackEvent(SettingsConstants.NO_RESPONSE);
                    callBackEvent = null;
                    log.Error("SubscribeOrUnsubscribe ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            }
            else
            {
                callBackEvent(SettingsConstants.NO_RESPONSE);
                callBackEvent = null;
                log.Error("SubscribeOrUnsubscribe Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            }

        }
    }
}

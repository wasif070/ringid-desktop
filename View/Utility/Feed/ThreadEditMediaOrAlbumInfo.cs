﻿using Auth.utility;
using log4net;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Threading;
using System.Windows;
using View.BindingModels;
using View.Utility.WPFMessageBox;

namespace View.Utility.Feed
{
    public class ThreadEditMediaOrAlbumInfo
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ThreadEditMediaOrAlbumInfo).Name);
        private SingleMediaModel singleMediaModel;
        private string sngNm, albmNm, artistNm, albmImgUrl;
        private double imageWidth, imageHeight;
        private MediaContentModel mediaContentModel;
        public delegate void CallBack(int success);
        public event CallBack callBackEvent;

        public void StartThread(SingleMediaModel model, string nm, string artistNm)
        {
            this.singleMediaModel = model;
            this.sngNm = nm;
            this.artistNm = artistNm;
            this.mediaContentModel = null;
            new Thread(new ThreadStart(Run)).Start();
        }
        public void StartThread(MediaContentModel model, string nm, string albmImgUrl, double ImageWidth, double ImageHeight)
        {
            this.mediaContentModel = model;
            this.albmNm = nm;
            this.albmImgUrl = albmImgUrl;
            this.singleMediaModel = null;
            this.imageHeight = ImageHeight;
            this.imageWidth = ImageWidth;
            new Thread(new ThreadStart(Run)).Start();
        }
        public void Run()
        {
            if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                try
                {
                    JObject pakToSend = new JObject();
                    string pakId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = pakId;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    if (singleMediaModel != null)
                    {
                        pakToSend[JsonKeys.Action] = AppConstants.TYPE_EDIT_MEDIA_CONTENT;
                        JObject mediaCntnt = new JObject();
                        mediaCntnt[JsonKeys.ContentId] = singleMediaModel.ContentId;
                        mediaCntnt[JsonKeys.StreamUrl] = singleMediaModel.StreamUrl;
                        mediaCntnt[JsonKeys.ThumbUrl] = singleMediaModel.ThumbUrl;
                        mediaCntnt[JsonKeys.Duration] = singleMediaModel.Duration;
                        mediaCntnt[JsonKeys.MediaType] = singleMediaModel.MediaType;
                        mediaCntnt[JsonKeys.AlbumId] = singleMediaModel.AlbumId;
                        mediaCntnt[JsonKeys.UserTableID] = singleMediaModel.MediaOwner.UserTableID;

                        mediaCntnt[JsonKeys.Title] = sngNm;
                        mediaCntnt[JsonKeys.Artist] = artistNm;

                        pakToSend[JsonKeys.MediaContentDTO] = mediaCntnt;
                    }
                    else if (mediaContentModel != null)
                    {
                        if (albmImgUrl != null)
                        {
                            pakToSend[JsonKeys.CoverImageUrl] = albmImgUrl;
                            pakToSend[JsonKeys.ImageHeight] = imageHeight;
                            pakToSend[JsonKeys.ImageWidth] = imageWidth;
                            pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_ALBUM_COVER_IMAGE;
                        }
                        else
                        {
                            pakToSend[JsonKeys.AlbumName] = albmNm;
                            pakToSend[JsonKeys.Action] = AppConstants.TYPE_UPDATE_MEDIA_ALBUM;
                        }
                        //pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_ALBUM_COVER_IMAGE;
                        pakToSend[JsonKeys.AlbumId] = mediaContentModel.AlbumId;
                        pakToSend[JsonKeys.MediaType] = mediaContentModel.MediaType;
                        //pakToSend[JsonKeys.CoverImageUrl] = albmImgUrl;
                        //pakToSend[JsonKeys.ImageHeight] = imageHeight;
                        //pakToSend[JsonKeys.ImageWidth] = ImageWidth;
                    }

                    JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, pakId);
                    if (feedbackfields != null && feedbackfields[JsonKeys.Success] != null)
                    {
                        if (pakId != null && feedbackfields[JsonKeys.PacketId] == null) feedbackfields[JsonKeys.PacketId] = pakId;
                        if (!(bool)feedbackfields[JsonKeys.Success])
                        {
                            callBackEvent(SettingsConstants.RESPONSE_NOTSUCCESS);
                            callBackEvent = null;
                        }
                        else
                        {
                            callBackEvent(SettingsConstants.RESPONSE_SUCCESS);
                            callBackEvent = null;
                            if (singleMediaModel != null)
                            {
                                singleMediaModel.Title = sngNm;
                                singleMediaModel.Artist = artistNm;
                            }
                            else if (mediaContentModel != null)
                            {
                                mediaContentModel.AlbumName = albmNm;
                                mediaContentModel.AlbumImageUrl = albmImgUrl;
                            }
                        }
                        return;
                    }
                    else
                    {
                        MainSwitcher.ThreadManager().PingNow();
                    }
                }
                catch (Exception e)
                {
                    log.Error("ThreadEditMediaContent ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            }
            else
            {
                log.Error("ThreadEditMediaContent Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            }
            callBackEvent(SettingsConstants.NO_RESPONSE);
            callBackEvent = null;
            //Application.Current.Dispatcher.Invoke((Action)(() =>
            //{
            //    CustomMessageBox.ShowWarning("Failed to Edit! Please Check your internet connection or try later!");
            //}));
        }
    }
}

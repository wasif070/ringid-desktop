﻿using Auth.utility;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Feed
{

    public class ThreadDetailsOfStatus
    {
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThreadDetailsOfStatus).Name);
        private Guid nfId;
        private int waitingTime = 0;

        public ThreadDetailsOfStatus(Guid nfId)
        {
            // TODO: Complete member initialization
            this.nfId = nfId;
        }
        public void Run(out bool isSuccess, out JObject feed, out string msg)
        {
            msg = "";
            isSuccess = false;
            feed = null;
            if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                try
                {
                    JObject pakToSend = new JObject();
                    string pakId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = pakId;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_SINGLE_STATUS_NOTIFICATION;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.NewsfeedId] = nfId;

                    JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakId);
                    if (feedbackfields != null)
                    {
                        if (feedbackfields[JsonKeys.Success] != null && !(bool)feedbackfields[JsonKeys.Success])
                        {
                        }
                        else
                        {
                            while (feedbackfields[JsonKeys.NewsFeedList] == null)
                            {
                                //Console.WriteLine("*************************=>" + waitingTime);
                                if (waitingTime == DefaultSettings.TRYING_TIME)
                                {
                                    break;
                                }
                                Thread.Sleep(DefaultSettings.WAITING_TIME);
                                RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out feedbackfields);
                                waitingTime++;
                                if (feedbackfields[JsonKeys.Success] != null && !(bool)feedbackfields[JsonKeys.Success])
                                {
                                    return;
                                }
                            }
                            JArray jarray = (JArray)feedbackfields[JsonKeys.NewsFeedList];
                            if (jarray != null)
                                foreach (JObject singleObj in jarray)
                                {
                                    //feed = HelperMethodsModel.BindFeedDetails(singleObj);
                                    //lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
                                    //{
                                    //    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
                                    //}
                                    //lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME)
                                    //{
                                    //    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
                                    //}
                                    feed = singleObj;
                                    isSuccess = true;
                                    return;
                                }
                        }
                    }
                    else
                    {
                        MainSwitcher.ThreadManager().PingNow();
                    }
                }
                catch (Exception e)
                {
                    log.Error("ThreadDetailsOfStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            }
            else
            {
                log.Error("ThreadDetailsOfStatus Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            }
        }
        //public void Run(out bool isSuccess, out FeedDTO feed, out string msg)
        //{
        //    msg = "";
        //    isSuccess = false;
        //    feed = null;
        //    if (!String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
        //    {
        //        try
        //        {
        //            JObject pakToSend = new JObject();
        //            String pakId = SendToServer.GetRanDomPacketID();
        //            pakToSend[JsonKeys.PacketId] = pakId;
        //            pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
        //            pakToSend[JsonKeys.Action] = AppConstants.TYPE_SINGLE_STATUS_NOTIFICATION;
        //            pakToSend[JsonKeys.NewsfeedId] = nfId;

        //            JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakId);
        //            if (feedbackfields != null)
        //            {
        //                if (feedbackfields[JsonKeys.Success] != null && !(bool)feedbackfields[JsonKeys.Success])
        //                {
        //                }
        //                else
        //                {
        //                    while (feedbackfields[JsonKeys.NewsFeedList] == null)
        //                    {
        //                        //Console.WriteLine("*************************=>" + waitingTime);
        //                        if (waitingTime == DefaultSettings.TRYING_TIME)
        //                        {
        //                            break;
        //                        }
        //                        Thread.Sleep(DefaultSettings.WAITING_TIME);
        //                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out feedbackfields);
        //                        waitingTime++;
        //                        if (feedbackfields[JsonKeys.Success] != null && !(bool)feedbackfields[JsonKeys.Success])
        //                        {
        //                            return;
        //                        }
        //                    }
        //                    if ((bool)feedbackfields[JsonKeys.Success] && feedbackfields[JsonKeys.NewsFeedList] != null)
        //                    {
        //                        isSuccess = true;
        //                        JArray jarray = (JArray)feedbackfields[JsonKeys.NewsFeedList];
        //                        foreach (JObject singleObj in jarray)
        //                        {
        //                            feed = HelperMethodsModel.BindFeedDetails(singleObj);
        //                            lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
        //                            {
        //                                NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
        //                            }

        //                            if (feed.ParentFeed != null)
        //                            {
        //                                lock (NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY)
        //                                {
        //                                    NewsFeedDictionaries.Instance.SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY[feed.NewsfeedId] = feed.ParentFeed.NewsfeedId;
        //                                }

        //                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
        //                                {
        //                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.ParentFeed.NewsfeedId] = feed.ParentFeed;
        //                                }
        //                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME)
        //                                {
        //                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.ParentFeed.NewsfeedId, feed.ParentFeed.Time, feed.ParentFeed.FeedCategory);

        //                                }
        //                            }
        //                            else
        //                            {
        //                                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME)
        //                                {
        //                                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else if (feedbackfields[JsonKeys.Message] != null)
        //                    {
        //                        msg = (string)feedbackfields[JsonKeys.Message];
        //                    }
        //                    else if (feedbackfields[JsonKeys.ReasonCode] != null)
        //                    {
        //                        msg = ReasonCodeConstants.GetReason((int)feedbackfields[JsonKeys.ReasonCode]);
        //                    }
        //                }
        //                //RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
        //            }
        //            else
        //            {
        //                if (!MainSwitcher.ThreadManager().PingNow())
        //                {
        //                    msg = NotificationMessages.INTERNET_UNAVAILABLE;
        //                }
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            log.Error("DetailsOfStatus ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //        }
        //    }
        //    else
        //    {
        //        log.Error("DetailsOfStatus Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
        //        msg = NotificationMessages.INTERNET_UNAVAILABLE;
        //    }

        //}
    }
}

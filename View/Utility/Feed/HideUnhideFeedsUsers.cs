﻿using Auth.utility.Feed;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using View.BindingModels;
using View.Dictonary;
using View.UI;
using View.UI.Circle;
using View.UI.Feed;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.Utility.Feed
{
    public class HideUnhideFeedsUsers
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(HideUnhideFeedsUsers).Name);

        public static void HideAllNewsFeedsByUser(long userIdentity, string name, long circleId)
        {
            //new Thread(() =>
            //{
            //    try
            //    {
            //        lock (NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS)
            //        {
            //            NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Add(userIdentity);
            //        }

            //        /////////////ALL FEEDS/////////////////////
            //        List<long> needToDelNfIds = new List<long>();
            //        foreach (CustomSortedList.Data data in NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME)
            //        {
            //            FeedDTO feed;
            //            NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.TryGetValue(data.NfId, out feed);

            //            if (feed.WhoShare != null)
            //            {
            //                FeedDTO whoShareFeed = feed.WhoShare;
            //                whoShareFeed.ParentFeed = feed;

            //                if (NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.RingID)
            //                    || NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.FriendUserTableId)
            //                    || NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.RingID)
            //                    || NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.FriendUserTableId))
            //                {
            //                    needToDelNfIds.Add(whoShareFeed.NewsfeedId);
            //                    needToDelNfIds.Add(whoShareFeed.ParentFeed.NewsfeedId);
            //                }

            //            }
            //            else
            //            {
            //                if (NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.RingID)
            //                    || NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.FriendUserTableId))
            //                {
            //                    needToDelNfIds.Add(feed.NewsfeedId);
            //                }
            //            }
            //        }

            //        foreach (long newsFeedId in needToDelNfIds)
            //        {
            //            RingIDViewModel.Instance.NewsFeeds.RemoveModel(newsFeedId);
            //            NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.RemoveData(newsFeedId);

            //            if (UCAllFeeds.Instance != null)
            //            {
            //                FeedModel model;
            //                //UCAllFeeds.Instance.FEEDMODELS_WAITINGLIST.TryRemove(newsFeedId, out model);
            //            }
            //        }

            //        //////////MEDIA FEEDS///////////
            //        needToDelNfIds.Clear();

            //        foreach (CustomSortedList.Data data in NewsFeedDictionaries.Instance.MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME)
            //        {
            //            FeedDTO feed;
            //            NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.TryGetValue(data.NfId, out feed);

            //            if (feed.WhoShare != null)
            //            {
            //                FeedDTO whoShareFeed = feed.WhoShare;
            //                whoShareFeed.ParentFeed = feed;

            //                if (NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.RingID)
            //                    || NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.FriendUserTableId)
            //                    || NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.RingID)
            //                    || NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.FriendUserTableId))
            //                {
            //                    needToDelNfIds.Add(whoShareFeed.NewsfeedId);
            //                    needToDelNfIds.Add(whoShareFeed.ParentFeed.NewsfeedId);
            //                }

            //            }
            //            else
            //            {
            //                if (NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.RingID)
            //                    || NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.FriendUserTableId))
            //                {
            //                    needToDelNfIds.Add(feed.NewsfeedId);
            //                }
            //            }
            //        }

            //        foreach (long newsFeedId in needToDelNfIds)
            //        {
            //            RingIDViewModel.Instance.MediaFeeds.RemoveModel(newsFeedId);
            //            NewsFeedDictionaries.Instance.MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME.RemoveData(newsFeedId);

            //            if (UCMiddlePanelSwitcher.View_MediaFeed != null && UCMiddlePanelSwitcher.View_MediaFeed._UCMediaFeeds != null)
            //            {
            //                FeedModel model;
            //                UCMiddlePanelSwitcher.View_MediaFeed._UCMediaFeeds.FEEDMODELS_WAITINGLIST.TryRemove(newsFeedId, out model);
            //            }
            //        }

            //        //////////MEDIA TRENDING FEEDS///////////
            //        needToDelNfIds.Clear();

            //        foreach (CustomSortedList.Data data in NewsFeedDictionaries.Instance.MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS)
            //        {
            //            FeedDTO feed;
            //            NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.TryGetValue(data.NfId, out feed);

            //            if (feed.WhoShare != null)
            //            {
            //                FeedDTO whoShareFeed = feed.WhoShare;
            //                whoShareFeed.ParentFeed = feed;

            //                if (NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.RingID)
            //                    || NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.FriendUserTableId)
            //                    || NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.RingID)
            //                    || NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.FriendUserTableId))
            //                {
            //                    needToDelNfIds.Add(whoShareFeed.NewsfeedId);
            //                    needToDelNfIds.Add(whoShareFeed.ParentFeed.NewsfeedId);
            //                }

            //            }
            //            else
            //            {
            //                if (NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.RingID)
            //                    || NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.FriendUserTableId))
            //                {
            //                    needToDelNfIds.Add(feed.NewsfeedId);
            //                }
            //            }
            //        }

            //        foreach (long newsFeedId in needToDelNfIds)
            //        {
            //            RingIDViewModel.Instance.MediaFeedsTrending.RemoveModel(newsFeedId);
            //            NewsFeedDictionaries.Instance.MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS.RemoveData(newsFeedId);

            //            if (UCMiddlePanelSwitcher.View_MediaFeed != null && UCMiddlePanelSwitcher.View_MediaFeed._UCMediaFeedsTrending != null)
            //            {
            //                FeedModel model;
            //                UCMiddlePanelSwitcher.View_MediaFeed._UCMediaFeedsTrending.FEEDMODELS_WAITINGLIST.TryRemove(newsFeedId, out model);
            //            }
            //        }

            //        ///////////////CIRCLE FEEDS///////
            //        if (circleId > 0)
            //        {

            //            //UCCirclePanel _UCCirclePanel = UIDictionaries.Instance.CIRCLE_PANEL_DICTIONARY[circleId];
            //            if (UCMiddlePanelSwitcher.View_UCCirclePanel != null && UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds != null && UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds != null)
            //            {
            //                needToDelNfIds.Clear();

            //                foreach (CustomSortedList.Data data in NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME[circleId])
            //                {
            //                    FeedDTO feed;
            //                    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.TryGetValue(data.NfId, out feed);

            //                    if (feed.WhoShare != null)
            //                    {
            //                        FeedDTO whoShareFeed = feed.WhoShare;
            //                        whoShareFeed.ParentFeed = feed;

            //                        if (NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.RingID)
            //                            || NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.FriendUserTableId)
            //                            || NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.RingID)
            //                            || NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(whoShareFeed.ParentFeed.FriendUserTableId))
            //                        {
            //                            needToDelNfIds.Add(whoShareFeed.NewsfeedId);
            //                            needToDelNfIds.Add(whoShareFeed.ParentFeed.NewsfeedId);
            //                        }

            //                    }
            //                    else
            //                    {
            //                        if (NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.RingID)
            //                            || NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Contains(feed.FriendUserTableId))
            //                        {
            //                            needToDelNfIds.Add(feed.NewsfeedId);
            //                        }
            //                    }
            //                }

            //                foreach (long newsFeedId in needToDelNfIds)
            //                {
            //                    //UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.CircleNewsFeeds.RemoveModel(newsFeedId);
            //                    NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME[circleId].RemoveData(newsFeedId);
            //                }
            //            }
            //        }


            //        DatabaseActivityDAO.Instance.InsertIntoHiddenPostsOfUsers(userIdentity);

            //        CustomMessageBox.ShowMessageWithTimerFromThread("You won't see posts from: " + name + "!");
            //    }
            //    catch (Exception e)
            //    {
            //        log.Error(e.Message + "\n" + e.StackTrace);
            //    }

            //}).Start();
        }

        public static void UnhideAllNewsFeedsByUser(long userIdentity)
        {
            new Thread(() =>
           {
               lock (NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS)
               {
                   NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Remove(userIdentity);
               }
               new DatabaseActivityDAO().RemoveFromHiddenPostsOfUsers(userIdentity);
           }).Start();
        }
    }
}

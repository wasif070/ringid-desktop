﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using View.BindingModels;
using View.UI;
using View.Utility.DataContainer;
using View.ViewModel;

namespace View.Utility.Feed
{
    public class ThrdCelebrityFollowUnfollow
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradSubscribeOrUnsubscribe).Name);

        #region "Private Fields"
        private bool running = false;
        private long utId;
        private bool flw;
        private string flwMsg;
        private CelebrityModel model;
        #endregion

        #region "Constructors"
        public ThrdCelebrityFollowUnfollow(long utId, bool flw, CelebrityModel model)
        {
            this.utId = utId;
            this.flw = flw;
            this.model = model;
            flwMsg = flw ? "follow" : "unfollow";
        }
        #endregion

        #region "Private Methods"
        private void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_FOLLOW_UNFOLLOW_CELEBRITY;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.UserTableID] = utId;
                pakToSend[JsonKeys.Flw] = flw;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (feedbackfields != null)
                {
                    if ((bool)feedbackfields[JsonKeys.Success])
                    {
                        model.IsSubscribed = flw;
                        if (flw)
                        {
                            model.SubscriberCount++;
                            var disCoverModel = RingIDViewModel.Instance.CelebritiesDiscovered.Where(p => p.UserTableID == utId).FirstOrDefault();
                            if (disCoverModel != null)
                            {
                                RingIDViewModel.Instance.CelebritiesDiscovered.InvokeRemove(model);
                            }
                            if (!RingIDViewModel.Instance.CelebritiesFollowing.Any(p => p.UserTableID == utId))
                                RingIDViewModel.Instance.CelebritiesFollowing.InvokeInsert(0, model);
                        }
                        else
                        {
                            model.SubscriberCount--;
                            var flwingModel = RingIDViewModel.Instance.CelebritiesFollowing.Where(p => p.UserTableID == utId).FirstOrDefault();
                            if (flwingModel != null)
                            {
                                RingIDViewModel.Instance.CelebritiesFollowing.InvokeRemove(model);
                            }
                            if (!RingIDViewModel.Instance.CelebritiesDiscovered.Any(p => p.UserTableID == utId))
                                RingIDViewModel.Instance.CelebritiesDiscovered.InvokeInsert(0, model);
                        }
                        if (UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel != null) UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesFeedPanel = null;
                        //RingIDViewModel.Instance.AllCelebrityFeeds.RemoveAllModels();
                        FeedDataContainer.Instance.AllCelebrityFeedsSortedIds.Clear();
                    }
                    else
                    {
                       UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to " + flwMsg + "! Please Try later!", "Failed");
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                   UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to " + flwMsg + "! Please Check your Inernet Connection or Try later!", "Failed");
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {
            }
            finally { running = false; }
        }
        #endregion

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }
        #endregion
    }
}

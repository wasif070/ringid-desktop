﻿using System;
using System.Collections.Generic;
using Auth.utility;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.ViewModel;
using View.BindingModels;

namespace View.Utility.Feed
{
    class ThradAddMediaAlbum
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradAddMediaAlbum).Name);
        private bool running = false;
        string imageUrl;
        int mediaType;
        string albumName;
        SingleMediaModel singleMediaModel;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        public ThradAddMediaAlbum(string imgUrl, int mT, string albmName, SingleMediaModel singleMediaModel = null)
        {
            this.imageUrl = imgUrl;
            this.mediaType = mT;
            this.albumName = albmName;
            if (singleMediaModel != null) this.singleMediaModel = singleMediaModel;
        }
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;

                string packetId = SendToServer.GetRanDomPacketID();
                JObject pakToSend = new JObject();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.MediaAlbumImageURL] = this.imageUrl;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_MEDIA_ALBUM;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.MediaType] = this.mediaType;
                pakToSend[JsonKeys.AlbumName] = this.albumName;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success] && feedbackfields[JsonKeys.AlbumId] != null)
                    {
                        Guid albumId = (Guid)feedbackfields[JsonKeys.AlbumId];
                        MediaContentModel albumModel = new MediaContentModel();
                        albumModel.AlbumId = albumId;
                        albumModel.AlbumName = albumName;
                        albumModel.MediaType = mediaType;
                        albumModel.UserTableID = DefaultSettings.LOGIN_TABLE_ID;
                        albumModel.AlbumImageUrl = imageUrl;
                        if (mediaType == SettingsConstants.MEDIA_TYPE_AUDIO)
                        {
                            RingIDViewModel.Instance.MyAudioAlbums.InvokeAdd(albumModel);
                        }
                        else if (mediaType == SettingsConstants.MEDIA_TYPE_VIDEO)
                        {
                            RingIDViewModel.Instance.MyVideoAlbums.InvokeAdd(albumModel);
                        }
                        ThradAddSingleMediatoAlbum addSingleMediatoAlbum = new ThradAddSingleMediatoAlbum(albumModel, mediaType, singleMediaModel.StreamUrl, singleMediaModel.ThumbUrl, singleMediaModel.Duration, singleMediaModel.Title, singleMediaModel.Artist, singleMediaModel.ThumbImageWidth, singleMediaModel.ThumbImageHeight, singleMediaModel.Privacy);

                        addSingleMediatoAlbum.Run();
                    }
                    else
                    {
                        string msg = (feedbackfields[JsonKeys.Message] != null) ? (string)feedbackfields[JsonKeys.Message] : "Failed to Add Media to the Album!";
                        //MainSwitcher.AuthSignalHandler().feedSignalHandler.EnableAddtoExistingOrNewAlbumPopup(Guid.Empty);
                       UIHelperMethods.ShowErrorMessageBoxFromThread(msg, "Failed!");
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    //MainSwitcher.AuthSignalHandler().feedSignalHandler.EnableAddtoExistingOrNewAlbumPopup(Guid.Empty);
                    string errormsg = (DefaultSettings.IsInternetAvailable) ? "" : ReasonCodeConstants.REASON_MSG_INTERNET_UNAVAILABLE;
                   UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to Add Album!" + errormsg, "Failed!");
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
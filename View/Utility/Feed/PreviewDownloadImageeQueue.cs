﻿using log4net;
using Models.Stores;
using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using View.BindingModels;
using View.Constants;

namespace View.Utility.Feed
{
    public class PreviewDownloadImageeQueue
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(PreviewDownloadImageeQueue).Name);

        private static readonly object _syncRoot = new object();
        public static PreviewDownloadImageeQueue Instance = null;
        private ConcurrentQueue<ImageUploaderModel> ImageUploaderModelsQueue = new ConcurrentQueue<ImageUploaderModel>();
        private BackgroundWorker worker;
        public PreviewDownloadImageeQueue()
        {
            if (worker == null)
            {
                worker = new BackgroundWorker();
                worker.WorkerSupportsCancellation = true;
                worker.WorkerReportsProgress = true;
                worker.DoWork += PreviewImageLoad_DowWork;
                worker.ProgressChanged += PreviewImageLoad_ProgressChanged;
                worker.RunWorkerCompleted += PreviewImageLoad_RunWorkerCompleted;
            }
        }

        public void LoadSingleImageUploaderModel(ImageUploaderModel model)
        {
            try
            {
                lock (_syncRoot)
                {
                    if (PreviewDownloadImageeQueue.Instance == null)
                    {
                        PreviewDownloadImageeQueue.Instance = new PreviewDownloadImageeQueue();
                        PreviewDownloadImageeQueue.Instance.ImageUploaderModelsQueue.Enqueue(model);
                        PreviewDownloadImageeQueue.Instance.Start();
                    }
                    else
                    {
                        PreviewDownloadImageeQueue.Instance.ImageUploaderModelsQueue.Enqueue(model);
                        if (PreviewDownloadImageeQueue.Instance.worker.IsBusy == false)
                        {
                            PreviewDownloadImageeQueue.Instance.Start();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("LoadSingleImageUploaderModel ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void Start()
        {
            worker.RunWorkerAsync();
        }

        private void PreviewImageLoad_DowWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                ImageUploaderModel modelToExecute = null;
                while (ImageUploaderModelsQueue.TryDequeue(out modelToExecute))
                {
                    string imageUrl = HelperMethods.ConvertUrlToName(modelToExecute.ImageUrl, ImageUtility.IMG_600);
                    bool success = ImageUtility.DownloadImage(imageUrl.Replace('_', '/'), modelToExecute.FilePath);



                    //////
                    //Bitmap bmp = (Bitmap)ImageUtility.GetOrientedImageFromFilePath(modelToExecute.FilePath);

                    //int mw = 0, mh = 0;
                    //ImageUtility.GetResizeImageParameters(bmp.Width, bmp.Height, 150, 150, out mw, out mh);

                    //MemoryStream ms = new MemoryStream();
                    //bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    //BitmapImage bitmapsource = new BitmapImage();
                    //bitmapsource.BeginInit();
                    //ms.Seek(0, SeekOrigin.Begin);
                    //bitmapsource.StreamSource = ms;
                    //bitmapsource.CacheOption = BitmapCacheOption.OnLoad;
                    //bitmapsource.DecodePixelHeight = mh + 20;
                    //bitmapsource.DecodePixelWidth = mw + 20;
                    //bitmapsource.EndInit();
                    //ms = null;
                    //bmp.Dispose();
                    //if (bmp != null) bmp.Dispose();

                    //if (ImageDictionaries.Instance.TMP_PREVIEW_IMAGES.ContainsKey(modelToExecute.FilePath))
                    //{
                    //    ImageDictionaries.Instance.TMP_PREVIEW_IMAGES[modelToExecute.FilePath] = bitmapsource;
                    //}
                    //else
                    //{
                    //    ImageDictionaries.Instance.TMP_PREVIEW_IMAGES.Add(modelToExecute.FilePath, bitmapsource);
                    //}

                    //bitmapsource.Freeze();
                    //bitmapsource = null;

                    modelToExecute.OnPropertyChanged("CurrentInstance");
                }
            }
            catch (Exception ex)
            {
                log.Error("PreviewImageLoad_DowWork ==> " + ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
        }


        private void PreviewImageLoad_ProgressChanged(object sender, ProgressChangedEventArgs e) { }

        private void PreviewImageLoad_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json.Linq;

namespace View.Utility.Feed
{
    public class ThradDiscoverOrFollowingChannelsList
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradDiscoverOrFollowingChannelsList).Name);
        private bool running = false;
        private int st, profileType, subScType; //1=dis,2=fol,0-both 
        private string searchParam, countryNm;
        public delegate void OnCallBack(int response);
        public event OnCallBack CallBack;
        #endregion "Private Fields"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_NEWSPORTAL_LIST;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.SubscribeType] = subScType;
                pakToSend[JsonKeys.Limit] = 10;
                if (!string.IsNullOrEmpty(searchParam)) pakToSend[JsonKeys.SearchParam] = searchParam;
                if (!string.IsNullOrEmpty(countryNm)) pakToSend[JsonKeys.CountryDiscover] = countryNm;
                if (st > 0) pakToSend[JsonKeys.StartLimit] = st;
                pakToSend[JsonKeys.ProfileType] = profileType;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
                if (feedbackfields != null)
                {
                    if (CallBack != null)
                    {
                        CallBack(((bool)feedbackfields[JsonKeys.Success]) ? SettingsConstants.RESPONSE_SUCCESS : SettingsConstants.RESPONSE_NOTSUCCESS);
                        CallBack = null;
                    }
                    //if ((bool)feedbackfields[JsonKeys.Success])
                    //{
                    //    if (st == 0)
                    //        stopLoader(0);
                    //    else
                    //        stopLoader(3);
                    //}
                    //else
                    //{
                    //    stopLoader(4);
                    //}
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                    return;
                }
                else
                {
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                    //if (st == 0)
                    //    stopLoader(2);
                    //else
                    //    stopLoader(5);
                }
            }
            catch (Exception)
            {
            }
            if (CallBack != null)
            {
                CallBack(SettingsConstants.NO_RESPONSE);
                CallBack = null;
            }
            running = false;
        }

        //private void stopLoader(int state)
        //{
        //    MainSwitcher.AuthSignalHandler().feedSignalHandler.StopLoaders(profileType, subScType, state);
        //}
        #endregion "Private methods"

        #region "Public Methods"

        public void StartThread(int subScType, int profileType, string searchParam, string countryNm, int st = 0)
        {
            if (!running)
            {
                this.subScType = subScType;
                this.st = st;
                this.searchParam = searchParam;
                this.countryNm = countryNm;
                this.profileType = profileType;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }
        public void StartThread(int subScType, int profileType, int st = 0)
        {
            if (!running)
            {
                this.subScType = subScType;
                this.st = st;
                this.profileType = profileType;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }
        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
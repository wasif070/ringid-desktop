﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Utility.DataContainer;
using View.ViewModel;


namespace View.Utility.Feed
{
    public class ThreadDeleteAnyComment
    {
        /// <summary>
        /// TYPE_DELETE_STATUS_COMMENT = 183;
        /// TYPE_DELETE_IMAGE_COMMENT = 182;
        /// TYPE_DELETE_COMMENT_ON_MEDIA = 267;
        /// </summary>
        
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThreadDeleteAnyComment).Name);
        private bool running = false, success = false, imageIdToContentId = false;
        private string message = string.Empty;
        private Guid newsfeedId, commentId, imageId, contentId;
        public delegate void CallBack(bool success);
        public event CallBack callBackEvent;
        #endregion "Private Fields"
        private JObject pakToSend;

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        public ThreadDeleteAnyComment(JObject PakToSend, bool ImageIdToContentId)
        {
            this.imageIdToContentId = ImageIdToContentId;
            this.pakToSend = PakToSend;
        }
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                    {
                        newsfeedId = (pakToSend[JsonKeys.NewsfeedId] != null) ? (Guid)pakToSend[JsonKeys.NewsfeedId] : Guid.Empty;
                        commentId = (pakToSend[JsonKeys.CommentId] != null) ? (Guid)pakToSend[JsonKeys.CommentId] : Guid.Empty;
                        if (imageIdToContentId)
                            imageId = (pakToSend[JsonKeys.ContentId] != null) ? (Guid)pakToSend[JsonKeys.ContentId] : Guid.Empty;
                        else
                        {
                            contentId = (pakToSend[JsonKeys.ContentId] != null) ? (Guid)pakToSend[JsonKeys.ContentId] : Guid.Empty;
                            imageId = (pakToSend[JsonKeys.ImageId] != null) ? (Guid)pakToSend[JsonKeys.ImageId] : Guid.Empty;
                        }
                        success = true;
                        MainSwitcher.AuthSignalHandler().feedSignalHandler.DeleteOrUpdateDeleteAnyComment(feedbackfields, commentId, newsfeedId, imageId, contentId);
                    }
                    else
                    {
                        if (!MainSwitcher.ThreadManager().PingNow())
                        {
                            message = ReasonCodeConstants.REASON_MSG_INTERNET_UNAVAILABLE;
                        }
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                if (!success)
                   UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to Delete Comment! " + message, "Failed!");
                if (callBackEvent != null)
                {
                    callBackEvent(success);
                    callBackEvent = null;
                }
                running = false;
            }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json.Linq;


namespace View.Utility.Feed
{
    public class ThradAllOrSavedPagesFeedRequest
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradAllOrSavedPagesFeedRequest).Name);
        private bool running = false;
        private long time;
        private short scl;
        private int limit; private bool svd;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                String packetId =SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                if (this.limit == -1)
                {
                    DefaultSettings.ALLPAGE_STARTPKT = packetId;
                    pakToSend[JsonKeys.Limit] = 10;
                }
                else pakToSend[JsonKeys.Limit] = limit;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_BUSINESS_PAGE_FEED;
                pakToSend[JsonKeys.Scroll] = this.scl;
                pakToSend[JsonKeys.Time] = time;
                if (svd) pakToSend[JsonKeys.IsSaved] = true;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
                if (feedbackfields != null)
                {
                    try
                    {
                        if (feedbackfields != null && feedbackfields[JsonKeys.Success] != null)
                        {
                            if ((bool)feedbackfields[JsonKeys.Success])
                            {
                            }
                            else
                            {
                                if (scl == 0 || scl == 2)
                                {
                                    //Thread.Sleep(2000);
                                    //        HelperMethodsAuth.NewsFeedHandlerInstance.BottomLoadingNewsPortalFeed(StatusConstants.LOADING_NO_MORE_FEED_VISIBLE, svd);
                                    BottomLoadingNewsPortalFeed(StatusConstants.LOADING_NO_MORE_FEED_VISIBLE);
                                }
                            }
                        }
                        else
                        {
                            if (scl == 0 || scl == 2)
                            {
                                //Thread.Sleep(2000);
                                //  HelperMethodsAuth.NewsFeedHandlerInstance.BottomLoadingNewsPortalFeed(StatusConstants.LOADING_NO_MORE_FEED_VISIBLE, svd);
                                BottomLoadingNewsPortalFeed(StatusConstants.LOADING_NO_MORE_FEED_VISIBLE);
                            }
                        }
                    }
                    finally { }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }

        private void BottomLoadingNewsPortalFeed(int status)
        {
            //MainSwitcher.AuthSignalHandler().feedSignalHandler.BottomLoadingNewsPortalFeed(status, svd);
        }

        #endregion "Private methods"

        #region "Public Methods"

        public void StartThread(long time, short scl, int limit, bool svd = false)
        {
            if (!running)
            {
                this.time = time;
                this.scl = scl;
                this.limit = limit;
                this.svd = svd;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }
        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using View.BindingModels;

namespace View.Utility.Feed
{
    public class ScaleFeedImages
    {
        private const int SEPERATOR = 3;

        private ObservableCollection<ImageModel> _Images;
        private long _NewsFeedId;
        private int PANEL_HEIGHT = 500;

        public long AddedTime
        {
            get { return _NewsFeedId; }
            set { _NewsFeedId = value; }
        }

        private double _ImageViewHeight = 100;
        public double ImageViewHeight
        {
            get { return _ImageViewHeight; }
            set { _ImageViewHeight = value; }
        }

        public ObservableCollection<ImageModel> Images
        {
            get { return _Images; }
            set { _Images = value; }
        }


        public ScaleFeedImages(ObservableCollection<ImageModel> images, long ActualTime, int PANEL_WIDTH)
        {
            try
            {
                this.AddedTime = ActualTime;
                this._Images = images;
                if (_Images.Count == 1)
                {
                    _Images[0].FeedImageInfoModel.Y = 0;
                    _Images[0].FeedImageInfoModel.X = 0;

                    _Images[0].FeedImageInfoModel.ReWidth = (_Images[0].FeedImageInfoModel.Width >= PANEL_WIDTH) ? PANEL_WIDTH : _Images[0].FeedImageInfoModel.Width;

                    // _Images[0].FeedImageInfoModel.ReHeight = (_Images[0].FeedImageInfoModel.Height >= PANEL_HEIGHT) ? ((_Images[0].FeedImageInfoModel.Height * _Images[0].FeedImageInfoModel.ReWidth) / _Images[0].FeedImageInfoModel.Width) : _Images[0].FeedImageInfoModel.Height;

                    _Images[0].FeedImageInfoModel.ReHeight = (int)(((double)_Images[0].FeedImageInfoModel.ReWidth / (double)Images[0].FeedImageInfoModel.Width) * (double)_Images[0].FeedImageInfoModel.Height);

                    //_Images[0].FeedImageInfoModel.ReHeight = (_Images[0].FeedImageInfoModel.Height >= PANEL_HEIGHT) ? PANEL_HEIGHT : _Images[0].FeedImageInfoModel.Height;

                    if (_Images[0].FeedImageInfoModel.ReHeight > PANEL_WIDTH)
                    {
                        _Images[0].FeedImageInfoModel.ReHeight = PANEL_HEIGHT;
                        _Images[0].FeedImageInfoModel.ReWidth = (int)(((double)_Images[0].FeedImageInfoModel.ReHeight / (double)Images[0].FeedImageInfoModel.Height) * (double)Images[0].FeedImageInfoModel.Width);
                    }

                    ImageViewHeight = _Images[0].FeedImageInfoModel.ReHeight;
                }
                else if (_Images.Count == 2)
                {
                    _Images[0].FeedImageInfoModel.Y = 0;
                    _Images[0].FeedImageInfoModel.X = 0;

                    _Images[1].FeedImageInfoModel.Y = 0;
                    _Images[1].FeedImageInfoModel.X = (PANEL_WIDTH / 2) + SEPERATOR;

                    _Images[1].FeedImageInfoModel.ReWidth = _Images[0].FeedImageInfoModel.ReWidth = (PANEL_WIDTH / 2);

                    _Images[1].FeedImageInfoModel.ReHeight = _Images[0].FeedImageInfoModel.ReHeight = (PANEL_WIDTH / 2);

                    ImageViewHeight = 300;
                }
                else if (_Images.Count == 3)
                {
                    int random = (int)(this.AddedTime % 4);

                    if (random == 1) // top single
                    {
                        _Images[0].FeedImageInfoModel.Y = 0;
                        _Images[0].FeedImageInfoModel.X = 0;

                        _Images[0].FeedImageInfoModel.ReWidth = PANEL_WIDTH + SEPERATOR;
                        _Images[0].FeedImageInfoModel.ReHeight = PANEL_HEIGHT / 2;

                        _Images[1].FeedImageInfoModel.Y = (PANEL_HEIGHT / 2) + SEPERATOR;
                        _Images[1].FeedImageInfoModel.X = 0;

                        _Images[2].FeedImageInfoModel.Y = (PANEL_HEIGHT / 2) + SEPERATOR;
                        _Images[2].FeedImageInfoModel.X = (PANEL_WIDTH / 2) + SEPERATOR;

                        _Images[2].FeedImageInfoModel.ReWidth = _Images[1].FeedImageInfoModel.ReWidth = (PANEL_WIDTH / 2);
                        _Images[2].FeedImageInfoModel.ReHeight = _Images[1].FeedImageInfoModel.ReHeight = (PANEL_HEIGHT / 2);

                        ImageViewHeight = PANEL_HEIGHT + SEPERATOR;

                    }
                    else if (random == 3) // bottom single
                    {
                        _Images[0].FeedImageInfoModel.Y = 0;
                        _Images[0].FeedImageInfoModel.X = 0;

                        _Images[1].FeedImageInfoModel.Y = 0;
                        _Images[1].FeedImageInfoModel.X = (PANEL_WIDTH / 2) + SEPERATOR;

                        _Images[1].FeedImageInfoModel.ReWidth = _Images[0].FeedImageInfoModel.ReWidth = (PANEL_WIDTH / 2);
                        _Images[1].FeedImageInfoModel.ReHeight = _Images[0].FeedImageInfoModel.ReHeight = (PANEL_HEIGHT / 2);

                        _Images[2].FeedImageInfoModel.Y = (PANEL_HEIGHT / 2) + SEPERATOR;
                        _Images[2].FeedImageInfoModel.X = 0;

                        _Images[2].FeedImageInfoModel.ReWidth = PANEL_WIDTH + SEPERATOR;
                        _Images[2].FeedImageInfoModel.ReHeight = (PANEL_HEIGHT / 2);

                        ImageViewHeight = PANEL_HEIGHT + SEPERATOR;
                    }
                    else if (random == 0) // left single
                    {
                        _Images[0].FeedImageInfoModel.Y = 0;
                        _Images[0].FeedImageInfoModel.X = 0;

                        _Images[0].FeedImageInfoModel.ReWidth = (PANEL_WIDTH / 2);
                        _Images[0].FeedImageInfoModel.ReHeight = PANEL_HEIGHT + SEPERATOR;

                        _Images[1].FeedImageInfoModel.Y = 0;
                        _Images[1].FeedImageInfoModel.X = (PANEL_WIDTH / 2) + SEPERATOR;

                        _Images[2].FeedImageInfoModel.Y = (PANEL_HEIGHT / 2) + SEPERATOR;
                        _Images[2].FeedImageInfoModel.X = (PANEL_WIDTH / 2) + SEPERATOR;

                        _Images[2].FeedImageInfoModel.ReWidth = _Images[1].FeedImageInfoModel.ReWidth = (PANEL_WIDTH / 2);
                        _Images[2].FeedImageInfoModel.ReHeight = _Images[1].FeedImageInfoModel.ReHeight = (PANEL_HEIGHT / 2);

                        ImageViewHeight = PANEL_HEIGHT + SEPERATOR;
                    }
                    else if (random == 2) // right single
                    {
                        _Images[0].FeedImageInfoModel.Y = 0;
                        _Images[0].FeedImageInfoModel.X = 0;

                        _Images[1].FeedImageInfoModel.Y = (PANEL_HEIGHT / 2) + SEPERATOR;
                        _Images[1].FeedImageInfoModel.X = 0;

                        _Images[1].FeedImageInfoModel.ReWidth = _Images[0].FeedImageInfoModel.ReWidth = (PANEL_WIDTH / 2);
                        _Images[1].FeedImageInfoModel.ReHeight = _Images[0].FeedImageInfoModel.ReHeight = (PANEL_HEIGHT / 2);

                        _Images[2].FeedImageInfoModel.Y = 0;
                        _Images[2].FeedImageInfoModel.X = (PANEL_WIDTH / 2) + SEPERATOR;

                        _Images[2].FeedImageInfoModel.ReWidth = (PANEL_WIDTH / 2);
                        _Images[2].FeedImageInfoModel.ReHeight = PANEL_HEIGHT + SEPERATOR;

                        ImageViewHeight = PANEL_HEIGHT + SEPERATOR;
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }

        public ScaleFeedImages(ObservableCollection<ImageModel> images, long _NewsFeedId, int PANEL_WIDTH, bool shareParent)
        {
            try
            {
                this.AddedTime = AddedTime;

                if (_Images.Count == 1)
                {
                    _Images[0].FeedImageInfoModel.Y = 0;
                    _Images[0].FeedImageInfoModel.Z = 0;

                    _Images[0].FeedImageInfoModel.ShareParentReWidth = (_Images[0].FeedImageInfoModel.ShareParentWidth >= PANEL_WIDTH) ? PANEL_WIDTH : _Images[0].FeedImageInfoModel.ShareParentWidth;

                    // _Images[0].FeedImageInfoModel.ReHeight = (_Images[0].FeedImageInfoModel.Height >= PANEL_HEIGHT) ? ((_Images[0].FeedImageInfoModel.Height * _Images[0].FeedImageInfoModel.ReWidth) / _Images[0].FeedImageInfoModel.Width) : _Images[0].FeedImageInfoModel.Height;

                    _Images[0].FeedImageInfoModel.ShareParentReHeight = (int)(((double)_Images[0].FeedImageInfoModel.ShareParentReWidth / (double)Images[0].FeedImageInfoModel.ShareParentWidth) * (double)_Images[0].FeedImageInfoModel.ShareParentHeight);

                    //_Images[0].FeedImageInfoModel.ReHeight = (_Images[0].FeedImageInfoModel.Height >= PANEL_HEIGHT) ? PANEL_HEIGHT : _Images[0].FeedImageInfoModel.Height;

                    if (_Images[0].FeedImageInfoModel.ShareParentReHeight > PANEL_WIDTH)
                    {
                        _Images[0].FeedImageInfoModel.ShareParentReHeight = PANEL_HEIGHT;
                        _Images[0].FeedImageInfoModel.ShareParentReWidth = (int)(((double)_Images[0].FeedImageInfoModel.ShareParentReHeight / (double)Images[0].FeedImageInfoModel.ShareParentHeight) * (double)Images[0].FeedImageInfoModel.ShareParentWidth);
                    }

                    ImageViewHeight = _Images[0].FeedImageInfoModel.ShareParentReHeight;
                }
                else if (_Images.Count == 2)
                {
                    _Images[0].FeedImageInfoModel.Y = 0;
                    _Images[0].FeedImageInfoModel.Z = 0;

                    _Images[1].FeedImageInfoModel.Y = 0;
                    _Images[1].FeedImageInfoModel.Z = (PANEL_WIDTH / 2) + SEPERATOR;

                    _Images[1].FeedImageInfoModel.ShareParentReWidth = _Images[0].FeedImageInfoModel.ShareParentReWidth = (PANEL_WIDTH / 2);

                    _Images[1].FeedImageInfoModel.ShareParentReHeight = _Images[0].FeedImageInfoModel.ShareParentReHeight = (PANEL_WIDTH / 2);

                    ImageViewHeight = 300;
                }
                else if (_Images.Count == 3)
                {
                    int random = (int)this.AddedTime % 4;

                    if (random == 1) // top single
                    {
                        _Images[0].FeedImageInfoModel.Y = 0;
                        _Images[0].FeedImageInfoModel.Z = 0;

                        _Images[0].FeedImageInfoModel.ShareParentReWidth = PANEL_WIDTH + SEPERATOR;
                        _Images[0].FeedImageInfoModel.ShareParentReHeight = PANEL_HEIGHT / 2;

                        _Images[1].FeedImageInfoModel.Y = (PANEL_HEIGHT / 2) + SEPERATOR;
                        _Images[1].FeedImageInfoModel.Z = 0;

                        _Images[2].FeedImageInfoModel.Y = (PANEL_HEIGHT / 2) + SEPERATOR;
                        _Images[2].FeedImageInfoModel.Z = (PANEL_WIDTH / 2) + SEPERATOR;

                        _Images[2].FeedImageInfoModel.ShareParentReWidth = _Images[1].FeedImageInfoModel.ShareParentReWidth = (PANEL_WIDTH / 2);
                        _Images[2].FeedImageInfoModel.ShareParentReHeight = _Images[1].FeedImageInfoModel.ShareParentReHeight = (PANEL_HEIGHT / 2);

                        ImageViewHeight = PANEL_HEIGHT + SEPERATOR;

                    }
                    else if (random == 3) // bot single
                    {
                        _Images[0].FeedImageInfoModel.Y = 0;
                        _Images[0].FeedImageInfoModel.Z = 0;

                        _Images[1].FeedImageInfoModel.Y = 0;
                        _Images[1].FeedImageInfoModel.Z = (PANEL_WIDTH / 2) + SEPERATOR;

                        _Images[1].FeedImageInfoModel.ShareParentReWidth = _Images[0].FeedImageInfoModel.ShareParentReWidth = (PANEL_WIDTH / 2);
                        _Images[1].FeedImageInfoModel.ShareParentReHeight = _Images[0].FeedImageInfoModel.ShareParentReHeight = (PANEL_HEIGHT / 2);

                        _Images[2].FeedImageInfoModel.Y = (PANEL_HEIGHT / 2) + SEPERATOR;
                        _Images[2].FeedImageInfoModel.Z = 0;

                        _Images[2].FeedImageInfoModel.ShareParentReWidth = PANEL_WIDTH + SEPERATOR;
                        _Images[2].FeedImageInfoModel.ShareParentReHeight = (PANEL_HEIGHT / 2);

                        ImageViewHeight = PANEL_HEIGHT + SEPERATOR;
                    }
                    else if (random == 0) // left single
                    {
                        _Images[0].FeedImageInfoModel.Y = 0;
                        _Images[0].FeedImageInfoModel.Z = 0;

                        _Images[0].FeedImageInfoModel.ShareParentReWidth = (PANEL_WIDTH / 2);
                        _Images[0].FeedImageInfoModel.ShareParentReHeight = PANEL_HEIGHT + SEPERATOR;

                        _Images[1].FeedImageInfoModel.Y = 0;
                        _Images[1].FeedImageInfoModel.Z = (PANEL_WIDTH / 2) + SEPERATOR;

                        _Images[2].FeedImageInfoModel.Y = (PANEL_HEIGHT / 2) + SEPERATOR;
                        _Images[2].FeedImageInfoModel.Z = (PANEL_WIDTH / 2) + SEPERATOR;

                        _Images[2].FeedImageInfoModel.ShareParentReWidth = _Images[1].FeedImageInfoModel.ShareParentReWidth = (PANEL_WIDTH / 2);
                        _Images[2].FeedImageInfoModel.ShareParentReHeight = _Images[1].FeedImageInfoModel.ShareParentReHeight = (PANEL_HEIGHT / 2);

                        ImageViewHeight = PANEL_HEIGHT + SEPERATOR;
                    }
                    else if (random == 2) // right single
                    {
                        _Images[0].FeedImageInfoModel.Y = 0;
                        _Images[0].FeedImageInfoModel.Z = 0;

                        _Images[1].FeedImageInfoModel.Y = (PANEL_HEIGHT / 2) + SEPERATOR;
                        _Images[1].FeedImageInfoModel.Z = 0;

                        _Images[1].FeedImageInfoModel.ShareParentReWidth = _Images[0].FeedImageInfoModel.ShareParentReWidth = (PANEL_WIDTH / 2);
                        _Images[1].FeedImageInfoModel.ShareParentReHeight = _Images[0].FeedImageInfoModel.ShareParentReHeight = (PANEL_HEIGHT / 2);

                        _Images[2].FeedImageInfoModel.Y = 0;
                        _Images[2].FeedImageInfoModel.Z = (PANEL_WIDTH / 2) + SEPERATOR;

                        _Images[2].FeedImageInfoModel.ShareParentReWidth = (PANEL_WIDTH / 2);
                        _Images[2].FeedImageInfoModel.ShareParentReHeight = PANEL_HEIGHT + SEPERATOR;

                        ImageViewHeight = PANEL_HEIGHT + SEPERATOR;
                    }

                }
            }
            catch (Exception) { }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
namespace View.Utility.Feed
{
    public class ThradDeleteFeed
    {
        #region Private Fields
        
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradDeleteFeed).Name);
        private bool running = false;
        private Guid NfId;

        #endregion 

        #region Private methods

        private void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_DELETE_STATUS;
                pakToSend[JsonKeys.NewsfeedId] = NfId;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (feedbackfields != null)
                {
                    try
                    {
                        if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                        {
                            MainSwitcher.AuthSignalHandler().feedSignalHandler.DeleteFeedActions(NfId);
                        }
                        else if (feedbackfields[JsonKeys.Message] != null)
                        {
                            string msg = (string)feedbackfields[JsonKeys.Message];
                           UIHelperMethods.ShowErrorMessageBoxFromThread(msg, "Failed!");
                        }
                    }
                    finally { }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Message : " + ex.Message + "StackTrace : " + ex.StackTrace);
            }
            finally { running = false; }
        }

        #endregion

        #region Public Methods

        public void StartThread(Guid NfId)
        {
            if (!running)
            {
                this.NfId = NfId;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }
        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion
    }
}

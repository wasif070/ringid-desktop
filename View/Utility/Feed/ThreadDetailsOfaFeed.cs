﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using System;
using System.Threading;

namespace View.Utility.Feed
{
    public class ThreadDetailsOfaFeed
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ThreadDetailsOfaFeed).Name);
        private Guid nfId;
        private int waitingTime = 0, editId;
        public delegate void CallBack(JObject feedJObject);
        public event CallBack CallBackEvent;

        public void StartThread(Guid nfId, int editId = 0)
        {
            this.nfId = nfId;
            this.editId = editId;
            new Thread(new ThreadStart(Run)).Start();
        }

        public void Run()
        {
            if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                try
                {
                    JObject pakToSend = new JObject();
                    string pakId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = pakId;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.NewsfeedId] = nfId;

                    if (editId > 0)
                    {
                        pakToSend[JsonKeys.Id] = editId;
                        pakToSend[JsonKeys.Action] = AppConstants.TYPE_NEWSFEED_EDIT_HISTORY_DETAILS;
                    }
                    else
                    {
                        pakToSend[JsonKeys.Action] = AppConstants.TYPE_SINGLE_STATUS_NOTIFICATION;
                    }

                    JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakId);
                    if (feedbackfields != null)
                    {
                        if (feedbackfields[JsonKeys.Success] != null && !(bool)feedbackfields[JsonKeys.Success])
                        {
                        }
                        else
                        {
                            while (feedbackfields[JsonKeys.NewsFeedList] == null)
                            {
                                //Console.WriteLine("*************************=>" + waitingTime);
                                if (waitingTime == DefaultSettings.TRYING_TIME)
                                {
                                    break;
                                }
                                Thread.Sleep(DefaultSettings.WAITING_TIME);
                                RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out feedbackfields);
                                waitingTime++;
                                if (feedbackfields[JsonKeys.Success] != null && !(bool)feedbackfields[JsonKeys.Success])
                                {
                                    if (CallBackEvent != null)
                                        CallBackEvent(null);
                                    CallBackEvent = null;
                                    return;
                                }
                            }
                            JArray jarray = (JArray)feedbackfields[JsonKeys.NewsFeedList];
                            if (jarray != null && jarray.Count > 0)
                                foreach (JObject singleObj in jarray)
                                {
                                    //FeedDTO feed = HelperMethodsModel.BindFeedDetails(singleObj);
                                    //lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
                                    //{
                                    //    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[feed.NewsfeedId] = feed;
                                    //}
                                    //lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME)
                                    //{
                                    //    NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.InsertDataFromServer(feed.NewsfeedId, feed.Time, feed.FeedCategory);
                                    //}
                                    if (CallBackEvent != null)
                                        CallBackEvent(singleObj);
                                    CallBackEvent = null;
                                    return;
                                }
                        }
                    }
                    else
                    {
                        MainSwitcher.ThreadManager().PingNow();
                    }
                }
                catch (Exception e)
                {
                    log.Error("ThreadDetailsOfAFeed ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            }
            else
            {
                log.Error("ThreadDetailsOfAFeed Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            }
            if (CallBackEvent != null)
                CallBackEvent(null);
            CallBackEvent = null;
        }
    }
}

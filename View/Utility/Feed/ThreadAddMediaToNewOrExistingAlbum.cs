﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.ObjectModel;
using View.BindingModels;
using View.Utility.DataContainer;
using View.ViewModel;

namespace View.Utility.Feed
{
    class ThreadAddMediaToNewOrExistingAlbum
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThreadAddMediaToNewOrExistingAlbum).Name);
        private SingleMediaModel mediaModel;
        private MediaContentModel albumModel;
        private string NewAlbmNm;
        public delegate void OnCallBack(Guid albmId);
        public event OnCallBack CallBack;

        private bool running = false;
        #endregion "Private Fields"

        #region "Constructors"
        public ThreadAddMediaToNewOrExistingAlbum(MediaContentModel albumModel, SingleMediaModel mediaModel, string NewAlbmNm = null)
        {
            this.mediaModel = mediaModel;
            this.albumModel = albumModel;
            this.NewAlbmNm = NewAlbmNm;
        }

        #endregion "Constructors"

        #region "Private methods"
        public void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                String packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_MEDIA_CONTENT;
                pakToSend[JsonKeys.AlbumId] = mediaModel.AlbumId;
                pakToSend[JsonKeys.OwnerId] = mediaModel.MediaOwner.UserTableID;

                //JArray mediasToSend = new JArray();
                JObject obj = new JObject();
                obj[JsonKeys.StreamUrl] = mediaModel.StreamUrl;
                obj[JsonKeys.Title] = mediaModel.Title;
                obj[JsonKeys.MediaDuration] = mediaModel.Duration;
                obj[JsonKeys.ContentId] = mediaModel.ContentId;
                if (!string.IsNullOrEmpty(mediaModel.ThumbUrl))
                {
                    obj[JsonKeys.ThumbUrl] = mediaModel.ThumbUrl;
                    obj[JsonKeys.ThumbImageWidth] = mediaModel.ThumbImageWidth;
                    obj[JsonKeys.ThumbImageHeight] = mediaModel.ThumbImageHeight;
                }
                obj[JsonKeys.MediaType] = mediaModel.MediaType;
                //obj[JsonKeys.UserTableID] = DefaultSettings.LOGIN_TABLE_ID;
                if (NewAlbmNm != null)
                    obj[JsonKeys.AlbumName] = NewAlbmNm;
                else
                {
                    obj[JsonKeys.AlbumId] = albumModel.AlbumId;
                }
                if (!string.IsNullOrEmpty(mediaModel.Artist)) obj[JsonKeys.Artist] = mediaModel.Artist;
                //mediasToSend.Add(obj);
                pakToSend[JsonKeys.MediaContentDTO] = obj;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (CallBack != null)
                {
                    CallBack((albumModel != null) ? albumModel.AlbumId : Guid.Empty);
                    CallBack = null;
                }
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success] && feedbackfields[JsonKeys.ContentId] != null
                                && feedbackfields[JsonKeys.AlbumId] != null)
                    {
                        Guid contentId = (Guid)feedbackfields[JsonKeys.ContentId];
                        Guid albumId = (Guid)feedbackfields[JsonKeys.AlbumId];
                        if (NewAlbmNm != null)
                        {
                            albumModel = new MediaContentModel { AlbumId = albumId, AlbumName = NewAlbmNm, MediaType = mediaModel.MediaType };
                            if (albumModel.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO)
                            {
                                RingIDViewModel.Instance.MyAudioAlbums.InvokeAdd(albumModel);
                            }
                            else if (albumModel.MediaType == SettingsConstants.MEDIA_TYPE_VIDEO)
                            {
                                RingIDViewModel.Instance.MyVideoAlbums.InvokeAdd(albumModel);
                            }
                        }
                        SingleMediaModel singleMediaModel = null;
                        if (!MediaDataContainer.Instance.ContentModels.TryGetValue(contentId, out singleMediaModel))
                        {
                            singleMediaModel = new SingleMediaModel { ContentId = contentId };
                            MediaDataContainer.Instance.ContentModels[contentId] = singleMediaModel;
                            singleMediaModel.AlbumId = albumId;
                            singleMediaModel.AlbumName = albumModel.AlbumName;
                        }
                        //singleMediaModel.AlbumId = albumId;
                        //singleMediaModel.AlbumName = albumModel.AlbumName;
                        singleMediaModel.MediaOwner = mediaModel.MediaOwner; //RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel;
                        singleMediaModel.Artist = mediaModel.Artist;
                        singleMediaModel.MediaType = mediaModel.MediaType;
                        singleMediaModel.ThumbImageHeight = mediaModel.ThumbImageHeight;
                        singleMediaModel.ThumbImageWidth = mediaModel.ThumbImageWidth;
                        singleMediaModel.StreamUrl = mediaModel.StreamUrl;
                        singleMediaModel.ThumbUrl = mediaModel.ThumbUrl;
                        singleMediaModel.Duration = mediaModel.Duration;
                        singleMediaModel.Title = mediaModel.Title;

                        //if (albumModel.MediaList == null) albumModel.MediaList = new ObservableCollection<SingleMediaModel>();
                        //albumModel.MediaList.InvokeAdd(singleMediaModel);
                        if (albumModel.MediaList != null) albumModel.MediaList.InvokeAdd(singleMediaModel);
                        albumModel.TotalMediaCount += 1;
                        UIHelperMethods.ShowTimerMessageBox(NotificationMessages.ADDING_MEDIA_CONFIRMATION, NotificationMessages.SUCCESS_CONFIRMATION_MESSAGE_TITLE);
                    }
                    else
                    {
                        int errorCode = (feedbackfields[JsonKeys.ReasonCode] != null) ? (int)feedbackfields[JsonKeys.ReasonCode] : 0;
                        if (errorCode == ReasonCodeConstants.REASON_CODE_ALREADY_SAVED)
                        {
                            UIHelperMethods.ShowTimerMessageBox(NotificationMessages.MEDIA_ALREADY_EXIST_IN_ALBUM, NotificationMessages.MEDIA_ALREADY_EXIST_IN_ALBUM_TITLE);
                        }
                        else if (errorCode == ReasonCodeConstants.REASON_CODE_INVALID_MEDIA_PRIVACY)
                        {
                            UIHelperMethods.ShowErrorMessageBoxFromThread(NotificationMessages.PUBLIC_MEDIA_ADD_MESSAGE, NotificationMessages.FAILED_CONFIRMATION_MESSAGE_TITLE);
                        }
                        else
                        {
                            string ms = (feedbackfields[JsonKeys.Message] != null) ? (string)feedbackfields[JsonKeys.Message] : NotificationMessages.ADD_TO_MEDIA_ALBUM_FAILED_MESSAGE;
                            UIHelperMethods.ShowTimerMessageBox(ms, NotificationMessages.FAILED_CONFIRMATION_MESSAGE_TITLE);
                        }
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                    //if (albumModel != null)
                    //    MainSwitcher.AuthSignalHandler().feedSignalHandler.EnableAddtoExistingOrNewAlbumPopup(albumModel.AlbumId);
                    //else MainSwitcher.AuthSignalHandler().feedSignalHandler.EnableAddtoExistingOrNewAlbumPopup(Guid.Empty);
                }
                else
                {
                    //if (albumModel != null)
                    //    MainSwitcher.AuthSignalHandler().feedSignalHandler.EnableAddtoExistingOrNewAlbumPopup(albumModel.AlbumId);
                    //else MainSwitcher.AuthSignalHandler().feedSignalHandler.EnableAddtoExistingOrNewAlbumPopup(Guid.Empty);
                    string msg = (DefaultSettings.IsInternetAvailable) ? "" : ReasonCodeConstants.REASON_MSG_INTERNET_UNAVAILABLE;
                    UIHelperMethods.ShowErrorMessageBoxFromThread(NotificationMessages.ADD_TO_MEDIA_ALBUM_FAILED_MESSAGE + msg, NotificationMessages.FAILED_CONFIRMATION_MESSAGE_TITLE);
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace);
            }
            if (CallBack != null)
            {
                CallBack((albumModel != null) ? albumModel.AlbumId : Guid.Empty);
                CallBack = null;
            }
            running = false;
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

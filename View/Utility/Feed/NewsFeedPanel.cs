﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using View.BindingModels;

namespace View.Utility.Feed
{
    public class NewsFeedPanel : WrapPanel
    {
        private double _MaxColumnHeight = 0;
        private double _MinColumnHeight = 0;

        public static int ALL_FEED { get { return 1; } }
        public static int MY_FEED { get { return 2; } }
        public static int FRIEND_FEED { get { return 3; } }
        public static int SHARE_FEED { get { return 4; } }
        public static int CIRCLE_FEED { get { return 5; } }
        public static int MEDIA_FEED { get { return 6; } }
        public static int NEWSPORTAL_FEED { get { return 7; } }
        public static int NEWSPORTAL_PROFILE_FEED { get { return 8; } }
        public static int NEWSPORTAL_SAVED_FEED { get { return 9; } }
        public static int DETAILS_FEED { get { return 10; } }
        public static int PAGE_FEED { get { return 11; } }
        public static int PAGE_PROFILE_FEED { get { return 12; } }
        public static int PAGE_SAVED_FEED { get { return 13; } }
        public static int CELEBRITY_PROFILE_FEED { get { return 14; } }
        public static int MEDIAPAGE_PROFILE_FEED { get { return 15; } }

        public static int SINGLE_FEED_PANEL_WIDTH { get { return 600; } }
        public static int SHARE_FEED_PANEL_WIDTH { get { return 580; } }

        public double DefaultWidth
        {
            get { return (double)GetValue(DefaultWidthProperty); }
            set { SetValue(DefaultWidthProperty, value); }
        }

        public double HGap
        {
            get { return (double)GetValue(HGapProperty); }
            set { SetValue(HGapProperty, value); }
        }

        public double VGap
        {
            get { return (double)GetValue(VGapProperty); }
            set { SetValue(VGapProperty, value); }
        }

        public int MaxColumn
        {
            get { return (int)GetValue(MaxColumnProperty); }
            set { SetValue(MaxColumnProperty, value); }
        }

        public int NumberOfColumn
        {
            get { return (int)GetValue(NumberOfColumnProperty); }
            set { SetValue(NumberOfColumnProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DefaultWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DefaultWidthProperty =
            DependencyProperty.Register("DefaultWidth", typeof(double), typeof(NewsFeedPanel), new UIPropertyMetadata(600.0, (s, e) => { }));

        // Using a DependencyProperty as the backing store for HGap.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HGapProperty =
            DependencyProperty.Register("HGap", typeof(double), typeof(NewsFeedPanel), new UIPropertyMetadata(0.0, (s, e) => { }));

        // Using a DependencyProperty as the backing store for VGap.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty VGapProperty =
            DependencyProperty.Register("VGap", typeof(double), typeof(NewsFeedPanel), new UIPropertyMetadata(0.0, (s, e) => { }));

        // Using a DependencyProperty as the backing store for MaxColumn.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaxColumnProperty =
            DependencyProperty.Register("MaxColumn", typeof(int), typeof(NewsFeedPanel), new UIPropertyMetadata(2, (s, e) =>
            {
                NewsFeedPanel panel = (NewsFeedPanel)s;
                if (panel != null)
                {
                    panel.Measure(panel.RenderSize);
                    //panel.UpdateLayout();
                }
            }));

        // Using a DependencyProperty as the backing store for MaxColumn.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NumberOfColumnProperty =
            DependencyProperty.Register("NumberOfColumn", typeof(int), typeof(NewsFeedPanel), new UIPropertyMetadata(0, (s, e) => { }));

        public ScrollViewer GetAncestorScrollViewer(UIElement element)
        {
            if (element == null)
            {
                return null;
            }

            UIElement parent = VisualTreeHelper.GetParent(element) as UIElement;
            while (parent != null && !(parent is ScrollViewer))
            {
                parent = VisualTreeHelper.GetParent(parent) as UIElement;
            }
            return parent is ScrollViewer ? parent as ScrollViewer : null;
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            ScrollViewer scrollViewer = GetAncestorScrollViewer(this);
            bool isScrollVisible = !(scrollViewer == null || scrollViewer.ComputedVerticalScrollBarVisibility == Visibility.Collapsed);

            double width = double.IsPositiveInfinity(availableSize.Width) ? 0 : availableSize.Width;
            width = width - Margin.Left - Margin.Right + (isScrollVisible ? 18 : 0);
            width = Math.Max(width, DefaultWidth + HGap + HGap);
            int estimatedNumberOfColumn = (int)(width - HGap) / (int)(DefaultWidth + HGap);

            NumberOfColumn = estimatedNumberOfColumn > 3 ? 3 : estimatedNumberOfColumn;

            int columnCount = estimatedNumberOfColumn > MaxColumn ? (int)MaxColumn : estimatedNumberOfColumn;
            columnCount = columnCount > 0 ? columnCount : 1;
            double[] columnHeightArray = new double[columnCount];

            UIElementCollection children = InternalChildren;
            int count = children.Count;

            for (int i = 0; i < count; i++)
            {
                UIElement child = children[i];
                if (child == null)
                    continue;

                int idx = GetMinColumnIndex(columnHeightArray);
                _MinColumnHeight = columnHeightArray[idx];

                Size childConstraint = new Size(0, 0);
                childConstraint.Width = Math.Max(0.0, DefaultWidth);
                childConstraint.Height = Math.Max(0.0, availableSize.Height - _MinColumnHeight);
                child.Measure(childConstraint);

                columnHeightArray[idx] = _MinColumnHeight + VGap + child.DesiredSize.Height;
            }

            int maxIdx = GetMaxColumnIndex(columnHeightArray);
            int minIdx = GetMinColumnIndex(columnHeightArray);

            _MaxColumnHeight = columnHeightArray[maxIdx] + VGap + Margin.Top + Margin.Bottom;
            _MinColumnHeight = columnHeightArray[minIdx] + VGap + Margin.Top + Margin.Bottom;
            availableSize.Height = _MaxColumnHeight;

            return (new Size(width, availableSize.Height));
        }

        protected override Size ArrangeOverride(Size totalAvailableSize)
        {
            double width = totalAvailableSize.Width;
            width = width - Margin.Left - Margin.Right;
            int estimatedNumberOfColumn = (int)(width - HGap) / (int)(DefaultWidth + HGap);

            NumberOfColumn = estimatedNumberOfColumn > 3 ? 3 : estimatedNumberOfColumn;

            int columnCount = estimatedNumberOfColumn > MaxColumn ? (int)MaxColumn : estimatedNumberOfColumn;
            columnCount = columnCount > 0 ? columnCount : 1;
            double[] columnHeightArray = new double[columnCount];

            for (int i = 0; i < columnHeightArray.Length; i++)
            {
                columnHeightArray[i] += Margin.Top;
            }

            double totalHgap = HGap * (columnHeightArray.Length + 1);
            double totalComponentWidth = DefaultWidth * columnHeightArray.Length;
            double totalWidth = totalHgap + totalComponentWidth;
            double emptySpace = (width - totalWidth) / 2;

            UIElementCollection children = InternalChildren;
            int count = children.Count;

            for (int i = 0; i < count; i++)
            {
                UIElement child = children[i];
                Rect finalRect = new Rect(0.0, 0.0, 0.0, 0.0);

                if (child == null)
                {
                    continue;
                }

                if (child.Visibility != Visibility.Collapsed)
                {
                    int idx = GetMinColumnIndex(columnHeightArray);
                    _MinColumnHeight = columnHeightArray[idx];
                    columnHeightArray[idx] = _MinColumnHeight + VGap + child.DesiredSize.Height;

                    finalRect.X = Margin.Left + emptySpace + (idx * (DefaultWidth + HGap)) + HGap;
                    finalRect.Y = _MinColumnHeight + VGap;
                    finalRect.Width = child.DesiredSize.Width;
                    finalRect.Height = child.DesiredSize.Height;
                }

                child.Arrange(finalRect);
            }

            return totalAvailableSize;
        }

        private int GetMinColumnIndex(double[] columnHeight)
        {
            int minIndex = 0;
            double minHeight = columnHeight[0];
            int idx = 1;

            while (idx < columnHeight.Length)
            {
                if (columnHeight[idx] < minHeight)
                {
                    minHeight = columnHeight[idx];
                    minIndex = idx;
                }
                idx++;
            }

            return minIndex;
        }

        private int GetMaxColumnIndex(double[] columnHeight)
        {
            int maxIndex = 0;
            double maxHeight = 0;
            int idx = 0;

            while (idx < columnHeight.Length)
            {
                if (columnHeight[idx] > maxHeight)
                {
                    maxHeight = columnHeight[idx];
                    maxIndex = idx;
                }
                idx++;
            }

            return maxIndex;
        }

    }
}


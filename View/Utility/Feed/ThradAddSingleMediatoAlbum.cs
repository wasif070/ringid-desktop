﻿using System;
using System.Collections.Generic;
using System.Linq;
using Auth.utility;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.ViewModel;
using System.Collections.ObjectModel;
using View.Utility.DataContainer;

namespace View.Utility.Feed
{
    class ThradAddSingleMediatoAlbum
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradAddSingleMediatoAlbum).Name);
        private bool running = false;
        private int mediaType, th, tw, mpvc;
        private string ttl, artist, streamUrl, thumbUrl;
        private long drtn;
        private MediaContentModel albumModel;
        #endregion "Private Fields"

        #region "Constructors"
        public ThradAddSingleMediatoAlbum(MediaContentModel albumModel, int mediaType, string streamUrl, string thumbUrl, long drtn, string ttl, string artist, int tw, int th, int mpvc)
        {
            this.mpvc = mpvc;
            this.mediaType = mediaType;
            this.albumModel = albumModel;
            this.streamUrl = streamUrl;
            this.thumbUrl = thumbUrl;
            this.drtn = drtn;
            this.ttl = ttl;
            this.artist = artist;
            this.tw = tw;
            this.th = th;
        }

        #endregion "Constructors"

        #region "Private methods"
        public void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                String packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_MEDIA_CONTENT;
                pakToSend[JsonKeys.AlbumId] = albumModel.AlbumId;
                pakToSend[JsonKeys.MediaType] = mediaType;
                JArray mediasToSend = new JArray();
                JObject obj = new JObject();
                obj[JsonKeys.StreamUrl] = streamUrl;
                obj[JsonKeys.Title] = ttl;
                obj[JsonKeys.MediaDuration] = drtn;
                if (mpvc > 0) obj[JsonKeys.MediaPrivacy] = mpvc;
                if (!string.IsNullOrEmpty(thumbUrl))
                {
                    obj[JsonKeys.ThumbUrl] = thumbUrl;
                    obj[JsonKeys.ThumbImageWidth] = tw;
                    obj[JsonKeys.ThumbImageHeight] = th;
                }
                if (!string.IsNullOrEmpty(artist)) obj[JsonKeys.Artist] = artist;
                mediasToSend.Add(obj);
                pakToSend[JsonKeys.MediaList] = mediasToSend;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success] && feedbackfields[JsonKeys.MediaIds] != null
                                && feedbackfields[JsonKeys.AlbumId] != null && (Guid)feedbackfields[JsonKeys.AlbumId] == albumModel.AlbumId)
                    {
                        JArray array = (JArray)feedbackfields[JsonKeys.MediaIds];
                        Guid contentId = (Guid)array.ElementAt(0);
                        SingleMediaModel singleMediaModel = new SingleMediaModel { ContentId = contentId };
                        MediaDataContainer.Instance.ContentModels[contentId] = singleMediaModel;
                        singleMediaModel.AlbumId = albumModel.AlbumId;
                        singleMediaModel.Artist = artist;
                        singleMediaModel.MediaType = mediaType;
                        singleMediaModel.ThumbImageHeight = th;
                        singleMediaModel.ThumbImageWidth = tw;
                        singleMediaModel.Privacy = mpvc;
                        singleMediaModel.StreamUrl = streamUrl;
                        singleMediaModel.ThumbUrl = thumbUrl;
                        singleMediaModel.Duration = drtn;
                        singleMediaModel.Title = ttl;

                        if (albumModel.MediaList == null) albumModel.MediaList = new ObservableCollection<SingleMediaModel>();
                        albumModel.MediaList.InvokeAdd(singleMediaModel);
                        albumModel.TotalMediaCount += 1;
                        UIHelperMethods.ShowTimerMessageBox(NotificationMessages.ADDING_MEDIA_CONFIRMATION, "Success!");
                    }
                    else
                    {
                        string ms = (feedbackfields[JsonKeys.Message] != null) ? (string)feedbackfields[JsonKeys.Message] : "Failed to Add Media to the Album!";
                        UIHelperMethods.ShowTimerMessageBox(ms, "Failed!");
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                    //MainSwitcher.AuthSignalHandler().feedSignalHandler.EnableAddtoExistingOrNewAlbumPopup(albumModel.AlbumId);
                }
                else
                {
                    //MainSwitcher.AuthSignalHandler().feedSignalHandler.EnableAddtoExistingOrNewAlbumPopup(albumModel.AlbumId);
                    string msg = (DefaultSettings.IsInternetAvailable) ? "" : ReasonCodeConstants.REASON_MSG_INTERNET_UNAVAILABLE;
                   UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to Add Media to the Album!" + msg, "Failed!");
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

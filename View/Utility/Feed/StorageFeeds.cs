﻿using Auth.utility.Feed;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System.Linq;
using System.Windows;
using View.BindingModels;
using View.Constants;
using View.UI;
using View.UI.Feed;
using View.Utility.DataContainer;
using View.ViewModel;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Threading;

namespace View.Utility.Feed
{
    #region Comparer Class

    /// <summary>
    /// This class implements IComaparer, used to write custom sort comparer
    /// </summary>
    /// <typeparam name="T"></typeparam>
    class CustomComparer<T> : IComparer<T>
    {
        public int Compare(T x, T y)
        {
            return Comparer<T>.Default.Compare(y, x);
        }
    }

    #endregion

    #region Storage Feeds class

    /// <summary>
    /// Use to store feeds locally
    /// </summary>
    public static class StorageFeeds
    {

        #region Private Fields

        private const int NUMBER_OF_SAVED_FEEDS = 20;
        private static readonly ILog log = LogManager.GetLogger(typeof(StorageFeeds).Name);
        private static SortedList<long, JObject> sortedFeedJObjList = new SortedList<long, JObject>(new CustomComparer<long>());

        #endregion

        #region Private Methods

        private static void fileSave()
        {
            Object fileSavingLock = new Object();
            try
            {
                lock (fileSavingLock)
                {
                    string localFeedSaveFilepath = Path.Combine(RingIDSettings.APP_FOLDER, DefaultSettings.LOGIN_RING_ID + ".fds");
                    if (File.Exists(localFeedSaveFilepath))
                    {
                        File.Delete(localFeedSaveFilepath);
                    }
                    List<JObject> feedJObjList = new List<JObject>();
                    feedJObjList = sortedFeedJObjList.Values.ToList();
                    string serializeFeedJsonDataString = JsonConvert.SerializeObject(feedJObjList, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    byte[] byteFormofSerializeFeedJsonDataString = Encoding.UTF8.GetBytes(serializeFeedJsonDataString);

                    for (int i = 0; i < byteFormofSerializeFeedJsonDataString.Length; i++)
                    {
                        byteFormofSerializeFeedJsonDataString[i] ^= 255;
                    }

                    File.WriteAllBytes(localFeedSaveFilepath, byteFormofSerializeFeedJsonDataString);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
        }

        private static void initialDataLoad()
        {
            foreach (var item in FeedDataContainer.Instance.StorageFeedJObjects.Values)
            {
                if (sortedFeedJObjList.Count < NUMBER_OF_SAVED_FEEDS)
                {
                    long currentFeedTime = (long)item[JsonKeys.Time];
                    if (!sortedFeedJObjList.ContainsKey(currentFeedTime))
                    {
                        sortedFeedJObjList.Add(currentFeedTime, item);
                    }
                    continue;
                }
                break;
            }
        }

        private static void SetValueForFeedValidityPopUp()
        {
            Application.Current.Dispatcher.Invoke((Action)(() =>
            {
                if (UCAllFeeds.Instance != null && UCAllFeeds.Instance.newStatusView != null)
                    UCAllFeeds.Instance.newStatusViewModel.SetSelectedModelValuesForValidityPopup();
                if (UCMiddlePanelSwitcher.View_UCMyProfile != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyNewsFeeds != null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyNewsFeeds.NewStatusView != null &&
                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyNewsFeeds.NewStatusView.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyNewsFeeds.NewStatusViewModel.SetSelectedModelValuesForValidityPopup();
                }
                if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendNewsFeeds != null && UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendNewsFeeds.NewStatusView != null &&
                    UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendNewsFeeds.NewStatusView.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendNewsFeeds.NewStatusViewModel.SetSelectedModelValuesForValidityPopup();
                }
                if (UCMiddlePanelSwitcher.View_UCCirclePanel != null && UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds != null && UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.NewStatusView != null &&
                    UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.NewStatusView.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.NewStatusViewModel.SetSelectedModelValuesForValidityPopup();
                }
                if (RingIDViewModel.Instance._WNRingIDSettings != null && RingIDViewModel.Instance._WNRingIDSettings._UCGeneralSettings != null)
                {
                    RingIDViewModel.Instance._WNRingIDSettings._UCGeneralSettings.SetSelectedModelValuesForFeedValidityPopup();
                }
            }));
        }

        #endregion

        #region Public Methods

        public static void SaveLastTwentyFeeds()
        {
            try
            {
                if (DefaultSettings.LOGIN_RING_ID > 0)
                {
                    initialDataLoad();
                    fileSave();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
        }

        public static void UpdateLocallySavedFeeds(JObject newFeed)
        {
            try
            {
                initialDataLoad();
                long newFeedTime = (long)newFeed[JsonKeys.Time];
                if (sortedFeedJObjList.Count < NUMBER_OF_SAVED_FEEDS)
                {
                    sortedFeedJObjList.Add(newFeedTime, newFeed);
                    fileSave();
                }
                else
                {
                    long maximumKey = sortedFeedJObjList.Keys[0];

                    if (newFeedTime > maximumKey)
                    {
                        sortedFeedJObjList.RemoveAt(NUMBER_OF_SAVED_FEEDS - 1);
                        sortedFeedJObjList.Add(newFeedTime, newFeed);
                        fileSave();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
        }

        public static void RetriveLastTwentyFeeds()
        {
            try
            {
                if (DefaultSettings.LOGIN_RING_ID > 0)
                {
                    string fileToDeleteFd = Path.Combine(RingIDSettings.APP_FOLDER, DefaultSettings.LOGIN_RING_ID + ".fd");
                    string fileToDeleteTxt = Path.Combine(RingIDSettings.APP_FOLDER, DefaultSettings.LOGIN_RING_ID + ".txt");
                    if (File.Exists(fileToDeleteFd))
                    {
                        File.Delete(fileToDeleteFd);
                    }
                    if (File.Exists(fileToDeleteTxt))
                    {
                        File.Delete(fileToDeleteTxt);
                    }
                    string path = Path.Combine(RingIDSettings.APP_FOLDER, DefaultSettings.LOGIN_RING_ID + ".fds");
                    if (File.Exists(path))
                    {
                        List<JObject> feedJObjList = new List<JObject>();
                        var byteFormOfFeedJsonString = File.ReadAllBytes(path);
                        for (int i = 0; i < byteFormOfFeedJsonString.Length; i++)
                        {
                            byteFormOfFeedJsonString[i] ^= 255;
                        }
                        string serializeFeedJsonDataString = Encoding.UTF8.GetString(byteFormOfFeedJsonString);
                        feedJObjList = JsonConvert.DeserializeObject<List<JObject>>(serializeFeedJsonDataString);
                        if (feedJObjList != null && feedJObjList.Count > 0)
                        {
                            foreach (JObject jObj in feedJObjList)
                            {
                                //System.Threading.Thread.Sleep(10);
                                Guid nfId = (jObj[JsonKeys.NewsfeedId] != null) ? (Guid)jObj[JsonKeys.NewsfeedId] : Guid.Empty;
                                FeedModel feedModel = null;
                                if (!FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out feedModel))
                                {
                                    feedModel = new FeedModel();
                                    FeedDataContainer.Instance.FeedModels[nfId] = feedModel;
                                }
                                feedModel.LoadData(jObj);
                                FeedDataContainer.Instance.StorageFeedJObjects[feedModel.NewsfeedId] = jObj;
                                feedModel.FeedType = -1;
                                FeedHolderModel holder = new FeedHolderModel(-1);
                                holder.FeedId = nfId;
                                holder.Feed = feedModel;
                                holder.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                holder.SelectViewTemplate();
                                if (NewsFeedViewModel.Instance.AllFeedsViewCollection != null)
                                {
                                    if (NewsFeedViewModel.Instance.AllFeedsViewCollection.Count >= 20)
                                    {
                                        break;
                                    }
                                    NewsFeedViewModel.Instance.CalculateandInsertAllFeeds(holder);
                                }
                            }
                        }
                        SetValueForFeedValidityPopUp();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
        }

        #endregion
    }

    #endregion
}

﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using View.BindingModels;
using View.UI;

namespace View.Utility.Feed
{
    public class CustomObservableCollection : ObservableCollection<FeedModel>
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CustomObservableCollection).Name);
        private object LockObj = new object();
        public int MinIndex = 0;
        public bool isUpdateTime = true;
        public int Type;
        public FeedModel LoadMoreModel;
        public long UserProfileUtId = 0;//for only profiles
        public long pId = 0;//for only page type of profiles

        public CustomObservableCollection(int type, bool isNewStatus = false)
        {
            this.Type = type;
            if (isNewStatus)
            {
                this.MinIndex = 1;
                this.Add(new FeedModel { FeedType = 0 }); //NewStatus
            }
            else
            {
                this.MinIndex = 0;
            }
            LoadMoreModel = new FeedModel { FeedType = 1 };
            this.Add(LoadMoreModel); //Load more
        }

        public void InsertModel(FeedModel model)
        {
            try
            {
                int min = MinIndex;
                int pivot;
                int max = this.Count - 1;
                while (max > min)
                {
                    pivot = (min + max) / 2;

                    if (model.ActualTime < this[pivot].ActualTime)
                    {
                        min = pivot + 1;
                    }
                    else
                    {
                        max = pivot;
                    }
                }
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    if (model.FeedType == 2 && min >= MinIndex && min <= this.Count - 1 && !this.Contains(model))
                    {
                        this.Insert(min, model);
                        //FeedDuplicateHandler.Instance.list = this;
                        //FeedDuplicateHandler.Instance.RemoveDuplicates();
                    }
                }, DispatcherPriority.Send);
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }
        public void InsertSavedFeedModel(FeedModel model)
        {
            try
            {
                int min = MinIndex;
                int pivot;
                int max = this.Count - 1;
                while (max > min)
                {
                    pivot = (min + max) / 2;

                    if (model.SavedTime < this[pivot].SavedTime)
                    {
                        min = pivot + 1;
                    }
                    else
                    {
                        max = pivot;
                    }
                }
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    if (model.FeedType == 2 && min >= MinIndex && min <= this.Count - 1 && !this.Any(P => P.NewsfeedId == model.NewsfeedId))
                    {
                        this.Insert(min, model);
                    }
                }, DispatcherPriority.Send);
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }
        public void RemoveTopModels()
        {
            try
            {
                if (this.Count > 10)
                {
                    int keepCount = 0, startIdx = 0;
                    for (startIdx = this.Count - 2; startIdx > 0; startIdx--)
                    {
                        if (keepCount >= 10) break;
                        else if (this[startIdx].FeedType == 2) keepCount++;
                    }
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        if (startIdx < this.Count)
                            for (int i = startIdx; i > 0; i--)
                            {
                                if (this[i].FeedType == 2)
                                    this.RemoveAt(i);
                                else break;
                            }
                    }, DispatcherPriority.Send);
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public void RemoveBottomModels()
        {
            try
            {
                if (this.Count > 10)
                {
                    int keepCount = 0, startIdx = 0;
                    for (startIdx = 1; startIdx < this.Count - 1; startIdx++)
                    {
                        if (keepCount >= 10) break;
                        else if (this[startIdx].FeedType == 2) keepCount++;
                    }
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        for (int i = this.Count - 1; i >= startIdx; i--)
                        {
                            if (this[i].FeedType == 2)
                                this.RemoveAt(i);
                            else break;
                        }
                    }, DispatcherPriority.Send);
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public FeedModel GetFirstIndexModel()
        {
            FeedModel model = null;
            try
            {
                //model = this.Where(P => P.FeedType == 2).FirstOrDefault();
                for (int i = 1; i < this.Count - 1; i++)
                {
                    model = this[i];
                    if (model.FeedType == 2) return model;
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
            return null;
        }

        public FeedModel GetFirstModel()
        {
            FeedModel model = null;
            try
            {
                //model = this.Where(P => P.FeedType == 2 && P.FeedCategory != SettingsConstants.SPECIAL_FEED).FirstOrDefault();
                if (this.Count > 1)
                    for (int i = MinIndex; i < this.Count - 1; i++)
                    {
                        model = this[i];
                        if (model.NewsfeedId != Guid.Empty && model.FeedCategory != SettingsConstants.SPECIAL_FEED) return model; //model.FeedType == 2
                    }
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
            return null;
        }

        public FeedModel GetFirstSpecialFeedModel()
        {
            FeedModel model = null;
            try
            {
                //model = this.Where(P => P.FeedType == 2 && P.FeedCategory != SettingsConstants.SPECIAL_FEED).FirstOrDefault();
                if (this.Count > 1)
                {
                    model = this[MinIndex];
                    if (model.NewsfeedId != Guid.Empty && model.FeedCategory == SettingsConstants.SPECIAL_FEED) return model;
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
            return null;
        }

        public FeedModel GetLastModel()
        {
            FeedModel model = null;
            try
            {
                //model = this.Where(P => P.FeedType == 2 && P.FeedCategory != SettingsConstants.SPECIAL_FEED).LastOrDefault();
                if (this.Count > 1)
                    for (int i = this.Count - 2; i >= MinIndex; i--)
                    {
                        model = this[i];
                        if (model != null && model.NewsfeedId != Guid.Empty && model.FeedCategory != SettingsConstants.SPECIAL_FEED) return model; //.FeedType == 2
                    }
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
            return null;
        }

        public void RemoveModel(Guid nfId, bool iDelete = false)
        {
            try
            {
                //List<FeedModel> tempList = this.ToList();
                //if (this.Count > 0)
                //{
                //    foreach (FeedModel data in tempList)
                //    {
                //        if (data.NewsfeedId == nfId && data.FeedType == 2)
                //        {
                //            if (data.ParentFeed != null)
                //            {
                //                if (data.ParentFeed.WhoShareList != null && data.ParentFeed.WhoShareList.Count > 0)
                //                {
                //                    foreach (FeedModel singleShare in data.ParentFeed.WhoShareList)
                //                    {
                //                        if (singleShare.NewsfeedId == nfId)
                //                        {
                //                            Application.Current.Dispatcher.Invoke((Action)delegate
                //                             {
                //                                 data.ParentFeed.WhoShareList.Remove(singleShare);
                //                             }, DispatcherPriority.Send);
                //                            if (iDelete)
                //                            {
                //                                data.ParentFeed.IShare = 0;
                //                                //#ActivistShortInfoModel
                //                                //if (data.ParentFeed.ActivistShortInfoModel != null && data.ParentFeed.ActivistShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID && data.ParentFeed.Activity == AppConstants.NEWSFEED_SHARED)
                //                                //{
                //                                //    data.ParentFeed.ActivistShortInfoModel = null;
                //                                //    data.ParentFeed.Activity = 0;
                //                                //}
                //                            }
                //                            data.ParentFeed.NumberOfShares -= 1;
                //                            data.ParentFeed.OnPropertyChanged("WhoShareList");
                //                            data.ParentFeed.OnPropertyChanged("CurrentInstance");
                //                            break;
                //                        }
                //                    }
                //                }
                //            }
                //            int idxData = this.IndexOf(data);
                //            Application.Current.Dispatcher.Invoke((Action)delegate
                //            {
                //                this.Remove(data);
                //            }, DispatcherPriority.Send);
                //            break;
                //        }
                //    }


                //    foreach (FeedModel data in tempList)
                //    {
                //        if (data.ParentFeed != null && data.ParentFeed.NewsfeedId == nfId)
                //        {
                //            data.ParentFeed = null;
                //            data.OnPropertyChanged("CurrentInstance");
                //            break;
                //        }
                //    }

                //    foreach (FeedModel data in tempList)
                //    {
                //        if (data.WhoShareList != null && data.WhoShareList.Count > 0)
                //        {
                //            foreach (FeedModel singleShare in data.WhoShareList)
                //            {
                //                if (singleShare.NewsfeedId == nfId)
                //                {
                //                    Application.Current.Dispatcher.Invoke((Action)delegate
                //                    {
                //                        data.WhoShareList.Remove(singleShare);
                //                    }, DispatcherPriority.Send);
                //                    if (iDelete)
                //                    {
                //                        data.IShare = 0;
                //                        //#ActivistShortInfoModel
                //                        //if (data.ActivistShortInfoModel != null && data.ActivistShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID && data.Activity == AppConstants.NEWSFEED_SHARED)
                //                        //{
                //                        //    data.ActivistShortInfoModel = null;
                //                        //    data.Activity = 0;
                //                        //    //data.OnPropertyChanged("CurrentInstance");
                //                        //}
                //                    }
                //                    data.NumberOfShares -= 1;
                //                    data.OnPropertyChanged("WhoShareList");
                //                    data.OnPropertyChanged("CurrentInstance");
                //                    break;
                //                }
                //            }
                //        }

                //    }

                //    foreach (FeedModel data in tempList)
                //    {
                //        if (data.ParentFeed != null && data.ParentFeed.WhoShareList != null && data.ParentFeed.WhoShareList.Count > 0)
                //        {
                //            foreach (FeedModel singleShare in data.ParentFeed.WhoShareList)
                //            {
                //                if (singleShare.NewsfeedId == nfId)
                //                {
                //                    Application.Current.Dispatcher.Invoke((Action)delegate
                //                   {
                //                       data.ParentFeed.WhoShareList.Remove(singleShare);
                //                   }, DispatcherPriority.Send);

                //                    if (iDelete)
                //                    {
                //                        data.ParentFeed.IShare = 0;
                //                        //#ActivistShortInfoModel
                //                        //if (data.ParentFeed.ActivistShortInfoModel != null && data.ParentFeed.ActivistShortInfoModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID && data.ParentFeed.Activity == AppConstants.NEWSFEED_SHARED)
                //                        //{
                //                        //    data.ParentFeed.ActivistShortInfoModel = null;
                //                        //    data.ParentFeed.Activity = 0;
                //                        //    //data.ParentFeed.OnPropertyChanged("CurrentInstance");
                //                        //}
                //                    }
                //                    data.ParentFeed.NumberOfShares -= 1;
                //                    data.ParentFeed.OnPropertyChanged("WhoShareList");
                //                    data.ParentFeed.OnPropertyChanged("CurrentInstance");
                //                    break;
                //                }
                //            }
                //        }
                //    }
                //}
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

        public FeedModel GetModel(Guid nfId)
        {
            FeedModel model = null;
            try
            {
                //if (this.Count > 2)
                //{
                //    List<FeedModel> tempList = this.ToList();
                //    foreach (FeedModel data in tempList)
                //    {
                //        if (data.NewsfeedId == nfId)
                //        {
                //            model = data;
                //            return model;
                //        }
                //        else if (data.ParentFeed != null && data.ParentFeed.NewsfeedId == nfId)
                //        {
                //            model = data.ParentFeed;
                //            return model;
                //        }
                //        else if (data.WhoShareList != null && data.WhoShareList.Count > 0)
                //        {
                //            List<FeedModel> tempWhoShareList = data.WhoShareList.ToList();
                //            foreach (FeedModel singleShare in tempWhoShareList)
                //            {
                //                if (singleShare.NewsfeedId == nfId)
                //                {
                //                    model = singleShare;
                //                    return model;
                //                }
                //            }
                //        }
                //        else if (data.ParentFeed != null && data.ParentFeed.WhoShareList != null && data.ParentFeed.WhoShareList.Count > 0)
                //        {
                //            List<FeedModel> tempParentWhoShareList = data.ParentFeed.WhoShareList.ToList();
                //            foreach (FeedModel singleShare in tempParentWhoShareList)
                //            {
                //                if (singleShare.NewsfeedId == nfId)
                //                {
                //                    model = singleShare;
                //                    return model;
                //                }
                //            }
                //        }
                //    }
                //    /*for (int i = 1; i < this.Count - 1; i++)
                //    {
                //        model = this[i];
                //        if (model.NewsfeedId == nfId) return model;
                //        if (model.ParentFeed != null && model.ParentFeed.NewsfeedId == nfId) return model.ParentFeed;
                //        if (model.WhoShareList != null && model.WhoShareList.Count > 0)
                //        {
                //            for (int j = 0; j < model.WhoShareList.Count; j++)
                //            {
                //                if (model.WhoShareList[j].NewsfeedId == nfId) return model.WhoShareList[j];
                //            }
                //        }
                //        if (model.ParentFeed != null && model.ParentFeed.WhoShareList != null && model.ParentFeed.WhoShareList.Count > 0)
                //        {
                //            for (int k = 0; k < model.ParentFeed.WhoShareList.Count; k++)
                //            {
                //                if (model.ParentFeed.WhoShareList[k].NewsfeedId == nfId) return model.ParentFeed.WhoShareList[k];
                //            }
                //        }
                //    } */
                //}
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
            return null;
        }

        public void RemoveAllModels()
        {
            try
            {
                if (this.Count > 2)
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            for (int i = this.Count - 2; i >= MinIndex; i--)
                            {
                                FeedModel fm = this[i];
                                if (fm.NewsfeedId != Guid.Empty) //FeedType == 2
                                    //lock (this)
                                    this.Remove(fm);
                            }
                        }, DispatcherPriority.Send);
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }

    }
}

﻿using log4net;
using Models.Stores;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Media.Imaging;
using View.BindingModels;
using System.Linq;
using Models.Constants;

namespace View.Utility.Feed
{
    public class ImageQueueLoader
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ImageQueueLoader).Name);

        //private readonly object _syncRoot;
        public static ImageQueueLoader Instance = new ImageQueueLoader();
        private ConcurrentDictionary<Guid, ImageModel> ImageCollection; //private ConcurrentBag<ImageUploaderModel> Queue;
        private BackgroundWorker Worker;

        public ImageQueueLoader()
        {
            ImageCollection = new ConcurrentDictionary<Guid, ImageModel>(); // Queue= new ConcurrentQueue<ImageModel>();
            //_syncRoot = new object();
            Worker = new BackgroundWorker();
            Worker.WorkerSupportsCancellation = true;
            Worker.WorkerReportsProgress = true;
            Worker.DoWork += ImageLoader_DoWork;
            //Worker.RunWorkerCompleted += ImageLoader_RunWorkerCompleted;
        }

        public void LoadImage(ImageModel model)
        {
            try
            {
                //lock (_syncRoot)
                //{
                ImageCollection[model.ImageId] = model;
                if (!Worker.IsBusy)
                    Worker.RunWorkerAsync();
                //}
            }
            catch (Exception ex)
            {
                log.Error("LoadImage ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private ImageModel RemoveAndReturnLatestOpenImageModel()
        {
            ImageModel model = null;
            for (int i = 0; i < ImageCollection.Count; i++)
            {
                KeyValuePair<Guid, ImageModel> kv = ImageCollection.ElementAt(i);
                if (kv.Value.IsOpenedInFeed)
                {
                    ImageCollection.TryRemove(kv.Key, out model);
                    return model;
                }
            }
            KeyValuePair<Guid, ImageModel> kvAll = ImageCollection.FirstOrDefault();
            ImageCollection.TryRemove(kvAll.Key, out model);
            return model;
        }
        private void LoadExistingImageToDictionary(ImageModel imageModel)
        {
            double mw = 0, mh = 0;
            ImageUtility.GetResizeImageParameters(imageModel.FeedImageInfoModel.Width, imageModel.FeedImageInfoModel.Height, imageModel.FeedImageInfoModel.ReWidth, imageModel.FeedImageInfoModel.ReHeight, out mw, out mh);
            BitmapImage bitmap = new BitmapImage();
            var stream = new FileStream(imageModel.ImageLocation, FileMode.Open);
            bitmap.BeginInit();
            bitmap.CacheOption = BitmapCacheOption.OnLoad;
            bitmap.StreamSource = stream;
            bitmap.DecodePixelHeight = (int)mh;
            bitmap.DecodePixelWidth = (int)mw;
            bitmap.EndInit();
            stream.Close();
            stream.Dispose();
            bitmap.Freeze();
            if (!ImageDictionaries.Instance.TMP_BOOK_IMAGES.ContainsKey(imageModel.ImageId))
            {
                ImageDictionaries.Instance.TMP_BOOK_IMAGES[imageModel.ImageId] = bitmap;
            }
        }
        private void ImageLoader_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                while (ImageCollection.Count > 0)
                {
                    ImageModel imageModel = RemoveAndReturnLatestOpenImageModel();
                    if (!string.IsNullOrEmpty(imageModel.ImageLocation) && File.Exists(imageModel.ImageLocation))
                    {
                        LoadExistingImageToDictionary(imageModel);
                        imageModel.OnPropertyChanged("CurrentInstance");
                    }
                    else if (!string.IsNullOrEmpty(imageModel.ImageUrl))
                    {
                        if (ServerAndPortSettings.GetImageServerResourceURL != null)
                        {
                            string imgUrl = HelperMethods.GetUrlWithSize(imageModel.ImageUrl, ImageUtility.IMG_600);
                            if (!string.IsNullOrWhiteSpace(imgUrl))
                            {
                                string image_url_with_base = ServerAndPortSettings.GetImageServerResourceURL + imgUrl;
                                if (ImageUtility.DownloadRemoteImageFile(image_url_with_base, imageModel.ImageLocation)) //successfully downloaded image locally
                                {
                                    LoadExistingImageToDictionary(imageModel);
                                    imageModel.OnPropertyChanged("CurrentInstance");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("ImageLoader_DoWork ==> " + ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
        }
    }
}

﻿using Auth.Service.Images;
using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using View.BindingModels;
using View.Utility.DataContainer;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.Utility.Feed
{
    public class ThreadAddOrEditAnyComment
    {
        /// <summary>
        /// TYPE_ADD_STATUS_COMMENT = 181;
        /// TYPE_EDIT_STATUS_COMMENT = 189;
        /// TYPE_EDIT_IMAGE_COMMENT = 194;
        /// TYPE_EDIT_COMMENT_ON_MEDIA = 266;
        /// </summary>

        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThreadAddOrEditAnyComment).Name);
        private bool running = false, success = false, ImgToContent = false;
        private string message = string.Empty, comment, serverUrl;
        private JArray commentTagsJArray;
        private Guid newsfeedId, commentId, imageId, contentId;
        private WebClient webClient;
        private CommonUploaderModel commonUploaderModel;
        private JObject pakToSend;
        private byte[] wholebytes, tmpPostData;
        #endregion "Private Fields"

        #region "Public Fields"
        public delegate void CallBack(bool success);
        public event CallBack callBackEvent;
        #endregion "Public Fields"

        #region "Constructors"
        public ThreadAddOrEditAnyComment(JObject PakToSend, CommonUploaderModel commonUploaderModel, bool ImageIdToContentId)
        {
            this.pakToSend = PakToSend;
            this.commonUploaderModel = commonUploaderModel;
            this.ImgToContent = ImageIdToContentId;
        }
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            running = true;
            if (commonUploaderModel != null && !commonUploaderModel.IsUploadedInServer)
            {
                switch (commonUploaderModel.UploadType)
                {
                    case SettingsConstants.UPLOADTYPE_IMAGE:
                        commonUploaderModel.IsUploading = true;
                        serverUrl = ServerAndPortSettings.GetCommentImageUploadingURL;
                        webClient = new WebClient();
                        webClient.UploadProgressChanged += new UploadProgressChangedEventHandler(UploadProgressChanged);
                        webClient.UploadDataCompleted += new UploadDataCompletedEventHandler(UploadDataCompleted);
                        UploadToImageOrMediaServer();
                        break;
                    case SettingsConstants.UPLOADTYPE_VIDEO:
                        commonUploaderModel.IsUploading = true;
                        serverUrl = ServerAndPortSettings.GetCommentVideoUploadingURL;
                        webClient = new WebClient();
                        webClient.UploadProgressChanged += new UploadProgressChangedEventHandler(UploadProgressChanged);
                        webClient.UploadDataCompleted += new UploadDataCompletedEventHandler(UploadDataCompleted);
                        UploadToImageOrMediaServer();
                        break;
                    case SettingsConstants.UPLOADTYPE_MUSIC:
                        commonUploaderModel.IsUploading = true;
                        serverUrl = ServerAndPortSettings.GetCommentAudioUploadingURL;
                        webClient = new WebClient();
                        webClient.UploadProgressChanged += new UploadProgressChangedEventHandler(UploadProgressChanged);
                        webClient.UploadDataCompleted += new UploadDataCompletedEventHandler(UploadDataCompleted);
                        UploadToImageOrMediaServer();
                        break;
                    default: //sticker
                        ContinueToAuthServer();
                        break;
                }
            }
            else ContinueToAuthServer();
        }
        private async void ThumbImageUpload()
        {
            try
            {
                TagLib.File file = TagLib.File.Create(commonUploaderModel.FilePath);
                byte[] response = await FeedImagesUpload.Instance.UploadThumbImageToImageServerWebRequest(file.Tag.Pictures[0].Data.Data, commonUploaderModel.FileWidth, commonUploaderModel.FileHeight);
                if (response != null && response[0] == 1)
                {
                    int len = response[1];
                    commonUploaderModel.UploadedThumbUrl = Encoding.UTF8.GetString(response, 2, len);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                commonUploaderModel.IsUploading = false;
                commonUploaderModel.IsUploadedInServer = true;
                ContinueToAuthServer();
            }
        }
        private void UploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {
            try
            {
                int Pecentage = (int)(((double)e.BytesSent / (double)e.TotalBytesToSend) * 100);
                commonUploaderModel.Pecentage = Pecentage;
            }
            catch (Exception)
            {
            }
        }
        private void UploadDataCompleted(object sender, UploadDataCompletedEventArgs e)
        {
            bool success = false;
            try
            {
                tmpPostData = null;
                if (e.Result != null && e.Result.Length > 0 && (int)e.Result[0] == 1)
                {
                    int urlLength = (int)e.Result[1];
                    commonUploaderModel.UploadedUrl = Encoding.UTF8.GetString(e.Result, 2, urlLength);
                    if (commonUploaderModel.UploadType == SettingsConstants.UPLOADTYPE_VIDEO)
                    {
                        if (!string.IsNullOrEmpty(commonUploaderModel.UploadedUrl)) commonUploaderModel.UploadedThumbUrl = commonUploaderModel.UploadedUrl.Replace(".mp4", ".jpg");
                    }
                    else if (commonUploaderModel.UploadType == SettingsConstants.UPLOADTYPE_MUSIC)
                    {
                        if (commonUploaderModel.IsThumbImageFound && commonUploaderModel.FileHeight > 0 && commonUploaderModel.FileWidth > 0)
                        {
                            success = true;
                            ThumbImageUpload();
                            return;
                        }
                    }
                    commonUploaderModel.IsUploading = false;
                    success = commonUploaderModel.IsUploadedInServer = true;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
            ContinueToAuthServer(success);
        }
        private void UploadToImageOrMediaServer()
        {
            try
            {
                string _lineEnd = "\r\n";
                string _twoHyphens = "--";
                string _boundary = "*****";

                webClient.Headers["Content-Type"] = "multipart/form-data; boundary=" + _boundary;
                webClient.Headers["User-Agent"] = "ringID" + "_" + DefaultSettings.VALUE_APP_INSTALLED_VERSION;
                webClient.Headers["access-control-allow-origin"] = "*";

                var firstPart = "";
                string fileheaderTemplate = "Content-Disposition: form-data; name=\"{0}\"" + _lineEnd + _lineEnd + "{1}" + _lineEnd;

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "sId", DefaultSettings.LOGIN_SESSIONID);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "uId", DefaultSettings.LOGIN_RING_ID);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "authServer", ServerAndPortSettings.AUTH_SERVER_IP);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "comPort", ServerAndPortSettings.COMMUNICATION_PORT);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "x-app-version", AppConfig.VERSION_PC);
                if (commonUploaderModel.UploadType == SettingsConstants.UPLOADTYPE_IMAGE)
                {
                    firstPart += _twoHyphens + _boundary + _lineEnd;
                    firstPart += string.Format(fileheaderTemplate, "iw", commonUploaderModel.FileWidth);

                    firstPart += _twoHyphens + _boundary + _lineEnd;
                    firstPart += string.Format(fileheaderTemplate, "ih", commonUploaderModel.FileHeight);

                    firstPart += _twoHyphens + _boundary + _lineEnd;
                    firstPart += string.Format(fileheaderTemplate, "imT", SettingsConstants.TYPE_NORMAL_BOOK_IMAGE);

                    firstPart += _twoHyphens + _boundary + _lineEnd;
                    firstPart += "Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + "pcupload.jpg" + "\"" + _lineEnd;
                    firstPart += _lineEnd;
                    byte[] firstBytes = webClient.Encoding.GetBytes(firstPart);

                    var lastPart = "";
                    lastPart += _lineEnd;
                    lastPart += _twoHyphens + _boundary + _twoHyphens + _lineEnd;
                    byte[] lastBytes = webClient.Encoding.GetBytes(lastPart);
                    ////////
                    int w = 0, h = 0;
                    System.Drawing.Image image = null;
                    long n;

                    image = ImageUtility.GetOrientedImageFromFilePath(commonUploaderModel.FilePath);//System.Drawing.Image.FromFile(@model.FilePath);
                    if (image == null)
                    {
                        UIHelperMethods.ShowWarning("Image Not found or Corrupted! ", "Image corrupted");
                        // CustomMessageBox.ShowError("Image Not found or Corrupted! ");
                        return;
                    }
                    tmpPostData = null;
                    if (long.TryParse(commonUploaderModel.FilePath, out n))
                    {
                        commonUploaderModel.FileWidth = w = (int)commonUploaderModel.ThumbnailBmp.Width;
                        commonUploaderModel.FileHeight = h = (int)commonUploaderModel.ThumbnailBmp.Height;
                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.Frames.Add(BitmapFrame.Create(commonUploaderModel.ThumbnailBmp));
                        using (MemoryStream ms = new MemoryStream())
                        {
                            encoder.Save(ms);
                            tmpPostData = ms.ToArray();
                        }
                    }
                    else
                    {
                        tmpPostData = ImageUtility.ImageResizeandQuality(image, 75, out w, out h);
                        commonUploaderModel.FileHeight = h;
                        commonUploaderModel.FileWidth = w;
                        image.Dispose();
                    }

                    int fileLength = tmpPostData.Length;
                    long length = firstBytes.Length + fileLength + lastBytes.Length;
                    wholebytes = new Byte[length];
                    int currentbytes = 0;

                    Buffer.BlockCopy(firstBytes, 0, wholebytes, currentbytes, firstBytes.Length);
                    currentbytes += firstBytes.Length;

                    Buffer.BlockCopy(tmpPostData, 0, wholebytes, currentbytes, fileLength);
                    currentbytes += fileLength;
                    if (tmpPostData != null) { GC.SuppressFinalize(tmpPostData); tmpPostData = null; }

                    Buffer.BlockCopy(lastBytes, 0, wholebytes, currentbytes, lastBytes.Length);
                    currentbytes += lastBytes.Length;
                }
                else
                {
                    firstPart += _twoHyphens + _boundary + _lineEnd;
                    firstPart += "Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + Path.GetFileName(commonUploaderModel.FilePath) + "\"" + _lineEnd;
                    firstPart += _lineEnd;
                    byte[] firstBytes = webClient.Encoding.GetBytes(firstPart);

                    var lastPart = "";
                    lastPart += _lineEnd;
                    lastPart += _twoHyphens + _boundary + _twoHyphens + _lineEnd;
                    byte[] lastBytes = webClient.Encoding.GetBytes(lastPart);


                    FileStream oFileStream = new FileStream(commonUploaderModel.FilePath, FileMode.Open, FileAccess.Read);
                    int fileLength = (int)oFileStream.Length;
                    long length = firstBytes.Length + fileLength + lastBytes.Length;
                    wholebytes = new Byte[length];
                    int currentbytes = 0;

                    Buffer.BlockCopy(firstBytes, 0, wholebytes, currentbytes, firstBytes.Length);
                    currentbytes += firstBytes.Length;

                    oFileStream.Read(wholebytes, currentbytes, fileLength);
                    currentbytes += fileLength;
                    oFileStream.Close();

                    Buffer.BlockCopy(lastBytes, 0, wholebytes, currentbytes, lastBytes.Length);
                    currentbytes += lastBytes.Length;

                }
                webClient.UploadDataAsync(new Uri(serverUrl), "POST", wholebytes);
                if (wholebytes != null) { GC.SuppressFinalize(wholebytes); wholebytes = null; }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
                if (wholebytes != null) { GC.SuppressFinalize(wholebytes); wholebytes = null; }
                if (tmpPostData != null) { GC.SuppressFinalize(tmpPostData); tmpPostData = null; }
                if (webClient != null) { GC.SuppressFinalize(webClient); webClient = null; }
            }
        }
        private void ContinueToAuthServer(bool UploadCompleteOrNotNecessary = true)
        {
            try
            {
                if (UploadCompleteOrNotNecessary && !string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
                {
                    string pakId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = pakId;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    if (commonUploaderModel != null)
                    {
                        pakToSend[JsonKeys.UrlType] = commonUploaderModel.UploadType;
                        if (!string.IsNullOrEmpty(commonUploaderModel.UploadedUrl))
                        {
                            pakToSend[JsonKeys.Url] = commonUploaderModel.UploadedUrl;
                        }
                        if (commonUploaderModel.UploadType == SettingsConstants.UPLOADTYPE_MUSIC || commonUploaderModel.UploadType == SettingsConstants.UPLOADTYPE_VIDEO)
                        {
                            pakToSend[JsonKeys.MediaDuration] = commonUploaderModel.FileDuration;
                            pakToSend[JsonKeys.ThumbUrlComment] = commonUploaderModel.UploadedThumbUrl;
                        }
                    }
                    string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
                    int action = (pakToSend[JsonKeys.Action] != null) ? (int)pakToSend[JsonKeys.Action] : 0;
                    Console.WriteLine("Sent in action " + action + "---> " + data);
                    Dictionary<String, byte[]> packets = SendToServer.BuildBrokenPacket(data, action, pakId);
                    int packetType = packets.Count > 1 ? AppConstants.BROKEN_PACKET : AppConstants.SINGLE_PACKET;
                    List<string> packetIds = new List<string>(packets.Keys);
                    SendToServer.SendBrokenPacket(packetType, packets);
                    Thread.Sleep(25);
                    string makeResponsepak = pakId + "_" + (int)pakToSend[JsonKeys.Action];

                    JObject feedbackfields = null;
                    for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
                    {
                        if (string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
                        {
                            break;
                        }
                        Thread.Sleep(DefaultSettings.WAITING_TIME);
                        if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(makeResponsepak, out feedbackfields))
                        {
                            if (SendToServer.CheckAllBrokenPacketConfirmation(packets))
                            {
                                if (i % DefaultSettings.SEND_INTERVAL == 0)
                                {
                                    SendToServer.SendBrokenPacket(packetType, packets);
                                }
                            }
                        }
                        else
                        {
                            if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                            {
                                //if (action == AppConstants.TYPE_ADD_STATUS_COMMENT && fm != null && (fm.Activist == null || (fm.Activist.UserTableID != DefaultSettings.LOGIN_TABLE_ID && fm.ActivityType == 2)))
                                //{
                                //    fm.Activist = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel;
                                //    fm.ActivityType = 2;
                                //}
                                commentTagsJArray = (pakToSend[JsonKeys.CommentTagList] != null) ? (JArray)pakToSend[JsonKeys.CommentTagList] : null;
                                comment = (pakToSend[JsonKeys.Comment] != null) ? (String)pakToSend[JsonKeys.Comment] : string.Empty;
                                if (commentTagsJArray != null && feedbackfields[JsonKeys.CommentTags] == null) feedbackfields[JsonKeys.CommentTags] = commentTagsJArray;
                                if (comment != null && feedbackfields[JsonKeys.Comment] == null) feedbackfields[JsonKeys.Comment] = comment;
                                if (commonUploaderModel == null)
                                {
                                    if (feedbackfields[JsonKeys.UrlType] == null) feedbackfields[JsonKeys.UrlType] = 0;
                                    if (feedbackfields[JsonKeys.Url] == null) feedbackfields[JsonKeys.Url] = string.Empty;
                                }
                                else
                                {
                                    if (feedbackfields[JsonKeys.UrlType] == null) feedbackfields[JsonKeys.UrlType] = commonUploaderModel.UploadType;
                                    if (feedbackfields[JsonKeys.Url] == null && !string.IsNullOrEmpty(commonUploaderModel.UploadedUrl)) feedbackfields[JsonKeys.Url] = commonUploaderModel.UploadedUrl;
                                    if (feedbackfields[JsonKeys.ThumbUrlComment] == null) feedbackfields[JsonKeys.ThumbUrlComment] = commonUploaderModel.UploadedThumbUrl; // && !string.IsNullOrEmpty(commonUploaderModel.UploadedThumbUrl)
                                    if (feedbackfields[JsonKeys.MediaDuration] == null) feedbackfields[JsonKeys.MediaDuration] = commonUploaderModel.FileDuration; //commonUploaderModel.FileDuration > 0 && 
                                }
                                success = true;
                                int activityType = (pakToSend[JsonKeys.ActivityType] != null) ? (int)pakToSend[JsonKeys.ActivityType] : 0;
                                if (action == AppConstants.TYPE_EDIT_STATUS_COMMENT || activityType == 1)
                                    newsfeedId = (pakToSend[JsonKeys.NewsfeedId] != null) ? (Guid)pakToSend[JsonKeys.NewsfeedId] : Guid.Empty;
                                else
                                {
                                    newsfeedId = (pakToSend[JsonKeys.NewsfeedId] != null) ? (Guid)pakToSend[JsonKeys.NewsfeedId] : Guid.Empty;
                                    if (ImgToContent)
                                        imageId = (pakToSend[JsonKeys.ContentId] != null) ? (Guid)pakToSend[JsonKeys.ContentId] : Guid.Empty;
                                    else
                                    {
                                        contentId = (pakToSend[JsonKeys.ContentId] != null) ? (Guid)pakToSend[JsonKeys.ContentId] : Guid.Empty;
                                        imageId = (pakToSend[JsonKeys.ImageId] != null) ? (Guid)pakToSend[JsonKeys.ImageId] : Guid.Empty;
                                    }
                                }
                                commentId = (pakToSend[JsonKeys.CommentId] != null) ? (Guid)pakToSend[JsonKeys.CommentId] : Guid.Empty;

                                if (commentId != Guid.Empty)
                                    MainSwitcher.AuthSignalHandler().feedSignalHandler.EditOrUpdateEditAnyComment(feedbackfields, commentId, newsfeedId, imageId, contentId);
                                else
                                    MainSwitcher.AuthSignalHandler().feedSignalHandler.AddOrUpdateAddAnyComment(feedbackfields, newsfeedId, imageId, contentId);
                            }
                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
                            break;
                        }
                    }
                    SendToServer.RemoveAllBrokenPacketConfirmation(packetIds);
                    if (feedbackfields == null)
                    {
                        if (!MainSwitcher.ThreadManager().PingNow())
                        {
                            message = ReasonCodeConstants.REASON_MSG_INTERNET_UNAVAILABLE;
                        }
                    }
                }
                else
                {
                    log.Error("Add or Edit Comment Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                }
            }
            catch (Exception e)
            {
                log.Error(e.StackTrace + e.Message);
            }
            finally
            {
                if (callBackEvent != null)
                {
                    callBackEvent(success);
                    callBackEvent = null;
                }
                running = false;
            }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace View.Utility.Feed
{
    //public class ScaleToFeedImageView
    //{
    //    private const int SEPERATOR = 5;

    //    private FeedImageInfo[] _ImageArray;
    //    private int _ImageViewHeight = 100;
    //    private int _Type = 1;

    //    public ScaleToFeedImageView(List<FeedImageInfo> images)
    //    {
    //        images = images.OrderBy(P => P.ImgType).OrderByDescending(P => P.Width * P.Height).ToList();
    //        Type = FeedImageInfo.FindViewType(images);
    //        ImageArray = FeedImageInfo.GetSortedFeedImageArray(images, Type);
    //        BuildView();
    //    }

    //    private void AssignFitMode()
    //    {
    //        for (int i = 0; i < ImageArray.Length; i++)
    //        {
    //            float oRation = (float)ImageArray[i].Width / (float)ImageArray[i].Height;
    //            float tRation = (float)ImageArray[i].ReWidth / (float)ImageArray[i].ReHeight;

    //            if (ImageArray[i].Width < ((float)ImageArray[i].ReWidth * 0.70) && ImageArray[i].Height < ((float)ImageArray[i].ReHeight * 0.70))
    //            {
    //                ImageArray[i].FitMode = FeedImageInfo.FIT_TO_NONE;
    //            }
    //            else if (oRation < tRation)
    //            {
    //                ImageArray[i].FitMode = FeedImageInfo.FIT_TO_WIDTH;
    //            }
    //            else
    //            {
    //                ImageArray[i].FitMode = FeedImageInfo.FIT_TO_HEIGHT;
    //            }
    //        }
    //    }

    //    public FeedImageInfo[] ImageArray
    //    {
    //        get { return _ImageArray; }
    //        set { _ImageArray = value; }
    //    }

    //    public int Type
    //    {
    //        get { return _Type; }
    //        set { _Type = value; }
    //    }

    //    public int ImageViewHeight
    //    {
    //        get { return _ImageViewHeight; }
    //        set { _ImageViewHeight = value; }
    //    }

    //    private void BuildView()
    //    {
    //        int currType = FeedImageInfo.SIEVE[Type];

    //        int i = 0;
    //        foreach (FeedImageInfo image in ImageArray)
    //        {
    //            image.ReWidth = FeedImageInfo.SIZE[currType][i++];
    //            image.ReHeight = FeedImageInfo.SIZE[currType][i++];
    //        }

    //        if (currType == FeedImageInfo.VIEW_TYPE_1
    //                || currType == FeedImageInfo.VIEW_TYPE_2
    //                || currType == FeedImageInfo.VIEW_TYPE_3)
    //        {

    //            int width = ImageArray[0].Width;
    //            int height = ImageArray[0].Height;
    //            if (width > 600)
    //            {
    //                width = 600;
    //                height = (ImageArray[0].Height * width) / ImageArray[0].Width;
    //            }

    //            ImageArray[0].ReWidth = width;
    //            ImageArray[0].ReHeight = height;
    //            ImageViewHeight = ImageArray[0].ReHeight;
    //        }
    //        else if (currType == FeedImageInfo.VIEW_TYPE_4
    //                || currType == FeedImageInfo.VIEW_TYPE_5
    //                || currType == FeedImageInfo.VIEW_TYPE_7
    //                || currType == FeedImageInfo.VIEW_TYPE_8
    //                || currType == FeedImageInfo.VIEW_TYPE_9)
    //        {
    //            ImageArray[1].X = ImageArray[0].ReWidth + SEPERATOR;
    //            ImageViewHeight = ImageArray[0].ReHeight;
    //        }
    //        else if (currType == FeedImageInfo.VIEW_TYPE_6)
    //        {
    //            ImageArray[1].Y = ImageArray[0].ReHeight + SEPERATOR;
    //            ImageViewHeight = ImageArray[0].ReHeight + SEPERATOR + ImageArray[1].ReHeight;
    //        }
    //        else if (currType == FeedImageInfo.VIEW_TYPE_10
    //                || currType == FeedImageInfo.VIEW_TYPE_17
    //                || currType == FeedImageInfo.VIEW_TYPE_19)
    //        {
    //            ImageArray[0].ReHeight = ImageArray[0].ReHeight + SEPERATOR;
    //            ImageArray[1].X = ImageArray[0].ReWidth + SEPERATOR;
    //            ImageArray[2].X = ImageArray[0].ReWidth + SEPERATOR;
    //            ImageArray[2].Y = ImageArray[1].ReHeight + SEPERATOR;
    //            ImageViewHeight = ImageArray[0].ReHeight;
    //        }
    //        else if (currType == FeedImageInfo.VIEW_TYPE_11)
    //        {
    //            int W = 600;
    //            float r1 = (float)ImageArray[0].Height / (float)ImageArray[2].Height;
    //            float r2 = (float)ImageArray[1].Height / (float)ImageArray[2].Height;
    //            float r3 = (float)1.00;
    //            float unit = (float)500 / (r1 + r2 + r3);

    //            ImageArray[0].ReWidth = W;
    //            ImageArray[0].ReHeight = (int)(unit * r1);
    //            ImageArray[1].ReWidth = W;
    //            ImageArray[1].ReHeight = (int)(unit * r2);
    //            ImageArray[2].ReWidth = W;
    //            ImageArray[2].ReHeight = (int)(unit * r3);

    //            ImageArray[1].Y = ImageArray[0].ReHeight + SEPERATOR;
    //            ImageArray[2].Y = ImageArray[0].ReHeight + SEPERATOR + ImageArray[1].ReHeight + SEPERATOR;
    //            ImageViewHeight = ImageArray[0].ReHeight + SEPERATOR + ImageArray[1].ReHeight + SEPERATOR + ImageArray[2].ReHeight;
    //        }
    //        else if (currType == FeedImageInfo.VIEW_TYPE_12
    //                || currType == FeedImageInfo.VIEW_TYPE_15
    //                || currType == FeedImageInfo.VIEW_TYPE_18)
    //        {
    //            ImageArray[1].X = ImageArray[0].ReWidth + SEPERATOR;
    //            ImageArray[2].X = ImageArray[0].ReWidth + SEPERATOR + ImageArray[1].ReWidth + SEPERATOR;
    //            ImageViewHeight = ImageArray[0].ReHeight;
    //        }
    //        else if (currType == FeedImageInfo.VIEW_TYPE_13)
    //        {
    //            ImageArray[1].ReHeight = ImageArray[1].ReHeight + SEPERATOR;
    //            ImageArray[1].X = ImageArray[0].ReWidth + SEPERATOR;
    //            ImageArray[2].Y = ImageArray[0].ReHeight + SEPERATOR;
    //            ImageViewHeight = ImageArray[1].ReHeight;
    //        }
    //        else if (currType == FeedImageInfo.VIEW_TYPE_14)
    //        {
    //            ImageArray[0].ReWidth = ImageArray[0].ReWidth + SEPERATOR;
    //            ImageArray[1].Y = ImageArray[0].ReHeight + SEPERATOR;
    //            ImageArray[2].X = ImageArray[1].ReWidth + SEPERATOR;
    //            ImageArray[2].Y = ImageArray[0].ReHeight + SEPERATOR;
    //            ImageViewHeight = ImageArray[0].ReHeight + SEPERATOR + ImageArray[1].ReHeight;
    //        }
    //        else if (currType == FeedImageInfo.VIEW_TYPE_16)
    //        {
    //            ImageArray[1].ReWidth = ImageArray[1].ReWidth + SEPERATOR;
    //            ImageArray[2].X = ImageArray[0].ReWidth + SEPERATOR;
    //            ImageArray[1].Y = ImageArray[0].ReHeight + SEPERATOR;
    //            ImageViewHeight = ImageArray[0].ReHeight + SEPERATOR + ImageArray[1].ReHeight;
    //        }

    //        AssignFitMode();

    //    }

    //}
}

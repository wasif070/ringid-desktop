﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json.Linq;

namespace View.Utility.Feed
{
    public class ThradMediaAlbumContentList
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradMediaAlbumContentList).Name);
        private bool running = false;
        Guid albumId;
        long userTableId;
        int mediaType;
        int StartLimit;
        long userId;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                bool success = false;
                string msg = string.Empty;
                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_MEDIA_ALBUM_CONTENT_LIST;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.StartLimit] = this.StartLimit;
                pakToSend[JsonKeys.AlbumId] = this.albumId;
                pakToSend[JsonKeys.UserTableID] = this.userTableId;
                pakToSend[JsonKeys.MediaType] = this.mediaType;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
                if (feedbackfields != null)
                {
                    try
                    {
                        if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                        {
                            success = true;
                        }
                        else if (feedbackfields[JsonKeys.Message] != null)
                        {
                            msg = (string)feedbackfields[JsonKeys.Message];
                        }
                    }
                    finally { }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    success = false;
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
                if (!success)
                {
                    bool nodata = (!string.IsNullOrEmpty(msg) && msg.Equals("No data found")) ? true : false;
                    //MainSwitcher.AuthSignalHandler().feedSignalHandler.ShowMorePanelVisiblityInMedia(userTableId, mediaType, userId, nodata);
                    //MainSwitcher.AuthSignalHandler().feedSignalHandler.HideLoaderOfAblumContentList(this.userTableId, this.mediaType);
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(int StartLimit, Guid albmId, long UtId, int mediaType, long userId = 0)
        {
            if (!running)
            {
                this.albumId = albmId;
                this.userTableId = UtId;
                this.mediaType = mediaType;
                this.StartLimit = StartLimit;
                this.userId = userId;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
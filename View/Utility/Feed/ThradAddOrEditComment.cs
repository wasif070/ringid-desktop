﻿using System;
using System.Collections.Generic;
using System.Threading;
using Auth.utility;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace View.Utility.Feed
{
    class ThradAddOrEditComment
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradAddOrEditComment).Name);
        private bool running = false;
        private long nfId;
        private string cmnt;
        private long cmntId, friendId, circleId;

        #endregion "Private Fields"

        #region "Constructors"
        public ThradAddOrEditComment(long nfId, string cmnt, long cmntId, long friendId, long circleId)
        {
            this.nfId = nfId;
            this.cmnt = cmnt;
            this.cmntId = cmntId;
            this.friendId = friendId;
            this.circleId = circleId;

        }
        #endregion "Constructors"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                bool succ = false;
                if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
                {
                    try
                    {
                        JObject pakToSend = new JObject();
                        string pakId = SendToServer.GetRanDomPacketID();
                        pakToSend[JsonKeys.PacketId] = pakId;
                        pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                        if (cmntId > 0)
                        {
                            pakToSend[JsonKeys.Action] = AppConstants.TYPE_EDIT_STATUS_COMMENT;
                            pakToSend[JsonKeys.CommentId] = cmntId;
                        }
                        else
                        {
                            pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_STATUS_COMMENT;
                        }
                        pakToSend[JsonKeys.NewsfeedId] = nfId;
                        pakToSend[JsonKeys.Comment] = cmnt;

                        string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
                        Dictionary<String, byte[]> packets = SendToServer.BuildBrokenPacket(data, (int)pakToSend[JsonKeys.Action], pakId);
                        int packetType = packets.Count > 1 ? AppConstants.BROKEN_PACKET : AppConstants.SINGLE_PACKET;
                        List<string> packetIds = new List<string>(packets.Keys);
                        SendToServer.SendBrokenPacket(packetType, packets);
                        Thread.Sleep(25);
                        string makeResponsepak = pakId + "_" + (int)pakToSend[JsonKeys.Action];
                        for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
                        {
                            if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
                            {
                                break;
                            }
                            Thread.Sleep(DefaultSettings.WAITING_TIME);
                            if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(makeResponsepak))
                            {
                                if (SendToServer.CheckAllBrokenPacketConfirmation(packets))
                                {
                                    if (i % DefaultSettings.SEND_INTERVAL == 0)
                                    {
                                        SendToServer.SendBrokenPacket(packetType, packets);
                                    }
                                }
                            }
                            else
                            {
                                JObject feedbackfields = null;
                                RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(makeResponsepak, out feedbackfields);
                                if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                                {
                                    succ = true;
                                    if (cmntId > 0)
                                    {
                                        MainSwitcher.AuthSignalHandler().feedSignalHandler.EditFeedComment(cmnt, cmntId, nfId, friendId, circleId, (feedbackfields[JsonKeys.ShowContinue] != null && (bool)feedbackfields[JsonKeys.ShowContinue]));
                                    }
                                    else
                                    {
                                        //CommentDTO comment = HelperMethodsModel.BindCommentDetails(feedbackfields);
                                        ///comment.Comment = cmnt;
                                        //MainSwitcher.AuthSignalHandler().feedSignalHandler.AddFeedComment(comment, nfId, friendId, circleId, DefaultSettings.LOGIN_TABLE_ID);
                                    }
                                }
                                if (!succ)
                                {
                                    string mg = "Failed to Comment! ";
                                    if (feedbackfields[JsonKeys.Message] != null)
                                    {
                                        mg = (string)feedbackfields[JsonKeys.Message];
                                    }
                                    MainSwitcher.AuthSignalHandler().ShowErrorMessageBox(mg, "Failed!");
                                }
                                RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
                                break;
                            }
                            // PingInServer.StartThread(i, DefaultSettings.TRYING_TIME);
                        }
                        MainSwitcher.ThreadManager().PingNow();
                        SendToServer.RemoveAllBrokenPacketConfirmation(packetIds);
                    }
                    catch (Exception e)
                    {
                        log.Error("Add or Edit Comment ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                    }
                }
                else
                {
#if AUTH_LOG
                log.Error("Add or Edit Comment Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
#endif
                    MainSwitcher.AuthSignalHandler().feedSignalHandler.ShowErrorMessageBox(NotificationMessages.INTERNET_UNAVAILABLE, "Failed!");
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                running = false;
            }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
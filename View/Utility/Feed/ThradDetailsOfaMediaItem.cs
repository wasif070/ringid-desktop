﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Utility.DataContainer;

namespace View.Utility.Feed
{
    public class ThradDetailsOfaMediaItem
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradDetailsOfaMediaItem).Name);
        private bool running = false;
        Guid contentId;
        long utId = 0;
        Guid cmntID;
        bool fromNotification;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        public ThradDetailsOfaMediaItem()
        {

        }
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.ContentId] = contentId;
                pakToSend[JsonKeys.UserTableID] = utId;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_MEDIA_CONTENT_DETAILS;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success] && feedbackfields[JsonKeys.MediaContentDTO] != null)
                    {
                        if (feedbackfields[JsonKeys.ReasonCode] != null && ReasonCodeConstants.REASON_CODE_NOT_FOUND == (long)feedbackfields[JsonKeys.ReasonCode])
                        {
                            UIHelperMethods.ShowErrorMessageBoxFromThread(ReasonCodeConstants.REASON_MSG_NOT_FOUND, "");
                        }
                        else
                        {
                            JObject mediaContent = (JObject)feedbackfields[JsonKeys.MediaContentDTO];
                            //
                            SingleMediaModel model = null;
                            if (!MediaDataContainer.Instance.ContentModels.TryGetValue(contentId, out model))
                            {
                                model = new SingleMediaModel();
                                MediaDataContainer.Instance.ContentModels[contentId] = model;
                            }
                            model.LoadDataFromDetails(mediaContent, model.MediaType);
                            if (model.LikeCommentShare == null)
                            {
                                if (model.NewsFeedId != Guid.Empty)
                                {
                                    FeedModel fm = null;
                                    if (!FeedDataContainer.Instance.FeedModels.TryGetValue(model.NewsFeedId, out fm) && fm.SingleMediaFeedModel != null && fm.LikeCommentShare != null)
                                    {
                                        model.LikeCommentShare = fm.LikeCommentShare;
                                    }
                                }
                                if (model.LikeCommentShare == null) model.LikeCommentShare = new LikeCommentShareModel();
                            }
                            model.LikeCommentShare.LoadData(mediaContent);
                            //model.UserTableID = utId;
                            MainSwitcher.AuthSignalHandler().feedSignalHandler.MediaContentDetails(model, cmntID, fromNotification);
                        }
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    //MainSwitcher.AuthSignalHandler().feedSignalHandler.LoadCommentUI();
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(Guid contentId, long utId, Guid cmntID, bool fromNotification = true)
        {
            if (!running)
            {
                this.contentId = contentId;
                this.utId = utId;
                this.cmntID = cmntID;
                this.fromNotification = fromNotification;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

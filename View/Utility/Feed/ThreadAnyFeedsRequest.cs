﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Threading;
using View.BindingModels;
using View.Utility.DataContainer;

namespace View.Utility.Feed
{
    public class ThreadAnyFeedsRequest
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThreadAnyFeedsRequest).Name);
        private long userTableId, pId;
        private short scl;
        private int startLimit, ptype, action;
        private string packetId;
        //private Guid pvtUUID = Guid.Empty; long time, 
        private object pvtObj;
        public delegate void CallBack(int response);
        public event CallBack callBackEvent;
        public Guid pvtGuid;

        private void Run()
        {
            try
            {
                JObject pakToSend = new JObject();
                if (packetId == null)
                    packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                //pakToSend[JsonKeys.Limit] = 10;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                //pakToSend[JsonKeys.ProfileType] = ptype;
                pakToSend[JsonKeys.Action] = action;
                if (pvtObj is Guid)
                {
                    pvtGuid = (Guid)pvtObj;
                    if (pvtGuid != Guid.Empty)
                        pakToSend[JsonKeys.PivotUUID] = (Guid)pvtObj;
                }
                else if (pvtObj is long)
                {
                    pakToSend[JsonKeys.PivotTime] = (long)pvtObj;
                }
                if (scl > 0)
                {
                    pakToSend[JsonKeys.Scroll] = scl;
                }
                pakToSend[JsonKeys.Limit] = 10;
                //if (userTableId > 0 && pId == 0)
                //{
                //    switch (ptype)
                //    {
                //        case SettingsConstants.PROFILE_TYPE_CIRCLE:
                //            pakToSend[JsonKeys.GroupId] = userTableId;
                //            break;
                //        default:
                //            pakToSend[JsonKeys.FutId] = userTableId;
                //            break;
                //    }
                //}
                //if (pId > 0 && (ptype == SettingsConstants.PROFILE_TYPE_NEWSPORTAL || ptype == SettingsConstants.PROFILE_TYPE_PAGES || ptype == SettingsConstants.PROFILE_TYPE_MUSICPAGE))
                //{
                //    pakToSend[JsonKeys.PageId] = pId;
                //}
                //pakToSend[JsonKeys.Time] = time;
                //if (userTableId > 0)
                //{
                //    pakToSend[JsonKeys.FutId] = userTableId;
                //}
                switch (ptype)
                {
                    case SettingsConstants.PROFILE_TYPE_CIRCLE:
                        if (userTableId > 0)
                        {
                            pakToSend[JsonKeys.WallOwnerId] = userTableId;
                            pakToSend[JsonKeys.WallOwnerType] = ptype;
                        }
                        break;
                    default:
                        if (userTableId > 0)
                            pakToSend[JsonKeys.WallOwnerId] = userTableId;
                        if (ptype != 100)
                            pakToSend[JsonKeys.WallOwnerType] = ptype;
                        break;
                }

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId, 1000);
                if (feedbackfields != null && (feedbackfields[JsonKeys.Success] != null || (feedbackfields[JsonKeys.Action] != null && (int)feedbackfields[JsonKeys.Action] == 200)))
                {
                    int response = SettingsConstants.RESPONSE_SUCCESS;
                    if (feedbackfields[JsonKeys.Success] != null && !(bool)feedbackfields[JsonKeys.Success])
                        response = SettingsConstants.RESPONSE_NOTSUCCESS;
                    if (scl != 1 && response == SettingsConstants.RESPONSE_SUCCESS && pvtGuid != Guid.Empty && !FeedDataContainer.Instance.AllRequestedPivotIds.Contains(pvtGuid))
                    {
                        FeedDataContainer.Instance.AllRequestedPivotIds.Add(pvtGuid);
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                    callBackEvent(response);
                    callBackEvent = null;
                    return;
                }
                else
                {
                    //if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
            callBackEvent(SettingsConstants.NO_RESPONSE);
            callBackEvent = null;
        }

        //public void StartThread(long time, short scl, int startLimit, int ptype, int action, string packetId, long userTableId, long pId)
        //{
        //    this.pvtUUID = Guid.Empty;
        //    this.time = time;
        //    this.scl = scl;
        //    this.startLimit = startLimit;
        //    this.ptype = ptype;
        //    this.action = action;
        //    this.packetId = packetId;
        //    this.userTableId = userTableId;
        //    this.pId = pId;
        //    Thread thrd = new System.Threading.Thread(Run);
        //    thrd.Start();
        //}

        //public void StartThread(Guid pvtUUID, short scl, int startLimit, int ptype, int action, string packetId, long userTableId, long pId)
        //{
        //    this.pvtUUID = pvtUUID;
        //    this.time = 0;
        //    this.scl = scl;
        //    this.startLimit = startLimit;
        //    this.ptype = ptype;
        //    this.action = action;
        //    this.packetId = packetId;
        //    this.userTableId = userTableId;
        //    this.pId = pId;
        //    Thread thrd = new System.Threading.Thread(Run);
        //    thrd.Start();
        //}
        public void StartThread(object pvtObj, short scl, int startLimit, int ptype, int action, string packetId, long userTableId, long pId)
        {
            this.pvtObj = pvtObj;
            this.scl = scl;
            this.startLimit = startLimit;
            this.ptype = ptype;
            this.action = action;
            this.packetId = packetId;
            this.userTableId = userTableId;
            this.pId = pId;
            Thread thrd = new System.Threading.Thread(Run);
            thrd.Start();
        }
    }
}

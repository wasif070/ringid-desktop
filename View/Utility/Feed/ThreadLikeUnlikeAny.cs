﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using View.BindingModels;
using View.Utility.DataContainer;
using View.ViewModel;

namespace View.Utility.Feed
{
    public class ThreadLikeUnlikeAny
    {
        /// <summary>
        /// TYPE_LIKE_STATUS = 184;
        /// ACTION_MERGED_LIKE_UNLIKE_COMMENT = 1123;
        /// </summary>
        
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThreadLikeUnlikeAny).Name);
        private bool running = false;
        private bool isSuccess = false;
        private string message = "";
        private bool imageToContentId;
        private JObject pakToSend;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                String packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (feedbackfields != null)
                {
                    try
                    {
                        if (feedbackfields[JsonKeys.Success] != null)
                        {
                            if ((bool)feedbackfields[JsonKeys.Success] || (feedbackfields[JsonKeys.ReasonCode] != null && ((int)feedbackfields[JsonKeys.ReasonCode]) == 34))
                            {
                                isSuccess = true;
                            }
                            if (feedbackfields[JsonKeys.Message] != null)
                            {
                                message = (string)feedbackfields[JsonKeys.Message];
                            }
                        }
                    }
                    finally { }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    message = NotificationMessages.INTERNET_UNAVAILABLE;
                    if (!isSuccess)
                    {
                        int activityType = (pakToSend[JsonKeys.ActivityType] != null) ? (int)pakToSend[JsonKeys.ActivityType] : 0;
                        Guid newsfeedId = (pakToSend[JsonKeys.NewsfeedId] != null) ? (Guid)pakToSend[JsonKeys.NewsfeedId] : Guid.Empty;
                        Guid contentId = (pakToSend[JsonKeys.ContentId] != null) ? (Guid)pakToSend[JsonKeys.ContentId] : Guid.Empty;
                        Guid commentId = (pakToSend[JsonKeys.CommentId] != null) ? (Guid)pakToSend[JsonKeys.CommentId] : Guid.Empty;
                        MainSwitcher.AuthSignalHandler().feedSignalHandler.OnLikeUnlikeFailed(newsfeedId, contentId, message, commentId, activityType);
                    }
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(JObject PacketToSend, bool ImageToContentId)
        {
            if (!running)
            {
                this.pakToSend = PacketToSend;
                this.imageToContentId = ImageToContentId;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }
        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

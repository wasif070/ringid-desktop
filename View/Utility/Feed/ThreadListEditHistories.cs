﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Threading;
using View.Utility.Auth;

namespace View.Utility.Feed
{
    public class ThreadListEditHistories
    {
        private Guid nfId, commentId;
        private int commentType;
        private readonly ILog log = LogManager.GetLogger(typeof(ThreadListEditHistories).Name);
        public delegate void OnSuccessHandler(int response);
        public event OnSuccessHandler OnSuccess;

        public void StartThread(Guid nfId, Guid commentId, int commentType)
        {
            this.commentId = commentId;
            this.commentType = commentType;
            this.nfId = nfId;
            Thread th = new Thread(new ThreadStart(run));
            th.Name = this.GetType().Name;
            th.Start();
        }

        public void run()
        {
            if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                try
                {
                    JObject pakToSend = new JObject();
                    string pakId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = pakId;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;

                    if (commentId != Guid.Empty)
                    {
                        pakToSend[JsonKeys.CommentId] = commentId;
                        pakToSend[JsonKeys.CommentType] = commentType;
                        if (nfId != Guid.Empty) pakToSend[JsonKeys.NewsfeedId] = nfId;
                        pakToSend[JsonKeys.Action] = AppConstants.TYPE_COMMENT_EDIT_HISTORY_LIST;
                    }
                    else
                    {
                        pakToSend[JsonKeys.NewsfeedId] = nfId;
                        pakToSend[JsonKeys.Action] = AppConstants.TYPE_NEWSFEED_EDIT_HISTORY_LIST;
                    }

                    JObject feedbackfields = SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakId);
                    if (feedbackfields != null && feedbackfields[JsonKeys.Success] != null)
                    {
                        if (OnSuccess != null)
                        {
                            OnSuccess(((bool)feedbackfields[JsonKeys.Success]) ? SettingsConstants.RESPONSE_SUCCESS : SettingsConstants.RESPONSE_NOTSUCCESS);
                            OnSuccess = null;
                        }
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
                        return;
                    }
                    else
                    {
                        if (!MainSwitcher.ThreadManager().PingNow())
                        {
                        }
                    }
                }
                catch (Exception e)
                {
                    log.Error(" ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            }
            else
            {
                log.Error(" Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            }
            if (OnSuccess != null)
            {
                OnSuccess(SettingsConstants.NO_RESPONSE);
                OnSuccess = null;
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Utility.DataContainer;

namespace View.Utility.Feed
{
    public class PreviousOrNextFeedComments
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(PreviousOrNextFeedComments).Name);
        private bool running = false;
        private long tm;
        private Guid cntId, imgId, nfid, albumId, pvtUUId;
        private int scl, st, action, activityType;

        #endregion "Private Fields"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = action;
                pakToSend[JsonKeys.ActivityType] = activityType;
                if (scl > 0)
                {
                    pakToSend[JsonKeys.Scroll] = scl;
                    pakToSend[JsonKeys.Time] = tm;
                }
                else pakToSend[JsonKeys.LogStartLimit] = st;

                if (pvtUUId != Guid.Empty)
                    pakToSend[JsonKeys.PivotUUID] = pvtUUId;


                if (activityType == SettingsConstants.ACTIVITY_ON_IMAGE)
                {
                    pakToSend[JsonKeys.ContentId] = imgId;
                }
                else if (activityType == SettingsConstants.ACTIVITY_ON_MULTIMEDIA)
                {
                    pakToSend[JsonKeys.ContentId] = cntId;
                }
                else
                {
                    FeedModel fm = null;
                    if (nfid != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(nfid, out fm))
                    {
                        pakToSend[JsonKeys.NewsfeedId] = nfid;
                        pakToSend[JsonKeys.Type] = fm.BookPostType;

                        if (fm.BookPostType == 2 || fm.BookPostType == 5 || fm.BookPostType == 8)
                        {
                            if(fm.SingleImageFeedModel != null)
                            {
                                pakToSend[JsonKeys.ContentId] = fm.SingleImageFeedModel.ImageId;
                            }
                            else
                            {
                                pakToSend[JsonKeys.ContentId] = fm.SingleMediaFeedModel.ContentId;
                            }
                            //if (cntId != Guid.Empty) pakToSend[JsonKeys.ContentId] = cntId;

                            //else if (imgId != Guid.Empty) pakToSend[JsonKeys.ContentId] = imgId;
                        }
                        else
                        {
                            if (fm.ImageList != null) albumId = fm.ImageList[0].AlbumId;
                            else if (fm.MediaContent != null) albumId = fm.MediaContent.AlbumId;

                            if (albumId != Guid.Empty)
                                pakToSend[JsonKeys.AlbumId] = albumId;
                        }
                    }
                }
                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
                if (feedbackfields != null)
                {
                    try
                    {
                        if (feedbackfields[JsonKeys.Success] != null && !(bool)feedbackfields[JsonKeys.Success])
                        {
                            if (scl == 1)
                                MainSwitcher.AuthSignalHandler().feedSignalHandler.HideNextCommentsLbl(nfid, cntId, imgId);
                            else //prev cmnts
                            {
                                if (imgId != Guid.Empty)
                                {
                                    string msg = "Unable to Load Comments!";
                                    if (feedbackfields[JsonKeys.Message] != null)
                                    {
                                        msg = (string)feedbackfields[JsonKeys.Message];
                                    }
                                    MainSwitcher.AuthSignalHandler().imageSignalHandler.FetchImageCommentsFromServer(imgId, null, nfid);
                                }
                                else if (cntId != Guid.Empty)
                                {
                                    string msg = "Unable to Load Comments!";
                                    if (feedbackfields[JsonKeys.Message] != null)
                                    {
                                        msg = (string)feedbackfields[JsonKeys.Message];
                                    }
                                    MainSwitcher.AuthSignalHandler().feedSignalHandler.LoadCommentList(null, nfid, Guid.Empty, cntId);
                                }
                                else
                                    MainSwitcher.AuthSignalHandler().feedSignalHandler.LoadFeedComments(null, nfid, 0, 0);
                            }
                        }
                    }
                    finally { }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    if (imgId != Guid.Empty)
                        MainSwitcher.AuthSignalHandler().imageSignalHandler.FetchImageCommentsUI();
                    else if (cntId != Guid.Empty)
                        //MainSwitcher.AuthSignalHandler().feedSignalHandler.LoadCommentUI();
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        //public void StartThread(int scl, long tm, Guid nfid, Guid cntId, Guid imgId, int action, Guid pvtUUID)
        //{
        //    if (!running)
        //    {
        //        this.imgId = imgId;
        //        this.pvtUUId = pvtUUID;
        //        this.cntId = cntId;
        //        this.tm = tm;
        //        this.nfid = nfid;
        //        this.scl = scl;
        //        this.action = action;
        //        System.Threading.Thread thrd = new System.Threading.Thread(Run);
        //        thrd.Start();
        //    }
        //}

        public void StartThread(int scl, long tm, Guid nfid, Guid cntId, Guid imgId, int action, Guid pvtUUID, int ActivityType)
        {
            if (!running)
            {
                this.imgId = imgId;
                this.pvtUUId = pvtUUID;
                this.cntId = cntId;
                this.tm = tm;
                this.nfid = nfid;
                this.scl = scl;
                this.action = action;
                this.activityType = ActivityType;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }
        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

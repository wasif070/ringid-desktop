﻿using Auth.utility;
using log4net;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Threading;

namespace View.Utility.Feed
{
    public class ThreadDeleteMediaOrAlbum
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ThreadDeleteMediaOrAlbum).Name);
        private Guid contentId, albumId;
        private int mediaType;
        public delegate void CallBack(int success);
        public event CallBack callBackEvent;

        public ThreadDeleteMediaOrAlbum(Guid contentId, Guid albumId, int mediaType)
        {
            this.contentId = contentId;
            this.albumId = albumId;
            this.mediaType = mediaType;
        }

        public void StartThread()
        {
            new Thread(new ThreadStart(Run)).Start();
        }

        public void Run()
        {
            if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                try
                {
                    JObject pakToSend = new JObject();
                    string pakId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = pakId;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;

                    if (contentId != Guid.Empty)
                    {
                        pakToSend[JsonKeys.Action] = AppConstants.TYPE_DELETE_MEDIA_CONTENT;
                        pakToSend[JsonKeys.ContentId] = contentId;
                    }
                    else
                    {
                        pakToSend[JsonKeys.Action] = AppConstants.TYPE_DELETE_MEDIA_ALBUM;
                    }
                    pakToSend[JsonKeys.AlbumId] = albumId;
                    pakToSend[JsonKeys.MediaType] = mediaType;
                    JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, pakId);
                    if (feedbackfields != null)
                    {
                        if (feedbackfields[JsonKeys.Success] != null)
                        {
                            callBackEvent(((bool)feedbackfields[JsonKeys.Success]) ? SettingsConstants.RESPONSE_SUCCESS : SettingsConstants.RESPONSE_NOTSUCCESS);
                            callBackEvent = null;
                            return;
                        }
                    }
                    else
                    {
                        callBackEvent(SettingsConstants.NO_RESPONSE);
                        callBackEvent = null;
                        MainSwitcher.ThreadManager().PingNow();
                    }
                }
                catch (Exception e)
                {
                    callBackEvent(SettingsConstants.NO_RESPONSE);
                    callBackEvent = null;
                    log.Error("ThreadDeleteMediaFromAlbum ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            }
            else
            {
                callBackEvent(SettingsConstants.NO_RESPONSE);
                callBackEvent = null;
                log.Error("ThreadDeleteMediaFromAlbum Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            }

        }
    }
}

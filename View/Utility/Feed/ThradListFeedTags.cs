﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json.Linq;

namespace View.Utility.Feed
{
    class ThradListFeedTags
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradListFeedTags).Name);
        private bool running = false;
        private Guid nfId;
        #endregion "Private Fields"

        #region "Constructors"

        public ThradListFeedTags()
        {

        }

        #endregion "Constructors"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_VIEW_TAGGED_LIST;
                pakToSend[JsonKeys.NewsfeedId] = nfId;
                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
                if (feedbackfields != null)
                {
                    bool succ = false;
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                    {
                        succ = true;
                    }
                    if (!succ)
                    {
                        string mg = "Failed!";
                        if (feedbackfields[JsonKeys.Message] != null)
                        {
                            mg = (string)feedbackfields[JsonKeys.Message];
                        }
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    if (MainSwitcher.ThreadManager().PingNow())
                    {
                       UIHelperMethods.ShowErrorMessageBoxFromThread(NotificationMessages.CAN_NOT_PROCESS, "Failed!");
                    }
                    else
                    {
                       UIHelperMethods.ShowErrorMessageBoxFromThread(NotificationMessages.INTERNET_UNAVAILABLE, "Failed!");
                    }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(Guid nfId)
        {
            if (!running)
            {
                this.nfId = nfId;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

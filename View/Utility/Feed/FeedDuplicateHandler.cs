﻿using Auth.utility;
using log4net;
using Models.Constants;
using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using View.BindingModels;
using View.ViewModel;

namespace View.Utility.Feed
{
    public class FeedDuplicateHandler : BackgroundWorker
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(FeedDuplicateHandler).Name);
        public static FeedDuplicateHandler Instance = new FeedDuplicateHandler();
        public int type = 0;
        public long frndOrCircleId;
        public CustomObservableCollection list;
        public FeedDuplicateHandler()
        {
            WorkerSupportsCancellation = true;
            WorkerReportsProgress = true;
            DoWork += FeedDuplicate_DowWork;
        }

        private void FeedDuplicate_DowWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                //if (list != null)
                //{
                //    int index = 0;
                //    if (list.Count > 1)
                //    {
                //        FeedModel lastModl = list[list.Count - 1];
                //        if (lastModl.FeedType != 1)
                //        {
                //            Application.Current.Dispatcher.Invoke((Action)delegate
                //            {
                //                if (list.IndexOf(lastModl) == list.Count - 1)
                //                {
                //                    list.Remove(lastModl);
                //                }
                //            }, DispatcherPriority.Send);
                //        }
                //    }
                //    while (index < list.Count - 1)
                //    {
                //        if (index > 0 && list[index].FeedType != 2)
                //        {
                //            FeedModel newStatusOrLoadMoreModel = list[index];
                //            Application.Current.Dispatcher.Invoke((Action)delegate
                //            {
                //                if ((newStatusOrLoadMoreModel.FeedType == 0 && list.IndexOf(newStatusOrLoadMoreModel) == 0)
                //                    || (newStatusOrLoadMoreModel.FeedType == 1 && list.IndexOf(newStatusOrLoadMoreModel) == list.Count - 1)
                //                    )
                //                { }
                //                else list.Remove(newStatusOrLoadMoreModel);
                //            }, DispatcherPriority.Send);
                //        }
                //        if (index < list.Count && ((index + 1) < list.Count) && list[index].FeedType == 2 && list[index + 1].FeedType == 2)
                //        {
                //            if (list[index].NewsfeedId == list[index + 1].NewsfeedId) //if duplicates found
                //            {
                //                FeedModel toRemove = list[index];
                //                Application.Current.Dispatcher.Invoke((Action)delegate
                //                {
                //                    int idx = list.IndexOf(toRemove);
                //                    list.Remove(toRemove);//list.RemoveAt(index);
                //                    if (type == DefaultSettings.FEED_TYPE_ALL)
                //                    {
                //                        int prevSeq, start;
                //                        if ((idx - 1) > 0)
                //                        {
                //                            prevSeq = list[idx - 1].SequenceForAllFeeds;
                //                            start = idx - 1;
                //                        }
                //                        else
                //                        {
                //                            prevSeq = 0;
                //                            start = 0;
                //                        }
                //                        for (int i = start; i < list.Count - 1; i++) //next e feeds thakle sob increment
                //                        {
                //                            list[i].SequenceForAllFeeds = prevSeq;
                //                            prevSeq++;
                //                        }
                //                    }
                //                }, DispatcherPriority.Send);
                //            }
                //            if (type == DefaultSettings.FEED_TYPE_ALL && list.Count > (index + 2) && (list[index].SequenceForAllFeeds + 1) != (list[index + 1].SequenceForAllFeeds)) //if Seq mismatch found
                //            {
                //                int correctSeq = list[index].SequenceForAllFeeds + 1;
                //                for (int i = index + 1; i < list.Count - 1; i++) //next e feeds thakle sob increment
                //                {
                //                    list[i].SequenceForAllFeeds = correctSeq;
                //                    correctSeq++;
                //                }
                //            }
                //        }
                //        index++;
                //    }
                //}
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
        }
        public void RemoveDuplicates()
        {
            try
            {
                if (IsBusy)
                {
                    CancelAsync();
                    //_resetEvent.WaitOne();
                }
                if (!IsBusy)
                    RunWorkerAsync();
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
        }
        //private AutoResetEvent _resetEvent = new AutoResetEvent(false);
    }
}

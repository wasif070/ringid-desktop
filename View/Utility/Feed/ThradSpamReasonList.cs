﻿using System;
using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System.Windows;

namespace View.Utility.Feed
{
    public class ThradSpamReasonList
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradSpamReasonList).Name);
        private bool running = false;
        int spam_type;
        private Func<bool, int> _OnComplete;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        public ThradSpamReasonList()
        { }
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_SPAM_REASON_LIST;
                pakToSend[JsonKeys.SpamType] = spam_type;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
                if (feedbackfields != null)
                {
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                    Callback(true);
                }
                else
                {
                    Callback(false);
                    if (!MainSwitcher.ThreadManager().PingNow()) {}
                }
            }
            catch (Exception)
            {
                Callback(false);
            }
            finally { running = false; }
        }

        private void Callback(bool status)
        {
            try
            {
                if (_OnComplete != null)
                {
                    _OnComplete(status);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in Callback() => " + ex.Message + "\n" + ex.StackTrace);
            }
            _OnComplete = null;
        }

        #endregion "Private methods"

        #region "Public Methods"

        public void StartThread(int spam_type, Func<bool, int> onComplete = null)
        {
            if (!running)
            {
                this.spam_type = spam_type;
                this._OnComplete = onComplete;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
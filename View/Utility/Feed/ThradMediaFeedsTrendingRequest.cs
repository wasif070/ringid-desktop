﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json.Linq;
namespace View.Utility.Feed
{
    public class ThradMediaFeedsTrendingRequest
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradMediaFeedsTrendingRequest).Name);
        private bool running = false;
        private long time;
        private short scl;
        private int limit;

        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                String packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_MEDIA_FEED;
                if (limit == -1)
                {
                    //DefaultSettings.START_MEDIAFEEDSTRENDING_PACKETID = packetId;
                    pakToSend[JsonKeys.Limit] = 10;
                }
                else pakToSend[JsonKeys.Limit] = limit;
                pakToSend[JsonKeys.StartLimit] = NewsFeedDictionaries.Instance.MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS.Count;
                pakToSend[JsonKeys.MediaTrendingFeed] = SettingsConstants.MEDIA_FEED_TYPE_TRENDING;

                if (scl > 0)
                {
                    pakToSend[JsonKeys.Scroll] = scl;
                }
                if (time > 0)
                {
                    pakToSend[JsonKeys.Time] = time;
                }


                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId, 1000);
                if (feedbackfields != null)
                {
                    try
                    {

                        if (feedbackfields != null && feedbackfields[JsonKeys.Success] != null)
                        {
                            if ((bool)feedbackfields[JsonKeys.Success])
                            {

                            }
                            else
                            {
                                if (scl == 0 || scl == 2)
                                {
                                    Thread.Sleep(2000);
                                    //MainSwitcher.AuthSignalHandler().feedSignalHandler.BottomLoadingMediaFeedTrending(StatusConstants.LOADING_NO_MORE_FEED_VISIBLE);
                                }
                            }
                        }
                        else
                        {
                        }
                    }
                    finally { }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(long time, short scl, int limit)
        {
            if (!running)
            {
                this.time = time;
                this.scl = scl;
                this.limit = limit;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }

        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
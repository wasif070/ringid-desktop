﻿using Auth.utility;
using Auth.utility.Feed;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using View.BindingModels;
using View.UI;
using View.UI.PopUp;
using View.Utility.DataContainer;
using View.ViewModel;

namespace View.Utility.Feed
{
    public class ThreadShareStatus
    {
        #region Private Fields

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThreadShareStatus).Name);
        private bool running = false;
        private string status;
        private int feedPrivacy;
        private Guid originalFeedId;
        private LocationDTO locationDTO;
        private List<long> taggedUtids;
        private long feedActivityId;
        private JArray statusTags;

        #endregion

        #region Properties

        public UCShareSingleFeedView ShareSingleFeedView
        {
            get
            {
                return MainSwitcher.PopupController.ShareSingleFeedView;
            }
        }

        #endregion

        #region Private methods

        private void Run()
        {
            try
            {
                running = true;
                JObject feedbackfields = null;
                JObject pakToSend = new JObject();
                string pakId = SendToServer.GetRanDomPacketID();

                pakToSend[JsonKeys.PacketId] = pakId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;

                pakToSend[JsonKeys.Status] = status;
                if (statusTags != null)
                {
                    pakToSend[JsonKeys.StatusTags] = statusTags;
                }
                pakToSend[JsonKeys.Type] = SettingsConstants.FEED_TYPE_TEXT;
                pakToSend[JsonKeys.SharedFeedId] = originalFeedId;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_SHARE_STATUS;

                if (locationDTO != null)
                {
                    JObject location = new JObject();
                    location[JsonKeys.Location] = locationDTO.LocationName;
                    location[JsonKeys.Latitude] = locationDTO.Latitude;
                    location[JsonKeys.Longitude] = locationDTO.Longitude;
                    pakToSend[JsonKeys.LocationDetails] = location;
                }

                if (taggedUtids != null)
                {
                    JArray array = new JArray();
                    foreach (long item in taggedUtids)
                    {
                        array.Add(item);
                    }
                    pakToSend[JsonKeys.TaggedFriendUtIds] = array;
                }

                if (this.feedActivityId > 0)
                {
                    JArray array = new JArray();
                    array.Add(feedActivityId);
                    pakToSend[JsonKeys.MoodIds] = array;
                }
                pakToSend[JsonKeys.FeedPrivacy] = feedPrivacy;

                string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
                System.Diagnostics.Debug.WriteLine(data);
                Dictionary<String, byte[]> packets = SendToServer.BuildBrokenPacket(data, AppConstants.TYPE_SHARE_STATUS, pakId);
                int packetType = packets.Count > 1 ? AppConstants.BROKEN_PACKET : AppConstants.SINGLE_PACKET;
                List<string> packetIds = new List<string>(packets.Keys);
                SendToServer.SendBrokenPacket(packetType, packets);
                Thread.Sleep(25);
                string makeResponsepak = pakId + "_" + AppConstants.TYPE_SHARE_STATUS;
                for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
                {
                    if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
                    {
                        break;
                    }
                    Thread.Sleep(DefaultSettings.WAITING_TIME);
                    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(makeResponsepak))
                    {
                        if (SendToServer.CheckAllBrokenPacketConfirmation(packets))
                        {
                            if (i % DefaultSettings.SEND_INTERVAL == 0)
                            {
                                SendToServer.SendBrokenPacket(packetType, packets);
                            }
                        }
                    }
                    else
                    {

                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(makeResponsepak, out feedbackfields);
                        bool success = false;
                        if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                        {
                            success = true;
                            FeedModel newExternalFeedModel = new FeedModel();
                            FeedModel originalFeed = FeedDataContainer.Instance.FeedModels[originalFeedId];
                            short orgIComment = (originalFeed.LikeCommentShare != null) ? originalFeed.LikeCommentShare.IComment : (short)0;
                            newExternalFeedModel.LoadData((JObject)feedbackfields[JsonKeys.NewsFeed]);
                            FeedDataContainer.Instance.FeedModels[newExternalFeedModel.NewsfeedId] = newExternalFeedModel;
                            originalFeed.LikeCommentShare.IShare = 1; //Force 1 due to server's change
                            originalFeed.LikeCommentShare.IComment = orgIComment; //Force 1 due to server's change

                            Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                FeedHolderModel prevExtModel = NewsFeedViewModel.Instance.AllFeedsViewCollection.Where(P => (P.Feed != null && P.Feed.ParentFeed != null && P.Feed.ParentFeed.NewsfeedId == originalFeed.NewsfeedId)).FirstOrDefault();
                                if (prevExtModel != null)
                                {
                                    //NewsFeedViewModel.Instance.AllFeedsViewCollection.Remove(prevExtModel);
                                    NewsFeedViewModel.Instance.DropModelandSeqViewCollection(prevExtModel);
                                }
                            }, DispatcherPriority.Send);

                            Application.Current.Dispatcher.Invoke((Action)(() =>
                            {
                                if (UCShareSingleFeedView.Instance != null && UCShareSingleFeedView.Instance.IsVisible)
                                {
                                    UCShareSingleFeedView.Instance.Hide();
                                    UCShareSingleFeedView.Instance.HideShareView();
                                }
                            }));

                            FeedHolderModel holder = new FeedHolderModel(2);
                            holder.FeedId = newExternalFeedModel.NewsfeedId;
                            holder.Feed = newExternalFeedModel;
                            holder.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                            holder.SelectViewTemplate();
                            NewsFeedViewModel.Instance.CalculateandInsertAllFeeds(holder);

                            FeedDataContainer.Instance.MyFeedsSortedIds.InsertIntoSortedList(newExternalFeedModel.ActualTime, newExternalFeedModel.NewsfeedId);
                            RingIDViewModel.Instance.ProfileMyCustomFeeds.CalculateandInsertOtherFeeds(holder, FeedDataContainer.Instance.ProfileMyCurrentIds);

                            List<Guid> extIds = null;
                            if (!FeedDataContainer.Instance.SharedNewsfeedIds.TryGetValue(originalFeedId, out extIds))
                            {
                                extIds = new List<Guid>();
                                FeedDataContainer.Instance.SharedNewsfeedIds[originalFeedId] = extIds;
                            }
                            extIds.Add(newExternalFeedModel.NewsfeedId);
                        }

                        if (!success)
                        {
                            string message = "Failed to share feed! ";
                            if (feedbackfields[JsonKeys.ReasonCode] != null)
                            {
                                int reasonCode = (int)feedbackfields[JsonKeys.ReasonCode];
                                if (reasonCode != 0)
                                    message = ReasonCodeConstants.GetReason((int)feedbackfields[JsonKeys.ReasonCode]);
                            }
                            UIHelperMethods.ShowErrorMessageBoxFromThread(message, "Failed!");
                        }
                        break;
                    }
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
                if (feedbackfields != null)
                {
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(makeResponsepak);
                    SendToServer.RemoveAllBrokenPacketConfirmation(packetIds);
                }
                Application.Current.Dispatcher.Invoke((Action)(() =>
                {
                    if (ShareSingleFeedView != null)
                    {
                        ShareSingleFeedView.EnableButtonsandLoader(true);
                    }
                }));
            }
            catch (Exception ex)
            {
                log.Error(" Thread Share Status , Message : " + ex.Message + " Stack Trace : " + ex.StackTrace);
            }
            finally
            {
                running = false;
            }
        }

        #endregion

        #region Public Methods

        public void StartThread(string status, JArray statusTags, Guid shareFeedId, LocationDTO locationDTO, List<long> taggedUtids, long feedActivityId, int feedPrivacy = 0)
        {
            if (!running)
            {
                this.status = status;
                this.feedPrivacy = feedPrivacy;
                this.statusTags = statusTags;
                this.originalFeedId = shareFeedId;
                this.locationDTO = locationDTO;
                this.taggedUtids = taggedUtids;
                this.feedActivityId = feedActivityId;
                System.Threading.Thread shareThread = new System.Threading.Thread(Run);
                shareThread.Start();
            }
        }

        #endregion
    }
}
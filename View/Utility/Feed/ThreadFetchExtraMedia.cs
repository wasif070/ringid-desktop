﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Threading;
using View.Utility.Auth;

namespace View.Utility.Feed
{
    public class ThreadFetchExtraMedia
    {
        private Guid nfId;
        private int limit, st;
        private readonly ILog log = LogManager.GetLogger(typeof(ThreadFetchExtraMedia).Name);
        public delegate void OnSuccessHandler(int response, JArray mediaArray);
        public event OnSuccessHandler CallBack;

        public void StartThread(Guid nfId, int st, int limit)
        {
            this.st = st;
            this.nfId = nfId;
            this.limit = limit;
            Thread th = new Thread(new ThreadStart(run));
            th.Name = this.GetType().Name;
            th.Start();
        }

        public void run()
        {
            if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                try
                {
                    JObject pakToSend = new JObject();
                    string pakId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = pakId;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_ACTION_GET_MORE_FEED_MEDIA;
                    pakToSend[JsonKeys.NewsfeedId] = nfId;
                    pakToSend[JsonKeys.StartLimit] = st;
                    pakToSend[JsonKeys.Scroll] = 2;
                    //pakToSend[JsonKeys.Limit] = limit;

                    JObject feedbackfields = SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakId);
                    if (feedbackfields != null && feedbackfields[JsonKeys.Success] != null)
                    {
                        if ((bool)feedbackfields[JsonKeys.Success])
                        {
                            JArray array = (JArray)feedbackfields[JsonKeys.MediaContentList];
                            if (CallBack != null)
                            {
                                CallBack(SettingsConstants.RESPONSE_SUCCESS, array);
                                CallBack = null;
                            }
                        }
                        else
                        {
                            if (CallBack != null)
                            {
                                CallBack(SettingsConstants.RESPONSE_NOTSUCCESS, null);
                                CallBack = null;
                            }
                        }
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
                        return;
                    }
                    else
                    {
                        if (!MainSwitcher.ThreadManager().PingNow())
                        {
                        }
                    }
                }
                catch (Exception e)
                {
                    log.Error(" ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            }
            else
            {
                log.Error(" Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            }
            if (CallBack != null)
            {
                CallBack(SettingsConstants.NO_RESPONSE, null);
                CallBack = null;
            }
        }

    }
}

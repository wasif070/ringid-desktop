﻿using Auth.utility;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading;

namespace View.Utility.Feed
{
    public class ThradEditFeed
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradEditFeed).Name);
        private bool running = false;
        private Guid NfId;
        private string sts;
        private long friendTableId;
        private long CircleId;
        private List<long> AddedTags, RemovedTags;
        private LocationDTO locationDTO;
        private string msg;
        private bool success = false;
        private JArray JobjStsTags;
        private int pvc;

        #endregion "Private Fields"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                string pakId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = pakId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_EDIT_STATUS;
                pakToSend[JsonKeys.NewsfeedId] = NfId;
                if (sts != null) pakToSend[JsonKeys.Status] = sts;
                if (pvc > 0) pakToSend[JsonKeys.FeedPrivacy] = pvc;
                if (JobjStsTags != null) pakToSend[JsonKeys.StatusTags] = JobjStsTags;
                //if (friendTableId > 0) pakToSend[JsonKeys.FriendId] = friendTableId;
                if (friendTableId > 0) pakToSend[JsonKeys.FutId] = friendTableId;
                if (CircleId > 0) pakToSend[JsonKeys.GroupId] = CircleId;
                if (AddedTags != null)
                {
                    JArray array = new JArray();
                    foreach (long item in AddedTags)
                    {
                        array.Add(item);
                    }
                    pakToSend[JsonKeys.TaggedFriendUtIds] = array;
                }
                if (RemovedTags != null)
                {
                    JArray array = new JArray();
                    foreach (long item in RemovedTags)
                    {
                        array.Add(item);
                    }
                    pakToSend[JsonKeys.RemovedTagsFriendUtIds] = array;
                }
                if (locationDTO != null)
                {
                    pakToSend[JsonKeys.Location] = locationDTO.LocationName;
                    pakToSend[JsonKeys.Latitude] = locationDTO.Latitude;
                    pakToSend[JsonKeys.Longitude] = locationDTO.Longitude;
                }
                JObject feedbackfields = null;
                string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
                Dictionary<String, byte[]> packets = SendToServer.BuildBrokenPacket(data, AppConstants.TYPE_EDIT_STATUS, pakId);
                int packetType = packets.Count > 1 ? AppConstants.BROKEN_PACKET : AppConstants.SINGLE_PACKET;
                List<string> packetIds = new List<string>(packets.Keys);
                SendToServer.SendBrokenPacket(packetType, packets);
                Thread.Sleep(25);
                string makeResponsepak = pakId + "_" + AppConstants.TYPE_EDIT_STATUS;
                for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
                {
                    if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
                    {
                        break;
                    }
                    Thread.Sleep(DefaultSettings.WAITING_TIME);
                    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(makeResponsepak))
                    {
                        if (SendToServer.CheckAllBrokenPacketConfirmation(packets))
                        {
                            if (i % DefaultSettings.SEND_INTERVAL == 0)
                                SendToServer.SendBrokenPacket(packetType, packets);
                        }
                    }
                    else
                    {

                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(makeResponsepak, out feedbackfields);
                        if (feedbackfields[JsonKeys.Success] != null)
                        {
                            success = (bool)feedbackfields[JsonKeys.Success];
                            List<StatusTagDTO> tagLst = (JobjStsTags != null) ? HelperMethodsModel.GetStatusTagListFromJArray(JobjStsTags) : null;
                            //MainSwitcher.AuthSignalHandler().feedSignalHandler.EditNewsFeed(success, NfId, sts, tagLst, friendTableId, CircleId, AddedTags, RemovedTags, locationDTO, pvc);
                            //FeedDTO feed = (success) ? HelperMethodsModel.BindFeedDetails(feedbackfields) : null;
                            //HelperMethodsAuth.NewsFeedHandlerInstance.EditFeed(feed);
                        }
                        if (feedbackfields[JsonKeys.Message] != null)
                        {
                            msg = (string)feedbackfields[JsonKeys.Message];
                        }
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
                        break;
                    }

                }
                if (!MainSwitcher.ThreadManager().PingNow())
                {
                    msg = NotificationMessages.INTERNET_UNAVAILABLE;
                }
                SendToServer.RemoveAllBrokenPacketConfirmation(packetIds);
                if (!success)
                {
                    if (string.IsNullOrEmpty(msg)) msg = "Failed to Edit the Feed!";
                    MainSwitcher.AuthSignalHandler().feedSignalHandler.OnEditFeedFailed(msg);
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }

        }
        #endregion "Private methods"

        #region "Public Methods"

        public void StartThread(int pvc, Guid NfId, long friendTableId, long CircleId, string sts, JArray JobjStsTags, List<long> AddedTags, List<long> RemovedTags, LocationDTO locationDTO)
        {

            if (!running)
            {
                this.friendTableId = friendTableId;
                this.CircleId = CircleId;
                this.NfId = NfId;
                this.pvc = pvc;
                this.sts = sts;
                this.JobjStsTags = JobjStsTags;
                this.AddedTags = AddedTags;
                this.RemovedTags = RemovedTags;
                this.locationDTO = locationDTO;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StartThread(int pvc, Guid NfId, string sts, JArray JobjStsTags, List<long> AddedTags, List<long> RemovedTags, LocationDTO locationDTO)
        {

            if (!running)
            {
                this.NfId = NfId;
                this.sts = sts;
                this.pvc = pvc;
                this.JobjStsTags = JobjStsTags;
                this.AddedTags = AddedTags;
                this.RemovedTags = RemovedTags;
                this.locationDTO = locationDTO;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StartThread(int pvc, Guid NfId, string sts, JArray JobjStsTags, long FriendIdentity, long CircleId, LocationDTO locationDTO)
        {

            if (!running)
            {
                this.NfId = NfId;
                this.sts = sts;
                this.pvc = pvc;
                this.JobjStsTags = JobjStsTags;
                this.friendTableId = FriendIdentity;
                this.CircleId = CircleId;
                this.locationDTO = locationDTO;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
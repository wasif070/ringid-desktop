﻿
using System.Collections.Generic;
using System.ComponentModel;
namespace View.Utility.Feed
{
    public class FeedImageInfo : INotifyPropertyChanged
    {

        public FeedImageInfo()
        {
            // _CurrentInstance = this;
        }
        public FeedImageInfo(double w, double h)
        {
            Width = w;
            Height = h;
            // _CurrentInstance = this;
        }

        #region Property

        //private FeedImageInfo _CurrentInstance;
        //public FeedImageInfo CurrentInstance
        //{
        //    get
        //    {
        //        return _CurrentInstance;
        //    }
        //}


        //public event PropertyChangedEventHandler PropertyChanged;
        //public void OnPropertyChanged(string propertyName)
        //{
        //    PropertyChangedEventHandler handler = PropertyChanged;
        //    if (handler != null)
        //    {
        //        handler(this, new PropertyChangedEventArgs(propertyName));
        //    }
        //}

        //private bool _NoImageFound = false;
        //public bool NoImageFound
        //{
        //    get
        //    {
        //        return _NoImageFound;
        //    }
        //    set
        //    {
        //        _NoImageFound = value;
        //    }
        //}
        //private long _NewsFeedId;
        //public long NewsFeedId
        //{
        //    get
        //    {
        //        return _NewsFeedId;
        //    }
        //    set
        //    {
        //        _NewsFeedId = value;
        //    }
        //}

        //private long _ImageID;
        //public long ImageID
        //{
        //    get
        //    {
        //        return _ImageID;
        //    }
        //    set
        //    {
        //        _ImageID = value;
        //    }
        //}

        //private string _Iurl;
        //public string Iurl
        //{
        //    get
        //    {
        //        return _Iurl;
        //    }
        //    set
        //    {
        //        _Iurl = value;
        //    }
        //}

        //private int _Index;
        //public int Index
        //{
        //    get
        //    {
        //        return _Index;
        //    }
        //    set
        //    {
        //        _Index = value;
        //    }
        //}

        private double _Width;
        public double Width
        {
            get
            {
                return _Width;
            }
            set
            {
                _Width = value;
                this.OnPropertyChanged("Width");
            }
        }

        private double _Height;
        public double Height
        {
            get
            {
                return _Height;
            }
            set
            {
                _Height = value;
                this.OnPropertyChanged("Height");
            }
        }

        private double _ReWidth;
        public double ReWidth
        {
            get
            {
                return _ReWidth;
            }
            set
            {
                _ReWidth = value;
                this.OnPropertyChanged("ReWidth");
            }
        }

        private double _ReHeight;
        public double ReHeight
        {
            get
            {
                return _ReHeight;
            }
            set
            {
                _ReHeight = value;
                this.OnPropertyChanged("ReHeight");
            }
        }

        private int _ShareParentWidth;
        public int ShareParentWidth
        {
            get
            {
                return _ShareParentWidth;
            }
            set
            {
                _ShareParentWidth = value;
                this.OnPropertyChanged("ShareParentWidth");
            }
        }

        private int _ShareParentHeight;
        public int ShareParentHeight
        {
            get
            {
                return _ShareParentHeight;
            }
            set
            {
                _ShareParentHeight = value;
                this.OnPropertyChanged("ShareParentHeight");
            }
        }

        private int _ShareParentReWidth;
        public int ShareParentReWidth
        {
            get
            {
                return _ShareParentReWidth;
            }
            set
            {
                _ShareParentReWidth = value;
                this.OnPropertyChanged("ShareParentReWidth");
            }
        }

        private int _ShareParentReHeight;
        public int ShareParentReHeight
        {
            get
            {
                return _ShareParentReHeight;
            }
            set
            {
                _ShareParentReHeight = value;
                this.OnPropertyChanged("ShareParentReHeight");
            }
        }

        private int _X;
        public int X
        {
            get
            {
                return _X;
            }
            set
            {
                _X = value;
                this.OnPropertyChanged("X");
            }
        }

        private int _Y;
        public int Y
        {
            get
            {
                return _Y;
            }
            set
            {
                _Y = value;
                this.OnPropertyChanged("Y");
            }
        }

        private int _Z;
        public int Z
        {
            get
            {
                return _Z;
            }
            set
            {
                _Z = value;
                this.OnPropertyChanged("Z");
            }
        }

        //private int _FitMode = FIT_TO_NONE;
        //public int FitMode
        //{
        //    get
        //    {
        //        return _FitMode;
        //    }
        //    set
        //    {
        //        _FitMode = value;
        //    }
        //}

        //private int _ImgType = TYPE_NORMAL;
        //public int ImgType
        //{
        //    get
        //    {
        //        FindRatioAndType();
        //        return _ImgType;
        //    }
        //}

        //private float? _Ratio;
        //public float? Ratio
        //{
        //    get
        //    {
        //        FindRatioAndType();
        //        return _Ratio;
        //    }
        //}

        #endregion Property

        //#region Utility Method

        //private void FindRatioAndType()
        //{
        //    if (_Ratio == null)
        //    {
        //        _Ratio = (float)_Width / (float)_Height;
        //        if (_Ratio >= 0.75 && _Ratio <= 1.34)
        //        {
        //            _ImgType = TYPE_NORMAL;
        //        }
        //        else if (_Ratio > 1.34)
        //        {
        //            _ImgType = TYPE_HORIZONTAL;
        //        }
        //        else if (_Ratio < 0.75)
        //        {
        //            _ImgType = TYPE_VERTICAL;
        //        }
        //    }
        //}

        //#endregion Utility Method

        //#region Static Members

        //public static int VIEW_TYPE_1 = 1;//N-1
        //public static int VIEW_TYPE_2 = 2;//V-2
        //public static int VIEW_TYPE_3 = 3;//H-3
        //public static int VIEW_TYPE_4 = 11;//NN-11
        //public static int VIEW_TYPE_5 = 22;//VV-22
        //public static int VIEW_TYPE_6 = 33;//HH-33
        //public static int VIEW_TYPE_7 = 12;//NV-12
        //public static int VIEW_TYPE_8 = 23;//HV-32
        //public static int VIEW_TYPE_9 = 31;//HN-31
        //public static int VIEW_TYPE_10 = 111;//NNN-111
        //public static int VIEW_TYPE_11 = 333;//HHH-333
        //public static int VIEW_TYPE_12 = 222;//VVV-222
        //public static int VIEW_TYPE_13 = 123;//NVH-123
        //public static int VIEW_TYPE_14 = 311;//HNN-311
        //public static int VIEW_TYPE_15 = 122;//NVV-122
        //public static int VIEW_TYPE_16 = 133;//NHH-133
        //public static int VIEW_TYPE_17 = 233;//VHH-233
        //public static int VIEW_TYPE_18 = 322;//HVV-322
        //public static int VIEW_TYPE_19 = 211;//VNN-211

        //public static int TYPE_NORMAL = 1;//N
        //public static int TYPE_VERTICAL = 2;//V
        //public static int TYPE_HORIZONTAL = 3;//H

        //public static int FIT_TO_NONE = 1;
        //public static int FIT_TO_WIDTH = 2;
        //public static int FIT_TO_HEIGHT = 3;

        //public static int[] SIEVE;
        //public static int[][] SIZE;

        //static FeedImageInfo()
        //{
        //    SIEVE = new int[334];
        //    SIZE = new int[334][];
        //    SIEVE[1] = VIEW_TYPE_1;
        //    SIEVE[2] = VIEW_TYPE_2;
        //    SIEVE[3] = VIEW_TYPE_3;
        //    SIEVE[11] = VIEW_TYPE_4;
        //    SIEVE[22] = VIEW_TYPE_5;
        //    SIEVE[33] = VIEW_TYPE_6;
        //    SIEVE[12] = VIEW_TYPE_7;
        //    SIEVE[21] = VIEW_TYPE_7;
        //    SIEVE[23] = VIEW_TYPE_8;
        //    SIEVE[32] = VIEW_TYPE_8;
        //    SIEVE[31] = VIEW_TYPE_9;
        //    SIEVE[13] = VIEW_TYPE_9;
        //    SIEVE[111] = VIEW_TYPE_10;
        //    SIEVE[333] = VIEW_TYPE_11;
        //    SIEVE[222] = VIEW_TYPE_12;
        //    SIEVE[123] = VIEW_TYPE_13;
        //    SIEVE[132] = VIEW_TYPE_13;
        //    SIEVE[231] = VIEW_TYPE_13;
        //    SIEVE[321] = VIEW_TYPE_13;
        //    SIEVE[213] = VIEW_TYPE_13;
        //    SIEVE[312] = VIEW_TYPE_13;
        //    SIEVE[311] = VIEW_TYPE_14;
        //    SIEVE[131] = VIEW_TYPE_14;
        //    SIEVE[113] = VIEW_TYPE_14;
        //    SIEVE[122] = VIEW_TYPE_15;
        //    SIEVE[212] = VIEW_TYPE_15;
        //    SIEVE[221] = VIEW_TYPE_15;
        //    SIEVE[133] = VIEW_TYPE_16;
        //    SIEVE[313] = VIEW_TYPE_16;
        //    SIEVE[331] = VIEW_TYPE_16;
        //    SIEVE[233] = VIEW_TYPE_17;
        //    SIEVE[323] = VIEW_TYPE_17;
        //    SIEVE[332] = VIEW_TYPE_17;
        //    SIEVE[322] = VIEW_TYPE_18;
        //    SIEVE[232] = VIEW_TYPE_18;
        //    SIEVE[223] = VIEW_TYPE_18;
        //    SIEVE[112] = VIEW_TYPE_19;
        //    SIEVE[121] = VIEW_TYPE_19;
        //    SIEVE[211] = VIEW_TYPE_19;

        //    SIZE[VIEW_TYPE_1] = new int[] { 0, 0, 0, 0, 0, 0 };
        //    SIZE[VIEW_TYPE_2] = new int[] { 0, 0, 0, 0, 0, 0 };
        //    SIZE[VIEW_TYPE_3] = new int[] { 0, 0, 0, 0, 0, 0 };
        //    SIZE[VIEW_TYPE_4] = new int[] { 300, 300, 300, 300, 0, 0 };
        //    SIZE[VIEW_TYPE_5] = new int[] { 300, 450, 300, 450, 0, 0 };
        //    SIZE[VIEW_TYPE_6] = new int[] { 600, 250, 600, 250, 0, 0 };
        //    SIZE[VIEW_TYPE_7] = new int[] { 400, 400, 200, 400, 0, 0 };
        //    SIZE[VIEW_TYPE_8] = new int[] { 450, 300, 150, 300, 0, 0 };
        //    SIZE[VIEW_TYPE_9] = new int[] { 400, 200, 200, 200, 0, 0 };
        //    SIZE[VIEW_TYPE_10] = new int[] { 400, 400, 200, 200, 200, 200 };
        //    SIZE[VIEW_TYPE_11] = new int[] { 0, 0, 0, 0, 0, 0 };
        //    SIZE[VIEW_TYPE_12] = new int[] { 200, 400, 200, 400, 200, 400 };
        //    SIZE[VIEW_TYPE_13] = new int[] { 350, 350, 250, 500, 350, 150 };
        //    SIZE[VIEW_TYPE_14] = new int[] { 600, 250, 300, 300, 300, 300 };
        //    SIZE[VIEW_TYPE_15] = new int[] { 300, 300, 150, 300, 150, 300 };
        //    SIZE[VIEW_TYPE_16] = new int[] { 225, 225, 600, 175, 350, 225 };
        //    SIZE[VIEW_TYPE_17] = new int[] { 200, 400, 400, 200, 400, 200 };
        //    SIZE[VIEW_TYPE_18] = new int[] { 300, 200, 150, 200, 150, 200 };
        //    SIZE[VIEW_TYPE_19] = new int[] { 300, 500, 300, 250, 300, 250 };
        //}

        //public static int FindViewType(List<FeedImageInfo> list)
        //{
        //    int friendCount = 0;

        //    for (int i = (list.Count - 1), mult = 1; i >= 0; i--, mult *= 10)
        //    {
        //        friendCount += list.ElementAt(i).ImgType * mult;
        //    }

        //    return friendCount;
        //}

        //public static FeedImageInfo[] GetSortedFeedImageArray(List<FeedImageInfo> list, int type)
        //{

        //    List<FeedImageInfo> _TempList = new List<FeedImageInfo>();
        //    foreach (FeedImageInfo temp in list)
        //    {
        //        _TempList.Add(temp);
        //    }

        //    FeedImageInfo[] imageArray = new FeedImageInfo[_TempList.Count];

        //    for (int idx = (_TempList.Count - 1), value = SIEVE[type]; idx >= 0; idx--)
        //    {

        //        int mod = value % 10;
        //        value = value / 10;

        //        for (int i = (_TempList.Count - 1); i < _TempList.Count; i--)
        //        {
        //            if (_TempList.ElementAt(i).ImgType == mod)
        //            {
        //                imageArray[idx] = _TempList.ElementAt(i);
        //                _TempList.RemoveAt(i);
        //                break;
        //            }
        //        }
        //    }

        //    return imageArray;
        //}

        //#endregion Static Members


        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.BindingModels;

namespace View.Utility.Feed
{
    public class ThreadHideSpecificFeed
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThreadHideSpecificFeed).Name);
        private bool running = false;
        //private Guid uuId;
        FeedModel feedModel;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        public ThreadHideSpecificFeed()
        {

        }
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_HIDE_UNHIDE_FEED;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.NewsfeedId] = feedModel.NewsfeedId;
                pakToSend[JsonKeys.Hide] = true;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                    {
                        MainSwitcher.AuthSignalHandler().feedSignalHandler.HideSpecificFeed(feedModel);
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                    }
                    else
                    {
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                       UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to hide this feed! Please Try later!", "Failed");
                    }
                }
                else
                {
                   UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to hide this feed! Please Try later!", "Failed");
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {
               UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to hide this feed! Please Try later!", "Failed");
            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(FeedModel feedModel)
        {
            if (!running)
            {
                this.feedModel = feedModel;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

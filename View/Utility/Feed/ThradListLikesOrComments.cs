﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Utility.DataContainer;

namespace View.Utility.Feed
{
    public class ThradListLikesOrComments
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradListLikesOrComments).Name);
        private bool running = false;
        private Guid nfId, cmntId, albumId;
        private int action;
        private int comment_like_number = 0;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = action;// AppConstants.TYPE_LIKES_FOR_STATUS;
                if (cmntId != Guid.Empty)
                    pakToSend[JsonKeys.CommentId] = cmntId;
                //pakToSend[JsonKeys.LogStartLimit] = comment_like_number;

                FeedModel fm = null;
                if (nfId != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(nfId, out fm))
                {
                    pakToSend[JsonKeys.NewsfeedId] = nfId;
                    pakToSend[JsonKeys.Type] = fm.BookPostType;
                    //pakToSend[JsonKeys.WallOwnerType] = fm.WallOrContentOwner.ProfileType;

                    if (fm.BookPostType == 1 || fm.BookPostType == 2 || fm.BookPostType == 5 || fm.BookPostType == 8)
                    {
                        if (fm.SingleMediaFeedModel != null) pakToSend[JsonKeys.ContentId] = fm.SingleMediaFeedModel.ContentId;
                        else if (fm.SingleImageFeedModel != null) pakToSend[JsonKeys.ContentId] = fm.SingleImageFeedModel.ImageId;
                    }
                    else
                    {
                        if (fm.ImageList != null) albumId = fm.ImageList[0].AlbumId;
                        else if (fm.MediaContent != null) albumId = fm.MediaContent.AlbumId;

                        if (albumId != Guid.Empty)
                            pakToSend[JsonKeys.AlbumId] = albumId;
                    }
                }
                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                    {

                    }
                    else
                    {
                        if (action == AppConstants.TYPE_LIKES_FOR_STATUS || action == AppConstants.TYPE_LIST_LIKES_OF_COMMENT)
                        {
                            MainSwitcher.AuthSignalHandler().feedSignalHandler.OnListLikesorCommentsFailed();
                        }
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"

        public void StartThread(Guid nfId, int action, int comment_like_number)
        {
            if (!running)
            {
                this.nfId = nfId;
                this.action = action;
                this.comment_like_number = comment_like_number;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StartThread(Guid nfId, Guid cmntId, int action, int comment_like_number)
        {
            if (!running)
            {
                this.nfId = nfId;
                this.cmntId = cmntId;
                this.action = action;
                this.comment_like_number = comment_like_number;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Threading;

namespace View.Utility.Feed
{
    public class ThreadMediaCloudItemsSearch
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ThreadMediaCloudItemsSearch).Name);
        private string SearchParam;
        private int waitingTime = 0, Searchtype, Scl, st;
        private Guid Pvtid;
        private long hashTagPvt = 0;
        public delegate void CallBack(bool success);
        public event CallBack CallBackEvent;

        public void StartThread(string searchParam, int searchtype, int st, Guid pvtid)//later pvtid,scl has to be parameterized
        {
            this.SearchParam = searchParam;
            Searchtype = searchtype;
            this.st = st;
            Pvtid = pvtid;
            new Thread(new ThreadStart(Run)).Start();
        }
        //public void StartThread(string searchParam, int searchtype, int scl, long pvtid)//later pvtid,scl has to be parameterized
        //{
        //    this.SearchParam = searchParam;
        //    Searchtype = searchtype;
        //    Scl = scl;
        //    hashTagPvt = pvtid;
        //    new Thread(new ThreadStart(Run)).Start();
        //}
        public void Run()
        {
            if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                try
                {
                    JObject pakToSend = new JObject();
                    string pakId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = pakId;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_MEDIA_CONTENTS_BASED_ON_KEYWORD;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.SearchParam] = SearchParam;
                    pakToSend[JsonKeys.MediaSuggestionType] = Searchtype;
                    //pakToSend[JsonKeys.Scroll] = 0;//1;//for now
                    pakToSend[JsonKeys.StartLimit] = st;
                    pakToSend[JsonKeys.Limit] = 10;
                    //if (Pvtid != Guid.Empty)
                    //    pakToSend[JsonKeys.PivotId] = Pvtid;//0;//for now
                    //else
                    //    pakToSend[JsonKeys.PivotId] = hashTagPvt;
                    JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakId);
                    if (feedbackfields != null)
                    {
                        if (feedbackfields[JsonKeys.Success] != null && !(bool)feedbackfields[JsonKeys.Success])
                        {
                        }
                        else
                        {
                            while (feedbackfields[JsonKeys.Action] != null && (int)feedbackfields[JsonKeys.Action] != AppConstants.TYPE_MEDIA_CONTENTS_BASED_ON_KEYWORD)
                            {
                                if (waitingTime == DefaultSettings.TRYING_TIME)
                                {
                                    break;
                                }
                                Thread.Sleep(DefaultSettings.WAITING_TIME);
                                RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out feedbackfields);
                                waitingTime++;
                                if (feedbackfields[JsonKeys.Success] != null && !(bool)feedbackfields[JsonKeys.Success])
                                {
                                    CallBackEvent(false);
                                    CallBackEvent = null;
                                    return;
                                }
                            }
                            CallBackEvent(true);
                            CallBackEvent = null;
                            return;
                        }
                    }
                    else
                    {
                        MainSwitcher.ThreadManager().PingNow();
                    }
                }
                catch (Exception e)
                {
                    log.Error("ThreadMediaCloudItemsSearch ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            }
            else
            {
                log.Error("ThreadMediaCloudItemsSearch Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            }
            CallBackEvent(false);
            CallBackEvent = null;
        }
    }
}

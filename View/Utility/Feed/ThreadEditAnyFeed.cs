﻿using Auth.Service.Images;
using Auth.utility;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using View.BindingModels;
using View.Utility.DataContainer;
using View.Utility.WPFMessageBox;
using System.Linq;
using View.ViewModel;
using View.Constants;
using View.UI;

namespace View.Utility.Feed
{
    public class ThreadEditAnyFeed
    {
        #region Private Fields

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThreadEditAnyFeed).Name);
        private bool running = false;
        private bool success = false;
        private string message = string.Empty;

        private Guid NfId;
        private string EditedStatus;
        private JArray EditedInnerTagsJArray, AddedOuterTagsJArray, RemovedOuterTagsJArray;
        private LocationModel AddedLocation;
        private int EditedPrivacy, EditedValidity;
        private JArray moodId;

        private WebClient webClient;
        private string serverUrl;
        private byte[] wholebytes, tmpPostData;

        private List<ImageUploaderModel> AddedImages;
        private List<MusicUploaderModel> AddedMusics;
        private List<FeedVideoUploaderModel> AddedVideos;
        private JArray EditedImagesJArray, EditedMediasJArray, RemovedImagesJArray, RemovedMediasJArray, AddedHashTagsJArray;

        public delegate void CallBack(bool success, string message);
        public event CallBack callBackEvent;

        #endregion

        #region "Constructors"

        public void SetEditedFields(Guid NfId, string EditedStatus, JArray EditedInnerTagsJArray, JArray AddedOuterTagsJArray, JArray RemovedOuterTagsJArray, int EditedPrivacy, LocationModel AddedLocation, int EditedValidity, List<ImageUploaderModel> AddedImages, JArray EditedImagesJArray, JArray RemovedImagesJArray, List<MusicUploaderModel> AddedMusics, List<FeedVideoUploaderModel> AddedVideos, JArray EditedMediasJArray, JArray RemovedMediasJArray, JArray AddedHashTagsJArray, JArray moodId)//TODO validity
        {
            this.NfId = NfId;
            this.EditedStatus = EditedStatus;
            this.EditedInnerTagsJArray = (EditedInnerTagsJArray == null) ? new JArray() : EditedInnerTagsJArray;
            this.AddedOuterTagsJArray = AddedOuterTagsJArray;
            this.RemovedOuterTagsJArray = RemovedOuterTagsJArray;
            this.EditedPrivacy = EditedPrivacy;
            this.AddedLocation = AddedLocation;
            this.EditedValidity = EditedValidity;

            this.AddedImages = AddedImages;
            this.EditedImagesJArray = EditedImagesJArray;
            this.RemovedImagesJArray = RemovedImagesJArray;

            this.AddedMusics = AddedMusics;
            this.AddedVideos = AddedVideos;
            this.EditedMediasJArray = EditedMediasJArray;
            this.RemovedMediasJArray = RemovedMediasJArray;
            this.AddedHashTagsJArray = null;//AddedHashTagsJArray;
            this.moodId = moodId;

            success = false;
            message = string.Empty;
        }
        #endregion "Constructors"

        #region Private methods

        private void Run()
        {
            running = true;
            ContinueToAuthServer();
        }

        private void UploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {
            try
            {
                if (e.UserState is ImageUploaderModel)
                {
                    ImageUploaderModel model = (ImageUploaderModel)e.UserState;
                    int Pecentage = (int)(((double)e.BytesSent / (double)e.TotalBytesToSend) * 100);
                    model.Pecentage = Pecentage;
                }
                else if (e.UserState is MusicUploaderModel)
                {
                    MusicUploaderModel model = (MusicUploaderModel)e.UserState;
                    int Pecentage = (int)(((double)e.BytesSent / (double)e.TotalBytesToSend) * 100);
                    model.Pecentage = Pecentage;
                }
                else if (e.UserState is FeedVideoUploaderModel)
                {
                    FeedVideoUploaderModel model = (FeedVideoUploaderModel)e.UserState;
                    int Pecentage = (int)(((double)e.BytesSent / (double)e.TotalBytesToSend) * 100);
                    model.Pecentage = Pecentage;
                }
            }
            catch (Exception)
            {
            }
        }

        private void UploadDataCompleted(object sender, UploadDataCompletedEventArgs e)
        {
            try
            {
                if (e.UserState is ImageUploaderModel)
                {
                    ImageUploaderModel model = (ImageUploaderModel)e.UserState;
                    tmpPostData = null;
                    string response = Encoding.UTF8.GetString(e.Result);
                    JObject responseObj = JObject.Parse(response);
                    if (responseObj != null && responseObj[JsonKeys.ImageUrl] != null && responseObj[JsonKeys.Success] != null && (bool)responseObj[JsonKeys.Success])
                    {
                        model.ImageUrl = (string)responseObj[JsonKeys.ImageUrl];
                        model.IsUploading = false;
                        model.IsUploadedInImageServer = true;
                        model = AddedImages.Where(P => string.IsNullOrEmpty(P.ImageUrl)).FirstOrDefault();
                        if (model == null) ContinueToAuthServer();
                        else
                        {
                            model.IsUploading = true;
                            model.Pecentage = 0;
                            UploadToImageOrMediaServer(model, model.FilePath);
                        }
                    }
                    else ContinueToAuthServer(false);
                }
                else if (e.UserState is MusicUploaderModel)
                {
                    MusicUploaderModel model = (MusicUploaderModel)e.UserState;
                    if (e.Result != null && e.Result.Length > 0 && (int)e.Result[0] == 1)
                    {
                        int urlLength = (int)e.Result[1];
                        model.StreamUrl = Encoding.UTF8.GetString(e.Result, 2, urlLength);
                        if (model.IsImageFound && model.ImageWidth > 0 && model.ImageHeight > 0)
                        {
                            ThumbImageUpload(model);
                        }
                        model.IsUploading = false;
                        model.IsUploadedInAudioServer = true;
                        model = AddedMusics.Where(P => string.IsNullOrEmpty(P.StreamUrl)).FirstOrDefault();
                        if (model == null) ContinueToAuthServer();
                        else
                        {
                            model.IsUploading = true;
                            model.Pecentage = 0;
                            UploadToImageOrMediaServer(model, model.FilePath);
                        }
                    }
                    else ContinueToAuthServer(false);
                }
                else if (e.UserState is FeedVideoUploaderModel)
                {
                    FeedVideoUploaderModel model = (FeedVideoUploaderModel)e.UserState;
                    if (e.Result != null && e.Result.Length > 0 && (int)e.Result[0] == 1)
                    {
                        int urlLength = (int)e.Result[1];
                        model.StreamUrl = Encoding.UTF8.GetString(e.Result, 2, urlLength);
                        if (!string.IsNullOrEmpty(model.StreamUrl)) model.ThumbUrl = model.StreamUrl.Replace(".mp4", ".jpg");
                        model.IsUploading = false;
                        model.IsUploadedInVideoServer = true;
                        model = AddedVideos.Where(P => string.IsNullOrEmpty(P.StreamUrl)).FirstOrDefault();
                        if (model == null) ContinueToAuthServer();
                        else
                        {
                            model.IsUploading = true;
                            model.Pecentage = 0;
                            UploadToImageOrMediaServer(model, model.FilePath);
                        }
                    }
                    else ContinueToAuthServer(false);
                }
                else ContinueToAuthServer(false);
            }
            catch (Exception)
            {
                ContinueToAuthServer(false);
            }
        }

        private void UploadToImageOrMediaServer(object currentUploadmodel, string filePath)
        {
            try
            {
                string _lineEnd = "\r\n";
                string _twoHyphens = "--";
                string _boundary = "*****";

                webClient.Headers["Content-Type"] = "multipart/form-data; boundary=" + _boundary;
                webClient.Headers["User-Agent"] = "ringID" + "_" + DefaultSettings.VALUE_APP_INSTALLED_VERSION;
                webClient.Headers["access-control-allow-origin"] = "*";

                var firstPart = "";
                string fileheaderTemplate = "Content-Disposition: form-data; name=\"{0}\"" + _lineEnd + _lineEnd + "{1}" + _lineEnd;

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "sId", DefaultSettings.LOGIN_SESSIONID);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "uId", DefaultSettings.LOGIN_RING_ID);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "authServer", ServerAndPortSettings.AUTH_SERVER_IP);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "comPort", ServerAndPortSettings.COMMUNICATION_PORT);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "x-app-version", AppConfig.VERSION_PC);
                if (currentUploadmodel is ImageUploaderModel)
                {
                    ImageUploaderModel model = (ImageUploaderModel)currentUploadmodel;
                    firstPart += _twoHyphens + _boundary + _lineEnd;
                    firstPart += string.Format(fileheaderTemplate, "iw", model.Width);

                    firstPart += _twoHyphens + _boundary + _lineEnd;
                    firstPart += string.Format(fileheaderTemplate, "ih", model.Height);

                    firstPart += _twoHyphens + _boundary + _lineEnd;
                    firstPart += string.Format(fileheaderTemplate, "imT", SettingsConstants.TYPE_NORMAL_BOOK_IMAGE);

                    firstPart += _twoHyphens + _boundary + _lineEnd;
                    firstPart += "Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + "pcupload.jpg" + "\"" + _lineEnd;
                    firstPart += _lineEnd;
                    byte[] firstBytes = webClient.Encoding.GetBytes(firstPart);

                    var lastPart = "";
                    lastPart += _lineEnd;
                    lastPart += _twoHyphens + _boundary + _twoHyphens + _lineEnd;
                    byte[] lastBytes = webClient.Encoding.GetBytes(lastPart);
                    ////////
                    int w = 0, h = 0;
                    System.Drawing.Image image = null;
                    long n;

                    image = ImageUtility.GetOrientedImageFromFilePath(filePath);//System.Drawing.Image.FromFile(@model.FilePath);
                    if (image == null)
                    {
                        UIHelperMethods.ShowWarning("Image Not found or Corrupted! ", "Image corrupted");
                        //CustomMessageBox.ShowError("Image Not found or Corrupted! ");
                        return;
                    }
                    tmpPostData = null;
                    if (long.TryParse(filePath, out n))
                    {
                        w = (int)model.CopiedBitmapImage.Width;
                        h = (int)model.CopiedBitmapImage.Height;
                        model.Height = h;
                        model.Width = w;
                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.Frames.Add(BitmapFrame.Create(model.CopiedBitmapImage));
                        using (MemoryStream ms = new MemoryStream())
                        {
                            encoder.Save(ms);
                            tmpPostData = ms.ToArray();
                        }
                    }
                    else
                    {
                        tmpPostData = ImageUtility.ImageResizeandQuality(image, 75, out w, out h);
                        model.Height = h;
                        model.Width = w;
                        image.Dispose();
                    }

                    int fileLength = tmpPostData.Length;
                    long length = firstBytes.Length + fileLength + lastBytes.Length;
                    wholebytes = new Byte[length];
                    int currentbytes = 0;

                    Buffer.BlockCopy(firstBytes, 0, wholebytes, currentbytes, firstBytes.Length);
                    currentbytes += firstBytes.Length;

                    Buffer.BlockCopy(tmpPostData, 0, wholebytes, currentbytes, fileLength);
                    currentbytes += fileLength;
                    if (tmpPostData != null) { GC.SuppressFinalize(tmpPostData); tmpPostData = null; }

                    Buffer.BlockCopy(lastBytes, 0, wholebytes, currentbytes, lastBytes.Length);
                    currentbytes += lastBytes.Length;
                }
                else
                {
                    firstPart += _twoHyphens + _boundary + _lineEnd;
                    firstPart += "Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + Path.GetFileName(filePath) + "\"" + _lineEnd;
                    firstPart += _lineEnd;
                    byte[] firstBytes = webClient.Encoding.GetBytes(firstPart);

                    var lastPart = "";
                    lastPart += _lineEnd;
                    lastPart += _twoHyphens + _boundary + _twoHyphens + _lineEnd;
                    byte[] lastBytes = webClient.Encoding.GetBytes(lastPart);


                    FileStream oFileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                    int fileLength = (int)oFileStream.Length;
                    long length = firstBytes.Length + fileLength + lastBytes.Length;
                    wholebytes = new Byte[length];
                    int currentbytes = 0;

                    Buffer.BlockCopy(firstBytes, 0, wholebytes, currentbytes, firstBytes.Length);
                    currentbytes += firstBytes.Length;

                    oFileStream.Read(wholebytes, currentbytes, fileLength);
                    currentbytes += fileLength;
                    oFileStream.Close();

                    Buffer.BlockCopy(lastBytes, 0, wholebytes, currentbytes, lastBytes.Length);
                    currentbytes += lastBytes.Length;

                }
                webClient.UploadDataAsync(new Uri(serverUrl), "POST", wholebytes, currentUploadmodel);
                if (wholebytes != null) { GC.SuppressFinalize(wholebytes); wholebytes = null; }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
                if (wholebytes != null) { GC.SuppressFinalize(wholebytes); wholebytes = null; }
                if (tmpPostData != null) { GC.SuppressFinalize(tmpPostData); tmpPostData = null; }
                if (webClient != null) { GC.SuppressFinalize(webClient); webClient = null; }
            }
        }

        private async void ThumbImageUpload(MusicUploaderModel model)
        {
            try
            {
                TagLib.File file = TagLib.File.Create(model.FilePath);
                byte[] response = await FeedImagesUpload.Instance.UploadThumbImageToImageServerWebRequest(file.Tag.Pictures[0].Data.Data, model.ImageWidth, model.ImageHeight);
                if (response != null && response[0] == 1)
                {
                    int len = response[1];
                    string ArtImageUrl = Encoding.UTF8.GetString(response, 2, len);
                    model.ThumbUrl = ArtImageUrl;
                }
            }
            catch (Exception)
            {
            }
        }

        private void ContinueToAuthServer(bool UploadCompleteOrNotNecessary = true)
        {
            try
            {
                if (wholebytes != null) { GC.SuppressFinalize(wholebytes); wholebytes = null; }
                if (tmpPostData != null) { GC.SuppressFinalize(tmpPostData); tmpPostData = null; }
                if (UploadCompleteOrNotNecessary && !string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
                {
                    JObject pakToSend = new JObject();
                    string pakId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = pakId;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_EDIT_STATUS;
                    pakToSend[JsonKeys.NewsfeedId] = NfId;
                    //inside status
                    if (moodId != null)
                    {
                        pakToSend[JsonKeys.MoodIds] = moodId;
                    }
                    if (EditedStatus != null)
                    {
                        pakToSend[JsonKeys.Status] = EditedStatus;
                    }
                    pakToSend[JsonKeys.StatusTags] = EditedInnerTagsJArray;
                    //outsidetags
                    if (AddedOuterTagsJArray != null)
                    {
                        pakToSend[JsonKeys.TaggedFriendUtIds] = AddedOuterTagsJArray;
                    }
                    if (RemovedOuterTagsJArray != null)
                    {
                        pakToSend[JsonKeys.RemovedTagsFriendUtIds] = RemovedOuterTagsJArray;
                    }
                    //location
                    JObject locationDetails = new JObject();
                    if (AddedLocation != null)
                    {
                        locationDetails[JsonKeys.Location] = AddedLocation.LocationName;
                        if (!string.IsNullOrEmpty(AddedLocation.LocationName))
                        {
                            locationDetails[JsonKeys.Latitude] = AddedLocation.Latitude;
                            locationDetails[JsonKeys.Longitude] = AddedLocation.Longitude;
                        }
                        pakToSend[JsonKeys.LocationDetails] = locationDetails;
                    }
                    //validity
                    if (EditedValidity != 0)
                    {
                        pakToSend[JsonKeys.Validity] = EditedValidity;
                    }
                    pakToSend[JsonKeys.WallOwnerType] = SettingsConstants.WALL_OWNER_TYPE_DEFAULT_USER;
                    string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
                    Console.Write(pakToSend);
                    Dictionary<String, byte[]> packets = SendToServer.BuildBrokenPacket(data, (int)pakToSend[JsonKeys.Action], pakId);
                    int packetType = packets.Count > 1 ? AppConstants.BROKEN_PACKET : AppConstants.SINGLE_PACKET;
                    List<string> packetIds = new List<string>(packets.Keys);
                    SendToServer.SendBrokenPacket(packetType, packets);
                    Thread.Sleep(25);
                    string makeResponsepak = pakId + "_" + (int)pakToSend[JsonKeys.Action];

                    JObject feedbackfields = null;
                    for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
                    {
                        if (string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
                        {
                            break;
                        }
                        Thread.Sleep(DefaultSettings.WAITING_TIME);
                        if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(makeResponsepak, out feedbackfields))
                        {
                            if (SendToServer.CheckAllBrokenPacketConfirmation(packets))
                            {
                                if (i % DefaultSettings.SEND_INTERVAL == 0)
                                {
                                    SendToServer.SendBrokenPacket(packetType, packets);
                                }
                            }
                        }
                        else
                        {
                            if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                            {
                                success = true;
                                FeedModel feedModel = null;
                                feedbackfields = (JObject)feedbackfields[JsonKeys.NewsFeed];
                                if (FeedDataContainer.Instance.FeedModels.TryGetValue(NfId, out feedModel))
                                {
                                    short BookPostType = (feedbackfields[JsonKeys.BookPostType] != null) ? (short)feedbackfields[JsonKeys.BookPostType] : (short)0;
                                    if (BookPostType < 2 //!= SettingsConstants.NEWS_FEED_TYPE_AUDIO && BookPostType != SettingsConstants.NEWS_FEED_TYPE_VIDEO && BookPostType != SettingsConstants.NEWS_FEED_TYPE_IMAGE
                                        && AddedImages == null && AddedMusics == null && AddedVideos == null && EditedImagesJArray == null && EditedMediasJArray == null
                                        && RemovedImagesJArray == null && RemovedMediasJArray == null)
                                    {
                                        if (feedbackfields[JsonKeys.Status] != null)
                                        {
                                            feedModel.Status = (string)feedbackfields[JsonKeys.Status];
                                            feedModel.OnPropertyChanged("Status");
                                        }
                                        if (feedbackfields[JsonKeys.StatusTags] != null)
                                        {
                                            JArray stsTags = (JArray)feedbackfields[JsonKeys.StatusTags];
                                            feedModel.StatusTags = new ObservableCollection<TaggedUserModel>();
                                            foreach (JObject obj in stsTags)
                                            {
                                                TaggedUserModel singleTagModel = new TaggedUserModel();
                                                if (obj[JsonKeys.UserIdentity] != null) singleTagModel.UserIdentity = (long)obj[JsonKeys.UserIdentity];//not needed eventually
                                                if (obj[JsonKeys.UserTableID] != null) singleTagModel.UserTableID = (long)obj[JsonKeys.UserTableID];
                                                if (obj[JsonKeys.FullName] != null) singleTagModel.FullName = (string)obj[JsonKeys.FullName];
                                                if (obj[JsonKeys.Position] != null) singleTagModel.Index = (int)obj[JsonKeys.Position];
                                                if (obj[JsonKeys.ProfileImage] != null) singleTagModel.ProfileImage = (string)obj[JsonKeys.ProfileImage]; //not needed eventually
                                                feedModel.StatusTags.InvokeAdd(singleTagModel);
                                            }
                                            feedModel.OnPropertyChanged("StatusTags");
                                        }
                                        //outsidetags
                                        if (feedbackfields[JsonKeys.FriendsTagList] != null)
                                        {
                                            JArray array = (JArray)feedbackfields[JsonKeys.FriendsTagList];
                                            feedModel.TaggedFriendsList = new ObservableCollection<BaseUserProfileModel>();

                                            feedModel.SecondTagProfile = null;
                                            feedModel.FirstTagProfile = null;

                                            foreach (JObject obj in array)
                                            {
                                                if (obj[JsonKeys.UserTableID] != null && obj[JsonKeys.Name] != null)
                                                {
                                                    string taggedFriendName = (string)obj[JsonKeys.Name];
                                                    long taggedFriendUtId = (long)obj[JsonKeys.UserTableID];
                                                    long taggedFriendUId = (obj[JsonKeys.UserIdentity] != null) ? (long)obj[JsonKeys.UserIdentity] : 0;
                                                    string taggedFriendProfileImage = (string)obj[JsonKeys.ProfileImage];
                                                    UserShortInfoModel baseModel = new UserShortInfoModel
                                                    {
                                                        FullName = taggedFriendName,
                                                        UserTableID = taggedFriendUtId,
                                                        UserIdentity = taggedFriendUId,
                                                        ProfileImage = taggedFriendProfileImage,
                                                        ProfileType = SettingsConstants.PROFILE_TYPE_GENERAL
                                                    };

                                                    if (feedModel.FirstTagProfile != null && feedModel.FirstTagProfile.UserTableID != baseModel.UserTableID && array.Count == 1)
                                                    {
                                                        feedModel.FirstTagProfile = null;
                                                    }
                                                    if (feedModel.FirstTagProfile == null)
                                                    {
                                                        feedModel.FirstTagProfile = baseModel;
                                                        //todo use GetUserProfile when generalized
                                                    }
                                                    else if (feedModel.SecondTagProfile == null && array.Count == 2)
                                                    {
                                                        feedModel.SecondTagProfile = baseModel;
                                                    }
                                                    feedModel.TaggedFriendsList.Add(baseModel);
                                                }
                                            }
                                        }
                                        if (feedbackfields[JsonKeys.TotalTaggedFriends] != null)
                                        {
                                            short totalTag = (short)feedbackfields[JsonKeys.TotalTaggedFriends];
                                            if (feedModel.SecondTagProfile == null && totalTag > 2 && feedModel.FirstTagProfile != null)
                                            {
                                                feedModel.ExtraTagCount = (totalTag - 1) + " others";
                                            }
                                            else
                                            {
                                                feedModel.ExtraTagCount = string.Empty;
                                            }
                                        }
                                        feedModel.OnPropertyChanged("SecondTagProfile");

                                        //emoticon
                                        if (feedbackfields[JsonKeys.DoingList] != null)
                                        {
                                            JArray array = (JArray)feedbackfields[JsonKeys.DoingList];
                                            if (array.Count == 0)
                                            {
                                                feedModel.DoingId = 0;
                                                feedModel.DoingActivity = string.Empty;
                                                feedModel.DoingImageUrl = string.Empty;
                                            }
                                            foreach (JObject obj in array)
                                            {
                                                if (obj[JsonKeys.Name] != null)
                                                {
                                                    feedModel.DoingActivity = (string)obj[JsonKeys.Name];//temporarily "Feeling " + 
                                                    feedModel.DoingId = (long)obj[JsonKeys.Id];

                                                    if (obj[JsonKeys.Url] != null)
                                                        feedModel.DoingImageUrl = (string)obj[JsonKeys.Url];
                                                    else if (obj[JsonKeys.Id] != null)
                                                    {
                                                        long id = (long)obj[JsonKeys.Id];
                                                        DoingDTO dto = null;
                                                        if (NewsFeedDictionaries.Instance.DOING_DICTIONARY.TryGetValue(id, out dto))
                                                        {
                                                            feedModel.DoingImageUrl = dto.ImgUrl;
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        //location
                                        if (AddedLocation != null)
                                        {
                                            feedModel.LocationModel = AddedLocation;
                                            feedModel.LocationModel.ImageUrl = "http://maps.googleapis.com/maps/api/staticmap?"
                                             + "center=" + feedModel.LocationModel.Latitude + "," + feedModel.LocationModel.Longitude
                                             + "&key=" + SocialMediaConstants.GOOGLE_MAP_API_KEY
                                             + "&size=600x170"
                                             + "&markers=size:mid%7Ccolor:red%7C" + feedModel.LocationModel.Latitude + "," + feedModel.LocationModel.Longitude
                                             + "&zoom=15"
                                             + "&maptype=roadmap"
                                             + "&sensor=false";
                                        }
                                        feedModel.OnPropertyChanged("LocationModel");
                                        if (EditedValidity > 0)
                                        {
                                            feedModel.Validity = EditedValidity;
                                        }
                                        if (EditedPrivacy > 0)
                                        {
                                            feedModel.Privacy = EditedPrivacy;
                                        }
                                        //
                                        if (feedModel.ParentFeed != null)
                                        {
                                            feedModel.FeedPanelType = 1;
                                        }
                                        else if (feedModel.LinkModel != null && !string.IsNullOrEmpty(feedModel.LinkModel.PreviewUrl))
                                        {
                                            feedModel.FeedPanelType = 8;
                                        }
                                        else if (feedModel.LocationModel != null && feedModel.LocationModel.Latitude != 0 && feedModel.LocationModel.Longitude != 0)
                                        {
                                            feedModel.FeedPanelType = 9;
                                        }
                                        else
                                        {
                                            feedModel.FeedPanelType = 10;
                                        }
                                        feedModel.OnPropertyChanged("FeedPanelType");
                                        FeedHolderModel feedHolderModel = UCMiddlePanelSwitcher.View_UCAllFeeds.ViewCollection.Where(x => x.FeedId == feedModel.NewsfeedId).FirstOrDefault();
                                        feedHolderModel.FeedPanelType = feedModel.FeedPanelType;
                                    }
                                    else
                                    {
                                        bool isSuccess = false;
                                        JObject feed = null;
                                        string msg = string.Empty;
                                        ThreadDetailsOfStatus dt = new ThreadDetailsOfStatus(NfId);
                                        dt.Run(out isSuccess, out feed, out msg);
                                        if (feed != null) feedModel.LoadData(feed);
                                    }
                                    feedModel.OnPropertyChanged("CurrentInstance");
                                    feedModel.Edited = true;
                                }
                            }
                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
                            break;
                        }
                    }
                    SendToServer.RemoveAllBrokenPacketConfirmation(packetIds);
                    if (feedbackfields == null)
                    {
                        if (!MainSwitcher.ThreadManager().PingNow())
                        {
                            //message = ReasonCodeConstants.REASON_MSG_INTERNET_UNAVAILABLE;
                        }
                    }
                }
                else
                {
                    log.Error("Add or Edit Status Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
                }
            }
            catch (Exception e)
            {
                log.Error(e.StackTrace + e.Message);
            }
            finally
            {
                if (callBackEvent != null)
                {
                    callBackEvent(success, message);
                    callBackEvent = null;
                }
                running = false;
            }
        }

        #endregion

        #region Public Methods

        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion
    }
}


/*************************************Commented out code that may be needed later*****************************************/
//In ContinueToAuthServer method
//    if (feedbackfields[JsonKeys.BookPostType] != null)
//    {
//        feedModel.BookPostType = (short)feedbackfields[JsonKeys.BookPostType];
//    }
//    if (feedbackfields[JsonKeys.Status] != null)
//    {
//        feedModel.Status = (string)feedbackfields[JsonKeys.Status];
//    }
//    if (feedbackfields[JsonKeys.StatusTags] != null)
//    {
//        JArray stsTags = (JArray)feedbackfields[JsonKeys.StatusTags];
//        feedModel.StatusTags = new ObservableCollection<TaggedUserModel>();
//        foreach (JObject obj in stsTags)
//        {
//            TaggedUserModel singleTagModel = new TaggedUserModel();
//            if (obj[JsonKeys.UserIdentity] != null) singleTagModel.UserIdentity = (long)obj[JsonKeys.UserIdentity];//not needed eventually
//            if (obj[JsonKeys.UserTableID] != null) singleTagModel.UserTableID = (long)obj[JsonKeys.UserTableID];
//            if (obj[JsonKeys.FullName] != null) singleTagModel.FullName = (string)obj[JsonKeys.FullName];
//            if (obj[JsonKeys.Position] != null) singleTagModel.Index = (int)obj[JsonKeys.Position];
//            if (obj[JsonKeys.ProfileImage] != null) singleTagModel.ProfileImage = (string)obj[JsonKeys.ProfileImage]; //not needed eventually
//            feedModel.StatusTags.InvokeAdd(singleTagModel);
//        }
//    }
//    //outsidetags
//    if (feedbackfields[JsonKeys.FriendsTagList] != null)
//    {
//        JArray frnsTags = (JArray)feedbackfields[JsonKeys.FriendsTagList];
//        feedModel.TaggedFriendsList = new ObservableCollection<UserShortInfoModel>();
//        foreach (JObject obj in frnsTags)
//        {
//            long UserTableID = (long)obj[JsonKeys.UserTableID];
//            string ProfileImage = (string)obj[JsonKeys.ProfileImage]; //not needed eventually
//            string FullName = (string)obj[JsonKeys.FullName];
//            UserShortInfoModel shortModel = RingIDViewModel.Instance.GetUserShortInfoModelFromServerNotNullable(UserTableID, 0, FullName, ProfileImage);
//            feedModel.TaggedFriendsList.InvokeAdd(shortModel);
//        }
//        if (feedModel.TaggedFriendsList != null && feedModel.TaggedFriendsList.Count > 0)
//            feedModel.OnPropertyChanged("TaggedFriendsList");
//        if (feedbackfields[JsonKeys.TotalTaggedFriends] != null) feedModel.TotalTaggedFriends = (short)feedbackfields[JsonKeys.TotalTaggedFriends];
//    }
//    else
//    {
//        feedModel.TaggedFriendsList = null;
//        feedModel.TotalTaggedFriends = 0;
//    }
//    //location
//    if (AddedLocation != null)
//    {
//        feedModel.locationModel = AddedLocation;
//        feedModel.locationModel.imgUrl = "http://maps.googleapis.com/maps/api/staticmap?"
//         + "center=" + feedModel.locationModel.lat + "," + feedModel.locationModel.lon
//         + "&key=" + DefaultSettings.GOOGLE_MAP_API_KEY
//         + "&size=600x170"
//         + "&markers=size:mid%7Ccolor:red%7C" + feedModel.locationModel.lat + "," + feedModel.locationModel.lon
//         + "&zoom=15"
//         + "&maptype=roadmap"
//         + "&sensor=false";
//    }
//    if (EditedValidity > 0) feedModel.Validity = EditedValidity;
//    if (EditedPrivacy > 0) feedModel.Privacy = EditedPrivacy;

//    if (feedbackfields[JsonKeys.ImageList] != null)
//    {
//        feedModel.ImageList = new List<ImageModel>();
//        feedModel.FeedImageList = new List<ImageModel>();
//        JArray imageList = (JArray)feedbackfields[JsonKeys.ImageList];
//        int index = 0;
//        foreach (JObject singleImage in imageList)
//        {
//            ImageModel imgModel = new ImageModel();
//            imgModel.NewsFeedId = NfId;
//            imgModel.ImageHeight = (int)singleImage[JsonKeys.ImageHeight];
//            imgModel.ImageWidth = (int)singleImage[JsonKeys.ImageWidth];
//            imgModel.ImageUrl = (string)singleImage[JsonKeys.ImageUrl];
//            imgModel.UserShortInfoModel = feedModel.UserShortInfoModel;
//            feedModel.ImageList.Add(imgModel);
//            if (index < 3)
//            {
//                imgModel.FeedImageInfoModel = new FeedImageInfo();
//                imgModel.FeedImageInfoModel.Width = imgModel.ImageWidth;
//                imgModel.FeedImageInfoModel.Height = imgModel.ImageHeight;
//                feedModel.FeedImageList.Add(imgModel);
//                if (index == 2 && feedModel.TotalImage > feedModel.FeedImageList.Count)
//                {
//                    imgModel.MoreImagesCount = "+" + (feedModel.TotalImage - feedModel.FeedImageList.Count);
//                }
//            }
//            index++;
//        }
//        feedModel.ImageList.Sort((x, y) => x.ImageId.CompareTo(y.ImageId));
//        ScaleFeedImages scaler = new ScaleFeedImages(feedModel.FeedImageList, feedModel.NewsfeedId, 600);
//        feedModel.ImageViewHeight = scaler.ImageViewHeight;
//        if (feedModel.FeedImageList.Count == 1) feedModel.SingleImageFeedModel = feedModel.FeedImageList.ElementAt(0);
//        feedModel.TotalImage = feedModel.ImageList.Count;
//    }
//    else
//    {
//        feedModel.ImageList = null;
//        feedModel.FeedImageList = null;
//        feedModel.TotalImage = 0;
//        feedModel.ImageViewHeight = 0;
//    }
//    if (feedbackfields[JsonKeys.AddedMediaList] != null || feedbackfields[JsonKeys.EditedMediaList] != null || feedbackfields[JsonKeys.RemovedMediaIds] != null)
//    {
//        if (feedbackfields[JsonKeys.AddedMediaList] != null)
//        {
//            JArray addedmediaLst = (JArray)feedbackfields[JsonKeys.AddedMediaList];
//            foreach (JObject singleMedia in addedmediaLst)
//            {
//                SingleMediaModel singleMediaModel = new SingleMediaModel();
//                singleMediaModel.LoadData(singleMedia);
//                feedModel.MediaContent.MediaList.InvokeAdd(singleMediaModel);
//                if (feedModel.MediaContent.FeedMediaList.Count < 2) feedModel.MediaContent.FeedMediaList.InvokeAdd(singleMediaModel);
//            }
//        }
//        if (feedbackfields[JsonKeys.EditedMediaList] != null)
//        {
//            JArray editedMdaList = (JArray)feedbackfields[JsonKeys.EditedMediaList];
//            foreach (JObject singleMedia in editedMdaList)
//            {
//                long cntntId = (long)singleMedia[JsonKeys.ContentId];
//                SingleMediaModel singleMediaModel = feedModel.MediaContent.MediaList.Where(P => P.ContentId == cntntId).FirstOrDefault();
//                singleMediaModel.LoadData(singleMedia);
//                feedModel.MediaContent.MediaList.InvokeAdd(singleMediaModel);
//            }
//        }
//        if (feedbackfields[JsonKeys.RemovedMediaIds] != null)
//        {
//            JArray removedMdIds = (JArray)feedbackfields[JsonKeys.RemovedMediaIds];
//            foreach (var item in removedMdIds)
//            {
//                long cntntId = (long)item;
//                SingleMediaModel singleMediaModel = feedModel.MediaContent.MediaList.Where(P => P.ContentId == cntntId).FirstOrDefault();
//                feedModel.MediaContent.MediaList.InvokeRemove(singleMediaModel);
//                singleMediaModel = feedModel.MediaContent.FeedMediaList.Where(P => P.ContentId == cntntId).FirstOrDefault();
//                if (singleMediaModel != null) feedModel.MediaContent.FeedMediaList.InvokeRemove(singleMediaModel);
//            }
//        }
//        //
//        feedModel.MediaContent.TotalMediaCount = feedModel.MediaContent.MediaList.Count;
//        feedModel.MediaContent.MediaList.OrderBy(j => j.ContentId);
//        if (feedModel.MediaContent.MediaList.Count > 2)
//        {
//            feedModel.ExtraMediaCount = "+" + (feedModel.MediaContent.MediaList.Count - 2) + " More";
//        }
//        if (feedModel.MediaContent.FeedMediaList.Count == 1)
//        {
//            feedModel.SingleMediaFeedModel = feedModel.MediaContent.FeedMediaList[0];
//        }
//    }
//    if (feedModel.ParentFeed != null) feedModel.FeedPanelType = 1;
//    if (feedModel.ImageList != null && feedModel.ImageList.Count > 0) feedModel.FeedPanelType = 2;
//    else if (feedModel.MediaContent != null) feedModel.FeedPanelType = 5;
//    else if (!string.IsNullOrEmpty(feedModel.PreviewUrl)) feedModel.FeedPanelType = 8;
//    else if (feedModel.locationModel != null && feedModel.locationModel.lat != 0 && feedModel.locationModel.lon != 0) feedModel.FeedPanelType = 9;
//    else feedModel.FeedPanelType = 10;
//}
//feedModel.OnPropertyChanged("CurrentInstance");

//In Run method
//if (AddedImages != null && AddedImages.Count > 0)
//{
//    ImageUploaderModel model = AddedImages.Where(P => string.IsNullOrEmpty(P.ImageUrl)).FirstOrDefault(); //AddedImages[0];
//    if (model != null)
//    {
//        model.IsUploading = true;
//        model.Pecentage = 0;
//        serverUrl = ServerAndPortSettings.GetAlbumImageUploadingURL;
//        webClient = new WebClient();
//        webClient.UploadProgressChanged += new UploadProgressChangedEventHandler(UploadProgressChanged);
//        webClient.UploadDataCompleted += new UploadDataCompletedEventHandler(UploadDataCompleted);
//        UploadToImageOrMediaServer(model, model.FilePath);
//    }
//    else ContinueToAuthServer();
//}
//else if (AddedMusics != null && AddedMusics.Count > 0)
//{
//    MusicUploaderModel model = AddedMusics.Where(P => string.IsNullOrEmpty(P.StreamUrl)).FirstOrDefault();//AddedMusics[0];
//    if (model != null)
//    {
//        model.IsUploading = true;
//        model.Pecentage = 0;
//        serverUrl = ServerAndPortSettings.GetAudioUploadingURL;
//        webClient = new WebClient();
//        webClient.UploadProgressChanged += new UploadProgressChangedEventHandler(UploadProgressChanged);
//        webClient.UploadDataCompleted += new UploadDataCompletedEventHandler(UploadDataCompleted);
//        UploadToImageOrMediaServer(model, model.FilePath);
//    }
//    else ContinueToAuthServer();
//}
//else if (AddedVideos != null && AddedVideos.Count > 0)
//{
//    FeedVideoUploaderModel model = AddedVideos.Where(P => string.IsNullOrEmpty(P.StreamUrl)).FirstOrDefault();//AddedVideos[0];
//    if (model != null)
//    {
//        model.IsUploading = true;
//        model.Pecentage = 0;
//        serverUrl = ServerAndPortSettings.GetVideoUploadingURL;
//        webClient = new WebClient();
//        webClient.UploadProgressChanged += new UploadProgressChangedEventHandler(UploadProgressChanged);
//        webClient.UploadDataCompleted += new UploadDataCompletedEventHandler(UploadDataCompleted);
//        UploadToImageOrMediaServer(model, model.FilePath);
//    }
//    else ContinueToAuthServer();
//}
//else
//    ContinueToAuthServer();
/***********************************************************************************************************/
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using View.ViewModel.NewStatus;
using View.BindingModels;
using View.ViewModel;

namespace View.Utility.Feed
{
    class ThreadAddFeed
    {
        #region Private Fields
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThreadAddFeed).Name);
        private bool running = false;
        private int feedWallType; //1 for feed,2 on friends wall,3 on circle
        private int validity, w, h, mediatype;
        private string status, imgUrl, cptn;
        private long friendUtidOrCircleId;
        private string linkTitle, linkURL, linkDescription, linkImageURL, linkDomain;
        private int linkType;
        private LocationDTO locationDTO;
        private List<long> taggedUtids;
        private List<long> friendsToShowOrHideUtIds;
        private long DoingId;
        private bool NoResponse = true;
        private JObject JobjMediaContent;
        private JArray JobjectStatusTags, SharingPostContentIds, JobgImageList;
        private int privacy;
        private int feedContentType = 0;
        private string albumName = string.Empty;
        private Guid albumId = Guid.Empty;
        private string coverImageUrl = string.Empty;

        private NewStatusViewModel newStatusViewModel;
        #endregion

        #region Private methods
        private void Run()
        {
            try
            {
                running = true;
                JObject feedbackfields = null;
                JObject pakToSend = new JObject();
                string pakId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = pakId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_STATUS;

                pakToSend[JsonKeys.Status] = status;
                pakToSend[JsonKeys.Type] = feedContentType;
                pakToSend[JsonKeys.Validity] = validity;

                if (feedWallType == 2) 
                {
                    pakToSend[JsonKeys.WallOwnerId] = friendUtidOrCircleId;
                    pakToSend[JsonKeys.WallOwnerType] = SettingsConstants.WALL_OWNER_TYPE_DEFAULT_USER;
                    pakToSend[JsonKeys.FeedPrivacy] = AppConstants.PRIVACY_FRIENDSONLY; 
                }
                else if (feedWallType == 3)
                {
                    pakToSend[JsonKeys.WallOwnerId] = friendUtidOrCircleId;
                    pakToSend[JsonKeys.WallOwnerType] = SettingsConstants.WALL_OWNER_TYPE_GROUP;
                    pakToSend[JsonKeys.FeedPrivacy] = AppConstants.PRIVACY_FRIENDSONLY;
                }
                else
                {
                    pakToSend[JsonKeys.FeedPrivacy] = privacy;
                }
                if (JobjectStatusTags != null)
                {
                    pakToSend[JsonKeys.StatusTags] = JobjectStatusTags;
                }
                else if (SharingPostContentIds != null)
                {
                    pakToSend[JsonKeys.MediaType] = mediatype;
                    pakToSend[JsonKeys.SharingPostContentIds] = SharingPostContentIds;
                }
                else if (!string.IsNullOrEmpty(linkURL))
                {
                    JObject linkDetails = new JObject();
                    if (!string.IsNullOrEmpty(linkTitle))
                    {
                        linkDetails[JsonKeys.LinkTitle] = linkTitle;
                    }
                    if (!string.IsNullOrEmpty(linkDescription))
                    {
                        linkDetails[JsonKeys.LinkDescription] = linkDescription;
                    }
                    if (!string.IsNullOrEmpty(linkDomain))
                    {
                        linkDetails[JsonKeys.LinkDomain] = linkDomain;
                    }
                    if (!string.IsNullOrEmpty(linkImageURL) && linkImageURL.Length < 350)
                    {
                        linkDetails[JsonKeys.LinkImageURL] = linkImageURL;
                    }
                    if (!string.IsNullOrEmpty(linkURL))
                    {
                        linkDetails[JsonKeys.LinkURL] = linkURL;
                    }
                    if (linkType > 0)
                    {
                        linkDetails[JsonKeys.LinkType] = linkType;
                    }
                    pakToSend[JsonKeys.LinkDetails] = linkDetails;
                }
                if (locationDTO != null)
                {
                    JObject location = new JObject();
                    location[JsonKeys.Location] = locationDTO.LocationName;
                    location[JsonKeys.Latitude] = locationDTO.Latitude;
                    location[JsonKeys.Longitude] = locationDTO.Longitude;
                    pakToSend[JsonKeys.LocationDetails] = location;
                }
                if (taggedUtids != null)
                {
                    JArray array = new JArray();
                    foreach (long item in taggedUtids)
                    {
                        array.Add(item);
                    }
                    pakToSend[JsonKeys.TaggedFriendUtIds] = array;
                }
                if (friendsToShowOrHideUtIds != null && friendsToShowOrHideUtIds.Count > 0)
                {
                    JArray friendsToShowOrHideUtIdsArray = new JArray();
                    foreach (long friendsToShowOrHideUtId in friendsToShowOrHideUtIds)
                    {
                        friendsToShowOrHideUtIdsArray.Add(friendsToShowOrHideUtId);
                    }
                    pakToSend[JsonKeys.SpecificFriendsToShowOrHide] = friendsToShowOrHideUtIdsArray;
                }

                if (this.DoingId > 0)
                {
                    JArray array = new JArray();
                    array.Add(DoingId);
                    pakToSend[JsonKeys.MoodIds] = array;
                }

                //Media section
                JObject albumDetails = new JObject();
                if (!string.IsNullOrEmpty(albumName))
                {
                    albumDetails[JsonKeys.AlbumName] = albumName;
                }
                if (this.albumId != Guid.Empty)
                {
                    albumDetails[JsonKeys.AlbumId] = this.albumId;
                }
                if (!(string.IsNullOrEmpty(coverImageUrl)))
                {
                    albumDetails[JsonKeys.CoverImageUrl] = coverImageUrl;
                }
                if (JobjMediaContent != null)
                {
                    albumDetails[JsonKeys.MediaType] = mediatype;
                    albumDetails[JsonKeys.MediaList] = JobjMediaContent[JsonKeys.MediaList];
                }
                if (JobgImageList != null)
                {
                    albumDetails[JsonKeys.ImageType] = 1;
                    albumDetails[JsonKeys.ImageList] = JobgImageList;
                }
                pakToSend[JsonKeys.AlbumDetails] = albumDetails;

                string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
                log.Info("Send by 177..>" + data);
                Dictionary<String, byte[]> packets = SendToServer.BuildBrokenPacket(data, AppConstants.TYPE_ADD_STATUS, pakId);
                int packetType = packets.Count > 1 ? AppConstants.BROKEN_PACKET : AppConstants.SINGLE_PACKET;
                List<string> packetIds = new List<string>(packets.Keys);
                SendToServer.SendBrokenPacket(packetType, packets);
                Thread.Sleep(25);
                string makeResponsepak = pakId + "_" + AppConstants.TYPE_ADD_STATUS;
                for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
                {
                    if (string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
                    {
                        break;
                    }
                    Thread.Sleep(DefaultSettings.WAITING_TIME);
                    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(makeResponsepak))
                    {
                        if (SendToServer.CheckAllBrokenPacketConfirmation(packets))
                        {
                            SendToServer.SendBrokenPacket(packetType, packets);
                        }
                    }
                    else
                    {
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(makeResponsepak, out feedbackfields);
                        NoResponse = false;
                        break;
                    }
                }
                SendToServer.RemoveAllBrokenPacketConfirmation(packetIds);

                MainSwitcher.AuthSignalHandler().feedSignalHandler.EnablePostBtn(feedWallType, friendUtidOrCircleId, !NoResponse);
                if (feedbackfields == null)
                {
                    if (!MainSwitcher.ThreadManager().PingNow())
                    {
                       UIHelperMethods.ShowErrorMessageBoxFromThread(NotificationMessages.INTERNET_UNAVAILABLE, "Failed!");
                    }
                }
                else
                {
                    if ((bool)feedbackfields[JsonKeys.Success] && feedbackfields[JsonKeys.NewsFeed] != null)
                    {
                        JObject newsFeed = (JObject)feedbackfields[JsonKeys.NewsFeed];
                        JObject feedBackAlbumDetails = (JObject)newsFeed[JsonKeys.AlbumDetails];

                        if (feedBackAlbumDetails != null)
                        {
                            JArray mediaArray = (JArray)feedBackAlbumDetails[JsonKeys.MediaList];
                            JObject mediaObject = mediaArray != null ? (JObject)mediaArray[0] : null;

                            if((int)feedBackAlbumDetails[JsonKeys.MediaType] == SettingsConstants.MEDIA_TYPE_AUDIO)
                            {
                                MediaContentModel existingAudioAlbum = RingIDViewModel.Instance.MyAudioAlbums.Where(x => x.AlbumId == (Guid)feedBackAlbumDetails[JsonKeys.AlbumId]).FirstOrDefault();
                                if (existingAudioAlbum != null)
                                {
                                    existingAudioAlbum.TotalMediaCount += mediaArray.Count;
                                }
                                else if (mediaObject != null && feedBackAlbumDetails[JsonKeys.AlbumId] != null)
                                {
                                    MediaContentModel newAudioAlbum = new MediaContentModel();
                                    newAudioAlbum.AlbumId = (Guid)feedBackAlbumDetails[JsonKeys.AlbumId];
                                    newAudioAlbum.AlbumName = (string)feedBackAlbumDetails[JsonKeys.AlbumName];
                                    newAudioAlbum.AlbumImageUrl = (string)feedBackAlbumDetails[JsonKeys.CoverImageUrl];
                                    newAudioAlbum.TotalMediaCount = mediaArray.Count;
                                    newAudioAlbum.MediaType = SettingsConstants.MEDIA_TYPE_AUDIO;
                                    if(RingIDViewModel.Instance.MyAudioAlbums.Count >= 1)
                                    {
                                        RingIDViewModel.Instance.MyAudioAlbums.InvokeInsert(1, newAudioAlbum);
                                    }
                                }
                            }
                            else if((int)feedBackAlbumDetails[JsonKeys.MediaType] == SettingsConstants.MEDIA_TYPE_VIDEO)
                            {
                                MediaContentModel existingVideoAlbum = RingIDViewModel.Instance.MyVideoAlbums.Where(x => x.AlbumId == (Guid)feedBackAlbumDetails[JsonKeys.AlbumId]).FirstOrDefault();
                                if(existingVideoAlbum != null)
                                {
                                    existingVideoAlbum.TotalMediaCount += mediaArray.Count;
                                }
                                else if(mediaObject != null && feedBackAlbumDetails[JsonKeys.AlbumId] != null)
                                {
                                    MediaContentModel newVideoAlbum = new MediaContentModel();
                                    newVideoAlbum.AlbumId = (Guid)feedBackAlbumDetails[JsonKeys.AlbumId];
                                    newVideoAlbum.AlbumName = (string)feedBackAlbumDetails[JsonKeys.AlbumName];
                                    newVideoAlbum.AlbumImageUrl = (string)feedBackAlbumDetails[JsonKeys.CoverImageUrl];
                                    newVideoAlbum.TotalMediaCount = mediaArray.Count;
                                    newVideoAlbum.MediaType = SettingsConstants.MEDIA_TYPE_VIDEO;
                                    if(RingIDViewModel.Instance.MyVideoAlbums.Count >= 1)
                                    {
                                        RingIDViewModel.Instance.MyVideoAlbums.InvokeInsert(1, newVideoAlbum);
                                    }
                                }
                            }

                            JArray imageArray = (JArray)feedBackAlbumDetails[JsonKeys.ImageList];
                            JObject imageObject = imageArray != null ? (JObject)imageArray[0] : null;

                            AlbumModel existingImageAlbum = RingIDViewModel.Instance.MyImageAlubms.Where(x => x.AlbumId == (Guid)feedBackAlbumDetails[JsonKeys.AlbumId]).FirstOrDefault();
                            if (existingImageAlbum != null)
                            {
                                existingImageAlbum.TotalImagesInAlbum += imageArray.Count;
                            }
                            else if(imageObject != null && feedBackAlbumDetails[JsonKeys.AlbumId] != null)
                            {
                                AlbumModel imageAlbumModel = new AlbumModel(imageObject);
                                imageAlbumModel.AlbumName = (string)feedBackAlbumDetails[JsonKeys.AlbumName];
                                imageAlbumModel.AlbumId = (Guid)feedBackAlbumDetails[JsonKeys.AlbumId];
                                imageAlbumModel.AlbumCoverImageUrl = (string)feedBackAlbumDetails[JsonKeys.CoverImageUrl] ??  (string)imageObject[JsonKeys.ImageUrl];
                                imageAlbumModel.TotalImagesInAlbum = imageArray.Count;
                                if (RingIDViewModel.Instance.MyImageAlubms.Count >= 3)
                                {
                                    RingIDViewModel.Instance.MyImageAlubms.InvokeInsert(3, imageAlbumModel);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception : " + ex.Message + "Stack Trace : " + ex.StackTrace);
            }
            finally 
            {
                running = false;
                if (newStatusViewModel != null)
                {
                    newStatusViewModel.PrivacyValue = AppConstants.PRIVACY_PUBLIC;
                }
                newStatusViewModel.IsLocationSet = false;
                newStatusViewModel.IsImageAlbumSelectedFromExisting = false;
                newStatusViewModel.IsAudioAlbumSelectedFromExisting = false;
                newStatusViewModel.IsVideoAlbumSelectedFromExisting = false;
            }
        }

        #endregion

        #region Public Methods

        public void StartThread(NewStatusViewModel newStatusViewModel, int mediaType = 0, JObject mediaContent = null, JArray sharingPostContentId = null)
        {
            this.newStatusViewModel = newStatusViewModel;
            this.privacy = newStatusViewModel.PrivacyValue;
            this.feedWallType = newStatusViewModel.FeedWallType; ;
            this.status = newStatusViewModel.StatusTextWithoutTags;
            this.JobjectStatusTags = newStatusViewModel.StatusTagsJArray;
            this.validity = newStatusViewModel.ValidityValue;
            this.friendUtidOrCircleId = newStatusViewModel.FriendUtIdOrCircleId;
            this.linkDomain = newStatusViewModel.PreviewDomain;
            this.linkDescription = newStatusViewModel.PreviewDesc;
            this.linkTitle = newStatusViewModel.PreviewTitle;
            this.linkURL = newStatusViewModel.PreviewUrl;
            this.linkImageURL = newStatusViewModel.PreviewImgUrl;
            this.linkType = newStatusViewModel.PreviewLnkType;
            this.locationDTO = newStatusViewModel.SelectedLocationModel == null ? null : newStatusViewModel.SelectedLocationModel.GetLocationDTOFromModel();
            this.taggedUtids = newStatusViewModel.TaggedUtids;
            this.friendsToShowOrHideUtIds = newStatusViewModel.FriendsToShowOrHideUtIds;
            this.DoingId = newStatusViewModel.SelectedDoingId;
            this.SharingPostContentIds = sharingPostContentId;
            this.mediatype = mediaType;

            if (mediaType == 0)
            {
                this.feedContentType = SettingsConstants.FEED_TYPE_TEXT;
            }
            else if(mediaType == 1 || mediaType == 2)
            {
                this.JobjMediaContent = mediaContent;
                this.albumId = this.JobjMediaContent[JsonKeys.AlbumId] != null ? (Guid)this.JobjMediaContent[JsonKeys.AlbumId] : Guid.Empty;
                this.albumName = (string)JobjMediaContent[JsonKeys.AlbumName];
                var mediaList = this.JobjMediaContent[JsonKeys.MediaList];
                if ((int)this.JobjMediaContent[JsonKeys.MediaType] == SettingsConstants.MEDIA_TYPE_AUDIO)
                {
                    if (mediaList.Count() > 1)
                    {
                        feedContentType = SettingsConstants.FEED_TYPE_MULTIPLE_AUDIO_WITH_ALBUM;
                    }
                    else
                    {
                        if (String.IsNullOrEmpty((string)this.JobjMediaContent[JsonKeys.AlbumName]))
                        {
                            feedContentType = SettingsConstants.FEED_TYPE_SINGLE_AUDIO;
                        }
                        else
                        {
                            feedContentType = SettingsConstants.FEED_TYPE_SINGLE_AUDIO_WITH_ALBUM;
                        }
                    }
                }
                else if ((int)this.JobjMediaContent[JsonKeys.MediaType] == SettingsConstants.MEDIA_TYPE_VIDEO)
                {
                    if (mediaList.Count() > 1)
                    {
                        feedContentType = SettingsConstants.FEED_TYPE_MULTIPLE_VIDEO_WITH_ALBUM;
                    }
                    else
                    {
                        if (String.IsNullOrEmpty((string)this.JobjMediaContent[JsonKeys.AlbumName]))
                        {
                            feedContentType = SettingsConstants.FEED_TYPE_SINGLE_VIDEO;
                        }
                        else
                        {
                            feedContentType = SettingsConstants.FEED_TYPE_SINGLE_VIDEO_WITH_ALBUM;
                        }
                    }
                }

            }
            else
            {
                this.JobgImageList = (JArray)mediaContent[JsonKeys.ImageList];
                this.albumId = mediaContent[JsonKeys.AlbumId] != null ? (Guid)mediaContent[JsonKeys.AlbumId] : Guid.Empty;
                string imageAlbumName = (string)mediaContent[JsonKeys.AlbumName];
                if (JobgImageList.Count > 1)
                {
                    feedContentType = SettingsConstants.FEED_TYPE_MULTIPLE_IMAGE_WITH_ALBUM;
                    albumName = imageAlbumName;
                    if (albumId == Guid.Empty)
                    {
                        coverImageUrl = (string)mediaContent[JsonKeys.CoverImageUrl];
                    }
                }
                else
                {
                    if (String.IsNullOrEmpty(imageAlbumName))
                    {
                        feedContentType = SettingsConstants.FEED_TYPE_SINGLE_IMAGE;
                    }
                    else
                    {
                        feedContentType = SettingsConstants.FEED_TYPE_SINGLE_IMAGE_WITH_ALBUM;
                        albumName = imageAlbumName;
                        coverImageUrl = (string)mediaContent[JsonKeys.CoverImageUrl];
                    }
                }
            }
            System.Threading.Thread thread = new System.Threading.Thread(Run);
            thread.Start();
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion
    }
}

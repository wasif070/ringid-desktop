﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility.Feed
{
    public class CelebrityDiscoverPopularorFollowingList
    {
        private readonly ILog log = LogManager.GetLogger(typeof(CelebrityDiscoverPopularorFollowingList).Name);
        private int waitingTime = 0;
        private int st, profileType, clt; //clt => Discover=1, Popular=2, Following=3, All=4, NotFollowing=5
        bool iserl; //IsExcludeRemovedList
        bool issbp; //IsSortByPopularity
        string cntr;
        int catId;
        public delegate void CallBack(int success);
        public event CallBack callBackEvent;

        public CelebrityDiscoverPopularorFollowingList(int clt, int profileType, string cntryName = "", int catId = 0, int st = 0)
        {
            this.clt = clt;
            this.st = st;
            this.cntr = cntryName;
            this.catId = catId;
            this.profileType = profileType;
            if (clt == 1)
                iserl = true;
            else iserl = false;
            if (clt == 2) issbp = true;
            else issbp = false;
        }

        public void StartThread()
        {
            new Thread(new ThreadStart(Run)).Start();
        }

        public void Run()
        {
            if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                try
                {
                    JObject pakToSend = new JObject();
                    string pakId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = pakId;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_CELEBRITY_LIST;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.CelebrityListType] = clt;
                    pakToSend[JsonKeys.CelebrityCountry] = cntr;
                    pakToSend[JsonKeys.CelebrityCategoryId] = catId;
                    pakToSend[JsonKeys.IsExcludeRemovedList] = iserl;
                    pakToSend[JsonKeys.IsSortByPopularity] = issbp;
                    pakToSend[JsonKeys.Limit] = 10;
                    if (st > 0) pakToSend[JsonKeys.StartLimit] = st;
                    pakToSend[JsonKeys.ProfileType] = profileType;

                    JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakId);
                    if (feedbackfields != null)
                    {
                        //if (feedbackfields[JsonKeys.Success] != null)
                        //{
                        //    callBackEvent((bool)feedbackfields[JsonKeys.Success]);
                        //    callBackEvent = null;
                        //}
                        if (feedbackfields[JsonKeys.Success] != null)
                        {
                            callBackEvent(((bool)feedbackfields[JsonKeys.Success]) ? SettingsConstants.RESPONSE_SUCCESS : SettingsConstants.RESPONSE_NOTSUCCESS);
                            callBackEvent = null;
                            return;
                        }
                        else
                        {
                            while (feedbackfields[JsonKeys.Success] == null)
                            {
                                if (waitingTime == DefaultSettings.TRYING_TIME)
                                {
                                    break;
                                }
                                Thread.Sleep(DefaultSettings.WAITING_TIME);
                                RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out feedbackfields);
                                waitingTime++;
                                if (feedbackfields[JsonKeys.Success] != null)
                                {
                                    //callBackEvent((bool)feedbackfields[JsonKeys.Success]);
                                    callBackEvent(((bool)feedbackfields[JsonKeys.Success]) ? SettingsConstants.RESPONSE_SUCCESS : SettingsConstants.RESPONSE_NOTSUCCESS);
                                    callBackEvent = null;
                                    return;
                                }
                            }
                        }
                    }
                    else
                    {
                        callBackEvent(SettingsConstants.NO_RESPONSE);
                        callBackEvent = null;
                        MainSwitcher.ThreadManager().PingNow();
                    }
                    //string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
                    //SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
                    //Thread.Sleep(25);
                    //for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
                    //{
                    //    if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
                    //    {
                    //        break;
                    //    }
                    //    Thread.Sleep(DefaultSettings.WAITING_TIME);
                    //    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out FeedBackFields))
                    //    {
                    //        if (i % DefaultSettings.SEND_INTERVAL == 0)
                    //            SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
                    //    }
                    //    else if (FeedBackFields[JsonKeys.Success] != null)
                    //    {
                    //        callBackEvent((bool)FeedBackFields[JsonKeys.Success]);
                    //        callBackEvent = null;
                    //        return;
                    //    }
                    //    //  PingInServer.StartThread(1, DefaultSettings.TRYING_TIME);
                    //}

                }
                catch (Exception e)
                {
                    callBackEvent(SettingsConstants.NO_RESPONSE);
                    callBackEvent = null;
                    log.Error("SubscribeOrUnsubscribe ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            }
            else
            {
                callBackEvent(SettingsConstants.NO_RESPONSE);
                callBackEvent = null;
                log.Error("SubscribeOrUnsubscribe Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            }

        }
    }
}

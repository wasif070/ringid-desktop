﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using System;
using System.Threading;
using View.Utility.Auth;

namespace View.Utility.Feed
{
    public class ThreadChannelInfoRequest
    {
        private long pid, userTableId, userIdentity;
        private int profileType;
        private readonly ILog log = LogManager.GetLogger(typeof(ThreadChannelInfoRequest).Name);
        public delegate void OnSuccessHandler(JObject jObject);
        public event OnSuccessHandler OnSuccess;
        private bool mfc;

        public ThreadChannelInfoRequest(long pid, long userTableId, long userIdentity, int profileType, bool mfc = false)
        {
            this.pid = pid;
            this.userIdentity = userIdentity;
            this.userTableId = userTableId;
            this.profileType = profileType;
            this.mfc = mfc;
        }

        public void Start()
        {
            Thread th = new Thread(new ThreadStart(run));
            th.Name = this.GetType().Name;
            th.Start();
        }

        public void run()
        {
            if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                try
                {
                    JObject pakToSend = new JObject();
                    string pakId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = pakId;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_UNKNWON_PROFILE_INFO;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    //if (this.pid > 0) pakToSend[JsonKeys.PageId] = pid;
                    //else 
                    if (this.userTableId > 0) pakToSend[JsonKeys.UserTableID] = userTableId;
                    else if (this.userIdentity > 0) pakToSend[JsonKeys.UserIdentity] = userIdentity;

                    if (profileType > 0)
                        pakToSend[JsonKeys.ProfileType] = profileType;
                    if (mfc)
                        pakToSend[JsonKeys.MutualFriendCount] = true;

                    JObject feedbackfields = SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakId);
                    if (feedbackfields != null)
                    {
                        if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success] && feedbackfields[JsonKeys.UserDetails] != null)
                        {
                            //switch (profileType)
                            //{
                            //    case SettingsConstants.PROFILE_TYPE_MUSICPAGE:
                            //        MusicPageDTO musicPageDTO = HelperMethodsModel.BindMusicPageDetails((JObject)feedbackfields[JsonKeys.UserDetails]);
                            //        if (OnSuccess != null) { OnSuccess(musicPageDTO); OnSuccess = null; }
                            //        break;
                            //    case SettingsConstants.PROFILE_TYPE_CELEBRITY:
                            //        CelebrityDTO celebDTO = HelperMethodsModel.BindCelebrityDetails((JObject)feedbackfields[JsonKeys.UserDetails]);
                            //        if (OnSuccess != null) { OnSuccess(celebDTO); OnSuccess = null; }
                            //        break;
                            //    case SettingsConstants.PROFILE_TYPE_NEWSPORTAL:
                            //        NewsPortalDTO portalDTO = HelperMethodsModel.BindNewsPortalDetails((JObject)feedbackfields[JsonKeys.UserDetails]);
                            //        if (OnSuccess != null) { OnSuccess(portalDTO); OnSuccess = null; }
                            //        break;
                            //    case SettingsConstants.PROFILE_TYPE_PAGES:
                            //        PageInfoDTO pageDTO = HelperMethodsModel.BindPageDetails((JObject)feedbackfields[JsonKeys.UserDetails]);
                            //        if (OnSuccess != null) { OnSuccess(pageDTO); OnSuccess = null; }
                            //        break;
                            //    default:
                            //        if (OnSuccess != null) { OnSuccess(jObject); OnSuccess = null; }
                            //        break;
                            //}
                            if (OnSuccess != null) { OnSuccess((JObject)feedbackfields[JsonKeys.UserDetails]); OnSuccess = null; }
                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
                            return;
                        }
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
                    }
                    else
                    {
                        if (!MainSwitcher.ThreadManager().PingNow())
                        {
                        }
                    }
                }
                catch (Exception e)
                {
                    log.Error("ThreadChannelInfoRequest ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            }
            else
            {
                log.Error("ThreadChannelInfoRequest Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            }
            if (OnSuccess != null)
            {
                OnSuccess(null);
                OnSuccess = null;
            }
        }

    }
}

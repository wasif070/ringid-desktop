﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace View.Utility.Feed
{
    public class EncryptForShare
    {
        private static readonly char[] hexArray = "0123456789ABCDEF".ToCharArray();

        /**
         * Make hexadecimal string from byte array
         *
         * @param bytes byte array
         * @return String
         */
        public string bytesToHex(byte[] bytes)
        {
            char[] hexChars = new char[bytes.Length * 2];
            for (int j = 0; j < bytes.Length; j++)
            {
                int v = bytes[j] & 0xFF;
                int hexCharsPos = j << 1;
                hexChars[hexCharsPos] = hexArray[v >> 4];
                hexChars[hexCharsPos + 1] = hexArray[v & 0x0F];
            }

            return new string(hexChars);
        }

        /**
         * Encrypt provided byte array with our custom encryption technic
         *
         * @param data byte array
         * @return byte array
         */
        public byte[] encrypt(byte[] data)
        {
            Random random = new Random();
            int keyLength = random.Next(5) + 1;
            int encryptedDataLength = keyLength + data.Length + 1;
            byte[] encryptedData = new byte[encryptedDataLength];

            encryptedData[0] = (byte)keyLength;
            for (int i = 1; i < keyLength + 1; i++)
            {
                encryptedData[i] = (byte)(random.Next(64) + 1);
            }

            int j = 1;
            for (int i = keyLength + 1; i < encryptedDataLength; i++)
            {
                encryptedData[i] = (byte)(data[j - 1] ^ encryptedData[j & 3]);
                j++;
            }

            return encryptedData;
        }

        /**
         * Convert String IP address to byte array
         *
         * @param ipAddress String
         * @return byte array
         */
        public byte[] ipToByteArray(string ipAddress)
        {
            long ipInLong = 0;
            string[] ipAddressInArray = ipAddress.Split('.');
            for (int i = 3; i >= 0; i--)
            {
                long temp = long.Parse(ipAddressInArray[3 - i]);
                ipInLong |= temp << (i << 3);
            }
            byte[] result = new byte[8];
            for (int i = 7; i >= 0; i--)
            {
                result[i] = (byte)(ipInLong & 0xFF);
                ipInLong >>= 8;
            }
            return result;
        }

        /**
         * Convert Integer number to byte array
         *
         * @param value Integer
         * @return byte array
         */
        public byte[] intToByteArray(int value)
        {
            return new byte[]{
            (byte) (value >> 24),
            (byte) (value >> 16),
            (byte) (value >> 8),
            (byte) value};
        }
    }
}

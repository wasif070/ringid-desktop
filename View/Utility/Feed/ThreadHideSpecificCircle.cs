﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using View.BindingModels;

namespace View.Utility.Feed
{
    public class ThreadHideSpecificCircle
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThreadHideSpecificCircle).Name);
        private bool running = false;
        long circleId;
        CircleModel model;
        bool hide = true;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        public ThreadHideSpecificCircle()
        {

        }
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.Hide] = hide;
                JArray array = new JArray();
                array.Add(circleId);
                pakToSend[JsonKeys.IdsList] = array;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_HIDE_UNHIDE_CIRCLE_FEED;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                    {
                        MainSwitcher.AuthSignalHandler().feedSignalHandler.HideSpecificCircle(circleId, model, hide);
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                    }
                    else
                    {
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                       UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to hide this Circle feeds! Please Try later!", "Failed");
                    }
                }
                else
                {
                   UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to hide this Circle feeds! Please Try later!", "Failed");
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {
               UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to hide this Circle feeds! Please Try later!", "Failed");
            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(long circleId, CircleModel model, bool hide = true)
        {
            if (!running)
            {
                this.circleId = circleId;
                this.model = model;
                this.hide = hide;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

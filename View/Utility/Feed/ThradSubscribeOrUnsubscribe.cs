﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json.Linq;

namespace View.Utility.Feed
{
    class ThradSubscribeOrUnsubscribe
    {
        #region Private Fields

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradSubscribeOrUnsubscribe).Name);
        private bool running = false;
        private List<Guid> unsubscribeList, subscribeList;
        private long utId, pId;
        private string sb;
        private int profileType, subscribeOrUnsubscribeType; //0 edit, 1 unsc all, 2 subsc all
        private bool isEditInNewSubscription = false;

        #endregion


        #region "Constructor"

        public ThradSubscribeOrUnsubscribe(int profileType, long utId, int SubUnsubscrbType, List<Guid> subscrbeLst, List<Guid> unsubscrbeLst, long pId, bool IsEditInNewSubscription = false)
        {
            this.unsubscribeList = unsubscrbeLst;
            this.subscribeList = subscrbeLst;
            this.utId = utId;
            this.pId = pId;
            this.subscribeOrUnsubscribeType = SubUnsubscrbType;
            this.profileType = profileType;
            this.sb = (SubUnsubscrbType == 1) ? "unsubscribe" : "subscribe";
            this.isEditInNewSubscription = IsEditInNewSubscription;
        }

        #endregion

        #region Private Methods

        private void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_NEWSPORTAL_SUBSCRIBE_UNSUBSCRIBE;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                if (pId > 0) pakToSend[JsonKeys.UserTableID] = pId;
                else if (utId > 0) pakToSend[JsonKeys.UserTableID] = utId;
                pakToSend[JsonKeys.ProfileType] = profileType;
                if (subscribeOrUnsubscribeType > 0)
                {
                    pakToSend[JsonKeys.SubUnsubscrbType] = subscribeOrUnsubscribeType;
                }
                else
                {
                    if (isEditInNewSubscription)
                    {
                        pakToSend[JsonKeys.Edited] = false;
                        pakToSend[JsonKeys.SubUnsubscrbType] = subscribeOrUnsubscribeType;
                    }
                    else pakToSend[JsonKeys.Edited] = true;
                    JArray subArr = new JArray();
                    foreach (var item in subscribeList)
                    {
                        subArr.Add(item);
                    }
                    pakToSend[JsonKeys.SubscribedIDs] = subArr;

                    JArray unsubArr = new JArray();
                    if (unsubscribeList != null)
                    {
                        foreach (var item in unsubscribeList)
                        {
                            unsubArr.Add(item);
                        }
                    }
                    pakToSend[JsonKeys.UnsubscribedIDs] = unsubArr;
                }

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (feedbackfields != null)
                {
                    if ((bool)feedbackfields[JsonKeys.Success])
                    {
                        if (subscribeOrUnsubscribeType == 1)
                            MainSwitcher.AuthSignalHandler().feedSignalHandler.SubscribeUnsubscribeChannels(profileType, subscribeOrUnsubscribeType, utId, "Successfully subscribed to this Channel!");
                        else
                            MainSwitcher.AuthSignalHandler().feedSignalHandler.SubscribeUnsubscribeChannels(profileType, subscribeOrUnsubscribeType, utId, "Successfully unsubscribed from this Channel!");
                    }
                    else
                    {
                       UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to " + sb + " to this Channel! Please Try later!", "Failed");
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                   UIHelperMethods.ShowErrorMessageBoxFromThread("Failed to " + sb + " to this Channel! Please Check your Inernet Connection or Try later!", "Failed");
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception message : " + ex.Message + "Exception stackrace : " + ex.StackTrace);
            }
            finally { running = false; }
        }

        #endregion

        #region Public Methods

        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion
    }
}
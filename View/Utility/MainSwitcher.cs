﻿using log4net;
using View.Utility.Auth;
using View.Utility.Call;
using View.Utility.SignInSignUP;

namespace View.Utility
{
    public static class MainSwitcher
    {
        #region "Private Fields"

        private static readonly ILog log = LogManager.GetLogger(typeof(MainSwitcher).Name);
        private static ThreadController threadController;
        private static AuthSignalHandler authSignalHandler;
        private static PopupController popupController;

        #endregion "Private Fields"

        #region "Public Methods"

        public static ThreadController ThreadManager()
        {
            if (threadController == null) threadController = new ThreadController();
            return threadController;
        }

        public static AuthSignalHandler AuthSignalHandler()
        {
            if (authSignalHandler == null) { authSignalHandler = new AuthSignalHandler(); }
            return authSignalHandler;
        }

        public static PopupController PopupController
        {
            get { if (popupController == null) popupController = new PopupController(); return popupController; }
        }

        public static CallController CallController { get; set; }

        #endregion//"Public Methods"
    }
}

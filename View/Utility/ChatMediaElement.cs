﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using View.Utility.Recorder.DirectX.Capture;

namespace View.Utility
{
    public class ChatMediaElement : MediaElement
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ChatMediaElement).Name);

        public delegate void OffsetChangeHandler(TimeSpan timeSpan);
        public event OffsetChangeHandler OnOffsetChange;
        private CustomizeTimer PlayerProgressTimer { set; get; }

        public ChatMediaElement()
        {
            LoadedBehavior = MediaState.Pause;
            PlayerProgressTimer = new CustomizeTimer();
            PlayerProgressTimer.IntervalInSecond = 1;
            PlayerProgressTimer.Tick += PlayerProgressTimer_Tick;
            MediaOpened += ChatMediaElement_MediaOpened;
        }

        private void ChatMediaElement_MediaOpened(object sender, RoutedEventArgs e)
        {
            MediaOpened -= ChatMediaElement_MediaOpened;
            if (LoadedBehavior == MediaState.Pause)
            {
                LoadedBehavior = MediaState.Manual;
            }
            Position = TimeSpan.FromSeconds(OffsetPosition);
            Pause();

            if (IsDirectPlay)
            {
                MultiMediaState = StatusConstants.MEDIA_PLAY_STATE;
            }
        }

        public double RotateAngle
        {
            get { return (double)GetValue(RotateAngleProperty); }
            set { SetValue(RotateAngleProperty, value); }
        }

        public static readonly DependencyProperty RotateAngleProperty = DependencyProperty.Register("RotateAngle", typeof(double), typeof(ChatMediaElement), new PropertyMetadata(0D, (s, e) => 
        {
            if (e.NewValue != null)
            {
                ChatMediaElement chatMediaElement = (ChatMediaElement)s;
                TransformGroup transformGroup = null;
                RotateTransform rotateTransform = null;

                if (chatMediaElement.LayoutTransform is TransformGroup)
                {
                    transformGroup = (TransformGroup)chatMediaElement.LayoutTransform;
                    rotateTransform = (RotateTransform)transformGroup.Children[0];
                }
                else
                {
                    transformGroup = new TransformGroup();
                    rotateTransform = new RotateTransform { Angle = 0 };
                    transformGroup.Children.Add(rotateTransform);
                    chatMediaElement.LayoutTransform = transformGroup;
                }

                rotateTransform.Angle = (double)e.NewValue;
            }
        }));

        public bool IsDirectPlay
        {
            get { return (bool)GetValue(IsDirectPlayProperty); }
            set { SetValue(IsDirectPlayProperty, value); }
        }

        public static readonly DependencyProperty IsDirectPlayProperty = DependencyProperty.Register("IsDirectPlay", typeof(bool), typeof(ChatMediaElement), new PropertyMetadata(false));

        public int ProgressPercentage
        {
            get { return (int)GetValue(ProgressPercentageProperty); }
            private set { SetValue(ProgressPercentageProperty, value); }
        }

        public static readonly DependencyProperty ProgressPercentageProperty = DependencyProperty.Register("ProgressPercentage", typeof(int), typeof(ChatMediaElement), new PropertyMetadata(0));

        public TimeSpan OffsetTimeSpan
        {
            get { return (TimeSpan)GetValue(OffsetTimeSpanProperty); }
            private set { SetValue(OffsetTimeSpanProperty, value); }
        }

        public static readonly DependencyProperty OffsetTimeSpanProperty = DependencyProperty.Register("OffsetTimeSpan", typeof(TimeSpan), typeof(ChatMediaElement), new PropertyMetadata(TimeSpan.FromSeconds(0), (s, e) =>
        {

            ChatMediaElement mediaElement = (ChatMediaElement)s;
            if (mediaElement.OnOffsetChange != null)
            {
                TimeSpan prevSpan = (TimeSpan)e.OldValue;
                TimeSpan newSpan = (TimeSpan)e.NewValue;
                if ((int)prevSpan.TotalSeconds != (int)newSpan.TotalSeconds)
                {
                    mediaElement.OnOffsetChange(newSpan);
                }
            }
        }));

        public TimeSpan TotalTimeSpan
        {
            get { return (TimeSpan)GetValue(TotalTimeSpanProperty); }
            private set { SetValue(TotalTimeSpanProperty, value); }
        }

        public static readonly DependencyProperty TotalTimeSpanProperty = DependencyProperty.Register("TotalTimeSpan", typeof(TimeSpan), typeof(ChatMediaElement), new PropertyMetadata(TimeSpan.FromSeconds(0)));

        public int OffsetPosition
        {
            get { return (int)GetValue(OffsetPositionProperty); }
            set { SetValue(OffsetPositionProperty, value); }
        }

        public static readonly DependencyProperty OffsetPositionProperty = DependencyProperty.Register("OffsetPosition", typeof(int), typeof(ChatMediaElement), new PropertyMetadata(0));

        public int MultiMediaState
        {
            get { return (int)GetValue(MultiMediaStateProperty); }
            set { SetValue(MultiMediaStateProperty, value); }
        }

        public static readonly DependencyProperty MultiMediaStateProperty = DependencyProperty.Register("MultiMediaState", typeof(int), typeof(ChatMediaElement), new PropertyMetadata(StatusConstants.MEDIA_INIT_STATE, (s, e) =>
        {
            ChatMediaElement mediaElement = (ChatMediaElement)s;
            int prevState = (int)e.OldValue;
            int state = (int)e.NewValue;

            switch (state)
            {
                case StatusConstants.MEDIA_INIT_STATE:
                    //log.Info(" MultiMediaState => COMPLETE ");
                    if (prevState == StatusConstants.MEDIA_PLAY_STATE || prevState == StatusConstants.MEDIA_PAUSE_STATE)
                    {
                        mediaElement.Stop();
                        mediaElement.PlayerProgressTimer.Stop();
                    }
                    break;
                case StatusConstants.MEDIA_PLAY_STATE:
                    //log.Info(" MultiMediaState => PLAY ");
                    mediaElement.Volume = 1.0;
                    mediaElement.Play();
                    mediaElement.PlayerProgressTimer.Start();
                    break;
                case StatusConstants.MEDIA_PAUSE_STATE:
                    //log.Info(" MultiMediaState => PAUSE ");
                    mediaElement.Pause();
                    mediaElement.PlayerProgressTimer.Pause();
                    break;
            }
        }));

        private void PlayerProgressTimer_Tick(int counter, bool initTick, object state)
        {
            try
            {
                if (initTick) return;

                Application.Current.Dispatcher.Invoke(() =>
                {
                    bool hasTimeSpan = NaturalDuration.HasTimeSpan;
                    if (hasTimeSpan)
                    {
                        TimeSpan tempTimeSpan = Position;
                        double diff = Math.Ceiling(1000 - (tempTimeSpan.TotalMilliseconds % 1000));
                        OffsetTimeSpan = diff < 100 ? tempTimeSpan.Add(TimeSpan.FromMilliseconds(diff)) : tempTimeSpan;
                        OffsetPosition = (int)OffsetTimeSpan.TotalSeconds;
                        //log.Info(" OffsetPosition => " + OffsetPosition + ", TotalSeconds => " + TotalTimeSpan.TotalSeconds + ", ProgressPercentage => " + ProgressPercentage);

                        if (TotalTimeSpan.TotalSeconds <= 0)
                        {
                            TotalTimeSpan = NaturalDuration.TimeSpan;
                        }
                        ProgressPercentage = (int)((OffsetTimeSpan.TotalSeconds * 100) / (int)TotalTimeSpan.TotalSeconds);

                        if ((int)OffsetTimeSpan.TotalSeconds >= (int)TotalTimeSpan.TotalSeconds)
                        {
                            PlayerProgressTimer.Stop();
                            MultiMediaState = StatusConstants.MEDIA_INIT_STATE;
                        }
                    }
                }, DispatcherPriority.Send);
            }
            catch (Exception ex)
            {
                log.Error("Error: PlayerProgressTimer_Tick() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public new void Close()
        {
            if (LoadedBehavior == MediaState.Pause)
            {
                LoadedBehavior = MediaState.Manual;
            }
            PlayerProgressTimer.Tick -= PlayerProgressTimer_Tick;
            MediaOpened -= ChatMediaElement_MediaOpened;
            PlayerProgressTimer.Stop();
            base.Close();
        }

    }
}

﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using View.Constants;

namespace View.Utility
{
    public class ChatTextBlock : TextBlock
    {
        
        private static readonly ILog log = LogManager.GetLogger(typeof(ChatTextBlock).Name);
        bool m_IgnoreChanges = false;

        public ChatTextBlock()
        {
        }

        public string Content
        {
            get { return (string)GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }

        public static readonly DependencyProperty ContentProperty = DependencyProperty.Register("Content", typeof(string), typeof(ChatTextBlock), new UIPropertyMetadata(string.Empty, OnContentChanged));
        /// <summary>
        /// 
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        static void OnContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ChatTextBlock ctb = (ChatTextBlock)d;
            if (!ctb.m_IgnoreChanges)
            {
                ctb.m_IgnoreChanges = true;
                ctb.Text = (string)e.NewValue;
                ctb.ProcessInlines(ctb.Inlines);
                ctb.m_IgnoreChanges = false;
            }
        }

        public int EmoSize
        {
            get { return (int)GetValue(EmoSizeProperty); }
            set { SetValue(EmoSizeProperty, value); }
        }

        public static readonly DependencyProperty EmoSizeProperty = DependencyProperty.Register("EmoSize", typeof(int), typeof(ChatTextBlock), new UIPropertyMetadata(25));

        private static int FindFirstEmoticon(string text, int startIndex, out string found)
        {
            found = string.Empty;
            int minIndex = -1;
            try
            {
                foreach (KeyValuePair<String, BitmapImage> e in ImageObjects.EMOTICON_ICON.ToList())
                {
                    int index = text.IndexOf(e.Key, startIndex);
                    if (index >= 0)
                    {
                        if (minIndex < 0 || index < minIndex)
                        {
                            minIndex = index;
                            found = e.Key;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
               log.Error("Error: FindFirstEmoticon()." + ex.Message + "\n" + ex.StackTrace);
            }
            return minIndex;
        }

        private void ProcessInlines(InlineCollection inlines)
        {
            try
            {
                for (int inlineIndex = 0; inlineIndex < inlines.Count; inlineIndex++)
                {
                    Inline i = inlines.ElementAt(inlineIndex);
                    if (i is Run)
                    {
                        Run r = i as Run;
                        string text = r.Text;

                        //BitmapImage emoticonImage = null;
                        string found = string.Empty;
                        int index = FindFirstEmoticon(text, 0, out found);

                        if (index >= 0)
                        {
                            if (this.TextWrapping == System.Windows.TextWrapping.NoWrap && text.Substring(0, index).Contains("\r\n"))
                            {
                                return;
                            }

                            TextPointer start = i.ContentStart;
                            while (!start.GetTextInRun(LogicalDirection.Forward).StartsWith(found))
                                start = start.GetNextInsertionPosition(LogicalDirection.Forward);
                            TextPointer end = start;
                            for (int j = 0; j < found.Length; j++)
                                end = end.GetNextInsertionPosition(LogicalDirection.Forward);

                            if (!string.IsNullOrEmpty(found))
                            {
                                CreateEmoticonImage(found, start, end, EmoSize);
                            }

                            if (this.TextWrapping == System.Windows.TextWrapping.NoWrap && text.Substring(index + found.Length).Contains("\r\n"))
                            {
                                return;
                            }

                            //else
                            //{
                            //    CreateHyperlink(found, start, end);
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ProcessInlines()." + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static void CreateHyperlink(string value, TextPointer start, TextPointer end)
        {
            Uri uri;
            if (!(Uri.TryCreate(value, UriKind.Absolute, out uri) && (uri.Scheme == Uri.UriSchemeHttp || uri.Scheme == Uri.UriSchemeHttps || uri.Scheme == Uri.UriSchemeFtp)))
            {
                uri = new Uri("http://" + value);
            }

            Hyperlink hyperlink = new Hyperlink(start, end);
            hyperlink.NavigateUri = uri;
            hyperlink.TargetName = "_blank";
            hyperlink.Cursor = Cursors.Hand;
            hyperlink.Foreground = Brushes.SkyBlue;
            hyperlink.Click += (sender, e) =>
            {
                try
                {
                    Hyperlink link = (Hyperlink)sender;
                    Process.Start(link.NavigateUri.ToString());
                }
                catch (Exception ex)
                {
                    log.Error("Error: HyperLink Click." + ex.Message + "\n" + ex.StackTrace);
                }
            };
            //hyperlink.Inlines.Add(value);
        }

        public static void CreateEmoticonImage(string value, TextPointer start, TextPointer end, int emoSize)
        {
            TextRange tr = new TextRange(start, end);
            tr.Text = string.Empty; 
         
            Image image = new Image();
            image.ToolTip = value;
            BitmapImage icon = ImageObjects.EMOTICON_ICON[value];
            if (icon == null)
            {
                EmoticonDTO entry = DefaultDictionaries.Instance.EMOTICON_DICTIONARY[value];
                if (entry != null)
                {
                    icon = ImageUtility.GetBitmapImageOfDynamicResource(RingIDSettings.EMOTICON_FOLDER + Path.DirectorySeparatorChar + "dmid" + Path.DirectorySeparatorChar + entry.Url);
                    if (icon != null)
                    {
                        ImageObjects.EMOTICON_ICON[entry.Symbol] = icon;
                    }
                }
            }
            
            image.Source = icon;
            image.Height = emoSize;
            image.Width = emoSize;
            image.Margin = new Thickness(0, 0, 0, 0);

            InlineUIContainer iui = new InlineUIContainer(image, start);
            iui.BaselineAlignment = BaselineAlignment.Center;
        }

    }
}

﻿using View.Utility.Feed;
using View.Utility.FriendList;
using View.Utility.InternetOptions;
using View.Utility.Myprofile;
using View.Utility.Notification;
using View.Utility.SignInSignUP;

namespace View.Utility
{
    public class ThreadController
    {
        #region "Private Fields"
        //private BWAppStart bWAppStart;
        private KeepAlive keepAlive;
        private ThradNoInternet thradNoInternet;
        private DeleteOldImages deleteOldImages;
        private ThreadContactUtIdsRequest _ContactUtIdsRequest;
        private ThreadContactListRequest _ContactListRequest;
        //private ThradMyNewsFeedsRequest _MyNewsFeedsRequest;
        //private ThreadSendOrVerfyCodeForEmail _SendOrVerfyCodeForEmail;
        private ThreadDetailsOfaFeed _DetailsOfaFeed;
        private ThreadLeftSearchFriend _LeftSearchFriend;
        private ThrdChangeMood _ChangeMood;
        private ThreadHideSpecificFeed _HideFeed;
        private ThreadHideSpecificCircle _HideCircle;
        private ThreadHideSpecificUser _HideUser;
        private ThreadListEditHistories _ListEditHistories;
        private ThreadEditAnyFeed _EditFeed;
        private ThreadEditMediaOrAlbumInfo _EditMediaOrAlbumInfo;

        #endregion "Private Fields"

        #region "Public Fields"
        //public ThradDigitsVerificationThread DigitsVerificationThread;
        public ThradMyProfileDetails ThradMyProfileDetails = null;
        public ThradAcceptFriendRequest thradAcceptFriendRequest;
        public SuggestionTimer InstanceOfSuggestionTimer;
        //public ThradNewsFeedsRequest thradNewsFeedsRequest;
        //  public ThreadSendOrVerfyCodeForEmail SendPasscodeSignUP;
        public ThreadChangeNotificationStateRequest ChangeNotificationStateRequest;


        public ThreadDetailsOfaFeed DetailsOfaFeed
        {
            get { if (_DetailsOfaFeed == null) _DetailsOfaFeed = new ThreadDetailsOfaFeed(); return _DetailsOfaFeed; }
        }

        public ThreadHideSpecificCircle HideCircle
        {
            get { if (_HideCircle == null) _HideCircle = new ThreadHideSpecificCircle(); return _HideCircle; }
        }

        public ThreadHideSpecificUser HideUser
        {
            get { if (_HideUser == null) _HideUser = new ThreadHideSpecificUser(); return _HideUser; }
        }

        public ThreadHideSpecificFeed HideFeed
        {
            get { if (_HideFeed == null) _HideFeed = new ThreadHideSpecificFeed(); return _HideFeed; }
        }

        public ThreadListEditHistories ListEditHistories
        {
            get { if (_ListEditHistories == null) _ListEditHistories = new ThreadListEditHistories(); return _ListEditHistories; }
        }

        public ThreadEditAnyFeed EditFeed
        {
            get { if (_EditFeed == null) _EditFeed = new ThreadEditAnyFeed(); return _EditFeed; }
        }

        public ThreadEditMediaOrAlbumInfo EditMediaOrAlbumInfo
        {
            get { if (_EditMediaOrAlbumInfo == null) _EditMediaOrAlbumInfo = new ThreadEditMediaOrAlbumInfo(); return _EditMediaOrAlbumInfo; }
        }

        #endregion "Private Fields"

        #region "Constructors"
        #endregion "Constructors"

        #region "Properties"
        public ThreadLeftSearchFriend LeftSearchFriend
        {
            get
            {
                if (_LeftSearchFriend == null)
                { _LeftSearchFriend = new ThreadLeftSearchFriend(); }
                return _LeftSearchFriend;
            }
        }
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        #endregion "Private methods"

        #region "Public Methods"

        public ThreadContactUtIdsRequest ContactUtIdsRequest()
        {
            if (_ContactUtIdsRequest == null) _ContactUtIdsRequest = new ThreadContactUtIdsRequest();
            return _ContactUtIdsRequest;
        }

        public ThreadContactListRequest ContactListRequest()
        {
            if (_ContactListRequest == null) _ContactListRequest = new ThreadContactListRequest();
            return _ContactListRequest;
        }

        public ThrdChangeMood ChangeMood()
        {
            if (_ChangeMood == null) _ChangeMood = new ThrdChangeMood();
            return _ChangeMood;
        }

        public bool PingNow(bool needToStartNetworkThread = true)
        {
            return InternetCheckUtility.CheckIsNetwork(needToStartNetworkThread);
        }

        public void StartKeepAliveThread()
        {
            if (keepAlive == null) keepAlive = new KeepAlive();
            keepAlive.StartThread();
        }

        public void StopKeepAliveThread()
        {
            if (keepAlive != null) keepAlive.StopThread();
        }

        public void StartNetworkThread()
        {
            if (thradNoInternet == null) thradNoInternet = new ThradNoInternet();
            thradNoInternet.StartThread();
        }

        public void StopNetworkThread()
        {
            if (thradNoInternet != null) thradNoInternet.StopThread();
        }

        public void StartDeleteOldImagesThread()
        {
            if (deleteOldImages == null) deleteOldImages = new DeleteOldImages();
            deleteOldImages.StartThread();
        }
        #endregion "Public methods"
    }
}

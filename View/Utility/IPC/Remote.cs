﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using log4net;
using Models.Constants;
using View.Constants;
using View.UI;
using View.Utility.WPFMessageBox;

//From http://bartdesmet.net/blogs/bart/archive/2007/04/12/getting-started-with-named-pipes.aspx
namespace View.Utility.IPC
{
    class Remote
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Remote).Name);
        static string CHANNEL = RingIDSettings.APP_NAME;
        const int TIMEOUT = 30000;  //30 seconds if we start an application
        const int EXIT_TIME = 1000; //1 second if we are exiting

        static NamedPipeServerStream server;
        static Thread thread;
        static StreamReader reader;
        static bool waiting;
        static bool abort;

        #region "Property"
        #endregion "Property"

        public static void Send(string args)
        {
            using (NamedPipeClientStream client = new NamedPipeClientStream(".", CHANNEL, PipeDirection.InOut))
            {
                try
                {
                    client.Connect(TIMEOUT);
                }
                finally { }

                using (StreamWriter writer = new StreamWriter(client))
                {
                    writer.WriteLine(args);
                    writer.Flush();
                }
            }
        }

        public static void Start()
        {
            server = new NamedPipeServerStream(CHANNEL, PipeDirection.InOut);
            thread = new Thread(WaitForMessages);
            thread.Start();
        }

        private static void WaitForMessages()
        {
            while (!abort)
            {
                try
                {
                    server.WaitForConnection();
                    if (abort) break;
                    reader = new StreamReader(server);
                    while (server.IsConnected)
                    {
                        if (abort) break;
                        string line = reader.ReadLine();
                        if (!string.IsNullOrEmpty(line))
                        {
                            if (line.Equals(StartUpConstatns.ARGUMENT_EXIT_FROM_ANOTHER_PROCESS))
                            {
                                Application.Current.Dispatcher.BeginInvoke(() =>
                                  {
                                      if (WNRingIDMain.WinRingIDMain != null) WNRingIDMain.WinRingIDMain.FocusWindow();
                                      //MessageBoxResult result = CustomMessageBox.ShowQuestion(NotificationMessages.EXIT_NOTIFICAITON, RingIDSettings.APP_NAME);
                                      //if (result == MessageBoxResult.Yes)
                                      //{
                                      bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.SURE_WANT_TO, "exit"), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Exit"));
                                      if (isTrue)
                                      {
                                          End();
                                          if (UCGuiRingID.Instance != null) UIHelperMethods.ShowSignoutLoader(true, UCGuiRingID.Instance.motherGridToShowPopup);
                                          else System.Diagnostics.Process.GetCurrentProcess().Kill();
                                      }

                                  }, DispatcherPriority.Send);
                            }
                            else if (line.Equals(StartUpConstatns.ARGUMENT_RESTART_FROM_ANOTHER_PROCESS))
                            {
                                End();
                                Application.Current.Dispatcher.BeginInvoke(() =>
                                  {
                                      if (UCGuiRingID.Instance != null) UIHelperMethods.ShowSignoutLoader(false, UCGuiRingID.Instance.motherGridToShowPopup);
                                  }, DispatcherPriority.Send);
                            }
                            else if (HelperMethods.IsValidFilePath(line)) HelperMethods.OpenChatForwardBySentToRingID(line);
                        }
                        waiting = true;
                    }
                    reader.Dispose();
                    reader = null;
                    server.Dispose();
                    server = null;
                    if (abort) break;
                    server = new NamedPipeServerStream(CHANNEL, PipeDirection.InOut);
                }
                catch (Exception e)
                {
                    log.Error(e.StackTrace + "==>" + e.Message);
                }
            }
        }

        public static bool MessageWaiting
        {
            get { return waiting; }
        }

        public static void End()
        {
            try
            {
                abort = true;
                if (server != null)
                {
                    if (server.IsConnected) server.Disconnect();
                    else using (NamedPipeClientStream client = new NamedPipeClientStream(".", CHANNEL, PipeDirection.InOut)) client.Connect(EXIT_TIME);
                    server.Dispose();
                    server = null;
                }
                thread = null;
                if (reader != null)
                {
                    reader.Dispose();
                    reader = null;
                }
            }
            finally { }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace View.Utility
{
    public interface ISwitchable
    {
        bool UtilizeState(object state);
    }
}

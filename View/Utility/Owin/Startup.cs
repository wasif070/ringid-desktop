﻿using Owin;
using View.UI;

namespace View.Utility.Owin
{
    public class Startup
    {
        public void Configuration(IAppBuilder sampleapp)
        {
            sampleapp.Run(sample =>
            {
                System.Console.WriteLine("View.Utility.RingPlayer.MediaPlayerController.CurrentURi==>" + View.Utility.RingPlayer.OwinServerSetup.CurrentURi);
                sample.Response.Redirect(View.Utility.RingPlayer.OwinServerSetup.CurrentURi);
                sample.Response.ContentType = "text/plain";
                return sample.Response.WriteAsync("Hello from OWIN");
            });
        }
    }
}

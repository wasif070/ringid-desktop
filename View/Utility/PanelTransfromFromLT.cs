﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace View.Utility
{
    class PanelTransfromFromLT
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        #region "Private Fields"
        private int position = 0;
        TranslateTransform translate = null;
        FrameworkElement element = null;
        private Grid parentElement;
        Point m_start;
        Vector m_startOffset;
        Storyboard sb = null;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        public PanelTransfromFromLT(Grid panel)
        {
            this.parentElement = panel;
        }
        #endregion "Constructors"

        #region "Event Trigger"

        public void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            element = sender as DockPanel;
            translate = element.RenderTransform as TranslateTransform;

            m_start = e.GetPosition(parentElement);
            m_startOffset = new Vector(translate.X, translate.Y);
            element.CaptureMouse();
        }

        public void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            element = sender as DockPanel;
            translate = element.RenderTransform as TranslateTransform;

            if (element.IsMouseCaptured)
            {
                Vector offset = Point.Subtract(e.GetPosition(parentElement), m_start);

                if (offset.X < 0)
                {
                    if (m_startOffset.X + offset.X > 0)
                        //   translate.X = m_startOffset.X + offset.X;
                        moveElementWithAnimation(m_startOffset.X + offset.X, TranslateTransform.XProperty);
                }
                else
                {
                    if (m_startOffset.X + offset.X < (parentElement.ActualWidth - element.ActualWidth))
                        // translate.X = m_startOffset.X + offset.X;
                        moveElementWithAnimation(m_startOffset.X + offset.X, TranslateTransform.XProperty);
                }
                if (offset.Y < 0)
                {
                    if (m_startOffset.Y + offset.Y > 0)
                        // translate.Y = m_startOffset.Y + offset.Y;
                        moveElementWithAnimation(m_startOffset.Y + offset.Y, TranslateTransform.YProperty);
                }
                else
                {
                    if (m_startOffset.Y + offset.Y < (parentElement.ActualHeight - element.ActualHeight))
                        //    translate.Y = m_startOffset.Y + offset.Y;
                        moveElementWithAnimation(m_startOffset.Y + offset.Y, TranslateTransform.YProperty);
                }
            }
        }

        public void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            element = sender as DockPanel;
            TranslateTransform translate = element.RenderTransform as TranslateTransform;

            int halfHeight = (int)parentElement.ActualHeight / 2;
            int halfWidth = (int)parentElement.ActualWidth / 2;
            double xValue = Math.Abs(translate.X);
            double yValue = Math.Abs(translate.Y);

            if (xValue <= halfWidth && yValue <= halfHeight)
            {
                position = 0;
            }
            else if (xValue >= halfWidth && yValue <= halfHeight)
            {
                position = 1;
            }
            else if (xValue <= halfWidth && yValue >= halfHeight)
            {
                position = 2;
            }
            else if (xValue >= halfWidth && yValue >= halfHeight)
            {
                position = 3;
            }
            //Translate();
            Translate();
            element.ReleaseMouseCapture();
        }

        void sb_Completed(object sender, EventArgs e)
        {
            if (sb != null)
            {
                sb.Remove();
                sb.Stop();
                sb = null;

                element.RenderTransform = translate;
            }
        }

        #endregion "Event Trigger"

        #region "Private methods"



        private void moveElementWithAnimation(double value, DependencyProperty property)
        {
            Duration dutn = new Duration(new TimeSpan(0, 0, 0, 0, 0));
            DoubleAnimation direction = new DoubleAnimation(value, dutn);
            translate.BeginAnimation(property, direction);
        }

        private void moveElementWithAnimation(Point translatePosition)
        {
            Duration d = new Duration(new TimeSpan(0, 0, 0, 0, 300));
            DoubleAnimation x = new DoubleAnimation(translatePosition.X, d);
            DoubleAnimation y = new DoubleAnimation(translatePosition.Y, d);
            Storyboard.SetTarget(x, element);
            Storyboard.SetTargetProperty(x,
                        new PropertyPath("(UIElement.RenderTransform).(TranslateTransform.X)"));

            Storyboard.SetTarget(y, element);
            Storyboard.SetTargetProperty(y,
                        new PropertyPath("(UIElement.RenderTransform).(TranslateTransform.Y)"));

            Storyboard sb = new Storyboard();
            sb.Children.Add(x);
            sb.Children.Add(y);
            sb.Completed += sb_Completed;
            sb.Begin();
        }

        #endregion "Private methods"

        #region "Public Methods"

        public void Translate()
        {
            Point translatePosition = new Point(0, 0);
            if (translate != null && element != null)
            {
                //if (position == 0)//00
                //{
                //    DoubleAnimation anim1 = new DoubleAnimation(moved_X, 0, TimeSpan.FromMilliseconds(100));
                //    DoubleAnimation anim2 = new DoubleAnimation(moved_Y, 0, TimeSpan.FromMilliseconds(100));
                //    translate.BeginAnimation(TranslateTransform.XProperty, anim1);
                //    translate.BeginAnimation(TranslateTransform.YProperty, anim2);

                //}
                //else 
                if (position == 1)//01
                {
                    //translate.X = (parentElement.ActualWidth - element.ActualWidth);
                    //translate.Y = 0;
                    translatePosition.X = (parentElement.ActualWidth - element.ActualWidth);
                    translatePosition.Y = 0;
                }
                else if (position == 2)//10
                {
                    //translate.X = 0;
                    //translate.Y = (parentElement.ActualHeight - element.ActualHeight);
                    translatePosition.X = 0;
                    translatePosition.Y = (parentElement.ActualHeight - element.ActualHeight);
                }
                else if (position == 3)//11
                {
                    //translate.X = (parentElement.ActualWidth - element.ActualWidth);
                    //translate.Y = (parentElement.ActualHeight - element.ActualHeight);
                    translatePosition.X = (parentElement.ActualWidth - element.ActualWidth);
                    translatePosition.Y = (parentElement.ActualHeight - element.ActualHeight);
                }
                moveElementWithAnimation(translatePosition);
            }
        }
        //public void Translate()
        //{
        //    if (translate != null && element != null)
        //    {
        //        if (position == 0)//00
        //        {
        //            translate.X = 0;
        //            translate.Y = 0;
        //        }
        //        else if (position == 1)//01
        //        {
        //            translate.X = (parentElement.ActualWidth - element.ActualWidth);
        //            translate.Y = 0;
        //        }
        //        else if (position == 2)//10
        //        {
        //            translate.X = 0;
        //            translate.Y = (parentElement.ActualHeight - element.ActualHeight);
        //        }
        //        else if (position == 3)//11
        //        {
        //            translate.X = (parentElement.ActualWidth - element.ActualWidth);
        //            translate.Y = (parentElement.ActualHeight - element.ActualHeight);
        //        }
        //    }
        //}

        #endregion "Public methods"



    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
namespace View.Utility.FriendProfile
{
    class ThradUnknownProfileInfoRequest
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradUnknownProfileInfoRequest).Name);
        private bool running = false;

        private long userTableId;
        public delegate void OnSuccessHandler(UserBasicInfoDTO userDTO);
        public event OnSuccessHandler OnSuccess;
        private bool InsertIntoDB;
        private bool alreadyFriendRequested;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Delegate Methods"
        private void Callback(UserBasicInfoDTO unknownProfileDTO)
        {
            try
            {
                if (OnSuccess != null)
                {
                    OnSuccess(unknownProfileDTO);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in Callback() => " + ex.Message + "\n" + ex.StackTrace);
            }
            OnSuccess = null;
        }
        #endregion "Delegate Methods"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_UNKNWON_PROFILE_INFO;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.UserTableID] = this.userTableId;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
                if (feedbackfields != null)
                {
                    try
                    {
                        if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                        {
                            JObject UserDetails = (JObject)feedbackfields[JsonKeys.UserDetails];

                            long uID = 0;


                            //UserBasicInfoDTO unknownProfileDTO = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(userTableId);
                            ////    FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(userTableId, out unknownProfileDTO);
                            //if (unknownProfileDTO == null)
                            //{
                            //    unknownProfileDTO = new UserBasicInfoDTO();
                            //    FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[userTableId] = unknownProfileDTO;
                            //}
                            UserBasicInfoDTO unknownProfileDTO = FriendDictionaries.Instance.GetFromBasicInfoDictionaryNotNuallable(userTableId);
                            unknownProfileDTO.UserTableID = userTableId;

                            if (UserDetails[JsonKeys.UserIdentity] != null)
                            {
                                unknownProfileDTO.RingID = (long)UserDetails[JsonKeys.UserIdentity];
                            }

                            if (UserDetails[JsonKeys.FullName] != null)
                            {
                                unknownProfileDTO.FullName = (string)UserDetails[JsonKeys.FullName];
                            }
                            if (UserDetails[JsonKeys.ProfileImage] != null)
                            {
                                unknownProfileDTO.ProfileImage = (string)UserDetails[JsonKeys.ProfileImage];
                            }
                            if (UserDetails[JsonKeys.FriendshipStatus] != null)
                            {
                                unknownProfileDTO.FriendShipStatus = (int)UserDetails[JsonKeys.FriendshipStatus];
                            }
                            if (UserDetails[JsonKeys.CallAccess] != null)
                            {
                                unknownProfileDTO.CallAccess = (int)UserDetails[JsonKeys.CallAccess];
                            }
                            if (UserDetails[JsonKeys.ChatAccess] != null)
                            {
                                unknownProfileDTO.ChatAccess = (int)UserDetails[JsonKeys.ChatAccess];
                            }
                            if (UserDetails[JsonKeys.FeedAccess] != null)
                            {
                                unknownProfileDTO.FeedAccess = (int)UserDetails[JsonKeys.FeedAccess];
                            }
                            if (UserDetails[JsonKeys.UserTableID] != null)
                            {
                                unknownProfileDTO.UserTableID = (long)UserDetails[JsonKeys.UserTableID];
                            }

                            if (this.userTableId == DefaultSettings.RINGID_OFFICIAL_UTID)
                            {
                                unknownProfileDTO.FriendShipStatus = StatusConstants.FRIENDSHIP_STATUS_ACCEPTED;
                                unknownProfileDTO.ContactType = SettingsConstants.SPECIAL_CONTACT;
                                unknownProfileDTO.CallAccess = 1;
                                unknownProfileDTO.ChatAccess = 1;
                                unknownProfileDTO.FeedAccess = 1;
                            }

                            unknownProfileDTO.BirthDate = DateTime.MinValue;
                            unknownProfileDTO.MarriageDate = DateTime.MinValue;

                            if (InsertIntoDB)
                            {
                                FriendDictionaries.Instance.AddOrUPdateInBasicInfoDictionary(userTableId, unknownProfileDTO, true);
                                // FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[userTableId] = unknownProfileDTO;
                                List<UserBasicInfoDTO> userList = new List<UserBasicInfoDTO>();
                                userList.Add(unknownProfileDTO);
                                new InsertIntoUserBasicInfoTable(userList).Start();
                            }

                            MainSwitcher.AuthSignalHandler().profileSignalHandler.UI_UnknownProfileInfoRequest(unknownProfileDTO, alreadyFriendRequested);

                            Callback(unknownProfileDTO);
                        }
                    }
                    finally { }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    Callback(null);
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {
                Callback(null);

            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"

        public void StartThread(long userTableId, bool insertIntoDB = false, bool alreadyFriendRequested = false)
        {
            if (!running)
            {
                this.userTableId = userTableId;
                this.InsertIntoDB = insertIntoDB;
                this.alreadyFriendRequested = alreadyFriendRequested;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }

        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading;

namespace View.Utility.FriendProfile
{
    public class ThreadChangeFriendAccess
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ThreadChangeFriendAccess).Name);
        private long _UserTableId;
        private SettingsDTO _Settings;

        public delegate void CompleteHandler(SettingsDTO settings, bool status, string errorMsg);
        public event CompleteHandler OnComplete;

        public ThreadChangeFriendAccess(long userTableId, SettingsDTO settings)
        {
            this._UserTableId = userTableId;
            this._Settings = settings;
        }

        public void Start()
        {
            Thread th = new Thread(Run);
            th.Name = this.GetType().Name;
            th.Start();
        }

        public void Run()
        {
            if (!String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {

                try
                {
                    JObject pakToSend = new JObject();
                    String packetID = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = packetID;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_UPDATE_CONTACT_ACCESS;
                    pakToSend[JsonKeys.SettingsName] = _Settings.SettingsName;
                    pakToSend[JsonKeys.SettingsValue] = _Settings.SettingsValue;
                    pakToSend[JsonKeys.UserTableID] = _UserTableId;

                    JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetID);
                    if (feedbackfields != null)
                    {
                        try
                        {
                            if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                            {
                                UserBasicInfoDTO userBasicInfo = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(_UserTableId);
                                //    FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(_UserTableId, out userBasicInfo);
                                if (userBasicInfo != null)
                                {
                                    if (feedbackfields[JsonKeys.SettingsName] != null && feedbackfields[JsonKeys.SettingsValue] != null)
                                    {
                                        if ((int)feedbackfields[JsonKeys.SettingsName] == StatusConstants.CALL_ACCESS)
                                        {
                                            userBasicInfo.CallAccess = (int)feedbackfields[JsonKeys.SettingsValue];
                                        }
                                        else if ((int)feedbackfields[JsonKeys.SettingsName] == StatusConstants.CHAT_ACCESS)
                                        {
                                            userBasicInfo.ChatAccess = (int)feedbackfields[JsonKeys.SettingsValue];
                                        }
                                        else if ((int)feedbackfields[JsonKeys.SettingsName] == StatusConstants.FEED_ACCESS)
                                        {
                                            userBasicInfo.FeedAccess = (int)feedbackfields[JsonKeys.SettingsValue];
                                        }

                                        List<UserBasicInfoDTO> contactList = new List<UserBasicInfoDTO>();
                                        contactList.Add(userBasicInfo);
                                        new InsertIntoUserBasicInfoTable(contactList).Start();
                                    }
                                }

                                Callback(true, String.Empty);
                            }
                            else
                            {
                                String msg = String.Empty;
                                if (feedbackfields[JsonKeys.Message] != null)
                                {
                                    msg = (string)feedbackfields[JsonKeys.Message];
                                }
                                else if (feedbackfields[JsonKeys.ReasonCode] != null)
                                {
                                    msg = ReasonCodeConstants.GetReason((int)feedbackfields[JsonKeys.ReasonCode]);
                                }

                                Callback(false, msg);
                            }
                        }
                        finally
                        {
                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetID, out feedbackfields);
                        }
                    }
                    else
                    {
                        Callback(false, String.Empty);
                        if (!MainSwitcher.ThreadManager().PingNow()) { }
                    }
                    //string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
                    //SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_UPDATE, data);
                    //Thread.Sleep(25);
                    //for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
                    //{
                    //    if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
                    //    {
                    //        break;
                    //    }
                    //    Thread.Sleep(DefaultSettings.WAITING_TIME);
                    //    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(pakId))
                    //    {
                    //        if (i % DefaultSettings.SEND_INTERVAL == 0)
                    //            SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_UPDATE, data);
                    //    }
                    //    else
                    //    {
                    //        JObject feedbackfields = null;
                    //        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out feedbackfields);

                    //        if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                    //        {
                    //            UserBasicInfoDTO userBasicInfo = null;
                    //            FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(_UserIdentity, out userBasicInfo);
                    //            if (userBasicInfo != null)
                    //            {
                    //                if (feedbackfields[JsonKeys.SettingsName] != null && feedbackfields[JsonKeys.SettingsValue] != null)
                    //                {
                    //                    if ((int)feedbackfields[JsonKeys.SettingsName] == StatusConstants.CALL_ACCESS)
                    //                    {
                    //                        userBasicInfo.CallAccess = (int)feedbackfields[JsonKeys.SettingsValue];
                    //                    }
                    //                    else if ((int)feedbackfields[JsonKeys.SettingsName] == StatusConstants.CHAT_ACCESS)
                    //                    {
                    //                        userBasicInfo.ChatAccess = (int)feedbackfields[JsonKeys.SettingsValue];
                    //                    }
                    //                    else if ((int)feedbackfields[JsonKeys.SettingsName] == StatusConstants.FEED_ACCESS)
                    //                    {
                    //                        userBasicInfo.FeedAccess = (int)feedbackfields[JsonKeys.SettingsValue];
                    //                    }

                    //                    List<UserBasicInfoDTO> contactList = new List<UserBasicInfoDTO>();
                    //                    contactList.Add(userBasicInfo);
                    //                    new InsertIntoUserBasicInfoTable(contactList).Start();
                    //                }
                    //            }

                    //            Callback(true, String.Empty);
                    //        }
                    //        else
                    //        {
                    //            String msg = String.Empty;
                    //            if (feedbackfields[JsonKeys.Message] != null)
                    //            {
                    //                msg = (string)feedbackfields[JsonKeys.Message];
                    //            }
                    //            else if (feedbackfields[JsonKeys.ReasonCode] != null)
                    //            {
                    //                msg = ReasonCodeConstants.GetReason((int)feedbackfields[JsonKeys.ReasonCode]);
                    //            }

                    //            Callback(false, msg);
                    //        }
                    //        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
                    //        return;
                    //    }
                    //    PingInServer.StartThread(i, DefaultSettings.TRYING_TIME);
                    //}
                }
                catch (Exception e)
                {
                    log.Error("ChangeFriendAccess ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
            }
            else
            {
                log.Error("ChangeFriendAccess Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            }

            //Callback(false, String.Empty);
        }

        private void Callback(bool status, string errorMsg)
        {
            try
            {
                if (OnComplete != null)
                {
                    OnComplete(_Settings, status, errorMsg);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in Callback() => " + ex.Message + "\n" + ex.StackTrace);
            }
            OnComplete = null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;

namespace View.Utility.FriendProfile
{
    public class ThradFriendDetailsInfoRequest
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradFriendDetailsInfoRequest).Name);
        private bool running = false;
        private long _Utid;
        public delegate void OnSuccessHandler(UserBasicInfoDTO userDTO);
        public event OnSuccessHandler OnSuccess;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Delegate Methods"
        private void Callback(UserBasicInfoDTO userDTO)
        {
            try
            {
                if (OnSuccess != null)
                {
                    OnSuccess(userDTO);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in Callback() => " + ex.Message + "\n" + ex.StackTrace);
            }
            OnSuccess = null;
        }
        #endregion "Delegate Methods"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_USER_DETAILS;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.UserTableID] = this._Utid;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
                if (feedbackfields != null)
                {
                    try
                    {
                        if (feedbackfields != null && (bool)feedbackfields[JsonKeys.Success])
                        {
                            JObject UserDetails = (JObject)feedbackfields[JsonKeys.UserDetails];

                            UserBasicInfoDTO _FriendDetailsInDictionary = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(_Utid);
                            // FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(_Utid, out _FriendDetailsInDictionary);
                            if (_FriendDetailsInDictionary != null)
                            {
                                #region Commented Code
                                //if (UserDetails[JsonKeys.UserIdentity] != null)
                                //{
                                //<<<<<<< .mine
                                //                                    _FriendDetailsInDictionary.RingID = (long)UserDetails[JsonKeys.UserIdentity];
                                //                                }
                                //                                if (UserDetails[JsonKeys.UserTableID] != null)
                                //                                {
                                //                                    _FriendDetailsInDictionary.UserTableID = (long)UserDetails[JsonKeys.UserTableID];
                                //                                }
                                //                                else
                                //                                {
                                //                                    _FriendDetailsInDictionary.UserTableID = this._Utid;
                                //                                }
                                //                                if (UserDetails[JsonKeys.FullName] != null)
                                //                                {
                                //                                    _FriendDetailsInDictionary.FullName = (string)UserDetails[JsonKeys.FullName];
                                //                                }
                                //                                if (UserDetails[JsonKeys.Gender] != null)
                                //                                {
                                //                                    _FriendDetailsInDictionary.Gender = (string)UserDetails[JsonKeys.Gender];
                                //                                }
                                //                                if (UserDetails[JsonKeys.BirthDay] != null)
                                //                                {
                                //                                    //if ((long)UserDetails[JsonKeys.BirthDay] != 1)
                                //                                    //{
                                //                                    _FriendDetailsInDictionary.BirthDay = (long)UserDetails[JsonKeys.BirthDay];
                                //                                    //}
                                //=======
                                #endregion

                                int bDay = 0, bMnth = 0, bYr = 0, mDay = 0, mMnth = 0, mYr = 0;
                                if (UserDetails[JsonKeys.UserIdentity] != null)
                                {
                                    _FriendDetailsInDictionary.RingID = (long)UserDetails[JsonKeys.UserIdentity];
                                }
                                if (UserDetails[JsonKeys.UserTableID] != null)
                                {
                                    _FriendDetailsInDictionary.UserTableID = (long)UserDetails[JsonKeys.UserTableID];
                                }
                                else
                                {
                                    _FriendDetailsInDictionary.UserTableID = this._Utid;
                                }
                                if (UserDetails[JsonKeys.FullName] != null)
                                {
                                    _FriendDetailsInDictionary.FullName = (string)UserDetails[JsonKeys.FullName];
                                }
                                if (UserDetails[JsonKeys.Gender] != null)
                                {
                                    _FriendDetailsInDictionary.Gender = (string)UserDetails[JsonKeys.Gender];
                                }

                                //BirthDate
                                if (UserDetails[JsonKeys.BirthDay] != null) bDay = (int)UserDetails[JsonKeys.BirthDay];
                                if (UserDetails[JsonKeys.BirthMonth] != null) bMnth = (int)UserDetails[JsonKeys.BirthMonth];
                                if (UserDetails[JsonKeys.BirthYear] != null) bYr = (int)UserDetails[JsonKeys.BirthYear];

                                if (bDay > 0 && bMnth > 0 && bYr > 0) _FriendDetailsInDictionary.BirthDate = new DateTime(bYr, bMnth, bDay);
                                else _FriendDetailsInDictionary.BirthDate = DateTime.MinValue;
                                //if (UserDetails[JsonKeys.BirthDay] != null)
                                //{
                                //    //if ((long)UserDetails[JsonKeys.BirthDay] != 1)
                                //    //{
                                //    _FriendDetailsInDictionary.BirthDay = (long)UserDetails[JsonKeys.BirthDay];
                                //    //}
                                //}

                                //Marriagedate
                                if (UserDetails[JsonKeys.MarriageDay] != null) mDay = (int)UserDetails[JsonKeys.MarriageDay];
                                if (UserDetails[JsonKeys.MarriageMonth] != null) mMnth = (int)UserDetails[JsonKeys.MarriageMonth];
                                if (UserDetails[JsonKeys.MarriageYear] != null) mYr = (int)UserDetails[JsonKeys.MarriageYear];

                                if (mDay > 0 && mMnth > 0 && mYr > 0) _FriendDetailsInDictionary.MarriageDate = new DateTime(mYr, mMnth, mDay);
                                else _FriendDetailsInDictionary.MarriageDate = DateTime.MinValue;                
                                //if (UserDetails[JsonKeys.MarriageDay] != null)
                                //{
                                //    //if ((long)UserDetails[JsonKeys.MarriageDay] != 1)
                                //    // {
                                //    _FriendDetailsInDictionary.MarriageDay = (long)UserDetails[JsonKeys.MarriageDay];
                                //    //  }
                                //}

                                if (UserDetails[JsonKeys.Country] != null)
                                {
                                    _FriendDetailsInDictionary.Country = (string)UserDetails[JsonKeys.Country];
                                }
                                if (UserDetails[JsonKeys.HomeCity] != null)
                                {
                                    _FriendDetailsInDictionary.HomeCity = (string)UserDetails[JsonKeys.HomeCity];
                                }
                                if (UserDetails[JsonKeys.CurrentCity] != null)
                                {
                                    _FriendDetailsInDictionary.CurrentCity = (string)UserDetails[JsonKeys.CurrentCity];
                                }
                                if (UserDetails[JsonKeys.AboutMe] != null)
                                {
                                    _FriendDetailsInDictionary.AboutMe = (string)UserDetails[JsonKeys.AboutMe];
                                }
                                if (UserDetails[JsonKeys.Email] != null)
                                {
                                    _FriendDetailsInDictionary.Email = (string)UserDetails[JsonKeys.Email];
                                }
                                if (UserDetails[JsonKeys.MobilePhone] != null)
                                {
                                    _FriendDetailsInDictionary.MobileNumber = (string)UserDetails[JsonKeys.MobilePhone];
                                }
                                if (UserDetails[JsonKeys.DialingCode] != null)
                                {
                                    _FriendDetailsInDictionary.MobileDialingCode = (string)UserDetails[JsonKeys.DialingCode];
                                }
                                if (UserDetails[JsonKeys.ProfileImage] != null)
                                {
                                    _FriendDetailsInDictionary.ProfileImage = (string)UserDetails[JsonKeys.ProfileImage];
                                }
                                if (UserDetails[JsonKeys.ProfileImageId] != null)
                                {
                                    _FriendDetailsInDictionary.ProfileImageId = (Guid)UserDetails[JsonKeys.ProfileImageId];
                                }
                                if (UserDetails[JsonKeys.CoverImage] != null)
                                {
                                    _FriendDetailsInDictionary.CoverImage = (string)UserDetails[JsonKeys.CoverImage];
                                }
                                if (UserDetails[JsonKeys.CoverImageId] != null)
                                {
                                    _FriendDetailsInDictionary.CoverImageId = (Guid)UserDetails[JsonKeys.CoverImageId];
                                }
                                if (UserDetails[JsonKeys.CropImageX] != null)
                                {
                                    _FriendDetailsInDictionary.CropImageX = (int)UserDetails[JsonKeys.CropImageX];
                                }
                                if (UserDetails[JsonKeys.CropImageY] != null)
                                {
                                    _FriendDetailsInDictionary.CropImageY = (int)UserDetails[JsonKeys.CropImageY];
                                }
                                if (UserDetails[JsonKeys.Privacy] != null)
                                {
                                    JArray privacy = (JArray)UserDetails[JsonKeys.Privacy];

                                    short[] pvc = new short[7];
                                    for (int j = 0; j < privacy.Count; j++)
                                    {
                                        pvc[j] = (short)privacy.ElementAt(j);
                                    }
                                    _FriendDetailsInDictionary.Privacy = pvc;
                                    _FriendDetailsInDictionary.EmailPrivacy = pvc[0];
                                    _FriendDetailsInDictionary.MobilePrivacy = pvc[1];
                                    _FriendDetailsInDictionary.ProfileImagePrivacy = pvc[2];
                                    _FriendDetailsInDictionary.BirthdayPrivacy = pvc[3];
                                    _FriendDetailsInDictionary.CoverImagePrivacy = pvc[4];

                                }
                                if (UserDetails[JsonKeys.UpdateTime] != null)
                                {
                                    _FriendDetailsInDictionary.UpdateTime = (long)UserDetails[JsonKeys.UpdateTime];
                                }
                                if (UserDetails[JsonKeys.DeviceToken] != null)
                                {
                                    _FriendDetailsInDictionary.DeviceToken = (string)UserDetails[JsonKeys.DeviceToken];
                                }
                                if (UserDetails[JsonKeys.CallAccess] != null)
                                {
                                    _FriendDetailsInDictionary.CallAccess = (int)UserDetails[JsonKeys.CallAccess];
                                }
                                if (UserDetails[JsonKeys.ChatAccess] != null)
                                {
                                    _FriendDetailsInDictionary.ChatAccess = (int)UserDetails[JsonKeys.ChatAccess];
                                }
                                if (UserDetails[JsonKeys.FeedAccess] != null)
                                {
                                    _FriendDetailsInDictionary.FeedAccess = (int)UserDetails[JsonKeys.FeedAccess];
                                }

                                //Album ID
                                if (feedbackfields[JsonKeys.AlbumId] != null)
                                {
                                    _FriendDetailsInDictionary.AlbumId = (Guid)feedbackfields[JsonKeys.AlbumId];
                                }
                                if (feedbackfields[JsonKeys.IsUserFeedHidden] != null)
                                {
                                    _FriendDetailsInDictionary.IsProfileHidden = (bool)feedbackfields[JsonKeys.IsUserFeedHidden];
                                }
                                List<UserBasicInfoDTO> contactList = new List<UserBasicInfoDTO>();
                                contactList.Add(_FriendDetailsInDictionary);
                                new InsertIntoUserBasicInfoTable(contactList).Start();
                                MainSwitcher.AuthSignalHandler().profileSignalHandler.UI_FriendDetailsInfoRequest(_FriendDetailsInDictionary);
                                Callback(_FriendDetailsInDictionary);
                            }

                        }
                        else
                        {

                        }
                    }
                    finally { }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    Callback(null);
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {
                Callback(null);
            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(long utId)
        {
            if (!running)
            {
                this._Utid = utId;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

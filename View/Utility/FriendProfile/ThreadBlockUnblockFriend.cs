﻿using Auth.utility;
using log4net;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading;
using View.BindingModels;
using View.ViewModel;

namespace View.Utility.FriendProfile
{
    public class ThreadBlockUnblockFriend
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ThreadBlockUnblockFriend).Name);

        private long _UserTableId;
        private int _Block;

        public delegate void CompleteHandler(bool status, string errorMsg);
        public event CompleteHandler OnComplete;

        public ThreadBlockUnblockFriend(long utid, int block)
        {
            this._UserTableId = utid;
            this._Block = block;
        }

        public void Start()
        {
            Thread th = new Thread(Run);
            th.Name = this.GetType().Name;
            th.Start();
        }

        public void Run()
        {
            if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                try
                {
                    JObject pakToSend = new JObject();
                    String packetID = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = packetID;

                    JArray jArray = new JArray();
                    jArray.Add((long)_UserTableId);

                    pakToSend[JsonKeys.ContactUtIdList] = jArray;
                    pakToSend[JsonKeys.Action] = AppConstants.TYPE_BLOCK_UNBLOCK_FRIEND;
                    pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                    pakToSend[JsonKeys.BlockValue] = _Block;


                    JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetID);
                    if (feedbackfields != null)
                    {
                        try
                        {
                            if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                            {
                                UserBasicInfoDTO userBasicInfo = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(_UserTableId);
                                //FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(_UserTableId, out userBasicInfo);
                                if (userBasicInfo != null)
                                {
                                    userBasicInfo.CallAccess = _Block;
                                    userBasicInfo.ChatAccess = _Block;
                                    userBasicInfo.FeedAccess = _Block;
                                    List<UserBasicInfoDTO> contactList = new List<UserBasicInfoDTO>();
                                    contactList.Add(userBasicInfo);
                                    new InsertIntoUserBasicInfoTable(contactList).Start();
                                }
                                Callback(true, String.Empty);
                            }
                            else
                            {
                                String msg = String.Empty;
                                if (feedbackfields[JsonKeys.Message] != null)
                                {
                                    msg = (string)feedbackfields[JsonKeys.Message];
                                }
                                else if (feedbackfields[JsonKeys.ReasonCode] != null)
                                {
                                    msg = ReasonCodeConstants.GetReason((int)feedbackfields[JsonKeys.ReasonCode]);
                                }

                                Callback(false, msg);
                            }


                            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(packetID, out feedbackfields);
                        }
                        finally
                        {

                        }
                    }
                    else
                    {
                        Callback(false, String.Empty);
                        if (!MainSwitcher.ThreadManager().PingNow()) { }
                    }

                    //string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
                    //SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
                    //Thread.Sleep(25);
                    //for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
                    //{
                    //    if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
                    //    {
                    //        break;
                    //    }
                    //    Thread.Sleep(DefaultSettings.WAITING_TIME);
                    //    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(pakId))
                    //    {
                    //        if (i % DefaultSettings.SEND_INTERVAL == 0)
                    //            SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
                    //    }
                    //    else
                    //    {
                    //        JObject feedbackfields = null;
                    //        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out feedbackfields);

                    //        if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                    //        {
                    //            long uID = 0;
                    //            if (FriendDictionaries.Instance.UTID_UID_DICTIONARY.TryGetValue(_UserTableId, out uID))
                    //            {
                    //                UserBasicInfoDTO userBasicInfo = null;
                    //                FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY.TryGetValue(uID, out userBasicInfo);
                    //                if (userBasicInfo != null)
                    //                {
                    //                    userBasicInfo.CallAccess = _Block;
                    //                    userBasicInfo.ChatAccess = _Block;
                    //                    userBasicInfo.FeedAccess = _Block;
                    //                    if (_Block == StatusConstants.TYPE_ACCESS_UNBLOCKED)
                    //                        userBasicInfo.BlockedBy = 0;
                    //                    List<UserBasicInfoDTO> contactList = new List<UserBasicInfoDTO>();
                    //                    contactList.Add(userBasicInfo);
                    //                    new InsertIntoUserBasicInfoTable(contactList).Start();
                    //                }
                    //            }

                    //            Callback(true, String.Empty);
                    //        }
                    //        else
                    //        {
                    //            String msg = String.Empty;
                    //            if (feedbackfields[JsonKeys.Message] != null)
                    //            {
                    //                msg = (string)feedbackfields[JsonKeys.Message];
                    //            }
                    //            else if (feedbackfields[JsonKeys.ReasonCode] != null)
                    //            {
                    //                msg = ReasonCodeConstants.GetReason((int)feedbackfields[JsonKeys.ReasonCode]);
                    //            }

                    //            Callback(false, msg);
                    //        }
                    //        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out feedbackfields);
                    //        return;
                    //    }
                    //    PingInServer.StartThread(i, DefaultSettings.TRYING_TIME);
                    //}

                }
                catch (Exception ex)
                {
                    log.Error("BlockUnblockFriend ex ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
                }
            }
            else
            {
                log.Error("BlockUnblockFriend Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            }


        }

        private void Callback(bool status, string errorMsg)
        {
            try
            {
                if (OnComplete != null)
                {
                    OnComplete(status, errorMsg);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error in Callback() => " + ex.Message + "\n" + ex.StackTrace);
            }
            OnComplete = null;
        }
    }
}

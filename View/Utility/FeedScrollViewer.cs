﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using log4net;
using Models.Constants;
using View.BindingModels;
using View.Utility;
using View.Utility.Feed;
using View.Utility.DataContainer;

namespace View.UI
{
    public class FeedScrollViewer : ScrollViewer
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(FeedScrollViewer).Name);

        #region "initialize"
        public FeedScrollViewer()
        {
            this.Focusable = true;
            this.FocusVisualStyle = null;
        }

        public void SetScrollValues(CustomObservableCollection viewCollection, SortedList<long, Guid> sortedIds, int profileType, int action)
        {
            this.ViewCollection = viewCollection;
            this.SortedIds = sortedIds;
            this.ProfileType = profileType;
            this.Action = action;
            this.UserTableID = viewCollection.UserProfileUtId;
            this.pId = viewCollection.pId;
            this.CanTopRequest = true;
            this.CanBottomRequest = true;
        }

        public void SetScrollEvents(bool add)
        {
            this.ScrollChanged -= ScrollPositionChanged;
            this.PreviewKeyDown -= ScrollPreviewKeyDown;
            if (add)
            {
                this.ScrollChanged += ScrollPositionChanged;
                this.PreviewKeyDown += ScrollPreviewKeyDown;
            }
        }
        #endregion "initialize"

        #region "variables"
        public CustomObservableCollection ViewCollection;
        private int ProfileType, Action;
        private long UserTableID, pId;
        private SortedList<long, Guid> SortedIds;
        private bool CanTopRequest = true, CanBottomRequest = true;
        #endregion "variables"

        #region "methods"
        private void ScrollPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                this.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                this.ScrollToEnd();
                e.Handled = true;
            }
            //else if (e.Key == Key.Escape && MainSwitcher.PopupController.BasicMediaViewWrapper.ucBasicMediaView != null
            //   && !View.Utility.RingPlayer.RingPlayerViewModel.Instance.SmallPlayerOpen)
            //{
            //    View.Utility.RingPlayer.RingPlayerViewModel.Instance.OnDoCancelRequested();
            //}
        }

        public void ScrollPositionChanged(object sender, ScrollChangedEventArgs e)
        {
            try
            {
                if (e.VerticalChange > 0)
                {
                    if (CanBottomRequest && (this.VerticalOffset >= (this.ScrollableHeight * 0.67)))
                    {
                        Task.Factory.StartNew(() =>
                        {
                            BottomLoadNewsFeeds();
                        });
                    }
                    else if (this.VerticalOffset == this.ScrollableHeight)
                    {
                        Task.Factory.StartNew(() =>
                        {
                            BottomLoadNewsFeeds();
                        });
                    }
                }
                else if (e.VerticalChange < 0)
                {
                    if (CanTopRequest && (this.VerticalOffset <= (this.ScrollableHeight * 0.33)))
                    {
                        Task.Factory.StartNew(() =>
                        {
                            TopLoadNewsFeeds();
                        });
                    }
                    else if (this.VerticalOffset == 0)
                    {
                        Task.Factory.StartNew(() =>
                        {
                            TopLoadNewsFeeds();
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void BottomLoadNewsFeeds()
        {
            try
            {
                if (ViewCollection != null)
                {
                    FeedModel BottomModelInView = ViewCollection.GetLastModel();
                    Guid toInsertIdFromSortedList = Guid.Empty;
                    foreach (var item in SortedIds)
                    {
                        if (item.Value == BottomModelInView.NewsfeedId) { break; }
                        else toInsertIdFromSortedList = item.Value;
                    }
                    FeedModel modeltoInsert = null;
                    if (toInsertIdFromSortedList != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(toInsertIdFromSortedList, out modeltoInsert))
                    {
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            if (!ViewCollection.Any(P => P.NewsfeedId == modeltoInsert.NewsfeedId))
                                ViewCollection.Insert(ViewCollection.Count - 1, modeltoInsert);
                        }, DispatcherPriority.Send);

                        ViewCollection.LoadMoreModel.FeedType = 1;
                    }
                    else if (!ViewCollection.LoadMoreModel.ShowContinue)
                    {
                        long BottomFeedTime = (SortedIds.Count > 0) ? SortedIds.FirstOrDefault().Key : 0;
                        RequestFeeds(BottomFeedTime, 2, 0);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.StackTrace);
            }
        }

        public void TopLoadNewsFeeds()
        {
            try
            {
                if (ViewCollection != null)
                {
                    FeedModel TopModelInView = ViewCollection.GetFirstModel();
                    Guid toInsertIdFromSortedList = Guid.Empty;
                    bool found = (TopModelInView != null) ? false : true;
                    foreach (var item in SortedIds)
                    {
                        if (found) { toInsertIdFromSortedList = item.Value; break; }
                        else if (item.Value == TopModelInView.NewsfeedId) found = true;
                    }
                    FeedModel modeltoInsert = null;
                    if (toInsertIdFromSortedList != Guid.Empty && FeedDataContainer.Instance.FeedModels.TryGetValue(toInsertIdFromSortedList, out modeltoInsert))
                    {
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            if (!ViewCollection.Any(P => P.NewsfeedId == modeltoInsert.NewsfeedId))
                                ViewCollection.Insert(ViewCollection.MinIndex, modeltoInsert);
                        }, DispatcherPriority.Send);
                    }
                    else if (!ViewCollection.LoadMoreModel.ShowContinue)
                    {
                        long TopFeedTime = (SortedIds.Count > 0) ? SortedIds.LastOrDefault().Key : Models.Utility.ModelUtility.CurrentTimeMillis();
                        RequestFeeds(TopFeedTime, 1, 0);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.StackTrace);
            }
        }

        public void RequestFeeds(long pvtUUId, short scroll, int startLimit, string packetId = null)
        {
            if (!ViewCollection.LoadMoreModel.ShowContinue)
            {
                this.ScrollChanged -= ScrollPositionChanged;
                ViewCollection.LoadMoreModel.ShowContinue = true;
                ViewCollection.LoadMoreModel.FeedType = 3;
                ThreadAnyFeedsRequest thread = new ThreadAnyFeedsRequest();
                thread.callBackEvent += (response) =>
                {
                    switch (response)
                    {
                        case SettingsConstants.RESPONSE_SUCCESS:
                            ViewCollection.LoadMoreModel.FeedType = 1;
                            Thread.Sleep(100);
                            if (packetId == null)
                            {
                                if (scroll == 1) TopLoadNewsFeeds();
                                else BottomLoadNewsFeeds();
                            }
                            break;
                        case SettingsConstants.RESPONSE_NOTSUCCESS:
                            if (scroll == 1) CanTopRequest = false;
                            else CanBottomRequest = false;
                            ViewCollection.LoadMoreModel.FeedType = 4;
                            Thread.Sleep(100);
                            break;
                        default: //no response
                            if (packetId != null) ViewCollection.LoadMoreModel.FeedType = 5;
                            else ViewCollection.LoadMoreModel.FeedType = 1;
                            Thread.Sleep(100);
                            break;
                    }
                    //TODO dispatcher timer insteadof thread.sleep & topload bottomload in case of success
                    this.ScrollChanged -= ScrollPositionChanged;
                    this.ScrollChanged += ScrollPositionChanged;
                    ViewCollection.LoadMoreModel.ShowContinue = false;
                };
                //if (time > 0)
                //    thread.StartThread(time, scroll, startLimit, ProfileType, Action, packetId, UserTableID, pId);
                //else
                //    thread.StartThread(pvtUUId, scroll, startLimit, ProfileType, Action, packetId, UserTableID, pId);
            }
        }
        #endregion "methods"
    }
}

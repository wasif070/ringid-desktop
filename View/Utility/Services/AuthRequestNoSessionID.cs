﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace View.Utility.Services
{
    public class AuthRequestNoSessionID
    {
        #region "Fields"
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(AuthRequestNoSessionID).Name);
        bool stopReqeust = false;
        #endregion //"Fields"

        #region "Public Methods"
        public JObject SendRequestNoSessionID(JObject packetToSend, int requestType, string packetID, int firstDelay = 25)
        {
            JObject feedBackFields = null;
            try
            {
                string data = JsonConvert.SerializeObject(packetToSend, Formatting.None);
                SendToServer.SendNormalPacket(requestType, data);
                Thread.Sleep(firstDelay);
                for (int i = 1; i <= DefaultSettings.TRYING_TIME; i++)
                {
                    if (stopReqeust)
                    {
                        feedBackFields = new JObject();
                        feedBackFields[JsonKeys.RequestCancelled] = true;
                        break;
                    }
                    Thread.Sleep(DefaultSettings.WAITING_TIME);
                    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(packetID))
                    {
                        if (i % DefaultSettings.SEND_INTERVAL == 0) SendToServer.SendNormalPacket(requestType, data);
                    }
                    else
                    {
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(packetID, out feedBackFields);
                        break;
                    }
                    if (i == DefaultSettings.TRYING_TIME && feedBackFields == null)
                    {
                        feedBackFields = new JObject();
                        feedBackFields[JsonKeys.Message] = NotificationMessages.REQUEST_TIME_OUT;
                        feedBackFields[JsonKeys.NoResponseFromServer] = true;
                    }
                }
            }
            catch (Exception e) { log.Error("ChangeMood ex ==>" + e.StackTrace); }
            return feedBackFields;
        }

        public void Stop() { stopReqeust = true; }
        #endregion //"Public Methods"
    }
}

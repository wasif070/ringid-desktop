﻿using System;
using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;

namespace View.Utility.Services
{
    public class ThreadVerifyEmailOrMobile
    {
        /// <summary>
        /// If Digits Mobile 
        /// Reqeust=>{"sId":"78593846622446542110010034","actn":212,"idgt":1,"pckId":"2110010034211483441286234"}
        /// Response=>{"sucs":true,"mg":"Your number is successfully verified.","mblDc":"+880","mbl":"1914469984","rc":0,"wOnrT":0,"wOnrId":0}
        /// 
        /// If Email
        /// Reqeust1 (get code)=>>{"sId":"78593846622446542110010034","actn":221,"el":"faiz371@yopmail.com","pckId":"2110010034221483441346607"}
        /// Resposne1=>{"sucs":true,"mg":"Verification code has been sent to your email.","el":"faiz371@yopmail.com","rc":0,"wOnrT":0,"wOnrId":0}
        /// Reqeust2 (Verify code)=>>{"sId":"78593846622446542110010034","actn":221,"el":"faiz371@yopmail.com","vc":"0131","pckId":"2110010034231483441365120"}
        /// Resposne=> {"sucs":true,"mg":"Your email is successfully verified.","vc":"0131","el":"faiz371@yopmail.com","rc":0,"wOnrT":0,"wOnrId":0}
        /// </summary>
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThreadVerifyEmailOrMobile).Name);
        private bool running = false;
        JObject pakToSend = null;
        public event DelegateBoolStringString OnVerifyEmailOrMobile;
        #endregion "Private Fields"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                if (pakToSend != null)
                {
                    String packetId = SendToServer.GetRanDomPacketID();
                    pakToSend[JsonKeys.PacketId] = packetId;

                    JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
                    if (feedbackfields != null)
                    {
                        string msg = (feedbackfields[JsonKeys.Message] != null) ? (string)feedbackfields[JsonKeys.Message] : null;
                        if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                        {
                            if (OnVerifyEmailOrMobile != null) OnVerifyEmailOrMobile(true, msg, null);
                        }
                        else if (OnVerifyEmailOrMobile != null) OnVerifyEmailOrMobile(false, msg, null);
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                    }
                    else
                    {
                        if (OnVerifyEmailOrMobile != null) OnVerifyEmailOrMobile(false, NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK, null);
                        if (!MainSwitcher.ThreadManager().PingNow()) { }
                    }
                }
                else
                {
                    if (OnVerifyEmailOrMobile != null) OnVerifyEmailOrMobile(false, NotificationMessages.FAILDED_TRY_AGAIN, null);
                }
            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(int action, string email, string mobile, string mblDc, string code, bool isDigits = false)
        {
            if (!running)
            {
                pakToSend = new JObject();
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = action;
                if (isDigits) pakToSend[JsonKeys.IsDigits] = 1;
                else
                {
                    if (!string.IsNullOrEmpty(mobile) && !string.IsNullOrEmpty(mblDc))
                    {
                        if (mblDc.Equals("+880") && mobile[0] == '0') mobile = mobile.Substring(1);
                        pakToSend[JsonKeys.MobilePhone] = mobile;
                        pakToSend[JsonKeys.DialingCode] = mblDc;
                    }
                    if (!string.IsNullOrEmpty(email)) pakToSend[JsonKeys.Email] = email;
                    if (!string.IsNullOrEmpty(code)) pakToSend[JsonKeys.VerificationCode] = code;
                }
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }
        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

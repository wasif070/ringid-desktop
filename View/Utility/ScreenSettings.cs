﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace View.Utility
{
    public static class ScreenSettings
    {
        #region constructor
        static ScreenSettings()
        {
            if (System.Windows.SystemParameters.PrimaryScreenWidth > 1000)
            {
                ScreenWidth = System.Windows.SystemParameters.PrimaryScreenWidth - 400;
            }
            else
            {
                ScreenWidth = 1000;
            }
            if (System.Windows.SystemParameters.PrimaryScreenHeight > 720)
            {
                ScreenHeight = System.Windows.SystemParameters.PrimaryScreenHeight - 100;
            }
            else
            {
                ScreenHeight = 720;
            }
            ScreenMinimumWidth = 600;
            ScreenMinimumHeight = 720;
        }
        #endregion
        #region getter setters
        public static double ScreenWidth
        {
            set;
            get;
        }
        public static double ScreenMinimumWidth
        {
            set;
            get;
        }
        public static double ScreenMinimumHeight
        {
            set;
            get;
        }
        public static double ScreenHeight
        {
            set;
            get;
        }
        #endregion
    }

}

﻿using Models.Constants;
using Models.Entity;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace View.Utility
{
    public class FeedUtils
    {

        private static readonly DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static long CurrentTimeMillis()
        {
            return (long)(DateTime.UtcNow - Jan1st1970).TotalMilliseconds;
        }

        public static string GetFormatedTime(long timestamp, string format)
        {
            // Get current UTC time.   
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            DateTime date = start.AddMilliseconds(timestamp).ToLocalTime();
            // Change time to match GMT + 1.
            var gmt1Date = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(date, "W. Europe Standard Time");
            // Output the GMT+1 time in our specified format using the US-culture. 
            var str = gmt1Date.ToString(format, new CultureInfo("en-US"));

            return str;
        }


        public static string GetShowableDate(long milliSeconds, long chatServerTime = 0)
        {

            long timestamp = milliSeconds;
            long currentime = (chatServerTime > 0) ? chatServerTime : CurrentTimeMillis();
            long duration = currentime - timestamp;

            DateTime dt = ModelUtility.GetLocalDateTime(timestamp);

            if (duration < 0)
            {
                duration = 1;
                //return dt.ToString("yyyy MMMM dd");
            }
            long days = (long)TimeSpan.FromMilliseconds(duration).TotalDays;

            if (days > 0)
            {
                DateTime cal = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0, DateTimeKind.Utc);

                long diff = ModelUtility.CurrentTimeMillisLocal(cal) - timestamp;
                long d = (long)TimeSpan.FromMilliseconds(diff).TotalDays;

                if (d == 0)
                {
                    return dt.ToString("'Yesterday at' hh.mm tt");
                }
                else if (d < 6)
                {
                    return dt.ToString("dddd 'at' hh.mm tt");
                }
                else if (d < 365)
                {
                    return dt.ToString("MMMM dd 'at' hh.mm tt");
                }
                else
                {
                    return dt.ToString("yyyy MMMM dd");
                }

            }
            long hours = (long)TimeSpan.FromMilliseconds(duration).TotalHours;
            if (hours > 0)
            {
                return hours + (hours == 1 ? " hr" : " hrs") + " ago";
            }
            long minutes = (long)TimeSpan.FromMilliseconds(duration).TotalMinutes;

            if (minutes > 0)
            {
                return minutes + (minutes == 1 ? " min" : " mins") + " ago";
            }

            long miliseconds = (long)TimeSpan.FromMilliseconds(duration).TotalMilliseconds;
            if (miliseconds > 0)
            {
                return "few secs ago";
            }
            return dt.ToShortDateString() + ", " + dt.ToShortTimeString();

            //long currentime = CurrentTimeMillisLocal();
            //long duration = currentime - milliSeconds;
            //if (duration < 0)
            //{
            //    return GetFormatedTime(milliSeconds, "yyyy MMMM dd");
            //}
            //double days = TimeSpan.FromMilliseconds(duration).TotalDays;
            //if (days > 0)
            //{

            //    DateTime currentDate = new DateTime(currentime);
            //    DateTime cal = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, 0, 0, 0, 0, DateTimeKind.Utc);

            //    long diff = (long)(cal - Jan1st1970Utc).TotalMilliseconds - milliSeconds;
            //    double d = TimeSpan.FromMilliseconds(diff).TotalDays;

            //    if (d == 0)
            //    {
            //        return GetFormatedTime(milliSeconds, "'Yesterday at' hh.mm tt");
            //    }
            //    else if (d < 6)
            //    {
            //        return GetFormatedTime(milliSeconds, "dddd 'at' hh.mm tt");
            //    }
            //    else if (d < 365)
            //    {
            //        return GetFormatedTime(milliSeconds, "MMMM dd 'at' hh.mm tt");
            //    }
            //    else
            //    {
            //        return GetFormatedTime(milliSeconds, "yyyy MMMM dd");
            //    }

            //}
            //double hours = TimeSpan.FromMilliseconds(duration).TotalHours;
            //if (hours > 0)
            //{
            //    return hours + (hours == 1 ? " hr" : " hrs") + " ago";
            //}
            //double minutes = TimeSpan.FromMilliseconds(duration).TotalMinutes;
            //if (minutes > 0)
            //{
            //    return minutes + (minutes == 1 ? " min" : " mins") + " ago";
            //}
            //return "few secs ago";

        }

        public static string GetLastOnlineTime(long milliSeconds, long chatServerTime = 0)
        {

            long timestamp = milliSeconds;
            long currentime = (chatServerTime > 0) ? chatServerTime : CurrentTimeMillis();
            long duration = currentime - timestamp;

            //DateTime dt = ModelUtility.GetLocalDateTime(timestamp);

            //if (duration < 0)
            //{
            //    duration = 1;
            //}

            //long days = (long)TimeSpan.FromMilliseconds(duration).TotalDays;

            //if (days > 0)
            //{
            //    DateTime cal = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0, DateTimeKind.Utc);

            //    long diff = ModelUtility.CurrentTimeMillisLocal(cal) - timestamp;
            //    long d = (long)TimeSpan.FromMilliseconds(diff).TotalDays;

            //    if (d == 0)
            //    {
            //        return dt.ToString("'Yesterday at' hh.mm tt");
            //    }
            //    else if (d < 6)
            //    {
            //        return dt.ToString("dddd 'at' hh.mm tt");
            //    }
            //    else if (d < 365)
            //    {
            //        return dt.ToString("MMMM dd 'at' hh.mm tt");
            //    }
            //    else
            //    {
            //        return dt.ToString("yyyy MMMM dd");
            //    }
            //}

            long hours = (long)TimeSpan.FromMilliseconds(duration).TotalHours;
            if (hours > 23)
            {
                return "";
            }

            if (hours > 0)
            {
                return hours + "h";
                //return hours + (hours == 1 ? " hr" : " hrs") + " ago";
            }

            long minutes = (long)TimeSpan.FromMilliseconds(duration).TotalMinutes;
            if (minutes > 0)
            {
                return minutes + "m";
                //return minutes + (minutes == 1 ? " min" : " mins") + " ago";
            }

            long seconds = (long)TimeSpan.FromMilliseconds(duration).TotalMinutes;
            if (seconds > 0)
            {
                return seconds + "s";
            }

            //long miliseconds = (long)TimeSpan.FromMilliseconds(duration).TotalMilliseconds;
            //if (miliseconds > 0)
            //{
            //    return "few secs ago";
            //}
            return "";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using log4net;
using Models.Constants;
using View.UI;
using View.UI.Chat;
using View.UI.PopUp;
using View.Utility.Chat;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.Utility
{
    public class UIHelperMethods
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UIHelperMethods).Name);
        public static void HideTrayICon()
        {
            if (WNRingIDMain.WinRingIDMain != null) WNRingIDMain.WinRingIDMain.HideTrayIcon();
        }

        public static void ShowErrorMessageBoxFromThread(string msg, string caption)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                WNConfirmationView cv = new WNConfirmationView(caption, msg, CustomConfirmationDialogButtonOptions.OK);
                var result = cv.ShowCustomDialog();
            });
        }

        public static void ShowWarning(string msg, string caption = "Warning!")
        {
            WNConfirmationView cv = new WNConfirmationView(caption, msg, CustomConfirmationDialogButtonOptions.OK);
            cv.ChangeICon(ConfirmationDialogType.WARNING);
            var result = cv.ShowCustomDialog();
        }

        public static void ShowWarning(string msg, string description, string caption = "Warning!")
        {
            WNConfirmationView cv = new WNConfirmationView(caption, msg, CustomConfirmationDialogButtonOptions.OK, description);
            cv.ChangeICon(ConfirmationDialogType.WARNING);
            var result = cv.ShowCustomDialog();
        }

        public static void ShowFailed(string msg, string caption = "Failed!")
        {
            WNConfirmationView cv = new WNConfirmationView(caption, msg, CustomConfirmationDialogButtonOptions.OK);
            cv.ChangeICon(ConfirmationDialogType.FAILD);
            var result = cv.ShowCustomDialog();
        }

        public static bool ShowQuestion(string msg, string caption)
        {
            WNConfirmationView cv = new WNConfirmationView(caption, msg, CustomConfirmationDialogButtonOptions.YesNo);
            var result = cv.ShowCustomDialog();
            return (result == ConfirmationDialogResult.Yes);
        }

        public static bool ShowQuestion(string msg, string caption, string description)
        {
            WNConfirmationView cv = new WNConfirmationView(caption, msg, CustomConfirmationDialogButtonOptions.YesNo, description);
            var result = cv.ShowCustomDialog();
            return (result == ConfirmationDialogResult.Yes);
        }

        public static bool ShowOKCancel(string msg, string caption, string description = null)
        {
            WNConfirmationView cv = new WNConfirmationView(caption, msg, CustomConfirmationDialogButtonOptions.OKCancel, description);
            var result = cv.ShowCustomDialog();
            return (result == ConfirmationDialogResult.OK);
        }

        public static void ShowInformation(string msg, string caption)
        {
            WNConfirmationView cv = new WNConfirmationView(caption, msg, CustomConfirmationDialogButtonOptions.OK);
            var result = cv.ShowCustomDialog();
        }

        public static void ShowMessageWithTimerFromThread(UIElement uiElement, string msg, int sleepInMilis = 1500, bool smallWindowOpen = false)
        {
            UCMessegeBoxWithTimer ucMessegeBoxWithTimer = null;
            if (uiElement == null) uiElement = UCGuiRingID.Instance.MotherPanel;
            Grid motherPanel = null;
            Application.Current.Dispatcher.Invoke(() =>
            {
                motherPanel = (Grid)uiElement;
                ucMessegeBoxWithTimer = new UCMessegeBoxWithTimer(motherPanel);
                ucMessegeBoxWithTimer.Show();
                ucMessegeBoxWithTimer.Message = msg;
            });
            System.Threading.Thread.Sleep(sleepInMilis);

            Application.Current.Dispatcher.Invoke(() =>
            {
                ucMessegeBoxWithTimer.Hide();
                motherPanel.Children.Remove(ucMessegeBoxWithTimer);
                ucMessegeBoxWithTimer = null;
            });
        }

        public static void ShowMessageWithTimerFromNonThread(UIElement uiElement, string msg, int sleepInMilis = 1500, bool smallWindowOpen = false)
        {
            UCMessegeBoxWithTimer ucMessegeBoxWithTimer = null;
            if (uiElement == null && UCGuiRingID.Instance != null) uiElement = UCGuiRingID.Instance.MotherPanel;
            Grid motherPanel = null;
            new System.Threading.Thread(() =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    motherPanel = (Grid)uiElement;
                    ucMessegeBoxWithTimer = new UCMessegeBoxWithTimer(motherPanel);
                    ucMessegeBoxWithTimer.Show();
                    ucMessegeBoxWithTimer.Message = msg;
                });
                System.Threading.Thread.Sleep(sleepInMilis);

                Application.Current.Dispatcher.Invoke(() =>
                {
                    ucMessegeBoxWithTimer.Hide();
                    motherPanel.Children.Remove(ucMessegeBoxWithTimer);
                    ucMessegeBoxWithTimer = null;
                });
            }).Start();
        }

        public static MessegeBoxWithLoader ShowMessageWithLoader(string msg)
        {
            MessegeBoxWithLoader msBox = null;
            Application.Current.Dispatcher.Invoke(() =>
            {
                msBox = new MessegeBoxWithLoader();
                msBox.Message = msg;
                msBox.ShowMessage();
            }, System.Windows.Threading.DispatcherPriority.Send);
            return msBox;
        }

        public static void ShowTimerMessageBox(string msg, string caption)
        {
            ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, msg);
        }

        public static bool IsViewActivate(UserControl userControl)
        {
            bool value = false;
            Window window = userControl != null && userControl.Parent != null && userControl.Parent is WNChatView ? (Window)userControl.Parent : WNRingIDMain.WinRingIDMain;
            Application.Current.Dispatcher.Invoke(() =>
            {
                value = window.IsActive && window.WindowState != WindowState.Minimized;
            }, DispatcherPriority.Send);
            return value;
        }

        public static void ChangeTaskBarOverlay()
        {
            int count = ChatViewModel.Instance != null ? ChatViewModel.Instance.UnreadCallContactList.Count + ChatViewModel.Instance.UnreadChatContactList.Count : 0;
            RingIDViewModel.Instance.WinDataModel.TaskBarOverLayCounter = count;
        }

        public static void ShowSignoutLoader(bool isExtit, Grid motherPanel, string restartMessage = null)
        {
            UCSignOutLoader signOutLoader = new UCSignOutLoader();
            signOutLoader.ShowThis(isExtit, motherPanel, restartMessage);
            signOutLoader.OnRemovedUserControl += () => { signOutLoader = null; };
        }

        public static void FocusMainWindow()
        {
            try
            {
<<<<<<< HEAD
                if (SettingsConstants.VALUE_RINGID_SEPARATE_CHAT_VIEW)
                    if (UCGuiRingID.Instance != null && WNRingIDMain.WinRingIDMain != null) WNRingIDMain.WinRingIDMain.FocusWindow();
=======
                //if (SettingsConstants.VALUE_RINGID_SEPARATE_CHAT_VIEW)
                if (UCGuiRingID.Instance != null && WNRingIDMain.WinRingIDMain != null) WNRingIDMain.WinRingIDMain.FocusWindow();
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
            }
            finally { }
        }

        public static bool IsViewActivateAndVisible(UserControl userControl)
        {
            bool value = false;

            if (userControl == null || userControl.IsVisible == false || userControl.Parent == null) return value;
            if (UCGuiRingID.Instance != null)
            {
                Window window = userControl.Parent is WNChatView ? (Window)userControl.Parent : WNRingIDMain.WinRingIDMain;
                Application.Current.Dispatcher.Invoke(() =>
                {
                    value = window.IsActive && window.WindowState != WindowState.Minimized;
                }, DispatcherPriority.Send);
            }
            return value;
        }

        public static void ShowMainUIAfterLogin()
        {
            try
            {
                if (WNRingIDMain.WinRingIDMain != null)
                    if (UCGuiRingID.Instance == null)
                    {
                        UCGuiRingID.Instance = new UCGuiRingID(WNRingIDMain.WinRingIDMain._WindowFirstGrid);
                        WNRingIDMain.WinRingIDMain.Navigate(UCGuiRingID.Instance);
                    }
                RingIDViewModel.Instance.WinDataModel.MainResizeMode = System.Windows.ResizeMode.CanResize;
            }
            catch (Exception ex) { log.Error(ex.StackTrace); }
        }

        public static void MultipleSessionWarning()
        {
            if (UCGuiRingID.Instance != null)
                Application.Current.Dispatcher.Invoke(() => { ShowSignoutLoader(false, UCGuiRingID.Instance.motherGridToShowPopup, StartUpConstatns.ARGUMENT_MULTIPLELOGIN); });
        }

        public static void OpenYoutubeViewer(string youtubeVideoID)
        {
            Application.Current.Dispatcher.BeginInvoke(delegate
            {
                var youtubeViewer = new View.UI.SocialMedia.UCYoutubeViewer(youtubeVideoID);
                youtubeViewer.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
                if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                    youtubeViewer.SetParent(UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid);
                else
                    youtubeViewer.SetParent(UCGuiRingID.Instance.MotherPanel);
                youtubeViewer.OnRemovedUserControl += () =>
                {
                    youtubeViewer = null;
                    if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                };
                youtubeViewer.Show();
            });
        }
    }
}

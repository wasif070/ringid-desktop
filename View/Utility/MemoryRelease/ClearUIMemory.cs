﻿using log4net;
using Models.Constants;
using System;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using View.BindingModels;
using View.UI;
using View.UI.Feed;
using View.Utility.Circle;
using View.ViewModel;

namespace View.Utility.MemoryRelease
{
    public class ClearUIMemory
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ClearUIMemory).Name);
        private static ClearUIMemory _Instance;
        public bool IsRunning;

        public static ClearUIMemory Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new ClearUIMemory();
                }
                return _Instance;
            }
        }

        public void StartThread()
        {
            new Thread(new ThreadStart(Run)).Start();
        }

        public void Run()
        {
            try
            {
                IsRunning = true;

                ClearAllFeed();

                ClearMyProfile();

                ClearFriendProfile();

                ClearCircle();

                ClearMediaPageFeed();

                ClearNewsPortalFeed();

                ClearPagesFeed();

                ClearCelebrityFeed();

                ClearSavedFeeds();
            }
            catch (Exception e)
            {
                log.Error("Exception in ClearUIForHomeFeed==>" + e.Message + "\n" + e.StackTrace);
            }
            finally
            {
                IsRunning = false;
            }
        }

        private void ClearPagesFeed()
        {
            if (UCMiddlePanelSwitcher.View_UCPageProfile != null)
            {
                if (!UCMiddlePanelSwitcher.View_UCPageProfile.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCPageProfile = null;
                }
            }
            if (UCMiddlePanelSwitcher.View_UCPagesMainPanel != null)
            {

                if (UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCNewsContainerPanel != null && !UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCNewsContainerPanel.IsVisible)
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCNewsContainerPanel.BreakingFeeds.Clear();
                    }, DispatcherPriority.Send);
                    UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCNewsContainerPanel = null;
                }
                if (UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCSavedContentPanel != null && !UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCSavedContentPanel.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCSavedContentPanel = null;
                }
                if (UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCDiscoverPanel != null && !UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCDiscoverPanel.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCDiscoverPanel = null;
                }
                if (UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCFollowingListPanel != null && !UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCFollowingListPanel.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCPagesMainPanel._UCFollowingListPanel = null;
                }

                if (UCMiddlePanelSwitcher.View_UCPagesSearchPanel != null && !UCMiddlePanelSwitcher.View_UCPagesSearchPanel.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCPagesSearchPanel = null;
                }
                if (!UCMiddlePanelSwitcher.View_UCPagesMainPanel.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCPagesMainPanel = null;
                }
            }
            //
        }

        private void ClearNewsPortalFeed()
        {
            if (UCMiddlePanelSwitcher.View_UCNewsPortalProfile != null)
            {
                if (!UCMiddlePanelSwitcher.View_UCNewsPortalProfile.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCNewsPortalProfile = null;
                }
            }
            if (UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel != null)
            {

                if (UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCNewsContainerPanel != null
                    && !UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCNewsContainerPanel.IsVisible)
                {
                    //Application.Current.Dispatcher.Invoke((Action)delegate
                    //{
                    //    UCMiddlePanelSwitcher.View_UCNewsPortalMainWrapperPanel.View_UCNewsPortalMainPanel._UCNewsContainerPanel.BreakingNewsPortalFeeds.Clear();
                    //}, DispatcherPriority.Send);
                    UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCNewsContainerPanel = null;
                }
                if (UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCSavedContentPanel != null && !UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCSavedContentPanel.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCSavedContentPanel = null;
                }
                if (UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel != null && !UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCDiscoverPanel = null;
                }
                if (UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCFollowingListPanel != null && !UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCFollowingListPanel.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel._UCFollowingListPanel = null;
                }
                if (UCMiddlePanelSwitcher.View_UCNewsPortalSearchPanel != null && !UCMiddlePanelSwitcher.View_UCNewsPortalSearchPanel.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCNewsPortalSearchPanel = null;
                }
                if (!UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCNewsPortalMainPanel = null;
                }
            }
            //
        }

        private void ClearMediaPageFeed()
        {
            if (UCMiddlePanelSwitcher.View_MediaPageProfile != null)
            {
                if (!UCMiddlePanelSwitcher.View_MediaPageProfile.IsVisible)
                    UCMiddlePanelSwitcher.View_MediaPageProfile = null;
            }
            if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null)
            {
                if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudFeedsPanel != null && !UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudFeedsPanel.IsVisible)
                {
                    //Application.Current.Dispatcher.Invoke((Action)delegate
                    //{
                    //    UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudFeedsPanel.BreakingMediaCloudFeeds.Clear();
                    //}, DispatcherPriority.Send);
                    UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudFeedsPanel = null;
                }
                if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCAlbumDownloadPanel != null && !UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCAlbumDownloadPanel.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCAlbumDownloadPanel = null;
                }
                if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudDiscoverPanel != null && !UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudDiscoverPanel.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCMediaCloudDiscoverPanel = null;
                }
                if (UCMiddlePanelSwitcher._UCMediaCloudSearch != null && !UCMiddlePanelSwitcher._UCMediaCloudSearch.IsVisible)
                {
                    UCMiddlePanelSwitcher._UCMediaCloudSearch = null;
                }
                if (UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel != null && !UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel.IsVisible)
                    UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel = null;
            }
        }

        private void ClearSavedFeeds()
        {
            if (UCMiddlePanelSwitcher.View_UCSavedFeedPanel != null)
            {
                if (!UCMiddlePanelSwitcher.View_UCSavedFeedPanel.IsVisible)
                    UCMiddlePanelSwitcher.View_UCSavedFeedPanel = null;
            }
        }

        private void ClearCelebrityFeed()
        {
            if (UCMiddlePanelSwitcher.View_CelebrityProfile != null)
            {
                if (UCMiddlePanelSwitcher.View_CelebrityProfile._UCCelebProfileFeeds != null && !UCMiddlePanelSwitcher.View_CelebrityProfile._UCCelebProfileFeeds.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_CelebrityProfile._UCCelebProfileFeeds = null;
                }
                //TODO celeb photos
                if (!UCMiddlePanelSwitcher.View_CelebrityProfile.IsVisible)
                    UCMiddlePanelSwitcher.View_CelebrityProfile = null;
            }
            if (UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel != null)
            {
                if (UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesFeedPanel != null && !UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesFeedPanel.IsVisible)
                {
                    //TODO popular celeb UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesFeedPanel.p
                    UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesFeedPanel = null;
                }
                if (UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesSavedFeed != null && !UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesSavedFeed.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesSavedFeed = null;
                }
                if (UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesFollowingPanel != null && !UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesFollowingPanel.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesFollowingPanel = null;
                }
                if (UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesDiscoverPanel != null && !UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesDiscoverPanel.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel._UCCelebritiesDiscoverPanel = null;
                }
                if (UCMiddlePanelSwitcher._UCCelebritiesSearchPanel != null && !UCMiddlePanelSwitcher._UCCelebritiesSearchPanel.IsVisible)
                {
                    UCMiddlePanelSwitcher._UCCelebritiesSearchPanel = null;
                }
                if (!UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel.IsVisible)
                    UCMiddlePanelSwitcher.View_UCCelebritiesMainPanel = null;
            }
        }

        private void ClearAllFeed()
        {
            //if (UCMiddlePanelSwitcher.View_UCAllFeeds != null)
            //{
            //    if (!UCMiddlePanelSwitcher.View_UCAllFeeds.IsVisible)
            //        RingIDViewModel.Instance.NewsFeeds.RemoveAllModels();
            //    //else if (RingIDViewModel.Instance.NewsFeeds.Count > 50)
            //    //TODO    UCMiddlePanelSwitcher.View_UCAllFeeds.RemoveTopOrBottomModels();
            //}

            if (NewsFeedViewModel.Instance.AllFeedsViewCollection.Count > DefaultSettings.FEED_MAX_LIMIT)
            {
                if (!UCAllFeeds.Instance.IsInUpperHalf)
                    NewsFeedViewModel.Instance.ClearTopModels();
                //foreach (var item in NewsFeedViewModel.Instance.AllFeedsViewCollection)
                //{
                //    if (item.Feed != null && item.Feed.FeedImageList != null)
                //    {
                //        foreach (var x in item.Feed.FeedImageList)
                //        {
                //            x.IsOpenedInFeed = false;
                //        }
                //    }
                //}
                //NewsFeedViewModel.Instance.RemoveAllTopFeeds();
                //Application.Current.Dispatcher.Invoke((Action)delegate
                //{
                //    UCAllFeeds.Instance.scroll.ScrollToVerticalOffset(UCAllFeeds.Instance.scroll.ScrollableHeight - 30);
                //}, DispatcherPriority.Send);
            }
        }

        private void ClearMyProfile()
        {
            if (UCMiddlePanelSwitcher.View_UCMyProfile != null)
            {
                if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyNewsFeeds != null)
                {
                    if (!UCMiddlePanelSwitcher.View_UCMyProfile._UCMyNewsFeeds.IsVisible)
                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyNewsFeeds = null;
                    //RingIDViewModel.Instance.MyNewsFeeds.RemoveAllModels();
                    //else
                    //    UCMiddlePanelSwitcher.View_UCMyProfile.RemoveTopOrBottomModels();
                }
                if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos != null && !UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos = null;
                }
                if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange != null && !UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange = null;
                }
                if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileCoverPhotosSelection != null && !UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileCoverPhotosSelection.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileCoverPhotosSelection = null;
                }
                if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotos == null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfilePhotosChange == null && UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileCoverPhotosSelection == null)
                {
                    lock (RingIDViewModel.Instance.ProfileImageList)
                    {
                        while (RingIDViewModel.Instance.ProfileImageList.Count > 1)
                        {
                            ImageModel model = RingIDViewModel.Instance.ProfileImageList.FirstOrDefault();
                            if (!model.IsLoadMore)
                            {
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    RingIDViewModel.Instance.ProfileImageList.Remove(model);
                                    GC.SuppressFinalize(model);
                                }, DispatcherPriority.Send);
                            }
                        }
                    }

                    lock (RingIDViewModel.Instance.CoverImageList)
                    {
                        while (RingIDViewModel.Instance.CoverImageList.Count > 1)
                        {
                            ImageModel model = RingIDViewModel.Instance.CoverImageList.FirstOrDefault();
                            if (!model.IsLoadMore)
                            {
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    RingIDViewModel.Instance.CoverImageList.Remove(model);
                                    GC.SuppressFinalize(model);
                                }, DispatcherPriority.Send);
                            }
                        }
                    }

                    lock (RingIDViewModel.Instance.FeedImageList)
                    {
                        while (RingIDViewModel.Instance.FeedImageList.Count > 1)
                        {
                            ImageModel model = RingIDViewModel.Instance.FeedImageList.FirstOrDefault();
                            if (!model.IsLoadMore)
                            {
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    RingIDViewModel.Instance.FeedImageList.Remove(model);
                                    GC.SuppressFinalize(model);
                                }, DispatcherPriority.Send);
                            }
                        }
                    }
                }

                if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists != null && !UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.IsVisible)
                {

                    lock (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.FriendListInUserProfile)
                    {
                        while (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.FriendListInUserProfile.Count > 1)
                        {
                            UserBasicInfoModel model = UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.FriendListInUserProfile.FirstOrDefault();
                            if (model.ShortInfoModel.UserTableID != 0)
                            {
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.FriendListInUserProfile.Remove(model);
                                    GC.SuppressFinalize(model);
                                }, DispatcherPriority.Send);
                            }
                        }
                    }

                    lock (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.SearchListInUserProfile)
                    {
                        while (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.SearchListInUserProfile.Count > 0)
                        {
                            UserBasicInfoModel model = UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.SearchListInUserProfile.FirstOrDefault();
                            Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists.SearchListInUserProfile.Remove(model);
                                GC.SuppressFinalize(model);
                            }, DispatcherPriority.Send);
                        }
                    }

                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyContactLists = null;
                }

                if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout != null && !UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.IsVisible)
                {
                    if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyProfessionalCareer != null)
                    {
                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyProfessionalCareer = null;

                        lock (RingIDViewModel.Instance.MyWorkModelsList)
                        {
                            while (RingIDViewModel.Instance.MyWorkModelsList.Count > 0)
                            {
                                SingleWorkModel model = RingIDViewModel.Instance.MyWorkModelsList.FirstOrDefault();
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    RingIDViewModel.Instance.MyWorkModelsList.Remove(model);
                                    GC.SuppressFinalize(model);
                                }, DispatcherPriority.Send);
                            }
                        }
                    }

                    if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyEducation != null)
                    {
                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMyEducation = null;

                        lock (RingIDViewModel.Instance.MyEducationModelsList)
                        {
                            while (RingIDViewModel.Instance.MyEducationModelsList.Count > 0)
                            {
                                SingleEducationModel model = RingIDViewModel.Instance.MyEducationModelsList.FirstOrDefault();
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    RingIDViewModel.Instance.MyEducationModelsList.Remove(model);
                                    GC.SuppressFinalize(model);
                                }, DispatcherPriority.Send);
                            }
                        }
                    }

                    if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMySkill != null)
                    {
                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout.ucMySkill = null;

                        lock (RingIDViewModel.Instance.MySkillModelsList)
                        {
                            while (RingIDViewModel.Instance.MySkillModelsList.Count > 0)
                            {
                                SingleSkillModel model = RingIDViewModel.Instance.MySkillModelsList.FirstOrDefault();
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    RingIDViewModel.Instance.MySkillModelsList.Remove(model);
                                    GC.SuppressFinalize(model);
                                }, DispatcherPriority.Send);
                            }
                        }
                    }
                    UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileAbout = null;
                }

                if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum != null)
                {
                    if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyProfileMusicAlbum != null && !UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyProfileMusicAlbum.IsVisible)
                    {
                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyProfileMusicAlbum = null;
                    }

                    if (UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyprofileVideoAlbum != null && !UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyprofileVideoAlbum.IsVisible)
                    {
                        UCMiddlePanelSwitcher.View_UCMyProfile._UCMyProfileMediaAlbum._UCMyprofileVideoAlbum = null;
                    }
                }

                if (!UCMiddlePanelSwitcher.View_UCMyProfile.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCMyProfile = null;
                }
            }
        }

        private void ClearFriendProfile()
        {
            if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel != null)
            {
                if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendNewsFeeds != null)
                {
                    if (!UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendNewsFeeds.IsVisible)
                        UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendNewsFeeds = null;
                    //    UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendNewsFeeds.FriendNewsFeeds.RemoveAllModels();
                    //else
                    //    UCMiddlePanelSwitcher.View_UCFriendProfilePanel.RemoveTopOrBottomModels();
                }

                if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendPhotos != null && !UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendPhotos.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendPhotos = null;

                    lock (UCMiddlePanelSwitcher.View_UCFriendProfilePanel.FriendProfileImageList)
                    {
                        while (UCMiddlePanelSwitcher.View_UCFriendProfilePanel.FriendProfileImageList.Count > 1)
                        {
                            ImageModel model = UCMiddlePanelSwitcher.View_UCFriendProfilePanel.FriendProfileImageList.FirstOrDefault();
                            if (!model.IsLoadMore)
                            {
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    UCMiddlePanelSwitcher.View_UCFriendProfilePanel.FriendProfileImageList.Remove(model);
                                    GC.SuppressFinalize(model);
                                }, DispatcherPriority.Send);
                            }
                        }
                    }

                    lock (UCMiddlePanelSwitcher.View_UCFriendProfilePanel.FriendCoverImageList)
                    {
                        while (UCMiddlePanelSwitcher.View_UCFriendProfilePanel.FriendCoverImageList.Count > 1)
                        {
                            ImageModel model = UCMiddlePanelSwitcher.View_UCFriendProfilePanel.FriendCoverImageList.FirstOrDefault();
                            if (!model.IsLoadMore)
                            {
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    UCMiddlePanelSwitcher.View_UCFriendProfilePanel.FriendCoverImageList.Remove(model);
                                    GC.SuppressFinalize(model);
                                }, DispatcherPriority.Send);
                            }
                        }
                    }

                    lock (UCMiddlePanelSwitcher.View_UCFriendProfilePanel.FriendFeedImageList)
                    {
                        while (UCMiddlePanelSwitcher.View_UCFriendProfilePanel.FriendFeedImageList.Count > 1)
                        {
                            ImageModel model = UCMiddlePanelSwitcher.View_UCFriendProfilePanel.FriendFeedImageList.FirstOrDefault();
                            if (!model.IsLoadMore)
                            {
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    UCMiddlePanelSwitcher.View_UCFriendProfilePanel.FriendFeedImageList.Remove(model);
                                    GC.SuppressFinalize(model);
                                }, DispatcherPriority.Send);
                            }
                        }
                    }
                }

                if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendContactList != null && !UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendContactList.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendContactList = null;
                    lock (UCMiddlePanelSwitcher.View_UCFriendProfilePanel.FriendList)
                    {
                        while (UCMiddlePanelSwitcher.View_UCFriendProfilePanel.FriendList.Count > 1)
                        {
                            UserBasicInfoModel model = UCMiddlePanelSwitcher.View_UCFriendProfilePanel.FriendList.FirstOrDefault();
                            if (model.ShortInfoModel.UserTableID != 0)
                            {
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    UCMiddlePanelSwitcher.View_UCFriendProfilePanel.FriendList.Remove(model);
                                    GC.SuppressFinalize(model);
                                }, DispatcherPriority.Send);
                            }
                        }
                    }

                    lock (UCMiddlePanelSwitcher.View_UCFriendProfilePanel.MutualList)
                    {
                        while (UCMiddlePanelSwitcher.View_UCFriendProfilePanel.MutualList.Count > 1)
                        {
                            UserBasicInfoModel model = UCMiddlePanelSwitcher.View_UCFriendProfilePanel.MutualList.FirstOrDefault();
                            if (model.ShortInfoModel.UserTableID != 0)
                            {
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    UCMiddlePanelSwitcher.View_UCFriendProfilePanel.MutualList.Remove(model);
                                    GC.SuppressFinalize(model);
                                }, DispatcherPriority.Send);
                            }
                        }
                    }
                }

                if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout != null && !UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout.IsVisible)
                {
                    if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout.ucFriendProfessionalCareer != null)
                    {
                        lock (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout.ucFriendProfessionalCareer.FriendWorkModelsList)
                        {
                            while (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout.ucFriendProfessionalCareer.FriendWorkModelsList.Count > 0)
                            {
                                SingleWorkModel model = UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout.ucFriendProfessionalCareer.FriendWorkModelsList.FirstOrDefault();
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout.ucFriendProfessionalCareer.FriendWorkModelsList.Remove(model);
                                    GC.SuppressFinalize(model);
                                }, DispatcherPriority.Send);
                            }
                        }
                        UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout.ucFriendProfessionalCareer = null;
                    }

                    if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout.ucFriendEducation != null)
                    {
                        lock (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout.ucFriendEducation.FriendEducationModelsList)
                        {
                            while (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout.ucFriendEducation.FriendEducationModelsList.Count > 0)
                            {
                                SingleEducationModel model = UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout.ucFriendEducation.FriendEducationModelsList.FirstOrDefault();
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout.ucFriendEducation.FriendEducationModelsList.Remove(model);
                                    GC.SuppressFinalize(model);
                                }, DispatcherPriority.Send);
                            }
                        }

                        UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout.ucFriendEducation = null;
                    }

                    if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout.ucFriendSkill != null)
                    {
                        lock (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout.ucFriendSkill.FriendSkillModelsList)
                        {
                            while (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout.ucFriendSkill.FriendSkillModelsList.Count > 0)
                            {
                                SingleSkillModel model = UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout.ucFriendSkill.FriendSkillModelsList.FirstOrDefault();
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout.ucFriendSkill.FriendSkillModelsList.Remove(model);
                                    GC.SuppressFinalize(model);
                                }, DispatcherPriority.Send);
                            }
                        }
                        UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout.ucFriendSkill = null;
                    }

                    UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendAbout = null;
                }

                if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum != null)
                {
                    if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileMusicAlbum != null && !UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileMusicAlbum.IsVisible)
                    {
                        lock (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileMusicAlbum.FriendAudioAlbums)
                        {
                            while (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileMusicAlbum.FriendAudioAlbums.Count > 0)
                            {
                                MediaContentModel model = UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileMusicAlbum.FriendAudioAlbums.FirstOrDefault();
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileMusicAlbum.FriendAudioAlbums.Remove(model);
                                    GC.SuppressFinalize(model);
                                }, DispatcherPriority.Send);
                            }
                        }
                        UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileMusicAlbum = null;
                    }

                    if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileVideoAlbum != null && !UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileVideoAlbum.IsVisible)
                    {
                        lock (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileVideoAlbum.FriendVideoAlbums)
                        {
                            while (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileVideoAlbum.FriendVideoAlbums.Count > 0)
                            {
                                MediaContentModel model = UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileVideoAlbum.FriendVideoAlbums.FirstOrDefault();
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileVideoAlbum.FriendVideoAlbums.Remove(model);
                                    GC.SuppressFinalize(model);
                                }, DispatcherPriority.Send);
                            }
                        }
                        UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum._UCFriendProfileVideoAlbum = null;
                    }

                    if (UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum != null && !UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum.IsVisible)
                    {
                        UCMiddlePanelSwitcher.View_UCFriendProfilePanel._UCFriendProfileMediaAlbum = null;
                    }
                }

                if (!UCMiddlePanelSwitcher.View_UCFriendProfilePanel.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCFriendProfilePanel = null;
                }
            }
        }

        private void ClearCircle()
        {
            if (UCMiddlePanelSwitcher.View_UCCirclePanel != null)
            {
                if (UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds != null)
                {
                    if (!UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds.IsVisible)
                    {
                        UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleNewsFeeds = null;
                    }
                }

                if (UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel != null && !UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel.IsVisible)
                {
                    lock (VMCircle.Instance.MembersList)
                    {
                        while (VMCircle.Instance.MembersList.Count > 0)
                        {
                            UserBasicInfoModel model = VMCircle.Instance.MembersList.FirstOrDefault();
                            Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                VMCircle.Instance.MembersList.Remove(model);
                                GC.SuppressFinalize(model);
                            }, DispatcherPriority.Send);

                        }
                    }
                    UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel = null;
                }

                if (!UCMiddlePanelSwitcher.View_UCCirclePanel.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCCirclePanel = null;
                }
            }

            if (UCMiddlePanelSwitcher.View_UCCircleInitPanel != null)
            {
                if (UCMiddlePanelSwitcher.View_UCCircleInitPanel._UCCircleList != null && !UCMiddlePanelSwitcher.View_UCCircleInitPanel._UCCircleList.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCCircleInitPanel._UCCircleList = null;
                }
                if (UCMiddlePanelSwitcher.View_UCCircleInitPanel._UCCircleNewsFeeds != null && !UCMiddlePanelSwitcher.View_UCCircleInitPanel._UCCircleNewsFeeds.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCCircleInitPanel._UCCircleNewsFeeds = null;
                }
                if (UCMiddlePanelSwitcher.View_UCCircleInitPanel._UCSavedFeed != null && !UCMiddlePanelSwitcher.View_UCCircleInitPanel._UCSavedFeed.IsVisible)
                {
                    UCMiddlePanelSwitcher.View_UCCircleInitPanel._UCSavedFeed = null;
                }
                if (!UCMiddlePanelSwitcher.View_UCCircleInitPanel.IsVisible)
                    UCMiddlePanelSwitcher.View_UCCircleInitPanel = null;
            }
        }

    }
}

﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using log4net;
using Models.Utility;
using View.Constants;

namespace View.Utility
{
    public class DeleteOldImages
    {
        private readonly ILog log = LogManager.GetLogger(typeof(DeleteOldImages).Name);
        private DirectoryInfo[] folders = new DirectoryInfo[10];
        private FileInfo[] listOfFiles;
        private bool running = false;

        private long SEVEN_DAYS_AGO_IN_MS;
        private const int MAX_IMAGE = 100;
        private int EXTRA_IMAGE = 0;

        public void StartThread()
        {
            if (!running)
            {
                Thread th = new Thread(new ThreadStart(Run));
                th.Start();
                th.Name = this.GetType().Name;
            }
        }

        public void Run()
        {
            running = true;
            try
            {
                SEVEN_DAYS_AGO_IN_MS = ModelUtility.CurrentTimeMillisLocal(DateTime.Today.AddDays(-7));
                if (Directory.Exists(RingIDSettings.TEMP_BOOK_IMAGE_FOLDER)) this.folders[0] = new DirectoryInfo(RingIDSettings.TEMP_BOOK_IMAGE_FOLDER);
                if (Directory.Exists(RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER)) this.folders[1] = new DirectoryInfo(RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER);
                if (Directory.Exists(RingIDSettings.TEMP_COVER_IMAGE_FOLDER)) this.folders[2] = new DirectoryInfo(RingIDSettings.TEMP_COVER_IMAGE_FOLDER);
                if (Directory.Exists(RingIDSettings.TEMP_MEDIA_IMAGES_FOLDER)) this.folders[3] = new DirectoryInfo(RingIDSettings.TEMP_MEDIA_IMAGES_FOLDER);
                if (Directory.Exists(RingIDSettings.TEMP_MAP_FOLDER)) this.folders[4] = new DirectoryInfo(RingIDSettings.TEMP_MAP_FOLDER);
                if (Directory.Exists(RingIDSettings.TEMP_NEWSPORTAL_IMAGES_FOLDER)) this.folders[5] = new DirectoryInfo(RingIDSettings.TEMP_NEWSPORTAL_IMAGES_FOLDER);
                if (Directory.Exists(RingIDSettings.TEMP_PAGES_IMAGES_FOLDER)) this.folders[6] = new DirectoryInfo(RingIDSettings.TEMP_PAGES_IMAGES_FOLDER);
                if (Directory.Exists(RingIDSettings.TEMP_MEDIAPAGE_IMAGES_FOLDER)) this.folders[7] = new DirectoryInfo(RingIDSettings.TEMP_MEDIAPAGE_IMAGES_FOLDER);
                if (Directory.Exists(RingIDSettings.TEMP_CELEBRITY_IMAGES_FOLDER)) this.folders[8] = new DirectoryInfo(RingIDSettings.TEMP_CELEBRITY_IMAGES_FOLDER);
                foreach (DirectoryInfo singleFolder in folders)
                {
                    if (singleFolder != null)
                    {
                        listOfFiles = singleFolder.GetFiles().OrderBy(P => P.CreationTime).ToArray();
                        if (listOfFiles != null && listOfFiles.Count() > 0)
                        {
                            if (listOfFiles.Count() > MAX_IMAGE)
                            {
                                EXTRA_IMAGE = listOfFiles.Count() - MAX_IMAGE;
                                FindOldExtraImagesAndDelete();
                            }
                            else Find7DaysOldExtraImagesAndDelete();
                            listOfFiles = null;
                        }
                    }
                }
            }
            catch (Exception ex) { log.Error("Run() ex => " + "\n" + ex.StackTrace); }
            finally { running = false; }
        }

        private void FindOldExtraImagesAndDelete()
        {
            try
            {
                for (int i = 0; i < EXTRA_IMAGE; i++) listOfFiles[i].Delete();
            }
            catch (Exception ex) { log.Error("FindOldExtraImagesAndDelete ex => " + ex.StackTrace); }
        }

        private void Find7DaysOldExtraImagesAndDelete()
        {
            try
            {
                foreach (FileInfo file in listOfFiles)
                {
                    if (ModelUtility.CurrentTimeMillisLocal(file.CreationTime) < SEVEN_DAYS_AGO_IN_MS) file.Delete();
                }
            }
            catch (Exception ex) { log.Error("Find7DaysOldExtraImagesAndDelete ex => " + ex.StackTrace); }
        }
    }
}

﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using View.Constants;
using View.UI;
using View.UI.Circle;
using View.UI.Feed;
using View.UI.PopUp;
using View.UI.PopUp.MediaSendViaChat;
using View.UI.Profile.MyProfile;
using View.UI.Wallet;
using View.Utility.WPFMessageBox;
using View.ViewModel.NewStatus;

namespace View.Utility
{
    public class PopupController
    {
        #region "Private Fields"

        UCPhotoSelectionView photoSelectionView;
        UCProfilePictureUpload profilePictureUpload;
        UCAlbumImageUpload albumImageUpload;
        public UCCircleViewWrapper circleViewWrapper;

        UCShareSingleFeedView shareSingleFeedView;
        UCAddLocationView _ucAddLocationView;
        CustomPrivacyPopupView _customPrivacyPopupView;
        UCMediaCloudUploadPopup _MediaCloudUploadPopup;

        private UCAddMemberToGroupPopupWrapper _Instance;
        private UCHashTagPopUp hashTagPopUp;
        UCFeedDownloadPauseCancelPopup feedDownloadPauseCancelPopup;
        private UCEmoticonPopupWrapper emoticonPopupWrapper;
        private UCEditHistoryPopupStyle editHistoryToolTipPopup;
        #endregion "Private Fields"

        #region "Public Fields"
        public UCStickerPopUPInComments stickerPopUPInComments;
        public UCWalletPopupWrapper walletPopupWrapper = null;
        public UCEditStatusCommentView ucEditStatusCommentView;
        public UCUploadingPopup _ucUploadingPopup;
        public UCEditedHistoryView _ucEditedHistoryView;
        public UCNewStatusPrivacyPopup ucNewStatusPrivacyPopup = null;

        #endregion "Public Fields"

        #region "Constructors"
        #endregion "Constructors"

        #region "Properties"

        public UCPhotoSelectionView PhotoSelectionView
        {
            get
            {
                if (photoSelectionView == null) photoSelectionView = new UCPhotoSelectionView(UCGuiRingID.Instance.MotherPanel);
                return photoSelectionView;
            }
        }

        public UCAlbumImageUpload AlbumImageUpload
        {
            get
            {
                if (albumImageUpload == null)
                {
                    albumImageUpload = new UCAlbumImageUpload();
                    albumImageUpload.SetParent(UCGuiRingID.Instance.MotherPanel);
                    UCGuiRingID.Instance.AddUserControlInMainGrid(albumImageUpload);
                }
                return albumImageUpload;
            }
        }


        public UCCircleViewWrapper GetCircleViewWrapper
        {
            get
            {
                if (circleViewWrapper == null) circleViewWrapper = new UCCircleViewWrapper(UCGuiRingID.Instance.MotherPanel);
                return circleViewWrapper;
            }
        }

        public UCShareSingleFeedView ShareSingleFeedView
        {
            get
            {
                if (shareSingleFeedView == null) shareSingleFeedView = new UCShareSingleFeedView(UCGuiRingID.Instance.MotherPanel);
                return shareSingleFeedView;
            }
        }

        public UCAddLocationView ucAddLocationView
        {
            get
            {
                if (_ucAddLocationView == null) _ucAddLocationView = new UCAddLocationView(UCGuiRingID.Instance.MotherPanel);
                return _ucAddLocationView;
            }
        }
        public UCEditStatusCommentView EditStatusCommentView
        {
            get
            {
                if (ucEditStatusCommentView == null)
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        ucEditStatusCommentView = new UCEditStatusCommentView(UCGuiRingID.Instance.MotherPanel);
                    }, DispatcherPriority.Send);
                }
                return ucEditStatusCommentView;
            }
        }
        public UCUploadingPopup ucUploadingPopup
        {
            get
            {
                if (_ucUploadingPopup == null)
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        _ucUploadingPopup = new UCUploadingPopup(UCGuiRingID.Instance.MotherPanel);
                    }, DispatcherPriority.Send);
                }
                return _ucUploadingPopup;
            }
        }

        public UCEditedHistoryView ucEditedHistoryView
        {
            get
            {
                if (_ucEditedHistoryView == null)
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        _ucEditedHistoryView = new UCEditedHistoryView(UCGuiRingID.Instance.MotherPanel);
                    }, DispatcherPriority.Send);
                }
                return _ucEditedHistoryView;
            }
        }

        public CustomPrivacyPopupView customPrivacyPopupView
        {
            get
            {
                if (_customPrivacyPopupView == null) _customPrivacyPopupView = new CustomPrivacyPopupView(UCGuiRingID.Instance.MotherPanel);
                return _customPrivacyPopupView;
            }
        }

        public UCMediaCloudUploadPopup ucMediaCloudUploadPopup
        {
            get
            {
                if (_MediaCloudUploadPopup == null)
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        _MediaCloudUploadPopup = new UCMediaCloudUploadPopup(UCGuiRingID.Instance.MotherPanel);
                    }, DispatcherPriority.Send);
                }
                return _MediaCloudUploadPopup;
            }
        }

        public UCHashTagPopUp HashTagPopUp
        {
            get
            {
                if (hashTagPopUp == null)
                {
                    hashTagPopUp = new UCHashTagPopUp();
                    UCGuiRingID.Instance.AddUserControlInMainGrid(hashTagPopUp);
                }
                return hashTagPopUp;
            }
        }

        public UCAddMemberToGroupPopupWrapper Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new UCAddMemberToGroupPopupWrapper();
                    UCGuiRingID.Instance.AddUserControlInMainGrid(_Instance);
                }
                return _Instance;
            }
        }

        public UCFeedDownloadPauseCancelPopup FeedDownloadPauseCancelPopup
        {
            get
            {
                if (feedDownloadPauseCancelPopup == null)
                {
                    feedDownloadPauseCancelPopup = new UCFeedDownloadPauseCancelPopup();
                    if (UCGuiRingID.Instance != null) UCGuiRingID.Instance.AddUserControlInMainGrid(feedDownloadPauseCancelPopup);
                }
                return feedDownloadPauseCancelPopup;
            }
        }

        public UCEmoticonPopupWrapper EmoticonPopupWrapper
        {
            get
            {
                if (emoticonPopupWrapper == null)
                {
                    emoticonPopupWrapper = new UCEmoticonPopupWrapper();
                    UCGuiRingID.Instance.AddUserControlInMainGrid(emoticonPopupWrapper);
                }
                return emoticonPopupWrapper;
            }
        }


        public UCEditHistoryPopupStyle EditHistoryToolTipPopup
        {
            get
            {
                if (editHistoryToolTipPopup == null) editHistoryToolTipPopup = new UCEditHistoryPopupStyle();
                return editHistoryToolTipPopup;
            }
        }

        public UCWalletPopupWrapper WalletPopupWrapper
        {
            get
            {
                if (walletPopupWrapper == null)
                {
                    walletPopupWrapper = new UCWalletPopupWrapper();
                    UCGuiRingID.Instance.AddUserControlInMainGrid(walletPopupWrapper);
                }
                return walletPopupWrapper;
            }

        }

        public UCStickerPopUPInComments StickerPopUPInComments
        {
            get
            {
                if (stickerPopUPInComments == null)
                {
                    stickerPopUPInComments = new UCStickerPopUPInComments();
                    UCGuiRingID.Instance.AddUserControlInMainGrid(stickerPopUPInComments, 1);
                }
                return stickerPopUPInComments;
            }
        }

        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        #endregion "Private methods"

    }
}

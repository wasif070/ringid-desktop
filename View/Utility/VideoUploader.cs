<<<<<<< HEAD
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using log4net;
using MediaToolkit;
using MediaToolkit.Model;
using MediaToolkit.Options;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Constants;
using View.UI;
using View.UI.Feed;
using View.UI.PopUp;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.ViewModel.NewStatus;
using View.UI.PopUp.Stream;

namespace View.Utility
{
    class VideoUploader
    {
        private readonly ILog log = LogManager.GetLogger(typeof(VideoUploader).Name);
        private NewStatusViewModel newStatusViewModel;
        //private CustomWebClient _WebClient = null;
        private bool _SingleVideoUploadFailed = false;
        private List<FeedVideoUploaderModel> _VideoUploaderList;
        private JObject _MediaJobject = null;
        private string _lineEnd = "\r\n";
        private string _twoHyphens = "--";
        private string _boundary = "*****";
        private byte[] _boundarybytes, _trailerbytes;
        private long TotalBytesLength = 0;
        private long _BytesRead = 0;
        private UploadingModel _uploadingModel = new UploadingModel();
        private long TotalFileSize = 0;
        private long TotalUploaded = 0;
        private Func<bool, int> _OnComplete;
        private bool _IsUploadToChannel = false;
        public VideoUploader(NewStatusViewModel newStatusViewModel)
        {
            this.newStatusViewModel = newStatusViewModel;
            this._SingleVideoUploadFailed = false;
            this._IsUploadToChannel = false;
            this._VideoUploaderList = new List<FeedVideoUploaderModel>();
            this.newStatusViewModel.EnablePostbutton(false);
            this._VideoUploaderList.AddRange(newStatusViewModel.NewStatusVideoUpload);

            RingIDViewModel.Instance.UploadingModelList.InvokeAdd(_uploadingModel);
            _uploadingModel.UploadingContent = _VideoUploaderList.Count > 1 ? _VideoUploaderList.Count + " videos are uploading..." : _VideoUploaderList.Count + " video is uploading...";
            for (int i = 0; i < _VideoUploaderList.Count; i++)
            {
                TotalFileSize += _VideoUploaderList[i].FileSize;
            }

            new Thread(new ThreadStart(BeginToVideoUpload)).Start();
        }

        public VideoUploader(UCUploadMediaToChannelPopUp ucUploadMediaToChannelPopUp, Func<bool, int> onComplete = null)
        {
            this._SingleVideoUploadFailed = false;
            this._OnComplete = onComplete;
            this._IsUploadToChannel = true;
            this._VideoUploaderList = new List<FeedVideoUploaderModel>();
            this._VideoUploaderList.AddRange(ucUploadMediaToChannelPopUp.ChannelVideoUploadList);
            RingIDViewModel.Instance.UploadingModelList.InvokeAdd(_uploadingModel);
            _uploadingModel.UploadingContent = _VideoUploaderList.Count > 1 ? _VideoUploaderList.Count + " videos are uploading..." : _VideoUploaderList.Count + " video is uploading...";
            for (int i = 0; i < _VideoUploaderList.Count; i++)
            {
                TotalFileSize += _VideoUploaderList[i].FileSize;
            }

            new Thread(new ThreadStart(BeginToVideoUpload)).Start();
        }

        #region "Properties"
        public UCUploadingPopup ucUploadingPopup
        {
            get
            {
                return MainSwitcher.PopupController.ucUploadingPopup;
            }
        }
        #endregion"Properties"
        private void BeginToVideoUpload()
        {
            try
            {
                if (_VideoUploaderList.Count > 0)
                {
                    if (!_SingleVideoUploadFailed)
                    {
                        FeedVideoUploaderModel model = _VideoUploaderList.FirstOrDefault();
                        if (string.IsNullOrEmpty(model.StreamUrl))
                        {
                            if (model.IsRecorded) ConvertToMP4AndUploadToServer(model);
                            else
                            {
                                Task responseStream = UploadFilesToRemoteUrl(model);
                            }
                        }
                        else
                        {
                            _VideoUploaderList.Remove(model);
                            model.IsUploadedInVideoServer = true;
                            BeginToVideoUpload();
                        }
                    }
                    else
                    {
                        ResetDataWhenUploadFailed();
                    }
                }
                else
                {
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    Process prs = Process.GetCurrentProcess();
                    try
                    {
                        prs.MaxWorkingSet = (IntPtr)(DefaultSettings.MaxFileLimit500MBinBytes); //500mb
                    }
                    catch (Exception exception)
                    {
                        log.Error(exception.StackTrace + "==>" + exception.Message);
                    }


                    if (!_SingleVideoUploadFailed)
                    {
                        _MediaJobject = new JObject();
                        if (newStatusViewModel != null)
                        {
                            HideUploadingPopup();
                            LoadVideoUploaderModeltoMediaContentDTO();
                            LoadAlbumIdNameinMediaContentDTO();

                            new ThreadAddFeed().StartThread(newStatusViewModel, SettingsConstants.MEDIA_TYPE_VIDEO, _MediaJobject);
                        }
                        else if (_OnComplete != null)
                        {
                            RingIDViewModel.Instance.UploadingModelList.InvokeRemove(_uploadingModel);
                            this._OnComplete(true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ResetDataWhenUploadFailed();
                log.Error("BeginToVideoUpload => " + ex.Message + ex.StackTrace);
            }
        }

        private void HideUploadingPopup()
        {
            RingIDViewModel.Instance.UploadingModelList.InvokeRemove(_uploadingModel);
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (ucUploadingPopup != null && RingIDViewModel.Instance.UploadingModelList.Count == 0)
                {
                    ucUploadingPopup.popupUploading.IsOpen = false;
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        #region Conversion Utility
        private void ConvertToMP4AndUploadToServer(FeedVideoUploaderModel model)
        {
            string sourceFile = model.FilePath;
            //string destFile = Path.ChangeExtension(model.FilePath, "mp4");
            string destFile = Path.GetDirectoryName(model.FilePath) + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(model.FilePath) + "_.mp4";
            long duration = model.VideoDuration;
            model.IsUploading = true;

            ConversionOptions convertOptions = new ConversionOptions
            {
                VideoEncoder = VideoEncoder.MPEG4,
                VideoBitRate = 1200,
                AudioEncoder = AudioEncoder.AAC,
                AudioSampleRate = AudioSampleRate.Hz44100
            };
            Metadata md1 = MediaService.ConvertMedia(RingIDSettings.TEMP_MEDIA_TOOLKIT, sourceFile, destFile, convertOptions);
            if (md1 != null && md1.VideoData != null)
            {
                //if (System.IO.File.Exists(sourceFile)) System.IO.File.Delete(sourceFile);
                model.FilePath = destFile;
                Task responseStream = UploadFilesToRemoteUrl(model);
            }
            else
            {
                //if (System.IO.File.Exists(sourceFile)) System.IO.File.Delete(sourceFile);
                model.IsUploading = false;
                _SingleVideoUploadFailed = true;
                BeginToVideoUpload();
            }
        }

        #endregion
        private void LoadVideoUploaderModeltoMediaContentDTO()
        {
            JArray hashtagsToSend = null;
            if (newStatusViewModel.NewStatusHashTag.Count > 0)
            {
                hashtagsToSend = new JArray();
                foreach (HashTagModel hashTagModel in newStatusViewModel.NewStatusHashTag)
                {
                    JObject obj = new JObject();
                    if (hashTagModel.HashTagSearchKey.Length == 0) continue;
                    hashtagsToSend.Add(hashTagModel.HashTagSearchKey);
                }
                _MediaJobject[JsonKeys.HashTagList] = hashtagsToSend;
            }
            ///////
            _MediaJobject[JsonKeys.MediaType] = 2;
            JArray mediasToSend = new JArray();
            bool IsAlbumUrlPicked = false;
            foreach (var item in newStatusViewModel.NewStatusVideoUpload)
            {
                JObject obj = new JObject();
                obj[JsonKeys.StreamUrl] = item.StreamUrl;
                if (!string.IsNullOrEmpty(item.ThumbUrl))
                {
                    obj[JsonKeys.ThumbUrl] = item.ThumbUrl;
                    if (item.VideoThumbnail != null)
                    {
                        obj[JsonKeys.ThumbImageWidth] = (int)item.VideoThumbnail.Width;
                        obj[JsonKeys.ThumbImageHeight] = (int)item.VideoThumbnail.Height;
                    }
                    else
                    {
                        obj[JsonKeys.ThumbImageWidth] = AppConstants.DEFAULT_THUMB_WIDTH;
                        obj[JsonKeys.ThumbImageHeight] = AppConstants.DEFAULT_THUMB_HEIGHT;
                    }
                    if (!IsAlbumUrlPicked) { _MediaJobject[JsonKeys.MediaAlbumImageURL] = item.ThumbUrl; IsAlbumUrlPicked = true; }
                }
                obj[JsonKeys.Title] = item.VideoTitle;
                obj[JsonKeys.MediaPrivacy] = newStatusViewModel.PrivacyValue;
                if (!string.IsNullOrEmpty(item.VideoArtist)) obj[JsonKeys.Artist] = item.VideoArtist;
                obj[JsonKeys.MediaDuration] = item.VideoDuration;
                if (hashtagsToSend != null) obj[JsonKeys.HashTagList] = hashtagsToSend;
                mediasToSend.Add(obj);
            }
            _MediaJobject[JsonKeys.MediaList] = mediasToSend;
        }

        private void LoadAlbumIdNameinMediaContentDTO()
        {
            string albumName = "";
            albumName = newStatusViewModel.VideoAlbumTitleTextBoxText.Trim();

            if (!string.IsNullOrEmpty(albumName))
            {
                MediaContentModel mediaContentModel = RingIDViewModel.Instance.MyVideoAlbums.Where(x => x.AlbumName == albumName).FirstOrDefault();
                _MediaJobject[JsonKeys.AlbumId] = mediaContentModel != null ? mediaContentModel.AlbumId : Guid.Empty;
                _MediaJobject[JsonKeys.AlbumName] = albumName;
            }
        }

        public async Task UploadFilesToRemoteUrl(FeedVideoUploaderModel model)
        {
            byte[] _serverResponse = default(byte[]);
            if (!String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                HttpWebRequest httpWebRequest = null;
                try
                {
                    _boundarybytes = Encoding.UTF8.GetBytes(_twoHyphens + _boundary + _lineEnd);
                    _trailerbytes = Encoding.UTF8.GetBytes(_twoHyphens + _boundary + _twoHyphens + _lineEnd);

                    Dictionary<string, object> _headerContents = new Dictionary<string, object>();
                    AddDefaultDataWithImage(_headerContents);
                    string url = _IsUploadToChannel ? ServerAndPortSettings.GetChannelVideoUploadingURL : ServerAndPortSettings.GetVideoUploadingURL;
                    httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    string ip = ServerAndPortSettings.AUTH_SERVER_IP;
                    int port = ServerAndPortSettings.COMMUNICATION_PORT;
                    long lid = DefaultSettings.LOGIN_RING_ID;
                    string sid = DefaultSettings.LOGIN_SESSIONID;

                    httpWebRequest.KeepAlive = false;
                    httpWebRequest.ProtocolVersion = HttpVersion.Version10; // fix 1
                    httpWebRequest.Timeout = 200 * 60000; // fix 3
                    httpWebRequest.ReadWriteTimeout = 200 * 60000; // fix 4

                    httpWebRequest.AllowAutoRedirect = true;
                    httpWebRequest.MaximumAutomaticRedirections = 10;

                    httpWebRequest.AllowWriteStreamBuffering = false;

                    string fileName = Path.GetFileName(model.FilePath);
                    httpWebRequest.ContentLength = SetRequestHeaderLength(fileName, model.FilePath, _headerContents);
                    httpWebRequest.ContentType = "multipart/form-data; boundary=" + _boundary;
                    httpWebRequest.UserAgent = "ringID" + "_" + DefaultSettings.VALUE_APP_INSTALLED_VERSION;
                    httpWebRequest.Method = WebRequestMethods.Http.Post;
                    httpWebRequest.Headers["access-control-allow-origin"] = "*";
                    httpWebRequest.Proxy = null;

                    using (var stream = await Task.Factory.FromAsync<System.IO.Stream>(httpWebRequest.BeginGetRequestStream, httpWebRequest.EndGetRequestStream, null))
                    {
                        model.IsUploading = true;
                        /*New Length including header*/
                        TotalFileSize -= model.FileSize;
                        model.FileSize = TotalBytesLength;
                        TotalFileSize += model.FileSize;

                        await SetRequestHeader(model, stream, _headerContents);
                        using (var webResponse = await Task<WebResponse>.Factory.FromAsync(httpWebRequest.BeginGetResponse, httpWebRequest.EndGetResponse, httpWebRequest))
                        {
                            HttpStatusCode statusCode = ((HttpWebResponse)webResponse).StatusCode;
                            if (statusCode == HttpStatusCode.OK)
                            {
                                using (var responseStream = webResponse.GetResponseStream())
                                {
                                    using (var memoryStream = new MemoryStream())
                                    {
                                        responseStream.CopyTo(memoryStream);
                                        _serverResponse = memoryStream.ToArray();
                                        GetVideoUploadResponse(model, _serverResponse);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    _SingleVideoUploadFailed = true;
                    model.IsUploading = false;
                    ResetDataWhenUploadFailed(model);
                    log.Error("Exception in UploadToImageServerWebRequest()==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }

                finally
                {
                    if (httpWebRequest != null)
                    {
                        httpWebRequest.Abort();
                        httpWebRequest = null;
                    }
                }
            }
            else
            {
                log.Error("UploadToImageServerWebRequest() Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            }
        }

        void FinishWebRequest(IAsyncResult result)
        {
            HttpWebResponse response = (result.AsyncState as HttpWebRequest).EndGetResponse(result) as HttpWebResponse;
        }
        private void AddDefaultDataWithImage(Dictionary<string, object> _headerContents)
        {
            _headerContents.Add("sId", DefaultSettings.LOGIN_SESSIONID);
            _headerContents.Add("uId", DefaultSettings.LOGIN_RING_ID);
            _headerContents.Add("authServer", ServerAndPortSettings.AUTH_SERVER_IP);
            _headerContents.Add("comPort", ServerAndPortSettings.COMMUNICATION_PORT);
            _headerContents.Add("x-app-version", AppConfig.VERSION_PC);
        }

        private long SetRequestHeaderLength(string fileName, string filePath, Dictionary<string, object> headerContents)
        {
            try
            {
                /// the form-data file upload, properly formatted
                string fileheaderTemplate = "Content-Disposition: form-data; name=\"{0}\"" + _lineEnd + _lineEnd + "{1}" + _lineEnd;

                //WriteToStream Method for Dictionary Add
                foreach (var headerContent in headerContents)
                {
                    TotalBytesLength += _boundarybytes.Length;
                    TotalBytesLength += Encoding.UTF8.GetBytes(string.Format(fileheaderTemplate, headerContent.Key, headerContent.Value)).Length;
                }

                TotalBytesLength += _boundarybytes.Length;
                TotalBytesLength += Encoding.UTF8.GetBytes("Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + fileName + "\"" + _lineEnd).Length;
                TotalBytesLength += Encoding.UTF8.GetBytes(_lineEnd).Length;
                TotalBytesLength += File.OpenRead(filePath).Length;
                TotalBytesLength += Encoding.UTF8.GetBytes(_lineEnd).Length;
                TotalBytesLength += _trailerbytes.Length;
            }
            catch (Exception ex)
            {
                log.Error("Exception in SetRequestHeaderLength() ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
            return TotalBytesLength;
        }

        private async Task<string> SetRequestHeader(FeedVideoUploaderModel model, System.IO.Stream stream, Dictionary<string, object> headerContents)
        {
            try
            {
                /// the form-data file upload, properly formatted
                string fileheaderTemplate = "Content-Disposition: form-data; name=\"{0}\"" + _lineEnd + _lineEnd + "{1}" + _lineEnd;

                //WriteToStream Method for Dictionary Add
                foreach (var headerContent in headerContents)
                {
                    WriteToStream(stream, _boundarybytes);
                    string a = string.Format(fileheaderTemplate, headerContent.Key, headerContent.Value);
                    WriteToStream(stream, a);
                }
                WriteToStream(stream, _boundarybytes);
                WriteToStream(stream, "Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + Path.GetFileName(model.FilePath) + "\"" + _lineEnd);
                WriteToStream(stream, _lineEnd);

                System.IO.Stream source = File.OpenRead(model.FilePath);
                int fourtyKiloByte = 40960; // 1 KB
                byte[] buffer = new byte[fourtyKiloByte];
                int bytesRead;

                await Task.Factory.StartNew(() =>
                    {
                        while ((bytesRead = source.Read(buffer, 0, buffer.Length)) != 0)
                        {
                            stream.Write(buffer, 0, bytesRead);
                            stream.Flush();
                            _BytesRead += bytesRead;
                            ShowUploadStatus(_BytesRead, model);
                            buffer = null;
                            buffer = new byte[fourtyKiloByte];
                        }

                    });

                WriteToStream(stream, _lineEnd);
                WriteToStream(stream, _trailerbytes);
                model.Pecentage = 100;
                _uploadingModel.TotalUploadedInPercentage = (int)(((double)(model.FileSize + TotalUploaded) / (double)TotalFileSize) * 100);
                stream.Close();
                source.Close();
            }
            catch (Exception ex)
            {
                log.Error("Exception in SetRequestHeader() ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
            return null;
        }

        private void ShowUploadStatus(long TotalbytesRead, FeedVideoUploaderModel model)
        {
            try
            {
                model.Pecentage = (int)((double)((_BytesRead * 100) / TotalBytesLength));
                _uploadingModel.TotalUploadedInPercentage = (int)(((double)(_BytesRead + TotalUploaded) / (double)TotalFileSize) * 100);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
                _SingleVideoUploadFailed = true;
                ResetDataWhenUploadFailed(model);
            }
        }

        private string GetVideoUploadResponse(FeedVideoUploaderModel model, byte[] response)
        {
            string imageUrl = String.Empty;
            try
            {
                if (response != null && response.Length > 0 && (int)response[0] == 1)
                {
                    TotalUploaded += model.FileSize;
                    int urlLength = (int)response[1];
                    model.StreamUrl = System.Text.Encoding.UTF8.GetString(response, 2, urlLength);
                    TotalBytesLength = 0;
                    _BytesRead = 0;
                    GC.SuppressFinalize(model);
                    _VideoUploaderList.Remove(model);
                    if (model.IsRecorded)
                    {
                        if (System.IO.File.Exists(model.FilePath)) System.IO.File.Delete(model.FilePath);
                    }
                    if (!string.IsNullOrEmpty(model.StreamUrl))
                    {
                        model.ThumbUrl = model.StreamUrl.Replace(".mp4", ".jpg");
                    }
                    model.IsUploading = false;
                    model.IsUploadedInVideoServer = true;
                    BeginToVideoUpload();
                }
                else
                {
                    int urlLength = (int)response[1];
                    string msg = System.Text.Encoding.UTF8.GetString(response, 2, urlLength);
                    model.IsUploading = false;
                    _SingleVideoUploadFailed = true;
                    ResetDataWhenUploadFailed(model, msg);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
                _SingleVideoUploadFailed = true;
                ResetDataWhenUploadFailed(model);
            }
            return imageUrl;
        }

        private void ResetDataWhenUploadFailed(FeedVideoUploaderModel model = null, string msg = "")
        {
            if (newStatusViewModel != null)
            {
                newStatusViewModel.EnablePostbutton(true);
                log.Error(msg);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                });
                if (model != null) { model.Pecentage = 0; model.IsUploading = false; }
                UIHelperMethods.ShowErrorMessageBoxFromThread(NotificationMessages.VIDEO_UPLOAD_FAILED, "Failed!");
                if (newStatusViewModel.ucMediaCloudUploadPopup != null) newStatusViewModel.ucMediaCloudUploadPopup.LoadStatesInUI(1);
                HideUploadingPopup();

            }
            else if (_OnComplete != null)
            {
                UIHelperMethods.ShowErrorMessageBoxFromThread(NotificationMessages.VIDEO_UPLOAD_FAILED, "Failed!");
                this._OnComplete(false);
            }
        }

        private void WriteToStream(System.IO.Stream s, string txt)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(txt);
            s.Write(bytes, 0, bytes.Length);
            _BytesRead += bytes.Length;
        }

        private void WriteToStream(System.IO.Stream s, byte[] bytes)
        {
            s.Write(bytes, 0, bytes.Length);
            _BytesRead += bytes.Length;
        }

        public static T[] Combine<T>(params T[][] arrays)
        {
            T[] ret = new T[arrays.Sum(x => x.Length)];
            int offset = 0;
            foreach (T[] data in arrays)
            {
                Buffer.BlockCopy(data, 0, ret, offset, data.Length);
                offset += data.Length;
            }
            return ret;
        }
    }
=======
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using log4net;
using MediaToolkit;
using MediaToolkit.Model;
using MediaToolkit.Options;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Constants;
using View.UI;
using View.UI.Feed;
using View.UI.PopUp;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.ViewModel.NewStatus;
using View.UI.PopUp.Stream;

namespace View.Utility
{
    class VideoUploader
    {
        #region Private Fields

        private readonly ILog log = LogManager.GetLogger(typeof(VideoUploader).Name);
        private NewStatusViewModel newStatusViewModel;
        private bool singleVideoUploadFailed = false;
        private List<FeedVideoUploaderModel> videoUploaderList;
        private JObject mediaJobject = null;
        private string lineEnd = "\r\n";
        private string twoHyphens = "--";
        private string boundary = "*****";
        private byte[] boundarybytes, trailerbytes;
        private long totalBytesLength = 0;
        private long bytesRead = 0;
        private UploadingModel uploadingModel = new UploadingModel();
        private long totalFileSize = 0;
        private long totalUploaded = 0;
        private Func<bool, int> onComplete;
        private bool isUploadToChannel = false;

        #endregion

        #region Constructor

        public VideoUploader(NewStatusViewModel newStatusViewModel)
        {
            this.newStatusViewModel = newStatusViewModel;
            this.singleVideoUploadFailed = false;
            this.isUploadToChannel = false;
            this.videoUploaderList = new List<FeedVideoUploaderModel>();
            this.newStatusViewModel.EnablePostbutton(false);
            this.videoUploaderList.AddRange(newStatusViewModel.NewStatusVideoUpload);
            RingIDViewModel.Instance.UploadingModelList.InvokeAdd(uploadingModel);
            uploadingModel.UploadingContent = videoUploaderList.Count > 1 ? videoUploaderList.Count + " videos are uploading..." : videoUploaderList.Count + " video is uploading...";
            for (int i = 0; i < videoUploaderList.Count; i++)
            {
                totalFileSize += videoUploaderList[i].FileSize;
            }
            new Thread(new ThreadStart(BeginToVideoUpload)).Start();
        }

        public VideoUploader(UCUploadMediaToChannelPopUp ucUploadMediaToChannelPopUp, Func<bool, int> onComplete = null)
        {
            this.singleVideoUploadFailed = false;
            this.onComplete = onComplete;
            this.isUploadToChannel = true;
            this.videoUploaderList = new List<FeedVideoUploaderModel>();
            this.videoUploaderList.AddRange(ucUploadMediaToChannelPopUp.ChannelVideoUploadList);
            RingIDViewModel.Instance.UploadingModelList.InvokeAdd(uploadingModel);
            uploadingModel.UploadingContent = videoUploaderList.Count > 1 ? videoUploaderList.Count + " videos are uploading..." : videoUploaderList.Count + " video is uploading...";
            for (int i = 0; i < videoUploaderList.Count; i++)
            {
                totalFileSize += videoUploaderList[i].FileSize;
            }
            new Thread(new ThreadStart(BeginToVideoUpload)).Start();
        }

        #endregion

        #region Properties

        public UCUploadingPopup ucUploadingPopup
        {
            get
            {
                return MainSwitcher.PopupController.ucUploadingPopup;
            }
        }

        #endregion

        #region Private Methods

        private void BeginToVideoUpload()
        {
            try
            {
                if (videoUploaderList.Count > 0)
                {
                    if (!singleVideoUploadFailed)
                    {
                        FeedVideoUploaderModel model = videoUploaderList.FirstOrDefault();
                        if (string.IsNullOrEmpty(model.StreamUrl))
                        {
                            if (model.IsRecorded)
                            {
                                ConvertToMP4AndUploadToServer(model);
                            }
                            else
                            {
                                Task responseStream = UploadFilesToRemoteUrl(model);
                            }
                        }
                        else
                        {
                            videoUploaderList.Remove(model);
                            model.IsUploadedInVideoServer = true;
                            BeginToVideoUpload();
                        }
                    }
                    else
                    {
                        ResetDataWhenUploadFailed();
                    }
                }
                else
                {
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    Process prs = Process.GetCurrentProcess();
                    try
                    {
                        prs.MaxWorkingSet = (IntPtr)(DefaultSettings.MaxFileLimit500MBinBytes); //500mb
                    }
                    catch (Exception exception)
                    {
                        log.Error(exception.StackTrace + "==>" + exception.Message);
                    }
                    if (!singleVideoUploadFailed)
                    {
                        mediaJobject = new JObject();
                        if (newStatusViewModel != null)
                        {
                            HideUploadingPopup();
                            LoadVideoUploaderModeltoMediaContentDTO();
                            LoadAlbumIdNameinMediaContentDTO();
                            new ThreadAddFeed().StartThread(newStatusViewModel, SettingsConstants.MEDIA_TYPE_VIDEO, mediaJobject);
                        }
                        else if (onComplete != null)
                        {
                            RingIDViewModel.Instance.UploadingModelList.InvokeRemove(uploadingModel);
                            this.onComplete(true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ResetDataWhenUploadFailed();
                log.Error("BeginToVideoUpload => " + ex.Message + ex.StackTrace);
            }
        }

        private void HideUploadingPopup()
        {
            RingIDViewModel.Instance.UploadingModelList.InvokeRemove(uploadingModel);
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (ucUploadingPopup != null && RingIDViewModel.Instance.UploadingModelList.Count == 0)
                {
                    ucUploadingPopup.popupUploading.IsOpen = false;
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        private void LoadVideoUploaderModeltoMediaContentDTO()
        {
            JArray hashtagsToSend = null;
            if (newStatusViewModel.NewStatusHashTag.Count > 0)
            {
                hashtagsToSend = new JArray();
                foreach (HashTagModel hashTagModel in newStatusViewModel.NewStatusHashTag)
                {
                    JObject obj = new JObject();
                    if (hashTagModel.HashTagSearchKey.Length == 0)
                    {
                        continue;
                    }
                    hashtagsToSend.Add(hashTagModel.HashTagSearchKey);
                }
                mediaJobject[JsonKeys.HashTagList] = hashtagsToSend;
            }
            mediaJobject[JsonKeys.MediaType] = 2;
            JArray mediasToSend = new JArray();
            bool IsAlbumUrlPicked = false;
            foreach (var item in newStatusViewModel.NewStatusVideoUpload)
            {
                JObject obj = new JObject();
                obj[JsonKeys.StreamUrl] = item.StreamUrl;
                if (!string.IsNullOrEmpty(item.ThumbUrl))
                {
                    obj[JsonKeys.ThumbUrl] = item.ThumbUrl;
                    if (item.VideoThumbnail != null)
                    {
                        obj[JsonKeys.ThumbImageWidth] = (int)item.VideoThumbnail.Width;
                        obj[JsonKeys.ThumbImageHeight] = (int)item.VideoThumbnail.Height;
                    }
                    else
                    {
                        obj[JsonKeys.ThumbImageWidth] = AppConstants.DEFAULT_THUMB_WIDTH;
                        obj[JsonKeys.ThumbImageHeight] = AppConstants.DEFAULT_THUMB_HEIGHT;
                    }
                    if (!IsAlbumUrlPicked)
                    { 
                        mediaJobject[JsonKeys.MediaAlbumImageURL] = item.ThumbUrl;
                        IsAlbumUrlPicked = true;
                    }
                }
                obj[JsonKeys.Title] = item.VideoTitle;
                obj[JsonKeys.MediaPrivacy] = newStatusViewModel.PrivacyValue;
                if (!string.IsNullOrEmpty(item.VideoArtist))
                {
                    obj[JsonKeys.Artist] = item.VideoArtist;
                }
                obj[JsonKeys.MediaDuration] = item.VideoDuration;
                if (hashtagsToSend != null)
                {
                    obj[JsonKeys.HashTagList] = hashtagsToSend;
                }
                mediasToSend.Add(obj);
            }
            mediaJobject[JsonKeys.MediaList] = mediasToSend;
        }

        private void LoadAlbumIdNameinMediaContentDTO()
        {
            string albumName = "";
            albumName = newStatusViewModel.VideoAlbumTitleTextBoxText.Trim();
            if (!string.IsNullOrEmpty(albumName))
            {
                MediaContentModel mediaContentModel = RingIDViewModel.Instance.MyVideoAlbums.Where(x => x.AlbumName == albumName).FirstOrDefault();
                mediaJobject[JsonKeys.AlbumId] = mediaContentModel != null ? mediaContentModel.AlbumId : Guid.Empty;
                mediaJobject[JsonKeys.AlbumName] = albumName;
            }
        }

        private void AddDefaultDataWithImage(Dictionary<string, object> _headerContents)
        {
            _headerContents.Add("sId", DefaultSettings.LOGIN_SESSIONID);
            _headerContents.Add("uId", DefaultSettings.LOGIN_RING_ID);
            _headerContents.Add("authServer", ServerAndPortSettings.AUTH_SERVER_IP);
            _headerContents.Add("comPort", ServerAndPortSettings.COMMUNICATION_PORT);
            _headerContents.Add("x-app-version", AppConfig.VERSION_PC);
        }

        private long SetRequestHeaderLength(string fileName, string filePath, Dictionary<string, object> headerContents)
        {
            try
            {
                /// the form-data file upload, properly formatted
                string fileheaderTemplate = "Content-Disposition: form-data; name=\"{0}\"" + lineEnd + lineEnd + "{1}" + lineEnd;
                //WriteToStream Method for Dictionary Add
                foreach (var headerContent in headerContents)
                {
                    totalBytesLength += boundarybytes.Length;
                    totalBytesLength += Encoding.UTF8.GetBytes(string.Format(fileheaderTemplate, headerContent.Key, headerContent.Value)).Length;
                }
                totalBytesLength += boundarybytes.Length;
                totalBytesLength += Encoding.UTF8.GetBytes("Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + fileName + "\"" + lineEnd).Length;
                totalBytesLength += Encoding.UTF8.GetBytes(lineEnd).Length;
                totalBytesLength += File.OpenRead(filePath).Length;
                totalBytesLength += Encoding.UTF8.GetBytes(lineEnd).Length;
                totalBytesLength += trailerbytes.Length;
            }
            catch (Exception ex)
            {
                log.Error("Exception in SetRequestHeaderLength() ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
            return totalBytesLength;
        }

        private async Task<string> SetRequestHeader(FeedVideoUploaderModel model, System.IO.Stream stream, Dictionary<string, object> headerContents)
        {
            try
            {
                /// the form-data file upload, properly formatted
                string fileheaderTemplate = "Content-Disposition: form-data; name=\"{0}\"" + lineEnd + lineEnd + "{1}" + lineEnd;

                //WriteToStream Method for Dictionary Add
                foreach (var headerContent in headerContents)
                {
                    WriteToStream(stream, boundarybytes);
                    string a = string.Format(fileheaderTemplate, headerContent.Key, headerContent.Value);
                    WriteToStream(stream, a);
                }
                WriteToStream(stream, boundarybytes);
                WriteToStream(stream, "Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + Path.GetFileName(model.FilePath) + "\"" + lineEnd);
                WriteToStream(stream, lineEnd);

                System.IO.Stream source = File.OpenRead(model.FilePath);
                int fourtyKiloByte = 40960; // 1 KB
                byte[] buffer = new byte[fourtyKiloByte];
                int bytesRead;

                await Task.Factory.StartNew(() =>
                {
                    while ((bytesRead = source.Read(buffer, 0, buffer.Length)) != 0)
                    {
                        stream.Write(buffer, 0, bytesRead);
                        stream.Flush();
                        this.bytesRead += bytesRead;
                        ShowUploadStatus(this.bytesRead, model);
                        buffer = null;
                        buffer = new byte[fourtyKiloByte];
                    }

                });

                WriteToStream(stream, lineEnd);
                WriteToStream(stream, trailerbytes);
                model.Pecentage = 100;
                uploadingModel.TotalUploadedInPercentage = (int)(((double)(model.FileSize + totalUploaded) / (double)totalFileSize) * 100);
                stream.Close();
                source.Close();
            }
            catch (Exception ex)
            {
                log.Error("Exception in SetRequestHeader() ==>" + ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
            }
            return null;
        }

        private void ShowUploadStatus(long TotalbytesRead, FeedVideoUploaderModel model)
        {
            try
            {
                model.Pecentage = (int)((double)((bytesRead * 100) / totalBytesLength));
                uploadingModel.TotalUploadedInPercentage = (int)(((double)(bytesRead + totalUploaded) / (double)totalFileSize) * 100);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
                singleVideoUploadFailed = true;
                ResetDataWhenUploadFailed(model);
            }
        }

        private string GetVideoUploadResponse(FeedVideoUploaderModel model, byte[] response)
        {
            string imageUrl = String.Empty;
            try
            {
                if (response != null && response.Length > 0 && (int)response[0] == 1)
                {
                    totalUploaded += model.FileSize;
                    int urlLength = (int)response[1];
                    model.StreamUrl = System.Text.Encoding.UTF8.GetString(response, 2, urlLength);
                    totalBytesLength = 0;
                    bytesRead = 0;
                    GC.SuppressFinalize(model);
                    videoUploaderList.Remove(model);
                    if (model.IsRecorded)
                    {
                        if (System.IO.File.Exists(model.FilePath))
                        {
                            System.IO.File.Delete(model.FilePath);
                        }
                    }
                    if (!string.IsNullOrEmpty(model.StreamUrl))
                    {
                        model.ThumbUrl = model.StreamUrl.Replace(".mp4", ".jpg");
                    }
                    model.IsUploading = false;
                    model.IsUploadedInVideoServer = true;
                    BeginToVideoUpload();
                }
                else
                {
                    int urlLength = (int)response[1];
                    string msg = System.Text.Encoding.UTF8.GetString(response, 2, urlLength);
                    model.IsUploading = false;
                    singleVideoUploadFailed = true;
                    ResetDataWhenUploadFailed(model, msg);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
                singleVideoUploadFailed = true;
                ResetDataWhenUploadFailed(model);
            }
            return imageUrl;
        }

        private void ResetDataWhenUploadFailed(FeedVideoUploaderModel model = null, string message = "")
        {
            if (newStatusViewModel != null)
            {
                newStatusViewModel.EnablePostbutton(true);
                log.Error(message);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                });
                if (model != null)
                { 
                    model.Pecentage = 0; 
                    model.IsUploading = false; 
                }
                UIHelperMethods.ShowErrorMessageBoxFromThread(NotificationMessages.VIDEO_UPLOAD_FAILED, "Failed!");
                if (newStatusViewModel.ucMediaCloudUploadPopup != null)
                {
                    newStatusViewModel.ucMediaCloudUploadPopup.LoadStatesInUI(1);
                }
                HideUploadingPopup();
            }
            else if (onComplete != null)
            {
                UIHelperMethods.ShowErrorMessageBoxFromThread(NotificationMessages.VIDEO_UPLOAD_FAILED, "Failed!");
                this.onComplete(false);
            }
        }

        private void WriteToStream(System.IO.Stream s, string txt)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(txt);
            s.Write(bytes, 0, bytes.Length);
            bytesRead += bytes.Length;
        }

        private void WriteToStream(System.IO.Stream s, byte[] bytes)
        {
            s.Write(bytes, 0, bytes.Length);
            bytesRead += bytes.Length;
        }

        #endregion

        #region Public Methods

        public async Task UploadFilesToRemoteUrl(FeedVideoUploaderModel model)
        {
            byte[] _serverResponse = default(byte[]);
            if (!String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) && DefaultSettings.IsInternetAvailable)
            {
                HttpWebRequest httpWebRequest = null;
                try
                {
                    boundarybytes = Encoding.UTF8.GetBytes(twoHyphens + boundary + lineEnd);
                    trailerbytes = Encoding.UTF8.GetBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                    Dictionary<string, object> _headerContents = new Dictionary<string, object>();
                    AddDefaultDataWithImage(_headerContents);
                    string url = isUploadToChannel ? ServerAndPortSettings.GetChannelVideoUploadingURL : ServerAndPortSettings.GetVideoUploadingURL;
                    httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    string ip = ServerAndPortSettings.AUTH_SERVER_IP;
                    int port = ServerAndPortSettings.COMMUNICATION_PORT;
                    long loginRingId = DefaultSettings.LOGIN_RING_ID;
                    string sessionId = DefaultSettings.LOGIN_SESSIONID;

                    httpWebRequest.KeepAlive = false;
                    httpWebRequest.ProtocolVersion = HttpVersion.Version10; // fix 1
                    httpWebRequest.Timeout = 200 * 60000; // fix 3
                    httpWebRequest.ReadWriteTimeout = 200 * 60000; // fix 4

                    httpWebRequest.AllowAutoRedirect = true;
                    httpWebRequest.MaximumAutomaticRedirections = 10;

                    httpWebRequest.AllowWriteStreamBuffering = false;

                    string fileName = Path.GetFileName(model.FilePath);
                    httpWebRequest.ContentLength = SetRequestHeaderLength(fileName, model.FilePath, _headerContents);
                    httpWebRequest.ContentType = "multipart/form-data; boundary=" + boundary;
                    httpWebRequest.UserAgent = "ringID" + "_" + DefaultSettings.VALUE_APP_INSTALLED_VERSION;
                    httpWebRequest.Method = WebRequestMethods.Http.Post;
                    httpWebRequest.Headers["access-control-allow-origin"] = "*";
                    httpWebRequest.Proxy = null;

                    using (var stream = await Task.Factory.FromAsync<System.IO.Stream>(httpWebRequest.BeginGetRequestStream, httpWebRequest.EndGetRequestStream, null))
                    {
                        model.IsUploading = true;
                        totalFileSize -= model.FileSize;
                        model.FileSize = totalBytesLength;
                        totalFileSize += model.FileSize;

                        await SetRequestHeader(model, stream, _headerContents);
                        using (var webResponse = await Task<WebResponse>.Factory.FromAsync(httpWebRequest.BeginGetResponse, httpWebRequest.EndGetResponse, httpWebRequest))
                        {
                            HttpStatusCode statusCode = ((HttpWebResponse)webResponse).StatusCode;
                            if (statusCode == HttpStatusCode.OK)
                            {
                                using (var responseStream = webResponse.GetResponseStream())
                                {
                                    using (var memoryStream = new MemoryStream())
                                    {
                                        responseStream.CopyTo(memoryStream);
                                        _serverResponse = memoryStream.ToArray();
                                        GetVideoUploadResponse(model, _serverResponse);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    singleVideoUploadFailed = true;
                    model.IsUploading = false;
                    ResetDataWhenUploadFailed(model);
                    log.Error("Exception in UploadToImageServerWebRequest()==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                }
                finally
                {
                    if (httpWebRequest != null)
                    {
                        httpWebRequest.Abort();
                        httpWebRequest = null;
                    }
                }
            }
            else
            {
                log.Error("UploadToImageServerWebRequest() Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            }
        }

        public static T[] Combine<T>(params T[][] arrays)
        {
            T[] ret = new T[arrays.Sum(x => x.Length)];
            int offset = 0;
            foreach (T[] data in arrays)
            {
                Buffer.BlockCopy(data, 0, ret, offset, data.Length);
                offset += data.Length;
            }
            return ret;
        }

        #endregion

        #region Conversion Utility

        private void ConvertToMP4AndUploadToServer(FeedVideoUploaderModel model)
        {
            string sourceFile = model.FilePath;
            string destinationFile = Path.GetDirectoryName(model.FilePath) + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(model.FilePath) + "_.mp4";
            long duration = model.VideoDuration;
            model.IsUploading = true;

            ConversionOptions convertOptions = new ConversionOptions
            {
                VideoEncoder = VideoEncoder.MPEG4,
                VideoBitRate = 1200,
                AudioEncoder = AudioEncoder.AAC,
                AudioSampleRate = AudioSampleRate.Hz44100
            };
            Metadata metadata = MediaService.ConvertMedia(RingIDSettings.TEMP_MEDIA_TOOLKIT, sourceFile, destinationFile, convertOptions);
            if (metadata != null && metadata.VideoData != null)
            {
                model.FilePath = destinationFile;
                Task responseStream = UploadFilesToRemoteUrl(model);
            }
            else
            {
                model.IsUploading = false;
                singleVideoUploadFailed = true;
                BeginToVideoUpload();
            }
        }

        #endregion
    }
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
}
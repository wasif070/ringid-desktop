﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace View.Utility
{
    public class SearchCircleMember
    {
        public static string _SearchParam = string.Empty;
        public static long _SearchCircleId = 0;
        public static long _SearchLimit = 0;
        public static bool _IsSearchRunning = false;
        private string _PreviousString = string.Empty;
        private static int _Waiting = 0;
        public static long _UserIdentity = 0;
        public static BindingModels.UserBasicInfoModel _UserBasicInfoModel = null;

        //private int SLEEP_TIME = 5;

        public SearchCircleMember(long circleId)
        {
            SearchCircleMember._SearchCircleId = circleId;
            //SearchCircleMember._SearchParam = searchParam;
        }

        public SearchCircleMember(long circleId, long startLimit)
        {
            SearchCircleMember._SearchCircleId = circleId;
            SearchCircleMember._SearchLimit = startLimit;
            //SearchCircleMember._SearchParam = searchParam;
        }

        public void StartThread()
        {
            if (!SearchCircleMember._IsSearchRunning)
            {
                SearchCircleMember._IsSearchRunning = true;
                Thread th = new Thread(Run);
                th.Name = "SearchCircleMember";
                th.Start();
            }
        }
        private void Run()
        {
            try
            {
                while (SearchCircleMember._IsSearchRunning)
                {
                    if (SearchCircleMember._SearchParam != null)
                    {
                        if (_PreviousString == null || !_PreviousString.Equals(SearchCircleMember._SearchParam))
                        {
                            _Waiting = 0;
                            _PreviousString = SearchCircleMember._SearchParam;
                            //
                            //
                            if (!string.IsNullOrEmpty(SearchCircleMember._SearchParam))
                            {
                                SeandSearchRequest();
                            }
                        }
                        else
                        {
                            _PreviousString = string.Empty;
                            break;
                        }
                    }
                    if (_Waiting != 0 && _Waiting % 15 == 0)
                    {
                        break;
                    }
                    _Waiting++;
                    Thread.Sleep(300);
                }
            }
            catch (Exception )
            {

                //throw;
            }
            finally
            {
                _IsSearchRunning = false;
            }
            //System.Windows.Application.Current.Dispatcher.BeginInvoke(() =>
            //    {
            //        View.UI.UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel.HideShowMore();
            //    }, System.Windows.Threading.DispatcherPriority.Send);
        }

        public static void SeandSearchRequest()
        {
            try
            {
                //Sending data for action 101-->{"pckId":"14565651448482110010002","schPm":"ash","actn":101,"st":0,"sId":"8441401462529442110010002","grpId":133}
                JObject pakToSend = new JObject();
                string pakId =SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = pakId;
                pakToSend[JsonKeys.LogStartLimit] = _SearchLimit;//change later
                pakToSend[JsonKeys.SearchParam] = _SearchParam;
                pakToSend[JsonKeys.GroupId] = _SearchCircleId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_SEARCH_CIRCLE_MEMBER;
               SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, pakToSend.ToString());
                Thread.Sleep(200);
                for (int pakSendingAttempt = 1; pakSendingAttempt < DefaultSettings.TRYING_TIME; pakSendingAttempt++)
                {
                    if (string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) || SearchCircleMember._IsSearchRunning)
                    {
                        return;
                    }
                    Thread.Sleep(DefaultSettings.WAITING_TIME);
                    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(pakId))
                    {
                        if (pakSendingAttempt % 9 == 0)
                           SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, pakToSend.ToString());
                    }
                    else
                    {
                        JObject feedbackfields;
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryGetValue(pakId, out feedbackfields);
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackfields);
                        break;
                    }
                    
                }
            }
            catch (Exception )
            {

                //throw;
            }
        }
        
    }
}

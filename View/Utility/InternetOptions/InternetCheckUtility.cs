﻿using System;
using System.Net.Sockets;
using Models.Constants;

namespace View.Utility.InternetOptions
{
    public class InternetCheckUtility
    {
        private readonly static log4net.ILog log = log4net.LogManager.GetLogger(typeof(InternetCheckUtility).Name);

        public static bool CheckIsNetwork(bool needTostartThead = true)
        {
            bool flag = IsInternetAvailable();
            DefaultSettings.IsInternetAvailable = flag;
            if (flag == false && needTostartThead) MainSwitcher.ThreadManager().StartNetworkThread();
            return flag;
        }

        public static bool IsInternetAvailable()
        {
            return isHostAvailable("m.ringid.com") || isHostAvailable("google.com") || isHostAvailable("amazon.com") || isHostAvailable("facebook.com") || isHostAvailable("apple.com");
        }

        private static bool isHostAvailable(String hostName)
        {
            bool ok;
            TcpClient client = new TcpClient();
            try
            {
                client.Connect(hostName, 80);
                ok = true;
            }
            catch (Exception ex) { ok = false; }
            finally { client.Close(); }
            return ok;
        }
    }
}

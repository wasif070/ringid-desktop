﻿using System;
using System.Threading;
using FileTrasnferSDKCLI;
using Models.Constants;
using Models.Stores;
using View.Utility.Chat.Service;
using View.ViewModel;

namespace View.Utility.InternetOptions
{
    public class ThradNoInternet
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradNoInternet).Name);
        private bool running = false;
        private bool stop = false;
        bool check = false;
        bool isUIChangedWhileNoNet = false;
        #endregion "Private Fields"

        #region "Private methods"

        /// <summary>
        /// this thread will runn infinity time if there is no network connection,
        /// first loop for connect/disconnect again and again 
        ///
        /// </summary>
        private void Run()
        {
            try
            {
                running = true;
                isUIChangedWhileNoNet = false;
                while (!stop)
                {
                    try
                    {
                        if (DefaultSettings.IS_NETWORK_INTERFACE_AVAILABLE)
                        {
                            for (int idx = 0; idx < 10; idx++)
                            {
                                Thread.Sleep(1000);
                                check = InternetCheckUtility.IsInternetAvailable();
                                if (check) break;
                            }
                        }
                        else check = InternetCheckUtility.IsInternetAvailable();
                        ResetWhileInternet.ClearBrokenPacket();
                        if (check)
                        {
                            bool basRacReqeust = InternetCheckUtility.IsInternetAvailable();
                            if (basRacReqeust)
                            {
                                StopThread();
                                HaveInternetActions();
                            }
                        }
                        else
                        {
                            if (RingIDViewModel.Instance != null && RingIDViewModel.Instance.WinDataModel != null) RingIDViewModel.Instance.WinDataModel.IsSignIn = false;
                            DefaultSettings.LOGIN_SESSIONID = null;
                            NoInternetActions();
                            Thread.Sleep(10 * 1000);
                        }
                    }
                    catch (Exception ex) { log.Error(ex.StackTrace); }
                }
            }
            catch (Exception ex) { log.Error(ex.StackTrace); }
            finally
            {
                running = false;
                stop = false;
                isUIChangedWhileNoNet = false;
            }
        }

        private void NoInternetActions()
        {
            if (!isUIChangedWhileNoNet)
            {
                try
                {
                    UIHelperMethods.ChangeTaskBarOverlay();
                    ResetWhileInternet.OnInternetUnavailable();
                }
                catch (Exception ex) { log.Error(ex.StackTrace); }
                finally { isUIChangedWhileNoNet = true; }
            }
        }

        private void HaveInternetActions()
        {
            DefaultSettings.IsInternetAvailable = true;
            RingIDViewModel.Instance.IsInternetAvailable = true;
            ResetWhileInternet.Login();
            ResetWhileInternet.OnInternetAvailable();
            //ChatService.SendNetworkStatus(BaseNetworkStatus.INTERNET_OK);
            //ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
            //ImageDictionaries.Instance.FILE_UPLOAD_PROCESSOR_QUEUE.OnComplete();
            //UIHelperMethods.ChangeTaskBarOverlay();
        }

        #endregion "Private methods"

        #region "Public Methods"

        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
                thrd.Name = this.GetType().Name;
            }
        }

        public void StopThread()
        {
            stop = true;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

﻿using System;
using System.Threading.Tasks;
using Auth.Parser;
using FileTrasnferSDKCLI;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Utility;
using View.Dictonary;
using View.UI;
using View.UI.Profile.FriendProfile;
using View.Utility.Chat.Service;
using View.Utility.SignInSignUP;
using System.Collections.Generic;
using Models.Stores;
using View.ViewModel;

namespace View.Utility.InternetOptions
{
    public class ResetWhileInternet
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ResetWhileInternet).Name);

        public static object NetConnection = new object();

        public static void OnInternetAvailable()
        {
            try
            {
                ChatService.SendNetworkStatus(BaseNetworkStatus.INTERNET_OK);
                if (MainSwitcher.ThreadManager().InstanceOfSuggestionTimer != null)
                {
                    if (MainSwitcher.ThreadManager().InstanceOfSuggestionTimer.IsAlive) MainSwitcher.ThreadManager().InstanceOfSuggestionTimer.Resume();
                    else MainSwitcher.ThreadManager().InstanceOfSuggestionTimer.Start();
                }
                if (DefaultSettings.userProfile != null)
                {
                    DefaultSettings.userProfile.Mood = StatusConstants.MOOD_ALIVE;
                    RingIDViewModel.Instance.ChangeMyStatusIcon(StatusConstants.MOOD_ALIVE);
                    ChatService.ClearAllRegistrations();
                }
                ImageDictionaries.Instance.FILE_DOWNLOAD_PROCESSOR_QUEUE.OnComplete();
                ImageDictionaries.Instance.FILE_UPLOAD_PROCESSOR_QUEUE.OnComplete();
            }
            catch (Exception ex) { log.Error("Error: OnInternetAvailable()." + ex.Message + "\n" + ex.StackTrace); }
        }

        public static void OnInternetUnavailable()
        {
            try
            {
                DefaultSettings.IsInternetAvailable = false;
                RingIDViewModel.Instance.IsInternetAvailable = false;
                if (MainSwitcher.ThreadManager().InstanceOfSuggestionTimer != null && MainSwitcher.ThreadManager().InstanceOfSuggestionTimer.IsAlive) MainSwitcher.ThreadManager().InstanceOfSuggestionTimer.Pause();
                if (DefaultSettings.userProfile != null)
                {
                    DefaultSettings.userProfile.Mood = StatusConstants.MOOD_OFFLINE;
                    RingIDViewModel.Instance.ChangeMyStatusIcon(StatusConstants.MOOD_OFFLINE);
                    ChatService.SendNetworkStatus(BaseNetworkStatus.INTERNET_NOT_AVAILABLE);
                }
            }
            catch (Exception ex) { log.Error("Error: OnInternetUnavailable()." + ex.Message + "\n" + ex.StackTrace); }
        }

        public static void ClearBrokenPacket()
        {
            try
            {
                if (BreakingPacketRepository.Instance.breaking_packets != null && BreakingPacketRepository.Instance.breaking_packets.Count > 0)
                {
                    foreach (long key in BreakingPacketRepository.Instance.breaking_packets.Keys)
                    {
                        BreakingPacketData packet = null;
                        BreakingPacketRepository.Instance.breaking_packets.TryGetValue(key, out packet);
                        if (packet != null) if (packet.Time < ModelUtility.CurrentTimeMillis() - AppConstants.FIVE_MINUTES) BreakingPacketRepository.Instance.RemoveBreakingPacketList(key);
                    }
                }
            }
            catch (Exception ex) { log.Error("Error: ClearBrokenPacket()." + ex.Message + "\n" + ex.StackTrace); }
        }

        public static void Login()
        {
            try
            {
                if (string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID)
                    && !string.IsNullOrEmpty(DefaultSettings.VALUE_LOGIN_USER_NAME)
                    && !string.IsNullOrEmpty(DefaultSettings.VALUE_LOGIN_USER_PASSWORD))
                {
                    Task.Factory.StartNew(() =>
                    {
                        SignedInInfoDTO signedInInfoDTO = new SigninRequest().SignInRequestBackground();
                        if (signedInInfoDTO.isSuccess)
                        {
                            RingIDViewModel.Instance.ReloadMyProfile();
                            System.Windows.Application.Current.Dispatcher.Invoke(() =>
                            {
                                OnInternetAvailable();
                            });
                            log.Info("Internet Connection Available. => SendDefaultRequestToServer() ==> Resend All");
                            HelperMethods.SendDefaultRequestToServer();
                            foreach (UCFriendChatCallPanel ucFriendChatCall in UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.Values)
                                ucFriendChatCall.ChatScrollViewer._IsChatStatusLoaded = false;
                        }
                        else
                        {
                            if (signedInInfoDTO.isDownloadMandatory) HelperMethods.DownloadMandatoryUpdater(signedInInfoDTO.versionMsg);
                            else if (signedInInfoDTO.reasonCode == ReasonCodeConstants.REASON_CODE_PASSWORD_DID_NOT_MATCHED)
                            {
                                UIHelperMethods.ShowErrorMessageBoxFromThread(ReasonCodeConstants.GetReason(ReasonCodeConstants.REASON_CODE_PASSWORD_DID_NOT_MATCHED), "Signin Failed!");
                                MainSwitcher.AuthSignalHandler().AppRestart();
                            }
                            else if (signedInInfoDTO.reasonCode == ReasonCodeConstants.REASON_CODE_INVALID_RINGID)
                            {
                                UIHelperMethods.ShowErrorMessageBoxFromThread(ReasonCodeConstants.GetReason(ReasonCodeConstants.REASON_CODE_INVALID_RINGID), "Signin Failed!");
                                MainSwitcher.AuthSignalHandler().AppRestart();
                            }
                        }
                    });
                }
                else if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
                {
                    System.Windows.Application.Current.Dispatcher.Invoke(() => { OnInternetAvailable(); });
                    log.Info("Internet Connection Available. => SendDefaultRequestToServer(true) ==> Resend Pending Chat & Fetch Offline Chat");
                    HelperMethods.SendDefaultRequestToServer(true);
                    if (UCGuiRingID.Instance.IsVisible)
                    {
                        foreach (UCFriendChatCallPanel ucFriendChatCall in UIDictionaries.Instance.FRIEND_CHAT_CALL_DICTIONARY.Values) ucFriendChatCall.ChatScrollViewer._IsChatStatusLoaded = false;
                    }
                }
                else if (string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID)) MainSwitcher.ThreadManager().StartNetworkThread();
            }
            catch (Exception ex) { log.Error("Error: Login()." + ex.Message + "\n" + ex.StackTrace); }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VirtualPanel.Controls
{
    public enum VirtualViewType
    {
        DEFAULT = 0,
        GROUP_CREATE_PANEL__FRIEND_LIST = 1,
        MEDIA_SHARE_RECENT_LIST_PANEL__RECENT_LIST = 2,
        MEDIA_SHARE_FRIEND_LIST_PANEL__FRIEND_LIST = 3,
        MEDIA_SHARE_GROUP_LIST_PANEL__GROUP_LIST = 4,
        CALL_DIVERT_PANEL_LIST = 5,
        STATUS_TAG_PANEL_LIST = 6,
        GROUP_CREATE_PANEL_FROM_CHAT_CALL_PANEL__FRIEND_LIST = 7,
        ROOM_MEMBERS_PANEL__MEMBER_LIST = 8,
        CONTACT_PANEL__CONTACT_SHARE_LIST = 9
    }
}

﻿using System;
using System.Diagnostics;
using System.Text;
using System.Threading;
using Auth.utility;
using Models.Constants;
using View.Utility.MemoryRelease;
using View.ViewModel;

namespace View.Utility
{
    public class KeepAlive
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(KeepAlive).Name);
        private bool running = false;
        private bool stop = false;
        private int checkForHalfHour = 0;
        //private int checkInactiveTime = 0;
        //private int checkTenMinutesTime = 0;
        private int totalHalfMinsWhileInactive = 0;
        private int totalHalfMins = 0;
        private bool isContactRequested = false;//prevent to request contact twice
        #endregion "Private Fields"

        #region "Private methods"
        /// <summary>
        /// 1. Send keep Alive in every 30secs if there is a sessionID 
        /// 2. If no sessionID stop This service
        /// 3. Becasue of running this Service always we added some extra functionality here
        ///     - Send contact list (29) request in every 30 mins
        ///     -if window is inactive need to send contact list request in every 3 mins
        ///     - Send Feed Reqeust in every 10 mins if window is deactivated
        ///</summary>
        private void Run()
        {
            try
            {
                running = true;
                RingIDViewModel.Instance.ReloadMyProfile();
                while (!stop)
                {
                    if (DefaultSettings.LOGIN_TABLE_ID <= 0) break;
                    try
                    {
                        long usageSpace = Process.GetCurrentProcess().PrivateMemorySize64 / DefaultSettings.MEGA_BYTE;
                        if ((DefaultSettings.MEMORY_MAX_LIMIT - usageSpace) < DefaultSettings.MEMORY_RELEASE_LIMIT && !ClearUIMemory.Instance.IsRunning) ClearUIMemory.Instance.StartThread();
                        sendkeepAliveRequest();
                        Thread.Sleep(DefaultSettings.KEEP_ALIVE_TIME);

                        checkForHalfHour += 30;
                        if (checkForHalfHour == 1800) //30*60sec
                        {
                            if (!RingIDViewModel.Instance.WinDataModel.IsLoadingCotactList)
                            {
                                DefaultSettings.FRIEND_LIST_LOADED = false;
                                MainSwitcher.ThreadManager().ContactUtIdsRequest().StarThread();
                                checkForHalfHour = 0;
                                isContactRequested = true;
                            }
                            else
                                checkForHalfHour -= 30;
                        }

                        totalHalfMins++;
                        if (!RingIDViewModel.Instance.WinDataModel.IsActiveWindow)
                        {
                            totalHalfMinsWhileInactive++;
                            if (totalHalfMinsWhileInactive % 6 == 0)//3 mins 
                            {
                                if (!RingIDViewModel.Instance.WinDataModel.IsLoadingCotactList && !isContactRequested)
                                {
                                    DefaultSettings.FRIEND_LIST_LOADED = false;
                                    MainSwitcher.ThreadManager().ContactUtIdsRequest().StarThread();
                                }
                            }
                            if (totalHalfMinsWhileInactive % 20 == 0)//10 mins
                                NewsFeedViewModel.Instance.ActionReloadFeeds();

                            if (totalHalfMinsWhileInactive % 10 == 0)//5 mins
                                View.Utility.Feed.StorageFeeds.SaveLastTwentyFeeds();
                        }
                        else totalHalfMinsWhileInactive = 0;
                        if (totalHalfMins > 0 && totalHalfMins % 60 == 0)
                        {
                            totalHalfMins = 0;
                        }
                        else totalHalfMins++;

                        isContactRequested = false;
                        //Application.Current.Dispatcher.Invoke(() =>
                        //{
                        //    Console.WriteLine("UIHelperMethods.IsActiveMainWindow()==>" + UIHelperMethods.IsActiveMainWindow());
                        //    if (!UIHelperMethods.IsActiveMainWindow()) checkInactiveTime += 30;
                        //    else
                        //    {
                        //        if ((checkInactiveTime + 30) >= 180)//3*60sec
                        //        {
                        //            if (FriendDictionaries.Instance.FRIEND_NEED_TO_FETCH_DICTIONARY.Count == 0 && DefaultSettings.FRIEND_LIST_LOADED == true)
                        //            {
                        //                DefaultSettings.FRIEND_LIST_LOADED = false;
                        //                MainSwitcher.ThreadManager().ContactUtIdsRequest().StarThread();
                        //                checkInactiveTime = 0;
                        //            }
                        //        }
                        //        else checkInactiveTime = 0;
                        //    }

                        //    if (!UIHelperMethods.IsActiveMainWindow()) checkTenMinutesTime += 30;
                        //    else
                        //    {
                        //        if ((checkTenMinutesTime + 30) >= 600)//10*60sec
                        //        {
                        //            NewsFeedViewModel.Instance.ActionReloadFeeds();
                        //            checkTenMinutesTime = 0;
                        //        }
                        //        else checkTenMinutesTime = 0;
                        //    }
                        //}, System.Windows.Threading.DispatcherPriority.Send);
                    }
                    catch (Exception e) { log.Error("sendkeepAliveRequest() ex ==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message); }
                }
            }
            finally { running = stop = false; }
        }

        private void sendkeepAliveRequest()
        {
            if (!string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
            {
                if (DefaultSettings.IsInternetAvailable) SendToServer.SendDatagramPacket(AppConstants.REQUEST_TYPE_KEEP_ALIVE, AppConstants.SINGLE_PACKET, Encoding.UTF8.GetBytes(DefaultSettings.LOGIN_SESSIONID));
            }
            else
            {
                running = false;
                StopThread();
                log.Error("Send_keepAlive() Failed ==> IsSessionNull = " + string.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID) + ", IsInternetAvailable = " + DefaultSettings.IsInternetAvailable);
            }
        }
        #endregion "Private methods"

        #region "Public Methods"

        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
                thrd.Name = this.GetType().Name;
            }
        }

        public void StopThread()
        {
            stop = true;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"

    }
}

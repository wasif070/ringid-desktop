﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace View.Utility
{
    public static class TabButtonDependency
    {
        public static readonly DependencyProperty BtnTextProperty =
         DependencyProperty.RegisterAttached("BtnText", typeof(String), typeof(TabButtonDependency));

        public static String GetBtnText(DependencyObject obj)
        {
            return (String)obj.GetValue(BtnTextProperty);
        }

        public static void SetBtnText(DependencyObject obj, String value)
        {
            obj.SetValue(BtnTextProperty, value);
        }

        public static readonly DependencyProperty BtnBottomNormalBackgroundProperty =
          DependencyProperty.RegisterAttached("BtnBottomNormalBackground", typeof(Brush), typeof(TabButtonDependency));

        public static Brush GetBtnBottomNormalBackground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(BtnBottomNormalBackgroundProperty);
        }

        public static void SetBtnBottomNormalBackground(DependencyObject obj, Brush value)
        {
            obj.SetValue(BtnBottomNormalBackgroundProperty, value);
        }

        public static readonly DependencyProperty BtnBottomHoverBackgroundProperty =
          DependencyProperty.RegisterAttached("BtnBottomHoverBackground", typeof(Brush), typeof(TabButtonDependency));

        public static Brush GetBtnBottomHoverBackground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(BtnBottomHoverBackgroundProperty);
        }

        public static void SetBtnBottomHoverBackground(DependencyObject obj, Brush value)
        {
            obj.SetValue(BtnBottomHoverBackgroundProperty, value);
        }

        public static readonly DependencyProperty BtnBottomPressedBackgroundProperty =
          DependencyProperty.RegisterAttached("BtnBottomPressedBackground", typeof(Brush), typeof(TabButtonDependency));

        public static Brush GetBtnBottomPressedBackground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(BtnBottomPressedBackgroundProperty);
        }

        public static void SetBtnBottomPressedBackground(DependencyObject obj, Brush value)
        {
            obj.SetValue(BtnBottomPressedBackgroundProperty, value);
        }

        public static readonly DependencyProperty BtnRightBackgroundProperty =
          DependencyProperty.RegisterAttached("BtnRightBackground", typeof(Brush), typeof(TabButtonDependency));

        public static Brush GetBtnRightBackground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(BtnRightBackgroundProperty);
        }

        public static void SetBtnRightBackground(DependencyObject obj, Brush value)
        {
            obj.SetValue(BtnRightBackgroundProperty, value);
        }

        public static readonly DependencyProperty BtnTextNormalForegroundProperty =
          DependencyProperty.RegisterAttached("BtnTextNormalForeground", typeof(Brush), typeof(TabButtonDependency));

        public static Brush GetBtnTextNormalForeground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(BtnTextNormalForegroundProperty);
        }

        public static void SetBtnTextNormalForeground(DependencyObject obj, Brush value)
        {
            obj.SetValue(BtnTextNormalForegroundProperty, value);
        }

        public static readonly DependencyProperty BtnTextPressedForegroundProperty =
          DependencyProperty.RegisterAttached("BtnTextPressedForeground", typeof(Brush), typeof(TabButtonDependency));

        public static Brush GetBtnTextPressedForeground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(BtnTextPressedForegroundProperty);
        }

        public static void SetBtnTextPressedForeground(DependencyObject obj, Brush value)
        {
            obj.SetValue(BtnTextPressedForegroundProperty, value);
        }
    }
}

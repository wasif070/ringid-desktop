﻿using Models.Constants;
using Models.DAO;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using View.BindingModels;
using View.Constants;
using View.UI.PopUp;
using View.Utility.WPFMessageBox;
using View.ViewModel;

namespace View.Utility
{
    public class CustomFileDownloader : FileDownloader
    {
        //private ConcurrentQueue<SingleMediaModel> Queue = new ConcurrentQueue<SingleMediaModel>();
        SingleMediaModel model;
        public CustomFileDownloader(SingleMediaModel model)
        {
            this.model = model;

            LocalDirectory = RingIDSettings.TEMP_DOWNLOADED_MEDIA_FOLDER;

            FileDownloadStarted += new EventHandler(downloader_FileDownloadStarted);
            Paused += new EventHandler(downloader_FileDownloadPaused);
            Resumed += new EventHandler(downloader_FileDownloadResumed);
            Canceled += new EventHandler(downloader_FileDownloadCanceled);
            Completed += new EventHandler(downloader_FileDownloadCompleted);
            //CalculatingFileSize += new FileDownloader.CalculatingFileSizeEventHandler(downloader_CalculationFileSize);
            ProgressChanged += new EventHandler(downloader_ProgressChanged);
            FileDownloadFailed += new View.Utility.FileDownloader.FailEventHandler(downloader_FileDownloadFailed);

            Files.Add(new FileDownloader.FileInfo(ServerAndPortSettings.GetVODServerResourceURL + model.StreamUrl, model.ContentId.ToString()));
            Start();
        }

        #region "Downloader Events"

        private void downloader_FileDownloadStarted(object sender, EventArgs e)
        {
            if (model.DownloadProgress < 100)
            {
                if (CurrentFileSize > 0)
                {
                    if (model.DownloadState != StatusConstants.MEDIA_DOWNLOAD_COMPLETED_STATE)
                        InsertOrUpdateDownloadMedia(StatusConstants.MEDIA_DOWNLOADING_STATE);
                }
                else
                {
                    Stop();
                }
            }
            else
            {
                ProgressChanged -= downloader_ProgressChanged;
                Completed -= downloader_FileDownloadCompleted;
                Started -= downloader_FileDownloadStarted;
                InsertOrUpdateDownloadMedia(StatusConstants.MEDIA_DOWNLOAD_COMPLETED_STATE);
                var modelSearched = RingIDViewModel.Instance.UploadingModelList.FirstOrDefault(p => p.ContentId == model.ContentId);
                RingIDViewModel.Instance.UploadingModelList.InvokeRemove(modelSearched);
                if (ucUploadingPopup != null && RingIDViewModel.Instance.UploadingModelList.Count == 0)
                {
                    ucUploadingPopup.popupUploading.IsOpen = false;
                }
            }
        }

        private void downloader_FileDownloadPaused(object sender, EventArgs e)
        {
            InsertOrUpdateDownloadMedia(StatusConstants.MEDIA_DOWNLOAD_PAUSE_STATE, model.DownloadProgress);
        }

        private void downloader_FileDownloadResumed(object sender, EventArgs e)
        {
            InsertOrUpdateDownloadMedia(StatusConstants.MEDIA_DOWNLOADING_STATE, model.DownloadProgress);
        }

        private void downloader_FileDownloadCanceled(object sender, EventArgs e)
        {
            //DeleteDownloadMedia(model.ContentId);
            model.DownloadState = StatusConstants.MEDIA_DOWNLOAD_NOT_START_STATE;
            model.DownloadProgress = 0;
            MediaDAO.Instance.DeleteFromDownloadedMediasTable(model.ContentId);
            //TODO delete downloaded files from PC
            if (RingIDViewModel.Instance.MyDownloads.Any(P => P.ContentId == model.ContentId))
                RingIDViewModel.Instance.MyDownloads.Remove(model);
        }

        private void downloader_FileDownloadCompleted(object sender, EventArgs e)
        {
            InsertOrUpdateDownloadMedia(StatusConstants.MEDIA_DOWNLOAD_COMPLETED_STATE);
            var modelSearched = RingIDViewModel.Instance.UploadingModelList.FirstOrDefault(p => p.ContentId == model.ContentId);
            RingIDViewModel.Instance.UploadingModelList.InvokeRemove(modelSearched);
            if (ucUploadingPopup != null && RingIDViewModel.Instance.UploadingModelList.Count == 0)
            {
                ucUploadingPopup.popupUploading.IsOpen = false;
            }
        }

        //private void downloader_CalculationFileSize(object sender, Int32 fileNr)
        //{

        //}

        private void downloader_ProgressChanged(object sender, EventArgs e)
        {
            model.DownloadProgress = (int)CurrentFilePercentage();
            var modelSearched = RingIDViewModel.Instance.UploadingModelList.FirstOrDefault(p => p.ContentId == model.ContentId);
            if (modelSearched != null)
            {
                modelSearched.TotalUploadedInPercentage = model.DownloadProgress;
            }
        }

        private void downloader_FileDownloadFailed(object sender, Exception ex)
        {
            //DeleteDownloadMedia(model.ContentId);
            model.DownloadState = StatusConstants.MEDIA_DOWNLOAD_NOT_START_STATE;
            model.DownloadProgress = 0;
            MediaDAO.Instance.DeleteFromDownloadedMediasTable(model.ContentId);
            if (RingIDViewModel.Instance.MyDownloads.Any(P => P.ContentId == model.ContentId))
                RingIDViewModel.Instance.MyDownloads.Remove(model);
            //TODO delete downloaded files from PC
            //CustomMessageBox.ShowError(ex.Message);
            UIHelperMethods.ShowWarning(ex.Message, "Downlaod failed");
        }

        #endregion

        #region "Properties" ucUploadingPopup

        public UCUploadingPopup ucUploadingPopup
        {
            get
            {
                return MainSwitcher.PopupController.ucUploadingPopup;
            }
        }

        public UCFeedDownloadPauseCancelPopup FeedDownloadPauseCancelPopup
        {
            get
            {
                return MainSwitcher.PopupController.FeedDownloadPauseCancelPopup;
            }
        }

        private void HandlePopupPausedResume()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                if (model.DownloadState != StatusConstants.MEDIA_DOWNLOADING_STATE && FeedDownloadPauseCancelPopup.popupPausedResume.IsOpen)
                {
                    FeedDownloadPauseCancelPopup.popupPausedResume.IsOpen = false;
                }
            });
        }

        public void InsertOrUpdateDownloadMedia(int state, int downloadProgress = 0)
        {
            model.DownloadState = state;
            HandlePopupPausedResume();
            model.DownloadProgress = downloadProgress;
            model.DownloadTime = Models.Utility.ModelUtility.CurrentTimeMillis();

            if (!RingIDViewModel.Instance.MyDownloads.Any(P => P.ContentId == model.ContentId))
                RingIDViewModel.Instance.MyDownloads.InvokeAdd(model);

            MediaDAO.Instance.InsertIntoDownloadedMediasTable(model.ContentId, model.MediaOwner.UserTableID, model.Title, model.Artist, model.StreamUrl, model.ThumbUrl, model.Duration, model.ThumbImageWidth, model.ThumbImageHeight, model.MediaType, model.AlbumId, model.AlbumName, model.AccessCount, model.DownloadState, model.DownloadTime, model.DownloadProgress);
        }
        #endregion

        //CommentedParts
        //public void DeleteDownloadMedia(long mediaContentId)
        //{
        //    //model.DownloadState = StatusConstants.MEDIA_DOWNLOAD_NOT_START_STATE;
        //    //model.DownloadProgress = 0;
        //    MediaDAO.Instance.DeleteFromDownloadedMediasTable(mediaContentId);

        //    //TODO
        //    //if (Files != null && Files.Count() > 1)
        //    //{
        //    //    System.IO.File.Delete(LocalDirectory + "\\" + Files[0].Name);
        //    //}

        //    //TODO
        //    //var modelSearched = RingIDViewModel.Instance.UploadingModelList.FirstOrDefault(p => p.ContentId == model.ContentId);
        //    //if (modelSearched != null)
        //    //{
        //    //    modelSearched.IsUploadFailed = true;
        //    //}
        //}

        //public void EnqueueSingleMediaModelForDownload(SingleMediaModel model)
        //{
        //    this.model = model;
        //    Files.Add(new FileDownloader.FileInfo(ServerAndPortSettings.GetVODServerResourceURL + model.StreamUrl, model.ContentId));
        //    Start();
        //}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;


namespace View.Utility
{
    class GroupMemberListPanel : WrapPanel
    {
        private double _MaxColumnHeight = 0;
        private double _MinColumnHeight = 0;

        public double DefaultWidth
        {
            get { return (double)GetValue(DefaultWidthProperty); }
            set { SetValue(DefaultWidthProperty, value); }
        }


        // Using a DependencyProperty as the backing store for DefaultWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DefaultWidthProperty =
            DependencyProperty.Register("DefaultWidth", typeof(double), typeof(GroupMemberListPanel), new UIPropertyMetadata(600.0, (s, e) => { }));

        public ScrollViewer GetAncestorScrollViewer(UIElement element)
        {
            if (element == null)
            {
                return null;
            }

            UIElement parent = VisualTreeHelper.GetParent(element) as UIElement;
            while (parent != null && !(parent is ScrollViewer))
            {
                parent = VisualTreeHelper.GetParent(parent) as UIElement;
            }
            return parent is ScrollViewer ? parent as ScrollViewer : null;
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            double width = double.IsPositiveInfinity(availableSize.Width) ? 0 : availableSize.Width;
            width = width - Margin.Left - Margin.Right;
            width = Math.Max(width, DefaultWidth);

            int estimatedNumberOfColumn = (int)width / (int)DefaultWidth;
            estimatedNumberOfColumn = estimatedNumberOfColumn > 0 ? estimatedNumberOfColumn : 1;
            double[] columnHeightArray = new double[estimatedNumberOfColumn];

            UIElementCollection children = InternalChildren;
            int count = children.Count;

            int activeChildCount = 0;
            for (int i = 0; i < count; i++)
            {
                UIElement child = children[i];
                if (child == null)
                    continue;

                ContentPresenter presenter = ((ContentPresenter)child);
                presenter.ApplyTemplate();
                DataTemplate templete = presenter.ContentTemplate;
                Control container = (Control)templete.FindName("conatiner", presenter);
                if (container.Visibility == Visibility.Collapsed)
                    continue;

                activeChildCount++;
            }

            int lastRow = (activeChildCount % columnHeightArray.Length) == 0
                ? activeChildCount - columnHeightArray.Length
                : activeChildCount - (activeChildCount % columnHeightArray.Length);

            for (int i = 0, activeIdx = 0; i < count; i++)
            {
                UIElement child = children[i];
                if (child == null)
                    continue;

                ContentPresenter presenter = ((ContentPresenter)child);
                DataTemplate templete = presenter.ContentTemplate;
                Control container = (Control)templete.FindName("conatiner", presenter);
                if (container.Visibility == Visibility.Collapsed)
                {
                    Size cc = new Size(0, 0);
                    cc.Width = Math.Max(0.0, 0);
                    cc.Height = Math.Max(0.0, 0);
                    child.Measure(cc);
                    continue;
                }

                int idx = GetMinColumnIndex(columnHeightArray);
                _MinColumnHeight = columnHeightArray[idx];

                Size childConstraint = new Size(0, 0);
                childConstraint.Width = Math.Max(0.0, DefaultWidth);
                childConstraint.Height = Math.Max(0.0, availableSize.Height - _MinColumnHeight);
                child.Measure(childConstraint);
                columnHeightArray[idx] = _MinColumnHeight + child.DesiredSize.Height;

                if (container != null)
                {
                    container.Tag = String.Empty;
                    if ((activeIdx + 1) % columnHeightArray.Length == 0 || (activeIdx + 1) == activeChildCount)
                    {
                        container.Tag += "RIGHT";
                    }
                    if (activeIdx >= lastRow)
                    {
                        container.Tag += "BOTTOM";
                    }
                }
                activeIdx++;
            }

            int maxIdx = GetMaxColumnIndex(columnHeightArray);
            int minIdx = GetMinColumnIndex(columnHeightArray);

            _MaxColumnHeight = columnHeightArray[maxIdx] + Margin.Top + Margin.Bottom;
            _MinColumnHeight = columnHeightArray[minIdx] + Margin.Top + Margin.Bottom;
            availableSize.Height = _MaxColumnHeight;

            return (new Size(width, availableSize.Height));
        }


        protected override Size ArrangeOverride(Size totalAvailableSize)
        {
            ScrollViewer scrollViewer = GetAncestorScrollViewer(this);
            int scrollBarWidth = !(scrollViewer == null || scrollViewer.ComputedVerticalScrollBarVisibility == Visibility.Collapsed) ? 10 : 0;

            double width = double.IsPositiveInfinity(totalAvailableSize.Width) ? 0 : totalAvailableSize.Width;
            width = width - Margin.Left - Margin.Right;
            width = Math.Max(width, DefaultWidth);

            int estimatedNumberOfColumn = (int)width / (int)DefaultWidth;
            estimatedNumberOfColumn = estimatedNumberOfColumn > 0 ? estimatedNumberOfColumn : 1;
            double[] columnHeightArray = new double[estimatedNumberOfColumn];

            for (int i = 0; i < columnHeightArray.Length; i++)
            {
                columnHeightArray[i] += Margin.Top;
            }

            double emptySpace = (width - DefaultWidth * columnHeightArray.Length) / 2;

            UIElementCollection children = InternalChildren;
            int count = children.Count;

            for (int i = 0; i < count; i++)
            {
                UIElement child = children[i];
                Rect finalRect = new Rect(0.0, 0.0, 0.0, 0.0);

                if (child == null)
                {
                    continue;
                }

                if (child.Visibility != Visibility.Collapsed)
                {
                    int idx = GetMinColumnIndex(columnHeightArray);
                    _MinColumnHeight = columnHeightArray[idx];
                    columnHeightArray[idx] = _MinColumnHeight + child.DesiredSize.Height;

                    finalRect.X = Margin.Left + emptySpace + (idx * DefaultWidth) + scrollBarWidth;
                    finalRect.Y = _MinColumnHeight;
                    finalRect.Width = child.DesiredSize.Width;
                    finalRect.Height = child.DesiredSize.Height;
                }

                child.Arrange(finalRect);
            }

            return totalAvailableSize;
        }

        private int GetMinColumnIndex(double[] columnHeight)
        {
            int minIndex = 0;
            double minHeight = columnHeight[0];
            int idx = 1;

            while (idx < columnHeight.Length)
            {
                if (columnHeight[idx] < minHeight)
                {
                    minHeight = columnHeight[idx];
                    minIndex = idx;
                }
                idx++;
            }

            return minIndex;
        }

        private int GetMaxColumnIndex(double[] columnHeight)
        {
            int maxIndex = 0;
            double maxHeight = 0;
            int idx = 0;

            while (idx < columnHeight.Length)
            {
                if (columnHeight[idx] > maxHeight)
                {
                    maxHeight = columnHeight[idx];
                    maxIndex = idx;
                }
                idx++;
            }

            return maxIndex;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace View.Utility
{
    class CustomSignInSignUpBtn
    {
        public static readonly DependencyProperty BtnNormalImageProperty =
           DependencyProperty.RegisterAttached("BtnNormalImage", typeof(ImageSource), typeof(CustomSignInSignUpBtn));

        public static ImageSource GetBtnNormalImage(DependencyObject obj)
        {
            return (ImageSource)obj.GetValue(BtnNormalImageProperty);
        }

        public static void SetBtnNormalImage(DependencyObject obj, ImageSource value)
        {
            obj.SetValue(BtnNormalImageProperty, value);
        }

        public static readonly DependencyProperty BtnTextProperty =
         DependencyProperty.RegisterAttached("BtnText", typeof(String), typeof(CustomSignInSignUpBtn));

        public static String GetBtnText(DependencyObject obj)
        {
            return (String)obj.GetValue(BtnTextProperty);
        }

        public static void SetBtnText(DependencyObject obj, String value)
        {
            obj.SetValue(BtnTextProperty, value);
        }

        public static readonly DependencyProperty BtnNormalBackgroundProperty =
          DependencyProperty.RegisterAttached("BtnNormalBackground", typeof(Brush), typeof(CustomSignInSignUpBtn));

        public static Brush GetBtnNormalBackground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(BtnNormalBackgroundProperty);
        }

        public static void SetBtnNormalBackground(DependencyObject obj, Brush value)
        {
            obj.SetValue(BtnNormalBackgroundProperty, value);
        }

        public static readonly DependencyProperty BtnHoverBackgroundProperty =
          DependencyProperty.RegisterAttached("BtnHoverBackground", typeof(Brush), typeof(CustomSignInSignUpBtn));

        public static Brush GetBtnHoverBackground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(BtnHoverBackgroundProperty);
        }

        public static void SetBtnHoverBackground(DependencyObject obj, Brush value)
        {
            obj.SetValue(BtnHoverBackgroundProperty, value);
        }

        public static readonly DependencyProperty BtnInactiveBackgroundProperty =
         DependencyProperty.RegisterAttached("BtnInactiveBackground", typeof(Brush), typeof(CustomSignInSignUpBtn));

        public static Brush GetBtnInactiveBackground(DependencyObject obj)
        {
            return (Brush)obj.GetValue(BtnInactiveBackgroundProperty);
        }

        public static void SetBtnInactiveBackground(DependencyObject obj, Brush value)
        {
            obj.SetValue(BtnInactiveBackgroundProperty, value);
        }

        public static readonly DependencyProperty BtnNormalBorderBrushProperty =
           DependencyProperty.RegisterAttached("BtnNormalBorderBrush", typeof(Brush), typeof(CustomSignInSignUpBtn));

        public static Brush GetBtnNormalBorderBrush(DependencyObject obj)
        {
            return (Brush)obj.GetValue(BtnNormalBorderBrushProperty);
        }

        public static void SetBtnNormalBorderBrush(DependencyObject obj, Brush value)
        {
            obj.SetValue(BtnNormalBorderBrushProperty, value);
        }

        public static readonly DependencyProperty BtnHoverBorderBrushProperty =
           DependencyProperty.RegisterAttached("BtnHoverBorderBrush", typeof(Brush), typeof(CustomSignInSignUpBtn));

        public static Brush GetBtnHoverBorderBrush(DependencyObject obj)
        {
            return (Brush)obj.GetValue(BtnHoverBorderBrushProperty);
        }

        public static void SetBtnHoverBorderBrush(DependencyObject obj, Brush value)
        {
            obj.SetValue(BtnHoverBorderBrushProperty, value);
        }

        public static readonly DependencyProperty BtnTypeProperty =
          DependencyProperty.RegisterAttached("BtnType", typeof(ImageSource), typeof(CustomSignInSignUpBtn));

        public static ImageSource GetBtnType(DependencyObject obj)
        {
            return (ImageSource)obj.GetValue(BtnTypeProperty);
        }

        public static void SetBtnType(DependencyObject obj, ImageSource value)
        {
            obj.SetValue(BtnTypeProperty, value);
        }

    }
}

﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using View.Constants;
using View.Utility.Chat;

namespace View.Utility.audio
{
    public class AudioFilesAndSettings
    {
        private static ILog log = LogManager.GetLogger(typeof(AudioFilesAndSettings).Name);

        #region audio variables
        private readonly static string AudioFolder = RingIDSettings.AUDIO_FOLDER + Path.DirectorySeparatorChar;
        public static readonly string CLIP_ON = AudioFolder + "on.wav";
        public static readonly string CLIP_OFF = AudioFolder + "off.wav";
        public static readonly string CLIP_CALL_PROGRESS = AudioFolder + "progress.wav";
        public static readonly string CLIP_RINGING = AudioFolder + "ringing.wav";
        public static readonly string LANDING_AUDIO = AudioFolder + "landing.wav";
        public static readonly string IM_DEVIEVERED = AudioFolder + "im_delievered.wav";
        public static readonly string IM_RECEIVED = AudioFolder + "im_received.wav";
        public static readonly string IM_RECEIVED_ALERT = AudioFolder + "im_received_alert.wav";
        public static readonly string IM_STICKER_SEND = AudioFolder + "im_sticker_send.wav";
        public static readonly string HOLD_TUNE = AudioFolder + "holdTune.wav";
        public static readonly string NOTIFICATION_TUNE = AudioFolder + "notification_alert.wav";
        #endregion

        public const int CLIP_OFF_TUNE = 0;
        public const int IM_DELIEVERED_TUNE = 1;
        public const int IM_RECEIVED_TUNE = 2;
        public const int IM_RECEIVED_ALERT_TUNE = 3;
        public const int IM_STICKER_SEND_TUNE = 4;
        public const int SPEAKER_TEST_TUNE = 5;
        public const int NOTIFICATION_RECEIVED = 6;

        public static SoundPlayer ClipOffTune;
        public static SoundPlayer ImDelieveredTune;
        public static SoundPlayer ImReceivedTune;
        public static SoundPlayer ImReceivedAlertTune;
        public static SoundPlayer ImStickerSendTune;
        public static SoundPlayer SepeakerTestTune;
        public static SoundPlayer NotificationTestTune;


        #region players init

        public static void initPlayers()
        {
            try
            {
                ClipOffTune = new SoundPlayer(new MemoryStream(ChatHelpers.ReadAllBytes(CLIP_OFF), false));
                ImDelieveredTune = new SoundPlayer(new MemoryStream(ChatHelpers.ReadAllBytes(IM_DEVIEVERED), false));
                ImReceivedTune = new SoundPlayer(new MemoryStream(ChatHelpers.ReadAllBytes(IM_RECEIVED), false));
                ImReceivedAlertTune = new SoundPlayer(new MemoryStream(ChatHelpers.ReadAllBytes(IM_RECEIVED_ALERT), false));
                ImStickerSendTune = new SoundPlayer(new MemoryStream(ChatHelpers.ReadAllBytes(IM_STICKER_SEND), false));
                SepeakerTestTune = new SoundPlayer(new MemoryStream(ChatHelpers.ReadAllBytes(CLIP_RINGING), false));
                NotificationTestTune = new SoundPlayer(new MemoryStream(ChatHelpers.ReadAllBytes(NOTIFICATION_TUNE), false));
            }
            catch (Exception ex)
            {
                log.Error("initPlayers() => " + ex.Message + "\n" + ex.StackTrace);
            }
        }


        public static void Play(int type)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    switch (type)
                    {
                        case CLIP_OFF_TUNE:
                            ClipOffTune.Stop();
                            ClipOffTune.Play();
                            break;
                        case IM_DELIEVERED_TUNE:
                            ImDelieveredTune.Stop();
                            ImDelieveredTune.Play();
                            break;
                        case IM_RECEIVED_TUNE:
                            ImReceivedTune.Stop();
                            ImReceivedTune.Play();
                            break;
                        case IM_RECEIVED_ALERT_TUNE:
                            ImReceivedAlertTune.Stop();
                            ImReceivedAlertTune.Play();
                            break;
                        case IM_STICKER_SEND_TUNE:
                            ImStickerSendTune.Stop();
                            ImStickerSendTune.Play();
                            break;
                        case SPEAKER_TEST_TUNE:
                            SepeakerTestTune.Stop();
                            SepeakerTestTune.Play();
                            break;
                        case NOTIFICATION_RECEIVED:
                            NotificationTestTune.Stop();
                            NotificationTestTune.Play();
                            break;
                            
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Play() => Type = " + type + ", Error = " + ex.Message + "\n" + ex.StackTrace);
                }
            }, System.Windows.Threading.DispatcherPriority.ApplicationIdle);
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using log4net;
using Models.Constants;
using NAudio.CoreAudioApi;

namespace View.Utility.audio
{
    public class MasterVolumControl
    {
        private static int volumeLabel;

        public static float GetCurrentVolumnLabel()
        {
            MMDeviceEnumerator devEnum = new MMDeviceEnumerator();
            MMDevice defaultDevice = devEnum.GetDefaultAudioEndpoint(DataFlow.Render, Role.Multimedia);
            string currVolume = "MasterPeakVolume : " + defaultDevice.AudioMeterInformation.MasterPeakValue.ToString();
            return defaultDevice.AudioMeterInformation.MasterPeakValue;
        }
        public static void SetVolume(int level)
        {
            try
            {
                //Instantiate an Enumerator to find audio devices
                NAudio.CoreAudioApi.MMDeviceEnumerator MMDE = new NAudio.CoreAudioApi.MMDeviceEnumerator();
                //Get all the devices, no matter what condition or status
                NAudio.CoreAudioApi.MMDeviceCollection DevCol = MMDE.EnumerateAudioEndPoints(NAudio.CoreAudioApi.DataFlow.All, NAudio.CoreAudioApi.DeviceState.All);
                //Loop through all devices
                foreach (NAudio.CoreAudioApi.MMDevice dev in DevCol)
                {
                    try
                    {
                        if (dev.State == NAudio.CoreAudioApi.DeviceState.Active)
                        {
                            var newVolume = (float)Math.Max(Math.Min(level, 100), 0) / (float)100;
                            volumeLabel = level;
                            //Set at maximum volume
                            dev.AudioEndpointVolume.MasterVolumeLevelScalar = newVolume;

                            dev.AudioEndpointVolume.Mute = level == 0;

                            //Get its audio volume
                            //  log.Info("Volume of " + dev.FriendlyName + " is " + dev.AudioEndpointVolume.MasterVolumeLevelScalar.ToString());
                        }
                        else
                        {
                            //   log.Debug("Ignoring device " + dev.FriendlyName + " with state " + dev.State);
                        }
                    }
                    catch (Exception)
                    {
                        //Do something with exception when an audio endpoint could not be muted
                        //log.Warn(dev.FriendlyName + " could not be muted with error " + ex);
                    }
                }
            }
            catch (Exception ex)
            {
                //When something happend that prevent us to iterate through the devices
                log.Warn("Could not enumerate devices due to an excepion: " + ex.Message);
            }
        }

        private static ILog log = LogManager.GetLogger(typeof(MasterVolumControl).Name);
        public static void MuteAllInput()
        {
            SetVolume(0);
            //try
            //{
            //    NAudio.CoreAudioApi.MMDeviceEnumerator MMDE = new NAudio.CoreAudioApi.MMDeviceEnumerator();
            //    NAudio.CoreAudioApi.MMDeviceCollection DevCol = MMDE.EnumerateAudioEndPoints(NAudio.CoreAudioApi.DataFlow.Capture, NAudio.CoreAudioApi.DeviceState.Active);
            //    if (DevCol.Count > 0 && DevCol.Count > SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER)
            //    {
            //        DevCol[SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER].AudioEndpointVolume.Mute = true;
            //    }
            //    if (CallConstants.CallSettings != null)
            //    {
            //        CallConstants.CallSettings.Muted = true;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    log.Error("audio endpoint could not be muted" + ex.Message + "\n" + ex.StackTrace);
            //}
        }
        public static void unMuteAllInput(int volumn = 100)
        {
            if (volumeLabel <= 0)
                SetVolume(volumn);
            //try
            //{
            //    NAudio.CoreAudioApi.MMDeviceEnumerator MMDE = new NAudio.CoreAudioApi.MMDeviceEnumerator();
            //    NAudio.CoreAudioApi.MMDeviceCollection DevCol = MMDE.EnumerateAudioEndPoints(NAudio.CoreAudioApi.DataFlow.Capture, NAudio.CoreAudioApi.DeviceState.Active);
            //    if (DevCol.Count > 0 && DevCol.Count > SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER)
            //    {
            //        DevCol[SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER].AudioEndpointVolume.Mute = false;
            //    }
            //    if (CallConstants.CallSettings != null)
            //    {
            //        CallConstants.CallSettings.Muted = false;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    log.Error("audio endpoint could not be unmuted" + ex.Message + "\n" + ex.StackTrace);
            //    //When something happend that prevent us to iterate through the devices
            //}
        }
    }
}

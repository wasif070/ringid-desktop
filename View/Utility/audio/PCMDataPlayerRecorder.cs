﻿using System;
using System.Runtime.InteropServices;
using Models.Constants;
using NAudio.CoreAudioApi.Interfaces;
using NAudio.Wave;
using View.Utility.Naudio;

namespace View.Utility.audio
{
    public class PCMDataPlayerRecorder
    {
        #region "Private Fields"
        private log4net.ILog log = log4net.LogManager.GetLogger(typeof(PCMDataPlayerRecorder).Name);
        private NAudio.CoreAudioApi.MMDeviceEnumerator mmde;
        private NAudio.CoreAudioApi.MMDeviceCollection devColRecorders;
        private NAudio.CoreAudioApi.MMDeviceCollection devColPlayer;
        private NotificationClientImplementation notificationClient;
        private NAudio.CoreAudioApi.Interfaces.IMMNotificationClient notifyClient;
        private NAudio.Wave.WaveFormat wvFormat = new WaveFormat(8000, 16, 1);
        private BufferedWaveProvider bufferedWaveProvider;
        private WaveIn recorder;
        private WaveOut player;
        private Object thisLock = new Object();
        public event DelegateByte OnPCMdataAvailable;
        #endregion "Private Fields"

        #region "Public Fields"

        #endregion "Public Fields"

        #region "Constructors"

        public PCMDataPlayerRecorder()
        {
            bufferedWaveProvider = new BufferedWaveProvider(wvFormat);
            mmde = new NAudio.CoreAudioApi.MMDeviceEnumerator();
            notificationClient = new NotificationClientImplementation();
            notifyClient = (NAudio.CoreAudioApi.Interfaces.IMMNotificationClient)notificationClient;
            mmde.RegisterEndpointNotificationCallback(notifyClient);
        }

        ~PCMDataPlayerRecorder()
        {
            if (recorder != null)
            {
                recorder.DataAvailable -= RecorderOnDataAvailable;
                mmde.UnregisterEndpointNotificationCallback(notifyClient);
            }
        }

        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        private void RecorderOnDataAvailable(object sender, WaveInEventArgs waveInEventArgs)
        {
            //try
            //{
            //    CallHelperMethods.SendPCMData(waveInEventArgs.Buffer);
            //}
            //catch (Exception ex)
            //{
            //    log.Error("RecorderOnDataAvailable==>" + ex.Message + "\n" + ex.StackTrace);
            //    StopPlayerRecorder();
            //}
            OnPCMdataAvailable(waveInEventArgs.Buffer);
        }
        #endregion "Event Trigger"

        #region "Private methods"
        private void InitActiveDeviceses()
        {
            devColRecorders = mmde.EnumerateAudioEndPoints(NAudio.CoreAudioApi.DataFlow.Capture, NAudio.CoreAudioApi.DeviceState.Active);
            devColPlayer = mmde.EnumerateAudioEndPoints(NAudio.CoreAudioApi.DataFlow.Render, NAudio.CoreAudioApi.DeviceState.Active);
        }
        private int RegisterEndpointNotificationCallback([In] [MarshalAs(UnmanagedType.Interface)] IMMNotificationClient client)
        {
            return mmde.RegisterEndpointNotificationCallback(client);

        }
        private int UnRegisterEndpointNotificationCallback([In] [MarshalAs(UnmanagedType.Interface)] IMMNotificationClient client)
        {
            return mmde.UnregisterEndpointNotificationCallback(client);
        }

        public long GetCurrentTimeStamp()
        {
            long milliseconds = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            return milliseconds;
        }

        #endregion "Private methods"

        #region "Public Methods"
        public string StartPlayerRecorder()
        {
            string messg = null;
            try
            {
                lock (thisLock)
                {
                    InitActiveDeviceses();
                    messg = PlayPlayer();
                    messg = PlayRecorder();
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace);
            }
            return messg;
        }

        public void StopPlayerRecorder()
        {
            lock (thisLock)
            {
                try
                {
                    if (recorder != null)
                    {
                        recorder.DataAvailable -= RecorderOnDataAvailable;
                        recorder.StopRecording();
                    }
                    if (player != null)
                    {
                        player.Stop();
                    }
                    player = null;
                    recorder = null;
                }
                catch (Exception ex)
                {
                    log.Error("StopPlayerRecorder==>" + ex.Message);
                }
            }
        }

        public string RestartPlayerRecorder()
        {
            try
            {
                StopPlayerRecorder();
                return StartPlayerRecorder();
            }
            catch (Exception)
            {
            }
            finally
            {

            }
            return null;
        }

        public string PlayPlayer()
        {
            try
            {
                player = new WaveOut();
                player.Init(bufferedWaveProvider);
                bufferedWaveProvider.DiscardOnBufferOverflow = true;
                bufferedWaveProvider.ClearBuffer();
                if (devColPlayer.Count == 1)
                {
                    player.DeviceNumber = 0;
                }
                else if (SettingsConstants.VALUE_RINGID_PLAYER_DEVICE_NUMBER >= 0 && devColPlayer.Count > SettingsConstants.VALUE_RINGID_PLAYER_DEVICE_NUMBER)
                {
                    player.DeviceNumber = SettingsConstants.VALUE_RINGID_PLAYER_DEVICE_NUMBER;
                }
                else if (devColPlayer.Count > 1)
                {
                    int dv = 0;
                    foreach (NAudio.CoreAudioApi.MMDevice device in devColPlayer)
                    {
                        if (device.FriendlyName.ToLower().Contains("high definition"))
                        {
                            player.DeviceNumber = dv;
                            SettingsConstants.VALUE_RINGID_PLAYER_DEVICE_NUMBER = dv;
                            player.Play();
                            return null;
                        }
                        dv++;
                    }
                    player.DeviceNumber = dv;
                }
                else
                {
                    log.Error("Audio Player Problem DevColPlayer.Count");
                    return NotificationMessages.TEXT_AUDIO_PROBLEM;
                }
                player.Play();
            }
            catch (Exception)
            {
                log.Error("Audio Player Problem exception");
                return NotificationMessages.TEXT_AUDIO_PROBLEM;
            }
            return null;
        }

        public string PlayRecorder()
        {
            try
            {
                recorder = new WaveIn();
                recorder.DataAvailable += RecorderOnDataAvailable;
                recorder.WaveFormat = wvFormat;
                if (devColRecorders.Count == 1)
                {
                    recorder.DeviceNumber = 0;
                }
                else if (SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER >= 0 && devColRecorders.Count > SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER)
                {
                    recorder.DeviceNumber = SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER;
                }
                else if (devColRecorders.Count > 1)
                {
                    int dv = 0;
                    foreach (NAudio.CoreAudioApi.MMDevice device in devColRecorders)
                    {
                        //  Console.WriteLine("device.FriendlyName==>" + device.FriendlyName);
                        if (device.FriendlyName.ToLower().Contains("high definition"))
                        {
                            recorder.DeviceNumber = dv;
                            SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER = dv;
                            recorder.StartRecording();
                            return null;
                        }
                        dv++;
                    }
                    recorder.DeviceNumber = 0;
                }
                else
                {
                    log.Error("No Recording device");
                    return NotificationMessages.TEXT_MICROPHONE_PROBLEM;
                }
                recorder.StartRecording();
            }
            catch (Exception ex)
            {
                log.Error("Audio Recorder Problem" + ex.StackTrace);
                return NotificationMessages.TEXT_MICROPHONE_PROBLEM;
            } return null;
        }

        public void AddVoiceSampleByteToPlay(byte[] byteToPlay)
        {
            try
            {
                bufferedWaveProvider.AddSamples(byteToPlay, 0, byteToPlay.Length);
            }
            catch (Exception ex)
            {
                log.Error("Voice Playing problem==>" + ex.Message + "\n" + ex.StackTrace + "  " + ex.Message);
                bufferedWaveProvider.ClearBuffer();
            }
        }

        public void UnRegisterEventTrigger()
        {
            mmde.UnregisterEndpointNotificationCallback(notifyClient);
        }
        #endregion "Public methods"
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace View.Utility
{
    public class Cryptographer
    {


        private static readonly char[] HEX_ARRAY = "0123456789ABCDEF".ToCharArray();

        /**
         * Make an encrypted string from <code>utId</code>, <code>mediaId</code>,
         * <code>authIp</code>, <code>authPort</code>.
         *
         * Byte structure First 8 byte authIp, next 4 byte authPort, next 8 byte
         * utId, next mediaId byte
         *
         * @param utId
         * @param mediaId
         * @param authIp
         * @param authPort
         * @return Encrypted String
         */
        public string encrypt(long utId, string mediaId, string authIp, int authPort)
        {
            byte[] ip = ipToByteArray(authIp);
            byte[] port = intToByteArray(authPort);
            byte[] utIdByte = longToBytes(utId);
            byte[] mediaIdByte = System.Text.Encoding.UTF8.GetBytes(mediaId);

            //byte[] finalByte = new byte[ip.Length + port.Length + utIdByte.Length + mediaIdByte.Length];
            //ByteBuffer target = ByteBuffer.wrap(finalByte);
            //target.put(ip).put(port).put(utIdByte).put(mediaIdByte);

            MemoryStream stream = new MemoryStream();
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                writer.Write(ip);
                writer.Write(port);
                writer.Write(utIdByte);
                writer.Write(mediaIdByte);
            }
            byte[] finalByte = stream.ToArray();

            Random random = new Random();
            int keyLength = random.Next(5) + 1;
            int encryptedDataLength = keyLength + finalByte.Length + 1;
            byte[] encryptedData = new byte[encryptedDataLength];

            encryptedData[0] = (byte)keyLength;
            for (int i = 1; i < keyLength + 1; i++)
            {
                encryptedData[i] = (byte)(random.Next(64) + 1);
            }
            int j = 1;
            for (int i = keyLength + 1; i < encryptedDataLength; i++)
            {
                encryptedData[i] = (byte)(finalByte[j - 1] ^ encryptedData[j & 3]);
                j++;
            }

            return bytesToHex(encryptedData);
        }


        /**
         * Make hexadecimal string from byte array
         *
         * @param bytes byte array
         * @return String
         */
        private string bytesToHex(byte[] bytes)
        {
            char[] hexChars = new char[bytes.Length * 2];
            for (int j = 0; j < bytes.Length; j++)
            {
                int v = bytes[j] & 0xFF;
                int hexCharsPos = j << 1;
                hexChars[hexCharsPos] = HEX_ARRAY[v >> 4];
                hexChars[hexCharsPos + 1] = HEX_ARRAY[v & 0x0F];
            }

            return new string(hexChars);
        }

        /**
         * Long to byte array
         *
         * @param number
         * @return
         */
        public byte[] longToBytes(long number)
        {
            //ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
            //buffer.putLong(number);
            //return buffer.array();
            //if (BitConverter.IsLittleEndian) Array.Reverse(number);
            byte[] tempByte = BitConverter.GetBytes(number);
            if (BitConverter.IsLittleEndian) Array.Reverse(tempByte);
            return tempByte;
        }

        /**
         * Convert String IP address to byte array
         *
         * @param ipAddress String
         * @return byte array
         */
        private byte[] ipToByteArray(string ipAddress)
        {
            long ipInLong = 0;
            string[] ipAddressInArray = ipAddress.Split('.');

            for (int i = 3; i >= 0; i--)
            {
                long temp = long.Parse(ipAddressInArray[3 - i]);
                ipInLong |= temp << (i << 3);
            }

            byte[] result = new byte[8];
            for (int i = 7; i >= 0; i--)
            {
                result[i] = (byte)(ipInLong & 0xFF);
                ipInLong >>= 8;
            }

            return result;
        }

        /**
         * Convert Integer number to byte array
         *
         * @param value Integer
         * @return byte array
         */
        private byte[] intToByteArray(int value)
        {
            return new byte[]{
            (byte) (value >> 24),
            (byte) (value >> 16),
            (byte) (value >> 8),
            (byte) value};
        }
    }
}

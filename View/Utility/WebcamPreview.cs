﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;
using View.Utility.Recorder.DirectX.Capture;

namespace View.Utility
{
    class WebcamPreview : Panel
    {
        private BitmapCapture capture = null;
        private Filters filters = null;
        public Bitmap Bitmap = null;

        public const int ERROR_NONE = 0;
        public const int ERROR_WEBCAM_NOT_FOUND = 1;
        public const int ERROR_WEBCAM_ALREADY_USING = 2;
        public const int FRAME_WIDTH = 320;
        public const int FRAME_HEIGHT = 240;

        public int StartWebcam(int width = FRAME_WIDTH, int height = FRAME_HEIGHT)
        {
            try
            {
                filters = new Filters();
                if (filters.VideoInputDevices == null || filters.VideoInputDevices.Count <= 0)
                {
                    return WebcamPreview.ERROR_WEBCAM_NOT_FOUND;
                }
                capture = new BitmapCapture(GetVideoDevice());
                capture.FrameSize = new System.Drawing.Size(width, height);
                capture.FrameRate = 60;
                capture.PreviewWindow = this;
                capture.CheckWebcam();

                numberofFrame = 0;
            }
            catch (Exception)
            {
                return WebcamPreview.ERROR_WEBCAM_ALREADY_USING;
            }
            return WebcamPreview.ERROR_NONE;
        }

        public int RestartWebcam(int width = FRAME_WIDTH, int height = FRAME_HEIGHT)
        {
            Bitmap = null;
            try
            {
                if (capture != null)
                {
                    capture.Dispose();
                    capture = null;
                }

                filters = new Filters();
                if (filters.VideoInputDevices == null || filters.VideoInputDevices.Count <= 0)
                {
                    return WebcamPreview.ERROR_WEBCAM_NOT_FOUND;
                }
                capture = new BitmapCapture(GetVideoDevice());
                capture.FrameSize = new System.Drawing.Size(width, height);
                //         capture.FrameRate = 60;
                capture.PreviewWindow = this;
            }
            catch (Exception)
            {
                return WebcamPreview.ERROR_WEBCAM_ALREADY_USING;
            }
            return WebcamPreview.ERROR_NONE;
        }

        public int StartStream()
        {
            try
            {
                if (capture == null)
                {
                    return WebcamPreview.ERROR_WEBCAM_NOT_FOUND;
                }

                if (filters == null || filters.VideoInputDevices == null || filters.VideoInputDevices.Count <= 0)
                {
                    return WebcamPreview.ERROR_WEBCAM_NOT_FOUND;
                }

                capture.TakePicture();
                capture.OnStream -= OnStream;
                capture.OnStream += OnStream;
                //capture.PausePreview();

                //capture.VideoCompressor = filters.VideoCompressors[0];
                //capture.AudioCompressor = filters.AudioCompressors[0];
                //capture.FrameRate = 29.997;
                //capture.FrameSize = new System.Drawing.Size(640, 480);
                //capture.AudioSamplingRate = 44100;
                //capture.AudioSampleSize = 16;
                //Filter videoDevice = capture.VideoDevice;
                //Filter audioDevice = capture.AudioDevice;
                //Filter videoCompressor = capture.VideoCompressor;
                //Filter audioCompressor = capture.AudioCompressor;
                //Source videoSource = capture.VideoSource;
                //Source audioSource = capture.AudioSource;
                //int frameRate = (int)(capture.FrameRate * 1000);
                //System.Drawing.Size frameSize = capture.FrameSize;
                //short audioChannels = capture.AudioChannels;
                //int samplingRate = capture.AudioSamplingRate;
                //short sampleSize = capture.AudioSampleSize;

            }
            catch (Exception)
            {
                return WebcamPreview.ERROR_WEBCAM_ALREADY_USING;
            }
            return WebcamPreview.ERROR_NONE;
        }

        public int TakePicture()
        {
            try
            {
                if (capture == null)
                {
                    return WebcamPreview.ERROR_WEBCAM_NOT_FOUND;
                }

                if (filters == null || filters.VideoInputDevices == null || filters.VideoInputDevices.Count <= 0)
                {
                    return WebcamPreview.ERROR_WEBCAM_NOT_FOUND;
                }

                capture.TakePicture();
                capture.OnTakePicture -= OnTakePicture;
                capture.OnTakePicture += OnTakePicture;

                capture.PausePreview();
            }
            catch (Exception)
            {
                return WebcamPreview.ERROR_WEBCAM_ALREADY_USING;
            }
            return WebcamPreview.ERROR_NONE;
        }

        public new void Dispose()
        {
            Bitmap = null;
            if (capture != null)
            {
                capture.Dispose();
                capture = null;
                base.Dispose();
            }
            GC.SuppressFinalize(this);
        }

        private void OnTakePicture(Bitmap bitmap)
        {
            capture.OnTakePicture -= OnTakePicture;
            Bitmap = bitmap;
        }
        public static int numberofFrame = 0;
        public static long time = Models.Utility.ModelUtility.CurrentTimeMillis();
        private void OnStream(Bitmap bmp)
        {
            if (capture != null) { System.Diagnostics.Debug.WriteLine("Rate==>" ); }

            numberofFrame++;
            if (Models.Utility.ModelUtility.CurrentTimeMillis() - time > 1000)
            {
                System.Diagnostics.Debug.WriteLine("numberoVedio==>" + numberofFrame);
                numberofFrame = 0;
                time = Models.Utility.ModelUtility.CurrentTimeMillis();
            }
            MemoryStream ms = new MemoryStream();
            bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            byte[] bmpBytes = ms.ToArray();
            bmp.Dispose();
            ms.Close();
        }

        private Filter GetVideoDevice()
        {
            Filter device = null;
            if (filters.VideoInputDevices.Count > 0)
            {
                device = filters.VideoInputDevices[0];
                if (!String.IsNullOrWhiteSpace(SettingsConstants.VALUE_RINGID_WEBCAM_DEVICE_ID))
                {
                    foreach (Filter filter in filters.VideoInputDevices)
                    {
                        if (filter.MonikerString.Equals(SettingsConstants.VALUE_RINGID_WEBCAM_DEVICE_ID))
                        {
                            device = filter;
                        }
                    }
                }
            }
            return device;
        }

    }
}

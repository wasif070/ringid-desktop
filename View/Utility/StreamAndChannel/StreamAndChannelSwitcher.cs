﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using View.UI.StreamAndChannel;

namespace View.Utility.StreamAndChannel
{
    public class StreamAndChannelSwitcher
    {
        public static UCStreamAndChannelWrapper pageSwitcher;

        public static void Switch(int type)
        {
            pageSwitcher.Navigate(type);
        }

        public static void Switch(int type, object state)
        {
            pageSwitcher.Navigate(type, state);
        }
    }
}

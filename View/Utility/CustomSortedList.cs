﻿using log4net;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace View.Utility
{
    public class CustomSortedList : SortedList<long, Guid>
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CustomSortedList).Name);

        //all feeds <ut, nfid>, pvts in nfid
        //saved feeds <svt, nfid>, pvts in svt
        //others <at, nfid>, pvts in nfid
        public Guid PvtMaxGuid, PvtMinGuid;
        public long PvtMaxLong = -1, PvtMinLong = -1;
        public bool IsSavedFeed = false;

        public CustomSortedList(bool IsSavedFeed = false)
        {
            this.IsSavedFeed = IsSavedFeed;
        }

        public void Reset()
        {
            PvtMaxGuid = Guid.Empty;
            PvtMinGuid = Guid.Empty;
            PvtMaxLong = -1;
            PvtMinLong = -1;
            Clear();
        }

        public object GetPvtMin()
        {
            if (!IsSavedFeed) return PvtMinGuid;
            else return PvtMinLong;
        }
        public object GetPvtMax()
        {
            if (!IsSavedFeed) return PvtMaxGuid;
            else return PvtMaxLong;
        }

        //public Guid NextFeed(Guid nfId, out int moreRemaining)
        //{
        //    moreRemaining = 0;
        //    if (nfId == Guid.Empty || this.Count == 0 || (this.ElementAt(0).Value == nfId)) return Guid.Empty;

        //    for (int i = 1; i < this.Count; i++)
        //    {
        //        if (this.ElementAt(i).Value == nfId)
        //        {
        //            moreRemaining = i;
        //            return this.ElementAt(i - 1).Value;
        //        }
        //    }
        //    return Guid.Empty;
        //}

        //public Guid PreviousFeed(Guid nfId, out int moreRemaining)
        //{
        //    moreRemaining = 0;
        //    if (nfId == Guid.Empty || this.Count == 0 || (this.ElementAt(this.Count - 1).Value == nfId)) return Guid.Empty;

        //    for (int i = this.Count - 2; i >= 0; i--)
        //    {
        //        if (this.ElementAt(i).Value == nfId)
        //        {
        //            moreRemaining = this.Count - 1 - i;
        //            return this.ElementAt(i + 1).Value;
        //        }
        //    }
        //    return Guid.Empty;
        //}

        public Guid FirstFeedIdRemoved()
        {
            try
            {
                if (Count == 0) return Guid.Empty;
                lock (this)
                {
                    if (Count > 0)
                    {
                        Guid nfId = this.ElementAt(0).Value;
                        this.RemoveAt(0);
                        return nfId;
                    }
                    else return Guid.Empty;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
            return Guid.Empty;
        }
        public Guid LastFeedIdRemoved()
        {
            try
            {
                if (Count == 0) return Guid.Empty;
                lock (this)
                {
                    if (Count > 0)
                    {
                        Guid nfId = this.LastOrDefault().Value;
                        this.RemoveAt(Count - 1);
                        return nfId;
                    }
                    else return Guid.Empty;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
            return Guid.Empty;
        }
        public void InsertIntoSortedList(long time, Guid nfId)
        {
            try
            {
                lock (this)
                {
                    if (nfId == Guid.Empty) return;
                    bool duplicate = false;
                    foreach (var item in this)
                    {
                        if (item.Value == nfId) { duplicate = true; break; }
                    }
                    if (!duplicate)
                    {
                        this.Add(time, nfId);
                        if (IsSavedFeed)
                        {
                            if (time >= PvtMaxLong) PvtMaxLong = time;
                            if (time < PvtMinLong) PvtMinLong = time;
                        }
                        else
                        {
                            //long lNf = HelperMethodsModel.GetTimeStampFromGUID(nfId);
                            //if (lNf >= PvtMaxLong) { PvtMaxLong = lNf; PvtMaxGuid = nfId; }
                            //if (lNf < PvtMinLong) { PvtMinLong = lNf; PvtMinGuid = nfId; }
                            //if (nfId.CompareTo(PvtMaxGuid) > 0 || PvtMaxGuid == Guid.Empty) { PvtMaxGuid = nfId; }
                            //if (nfId.CompareTo(PvtMinGuid) < 0 || PvtMinGuid == Guid.Empty) { PvtMinGuid = nfId; }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
        }

        public void RemovedFromSortedList(Guid nfId)
        {
            try
            {
                lock (this)
                {
                    int idx = -1;
                    for (int i = 0; i < this.Count; i++)
                    {
                        if (this.ElementAt(i).Value == nfId) { idx = i; break; }
                        //todo if feed removed pvts to update
                    }
                    if (idx >= 0)
                        this.RemoveAt(idx);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
        }
    }
}

<<<<<<< HEAD
﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Media.Imaging;
using log4net;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.UI;
using View.UI.Feed;
using View.UI.PopUp;
using View.Utility.Feed;
using View.Utility.Images;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.ViewModel.NewStatus;

namespace View.Utility
{
    public class ImageUploader
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ImageUploader).Name);
        private NewStatusViewModel newStatusViewModel;
        private WebClient _WebClient = null;
        private bool _SinglePictureUploadFailed = false;
        private List<ImageUploaderModel> _ImageUploaderList;
        private UploadingModel _uploadingModel = new UploadingModel();
        private long TotalFileSize = 0;
        private long TotalUploaded = 0;

        private string coverImageUrl = string.Empty;

        JObject Jobj = new JObject();
        JArray Jarr = new JArray();
        byte[] tmpPostData = null;
        int count;

        public ImageUploader(NewStatusViewModel newStatusViewModel)
        {
            this.newStatusViewModel = newStatusViewModel;
            this._SinglePictureUploadFailed = false;
            _ImageUploaderList = new List<ImageUploaderModel>();
            this.newStatusViewModel.EnablePostbutton(false);
            this._WebClient = new WebClient();
            this._WebClient.UploadProgressChanged += new UploadProgressChangedEventHandler(UploadProgressChanged);
            this._WebClient.UploadDataCompleted += new UploadDataCompletedEventHandler(UploadDataCompleted);
            this._ImageUploaderList.AddRange(newStatusViewModel.NewStatusImageUpload);


            Jobj[JsonKeys.ImageList] = Jarr;
            count = 1;

            string albumName = newStatusViewModel.ImageAlbumTitleTextBoxText.Trim();

            AlbumModel currentAlbum = RingIDViewModel.Instance.MyImageAlubms.Where(x => x.AlbumName == albumName).FirstOrDefault();
            Jobj[JsonKeys.AlbumName] = albumName;
            if (currentAlbum != null)
            {
                Jobj[JsonKeys.AlbumId] = currentAlbum.AlbumId;
            }

            RingIDViewModel.Instance.UploadingModelList.InvokeAdd(_uploadingModel);
            _uploadingModel.UploadingContent = _ImageUploaderList.Count > 1 ? _ImageUploaderList.Count + " images are uploading..." : _ImageUploaderList.Count + " image is uploading...";
            for (int i = 0; i < _ImageUploaderList.Count; i++)
            {
                TotalFileSize += _ImageUploaderList[i].FileSize;
            }
            new Thread(new ThreadStart(BeginPictureUpload)).Start();
        }

        #region "Properties"
        public UCUploadingPopup ucUploadingPopup
        {
            get
            {
                return MainSwitcher.PopupController.ucUploadingPopup;
            }
        }
        #endregion"Properties"

        private int quality = 75;
        private void BeginPictureUpload()
        {
            try
            {
                if (_ImageUploaderList.Count > 0)
                {
                    if (!_SinglePictureUploadFailed)
                    {
                        ImageUploaderModel model = _ImageUploaderList.FirstOrDefault();
                        try
                        {
                            newStatusViewModel.UploadingText = "Uploading " + count + " of " + newStatusViewModel.NewStatusImageUpload.Count;
                            count++;
                            int w = 0, h = 0;
                            System.Drawing.Image image = null;

                            long n;
                            if (!long.TryParse(model.FilePath, out n))
                            {
                                image = ImageUtility.GetOrientedImageFromFilePath(model.FilePath);
                                if (image == null)
                                {
                                    _SinglePictureUploadFailed = true;
                                    _ImageUploaderList.Remove(model);
                                    BeginPictureUpload();
                                }
                            }

                            if (!model.IsFromUpload)
                            {
                                _ImageUploaderList.Remove(model);
                                model.IsUploadedInImageServer = true;
                                ImageUtility.ImageHeightWidth(image, out w, out h);
                                model.Height = h;
                                model.Width = w;
                                JObject singleObj = Models.Utility.HelperMethodsModel.SingleJobject(model.ImageUrl, model.ImageCaption, w, h);
                                Jarr.Add(singleObj);
                                BeginPictureUpload();
                            }
                            else if (!string.IsNullOrEmpty(model.ImageUrl) && model.Height > 0 && model.Width > 0)
                            {
                                _ImageUploaderList.Remove(model);
                                model.IsUploadedInImageServer = true;
                                h = model.Height;
                                w = model.Width;
                                BeginPictureUpload();
                            }
                            else
                            {
                                tmpPostData = null;
                                if (long.TryParse(model.FilePath, out n))
                                {
                                    w = (int)model.CopiedBitmapImage.Width;
                                    h = (int)model.CopiedBitmapImage.Height;
                                    model.Height = h;
                                    model.Width = w;
                                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                                    encoder.Frames.Add(BitmapFrame.Create(model.CopiedBitmapImage));
                                    using (MemoryStream ms = new MemoryStream())
                                    {
                                        encoder.Save(ms);
                                        tmpPostData = ms.ToArray();
                                    }
                                }
                                else
                                {
                                    tmpPostData = ImageUtility.ImageResizeandQuality(image, quality, out w, out h);
                                    model.Height = h;
                                    model.Width = w;
                                    image.Dispose();
                                }

                                //string response = await FeedImagesUpload.Instance.UploadToImageServerWebRequest(tmpPostData, SettingsConstants.TYPE_NORMAL_BOOK_IMAGE, w, h);
                                UploadToImageServer(model, SettingsConstants.TYPE_NORMAL_BOOK_IMAGE);
                            }
                        }
                        catch (Exception e)
                        {
                            MessagContentShow();
                            log.Error("Exception in feedImgupload==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                            newStatusViewModel.EnablePostbutton(true);
                            newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                            HideUploadingPopup();
                        }
                    }
                    else
                    {
                        newStatusViewModel.EnablePostbutton(true);
                        MessagContentShow();
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                        });
                        HideUploadingPopup();
                    }
                }
                else
                {
                    if (!_SinglePictureUploadFailed)
                    {
                        HideUploadingPopup();

                        newStatusViewModel.UploadingText = "";
                        Jobj[JsonKeys.CoverImageUrl] = coverImageUrl;

                        new ThreadAddFeed().StartThread(newStatusViewModel, SettingsConstants.MEDIA_TYPE_IMAGE, Jobj);


                        if (this._WebClient != null)
                        {
                            this._WebClient.Dispose();
                        }
                    }
                }
            }
            catch (Exception)
            {
                newStatusViewModel.EnablePostbutton(true);
                MessagContentShow();
                Application.Current.Dispatcher.Invoke(() =>
                {
                    newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                });
                HideUploadingPopup();
                if (this._WebClient != null)
                {
                    this._WebClient.Dispose();
                }
            }
        }

        private void HideUploadingPopup()
        {
            RingIDViewModel.Instance.UploadingModelList.InvokeRemove(_uploadingModel);
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (ucUploadingPopup != null && RingIDViewModel.Instance.UploadingModelList.Count == 0)
                {
                    ucUploadingPopup.popupUploading.IsOpen = false;
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        private byte[] wholebytes;
        private void UploadToImageServer(ImageUploaderModel model, int imageType)
        {
            try
            {
                string _lineEnd = "\r\n";
                string _twoHyphens = "--";
                string _boundary = "*****";

                _WebClient.Headers["Content-Type"] = "multipart/form-data; boundary=" + _boundary;
                _WebClient.Headers["User-Agent"] = "ringID" + "_" + DefaultSettings.VALUE_APP_INSTALLED_VERSION;
                _WebClient.Headers["access-control-allow-origin"] = "*";

                var firstPart = "";
                string fileheaderTemplate = "Content-Disposition: form-data; name=\"{0}\"" + _lineEnd + _lineEnd + "{1}" + _lineEnd;

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "sId", DefaultSettings.LOGIN_SESSIONID);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "uId", DefaultSettings.LOGIN_RING_ID);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "authServer", ServerAndPortSettings.AUTH_SERVER_IP);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "comPort", ServerAndPortSettings.COMMUNICATION_PORT);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "x-app-version", AppConfig.VERSION_PC);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "iw", model.Width);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "ih", model.Height);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "imT", imageType);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += "Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + "pcupload.jpg" + "\"" + _lineEnd;
                firstPart += _lineEnd;

                byte[] firstBytes = _WebClient.Encoding.GetBytes(firstPart);

                var lastPart = "";
                lastPart += _lineEnd;
                lastPart += _twoHyphens + _boundary + _twoHyphens + _lineEnd;

                byte[] lastBytes = _WebClient.Encoding.GetBytes(lastPart);

                //////////////

                // FileStream oFileStream = new FileStream(model.FilePath, FileMode.Open, FileAccess.Read);
                int fileLength = tmpPostData.Length;

                long length = firstBytes.Length + fileLength + lastBytes.Length;
                wholebytes = new Byte[length];
                int currentbytes = 0;

                /*New Length including header*/
                TotalFileSize -= model.FileSize;
                model.FileSize = length;
                TotalFileSize += model.FileSize;

                Buffer.BlockCopy(firstBytes, 0, wholebytes, currentbytes, firstBytes.Length);
                currentbytes += firstBytes.Length;

                Buffer.BlockCopy(tmpPostData, 0, wholebytes, currentbytes, fileLength);
                currentbytes += fileLength;

                //oFileStream.Read(wholebytes, currentbytes, fileLength);
                //currentbytes += fileLength;

                // oFileStream.Close();
                //
                //byte[] tmp = new Byte[checked((uint)Math.Min(4096, (int)oFileStream.Length))];
                //int bytesRead = 0;
                //while ((bytesRead = oFileStream.Read(tmp, 0, tmp.Length)) != 0)
                //{
                //    Array.Copy(tmp, 0, wholebytes, currentbytes, tmp.Length);
                //    currentbytes += (int)oFileStream.Length;
                //}
                //

                Buffer.BlockCopy(lastBytes, 0, wholebytes, currentbytes, lastBytes.Length);
                currentbytes += lastBytes.Length;


                model.IsUploading = true;
                _WebClient.UploadDataAsync(new Uri(ServerAndPortSettings.GetAlbumImageUploadingURL), "POST", wholebytes, model);
                if (wholebytes != null) GC.SuppressFinalize(wholebytes);
                wholebytes = null;
                /*byte[] fileBytes = HelperMethods.ReadByteArrayFromFile(model.FilePath);
                model.IsUploading = true;
                _WebClient.UploadDataAsync(new Uri(ServerAndPortSettings.GetVideoUploadingURL), "POST", Combine(firstBytes, fileBytes, lastBytes), model);*/
            }
            catch (Exception ex)
            {
                //GC.Collect();
                if (wholebytes != null) GC.SuppressFinalize(wholebytes);
                wholebytes = null;
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
                //if (_WebClient != null) _WebClient.Dispose();
                if (_WebClient != null) GC.SuppressFinalize(_WebClient);
                _WebClient = null;
                _SinglePictureUploadFailed = true;
                model.IsUploading = false;
                newStatusViewModel.EnablePostbutton(true);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                    UIHelperMethods.ShowFailed("Failed to Upload image!", "Upload image");
                });

                //   CustomMessageBox.ShowError("Failed to Upload image!");
                HideUploadingPopup();
            }
        }

        private void UploadDataCompleted(object sender, UploadDataCompletedEventArgs e)
        {
            try
            {
                ImageUploaderModel model = (ImageUploaderModel)e.UserState;
                if (e.Result != null)
                {
                    TotalUploaded += model.FileSize;
                    tmpPostData = null;
                    string response = System.Text.Encoding.UTF8.GetString(e.Result);
                    _ImageUploaderList.Remove(model);

                    if (!string.IsNullOrEmpty(response))
                    {
                        JObject responseObj = JObject.Parse(response);
                        if (responseObj != null && responseObj[JsonKeys.ImageUrl] != null && responseObj[JsonKeys.Success] != null && (bool)responseObj[JsonKeys.Success])
                        {
                            model.IsUploadedInImageServer = true;
                            JObject singleObj = Models.Utility.HelperMethodsModel.SingleJobject((string)responseObj[JsonKeys.ImageUrl], model.ImageCaption, model.Width, model.Height);
                            if (string.IsNullOrEmpty(coverImageUrl))
                            {
                                coverImageUrl = (string)responseObj[JsonKeys.ImageUrl];
                            }
                            Jarr.Add(singleObj);
                            BeginPictureUpload();
                        }
                    }
                }
            }
            catch (Exception)
            {
            }

        }

        private void UploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {
            try
            {
                ImageUploaderModel obj = (ImageUploaderModel)e.UserState;
                obj.Pecentage = (int)(((double)e.BytesSent / (double)e.TotalBytesToSend) * 100);
                _uploadingModel.TotalUploadedInPercentage = (int)(((double)(e.BytesSent + TotalUploaded) / (double)TotalFileSize) * 100);
            }
            catch (Exception)
            {
                _SinglePictureUploadFailed = true;
                newStatusViewModel.EnablePostbutton(true);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                });
            }
        }

        #region "Utility Methods"
        private void MessagContentShow()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (newStatusViewModel.NewStatusImageUpload.Count > 1) UIHelperMethods.ShowFailed("Failed to upload all feed images!", "Image upload!");
                else UIHelperMethods.ShowFailed("Failed to upload feed image!", "Image upload!");
            });
        }

        #endregion "Utility Methods"
    }
}
=======
﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Media.Imaging;
using log4net;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.UI;
using View.UI.Feed;
using View.UI.PopUp;
using View.Utility.Feed;
using View.Utility.Images;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.ViewModel.NewStatus;

namespace View.Utility
{
    public class ImageUploader
    {
        #region Private Variables

        private readonly ILog log = LogManager.GetLogger(typeof(ImageUploader).Name);
        private NewStatusViewModel newStatusViewModel;
        private WebClient webClient = null;
        private bool singlePictureUploadFailed = false;
        private List<ImageUploaderModel> imageUploaderList;
        private UploadingModel uploadingModel = new UploadingModel();
        private long totalFileSize = 0;
        private long totalUploaded = 0;
        private string coverImageUrl = string.Empty;
        private JObject jObject = new JObject();
        private JArray jArray = new JArray();
        private byte[] temporaryPostData = null;
        private int count;
        private int quality = 75;
        private byte[] wholebytes;

        #endregion

        #region Constructor

        public ImageUploader(NewStatusViewModel newStatusViewModel)
        {
            this.newStatusViewModel = newStatusViewModel;
            this.singlePictureUploadFailed = false;
            this.imageUploaderList = new List<ImageUploaderModel>();
            this.newStatusViewModel.EnablePostbutton(false);
            this.webClient = new WebClient();
            this.webClient.UploadProgressChanged += new UploadProgressChangedEventHandler(UploadProgressChanged);
            this.webClient.UploadDataCompleted += new UploadDataCompletedEventHandler(UploadDataCompleted);
            this.imageUploaderList.AddRange(newStatusViewModel.NewStatusImageUpload);
            jObject[JsonKeys.ImageList] = jArray;
            count = 1;
            string albumName = newStatusViewModel.ImageAlbumTitleTextBoxText.Trim();
            AlbumModel currentAlbum = RingIDViewModel.Instance.MyImageAlubms.Where(x => x.AlbumName == albumName).FirstOrDefault();
            jObject[JsonKeys.AlbumName] = albumName;
            if (currentAlbum != null)
            {
                jObject[JsonKeys.AlbumId] = currentAlbum.AlbumId;
            }
            RingIDViewModel.Instance.UploadingModelList.InvokeAdd(uploadingModel);
            uploadingModel.UploadingContent = imageUploaderList.Count > 1 ? imageUploaderList.Count + " images are uploading..." : imageUploaderList.Count + " image is uploading...";
            for (int i = 0; i < imageUploaderList.Count; i++)
            {
                totalFileSize += imageUploaderList[i].FileSize;
            }
            new Thread(new ThreadStart(BeginPictureUpload)).Start();
        }

        #endregion

        #region Properties

        public UCUploadingPopup ucUploadingPopup
        {
            get
            {
                return MainSwitcher.PopupController.ucUploadingPopup;
            }
        }

        #endregion

        #region Private Methods

        private void BeginPictureUpload()
        {
            try
            {
                if (imageUploaderList.Count > 0)
                {
                    if (!singlePictureUploadFailed)
                    {
                        ImageUploaderModel model = imageUploaderList.FirstOrDefault();
                        try
                        {
                            newStatusViewModel.UploadingText = "Uploading " + count + " of " + newStatusViewModel.NewStatusImageUpload.Count;
                            count++;
                            int width = 0, height = 0;
                            System.Drawing.Image image = null;

                            long n;
                            if (!long.TryParse(model.FilePath, out n))
                            {
                                image = ImageUtility.GetOrientedImageFromFilePath(model.FilePath);
                                if (image == null)
                                {
                                    singlePictureUploadFailed = true;
                                    imageUploaderList.Remove(model);
                                    BeginPictureUpload();
                                }
                            }

                            if (!model.IsFromUpload)
                            {
                                imageUploaderList.Remove(model);
                                model.IsUploadedInImageServer = true;
                                ImageUtility.ImageHeightWidth(image, out width, out height);
                                model.Height = height;
                                model.Width = width;
                                JObject singleObj = Models.Utility.HelperMethodsModel.SingleJobject(model.ImageUrl, model.ImageCaption, width, height);
                                jArray.Add(singleObj);
                                BeginPictureUpload();
                            }
                            else if (!string.IsNullOrEmpty(model.ImageUrl) && model.Height > 0 && model.Width > 0)
                            {
                                imageUploaderList.Remove(model);
                                model.IsUploadedInImageServer = true;
                                height = model.Height;
                                width = model.Width;
                                BeginPictureUpload();
                            }
                            else
                            {
                                temporaryPostData = null;
                                if (long.TryParse(model.FilePath, out n))
                                {
                                    width = (int)model.CopiedBitmapImage.Width;
                                    height = (int)model.CopiedBitmapImage.Height;
                                    model.Height = height;
                                    model.Width = width;
                                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                                    encoder.Frames.Add(BitmapFrame.Create(model.CopiedBitmapImage));
                                    using (MemoryStream ms = new MemoryStream())
                                    {
                                        encoder.Save(ms);
                                        temporaryPostData = ms.ToArray();
                                    }
                                }
                                else
                                {
                                    temporaryPostData = ImageUtility.ImageResizeandQuality(image, quality, out width, out height);
                                    model.Height = height;
                                    model.Width = width;
                                    image.Dispose();
                                }
                                UploadToImageServer(model, SettingsConstants.TYPE_NORMAL_BOOK_IMAGE);
                            }
                        }
                        catch (Exception e)
                        {
                            MessagContentShow();
                            log.Error("Exception in feedImgupload==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message);
                            newStatusViewModel.EnablePostbutton(true);
                            newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                            HideUploadingPopup();
                        }
                    }
                    else
                    {
                        newStatusViewModel.EnablePostbutton(true);
                        MessagContentShow();
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                        });
                        HideUploadingPopup();
                    }
                }
                else
                {
                    if (!singlePictureUploadFailed)
                    {
                        HideUploadingPopup();
                        newStatusViewModel.UploadingText = "";
                        jObject[JsonKeys.CoverImageUrl] = coverImageUrl;
                        new ThreadAddFeed().StartThread(newStatusViewModel, SettingsConstants.MEDIA_TYPE_IMAGE, jObject);
                        if (this.webClient != null)
                        {
                            this.webClient.Dispose();
                        }
                    }
                }
            }
            catch (Exception)
            {
                newStatusViewModel.EnablePostbutton(true);
                MessagContentShow();
                Application.Current.Dispatcher.Invoke(() =>
                {
                    newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                });
                HideUploadingPopup();
                if (this.webClient != null)
                {
                    this.webClient.Dispose();
                }
            }
        }

        private void HideUploadingPopup()
        {
            RingIDViewModel.Instance.UploadingModelList.InvokeRemove(uploadingModel);
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (ucUploadingPopup != null && RingIDViewModel.Instance.UploadingModelList.Count == 0)
                {
                    ucUploadingPopup.popupUploading.IsOpen = false;
                }
            }, System.Windows.Threading.DispatcherPriority.Send);
        }

        private void UploadToImageServer(ImageUploaderModel model, int imageType)
        {
            try
            {
                string _lineEnd = "\r\n";
                string _twoHyphens = "--";
                string _boundary = "*****";

                webClient.Headers["Content-Type"] = "multipart/form-data; boundary=" + _boundary;
                webClient.Headers["User-Agent"] = "ringID" + "_" + DefaultSettings.VALUE_APP_INSTALLED_VERSION;
                webClient.Headers["access-control-allow-origin"] = "*";

                var firstPart = "";
                string fileheaderTemplate = "Content-Disposition: form-data; name=\"{0}\"" + _lineEnd + _lineEnd + "{1}" + _lineEnd;

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "sId", DefaultSettings.LOGIN_SESSIONID);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "uId", DefaultSettings.LOGIN_RING_ID);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "authServer", ServerAndPortSettings.AUTH_SERVER_IP);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "comPort", ServerAndPortSettings.COMMUNICATION_PORT);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "x-app-version", AppConfig.VERSION_PC);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "iw", model.Width);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "ih", model.Height);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += string.Format(fileheaderTemplate, "imT", imageType);

                firstPart += _twoHyphens + _boundary + _lineEnd;
                firstPart += "Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + "pcupload.jpg" + "\"" + _lineEnd;
                firstPart += _lineEnd;

                byte[] firstBytes = webClient.Encoding.GetBytes(firstPart);

                var lastPart = "";
                lastPart += _lineEnd;
                lastPart += _twoHyphens + _boundary + _twoHyphens + _lineEnd;

                byte[] lastBytes = webClient.Encoding.GetBytes(lastPart);

                int fileLength = temporaryPostData.Length;

                long length = firstBytes.Length + fileLength + lastBytes.Length;
                wholebytes = new Byte[length];
                int currentbytes = 0;

                /*New Length including header*/
                totalFileSize -= model.FileSize;
                model.FileSize = length;
                totalFileSize += model.FileSize;

                Buffer.BlockCopy(firstBytes, 0, wholebytes, currentbytes, firstBytes.Length);
                currentbytes += firstBytes.Length;

                Buffer.BlockCopy(temporaryPostData, 0, wholebytes, currentbytes, fileLength);
                currentbytes += fileLength;

                Buffer.BlockCopy(lastBytes, 0, wholebytes, currentbytes, lastBytes.Length);
                currentbytes += lastBytes.Length;

                model.IsUploading = true;
                webClient.UploadDataAsync(new Uri(ServerAndPortSettings.GetAlbumImageUploadingURL), "POST", wholebytes, model);
                if (wholebytes != null) GC.SuppressFinalize(wholebytes);
                wholebytes = null;
            }
            catch (Exception ex)
            {
                log.Error("Image Uploader () => Error Message : " + ex.Message + "\n Stack Trace : " + ex.StackTrace);
                if (wholebytes != null)
                {
                    GC.SuppressFinalize(wholebytes);
                }
                wholebytes = null;
                if (webClient != null)
                {
                    GC.SuppressFinalize(webClient);
                }
                webClient = null;
                singlePictureUploadFailed = true;
                model.IsUploading = false;
                newStatusViewModel.EnablePostbutton(true);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                    UIHelperMethods.ShowFailed("Failed to Upload image!", "Upload image");
                });
                HideUploadingPopup();
            }
        }

        private void UploadDataCompleted(object sender, UploadDataCompletedEventArgs e)
        {
            try
            {
                ImageUploaderModel model = (ImageUploaderModel)e.UserState;
                if (e.Result != null)
                {
                    totalUploaded += model.FileSize;
                    temporaryPostData = null;
                    string response = System.Text.Encoding.UTF8.GetString(e.Result);
                    imageUploaderList.Remove(model);

                    if (!string.IsNullOrEmpty(response))
                    {
                        JObject responseObj = JObject.Parse(response);
                        if (responseObj != null && responseObj[JsonKeys.ImageUrl] != null && responseObj[JsonKeys.Success] != null && (bool)responseObj[JsonKeys.Success])
                        {
                            model.IsUploadedInImageServer = true;
                            JObject singleObject = Models.Utility.HelperMethodsModel.SingleJobject((string)responseObj[JsonKeys.ImageUrl], model.ImageCaption, model.Width, model.Height);
                            if (string.IsNullOrEmpty(coverImageUrl))
                            {
                                coverImageUrl = (string)responseObj[JsonKeys.ImageUrl];
                            }
                            jArray.Add(singleObject);
                            BeginPictureUpload();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (DefaultSettings.IsInternetAvailable)
                    {
                        UIHelperMethods.ShowFailed(NotificationMessages.INTERNET_UNAVAILABLE, "Post failed!");
                    }
                });
                newStatusViewModel.EnablePostbutton(true);
                HideUploadingPopup();
                log.Error("UploadDataCompleted() => Error Message : " + ex.Message + "Stack Trace : " + ex.StackTrace);
            }
        }

        private void UploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {
            try
            {
                ImageUploaderModel obj = (ImageUploaderModel)e.UserState;
                obj.Pecentage = (int)(((double)e.BytesSent / (double)e.TotalBytesToSend) * 100);
                uploadingModel.TotalUploadedInPercentage = (int)(((double)(e.BytesSent + totalUploaded) / (double)totalFileSize) * 100);
            }
            catch (Exception)
            {
                singlePictureUploadFailed = true;
                newStatusViewModel.EnablePostbutton(true);
                HideUploadingPopup();
                Application.Current.Dispatcher.Invoke(() =>
                {
                    newStatusViewModel.PreviewCancelButtonVisibility = Visibility.Visible;
                });
            }
        }

        #endregion

        #region Utility Methods

        private void MessagContentShow()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (newStatusViewModel.NewStatusImageUpload.Count > 1)
                {
                    UIHelperMethods.ShowFailed("Failed to upload all feed images!", "Image upload!");
                }
                else
                {
                    UIHelperMethods.ShowFailed("Failed to upload feed image!", "Image upload!");
                }
            });
        }

        #endregion
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

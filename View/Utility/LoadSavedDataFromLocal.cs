﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.Constants;
using View.Utility.audio;
using View.Utility.Channel;
using View.Utility.Chat;
using View.Utility.Circle;
using View.Utility.DataContainer;
using View.Utility.FriendList;
using View.Utility.Stream;
using View.ViewModel;

namespace View.Utility
{
    class LoadSavedDataFromLocal
    {
        #region "Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(LoadSavedDataFromLocal).Name);
        private CreateDBandTables createDBandTables;
        DatabaseActivityDAO databaseActivityDAO;
        #endregion"Fields"

        #region "Ctors"
        public LoadSavedDataFromLocal()
        {
            databaseActivityDAO = new DatabaseActivityDAO();
            createDBandTables = databaseActivityDAO.createDBandTables;
            // createDBandTables = new CreateDBandTables();
        }
        #endregion"Ctors"

        #region "Methods"
        private void startAuthServerCommunications()
        {
            #region Auth Socket Initialize
            HelperMethodsAuth.StartAuthAsyncCommunication(MainSwitcher.AuthSignalHandler());
            #endregion Auth Socket Initialize
        }

        public bool CreateAutoStartInfoFile()
        {
            try
            {
                string path = System.IO.Path.Combine(RingIDSettings.APP_FOLDER, "AutoStartInfo.bat");
                if (!File.Exists(path))
                {
                    using (StreamWriter sw = File.AppendText(path))
                    {
                        sw.WriteLine("1"); //default autostartvalue
                        sw.Close();
                    }
                    return true;
                }
            }
            catch (Exception ex) { log.Error("Error: CreateAutoStartInfoFile() => " + ex.StackTrace); }
            return false;
        }

        public void CreateFolders()
        {
            try
            {
                HelperMethods.CreateDirectory(RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER);
                HelperMethods.CreateDirectory(RingIDSettings.TEMP_COVER_IMAGE_FOLDER);
                HelperMethods.CreateDirectory(RingIDSettings.TEMP_BOOK_IMAGE_FOLDER);
                HelperMethods.CreateDirectory(RingIDSettings.TEMP_IM_SHARED_FILES_FOLDER);
                HelperMethods.CreateDirectory(RingIDSettings.TEMP_CHAT_BACKGROUND_FOLDER);
                HelperMethods.CreateDirectory(RingIDSettings.TEMP_CHAT_FILES_FOLDER);
                HelperMethods.CreateDirectory(RingIDSettings.TEMP_CHAT_FILES_TRANSFER_FOLDER);
                HelperMethods.CreateDirectory(RingIDSettings.TEMP_CAM_IMAGE_FOLDER);
                HelperMethods.CreateDirectory(RingIDSettings.TEMP_DOING_FOLDER);
                HelperMethods.CreateDirectory(RingIDSettings.TEMP_DOING_MINI_FOLDER);
                HelperMethods.CreateDirectory(RingIDSettings.TEMP_DOING_FULL_FOLDER);
                HelperMethods.CreateDirectory(RingIDSettings.TEMP_MEDIA_IMAGES_FOLDER);
                HelperMethods.CreateDirectory(RingIDSettings.TEMP_NEWSPORTAL_IMAGES_FOLDER);
                HelperMethods.CreateDirectory(RingIDSettings.TEMP_PAGES_IMAGES_FOLDER);
                HelperMethods.CreateDirectory(RingIDSettings.TEMP_CELEBRITY_IMAGES_FOLDER);
                HelperMethods.CreateDirectory(RingIDSettings.TEMP_MEDIAPAGE_IMAGES_FOLDER);
                HelperMethods.CreateDirectory(RingIDSettings.TEMP_MAP_FOLDER);
                HelperMethods.CreateDirectory(RingIDSettings.TEMP_DOWNLOADED_MEDIA_FOLDER);
                HelperMethods.CreateDirectory(RingIDSettings.STREAM_GIFT_FOLDER);
            }
            catch (Exception) { }
        }

        public void DoDatabaseFunctionality()
        {
            try
            {
                createDBandTables.CreateNewDatabase(RingIDSettings.APP_FOLDER + System.IO.Path.DirectorySeparatorChar);
                createDBandTables.ConnectToDatabase();
                createDBandTables.CreateRingLoginSettingTable();
                createDBandTables.CreateOthersSettingTable();
                databaseActivityDAO.FetchRingSignInSettingsBeforeLogin();
                databaseActivityDAO.UpdateTableAndPrevData();
                createDBandTables.CreateAllTables();
                createDBandTables.InsertDefaultData(RingIDSettings.SCRIPT_EMOTICON, RingIDSettings.SCRIPT_INSTANCE_MESSAGE, RingIDSettings.SCRIPT_EVENT);
                databaseActivityDAO.FetchDateEventList();
                databaseActivityDAO.FetchEmoticonList();
            }
            catch (Exception esx) { log.Error("doDatabaseFunctionality ==> Can not create database or fetch initial data ==> " + esx.StackTrace); }
        }

        public void InitObjects()
        {
            RingIDViewModel.Instance.Reset();
            if (FriendDictionaries.Instance == null)
            {
                FriendDictionaries.Instance = new FriendDictionaries();
                FriendDictionaries.Instance.SetFriendDictionaries();
            }
        }

        public void LoadCircleList(List<CircleDTO> circleList)
        {
            foreach (CircleDTO circleDTO in circleList)
            {
                CircleModel circleModel = new CircleModel();
                circleModel.LoadData(circleDTO);
                CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY[circleDTO.CircleId] = circleModel;
            }
        }

        #endregion "Methods"

        #region"Public Methods"
        public void CreateSomeStaticObjects()
        {
            if (RingIDViewModel.Instance == null) RingIDViewModel.Instance = new RingIDViewModel();
            if (ChatViewModel.Instance == null) ChatViewModel.Instance = new ChatViewModel();
            if (StreamViewModel.Instance == null) StreamViewModel.Instance = new StreamViewModel();
            if (ChannelViewModel.Instance == null) ChannelViewModel.Instance = new ChannelViewModel();
        }
        /// <summary>
        /// if this method return true then need to show application main window 
        /// or need to show login screen
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public bool LoadAutoLoginInfoFromLocal(string[] args)
        {
            try
            {
                if (!Directory.Exists(RingIDSettings.APP_FOLDER)) Directory.CreateDirectory(RingIDSettings.APP_FOLDER);
                CreateSomeStaticObjects();
                CreateAutoStartInfoFile();
                AudioFilesAndSettings.initPlayers();
                CreateFolders();
                DoDatabaseFunctionality();
                ImageObjects.InitEmoticonIcon();
                startAuthServerCommunications();
                if ((args != null && args.Length > 0 && args[0].Equals(StartUpConstatns.ARGUMENT_RESTART)) == false)
                    if (DefaultSettings.VALUE_LOGIN_AUTO_SIGNIN == 1
                       && DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD == 1
                       && !string.IsNullOrEmpty(DefaultSettings.VALUE_LOGIN_USER_NAME)
                       && !string.IsNullOrEmpty(DefaultSettings.VALUE_LOGIN_USER_PASSWORD)) return true;// need to load guiringid
            }
            catch (Exception ex) { log.Error(ex.StackTrace); }
            finally { }
            return false;
        }

        public List<UserBasicInfoDTO> FetchAllCotactsFromDB()
        {
            databaseActivityDAO = new DatabaseActivityDAO();
            databaseActivityDAO.CreateContactListTable();
            databaseActivityDAO.FetchContactList();
            return FriendDictionaries.Instance.GetUserBasicInfoDictionaryAsList();
        }

        public List<UserBasicInfoDTO> CurrentUserContacts()
        {
            return FriendDictionaries.Instance.GetUserBasicInfoDictionaryAsList().Where(user => user.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED
                || user.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_INCOMING
                || user.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_PENDING).ToList();
        }

        private void AddAContactInMainDictionary(UserBasicInfoDTO userDTO)
        {
            try
            {
                UserBasicInfoModel model = new UserBasicInfoModel(userDTO);
                if (userDTO.ContactType == SettingsConstants.SPECIAL_CONTACT && userDTO.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED)
                {
                    System.Threading.Thread.Sleep(10);
                    AddRemoveInCollections.AddIntoSpecialFriendList(model);
                }
                if (userDTO.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_INCOMING)
                {
                    FriendListController.Instance.FriendDataContainer.AddIntoIncomingList(userDTO);
                    model.FriendListType = AddRemoveInCollections.Dictonary_IncomingRequestFriendList;
                    if (model.IncomingStatus == StatusConstants.INCOMING_REQUEST_UNREAD) AppConstants.ADD_FRIEND_NOTIFICATION_COUNT++;
                }
                else if (userDTO.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_PENDING)
                {
                    FriendListController.Instance.FriendDataContainer.AddIntoPendingList(userDTO);
                    model.FriendListType = AddRemoveInCollections.Dictonary_OutgoingRequestFriendList;
                }
                else if (userDTO.FriendShipStatus == 0) model.FriendListType = AddRemoveInCollections.Dictonary_UnknownFriendList;
                FriendListController.Instance.FriendDataContainer.AddIntoUserBasicInfoModels(model);
            }
            finally { }
        }

        public void MakeFriendsModelsFromDTO()
        {
            List<UserBasicInfoDTO> allUserBasicInofDtos = FriendDictionaries.Instance.GetUserBasicInfoDictionaryAsList();
            foreach (UserBasicInfoDTO userDTO in allUserBasicInofDtos)
            {
                if (userDTO.UserTableID > 0) AddAContactInMainDictionary(userDTO);
            }
        }

        public void MakeSeparateContactsDTOLists()
        {
            List<UserBasicInfoDTO> myFriendsUserInfo = CurrentUserContacts();
            List<UserBasicInfoDTO> RecentlyAddedFrndLits = null;
            List<UserBasicInfoDTO> favoriteList = null;
            List<UserBasicInfoDTO> topList = null;
            try
            {
                long unreadUserContactTime = Models.Utility.ModelUtility.CurrentTimeMillis() - Models.Utility.ModelUtility.MilliSecondsInSevenDay;
                long readUserContactTime = Models.Utility.ModelUtility.CurrentTimeMillis() - Models.Utility.ModelUtility.MilliSecondsInOneDay;
                RecentlyAddedFrndLits = (from l in myFriendsUserInfo
                                         where l.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED
                                         where l.ContactType != SettingsConstants.SPECIAL_CONTACT
                                         //where l.ContactAddedTime > (Models.Utility.ModelUtility.CurrentTimeMillisLocal() - Models.Utility.ModelUtility.MilliSecondsInSevenDay)
                                         where (l.ContactAddedReadUnread == SettingsConstants.RECENT_FRND_UNREAD && l.ContactAddedTime > unreadUserContactTime)
                                         || (l.ContactAddedReadUnread == SettingsConstants.RECENT_FRND_READ && l.ContactAddedTime > readUserContactTime)
                                         orderby l.ContactAddedTime descending
                                         select l).Take(10).ToList() ?? new List<UserBasicInfoDTO>();
            }
            catch (Exception ex) { log.Error("Error in RecentlyAddedFrndLits LoadShowMoreData ==> " + ex.StackTrace); }

            try
            {
                favoriteList = (from l in myFriendsUserInfo
                                where l.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED
                                where l.ContactType != SettingsConstants.SPECIAL_CONTACT
                                where l.FavoriteRank >= SettingsConstants.FORCE_FAVOURITE_VALUE
                                orderby l.FullName ascending
                                select l).ToList() ?? new List<UserBasicInfoDTO>();
                favoriteList.ForEach(P => P.IsFavouriteTemp = true);
            }
            catch (Exception ex) { log.Error("Error in favoriteList LoadShowMoreData ==> " + ex.StackTrace); }

            try
            {
                topList = (from l in myFriendsUserInfo
                           where l.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED
                           where l.ContactType != SettingsConstants.SPECIAL_CONTACT
                           where l.FavoriteRank < SettingsConstants.FORCE_FAVOURITE_VALUE
                           where (l.NumberOfCalls > 0 || l.NumberOfChats > 0)
                           orderby l.FavoriteRank descending, l.FullName ascending
                           select l).Take(10).ToList() ?? new List<UserBasicInfoDTO>();
                topList.ForEach(P => P.IsTopTemp = true);
            }
            catch (Exception ex) { log.Error("Error in topList LoadShowMoreData ==> " + ex.StackTrace); }

            try
            {
                FriendListController.Instance.FriendDataContainer.NonFavoriteList = (from l in myFriendsUserInfo
                                                                                     where l.FriendShipStatus == StatusConstants.FRIENDSHIP_STATUS_ACCEPTED
                                                                                     where !topList.Any(P => P.UserTableID == l.UserTableID)
                                                                                     where l.ContactType != SettingsConstants.SPECIAL_CONTACT
                                                                                     where !favoriteList.Any(P => P.UserTableID == l.UserTableID)
                                                                                     orderby l.FullName ascending
                                                                                     select l).ToList() ?? new List<UserBasicInfoDTO>();
            }
            catch (Exception ex) { log.Error("Error in nonFavoriteList LoadShowMoreData ==> " + ex.StackTrace); }

            #region "Add Friend Models in different Obserbable collections"
            if (RecentlyAddedFrndLits.Count > 0)
            {
                try
                {
                    foreach (UserBasicInfoDTO userDTO in RecentlyAddedFrndLits)
                    {
                        UserBasicInfoModel userbasicinforModel = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(userDTO.UserTableID);
                        System.Threading.Thread.Sleep(10);
                        AddRemoveInCollections.AddIntoRecentlyAddedFriendList(userbasicinforModel);
                    }
                    RingIDViewModel.Instance.RecentlyAddedFriendList = new ObservableCollection<UserBasicInfoModel>(RingIDViewModel.Instance.RecentlyAddedFriendList.Distinct());
                }
                catch (Exception ex) { log.Error("Error in RecentlyAddedFrndLits  ==> " + ex.StackTrace); }
            }
            if (topList.Count > 0)
            {
                try
                {
                    foreach (UserBasicInfoDTO userDTO in topList)
                    {
                        UserBasicInfoModel userbasicinforModel = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(userDTO.UserTableID);
                        userbasicinforModel.FriendListType = AddRemoveInCollections.Dictonary_TopFriendList;
                        AddRemoveInCollections.AddIntoTopFriendList(userbasicinforModel);
                    }
                    RingIDViewModel.Instance.TopFriendList = new ObservableCollection<UserBasicInfoModel>(RingIDViewModel.Instance.TopFriendList.Distinct());
                }
                catch (Exception ex) { log.Error("Error in topList  ==> " + ex.StackTrace); }
            }

            if (favoriteList.Count > 0)
            {
                try
                {
                    foreach (UserBasicInfoDTO userDTO in favoriteList)
                    {
                        // System.Threading.Thread.Sleep(5);
                        UserBasicInfoModel userbasicinforModel = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(userDTO.UserTableID);
                        userbasicinforModel.FriendListType = AddRemoveInCollections.Dictonary_FavouriteFriendList;
                        AddRemoveInCollections.AddIntoFavouriteFriendList(userbasicinforModel);
                    }
                }
                catch (Exception ex) { log.Error("Error in favoriteList  ==> " + ex.StackTrace); }
            }

            try
            {
                if ((topList.Count + favoriteList.Count) < 100)
                {
                    foreach (UserBasicInfoDTO userDTO in FriendListController.Instance.FriendDataContainer.NonFavoriteList)
                    {
                        if ((RingIDViewModel.Instance.NonFavouriteFriendList.Count + topList.Count + favoriteList.Count) >= 100) break;
                        System.Threading.Thread.Sleep(10);
                        UserBasicInfoModel userbasicinforModel = FriendListController.Instance.FriendDataContainer.GetFromUserBasicInfoModels(userDTO.UserTableID);
                        if (userbasicinforModel != null)
                        {
                            userbasicinforModel.FriendListType = AddRemoveInCollections.Dictonary_NonFavouriteFriendList;
                            AddRemoveInCollections.AddIntoNonFavouriteFriendList(userbasicinforModel);
                        }
                        else FriendListController.Instance.FriendDataContainer.NonFavoriteList.Remove(userDTO);
                    }
                }
            }
            catch (Exception ex) { log.Error("Dictonary_NonFavouriteFriendList==>" + ex.StackTrace); }
            #endregion //"Add Models in different Obserbable collections"
        }

        public void FetchOtherSavedInfo()
        {
            try
            {
                databaseActivityDAO.CreateGroupListTable();
                databaseActivityDAO.CreateGroupMemberListTable();
                databaseActivityDAO.CreateRoomListTable();
                databaseActivityDAO.CreateFriendChatTable();
                databaseActivityDAO.CreateGroupChatTable();
                databaseActivityDAO.CreateRoomChatTable();
                databaseActivityDAO.CreateCallLogTable();
                databaseActivityDAO.CreateActivityTable();
                databaseActivityDAO.FetchRingAllUsersSettings();
                HelperMethods.FetchBlockedNonFriendList();
                VMCircle.Instance.LoadCircleList(databaseActivityDAO.FetchAllCircles());
                databaseActivityDAO.FetchDoingTable();
                DataTable recentMediaData = MediaDAO.Instance.FetchRecentMediaList();
                if (recentMediaData != null && recentMediaData.Rows != null && recentMediaData.Rows.Count > 0)
                    MediaDataContainer.Instance.LoadRecentOrDownloadedMedias(recentMediaData, true);
                DataTable downloadedMediaData = MediaDAO.Instance.FetchDownloadMediaList();
                if (downloadedMediaData != null && downloadedMediaData.Rows != null && downloadedMediaData.Rows.Count > 0)
                    MediaDataContainer.Instance.LoadRecentOrDownloadedMedias(downloadedMediaData, false);
                NotificationHistoryDAO.Instance.LoadMaxTimeFromNotificationHistory();
                NotificationHistoryDAO.Instance.LoadNotificationFromDB(AppConstants.NOTIFICATION_MAX_UT + 1, true);
                CountHistoryDAO.Instance.LoadCountValuesOfNotifications();
                View.ViewModel.RingIDViewModel.Instance.AllNotificationCounter = AppConstants.ALL_NOTIFICATION_COUNT;
                HelperMethods.FetchRoomList();
                HelperMethods.FetchStickerList();
                HelperMethods.FetchChatBackgroundList();
                databaseActivityDAO.CreateRingHiddenPostsTable();
                databaseActivityDAO.CreateRingHiddenPostsUserTable();
                databaseActivityDAO.FetchHiddenPosts();
                databaseActivityDAO.FetchHiddenPostsFromUsers();
                ChatDictionaries.Instance.TEMP_GROUP_LIST.AddRange(GroupInfoDAO.GetAllGroupInfo());
                HelperMethods.BuildGroupList();
                HelperMethods.BuildUnreadChatCallList();
                Utility.Wallet.WalletViewModel.Instance.FetchInitialDwellTimeData();
            }
            catch (Exception ex) { log.Error("FetchOtherSavedInfo==>" + ex.StackTrace); }
            finally { }
        }
        #endregion"Public Methods"
    }
}

﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using View.BindingModels;
using View.UI;
using View.Utility.Feed;
using View.ViewModel;
using System.Linq;

namespace View.Utility
{
    public class FirstTimeFeedLoadWorker : BackgroundWorker
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(FirstTimeFeedLoadWorker).Name);
        public static FirstTimeFeedLoadWorker Instance = new FirstTimeFeedLoadWorker();
        private CustomObservableCollection List;
        private ConcurrentQueue<FeedModel> Queue = new ConcurrentQueue<FeedModel>();

        public FirstTimeFeedLoadWorker()
        {
            //List = RingIDViewModel.Instance.NewsFeeds;
            WorkerSupportsCancellation = true;
            WorkerReportsProgress = true;
            DoWork += FeedDuplicate_DowWork;
        }

        private void FeedDuplicate_DowWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                FeedModel tmp;
                while (Queue.TryDequeue(out tmp))
                {
                    //if (List.Type > DefaultSettings.FEED_TYPE_ALL)
                    //{
                    //    if (List.Type > 18 && List.Type < 25)
                    //        List.InsertSavedFeedModel(tmp);
                    //    else
                    //        List.InsertModel(tmp);
                    //}
                    //else
                    //{
                    switch (tmp.FeedType)
                    {
                        case 2:
                            if (!DefaultSettings.STORAGE_FEEDS_REMOVED)
                            {
                                List.RemoveAllModels();
                                DefaultSettings.STORAGE_FEEDS_REMOVED = true;
                            }
                            //if (UCMiddlePanelSwitcher.View_UCAllFeeds != null && !RingIDViewModel.Instance.NewsFeeds.Any(P => P.NewsfeedId == tmp.NewsfeedId))
                            //    UCMiddlePanelSwitcher.View_UCAllFeeds.InsertModel(tmp);
                            break;
                        case -1:
                            Application.Current.Dispatcher.Invoke((Action)delegate
                            {
                                List.Insert(List.Count - 1, tmp);
                            }, DispatcherPriority.Send);
                            break;
                    }
                    //}
                    if (CancellationPending) return;
                    //while (Queue.IsEmpty) Thread.Sleep(10);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
        }
        public void LoadFirstTimeFeed(CustomObservableCollection list, FeedModel model)
        {
            try
            {
                //if (List != null && List.Type == list.Type)
                //{
                Queue.Enqueue(model);
                if (!IsBusy) RunWorkerAsync();
                //}
                //else
                //{
                //    Queue.Enqueue(model);
                //    if (!IsBusy) RunWorkerAsync();
                //}
                //if (IsBusy)
                //{
                //    if (List != null && List.Type == list.Type)
                //    {
                //        Queue.Enqueue(model);
                //    }
                //    else
                //    {
                //        CancelAsync();

                //        for (int idx = 0; idx < 500 && IsBusy; idx += 10)
                //        {
                //            Thread.Sleep(10);
                //        }

                //        FeedModel tmp;
                //        this.List = list;
                //        while (!Queue.IsEmpty) Queue.TryDequeue(out tmp);
                //        Queue.Enqueue(model);
                //        RunWorkerAsync();
                //    }
                //}
                //else
                //{
                //    this.List = list;
                //    FeedModel tmp;
                //    while (!Queue.IsEmpty) Queue.TryDequeue(out tmp);
                //    Queue.Enqueue(model);
                //    if (!IsBusy)
                //    {
                //        CancelAsync();
                //        RunWorkerAsync();
                //    }
                //}
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace + ex.Message);
            }
        }
    }
}

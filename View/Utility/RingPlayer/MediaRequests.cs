﻿using System;
using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Utility.DataContainer;

namespace View.Utility.RingPlayer
{
    public class MediaRequests
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(MediaRequests).Name);
        #endregion "Private Fields"

        #region "Ctors"
        public MediaRequests()
        {

        }
        #endregion

        #region "Public Methods"
        public bool DetailsOfMedia(Guid contentId, long utId, Guid channelId)
        {
            bool value = false;
            try
            {
                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.ContentId] = contentId;
                pakToSend[JsonKeys.UserTableID] = utId;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_MEDIA_CONTENT_DETAILS;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                if (channelId != Guid.Empty) 
                {
                    pakToSend[JsonKeys.ChannelID] = channelId;
                }
                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId, true);
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success] && feedbackfields[JsonKeys.MediaContentDTO] != null)
                    {
                        if (feedbackfields[JsonKeys.ReasonCode] != null && ReasonCodeConstants.REASON_CODE_NOT_FOUND == (long)feedbackfields[JsonKeys.ReasonCode])
                        {
                            //MainSwitcher.AuthSignalHandler().ShowErrorMessageBox(ReasonCodeConstants.REASON_MSG_NOT_FOUND, "");
                        }
                        else
                        {
                            value = true;
                            JObject mediaContent = (JObject)feedbackfields[JsonKeys.MediaContentDTO];
                            SingleMediaModel model = null;
                            if (!MediaDataContainer.Instance.ContentModels.TryGetValue(contentId, out model))
                            {
                                model = new SingleMediaModel();
                                MediaDataContainer.Instance.ContentModels[contentId] = model;
                            }
                            model.LoadData(mediaContent, model.MediaType, model.PlayedFromFeed);
                            model.MediaOwner.UserTableID = utId;
                            if (model.LikeCommentShare == null)
                            {
                                if (model.NewsFeedId != Guid.Empty)
                                {
                                    FeedModel fm = null;
                                    if (!FeedDataContainer.Instance.FeedModels.TryGetValue(model.NewsFeedId, out fm) && fm.SingleMediaFeedModel != null && fm.LikeCommentShare != null)
                                    {
                                        model.LikeCommentShare = fm.LikeCommentShare;
                                    }
                                }
                                if (model.LikeCommentShare == null) model.LikeCommentShare = new LikeCommentShareModel();
                            }
                            model.LikeCommentShare.LoadData(mediaContent);
                        }
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    //MainSwitcher.AuthSignalHandler().feedSignalHandler.LoadCommentUI();
                }
            }
            finally
            {

            }
            return value;
        }

        public bool CommentReqeust(Guid newsFeedId, Guid contentId, Guid pivotId, long time, int scroll)
        {
            bool value = false;
            try
            {
                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                if (scroll > 0)
                {
                    pakToSend[JsonKeys.Scroll] = scroll;
                    pakToSend[JsonKeys.Time] = time;
                }
                if (pivotId != Guid.Empty)
                    pakToSend[JsonKeys.PivotUUID] = pivotId;
                if (newsFeedId != Guid.Empty)
                    pakToSend[JsonKeys.NewsfeedId] = newsFeedId;
                if (contentId != Guid.Empty)
                {
                    pakToSend[JsonKeys.ContentId] = contentId;
                    pakToSend[JsonKeys.Action] = AppConstants.ACTION_MERGED_COMMENTS_LIST;
                    pakToSend[JsonKeys.ActivityType] = SettingsConstants.ACTIVITY_ON_MULTIMEDIA;
                }
                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId, true);
                if (feedbackfields != null)
                {
                    value = true;
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
            }
            finally { }
            return value;
        }

        public bool ContentsOfMediaAlbumRequest(Guid albumId, string albumName, int mediaType, int scroll, Guid maxContentId) //Guid minContentId = (albumModel.MediaList.Count > 0) ? albumModel.MediaList.Min(item => item.ContentId) : Guid.Empty; 
        {
            bool value = false;
            try
            {
                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_MEDIA_ALBUM_CONTENT_LIST;
                pakToSend[JsonKeys.MediaType] = mediaType;
                pakToSend[JsonKeys.AlbumId] = albumId;
                pakToSend[JsonKeys.AlbumName] = albumName;
                pakToSend[JsonKeys.Scroll] = scroll;
                pakToSend[JsonKeys.PivotUUID] = maxContentId;
                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId, true);
                if (feedbackfields != null)
                {
                    value = true;
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
            }
            finally { }
            return value;
        }

        #endregion "Public methods"
    }
}

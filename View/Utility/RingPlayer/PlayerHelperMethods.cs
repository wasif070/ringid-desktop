﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using log4net;
using Models.Constants;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.UI;
using View.UI.MediaPlayer;
using View.ViewModel;

namespace View.Utility.RingPlayer
{
    //public class PlayerHelperMethods
    //{
    //    private static readonly ILog log = LogManager.GetLogger(typeof(PlayerHelperMethods));

    //    private static UCMediaPlayerInMain MediaPlayerInMain { get; set; }
    //    public static void RunPlayList(bool fromPortalOrNotUploaded, Guid nfid, ObservableCollection<SingleMediaModel> playList, BaseUserProfileModel OwnerShortInfoForAlbum = null, int idx = 0, bool autoNext = true)
    //    {
    //        try
    //        {
    //            if (MediaPlayerInMain != null)
    //            {
    //                MediaPlayerInMain.RemovePlayerFromGuiRingID();
    //                MediaPlayerInMain.CloseMediaPlayer();
    //            }
    //            RingIDViewModel.Instance.MediaList.Clear();
    //            VMPlayer DataModel = new VMPlayer();
    //            foreach (SingleMediaModel model in playList)
    //            {
    //                model.NewsFeedId = nfid;
    //                RingIDViewModel.Instance.MediaList.InvokeAdd(model);
    //            }
    //            DataModel.MediaList = RingIDViewModel.Instance.MediaList;
    //            if (fromPortalOrNotUploaded)
    //            {
    //                DataModel.PlayingState = ViewConstants.MEDIA_PAUSED;
    //            }
    //            DataModel.PlayingState = ViewConstants.MEDIA_PLAYING;
    //            MediaPlayerInMain = new UCMediaPlayerInMain(DataModel);
    //            MediaPlayerInMain.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1, "#FFd8e1e6", 1.0, null, false);
    //            MediaPlayerInMain.SetOwinServer();
    //            MediaPlayerInMain.SetParentGrid(UCGuiRingID.Instance.mainGrid);
    //            MediaPlayerInMain.ShowPlayerInMainView(true);
    //            MediaPlayerInMain.OnClosedMediaPlayer += () =>
    //            {
    //                MediaPlayerInMain.DataModel = null;
    //                MediaPlayerInMain = null;
    //            };
    //            MediaPlayerInMain.ChangeSingleMedia(idx, true);
    //            ViewConstants.AlbumID = DataModel.SingleMediaModel.AlbumId;
    //        }
    //        finally { }
    //    }

    //    public static void PlaySingleMediaNoContentID(SingleMediaModel singleMediaModel, bool isFromLocalDirectory)//playlist!=null &>0& idx<count,playList items must have utid
    //    {
    //        try
    //        {
    //            ObservableCollection<SingleMediaModel> playList = new ObservableCollection<SingleMediaModel>();

    //            singleMediaModel.IsFromLocalDirectory = isFromLocalDirectory;
    //            if (string.IsNullOrEmpty(singleMediaModel.Title))
    //            {
    //                singleMediaModel.Title = "<Unknown>";
    //            }
    //            playList.Add(singleMediaModel);
    //            RunPlayList(false, Guid.Empty, playList, singleMediaModel.MediaOwner, 0, false);
    //        }
    //        finally { }
    //    }

    //    public static void RunPlayListFromNotification(Guid nfid, Guid cmntID, ObservableCollection<SingleMediaModel> playList, UserShortInfoModel OwnerShortInfoForAlbum = null, int idx = 0, bool autoNext = true)//playlist!=null &>0& idx<count,playList items must have utid
    //    {
    //        try
    //        {
    //            RunPlayList(true, nfid, playList, OwnerShortInfoForAlbum, idx, autoNext);
    //        }
    //        finally { }
    //    }

    //    public static void RunMediaFromChat(JObject mediaObj)
    //    {
    //        try
    //        {
    //            SingleMediaModel singleMediaModel = new SingleMediaModel();
    //            singleMediaModel.LoadData(mediaObj, (int)mediaObj[JsonKeys.MediaType]);
    //            if (mediaObj[JsonKeys.NewsfeedId] != null)
    //            {
    //                singleMediaModel.NewsFeedId = (Guid)mediaObj[JsonKeys.NewsfeedId];
    //            }
    //            ObservableCollection<SingleMediaModel> playList = new ObservableCollection<SingleMediaModel>();
    //            playList.Add(singleMediaModel);

    //            RunPlayList(false, singleMediaModel.NewsFeedId, playList);
    //        }
    //        catch (Exception) { }
    //    }

    //    public static void RemoveAUserControlFromAnotherPanel(UserControl panel)
    //    {
    //        try
    //        {
    //            if (panel != null)
    //            {
    //                Panel parentPanel = (Panel)panel.Parent;
    //                if (parentPanel != null && parentPanel.Children.Count > 0)
    //                {
    //                    parentPanel.Children.Remove(panel);
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            log.Error(ex.Message + ex.StackTrace);
    //        }
    //        finally
    //        {

    //        }
    //    }

    //}
}

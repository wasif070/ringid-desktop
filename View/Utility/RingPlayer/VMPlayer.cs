﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using Models.Constants;
using View.BindingModels;
using View.Constants;
using View.ViewModel;

namespace View.Utility.RingPlayer
{
    public class VMPlayer : BaseViewModel
    {
        #region
        //private int owinServerPort;//2260
        //private OwinServerSetup owinServer;
        #endregion

        #region "Data Properties"

        private SingleMediaModel singleMediaModel;
        public SingleMediaModel SingleMediaModel
        {
            get { return singleMediaModel; }
            set
            {
                if (value == singleMediaModel) return;
                singleMediaModel = value; OnPropertyChanged("SingleMediaModel");
            }
        }

        //private UserShortInfoModel mediaOwnerShortInfo;
        //public UserShortInfoModel MediaOwnerShortInfo
        //{
        //    get { return mediaOwnerShortInfo; }
        //    set { mediaOwnerShortInfo = value; this.OnPropertyChanged("MediaOwnerShortInfo"); }
        //}

        private ObservableCollection<SingleMediaModel> mediaList = new ObservableCollection<SingleMediaModel>();
        public ObservableCollection<SingleMediaModel> MediaList
        {
            get { return mediaList; }
            set { mediaList = value; OnPropertyChanged("MediaList"); }
        }

        private ObservableCollection<CommentModel> commentList = new ObservableCollection<CommentModel>();
        public ObservableCollection<CommentModel> CommentList
        {
            get { return commentList; }
            set { commentList = value; OnPropertyChanged("CommentList"); }
        }
        public int playingState = 0;
        public int PlayingState
        {
            get { return playingState; }
            set
            {
                if (playingState == value) return;
                if (value == ViewConstants.MEDIA_PAUSED) LoaderOrPasusedImage = null;
                else if (value == ViewConstants.MEDIA_PLAYING && isBuffering) LoaderOrPasusedImage = ImageLocation.LOADER_PLAYER_BUFFER;
                else if (value == ViewConstants.MEDIA_PLAYING && !isBuffering)
                {
                    if (SingleMediaModel != null && SingleMediaModel.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO) LoaderOrPasusedImage = ImageLocation.LOADER_PLAYER_MUSIC;
                    else LoaderOrPasusedImage = null;
                }
                playingState = value; OnPropertyChanged("PlayingState");
            }
        }

        private bool isBuffering = false;
        public bool IsBuffering
        {
            get { return isBuffering; }
            set
            {
                if (playingState == ViewConstants.MEDIA_PAUSED) LoaderOrPasusedImage = null;
                else if (value && PlayingState == ViewConstants.MEDIA_PLAYING) LoaderOrPasusedImage = ImageLocation.LOADER_PLAYER_BUFFER;
                else if (!value && PlayingState == ViewConstants.MEDIA_PLAYING)
                {
                    if (SingleMediaModel != null && SingleMediaModel.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO) LoaderOrPasusedImage = ImageLocation.LOADER_PLAYER_MUSIC;
                    else LoaderOrPasusedImage = null;
                }
                else LoaderOrPasusedImage = null;
                isBuffering = value; this.OnPropertyChanged("IsBuffering");
            }
        }

        private Guid newsFeedID;
        public Guid NewsFeedID
        {
            get { return newsFeedID; }
            set { newsFeedID = value; OnPropertyChanged("NewsFeedID"); }
        }

        private int playerViewType = ViewConstants.POPUP;
        public int PlayerViewType
        {
            get { return playerViewType; }
            set { playerViewType = value; OnPropertyChanged("PlayerViewType"); }
        }

        private bool previousButtonEnable = true;
        public bool PreviousButtonEnable
        {
            get { return previousButtonEnable; }
            set { previousButtonEnable = value; this.OnPropertyChanged("PreviousButtonEnable"); }
        }

        private bool nextButtonEnable = true;
        public bool NextButtonEnable
        {
            get { return nextButtonEnable; }
            set { nextButtonEnable = value; this.OnPropertyChanged("NextButtonEnable"); }
        }

        private Uri _RingMediaUri;
        public Uri RingMediaUri
        {
            get { return _RingMediaUri; }
            set { _RingMediaUri = value; this.OnPropertyChanged("RingMediaUri"); }
        }

        private int mediaNumberNevigation = 1;
        public int MediaNumberNevigation
        {
            get { return mediaNumberNevigation; }
            set { mediaNumberNevigation = value; this.OnPropertyChanged("MediaNumberNevigation"); }
        }

        private double seekBarValue;
        public double SeekBarValue
        {
            get { return seekBarValue; }
            set { seekBarValue = value; this.OnPropertyChanged("SeekBarValue"); }
        }

        private double seekBarMaximum;
        public double SeekBarMaximum
        {
            get { return seekBarMaximum; }
            set { seekBarMaximum = value; this.OnPropertyChanged("SeekBarMaximum"); }
        }

        private TimeSpan mediaElementPosition;
        public TimeSpan MediaElementPosition
        {
            get { return mediaElementPosition; }
            set { mediaElementPosition = value; this.OnPropertyChanged("MediaElementPosition"); }
        }

        private double mediaElementDownoadProgress;
        public double MediaElementDownoadProgress
        {
            get { return mediaElementDownoadProgress; }
            set { mediaElementDownoadProgress = value; OnPropertyChanged("MediaElementDownoadProgress"); }
        }

        private double mediaElementVolume = 0.5;
        public double MediaElementVolume
        {
            get { return mediaElementVolume; }
            set { mediaElementVolume = value; this.OnPropertyChanged("MediaElementVolume"); }
        }

        private string volmunInPercentage;
        public string VolmunInPercentage
        {
            get { return volmunInPercentage; }
            set { volmunInPercentage = value; OnPropertyChanged("VolmunInPercentage"); }
        }

        private double elapsedTime;
        public double ElapsedTime
        {
            get { return elapsedTime; }
            set { elapsedTime = value; OnPropertyChanged("ElapsedTime"); }
        }

        private double maxDuration;
        public double MaxDuration
        {
            get { return maxDuration; }
            set { maxDuration = value; OnPropertyChanged("MaxDuration"); }
        }

        private bool showLoadMore = true;
        public bool ShowLoadMore
        {
            get { return showLoadMore; }
            set
            {
                if (value == showLoadMore) return;
                showLoadMore = value; OnPropertyChanged("ShowLoadMore");
            }
        }

        private string _likeAnimationloader;
        public string LikeAnimationloader
        {
            get { return _likeAnimationloader; }
            set
            {
                if (value == _likeAnimationloader) return;
                _likeAnimationloader = value; OnPropertyChanged("LikeAnimationloader");
            }
        }

        private string loaderOrPasusedImage;
        public string LoaderOrPasusedImage
        {
            get { return loaderOrPasusedImage; }
            set
            {
                if (value == loaderOrPasusedImage) return;
                loaderOrPasusedImage = value; OnPropertyChanged("LoaderOrPasusedImage");
            }
        }

        private bool isPlaylistLoading = false;
        public bool IsPlaylistLoading
        {
            get { return isPlaylistLoading; }
            set
            {
                if (value == isPlaylistLoading) return;
                isPlaylistLoading = value; OnPropertyChanged("IsPlaylistLoading");
            }
        }

        private bool isLoadingComments = false;
        public bool IsLoadingComments
        {
            get { return isLoadingComments; }
            set { isLoadingComments = value; OnPropertyChanged("IsLoadingComments"); }
        }

        private bool isStartedNewMedia = true;
        public bool IsStartedNewMedia
        {
            get { return isStartedNewMedia; }
            set { isStartedNewMedia = value; OnPropertyChanged("IsStartedNewMedia"); }
        }

        private int plyaingIndex;
        public int PlayingIndex
        {
            get { return plyaingIndex; }
            set { plyaingIndex = value; ChangeNextPreviousButton(); }
        }

        private bool isLoadded = false;
        public bool IsLoadded { get { return isLoadded; } set { isLoadded = value; } }

        private bool isSliderDragging = false;
        public bool IsSliderDragging { get { return isSliderDragging; } set { isSliderDragging = value; } }

        public bool IsAddedAllFromRecentPlayList { get; set; }
        #endregion "Data Properties"

        #region Custom Events

        /// <summary>
        /// Media Element can not work without events
        /// </summary>
        public event EventHandler PlayRequested;
        public void Play()
        {
            if (this.PlayRequested != null) this.PlayRequested(this, EventArgs.Empty);
        }

        public event EventHandler PauseRequested;
        public void Paused()
        {
            var handler = PauseRequested;
            if (this.PauseRequested != null) this.PauseRequested(this, EventArgs.Empty);
        }

        public event EventHandler CloseRequested;
        public void Close()
        {
            var handler = CloseRequested;
            if (this.CloseRequested != null) this.CloseRequested(this, EventArgs.Empty);
        }
        #endregion

        #region "Public methods"

        public void OnSingleClickOnScreen()
        {
            if (PlayingState == ViewConstants.MEDIA_PLAYING)
            {
                PlayingState = ViewConstants.MEDIA_PAUSED;
                Paused();
            }
            else
            {
                PlayingState = ViewConstants.MEDIA_PLAYING;
                Play();
            }
        }

        public void ChangeSelectedMedia(int index)
        {
            PlayingIndex = index;
            SingleMediaModel model = MediaList.ElementAt(index);
            ViewConstants.ContentID = model.ContentId;
            //if (SingleMediaModel != null && SingleMediaModel.MediaOwner.UserTableID != SingleMediaModel.MediaOwner.UserTableID)
            //    MediaOwnerShortInfo = null;
            SingleMediaModel = model;
            MaxDuration = model.Duration;
            ElapsedTime = 0;
        }

        private void ChangeNextPreviousButton()
        {
            MediaNumberNevigation = PlayingIndex + 1;
            if (MediaList == null || MediaList.Count < 2)
            {
                PreviousButtonEnable = false;
                NextButtonEnable = false;
            }
            else if (PlayingIndex == 0)
            {
                PreviousButtonEnable = false;
                NextButtonEnable = true;
            }
            else if (PlayingIndex == (MediaList.Count - 1))
            {
                PreviousButtonEnable = true;
                NextButtonEnable = false;
            }
            else
            {
                PreviousButtonEnable = true;
                NextButtonEnable = true;
            }
        }
        public void PlayCurrentMedia()
        {
            if (RingMediaUri != null)
            {
                PlayingState = ViewConstants.MEDIA_PLAYING;
                Play();
            }
        }
        #endregion "Public methods"
    }
}

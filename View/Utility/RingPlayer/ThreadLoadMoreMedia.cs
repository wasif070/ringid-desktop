﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using View.Utility.Auth;
using View.ViewModel;

namespace View.Utility.RingPlayer
{
    class ThreadLoadMoreMedia
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThreadLoadMoreMedia).Name);
        bool running = false;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"

        private void Run()
        {
            try
            {
                Console.WriteLine("ThreadStartPlayer");
                running = true;
            }
            catch (Exception)
            {
            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
          
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

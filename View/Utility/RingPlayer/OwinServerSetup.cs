﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Owin;

namespace View.Utility.RingPlayer
{
    public class OwinServerSetup
    {
        public static string CurrentURi = string.Empty;
        IDisposable memoryServer;
        public void StartOwinServer(int port)
        {
            if (memoryServer == null)
            {
                memoryServer = Microsoft.Owin.Hosting.WebApp.Start<Startup>("http://localhost:" + port); //"http://localhost:2260"
            }
        }

        public void StopOwinServer()
        {
            if (memoryServer != null)
            {
                memoryServer.Dispose();
                memoryServer = null;
            }
            CurrentURi = string.Empty;
        }
    }

    class Startup
    {
        public void Configuration(IAppBuilder sampleapp)
        {
            try
            {
                sampleapp.Run(sample =>
                {
                    //System.Console.WriteLine("View.Utility.RingPlayer.MediaPlayerController.CurrentURi==>" + OwinServerSetup.CurrentURi);
                    sample.Response.Redirect(OwinServerSetup.CurrentURi);
                    sample.Response.ContentType = "text/plain";
                    return sample.Response.WriteAsync("Hello from OWIN");
                });
            }
            finally { }
        }
    }
}

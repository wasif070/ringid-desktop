<<<<<<< HEAD
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.UI;
using View.UI.PopUp;
using View.Utility.Auth;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.UI.PopUp.MediaSendViaChat;
using Models.DAO;
using View.Utility.Channel;
using View.UI.SocialMedia;

namespace View.Utility
{
    public class ContextMenuMediaOptions
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ContextMenuMediaOptions).Name);

        Grid gridPanel;
        private int Type;//1=me, 2=download, 3=playlist
        public SingleMediaModel mediaModel = null;
        UCFBShare fbShare;

        #region "Property"
        private static ContextMenuMediaOptions _Instance;
        public static ContextMenuMediaOptions Instance
        {
            get
            {
                _Instance = _Instance ?? new ContextMenuMediaOptions();
                return _Instance;
            }
        }
        private UCAlbumMediaEditPopup ucAlbumMediaEditPopup { get; set; }
        #endregion "Properties"

        public void ShowHandler(Grid gridPanel, SingleMediaModel model, int type)
        {
            this.gridPanel = gridPanel;
            this.mediaModel = model;
            this.Type = type;
            if (mediaModel.MediaOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
            {
                mnuItem6.Visibility = Visibility.Visible; ;
                mnuItem8.Visibility = Visibility.Collapsed;
                //mnuItem6.Visibility = Visibility.Visible;
            }
            else
            {
                mnuItem6.Visibility = Visibility.Collapsed;
                mnuItem8.Visibility = Visibility.Visible;
                //mnuItem6.Visibility = Visibility.Collapsed;
            }
            if (Type == 1 || Type == 2 || Type == 3)
                mnuItem7.Visibility = Visibility.Visible;
            else
                mnuItem7.Visibility = Visibility.Collapsed;

            //if (mediaModel.Privacy == SettingsConstants.PRIVACY_PUBLIC)
            //    mnuItem1.Visibility = Visibility.Visible;
            //else mnuItem1.Visibility = Visibility.Collapsed;

            if (mediaModel.MediaType == SettingsConstants.MEDIA_TYPE_VIDEO)
                mnuItem2.Visibility = Visibility.Visible;
            else mnuItem2.Visibility = Visibility.Collapsed;
        }

        #region "ContextMenu"

        public ContextMenu cntxMenu = new ContextMenu();
        MenuItem mnuItem1 = new MenuItem();
        MenuItem mnuItem2 = new MenuItem();
        MenuItem mnuItem3 = new MenuItem();
        MenuItem mnuItem4 = new MenuItem();
        MenuItem mnuItem5 = new MenuItem();
        MenuItem mnuItem6 = new MenuItem();
        MenuItem mnuItem7 = new MenuItem();
        MenuItem mnuItem8 = new MenuItem();

        public void SetupMenuItem()
        {
            if (cntxMenu.Items != null)
                cntxMenu.Items.Clear();
            cntxMenu.Style = (Style)Application.Current.Resources["CustomCntxtMenu"];
            mnuItem1.Header = "Add to Album";
            //mnuItem2.Header = "Add to my Channel";
            mnuItem3.Header = "Send via Chat";
            mnuItem4.Header = "Share on facebook";
            mnuItem5.Header = "Copy link";
            mnuItem6.Header = "Rename";
            mnuItem7.Header = "Delete";
            mnuItem8.Header = "Report";// Media Content

            //mnuItem1.Visibility = Visibility.Collapsed;
            mnuItem2.Visibility = Visibility.Collapsed;

            mnuItem1.Style = (Style)Application.Current.Resources["ContextMenuItem"];
            //mnuItem2.Style = (Style)Application.Current.Resources["ContextMenuItem"];
            mnuItem3.Style = (Style)Application.Current.Resources["ContextMenuItem"];
            mnuItem4.Style = (Style)Application.Current.Resources["ContextMenuItem"];
            mnuItem5.Style = (Style)Application.Current.Resources["ContextMenuItem"];
            mnuItem6.Style = (Style)Application.Current.Resources["ContextMenuItem"];
            mnuItem7.Style = (Style)Application.Current.Resources["ContextMenuItem"];
            mnuItem8.Style = (Style)Application.Current.Resources["ContextMenuItem"];

            cntxMenu.Items.Add(mnuItem1);
            //cntxMenu.Items.Add(mnuItem2);
            cntxMenu.Items.Add(mnuItem3);
            cntxMenu.Items.Add(mnuItem4);
            cntxMenu.Items.Add(mnuItem5);
            cntxMenu.Items.Add(mnuItem6);
            cntxMenu.Items.Add(mnuItem7);
            cntxMenu.Items.Add(mnuItem8);

            mnuItem1.Click -= MnuAddtoAlbum_Click;
            //mnuItem2.Click -= MnuAddtoMyChannel_Click;
            mnuItem3.Click -= sendViaChat_Click;
            mnuItem4.Click -= shareOnFb_Click;
            mnuItem5.Click -= copyLink_Click;
            mnuItem6.Click -= renameMedia_Click;
            mnuItem7.Click -= deleteMedia_Click;
            mnuItem8.Click -= reportMedia_Click;
            //cntxMenu.Closed -= ContextMenu_Closed;

            //cntxMenu.Closed += ContextMenu_Closed;
            mnuItem1.Click += MnuAddtoAlbum_Click;
            //mnuItem2.Click += MnuAddtoMyChannel_Click;
            mnuItem3.Click += sendViaChat_Click;
            mnuItem4.Click += shareOnFb_Click;
            mnuItem5.Click += copyLink_Click;
            mnuItem6.Click += renameMedia_Click;
            mnuItem7.Click += deleteMedia_Click;
            mnuItem8.Click += reportMedia_Click;
        }

        private void ContextMenu_Closed(object sender, RoutedEventArgs e)
        {
            mnuItem1.Click -= MnuAddtoAlbum_Click;
            //mnuItem2.Click -= MnuAddtoMyChannel_Click;
            mnuItem3.Click -= sendViaChat_Click;
            mnuItem4.Click -= shareOnFb_Click;
            mnuItem5.Click -= copyLink_Click;
            mnuItem6.Click -= renameMedia_Click;
            mnuItem7.Click -= deleteMedia_Click;
            mnuItem8.Click -= reportMedia_Click;
            cntxMenu.Closed -= ContextMenu_Closed;

            cntxMenu.Style = null;
            mnuItem1.Style = null;
            mnuItem2.Style = null;
            mnuItem3.Style = null;
            mnuItem4.Style = null;
            mnuItem5.Style = null;
            mnuItem6.Style = null;
            mnuItem7.Style = null;
            mnuItem8.Style = null;

            //cntxMenu.Items.Remove(mnuItem1);
            //cntxMenu.Items.Remove(mnuItem2);
            cntxMenu.Items.Clear();

            gridPanel.ContextMenu = null;
        }

        private void MnuAddtoAlbum_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                cntxMenu.IsOpen = false;
                if (UCDownloadOrAddToAlbumPopUp.Instance != null)
                {
                    if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid.Children.Remove(UCDownloadOrAddToAlbumPopUp.Instance);
                    else UCGuiRingID.Instance.MotherPanel.Children.Remove(UCDownloadOrAddToAlbumPopUp.Instance);
                }
                if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                    UCDownloadOrAddToAlbumPopUp.Instance = new UCDownloadOrAddToAlbumPopUp(UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid);
                else UCDownloadOrAddToAlbumPopUp.Instance = new UCDownloadOrAddToAlbumPopUp(UCGuiRingID.Instance.MotherPanel);

                UCDownloadOrAddToAlbumPopUp.Instance.OnRemovedUserControl += () =>
                {
                    UCDownloadOrAddToAlbumPopUp.Instance = null;
                    if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                };
                if (mediaModel.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO)
                {
                    if (RingIDViewModel.Instance.MyAudioAlbums.Count == DefaultSettings.MY_AUDIO_ALBUMS_COUNT)
                    {
                        UCDownloadOrAddToAlbumPopUp.Instance.Show();
                        UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(mediaModel, SettingsConstants.MEDIA_TYPE_AUDIO, false, true, () => { return 0; });
                    }
                    //else if (NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID.ContainsKey(DefaultSettings.LOGIN_TABLE_ID)
                    //    && NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_TABLE_ID].Count == DefaultSettings.MY_AUDIO_ALBUMS_COUNT)
                    //{
                    //    List<MediaContentDTO> lst = NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_TABLE_ID].Values.ToList();
                    //    foreach (var albumDto in lst)
                    //    {
                    //        if (!RingIDViewModel.Instance.MyAudioAlbums.Any(P => P.AlbumId == albumDto.AlbumId))
                    //        {
                    //            MediaContentModel albumModel = new MediaContentModel();
                    //            albumModel.LoadData(albumDto);
                    //            RingIDViewModel.Instance.MyAudioAlbums.Add(albumModel);
                    //        }
                    //    }
                    //    DownloadOrAddToAlbumPopUpWrapper.Show(mediaModel, SettingsConstants.MEDIA_TYPE_AUDIO, false, true, () => { return 0; });
                    //}
                    else if (DefaultSettings.IsInternetAvailable)
                    {
                        UCDownloadOrAddToAlbumPopUp.Instance.Show();
                        UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(mediaModel, SettingsConstants.MEDIA_TYPE_AUDIO, false, true, () => { return 0; });
                        SendDataToServer.ListMediaAlbumsOfaUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_AUDIO, Guid.Empty); //TODO
                    }
                }
                else if (mediaModel.MediaType == SettingsConstants.MEDIA_TYPE_VIDEO)
                {
                    if (RingIDViewModel.Instance.MyVideoAlbums.Count == DefaultSettings.MY_VIDEO_ALBUMS_COUNT)
                    {
                        UCDownloadOrAddToAlbumPopUp.Instance.Show();
                        UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(mediaModel, SettingsConstants.MEDIA_TYPE_VIDEO, false, true, () => { return 0; });
                    }
                    //else if (NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID.ContainsKey(DefaultSettings.LOGIN_TABLE_ID) && NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_TABLE_ID].Count == DefaultSettings.MY_VIDEO_ALBUMS_COUNT)
                    //{
                    //    List<MediaContentDTO> lst = NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_TABLE_ID].Values.ToList();
                    //    foreach (var albumDto in lst)
                    //    {
                    //        if (!RingIDViewModel.Instance.MyVideoAlbums.Any(P => P.AlbumId == albumDto.AlbumId))
                    //        {
                    //            MediaContentModel albumModel = new MediaContentModel();
                    //            albumModel.LoadData(albumDto);
                    //            RingIDViewModel.Instance.MyVideoAlbums.Add(albumModel);
                    //        }
                    //    }
                    //    DownloadOrAddToAlbumPopUpWrapper.Show(mediaModel, SettingsConstants.MEDIA_TYPE_VIDEO, false, true, () => { return 0; });
                    //}
                    else if (DefaultSettings.IsInternetAvailable)
                    {
                        UCDownloadOrAddToAlbumPopUp.Instance.Show();
                        UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(mediaModel, SettingsConstants.MEDIA_TYPE_VIDEO, false, true, () => { return 0; });
                        SendDataToServer.ListMediaAlbumsOfaUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_VIDEO, Guid.Empty); //TODO
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
        }

        private void MnuAddtoMyChannel_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            cntxMenu.IsOpen = false;
            if (UCAddToMyChannelView.Instance != null)
                UCGuiRingID.Instance.MotherPanel.Children.Remove(UCAddToMyChannelView.Instance);
            if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._ChannelId == Guid.Empty)
            {
                UCAddToMyChannelView.Instance = new UCAddToMyChannelView(UCGuiRingID.Instance.MotherPanel);
                UCAddToMyChannelView.Instance.Show();
                UCAddToMyChannelView.Instance.ShowChannelView(mediaModel);
                UCAddToMyChannelView.Instance.OnRemovedUserControl += () =>
                {
                    UCAddToMyChannelView.Instance = null;
                };
            }
            else if (mediaModel != null)
            {
                List<ChannelMediaDTO> mediaDTOList = new List<ChannelMediaDTO>();
                ChannelMediaDTO channelMediaDTO = ChannelHelpers.GetChannelMediaDtoFromSingleMediaModel(mediaModel);
                channelMediaDTO.ChannelID = UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._ChannelId;
                channelMediaDTO.ChannelType = SettingsConstants.MEDIA_TYPE_VIDEO;
                channelMediaDTO.MediaStatus = ChannelConstants.MEDIA_STATUS_PENDING;
                channelMediaDTO.OwnerID = DefaultSettings.LOGIN_TABLE_ID;
                mediaDTOList.Add(channelMediaDTO);
                new ThrdAddMediaToChannel(UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._ChannelId, SettingsConstants.MEDIA_TYPE_VIDEO, mediaDTOList, (status, message) =>
                {
                    if (status)
                    {
                        UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, "Media added Successfully!", 1000);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(message))
                        {
                            UIHelperMethods.ShowFailed(message, "Add media");
                            //CustomMessageBox.ShowError(message);
                        }
                        else
                        {
                            UIHelperMethods.ShowFailed("Failed to add media", "Add media");
                        }
                    }
                    return 0;
                }).Start();
            }
        }

        private void MnuShare_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            cntxMenu.IsOpen = false;
            MainSwitcher.PopupController.ShareSingleFeedView.Show();
            MainSwitcher.PopupController.ShareSingleFeedView.ShowShareView(mediaModel);
        }

        private void sendViaChat_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            cntxMenu.IsOpen = false;
            if (UCAlbumContentView.Instance != null && UCAlbumContentView.Instance.IsVisible)
            {
                UCMediaShareToFriendPopup ucMediaShareToFriendPopup = new UCMediaShareToFriendPopup(UCAlbumContentView.Instance.mainPanel);
                ucMediaShareToFriendPopup.Show();
                ucMediaShareToFriendPopup.SetSingleMedia(this.mediaModel);
                ucMediaShareToFriendPopup.OnRemovedUserControl += () =>
                {
                    ucMediaShareToFriendPopup = null;
                    if (UCAlbumContentView.Instance != null && UCAlbumContentView.Instance.IsVisible)
                        UCAlbumContentView.Instance.OnLoadedControl();
                };
            }
            else
            {
                UCMediaShareToFriendPopup ucMediaShareToFriendPopup = new UCMediaShareToFriendPopup(UCGuiRingID.Instance.MotherPanel);
                ucMediaShareToFriendPopup.Show();
                ucMediaShareToFriendPopup.SetSingleMedia(this.mediaModel);
                ucMediaShareToFriendPopup.OnRemovedUserControl += () =>
                {
                    ucMediaShareToFriendPopup = null;
                };
            }
            //DownloadOrAddToAlbumPopUpWrapper.Show(this.mediaModel);
        }

        private void shareOnFb_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(ServerAndPortSettings.VOD_SERVER_RESOURCE) && !string.IsNullOrEmpty(this.mediaModel.StreamUrl))
            {
                try
                {
                    //if (Utility.Wallet.WalletViewModel.Instance.MediaShareViaWallet)
                    //{
                    //    Utility.Wallet.WalletViewModel.Instance.SetMediaShareInfo(MediaUtility.MakeEncriptedURL(this.mediaModel.MediaOwner.UserTableID, this.mediaModel.ContentId), this.mediaModel.MediaType, WalletConstants.SOCIAL_MEDIA_FACEBOOK);
                    //    UI.Wallet.WnMediaShareViaWallet.Instance.Show();
                    //}
                    //else
                    //{
                    //string fbShareLink = "https://www.facebook.com/sharer/sharer.php?u="
                    //            + MediaUtility.MakeEncriptedURL(this.mediaModel.MediaOwner.UserTableID, this.mediaModel.ContentId);
                    //Process myProcess = new Process();
                    //// true is the default, but it is important not to set it to false
                    //myProcess.StartInfo.UseShellExecute = true;
                    //myProcess.StartInfo.FileName = fbShareLink;
                    //myProcess.Start();
                    //}
                    if (fbShare == null)
                    {
                        string encyptedUrl = MediaUtility.MakeEncriptedURL(this.mediaModel.MediaOwner.UserTableID, this.mediaModel.ContentId);
                        fbShare = new UCFBShare(encyptedUrl, mediaModel.MediaType, true);
                        fbShare.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
                        if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                            fbShare.SetParent(UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid);
                        else fbShare.SetParent(UCGuiRingID.Instance.MotherPanel);
                        fbShare.OnRemovedUserControl += () =>
                        {
                            fbShare = null;
                            if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                                UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                        };
                        fbShare.Show();
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex.StackTrace + "==>" + ex.Message);
                }
                cntxMenu.IsOpen = false;
            }
            else
            {
                cntxMenu.IsOpen = false;
                UIHelperMethods.ShowFailed("Media link can not be shared to facebook right now!", "Facebook share");
                //CustomMessageBox.ShowError("Media link can not be shared to facebook right now!");
            }
        }

        private void copyLink_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(ServerAndPortSettings.VOD_SERVER_RESOURCE) && !string.IsNullOrEmpty(this.mediaModel.StreamUrl))
            {
                System.Windows.Clipboard.SetText(MediaUtility.MakeEncriptedURL(this.mediaModel.MediaOwner.UserTableID, this.mediaModel.ContentId));
                cntxMenu.IsOpen = false;
                UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, "Media link copied to clipboard!");
            }
            else
            {
                cntxMenu.IsOpen = false;
                UIHelperMethods.ShowFailed("Media link can not be copied right now!", "Copy link");
                //CustomMessageBox.ShowError("Media link can not be copied right now!");
            }
        }

        private void reportMedia_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            cntxMenu.IsOpen = false;
            MainSwitcher.AuthSignalHandler().feedSignalHandler.InitializeReportConentsView(this.mediaModel.ContentId, StatusConstants.SPAM_MEDIA_CONTENT, 0);
        }

        private void renameMedia_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            cntxMenu.IsOpen = false;
            if (ucAlbumMediaEditPopup != null) UCGuiRingID.Instance.MotherPanel.Children.Remove(ucAlbumMediaEditPopup);
            ucAlbumMediaEditPopup = new UCAlbumMediaEditPopup(UCGuiRingID.Instance.MotherPanel);
            ucAlbumMediaEditPopup.Show();
            ucAlbumMediaEditPopup.ShowView(this.mediaModel);
            ucAlbumMediaEditPopup.OnRemovedUserControl += () =>
            {
                ucAlbumMediaEditPopup.ClosePopUp();
                ucAlbumMediaEditPopup = null;
                //if (ImageViewInMain != null)
                //{
                //    ImageViewInMain.GrabKeyboardFocus();
            };
        }

        private void deleteMedia_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            cntxMenu.IsOpen = false;
            bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.DELETE_CONFIRMATIONS, "this Media"), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Delete"));
            if (isTrue)
            {
                if (Type == 2)//D
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        if (RingIDViewModel.Instance.MyDownloads.Count > 0 && RingIDViewModel.Instance.MyDownloads.Any(P => P.ContentId == this.mediaModel.ContentId))
                            RingIDViewModel.Instance.MyDownloads.Remove(this.mediaModel);
                    });
                    MediaDAO.Instance.DeleteFromDownloadedMediasTable(mediaModel.ContentId);
                }
                else if (Type == 3)//r
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        if (RingIDViewModel.Instance.MyRecentPlayList.Count > 0 && RingIDViewModel.Instance.MyRecentPlayList.Any(P => P.ContentId == this.mediaModel.ContentId))
                            RingIDViewModel.Instance.MyRecentPlayList.Remove(this.mediaModel);
                    });
                    MediaDAO.Instance.DeleteFromRecentMediasTable(mediaModel.ContentId);
                }
                else
                {
                    ThreadDeleteMediaOrAlbum thrd = new ThreadDeleteMediaOrAlbum(this.mediaModel.ContentId, mediaModel.AlbumId, mediaModel.MediaType);
                    thrd.callBackEvent += (success) =>
                    {
                        if (success == SettingsConstants.RESPONSE_SUCCESS)
                        {
                            if (this.mediaModel.AlbumId != Guid.Empty)
                            {
                                MediaContentModel albumModel = RingIDViewModel.Instance.GetMyAlbumModelFromAlbumID(this.mediaModel.AlbumId, this.mediaModel.MediaType);
                                if (albumModel != null)
                                {
                                    albumModel.TotalMediaCount -= 1;
                                    Application.Current.Dispatcher.Invoke(() =>
                                    {
                                        if (albumModel.MediaList != null && albumModel.MediaList.Any(P => P.ContentId == this.mediaModel.ContentId))
                                            albumModel.MediaList.Remove(this.mediaModel);
                                    });
                                }
                                UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, "Successfully Deleted this Media!");
                            }
                        }
                        else
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                UIHelperMethods.ShowFailed(NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK, "Delete media");
                                // CustomMessageBox.ShowError("Please check your Internet connection or try later!", "Failed!");
                            });
                        }
                    };
                    thrd.StartThread();
                }
            }
        }
        #endregion "ContextMenu"
    }
}
=======
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.UI;
using View.UI.PopUp;
using View.Utility.Auth;
using View.Utility.Feed;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.UI.PopUp.MediaSendViaChat;
using Models.DAO;
using View.Utility.Channel;
using View.UI.SocialMedia;

namespace View.Utility
{
    public class ContextMenuMediaOptions
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ContextMenuMediaOptions).Name);

        Grid gridPanel;
        private int Type;//1=me, 2=download, 3=playlist
        public SingleMediaModel mediaModel = null;
        UCFBShare fbShare;

        #region "Property"
        private static ContextMenuMediaOptions _Instance;
        public static ContextMenuMediaOptions Instance
        {
            get
            {
                _Instance = _Instance ?? new ContextMenuMediaOptions();
                return _Instance;
            }
        }
        private UCAlbumMediaEditPopup ucAlbumMediaEditPopup { get; set; }
        #endregion "Properties"

        public void ShowHandler(Grid gridPanel, SingleMediaModel model, int type)
        {
            this.gridPanel = gridPanel;
            this.mediaModel = model;
            this.Type = type;
            if (mediaModel.MediaOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
            {
                mnuItem6.Visibility = Visibility.Visible; ;
                mnuItem8.Visibility = Visibility.Collapsed;
                //mnuItem6.Visibility = Visibility.Visible;
            }
            else
            {
                mnuItem6.Visibility = Visibility.Collapsed;
                mnuItem8.Visibility = Visibility.Visible;
                //mnuItem6.Visibility = Visibility.Collapsed;
            }
            if (Type == 1 || Type == 2 || Type == 3)
                mnuItem7.Visibility = Visibility.Visible;
            else
                mnuItem7.Visibility = Visibility.Collapsed;

            //if (mediaModel.Privacy == SettingsConstants.PRIVACY_PUBLIC)
            //    mnuItem1.Visibility = Visibility.Visible;
            //else mnuItem1.Visibility = Visibility.Collapsed;

            if (mediaModel.MediaType == SettingsConstants.MEDIA_TYPE_VIDEO)
                mnuItem2.Visibility = Visibility.Visible;
            else mnuItem2.Visibility = Visibility.Collapsed;
        }

        #region "ContextMenu"

        public ContextMenu cntxMenu = new ContextMenu();
        MenuItem mnuItem1 = new MenuItem();
        MenuItem mnuItem2 = new MenuItem();
        MenuItem mnuItem3 = new MenuItem();
        MenuItem mnuItem4 = new MenuItem();
        MenuItem mnuItem5 = new MenuItem();
        MenuItem mnuItem6 = new MenuItem();
        MenuItem mnuItem7 = new MenuItem();
        MenuItem mnuItem8 = new MenuItem();

        public void SetupMenuItem()
        {
            if (cntxMenu.Items != null)
                cntxMenu.Items.Clear();
            cntxMenu.Style = (Style)Application.Current.Resources["CustomCntxtMenu"];
            mnuItem1.Header = "Add to Album";
            //mnuItem2.Header = "Add to my Channel";
            mnuItem3.Header = "Send via Chat";
            mnuItem4.Header = "Share on facebook";
            mnuItem5.Header = "Copy link";
            mnuItem6.Header = "Rename";
            mnuItem7.Header = "Delete";
            mnuItem8.Header = "Report";// Media Content

            //mnuItem1.Visibility = Visibility.Collapsed;
            mnuItem2.Visibility = Visibility.Collapsed;

            mnuItem1.Style = (Style)Application.Current.Resources["ContextMenuItem"];
            //mnuItem2.Style = (Style)Application.Current.Resources["ContextMenuItem"];
            mnuItem3.Style = (Style)Application.Current.Resources["ContextMenuItem"];
            mnuItem4.Style = (Style)Application.Current.Resources["ContextMenuItem"];
            mnuItem5.Style = (Style)Application.Current.Resources["ContextMenuItem"];
            mnuItem6.Style = (Style)Application.Current.Resources["ContextMenuItem"];
            mnuItem7.Style = (Style)Application.Current.Resources["ContextMenuItem"];
            mnuItem8.Style = (Style)Application.Current.Resources["ContextMenuItem"];

            cntxMenu.Items.Add(mnuItem1);
            //cntxMenu.Items.Add(mnuItem2);
            cntxMenu.Items.Add(mnuItem3);
            cntxMenu.Items.Add(mnuItem4);
            cntxMenu.Items.Add(mnuItem5);
            cntxMenu.Items.Add(mnuItem6);
            cntxMenu.Items.Add(mnuItem7);
            cntxMenu.Items.Add(mnuItem8);

            mnuItem1.Click -= MnuAddtoAlbum_Click;
            //mnuItem2.Click -= MnuAddtoMyChannel_Click;
            mnuItem3.Click -= sendViaChat_Click;
            mnuItem4.Click -= shareOnFb_Click;
            mnuItem5.Click -= copyLink_Click;
            mnuItem6.Click -= renameMedia_Click;
            mnuItem7.Click -= deleteMedia_Click;
            mnuItem8.Click -= reportMedia_Click;
            //cntxMenu.Closed -= ContextMenu_Closed;

            //cntxMenu.Closed += ContextMenu_Closed;
            mnuItem1.Click += MnuAddtoAlbum_Click;
            //mnuItem2.Click += MnuAddtoMyChannel_Click;
            mnuItem3.Click += sendViaChat_Click;
            mnuItem4.Click += shareOnFb_Click;
            mnuItem5.Click += copyLink_Click;
            mnuItem6.Click += renameMedia_Click;
            mnuItem7.Click += deleteMedia_Click;
            mnuItem8.Click += reportMedia_Click;
        }

        private void ContextMenu_Closed(object sender, RoutedEventArgs e)
        {
            mnuItem1.Click -= MnuAddtoAlbum_Click;
            //mnuItem2.Click -= MnuAddtoMyChannel_Click;
            mnuItem3.Click -= sendViaChat_Click;
            mnuItem4.Click -= shareOnFb_Click;
            mnuItem5.Click -= copyLink_Click;
            mnuItem6.Click -= renameMedia_Click;
            mnuItem7.Click -= deleteMedia_Click;
            mnuItem8.Click -= reportMedia_Click;
            cntxMenu.Closed -= ContextMenu_Closed;

            cntxMenu.Style = null;
            mnuItem1.Style = null;
            mnuItem2.Style = null;
            mnuItem3.Style = null;
            mnuItem4.Style = null;
            mnuItem5.Style = null;
            mnuItem6.Style = null;
            mnuItem7.Style = null;
            mnuItem8.Style = null;

            //cntxMenu.Items.Remove(mnuItem1);
            //cntxMenu.Items.Remove(mnuItem2);
            cntxMenu.Items.Clear();

            gridPanel.ContextMenu = null;
        }

        private void MnuAddtoAlbum_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                cntxMenu.IsOpen = false;
                if (UCDownloadOrAddToAlbumPopUp.Instance != null)
                {
                    if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid.Children.Remove(UCDownloadOrAddToAlbumPopUp.Instance);
                    else UCGuiRingID.Instance.MotherPanel.Children.Remove(UCDownloadOrAddToAlbumPopUp.Instance);
                }
                if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                    UCDownloadOrAddToAlbumPopUp.Instance = new UCDownloadOrAddToAlbumPopUp(UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid);
                else UCDownloadOrAddToAlbumPopUp.Instance = new UCDownloadOrAddToAlbumPopUp(UCGuiRingID.Instance.MotherPanel);

                UCDownloadOrAddToAlbumPopUp.Instance.OnRemovedUserControl += () =>
                {
                    UCDownloadOrAddToAlbumPopUp.Instance = null;
                    if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                        UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                };
                if (mediaModel.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO)
                {
                    if (RingIDViewModel.Instance.MyAudioAlbums.Count == DefaultSettings.MY_AUDIO_ALBUMS_COUNT)
                    {
                        UCDownloadOrAddToAlbumPopUp.Instance.Show();
                        UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(mediaModel, SettingsConstants.MEDIA_TYPE_AUDIO, false, true, () => { return 0; });
                    }
                    //else if (NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID.ContainsKey(DefaultSettings.LOGIN_TABLE_ID)
                    //    && NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_TABLE_ID].Count == DefaultSettings.MY_AUDIO_ALBUMS_COUNT)
                    //{
                    //    List<MediaContentDTO> lst = NewsFeedDictionaries.Instance.AUDIO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_TABLE_ID].Values.ToList();
                    //    foreach (var albumDto in lst)
                    //    {
                    //        if (!RingIDViewModel.Instance.MyAudioAlbums.Any(P => P.AlbumId == albumDto.AlbumId))
                    //        {
                    //            MediaContentModel albumModel = new MediaContentModel();
                    //            albumModel.LoadData(albumDto);
                    //            RingIDViewModel.Instance.MyAudioAlbums.Add(albumModel);
                    //        }
                    //    }
                    //    DownloadOrAddToAlbumPopUpWrapper.Show(mediaModel, SettingsConstants.MEDIA_TYPE_AUDIO, false, true, () => { return 0; });
                    //}
                    else if (DefaultSettings.IsInternetAvailable)
                    {
                        UCDownloadOrAddToAlbumPopUp.Instance.Show();
                        UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(mediaModel, SettingsConstants.MEDIA_TYPE_AUDIO, false, true, () => { return 0; });
                        SendDataToServer.ListMediaAlbumsOfaUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_AUDIO, Guid.Empty); //TODO
                    }
                }
                else if (mediaModel.MediaType == SettingsConstants.MEDIA_TYPE_VIDEO)
                {
                    if (RingIDViewModel.Instance.MyVideoAlbums.Count == DefaultSettings.MY_VIDEO_ALBUMS_COUNT)
                    {
                        UCDownloadOrAddToAlbumPopUp.Instance.Show();
                        UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(mediaModel, SettingsConstants.MEDIA_TYPE_VIDEO, false, true, () => { return 0; });
                    }
                    //else if (NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID.ContainsKey(DefaultSettings.LOGIN_TABLE_ID) && NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_TABLE_ID].Count == DefaultSettings.MY_VIDEO_ALBUMS_COUNT)
                    //{
                    //    List<MediaContentDTO> lst = NewsFeedDictionaries.Instance.VIDEO_ALBUM_LIST_BY_UTID[DefaultSettings.LOGIN_TABLE_ID].Values.ToList();
                    //    foreach (var albumDto in lst)
                    //    {
                    //        if (!RingIDViewModel.Instance.MyVideoAlbums.Any(P => P.AlbumId == albumDto.AlbumId))
                    //        {
                    //            MediaContentModel albumModel = new MediaContentModel();
                    //            albumModel.LoadData(albumDto);
                    //            RingIDViewModel.Instance.MyVideoAlbums.Add(albumModel);
                    //        }
                    //    }
                    //    DownloadOrAddToAlbumPopUpWrapper.Show(mediaModel, SettingsConstants.MEDIA_TYPE_VIDEO, false, true, () => { return 0; });
                    //}
                    else if (DefaultSettings.IsInternetAvailable)
                    {
                        UCDownloadOrAddToAlbumPopUp.Instance.Show();
                        UCDownloadOrAddToAlbumPopUp.Instance.ShowPopup(mediaModel, SettingsConstants.MEDIA_TYPE_VIDEO, false, true, () => { return 0; });
                        SendDataToServer.ListMediaAlbumsOfaUser(DefaultSettings.LOGIN_TABLE_ID, SettingsConstants.MEDIA_TYPE_VIDEO, Guid.Empty); //TODO
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ==> " + ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
        }

        private void MnuAddtoMyChannel_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            cntxMenu.IsOpen = false;
            if (UCAddToMyChannelView.Instance != null)
                UCGuiRingID.Instance.MotherPanel.Children.Remove(UCAddToMyChannelView.Instance);
            if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._ChannelId == Guid.Empty)
            {
                UCAddToMyChannelView.Instance = new UCAddToMyChannelView(UCGuiRingID.Instance.MotherPanel);
                UCAddToMyChannelView.Instance.Show();
                UCAddToMyChannelView.Instance.ShowChannelView(mediaModel);
                UCAddToMyChannelView.Instance.OnRemovedUserControl += () =>
                {
                    UCAddToMyChannelView.Instance = null;
                };
            }
            else if (mediaModel != null)
            {
                List<ChannelMediaDTO> mediaDTOList = new List<ChannelMediaDTO>();
                ChannelMediaDTO channelMediaDTO = ChannelHelpers.GetChannelMediaDtoFromSingleMediaModel(mediaModel);
                channelMediaDTO.ChannelID = UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._ChannelId;
                channelMediaDTO.ChannelType = SettingsConstants.MEDIA_TYPE_VIDEO;
                channelMediaDTO.MediaStatus = ChannelConstants.MEDIA_STATUS_PENDING;
                channelMediaDTO.OwnerID = DefaultSettings.LOGIN_TABLE_ID;
                mediaDTOList.Add(channelMediaDTO);
                new ThrdAddUploadedChannelMedia(mediaDTOList, (status, message) =>
                {
                    if (status)
                    {
                        UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, "Media added Successfully!", 1000);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(message))
                        {
                            UIHelperMethods.ShowFailed(message, "Add media");
                            //CustomMessageBox.ShowError(message);
                        }
                        else
                        {
                            UIHelperMethods.ShowFailed("Failed to add media", "Add media");
                        }
                    }
                    return 0;
                }).Start();
            }
        }

        private void MnuShare_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            cntxMenu.IsOpen = false;
            MainSwitcher.PopupController.ShareSingleFeedView.Show();
            MainSwitcher.PopupController.ShareSingleFeedView.ShowShareView(mediaModel);
        }

        private void sendViaChat_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            cntxMenu.IsOpen = false;
            if (UCAlbumContentView.Instance != null && UCAlbumContentView.Instance.IsVisible)
            {
                UCMediaShareToFriendPopup ucMediaShareToFriendPopup = new UCMediaShareToFriendPopup(UCAlbumContentView.Instance.mainPanel);
                ucMediaShareToFriendPopup.Show();
                ucMediaShareToFriendPopup.SetSingleMedia(this.mediaModel);
                ucMediaShareToFriendPopup.OnRemovedUserControl += () =>
                {
                    ucMediaShareToFriendPopup = null;
                    if (UCAlbumContentView.Instance != null && UCAlbumContentView.Instance.IsVisible)
                        UCAlbumContentView.Instance.OnLoadedControl();
                };
            }
            else
            {
                UCMediaShareToFriendPopup ucMediaShareToFriendPopup = new UCMediaShareToFriendPopup(UCGuiRingID.Instance.MotherPanel);
                ucMediaShareToFriendPopup.Show();
                ucMediaShareToFriendPopup.SetSingleMedia(this.mediaModel);
                ucMediaShareToFriendPopup.OnRemovedUserControl += () =>
                {
                    ucMediaShareToFriendPopup = null;
                };
            }
            //DownloadOrAddToAlbumPopUpWrapper.Show(this.mediaModel);
        }

        private void shareOnFb_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(ServerAndPortSettings.VOD_SERVER_RESOURCE) && !string.IsNullOrEmpty(this.mediaModel.StreamUrl))
            {
                try
                {
                    //if (Utility.Wallet.WalletViewModel.Instance.MediaShareViaWallet)
                    //{
                    //    Utility.Wallet.WalletViewModel.Instance.SetMediaShareInfo(MediaUtility.MakeEncriptedURL(this.mediaModel.MediaOwner.UserTableID, this.mediaModel.ContentId), this.mediaModel.MediaType, WalletConstants.SOCIAL_MEDIA_FACEBOOK);
                    //    UI.Wallet.WnMediaShareViaWallet.Instance.Show();
                    //}
                    //else
                    //{
                    //string fbShareLink = "https://www.facebook.com/sharer/sharer.php?u="
                    //            + MediaUtility.MakeEncriptedURL(this.mediaModel.MediaOwner.UserTableID, this.mediaModel.ContentId);
                    //Process myProcess = new Process();
                    //// true is the default, but it is important not to set it to false
                    //myProcess.StartInfo.UseShellExecute = true;
                    //myProcess.StartInfo.FileName = fbShareLink;
                    //myProcess.Start();
                    //}
                    if (fbShare == null)
                    {
                        string encyptedUrl = MediaUtility.MakeEncriptedURL(this.mediaModel.MediaOwner.UserTableID, this.mediaModel.ContentId);
                        fbShare = new UCFBShare(encyptedUrl, mediaModel.MediaType, true);
                        fbShare.InitPopUpBaseControl(ViewConstants.POP_UP_BG_BORDER1_LIGHT, ViewConstants.POP_UP_BG_BORDER2, 1.0);//background Color Transparent Black
                        if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                            fbShare.SetParent(UCSingleFeedDetailsView.Instance._PopUpWrapperMotherGrid);
                        else fbShare.SetParent(UCGuiRingID.Instance.MotherPanel);
                        fbShare.OnRemovedUserControl += () =>
                        {
                            fbShare = null;
                            if (UCSingleFeedDetailsView.Instance != null && UCSingleFeedDetailsView.Instance.IsVisible)
                                UCSingleFeedDetailsView.Instance.GrabKeyboardFocus();
                        };
                        fbShare.Show();
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex.StackTrace + "==>" + ex.Message);
                }
                cntxMenu.IsOpen = false;
            }
            else
            {
                cntxMenu.IsOpen = false;
                UIHelperMethods.ShowFailed("Media link can not be shared to facebook right now!", "Facebook share");
                //CustomMessageBox.ShowError("Media link can not be shared to facebook right now!");
            }
        }

        private void copyLink_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(ServerAndPortSettings.VOD_SERVER_RESOURCE) && !string.IsNullOrEmpty(this.mediaModel.StreamUrl))
            {
                System.Windows.Clipboard.SetText(MediaUtility.MakeEncriptedURL(this.mediaModel.MediaOwner.UserTableID, this.mediaModel.ContentId));
                cntxMenu.IsOpen = false;
                UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, "Media link copied to clipboard!");
            }
            else
            {
                cntxMenu.IsOpen = false;
                UIHelperMethods.ShowFailed("Media link can not be copied right now!", "Copy link");
                //CustomMessageBox.ShowError("Media link can not be copied right now!");
            }
        }

        private void reportMedia_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            cntxMenu.IsOpen = false;
            MainSwitcher.AuthSignalHandler().feedSignalHandler.InitializeReportConentsView(this.mediaModel.ContentId, StatusConstants.SPAM_MEDIA_CONTENT, 0);
        }

        private void renameMedia_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            cntxMenu.IsOpen = false;
            if (ucAlbumMediaEditPopup != null) UCGuiRingID.Instance.MotherPanel.Children.Remove(ucAlbumMediaEditPopup);
            ucAlbumMediaEditPopup = new UCAlbumMediaEditPopup(UCGuiRingID.Instance.MotherPanel);
            ucAlbumMediaEditPopup.Show();
            ucAlbumMediaEditPopup.ShowView(this.mediaModel);
            ucAlbumMediaEditPopup.OnRemovedUserControl += () =>
            {
                ucAlbumMediaEditPopup.ClosePopUp();
                ucAlbumMediaEditPopup = null;
                //if (ImageViewInMain != null)
                //{
                //    ImageViewInMain.GrabKeyboardFocus();
            };
        }

        private void deleteMedia_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            cntxMenu.IsOpen = false;
            bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.DELETE_CONFIRMATIONS, "this Media"), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Delete"));
            if (isTrue)
            {
                if (Type == 2)//D
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        if (RingIDViewModel.Instance.MyDownloads.Count > 0 && RingIDViewModel.Instance.MyDownloads.Any(P => P.ContentId == this.mediaModel.ContentId))
                            RingIDViewModel.Instance.MyDownloads.Remove(this.mediaModel);
                    });
                    MediaDAO.Instance.DeleteFromDownloadedMediasTable(mediaModel.ContentId);
                }
                else if (Type == 3)//r
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        if (RingIDViewModel.Instance.MyRecentPlayList.Count > 0 && RingIDViewModel.Instance.MyRecentPlayList.Any(P => P.ContentId == this.mediaModel.ContentId))
                            RingIDViewModel.Instance.MyRecentPlayList.Remove(this.mediaModel);
                    });
                    MediaDAO.Instance.DeleteFromRecentMediasTable(mediaModel.ContentId);
                }
                else
                {
                    ThreadDeleteMediaOrAlbum thrd = new ThreadDeleteMediaOrAlbum(this.mediaModel.ContentId, mediaModel.AlbumId, mediaModel.MediaType);
                    thrd.callBackEvent += (success) =>
                    {
                        if (success == SettingsConstants.RESPONSE_SUCCESS)
                        {
                            if (this.mediaModel.AlbumId != Guid.Empty)
                            {
                                MediaContentModel albumModel = RingIDViewModel.Instance.GetMyAlbumModelFromAlbumID(this.mediaModel.AlbumId, this.mediaModel.MediaType);
                                if (albumModel != null)
                                {
                                    albumModel.TotalMediaCount -= 1;
                                    Application.Current.Dispatcher.Invoke(() =>
                                    {
                                        if (albumModel.MediaList != null && albumModel.MediaList.Any(P => P.ContentId == this.mediaModel.ContentId))
                                            albumModel.MediaList.Remove(this.mediaModel);
                                    });
                                }
                                UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, "Successfully Deleted this Media!");
                            }
                        }
                        else
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                UIHelperMethods.ShowFailed(NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK, "Delete media");
                                // CustomMessageBox.ShowError("Please check your Internet connection or try later!", "Failed!");
                            });
                        }
                    };
                    thrd.StartThread();
                }
            }
        }
        #endregion "ContextMenu"
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

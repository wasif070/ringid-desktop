<<<<<<< HEAD
﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using View.BindingModels;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.ViewModel;

namespace View.UI
{
    public class CustomFeedScroll : ScrollViewer
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CustomFeedScroll).Name);

        private const int NUMBER_OF_CACHED_FEED = 10;

        #region "Initialize"

        public CustomFeedScroll()
        {
            this.Focusable = true;
            this.FocusVisualStyle = null;
            this.PreviewKeyDown += ScrollPreviewKeyDown;
        }

        public void SetScrollValues(CustomFeedCollection viewCollection, CustomSortedList topSortedIds, CustomSortedList bottomSortedIds, int profileType, int action)
        {
            this.ViewCollection = viewCollection;
            this.TopIds = topSortedIds;
            this.BottomIds = bottomSortedIds;
            this.ProfileType = profileType;
            this.Action = action;
            this.UserTableID = viewCollection.UserProfileUtId;
        }

        public void SetScrollValues(ObservableCollection<FeedModel> sliderCollection, CustomFeedCollection viewCollection, CustomSortedList topSortedIds, CustomSortedList bottomSortedIds, int profileType, int action)
        {
            this.SliderFeedCollection = sliderCollection;
            this.ViewCollection = viewCollection;
            this.TopIds = topSortedIds;
            this.BottomIds = bottomSortedIds;
            this.ProfileType = profileType;
            this.Action = action;
            this.UserTableID = viewCollection.UserProfileUtId;
        }

        public void SetScrollValues(ObservableCollection<SingleMediaModel> sliderCollection, CustomFeedCollection viewCollection, CustomSortedList topSortedIds, CustomSortedList bottomSortedIds, int profileType, int action)
        {
            this.SliderMediaCollection = sliderCollection;
            this.ViewCollection = viewCollection;
            this.TopIds = topSortedIds;
            this.BottomIds = bottomSortedIds;
            this.ProfileType = profileType;
            this.Action = action;
            this.UserTableID = viewCollection.UserProfileUtId;
        }
        //
        public void SetScrollValues(ItemsControl itemControls, CustomFeedCollection viewCollection, List<Guid> currentIds, CustomSortedList topSortedIds, CustomSortedList bottomSortedIds, int profileType, int action)
        {
            this.itemControls = itemControls;
            this.ViewCollection = viewCollection;
            this.TopIds = topSortedIds;
            this.CurrentIds = currentIds;
            this.BottomIds = bottomSortedIds;
            this.ProfileType = profileType;
            this.Action = action;
            this.UserTableID = viewCollection.UserProfileUtId;
        }

        public void SetScrollValues(ItemsControl itemControls, ObservableCollection<FeedModel> sliderCollection, CustomFeedCollection viewCollection, List<Guid> currentIds, CustomSortedList topSortedIds, CustomSortedList bottomSortedIds, int profileType, int action)
        {
            this.itemControls = itemControls;
            this.SliderFeedCollection = sliderCollection;
            this.ViewCollection = viewCollection;
            this.TopIds = topSortedIds;
            this.CurrentIds = currentIds;
            this.BottomIds = bottomSortedIds;
            this.ProfileType = profileType;
            this.Action = action;
            this.UserTableID = viewCollection.UserProfileUtId;
        }

        public void SetScrollValues(ItemsControl itemControls, ObservableCollection<SingleMediaModel> sliderCollection, CustomFeedCollection viewCollection, List<Guid> currentIds, CustomSortedList topSortedIds, CustomSortedList bottomSortedIds, int profileType, int action)
        {
            this.itemControls = itemControls;
            this.SliderMediaCollection = sliderCollection;
            this.ViewCollection = viewCollection;
            this.TopIds = topSortedIds;
            this.CurrentIds = currentIds;
            this.BottomIds = bottomSortedIds;
            this.ProfileType = profileType;
            this.Action = action;
            this.UserTableID = viewCollection.UserProfileUtId;
        }
        #endregion

        #region "Variables"

        public bool RequestOn = false;
        public ItemsControl itemControls;
        public CustomFeedCollection ViewCollection;
        public ObservableCollection<FeedModel> SliderFeedCollection;
        public ObservableCollection<SingleMediaModel> SliderMediaCollection;
        private int ProfileType, Action;
        private long UserTableID;
        private CustomSortedList TopIds, BottomIds;
        private List<Guid> CurrentIds;
        public BackgroundWorker bgworker;
        private ConcurrentQueue<FeedHolderModel> Queue = new ConcurrentQueue<FeedHolderModel>();

        #endregion

        #region "Methods"

        private void ScrollPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                this.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                this.ScrollToEnd();
                e.Handled = true;
            }
        }

        public void RequestFeeds(object pvtUUID, short scrollType, int startLimit, string packetId = null)
        {
            if (!RequestOn)
            {
                if (scrollType != 1)
                    ViewCollection.LoadMoreModel.FeedType = SettingsConstants.FEED_TYPE_LOADER_VISIBLE_NO_MORE_TEXT_PANEL_INVISIBLE;
                ThreadAnyFeedsRequest thread = new ThreadAnyFeedsRequest();
                thread.callBackEvent += (response) =>
                {
                    RequestOn = false;
                    if (scrollType != 1)
                    {
                        ViewCollection.DummyModel.FeedType = 12;
                        switch (response)
                        {
                            case SettingsConstants.RESPONSE_SUCCESS:
                                ViewCollection.LoadMoreModel.FeedType = SettingsConstants.FEED_TYPE_LOADER_ADDED_BUT_INVISIBLE;
                                if (BottomIds.Count < 5)
                                    ActionBottomLoad();
                                break;
                            case SettingsConstants.RESPONSE_NOTSUCCESS:
                                ViewCollection.LoadMoreModel.FeedType = SettingsConstants.FEED_TYPE_NO_MORE_TEXT_PANEL_VISIBLE_LOADER_INVISIBLE;
                                scrollEnabled = false;
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    ScrollToBottom();
                                    scrollEnabled = true;
                                }, DispatcherPriority.Send);
                                Thread.Sleep(100);
                                break;
                            case SettingsConstants.NO_RESPONSE:
                                ViewCollection.LoadMoreModel.FeedType = SettingsConstants.FEED_TYPE_RELOAD_BUTTON_VISIBLE;
                                scrollEnabled = false;
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    ScrollToBottom();
                                    scrollEnabled = true;
                                }, DispatcherPriority.Send);
                                Thread.Sleep(100);
                                break;
                        }
                    }
                    else
                    {
                        Thread.Sleep(100);
                        switch (response)
                        {
                            case SettingsConstants.RESPONSE_SUCCESS:
                                //ActionTopLoad();
                                break;
                            default:
                                //if (ViewCollection.LoadMoreModel.FeedType == SettingsConstants.FEED_TYPE_LOADER_VISIBLE_NO_MORE_TEXT_PANEL_INVISIBLE)
                                //    ViewCollection.LoadMoreModel.FeedType = SettingsConstants.FEED_TYPE_RELOAD_BUTTON_VISIBLE;
                                break;
                        }
                    }
                };
                RequestOn = true;
                thread.StartThread(pvtUUID, scrollType, startLimit, ProfileType, Action, packetId, UserTableID, 0);
            }
        }

        private void ActionBottomLoad()
        {
            Guid toInsertFeedId = BottomIds.LastFeedIdRemoved();
            if (toInsertFeedId != Guid.Empty && !CurrentIds.Contains(toInsertFeedId))
            {
                FeedHolderModel holder = new FeedHolderModel(SettingsConstants.FEED_TYPE_NORMAL_FEED);
                holder.FeedId = toInsertFeedId;
                EnqueueForLoad(holder);
            }
            else
            {
                Thread.Sleep(10);
                ViewCollection.DummyModel.FeedType = 11;
            }
        }
        public void ActionTopLoad()
        {
            Guid toInsertFeedId = TopIds.FirstFeedIdRemoved();
            if (toInsertFeedId != Guid.Empty && !CurrentIds.Contains(toInsertFeedId))
            {
                FeedHolderModel holder = new FeedHolderModel(2);
                holder.FeedId = toInsertFeedId;
                EnqueueForLoad(holder);
            }
            else
            {
                //Thread.Sleep(10);
            }
        }
        public void EnqueueForLoad(FeedHolderModel modelToQueue)
        {
            Queue.Enqueue(modelToQueue);
            if (bgworker == null)
            {
                bgworker = new BackgroundWorker();
                bgworker.DoWork += BgWorker_DoWork;
                if (!bgworker.IsBusy)
                    bgworker.RunWorkerAsync();
            }
            else if (!bgworker.IsBusy)
            {
                bgworker.RunWorkerAsync();
            }
        }
        private void BgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                while (!Queue.IsEmpty)
                {
                    FeedHolderModel holder = null;
                    if (Queue.TryDequeue(out holder))
                    {
                        FeedModel fm = null;
                        if (FeedDataContainer.Instance.FeedModels.TryGetValue(holder.FeedId, out fm))
                        {
                            holder.Feed = fm;
                            scrollEnabled = false;
                            CalculateandInsertAllFeeds(holder);
                            scrollEnabled = true;
                            holder.ShortModelType = SettingsConstants.FEED_PARTIAL_VIEW;
                            holder.SelectViewTemplate();
                        }
                        else ViewCollection.InvokeRemove(holder);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace);
            }
        }
        public void CalculateandInsertAllFeeds(FeedHolderModel model)
        {
            try
            {
                int max = ViewCollection.Count - 2;
                int pivot, min = 1;
                while (max > min)
                {
                    pivot = (min + max) / 2;

                    if (model.Feed != null && ViewCollection[pivot].Feed != null && model.Feed.Time < ViewCollection[pivot].Feed.Time)
                    {
                        min = pivot + 1;
                    }
                    else
                    {
                        max = pivot;
                    }
                }
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    try
                    {
                        lock (ViewCollection)
                        {
                            ViewCollection.Insert(min, model);
                            if (!CurrentIds.Contains(model.FeedId))
                                CurrentIds.Add(model.FeedId);
                        }
                    }
                    catch (Exception ex) { log.Error("Error: InViewCollection<Insert> ." + ex.Message + "\n" + ex.StackTrace); }
                }, DispatcherPriority.Send);
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }
        public bool StopRemoving = false;
        public bool scrollEnabled = true;
        private void ScrollPositionChanged(object sender, ScrollChangedEventArgs e)
        {
            if (scrollEnabled && e.VerticalChange != 0)
            {
                double offset = this.VerticalOffset + (e.VerticalChange * 3 / 6);
                if (offset < 0) offset = 0;
                else if (offset > this.ScrollableHeight - 30) offset = this.ScrollableHeight - 5;
                if (e.VerticalChange > 0)
                {
                    if (ViewCollection.LoadMoreModel != null)
                    {
                        if (ViewCollection.LoadMoreModel.FeedType != 4 && ViewCollection.LoadMoreModel.FeedType != 5 && BottomIds.Count < 10)
                        {
                            RequestFeeds(BottomIds.GetPvtMin(), 2, 0);
                        }
                        if ((ViewCollection.LoadMoreModel.FeedType != SettingsConstants.FEED_TYPE_NO_MORE_TEXT_PANEL_VISIBLE_LOADER_INVISIBLE
                            && ViewCollection.LoadMoreModel.FeedType != SettingsConstants.FEED_TYPE_RELOAD_BUTTON_VISIBLE) || BottomIds.Count > 0)
                        {
                            if (this.VerticalOffset >= (this.ScrollableHeight * 0.60))
                            {
                                new Task(delegate
                                {
                                    ActionBottomLoad();
                                }).Start();
                            }
                        }
                    }
                }
                else if (e.VerticalChange < 0)
                {
                    if (this.VerticalOffset <= (this.ScrollableHeight * 0.50))
                    {
                        if (!TopIds.IsSavedFeed)
                        {
                            if (TopIds.PvtMaxGuid != Guid.Empty)
                                TopIds.PvtMaxGuid = HelperMethods.GetMaxMinGuidInFeedCollection(ViewCollection, ViewCollection.MinIndex);
                            RequestFeeds(TopIds.PvtMaxGuid, 1, 0);
                        }
                        else
                        {
                            if (TopIds.PvtMaxLong < 0 && ViewCollection.Count > 0)
                                TopIds.PvtMaxLong = ViewCollection.Where(y => y.FeedType == SettingsConstants.FEED_TYPE_NORMAL_FEED).Max(y => y.Feed.SavedTime);
                            RequestFeeds(TopIds.PvtMaxLong, 1, 0);
                        }
                        new Task(delegate
                        {
                            ActionTopLoad();
                        }).Start();
                        if (offset == 0)
                        {
                            DropAllKeeping1To20();
                        }
                    }
                }
                if (offset < (this.ScrollableHeight / 2))
                {
                    if (!IsInUpperHalf
                        //|| ViewCollection.Count > 200
                        )
                    {
                        IsInUpperHalf = true;
                        //LoadShortOrDetailsFeedModels();
                    }
                }
                else
                {
                    if (IsInUpperHalf
                        //|| ViewCollection.Count > 200
                        )
                    {
                        IsInUpperHalf = false;
                        //LoadShortOrDetailsFeedModels();
                    }
                }
                this.ScrollToVerticalOffset(offset);
                e.Handled = true;
                scrollEnabled = true;
                LoadShortOrDetailsFeedModels();
            }
        }
        public void DropAllKeeping1To20(bool fromHomeUnloader = false)
        {
            Task unloadBottomData = new Task(delegate
            {
                try
                {
                    scrollEnabled = false;
                    StopRemoving = false;
                    int idxStarts = ((ViewCollection.Count - 3) > 10) ? 10 : ViewCollection.Count - 3;
                    for (int i = ViewCollection.Count - 3; i > 0; i--)
                    {
                        if (i > idxStarts)
                        {
                            if (!StopRemoving && i < ViewCollection.Count - 1)
                            {
                                FeedHolderModel fm = ViewCollection[i];
                                ViewCollection.InvokeRemove(fm);
                                CurrentIds.Remove(fm.FeedId);
                                if (fm.Feed != null) BottomIds.InsertIntoSortedList(fm.Feed.Time, fm.FeedId);
                            }
                        }
                        else
                        {
                            ViewCollection[i].ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                        }
                    }
                    foreach (var item in TopIds)
                    {
                        Guid toInsertFeedId = item.Value;
                        FeedModel feedModel = null;
                        if (!CurrentIds.Contains(toInsertFeedId)
                            && FeedDataContainer.Instance.FeedModels.TryGetValue(toInsertFeedId, out feedModel))
                        {
                            FeedHolderModel holder = new FeedHolderModel(SettingsConstants.FEED_TYPE_NORMAL_FEED);
                            holder.FeedId = toInsertFeedId;
                            holder.Feed = feedModel;
                            holder.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                            holder.SelectViewTemplate();
                            CalculateandInsertAllFeeds(holder);
                        }
                    }
                    Thread.Sleep(100);
                    scrollEnabled = true;
                }
                catch (Exception ex)
                {
                    scrollEnabled = true;
                    log.Error("Error: DropAllKeeping1To20" + ex.Message + "\n" + ex.StackTrace);
                }
            });
            unloadBottomData.Start();
        }
        public void OnIsVisibleChanged(bool visible)
        {
            try
            {
                this.ScrollChanged -= ScrollPositionChanged;//scrollenabledlater
                if (visible)
                {
                    StopRemoving = true;
                    this.ScrollChanged += ScrollPositionChanged;
                    if (!UCGuiRingID.Instance.IsAnyWindowAbove())
                        Keyboard.Focus(this);
                }
                else
                {
                    this.ScrollChanged -= ScrollPositionChanged;
                    if (ShortOrDetailsModelLoad != null && ShortOrDetailsModelLoad.IsEnabled)
                        ShortOrDetailsModelLoad.Stop();
                    if (ViewCollection.Count > NUMBER_OF_CACHED_FEED || (SliderFeedCollection != null && SliderFeedCollection.Count > 0)
                        || (SliderMediaCollection != null && SliderMediaCollection.Count > 0))
                    {
                        new Task(delegate
                        {
                            Thread.Sleep(300);
                            for (int i = ViewCollection.Count - 1; i >= NUMBER_OF_CACHED_FEED + 1; i--)
                            {
                                FeedHolderModel fm = ViewCollection[i];
                                if (fm.FeedType == SettingsConstants.FEED_TYPE_NORMAL_FEED)
                                {
                                    if (fm.Feed != null)
                                        BottomIds.InsertIntoSortedList(fm.Feed.ActualTime, fm.FeedId);
                                    ViewCollection.InvokeRemove(fm);
                                }
                            }
                            if (SliderFeedCollection != null && SliderFeedCollection.Count > 0)
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    SliderFeedCollection.Clear();
                                }, DispatcherPriority.Send);
                            else if (SliderMediaCollection != null && SliderMediaCollection.Count > 0)
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    SliderMediaCollection.Clear();
                                }, DispatcherPriority.Send);
                        }).Start();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace);
            }
        }
        public void LoadShortOrDetailsFeedModels()
        {
            try
            {
                if (ShortOrDetailsModelLoad == null)
                {
                    ShortOrDetailsModelLoad = new DispatcherTimer();
                    ShortOrDetailsModelLoad.Interval = TimeSpan.FromSeconds(3);
                    ShortOrDetailsModelLoad.Tick += timer_Tick;
                }
                if (!ShortOrDetailsModelLoad.IsEnabled)
                    ShortOrDetailsModelLoad.Start();
            }
            catch (Exception ex)
            {

                log.Error("Error: ChangeOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        DispatcherTimer ShortOrDetailsModelLoad;
        public bool IsInUpperHalf = true;
        void timer_Tick(object sender, object e)
        {
            try
            {
                if (itemControls != null)
                {
                    Rect scrollRectangle = new Rect(new Point(0, 0), this.RenderSize);
                    if (IsInUpperHalf)
                    {
                        for (int i = 0; i < itemControls.Items.Count - 1; i++)
                        {
                            if ((i + 1) < (itemControls.Items.Count - 1))
                            {
                                var nextChildPresenter = (ContentPresenter)itemControls.ItemContainerGenerator.ContainerFromIndex(i + 1);//idx bound check necess
                                if (nextChildPresenter == null) break;
                                if (nextChildPresenter.Content != null && nextChildPresenter.Content is FeedHolderModel)
                                {
                                    FeedHolderModel nextModel = (FeedHolderModel)nextChildPresenter.Content;
                                    if (nextModel.FeedType == 2)
                                    {
                                        Rect nextChildRectangle = nextChildPresenter.TransformToAncestor(this).TransformBounds(new Rect(new Point(0, 0), nextChildPresenter.RenderSize));
                                        bool isNextFeedModelInView = scrollRectangle.IntersectsWith(nextChildRectangle);
                                        if (isNextFeedModelInView) //current &nextnext?? make visible
                                        {
                                            nextModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                            var currentChildPresenter = (ContentPresenter)itemControls.ItemContainerGenerator.ContainerFromIndex(i);//opt check
                                            if (currentChildPresenter == null) break;
                                            if (currentChildPresenter.Content != null && currentChildPresenter.Content is FeedHolderModel)
                                            {
                                                FeedHolderModel currentModel = (FeedHolderModel)currentChildPresenter.Content;
                                                if (currentModel.FeedType == 2)
                                                {
                                                    currentModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                                }
                                            }
                                            if ((i + 2) < (itemControls.Items.Count - 1))
                                            {
                                                var nnChildPresenter = (ContentPresenter)itemControls.ItemContainerGenerator.ContainerFromIndex(i + 2); //idx bound check necess
                                                if (nnChildPresenter == null) break;
                                                if (nnChildPresenter.Content != null && nnChildPresenter.Content is FeedHolderModel)
                                                {
                                                    FeedHolderModel nnModel = (FeedHolderModel)nnChildPresenter.Content;
                                                    if (nnModel.FeedType == 2)
                                                    {
                                                        nnModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                                    }
                                                }
                                            }
                                            i = i + 1;
                                        }
                                        else
                                        {
                                            nextModel.ShortModelType = SettingsConstants.FEED_PARTIAL_VIEW;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = itemControls.Items.Count - 1; i > 0; i--)
                        {
                            if ((i - 1) > 0)
                            {
                                var prevChildPresenter = (ContentPresenter)itemControls.ItemContainerGenerator.ContainerFromIndex(i - 1);
                                if (prevChildPresenter == null) break;
                                if (prevChildPresenter.Content != null && prevChildPresenter.Content is FeedHolderModel)
                                {
                                    FeedHolderModel prevModel = (FeedHolderModel)prevChildPresenter.Content;
                                    if (prevModel.FeedType == 2)
                                    {
                                        Rect prevChildRectangle = prevChildPresenter.TransformToAncestor(this).TransformBounds(new Rect(new Point(0, 0), prevChildPresenter.RenderSize));
                                        bool isPrevFeedModelInView = scrollRectangle.IntersectsWith(prevChildRectangle);
                                        if (isPrevFeedModelInView) //current &prevprev?? make visible
                                        {
                                            prevModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                            var currentChildPresenter = (ContentPresenter)itemControls.ItemContainerGenerator.ContainerFromIndex(i);
                                            if (currentChildPresenter == null) break;
                                            if (currentChildPresenter.Content != null && currentChildPresenter.Content is FeedHolderModel)
                                            {
                                                FeedHolderModel currentModel = (FeedHolderModel)currentChildPresenter.Content;
                                                if (currentModel.FeedType == 2)
                                                {
                                                    currentModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                                }
                                            }
                                            if ((i - 2) > 0)
                                            {
                                                var ppChildPresenter = (ContentPresenter)itemControls.ItemContainerGenerator.ContainerFromIndex(i - 2);
                                                if (ppChildPresenter == null) break;
                                                if (ppChildPresenter.Content != null && ppChildPresenter.Content is FeedHolderModel)
                                                {
                                                    FeedHolderModel ppModel = (FeedHolderModel)ppChildPresenter.Content;
                                                    if (ppModel.FeedType == 2)
                                                    {
                                                        ppModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                                    }
                                                }
                                            }
                                            i = i - 1;
                                        }
                                        else
                                        {
                                            prevModel.ShortModelType = SettingsConstants.FEED_PARTIAL_VIEW;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                ShortOrDetailsModelLoad.Stop();
            }
            catch (Exception)
            {
            }
        }
        #endregion
    }
}
=======
﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using View.BindingModels;
using View.Utility;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.ViewModel;

namespace View.UI
{
    public class CustomFeedScroll : ScrollViewer
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CustomFeedScroll).Name);

        private const int NUMBER_OF_CACHED_FEED = 10;

        #region "Initialize"

        public CustomFeedScroll()
        {
            this.Focusable = true;
            this.FocusVisualStyle = null;
            this.PreviewKeyDown += ScrollPreviewKeyDown;
        }

        public void SetScrollValues(CustomFeedCollection viewCollection, CustomSortedList topSortedIds, CustomSortedList bottomSortedIds, int profileType, int action)
        {
            this.ViewCollection = viewCollection;
            this.TopIds = topSortedIds;
            this.BottomIds = bottomSortedIds;
            this.ProfileType = profileType;
            this.Action = action;
            this.UserTableID = viewCollection.UserProfileUtId;
        }

        public void SetScrollValues(ObservableCollection<FeedModel> sliderCollection, CustomFeedCollection viewCollection, CustomSortedList topSortedIds, CustomSortedList bottomSortedIds, int profileType, int action)
        {
            this.SliderFeedCollection = sliderCollection;
            this.ViewCollection = viewCollection;
            this.TopIds = topSortedIds;
            this.BottomIds = bottomSortedIds;
            this.ProfileType = profileType;
            this.Action = action;
            this.UserTableID = viewCollection.UserProfileUtId;
        }

        public void SetScrollValues(ObservableCollection<SingleMediaModel> sliderCollection, CustomFeedCollection viewCollection, CustomSortedList topSortedIds, CustomSortedList bottomSortedIds, int profileType, int action)
        {
            this.SliderMediaCollection = sliderCollection;
            this.ViewCollection = viewCollection;
            this.TopIds = topSortedIds;
            this.BottomIds = bottomSortedIds;
            this.ProfileType = profileType;
            this.Action = action;
            this.UserTableID = viewCollection.UserProfileUtId;
        }
        //
        public void SetScrollValues(ItemsControl itemControls, CustomFeedCollection viewCollection, List<Guid> currentIds, CustomSortedList topSortedIds, CustomSortedList bottomSortedIds, int profileType, int action)
        {
            this.itemControls = itemControls;
            this.ViewCollection = viewCollection;
            this.TopIds = topSortedIds;
            this.CurrentIds = currentIds;
            this.BottomIds = bottomSortedIds;
            this.ProfileType = profileType;
            this.Action = action;
            this.UserTableID = viewCollection.UserProfileUtId;
        }

        public void SetScrollValues(ItemsControl itemControls, ObservableCollection<FeedModel> sliderCollection, CustomFeedCollection viewCollection, List<Guid> currentIds, CustomSortedList topSortedIds, CustomSortedList bottomSortedIds, int profileType, int action)
        {
            this.itemControls = itemControls;
            this.SliderFeedCollection = sliderCollection;
            this.ViewCollection = viewCollection;
            this.TopIds = topSortedIds;
            this.CurrentIds = currentIds;
            this.BottomIds = bottomSortedIds;
            this.ProfileType = profileType;
            this.Action = action;
            this.UserTableID = viewCollection.UserProfileUtId;
        }

        public void SetScrollValues(ItemsControl itemControls, ObservableCollection<SingleMediaModel> sliderCollection, CustomFeedCollection viewCollection, List<Guid> currentIds, CustomSortedList topSortedIds, CustomSortedList bottomSortedIds, int profileType, int action)
        {
            this.itemControls = itemControls;
            this.SliderMediaCollection = sliderCollection;
            this.ViewCollection = viewCollection;
            this.TopIds = topSortedIds;
            this.CurrentIds = currentIds;
            this.BottomIds = bottomSortedIds;
            this.ProfileType = profileType;
            this.Action = action;
            this.UserTableID = viewCollection.UserProfileUtId;
        }
        #endregion

        #region "Variables"

        public bool RequestOn = false;
        public ItemsControl itemControls;
        public CustomFeedCollection ViewCollection;
        public ObservableCollection<FeedModel> SliderFeedCollection;
        public ObservableCollection<SingleMediaModel> SliderMediaCollection;
        private int ProfileType, Action;
        private long UserTableID;
        private CustomSortedList TopIds, BottomIds;
        private List<Guid> CurrentIds;
        public BackgroundWorker bgworker;
        private ConcurrentQueue<FeedHolderModel> Queue = new ConcurrentQueue<FeedHolderModel>();

        #endregion

        #region "Methods"

        private void ScrollPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Home)
            {
                this.ScrollToHome();
                e.Handled = true;
            }
            else if (e.Key == Key.End)
            {
                this.ScrollToEnd();
                e.Handled = true;
            }
        }

        public void RequestFeeds(object pvtUUID, short scrollType, int startLimit, string packetId = null)
        {
            if (!RequestOn)
            {
                if (scrollType != 1)
                    ViewCollection.LoadMoreModel.FeedType = SettingsConstants.FEED_TYPE_LOADER_VISIBLE_NO_MORE_TEXT_PANEL_INVISIBLE;
                ThreadAnyFeedsRequest thread = new ThreadAnyFeedsRequest();
                thread.callBackEvent += (response) =>
                {
                    RequestOn = false;
                    if (scrollType != 1)
                    {
                        ViewCollection.DummyModel.FeedType = 12;
                        switch (response)
                        {
                            case SettingsConstants.RESPONSE_SUCCESS:
                                ViewCollection.LoadMoreModel.FeedType = SettingsConstants.FEED_TYPE_LOADER_ADDED_BUT_INVISIBLE;
                                if (BottomIds.Count < 5)
                                    ActionBottomLoad();
                                break;
                            case SettingsConstants.RESPONSE_NOTSUCCESS:
                                ViewCollection.LoadMoreModel.FeedType = SettingsConstants.FEED_TYPE_NO_MORE_TEXT_PANEL_VISIBLE_LOADER_INVISIBLE;
                                scrollEnabled = false;
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    ScrollToBottom();
                                    scrollEnabled = true;
                                }, DispatcherPriority.Send);
                                Thread.Sleep(100);
                                break;
                            case SettingsConstants.NO_RESPONSE:
                                ViewCollection.LoadMoreModel.FeedType = SettingsConstants.FEED_TYPE_RELOAD_BUTTON_VISIBLE;
                                scrollEnabled = false;
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    ScrollToBottom();
                                    scrollEnabled = true;
                                }, DispatcherPriority.Send);
                                Thread.Sleep(100);
                                break;
                        }
                    }
                    else
                    {
                        Thread.Sleep(100);
                        switch (response)
                        {
                            case SettingsConstants.RESPONSE_SUCCESS:
                                //ActionTopLoad();
                                break;
                            default:
                                //if (ViewCollection.LoadMoreModel.FeedType == SettingsConstants.FEED_TYPE_LOADER_VISIBLE_NO_MORE_TEXT_PANEL_INVISIBLE)
                                //    ViewCollection.LoadMoreModel.FeedType = SettingsConstants.FEED_TYPE_RELOAD_BUTTON_VISIBLE;
                                break;
                        }
                    }
                };
                RequestOn = true;
                thread.StartThread(pvtUUID, scrollType, startLimit, ProfileType, Action, packetId, UserTableID, 0);
            }
        }

        private void ActionBottomLoad()
        {
            Guid toInsertFeedId = BottomIds.LastFeedIdRemoved();
            if (toInsertFeedId != Guid.Empty && !CurrentIds.Contains(toInsertFeedId))
            {
                FeedHolderModel holder = new FeedHolderModel(SettingsConstants.FEED_TYPE_NORMAL_FEED);
                holder.FeedId = toInsertFeedId;
                EnqueueForLoad(holder);
            }
            else
            {
                Thread.Sleep(10);
                ViewCollection.DummyModel.FeedType = 11;
            }
        }
        public void ActionTopLoad()
        {
            Guid toInsertFeedId = TopIds.FirstFeedIdRemoved();
            if (toInsertFeedId != Guid.Empty && !CurrentIds.Contains(toInsertFeedId))
            {
                FeedHolderModel holder = new FeedHolderModel(2);
                holder.FeedId = toInsertFeedId;
                EnqueueForLoad(holder);
            }
            else
            {
                //Thread.Sleep(10);
            }
        }
        public void EnqueueForLoad(FeedHolderModel modelToQueue)
        {
            Queue.Enqueue(modelToQueue);
            if (bgworker == null)
            {
                bgworker = new BackgroundWorker();
                bgworker.DoWork += BgWorker_DoWork;
                if (!bgworker.IsBusy)
                    bgworker.RunWorkerAsync();
            }
            else if (!bgworker.IsBusy)
            {
                bgworker.RunWorkerAsync();
            }
        }
        private void BgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                while (!Queue.IsEmpty)
                {
                    FeedHolderModel holder = null;
                    if (Queue.TryDequeue(out holder))
                    {
                        FeedModel fm = null;
                        if (FeedDataContainer.Instance.FeedModels.TryGetValue(holder.FeedId, out fm))
                        {
                            holder.Feed = fm;
                            scrollEnabled = false;
                            CalculateandInsertAllFeeds(holder);
                            scrollEnabled = true;
                            holder.ShortModelType = SettingsConstants.FEED_PARTIAL_VIEW;
                            holder.SelectViewTemplate();
                        }
                        else ViewCollection.InvokeRemove(holder);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace);
            }
        }
        public void CalculateandInsertAllFeeds(FeedHolderModel model)
        {
            try
            {
                int max = ViewCollection.Count - 2;
                int pivot, min = 1;
                while (max > min)
                {
                    pivot = (min + max) / 2;

                    if (model.Feed != null && ViewCollection[pivot].Feed != null && model.Feed.Time < ViewCollection[pivot].Feed.Time)
                    {
                        min = pivot + 1;
                    }
                    else
                    {
                        max = pivot;
                    }
                }
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    try
                    {
                        lock (ViewCollection)
                        {
                            ViewCollection.Insert(min, model);
                            if (!CurrentIds.Contains(model.FeedId))
                                CurrentIds.Add(model.FeedId);
                        }
                    }
                    catch (Exception ex) { log.Error("Error: InViewCollection<Insert> ." + ex.Message + "\n" + ex.StackTrace); }
                }, DispatcherPriority.Send);
            }
            catch (Exception e)
            {
                log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
            }
        }
        public bool StopRemoving = false;
        public bool scrollEnabled = true;
        private void ScrollPositionChanged(object sender, ScrollChangedEventArgs e)
        {
            if (scrollEnabled && e.VerticalChange != 0)
            {
                double offset = this.VerticalOffset + (e.VerticalChange * 3 / 6);
                if (offset < 0) offset = 0;
                else if (offset > this.ScrollableHeight - 30) offset = this.ScrollableHeight - 5;
                if (e.VerticalChange > 0)
                {
                    if (ViewCollection.LoadMoreModel != null)
                    {
                        if (ViewCollection.LoadMoreModel.FeedType != 4 && ViewCollection.LoadMoreModel.FeedType != 5 && BottomIds.Count < 10)
                        {
                            RequestFeeds(BottomIds.GetPvtMin(), 2, 0);
                        }
                        if ((ViewCollection.LoadMoreModel.FeedType != SettingsConstants.FEED_TYPE_NO_MORE_TEXT_PANEL_VISIBLE_LOADER_INVISIBLE
                            && ViewCollection.LoadMoreModel.FeedType != SettingsConstants.FEED_TYPE_RELOAD_BUTTON_VISIBLE) || BottomIds.Count > 0)
                        {
                            if (this.VerticalOffset >= (this.ScrollableHeight * 0.60))
                            {
                                new Task(delegate
                                {
                                    ActionBottomLoad();
                                }).Start();
                            }
                        }
                    }
                }
                else if (e.VerticalChange < 0)
                {
                    if (this.VerticalOffset <= (this.ScrollableHeight * 0.50))
                    {
                        if (!TopIds.IsSavedFeed)
                        {
                            if (TopIds.PvtMaxGuid != Guid.Empty)
                                TopIds.PvtMaxGuid = HelperMethods.GetMaxMinGuidInFeedCollection(ViewCollection, ViewCollection.MinIndex);
                            RequestFeeds(TopIds.PvtMaxGuid, 1, 0);
                        }
                        else
                        {
                            try
                            {
                                if (TopIds.PvtMaxLong < 0 && ViewCollection.Count > 0)
                                    TopIds.PvtMaxLong = ViewCollection.Where(y => y.FeedType == SettingsConstants.FEED_TYPE_NORMAL_FEED).Max(y => y.Feed.SavedTime);
                            }
                            catch (Exception)
                            {
                            }
                            RequestFeeds(TopIds.PvtMaxLong, 1, 0);
                        }
                        new Task(delegate
                        {
                            ActionTopLoad();
                        }).Start();
                        if (offset == 0)
                        {
                            DropAllKeeping1To20();
                        }
                    }
                }
                if (offset < (this.ScrollableHeight / 2))
                {
                    if (!IsInUpperHalf
                        //|| ViewCollection.Count > 200
                        )
                    {
                        IsInUpperHalf = true;
                        //LoadShortOrDetailsFeedModels();
                    }
                }
                else
                {
                    if (IsInUpperHalf
                        //|| ViewCollection.Count > 200
                        )
                    {
                        IsInUpperHalf = false;
                        //LoadShortOrDetailsFeedModels();
                    }
                }
                this.ScrollToVerticalOffset(offset);
                e.Handled = true;
                scrollEnabled = true;
                LoadShortOrDetailsFeedModels();
            }
        }
        public void DropAllKeeping1To20(bool fromHomeUnloader = false)
        {
            Task unloadBottomData = new Task(delegate
            {
                try
                {
                    scrollEnabled = false;
                    StopRemoving = false;
                    int idxStarts = ((ViewCollection.Count - 3) > 10) ? 10 : ViewCollection.Count - 3;
                    for (int i = ViewCollection.Count - 3; i > 0; i--)
                    {
                        if (i > idxStarts)
                        {
                            if (!StopRemoving && i < ViewCollection.Count - 1)
                            {
                                FeedHolderModel fm = ViewCollection[i];
                                ViewCollection.InvokeRemove(fm);
                                CurrentIds.Remove(fm.FeedId);
                                if (fm.Feed != null) BottomIds.InsertIntoSortedList(fm.Feed.Time, fm.FeedId);
                            }
                        }
                        else
                        {
                            ViewCollection[i].ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                        }
                    }
                    foreach (var item in TopIds)
                    {
                        Guid toInsertFeedId = item.Value;
                        FeedModel feedModel = null;
                        if (!CurrentIds.Contains(toInsertFeedId)
                            && FeedDataContainer.Instance.FeedModels.TryGetValue(toInsertFeedId, out feedModel))
                        {
                            FeedHolderModel holder = new FeedHolderModel(SettingsConstants.FEED_TYPE_NORMAL_FEED);
                            holder.FeedId = toInsertFeedId;
                            holder.Feed = feedModel;
                            holder.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                            holder.SelectViewTemplate();
                            CalculateandInsertAllFeeds(holder);
                        }
                    }
                    Thread.Sleep(100);
                    scrollEnabled = true;
                }
                catch (Exception ex)
                {
                    scrollEnabled = true;
                    log.Error("Error: DropAllKeeping1To20" + ex.Message + "\n" + ex.StackTrace);
                }
            });
            unloadBottomData.Start();
        }
        public void OnIsVisibleChanged(bool visible)
        {
            try
            {
                this.ScrollChanged -= ScrollPositionChanged;//scrollenabledlater
                if (visible)
                {
                    StopRemoving = true;
                    this.ScrollChanged += ScrollPositionChanged;
                    if (!UCGuiRingID.Instance.IsAnyWindowAbove())
                        Keyboard.Focus(this);
                }
                else
                {
                    this.ScrollChanged -= ScrollPositionChanged;
                    if (ShortOrDetailsModelLoad != null && ShortOrDetailsModelLoad.IsEnabled)
                        ShortOrDetailsModelLoad.Stop();
                    if (ViewCollection.Count > NUMBER_OF_CACHED_FEED || (SliderFeedCollection != null && SliderFeedCollection.Count > 0)
                        || (SliderMediaCollection != null && SliderMediaCollection.Count > 0))
                    {
                        new Task(delegate
                        {
                            Thread.Sleep(300);
                            for (int i = ViewCollection.Count - 1; i >= NUMBER_OF_CACHED_FEED + 1; i--)
                            {
                                FeedHolderModel fm = ViewCollection[i];
                                if (fm.FeedType == SettingsConstants.FEED_TYPE_NORMAL_FEED)
                                {
                                    if (fm.Feed != null)
                                        BottomIds.InsertIntoSortedList(fm.Feed.ActualTime, fm.FeedId);
                                    ViewCollection.InvokeRemove(fm);
                                }
                            }
                            if (SliderFeedCollection != null && SliderFeedCollection.Count > 0)
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    SliderFeedCollection.Clear();
                                }, DispatcherPriority.Send);
                            else if (SliderMediaCollection != null && SliderMediaCollection.Count > 0)
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    SliderMediaCollection.Clear();
                                }, DispatcherPriority.Send);
                        }).Start();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace);
            }
        }
        public void LoadShortOrDetailsFeedModels()
        {
            try
            {
                if (ShortOrDetailsModelLoad == null)
                {
                    ShortOrDetailsModelLoad = new DispatcherTimer();
                    ShortOrDetailsModelLoad.Interval = TimeSpan.FromSeconds(3);
                    ShortOrDetailsModelLoad.Tick += timer_Tick;
                }
                if (!ShortOrDetailsModelLoad.IsEnabled)
                    ShortOrDetailsModelLoad.Start();
            }
            catch (Exception ex)
            {

                log.Error("Error: ChangeOpenStatus() ==> " + ex.Message + "\n" + ex.StackTrace);

            }
        }
        DispatcherTimer ShortOrDetailsModelLoad;
        public bool IsInUpperHalf = true;
        void timer_Tick(object sender, object e)
        {
            try
            {
                if (itemControls != null)
                {
                    Rect scrollRectangle = new Rect(new Point(0, 0), this.RenderSize);
                    if (IsInUpperHalf)
                    {
                        for (int i = 0; i < itemControls.Items.Count - 1; i++)
                        {
                            if ((i + 1) < (itemControls.Items.Count - 1))
                            {
                                var nextChildPresenter = (ContentPresenter)itemControls.ItemContainerGenerator.ContainerFromIndex(i + 1);//idx bound check necess
                                if (nextChildPresenter == null) break;
                                if (nextChildPresenter.Content != null && nextChildPresenter.Content is FeedHolderModel)
                                {
                                    FeedHolderModel nextModel = (FeedHolderModel)nextChildPresenter.Content;
                                    if (nextModel.FeedType == 2)
                                    {
                                        Rect nextChildRectangle = nextChildPresenter.TransformToAncestor(this).TransformBounds(new Rect(new Point(0, 0), nextChildPresenter.RenderSize));
                                        bool isNextFeedModelInView = scrollRectangle.IntersectsWith(nextChildRectangle);
                                        if (isNextFeedModelInView) //current &nextnext?? make visible
                                        {
                                            nextModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                            var currentChildPresenter = (ContentPresenter)itemControls.ItemContainerGenerator.ContainerFromIndex(i);//opt check
                                            if (currentChildPresenter == null) break;
                                            if (currentChildPresenter.Content != null && currentChildPresenter.Content is FeedHolderModel)
                                            {
                                                FeedHolderModel currentModel = (FeedHolderModel)currentChildPresenter.Content;
                                                if (currentModel.FeedType == 2)
                                                {
                                                    currentModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                                }
                                            }
                                            if ((i + 2) < (itemControls.Items.Count - 1))
                                            {
                                                var nnChildPresenter = (ContentPresenter)itemControls.ItemContainerGenerator.ContainerFromIndex(i + 2); //idx bound check necess
                                                if (nnChildPresenter == null) break;
                                                if (nnChildPresenter.Content != null && nnChildPresenter.Content is FeedHolderModel)
                                                {
                                                    FeedHolderModel nnModel = (FeedHolderModel)nnChildPresenter.Content;
                                                    if (nnModel.FeedType == 2)
                                                    {
                                                        nnModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                                    }
                                                }
                                            }
                                            i = i + 1;
                                        }
                                        else
                                        {
                                            nextModel.ShortModelType = SettingsConstants.FEED_PARTIAL_VIEW;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = itemControls.Items.Count - 1; i > 0; i--)
                        {
                            if ((i - 1) > 0)
                            {
                                var prevChildPresenter = (ContentPresenter)itemControls.ItemContainerGenerator.ContainerFromIndex(i - 1);
                                if (prevChildPresenter == null) break;
                                if (prevChildPresenter.Content != null && prevChildPresenter.Content is FeedHolderModel)
                                {
                                    FeedHolderModel prevModel = (FeedHolderModel)prevChildPresenter.Content;
                                    if (prevModel.FeedType == 2)
                                    {
                                        Rect prevChildRectangle = prevChildPresenter.TransformToAncestor(this).TransformBounds(new Rect(new Point(0, 0), prevChildPresenter.RenderSize));
                                        bool isPrevFeedModelInView = scrollRectangle.IntersectsWith(prevChildRectangle);
                                        if (isPrevFeedModelInView) //current &prevprev?? make visible
                                        {
                                            prevModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                            var currentChildPresenter = (ContentPresenter)itemControls.ItemContainerGenerator.ContainerFromIndex(i);
                                            if (currentChildPresenter == null) break;
                                            if (currentChildPresenter.Content != null && currentChildPresenter.Content is FeedHolderModel)
                                            {
                                                FeedHolderModel currentModel = (FeedHolderModel)currentChildPresenter.Content;
                                                if (currentModel.FeedType == 2)
                                                {
                                                    currentModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                                }
                                            }
                                            if ((i - 2) > 0)
                                            {
                                                var ppChildPresenter = (ContentPresenter)itemControls.ItemContainerGenerator.ContainerFromIndex(i - 2);
                                                if (ppChildPresenter == null) break;
                                                if (ppChildPresenter.Content != null && ppChildPresenter.Content is FeedHolderModel)
                                                {
                                                    FeedHolderModel ppModel = (FeedHolderModel)ppChildPresenter.Content;
                                                    if (ppModel.FeedType == 2)
                                                    {
                                                        ppModel.ShortModelType = SettingsConstants.FEED_FULL_VIEW;
                                                    }
                                                }
                                            }
                                            i = i - 1;
                                        }
                                        else
                                        {
                                            prevModel.ShortModelType = SettingsConstants.FEED_PARTIAL_VIEW;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                ShortOrDetailsModelLoad.Stop();
            }
            catch (Exception)
            {
            }
        }
        #endregion
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

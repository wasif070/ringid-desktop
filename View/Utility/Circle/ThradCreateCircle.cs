﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using System.Windows;
using View.Utility.DataContainer;

namespace View.Utility.Circle
{
    class ThradCreateCircle
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradCreateCircle).Name);
        private bool running = false;
        private CircleModel circleModel = null;
        private string msg, circleNm;
        private JArray jArr;

        #endregion "Private Fields"

        #region "Constructors"
        public ThradCreateCircle(JArray jArr, string circleNm)
        {
            this.jArr = jArr;
            this.circleNm = circleNm;
        }

        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                String packetId = SendToServer.GetRanDomPacketID();
                JObject pakToSend = new JObject();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_CREATE_CIRCLE;
                pakToSend[JsonKeys.GroupName] = circleNm;
                pakToSend[JsonKeys.GroupMembers] = jArr;
                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                    {
                        msg = null;
                        circleModel = new CircleModel();
                        circleModel.UserTableID = (long)feedbackfields[JsonKeys.GroupId];
                        circleModel.UpdateTime = (long)feedbackfields[JsonKeys.UpdateTime];
                        circleModel.FullName = circleNm;
                        circleModel.MemberCount = jArr.Count + 1;
                        circleModel.AdminCount++;
                        circleModel.SuperAdmin = DefaultSettings.LOGIN_RING_ID;
                        circleModel.UserTableID = DefaultSettings.LOGIN_TABLE_ID;
                        circleModel.VisibilityLeaveCircle = Visibility.Collapsed;
                        circleModel.VisibilityDeleteCircle = Visibility.Visible;

                        lock (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY)
                        {
                            CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY[circleModel.UserTableID] = circleModel;
                        }

                        List<CircleDTO> list = new List<CircleDTO>();
                        CircleDTO circleDTO = circleModel.GetCircleDTO(circleModel);
                        list.Add(circleDTO);
                        new InsertIntoCircleListTable(list);
                        String str = string.Format(NotificationMessages.MSG_CREATE_CIRCLE, circleModel.FullName);
                       UIHelperMethods.ShowTimerMessageBox(str, "");
                    }
                    else
                    {
                        msg = (feedbackfields[JsonKeys.Message] != null) ? (string)feedbackfields[JsonKeys.Message] : "Failed to create circle!";
                    }

                    MainSwitcher.AuthSignalHandler().circleSignalHandler.CreateNewCircle(circleModel, msg);
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    msg = "Failed to create circle!";
                    MainSwitcher.AuthSignalHandler().circleSignalHandler.CreateNewCircle(circleModel, msg);
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
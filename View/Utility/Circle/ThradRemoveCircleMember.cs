﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Utility.DataContainer;

namespace View.Utility.Circle
{
    class ThradRemoveCircleMember
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradRemoveCircleMember).Name);
        private bool running = false;
        private long circleId, utid;
        private int mc = 0;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        public ThradRemoveCircleMember(long circleId, long utid)
        {
            this.circleId = circleId;
            this.utid = utid;
        }

        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                String packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_REMOVE_CIRCLE_MEMBER;
                pakToSend[JsonKeys.GroupId] = circleId;
                JArray jarry = new JArray();
                JObject singleIbj = new JObject();
                singleIbj[JsonKeys.UserTableID] = utid;
                jarry.Add(singleIbj);
                pakToSend[JsonKeys.GroupMembers] = jarry;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                    {
                        lock (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY)
                        {
                            CircleModel model = CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY[circleId];
                            if (model != null)
                            {
                                model.MemberCount -= 1;
                                mc = model.MemberCount;
                                CircleDTO circleDTO = model.GetCircleDTO(model);
                                List<CircleDTO> lst = new List<CircleDTO>();
                                lst.Add(circleDTO);
                                new InsertIntoCircleListTable(lst);
                            }
                        }
                        //lock (RingDictionaries.Instance.CIRCLE_LIST_DICTIONARY)
                        //{
                        //    if (RingDictionaries.Instance.CIRCLE_LIST_DICTIONARY.ContainsKey(circleId))
                        //    {
                        //        RingDictionaries.Instance.CIRCLE_LIST_DICTIONARY[circleId].MemberCount--;
                        //        mc = RingDictionaries.Instance.CIRCLE_LIST_DICTIONARY[circleId].MemberCount;
                        //        List<CircleDTO> lst = new List<CircleDTO>();
                        //        lst.Add(RingDictionaries.Instance.CIRCLE_LIST_DICTIONARY[circleId]);
                        //        new InsertIntoCircleListTable(lst);
                        //    }
                        //}

                        List<long> list = new List<long>();
                        list.Add(utid);
                        MainSwitcher.AuthSignalHandler().circleSignalHandler.DeleteCircleMembers(circleId, list, mc);
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    //  bool retrn = PingInServer.StartNoNewtorkThread();
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                    {
                       UIHelperMethods.ShowErrorMessageBoxFromThread(NotificationMessages.INTERNET_UNAVAILABLE, "Failed!");
                    }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json.Linq;


namespace View.Utility.Circle
{
    public class ThradCircleNewsFeedsRequest
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradCircleNewsFeedsRequest).Name);
        private bool running = false;
        private long circleId;
        private long time;
        private short scl;
        private Object obj;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"

        private void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_CIRCLE_NEWSFEED;
                pakToSend[JsonKeys.GroupId] = circleId;
                pakToSend[JsonKeys.Limit] = 10;
                if (NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME.ContainsKey(circleId))
                {
                    pakToSend[JsonKeys.StartLimit] = NewsFeedDictionaries.Instance.CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME[circleId].Count;
                }
                else
                {
                    pakToSend[JsonKeys.StartLimit] = 0;
                }

                if (scl > 0)
                {
                    pakToSend[JsonKeys.Scroll] = scl;
                }
                if (time > 0)
                {
                    pakToSend[JsonKeys.Time] = time;
                }
                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId, 1000);
                if (feedbackfields != null)
                {
                    try
                    {
                        if (feedbackfields != null && feedbackfields[JsonKeys.Success] != null)
                        {
                            if ((bool)feedbackfields[JsonKeys.Success])
                            {
                                if (scl == 0 || scl == 2)
                                {
                                    Thread.Sleep(2000);
                                    BottomCircleLoading(StatusConstants.LOADING_TEXT_VISIBLE);
                                }
                            }
                            else
                            {
                                if (scl == 0 || scl == 2)
                                {
                                    Thread.Sleep(2000);
                                    BottomCircleLoading(StatusConstants.LOADING_NO_MORE_FEED_VISIBLE);
                                }
                            }
                        }
                        else
                        {
                            if (scl == 0 || scl == 2)
                            {
                                Thread.Sleep(2000);
                                BottomCircleLoading(StatusConstants.LOADING_TEXT_VISIBLE);
                            }
                        }
                    }
                    finally
                    {

                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    if (scl == 0 || scl == 2)
                    {
                        BottomCircleLoading(StatusConstants.LOADING_TEXT_VISIBLE);
                    }
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }

        private void BottomCircleLoading(int status)
        {
            //BottomCircleLoading(obj, status);
            //MainSwitcher.AuthSignalHandler().feedSignalHandler.BottomCircleLoading(obj, status);
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread(long circleId, long time, short scl, Object obj)
        {
            if (!running)
            {
                this.circleId = circleId;
                this.time = time;
                this.scl = scl;
                this.obj = obj;
                if (this.scl == 0 || this.scl == 2)
                {
                    BottomCircleLoading(StatusConstants.LOADING_GIF_VISIBLE);
                }
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
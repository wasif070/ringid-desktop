﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Stores;
using Newtonsoft.Json.Linq;

namespace View.Utility.Circle
{
    class ThradFetchCircleMembers
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradFetchCircleMembers).Name);
        long circleId = 0;
        long pivotId = 0;
        long limit = 10;
        long scroll = 1;
        long memberCategory = -1;
        private bool running = false;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        public ThradFetchCircleMembers()
        {

        }
        #endregion "Constructors"

        #region "Properties"
        #endregion "Properties"

        #region "Event Trigger"
        #endregion "Event Trigger"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.GroupId] = circleId;
                pakToSend[JsonKeys.PivotId] = pivotId;
                pakToSend[JsonKeys.Limit] = limit;
                pakToSend[JsonKeys.Scroll] = scroll;
                if (memberCategory > -1) { pakToSend[JsonKeys.MemberCategoryCircle] = memberCategory; }
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_CIRCLE_MEMBERS_LIST;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
                if (feedbackfields != null)
                {
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    MainSwitcher.AuthSignalHandler().circleSignalHandler.StopLoader(circleId);
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"

        public void StartThread(long circleId, long pivotId = 0, long limit = 10, long memberCategory = -1)
        {
            this.circleId = circleId;
            this.pivotId = pivotId;
            this.limit = limit;
            this.memberCategory = memberCategory;
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }
        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using View.BindingModels;
using View.Utility.DataContainer;

namespace View.Utility.Circle
{
    public class ThradAddCircleMember
    {
        #region "Private Fields"

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradAddCircleMember).Name);
        private bool running = false;
        private long circleId;
        private List<CircleMemberDTO> circleMembersList;
        private string str = "Failed to add Member! ";

        #endregion "Private Fields"

        #region Utility

        private void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                String packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_CIRCLE_MEMBER;
                pakToSend[JsonKeys.GroupId] = circleId;
                JArray jarry = new JArray();
                foreach (CircleMemberDTO dto in circleMembersList)
                {
                    JObject singleIbj = new JObject();
                    //singleIbj[JsonKeys.UserIdentity] = dto.UserIdentity;
                    singleIbj[JsonKeys.UserTableID] = dto.UserTableID;
                    singleIbj[JsonKeys.Admin] = dto.IsAdmin;
                    jarry.Add(singleIbj);
                }
                pakToSend[JsonKeys.GroupMembers] = jarry;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                    {
                        List<CircleMemberDTO> listToInsert = new List<CircleMemberDTO>();                      

                        foreach (CircleMemberDTO circleMember in circleMembersList)
                        {
                            circleMember.IntegerStatus = 0;
                            listToInsert.Add(circleMember);
                        }

                         CircleModel circleModel = null;
                         if (CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.TryGetValue(circleId, out circleModel))
                        {
                            circleModel.MemberCount = circleModel.MemberCount + circleMembersList.Count;
                            MainSwitcher.AuthSignalHandler().circleSignalHandler.AddCircleMembers(circleId, listToInsert, circleModel.MemberCount);
                        }
                    }
                    else
                    {
                        if (feedbackfields[JsonKeys.Message] != null)
                        {
                            str = str + (string)feedbackfields[JsonKeys.Message];
                        }
                        MainSwitcher.AuthSignalHandler().circleSignalHandler.EnableButtonsOnAddCircleMembersFailure(circleId, str);
                    }
                    MainSwitcher.AuthSignalHandler().circleSignalHandler.SetUIControlOfCircleViewWrapper();
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                    {
                       UIHelperMethods.ShowErrorMessageBoxFromThread(NotificationMessages.INTERNET_UNAVAILABLE, "Failed!");
                    }
                    MainSwitcher.AuthSignalHandler().circleSignalHandler.EnableButtonsOnAddCircleMembersFailure(circleId, str);
                    MainSwitcher.AuthSignalHandler().circleSignalHandler.SetUIControlOfCircleViewWrapper();
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }

        public void StartThread(long circleId, List<CircleMemberDTO> circleMembersList)
        {
            if (!running)
            {
                this.circleId = circleId;
                this.circleMembersList = circleMembersList;
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion Utility
    }
}
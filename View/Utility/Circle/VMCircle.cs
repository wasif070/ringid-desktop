﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using View.BindingModels;
using System.Collections.ObjectModel;
using Models.Constants;
using System.Windows;
using System.Windows.Media;
using System.Windows.Data;
using System.Threading.Tasks;
using Models.Stores;
using System.Windows.Threading;
using System.Windows.Input;
using Auth.Service.Feed;
using View.Utility.WPFMessageBox;
using View.Constants;
using System.Collections.Concurrent;
using System.Threading;
using Models.Entity;
using View.UI;
using Newtonsoft.Json.Linq;
using View.Utility.DataContainer;

namespace View.Utility.Circle
{
    public class VMCircle : INotifyPropertyChanged
    {
        #region Properties
        private static VMCircle _instance;
        public static VMCircle Instance
        {
            get { _instance = _instance ?? new VMCircle(); return _instance; }
        }

        //private long _CircleId = 0;
        //public long CircleId
        //{
        //    get { return _CircleId; }
        //    set { _CircleId = value; this.OnPropertyChanged("CircleId"); }
        //}

        private int _TotalMember = 0;
        public int TotalMember
        {
            get { return _TotalMember; }
            set { _TotalMember = value; this.OnPropertyChanged("TotalMember"); this.SetTotalMembersLabel(); }
        }

        private int _AdminCount = 0;
        public int AdminCount
        {
            get { return _AdminCount; }
            set { _AdminCount = value; this.OnPropertyChanged("AdminCount"); this.SetAdminLabel(); }
        }

        private int _MemberCount = 0;
        public int MemberCount
        {
            get { return _MemberCount; }
            set { _MemberCount = value; this.OnPropertyChanged("MemberCount"); this.SetMemberLabel(); }
        }

        private ObservableCollection<UserBasicInfoModel> _MembersList = new ObservableCollection<UserBasicInfoModel>();
        public ObservableCollection<UserBasicInfoModel> MembersList
        {
            get { return _MembersList; }
            set { _MembersList = value; this.OnPropertyChanged("MembersList"); }
        }

        private ObservableCollection<UserBasicInfoModel> _MembersSearchList = new ObservableCollection<UserBasicInfoModel>();
        public ObservableCollection<UserBasicInfoModel> MembersSearchList
        {
            get { return _MembersSearchList; }
            set { _MembersSearchList = value; this.OnPropertyChanged("MembersSearchList"); }
        }

        private ObservableCollection<UserBasicInfoModel> _DragListForNewCircle = new ObservableCollection<UserBasicInfoModel>();
        public ObservableCollection<UserBasicInfoModel> DragListForNewCircle
        {
            get
            {
                return _DragListForNewCircle;
            }
            set
            {
                _DragListForNewCircle = value;
            }
        }

        private Dictionary<string, long> _SearchMap = new Dictionary<string, long>();
        public Dictionary<string, long> SearchMap
        {
            get { return _SearchMap; }
            set { _SearchMap = value; this.OnPropertyChanged("SearchMap"); }
        }

        private Dictionary<string, bool> _SearchMapFetcher = new Dictionary<string, bool>();
        public Dictionary<string, bool> SearchMapFetcher
        {
            get { return _SearchMapFetcher; }
            set { _SearchMapFetcher = value; this.OnPropertyChanged("SearchMapFetcher"); }
        }

        private int _ScrollMode = StatusConstants.CIRCLE_MEMBER_NORMAL_SCROLL;
        public int ScrollMode
        {
            get { return _ScrollMode; }
            set { _ScrollMode = value; this.OnPropertyChanged("ScrollMode"); }
        }

        private string _SearchString = string.Empty;
        public string SearchString
        {
            get { return _SearchString; }
            set
            {
                _SearchString = value;
                this.OnPropertyChanged("SearchString");
                OnSearch();
            }
        }

        private long _PivotID = 0;
        public long PivotID
        {
            get { return _PivotID; }
            set { _PivotID = value; this.OnPropertyChanged("PivotID"); }
        }

        private long _AdminPivotID = 0;
        public long AdminPivotID
        {
            get { return _AdminPivotID; }
            set { _AdminPivotID = value; this.OnPropertyChanged("AdminPivotID"); }
        }

        private Visibility _MemberListVisibility = Visibility.Visible;
        public Visibility MemberListVisibility
        {
            get { return _MemberListVisibility; }
            set { _MemberListVisibility = value; this.OnPropertyChanged("MemberListVisibility"); }
        }

        private Visibility _SearchListVisibility = Visibility.Collapsed;
        public Visibility SearchListVisibility
        {
            get { return _SearchListVisibility; }
            set { _SearchListVisibility = value; this.OnPropertyChanged("SearchListVisibility"); }
        }

        private Visibility _AddButtonVisible = Visibility.Collapsed;
        public Visibility AddButtonVisible
        {
            get { return _AddButtonVisible; }
            set { _AddButtonVisible = value; this.OnPropertyChanged("AddButtonVisible"); }
        }

        private Visibility _ShowMorePanelVisibility = Visibility.Visible;
        public Visibility ShowMorePanelVisibility
        {
            get { return _ShowMorePanelVisibility; }
            set { _ShowMorePanelVisibility = value; this.OnPropertyChanged("ShowMorePanelVisibility"); }
        }

        private Visibility _ShowMoreButtonVisibility = Visibility.Visible;
        public Visibility ShowMoreButtonVisibility
        {
            get { return _ShowMoreButtonVisibility; }
            set { _ShowMoreButtonVisibility = value; this.OnPropertyChanged("ShowMoreButtonVisibility"); }
        }

        private Visibility _ShowMoreLoaderBorder = Visibility.Visible;
        public Visibility ShowMoreLoaderBorder
        {
            get { return _ShowMoreLoaderBorder; }
            set { _ShowMoreLoaderBorder = value; this.OnPropertyChanged("ShowMoreLoaderBorder"); }
        }

        private Visibility _SearchBoxVisibility = Visibility.Visible;

        public Visibility SearchBoxVisibility
        {
            get { return _SearchBoxVisibility; }
            set { _SearchBoxVisibility = value; this.OnPropertyChanged("SearchBoxVisibility"); }
        }


        private ImageSource _Loader;
        public ImageSource Loader
        {
            get { return _Loader; }
            set { _Loader = value; this.OnPropertyChanged("Loader"); }
        }

        private bool _IsCircleMembersFetched;
        public bool IsCircleMembersFetched
        {
            get { return _IsCircleMembersFetched; }
            set { _IsCircleMembersFetched = value; this.OnPropertyChanged("IsCircleMembersFetched"); }
        }

        private CircleModel _CircleModel;

        public CircleModel CircleModel
        {
            get { return _CircleModel; }
            set { _CircleModel = value; this.OnPropertyChanged("CircleModel"); }
        }

        private string _MembersLabel = string.Empty;
        public string MembersLabel
        {
            get { return _MembersLabel; }
            set { _MembersLabel = value; this.OnPropertyChanged("MembersLabel"); }
        }

        private string _AdminsLabel;
        public string AdminsLabel
        {
            get { return _AdminsLabel; }
            set { _AdminsLabel = value; this.OnPropertyChanged("AdminsLabel"); }
        }

        private string _TotalMembersLabel;

        public string TotalMembersLabel
        {
            get { return _TotalMembersLabel; }
            set { _TotalMembersLabel = value; this.OnPropertyChanged("TotalMembersLabel"); }
        }

        private int _TotalCircles;
        public int TotalCircles
        {
            get { return _TotalCircles; }
            set
            {
                if (value == _TotalCircles) return;
                _TotalCircles = value;
                this.OnPropertyChanged("TotalCircles");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) { handler(this, new PropertyChangedEventArgs(propertyName)); }
        }

        #endregion

        #region Commands
        private ICommand _CommandMakeAdmin;
        public ICommand CommandMakeAdmin
        {
            get
            {
                if (_CommandMakeAdmin == null)
                {
                    _CommandMakeAdmin = new RelayCommand(param => OnMakeAdminClicked(param));
                }
                return _CommandMakeAdmin;
            }
        }

        private ICommand _CommandRemoveAdmin;
        public ICommand CommandRemoveAdmin
        {
            get
            {
                _CommandRemoveAdmin = _CommandRemoveAdmin ?? new RelayCommand(param => OnRemoveAdminClicked(param));
                return _CommandRemoveAdmin;
            }
        }

        private ICommand _CommandRemoveMember;
        public ICommand CommandRemoveMember
        {
            get
            {
                _CommandRemoveMember = _CommandRemoveMember ?? new RelayCommand(param => OnRemoveMemberClicked(param));
                return _CommandRemoveMember;
            }
        }

        private ICommand _CommandDeleteCircle;
        public ICommand CommandDeleteCircle
        {
            get
            {
                _CommandDeleteCircle = _CommandDeleteCircle ?? new RelayCommand(param => OnDeleteCircleClicked(param));
                return _CommandDeleteCircle;
            }

        }

        private ICommand _CommandLeaveFromCircle;
        public ICommand CommandLeaveFromCircle
        {
            get
            {
                _CommandLeaveFromCircle = _CommandLeaveFromCircle ?? new RelayCommand(param => OnLeaveFromCircleClicked(param));
                return _CommandLeaveFromCircle;
            }

        }

        private ICommand _CommandHideUnHide;
        public ICommand CommandHideUnHide
        {
            get
            {
                _CommandHideUnHide = _CommandHideUnHide ?? new RelayCommand(param => OnHideUnHideClicked(param));
                return _CommandHideUnHide;
            }
        }

        private ICommand _CircleClickCommand;
        public ICommand CircleClickCommand
        {
            get
            {
                if (_CircleClickCommand == null)
                {
                    _CircleClickCommand = new RelayCommand(param => OnCirclePanelClicked(param));
                }
                return _CircleClickCommand;
            }
        }
        private ICommand _BackCommand;
        public ICommand BackCommand
        {
            get
            {
                if (_BackCommand == null)
                {
                    _BackCommand = new RelayCommand(param => OnBackCommandClicked(param));
                }
                return _BackCommand;
            }
        }

        public void OnBackCommandClicked(object parameter)
        {
            MiddlePanelSwitcher.pageSwitcher.SwitchToPreviousUI();
        }

        public void OnCirclePanelClicked(object parameter)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeCirclePanel, parameter);
        }

        private void OnDeleteCircleClicked(object param)
        {
            CircleModel model = null;
            if (param is CircleModel)
            {
                model = param as CircleModel;
            }
            else if (param is VMCircle)
            {
                model = VMCircle.Instance.CircleModel;
            }

            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                //MessageBoxResult result = CustomMessageBox.ShowQuestion(string.Format(NotificationMessages.CIRCLE_DELETE_CONFIRMATION_MESSAGE, model.FullName), NotificationMessages.HEADER_CIRCLE_DELETE);
                //if (result == MessageBoxResult.Yes)
                //{
                bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.DELETE_CONFIRMATIONS, model.FullName), "Delete circle confirmation!");
                if (isTrue)
                {
                    CircleUtility.Instance.DeleteCircle(model.UserTableID);
                }
            });
        }

        private void OnLeaveFromCircleClicked(object param)
        {
            CircleModel model = null;
            if (param is CircleModel)
            {
                model = param as CircleModel;
            }
            else if (param is VMCircle)
            {
                model = VMCircle.Instance.CircleModel;
            }
            //CircleModel model = param as CircleModel;
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                //MessageBoxResult result = CustomMessageBox.ShowQuestion(string.Format(NotificationMessages.CIRCLE_LEAVE_CONFIRMATION_MESSAGE, model.FullName), NotificationMessages.HEADER_CIRCLE_LEAVE);
                //if (result == MessageBoxResult.Yes)
                //{
                bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.LEAVE_CONFIRMATIONS, model.FullName), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, NotificationMessages.HEADER_CIRCLE_LEAVE));
                if (isTrue)
                {
                    CircleUtility.Instance.LeaveCircle(model.UserTableID);
                }
            });
        }

        private void OnHideUnHideClicked(object param)
        {
            if (param is VMCircle)
            {
                CircleModel circleModel = VMCircle.Instance.CircleModel;
                if (!circleModel.IsProfileHidden)
                {//hide
                    //MessageBoxResult result = CustomMessageBox.ShowQuestion("You won't see feeds from " + circleModel.FullName + " circle anymore.");
                    //if (result == MessageBoxResult.Yes)
                    bool isTrue = UIHelperMethods.ShowQuestion(String.Format("You won't see feeds from \"{0}\" circle anymore.", circleModel.FullName), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Hide"));
                    if (isTrue)
                        MainSwitcher.ThreadManager().HideCircle.StartThread(circleModel.UserTableID, circleModel);
                }
                else
                {//unhide
                    MainSwitcher.ThreadManager().HideCircle.StartThread(circleModel.UserTableID, circleModel, false);
                }
            }
        }

        private void OnRemoveAdminClicked(object param)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                UserBasicInfoModel _model = param as UserBasicInfoModel;

                //MessageBoxResult result = CustomMessageBox.ShowQuestion(string.Format(NotificationMessages.REMOVE_CIRCLE_ADMIN, _model.ShortInfoModel.FullName), NotificationMessages.HEADER_CIRCLE_REMOVE_ADMIN);
                //if (result == MessageBoxResult.Yes)
                //{
                bool isTrue = UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.REMOVE_CIRCLE_ADMIN, _model.ShortInfoModel.FullName), string.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Remove admin"));
                if (isTrue)
                {
                    ThradAddOrRemoveCircleAdmin addremoveAdmin = new ThradAddOrRemoveCircleAdmin(VMCircle.Instance.CircleModel.UserTableID, _model.ShortInfoModel.UserTableID, false);
                    addremoveAdmin.StartThread();
                }
            });
        }

        private void OnRemoveMemberClicked(object param)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                UserBasicInfoModel _model = param as UserBasicInfoModel;

                //MessageBoxResult result = CustomMessageBox.ShowQuestion(string.Format(NotificationMessages.REMOVE_FROM_CIRCLE, _model.ShortInfoModel.FullName), NotificationMessages.HEADER_CIRCLE_REMOVE_MEMBER);
                //if (result == MessageBoxResult.Yes)
                bool isTrue = UIHelperMethods.ShowQuestion(string.Format(NotificationMessages.REMOVE_FROM_CIRCLE, _model.ShortInfoModel.FullName), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Remove"));
                if (isTrue)
                    new ThradRemoveCircleMember(VMCircle.Instance.CircleModel.UserTableID, _model.ShortInfoModel.UserTableID).StartThread();
            });
        }

        private void OnMakeAdminClicked(object param)
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                UserBasicInfoModel _model = param as UserBasicInfoModel;

                //MessageBoxResult result = CustomMessageBox.ShowQuestion(string.Format(NotificationMessages.MAKE_CIRCLE_ADMIN, _model.ShortInfoModel.FullName), NotificationMessages.HEADER_CIRCLE_MAKE_ADMIN);
                //if (result == MessageBoxResult.Yes)
                bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.MAKE_CIRCLE_ADMIN, _model.ShortInfoModel.FullName), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, NotificationMessages.HEADER_CIRCLE_MAKE_ADMIN));
                if (isTrue)
                { new ThradAddOrRemoveCircleAdmin(VMCircle.Instance.CircleModel.UserTableID, _model.ShortInfoModel.UserTableID, true).StartThread(); }

            });
        }

        #endregion

        #region Methods

        /* UI Methods */

        public void LoadCircleList(List<CircleDTO> circleList)
        {
            new Thread(() =>
            {
                foreach (CircleDTO circleDTO in circleList)
                {
                    CircleModel circleModel = new CircleModel();
                    circleModel.LoadData(circleDTO);
                    CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY[circleDTO.CircleId] = circleModel;
                }
            }).Start();
        }

        private void SetAdminLabel()
        {
            this.AdminsLabel = this.AdminCount < 2 ? string.Format("Admin ({0})", this.AdminCount) : string.Format("Admins ({0})", this.AdminCount);
        }
        private void SetMemberLabel()
        {
            this.MembersLabel = this.MemberCount < 2 ? string.Format("Member ({0})", this.MemberCount) : string.Format("Members ({0})", this.MemberCount);
        }

        private void SetTotalMembersLabel()
        {
            this.TotalMembersLabel = this.TotalMember < 2 ? string.Format("Participant ({0})", this.TotalMember) : string.Format("Participants ({0})", this.TotalMember);
        }

        public void UpdateAdminCount(int count = 0)
        {
            this.AdminCount += count;
        }
        public void UpdateMemberOnlyCount(int count = 0)
        {
            this.MemberCount += count;
        }

        public void UpdateTotalMembersCount(int count = 0)
        {
            this.TotalMember += count;
        }

        public void LoadShowMore()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                ShowMorePanelVisibility = Visibility.Visible;
                ShowMoreButtonVisibility = Visibility.Visible;
                ShowMoreLoaderBorder = Visibility.Collapsed;
            });
        }
        public void HideShowMore()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                ShowMorePanelVisibility = Visibility.Collapsed;
            });
        }

        public void ShowLoader(bool show = false)
        {
            if (show)
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    ShowMoreLoaderBorder = Visibility.Visible;
                    Loader = ImageObjects.LOADER_FEED;
                });
            }
            else
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    ShowMoreLoaderBorder = Visibility.Collapsed;
                    if (ImageObjects.LOADER_FEED != null)
                    {
                        GC.SuppressFinalize(ImageObjects.LOADER_FEED);
                    }
                    if (Loader != null)
                    {
                        GC.SuppressFinalize(Loader);
                    }
                    ImageObjects.LOADER_FEED = null;
                    Loader = null;
                });

            }
        }

        /* Modus Operandi Load Data */
        public void LoadCircleMembers()
        {
            ShowLoader(true);
            CircleUtility.Instance.RequestCircleMemberList(this.CircleModel.UserTableID, this.PivotID);
            if (this.MembersList.Count - this.AdminCount != this.MemberCount)
            {
                this.LoadShowMore();
            }
        }

        /* Modus Operandi Scroll Task */
        public void ScrollAction()
        {
            if (this.ScrollMode == StatusConstants.CIRCLE_MEMBER_NORMAL_SEARCH)
            {
                this.OnSearch();
            }
            else
            {
                this.ShowLoader(true);
                if (this.ScrollMode == StatusConstants.CIRCLE_MEMBER_NORMAL_SCROLL)
                {
                    CircleUtility.Instance.RequestCircleMemberListWithFilter(circleId: this.CircleModel.UserTableID, adminPivotId: this.PivotID, memberCategory: StatusConstants.CIRCLE_MEMBER);
                }
                else if (ScrollMode == StatusConstants.CIRCLE_MEMBER_ADMIN_SCROLL)
                {
                    CircleUtility.Instance.RequestCircleMemberListWithFilter(circleId: this.CircleModel.UserTableID, adminPivotId: this.AdminPivotID, memberCategory: StatusConstants.CIRCLE_ADMIN);
                }
            }
        }

        /* Modus Operandi Tab switching */
        public void TabActionMembers()
        {
            if (this.MembersList.Where(x => x.CircleMembershipStatus == StatusConstants.CIRCLE_MEMBER).ToList().Count == this.MemberCount)
            {
                HideShowMore();
            }
            else
            {
                ShowLoader(false);
                LoadShowMore();
            }
        }
        public void TabActionAdmins()
        {
            this.SearchBoxVisibility = Visibility.Hidden;

            if (!this.MembersList.Any(x => x.ShortInfoModel.UserIdentity == CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY[this.CircleModel.UserTableID].SuperAdmin))
            {
                Task.Factory.StartNew(() =>
                {
                    CircleUtility.Instance.RequestCircleMemberListWithFilter(this.CircleModel.UserTableID, 0, StatusConstants.CIRCLE_SUPER_ADMIN);
                });
            }
            if (this.MembersList.Where(x => x.CircleMembershipStatus != StatusConstants.CIRCLE_MEMBER).ToList().Count != this.CircleModel.AdminCount)
            {
                Task.Factory.StartNew(() =>
                {
                    CircleUtility.Instance.RequestCircleMemberListWithFilter(this.CircleModel.UserTableID, 0, StatusConstants.CIRCLE_ADMIN);
                });
            }

            if (this.MembersList.Where(x => x.CircleMembershipStatus != StatusConstants.CIRCLE_MEMBER).ToList().Count == this.CircleModel.AdminCount)
            {
                HideShowMore();
            }
            else
            {
                ShowLoader(false);
                LoadShowMore();
            }

        }

        /* Modus Operandi Change View */
        public ICollectionView GetMembers()
        {
            ICollectionView view = CollectionViewSource.GetDefaultView(this.MembersList);
            view.SortDescriptions.Add(new SortDescription("PivotId", ListSortDirection.Ascending));
            view.Filter = (x) => { return ((UserBasicInfoModel)x).CircleMembershipStatus == StatusConstants.CIRCLE_MEMBER; };
            return view;
        }
        public ICollectionView GetAdmins()
        {
            ICollectionView view = CollectionViewSource.GetDefaultView(this.MembersList);
            view.GroupDescriptions.Add(new System.Windows.Data.PropertyGroupDescription("CircleMembershipStatus"));
            view.SortDescriptions.Add(new SortDescription("CircleMembershipStatus", ListSortDirection.Descending));
            view.Filter = (x) => { return ((UserBasicInfoModel)x).CircleMembershipStatus != StatusConstants.CIRCLE_MEMBER; };
            return view;
        }
        public ICollectionView GetSearchResult(string searchParam)
        {
            ICollectionView view = CollectionViewSource.GetDefaultView(this.MembersSearchList);
            view.Filter = (x) => { return (((UserBasicInfoModel)x).ShortInfoModel).FullName.ToLower().StartsWith(searchParam); };
            return view;
        }

        /* Modus Operandi Search */
        public void OnSearch()
        {
            try
            {
                SearchCircleMember._SearchParam = SearchString.Trim().ToLower();
                this.ClearSearchRecords();
                if (string.IsNullOrEmpty(SearchCircleMember._SearchParam))
                {
                    this.ClearSearchRecords();
                    this.ScrollMode = StatusConstants.CIRCLE_MEMBER_NORMAL_SCROLL;
                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        //MembersView();
                        if (View.UI.UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel == null)
                        {
                            return;
                        }
                        View.UI.UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel.MembersView();
                    });

                    if (this.MembersList.Count - this.CircleModel.AdminCount != this.MemberCount)
                    {
                        LoadShowMore();
                    }
                    return;
                }

                HideShowMore();

                this.SetSearchList();

                if (!SearchCircleMember._IsSearchRunning)
                {
                    this.ScrollMode = StatusConstants.CIRCLE_MEMBER_NORMAL_SEARCH;
                    long lmt = 0;
                    if (SearchMap.ContainsKey(SearchCircleMember._SearchParam))
                    {
                        lmt = SearchMap[SearchCircleMember._SearchParam];
                    }
                    else
                    {
                        SearchMap.Add(SearchCircleMember._SearchParam, lmt);
                    }
                    if (SearchMapFetcher.ContainsKey(SearchCircleMember._SearchParam) && SearchMapFetcher[SearchCircleMember._SearchParam])
                    {
                        HideShowMore();
                    }
                    else
                    {
                        (new SearchCircleMember(this.CircleModel.UserTableID, lmt)).StartThread();
                    }

                }
            }
            catch (Exception e)
            {
                // log.Error("ERROR ==> " + e.Message + "\n" + e.StackTrace);
            }
        }
        public void SearchFriends()
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(SearchString))
                {
                    string searchText = SearchString.ToLower();
                    Application.Current.Dispatcher.BeginInvoke(() =>
                    {
                        View.UI.UCMiddlePanelSwitcher.View_UCCirclePanel._UCCircleMembersPanel.SearchResultView(searchText);
                        this.MemberListVisibility = Visibility.Collapsed;
                        this.SearchListVisibility = Visibility.Visible;
                    });
                }
            }
            catch (Exception e)
            {
                //log.Error("ERROR ==> " + e.Message + "\n" + e.StackTrace);
            }
        }
        private void SetSearchList()
        {
            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                MemberListVisibility = Visibility.Collapsed;
                SearchListVisibility = Visibility.Visible;
            });
        }
        public void ClearSearchRecords()
        {
            try
            {
                Application.Current.Dispatcher.BeginInvoke(() =>
                {
                    MemberListVisibility = Visibility.Visible;
                    SearchListVisibility = Visibility.Collapsed;
                });
            }
            catch (Exception)
            {
                //log.Error("ERROR ==> " + e.Message + "\n" + e.StackTrace);
            }
        }

        /*
        public void ReleaseCircleSearchedMemberFromMemory()
        {
            new System.Threading.Thread(() =>
            {
                DispatcherTimer _RemoveSearchedMembers = new DispatcherTimer();
                _RemoveSearchedMembers.Interval = TimeSpan.FromMilliseconds(5);
                _RemoveSearchedMembers.Tick += (s, e) =>
                {
                    lock (MembersSearchList)
                    {
                        while (MembersSearchList.Count > 0)
                        {
                            UserBasicInfoModel model = MembersSearchList.FirstOrDefault();
                            MembersSearchList.Remove(model);
                            GC.SuppressFinalize(model);
                        }
                    }
                    _RemoveSearchedMembers.Stop();
                };
                _RemoveSearchedMembers.Stop();
                _RemoveSearchedMembers.Start();
            });
        }
        public void ReleaseCircleMembeFromMemory()
        {
            new System.Threading.Thread(() =>
            {
                DispatcherTimer _RemoveMembers = new DispatcherTimer();
                _RemoveMembers.Interval = TimeSpan.FromMilliseconds(5);
                _RemoveMembers.Tick += (s, e) =>
                {
                    lock (MembersList)
                    {
                        while (MembersList.Count > 0)
                        {
                            UserBasicInfoModel model = MembersList.FirstOrDefault();
                            MembersList.Remove(model);
                            GC.SuppressFinalize(model);
                        }
                    }
                    _RemoveMembers.Stop();
                };
                _RemoveMembers.Stop();
                _RemoveMembers.Start();
            });
            
        } 
        */

        #region CircleInitPanel

        private void OnCreateNewCircle(object param)
        {
            MiddlePanelSwitcher.Switch(MiddlePanelConstants.TypeCreateCirclePanel);
        }

        public void OnBtnTabCommandClicked(object parameter)
        {
            int intype = Convert.ToInt32(parameter);
            MiddlePanelSwitcher.pageSwitcher.InsertTabIndexSearchVisited(MiddlePanelConstants.TypeCircleMain, intype);
            UCMiddlePanelSwitcher.View_UCCircleInitPanel.SwitchToTab(intype);
        }

        private ICommand _CreateCircleFromCircleListCommand;
        public ICommand CreateCircleFromCircleListCommand
        {
            get
            {
                if (_CreateCircleFromCircleListCommand == null)
                {
                    _CreateCircleFromCircleListCommand = new RelayCommand((param) => OnCreateNewCircle(param));
                }
                return _CreateCircleFromCircleListCommand;
            }
        }

        private ICommand _BtnTabCommand;
        public ICommand BtnTabCommand
        {
            get
            {
                if (_BtnTabCommand == null)
                {
                    _BtnTabCommand = new RelayCommand(param => OnBtnTabCommandClicked(param));
                }
                return _BtnTabCommand;
            }
        }

        #endregion "ICommand"

        #region Property

        private int _TabType = 0;
        public int TabType
        {
            get { return _TabType; }
            set
            {
                _TabType = value;
                OnPropertyChanged("TabType");
            }
        }

        #endregion CircleInitPanel

        #region CreateCirclePanel

        #region Utility


        #endregion

        private ICommand _CreateCircleAfterDragCommand;
        public ICommand CreateCircleAfterDragCommand
        {
            get
            {
                if (_CreateCircleAfterDragCommand == null)
                {
                    _CreateCircleAfterDragCommand = new RelayCommand((param) => OnCreateCircleAfterDragCommand(param));
                }
                return _CreateCircleAfterDragCommand;
            }
        }

        public void OnCreateCircleAfterDragCommand(object param)
        {
            string groupName = (string)param;
            JObject jObj = new JObject();
            JArray jArr = new JArray();
            foreach (UserBasicInfoModel userBasicInfoModel in VMCircle.Instance.DragListForNewCircle)
            {
                JObject singleObj = SingleJobjectFromUserBasicInfoModel(userBasicInfoModel);
                jArr.Add(singleObj);
            }

            jObj[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
            jObj[JsonKeys.Action] = AppConstants.TYPE_CREATE_CIRCLE;
            jObj[JsonKeys.GroupName] = groupName;
            jObj[JsonKeys.GroupMembers] = jArr;
            new ThradCreateCircle(jArr, groupName).StartThread();
        }

        public JObject SingleJobjectFromUserBasicInfoModel(UserBasicInfoModel model)
        {
            JObject obj = new JObject();
            //obj[JsonKeys.UserIdentity] = model.ShortInfoModel.UserIdentity;
            obj[JsonKeys.UserTableID] = model.ShortInfoModel.UserTableID;
            obj[JsonKeys.Admin] = model.IsAdmin;
            return obj;
        }

        #endregion


        #region CircleList



        public int _ItemsNeedToLoad { get; set; }


        private ObservableCollection<CircleModel> _CircleListYouManage = new ObservableCollection<CircleModel>();
        public ObservableCollection<CircleModel> CircleListYouManage
        {
            get
            {
                return _CircleListYouManage;
            }
            set
            {
                if (_CircleListYouManage == value) return;
                _CircleListYouManage = value;
                OnPropertyChanged("CircleListYouManage");
            }
        }

        private ObservableCollection<CircleModel> _CircleListYouAreIn = new ObservableCollection<CircleModel>();
        public ObservableCollection<CircleModel> CircleListYouAreIn
        {
            get
            {
                return _CircleListYouAreIn;
            }
            set
            {
                if (_CircleListYouAreIn == value) return;
                _CircleListYouAreIn = value;
                OnPropertyChanged("CircleListYouAreIn");
            }
        }

        private ObservableCollection<CircleModel> _CircleListYouSearch = new ObservableCollection<CircleModel>();
        public ObservableCollection<CircleModel> CircleListYouSearch
        {
            get
            {
                return _CircleListYouSearch;
            }
            set
            {
                if (_CircleListYouSearch == value) return;
                _CircleListYouSearch = value;
                OnPropertyChanged("CircleListYouSearch");
            }
        }

        private bool _IsCircleSearch = false;
        public bool IsCircleSearch
        {
            get { return _IsCircleSearch; }
            set
            {
                if (_IsCircleSearch == value) return;
                _IsCircleSearch = value;
                OnPropertyChanged("IsCircleSearch");
            }
        }

        private string _CircleSearchString = string.Empty;
        public string CircleSearchString
        {
            get { return _CircleSearchString; }
            set
            {
                _CircleSearchString = value;
                CircleListYouSearch.Clear();
                OnPropertyChanged("CircleSearchString");

                if (!string.IsNullOrWhiteSpace(_CircleSearchString) && _CircleSearchString.Trim().Length > 0 && CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.Values.Count > 0)
                {
                    IsCircleSearch = true;
                    OnCircleSearchInCircleList();
                }
                else
                {
                    IsCircleSearch = false;
                }
            }
        }

        private void OnCircleSearchInCircleList()
        {
            List<CircleModel> circleModels = CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.Values.Where(P => CircleDataContainer.Instance.CircleIDs.Any(Q => P.UserTableID == Q)).OrderBy(P => P.FullName).ToList();
            List<CircleModel> tempCircleList = circleModels.Where(P => P.FullName.ToLower().Contains(CircleSearchString.ToLower())).ToList();
            foreach (CircleModel model in tempCircleList)
            {
                CircleListYouSearch.InvokeAdd(model);
            }
        }

        private void OnDeleteClick(object param)
        {
            CircleSearchString = string.Empty;
        }

        private ICommand _OnDeleteCommand;
        public ICommand OnDeleteCommand
        {
            get
            {
                if (_OnDeleteCommand == null)
                {
                    _OnDeleteCommand = new RelayCommand((param) => OnDeleteClick(param));
                }
                return _OnDeleteCommand;
            }
        }

        #endregion CircleList


        #endregion
    }
}

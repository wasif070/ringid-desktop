﻿using System;
using System.Threading;
using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace View.Utility.Circle
{
    public class ThreadFetchCircleDetails
    {
        private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThreadFetchCircleDetails).Name);
        long circleId = 0;
        bool running = false;
        public ThreadFetchCircleDetails(long circleId)
        {
            this.circleId = circleId;

            Thread th = new Thread(Run);
            th.Name = this.GetType().Name;
            th.Start();
        }

        public void StartThread(long circleId)
        {
            if (!running)
            {
                this.circleId = circleId;
                Thread th = new Thread(Run);
                th.Name = this.GetType().Name;
                th.Start();
            }
        }

        private void Run()
        {
            running = true;

            try
            {
                JObject pakToSend = new JObject();
                string packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.GroupId] = circleId;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_CIRCLE_DETAILS;

                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, packetId);
                if (feedbackfields != null)
                {
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                    {
                        UIHelperMethods.ShowInformation(NotificationMessages.INTERNET_UNAVAILABLE, "Circle details");
                    });
                }
                //string data = JsonConvert.SerializeObject(pakToSend, Formatting.None);
                //SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);

                //Thread.Sleep(25);
                //for (int attempt = 1; attempt <= DefaultSettings.TRYING_TIME; attempt++)
                //{
                //    if (String.IsNullOrEmpty(DefaultSettings.LOGIN_SESSIONID))
                //    {
                //        break;
                //    }
                //    Thread.Sleep(DefaultSettings.WAITING_TIME);
                //    if (!RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.ContainsKey(pakId))
                //    {
                //        if (attempt % 9 == 0)
                //            SendToServer.SendNormalPacket(AppConstants.REQUEST_TYPE_REQUEST, data);
                //    }
                //    else
                //    {
                //        return;
                //    }
                //}
            }
            catch (Exception e)
            {

                log.Error("ERROR==> " + e.StackTrace);
            }
            finally
            {
                running = true;
            }


        }
    }
}

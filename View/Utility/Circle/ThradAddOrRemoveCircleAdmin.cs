﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Auth.utility;
using Models.Constants;
using Models.DAO;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;

namespace View.Utility.Circle
{
    class ThradAddOrRemoveCircleAdmin
    {
        #region "Private Fields"
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ThradAddOrRemoveCircleAdmin).Name);
        private bool running = false;
        private long circleId, utid;
        private bool MakeAdmin;
        #endregion "Private Fields"

        #region "Constructors"
        public ThradAddOrRemoveCircleAdmin(long circleId, long utid, bool MakeAdmin)
        {
            this.circleId = circleId;
            this.utid = utid;
            this.MakeAdmin = MakeAdmin;
        }
        #endregion "Constructors"

        #region "Private methods"
        private void Run()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                String packetId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = packetId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_EDIT_CIRCLE_MEMBER;
                pakToSend[JsonKeys.GroupId] = circleId;
                JArray jarry = new JArray();
                JObject singleIbj = new JObject();
                singleIbj[JsonKeys.UserTableID] = utid;
                singleIbj[JsonKeys.Admin] = MakeAdmin;
                jarry.Add(singleIbj);
                pakToSend[JsonKeys.GroupMembers] = jarry;


                JObject feedbackfields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, packetId);
                if (feedbackfields != null)
                {
                    if (feedbackfields[JsonKeys.Success] != null && (bool)feedbackfields[JsonKeys.Success])
                    {
                        CircleMemberDTO circleMember = new CircleMemberDTO { UserTableID = utid, IsAdmin = MakeAdmin };
                        //CircleMemberDTO circleMember = RingDictionaries.Instance.CIRCLE_MEMBERS_LIST_DICTIONARY[circleId][uid];
                        List<CircleMemberDTO> list = new List<CircleMemberDTO>();
                        list.Add(circleMember);
                        //HelperMethodsAuth.CircleHandlerInstance.EditCircleMembers(circleId, list);
                        MainSwitcher.AuthSignalHandler().circleSignalHandler.EditCircleMembers(circleId, list);
                    }
                    else if (feedbackfields[JsonKeys.Message] != null)
                    {

                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(packetId, out feedbackfields);
                }
                else
                {
                    if (!MainSwitcher.ThreadManager().PingNow()) { }
                }
            }
            catch (Exception)
            {


            }
            finally { running = false; }
        }
        #endregion "Private methods"

        #region "Public Methods"
        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(Run);
                thrd.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning()
        {
            return running;
        }

        #endregion "Public methods"
    }
}
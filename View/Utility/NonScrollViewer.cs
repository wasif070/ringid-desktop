﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace View.Utility
{
    public class NonScrollViewer : ScrollViewer
    {

        public NonScrollViewer()
            : base()
        {
            this.HorizontalScrollBarVisibility = ScrollBarVisibility.Hidden;
            this.VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;
            this.CanContentScroll = false;
        }

        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            var parentElement = Parent as UIElement;
            if (parentElement != null)
            {
                if ((e.Delta > 0 && VerticalOffset == 0) ||
                    (e.Delta < 0 && VerticalOffset == ScrollableHeight))
                {
                    e.Handled = true;

                    var routedArgs = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
                    routedArgs.RoutedEvent = UIElement.MouseWheelEvent;
                    parentElement.RaiseEvent(routedArgs);
                }
            }

            base.OnMouseWheel(e);
        }
    }
}

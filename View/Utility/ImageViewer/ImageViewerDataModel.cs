﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using View.BindingModels;
using View.Constants;
using View.ViewModel;

namespace View.Utility.ImageViewer
{
    public class ImageViewerDataModel : BaseViewModel
    {
        #region "Data Properties"

        private ImageModel imageModel;
        public ImageModel SingleImageModel
        {
            get { return imageModel; }
            set
            {
                if (value == imageModel) return;
                imageModel = value; OnPropertyChanged("SingleImageModel");
            }
        }

        private ObservableCollection<CommentModel> commentList = new ObservableCollection<CommentModel>();
        public ObservableCollection<CommentModel> CommentList
        {
            get { return commentList; }
            set
            {
                commentList = value;
                if (value.Count > 0) IsStartedCommentUILoading = false;
                OnPropertyChanged("CommentList");
            }
        }

        private string imageSourceToView = null;
        public string ImageSourceToView
        {
            get { return imageSourceToView; }
            set
            {
                imageSourceToView = value;
                if (value == null) LoaderImage = ImageLocation.LOADER_CYCLE_MINI_BG_BLACK;
                else LoaderImage = null;
                this.OnPropertyChanged("ImageSourceToView");
            }
        }

        private bool isBuffering = false;
        public bool IsBuffering
        {
            get { return isBuffering; }
            set { isBuffering = value; this.OnPropertyChanged("IsBuffering"); }
        }

        private Guid newsFeedID;
        public Guid NewsFeedID
        {
            get { return newsFeedID; }
            set { newsFeedID = value; OnPropertyChanged("NewsFeedID"); }
        }

        private bool previousButtonEnable = false;
        public bool PreviousButtonEnable
        {
            get { return previousButtonEnable; }
            set { previousButtonEnable = value; this.OnPropertyChanged("PreviousButtonEnable"); }
        }

        private bool nextButtonEnable = false;
        public bool NextButtonEnable
        {
            get { return nextButtonEnable; }
            set { nextButtonEnable = value; this.OnPropertyChanged("NextButtonEnable"); }
        }

        private string loaderImage;
        public string LoaderImage
        {
            get { return loaderImage; }
            set
            {
                if (value == loaderImage) return;
                loaderImage = value; OnPropertyChanged("LoaderImage");
            }
        }

        private int selectedIndexText = 1;
        public int SelectedIndexText
        {
            get { return selectedIndexText; }
            set { selectedIndexText = value; OnPropertyChanged("SelectedIndexText"); }
        }

        private int totalImages = 1;
        public int TotalImages
        {
            get { return totalImages; }
            set { totalImages = value; OnPropertyChanged("TotalImages"); }
        }

        private int selectedIndex;
        public int SelectedIndex
        {
            get { return selectedIndex; }
            set
            {
                selectedIndex = value;
                ChangeNextPreviousButton();
            }
        }

        private string _likeAnimationloader;
        public string LikeAnimationloader
        {
            get { return _likeAnimationloader; }
            set
            {
                if (value == _likeAnimationloader) return;
                _likeAnimationloader = value; OnPropertyChanged("LikeAnimationloader");
            }
        }

        private bool isLoadedComment = false;
        public bool IsLoadedComment
        {
            get { return isLoadedComment; }
            set { isLoadedComment = value; this.OnPropertyChanged("IsLoadedComment"); }
        }

        private bool needToShowLikeComment = true;
        public bool NeedToShowLikeComment
        {
            get { return needToShowLikeComment; }
            set { needToShowLikeComment = value; this.OnPropertyChanged("NeedToShowLikeComment"); }
        }

        private string imageDirectory = null;
        public string ImageDirectory
        {
            get { return imageDirectory; }
            set
            {
                if (value == imageDirectory) return;
                imageDirectory = value; OnPropertyChanged("ImageDirectory");
            }
        }

        private bool showCommentsPanel = false;
        public bool ShowCommentsPanel
        {
            get { return showCommentsPanel; }
            set
            {
                showCommentsPanel = value;
                if (!value) IsStartedCommentUILoading = false;
                this.OnPropertyChanged("ShowCommentsPanel");
            }
        }

        private bool isStartedCommentUILoading = false;
        public bool IsStartedCommentUILoading
        {
            get { return isStartedCommentUILoading; }
            set { isStartedCommentUILoading = value; OnPropertyChanged("IsStartedCommentUILoading"); }
        }

        private bool isLoadingComments = false;
        public bool IsLoadingComments
        {
            get { return isLoadingComments; }
            set { isLoadingComments = value; OnPropertyChanged("IsLoadingComments"); }
        }

        public bool IsLoadded { get; set; }

        #endregion "Data Properties"

        #region "Utility Methods"

        public void ChangeNextPreviousButton()
        {
            SelectedIndexText = SelectedIndex + 1;
            if (TotalImages < 2)
            {
                PreviousButtonEnable = false;
                NextButtonEnable = false;
            }
            else if (SelectedIndex == 0)
            {
                PreviousButtonEnable = false;
                NextButtonEnable = true;
            }
            else if (SelectedIndex == (TotalImages - 1))
            {
                PreviousButtonEnable = true;
                NextButtonEnable = false;
            }
            else
            {
                PreviousButtonEnable = true;
                NextButtonEnable = true;
            }
        }
        #endregion "Utility methods"
    }
}

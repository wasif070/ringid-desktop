﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using Newtonsoft.Json.Linq;
using Models.Constants;
using Auth.utility;
using View.Constants;

public class OAuthHelper
{
    public OAuthHelper() { }

    public string callbackUrl = "http://127.0.0.1/twitshoppers/oauth/oauthreturn.aspx";

    #region (Changable) Do Not Change It
    static string REQUEST_TOKEN = "https://api.twitter.com/oauth/request_token";
    static string AUTHORIZE = "https://api.twitter.com/oauth/authorize";
    static string ACCESS_TOKEN = "https://api.twitter.com/oauth/access_token";
    static string VERIFY_TOKEN = "https://api.twitter.com/1.1/account/verify_credentials.json";

    public enum httpMethod
    {
        POST, GET
    }
    public string oauth_request_token { get; set; }
    public string oauth_access_token { get; set; }
    public string oauth_access_token_secret { get; set; }
    public string user_id { get; set; }
    public string screen_name { get; set; }
    public string full_name { get; set; }
    public string profile_img_url { get; set; }
    public string oauth_error { get; set; }

    public void ResetAllTokens()
    {
        //oauth_request_token = null;
        oauth_access_token = null;
        oauth_access_token_secret = null;
        user_id = null;
        screen_name = null;
        full_name = null;
        profile_img_url = null;
        oauth_error = null;
    }

    public string GetRequestToken()
    {
        HttpWebRequest request = FetchRequestToken(httpMethod.POST, SocialMediaConstants.TWITTER_CONSUMER_KEY, SocialMediaConstants.TWITTER_CONSUMER_SECRET);
        string result = getResponce(request);
        Dictionary<string, string> resultData = OAuthUtility.GetQueryParameters(result);
        if (resultData.Keys.Contains("oauth_token"))
        {
            oauth_request_token = resultData["oauth_token"];
            return oauth_request_token;
        }
        else
        {
            this.oauth_error = result;
            return "";
        }
    }

    public string GetAuthorizeUrl(string requestToken)
    {
        return string.Format("{0}?oauth_token={1}", AUTHORIZE, requestToken);
    }

    public void GetUserTwAccessToken(string oauth_token, string oauth_verifier)
    {
        HttpWebRequest request = FetchAccessToken(httpMethod.POST, SocialMediaConstants.TWITTER_CONSUMER_KEY, SocialMediaConstants.TWITTER_CONSUMER_SECRET, oauth_token, oauth_verifier);
        string result = getResponce(request);

        Dictionary<string, string> resultData = OAuthUtility.GetQueryParameters(result);
        if (resultData.Keys.Contains("oauth_token"))
        {
            this.oauth_access_token = resultData["oauth_token"];
            this.oauth_access_token_secret = resultData["oauth_token_secret"];
            this.user_id = resultData["user_id"];
            this.screen_name = resultData["screen_name"];
            //this.profile_img_url = resultData["profile_image_url_https"];
        }
        else
            this.oauth_error = result;
    }
    public void GetVerificationURL()
    {
        HttpWebRequest request = FetchVerifyURL(httpMethod.GET, SocialMediaConstants.TWITTER_CONSUMER_KEY, SocialMediaConstants.TWITTER_CONSUMER_SECRET, "", "");
        string result = getResponce(request);

        Dictionary<string, string> resultData = OAuthUtility.GetQueryParameters(result);
        if (resultData.Keys.Contains("profile_image_url_https"))
        {
            // this.oauth_access_token = resultData["oauth_token"];
            // this.oauth_access_token_secret = resultData["oauth_token_secret"];
            //this.user_id = resultData["user_id"];
            //this.screen_name = resultData["screen_name"];
            //this.profile_img_url = resultData["profile_image_url_https"];
        }
        else
            this.oauth_error = result;
    }
    public void TweetOnBehalfOf(string oauth_access_token, string oauth_token_secret, string postData)
    {
        HttpWebRequest request = PostTwits(SocialMediaConstants.TWITTER_CONSUMER_KEY, SocialMediaConstants.TWITTER_CONSUMER_SECRET, oauth_access_token, oauth_token_secret, postData);
        string result = OAuthHelper.getResponce(request);
        Dictionary<string, string> dcResult = OAuthUtility.GetQueryParameters(result);
        if (dcResult["status"] != "200")
        {
            this.oauth_error = result;
        }


    }


    HttpWebRequest FetchRequestToken(httpMethod method, string oauth_consumer_key, string oauth_consumer_secret)
    {
        string OutUrl = "";
        string OAuthHeader = OAuthUtility.GetAuthorizationHeaderForPost_OR_QueryParameterForGET(new Uri(REQUEST_TOKEN), callbackUrl, method.ToString(), oauth_consumer_key, oauth_consumer_secret, "", "", out OutUrl);

        if (method == httpMethod.GET)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(OutUrl + "?" + OAuthHeader);
            request.Method = method.ToString();
            return request;
        }
        else if (method == httpMethod.POST)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(OutUrl);
            request.Method = method.ToString();
            request.Headers["Authorization"] = OAuthHeader;
            return request;
        }
        else
            return null;


    }
    HttpWebRequest FetchVerifyURL(httpMethod method, string oauth_consumer_key, string oauth_consumer_secret, string oauth_token, string oauth_verifier)
    {
        //string postData = "oauth_verifier=" + oauth_verifier;
        //string AccessTokenURL = string.Format("{0}?{1}", ACCESS_TOKEN, postData);
        //string OAuthHeader = OAuthUtility.GetAuthorizationHeaderForPost_OR_QueryParameterForGET(new Uri(VERIFY_TOKEN), callbackUrl, method.ToString(), oauth_consumer_key, oauth_consumer_secret, oauth_token, "", out AccessTokenURL);

        if (method == httpMethod.GET)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(VERIFY_TOKEN);
            request.Method = method.ToString();
            return request;
        }
        else if (method == httpMethod.POST)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(VERIFY_TOKEN);
            request.Method = method.ToString();
            //request.Headers["Authorization"] = OAuthHeader;

            //byte[] array = Encoding.ASCII.GetBytes(postData);
            //request.GetRequestStream().Write(array, 0, array.Length);
            return request;
        }
        else
            return null;

    }
    HttpWebRequest FetchAccessToken(httpMethod method, string oauth_consumer_key, string oauth_consumer_secret, string oauth_token, string oauth_verifier)
    {
        string postData = "oauth_verifier=" + oauth_verifier;
        string AccessTokenURL = string.Format("{0}?{1}", ACCESS_TOKEN, postData);
        string OAuthHeader = OAuthUtility.GetAuthorizationHeaderForPost_OR_QueryParameterForGET(new Uri(AccessTokenURL), callbackUrl, method.ToString(), oauth_consumer_key, oauth_consumer_secret, oauth_token, "", out AccessTokenURL);

        if (method == httpMethod.GET)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(AccessTokenURL + "?" + OAuthHeader);
            request.Method = method.ToString();
            return request;
        }
        else if (method == httpMethod.POST)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(AccessTokenURL);
            request.Method = method.ToString();
            request.Headers["Authorization"] = OAuthHeader;

            byte[] array = Encoding.ASCII.GetBytes(postData);
            request.GetRequestStream().Write(array, 0, array.Length);
            return request;
        }
        else
            return null;

    }
    HttpWebRequest PostTwits(string oauth_consumer_key, string oauth_consumer_secret, string oauth_access_token, string oauth_token_secret, string postData)
    {
        postData = "trim_user=true&include_entities=true&status=" + postData;
        string updateStatusURL = "https://api.twitter.com/1/statuses/update.json?" + postData;

        string outUrl;
        string OAuthHeaderPOST = OAuthUtility.GetAuthorizationHeaderForPost_OR_QueryParameterForGET(new Uri(updateStatusURL), callbackUrl, httpMethod.POST.ToString(), oauth_consumer_key, oauth_consumer_secret, oauth_access_token, oauth_token_secret, out outUrl);

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(outUrl);
        request.Method = httpMethod.POST.ToString();
        request.Headers["Authorization"] = OAuthHeaderPOST;

        byte[] array = Encoding.ASCII.GetBytes(postData);
        request.GetRequestStream().Write(array, 0, array.Length);
        return request;

    }
    private string EncodeCharacters(string data)
    {
        //as per OAuth Core 1.0 Characters in the unreserved character set MUST NOT be encoded
        //unreserved = ALPHA, DIGIT, '-', '.', '_', '~'
        if (data.Contains("!"))
            data = data.Replace("!", "%21");
        if (data.Contains("'"))
            data = data.Replace("'", "%27");
        if (data.Contains("("))
            data = data.Replace("(", "%28");
        if (data.Contains(")"))
            data = data.Replace(")", "%29");
        if (data.Contains("*"))
            data = data.Replace("*", "%2A");
        if (data.Contains(","))
            data = data.Replace(",", "%2C");

        return data;
    }
    public string Verify_CredentialsandReturnInputToken()
    {
        try
        {
            string oauthconsumerkey = SocialMediaConstants.TWITTER_CONSUMER_KEY;
            string oauthconsumersecret = SocialMediaConstants.TWITTER_CONSUMER_SECRET;
            string oauthsignaturemethod = "HMAC-SHA1";
            string oauthversion = "1.0";
            string oauthtoken = oauth_access_token;
            string oauthtokensecret = oauth_access_token_secret;
            string oauthnonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            string oauthtimestamp = Convert.ToInt64(ts.TotalSeconds).ToString();
            SortedDictionary<string, string> basestringParameters = new SortedDictionary<string, string>();
            basestringParameters.Add("oauth_version", oauthversion);
            basestringParameters.Add("oauth_consumer_key", oauthconsumerkey);
            basestringParameters.Add("oauth_nonce", oauthnonce);
            basestringParameters.Add("oauth_signature_method", oauthsignaturemethod);
            basestringParameters.Add("oauth_timestamp", oauthtimestamp);
            basestringParameters.Add("oauth_token", oauthtoken);
            //GS - Build the signature string
            StringBuilder baseString = new StringBuilder();
            baseString.Append("GET" + "&");
            baseString.Append(EncodeCharacters(Uri.EscapeDataString("https://api.twitter.com/1.1/account/verify_credentials.json") + "&"));
            foreach (KeyValuePair<string, string> entry in basestringParameters)
            {
                baseString.Append(EncodeCharacters(Uri.EscapeDataString(entry.Key + "=" + entry.Value + "&")));
            }

            //Since the baseString is urlEncoded we have to remove the last 3 chars - %26
            string finalBaseString = baseString.ToString().Substring(0, baseString.Length - 3);

            //Build the signing key
            string signingKey = EncodeCharacters(Uri.EscapeDataString(oauthconsumersecret)) + "&" +
            EncodeCharacters(Uri.EscapeDataString(oauthtokensecret));

            //Sign the request
            HMACSHA1 hasher = new HMACSHA1(new ASCIIEncoding().GetBytes(signingKey));
            string oauthsignature = Convert.ToBase64String(hasher.ComputeHash(new ASCIIEncoding().GetBytes(finalBaseString)));

            //Tell Twitter we don't do the 100 continue thing
            ServicePointManager.Expect100Continue = false;

            //authorization header
            HttpWebRequest hwr = (HttpWebRequest)WebRequest.Create(@"https://api.twitter.com/1.1/account/verify_credentials.json");
            StringBuilder authorizationHeaderParams = new StringBuilder();
            authorizationHeaderParams.Append("OAuth ");
            authorizationHeaderParams.Append("oauth_nonce=" + "\"" + Uri.EscapeDataString(oauthnonce) + "\",");
            authorizationHeaderParams.Append("oauth_signature_method=" + "\"" + Uri.EscapeDataString(oauthsignaturemethod) + "\",");
            authorizationHeaderParams.Append("oauth_timestamp=" + "\"" + Uri.EscapeDataString(oauthtimestamp) + "\",");
            authorizationHeaderParams.Append("oauth_consumer_key=" + "\"" + Uri.EscapeDataString(oauthconsumerkey) + "\",");
            if (!string.IsNullOrEmpty(oauthtoken))
                authorizationHeaderParams.Append("oauth_token=" + "\"" + Uri.EscapeDataString(oauthtoken) + "\",");
            authorizationHeaderParams.Append("oauth_signature=" + "\"" + Uri.EscapeDataString(oauthsignature) + "\",");
            authorizationHeaderParams.Append("oauth_version=" + "\"" + Uri.EscapeDataString(oauthversion) + "\"");
            hwr.Headers["Authorization"] = authorizationHeaderParams.ToString();
            hwr.Method = "GET";
            hwr.ContentType = "application/x-www-form-urlencoded";

            //Allow us a reasonable timeout in case Twitter's busy
            hwr.Timeout = 3 * 60 * 1000;
            string result = getResponce(hwr);
            if (result.Contains("&status=200"))
            {
                string info = result.Replace("&status=200", "");
                JObject ob = JObject.Parse(info);
                if (ob["profile_image_url_https"] != null) { profile_img_url = (string)ob["profile_image_url_https"]; }
                if (ob["name"] != null) { full_name = (string)ob["name"]; if (!string.IsNullOrEmpty(full_name)) screen_name = full_name; }
                // System.Diagnostics.Debug.WriteLine("TW SCREENNAME==>" + screen_name + "\n TW FULLNAME==>" + full_name + "\n TW PROFILEPIC ==>" + profile_img_url + "\n TW ID ==>" + user_id);
                JObject jobJ = new JObject();
                string st = "https://api.twitter.com/1.1/account/verify_credentials.json";
                jobJ["url"] = st;// Auth.utility.HelperMethodsAuth.GetUrlEncoded(st); //EncodeCharacters(Uri.EscapeDataString(st));
                string tmp = "oauth_timestamp=" + oauthtimestamp + "&oauth_version=" + oauthversion + "&oauth_consumer_key=" + oauthconsumerkey
                + "&oauth_signature=" + HelperMethodsAuth.GetUrlEncoded(oauthsignature) + "&oauth_token=" + HelperMethodsAuth.GetUrlEncoded(oauthtoken)
                + "&oauth_nonce=" + oauthnonce + "&oauth_signature_method=" + oauthsignaturemethod;
                jobJ["qs"] = tmp;
                return jobJ.ToString();
                //System.Diagnostics.Debug.WriteLine(jobJ.ToString());
            }
        }
        catch (Exception)
        {
        }
        return null;
    }
    public static string getResponce(HttpWebRequest request)
    {
        try
        {
            HttpWebResponse resp = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(resp.GetResponseStream());
            string result = reader.ReadToEnd();
            reader.Close();
            return result + "&status=200";
        }
        catch (Exception ex)
        {
            string statusCode = "";
            if (ex.Message.Contains("403"))
                statusCode = "403";
            else if (ex.Message.Contains("401"))
                statusCode = "401";
            return string.Format("status={0}&error={1}", statusCode, ex.Message);
        }
    }
    #endregion
}
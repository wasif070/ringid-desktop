﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using View.BindingModels;
using View.Utility.Circle;
using View.ViewModel;
using View.UI;
using System.Threading;
using View.Utility.DataContainer;

namespace View.Utility
{
    public class CircleUtility
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CircleUtility).Name);
        private static CircleUtility _Instance;
        private ThradFetchCircleMembers FetchCircleMembers;
        private ThradLeaveCircle leaveCircle;
        private ThradDeleteCircle deleteCircle;
        private int _Items_Loaded = 0;

        public static CircleUtility Instance
        {
            get
            {
                _Instance = _Instance ?? new CircleUtility();
                return _Instance;
            }
        }

        public void RequestCircleMemberList(long circleId, long pivotId = 0, long limit = 10)
        {
            if (FetchCircleMembers == null)
            {
                FetchCircleMembers = new ThradFetchCircleMembers();
            }
            FetchCircleMembers.StartThread(circleId, pivotId);
        }

        public void RequestCircleMemberListWithFilter(long circleId, long adminPivotId = 0, long memberCategory = -1)
        {
            // new FetchCircleMembers(circleId: circleId, pivotId: adminPivotId, memberCategory: memberCategory, limit: 20);
            if (FetchCircleMembers == null)
            {
                FetchCircleMembers = new ThradFetchCircleMembers();
            }
            FetchCircleMembers.StartThread(circleId: circleId, pivotId: adminPivotId, memberCategory: memberCategory, limit: 20);
        }

        public void DeleteCircle(long circleId)
        {
            if (deleteCircle == null)
            {
                deleteCircle = new ThradDeleteCircle();
            }
            deleteCircle.StartThread(circleId);
        }

        public void LeaveCircle(long circleId)
        {
            if (leaveCircle == null)
            {
                leaveCircle = new ThradLeaveCircle();
            }
            leaveCircle.StartThread(circleId);
        }


        public void LoadAllCirclesToUI(int itemsNeedToLoad = 0)
        {
            try
            {
                new Thread(() =>
                {
                    List<CircleModel> circleModels = CircleDataContainer.Instance.CIRCLE_MODELS_DICTIONARY.Values.Where(P => CircleDataContainer.Instance.CircleIDs.Any(Q => P.UserTableID == Q)).OrderBy(P => P.FullName).ToList();
                    VMCircle.Instance.TotalCircles = circleModels.Count;

                    foreach (CircleModel circleModel in circleModels)
                    {
                        if (circleModel.IntegerStatus != StatusConstants.STATUS_DELETED)
                        {
                            if (circleModel.SuperAdmin == DefaultSettings.LOGIN_RING_ID)
                            {
                                lock (VMCircle.Instance.CircleListYouManage)
                                {
                                    if (!VMCircle.Instance.CircleListYouManage.Any(P => P.UserTableID == circleModel.UserTableID))
                                    {
                                        Application.Current.Dispatcher.BeginInvoke(delegate
                                        {
                                            VMCircle.Instance.CircleListYouManage.Add(circleModel);
                                            Thread.Sleep(10);
                                        });
                                    }
                                }
                            }
                            else
                            {
                                if ((VMCircle.Instance.CircleListYouManage.Count + _Items_Loaded) < itemsNeedToLoad)
                                {
                                    lock (VMCircle.Instance.CircleListYouAreIn)
                                    {
                                        if (!VMCircle.Instance.CircleListYouAreIn.Any(x => x.UserTableID == circleModel.UserTableID))
                                        {
                                            _Items_Loaded++; // because Dispacher adds item a few moments later.
                                            Application.Current.Dispatcher.BeginInvoke(delegate
                                            {
                                                VMCircle.Instance.CircleListYouAreIn.Add(circleModel);
                                                Thread.Sleep(10);
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }).Start();
            }
            catch (Exception ex)
            {
                log.Error("Error: LoadCircleList() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
    }
}

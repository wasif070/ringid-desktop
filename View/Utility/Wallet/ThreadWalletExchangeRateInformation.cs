﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace View.Utility.Wallet
{
    public class ThreadWalletExchangeRateInformation
    {
        private bool running = false;

        private void Run()
        {
            try
            {
                //running = true;

                //JObject pakToSend = new JObject();
                //string pakId = SendToServer.GetRanDomPacketID();
                //pakToSend[JsonKeys.PacketId] = pakId;
                //pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                //pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_COIN_EXCHANGE_RATE_INFORMATION;//1027;

                //JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakId);
                //if (feedbackFields != null)
                //{
                //    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackFields);
                //}
                //else
                //{
                //    WalletViewModel.Instance.IsExchangeRateRequested = false;
                //    WalletViewModel.Instance.ExchangeRateError = true;
                //}                
            }
            catch (Exception ex)
            {

                log4net.LogManager.GetLogger(typeof(ThreadWalletExchangeRateInformation).Name).Error("Error ==> " + ex.Message + Environment.NewLine + ex.StackTrace);
            }
        }

        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(Run);
                th.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning { get { return running; } }
    }
}

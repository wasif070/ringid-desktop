﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace View.Utility.Wallet
{
    public class ThreadGetWalletCurrencyList
    {
        private bool running = false;

        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(Run);
                th.Start();
            }
        }

        private void Run()
        {
            try
            {
                //running = true;
                //JObject pakToSend = new JObject();
                //string pakID = SendToServer.GetRanDomPacketID();
                //pakToSend[JsonKeys.PacketId] = pakID;
                //pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                //pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_CURRENCY_LIST;//1030;

                //JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
                //if (feedbackFields != null)
                //{
                //    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                //}
                //else
                //{
                //    WalletViewModel.Instance.ExchangeRateError = true;
                //}
            }
            catch (Exception)
            {

                //throw;
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning { get { return running; } }
    }
}

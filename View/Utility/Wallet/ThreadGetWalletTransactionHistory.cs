﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using View.BindingModels;

namespace View.Utility.Wallet
{
    public class ThreadGetWalletTransactionHistory
    {
        private bool running = false;
        private int limit = 10;
        
        //public void StartSentTransactionHistory()
        //{
        //    if (!running)
        //    {
        //        System.Threading.Thread th = new System.Threading.Thread(RunSent);
        //        th.Start();
        //    }
        //}

        //public void StartReceivedTransactionHistory()
        //{
        //    if (!running)
        //    {
        //        System.Threading.Thread th = new System.Threading.Thread(RunReceived);
        //        th.Start();
        //    }
        //}

        //public void StartBonusMigrationTransactionHistory()
        //{
        //    if (!running)
        //    {
        //        System.Threading.Thread th = new System.Threading.Thread(RunBonusMigration);
        //        th.Start();
        //    }
        //}

        //private void RunSent()
        //{
        //    try
        //    {
        //        running = true;
        //        JObject pakToSend = new JObject();
        //        string pakID = SendToServer.GetRanDomPacketID();
        //        pakToSend[JsonKeys.PacketId] = pakID;
        //        pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
        //        pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_TRANSACTION_HISTORY;//1031;
        //        pakToSend[JsonKeys.Time] = WalletViewModel.Instance.SentTransactionHistory.Count > 0 ? WalletViewModel.Instance.SentTransactionHistory.Max(x => x.TransactionDate) : 0; //WalletViewModel.Instance.CoinTransactionHistoryTime;
        //        pakToSend[JsonKeys.TransactionType] = StatusConstants.WALLET_TRANSACTION_TYPE_SENT;
        //        pakToSend[JsonKeys.Limit] = limit;
        //        pakToSend[JsonKeys.StartLimit] = WalletViewModel.Instance.SentTransactionHistory.Count;
        //        pakToSend[JsonKeys.Scroll] = 1;

        //        JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
        //        if (feedbackFields != null)
        //        {                    
        //            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
        //        }
        //        else
        //        {
        //            WalletViewModel.Instance.IsSentLoading = false;
        //            WalletViewModel.Instance.SentNetworkError = true;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}

        //private void RunReceived()
        //{
        //    try
        //    {
        //        running = true;
        //        JObject pakToSend = new JObject();
        //        string pakID = SendToServer.GetRanDomPacketID();
        //        pakToSend[JsonKeys.PacketId] = pakID;
        //        pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
        //        pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_TRANSACTION_HISTORY;//1031;
        //        pakToSend[JsonKeys.Time] = WalletViewModel.Instance.ReceivedTransactionHistory.Count > 0? WalletViewModel.Instance.ReceivedTransactionHistory.Max(x=>x.TransactionDate) : 0; //WalletViewModel.Instance.CoinTransactionHistoryTime;
        //        pakToSend[JsonKeys.TransactionType] = StatusConstants.WALLET_TRANSACTION_TYPE_RECEIVED;
        //        pakToSend[JsonKeys.Limit] = limit;
        //        pakToSend[JsonKeys.StartLimit] = WalletViewModel.Instance.ReceivedTransactionHistory.Count;
        //        pakToSend[JsonKeys.Scroll] = 1;

        //        JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
        //        if (feedbackFields != null)
        //        {
        //            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
        //        }
        //        else
        //        {
        //            WalletViewModel.Instance.IsReceivedLoading = false;
        //            WalletViewModel.Instance.ReceivedNetworkError = true;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}

        //private void RunBonusMigration()
        //{
        //    try
        //    {
        //        running = true;
        //        JObject pakToSend = new JObject();
        //        string pakID = SendToServer.GetRanDomPacketID();
        //        pakToSend[JsonKeys.PacketId] = pakID;
        //        pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
        //        pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_TRANSACTION_HISTORY;//1031;
        //        pakToSend[JsonKeys.Time] = WalletViewModel.Instance.BonusMigrationTransactionHistory.Count> 0? WalletViewModel.Instance.BonusMigrationTransactionHistory.Max(x => x.TransactionDate) : 0; //WalletViewModel.Instance.CoinTransactionHistoryTime;
        //        pakToSend[JsonKeys.TransactionType] = StatusConstants.WALLET_TRANSACTION_TYPE_BONUS_MIGRATION;
        //        pakToSend[JsonKeys.Limit] = limit;
        //        pakToSend[JsonKeys.StartLimit] = WalletViewModel.Instance.BonusMigrationTransactionHistory.Count;
        //        pakToSend[JsonKeys.Scroll] = 1;

        //        JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
        //        if (feedbackFields != null)
        //        {
        //            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
        //        }
        //        else
        //        {
        //            WalletViewModel.Instance.IsBonusLoading = false;
        //            WalletViewModel.Instance.BonusNetworkError = true;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning { get { return running; } }
    }
}

﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using View.BindingModels;

namespace View.Utility.Wallet
{
    public class ThreadGetCoinBundleList
    {
        private bool running = false;

        public void StartCoinBundleForPurchase()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(RunPurchase);
                th.Start();
            }
        }
        //public const string keyCoinBundleType = "cnBnType";
        private void RunPurchase()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                string pakID = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = pakID;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_COIN_BUNDLE_LIST;
                pakToSend[WalletConstants.REQ_KEY_COIN_BUNDLE_TYPE] = WalletConstants.COIN_BUNDLE_TYPE_BUY;
                pakToSend[WalletConstants.REQ_KEY_PAYMENT_METHOD_TYPE] = WalletViewModel.Instance.PaymentMethod;

                JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
                WalletViewModel.Instance.CoinBundleListLoading = false;
                if (feedbackFields != null)
                {
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                }
                else
                {
                    WalletViewModel.Instance.CoinBundleListReload = true;
                    System.Diagnostics.Debug.WriteLine("Internet Error! ==> 1038");
                }
            }
            catch (Exception)
            {
                System.Diagnostics.Debug.WriteLine("Exception Error! ==> 1038");
                //throw;
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning { get { return running; } }
    }
}

﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace View.Utility.Wallet
{
    public class ThreadSendGift
    {
        //   {
        //    "rmk": "test",
        //    "toUtId": 507,
        //    "giftSaleDetail": [
        //        {
        //          "productId": 1,
        //          "quantity": 2
        //        },
        //        {
        //          "productId": 2,
        //          "quantity": 2
        //        }], 
        //    "actn": 1040,
        //    "pckId": "2110072255VdDXniOy",
        //    "sId": "384528171238652110072255"
        //   }
        private bool running = false;
        private long utID = 0;
        private Func<bool, string, int> GiftSendHandler;
        private List<BindingModels.WalletGiftProductModel> gifts = new List<BindingModels.WalletGiftProductModel>();
        public void StartSendGift(long utID, List<BindingModels.WalletGiftProductModel> giftProducts, Func<bool, string, int> giftSendHandler = null)
        {
            this.utID = utID;
            this.GiftSendHandler = giftSendHandler;
            this.gifts.Clear();
            this.gifts.AddRange(giftProducts);
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(Run);
                th.Start();
            }
        }
        private void Run()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                string pakID = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.PacketId] = pakID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_SEND_GIFT;
                pakToSend[WalletConstants.REQ_KEY_SENDER_UTID] = this.utID;
                pakToSend[WalletConstants.REQ_KEY_REMARK] = "test";

                JArray giftArr = new JArray();
                foreach (BindingModels.WalletGiftProductModel gift in this.gifts)
                {
                    JObject giftObj = BindingModels.WalletGiftProductModel.MakeGiftObjects(gift);
                    giftArr.Add(giftObj);
                }
                //JObject gift1 = new JObject();
                //gift1["productId"] = 1;
                //gift1["quantity"] = 2;
                //JObject gift2 = new JObject();
                //gift2["productId"] = 2;
                //gift2["quantity"] = 2;
                //giftArr.Add(gift1); 
                //giftArr.Add(gift2);
                pakToSend[WalletConstants.REQ_KEY_GIFT_SALE_DETAILS] = giftArr;
                string message = string.Empty;
                JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
                if (feedbackFields != null)
                {

                    if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success])
                    {
                        if (feedbackFields[WalletConstants.RSP_KEY_MY_WALLET_INFO] != null && ((JObject)feedbackFields[WalletConstants.RSP_KEY_MY_WALLET_INFO])[WalletConstants.RSP_KEY_MY_COIN] != null)
                        {
                            JArray myCoin = (JArray)(((JObject)feedbackFields[WalletConstants.RSP_KEY_MY_WALLET_INFO])[WalletConstants.RSP_KEY_MY_COIN]);
                            foreach (JObject jObj in myCoin)
                            {
                                BindingModels.WalletCoinModel model = BindingModels.WalletCoinModel.GetDataFromJson(jObj);
                                model.UserTableID = DefaultSettings.userProfile.UserTableID;//feedbackFields[JsonKeys.WUserTableId] != null ? (long)feedbackFields[JsonKeys.WUserTableId] : 0;
                                System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                                {
                                    if (model.CoinId != WalletConstants.COIN_ID_DEFAULT)
                                    {
                                        WalletViewModel.Instance.MyCoinStat = model;
                                    }
                                });
                            }
                        }
                        else
                        {
                            new ThreadGetWalletInformation().StartThread();
                        }
                        message = "Gift Sent Successfully!";
                    }
                    else // failed error occured
                    {
                        message = "Eroor Occured!";
                    }
                    if (GiftSendHandler != null)
                    {
                        GiftSendHandler((bool)feedbackFields[JsonKeys.Success], message);
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                }
                else
                {
                    //internet error
                    if (GiftSendHandler != null)
                    {
                        GiftSendHandler(false, "Error occured. Please try later!");
                    }
                }

            }
            catch (Exception)
            {
            }
        }

        //public delegate void OnGiftProcessed(bool success, string message);
        //public event OnGiftProcessed GiftSendHandler;

    }
}

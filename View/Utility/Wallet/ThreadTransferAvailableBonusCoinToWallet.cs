﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using View.BindingModels;
using View.UI;

namespace View.Utility.Wallet
{
    public class ThreadTransferAvailableBonusCoinToWallet
    {
        private bool running = false;
        private int coinId = 1;
        private int coinQuantity = 0;

        private void Run()
        {
            try
            {
                running = true;

                JObject pakToSend = new JObject();
                string pakId = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = pakId;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_TRANSFER_AVAILABLE_BONUS_COIN_TO_WALLET;//1028;
                pakToSend[JsonKeys.RequestCoinID] = this.coinId;
                pakToSend[JsonKeys.TransactionQuantity] = this.coinQuantity;

                JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakId);

                System.Windows.Application.Current.Dispatcher.Invoke(delegate
                {
                    //WalletViewModel.Instance.ToggleBonusCoinMigrationPanel();
                });

                if (feedbackFields != null)
                {
                    if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success]) // success
                    {
                        //WalletCoinModel myCoinModel = WalletViewModel.Instance.MyCoinList.Where(x => x.CoinId == this.coinId).FirstOrDefault();
                        //if (myCoinModel != null)
                        //{
                        //    myCoinModel.Quantity += this.coinQuantity;
                        //}
                        WalletViewModel.Instance.MyCoinStat.Quantity += this.coinQuantity;

                        //WalletCoinModel bonusCoinModel = WalletViewModel.Instance.BonusCoinList.Where(x => x.CoinId == this.coinId).FirstOrDefault();
                        //if (bonusCoinModel != null)
                        //{
                        //    bonusCoinModel.AvailableQuantity -= this.coinQuantity;
                        //    bonusCoinModel.TotalQuantity -= this.coinQuantity;
                        //}
                        System.Windows.Application.Current.Dispatcher.Invoke(delegate
                        {
                            if (UI.UCMiddlePanelSwitcher.View_UCWalletMainPanel != null && Utility.MainSwitcher.PopupController.WalletPopupWrapper != null && Utility.MainSwitcher.PopupController.WalletPopupWrapper.userControl.Content is UI.Wallet.UCCoinExchange)
                            {
                                Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
                            }
                        });

                        UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.COIN_TRANSFER_SUCCESS);
                    }
                    else // succ false
                    {
                        System.Windows.Application.Current.Dispatcher.Invoke(delegate
                        {
                            if (View.UI.UCMiddlePanelSwitcher.View_UCWalletMainPanel != null && View.Utility.MainSwitcher.PopupController.WalletPopupWrapper != null && View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.userControl.Content is View.UI.Wallet.UCCoinExchange)
                            {
                                //WalletViewModel.Instance.CoinTransferFromBonusToWalletErrorMessage = NotificationMessages.TRANSACTION_ERROR;
                            }
                        });
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackFields);
                }
                else // fail
                {
                    System.Windows.Application.Current.Dispatcher.Invoke(delegate
                    {
                        if (View.UI.UCMiddlePanelSwitcher.View_UCWalletMainPanel != null && View.Utility.MainSwitcher.PopupController.WalletPopupWrapper != null && View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.userControl.Content is View.UI.Wallet.UCCoinExchange)
                        {
                            //WalletViewModel.Instance.CoinTransferFromBonusToWalletErrorMessage = NotificationMessages.TRANSACTION_ERROR;
                        }
                    });
                }
            }
            catch (Exception ex)
            {

                log4net.LogManager.GetLogger(typeof(ThreadGetWalletInformation).Name).Error("Error ==> " + ex.Message + Environment.NewLine + ex.StackTrace);
            }
        }

        public void StartThread()
        {
            if (!running)
            {
                this.coinId = 1;
                //this.coinQuantity = Convert.ToInt32(WalletViewModel.Instance.CoinTransactionAmount);
                System.Threading.Thread th = new System.Threading.Thread(Run);
                th.Start();
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning { get { return running; } }
    }
}

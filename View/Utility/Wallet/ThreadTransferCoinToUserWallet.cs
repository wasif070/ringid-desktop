﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using View.BindingModels;
using View.UI;

namespace View.Utility.Wallet
{
    public class ThreadTransferCoinToUserWallet
    {
        private bool running = false;
        private int coinId = 1;
        private long toUtId = 0;
        private int coinQuantity = 0;

        //private void Run()
        //{
        //    try
        //    {
        //        running = true;

        //        JObject pakToSend = new JObject();
        //        string pakId = SendToServer.GetRanDomPacketID();
        //        pakToSend[JsonKeys.PacketId] = pakId;
        //        pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
        //        pakToSend[JsonKeys.Action] = AppConstants.TYPE_TRANSFER_COIN_USER_TO_USER_WALLET;//1029;
        //        pakToSend[JsonKeys.RequestCoinID] = this.coinId;
        //        pakToSend[JsonKeys.TransfereeUtID] = this.toUtId;
        //        pakToSend[JsonKeys.TransactionQuantity] = this.coinQuantity;

        //        if (!string.IsNullOrWhiteSpace(WalletViewModel.Instance.TransactionRemarks))
        //        {
        //            pakToSend[JsonKeys.TransactionRemark] = WalletViewModel.Instance.TransactionRemarks;
        //        }

        //        JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakId);
        //        System.Windows.Application.Current.Dispatcher.Invoke(delegate
        //        {
        //            // WalletViewModel.Instance.ToggleCoinTransferPanel();
        //        });

        //        if (feedbackFields != null)
        //        {
        //            if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success]) // success
        //            {
        //                //WalletCoinModel myCoinModel = WalletViewModel.Instance.MyCoinList.Where(x => x.CoinId == this.coinId).FirstOrDefault();
        //                //if (myCoinModel != null)
        //                //{
        //                //    myCoinModel.Quantity -= this.coinQuantity;
        //                //}
        //                WalletViewModel.Instance.MyCoinStat.Quantity -= this.coinQuantity;

        //                System.Windows.Application.Current.Dispatcher.Invoke(delegate
        //                {
        //                    if (UI.UCMiddlePanelSwitcher.View_UCWalletMainPanel != null && UI.UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCCoinsPanel != null)
        //                    {
        //                        UI.UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCCoinsPanel.LoadCoinStat();
        //                    }
        //                });
        //                UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.COIN_TRANSFER_SUCCESS);
        //            }
        //            else // succ false
        //            {
        //                Wallet.WalletViewModel.Instance.CoinTransferErrorMessage = NotificationMessages.TRANSFER_ERROR;
        //            }
        //            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackFields);
        //        }
        //        else //failed
        //        {
        //            Wallet.WalletViewModel.Instance.CoinTransferErrorMessage = NotificationMessages.WALLET_NETWORK_ERROR;
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        log4net.LogManager.GetLogger(typeof(ThreadTransferCoinToUserWallet).Name).Error("Error ==> " + ex.Message + Environment.NewLine + ex.StackTrace);
        //    }
        //}

        //public void StartThread()//long toUtId = 0,int coinId = 1, int coinQuantity = 0)
        //{
        //    if (!running)
        //    {
        //        this.toUtId = WalletViewModel.Instance.SelectedReferral.ShortInfoModel.UserTableID; //toUtId;
        //        //this.coinId = coinId;
        //        this.coinQuantity = Convert.ToInt32(WalletViewModel.Instance.CoinTransactionAmount);//coinQuantity;
        //        System.Threading.Thread th = new System.Threading.Thread(Run);
        //        th.Start();
        //    }
        //}

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning { get { return running; } }
    }
}

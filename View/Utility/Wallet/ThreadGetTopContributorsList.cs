﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using View.BindingModels;
using View.Utility.Stream;

namespace View.Utility.Wallet
{
    public class ThreadGetTopContributorsList
    {
        private bool running = false;
        private long userTableId = 0;
        public const string keyContributorList = "topContributors";

        public void StartGetTopContributors(long userTableId = 0)
        {
            if (!running)
            {
                this.userTableId = userTableId;
                System.Threading.Thread th = new System.Threading.Thread(RunGetContributorsList);
                th.Start();
            }
        }

        private void RunGetContributorsList()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                string pakID = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = pakID;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_TOP_CONTRIBUTORS;
                pakToSend[JsonKeys.UserTableID] = this.userTableId; // dynamic
                pakToSend[JsonKeys.Limit] = 10;

                JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
                if (feedbackFields != null)
                {
                    //handle true or false
                    if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success] && feedbackFields[WalletConstants.RSP_KEY_TOP_CONTRIBUTORS] != null && ((JArray)feedbackFields[WalletConstants.RSP_KEY_TOP_CONTRIBUTORS]).Count() > 0)
                    {
                        JArray contributorArray = (JArray)feedbackFields[WalletConstants.RSP_KEY_TOP_CONTRIBUTORS];
                        foreach (JObject contributorDTO in contributorArray)
                        {
                            BindingModels.WalletContributorModel wContributorModel = BindingModels.WalletContributorModel.LoadDataFromJson(contributorDTO);
                            if (wContributorModel.UserTableID != -1)
                            {
                                StreamUserModel userModel = new StreamUserModel();
                                userModel.UserTableID = wContributorModel.UserTableID;
                                userModel.UserName = wContributorModel.FullName;
                                userModel.ProfileImage = wContributorModel.ProfileImage;
                                userModel.Contribution = wContributorModel.CoinQuantity;
                                StreamHelpers.AddIntoStreamContributorList(userModel);
                            }
                        }
                    }
                    else // false
                    {

                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                }
                else // internet error
                {

                }
            }
            catch (Exception)
            {
                //exception error
                //throw;
            }
        }
    }
}

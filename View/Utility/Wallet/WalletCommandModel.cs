﻿using System.Windows.Input;

namespace View.Utility.Wallet
{
    public class WalletCommandModel
    {
        private static WalletCommandModel instance; public static WalletCommandModel Instance { get { instance = instance ?? new WalletCommandModel(); return instance; } }
        private ICommand reloadWalletInfoCommand; public ICommand ReloadWalletInfoCommand { get { reloadWalletInfoCommand = reloadWalletInfoCommand ?? new RelayCommand(param => WalletViewModel.Instance.onReloadWalletInfoClicked()); return reloadWalletInfoCommand; } }
        private ICommand buyCoinBundleCommand; public ICommand BuyCoinBundleCommand { get { buyCoinBundleCommand = buyCoinBundleCommand ?? new RelayCommand(param => WalletViewModel.Instance.onBuyCoinBundleClicked(param)); return buyCoinBundleCommand; } }
        private ICommand showCoinRechargeMethodCommand; public ICommand ShowCoinRechargeMethodCommand { get { showCoinRechargeMethodCommand = showCoinRechargeMethodCommand ?? new RelayCommand(param => WalletViewModel.Instance.onShowCoinRechargeMethodClicked()); return showCoinRechargeMethodCommand; } }
        private ICommand buyCoinCommand; public ICommand BuyCoinCommand { get { buyCoinCommand = buyCoinCommand ?? new RelayCommand((param) => WalletViewModel.Instance.onBuyCoinClicked(param)); return buyCoinCommand; } }
        private ICommand loadCoinBundleListCommand; public ICommand LoadCoinBundleListCommand { get { loadCoinBundleListCommand = loadCoinBundleListCommand ?? new RelayCommand((param) => WalletViewModel.Instance.onLoadCoinBundleClicked()); return loadCoinBundleListCommand; } }
        private ICommand loadEarningRuleCommand; public ICommand LoadEarningRuleCommand { get { loadEarningRuleCommand = loadEarningRuleCommand ?? new RelayCommand((param) => WalletViewModel.Instance.onLoadEarningRuleClicked()); return loadEarningRuleCommand; } }
        private ICommand skipReferrerCommand; public ICommand SkipReferrerCommand { get { skipReferrerCommand = skipReferrerCommand ?? new RelayCommand((param) => WalletViewModel.Instance.onSkipReferrerClicked()); return skipReferrerCommand; } }
        private ICommand loadReferrerSummaryCommand; public ICommand LoadReferrerSummaryCommand { get { loadReferrerSummaryCommand = loadReferrerSummaryCommand ?? new RelayCommand((param) => WalletViewModel.Instance.onLoadReferrerSummaryClicked()); return loadReferrerSummaryCommand; } }
        private ICommand showReferralListCommand; public ICommand ShowReferralListCommand { get { showReferralListCommand = showReferralListCommand ?? new RelayCommand((param) => WalletViewModel.Instance.onShowReferralListClick()); return showReferralListCommand; } }
        private ICommand showSetReferrerCommand; public ICommand ShowSetReferrerCommand { get { showSetReferrerCommand = showSetReferrerCommand ?? new RelayCommand((param) => WalletViewModel.Instance.onShowSetReferrerClicked()); return showSetReferrerCommand; } }
        private ICommand setReferrerCommand; public ICommand SetReferrerCommand { get { setReferrerCommand = setReferrerCommand ?? new RelayCommand((param) => WalletViewModel.Instance.onSetReferrerClicked()); return setReferrerCommand; } }
        private ICommand loadReferralListCommand; public ICommand LoadReferralListCommand { get { loadReferralListCommand = loadReferralListCommand ?? new RelayCommand((param) => WalletViewModel.Instance.onLoadReferralCommandClicked()); return loadReferralListCommand; } }
        private ICommand dailyCheckInCommand; public ICommand DailyCheckInCommand { get { dailyCheckInCommand = dailyCheckInCommand ?? new RelayCommand((param) => WalletViewModel.Instance.onDailyCheckInClicked()); return dailyCheckInCommand; } }
        private ICommand loadCheckInCommand; public ICommand LoadCheckInCommand { get { loadCheckInCommand = loadCheckInCommand ?? new RelayCommand((param) => WalletViewModel.Instance.onLoadCheckInClicked()); return loadCheckInCommand; } }
        private ICommand inviteFriendsCommand; public ICommand InviteFriendsCommand { get { inviteFriendsCommand = inviteFriendsCommand ?? new RelayCommand((param) => WalletViewModel.Instance.onInviteFriendClicked()); return inviteFriendsCommand; } }
        private ICommand moreShareCommand; public ICommand MoreShareCommand { get { moreShareCommand = moreShareCommand ?? new RelayCommand((param) => WalletViewModel.Instance.onMoreShareClicked()); return moreShareCommand; } }
        private ICommand switchToMediaCloudSharingCommand; public ICommand SwitchToMediaCloudSharingCommand { get { switchToMediaCloudSharingCommand = switchToMediaCloudSharingCommand ?? new RelayCommand(param => WalletViewModel.Instance.onSwitchToMediaCloudSharingClicked()); return switchToMediaCloudSharingCommand; } }
        private ICommand earnFreeCoinCommand; public ICommand EarnFreeCoinCommand { get { earnFreeCoinCommand = earnFreeCoinCommand ?? new RelayCommand(param => WalletViewModel.Instance.OnEarnFreeCommandClicked(param)); return earnFreeCoinCommand; } }
    }
}

﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace View.Utility.Wallet
{
    public class ThreadGetReferrerSummary
    {
        private bool running = false;

        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(() => Run());
                th.Start();
            }
        }

        private void Run()
        {
            try
            {
                running = true;
                string pakID = SendToServer.GetRanDomPacketID();
                JObject pakToSend = new JObject();
                pakToSend[JsonKeys.PacketId] = pakID;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_REFERRAL_NETWORK_SUMMARY;

                JObject feedbackFields = Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
                WalletViewModel.Instance.ReferrerNetworkLoading = false;
                if (feedbackFields != null)
                {
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID,out feedbackFields);
                }
                else
                {
                    //internet error
                    WalletViewModel.Instance.ReferrerNetworkError = true;
                }
            }
            catch (Exception)
            {

                //throw;
            }
        }
    }
}

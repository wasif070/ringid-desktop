<<<<<<< HEAD
﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace View.Utility.Wallet
{
    public class ThreadGetMyReferralList
    {
        private bool running = false;
        //private int userRank = 1;
        //private int startLimit = 0;
        private int limit = 10;
        private int scroll = 1;
        private bool pendingRequest = false;

        public void StartMyReferralList()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(Run);
                th.Start();
            }
        }

        private void Run()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                string pakID = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = pakID;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;                
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_MY_REFERRALS_LIST;
                if (WalletViewModel.Instance.Referrals.Count>0)
                {
                    pakToSend[JsonKeys.StartLimit] = WalletViewModel.Instance.Referrals.Count;
                }
                JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
                WalletViewModel.Instance.ReferralListLoading = false;
                if (feedbackFields != null)
                {                   
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                }
                else // failed
                {
                    WalletViewModel.Instance.ReferralListNetworkError = true;
                    System.Diagnostics.Debug.WriteLine("Internet Error ==> New 1042");
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception Error ==> New 1042");
                //throw;
            }
        }

        public void StartPendingList()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(RunPending);
                th.Start();
            }
        }
        private void RunPending(object obj)
        {
            try
            {
                //running = true;
                //JObject pakToSend = new JObject();
                //string pakID = SendToServer.GetRanDomPacketID();
                //pakToSend[JsonKeys.PacketId] = pakID;
                //pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                //pakToSend[JsonKeys.StartLimit] = WalletViewModel.Instance.PendingRequestsList.Count > 0 ? WalletViewModel.Instance.PendingRequestsList.Count : 0;//WalletViewModel.Instance.PendingRequestStartLimit;
                //pakToSend[JsonKeys.Action] = AppConstants.TYPE_MY_REFERRALS_LIST;
                //pakToSend[JsonKeys.Scroll] = scroll;
                //pakToSend[JsonKeys.PendingRequests] = true;

                //JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
                //if (feedbackFields != null)
                //{
                //    if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success] && feedbackFields[JsonKeys.ReferralList] != null) // success
                //    {
                //        System.Windows.Application.Current.Dispatcher.Invoke(delegate
                //        {
                //            WalletViewModel.Instance.IsPendingLoading = false;
                //        });
                //        //create pending lists
                //        JArray jArray = (JArray)feedbackFields[JsonKeys.ReferralList];
                //        foreach (JObject jObj in jArray)
                //        {
                //            BindingModels.WalletReferralModel model = BindingModels.WalletReferralModel.LoadData(jObj);

                //            if (model != null && !WalletViewModel.Instance.PendingRequestsList.Any(x => x.UserTableID == model.UserTableID))
                //            {
                //                System.Windows.Application.Current.Dispatcher.Invoke(delegate
                //                {
                //                    WalletViewModel.Instance.PendingRequestsList.InvokeAdd(model);
                //                });

                //            }
                //        }
                //    }
                //    else // faild
                //    {
                //        WalletViewModel.Instance.IsPendingLoading = false;
                //        WalletViewModel.Instance.NoMorePendingReferral = true;
                //    }
                //    WalletViewModel.Instance.IsReferralPanelLoading = false;
                //    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                //}
                //else // failed
                //{
                //    WalletViewModel.Instance.IsPendingLoading = false;
                //    WalletViewModel.Instance.ReferralNetworkError = true;
                //}

            }
            catch (Exception ex)
            {

                //throw;
            }
        }

        public void StartGoldMemberList()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(RunGold);
                th.Start();
            }
        }

        private void RunGold(object obj)
        {
            try
            {
                //running = true;
                //JObject pakToSend = new JObject();
                //string pakID = SendToServer.GetRanDomPacketID();
                //pakToSend[JsonKeys.PacketId] = pakID;
                //pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                //pakToSend[JsonKeys.StartLimit] = WalletViewModel.Instance.GoldMembersList.Count > 0 ? WalletViewModel.Instance.GoldMembersList.Count : 0; //WalletViewModel.Instance.GoldMembersStartLimit;
                //pakToSend[JsonKeys.Action] = AppConstants.TYPE_MY_REFERRALS_LIST;
                //pakToSend[JsonKeys.Scroll] = scroll;
                //pakToSend[JsonKeys.PendingRequests] = false;
                //pakToSend[JsonKeys.UserRanks] = StatusConstants.WALLET_USER_RANK_GOLD;

                //JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
                //if (feedbackFields != null)
                //{
                //    if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success] && feedbackFields[JsonKeys.ReferralList] != null) // success
                //    {
                //        System.Windows.Application.Current.Dispatcher.Invoke(delegate
                //        {
                //            WalletViewModel.Instance.IsMemberListLoading = false;
                //        });

                //        JArray jArray = (JArray)feedbackFields[JsonKeys.ReferralList];
                //        foreach (JObject jObj in jArray)
                //        {
                //            BindingModels.WalletReferralModel model = BindingModels.WalletReferralModel.LoadData(jObj);

                //            if (model != null && !WalletViewModel.Instance.GoldMembersList.Any(x => x.UserTableID == model.UserTableID))
                //            {
                //                System.Windows.Application.Current.Dispatcher.Invoke(delegate
                //                {
                //                    WalletViewModel.Instance.GoldMembersList.InvokeAdd(model);
                //                });

                //            }
                //        }
                //    }
                //    else // no more
                //    {
                //        System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                //        {
                //            WalletViewModel.Instance.IsMemberListLoading = false;                         
                //            WalletViewModel.Instance.NoMoreGoldMembers = true;
                //        });
                //    }
                //    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                //}
                //else // failed
                //{
                //    System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                //    {
                //        WalletViewModel.Instance.MemberLoadingError = true;
                //        WalletViewModel.Instance.IsMemberListLoading = false;                        
                //    });
                //}
            }
            catch (Exception)
            {
            }
        }
        public void StartSilverMemberList()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(RunSilver);
                th.Start();
            }
        }

        private void RunSilver(object obj)
        {
            try
            {
                //running = true;
                //JObject pakToSend = new JObject();
                //string pakID = SendToServer.GetRanDomPacketID();
                //pakToSend[JsonKeys.PacketId] = pakID;
                //pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                //pakToSend[JsonKeys.StartLimit] = WalletViewModel.Instance.SilverMembersList.Count > 0 ? WalletViewModel.Instance.SilverMembersList.Count : 0;//WalletViewModel.Instance.SilverMemberStartLimit;
                //pakToSend[JsonKeys.Action] = AppConstants.TYPE_MY_REFERRALS_LIST;
                //pakToSend[JsonKeys.Scroll] = scroll;
                //pakToSend[JsonKeys.PendingRequests] = false;
                //pakToSend[JsonKeys.UserRanks] = StatusConstants.WALLET_USER_RANK_SILVER;

                //JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
                //if (feedbackFields != null)
                //{
                //    if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success] && feedbackFields[JsonKeys.ReferralList] != null) // success
                //    {
                //        System.Windows.Application.Current.Dispatcher.Invoke(delegate
                //        {
                //            WalletViewModel.Instance.IsMemberListLoading = false;
                //        });

                //        JArray jArray = (JArray)feedbackFields[JsonKeys.ReferralList];
                //        foreach (JObject jObj in jArray)
                //        {
                //            BindingModels.WalletReferralModel model = BindingModels.WalletReferralModel.LoadData(jObj);

                //            if (model != null && !WalletViewModel.Instance.SilverMembersList.Any(x => x.UserTableID == model.UserTableID))
                //            {
                //                System.Windows.Application.Current.Dispatcher.Invoke(delegate
                //                {
                //                    WalletViewModel.Instance.SilverMembersList.InvokeAdd(model);
                //                });

                //            }
                //        }
                //    }
                //    else // no more
                //    {
                //        System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                //        {
                //            WalletViewModel.Instance.IsMemberListLoading = false;
                //            WalletViewModel.Instance.NoMoreSilverMembers = true;
                //        });
                //    }
                //    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                //}
                //else // failed
                //{
                //    System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                //    {
                //        WalletViewModel.Instance.IsMemberListLoading = false;
                //        WalletViewModel.Instance.MemberLoadingError = true;
                //    });
                //}

            }
            catch (Exception)
            {
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning { get { return running; } }
    }
}
=======
﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;

namespace View.Utility.Wallet
{
    public class ThreadGetMyReferralList
    {
        private bool running = false;
        //private int userRank = 1;
        //private int startLimit = 0;
        //private int limit = 10;
        //private int scroll = 1;
        //private bool pendingRequest = false;

        public void StartMyReferralList()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(Run);
                th.Start();
            }
        }

        private void Run()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                string pakID = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = pakID;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;                
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_MY_REFERRALS_LIST;
                if (WalletViewModel.Instance.Referrals.Count>0)
                {
                    pakToSend[JsonKeys.StartLimit] = WalletViewModel.Instance.Referrals.Count;
                }
                JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
                WalletViewModel.Instance.ReferralListLoading = false;
                if (feedbackFields != null)
                {                   
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                }
                else // failed
                {
                    WalletViewModel.Instance.ReferralListNetworkError = true;
                    System.Diagnostics.Debug.WriteLine("Internet Error ==> New 1042");
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception Error ==> New 1042");
                //throw;
            }
        }

        public void StartPendingList()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(RunPending);
                th.Start();
            }
        }
        private void RunPending(object obj)
        {
            try
            {
                //running = true;
                //JObject pakToSend = new JObject();
                //string pakID = SendToServer.GetRanDomPacketID();
                //pakToSend[JsonKeys.PacketId] = pakID;
                //pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                //pakToSend[JsonKeys.StartLimit] = WalletViewModel.Instance.PendingRequestsList.Count > 0 ? WalletViewModel.Instance.PendingRequestsList.Count : 0;//WalletViewModel.Instance.PendingRequestStartLimit;
                //pakToSend[JsonKeys.Action] = AppConstants.TYPE_MY_REFERRALS_LIST;
                //pakToSend[JsonKeys.Scroll] = scroll;
                //pakToSend[JsonKeys.PendingRequests] = true;

                //JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
                //if (feedbackFields != null)
                //{
                //    if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success] && feedbackFields[JsonKeys.ReferralList] != null) // success
                //    {
                //        System.Windows.Application.Current.Dispatcher.Invoke(delegate
                //        {
                //            WalletViewModel.Instance.IsPendingLoading = false;
                //        });
                //        //create pending lists
                //        JArray jArray = (JArray)feedbackFields[JsonKeys.ReferralList];
                //        foreach (JObject jObj in jArray)
                //        {
                //            BindingModels.WalletReferralModel model = BindingModels.WalletReferralModel.LoadData(jObj);

                //            if (model != null && !WalletViewModel.Instance.PendingRequestsList.Any(x => x.UserTableID == model.UserTableID))
                //            {
                //                System.Windows.Application.Current.Dispatcher.Invoke(delegate
                //                {
                //                    WalletViewModel.Instance.PendingRequestsList.InvokeAdd(model);
                //                });

                //            }
                //        }
                //    }
                //    else // faild
                //    {
                //        WalletViewModel.Instance.IsPendingLoading = false;
                //        WalletViewModel.Instance.NoMorePendingReferral = true;
                //    }
                //    WalletViewModel.Instance.IsReferralPanelLoading = false;
                //    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                //}
                //else // failed
                //{
                //    WalletViewModel.Instance.IsPendingLoading = false;
                //    WalletViewModel.Instance.ReferralNetworkError = true;
                //}

            }
            catch (Exception ex)
            {

                //throw;
            }
        }

        public void StartGoldMemberList()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(RunGold);
                th.Start();
            }
        }

        private void RunGold(object obj)
        {
            try
            {
                //running = true;
                //JObject pakToSend = new JObject();
                //string pakID = SendToServer.GetRanDomPacketID();
                //pakToSend[JsonKeys.PacketId] = pakID;
                //pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                //pakToSend[JsonKeys.StartLimit] = WalletViewModel.Instance.GoldMembersList.Count > 0 ? WalletViewModel.Instance.GoldMembersList.Count : 0; //WalletViewModel.Instance.GoldMembersStartLimit;
                //pakToSend[JsonKeys.Action] = AppConstants.TYPE_MY_REFERRALS_LIST;
                //pakToSend[JsonKeys.Scroll] = scroll;
                //pakToSend[JsonKeys.PendingRequests] = false;
                //pakToSend[JsonKeys.UserRanks] = StatusConstants.WALLET_USER_RANK_GOLD;

                //JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
                //if (feedbackFields != null)
                //{
                //    if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success] && feedbackFields[JsonKeys.ReferralList] != null) // success
                //    {
                //        System.Windows.Application.Current.Dispatcher.Invoke(delegate
                //        {
                //            WalletViewModel.Instance.IsMemberListLoading = false;
                //        });

                //        JArray jArray = (JArray)feedbackFields[JsonKeys.ReferralList];
                //        foreach (JObject jObj in jArray)
                //        {
                //            BindingModels.WalletReferralModel model = BindingModels.WalletReferralModel.LoadData(jObj);

                //            if (model != null && !WalletViewModel.Instance.GoldMembersList.Any(x => x.UserTableID == model.UserTableID))
                //            {
                //                System.Windows.Application.Current.Dispatcher.Invoke(delegate
                //                {
                //                    WalletViewModel.Instance.GoldMembersList.InvokeAdd(model);
                //                });

                //            }
                //        }
                //    }
                //    else // no more
                //    {
                //        System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                //        {
                //            WalletViewModel.Instance.IsMemberListLoading = false;                         
                //            WalletViewModel.Instance.NoMoreGoldMembers = true;
                //        });
                //    }
                //    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                //}
                //else // failed
                //{
                //    System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                //    {
                //        WalletViewModel.Instance.MemberLoadingError = true;
                //        WalletViewModel.Instance.IsMemberListLoading = false;                        
                //    });
                //}
            }
            catch (Exception)
            {
            }
        }
        public void StartSilverMemberList()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(RunSilver);
                th.Start();
            }
        }

        private void RunSilver(object obj)
        {
            try
            {
                //running = true;
                //JObject pakToSend = new JObject();
                //string pakID = SendToServer.GetRanDomPacketID();
                //pakToSend[JsonKeys.PacketId] = pakID;
                //pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                //pakToSend[JsonKeys.StartLimit] = WalletViewModel.Instance.SilverMembersList.Count > 0 ? WalletViewModel.Instance.SilverMembersList.Count : 0;//WalletViewModel.Instance.SilverMemberStartLimit;
                //pakToSend[JsonKeys.Action] = AppConstants.TYPE_MY_REFERRALS_LIST;
                //pakToSend[JsonKeys.Scroll] = scroll;
                //pakToSend[JsonKeys.PendingRequests] = false;
                //pakToSend[JsonKeys.UserRanks] = StatusConstants.WALLET_USER_RANK_SILVER;

                //JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
                //if (feedbackFields != null)
                //{
                //    if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success] && feedbackFields[JsonKeys.ReferralList] != null) // success
                //    {
                //        System.Windows.Application.Current.Dispatcher.Invoke(delegate
                //        {
                //            WalletViewModel.Instance.IsMemberListLoading = false;
                //        });

                //        JArray jArray = (JArray)feedbackFields[JsonKeys.ReferralList];
                //        foreach (JObject jObj in jArray)
                //        {
                //            BindingModels.WalletReferralModel model = BindingModels.WalletReferralModel.LoadData(jObj);

                //            if (model != null && !WalletViewModel.Instance.SilverMembersList.Any(x => x.UserTableID == model.UserTableID))
                //            {
                //                System.Windows.Application.Current.Dispatcher.Invoke(delegate
                //                {
                //                    WalletViewModel.Instance.SilverMembersList.InvokeAdd(model);
                //                });

                //            }
                //        }
                //    }
                //    else // no more
                //    {
                //        System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                //        {
                //            WalletViewModel.Instance.IsMemberListLoading = false;
                //            WalletViewModel.Instance.NoMoreSilverMembers = true;
                //        });
                //    }
                //    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                //}
                //else // failed
                //{
                //    System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                //    {
                //        WalletViewModel.Instance.IsMemberListLoading = false;
                //        WalletViewModel.Instance.MemberLoadingError = true;
                //    });
                //}

            }
            catch (Exception)
            {
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning { get { return running; } }
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

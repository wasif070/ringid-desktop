﻿using Models.Constants;
using Models.DAO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using View.BindingModels;
using View.ViewModel;

namespace View.Utility.Wallet
{
    public class WalletViewModel : INotifyPropertyChanged
    {
        public const int SUNDAY = 1;
        public const int MONDAY = 2;
        public const int TUESDAY = 3;
        public const int WEDNESDAY = 4;
        public const int THURSDAY = 5;
        public const int FRIDAY = 6;
        public const int SATURDAY = 7;

        #region < Command and Utility Methods >
        //public void SetMediaShareInfo(string encryptedUrl, int mediaType, int socialMediaType)
        //{
        //    ContentIDHash = encryptedUrl;
        //    ShareContentType = mediaType;
        //    SocialMediaType = socialMediaType;
        //}

        //public void ResetMediaShareInfo()
        //{
        //    MediaShareViaWallet = false;
        //    ContentIDHash = string.Empty;
        //    ShareContentType = Models.Constants.WalletConstants.MEDIA_DEFAULT;
        //    SocialMediaType = Models.Constants.WalletConstants.SOCIAL_MEDIA_DEFAULT;
        //}       

        public void ProcessCheckInList(List<WalletCheckInHistoryModel> previousCheckIns)
        {
            try
            {
                CheckInDays.Clear();
                previousCheckIns.ForEach(CheckInDays.Add);

                if (CheckInDays[7].CheckIn)
                {
                    CheckInDays[7].DayCheckInType = 1;
                }
                for (int idx = 6; idx >= 0; idx--)
                {
                    if (!CheckInDays[idx].CheckIn)
                    {
                        CheckInDays[idx].DayCheckInType = WalletConstants.RECENT_MISSED;
                        if (idx > 0)
                        {
                            for (int jdx = 0; jdx < idx; jdx++)
                            {
                                CheckInDays[jdx].DayCheckInType = CheckInDays[jdx].CheckIn ? WalletConstants.CHECKED_IN_PREVIOUS : WalletConstants.BREAK_CYCLE_DAY;
                            }
                        }
                        else
                        {
                            CheckInDays[0].DayCheckInType = CheckInDays[0].CheckIn ? WalletConstants.CHECKED_IN_PREVIOUS : WalletConstants.BREAK_CYCLE_DAY;
                        }
                        break;
                    }
                    else
                    {
                        CheckInDays[idx].DayCheckInType = WalletConstants.CHECKED_IN_PREVIOUS;
                    }
                }
                for (int idx = 8; idx < 15; idx++)
                {
                    WalletCheckInHistoryModel model = new WalletCheckInHistoryModel
                    {
                        ReportingDate = Convert.ToDateTime(CheckInDays.Last().ReportingDate).AddDays(1).ToString("yyyy-MM-dd"),
                        CheckIn = false,
                        DayCheckInType = WalletConstants.UPCOMING
                    };
                    //model.DayCheckInType = WalletCheckInRules.Where(x => x.DailyCheckInDayTypeID == 4).First().DayNumber == model.ReportingDay ? 3 : 4;
                    CheckInDays.Add(model);
                }
                if (CheckInDays[7].ReportingDay == WalletCheckInRules.Where(x => x.DailyCheckInDayTypeID == WalletConstants.CHECK_IN_TYPE_GIFT).First().DayNumber)
                {
                    CheckInDays[7].DayCheckInType = WalletConstants.GIFT_DAY;
                }
                else
                {
                    for (int idx = 8; idx < 15; idx++)
                    {
                        if (CheckInDays[idx].ReportingDay == WalletCheckInRules.Where(x => x.DailyCheckInDayTypeID == WalletConstants.CHECK_IN_TYPE_GIFT).First().DayNumber)
                        {
                            CheckInDays[idx].DayCheckInType = WalletConstants.GIFT_DAY;
                            break;
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        public void ProcessCheckInRuleCoinDetails()
        {
            DailyCheckInCoinQuantity = WalletCheckInRules.Where(x => x.DailyCheckInDayTypeID == WalletConstants.CHECK_IN_TYPE_DAILY).First().CoinQuantity;
            ConsecutiveCheckInCoinQuantity = WalletCheckInRules.Where(x => x.DailyCheckInDayTypeID == WalletConstants.CHECK_IN_TYPE_CONSECUTIVE).First().CoinQuantity;
        }

        public int SetDayNumber(string day)
        {
            int dayNumber = 0;
            if (day == "sunday")
            {
                dayNumber = SUNDAY;
            }
            else if (day == "monday")
            {
                dayNumber = MONDAY;
            }
            else if (day == "tuesday")
            {
                dayNumber = TUESDAY;
            }
            else if (day == "wednesday")
            {
                dayNumber = WEDNESDAY;
            }
            else if (day == "thursday")
            {
                dayNumber = THURSDAY;
            }
            else if (day == "friday")
            {
                dayNumber = FRIDAY;
            }
            else if (day == "saturday")
            {
                dayNumber = SATURDAY;
            }
            return dayNumber;
        }

        public void FetchInitialDwellTimeData()
        {
            AppStartDateTime = DateTime.UtcNow;
            DwellTimeInMinutes = SettingsConstants.VALUE_WALLET_DWELLING_TIME_IN_MINUTES;
            string[] dates = SettingsConstants.VALUE_WALLET_DWELLING_DATE.Split('-');
            LastDwellingTime = new DateTime(Convert.ToInt32(dates[0]), Convert.ToInt32(dates[1]), Convert.ToInt32(dates[2]));
        }

        public void ProcessWalletDailyDwellingOnStartup()
        {
            if (LastDwellingTime == new DateTime(1970, 1, 1))
            {
                //first time save and init
                new ThreadWalletDailyUtilities().StartSendDailyTaskDwellTime();
            }
            else
            {
                //compare last dwell with appstart
                int day = Math.Abs(AppStartDateTime.Date.Subtract(LastDwellingTime.Date).Days);
                if (day > 0)//different date
                {
                    //send last dwelling_date and dwelling_time_in_minutes then after response reset dwell_time_in_minute and set dwell_date as today's date
                    new ThreadWalletDailyUtilities().StartSendDailyTaskDwellTime();
                }
                else//same date, append dwelling time
                {
                    DwellTimeInMinutes += Convert.ToInt32(new TimeSpan(Math.Abs(AppStartDateTime.Ticks - DateTime.UtcNow.Ticks)).TotalMinutes);
                }
            }

        }

        public void ProcessWalletDailyDwellingOnClose()
        {
            DateTime closingTime = DateTime.UtcNow;
            int days = Math.Abs(closingTime.Date.Subtract(LastDwellingTime.Date).Days);
            if (days > 0)
            {
                DateTime nextDate = LastDwellingTime.Date.AddDays(1);
                DwellTimeInMinutes += Convert.ToInt32(new TimeSpan(closingTime.Ticks - new DateTime(nextDate.Year, nextDate.Month, nextDate.Day).Ticks).TotalMinutes);
            }
            else
            {
                DwellTimeInMinutes += Convert.ToInt32(new TimeSpan(Math.Abs(closingTime.Ticks - AppStartDateTime.Ticks)).TotalMinutes);
            }
            new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_WALLET_DWELLING_TIME_IN_MINUTES, DwellTimeInMinutes.ToString());
        }

        public void ProcessDwellTimeAfterResponse(DateTime currentServerTime)
        {
            LastDwellingTime = currentServerTime.Date;
            DwellTimeInMinutes = 0;
            saveToDatabase();
        }

        public void LoadDefaultMyCoins()
        {
            MyCoinStat = new WalletCoinModel();
            MyCoinStat.CoinId = WalletConstants.COIN_ID_GOLD;
        }

        private void saveToDatabase()
        {
            Dictionary<string, string> dwellTimeInfo = new Dictionary<string, string>();
            dwellTimeInfo[SettingsConstants.KEY_WALLET_DWELLING_DATE] = LastDwellingTime.ToString("yyyy-MM-dd");
            dwellTimeInfo[SettingsConstants.KEY_WALLET_DWELLING_TIME_IN_MINUTES] = DwellTimeInMinutes.ToString();
            new InsertIntoAllRingUsersSettings().StartProcess(dwellTimeInfo);
        }        

        public void onLoadCheckInClicked()
        {
            try
            {
                CheckInReloadRequire = false;
                CheckInLoading = true;
                new ThreadWalletDailyUtilities().StartDailyCheckIn();
            }
            catch (Exception)
            {

                //throw;
            }
        }

        public void onDailyCheckInClicked()
        {
            try
            {
                //System.Windows.Threading.DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer { Interval = TimeSpan.FromSeconds(1) };
                //timer.Tick += ((snd, exc) =>
                //    {
                //        if (WalletCheckInRules.Count>0)
                //        {
                //            timer.Stop();                
                //            if (!IsCheckedIn)
                //            {
                //                new ThreadWalletDailyUtilities().StartDailyCheckIn();
                //            }
                //            else
                //            {
                //                MainSwitcher.PopupController.WalletPopupWrapper.ShowCheckInFailedPanel();
                //            }                            
                //        }
                //    });

                //timer.Start();
                new ThreadWalletDailyUtilities().StartDailyCheckIn();
            }
            catch (Exception)
            {

                //throw;
            }
        }

        public void onLoadReferralCommandClicked()
        {
            try
            {
                ReferralListLoading = true;
                if (ReferralListNetworkError)
                {
                    ReferralListNetworkError = false;
                }
                new ThreadGetMyReferralList().StartMyReferralList();
            }
            catch (Exception)
            {

                //throw;
            }
        }

        public void OnCheckClicked(UserBasicInfoModel model)
        {
            if (model.ShortInfoModel.IsVisibleInTagList)
            {
                SelectedReferrer = model;
            }
            else
            {
                SelectedReferrer = null;
            }
        }

        public void onSetReferrerClicked()
        {
            try
            {
                ReferrerProcessing = true;
                SetReferrerErrorMessage = string.Empty;
                if (SelectedReferrer != null)
                {
                    new ThreadReferralOperation().StartSetReferrer();
                }
                else
                {
                    ReferrerProcessing = false;
                    SetReferrerErrorMessage = "Please select a proper user!";
                }
            }
            catch (Exception)
            {

                //throw;
            }
        }

        public void onShowSetReferrerClicked()
        {
            try
            {
                //MainSwitcher.PopupController.WalletPopupWrapper.ShowSetReferrer();
                MainSwitcher.PopupController.WalletPopupWrapper.ShowPopup(WalletConstants.PU_SET_REFERRER);
            }
            catch (Exception)
            {
            }
        }

        public void onShowReferralListClick()
        {
            try
            {
                //MainSwitcher.PopupController.WalletPopupWrapper.ShowReferralList();
                MainSwitcher.PopupController.WalletPopupWrapper.ShowPopup(WalletConstants.PU_REFERRAL_LIST);
            }
            catch (Exception)
            {
            }
        }

        public void onLoadReferrerSummaryClicked()
        {
            try
            {
                ReferrerNetworkLoading = true;
                ReferrerNetworkError = false;
                new Utility.Wallet.ThreadGetReferrerSummary().StartThread();
            }
            catch (Exception)
            {
            }
        }

        public void onSkipReferrerClicked()
        {
            try
            {
                new ThreadReferralOperation().StartSkipReferrer();
            }
            catch (Exception)
            {

                //throw;
            }
        }

        public void onLoadEarningRuleClicked()
        {
            try
            {
                RuleListReloadRequire = false;
                RuleListLoading = true;
                new Utility.Wallet.ThreadCoinEarningRuleList().StartGetCoinEarningRuleList();
            }
            catch (Exception)
            {

                //throw;
            }
        }

        public void onLoadCoinBundleClicked()
        {
            try
            {
                CoinBundleListReload = false;
                CoinBundleListLoading = true;
                new Utility.Wallet.ThreadGetCoinBundleList().StartCoinBundleForPurchase();
            }
            catch (Exception)
            {

                //throw;
            }
        }

        public void onBuyCoinClicked(object param)
        {
            try
            {
                PaymentMethod = Convert.ToInt32(param);
                //if (ExchangeRateList.Count == 0)
                //{
                //    new ThreadWalletExchangeRateInformation().StartThread();                    
                //}

                //View.Utility.WPFMessageBox.MessegeBoxWithLoader msgBox = View.Utility.WPFMessageBox.CustomMessageBox.ShowMessageWithLoader("Fetching Information. Please wait!");                    
                //System.Windows.Threading.DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer { Interval = TimeSpan.FromMilliseconds(250) };                
                //timer.Tick += (ss, ee) =>
                //{
                //    if (ExchangeRateList.Count > 0)
                //    {
                //        timer.Stop();
                //        System.Threading.Thread.Sleep(1000);
                //        msgBox.CloseMessage();
                //        Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowBuyCoinPanel();
                //    }
                //};
                //timer.Start();
                Utility.MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
                //Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowBuyCoinPanel();
                Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowPopup(WalletConstants.PU_COIN_BUNDLE_PANEL);
            }
            catch (Exception)
            {

                //throw;
            }
        }

        public void onShowCoinRechargeMethodClicked()
        {
            //Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowCoinRechargeMethods();
            Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowPopup(WalletConstants.PU_COIN_RECHARGE_METHODS);
        }

        public void onBuyCoinBundleClicked(object param)
        {
            try
            {
                //MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
                int coinBundleID = (int)param;
                new ThreadPurchaseCoin().StartPurchaseCoin(coinBundleID);
            }
            catch (Exception)
            {

                //throw;
            }
        }

        public void onReloadWalletInfoClicked()
        {
            IsCoinStatsLoading = true;
            CoinStatsError = false;
            new ThreadGetWalletInformation().StartThread();
        }

        public void onSwitchToMediaCloudSharingClicked()
        {
            MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
            //MediaShareViaWallet = true;
            RingIDViewModel.Instance.MediaShareViaWallet = true;
            MiddlePanelSwitcher.Switch(View.Constants.MiddlePanelConstants.TypeMediaCloud);
        }

        public void onMoreShareClicked()
        {
            //MainSwitcher.PopupController.WalletPopupWrapper.ShowMediaSharePopup();
            MainSwitcher.PopupController.WalletPopupWrapper.ShowPopup(WalletConstants.PU_MEDIA_SHARE_MESSAGE);
        }

        public void onInviteFriendClicked()
        {
            //MiddlePanelSwitcher.Switch(View.Constants.MiddlePanelConstants.invi)
            //MainSwitcher.PopupController.WalletPopupWrapper.ShowInvitePanel();
            MainSwitcher.PopupController.WalletPopupWrapper.ShowPopup(WalletConstants.PU_INVITE);
            //MiddlePanelSwitcher.Switch(View.Constants.MiddlePanelConstants.TypeAddFriend);            
        }

        public void OnEarnFreeCommandClicked(object param)
        {
            int ruleItemID = (int)param;
            switch (ruleItemID)
            {
                case WalletConstants.COIN_EARNING_RULE_INVITE_FRIENDS:
                    onInviteFriendClicked();
                    break;
                case WalletConstants.COIN_EARNING_RULE_DAILY_CHECK_IN:
                    onDailyCheckInClicked();
                    break;
                case WalletConstants.COIN_EARNING_RULE_MORE_SHARE:
                    onMoreShareClicked();
                    break;
                default:
                    break;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        private static WalletViewModel instance;
        public static WalletViewModel Instance { get { instance = instance ?? new WalletViewModel(); return instance; } }

        private int paymentMethod = WalletConstants.PAYMENT_METHOD_DEFAULT;
        public int PaymentMethod { get { return paymentMethod; } set { paymentMethod = value; this.OnPropertyChanged("PaymentMethod"); } }

        #region Coin_Stats_1026 (My Coins and Bonus Coins)
        private WalletCoinModel myCoinStat = new WalletCoinModel();
        public WalletCoinModel MyCoinStat { get { return myCoinStat; } set { myCoinStat = value; this.OnPropertyChanged("MyCoinStat"); } }

        private bool isCoinStatsRequested = false;
        public bool IsCoinStatsRequested { get { return isCoinStatsRequested; } set { isCoinStatsRequested = value; this.OnPropertyChanged("IsCoinStatsRequested"); } }

        private bool isCoinStatsLoading = false;
        public bool IsCoinStatsLoading { get { return isCoinStatsLoading; } set { isCoinStatsLoading = value; this.OnPropertyChanged("IsCoinStatsLoading"); } }

        private bool coinStatsError = false;
        public bool CoinStatsError { get { return coinStatsError; } set { coinStatsError = value; this.OnPropertyChanged("CoinStatsError"); } }
        #endregion

        #region Wallet Coin Bundles
        private ObservableCollection<WalletCoinBundleModel> coinBundles = new ObservableCollection<WalletCoinBundleModel>();
        public ObservableCollection<WalletCoinBundleModel> CoinBundles { get { return coinBundles; } set { coinBundles = value; this.OnPropertyChanged("CoinBundles"); } }

        private bool coinBundleListReload = false;
        public bool CoinBundleListReload { get { return coinBundleListReload; } set { coinBundleListReload = value; this.OnPropertyChanged("CoinBundleListReload"); } }

        private bool coinBundleListLoading = false;
        public bool CoinBundleListLoading { get { return coinBundleListLoading; } set { coinBundleListLoading = value; this.OnPropertyChanged("CoinBundleListLoading"); } }        
        #endregion

        private ObservableCollection<WalletGiftProductModel> _GiftProductsList = new ObservableCollection<WalletGiftProductModel>();
        public ObservableCollection<WalletGiftProductModel> GiftProductsList { get { return _GiftProductsList; } set { _GiftProductsList = value; this.OnPropertyChanged("GiftProductsList"); } }
        
        #region Referral Coin Earning Rule List
        private ObservableCollection<WalletReferralRuleListModel> coinEarningRules = new ObservableCollection<WalletReferralRuleListModel>();
        public ObservableCollection<WalletReferralRuleListModel> CoinEarningRules { get { return coinEarningRules; } set { coinEarningRules = value; this.OnPropertyChanged("CoinEarningRules"); } }

        private bool ruleListLoading = false;
        public bool RuleListLoading { get { return ruleListLoading; } set { ruleListLoading = value; this.OnPropertyChanged("RuleListLoading"); } }

        private bool ruleListReloadRequire = false;
        public bool RuleListReloadRequire { get { return ruleListReloadRequire; } set { ruleListReloadRequire = value; this.OnPropertyChanged("RuleListReloadRequire"); } }
        #endregion

        #region Referrer Task
        private bool isReferrerSet = false;
        public bool IsReferrerSet { get { return isReferrerSet; } set { isReferrerSet = value; this.OnPropertyChanged("IsReferrerSet"); } }

        private bool isReferrerSkipped = false;
        public bool IsReferrerSkipped { get { return isReferrerSkipped; } set { isReferrerSkipped = value; this.OnPropertyChanged("IsReferrerSkipped"); } }

        private string setReferrerErrorMessage = string.Empty;
        public string SetReferrerErrorMessage { get { return setReferrerErrorMessage; } set { setReferrerErrorMessage = value; this.OnPropertyChanged("SetReferrerErrorMessage"); } }

        private UserBasicInfoModel selectedReferrer = null;
        public UserBasicInfoModel SelectedReferrer { get { return selectedReferrer; } set { selectedReferrer = value; this.OnPropertyChanged("SelectedReferrer"); } }

        private UserBasicInfoModel referrer = null;
        public UserBasicInfoModel Referrer { get { return referrer; } set { referrer = value; this.OnPropertyChanged("Referrer"); } }

        private int referralCount = 0;
        public int ReferralCount { get { return referralCount; } set { referralCount = value; this.OnPropertyChanged("ReferralCount"); } }

        private ObservableCollection<UserBasicInfoModel> referrals = new ObservableCollection<UserBasicInfoModel>();
        public ObservableCollection<UserBasicInfoModel> Referrals { get { return referrals; } set { referrals = value; this.OnPropertyChanged("Referrals"); } }

        private bool referrerResponseSuccess = false;
        public bool ReferrerResponseSuccess { get { return referrerResponseSuccess; } set { referrerResponseSuccess = value; this.OnPropertyChanged("ReferrerResponseSuccess"); } }

        private bool referrerNetworkError = false;
        public bool ReferrerNetworkError { get { return referrerNetworkError; } set { referrerNetworkError = value; this.OnPropertyChanged("ReferrerNetworkError"); } }

        private bool referrerNetworkLoading = false;
        public bool ReferrerNetworkLoading { get { return referrerNetworkLoading; } set { referrerNetworkLoading = value; this.OnPropertyChanged("ReferrerNetworkLoading"); } }

        private bool endOfReferrer = false;
        public bool EndOfReferrer { get { return endOfReferrer; } set { endOfReferrer = value; this.OnPropertyChanged("EndOfReferrer"); } }

        private bool referrerProcessing = false;
        public bool ReferrerProcessing { get { return referrerProcessing; } set { referrerProcessing = value; this.OnPropertyChanged("ReferrerProcessing"); } }

        private bool referralListNetworkError = false;
        public bool ReferralListNetworkError { get { return referralListNetworkError; } set { referralListNetworkError = value; this.OnPropertyChanged("ReferralListNetworkError"); } }

        private bool endOfReferralList = false;
        public bool EndOfReferralList { get { return endOfReferralList; } set { endOfReferralList = value; this.OnPropertyChanged("EndOfReferralList"); } }

        private bool referralListLoading = false;
        public bool ReferralListLoading { get { return referralListLoading; } set { referralListLoading = value; this.OnPropertyChanged("ReferralListLoading"); } }

        private bool initialRequestReferralList = false;
        public bool InitialRequestReferralList
        {
            get { return initialRequestReferralList; }
            set { initialRequestReferralList = value; this.OnPropertyChanged("InitialRequestReferralList"); }
        }

        #endregion

        #region Dwelling Time
        private int dwellTimeInMinutes = 0;
        public int DwellTimeInMinutes { get { return dwellTimeInMinutes; } set { dwellTimeInMinutes = value; this.OnPropertyChanged("DwellTimeInMinutes"); } }

        private DateTime lastDwellingTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        public DateTime LastDwellingTime { get { return lastDwellingTime; } set { lastDwellingTime = value; this.OnPropertyChanged("LastDwellingTime"); } }

        private DateTime appStartDateTime;
        public DateTime AppStartDateTime { get { return appStartDateTime; } set { appStartDateTime = value; this.OnPropertyChanged("AppStartDateTime"); } }
        #endregion

        #region CheckIn
        private bool checkInLoading = false;
        public bool CheckInLoading { get { return checkInLoading; } set { checkInLoading = value; this.OnPropertyChanged("CheckInLoading"); } }

        private bool isCheckedIn = false;
        public bool IsCheckedIn { get { return isCheckedIn; } set { isCheckedIn = value; this.OnPropertyChanged("IsCheckedIn"); } }

        private bool checkInReloadRequire = false;
        public bool CheckInReloadRequire { get { return checkInReloadRequire; } set { checkInReloadRequire = value; this.OnPropertyChanged("CheckInReloadRequire"); } }

        private ObservableCollection<WalletCheckInHistoryModel> checkInDays = new ObservableCollection<WalletCheckInHistoryModel>();
        public ObservableCollection<WalletCheckInHistoryModel> CheckInDays { get { return checkInDays; } set { checkInDays = value; this.OnPropertyChanged("CheckInDays"); } }       

        private ObservableCollection<WalletCheckInRuleModel> walletCheckInRules = new ObservableCollection<WalletCheckInRuleModel>();
        public ObservableCollection<WalletCheckInRuleModel> WalletCheckInRules { get { return walletCheckInRules; } set { walletCheckInRules = value; this.OnPropertyChanged("CheckInRules"); } }

        public int DailyCheckInCoinQuantity { get; set; }
        public int ConsecutiveCheckInCoinQuantity { get; set; }
        #endregion        

        #region Media Sharing Tools
        //private bool mediaShareViaWallet = false;
        //public bool MediaShareViaWallet { get { return mediaShareViaWallet; } set { mediaShareViaWallet = value; this.OnPropertyChanged("MediaShareViaWallet"); } }

        //private string contentIDHash  = string.Empty;
        //public string ContentIDHash { get { return contentIDHash; } set { contentIDHash = value; this.OnPropertyChanged("ContentIDHash"); } }

        //private int shareContentType = WalletConstants.MEDIA_DEFAULT;
        //public int ShareContentType { get { return shareContentType; } set { shareContentType = value; this.OnPropertyChanged("ShareContentType"); } }

        //private int sociaMediaType = WalletConstants.SOCIAL_MEDIA_DEFAULT;
        //public int SocialMediaType { get { return sociaMediaType; } set { sociaMediaType = value; this.OnPropertyChanged("SocialMediaType"); } }       
        #endregion     
        #region <Obsolete 01>
        //public event EventHandler PinNumberSuccessRequested;
        //private void onPinNumberSuccessRequested()
        //{
        //    var handler = PinNumberSuccessRequested;
        //    if (handler != null)
        //    {
        //        handler(this, EventArgs.Empty);
        //    }
        //}

        //public delegate void OnCoinTransactionToggled(bool enable);
        //public event OnCoinTransactionToggled CoinTransactionToggleHandler;

        //public delegate void OnBonusCoinMigrationToggled(bool enable);
        //public event OnBonusCoinMigrationToggled BonusCoinMigrationToggleHandler;

        //public delegate void OnReferralIncomingPanelToggled(bool enable);
        //public event OnReferralIncomingPanelToggled ReferralIncomingPanelToggleHandler;

        //public delegate void OnSendReferralToggleRequested(bool enable);
        //public event OnSendReferralToggleRequested SendReferralToggleHandler;

        //public void ToggleCoinTransferPanel(bool enable = true)
        //{
        //    if (CoinTransactionToggleHandler != null)
        //    {
        //        CoinTransactionToggleHandler(enable);
        //    }
        //}

        //public void ToggleBonusCoinMigrationPanel(bool enable = true)
        //{
        //    if (BonusCoinMigrationToggleHandler != null)
        //    {
        //        BonusCoinMigrationToggleHandler(enable);
        //    }
        //}

        //public void ToggleReferralIncomingPanel(bool enable = true)
        //{
        //    if (ReferralIncomingPanelToggleHandler != null)
        //    {
        //        ReferralIncomingPanelToggleHandler(enable);
        //    }
        //}

        //public void ToggleSendReferralPanel(bool enable = true)
        //{
        //    if (SendReferralToggleHandler != null)
        //    {
        //        SendReferralToggleHandler(enable);
        //    }
        //}

        //private void onPinNumberProceedClicked()
        //{
        //    //string password = param as string;
        //    WalletPinErrorMessage = string.Empty;
        //    if (string.IsNullOrWhiteSpace(InputPin))
        //    {
        //        WalletPinErrorMessage = NotificationMessages.WALLET_PIN_NUMBER_BLANK;
        //        return;
        //    }
        //    if (InputPin.Length < 4)
        //    {
        //        WalletPinErrorMessage = NotificationMessages.WALLET_PIN_NUMBER_LENGTH_ERROR;
        //        return;
        //    }
        //    if (!isNumeric(InputPin))
        //    {
        //        WalletPinErrorMessage = NotificationMessages.WALLET_PIN_NUMBER_NUMERIC_ERROR;
        //        return;
        //    }
        //    if (!IsPinNumberSet)
        //    {
        //        if (IsResetPinNumber)
        //        {
        //            if (Password != DefaultSettings.VALUE_LOGIN_USER_PASSWORD)
        //            {
        //                WalletPinErrorMessage = NotificationMessages.WALLET_INCORRECT_USER_PASSWORD;
        //                return;
        //            }
        //        }
        //        if (string.IsNullOrWhiteSpace(ConfirmPin))
        //        {
        //            WalletPinErrorMessage = NotificationMessages.WALLET_CONFIRMATION_PIN_NOT_PROVIDED;
        //            return;
        //        }
        //        if (InputPin != ConfirmPin)
        //        {
        //            WalletPinErrorMessage = NotificationMessages.WALLET_PIN_NUMBERS_NOT_SAME;
        //            return;
        //        }
        //        SettingsConstants.VALUE_WALLET_PIN_NUMBER = InputPin;
        //        new InsertIntoAllRingUsersSettings().UpadateSingleValue(SettingsConstants.KEY_WALLET_PIN_NUMBER, InputPin);
        //        IsPinNumberSet = true;
        //        IsResetPinNumber = false;
        //        switchToWalletPanel();
        //    }
        //    else
        //    {
        //        if (SettingsConstants.VALUE_WALLET_PIN_NUMBER == InputPin)
        //        {
        //            switchToWalletPanel();
        //        }
        //        else
        //        {
        //            WalletPinErrorMessage = NotificationMessages.WALLET_INCORRECT_PIN_NUMBER;
        //            return;
        //        }
        //    }
        //}

        //private void onForgetPinClicked()
        //{
        //    this.IsResetPinNumber = true;
        //    this.IsPinNumberSet = false;
        //    (View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.userControl.Content as View.UI.Wallet.UCWalletPinNumber).passwordBox.Focus();
        //}

        //private void onCoinExchangeClicked()
        //{
        //    View.Utility.MainSwitcher.PopupController.WalletPopupWrapper.ShowCoinExchangePopup();
        //}

        //private void onCoinTransactionClicked()
        //{
        //    CoinTransferErrorMessage = string.Empty;
        //    if (SelectedReferral == null)
        //    {
        //        CoinTransferErrorMessage = NotificationMessages.TRANSFER_REFERRAL_ERROR;
        //        return;
        //    }
        //    if (!(!string.IsNullOrWhiteSpace(CoinTransactionAmount) && isNumeric(CoinTransactionAmount) && Convert.ToUInt32(CoinTransactionAmount) > 0))
        //    {
        //        CoinTransferErrorMessage = NotificationMessages.TRANSFER_AMOUNT_ERROR;
        //        return;
        //    }
        //    ToggleCoinTransferPanel(false);
        //    new View.Utility.Wallet.ThreadTransferCoinToUserWallet().StartThread();
        //}

        //private void onSendReferralClicked()
        //{
        //    SendReferralErrorMessage = string.Empty;
        //    if (SelectedReferral == null)
        //    {
        //        SendReferralErrorMessage = NotificationMessages.REFERRAL_ERROR;
        //        return;
        //    }
        //    this.ToggleSendReferralPanel(false);
        //    new ThreadReferralOperation().StartSendReferral();
        //}

        //private void onTransferBonusCoinToWalletClicked()
        //{
        //    CoinTransferFromBonusToWalletErrorMessage = string.Empty;
        //    if (!string.IsNullOrWhiteSpace(CoinTransactionAmount) && isNumeric(CoinTransactionAmount) && Convert.ToInt32(CoinTransactionAmount) > 0)
        //    {
        //        ToggleBonusCoinMigrationPanel(false);
        //        new ThreadTransferAvailableBonusCoinToWallet().StartThread();
        //    }
        //    else
        //    {
        //        CoinTransferFromBonusToWalletErrorMessage = NotificationMessages.AMOUNT_ERROR;
        //        return;
        //    }
        //}

        //private void onReferralAcceptClicked(object param)
        //{
        //    ToggleReferralIncomingPanel(false);
        //    ReferralAcceptOrRejectUtID = 0;
        //    long utID = (long)param;
        //    if (utID > 0)
        //    {
        //        ReferralAcceptOrRejectUtID = utID;
        //        new ThreadReferralOperation().StartAcceptReferralRequest();
        //    }
        //}

        //private void onReferralRejectClicked(object param)
        //{
        //    ToggleReferralIncomingPanel(false);
        //    ReferralAcceptOrRejectUtID = 0;
        //    long utID = (long)param;
        //    if (utID > 0)
        //    {
        //        ReferralAcceptOrRejectUtID = utID;
        //        new ThreadReferralOperation().StartRejectReferralRequest();
        //    }
        //}

        //private void onPendingShowMoreClicked()
        //{
        //    IsPendingLoading = true;
        //    new ThreadGetMyReferralList().StartPendingList();
        //}

        //private void onReloadReferralPanelClicked()
        //{
        //    try
        //    {
        //        ReferralNetworkError = false;
        //        IsPendingLoading = true;
        //        IsReferralPanelLoading = true;
        //        new Utility.Wallet.ThreadGetWalletReferralNetworkSummary().StartThread();
        //        new Utility.Wallet.ThreadGetMyReferralList().StartPendingList();
        //    }
        //    catch (Exception)
        //    {
        //        //throw;
        //    }
        //}

        //private void onReloadAcceptedReferralListClicked()
        //{
        //    try
        //    {
        //        MemberLoadingError = false;
        //        IsMemberListLoading = true;
        //        switch (AcceptedReferralListType)
        //        {
        //            case StatusConstants.WALLET_USER_RANK_GOLD:
        //                new ThreadGetMyReferralList().StartGoldMemberList();
        //                break;
        //            case StatusConstants.WALLET_USER_RANK_SILVER:
        //                new ThreadGetMyReferralList().StartSilverMemberList();
        //                break;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}

        //private void onLoadSentTransactionClicked()
        //{
        //    try
        //    {
        //        SentNetworkError = false;
        //        IsSentLoading = true;
        //        new ThreadGetWalletTransactionHistory().StartSentTransactionHistory();
        //    }
        //    catch (Exception)
        //    {
        //        //throw;
        //    }
        //}

        //private void onLoadReceivedTransactionClicked()
        //{
        //    try
        //    {
        //        ReceivedNetworkError = false;
        //        IsReceivedLoading = true;
        //        new ThreadGetWalletTransactionHistory().StartReceivedTransactionHistory();
        //    }
        //    catch (Exception)
        //    {
        //        //throw;
        //    }
        //}

        //private void onLoadBonusMigrationClicked()
        //{
        //    try
        //    {
        //        BonusNetworkError = false;
        //        IsBonusLoading = true;
        //        new ThreadGetWalletTransactionHistory().StartBonusMigrationTransactionHistory();
        //    }
        //    catch (Exception)
        //    {
        //        //throw;
        //    }
        //}

        //private void onLoadExchangeRateClicked()
        //{
        //    try
        //    {
        //        IsExchangeRateLoading = true;
        //        ExchangeRateError = false;
        //        new ThreadWalletExchangeRateInformation().StartThread();
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}

        //private void switchToWalletPanel()
        //{
        //    Password = string.Empty;
        //    WalletPinErrorMessage = string.Empty;
        //    InputPin = string.Empty;
        //    ConfirmPin = string.Empty;
        //    IsLoggedIn = true;
        //    this.onPinNumberSuccessRequested();
        //}

        //private bool isNumeric(string input)
        //{
        //    System.Text.RegularExpressions.Regex numRegex = new System.Text.RegularExpressions.Regex(@"^\d+$");
        //    System.Diagnostics.Debug.WriteLine(numRegex.IsMatch(input));
        //    return numRegex.IsMatch(input);
        //}

        //public void LoadDummy()
        //{
        //    //MyCoinList = new ObservableCollection<WalletCoinModel>
        //    //{
        //    //    new WalletCoinModel{CoinId = 1, CoinName = "Gold", AvailableQuantity = 10, Quantity = 40, TotalQuantity = 50},
        //    //    new WalletCoinModel{CoinId = 2, CoinName = "Silver", AvailableQuantity = 10, Quantity = 40, TotalQuantity = 50},
        //    //    new WalletCoinModel{CoinId = 3, CoinName = "Bronze", AvailableQuantity = 10, Quantity = 40, TotalQuantity = 50},
        //    //};
        //    BonusCoinList = new ObservableCollection<WalletCoinModel>
        //    {
        //        new WalletCoinModel{CoinId = 1, CoinName = "Gold", AvailableQuantity = 10, Quantity = 40, TotalQuantity = 50},
        //        new WalletCoinModel{CoinId = 2, CoinName = "Silver", AvailableQuantity = 10, Quantity = 40, TotalQuantity = 50},
        //        new WalletCoinModel{CoinId = 3, CoinName = "Bronze", AvailableQuantity = 10, Quantity = 40, TotalQuantity = 50},
        //    };
        //    //NoMyCoinsAvailable = false;
        //    //NoBonusCoinAvailable = false;
        //}

        //public void LoadDefaultBonusCoins()
        //{
        //    if (!BonusCoinList.Any(x => x.CoinId == 1))
        //    {
        //        BonusCoinList.InvokeAdd(new WalletCoinModel { CoinId = 1, CoinName = "Gold", AvailableQuantity = 0, TotalQuantity = 0, UserTableID = DefaultSettings.LOGIN_TABLE_ID });
        //    }
        //    if (!BonusCoinList.Any(x => x.CoinId == 2))
        //    {
        //        BonusCoinList.InvokeAdd(new WalletCoinModel { CoinId = 2, CoinName = "Silver", AvailableQuantity = 0, TotalQuantity = 0, UserTableID = DefaultSettings.LOGIN_TABLE_ID });
        //    }
        //    if (!BonusCoinList.Any(x => x.CoinId == 3))
        //    {
        //        BonusCoinList.InvokeAdd(new WalletCoinModel { CoinId = 3, CoinName = "Bronze", AvailableQuantity = 0, TotalQuantity = 0, UserTableID = DefaultSettings.LOGIN_TABLE_ID });
        //    }
        //}
        #endregion
        #region <Obsolete Commands>
        //private ICommand pinNumberProceedCommand;
        //public ICommand PinNumberProceedCommand
        //{
        //    get
        //    {
        //        pinNumberProceedCommand = pinNumberProceedCommand ?? new RelayCommand(param => onPinNumberProceedClicked()); return pinNumberProceedCommand;
        //    }
        //}

        //private ICommand forgetPinCommand;
        //public ICommand ForgetPinCommand
        //{
        //    get
        //    {
        //        forgetPinCommand = forgetPinCommand ?? new RelayCommand(param => onForgetPinClicked()); return forgetPinCommand;
        //    }
        //}

        //private ICommand coinExchangeCommand;
        //public ICommand CoinExchangeCommand
        //{
        //    get
        //    {
        //        coinExchangeCommand = coinExchangeCommand ?? new RelayCommand(param => onCoinExchangeClicked()); return coinExchangeCommand;
        //    }
        //}

        //private ICommand coinTransactionCommand;
        //public ICommand CoinTransactionCommand
        //{
        //    get
        //    {
        //        coinTransactionCommand = coinTransactionCommand ?? new RelayCommand(param => onCoinTransactionClicked()); return coinTransactionCommand;
        //    }
        //}

        //private ICommand sendReferralCommand;
        //public ICommand SendReferralCommand
        //{
        //    get
        //    {
        //        sendReferralCommand = sendReferralCommand ?? new RelayCommand(param => onSendReferralClicked()); return sendReferralCommand;
        //    }
        //}

        //private ICommand transferBonusCoinToWalletCommand;
        //public ICommand TransferBonusCoinToWalletCommand
        //{
        //    get
        //    {
        //        transferBonusCoinToWalletCommand = transferBonusCoinToWalletCommand ?? new RelayCommand(param => onTransferBonusCoinToWalletClicked()); return transferBonusCoinToWalletCommand;
        //    }
        //}

        //private ICommand referralAcceptCommand;
        //public ICommand ReferralAcceptCommand
        //{
        //    get
        //    {
        //        referralAcceptCommand = referralAcceptCommand ?? new RelayCommand(param => onReferralAcceptClicked(param)); return referralAcceptCommand;
        //    }
        //}

        //private ICommand referralRejectCommand;
        //public ICommand ReferralRejectCommand
        //{
        //    get
        //    {
        //        referralRejectCommand = referralRejectCommand ?? new RelayCommand(param => onReferralRejectClicked(param)); return referralRejectCommand;
        //    }
        //}

        //private ICommand pendingShowMoreCommand;
        //public ICommand PendingShowMoreCommand
        //{
        //    get
        //    {
        //        pendingShowMoreCommand = pendingShowMoreCommand ?? new RelayCommand(param => onPendingShowMoreClicked()); return pendingShowMoreCommand;
        //    }
        //}

        //private ICommand reloadWalletInfoCommand;
        //public ICommand ReloadWalletInfoCommand
        //{
        //    get
        //    {
        //        reloadWalletInfoCommand = reloadWalletInfoCommand ?? new RelayCommand(param => onReloadWalletInfoClicked()); return reloadWalletInfoCommand;
        //    }
        //}

        //private ICommand reloadReferralPanelCommand;
        //public ICommand ReloadReferralPanelCommand
        //{
        //    get
        //    {
        //        reloadReferralPanelCommand = reloadReferralPanelCommand ?? new RelayCommand(param => onReloadReferralPanelClicked()); return reloadReferralPanelCommand;
        //    }
        //}

        //private ICommand reloadAcceptedReferralListCommand;
        //public ICommand ReloadAcceptedReferralListCommand
        //{
        //    get
        //    {
        //        reloadAcceptedReferralListCommand = reloadAcceptedReferralListCommand ?? new RelayCommand(param => onReloadAcceptedReferralListClicked()); return reloadAcceptedReferralListCommand;
        //    }
        //}

        //private ICommand loadSentTransactionCommand;
        //public ICommand LoadSentTransactionCommand
        //{
        //    get
        //    {
        //        loadSentTransactionCommand = loadSentTransactionCommand ?? new RelayCommand(param => onLoadSentTransactionClicked()); return loadSentTransactionCommand;
        //    }
        //}

        //private ICommand loadReceivedTransactionCommand;
        //public ICommand LoadReceivedTransactionCommand
        //{
        //    get
        //    {
        //        loadReceivedTransactionCommand = loadReceivedTransactionCommand ?? new RelayCommand(param => onLoadReceivedTransactionClicked()); return loadReceivedTransactionCommand;
        //    }
        //}

        //private ICommand loadBonusMigrationCommand;
        //public ICommand LoadBonusMigrationCommand
        //{
        //    get
        //    {
        //        loadBonusMigrationCommand = loadBonusMigrationCommand ?? new RelayCommand(param => onLoadBonusMigrationClicked()); return loadBonusMigrationCommand;
        //    }
        //}

        //private ICommand loadExchangeRateCommand;
        //public ICommand LoadExchangeRateCommand
        //{
        //    get
        //    {
        //        loadExchangeRateCommand = loadExchangeRateCommand ?? new RelayCommand(param => onLoadExchangeRateClicked()); return loadExchangeRateCommand;
        //    }
        //}

        //private ICommand buyCoinBundleCommand;
        //public ICommand BuyCoinBundleCommand
        //{
        //    get { buyCoinBundleCommand = buyCoinBundleCommand ?? new RelayCommand(param => onBuyCoinBundleClicked(param)); return buyCoinBundleCommand; }
        //}

        //private ICommand showCoinRechargeMethodCommand;
        //public ICommand ShowCoinRechargeMethodCommand
        //{
        //    get { showCoinRechargeMethodCommand = showCoinRechargeMethodCommand ?? new RelayCommand(param => onShowCoinRechargeMethodClicked()); return showCoinRechargeMethodCommand; }           
        //}

        //private ICommand buyCoinCommand;
        //public ICommand BuyCoinCommand
        //{
        //    get { buyCoinCommand = buyCoinCommand ?? new RelayCommand((param) => onBuyCoinClicked(param)); return buyCoinCommand; }
        //}

        //private ICommand loadCoinBundleListCommand;
        //public ICommand LoadCoinBundleListCommand
        //{
        //    get { loadCoinBundleListCommand = loadCoinBundleListCommand ?? new RelayCommand((param) => onLoadCoinBundleClicked()); return loadCoinBundleListCommand; }
        //}

        //private ICommand loadEarningRuleCommand;
        //public ICommand LoadEarningRuleCommand
        //{
        //    get { loadEarningRuleCommand = loadEarningRuleCommand ?? new RelayCommand((param) => onLoadEarningRuleClicked()); return loadEarningRuleCommand; }
        //}

        //private ICommand skipReferrerCommand;
        //public ICommand SkipReferrerCommand
        //{
        //    get { skipReferrerCommand = skipReferrerCommand ?? new RelayCommand((param) => onSkipReferrerClicked()); return skipReferrerCommand; }
        //}

        //private ICommand loadReferrerSummaryCommand;
        //public ICommand LoadReferrerSummaryCommand
        //{
        //    get { loadReferrerSummaryCommand = loadReferrerSummaryCommand ?? new RelayCommand((param) => onLoadReferrerSummaryClicked()); return loadReferrerSummaryCommand; }
        //}

        //private ICommand showReferralListCommand;
        //public ICommand ShowReferralListCommand
        //{
        //    get { showReferralListCommand = showReferralListCommand ?? new RelayCommand((param) => onShowReferralListClick()); return showReferralListCommand; }
        //}

        //private ICommand showSetReferrerCommand;
        //public ICommand ShowSetReferrerCommand
        //{
        //    get { showSetReferrerCommand = showSetReferrerCommand ?? new RelayCommand((param) => onShowSetReferrerClicked()); return showSetReferrerCommand; }
        //}

        //private ICommand setReferrerCommand;
        //public ICommand SetReferrerCommand
        //{
        //    get { setReferrerCommand = setReferrerCommand ?? new RelayCommand((param) => onSetReferrerClicked()); return setReferrerCommand; }
        //}

        //private ICommand loadReferralListCommand;
        //public ICommand LoadReferralListCommand
        //{
        //    get { loadReferralListCommand = loadReferralListCommand ?? new RelayCommand((param) => onLoadReferralCommandClicked()); return loadReferralListCommand; }
        //}

        //private ICommand dailyCheckInCommand;
        //public ICommand DailyCheckInCommand
        //{
        //    get { dailyCheckInCommand = dailyCheckInCommand ?? new RelayCommand((param) => onDailyCheckInClicked()); return dailyCheckInCommand; }            
        //}

        //private ICommand loadCheckInCommand;
        //public ICommand LoadCheckInCommand
        //{
        //    get { loadCheckInCommand = loadCheckInCommand ?? new RelayCommand((param) => onLoadCheckInClicked()); return loadCheckInCommand; }            
        //}

        //private ICommand inviteFriendsCommand;
        //public ICommand InviteFriendsCommand
        //{
        //    get { inviteFriendsCommand = inviteFriendsCommand ?? new RelayCommand((param) => onInviteFriendClicked()); return inviteFriendsCommand; }
        //    //set { inviteFriendsCommand = value; }
        //}

        //private ICommand moreShareCommand;
        //public ICommand MoreShareCommand
        //{
        //    get { moreShareCommand = moreShareCommand ?? new RelayCommand((param) => onMoreShareClicked()); return moreShareCommand; }            
        //}

        //private ICommand switchToMediaCloudSharingCommand;
        //public ICommand SwitchToMediaCloudSharingCommand
        //{
        //    get { switchToMediaCloudSharingCommand = switchToMediaCloudSharingCommand ?? new RelayCommand(param => onSwitchToMediaCloudSharingClicked()); return switchToMediaCloudSharingCommand; }        
        //}
        #endregion
        #region <Obsolete Properties>
        //private string inputPin;
        //public string InputPin
        //{
        //    get { return inputPin; }
        //    set { inputPin = value; this.OnPropertyChanged("InputPin"); }
        //}

        //private string confirmPin;
        //public string ConfirmPin
        //{
        //    get { return confirmPin; }
        //    set { confirmPin = value; this.OnPropertyChanged("ConfirmPin"); }
        //}

        //private string password = string.Empty;
        //public string Password
        //{
        //    get { return password; }
        //    set { password = value; this.OnPropertyChanged("Password"); }
        //}

        //private string walletPinErrorMessage = string.Empty;
        //public string WalletPinErrorMessage
        //{
        //    get { return walletPinErrorMessage; }
        //    set { walletPinErrorMessage = value; this.OnPropertyChanged("WalletPinErrorMessage"); }
        //}

        //private bool isPinNumberSet = false;
        //public bool IsPinNumberSet
        //{
        //    get
        //    {
        //        return isPinNumberSet;
        //    }
        //    set { isPinNumberSet = value; this.OnPropertyChanged("IsPinNumberSet"); }
        //}

        //private bool isResetPinNumber = false;
        //public bool IsResetPinNumber
        //{
        //    get { return isResetPinNumber; }
        //    set { isResetPinNumber = value; this.OnPropertyChanged("IsResetPinNumber"); }
        //}

        //private bool isLoggedIn = false;
        //public bool IsLoggedIn
        //{
        //    get { return isLoggedIn; }
        //    set { isLoggedIn = value; this.OnPropertyChanged("IsLoggedIn"); }
        //}

        //private bool isMobileNumberVerified = false;
        //public bool IsMobileNumberVerified
        //{
        //    get { return isMobileNumberVerified; }
        //    set { isMobileNumberVerified = value; this.OnPropertyChanged("IsMobileNumberVerified"); }
        //}

        //private bool isDigitFromWallet = false;
        //public bool IsDigitFromWallet
        //{
        //    get { return isDigitFromWallet; }
        //    set { isDigitFromWallet = value; this.OnPropertyChanged("IsDigitFromWallet"); }
        //}

        //private string sendReferralErrorMessage = string.Empty;
        //public string SendReferralErrorMessage
        //{
        //    get { return sendReferralErrorMessage; }
        //    set { sendReferralErrorMessage = value; this.OnPropertyChanged("SendReferralErrorMessage"); }
        //}

        //private bool isExchangeRateRequested = false;
        //public bool IsExchangeRateRequested
        //{
        //    get { return isExchangeRateRequested; }
        //    set { isExchangeRateRequested = value; this.OnPropertyChanged("IsExchangeRateRequested"); }
        //}

        //private bool isExchangeRateLoading = false;
        //public bool IsExchangeRateLoading
        //{
        //    get { return isExchangeRateLoading; }
        //    set { isExchangeRateLoading = value; this.OnPropertyChanged("IsExchangeRateLoading"); }
        //}

        //private bool exchangeRateError = false;
        //public bool ExchangeRateError
        //{
        //    get { return exchangeRateError; }
        //    set { exchangeRateError = value; this.OnPropertyChanged("ExchangeRateError"); }
        //}

        //private WalletExchangeRateModel selectedExchangeRateModel = null;
        //public WalletExchangeRateModel SelectedExchangeRateModel
        //{
        //    get { return selectedExchangeRateModel; }
        //    set { selectedExchangeRateModel = value; this.OnPropertyChanged("SelectedExchangeRateModel"); }
        //}

        //private string coinTransactionAmount = string.Empty;
        //public string CoinTransactionAmount
        //{
        //    get { return coinTransactionAmount; }
        //    set { coinTransactionAmount = value; this.OnPropertyChanged("CoinTransactionAmount"); }
        //}

        //private string transactionRemarks = string.Empty;
        //public string TransactionRemarks
        //{
        //    get { return transactionRemarks; }
        //    set { transactionRemarks = value; this.OnPropertyChanged("TransactionRemarks"); }
        //}

        //private string coinTransferErrorMessage = string.Empty;
        //public string CoinTransferErrorMessage
        //{
        //    get { return coinTransferErrorMessage; }
        //    set { coinTransferErrorMessage = value; this.OnPropertyChanged("CoinTransferErrorMessage"); }
        //}

        //private string coinTransferFromBonusToWalletErrorMessage = string.Empty;
        //public string CoinTransferFromBonusToWalletErrorMessage
        //{
        //    get { return coinTransferFromBonusToWalletErrorMessage; }
        //    set { coinTransferFromBonusToWalletErrorMessage = value; this.OnPropertyChanged("CoinTransferFromBonusToWalletErrorMessage"); }
        //}

        //private UserBasicInfoModel selectedReferral = null;
        //public UserBasicInfoModel SelectedReferral
        //{
        //    get { return selectedReferral; }
        //    set { selectedReferral = value; this.OnPropertyChanged("SelectedReferral"); }
        //}

        //private bool isReferralNetworkSummaryRequested = false;
        //public bool IsReferralNetworkSummaryRequested
        //{
        //    get { return isReferralNetworkSummaryRequested; }
        //    set { isReferralNetworkSummaryRequested = value; this.OnPropertyChanged("IsReferralNetworkSummaryRequested"); }
        //}

        //private bool isReferralPanelLoading = false;
        //public bool IsReferralPanelLoading
        //{
        //    get { return isReferralPanelLoading; }
        //    set { isReferralPanelLoading = value; this.OnPropertyChanged("IsReferralPanelLoading"); }
        //}

        //private bool noMoreIncomingRequest = false;
        //public bool NoMoreIncomingRequest
        //{
        //    get { return noMoreIncomingRequest; }
        //    set { noMoreIncomingRequest = value; this.OnPropertyChanged("NoMoreIncomingRequest"); }
        //}

        //private bool referralNetworkError = false;
        //public bool ReferralNetworkError
        //{
        //    get { return referralNetworkError; }
        //    set { referralNetworkError = value; this.OnPropertyChanged("ReferralNetworkError"); }
        //}

        //private bool isPendingLoading = false;
        //public bool IsPendingLoading
        //{
        //    get { return isPendingLoading; }
        //    set { isPendingLoading = value; this.OnPropertyChanged("IsPendingLoading"); }
        //}

        //private bool noMorePendingReferral = false;
        //public bool NoMorePendingReferral
        //{
        //    get { return noMorePendingReferral; }
        //    set { noMorePendingReferral = value; this.OnPropertyChanged("NoMorePendingReferral"); }
        //}

        //private int referralNetworkSummaryGoldMemberCount = 0;
        //public int ReferralNetworkSummaryGoldMemberCount
        //{
        //    get { return referralNetworkSummaryGoldMemberCount; }
        //    set { referralNetworkSummaryGoldMemberCount = value; this.OnPropertyChanged("ReferralNetworkSummaryGoldMemberCount"); }
        //}

        //private int referralNetworkSummarySilverMemberCount = 0;
        //public int ReferralNetworkSummarySilverMemberCount
        //{
        //    get { return referralNetworkSummarySilverMemberCount; }
        //    set { referralNetworkSummarySilverMemberCount = value; this.OnPropertyChanged("ReferralNetworkSummarySilverMemberCount"); }
        //}

        //private bool isSentLoading = false;
        //public bool IsSentLoading
        //{
        //    get { return isSentLoading; }
        //    set { isSentLoading = value; this.OnPropertyChanged("IsSentLoading"); }
        //}

        //private bool noMoreSent = false;
        //public bool NoMoreSent
        //{
        //    get { return noMoreSent; }
        //    set { noMoreSent = value; this.OnPropertyChanged("NoMoreSent"); }
        //}

        //private bool sentNetworkError = false;
        //public bool SentNetworkError
        //{
        //    get { return sentNetworkError; }
        //    set { sentNetworkError = value; this.OnPropertyChanged("SentNetworkError"); }
        //}

        //private bool isReceivedLoading = false;
        //public bool IsReceivedLoading
        //{
        //    get { return isReceivedLoading; }
        //    set { isReceivedLoading = value; this.OnPropertyChanged("IsReceivedLoading"); }
        //}

        //private bool noMoreReceived = false;
        //public bool NoMoreReceived
        //{
        //    get { return noMoreReceived; }
        //    set { noMoreReceived = value; this.OnPropertyChanged("NoMoreReceived"); }
        //}

        //private bool receivedNetworkError = false;
        //public bool ReceivedNetworkError
        //{
        //    get { return receivedNetworkError; }
        //    set { receivedNetworkError = value; this.OnPropertyChanged("ReceivedNetworkError"); }
        //}

        //private bool isBonusLoading = false;
        //public bool IsBonusLoading
        //{
        //    get { return isBonusLoading; }
        //    set { isBonusLoading = value; this.OnPropertyChanged("IsBonusLoading"); }
        //}

        //private bool noMoreBonus = false;
        //public bool NoMoreBonus
        //{
        //    get { return noMoreBonus; }
        //    set { noMoreBonus = value; this.OnPropertyChanged("NoMoreBonus"); }
        //}

        //private bool bonusNetworkError = false;
        //public bool BonusNetworkError
        //{
        //    get { return bonusNetworkError; }
        //    set { bonusNetworkError = value; this.OnPropertyChanged("BonusNetworkError"); }
        //}

        //private bool isMemberListLoading = false;
        //public bool IsMemberListLoading
        //{
        //    get { return isMemberListLoading; }
        //    set { isMemberListLoading = value; this.OnPropertyChanged("IsMemberListLoading"); }
        //}

        //private bool memberLoadingError = false;
        //public bool MemberLoadingError
        //{
        //    get { return memberLoadingError; }
        //    set { memberLoadingError = value; this.OnPropertyChanged("MemberLoadingError"); }
        //}

        //private bool noMoreGoldMembers = false;
        //public bool NoMoreGoldMembers
        //{
        //    get { return noMoreGoldMembers; }
        //    set { noMoreGoldMembers = value; this.OnPropertyChanged("NoMoreGoldMembers"); }
        //}

        //private bool noMoreSilverMembers = false;
        //public bool NoMoreSilverMembers
        //{
        //    get { return noMoreSilverMembers; }
        //    set { noMoreSilverMembers = value; this.OnPropertyChanged("NoMoreSilverMembers"); }
        //}

        //private bool endOfCoinBundleList = false;
        //public bool EndOfCoinBundleList
        //{
        //    get { return endOfCoinBundleList; }
        //    set { endOfCoinBundleList = value; this.OnPropertyChanged("EndOfCoinBundleList"); }
        //}
        //private bool emailVerificationTrack = false;
        //public bool EmailVerificationTrack
        //{
        //    get { return emailVerificationTrack; }
        //    set { emailVerificationTrack = value; this.OnPropertyChanged("EmailVerificationTrack"); }
        //}

        //private long referralAcceptOrRejectUtID = 0;
        //public long ReferralAcceptOrRejectUtID
        //{
        //    get { return referralAcceptOrRejectUtID; }
        //    set { referralAcceptOrRejectUtID = value; this.OnPropertyChanged("ReferralAcceptOrRejectUtID"); }
        //}

        //private int acceptedReferralListType = 0;
        //public int AcceptedReferralListType
        //{
        //    get { return acceptedReferralListType; }
        //    set { acceptedReferralListType = value; this.OnPropertyChanged("AcceptedReferralListType"); }
        //}

        //private ObservableCollection<WalletExchangeRateModel> exchangeRateList = new ObservableCollection<WalletExchangeRateModel>();
        //public ObservableCollection<WalletExchangeRateModel> ExchangeRateList
        //{
        //    get { return exchangeRateList; }
        //    set { exchangeRateList = value; this.OnPropertyChanged("ExchangeRateList"); }
        //}

        //private ObservableCollection<WalletCoinModel> bonusCoinList = new ObservableCollection<WalletCoinModel>();
        //public ObservableCollection<WalletCoinModel> BonusCoinList
        //{
        //    get { return bonusCoinList; }
        //    set { bonusCoinList = value; this.OnPropertyChanged("BonusCoinList"); }
        //}

        //private ObservableCollection<WalletCoinModel> myCoinList = new ObservableCollection<WalletCoinModel>();
        //public ObservableCollection<WalletCoinModel> MyCoinList
        //{
        //    get { return myCoinList; }
        //    set { myCoinList = value; }
        //}

        //private ObservableCollection<WalletCoinTransactionModel> coinTransactionHistory = new ObservableCollection<WalletCoinTransactionModel>();
        //public ObservableCollection<WalletCoinTransactionModel> CoinTransactionHistory
        //{
        //    get { return coinTransactionHistory; }
        //    set { coinTransactionHistory = value; this.OnPropertyChanged("CoinTransactionHistory"); }
        //}

        //private ObservableCollection<WalletReferralModel> pendingRequestsList = new ObservableCollection<WalletReferralModel>();
        //public ObservableCollection<WalletReferralModel> PendingRequestsList
        //{
        //    get { return pendingRequestsList; }
        //    set { pendingRequestsList = value; this.OnPropertyChanged("PendingRequestsList"); }
        //}

        //private ObservableCollection<WalletReferralModel> incomingRequestList = new ObservableCollection<WalletReferralModel>();
        //public ObservableCollection<WalletReferralModel> IncomingRequestList
        //{
        //    get { return incomingRequestList; }
        //    set { incomingRequestList = value; this.OnPropertyChanged("IncomingRequestList"); }
        //}

        //private ObservableCollection<WalletReferralModel> goldMembersList = new ObservableCollection<WalletReferralModel>();
        //public ObservableCollection<WalletReferralModel> GoldMembersList
        //{
        //    get { return goldMembersList; }
        //    set { goldMembersList = value; this.OnPropertyChanged("GoldMembersList"); }
        //}

        //private ObservableCollection<WalletReferralModel> silverMembersList = new ObservableCollection<WalletReferralModel>();
        //public ObservableCollection<WalletReferralModel> SilverMembersList
        //{
        //    get { return silverMembersList; }
        //    set { silverMembersList = value; this.OnPropertyChanged("SilverMembersList"); }
        //}

        //private ObservableCollection<WalletCoinTransactionModel> sentTransactionHistory = new ObservableCollection<WalletCoinTransactionModel>();
        //public ObservableCollection<WalletCoinTransactionModel> SentTransactionHistory
        //{
        //    get { return sentTransactionHistory; }
        //    set { sentTransactionHistory = value; this.OnPropertyChanged("SentTransactionHistory"); }
        //}

        //private ObservableCollection<WalletCoinTransactionModel> receivedTransactionHistory = new ObservableCollection<WalletCoinTransactionModel>();
        //public ObservableCollection<WalletCoinTransactionModel> ReceivedTransactionHistory
        //{
        //    get { return receivedTransactionHistory; }
        //    set { receivedTransactionHistory = value; this.OnPropertyChanged("ReceivedTransactionHistory"); }
        //}

        //private ObservableCollection<WalletCoinTransactionModel> bonusMigrationTransactionHistory = new ObservableCollection<WalletCoinTransactionModel>();
        //public ObservableCollection<WalletCoinTransactionModel> BonusMigrationTransactionHistory
        //{
        //    get { return bonusMigrationTransactionHistory; }
        //    set { bonusMigrationTransactionHistory = value; this.OnPropertyChanged("BonusMigrationTransactionHistory"); }
        //}

        //private bool shareCancelled = false;
        //public bool ShareCancelled
        //{
        //    get { return shareCancelled; }
        //    set { shareCancelled = value; this.OnPropertyChanged("ShareCancelled"); }
        //}
        #endregion
    }
}

﻿using Auth.utility;
using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace View.Utility.Wallet
{
    public class ThreadWalletPaymentInBrowser
    {
        private bool running = false;
        private Guid transactionID = Guid.Empty;
        private double cashAmount = 0.0;
        private string transactionUrl = string.Empty;
        public void StartThread(Guid transactionID , double cashAmount)
        {
            if (!running)
            {
                this.transactionID = transactionID;
                this.cashAmount = cashAmount;
                System.Threading.Thread th = new System.Threading.Thread(Run);
                th.Start();
            }
        }

        private void Run()
        {
            try
            {
                running = true;
                //string reqURL = "https://dev.ringid.com/payment/?" + HelperMethodsAuth.GetUrlEncoded( "cus_name="+ "sakib arman" +"&cus_email=pungi@yopmail.com&cus_phone=01913584994&tran_id=2bc88bfc-4576-43c0-9df8-57e2bc9c0bae&total_amount=2000&utId=51251");
                //string reqURL = "https://dev.ringid.com/payment/?" + HelperMethodsAuth.GetUrlEncoded(CreateTransactionUrl());
                //string reqURL = ServerAndPortSettings.WALLET_PAYMENT_GATEWAY_URL + SetUriAuxilary() + HelperMethodsAuth.GetUrlEncoded(CreateTransactionUrl());
                string reqURL = ServerAndPortSettings.WALLET_PAYMENT_GATEWAY_URL + SetUriAuxilary() + CreateTransactionUrl();
                HelperMethodsAuth.GoToSite(reqURL);
                
                //JObject pakToSend = new JObject();
                //string pakID = SendToServer.GetRanDomPacketID();
               // pakID[JsonKeys.PacketId] = pakID;

            }
            catch (Exception)
            {

                //throw;
            }
        }

        private string SetUriAuxilary()
        {
            StringBuilder auxBuilder = new StringBuilder();
            if (ServerAndPortSettings.WALLET_PAYMENT_GATEWAY_URL.EndsWith("/"))
            {
                auxBuilder.Append("?");
            }
            else if (ServerAndPortSettings.WALLET_PAYMENT_GATEWAY_URL.EndsWith("/?"))
            {
                auxBuilder.Append(string.Empty);
            }
            else
            {
                auxBuilder.Append("/?");
            }
            return auxBuilder.ToString();
        }

        private string CreateTransactionUrl()
        {
            StringBuilder urlBuilder = new StringBuilder();
           
            //urlBuilder.Append("cus_name=" + DefaultSettings.userProfile.FullName);
            //urlBuilder.Append("&");
            //urlBuilder.Append("cus_email=" + DefaultSettings.userProfile.Email);
            //urlBuilder.Append("&");
            //urlBuilder.Append("cus_phone=" + DefaultSettings.userProfile.MobileDialingCode + DefaultSettings.userProfile.MobileNumber);
            //urlBuilder.Append("&");
            //urlBuilder.Append("tran_id=" + this.transactionID.ToString());
            //urlBuilder.Append("&");
            //urlBuilder.Append("total_amount=" + string.Format("{0:0.00}", this.cashAmount));
            //urlBuilder.Append("&");
            //urlBuilder.Append("utId=" + DefaultSettings.userProfile.UserTableID);
            urlBuilder.Append("tranId=" + this.transactionID.ToString());
            urlBuilder.Append("&email=" + DefaultSettings.userProfile.Email);            
            urlBuilder.Append("&pf=" + AppConstants.PLATFORM_DESKTOP);
            return urlBuilder.ToString();
        }

    }
}

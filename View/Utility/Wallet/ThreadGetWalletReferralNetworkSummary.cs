﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace View.Utility.Wallet
{
    public class ThreadGetWalletReferralNetworkSummary
    {
        private bool running = false;

        public void StartThread()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(Run);
                th.Start();
            }
        }

        private void Run()
        {
            try
            {
                //running = true;
                //JObject pakToSend = new JObject();
                //string pakID = SendToServer.GetRanDomPacketID();
                //pakToSend[JsonKeys.PacketId] = pakID;
                //pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                //pakToSend[JsonKeys.Action] = AppConstants.TYPE_REFERRAL_NETWORK_SUMMARY;//1041;

                //JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
                //if (feedbackFields != null)
                //{
                //    if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success]) // success
                //    {
                //        if (feedbackFields[JsonKeys.ReferralSummary] != null)
                //        {
                //            JObject jObjReferralSummary = (JObject)feedbackFields[JsonKeys.ReferralSummary];
                //            if (jObjReferralSummary[JsonKeys.ReferralGoldMemberCount] != null)
                //            {
                //                WalletViewModel.Instance.ReferralNetworkSummaryGoldMemberCount = (int)jObjReferralSummary[JsonKeys.ReferralGoldMemberCount];
                //            }
                //            if (jObjReferralSummary[JsonKeys.ReferralSilverMemberCount] !=null)
                //            {
                //                WalletViewModel.Instance.ReferralNetworkSummarySilverMemberCount = (int)jObjReferralSummary[JsonKeys.ReferralSilverMemberCount];
                //            }
                //        }
                //        if (feedbackFields[JsonKeys.MyReferralStat] != null)
                //        {
                //            JObject jObjIncoming = (JObject)feedbackFields[JsonKeys.MyReferralStat];
                //            BindingModels.WalletReferralModel incomingModel = new BindingModels.WalletReferralModel();
                //            if (jObjIncoming[JsonKeys.ReferralID] != null)
                //            {
                //                incomingModel.UserTableID = (long)jObjIncoming[JsonKeys.ReferralID];
                //            }
                //            if (jObjIncoming[JsonKeys.UserIdentity] != null)
                //            {
                //                incomingModel.UserIdentity = (long)jObjIncoming[JsonKeys.UserIdentity];
                //            }
                //            if (jObjIncoming[JsonKeys.FullName] != null)
                //            {
                //                incomingModel.FullName = (string)jObjIncoming[JsonKeys.FullName];
                //            }
                //            if (jObjIncoming[JsonKeys.ProfileImage] != null)
                //            {
                //                incomingModel.ProfileImage = (string)jObjIncoming[JsonKeys.ProfileImage];
                //            }
                //            if (jObjIncoming[JsonKeys.ReferralAcceptedStatus] != null)
                //            {
                //                incomingModel.Accepted = (bool)jObjIncoming[JsonKeys.ReferralAcceptedStatus];
                //            }
                //            if (jObjIncoming[JsonKeys.ReferralAddedTime] != null)
                //            {
                //                incomingModel.UpdateTime = (long)jObjIncoming[JsonKeys.ReferralAddedTime];
                //            }
                //            if (WalletViewModel.Instance.IncomingRequestList.Count > 0)
                //            {
                //                WalletViewModel.Instance.IncomingRequestList.Clear();
                //            }
                //            if (!incomingModel.Accepted)
                //            {
                //                WalletViewModel.Instance.IncomingRequestList.InvokeAdd(incomingModel);
                //            }
                //            else
                //            {
                //                WalletViewModel.Instance.NoMoreIncomingRequest = true;
                //            }                            
                //        }
                //        else
                //        {
                //            WalletViewModel.Instance.NoMoreIncomingRequest = true;
                //        }
                //        WalletViewModel.Instance.IsReferralNetworkSummaryRequested = true;
                //    }
                //    else // success false
                //    {
                //        WalletViewModel.Instance.ReferralNetworkSummaryGoldMemberCount = 0;
                //        WalletViewModel.Instance.ReferralNetworkSummarySilverMemberCount = 0;
                //        WalletViewModel.Instance.NoMoreIncomingRequest = true;
                //    }
                //    WalletViewModel.Instance.IsReferralPanelLoading = false;
                //    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                //}
                //else
                //{
                //    WalletViewModel.Instance.IsReferralPanelLoading = false;
                //    WalletViewModel.Instance.ReferralNetworkError = true;
                //}
            }
            catch (Exception)
            {
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning { get { return running; } }
    }
}

﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using View.ViewModel;

namespace View.Utility.Wallet
{
    public class ThreadWalletDailyUtilities
    {
        private bool running = false;

        public void StartGetGetServerTime()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(() => RunServerTime());
                th.Start();
            }
        }

        public void StartDailyCheckIn()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(() => runDailyCheckIn());
                th.Start();
            }
        } 
       
        public void StartDailyTaskDwellingRule()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(() => RunDailyTaskDwellingRule());
                th.Start();
            }
        }

        public void StartSendDailyTaskDwellTime()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(() => RunSendDailyTaskDwellTime());
                th.Start();
            }
        }

        public void StartDailyCheckInHistory()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(() => runDailyCheckInHistory());
                th.Start();
            }
        }

        public void StartDailyCheckInRules()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(() => runDailyCheckInRules());
                th.Start();
            }
        }

        public void StartMediaShareOnFacebook()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(() => runShareMediaOnFacebook());
                th.Start();
            }
        }

        private void runShareMediaOnFacebook()
        {
            string message = string.Empty;
            try
            {   
                running = true;
                string pakID = SendToServer.GetRanDomPacketID();
                JObject pakToSend = new JObject();
                pakToSend[JsonKeys.PacketId] = pakID;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_SHARE_TO_SOCIAL_MEDIA;
                int idx = RingIDViewModel.Instance.ContentIDHash.IndexOf('=') + 1;
                pakToSend[WalletConstants.REQ_KEY_ENCRYPTED_CONTENT_ID] = RingIDViewModel.Instance.ContentIDHash.Substring(idx); //"05371E0C3738371E0C0511614831371E906204286A33527B3D341A7C3A6003333D345228213D032E35280F2F3A37072C3534037C6E34";
                pakToSend[JsonKeys.MediaType] = RingIDViewModel.Instance.ShareContentType;//2;
                pakToSend[WalletConstants.REQ_KEY_SOCIAL_MEDIA_TYPE] = RingIDViewModel.Instance.SocialMediaType;//1;

                JObject feedbackFields = Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, pakID);
                if (feedbackFields != null)
                {
                    if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success])
                    {
                        message = "Media shared successfully!";
                    }
                    else
                    {
                        message = "Something went wrong!";
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                }
                else
                {
                    message = "Error occured! Please try later!";
                }
            }
            catch (Exception ex)
            {
                message = string.Format("Exception occured ==> {1} {0} {2}" , Environment.NewLine, ex.Message, ex.StackTrace);
                //throw;
            }
            finally
            {
                System.Diagnostics.Debug.WriteLine("ACTION 1051 ==> " + message);
                //System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                //{
                //    RingIDViewModel.Instance.ResetMediaShareInfo();

                //    UI.Wallet.WnMediaShareViaWallet.Instance.CloseWindow();
                //    MiddlePanelSwitcher.Switch(Constants.MiddlePanelConstants.TypeWallet);
                //    UIHelperMethods.ShowMessageWithTimerFromNonThread(UI.UCGuiRingID.Instance.MotherPanel, message);
                //});
            }
        }

        public const string keyDailyCheckInRules = "dailyCheckInRules";        

        private void runDailyCheckInRules()
        {
            try
            {
                running = true;
                string pakID = SendToServer.GetRanDomPacketID();
                JObject pakToSend = new JObject();
                pakToSend[JsonKeys.PacketId] = pakID;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_DAILY_CHECKIN_RULES;

                JObject feedbackFields = Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
                if (feedbackFields != null)
                {
                    if (feedbackFields[JsonKeys.Success]!=null && (bool)feedbackFields[JsonKeys.Success] && feedbackFields[keyDailyCheckInRules] != null)
                    {
                        JArray checkInRules = (JArray)feedbackFields[keyDailyCheckInRules];
                        foreach (JObject ruleDTO in checkInRules)
                        {
                            BindingModels.WalletCheckInRuleModel model = BindingModels.WalletCheckInRuleModel.LoadDataFromJson(ruleDTO);
                            if (model.DailyCheckInDayTypeID != WalletConstants.CHECK_IN_TYPE_DEFAULT && !WalletViewModel.Instance.WalletCheckInRules.Any(x=>x.DailyCheckInDayTypeID == model.DailyCheckInDayTypeID))
                            {
                                WalletViewModel.Instance.WalletCheckInRules.Add(model);
                            }
                        }
                        WalletViewModel.Instance.ProcessCheckInRuleCoinDetails();
                    }
                    else
                    {
                        //succ false
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                }
                else
                {
                    //error net
                }
            }
            catch (Exception)
            {

                //throw;
            }
        }

        private void runDailyCheckInHistory()
        {
            try
            {
                running = true;
                string pakID = SendToServer.GetRanDomPacketID();
                JObject pakToSend = new JObject();
                pakToSend[JsonKeys.PacketId] = pakID;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_DAILY_CHECKIN_HISTORY;//1054;

                JObject feedbackFields = Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
                if (feedbackFields != null)
                {
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                }
                else
                {
                    //error net
                }
            }
            catch (Exception)
            {

                //throw;
            }
        }

        private void RunSendDailyTaskDwellTime()
        {
            DateTime currentTime = DateTime.UtcNow;
            try
            {
                running = true;
                string pakID = SendToServer.GetRanDomPacketID();
                JObject pakToSend = new JObject();
                pakToSend[JsonKeys.PacketId] = pakID;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_DAILY_TASK_DWELL_TIME;
                if (WalletViewModel.Instance.LastDwellingTime != new DateTime(1970, 1, 1)) 
                {
                    pakToSend[WalletConstants.REQ_KEY_DWELL_DATE] = WalletViewModel.Instance.LastDwellingTime.ToString("yyyy-MM-dd");
                    pakToSend[WalletConstants.REQ_KEY_DWELL_TIME_IN_MINUTES] = WalletViewModel.Instance.DwellTimeInMinutes;
                }
                JObject feedbackFields = Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, pakID);
                if (feedbackFields != null)
                {
                    if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success] && feedbackFields[WalletConstants.RSP_KEY_SERVER_TIME] != null)
                    {
                        currentTime = Models.Utility.ModelUtility.DateTimeFromMillisSince1970((long)feedbackFields[WalletConstants.RSP_KEY_SERVER_TIME]);                        
                    }                    
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                }                
            }
            catch (Exception)
            {
            }
            finally
            {
                WalletViewModel.Instance.ProcessDwellTimeAfterResponse(currentTime);
            }
        }

        private void RunDailyTaskDwellingRule()
        {
            try
            {
                running = true;
                string pakID = SendToServer.GetRanDomPacketID();
                JObject pakToSend = new JObject();
                pakToSend[JsonKeys.PacketId] = pakID;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = 1053;

                JObject feedbackFilds = Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
                if (feedbackFilds!=null)
                {
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFilds);
                }
                else
                {

                }
            }
            catch (Exception)
            {

            }
        }

        private void RunServerTime()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                string pakID = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = pakID;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = 1048;
                                
                JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
                if (feedbackFields != null)
                {
                    if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success])
                    {
                        long time = (long)feedbackFields[JsonKeys.Time];
                        System.Diagnostics.Debug.WriteLine("SERVER TIME==> " + Models.Utility.ModelUtility.DateTimeFromMillisSince1970(time));
                    }
                    else
                    {
                        WalletViewModel.Instance.AppStartDateTime = DateTime.UtcNow;
                    }
                }
                else // internet error
                {
                    WalletViewModel.Instance.AppStartDateTime = DateTime.UtcNow;
                }
                
            }
            catch (Exception)
            {

                //throw;
            }
        }

        private void runDailyCheckIn()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                string pakID = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = pakID;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_DAILY_CHECK_IN;
                //System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                //{
                    //WalletViewModel.Instance.CheckInLoading = false;
                //View.Utility.WPFMessageBox.MessegeBoxWithLoader msgBox = View.Utility.WPFMessageBox.CustomMessageBox.ShowMessageWithLoader("Please wait!");
                //});
                JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, pakID);
                //System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                //{
                //    msgBox.Close();                
                //});
                
                if (feedbackFields != null)
                {
                    if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success])
                    {
                        System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                        {
                            WalletViewModel.Instance.MyCoinStat.Quantity += feedbackFields[WalletConstants.RSP_KEY_CHECKED_IN_AWARDED_QUANTITY] != null ? (int)feedbackFields[WalletConstants.RSP_KEY_CHECKED_IN_AWARDED_QUANTITY] : 0;
                            WalletViewModel.Instance.IsCheckedIn = true;
                            //MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
                            //MainSwitcher.PopupController.WalletPopupWrapper.ShowCheckInSuccessPanel();
                            MainSwitcher.PopupController.WalletPopupWrapper.ShowPopup(WalletConstants.PU_CHECK_IN_SUCCESS);
                        });                        
                    }
                    else
                    {
                        if (feedbackFields[WalletConstants.RSP_KEY_ALREADY_CHECKED_IN] != null && (bool)feedbackFields[WalletConstants.RSP_KEY_ALREADY_CHECKED_IN])
                        {
                            System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                            {
                            //    MainSwitcher.PopupController.WalletPopupWrapper.ShowCheckInFailedPanel();
                                MainSwitcher.PopupController.WalletPopupWrapper.ShowPopup(WalletConstants.PU_CHECK_IN_FAIL);
                                WalletViewModel.Instance.IsCheckedIn = true;
                            });
                        }
                        else
                        {
                            UIHelperMethods.ShowMessageWithTimerFromNonThread(UI.UCGuiRingID.Instance.MotherPanel, "An error occured. Please try later!");
                        }
                        //if (WalletViewModel.Instance.CheckInDays.Count == 0)
                        //{
                        //    runDailyCheckInHistory();
                        //}
                    }
                }
                else // internet error
                {   
                    //WalletViewModel.Instance.CheckInReloadRequire = true;

                    UIHelperMethods.ShowMessageWithTimerFromNonThread(UI.UCGuiRingID.Instance.MotherPanel, "An error occured. Please try later!");
                }
            }
            catch (Exception)
            {

                //throw;
            }
        }
    }
}

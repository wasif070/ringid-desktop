﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace View.Utility.Wallet
{
    public class ThreadCoinEarningRuleList
    {
        bool running = false;

        public void StartGetCoinEarningRuleList()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(Run);
                th.Start();
            }
        }

        private void Run()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                string pakID = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = pakID;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_GET_COIN_EARNING_RULE;
                        
                JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_REQUEST, pakID);
                WalletViewModel.Instance.RuleListLoading = false;
                if (feedbackFields != null)
                {
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                }
                else
                {
                    WalletViewModel.Instance.RuleListReloadRequire = true;
                    //System.Diagnostics.Debug.WriteLine("Internet Error! ==> 1045");
                }
            }
            catch (Exception)
            {
                System.Diagnostics.Debug.WriteLine("Exception Error! ==> 1045");
                //throw;
            }
        }

        public void StopThread()
        {
            running = false;
        }        

        public bool IsRunning { get { return running; } }
    }
}

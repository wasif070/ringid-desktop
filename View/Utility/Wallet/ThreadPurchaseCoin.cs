﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using View.ViewModel;
using View.UI;

namespace View.Utility.Wallet
{
    public class ThreadPurchaseCoin
    {
        //{
        //    "rmk": "test",
        //    "curnIso": "BDT",
        //    "cashAmnt": 200.0,
        //    "exchRt": 20.0,
        //    "nocn": 10,
        //    "cnReBnId": 1,
        //    "cnId": 1,

        //    "actn": 1043,
        //    "pckId": "2110010568vddHy8W0",
        //    "sId": "164915876451492110010568"
        //}
        private bool running = false;
        private int coinBundleID = 0;
        //public const string keyRemark = "rmk";
        //public const string keyCurrencyISO = "curnIso";
        //public const string keyCashAmount = "cashAmnt";
        //public const string keyExchangeRate = "exchRt";
        //public const string keyCoinQuantity = "nocn";
        //public const string keyCoinBundleID = "cnReBnId";
        //public const string keyCoinID = "cnId";
        //public const string keyTransactionID = "tranId";
        public void StartPurchaseCoin(int coinBundleID = 0)
        {
            if (!running)
            {
                this.coinBundleID = coinBundleID;
                System.Threading.Thread th = new System.Threading.Thread(Run);
                th.Start();
            }
        }
        private void Run()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                string pakID = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = pakID;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_PURCHASE_COIN;

                BindingModels.WalletCoinBundleModel model = WalletViewModel.Instance.CoinBundles.Where(x => x.CoinBundleID == coinBundleID).FirstOrDefault();
                if (model != null)
                {
                    pakToSend[WalletConstants.REQ_KEY_REMARK] = "test";
                    pakToSend[WalletConstants.REQ_KEY_CURRENCY_ISO] = model.CurrencyISOCode;
                    pakToSend[WalletConstants.RR_KEY_CASH_AMOUNT] = model.Price;
                    pakToSend[WalletConstants.REQ_KEY_EXCHANGE_RATE] = model.ExchangeRate;//WalletViewModel.Instance.ExchangeRateList.Where(x => x.CurrencyISOCode == model.CurrencyISOCode).First().ExchangeRate;
                    pakToSend[WalletConstants.REQ_KEY_COIN_QUANTITY] = model.CoinQuantity;
                    pakToSend[WalletConstants.REQ_KEY_COIN_BUNDLE_ID] = model.CoinBundleID;
                    pakToSend[WalletConstants.REQ_KEY_COIN_ID] = model.CoinID;

                    JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, pakID);
                    if (feedbackFields != null)
                    {
                        if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success])
                        {
                            if (DefaultSettings.userProfile.IsEmailVerified == 1)
                            {
                                Guid transactionID = Guid.Parse(feedbackFields[WalletConstants.RSP_KEY_TRANSACTION_ID].ToString());
                                double cashAmount = (double)feedbackFields[WalletConstants.RR_KEY_CASH_AMOUNT];
                                new ThreadWalletPaymentInBrowser().StartThread(transactionID, cashAmount);
                            }
                            else
                            {
                                System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                                {
                                    MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
                                    //System.Windows.MessageBoxResult result = View.Utility.WPFMessageBox.CustomMessageBox.ShowInformation("To ensure secured transaction in ringID, please verify your Email from Settings.", "Verification Needed", true, customButtonText: new string[]{"Settings", "Cancel"});
                                    WNConfirmationView cv = new WNConfirmationView("Verification confirmation!", "To ensure secured transaction in ringID, please verify your email from Settings.", CustomConfirmationDialogButtonOptions.YesNo, new string[] { "Settings", "Cancel" });
                                    var result = cv.ShowCustomDialog();
                                    if (result == ConfirmationDialogResult.Yes)
                                    {
                                        //if (result == System.Windows.MessageBoxResult.OK)
                                        //{
                                        //WalletViewModel.Instance.EmailVerificationTrack = true;
                                        DefaultSettings.EMAIL_VERIFICATION_FROM_WALLET = true;
                                        View.ViewModel.RingIDViewModel.Instance.GetWindowRingIDSettings.Show();
                                        View.ViewModel.RingIDViewModel.Instance.GetWindowRingIDSettings.Focus();
                                    }
                                });

                            }

                        }
                        else//error
                        {
                            showError();
                        }
                        RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                    }
                    else
                    {
                        //internet error
                        showError();

                    }
                }
                else // bundle missing
                {
                    showError();
                }
            }
            catch (Exception)
            {

                //throw;
            }
        }

        private void showError()
        {
            System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
            {
                MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
                UIHelperMethods.ShowMessageWithTimerFromNonThread(View.UI.UCGuiRingID.Instance.MotherPanel, "Sorry, error occured. Please try again later!");
            });

        }
    }
}

﻿using Auth.utility;
using Models.Constants;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using View.UI;

namespace View.Utility.Wallet
{
    public class ThreadReferralOperation
    {
        private bool running = false;

        public void StartSetReferrer()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(runAddReferrer);
                th.Start();
            }
        }

        public void StartSkipReferrer()
        {
            if (!running)
            {
                System.Threading.Thread th = new System.Threading.Thread(runSkipReferrer);
                th.Start();
            }
        }

        private void runAddReferrer()
        {
            try
            {
                running = true;
                string pakID = SendToServer.GetRanDomPacketID();
                JObject pakToSend = new JObject();
                pakToSend[JsonKeys.PacketId] = pakID;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_REFERRER;
                pakToSend[JsonKeys.UserIdentity] = Wallet.WalletViewModel.Instance.SelectedReferrer.basicInfoDTO.RingID;
                JObject feedbackFields = Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, pakID);
                WalletViewModel.Instance.ReferrerProcessing = false;
                if (feedbackFields != null)
                {
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                    {
                        MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
                    });
                    
                    if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success])
                    {
                        BindingModels.UserBasicInfoModel model = new BindingModels.UserBasicInfoModel();
                        model.ShortInfoModel.UserTableID = (long)feedbackFields[JsonKeys.UserTableID];
                        model.ShortInfoModel.UserIdentity = (long)feedbackFields[JsonKeys.UserIdentity];
                        model.ShortInfoModel.FullName = (string)feedbackFields[JsonKeys.FullName];
                        model.Gender = (string)feedbackFields[JsonKeys.Gender];
                        model.ShortInfoModel.FriendShipStatus = (int)feedbackFields[JsonKeys.FriendshipStatus];
                        model.ShortInfoModel.ProfileImage = (string)feedbackFields[JsonKeys.ProfileImage];
                        model.ShortInfoModel.ContactType = (int)feedbackFields[JsonKeys.ContactType];
                        model.CallAccess = (int)feedbackFields[JsonKeys.CallAccess];
                        model.ChatAccess = (int)feedbackFields[JsonKeys.ChatAccess];
                        model.FeedAccess = (int)feedbackFields[JsonKeys.FeedAccess];
                        UIHelperMethods.ShowMessageWithTimerFromNonThread(UI.UCGuiRingID.Instance.MotherPanel, "Referrer added successfully!");
                        WalletViewModel.Instance.IsReferrerSet = true;
                        WalletViewModel.Instance.IsReferrerSkipped = false;
                        WalletViewModel.Instance.Referrer = model;
                    }
                    else // success false
                    {
                        UIHelperMethods.ShowMessageWithTimerFromNonThread(UI.UCGuiRingID.Instance.MotherPanel, "User already added a referrer!");
                    }
                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                }
                else // net error
                {
                    WalletViewModel.Instance.SetReferrerErrorMessage = "An Error occured! Please try later!";
                }

            }
            catch (Exception)
            {

                //throw;
            }
        }                

        private void runSkipReferrer()
        {
            try
            {
                running = true;
                JObject pakToSend = new JObject();
                string pakID = SendToServer.GetRanDomPacketID();
                pakToSend[JsonKeys.PacketId] = pakID;
                pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
                pakToSend[JsonKeys.Action] = AppConstants.TYPE_SKIP_REFERRER;

                JObject feedbackFields = Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, pakID);
                System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                {
                    MainSwitcher.PopupController.WalletPopupWrapper.HidePopup();
                });
                if (feedbackFields != null)
                {
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
                    {
                        WalletViewModel.Instance.IsReferrerSkipped = true;
                        UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, "You have skipped referrer!");
                    });

                    RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakID, out feedbackFields);
                }
                else
                {
                    //internet error
                    UIHelperMethods.ShowMessageWithTimerFromNonThread(UCGuiRingID.Instance.MotherPanel, "An error occured! Please try later!");
                }
            }
            catch (Exception)
            {

                //throw;
            }
        }

        public void StopThread()
        {
            running = false;
        }

        public bool IsRunning { get { return running; } }

        //public void StartSendReferral()
        //{
        //    if (!running)
        //    {
        //        System.Threading.Thread th = new System.Threading.Thread(RunSendReferral);
        //        th.Start();
        //    }
        //}

        //public void StartAcceptReferralRequest()
        //{
        //    if (!running)
        //    {
        //        System.Threading.Thread th = new System.Threading.Thread(RunReferralAccept);
        //        th.Start();
        //    }
        //}

        //public void StartRejectReferralRequest()
        //{
        //    if (!running)
        //    {
        //        System.Threading.Thread th = new System.Threading.Thread(RunReferralReject);
        //        th.Start();
        //    }
        //}

        //JObject MakeReferralRequestPacket(out string pakId, bool accept = true)
        //{
        //    JObject jPacket = new JObject();
        //    pakId = SendToServer.GetRanDomPacketID();
        //    jPacket[JsonKeys.PacketId] = pakId;
        //    jPacket[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
        //    jPacket[JsonKeys.Action] = AppConstants.TYPE_ACCEPT_OR_DECLINE_REFERRAL_REQUEST;//1038;
        //    jPacket[JsonKeys.UserTableID] = WalletViewModel.Instance.ReferralAcceptOrRejectUtID;
        //    jPacket[JsonKeys.Accept] = accept;
        //    return jPacket;
        //}

        

        //private void RunSendReferral()
        //{
        //    try
        //    {
        //        running = true;

        //        JObject pakToSend = new JObject();
        //        string pakId = SendToServer.GetRanDomPacketID();
        //        pakToSend[JsonKeys.PacketId] = pakId;
        //        pakToSend[JsonKeys.SessionId] = DefaultSettings.LOGIN_SESSIONID;
        //        pakToSend[JsonKeys.Action] = AppConstants.TYPE_ADD_REFERRER;//1036;
        //        pakToSend[JsonKeys.UserTableID] = WalletViewModel.Instance.SelectedReferral.ShortInfoModel.UserTableID;

        //        JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, pakId);
        //        System.Windows.Application.Current.Dispatcher.BeginInvoke(delegate
        //        {
        //            //WalletViewModel.Instance.ToggleSendReferralPanel();
        //        });
        //        if (feedbackFields != null)
        //        {
        //            if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success]) //success
        //            {
        //                View.BindingModels.WalletReferralModel model = BindingModels.WalletReferralModel.LoadData(feedbackFields);
        //                if (model != null)
        //                {
        //                    UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.WALLET_REFERRAL_SENDING_SUCCESS);
        //                    System.Windows.Application.Current.Dispatcher.Invoke(delegate
        //                    {
        //                        if (UI.UCMiddlePanelSwitcher.View_UCWalletMainPanel != null && UI.UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCReferralPanel != null && UI.UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCReferralPanel._ReferralControlPanel.Child is UI.Wallet.UCSendReferral)
        //                        {
        //                            UI.UCMiddlePanelSwitcher.View_UCWalletMainPanel._UCReferralPanel.LoadSendReferralRequest();
        //                        }

        //                    });
        //                    if (!WalletViewModel.Instance.PendingRequestsList.Any(x => x.UserTableID == model.UserTableID))
        //                    {
        //                        WalletViewModel.Instance.PendingRequestsList.InvokeAdd(model);
        //                    }
        //                }
        //                else
        //                {
        //                    WalletViewModel.Instance.SendReferralErrorMessage = NotificationMessages.WALLET_REFERRAL_SENDING_ERROR;
        //                    //return;
        //                }
        //            }
        //            else //fail
        //            {
        //                WalletViewModel.Instance.SendReferralErrorMessage = NotificationMessages.WALLET_REFERRAL_SENDING_ERROR;
        //                //return;
        //            }
        //            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackFields);
        //        }
        //        else
        //        {
        //            WalletViewModel.Instance.SendReferralErrorMessage = NotificationMessages.WALLET_REFERRAL_SENDING_FAIL;
        //        }                
        //    }
        //    catch (Exception) { }
        //}

        //private void RunReferralAccept()
        //{
        //    try
        //    {
        //        string pakId;
        //        JObject pakToSend = MakeReferralRequestPacket(out pakId);

        //        JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, pakId);
        //        System.Windows.Application.Current.Dispatcher.Invoke(delegate
        //        {
        //            //WalletViewModel.Instance.ToggleReferralIncomingPanel();
        //        });
        //        if (feedbackFields != null)
        //        {
        //            if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success]) //success
        //            {

        //                BindingModels.WalletReferralModel mdl = null;
        //                if (WalletViewModel.Instance.IncomingRequestList.Count > 0)
        //                {
        //                    mdl = WalletViewModel.Instance.IncomingRequestList[0];
        //                }
        //                System.Windows.Application.Current.Dispatcher.Invoke(delegate
        //                {
        //                    WalletViewModel.Instance.IncomingRequestList.Clear();
        //                });
        //                WalletViewModel.Instance.ReferralAcceptOrRejectUtID = 0;
        //                WalletViewModel.Instance.NoMoreIncomingRequest = true;
        //                UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, string.Format(NotificationMessages.WALLET_REFERRAL_ACCEPT_SUCCESS, mdl.FullName, mdl.UserIdentity.ToString().Substring(2).Insert(4, " ")));
        //            }
        //            else //fail
        //            {
        //                UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.WALLET_REFERRAL_PROCESSING_FAIL);
        //            }
        //            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackFields);
        //        }
        //        else
        //        {
        //            UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.WALLET_REFERRAL_PROCESSING_FAIL);
        //        }
        //    }
        //    catch (Exception) { }
        //}

        //private void RunReferralReject()
        //{
        //    try
        //    {
        //        string pakId;
        //        JObject pakToSend = MakeReferralRequestPacket(out pakId, false);

        //        JObject feedbackFields = View.Utility.Auth.SendAuthReqeust.Send(pakToSend, AppConstants.REQUEST_TYPE_UPDATE, pakId);

        //        System.Windows.Application.Current.Dispatcher.Invoke(delegate
        //        {
        //           // WalletViewModel.Instance.ToggleReferralIncomingPanel();
        //        });

        //        if (feedbackFields != null)
        //        {
        //            if (feedbackFields[JsonKeys.Success] != null && (bool)feedbackFields[JsonKeys.Success]) //success
        //            {
        //                BindingModels.WalletReferralModel mdl = null;
        //                if (WalletViewModel.Instance.IncomingRequestList.Count > 0)
        //                {
        //                    mdl = WalletViewModel.Instance.IncomingRequestList[0];
        //                }
        //                System.Windows.Application.Current.Dispatcher.Invoke(delegate
        //                {
        //                    WalletViewModel.Instance.IncomingRequestList.Clear();
        //                });
        //                WalletViewModel.Instance.ReferralAcceptOrRejectUtID = 0;
        //                WalletViewModel.Instance.NoMoreIncomingRequest = true;
        //                UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, string.Format(NotificationMessages.WALLET_REFERRAL_REJECT_SUCCESS, mdl.FullName, mdl.UserIdentity.ToString().Substring(2).Insert(4, " ")));
        //            }
        //            else //fail
        //            {
        //                UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.WALLET_REFERRAL_PROCESSING_FAIL);
        //            }
        //            RingDictionaries.Instance.PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.TryRemove(pakId, out feedbackFields);
        //        }
        //        else
        //        {
        //            UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, NotificationMessages.WALLET_REFERRAL_PROCESSING_FAIL);
        //        }
        //    }
        //    catch (Exception) { }
        //}

        
    }
}

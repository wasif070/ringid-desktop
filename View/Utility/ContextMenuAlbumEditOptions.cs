﻿using MediaInfoDotNet;
using Microsoft.Win32;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using View.BindingModels;
using View.UI;
using View.Utility.Feed;
using View.UI.Profile.MyProfile;
using View.Utility.WPFMessageBox;
using View.ViewModel;
using View.UI.PopUp;

namespace View.Utility
{
    public class ContextMenuAlbumEditOptions
    {
        Grid gridPanel;
        public MediaContentModel contentModel = null;

        #region "Property"
        private static ContextMenuAlbumEditOptions _Instance;
        public static ContextMenuAlbumEditOptions Instance
        {
            get
            {
                _Instance = _Instance ?? new ContextMenuAlbumEditOptions();
                return _Instance;
            }
        }

        private UCAlbumMediaEditPopup ucAlbumMediaEditPopup { get; set; }

        #endregion "Properties"

        public void ShowHandler(Grid gridPanel, MediaContentModel model)
        {
            this.gridPanel = gridPanel;
            this.contentModel = model;

        }

        #region "ContextMenu"

        public ContextMenu cntxMenu = new ContextMenu();
        MenuItem mnuItem1 = new MenuItem();
        MenuItem mnuItem2 = new MenuItem();
        MenuItem mnuItem3 = new MenuItem();

        public void SetupMenuItem()
        {
            if (cntxMenu.Items != null)
                cntxMenu.Items.Clear();
            cntxMenu.Style = (Style)Application.Current.Resources["CustomCntxtMenu"];
            mnuItem1.Header = "Rename";
            mnuItem2.Header = "Change Image";
            mnuItem3.Header = "Delete";


            mnuItem1.Style = (Style)Application.Current.Resources["ContextMenuItem"];
            mnuItem2.Style = (Style)Application.Current.Resources["ContextMenuItem"];
            mnuItem3.Style = (Style)Application.Current.Resources["ContextMenuItem"];

            cntxMenu.Items.Add(mnuItem1);
            cntxMenu.Items.Add(mnuItem2);
            cntxMenu.Items.Add(mnuItem3);

            mnuItem1.Click -= renameAlbum_Click;
            mnuItem2.Click -= changeAlbumImage_Click;
            mnuItem3.Click -= deleteAlbum_Click;
            //cntxMenu.Closed -= ContextMenu_Closed;

            //cntxMenu.Closed += ContextMenu_Closed;
            mnuItem1.Click += renameAlbum_Click;
            mnuItem2.Click += changeAlbumImage_Click;
            mnuItem3.Click += deleteAlbum_Click;
        }

        private void ContextMenu_Closed(object sender, RoutedEventArgs e)
        {
            mnuItem1.Click -= renameAlbum_Click;
            mnuItem2.Click -= changeAlbumImage_Click;
            mnuItem3.Click -= deleteAlbum_Click;
            cntxMenu.Closed -= ContextMenu_Closed;

            cntxMenu.Style = null;
            mnuItem1.Style = null;
            mnuItem2.Style = null;
            mnuItem3.Style = null;
            //cntxMenu.Items.Remove(mnuItem1);
            //cntxMenu.Items.Remove(mnuItem2);
            cntxMenu.Items.Clear();

            gridPanel.ContextMenu = null;
        }

        private void renameAlbum_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            cntxMenu.IsOpen = false;
            if (ucAlbumMediaEditPopup != null)
                UCGuiRingID.Instance.MotherPanel.Children.Remove(ucAlbumMediaEditPopup);
            ucAlbumMediaEditPopup = new UCAlbumMediaEditPopup(UCGuiRingID.Instance.MotherPanel);
            ucAlbumMediaEditPopup.Show();
            ucAlbumMediaEditPopup.ShowView(this.contentModel);
            ucAlbumMediaEditPopup.OnRemovedUserControl += () =>
            {
                ucAlbumMediaEditPopup.ClosePopUp();
                ucAlbumMediaEditPopup = null;
                //if (ImageViewInMain != null)
                //{
                //    ImageViewInMain.GrabKeyboardFocus();
                //}
            };
        }

        private void changeAlbumImage_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            cntxMenu.IsOpen = false;

            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            op.Multiselect = false;
            bool anyCorrupted = false;
            bool anyMorethan500MB = false;
            if ((bool)op.ShowDialog())
            {
                try
                {
                    MediaFile mf = new MediaFile(op.FileName);
                    if (new FileInfo(op.FileName).Length > DefaultSettings.MaxFileLimit250MBinBytes) { anyMorethan500MB = true; }
                    int formatChecked = ImageUtility.GetFileImageTypeFromHeader(op.FileName);
                    if (formatChecked != 1 && formatChecked != 2) { anyCorrupted = true; }
                    if (!anyCorrupted && !anyMorethan500MB)
                        albumImageUpload.ShowHandlerDialog(op.FileName, contentModel);
                    else
                    {
                        UIHelperMethods.ShowFailed("Invalid File Format! Please choose a different file!", "File format");
                        // CustomMessageBox.ShowError("Invalid File Format! Please choose a different file!", "Failed!");
                    }
                }
                catch (Exception) { anyCorrupted = true; }
            }
        }

        public UCAlbumImageUpload albumImageUpload
        {
            get
            {
                return MainSwitcher.PopupController.AlbumImageUpload;
            }
        }

        private void deleteAlbum_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            cntxMenu.IsOpen = false;
            bool isTrue = UIHelperMethods.ShowQuestion(String.Format(NotificationMessages.DELETE_CONFIRMATIONS, "this Album"), String.Format(NotificationMessages.TITLE_CONFIRMATIONS, "Delete"));
            if (isTrue)
            {
                ThreadDeleteMediaOrAlbum thrd = new ThreadDeleteMediaOrAlbum(Guid.Empty, contentModel.AlbumId, contentModel.MediaType);
                thrd.callBackEvent += (success) =>
                {
                    if (success == SettingsConstants.RESPONSE_SUCCESS)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            if (contentModel.MediaType == SettingsConstants.MEDIA_TYPE_AUDIO
                                && RingIDViewModel.Instance.MyAudioAlbums.Any(P => P.AlbumId == contentModel.AlbumId))
                            {
                                RingIDViewModel.Instance.MyAudioAlbums.Remove(contentModel);
                                if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCAlbumDownloadPanel != null
                                    && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCAlbumDownloadPanel.IsVisible
                                    && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCAlbumDownloadPanel.MyAudioAlbumsList.Any(P => P.AlbumId == contentModel.AlbumId))
                                    UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCAlbumDownloadPanel.MyAudioAlbumsList.Remove(contentModel);
                            }
                            else if (contentModel.MediaType == SettingsConstants.MEDIA_TYPE_VIDEO
                                && RingIDViewModel.Instance.MyVideoAlbums.Any(P => P.AlbumId == contentModel.AlbumId))
                            {
                                RingIDViewModel.Instance.MyVideoAlbums.Remove(contentModel);
                                if (UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel != null && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCAlbumDownloadPanel != null
                                    && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCAlbumDownloadPanel.IsVisible
                                    && UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCAlbumDownloadPanel.MyVideoAlbumsList.Any(P => P.AlbumId == contentModel.AlbumId))
                                    UCMiddlePanelSwitcher.View_UCMediaCloudMainPanel._UCAlbumDownloadPanel.MyVideoAlbumsList.Remove(contentModel);
                            }
                        });
                        UIHelperMethods.ShowMessageWithTimerFromThread(UCGuiRingID.Instance.MotherPanel, "Successfully Deleted this Album!");
                    }
                    else
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            UIHelperMethods.ShowWarning(NotificationMessages.FAILED_TRY_WITH_NETWORK_CHECK);
                            //   CustomMessageBox.ShowError("Please check your Internet connection or try later!", "Failed!");
                        });
                    }
                };
                thrd.StartThread();
            }
        }
        #endregion "ContextMenu"
    }
}

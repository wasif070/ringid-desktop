<<<<<<< HEAD
﻿
namespace View.Constants
{
    public class ImageLocation
    {
        private static string BASE = "/Resources/Images/";
        private static string BASE_MENU = "/Resources/Images/menu/";
        private static string BASE_LOADERS = "/Resources/Images/loaders/";
        private static string BASE_BUTTONS = "/Resources/Images/Buttons/";
        private static string BASE_STATUS = "/Resources/Images/Status/";
        private static string BASE_SETTINGS = "/Resources/Images/Settings/";
        private static string BASE_CALL = "/Resources/Images/Call/";
        private static string BASE_CHAT = "/Resources/Images/Chat/";
        private static string BASE_CONTEXT = "/Resources/Images/Context_Menu/";
        public static string BASE_COUNTRY = "/Resources/Images/Countries/";
        public static string BASE_STREAM = "/Resources/Images/Stream/";
        public static string BASE_CHANNEL = "/Resources/Images/Channel/";
        public static string BASE_CONFIRMATIONS = "/Resources/Images/Confirmations/";
        private static string BASE_WINDOW = "/Resources/Images/window/";
        public static string APP_ICON { get { return BASE + "ring_icon.ico"; } }//icon.ico/ringID.ico

        public static string OFFLINE_TASKBAR_ICON { get { return BASE + "offlineTaskOverlay.png"; } }//offnet.png
        public static string DEFAULT_MY_PROFILE_BG_IMAGE { get { return BASE + "default_myprofile_bg_image.jpg"; } }
        public static string MY_PROFILE_BG_IMAGE_DEFAULT { get { return BASE + "my_profile_bg_image_default.png"; } }
        public static string MY_PROFILE_BG_IMAGE_PRESSED { get { return BASE + "my_profile_bg_image_pressed.png"; } }
        public static string DEFAULT_MY_PROFILE_BG_IMAGE_H { get { return BASE + "default_myprofile_bg_image_h.jpg"; } }
        public static string LOGIN_BG { get { return BASE + "sign_in_img.jpg"; } }
        public static string WELCOME_BG_IMAGE { get { return BASE + "welcome.jpg"; } }
        public static string WELCOME_LOGO_IMAGE { get { return BASE + "Logo.png"; } }

        public static string LOGIN_DEFAULT_IMAGE_1 { get { return BASE + "default_login_image_1.jpg"; } }
        public static string LOGIN_DEFAULT_IMAGE_2 { get { return BASE + "default_login_image_2.jpg"; } }
        public static string LOGIN_DEFAULT_IMAGE_3 { get { return BASE + "default_login_image_3.jpg"; } }

        public static string PRIVACY_NEW_STATUS_ONLY_ME { get { return BASE_BUTTONS + "privacy_only_me.png"; } }
        public static string PRIVACY_NEW_STATUS_FRIENDS { get { return BASE_BUTTONS + "privacy_friends.png"; } }
        public static string PRIVACY_NEW_STATUS_PUBLIC { get { return BASE_BUTTONS + "privacy_public.png"; } }
        public static string CUSTOM_PRIVACY { get { return BASE_BUTTONS + "custom_privacy.png"; } }
        public static string CUSTOM_PRIVACY_H { get { return BASE_BUTTONS + "custom_privacy_h.png"; } }

        public static string SIGNIN_SIGNUP_WITH_EMAIL_IMAGE { get { return BASE + "with-email.png"; } }
        public static string SIGNIN_SIGNUP_WITH_FACEBOOK_IMAGE { get { return BASE + "with_fb.png"; } }
        public static string SIGNIN_SIGNUP_WITH_ID_IMAGE { get { return BASE + "with_id.png"; } }
        public static string SIGNIN_SIGNUP_WITH_PHONE_IMAGE { get { return BASE + "with_phone.png"; } }
        public static string SIGNIN_SIGNUP_WITH_TWITTER_IMAGE { get { return BASE + "with_twitter.png"; } }
        public static string SIGNIN_SIGNUP_BACK_BUTTON_IMAGE { get { return BASE_BUTTONS + "signUp_back.png"; } }
        public static string SIGNIN_SIGNUP_BACK_BUTTON_IMAGE_H { get { return BASE_BUTTONS + "signUp_back_h.png"; } }
        public static string SIGNIN_SIGNUP_BACK_BUTTON_IMAGE_P { get { return BASE_BUTTONS + "signUp_back_p.png"; } }
        public static string SIGNIN_SIGNUP_ARROW_IMAGE { get { return BASE + "signUp_arrow.png"; } }

        public static string NO_IMAGE_FOUND { get { return BASE + "no_image_found.jpg"; } }
        public static string DEFAULT_COVER_IMAGE { get { return BASE + "default_cover_image.jpg"; } }
        public static string DEFAULT_CHAT_BG { get { return BASE + "default_chat_bg.png"; } }
        public static string UNKNOWN_GROUP_IMAGE { get { return BASE + "unknown_group_imge.png"; } }
        public static string UNKNOWN_GROUP_LARGE_IMAGE { get { return BASE + "unknown_group_large_imge.png"; } }
        public static string UNKNOWN_ROOM_IMAGE { get { return BASE + "unknown_room_image.png"; } }
        public static string UNKNOWN_IMAGE_FOR_MUTUAL_FRIEND { get { return BASE + "unknown_user_mutual.png"; } }
        public static string GROUP_TOP { get { return BASE_MENU + "group.png"; } }
        public static string GROUP_TOP_H { get { return BASE_MENU + "group_h.png"; } }
        public static string GROUP_TOP_SELECTED { get { return BASE_MENU + "group_selected.png"; } }

        public static string RELOAD { get { return BASE_BUTTONS + "reload_icon.png"; } }
        public static string RELOAD_H { get { return BASE_BUTTONS + "reload_icon_h.png"; } }
        //public static string CIRCLE_TOP { get { return BASE_MENU + "circle.png"; } }
        //public static string CIRCLE_TOP_H { get { return BASE_MENU + "circle_h.png"; } }
        //public static string CIRCLE_TOP_SELECTED { get { return BASE_MENU + "circle_selected.png"; } }
        public static string UNKNOWN_70 { get { return BASE_MENU + "unknown_70.png"; } }
        public static string UNKNOWN_25 { get { return BASE_MENU + "unknown_25.png"; } }
        public static string POPUP_SIGN_IN { get { return BASE + "sign_in_popup.png"; } }
        public static string POPUP_MUTUAL_FRIENDS { get { return BASE + "like_list.png"; } }
        public static string POPUP_ABOUT_RINGID { get { return BASE + "about_ringid.png"; } }
        public static string POPUP_EDIT_DELETE { get { return BASE + "popup_edit_delete_photo.png"; } }
        public static string POPUP_THREE_ITEMS { get { return BASE + "popup_three_items.png"; } }
        public static string POPUP_DELETE { get { return BASE + "popup_delete_photo.png"; } }
        public static string POPUP_SHARE { get { return BASE + "share_popup.png"; } }
        public static string BOOK_ARROW { get { return BASE + "book_arrow.png"; } }

        public static string GROUP_DELETE { get { return BASE + "delete.png"; } }
        public static string GROUP_LEAVE { get { return BASE + "leave.png"; } }
        public static string GROUP_JOIN { get { return BASE + "join.png"; } }
        public static string GROUP_INFO { get { return BASE + "group_info.png"; } }
        public static string BLOCK_USER { get { return BASE + "block.png"; } }
        public static string CHANGE_ACCESS { get { return BASE + "change_access.png"; } }
        public static string INFO_ICON { get { return BASE + "info_icon.png"; } }
        public static string WORK_ICON { get { return BASE + "work_icon.png"; } }
        public static string EDUCATION_ICON { get { return BASE + "education_icon.png"; } }
        public static string SKILL_ICON { get { return BASE + "skill_icon.png"; } }
        public static string ADD_MORE { get { return BASE + "add_more_photo.png"; } }
        public static string ADD_MORE_H { get { return BASE + "add_more_photo_h.png"; } }
        public static string DEFAULT_ID_ICON { get { return BASE + "ringID.jpg"; } }
        public static string FRIEND_BLOCK_ICON { get { return BASE + "block_h.png"; } }
        public static string SEQUENCE_NUMBER { get { return BASE + "sequence_number.jpg"; } }
        public static string POPUP_UPLOAD_PHOTO { get { return BASE + "popup_upload_photo.png"; } }
        public static string LOCATION_USER { get { return BASE + "location.png"; } }
        public static string POPUP_UPLOAD_VIDEO { get { return BASE + "popup_upload_video.png"; } }
        public static string AUDIO_ALBUM_LIST_ICON { get { return BASE + "audio_album_list.png"; } }
        public static string VIDEO_ALBUM_LIST_ICON { get { return BASE + "video_album_list.png"; } }
        public static string DOWNLOAD_LIST_ICON { get { return BASE + "download_list.png"; } }

        public static string ADD_FRIEND_FACEBOOK { get { return BASE + "fb.png"; } }
        public static string ADD_FRIEND_FACEBOOK_H { get { return BASE + "fb_h.png"; } }
        public static string ADD_FRIEND_TWITTER { get { return BASE + "tt.png"; } }
        public static string ADD_FRIEND_TWITTER_H { get { return BASE + "tt_h.png"; } }
        public static string ADD_FRIEND_GOOGLE_PLUS { get { return BASE + "g+.png"; } }
        public static string ADD_FRIEND_GOOGLE_PLUS_H { get { return BASE + "g+_h.png"; } }
        public static string ADD_FRIEND_EMAIL { get { return BASE + "email.png"; } }
        public static string ADD_FRIEND_EMAIL_H { get { return BASE + "email_h.png"; } }
        public static string ADD_FRIEND_RING { get { return BASE + "invite_background.png"; } }
        //public static string FEED { get { return BASE_MENU + "feed.png"; } }
        //public static string FEED_H { get { return BASE_MENU + "feed_h.png"; } }
        //public static string FEED_SELECTED { get { return BASE_MENU + "feed_selected.png"; } }
        //public static string LIVE_AND_CHANNEL { get { return BASE_MENU + "home.png"; } }
        //public static string LIVE_AND_CHANNEL_H { get { return BASE_MENU + "home_h.png"; } }
        //public static string LIVE_AND_CHANNEL_SELECTED { get { return BASE_MENU + "home_selected.png"; } }
        public static string FEED { get { return BASE_MENU + "home.png"; } }
        public static string FEED_H { get { return BASE_MENU + "home_h.png"; } }
        public static string FEED_SELECTED { get { return BASE_MENU + "home_selected.png"; } }
        public static string LIVE_AND_CHANNEL { get { return BASE_MENU + "live.png"; } }
        public static string LIVE_AND_CHANNEL_H { get { return BASE_MENU + "live_h.png"; } }
        public static string LIVE_AND_CHANNEL_SELECTED { get { return BASE_MENU + "live_selected.png"; } }
        public static string MY_CHANNEL { get { return BASE_MENU + "my_channel.png"; } }
        public static string MY_CHANNEL_H { get { return BASE_MENU + "my_channel_h.png"; } }
        public static string MY_CHANNEL_SELECTED { get { return BASE_MENU + "my_channel_selected.png"; } }
        public static string NEWS_PORTAL { get { return BASE_MENU + "news.png"; } }
        public static string NEWS_PORTAL_H { get { return BASE_MENU + "news_h.png"; } }
        public static string NEWS_PORTAL_SELECTED { get { return BASE_MENU + "news_selected.png"; } }
        public static string PAGES { get { return BASE_MENU + "page.png"; } }
        public static string PAGES_H { get { return BASE_MENU + "page_h.png"; } }
        public static string PAGES_SELECTED { get { return BASE_MENU + "page_selected.png"; } }
        public static string FEED_DETAILS_BACK { get { return BASE + "back.png"; } }
        public static string FEED_DETAILS_BACK_P { get { return BASE + "back_p.png"; } }
        public static string SAVED_FEED { get { return BASE_MENU + "saved.png"; } }
        public static string SAVED_FEED_H { get { return BASE_MENU + "saved_h.png"; } }
        public static string SAVED_FEED_SELECTED { get { return BASE_MENU + "saved_selected.png"; } }
        public static string CELEBRITY { get { return BASE_MENU + "celebrity.png"; } }
        public static string CELEBRITY_H { get { return BASE_MENU + "celebrity_h.png"; } }
        public static string CELEBRITY_SELECTED { get { return BASE_MENU + "celebrity_selected.png"; } }
        public static string FOLLOWING_LIST { get { return BASE_MENU + "following.png"; } }
        public static string FOLLOWING_LIST_H { get { return BASE_MENU + "following_h.png"; } }
        public static string FOLLOWING_LIST_SELECTED { get { return BASE_MENU + "following_selected.png"; } }

        public static string DIALPAD { get { return BASE_MENU + "dialpad.png"; } }
        public static string DIALPAD_H { get { return BASE_MENU + "dialpad_h.png"; } }
        public static string DIALPAD_SELECTED { get { return BASE_MENU + "dialpad_selected.png"; } }
        public static string ROOM { get { return BASE_MENU + "room.png"; } }
        public static string ROOM_H { get { return BASE_MENU + "room_h.png"; } }
        public static string ROOM_SELECTED { get { return BASE_MENU + "room_selected.png"; } }

        public static string DIALPAD_BOTTOM { get { return BASE_MENU + "dialpad_bottom.png"; } }
        public static string DIALPAD_BOTTOM_H { get { return BASE_MENU + "dialpad_bottom_h.png"; } }
        public static string DIALPAD_BOTTOM_SELECTED { get { return BASE_MENU + "dialpad_bottom_selected.png"; } }

        /*****Dialpad Button******/
        public static string DIALPAD_SINGLE { get { return BASE_BUTTONS + "dial_single_button.png"; } }
        public static string DIALPAD_SINGLE_H { get { return BASE_BUTTONS + "dial_single_btton_h.png"; } }
        public static string DIALPAD_SINGLE_SELECTED { get { return BASE_BUTTONS + "dial_single_button_selected.png"; } }
        public static string CALL_UPPER { get { return BASE_BUTTONS + "call_upper.png"; } }
        public static string CALL_UPPER_H { get { return BASE_BUTTONS + "call_upper_h.png"; } }
        public static string CALL_UPPER_SELECTED { get { return BASE_BUTTONS + "call_upper_selected.png"; } }
        public static string CALL_BOTTOM { get { return BASE_BUTTONS + "call_bottom.png"; } }
        public static string CALL_BOTTOM_H { get { return BASE_BUTTONS + "call_bottom_h.png"; } }
        public static string CALL_BOTTOM_INACTIVE { get { return BASE_BUTTONS + "call_inactive.png"; } }
        public static string DIALPAD_CROSS_BUTTON { get { return BASE_BUTTONS + "Cross_normal.png"; } }
        public static string DIALPAD_CROSS_BUTTON_SELECTED { get { return BASE_BUTTONS + "Cross_Selected.png"; } }
        public static string DIALPAD_NEW_CROSS_BUTTON { get { return BASE_BUTTONS + "dial_cross.png"; } }
        public static string DIALPAD_NEW_CROSS_BUTTON_SELECTED { get { return BASE_BUTTONS + "dial_cross_selected.png"; } }
        public static string DIAL_PAD_SEARCH { get { return BASE_BUTTONS + "dialpad_search_icon.png"; } }
        public static string MUSIC_VIDEO_SEARCH { get { return BASE_BUTTONS + "dialpad_search.png"; } }
        public static string DIALPAD_DOWN_BUTTON { get { return BASE_BUTTONS + "dialpad_down.png"; } }
        public static string DIALPAD_DOWN_BUTTON_H { get { return BASE_BUTTONS + "dialpad_down_h.png"; } }
        public static string DIALPAD_USER_BUTTON { get { return BASE_BUTTONS + "dialpad_user.png"; } }
        public static string DIALPAD_USER_BUTTON_H { get { return BASE_BUTTONS + "dialpad_user_select.png"; } }

        /*****Dialpad Button******/


        public static string ALL_NOTIFICATION { get { return BASE_MENU + "all_notification_icon.png"; } }
        public static string ALL_NOTIFICATION_H { get { return BASE_MENU + "all_notification_icon_h.png"; } }
        public static string ALL_NOTIFICATION_SELECTED { get { return BASE_MENU + "all_notification_icon_selected.png"; } }
        public static string FRIEND_ICON { get { return BASE_MENU + "friends_icon.png"; } }
        public static string FRIEND_ICON_H { get { return BASE_MENU + "friends_icon_h.png"; } }
        public static string FRIEND_ICON_SELECTED { get { return BASE_MENU + "friends_icon_selected.png"; } }

        public static string MEDIA_CLOUD { get { return BASE_MENU + "media_cloud.png"; } }
        public static string MEDIA_CLOUD_H { get { return BASE_MENU + "media_cloud_h.png"; } }
        public static string MEDIA_CLOUD_SELECTED { get { return BASE_MENU + "media_cloud_selected.png"; } }
        public static string MEDIA_FEED { get { return BASE_MENU + "media_feed.png"; } }
        public static string MEDIA_FEED_H { get { return BASE_MENU + "media_feed_h.png"; } }
        public static string MEDIA_FEED_SELECTED { get { return BASE_MENU + "media_feed_selected.png"; } }

        public static string CIRCLE_ICON { get { return BASE_MENU + "circle.png"; } }
        public static string CIRCLE_ICON_H { get { return BASE_MENU + "circle_h.png"; } }
        public static string CIRCLE_ICON_SELECTED { get { return BASE_MENU + "circle_selected.png"; } }
        public static string STICKER_MARKET { get { return BASE_MENU + "sticker_market.png"; } }
        public static string STICKER_MARKET_H { get { return BASE_MENU + "sticker_market_h.png"; } }
        public static string STICKER_MARKET_SELECTED { get { return BASE_MENU + "sticker_market_selected.png"; } }
        public static string STICKER_DOWNLOAD_BUTTON { get { return BASE_BUTTONS + "sticker_download_btn.png"; } }
        public static string STICKER_DOWNLOAD_BUTTON_H { get { return BASE_BUTTONS + "sticker_download_btn_h.png"; } }
        public static string STICKER_DOWNLOADING_BUTTON { get { return BASE_BUTTONS + "sticker_downloading_h_btn.png"; } }
        public static string MORE_STICKER_IN_COMMENTS { get { return BASE_CHAT + "more_sticker.png"; } }
        public static string MORE_STICKER_IN_COMMENTS_H { get { return BASE_CHAT + "more_sticker_hover.png"; } }

        public static string MSG_ICON { get { return BASE_MENU + "msg_icon.png"; } }
        public static string MSG_ICON_H { get { return BASE_MENU + "msg_icon_h.png"; } }
        public static string MSG_ICON_SELECTED { get { return BASE_MENU + "msg_icon_selected.png"; } }
        public static string CALLHISTORY_ICON { get { return BASE_MENU + "callhistory_icon.png"; } }
        public static string CALLHISTORY_ICON_H { get { return BASE_MENU + "callhistory_icon_h.png"; } }
        public static string CALLHISTORY_ICON_SELECTED { get { return BASE_MENU + "callhistory_icon_selected.png"; } }
        /*public static string ADD_FRIEND_STICKER { get { return BASE + "charecter.png"; } }
        public static string ADD_FRIEND_STICKER_H { get { return BASE + "charecter_h.png"; } }
        public static string ADD_FRIEND_STICKER_SELECTED { get { return BASE + "charecter_h2.png"; } }*/
        public static string ADD_FRIEND_STICKER { get { return BASE_MENU + "add_friend_avatar.png"; } }
        public static string ADD_FRIEND_STICKER_H { get { return BASE_MENU + "add_friend_avatar_h.png"; } }
        public static string ADD_FRIEND_STICKER_SELECTED { get { return BASE_MENU + "add_friend_avatar_selected.png"; } }

        public static string ADD_FRIEND_STICKER_HOME_H { get { return BASE + "charecter_h3.png"; } }
        public static string ADD_FRIEND { get { return BASE_BUTTONS + "add_friend.png"; } }
        public static string ADD_FRIEND_H { get { return BASE_BUTTONS + "add_friend_h.png"; } }
        public static string TAKE_PROFILE_PHOTO { get { return BASE_BUTTONS + "take_profile_photo.png"; } }
        public static string TAKE_PROFILE_PHOTO_H { get { return BASE_BUTTONS + "take_profile_photo_h.png"; } }
        public static string TAKE_PROFILE_PHOTO_SELECTED { get { return BASE_BUTTONS + "take_profile_photo_selected.png"; } }
        public static string UPLOAD_PROFILE_PHOTO { get { return BASE_BUTTONS + "upload_from_computer.png"; } }
        public static string UPLOAD_PROFILE_PHOTO_H { get { return BASE_BUTTONS + "upload_from_computer_h.png"; } }
        public static string UPLOAD_PROFILE_PHOTO_SELECTED { get { return BASE_BUTTONS + "upload_from_computer_selected.png"; } }
        public static string EDIT_PHOTO { get { return BASE_BUTTONS + "edit_profile_photo.png"; } }
        public static string EDIT_PHOTO_H { get { return BASE_BUTTONS + "edit_profile_photo_h.png"; } }
        public static string EDIT_PHOTO_SELECTED { get { return BASE_BUTTONS + "edit_profile_photo_selected.png"; } }

        public static string ADD_FRIEND_SEARCH { get { return BASE + "add_friend_search.png"; } }
        public static string ADD_FRIEND_SEARCH_H { get { return BASE + "add_friend_search_h.png"; } }
        public static string ADD_FRIEND_SEARCH_SELECTED { get { return BASE + "add_friend_search_selected.png"; } }
        public static string ADD_FRIEND_SUGGESTION { get { return BASE + "discover.png"; } }
        public static string ADD_FRIEND_SUGGESTION_H { get { return BASE + "discover_h.png"; } }
        public static string ADD_FRIEND_SUGGESTION_SELECTED { get { return BASE + "discover_selected.png"; } }
        public static string ADD_FRIEND_INVITE { get { return BASE + "invite.png"; } }
        public static string ADD_FRIEND_INVITE_H { get { return BASE + "invite_h.png"; } }
        public static string ADD_FRIEND_INVITE_SELECTED { get { return BASE + "invite_select.png"; } }
        public static string ADD_FRIEND_PENDING { get { return BASE + "pending.png"; } }
        public static string ADD_FRIEND_PENDING_H { get { return BASE + "pending_h.png"; } }
        public static string ADD_FRIEND_PENDING_SELECTED { get { return BASE + "pending_select.png"; } }

        public static string _ADD_FRIEND_BLOCK_LIST { get { return BASE + "block_add_friend.png"; } }
        public static string _ADD_FRIEND_BLOCK_LIST_H { get { return BASE + "block_add_friend_h.png"; } }
        public static string _ADD_FRIEND_BLOCK_LIST_SELECTED { get { return BASE + "block_add_friend_selected.png"; } }

        public static string ADD_FRIEND_ICON_CONTACT_LIST { get { return BASE + "add_friend.png"; } }
        public static string FRIEND_ICON_CONTACT_LIST { get { return BASE + "friend.png"; } }
        public static string PENDING_IN_FRIEND_ICON_CONTACT_LIST { get { return BASE + "panding_in.png"; } }
        public static string PENDING_OUT_FRIEND_ICON_CONTACT_LIST { get { return BASE + "panding_out.png"; } }

        public static string ADD_FRIEND_ICON_CONTACT_LIST_MEDIUM { get { return BASE + "add_friend_icon_medium.png"; } }
        public static string FRIEND_ICON_CONTACT_LIST_MEDIUM { get { return BASE + "friend_icon_medium.png"; } }
        public static string INCOMING_FRIEND_ICON_CONTACT_LIST_MEDIUM { get { return BASE + "incoming_icon_medium.png"; } }
        public static string OUTGOING_FRIEND_ICON_CONTACT_LIST_MEDIUM { get { return BASE + "outgoing_icon_medium.png"; } }

        #region Context Menu
        public static string ADD_FRIEND_ICON_CM { get { return BASE_CONTEXT + "add_icon.png"; } }
        public static string ADD_FRIEND_ICON_CM_H { get { return BASE_CONTEXT + "add_icon_h.png"; } }
        public static string BLOCK_USER_ICON_CM { get { return BASE_CONTEXT + "block_friend.png"; } }
        public static string BLOCK_USER_ICON_CM_H { get { return BASE_CONTEXT + "block_friend_h.png"; } }
        public static string CANCEL_REQUEST_ICON_CM { get { return BASE_CONTEXT + "cancel_rqst.png"; } }
        public static string CANCEL_REQUEST_ICON_CM_H { get { return BASE_CONTEXT + "cancel_rqst_h.png"; } }
        public static string CIRCLE_SMALL_ICON_CM { get { return BASE_CONTEXT + "circle_small.png"; } }
        public static string CIRCLE_SMALL_ICON_CM_H { get { return BASE_CONTEXT + "circle_small_h.png"; } }
        public static string FRIEND_ICON_CM { get { return BASE_CONTEXT + "friend_icon.png"; } }
        public static string FRIEND_ICON_CM_H { get { return BASE_CONTEXT + "friend_icon_h.png"; } }
        public static string REJECT_ICON_CM { get { return BASE_CONTEXT + "reject.png"; } }
        public static string REJECT_ICON_CM_H { get { return BASE_CONTEXT + "reject_h.png"; } }

        public static string GROUP_DELETE_CM { get { return BASE_CONTEXT + "delete.png"; } }
        public static string GROUP_DELETE_CM_H { get { return BASE_CONTEXT + "delete_h.png"; } }
        public static string EDIT_INFO_CM { get { return BASE_CONTEXT + "edit_info.png"; } }
        public static string EDIT_INFO_CM_H { get { return BASE_CONTEXT + "edit_info_h.png"; } }
        public static string GROUP_LEAVE_CM { get { return BASE_CONTEXT + "leave.png"; } }
        public static string GROUP_LEAVE_CM_H { get { return BASE_CONTEXT + "leave_h.png"; } }
        #endregion


        public static string ADD_ICON_CONTACT_LIST { get { return BASE + "add_icon.png"; } }
        public static string ADD_ICON_CONTACT_LIST_H { get { return BASE + "add_icon_h.png"; } }
        public static string INCOMING_FRIEND_ICON_CONTACT_LIST { get { return BASE + "incoming_icon.png"; } }
        public static string INCOMING_FRIEND_ICON_CONTACT_LIST_H { get { return BASE + "incoming_icon_h.png"; } }
        public static string OUTGOING_FRIEND_ICON_CONTACT_LIST { get { return BASE + "outgoing_icon.png"; } }
        public static string OUTGOING_FRIEND_ICON_CONTACT_LIST_H { get { return BASE + "outgoing_icon_h.png"; } }
        public static string FRIEND_CONTACT_LIST { get { return BASE + "friend_icon.png"; } }
        public static string FRIEND_CONTACT_LIST_H { get { return BASE + "friend_icon_h.png"; } }
        public static string FRIEND_CONTACT_ID { get { return BASE + "friend_id.png"; } }
        public static string REJECT_ICON_CONTACT_LIST { get { return BASE + "reject.png"; } }
        public static string REJECT_ICON_CONTACT_LIST_H { get { return BASE + "reject_h.png"; } }
        public static string CANCEL_REQUEST_ICON_CONTACT_LIST { get { return BASE + "cancel_rqst.png"; } }
        public static string CANCEL_REQUEST_ICON_CONTACT_LIST_H { get { return BASE + "cancel_rqst_h.png"; } }
        public static string REMOVE_ICON_PUMAYKNOW_LIST { get { return BASE + "remove.png"; } }
        public static string REMOVE_ICON_PUMAYKNOW_LIST_H { get { return BASE + "remove_h.png"; } }
        public static string VIEW_PREVIOUS_COMMENT_ICON { get { return BASE + "up_arrow.png"; } }
        public static string ADD_FRIEND_ICON_CIRCLE { get { return BASE + "add_friend_icon_circle.png"; } }
        public static string FRIEND_ICON_CIRCLE { get { return BASE + "friend_icon_circle.png"; } }
        public static string INCOMING_FRIEND_ICON_CIRCLE { get { return BASE + "incoming_icon_circle.png"; } }
        public static string OUTGOING_FRIEND_ICON_CIRCLE { get { return BASE + "outgoing_icon_circle.png"; } }
        public static string BLOCK_FRIEND_ICON { get { return BASE + "block_friend.png"; } }
        public static string BLOCK_FRIEND_ICON_H { get { return BASE + "block_friend_h.png"; } }
        public static string CIRCLE_SMALL_ICON { get { return BASE + "circle_small.png"; } }
        public static string CIRCLE_SMALL_ICON_H { get { return BASE + "circle_small_h.png"; } }
        public static string CHAI_ICON_FOR_NAME_TOOLTIP { get { return BASE + "chat_tooltip_icon.png"; } }

        public static string BUTTON_ZOOM_OUT { get { return BASE_BUTTONS + "zoom_out.png"; } }
        public static string BUTTON_ZOOM_IN { get { return BASE_BUTTONS + "zoom_in.png"; } }
        public static string BUTTON_ADD_FRIEND_INCOMING { get { return BASE_BUTTONS + "incoming_friend.png"; } }
        public static string BUTTON_ADD_FRIEND_INCOMING_H { get { return BASE_BUTTONS + "incoming_friend_h.png"; } }
        public static string BUTTON_ADD_FRIEND_OUTGOING { get { return BASE_BUTTONS + "outgoing_friend.png"; } }
        public static string BUTTON_ADD_FRIEND_OUTGOING_H { get { return BASE_BUTTONS + "outgoing_friend_h.png"; } }
        public static string BUTTON_FRIEND_ACCESS_H { get { return BASE_BUTTONS + "access_change_h.png"; } }

        public static string CHAT_CALL_DEFAULT { get { return BASE + "call_chat_default.png"; } }
        public static string CHAT_CALL_SELECTED { get { return BASE + "call_chat_selected.png"; } }
        public static string ID_DEFAULT { get { return BASE + "id_default.png"; } }
        public static string ID_SELECTED { get { return BASE + "id_selected.png"; } }
        public static string NO_CHAT_LOG { get { return BASE + "no_chat_log.png"; } }
        public static string NO_CALL_LOG { get { return BASE + "no_call_log.png"; } }
        public static string NO_FRIENDS { get { return BASE + "no_friends.png"; } }
        public static string NO_NOTIFICATIONS { get { return BASE + "no_notifications.png"; } }

        public static string BUTTON_FRIEND_ADD { get { return BASE_BUTTONS + "friend_add.png"; } }
        public static string BUTTON_FRIEND_ADD_H { get { return BASE_BUTTONS + "friend_add_h.png"; } }
        public static string BUTTON_INFO { get { return BASE_BUTTONS + "info.png"; } }
        public static string BUTTON_INFO_H { get { return BASE_BUTTONS + "info_h.png"; } }
        public static string BUTTON_CHAT { get { return BASE_BUTTONS + "chat_mini.png"; } }
        public static string BUTTON_CHAT_H { get { return BASE_BUTTONS + "chat_mini_h.png"; } }
        public static string BUTTON_VOICE_CALL { get { return BASE_BUTTONS + "sip_call.png"; } }
        public static string BUTTON_VOICE_CALL_H { get { return BASE_BUTTONS + "sip_call_h.png"; } }
        public static string BUTTON_VIDEO_CALL { get { return BASE_BUTTONS + "video_call.png"; } }
        public static string BUTTON_VIDEO_CALL_H { get { return BASE_BUTTONS + "video_call_h.png"; } }
        public static string BUTTON_PASSWORD_HIDE { get { return BASE_BUTTONS + "password_hide.png"; } }
        public static string BUTTON_PASSWORD_SHOW { get { return BASE_BUTTONS + "password_show.png"; } }
        public static string BUTTON_ADD_ICON { get { return BASE_BUTTONS + "add.png"; } }
        public static string BUTTON_ADD_ICON_PRESSED { get { return BASE_BUTTONS + "add_pressed.png"; } }
        public static string BUTTON_ADD_MY_PROFILE { get { return BASE_BUTTONS + "add_more.png"; } }
        public static string BUTTON_ADD_MY_PROFILE_H { get { return BASE_BUTTONS + "add_more_h.png"; } }
        public static string BUTTON_EDIT { get { return BASE_BUTTONS + "edit.png"; } }
        public static string BUTTON_EDIT_H { get { return BASE_BUTTONS + "edit_h.png"; } }
        public static string BUTTON_EDIT_RECOVERY { get { return BASE_BUTTONS + "edit_recovery.png"; } }
        public static string BUTTON_EDIT_RECOVERY_H { get { return BASE_BUTTONS + "edit_recovery_h.png"; } }
        public static string BUTTON_CANCEL_EDIT { get { return BASE_BUTTONS + "close_mini.png"; } }
        public static string BUTTON_CANCEL_EDIT_H { get { return BASE_BUTTONS + "close_mini_h.png"; } }
        public static string BUTTON_OK_MINI { get { return BASE_BUTTONS + "ok_mini.png"; } }
        public static string BUTTON_OK_MINI_H { get { return BASE_BUTTONS + "ok_mini_h.png"; } }
        public static string BUTTON_TAKE_PHOTOS { get { return BASE_BUTTONS + "option_take_photo.png"; } }
        public static string BUTTON_UPLOAD_PHOTO { get { return BASE_BUTTONS + "option_upload_photo.png"; } }
        public static string BUTTON_ALBUM_PHOTO { get { return BASE_BUTTONS + "option_album_photo.png"; } }
        public static string BUTTON_EDIT_PHOTO { get { return BASE_BUTTONS + "edit_photo.png"; } }
        public static string BUTTON_EDIT_PHOTO_H { get { return BASE_BUTTONS + "edit_photo_h.png"; } }
        public static string BUTTON_EDIT_PHOTO_CLICK { get { return BASE_BUTTONS + "edit_photo_click.png"; } }
        public static string BUTTON_UPLOAD_FROM_COMPUTER { get { return BASE_BUTTONS + "from_computer.png"; } }
        public static string BUTTON_UPLOAD_FROM_COMPUTER_H { get { return BASE_BUTTONS + "from_computer_h.png"; } }
        public static string BUTTON_UPLOAD_FROM_COMPUTER_CLICK { get { return BASE_BUTTONS + "from_computer_click.png"; } }
        public static string BUTTON_TAKE_PHOTO { get { return BASE_BUTTONS + "take_photo.png"; } }
        public static string BUTTON_TAKE_PHOTO_H { get { return BASE_BUTTONS + "take_photo_h.png"; } }
        public static string BUTTON_TAKE_PHOTO_CLICK { get { return BASE_BUTTONS + "take_photo_click.png"; } }
        public static string COVER_IMAGE_BACKGROUND { get { return BASE + "cover_image_box.png"; } }
        public static string NEW_STATUS_OPTIONS { get { return BASE_BUTTONS + "upload.png"; } }
        public static string NEW_STATUS_OPTIONS_H { get { return BASE_BUTTONS + "upload_h.png"; } }
        public static string NEW_STATUS_IMAGE_UPLOAD_CROSS { get { return BASE_BUTTONS + "cross.png"; } }
        public static string NEW_STATUS_IMAGE_UPLOAD_CROSS_H { get { return BASE_BUTTONS + "cross_h.png"; } }
        public static string NEW_STATUS_EMOTICONS { get { return BASE_BUTTONS + "sts_emoticon.png"; } }
        public static string NEW_STATUS_EMOTICONS_H { get { return BASE_BUTTONS + "sts_emoticon_h.png"; } }
        public static string NEW_STATUS_LOCATION { get { return BASE_BUTTONS + "sts_location.png"; } }
        public static string NEW_STATUS_LOCATION_H { get { return BASE_BUTTONS + "sts_location_h.png"; } }
        public static string NEW_STATUS_TAG { get { return BASE_BUTTONS + "sts_tag.png"; } }
        public static string NEW_STATUS_TAG_H { get { return BASE_BUTTONS + "sts_tag_h.png"; } }
        public static string NEW_STATUS_MUSIC { get { return BASE_BUTTONS + "music.png"; } }
        public static string NEW_STATUS_MUSIC_H { get { return BASE_BUTTONS + "music_h.png"; } }
        public static string NEW_STATUS_WRITE_POST_ICON { get { return BASE + "new_status_write_icon.png"; } }
        public static string BUTTON_VIDEO_FEED { get { return BASE_BUTTONS + "video_feed.png"; } }
        public static string BUTTON_VIDEO_FEED_H { get { return BASE_BUTTONS + "video_feed_h.png"; } }
        public static string BUTTON_CREATE_CIRCLE_ICON { get { return BASE_BUTTONS + "create_circle_icon.png"; } }
        public static string CELEBRITY_DISCOVER_COUNTRY_ICON { get { return BASE + "disLctn.png"; } }
        public static string CELEBRITY_DISCOVER_CATEGORY_ICON { get { return BASE + "category.png"; } }

        public static string NEW_STATUS_MEDIA_ALBUM_LIST { get { return BASE_BUTTONS + "media_album_list.png"; } }
        public static string NEW_STATUS_MEDIA_ALBUM_LIST_H { get { return BASE_BUTTONS + "media_album_list_H.png"; } }
        public static string MEDIA_ADD_ALBUM_LIST_VIEW { get { return BASE_BUTTONS + "addMedia_album.png"; } }
        public static string MEDIA_ADD_ALBUM_LIST_VIEW_H { get { return BASE_BUTTONS + "addMedia_album_h.png"; } }
        public static string MEDIA_ALBUM_LIST_VIEW { get { return BASE_BUTTONS + "albumMedia_list.png"; } }
        public static string MEDIA_ALBUM_LIST_VIEW_H { get { return BASE_BUTTONS + "albumMedia_list_h.png"; } }
        public static string MEDIA_FULL_SCREEN_VIEW { get { return BASE_BUTTONS + "full_screen.png"; } }
        public static string MEDIA_FULL_SCREEN_VIEW_H { get { return BASE_BUTTONS + "full_screen_h.png"; } }
        public static string MEDIA_FULL_SCREEN_VIEW_SELECTED { get { return BASE_BUTTONS + "full_screen_selected.png"; } }
        public static string MEDIA_EXIT_FULL_SCREEN_VIEW { get { return BASE_BUTTONS + "exit_screen.png"; } }
        public static string MEDIA_EXIT_FULL_SCREEN_VIEW_H { get { return BASE_BUTTONS + "exit_screen_h.png"; } }
        public static string MEDIA_EXIT_FULL_SCREEN_VIEW_SELECTED { get { return BASE_BUTTONS + "exit_screen_selected.png"; } }
        public static string MEDIA_NEW_WINDOW { get { return BASE_BUTTONS + "new_window.png"; } }
        public static string MEDIA_NEW_WINDOW_H { get { return BASE_BUTTONS + "new_window_h.png"; } }
        public static string MEDIA_NEW_WINDOW_SELECTED { get { return BASE_BUTTONS + "new_window_selected.png"; } }
        public static string MEDIA_NEW_WINDOW_BACK { get { return BASE_BUTTONS + "new_window_back.png"; } }
        public static string MEDIA_NEW_WINDOW_BACK_H { get { return BASE_BUTTONS + "new_window_back_h.png"; } }
        public static string MEDIA_NEW_WINDOW_BACK_SELECTED { get { return BASE_BUTTONS + "new_window_back_selected.png"; } }
        public static string MEDIA_VIEWS_COUNT { get { return BASE + "media_views.png"; } }
        public static string CELEBRITY_FOLLOWING_ICON { get { return BASE_BUTTONS + "cel_flw_normal.png"; } }
        public static string CELEBRITY_FOLLOWING_ICON_P { get { return BASE_BUTTONS + "cel_flw_click.png"; } }

        public static string MEDIA_VIEW_PREVIOUS_ICON { get { return BASE_BUTTONS + "preMedia.png"; } }
        public static string MEDIA_VIEW_PREVIOUS_ICON_H { get { return BASE_BUTTONS + "preMedia_h.png"; } }
        public static string MEDIA_VIEW_PREVIOUS_ICON_D { get { return BASE_BUTTONS + "preMedia_d.png"; } }
        public static string MEDIA_VIEW_PREVIOUS_ICON_P { get { return BASE_BUTTONS + "preMedia_selected.png"; } }
        public static string MEDIA_VIEW_NEXT_ICON { get { return BASE_BUTTONS + "nextMedia.png"; } }
        public static string MEDIA_VIEW_NEXT_ICON_H { get { return BASE_BUTTONS + "nextMedia_h.png"; } }
        public static string MEDIA_VIEW_NEXT_ICON_D { get { return BASE_BUTTONS + "nextMedia_d.png"; } }
        public static string MEDIA_VIEW_NEXT_ICON_P { get { return BASE_BUTTONS + "nextMediat_selected.png"; } }
        public static string MEDIA_VIEW_PLAY_ICON { get { return BASE_BUTTONS + "play.png"; } }
        public static string MEDIA_VIEW_PLAY_ICON_H { get { return BASE_BUTTONS + "play_h.png"; } }
        public static string MEDIA_VIEW_PLAY_LARGE { get { return BASE_BUTTONS + "play_large.png"; } }
        public static string MEDIA_VIEW_PLAY_LARGE_H { get { return BASE_BUTTONS + "play_large_h.png"; } }
        public static string MEDIA_VIEW_PLAY_ICON_P { get { return BASE_BUTTONS + "play_selected.png"; } }
        public static string MEDIA_VIEW_PAUSE_ICON { get { return BASE_BUTTONS + "pause.png"; } }
        public static string MEDIA_VIEW_PAUSE_ICON_H { get { return BASE_BUTTONS + "pause_h.png"; } }
        public static string MEDIA_VIEW_PAUSE_ICON_P { get { return BASE_BUTTONS + "pause_selected.png"; } }
        public static string MEDIA_VIEW_VOLUME_ICON { get { return BASE_BUTTONS + "volume.png"; } }

        public static string CLOSE { get { return BASE_BUTTONS + "close.png"; } }
        public static string CLOSE_H { get { return BASE_BUTTONS + "close_h.png"; } }
        public static string CLOSE_TRANSPARENT { get { return BASE_BUTTONS + "close_transparent.png"; } }
        public static string STICKER_REMOVE { get { return BASE_BUTTONS + "sticker_remove.png"; } }
        public static string STICKER_REMOVE_H { get { return BASE_BUTTONS + "sticker_remove_h.png"; } }
        public static string STICKER_RECENT_ICON { get { return BASE + "recent_sticker_icon.png"; } }
        public static string BUTTON_EXPAND { get { return BASE_BUTTONS + "expand.png"; } }
        public static string BUTTON_EXPAND_H { get { return BASE_BUTTONS + "expand_h.png"; } }
        public static string BUTTON_CLOSE_BOLD { get { return BASE_BUTTONS + "close_bold.png"; } }
        public static string BUTTON_CLOSE_BOLD_H { get { return BASE_BUTTONS + "close_bold_h.png"; } }

        public static string CHAT_POPUP_CLOSE_BOLD { get { return BASE_BUTTONS + "chat_popup_msg_cross.png"; } }
        public static string CHAT_POPUP_CLOSE_BOLD_H { get { return BASE_BUTTONS + "chat_popup_msg_cross_h.png"; } }
        public static string CHAT_POPUP_ID { get { return BASE_BUTTONS + "caht_popup_msg_id.png"; } }

        public static string BUTTON_CLOSE_BOLD_WHITE { get { return BASE_BUTTONS + "close_bold_white.png"; } }
        public static string BUTTON_CLOSE_SMALL_H { get { return BASE_BUTTONS + "close_small_h.png"; } }
        public static string BUTTON_CLOSE_SMALL_WHITE { get { return BASE_BUTTONS + "close_small_white.png"; } }
        public static string BUTTON_SETTING_MINI { get { return BASE_BUTTONS + "setting_mini.png"; } }
        public static string BUTTON_SETTING_MINI_H { get { return BASE_BUTTONS + "setting_mini_h.png"; } }
        public static string BUTTON_SETTING_MINI_SELECTED { get { return BASE_BUTTONS + "setting_mini_selected.png"; } }
        public static string BUTTON_ABOUT_SETTING_MINI { get { return BASE_BUTTONS + "about_setting_mini.png"; } }
        public static string BUTTON_ABOUT_SETTING_MINI_H { get { return BASE_BUTTONS + "about_setting_mini_h.png"; } }
        public static string BUTTON_ABOUT_SETTING_MINI_SELECTED { get { return BASE_BUTTONS + "about_setting_mini_selected.png"; } }
        public static string BUTTON_UPLOAD_FROM_DIRECTORY { get { return BASE_BUTTONS + "directory_upload.png"; } }
        public static string BUTTON_UPLOAD_FROM_DIRECTORY_H { get { return BASE_BUTTONS + "directory_upload_h.png"; } }
        public static string BUTTON_CAMERA { get { return BASE_BUTTONS + "cam_upload.png"; } }
        public static string BUTTON_CAMERA_H { get { return BASE_BUTTONS + "cam_upload_h.png"; } }
        public static string BUTTON_CAMERA_WHITE { get { return BASE + "cam_upload_white.png"; } }
        public static string BUTTON_MARK_AS_READ { get { return BASE_BUTTONS + "radio_button.png"; } }
        public static string BUTTON_MARK_AS_READ_H { get { return BASE_BUTTONS + "radio_button_select.png"; } }
        public static string BUTTON_NOTIFICATION_SELECT { get { return BASE_BUTTONS + "tick.png"; } }//offnet.png
        public static string BUTTON_NOTIFICATION_SELECT_H { get { return BASE_BUTTONS + "tick_h.png"; } }
        public static string BUTTON_NOTIFICATION_SELECTED { get { return BASE_BUTTONS + "tick_h2.png"; } }
        public static string RADIO_BUTTON_CHAT_BG { get { return BASE_BUTTONS + "radiobtn_chat_bg.png"; } }
        public static string RADIO_BUTTON_CHAT_BG_SELECTED { get { return BASE_BUTTONS + "radio_btn_selectedchat_bg.png"; } }
        public static string CIRCLE_OUTSIDE_PROFILE_PIC { get { return BASE + "cricle_outside_profile_pic.png"; } }
        public static string CIRCLE_OUTSIDE_PROFILE_PIC_MYNAMEPANEL { get { return BASE + "cricle_outside_profile_pic_mynamepanel.png"; } }
        public static string CIRCLE_OUTSIDE_PROFILE_PIC_PROFILE { get { return BASE + "cricle_outside_profile_pic_profile.png"; } }

        public static string CALL_INCOMING { get { return BASE + "callhistory_incoming.png"; } }
        public static string CALL_OUTGOING { get { return BASE + "callhistory_outgoing.png"; } }
        public static string CALL_MISSED { get { return BASE + "callhistory_missed.png"; } }
        public static string CALL_FROM_LOG { get { return BASE + "log_call.png"; } }
        public static string CALL_FROM_LOG_H { get { return BASE + "log_call_h.png"; } }
        public static string CALL_FROM_LOG_SELECTED { get { return BASE + "log_call_selected.png"; } }
        public static string DIALPAD_CALL { get { return BASE + "diapd_call.png"; } }
        public static string DIALPAD_CALL_H { get { return BASE + "diapd_call_h.png"; } }
        public static string DIALPAD_CALL_SELECTED { get { return BASE + "diapd_call_selected.png"; } }

        public static string VIDEO_CALL_FROM_LOG { get { return BASE + "log_video_call.png"; } }
        public static string VIDEO_CALL_FROM_LOG_H { get { return BASE + "log_video_call_h.png"; } }
        public static string VIDEO_CALL_FROM_LOG_SELECTED { get { return BASE + "log_video_call_selected.png"; } }

        public static string CHAT_INCOMING { get { return BASE + "chat_incoming.png"; } }
        public static string CHAT_OUTGOING { get { return BASE + "chat_outgoing.png"; } }
        public static string SEARCH_IMAGE { get { return BASE + "search_img.png"; } }
        public static string SPECIAL_SEARCH_IMAGE { get { return BASE + "special_search.png"; } }
        public static string UNKNOWN_IMAGE { get { return BASE + "unknown.png"; } }
        public static string PROFILE_IMAGE_BOX_UNKNOWN { get { return BASE + "profile_image_box.png"; } }
        public static string UNKNOWN_IMAGE_LARGE { get { return BASE + "unknown_large.png"; } }
        public static string VERIFY { get { return BASE + "verify.png"; } }
        public static string CELEBRITY_DEFAULT_IMAGE { get { return BASE + "celebrity_default_image.jpg"; } }
        public static string ROOM_DEFAULT_IMAGE { get { return BASE + "room_default_image.png"; } }


        public static string MENU_MOOD { get { return BASE_MENU + "menu_mood.png"; } }
        public static string MENU_SIGN_OUT { get { return BASE_MENU + "menu_sign_out.png"; } }
        public static string MENU_CLOSE { get { return BASE_MENU + "menu_close.png"; } }
        public static string MENU_CHECK_FOR_UPDATE { get { return BASE_MENU + "menu_check_for_update.png"; } }
        public static string MENU_SETTINGS { get { return BASE_MENU + "menu_settings.png"; } }
        public static string MENU_HELP_DESK { get { return BASE_MENU + "menu_help.png"; } }
        public static string MENU_INFO { get { return BASE_MENU + "menu_info.png"; } }
        public static string PRIVACE_POLICY { get { return BASE_MENU + "menu_privacy.png"; } }
        public static string CONTACT_US_ACTIVE { get { return BASE_MENU + "menu_contact_active.png"; } }
        public static string CONTACT_US_INACTIVE { get { return BASE_MENU + "menu_contact_inactive.png"; } }
        public static string ABOUIT_RINGID { get { return BASE_MENU + "menu_about_ringid.png"; } }

        public static string STATUS_ONLINE { get { return BASE_STATUS + "status_online.png"; } }
        public static string STATUS_OFFLINE { get { return BASE_STATUS + "status_offline.png"; } }
        public static string STATUS_HOVER { get { return BASE_STATUS + "status_hover.png"; } }
        public static string STATUS_DONOT_DISTURB { get { return BASE_STATUS + "status_dont_disturb.png"; } }
        public static string ONLINE_DESKTOP { get { return BASE_STATUS + "online_desktop.png"; } }
        public static string ONLINE_ANDROID { get { return BASE_STATUS + "online_android.png"; } }
        public static string ONLINE_IOS { get { return BASE_STATUS + "online_ios.png"; } }
        public static string ONLINE_WEB { get { return BASE_STATUS + "online_web.png"; } }
        public static string ONLINE_WINDOWS { get { return BASE_STATUS + "online_windows.png"; } }
        public static string OFFLINE { get { return BASE_STATUS + "offline.png"; } }
        public static string AWAY { get { return BASE_STATUS + "away.png"; } }
        public static string POPUP_STATUS { get { return BASE + "popup_status.png"; } }
        public static string NO_INTERNET_AVAILABLE { get { return BASE_STATUS + "no_internet.png"; } }
        public static string CHECK_BOX { get { return BASE + "check_box.png"; } }
        public static string CHECK_BOX_SLECTED { get { return BASE + "check_box_selected.png"; } }

        public static string SETTING_MINI { get { return BASE + "setting_mini.png"; } }
        public static string SETTING_MINI_H { get { return BASE + "setting_mini_h.png"; } }
        public static string STAR { get { return BASE + "star.png"; } }
        public static string STAR_H { get { return BASE + "star_h.png"; } }
        public static string STAR_F { get { return BASE + "star_f.png"; } }
        public static string CIRCLE_IMAGE_IN_LIST { get { return BASE + "group_mini.png"; } }
        public static string TICK_MARK { get { return BASE + "tick_mark.png"; } }
        public static string TICK_MARK_WHITE { get { return BASE + "tick_white.png"; } }
        public static string PROFILE_IMAGE_BOX { get { return BASE + "profile_image_box.png"; } }


        public static string EMO_UP_ARROW { get { return BASE_BUTTONS + "emo_up_arrow.png"; } }
        public static string EMO_UP_ARROW_H { get { return BASE_BUTTONS + "emo_up_arrow_h.png"; } }
        public static string EMO_DOWN_ARROW { get { return BASE_BUTTONS + "emo_down_arrow.png"; } }
        public static string EMO_DOWN_ARROW_H { get { return BASE_BUTTONS + "emo_down_arrow_h.png"; } }
        public static string STICKER_ARRANGE { get { return BASE_BUTTONS + "sticker_arrange.png"; } }
        public static string STICKER_ARRANGE_H { get { return BASE_BUTTONS + "sticker_arrange_h.png"; } }

        public static string DOWN_ARROW { get { return BASE + "down_arrow.png"; } }
        public static string DOWN_ARROW_H { get { return BASE + "down_arrow_h.png"; } }
        public static string EDIT_ALBUM_SONG_ARROW { get { return BASE + "album_edit.png"; } }
        public static string RIGHT_ARROW { get { return BASE_BUTTONS + "right_arrow.png"; } }
        public static string RIGHT_ARROW_H { get { return BASE_BUTTONS + "right_arrow_h.png"; } }
        public static string RIGHT_MINI_ARROW { get { return BASE + "right_mini_arrow.png"; } }
        public static string RIGHT_MINI_ARROW_H { get { return BASE + "right_mini_arrow_h.png"; } }
        public static string LEFT_ARROW { get { return BASE_BUTTONS + "left_arrow.png"; } }
        public static string LEFT_ARROW_H { get { return BASE_BUTTONS + "left_arrow_h.png"; } }
        public static string ALBUM_IMAGE_SELECTION_BOX { get { return BASE_BUTTONS + "album_image_selection.png"; } }

        /*FEED*/
        public static string NO_FEED_ANIMATION
        {
            get
            {
                return BASE_LOADERS + "no_feed_animation.gif";
            }
        }

        public static string RECORD_ANIMATION { get { return BASE_LOADERS + "record.gif"; } }
        public static string PLAY_RECORD_VIDEO { get { return BASE_CHAT + "play_rec_vdo.png"; } }
        public static string PLAY_RECORD_VIDEO_H { get { return BASE_CHAT + "play_rec_vdo_h.png"; } }
        /********CALL*******/
        public static string MESSAGE { get { return BASE_CALL + "messege.png"; } }
        public static string MESSAGE_H { get { return BASE_CALL + "messege_h.png"; } }

        public static string CHAT_WITH_CALLING_FRIEND { get { return BASE_CALL + "chat.png"; } }
        public static string CHAT_WITH_CALLING_FRIEND_H { get { return BASE_CALL + "chat_h.png"; } }
        public static string ANSWER_WITH_VOICE_ONLY { get { return BASE_CALL + "answer_with_voice_Only.png"; } }
        public static string ANSWER_WITH_VOICE_ONLY_H { get { return BASE_CALL + "answer_with_voice_Only_h.png"; } }

        public static string IGNORE_CALL { get { return BASE_CALL + "ignore.png"; } }
        public static string IGNORE_CALL_H { get { return BASE_CALL + "ignore_h.png"; } }
        public static string INCOMING_CALL_ANIMATION { get { return BASE_CALL + "incoming_call_animation.gif"; } }
        //   public static string OUTGOING_CALL_ANIMATION { get { return BASE_CALL + "outgoingCallAnim.gif"; } }
        public static string CALL_ACCEPT { get { return BASE_CALL + "receive.png"; } }
        public static string CALL_ACCEPT_H { get { return BASE_CALL + "receive_h.png"; } }
        public static string CALL_HOLD { get { return BASE_CALL + "hold.png"; } }
        public static string CALL_HOLD_H { get { return BASE_CALL + "hold_h.png"; } }
        public static string CALL_UNHOLD { get { return BASE_CALL + "unhold.png"; } }
        public static string CALL_UNHOLD_H { get { return BASE_CALL + "unhold_h.png"; } }
        public static string CALL_MUTE { get { return BASE_CALL + "mute.png"; } }
        public static string CALL_MUTE_H { get { return BASE_CALL + "mute_h.png"; } }
        public static string CALL_UMMUTE { get { return BASE_CALL + "unmute.png"; } }
        public static string CALL_UMMUTE_H { get { return BASE_CALL + "unmute_h.png"; } }
        public static string BUTTON_VOLUME { get { return BASE_CALL + "speker.png"; } }
        public static string BUTTON_VOLUME_H { get { return BASE_CALL + "speker_h.png"; } }

        public static string BUTTON_REC_VIDEO { get { return BASE_CALL + "vdo_recv.png"; } }
        public static string BUTTON_REC_VIDEO_H { get { return BASE_CALL + "vdo_recv_h.png"; } }

        public static string BUTTON_VIDEO { get { return BASE_CALL + "video.png"; } }
        public static string BUTTON_VIDEO_H { get { return BASE_CALL + "video_h.png"; } }

        public static string BUTTON_STOP_VIDEO { get { return BASE_CALL + "video_off.png"; } }
        public static string BUTTON_STOP_VIDEO_H { get { return BASE_CALL + "video_off_h.png"; } }
        public static string CALL_END { get { return BASE_CALL + "end_call.png"; } }
        public static string CALL_END_H { get { return BASE_CALL + "end_call_h.png"; } }
        public static string CALL_TOP_BAR_INCOMING { get { return BASE_CALL + "call_top_bar_incoming.png"; } }
        public static string CALL_TOP_BAR_OUTGOING { get { return BASE_CALL + "call_top_bar_outgoing.png"; } }

        public static string CALL_NET0 { get { return BASE_CALL + "net0.png"; } }
        public static string CALL_NET1 { get { return BASE_CALL + "net1.png"; } }
        public static string CALL_NET2 { get { return BASE_CALL + "net2.png"; } }
        public static string CALL_NET3 { get { return BASE_CALL + "net3.png"; } }
        public static string CALL_NET4 { get { return BASE_CALL + "net4.png"; } }


        public static string ACCEPT_CALL_MINI { get { return BASE_CALL + "accept_call_mini.png"; } }
        public static string ACCEPT_CALL_MINI_H { get { return BASE_CALL + "accept_call_mini_h.png"; } }
        public static string BUSY_MSG_MINI { get { return BASE_CALL + "busy_msg_mini.png"; } }
        public static string BUSY_MSG_MINI_H { get { return BASE_CALL + "busy_msg_mini_h.png"; } }
        public static string END_CALL_MINI { get { return BASE_CALL + "end_call_mini.png"; } }
        public static string END_CALL_MINI_H { get { return BASE_CALL + "end_call_mini_h.png"; } }
        public static string REJECT_END_CALL_MINI { get { return BASE_CALL + "reject_end_call_mini.png"; } }
        public static string REJECT_END_CALL_MINI_H { get { return BASE_CALL + "reject_end_call_mini_h.png"; } }
        //public static string CALL_RING_ID { get { return BASE_CALL + "call_ringid.png"; } }

        /********CALL*******/

        /********** Settings ***************/
        public static string CALL_SETTINGS { get { return BASE_SETTINGS + "call-settings.png"; } }
        public static string CALL_SETTINGS_H { get { return BASE_SETTINGS + "call-settings-h.png"; } }
        public static string CHAT_BACKGROUND { get { return BASE_SETTINGS + "change-BG.png"; } }
        public static string CHAT_BACKGROUND_H { get { return BASE_SETTINGS + "change-BG-h.png"; } }
        public static string CUSTOMIZE_MSG { get { return BASE_SETTINGS + "customise-msg.png"; } }
        public static string CUSTOMIZE_MSG_H { get { return BASE_SETTINGS + "customise-msg-h.png"; } }
        public static string IM_SETTINGS { get { return BASE_SETTINGS + "im_settings.png"; } }
        public static string IM_SETTINGS_H { get { return BASE_SETTINGS + "im_settings_h.png"; } }
        public static string HISTORY { get { return BASE_SETTINGS + "history.png"; } }
        public static string HISTORY_H { get { return BASE_SETTINGS + "history-h.png"; } }
        public static string PASSWORD { get { return BASE_SETTINGS + "password.png"; } }
        public static string PASSWORD_H { get { return BASE_SETTINGS + "password-h.png"; } }
        public static string PRIVACY { get { return BASE_SETTINGS + "privacy.png"; } }
        public static string PRIVACY_H { get { return BASE_SETTINGS + "privacy-h.png"; } }
        public static string BLOCKED { get { return BASE_SETTINGS + "blocked.png"; } }
        public static string BLOCKED_H { get { return BASE_SETTINGS + "blocked-h.png"; } }
        public static string RING_SETTINGS { get { return BASE_SETTINGS + "setting.png"; } }
        public static string RING_SETTINGS_H { get { return BASE_SETTINGS + "setting_h.png"; } }
        public static string RECOVERY_SETTINGS { get { return BASE_SETTINGS + "recovery.png"; } }
        public static string RECOVERY_SETTINGS_H { get { return BASE_SETTINGS + "recovery_h.png"; } }
        public static string USER_PREFER { get { return BASE_SETTINGS + "user.png"; } }
        public static string USER_PREFER_H { get { return BASE_SETTINGS + "user-h.png"; } }
        public static string CALL_DIVERT { get { return BASE_SETTINGS + "call-divart.png"; } }
        public static string CALL_DIVERT_H { get { return BASE_SETTINGS + "call-divart-h.png"; } }
        public static string AUDIO { get { return BASE_SETTINGS + "audio.png"; } }
        public static string AUDIO_H { get { return BASE_SETTINGS + "audio-h.png"; } }
        public static string VOLUME { get { return BASE_SETTINGS + "microphone.png"; } }
        public static string SPEAKER { get { return BASE_SETTINGS + "speaker.png"; } }
        public static string SPEAKER_TEST { get { return BASE_SETTINGS + "speaker_test.png"; } }
        public static string SPEAKER_TEST_H { get { return BASE_SETTINGS + "speaker_test_h.png"; } }
        public static string SPEAKER_TEST_SELECTED { get { return BASE_SETTINGS + "speaker_test_selected.png"; } }
        public static string VIDEO_SETTINGS { get { return BASE_SETTINGS + "video_settings.png"; } }
        public static string VIDEO_SETTINGS_H { get { return BASE_SETTINGS + "video_settings_h.png"; } }

        /************************************/

        //CHAT

        public static string CHAT_EDITED { get { return BASE_CHAT + "chat_edited.png"; } }
        public static string CHAT_EMOTICON { get { return BASE_CHAT + "chat_emoticon.png"; } }
        public static string CHAT_EMOTICON_H { get { return BASE_CHAT + "chat_emoticon_h.png"; } }
        public static string CHAT_MORE_OPTION_BG { get { return BASE_CHAT + "chat_more_option_bg.png"; } }
        public static string CHAT_RECENT { get { return BASE_CHAT + "chat_recent.png"; } }
        public static string CHAT_RECENT_H { get { return BASE_CHAT + "chat_recent_h.png"; } }
        public static string CHAT_SEND { get { return BASE_CHAT + "chat_send.png"; } }
        public static string CHAT_SEND_H { get { return BASE_CHAT + "chat_send_h.png"; } }
        public static string CHAT_STICKER { get { return BASE_CHAT + "chat_sticker.png"; } }
        public static string CHAT_STICKER_H { get { return BASE_CHAT + "chat_sticker_h.png"; } }
        public static string MINUS { get { return BASE_CHAT + "minus.png"; } }
        public static string MINUS_H { get { return BASE_CHAT + "minus_h.png"; } }
        public static string OFF { get { return BASE_CHAT + "off.png"; } }
        public static string ON { get { return BASE_CHAT + "on.png"; } }
        public static string PLUS { get { return BASE_CHAT + "plus.png"; } }
        public static string PLUS_H { get { return BASE_CHAT + "plus_h.png"; } }
        public static string RECORD_PAUSE { get { return BASE_CHAT + "record_pause.png"; } }
        public static string RECORD_PAUSE_H { get { return BASE_CHAT + "record_pause_h.png"; } }
        public static string RECORD_RESUME { get { return BASE_CHAT + "record_resume.png"; } }
        public static string RECORD_RESUME_H { get { return BASE_CHAT + "record_resume_h.png"; } }
        public static string RECORD_SEND { get { return BASE_CHAT + "record_send.png"; } }
        public static string RECORD_SEND_H { get { return BASE_CHAT + "record_send_h.png"; } }
        public static string RECORD_START { get { return BASE_CHAT + "record_start.png"; } }
        public static string RECORD_START_H { get { return BASE_CHAT + "record_start_h.png"; } }
        public static string RECORD_STOP { get { return BASE_CHAT + "record_stop.png"; } }
        public static string RECORD_STOP_H { get { return BASE_CHAT + "record_stop_h.png"; } }
        public static string WATCH { get { return BASE_CHAT + "watch.png"; } }
        public static string WATCH_H { get { return BASE_CHAT + "watch_h.png"; } }
        public static string EMOTICON_POPUP_DOWN_ARROW { get { return BASE + "emoticon_popup_down_arrow.png"; } }
        public static string EMOTICON_POPUP_UP_ARROW { get { return BASE + "emoticon_popup_up_arrow.png"; } }
        public static string EMOTICON_POPUP_NO_ARROW { get { return BASE + "emoticon_popup_no_arrow.png"; } }
        public static string POPUP_CHAT_RECENT { get { return BASE_CHAT + "popup_chat_recent.png"; } }
        public static string CHAT_DEFAULT_AUDIO { get { return BASE_CHAT + "chat_default_audio.png"; } }
        public static string CHAT_DEFAULT_FILE { get { return BASE_CHAT + "chat_default_file.png"; } }
        public static string CHAT_DEFAULT_FILE_DOWNLOAD { get { return BASE_CHAT + "chat_default_file_download.png"; } }
        public static string CHAT_DEFAULT_FILE_UPLOAD { get { return BASE_CHAT + "chat_default_file_upload.png"; } }
        public static string CHAT_DEFAULT_VIDEO { get { return BASE_CHAT + "chat_default_video.png"; } }
        public static string CHAT_DEFAULT_IMAGE { get { return BASE_CHAT + "chat_default_image.png"; } }
        public static string RECORDER_CLOSE { get { return BASE_CHAT + "recorder_close.png"; } }
        public static string RECORDER_CLOSE_H { get { return BASE_CHAT + "recorder_close_h.png"; } }
        public static string INCLUDE_VOICE { get { return BASE_CHAT + "include_voice.png"; } }
        public static string INCLUDE_VOICE_H { get { return BASE_CHAT + "include_voice_h.png"; } }
        public static string INCLUDE_VOICE_SELECTED { get { return BASE_CHAT + "include_voice_selected.png"; } }
        public static string SEND_CHAT_IMAGE { get { return BASE_CHAT + "send_chat_image.png"; } }
        public static string SEND_CHAT_IMAGE_H { get { return BASE_CHAT + "send_chat_image_h.png"; } }
        public static string SEND_CHAT_IMAGE_SELECTED { get { return BASE_CHAT + "send_chat_image_selected.png"; } }
        public static string CHAT_RESUME_BIG { get { return BASE_CHAT + "chat_resume_big.png"; } }
        public static string CHAT_RESUME_BIG_H { get { return BASE_CHAT + "chat_resume_big_h.png"; } }
        public static string CHAT_RESUME_BIG_SELECTED { get { return BASE_CHAT + "chat_resume_big_selected.png"; } }
        public static string ADD_FROM_WEBCAM { get { return BASE_CHAT + "add_from_webcam.png"; } }
        public static string ADD_FROM_DIRECTORY { get { return BASE_CHAT + "add_from_directory.png"; } }
        public static string ADD_FROM_WEBCAM_H { get { return BASE_CHAT + "add_from_webcam_h.png"; } }
        public static string ADD_FROM_DIRECTORY_H { get { return BASE_CHAT + "add_from_directory_h.png"; } }
        public static string CHAT_MULTIMEDIA { get { return BASE_CHAT + "chat_multimedia.png"; } }
        public static string CHAT_MULTIMEDIA_H { get { return BASE_CHAT + "chat_multimedia_h.png"; } }
        public static string ADD_STICKER { get { return BASE_CHAT + "add_sticker.png"; } }
        public static string ADD_STICKER_H { get { return BASE_CHAT + "add_sticker_h.png"; } }
        public static string STICKER_DOWNLOAD { get { return BASE + "sticker_download.png"; } }
        public static string STICKER_DOWNLOAD_H { get { return BASE + "sticker_download_h.png"; } }
        public static string CHAT_MEDIA_PLAY { get { return BASE_CHAT + "chat_media_play.png"; } }
        public static string CHAT_MEDIA_PLAY_H { get { return BASE_CHAT + "chat_media_play_h.png"; } }
        public static string CHAT_MEDIA_PLAY_DISABLE { get { return BASE_CHAT + "chat_media_play_disable.png"; } }
        public static string CHAT_MEDIA_PAUSE { get { return BASE_CHAT + "chat_media_pause.png"; } }
        public static string CHAT_MEDIA_PAUSE_H { get { return BASE_CHAT + "chat_media_pause_h.png"; } }
        public static string CHAT_MEDIA_PAUSE_DISABLE { get { return BASE_CHAT + "chat_media_pause_disable.png"; } }
        public static string HAND_GRAB { get { return BASE_CHAT + "hand_grab.ico"; } }
        public static string HAND_RELEASE { get { return BASE_CHAT + "hand_release.ico"; } }
        public static string IMAGE_ORIGINAL_SIZE { get { return BASE_CHAT + "image_original_size.png"; } }
        public static string IMAGE_ORIGINAL_SIZE_H { get { return BASE_CHAT + "image_original_size_h.png"; } }
        public static string IMAGE_ZOOM { get { return BASE_CHAT + "image_zoom.png"; } }
        public static string IMAGE_ZOOM_H { get { return BASE_CHAT + "image_zoom_h.png"; } }
        public static string RECORD_EXPAND { get { return BASE_CHAT + "record_expand.png"; } }
        public static string RECORD_EXPAND_H { get { return BASE_CHAT + "record_expand_h.png"; } }
        public static string RECORD_NORMAL { get { return BASE_CHAT + "record_normal.png"; } }
        public static string RECORD_NORMAL_H { get { return BASE_CHAT + "record_normal_h.png"; } }
        public static string LOVE { get { return BASE_CHAT + "love.png"; } }
        public static string LOVE_H { get { return BASE_CHAT + "love_h.png"; } }
        public static string CHAT_LOG_PLUS { get { return BASE_CHAT + "chat_log_plus.png"; } }
        public static string CHAT_LOG_PLUS_H { get { return BASE_CHAT + "chat_log_plus_h.png"; } }
        public static string CHAT_EMOTICON_LARGE { get { return BASE_CHAT + "chat_emoticon_large.png"; } }
        public static string CHAT_EMOTICON_LARGE_H { get { return BASE_CHAT + "chat_emoticon_large_h.png"; } }
        public static string DELIVERED { get { return BASE_CHAT + "delivered.png"; } }
        public static string SEEN { get { return BASE_CHAT + "seen.png"; } }
        public static string ADD_GROUP_MEMBER { get { return BASE_BUTTONS + "add_group_member.png"; } }
        public static string ADD_GROUP_MEMBER_H { get { return BASE_BUTTONS + "add_group_member_h.png"; } }
        public static string EDIT_INFO { get { return BASE + "edit_info.png"; } }

        public static string LINK_BUTTON_PREVIOUS_IMAGE { get { return BASE_BUTTONS + "link_prev.png"; } }
        public static string LINK_BUTTON_PREVIOUS_IMAGE_H { get { return BASE_BUTTONS + "link_prev_h.png"; } }
        public static string LINK_BUTTON_PREVIOUS_IMAGE_P { get { return BASE_BUTTONS + "link_prev_select.png"; } }
        public static string LINK_BUTTON_NEXT_IMAGE { get { return BASE_BUTTONS + "link_next.png"; } }
        public static string LINK_BUTTON_NEXT_IMAGE_H { get { return BASE_BUTTONS + "link_next_h.png"; } }
        public static string LINK_BUTTON_NEXT_IMAGE_P { get { return BASE_BUTTONS + "link_next_select.png"; } }
        public static string LINK_VIDEO_ICON { get { return BASE + "Link_Video_Icon.png"; } }
        public static string LINK_VIDEO_ICON_H { get { return BASE + "Link_Video_Icon_h.png"; } }
        public static string LINK_MESSAGE_ICON { get { return BASE + "link_mg_icon.png"; } }
        public static string INFO_BIG { get { return BASE + "info_big.png"; } }
        public static string PHOTOS { get { return BASE + "photos.png"; } }
        public static string MV { get { return BASE + "music_video.png"; } }

        public static string LIST_VIEW_ALBUM { get { return BASE_BUTTONS + "list_view.png"; } }
        public static string LIST_VIEW_ALBUM_H { get { return BASE_BUTTONS + "list_view_h.png"; } }
        public static string THUMBNAIL_VIEW { get { return BASE_BUTTONS + "thumbnail_view.png"; } }
        public static string THUMBNAIL_VIEW_H { get { return BASE_BUTTONS + "thumbnail_view_h.png"; } }
        public static string UPLOAD_MUSIC_AND_VIDEO_ICON { get { return BASE_BUTTONS + "uploadMusic.png"; } }
        public static string UPLOAD_MUSIC_AND_VIDEO_ICON_H { get { return BASE_BUTTONS + "uploadMusic_h.png"; } }
        public static string UPLOAD_MUSIC_AND_VIDEO_ICON_SELECTED { get { return BASE_BUTTONS + "uploadMusic_selected.png"; } }
        public static string COMMENT_UPLOAD_ICON { get { return BASE_BUTTONS + "cmnt_upload.png"; } }
        public static string COMMENT_UPLOAD_ICON_H { get { return BASE_BUTTONS + "cmnt_upload_h.png"; } }
        public static string DISCOVER_SEARCH_ICON { get { return BASE_BUTTONS + "discover_search.png"; } }
        public static string DISCOVER_SEARCH_ICON_H { get { return BASE_BUTTONS + "discover_search_h.png"; } }
        public static string DISCOVER_SEARCH_ICON_CLICKED { get { return BASE_BUTTONS + "discover_search_click.png"; } }

        public static string MEDIA_ADD_TO_ALBUM_ICON { get { return BASE + "add_to_album_Media.png"; } }
        public static string MEDIA_ADD_TO_ALBUM_ORANGE_ICON { get { return BASE + "add_to_orange_album.png"; } }
        public static string MEDIA_SHARE_ORANGE_ICON { get { return BASE + "share_media_orange_icon.png"; } }
        public static string SINGLE_MEDIA_SHARE_ICON { get { return BASE + "share_media_icon.png"; } }
        //public static string MEDIA_DOWNLOAD_ICON { get { return BASE + "download.png"; } }
        public static string MEDIA_DOWNLOAD_ORANGE_ICON { get { return BASE + "download_orange.png"; } }
        public static string MEDIA_DOWNLOADING_ICON { get { return BASE + "downloading.png"; } }
        public static string MEDIA_WAITING_ICON { get { return BASE + "download_waiting.png"; } }
        public static string MEDIA_DOWNLOADED_ICON { get { return BASE + "downloaded.png"; } }

        public static string MEDIA_SHARE_ICON { get { return BASE + "shareMedia.png"; } }
        public static string MEDIA_UPLOAD_WAITING_ICON { get { return BASE + "waiting.jpg"; } }

        public static string DEFAULT_AUDIO_IMAGE { get { return BASE + "default_audio_image.jpg"; } }
        public static string DEFAULT_AUDIO_SMALL { get { return BASE + "default_audio_small.png"; } }
        public static string DEFAULT_VIDEO_IMAGE { get { return BASE + "default_video_image.png"; } }
        public static string DEFAULT_VIDEO_SMALL { get { return BASE + "default_video_small.png"; } }
        public static string NEW_STATUS_VALIDITY_ICON { get { return BASE_BUTTONS + "validity.png"; } }
        public static string NEW_STATUS_VALIDITY_ICON_H { get { return BASE_BUTTONS + "validity_h.png"; } }
        public static string NEW_STATUS_VALIDITY_ICON_SELECTED { get { return BASE_BUTTONS + "validity_selected.png"; } }
        public static string NEW_STATUS_AUDIO_ICON { get { return BASE + "audio.png"; } }
        public static string NEW_STATUS_VIDEO_ICON { get { return BASE + "video.png"; } }
        public static string POPUP_VALIDITY_UP_ARROW { get { return BASE + "validity_popup_up_arrow.png"; } }
        public static string POPUP_VALIDITY_DOWN_ARROW { get { return BASE + "validity_popup_down_arrow.png"; } }
        public static string MUSIC_AND_VIDEO_TAG_ICON { get { return BASE + "music_tag.png"; } }
        public static string MEDIA_CLOUD_HASH_TAG_IMAGE { get { return BASE + "cloudHashTag_Icon.png"; } }
        public static string NEWSPORTAL_DEFAULT_IMAGE { get { return BASE + "NewsPortalDefaultImage.png"; } }

        public static string HASHTAG_ALL_IMAGE_ICON { get { return BASE + "hashTag_all.png"; } }
        public static string HASHTAG_ALBUM_IMAGE_ICON { get { return BASE + "hashTag_album.png"; } }
        public static string HASHTAG_IMAGE_ICON { get { return BASE + "hashTag_Icon.png"; } }
        public static string HASHTAG_SONG_IMAGE_ICON { get { return BASE + "hashTag_song.png"; } }
        public static string MEDIA_NEXT_SEARCH_DEATAILS { get { return BASE_BUTTONS + "media_next.png"; } }
        public static string MEDIA_NEXT_SEARCH_DEATAILS_H { get { return BASE_BUTTONS + "media_next_h.png"; } }

        public static string RECOVERY_OPTION_FACEBOOK { get { return BASE + "recovery_fb.jpg"; } }
        public static string RECOVERY_OPTION_TWITTER { get { return BASE + "recovery_twitter.jpg"; } }

        public static string CREATE_GROUP { get { return BASE + "create_group.png"; } }
        public static string GROUP_NAME_EDIT { get { return BASE + "group_name_edit.png"; } }
        public static string GROUP_NAME_EDIT_OK { get { return BASE + "group_name_edit_ok.png"; } }
        public static string GROUP_NAME_EDIT_OK_H { get { return BASE + "group_name_edit_ok_h.png"; } }
        public static string GROUP_NAME_EDIT_CANCEL { get { return BASE + "group_name_edit_cancel.png"; } }
        public static string GROUP_NAME_EDIT_CANCEL_H { get { return BASE + "group_name_edit_cancel_h.png"; } }

        public static string ADD_CHAT_BG { get { return BASE_CHAT + "add_chat_bg.png"; } }
        public static string ADD_CHAT_BG_H { get { return BASE_CHAT + "add_chat_bg_h.png"; } }
        public static string CHAT_BG_BACK { get { return BASE_CHAT + "chat_bg_back.png"; } }
        public static string CHAT_BG_BACK_H { get { return BASE_CHAT + "chat_bg_back_H.png"; } }
        public static string CHAT_BG_NEXT { get { return BASE_CHAT + "chat_bg_next.png"; } }
        public static string CHAT_BG_NEXT_H { get { return BASE_CHAT + "chat_bg_next_h.png"; } }
        public static string CHAT_BG_SELECT { get { return BASE_CHAT + "chat_bg_select.png"; } }
        public static string CHAT_BG_DESELECT { get { return BASE_CHAT + "chat_bg_deselect.png"; } }
        public static string CHAT_BG_DESELECT_H { get { return BASE_CHAT + "chat_bg_deselect_h.png"; } }
        public static string CHAT_BG_DOWNLOAD { get { return BASE_CHAT + "chat_bg_download.png"; } }
        public static string CHAT_BG_DOWNLOAD_H { get { return BASE_CHAT + "chat_bg_download_h.png"; } }
        public static string CHAT_FILE_UPLOAD { get { return BASE_CHAT + "chat_file_upload.png"; } }
        public static string FILE_TRANFER_CANCEL { get { return BASE_CHAT + "file_transfer_cancel.png"; } }
        public static string FILE_TRANFER_CANCEL_H { get { return BASE_CHAT + "file_transfer_cancel_h.png"; } }
        public static string GROUP_SETTINGS { get { return BASE_BUTTONS + "group_settings.png"; } }
        public static string GROUP_SETTINGS_H { get { return BASE_BUTTONS + "group_settings_h.png"; } }
        public static string CHAT_PLAY { get { return BASE_CHAT + "chat_play.png"; } }
        public static string CHAT_PLAY_H { get { return BASE_CHAT + "chat_play_h.png"; } }
        public static string CHAT_PAUSE { get { return BASE_CHAT + "chat_pause.png"; } }
        public static string CHAT_PAUSE_H { get { return BASE_CHAT + "chat_pause_h.png"; } }
        public static string CHAT_AUDIO { get { return BASE_CHAT + "chat_audio.png"; } }
        public static string DEFAULT_RING_MEDIA_IMAGE { get { return BASE_CHAT + "default_ring_media_image.jpg"; } }

        public static string FEED_1_IMAGE { get { return BASE_MENU + "feed_1.png"; } }
        public static string FEED_1_IMAGE_H { get { return BASE_MENU + "feed_1_h.png"; } }
        public static string FEED_2_IMAGE { get { return BASE_MENU + "feed_2.png"; } }
        public static string FEED_2_IMAGE_H { get { return BASE_MENU + "feed_2_h.png"; } }
        public static string FEED_3_IMAGE { get { return BASE_MENU + "feed_3.png"; } }
        public static string FEED_3_IMAGE_H { get { return BASE_MENU + "feed_3_h.png"; } }

        public static string BACK_IMAGE_ICON { get { return BASE_BUTTONS + "back_icon.png"; } }
        public static string BACK_IMAGE_ICON_H { get { return BASE_BUTTONS + "back_icon_h.png"; } }
        public static string BACK_IMAGE_ICON_P { get { return BASE_BUTTONS + "back_icon_p.png"; } }

        public static string HDOT { get { return BASE_BUTTONS + "HDot.png"; } }
        public static string HDOT_H { get { return BASE_BUTTONS + "HDot_h.png"; } }
        public static string VDOT { get { return BASE_BUTTONS + "VDot.png"; } }
        public static string VDOT_H { get { return BASE_BUTTONS + "VDot_h.png"; } }
        public static string HDOT_SELECTED { get { return BASE_BUTTONS + "HDot_selected.png"; } }
        public static string VDOT_SELECTED { get { return BASE_BUTTONS + "VDot_selected.png"; } }
        public static string UP_LOADING_ICON { get { return BASE_BUTTONS + "Up_loading.png"; } }
        public static string UP_LOADING_ICON_H { get { return BASE_BUTTONS + "Up_loading_h.png"; } }

        public static string UP_ARROW { get { return BASE + "up.png"; } }
        public static string UP_ARROW_H { get { return BASE + "up_h.png"; } }

        public static string UPDATE_VERSION_ICON { get { return BASE + "update.png"; } }
        public static string PHOTO_LOADING { get { return BASE + "photo_loading.png"; } }
        public static string PHOTO_ALBUM_COVER_IMAGE { get { return BASE + "photo_album_default.png"; } }

        public static string UPLOAD { get { return BASE + "upload.png"; } }
        public static string UPLOAD_H { get { return BASE + "upload_h.png"; } }
        public static string UPLOAD_POPUP_UP_ARROW { get { return BASE + "upload_popup_up_arrow.png"; } }
        public static string DOWNLOAD { get { return BASE + "download.png"; } }
        public static string BUTTON_HD
        { get { return BASE_BUTTONS + "hd.png"; } }

        public static string BUTTON_HD_H
        { get { return BASE_BUTTONS + "hd_h.png"; } }

        #region GIF
        public static string LOADER_CLOCK { get { return BASE_LOADERS + "waiting.gif"; } }
        public static string LOADER_PRGOGRESS
        {
            get
            {
                return BASE_LOADERS + "calling.gif";
            }
        }
        public static string LOADER_SMALL { get { return BASE_LOADERS + "Loading_30x30.gif"; } }
        public static string LOADER_MEDIUM { get { return BASE_LOADERS + "Loading_40x40.gif"; } }
        public static string LOADER_100 { get { return BASE_LOADERS + "Loading_100.gif"; } }
        public static string LOADING_RINGID_FEED { get { return BASE_LOADERS + "LOADING_RINGID_FEED.gif"; } }
        public static string SPLASH_SCREEN_LOADER { get { return BASE_LOADERS + "splash_screen.gif"; } }
        public static string TYPING { get { return BASE_LOADERS + "typing.gif"; } }
        public static string LOADER_LIKE_ANIMATION { get { return BASE_LOADERS + "like_animation.gif"; } }
        public static string LOADER_FEED { get { return BASE_LOADERS + "loader.gif"; } }
        public static string LOADER_FEED_CYCLE { get { return BASE_LOADERS + "loader_cycle.gif"; } }
        public static string LOADER_FEED_CYCLE_MINI { get { return BASE_LOADERS + "loader_cycle_30.gif"; } }
        public static string LOADER_CYCLE_MINI_BG_BLACK { get { return BASE_LOADERS + "loader_cycle_blackBG.gif"; } }//loader_cycle_blackBG
        public static string LOADER_CHAT_HISTORY { get { return BASE_LOADERS + "loader_chat_history.gif"; } }
        public static string LOADER_PLAYER_BUFFER { get { return BASE_LOADERS + "player_buffer.gif"; } }
        public static string LOADER_PLAYER_MUSIC { get { return BASE_LOADERS + "player_music.gif"; } }
        public static string LOADER_SIGN_IN { get { return BASE_LOADERS + "signIn_loader.gif"; } }
        public static string LOADER_DOWNLOAD_SMALL { get { return BASE_LOADERS + "download_loader.gif"; } }
        public static string MORE_LOADER { get { return BASE_LOADERS + "more_loader.gif"; } }
        public static string OUTGOING_ANIMATION { get { return BASE_LOADERS + "outgoingCallAnim.gif"; } }//outgoingCallAnim.gif/calling.gif
        public static string LOADER_OFFLINE { get { return BASE_LOADERS + "offline_loader.gif"; } }
        public static string UPLOAD_DOWNLOAD { get { return BASE + "upload_download.gif"; } }
        public static string UPLOADING { get { return BASE + "uploading.gif"; } }
        public static string DOWNLOADING { get { return BASE + "downloading.gif"; } }
        #endregion
        #region "Full Image view"
        public static string BUTTON_BACK_IMAGE
        {
            get
            {
                return BASE_BUTTONS + "back.png";
            }
        }
        public static string BUTTON_BACK_IMAGE_H
        {
            get
            {
                return BASE_BUTTONS + "back_h.png";
            }
        }
        public static string BUTTON_BACK_IMAGE_P
        {
            get
            {
                return BASE_BUTTONS + "back_p.png";
            }
        }
        public static string BUTTON_NEXT_IMAGE
        {
            get
            {
                return BASE_BUTTONS + "next.png";
            }
        }
        public static string BUTTON_NEXT_IMAGE_H
        {
            get
            {
                return BASE_BUTTONS + "next_h.png";
            }
        }
        public static string BUTTON_NEXT_IMAGE_P
        {
            get
            {
                return BASE_BUTTONS + "next_p.png";
            }
        }
        #endregion

        //wallet
        public static string WALLET { get { return BASE + "wallet.png"; } }
        public static string WALLET_H { get { return BASE + "wallet-h.png"; } }
        public static string WALLET_SELECTED { get { return BASE + "wallet-selected.png"; } }
        public static string GOLD { get { return BASE + "gold.png"; } }
        public static string SILVER { get { return BASE + "silver.png"; } }
        public static string BRONZE { get { return BASE + "bronze.png"; } }
        public static string COIN_EXCHANGE { get { return BASE_BUTTONS + "coin_exchange.png"; } }
        public static string COIN_EXCHANGE_H { get { return BASE_BUTTONS + "coin_exchange_h.png"; } }
        public static string WALLET_COIN { get { return BASE + "wallet_coin.png"; } }
        public static string WALLET_DOUBLE_COIN { get { return BASE + "wallet_double_coin.png"; } }
        public static string WALLET_CHECK_IN { get { return BASE + "wallet_check_in.png"; } }
        public static string WALLET_CHECK_IN_GIFT { get { return BASE + "wallet_check_in_gift.png"; } }
        public static string WALLET_CHECK_IN_SUCCESS { get { return BASE + "wallet_check_in_success.png"; } }
        public static string WALLET_INVITE { get { return BASE + "wallet_invite.png"; } }
        public static string WALLET_INVITE_H { get { return BASE + "wallet_invite_h.png"; } }
        public static string WALLET_MORE_SHARE { get { return BASE + "wallet_more_share.png"; } }
        public static string WALLET_MORE_SHARE_H { get { return BASE + "wallet_more_share_h.png"; } }
        public static string WALLET_ALREADY_CHECKED_IN { get { return BASE + "wallet_already_checked_in_l.png"; } }
        public static string WALLET_MOBILE_BANKING { get { return BASE + "wallet_mobile_banking.png"; } }
        public static string WALLET_INTERNET_BANKING { get { return BASE + "wallet_internet_banking.png"; } }
        public static string WALLET_BANK_CARDS { get { return BASE + "wallet_bank_cards.png"; } }
        public static string WALLET_RIGHT_ARROW { get { return BASE + "wallet_arrow_right_35x35.png"; } }
        public static string WALLET_SSL { get { return BASE + "wallet_ssl.jpg"; } }
        public static string WALLET_PAYPAL { get { return BASE + "wallet_paypal.jpg"; } }

        // Stream
        public static string COIN_COUNT { get { return BASE_STREAM + "coin_count.png"; } }
        public static string COIN_COUNT_H { get { return BASE_STREAM + "coin_count_h.png"; } }
        public static string COIN_RIGHT_ARROW { get { return BASE_STREAM + "coin_right_arrow.png"; } }
        public static string COIN_RIGHT_ARROW_H { get { return BASE_STREAM + "coin_right_arrow_h.png"; } }
        public static string COIN_SMALL { get { return BASE_STREAM + "coin_small.png"; } }
        public static string COIN_LARGE { get { return BASE_STREAM + "coin_large.png"; } }

        public static string EYE { get { return BASE_STREAM + "eye.png"; } }
        public static string EYE_H { get { return BASE_STREAM + "eye_h.png"; } }
        public static string GIFT { get { return BASE_STREAM + "gift.png"; } }
        public static string GIFT_H { get { return BASE_STREAM + "gift_h.png"; } }
        public static string GO_LIVE { get { return BASE_STREAM + "go_live.png"; } }
        public static string GO_LIVE_H { get { return BASE_STREAM + "go_live_h.png"; } }
        public static string GO_MY_CHANNEL { get { return BASE_STREAM + "go_my_channel.png"; } }
        public static string GO_MY_CHANNEL_H { get { return BASE_STREAM + "go_my_channel_h.png"; } }
        public static string SEND { get { return BASE_STREAM + "send.png"; } }
        public static string SEND_H { get { return BASE_STREAM + "send_h.png"; } }
        public static string SELECT_CAMERA { get { return BASE_STREAM + "select_camera.png"; } }
        public static string SELECT_CAMERA_H { get { return BASE_STREAM + "select_camera_h.png"; } }
        public static string SELECT_MONITOR { get { return BASE_STREAM + "select_monitor.png"; } }
        public static string SELECT_MONITOR_H { get { return BASE_STREAM + "select_monitor_h.png"; } }
        public static string SELECT_APP { get { return BASE_STREAM + "select_app.png"; } }
        public static string SELECT_APP_H { get { return BASE_STREAM + "select_app_h.png"; } }
        public static string SOURCE_CAMERA_WHITE { get { return BASE_STREAM + "source_camera_white.png"; } }
        public static string SOURCE_CAMERA_WHITE_H { get { return BASE_STREAM + "source_camera_white_h.png"; } }
        public static string SOURCE_MONITOR_WHITE { get { return BASE_STREAM + "source_monitor_white.png"; } }
        public static string SOURCE_MONITOR_WHITE_H { get { return BASE_STREAM + "source_monitor_white_h.png"; } }
        public static string SOURCE_APP_WHITE { get { return BASE_STREAM + "source_app_white.png"; } }
        public static string SOURCE_APP_WHITE_H { get { return BASE_STREAM + "source_app_white_h.png"; } }
        public static string SOURCE_CAMERA_ORANGE { get { return BASE_STREAM + "source_camera_orange.png"; } }
        public static string SOURCE_CAMERA_ORANGE_H { get { return BASE_STREAM + "source_camera_orange_h.png"; } }
        public static string SOURCE_MONITOR_ORANGE { get { return BASE_STREAM + "source_monitor_orange.png"; } }
        public static string SOURCE_MONITOR_ORANGE_H { get { return BASE_STREAM + "source_monitor_orange_h.png"; } }
        public static string SOURCE_APP_ORANGE { get { return BASE_STREAM + "source_app_orange.png"; } }
        public static string SOURCE_APP_ORANGE_H { get { return BASE_STREAM + "source_app_orange_h.png"; } }
        public static string STREAM_CANCEL_WHITE { get { return BASE_STREAM + "stream_cancel_white.png"; } }
        public static string STREAM_CANCEL_WHITE_H { get { return BASE_STREAM + "stream_cancel_white_h.png"; } }
        public static string STREAM_SHARE_WHITE { get { return BASE_STREAM + "stream_share_white.png"; } }
        public static string STREAM_SHARE_WHITE_H { get { return BASE_STREAM + "stream_share_white_h.png"; } }
        public static string STREAM_LOCATION_WHITE { get { return BASE_STREAM + "stream_location_white.png"; } }
        public static string STREAM_LOCATION_WHITE_H { get { return BASE_STREAM + "stream_location_white_h.png"; } }
        public static string STREAM_MIC_WHITE { get { return BASE_STREAM + "stream_mic_white.png"; } }
        public static string STREAM_MIC_WHITE_H { get { return BASE_STREAM + "stream_mic_white_h.png"; } }
        public static string TIME { get { return BASE_STREAM + "time.png"; } }
        public static string TIME_H { get { return BASE_STREAM + "time_h.png"; } }
        public static string TWITTER_WHITE { get { return BASE_STREAM + "twitter_white.png"; } }
        public static string TWITTER_WHITE_H { get { return BASE_STREAM + "twitter_white_h.png"; } }
        public static string FACEBOOK_WHITE { get { return BASE_STREAM + "facebook_white.png"; } }
        public static string FACEBOOK_WHITE_H { get { return BASE_STREAM + "facebook_white_h.png"; } }
        public static string STREAM_CURSOR { get { return BASE_STREAM + "stream_cursor.ico"; } }
        public static string STREAM_CATEGORY_DOWN { get { return BASE_STREAM + "category_arrow_down.png"; } }
        public static string DEFAULT_APP_ICON { get { return BASE_STREAM + "default_app_icon.png"; } }
        public static string STREAM_CATEGORY_SELECT { get { return BASE_STREAM + "stream_category_select.png"; } }
        public static string STREAM_CHAT_OFF { get { return BASE_STREAM + "stream_chat_off.png"; } }
        public static string STREAM_CHAT_ON { get { return BASE_STREAM + "stream_chat_on.png"; } }
        public static string STREAM_FULL_VIEW { get { return BASE_STREAM + "stream_full_view.png"; } }
        public static string STREAM_FULL_VIEW_H { get { return BASE_STREAM + "stream_full_view_h.png"; } }
        public static string STREAM_CHANNEL_DISCOVER { get { return BASE_STREAM + "stream_channel_discover.png"; } }
        public static string STREAM_CHANNEL_DISCOVER_H { get { return BASE_STREAM + "stream_channel_discover_h.png"; } }
        public static string STREAM_CHANNEL_RELOAD { get { return BASE_STREAM + "stream_channel_reload.png"; } }
        public static string STREAM_CHANNEL_RELOAD_H { get { return BASE_STREAM + "stream_channel_reload_h.png"; } }
        public static string CHANNEL_INFO { get { return BASE_STREAM + "channel_info.png"; } }
        public static string CHANNEL_INFO_H { get { return BASE_STREAM + "channel_info_h.png"; } }
        public static string CHANNEL_INFO_SMALL { get { return BASE_STREAM + "channel_info_small.png"; } }
        public static string CHANNEL_INFO_SMALL_H { get { return BASE_STREAM + "channel_info_small_h.png"; } }
        public static string STREAMING_LOAD_ICON { get { return BASE_STREAM + "streaming_load_icon.png"; } }
        public static string CHANNEL_SETTINGS_HD { get { return BASE_STREAM + "channel_settings_hd.png"; } }
        public static string CHANNEL_SETTINGS_HD_H { get { return BASE_STREAM + "channel_settings_hd_h.png"; } }
        public static string CHANNEL_SETTINGS_SD { get { return BASE_STREAM + "channel_settings_sd.png"; } }
        public static string CHANNEL_SETTINGS_SD_H { get { return BASE_STREAM + "channel_settings_sd_h.png"; } }
        public static string STREAM_FOLLOWING { get { return BASE_STREAM + "stream_following.png"; } }
        public static string STREAM_FOLLOWING_H { get { return BASE_STREAM + "stream_following_h.png"; } }
        public static string RECENT_LIVE { get { return BASE_STREAM + "recent_live.png"; } }

        public static string CHANNEL_PROFILE_UNKNOWN { get { return BASE_CHANNEL + "channel_pro_image_unknown.png"; } }
        public static string CHANNEL_PROFILE_IMAGE_NORMAL { get { return BASE_CHANNEL + "channel_pro_Image_normal.png"; } }
        public static string CHANNEL_COVER_IMAGE_H { get { return BASE_CHANNEL + "cover_photo_upload_h.png"; } }
        public static string CHANNEL_PROFILE_IMAGE_CROSS { get { return BASE_CHANNEL + "channel_pro_img_cancel.png"; } }
        public static string CHANNEL_PROFILE_IMAGE_CROSS_H { get { return BASE_CHANNEL + "channel_pro_image_cancel_h.png"; } }

        public static string TEMP_DOLLAR { get { return BASE_STREAM + "dollar.png"; } }

        public static string CONFIRMATIONS_CROSS { get { return BASE_CONFIRMATIONS + "cross.png"; } }
        public static string CONFIRMATIONS_INFO { get { return BASE_CONFIRMATIONS + "info.png"; } }
        public static string CONFIRMATIONS_QUESTION { get { return BASE_CONFIRMATIONS + "question.png"; } }
        public static string CONFIRMATIONS_WARNING { get { return BASE_CONFIRMATIONS + "warning.png"; } }
        //window buttons
        public static string WINDOW_BACK_TO_MAIN { get { return BASE_WINDOW + "back_to_main.png"; } }
        public static string WINDOW_BACK_TO_MAIN_H { get { return BASE_WINDOW + "back_to_main_h.png"; } }
        public static string WINDOW_BACK_TO_MAIN_P { get { return BASE_WINDOW + "back_to_main_p.png"; } }
        public static string WINDOW_EXIT_FULL_SCREEN { get { return BASE_WINDOW + "exit_full_screen.png"; } }
        public static string WINDOW_EXIT_FULL_SCREEN_H { get { return BASE_WINDOW + "exit_full_screen_h.png"; } }
        public static string WINDOW_EXIT_FULL_SCREEN_P { get { return BASE_WINDOW + "exit_full_screen_p.png"; } }
        public static string WINDOW_FULL_SCREEN { get { return BASE_WINDOW + "full_screen.png"; } }
        public static string WINDOW_FULL_SCREEN_H { get { return BASE_WINDOW + "full_screen_h.png"; } }
        public static string WINDOW_FULL_SCREEN_P { get { return BASE_WINDOW + "full_screen_p.png"; } }
        public static string SHOW_NEW_WINDOW { get { return BASE_WINDOW + "new_window.png"; } }
        public static string SHOW_NEW_WINDOW_H { get { return BASE_WINDOW + "new_window_h.png"; } }
        public static string SHOW_NEW_WINDOW_P { get { return BASE_WINDOW + "new_window_p.png"; } }
    }
}
=======
﻿
namespace View.Constants
{
    public class ImageLocation
    {
        private static string BASE = "/Resources/Images/";
        private static string BASE_MENU = "/Resources/Images/menu/";
        private static string BASE_LOADERS = "/Resources/Images/loaders/";
        private static string BASE_BUTTONS = "/Resources/Images/Buttons/";
        private static string BASE_STATUS = "/Resources/Images/Status/";
        private static string BASE_SETTINGS = "/Resources/Images/Settings/";
        private static string BASE_CALL = "/Resources/Images/Call/";
        private static string BASE_CHAT = "/Resources/Images/Chat/";
        private static string BASE_CONTEXT = "/Resources/Images/Context_Menu/";
        public static string BASE_COUNTRY = "/Resources/Images/Countries/";
        public static string BASE_STREAM = "/Resources/Images/Stream/";
        public static string BASE_CHANNEL = "/Resources/Images/Channel/";
        public static string BASE_CONFIRMATIONS = "/Resources/Images/Confirmations/";
        private static string BASE_WINDOW = "/Resources/Images/window/";
        public static string APP_ICON { get { return BASE + "ring_icon.ico"; } }//icon.ico/ringID.ico

        public static string OFFLINE_TASKBAR_ICON { get { return BASE + "offlineTaskOverlay.png"; } }//offnet.png
        public static string DEFAULT_MY_PROFILE_BG_IMAGE { get { return BASE + "default_myprofile_bg_image.jpg"; } }
        public static string MY_PROFILE_BG_IMAGE_DEFAULT { get { return BASE + "my_profile_bg_image_default.png"; } }
        public static string MY_PROFILE_BG_IMAGE_PRESSED { get { return BASE + "my_profile_bg_image_pressed.png"; } }
        public static string DEFAULT_MY_PROFILE_BG_IMAGE_H { get { return BASE + "default_myprofile_bg_image_h.jpg"; } }
        public static string LOGIN_BG { get { return BASE + "sign_in_img.jpg"; } }
        public static string WELCOME_BG_IMAGE { get { return BASE + "welcome.jpg"; } }
        public static string WELCOME_LOGO_IMAGE { get { return BASE + "Logo.png"; } }

        public static string LOGIN_DEFAULT_IMAGE_1 { get { return BASE + "default_login_image_1.jpg"; } }
        public static string LOGIN_DEFAULT_IMAGE_2 { get { return BASE + "default_login_image_2.jpg"; } }
        public static string LOGIN_DEFAULT_IMAGE_3 { get { return BASE + "default_login_image_3.jpg"; } }

        public static string PRIVACY_NEW_STATUS_ONLY_ME { get { return BASE_BUTTONS + "privacy_only_me.png"; } }
        public static string PRIVACY_NEW_STATUS_FRIENDS { get { return BASE_BUTTONS + "privacy_friends.png"; } }
        public static string PRIVACY_NEW_STATUS_PUBLIC { get { return BASE_BUTTONS + "privacy_public.png"; } }
        public static string CUSTOM_PRIVACY { get { return BASE_BUTTONS + "custom_privacy.png"; } }
        public static string CUSTOM_PRIVACY_H { get { return BASE_BUTTONS + "custom_privacy_h.png"; } }

        public static string SIGNIN_SIGNUP_WITH_EMAIL_IMAGE { get { return BASE + "with-email.png"; } }
        public static string SIGNIN_SIGNUP_WITH_FACEBOOK_IMAGE { get { return BASE + "with_fb.png"; } }
        public static string SIGNIN_SIGNUP_WITH_ID_IMAGE { get { return BASE + "with_id.png"; } }
        public static string SIGNIN_SIGNUP_WITH_PHONE_IMAGE { get { return BASE + "with_phone.png"; } }
        public static string SIGNIN_SIGNUP_WITH_TWITTER_IMAGE { get { return BASE + "with_twitter.png"; } }
        public static string SIGNIN_SIGNUP_BACK_BUTTON_IMAGE { get { return BASE_BUTTONS + "signUp_back.png"; } }
        public static string SIGNIN_SIGNUP_BACK_BUTTON_IMAGE_H { get { return BASE_BUTTONS + "signUp_back_h.png"; } }
        public static string SIGNIN_SIGNUP_BACK_BUTTON_IMAGE_P { get { return BASE_BUTTONS + "signUp_back_p.png"; } }
        public static string SIGNIN_SIGNUP_ARROW_IMAGE { get { return BASE + "signUp_arrow.png"; } }

        public static string NO_IMAGE_FOUND { get { return BASE + "no_image_found.jpg"; } }
        public static string DEFAULT_COVER_IMAGE { get { return BASE + "default_cover_image.jpg"; } }
        public static string DEFAULT_CHAT_BG { get { return BASE + "default_chat_bg.png"; } }
        public static string UNKNOWN_GROUP_IMAGE { get { return BASE + "unknown_group_imge.png"; } }
        public static string UNKNOWN_GROUP_LARGE_IMAGE { get { return BASE + "unknown_group_large_imge.png"; } }
        public static string UNKNOWN_ROOM_IMAGE { get { return BASE + "unknown_room_image.png"; } }
        public static string UNKNOWN_IMAGE_FOR_MUTUAL_FRIEND { get { return BASE + "unknown_user_mutual.png"; } }
        public static string GROUP_TOP { get { return BASE_MENU + "group.png"; } }
        public static string GROUP_TOP_H { get { return BASE_MENU + "group_h.png"; } }
        public static string GROUP_TOP_SELECTED { get { return BASE_MENU + "group_selected.png"; } }

        public static string RELOAD { get { return BASE_BUTTONS + "reload_icon.png"; } }
        public static string RELOAD_H { get { return BASE_BUTTONS + "reload_icon_h.png"; } }
        //public static string CIRCLE_TOP { get { return BASE_MENU + "circle.png"; } }
        //public static string CIRCLE_TOP_H { get { return BASE_MENU + "circle_h.png"; } }
        //public static string CIRCLE_TOP_SELECTED { get { return BASE_MENU + "circle_selected.png"; } }
        public static string UNKNOWN_70 { get { return BASE_MENU + "unknown_70.png"; } }
        public static string UNKNOWN_25 { get { return BASE_MENU + "unknown_25.png"; } }
        public static string POPUP_SIGN_IN { get { return BASE + "sign_in_popup.png"; } }
        public static string POPUP_MUTUAL_FRIENDS { get { return BASE + "like_list.png"; } }
        public static string POPUP_ABOUT_RINGID { get { return BASE + "about_ringid.png"; } }
        public static string POPUP_EDIT_DELETE { get { return BASE + "popup_edit_delete_photo.png"; } }
        public static string POPUP_THREE_ITEMS { get { return BASE + "popup_three_items.png"; } }
        public static string POPUP_DELETE { get { return BASE + "popup_delete_photo.png"; } }
        public static string POPUP_SHARE { get { return BASE + "share_popup.png"; } }
        public static string BOOK_ARROW { get { return BASE + "book_arrow.png"; } }

        public static string GROUP_DELETE { get { return BASE + "delete.png"; } }
        public static string GROUP_LEAVE { get { return BASE + "leave.png"; } }
        public static string GROUP_JOIN { get { return BASE + "join.png"; } }
        public static string GROUP_INFO { get { return BASE + "group_info.png"; } }
        public static string BLOCK_USER { get { return BASE + "block.png"; } }
        public static string CHANGE_ACCESS { get { return BASE + "change_access.png"; } }
        public static string INFO_ICON { get { return BASE + "info_icon.png"; } }
        public static string WORK_ICON { get { return BASE + "work_icon.png"; } }
        public static string EDUCATION_ICON { get { return BASE + "education_icon.png"; } }
        public static string SKILL_ICON { get { return BASE + "skill_icon.png"; } }
        public static string ADD_MORE { get { return BASE + "add_more_photo.png"; } }
        public static string ADD_MORE_H { get { return BASE + "add_more_photo_h.png"; } }
        public static string DEFAULT_ID_ICON { get { return BASE + "ringID.jpg"; } }
        public static string FRIEND_BLOCK_ICON { get { return BASE + "block_h.png"; } }
        public static string SEQUENCE_NUMBER { get { return BASE + "sequence_number.jpg"; } }
        public static string POPUP_UPLOAD_PHOTO { get { return BASE + "popup_upload_photo.png"; } }
        public static string LOCATION_USER { get { return BASE + "location.png"; } }
        public static string POPUP_UPLOAD_VIDEO { get { return BASE + "popup_upload_video.png"; } }
        public static string AUDIO_ALBUM_LIST_ICON { get { return BASE + "audio_album_list.png"; } }
        public static string VIDEO_ALBUM_LIST_ICON { get { return BASE + "video_album_list.png"; } }
        public static string DOWNLOAD_LIST_ICON { get { return BASE + "download_list.png"; } }

        public static string ADD_FRIEND_FACEBOOK { get { return BASE + "fb.png"; } }
        public static string ADD_FRIEND_FACEBOOK_H { get { return BASE + "fb_h.png"; } }
        public static string ADD_FRIEND_TWITTER { get { return BASE + "tt.png"; } }
        public static string ADD_FRIEND_TWITTER_H { get { return BASE + "tt_h.png"; } }
        public static string ADD_FRIEND_GOOGLE_PLUS { get { return BASE + "g+.png"; } }
        public static string ADD_FRIEND_GOOGLE_PLUS_H { get { return BASE + "g+_h.png"; } }
        public static string ADD_FRIEND_EMAIL { get { return BASE + "email.png"; } }
        public static string ADD_FRIEND_EMAIL_H { get { return BASE + "email_h.png"; } }
        public static string ADD_FRIEND_RING { get { return BASE + "invite_background.png"; } }
        //public static string FEED { get { return BASE_MENU + "feed.png"; } }
        //public static string FEED_H { get { return BASE_MENU + "feed_h.png"; } }
        //public static string FEED_SELECTED { get { return BASE_MENU + "feed_selected.png"; } }
        //public static string LIVE_AND_CHANNEL { get { return BASE_MENU + "home.png"; } }
        //public static string LIVE_AND_CHANNEL_H { get { return BASE_MENU + "home_h.png"; } }
        //public static string LIVE_AND_CHANNEL_SELECTED { get { return BASE_MENU + "home_selected.png"; } }
        public static string FEED { get { return BASE_MENU + "home.png"; } }
        public static string FEED_H { get { return BASE_MENU + "home_h.png"; } }
        public static string FEED_SELECTED { get { return BASE_MENU + "home_selected.png"; } }
        public static string LIVE_AND_CHANNEL { get { return BASE_MENU + "live.png"; } }
        public static string LIVE_AND_CHANNEL_H { get { return BASE_MENU + "live_h.png"; } }
        public static string LIVE_AND_CHANNEL_SELECTED { get { return BASE_MENU + "live_selected.png"; } }
        public static string MY_CHANNEL { get { return BASE_MENU + "my_channel.png"; } }
        public static string MY_CHANNEL_H { get { return BASE_MENU + "my_channel_h.png"; } }
        public static string MY_CHANNEL_SELECTED { get { return BASE_MENU + "my_channel_selected.png"; } }
        public static string NEWS_PORTAL { get { return BASE_MENU + "news.png"; } }
        public static string NEWS_PORTAL_H { get { return BASE_MENU + "news_h.png"; } }
        public static string NEWS_PORTAL_SELECTED { get { return BASE_MENU + "news_selected.png"; } }
        public static string PAGES { get { return BASE_MENU + "page.png"; } }
        public static string PAGES_H { get { return BASE_MENU + "page_h.png"; } }
        public static string PAGES_SELECTED { get { return BASE_MENU + "page_selected.png"; } }
        public static string FEED_DETAILS_BACK { get { return BASE + "back.png"; } }
        public static string FEED_DETAILS_BACK_P { get { return BASE + "back_p.png"; } }
        public static string SAVED_FEED { get { return BASE_MENU + "saved.png"; } }
        public static string SAVED_FEED_H { get { return BASE_MENU + "saved_h.png"; } }
        public static string SAVED_FEED_SELECTED { get { return BASE_MENU + "saved_selected.png"; } }
        public static string CELEBRITY { get { return BASE_MENU + "celebrity.png"; } }
        public static string CELEBRITY_H { get { return BASE_MENU + "celebrity_h.png"; } }
        public static string CELEBRITY_SELECTED { get { return BASE_MENU + "celebrity_selected.png"; } }
        public static string FOLLOWING_LIST { get { return BASE_MENU + "following.png"; } }
        public static string FOLLOWING_LIST_H { get { return BASE_MENU + "following_h.png"; } }
        public static string FOLLOWING_LIST_SELECTED { get { return BASE_MENU + "following_selected.png"; } }

        public static string DIALPAD { get { return BASE_MENU + "dialpad.png"; } }
        public static string DIALPAD_H { get { return BASE_MENU + "dialpad_h.png"; } }
        public static string DIALPAD_SELECTED { get { return BASE_MENU + "dialpad_selected.png"; } }
        public static string ROOM { get { return BASE_MENU + "room.png"; } }
        public static string ROOM_H { get { return BASE_MENU + "room_h.png"; } }
        public static string ROOM_SELECTED { get { return BASE_MENU + "room_selected.png"; } }

        public static string DIALPAD_BOTTOM { get { return BASE_MENU + "dialpad_bottom.png"; } }
        public static string DIALPAD_BOTTOM_H { get { return BASE_MENU + "dialpad_bottom_h.png"; } }
        public static string DIALPAD_BOTTOM_SELECTED { get { return BASE_MENU + "dialpad_bottom_selected.png"; } }

        /*****Dialpad Button******/
        public static string DIALPAD_SINGLE { get { return BASE_BUTTONS + "dial_single_button.png"; } }
        public static string DIALPAD_SINGLE_H { get { return BASE_BUTTONS + "dial_single_btton_h.png"; } }
        public static string DIALPAD_SINGLE_SELECTED { get { return BASE_BUTTONS + "dial_single_button_selected.png"; } }
        public static string CALL_UPPER { get { return BASE_BUTTONS + "call_upper.png"; } }
        public static string CALL_UPPER_H { get { return BASE_BUTTONS + "call_upper_h.png"; } }
        public static string CALL_UPPER_SELECTED { get { return BASE_BUTTONS + "call_upper_selected.png"; } }
        public static string CALL_BOTTOM { get { return BASE_BUTTONS + "call_bottom.png"; } }
        public static string CALL_BOTTOM_H { get { return BASE_BUTTONS + "call_bottom_h.png"; } }
        public static string CALL_BOTTOM_INACTIVE { get { return BASE_BUTTONS + "call_inactive.png"; } }
        public static string DIALPAD_CROSS_BUTTON { get { return BASE_BUTTONS + "Cross_normal.png"; } }
        public static string DIALPAD_CROSS_BUTTON_SELECTED { get { return BASE_BUTTONS + "Cross_Selected.png"; } }
        public static string DIALPAD_NEW_CROSS_BUTTON { get { return BASE_BUTTONS + "dial_cross.png"; } }
        public static string DIALPAD_NEW_CROSS_BUTTON_SELECTED { get { return BASE_BUTTONS + "dial_cross_selected.png"; } }
        public static string DIAL_PAD_SEARCH { get { return BASE_BUTTONS + "dialpad_search_icon.png"; } }
        public static string MUSIC_VIDEO_SEARCH { get { return BASE_BUTTONS + "dialpad_search.png"; } }
        public static string DIALPAD_DOWN_BUTTON { get { return BASE_BUTTONS + "dialpad_down.png"; } }
        public static string DIALPAD_DOWN_BUTTON_H { get { return BASE_BUTTONS + "dialpad_down_h.png"; } }
        public static string DIALPAD_USER_BUTTON { get { return BASE_BUTTONS + "dialpad_user.png"; } }
        public static string DIALPAD_USER_BUTTON_H { get { return BASE_BUTTONS + "dialpad_user_select.png"; } }

        /*****Dialpad Button******/


        public static string ALL_NOTIFICATION { get { return BASE_MENU + "all_notification_icon.png"; } }
        public static string ALL_NOTIFICATION_H { get { return BASE_MENU + "all_notification_icon_h.png"; } }
        public static string ALL_NOTIFICATION_SELECTED { get { return BASE_MENU + "all_notification_icon_selected.png"; } }
        public static string FRIEND_ICON { get { return BASE_MENU + "friends_icon.png"; } }
        public static string FRIEND_ICON_H { get { return BASE_MENU + "friends_icon_h.png"; } }
        public static string FRIEND_ICON_SELECTED { get { return BASE_MENU + "friends_icon_selected.png"; } }

        public static string MEDIA_CLOUD { get { return BASE_MENU + "media_cloud.png"; } }
        public static string MEDIA_CLOUD_H { get { return BASE_MENU + "media_cloud_h.png"; } }
        public static string MEDIA_CLOUD_SELECTED { get { return BASE_MENU + "media_cloud_selected.png"; } }
        public static string MEDIA_FEED { get { return BASE_MENU + "media_feed.png"; } }
        public static string MEDIA_FEED_H { get { return BASE_MENU + "media_feed_h.png"; } }
        public static string MEDIA_FEED_SELECTED { get { return BASE_MENU + "media_feed_selected.png"; } }

        public static string CIRCLE_ICON { get { return BASE_MENU + "circle.png"; } }
        public static string CIRCLE_ICON_H { get { return BASE_MENU + "circle_h.png"; } }
        public static string CIRCLE_ICON_SELECTED { get { return BASE_MENU + "circle_selected.png"; } }
        public static string STICKER_MARKET { get { return BASE_MENU + "sticker_market.png"; } }
        public static string STICKER_MARKET_H { get { return BASE_MENU + "sticker_market_h.png"; } }
        public static string STICKER_MARKET_SELECTED { get { return BASE_MENU + "sticker_market_selected.png"; } }
        public static string STICKER_DOWNLOAD_BUTTON { get { return BASE_BUTTONS + "sticker_download_btn.png"; } }
        public static string STICKER_DOWNLOAD_BUTTON_H { get { return BASE_BUTTONS + "sticker_download_btn_h.png"; } }
        public static string STICKER_DOWNLOADING_BUTTON { get { return BASE_BUTTONS + "sticker_downloading_h_btn.png"; } }
        public static string MORE_STICKER_IN_COMMENTS { get { return BASE_CHAT + "more_sticker.png"; } }
        public static string MORE_STICKER_IN_COMMENTS_H { get { return BASE_CHAT + "more_sticker_hover.png"; } }

        public static string MSG_ICON { get { return BASE_MENU + "msg_icon.png"; } }
        public static string MSG_ICON_H { get { return BASE_MENU + "msg_icon_h.png"; } }
        public static string MSG_ICON_SELECTED { get { return BASE_MENU + "msg_icon_selected.png"; } }
        public static string CALLHISTORY_ICON { get { return BASE_MENU + "callhistory_icon.png"; } }
        public static string CALLHISTORY_ICON_H { get { return BASE_MENU + "callhistory_icon_h.png"; } }
        public static string CALLHISTORY_ICON_SELECTED { get { return BASE_MENU + "callhistory_icon_selected.png"; } }
        /*public static string ADD_FRIEND_STICKER { get { return BASE + "charecter.png"; } }
        public static string ADD_FRIEND_STICKER_H { get { return BASE + "charecter_h.png"; } }
        public static string ADD_FRIEND_STICKER_SELECTED { get { return BASE + "charecter_h2.png"; } }*/
        public static string ADD_FRIEND_STICKER { get { return BASE_MENU + "add_friend_avatar.png"; } }
        public static string ADD_FRIEND_STICKER_H { get { return BASE_MENU + "add_friend_avatar_h.png"; } }
        public static string ADD_FRIEND_STICKER_SELECTED { get { return BASE_MENU + "add_friend_avatar_selected.png"; } }

        public static string ADD_FRIEND_STICKER_HOME_H { get { return BASE + "charecter_h3.png"; } }
        public static string ADD_FRIEND { get { return BASE_BUTTONS + "add_friend.png"; } }
        public static string ADD_FRIEND_H { get { return BASE_BUTTONS + "add_friend_h.png"; } }
        public static string TAKE_PROFILE_PHOTO { get { return BASE_BUTTONS + "take_profile_photo.png"; } }
        public static string TAKE_PROFILE_PHOTO_H { get { return BASE_BUTTONS + "take_profile_photo_h.png"; } }
        public static string TAKE_PROFILE_PHOTO_SELECTED { get { return BASE_BUTTONS + "take_profile_photo_selected.png"; } }
        public static string UPLOAD_PROFILE_PHOTO { get { return BASE_BUTTONS + "upload_from_computer.png"; } }
        public static string UPLOAD_PROFILE_PHOTO_H { get { return BASE_BUTTONS + "upload_from_computer_h.png"; } }
        public static string UPLOAD_PROFILE_PHOTO_SELECTED { get { return BASE_BUTTONS + "upload_from_computer_selected.png"; } }
        public static string EDIT_PHOTO { get { return BASE_BUTTONS + "edit_profile_photo.png"; } }
        public static string EDIT_PHOTO_H { get { return BASE_BUTTONS + "edit_profile_photo_h.png"; } }
        public static string EDIT_PHOTO_SELECTED { get { return BASE_BUTTONS + "edit_profile_photo_selected.png"; } }

        public static string ADD_FRIEND_SEARCH { get { return BASE + "add_friend_search.png"; } }
        public static string ADD_FRIEND_SEARCH_H { get { return BASE + "add_friend_search_h.png"; } }
        public static string ADD_FRIEND_SEARCH_SELECTED { get { return BASE + "add_friend_search_selected.png"; } }
        public static string ADD_FRIEND_SUGGESTION { get { return BASE + "discover.png"; } }
        public static string ADD_FRIEND_SUGGESTION_H { get { return BASE + "discover_h.png"; } }
        public static string ADD_FRIEND_SUGGESTION_SELECTED { get { return BASE + "discover_selected.png"; } }
        public static string ADD_FRIEND_INVITE { get { return BASE + "invite.png"; } }
        public static string ADD_FRIEND_INVITE_H { get { return BASE + "invite_h.png"; } }
        public static string ADD_FRIEND_INVITE_SELECTED { get { return BASE + "invite_select.png"; } }
        public static string ADD_FRIEND_PENDING { get { return BASE + "pending.png"; } }
        public static string ADD_FRIEND_PENDING_H { get { return BASE + "pending_h.png"; } }
        public static string ADD_FRIEND_PENDING_SELECTED { get { return BASE + "pending_select.png"; } }

        public static string _ADD_FRIEND_BLOCK_LIST { get { return BASE + "block_add_friend.png"; } }
        public static string _ADD_FRIEND_BLOCK_LIST_H { get { return BASE + "block_add_friend_h.png"; } }
        public static string _ADD_FRIEND_BLOCK_LIST_SELECTED { get { return BASE + "block_add_friend_selected.png"; } }

        public static string ADD_FRIEND_ICON_CONTACT_LIST { get { return BASE + "add_friend.png"; } }
        public static string FRIEND_ICON_CONTACT_LIST { get { return BASE + "friend.png"; } }
        public static string PENDING_IN_FRIEND_ICON_CONTACT_LIST { get { return BASE + "panding_in.png"; } }
        public static string PENDING_OUT_FRIEND_ICON_CONTACT_LIST { get { return BASE + "panding_out.png"; } }

        public static string ADD_FRIEND_ICON_CONTACT_LIST_MEDIUM { get { return BASE + "add_friend_icon_medium.png"; } }
        public static string FRIEND_ICON_CONTACT_LIST_MEDIUM { get { return BASE + "friend_icon_medium.png"; } }
        public static string INCOMING_FRIEND_ICON_CONTACT_LIST_MEDIUM { get { return BASE + "incoming_icon_medium.png"; } }
        public static string OUTGOING_FRIEND_ICON_CONTACT_LIST_MEDIUM { get { return BASE + "outgoing_icon_medium.png"; } }

        #region Context Menu
        public static string ADD_FRIEND_ICON_CM { get { return BASE_CONTEXT + "add_icon.png"; } }
        public static string ADD_FRIEND_ICON_CM_H { get { return BASE_CONTEXT + "add_icon_h.png"; } }
        public static string BLOCK_USER_ICON_CM { get { return BASE_CONTEXT + "block_friend.png"; } }
        public static string BLOCK_USER_ICON_CM_H { get { return BASE_CONTEXT + "block_friend_h.png"; } }
        public static string CANCEL_REQUEST_ICON_CM { get { return BASE_CONTEXT + "cancel_rqst.png"; } }
        public static string CANCEL_REQUEST_ICON_CM_H { get { return BASE_CONTEXT + "cancel_rqst_h.png"; } }
        public static string CIRCLE_SMALL_ICON_CM { get { return BASE_CONTEXT + "circle_small.png"; } }
        public static string CIRCLE_SMALL_ICON_CM_H { get { return BASE_CONTEXT + "circle_small_h.png"; } }
        public static string FRIEND_ICON_CM { get { return BASE_CONTEXT + "friend_icon.png"; } }
        public static string FRIEND_ICON_CM_H { get { return BASE_CONTEXT + "friend_icon_h.png"; } }
        public static string REJECT_ICON_CM { get { return BASE_CONTEXT + "reject.png"; } }
        public static string REJECT_ICON_CM_H { get { return BASE_CONTEXT + "reject_h.png"; } }

        public static string GROUP_DELETE_CM { get { return BASE_CONTEXT + "delete.png"; } }
        public static string GROUP_DELETE_CM_H { get { return BASE_CONTEXT + "delete_h.png"; } }
        public static string EDIT_INFO_CM { get { return BASE_CONTEXT + "edit_info.png"; } }
        public static string EDIT_INFO_CM_H { get { return BASE_CONTEXT + "edit_info_h.png"; } }
        public static string GROUP_LEAVE_CM { get { return BASE_CONTEXT + "leave.png"; } }
        public static string GROUP_LEAVE_CM_H { get { return BASE_CONTEXT + "leave_h.png"; } }
        #endregion


        public static string ADD_ICON_CONTACT_LIST { get { return BASE + "add_icon.png"; } }
        public static string ADD_ICON_CONTACT_LIST_H { get { return BASE + "add_icon_h.png"; } }
        public static string INCOMING_FRIEND_ICON_CONTACT_LIST { get { return BASE + "incoming_icon.png"; } }
        public static string INCOMING_FRIEND_ICON_CONTACT_LIST_H { get { return BASE + "incoming_icon_h.png"; } }
        public static string OUTGOING_FRIEND_ICON_CONTACT_LIST { get { return BASE + "outgoing_icon.png"; } }
        public static string OUTGOING_FRIEND_ICON_CONTACT_LIST_H { get { return BASE + "outgoing_icon_h.png"; } }
        public static string FRIEND_CONTACT_LIST { get { return BASE + "friend_icon.png"; } }
        public static string FRIEND_CONTACT_LIST_H { get { return BASE + "friend_icon_h.png"; } }
        public static string FRIEND_CONTACT_ID { get { return BASE + "friend_id.png"; } }
        public static string REJECT_ICON_CONTACT_LIST { get { return BASE + "reject.png"; } }
        public static string REJECT_ICON_CONTACT_LIST_H { get { return BASE + "reject_h.png"; } }
        public static string CANCEL_REQUEST_ICON_CONTACT_LIST { get { return BASE + "cancel_rqst.png"; } }
        public static string CANCEL_REQUEST_ICON_CONTACT_LIST_H { get { return BASE + "cancel_rqst_h.png"; } }
        public static string REMOVE_ICON_PUMAYKNOW_LIST { get { return BASE + "remove.png"; } }
        public static string REMOVE_ICON_PUMAYKNOW_LIST_H { get { return BASE + "remove_h.png"; } }
        public static string VIEW_PREVIOUS_COMMENT_ICON { get { return BASE + "up_arrow.png"; } }
        public static string ADD_FRIEND_ICON_CIRCLE { get { return BASE + "add_friend_icon_circle.png"; } }
        public static string FRIEND_ICON_CIRCLE { get { return BASE + "friend_icon_circle.png"; } }
        public static string INCOMING_FRIEND_ICON_CIRCLE { get { return BASE + "incoming_icon_circle.png"; } }
        public static string OUTGOING_FRIEND_ICON_CIRCLE { get { return BASE + "outgoing_icon_circle.png"; } }
        public static string BLOCK_FRIEND_ICON { get { return BASE + "block_friend.png"; } }
        public static string BLOCK_FRIEND_ICON_H { get { return BASE + "block_friend_h.png"; } }
        public static string CIRCLE_SMALL_ICON { get { return BASE + "circle_small.png"; } }
        public static string CIRCLE_SMALL_ICON_H { get { return BASE + "circle_small_h.png"; } }
        public static string CHAI_ICON_FOR_NAME_TOOLTIP { get { return BASE + "chat_tooltip_icon.png"; } }

        public static string BUTTON_ZOOM_OUT { get { return BASE_BUTTONS + "zoom_out.png"; } }
        public static string BUTTON_ZOOM_IN { get { return BASE_BUTTONS + "zoom_in.png"; } }
        public static string BUTTON_ADD_FRIEND_INCOMING { get { return BASE_BUTTONS + "incoming_friend.png"; } }
        public static string BUTTON_ADD_FRIEND_INCOMING_H { get { return BASE_BUTTONS + "incoming_friend_h.png"; } }
        public static string BUTTON_ADD_FRIEND_OUTGOING { get { return BASE_BUTTONS + "outgoing_friend.png"; } }
        public static string BUTTON_ADD_FRIEND_OUTGOING_H { get { return BASE_BUTTONS + "outgoing_friend_h.png"; } }
        public static string BUTTON_FRIEND_ACCESS_H { get { return BASE_BUTTONS + "access_change_h.png"; } }

        public static string CHAT_CALL_DEFAULT { get { return BASE + "call_chat_default.png"; } }
        public static string CHAT_CALL_SELECTED { get { return BASE + "call_chat_selected.png"; } }
        public static string ID_DEFAULT { get { return BASE + "id_default.png"; } }
        public static string ID_SELECTED { get { return BASE + "id_selected.png"; } }
        public static string NO_CHAT_LOG { get { return BASE + "no_chat_log.png"; } }
        public static string NO_CALL_LOG { get { return BASE + "no_call_log.png"; } }
        public static string NO_FRIENDS { get { return BASE + "no_friends.png"; } }
        public static string NO_NOTIFICATIONS { get { return BASE + "no_notifications.png"; } }

        public static string BUTTON_FRIEND_ADD { get { return BASE_BUTTONS + "friend_add.png"; } }
        public static string BUTTON_FRIEND_ADD_H { get { return BASE_BUTTONS + "friend_add_h.png"; } }
        public static string BUTTON_INFO { get { return BASE_BUTTONS + "info.png"; } }
        public static string BUTTON_INFO_H { get { return BASE_BUTTONS + "info_h.png"; } }
        public static string BUTTON_CHAT { get { return BASE_BUTTONS + "chat_mini.png"; } }
        public static string BUTTON_CHAT_H { get { return BASE_BUTTONS + "chat_mini_h.png"; } }
        public static string BUTTON_VOICE_CALL { get { return BASE_BUTTONS + "sip_call.png"; } }
        public static string BUTTON_VOICE_CALL_H { get { return BASE_BUTTONS + "sip_call_h.png"; } }
        public static string BUTTON_VIDEO_CALL { get { return BASE_BUTTONS + "video_call.png"; } }
        public static string BUTTON_VIDEO_CALL_H { get { return BASE_BUTTONS + "video_call_h.png"; } }
        public static string BUTTON_PASSWORD_HIDE { get { return BASE_BUTTONS + "password_hide.png"; } }
        public static string BUTTON_PASSWORD_SHOW { get { return BASE_BUTTONS + "password_show.png"; } }
        public static string BUTTON_ADD_ICON { get { return BASE_BUTTONS + "add.png"; } }
        public static string BUTTON_ADD_ICON_PRESSED { get { return BASE_BUTTONS + "add_pressed.png"; } }
        public static string BUTTON_ADD_MY_PROFILE { get { return BASE_BUTTONS + "add_more.png"; } }
        public static string BUTTON_ADD_MY_PROFILE_H { get { return BASE_BUTTONS + "add_more_h.png"; } }
        public static string BUTTON_EDIT { get { return BASE_BUTTONS + "edit.png"; } }
        public static string BUTTON_EDIT_H { get { return BASE_BUTTONS + "edit_h.png"; } }
        public static string BUTTON_EDIT_RECOVERY { get { return BASE_BUTTONS + "edit_recovery.png"; } }
        public static string BUTTON_EDIT_RECOVERY_H { get { return BASE_BUTTONS + "edit_recovery_h.png"; } }
        public static string BUTTON_CANCEL_EDIT { get { return BASE_BUTTONS + "close_mini.png"; } }
        public static string BUTTON_CANCEL_EDIT_H { get { return BASE_BUTTONS + "close_mini_h.png"; } }
        public static string BUTTON_OK_MINI { get { return BASE_BUTTONS + "ok_mini.png"; } }
        public static string BUTTON_OK_MINI_H { get { return BASE_BUTTONS + "ok_mini_h.png"; } }
        public static string BUTTON_TAKE_PHOTOS { get { return BASE_BUTTONS + "option_take_photo.png"; } }
        public static string BUTTON_UPLOAD_PHOTO { get { return BASE_BUTTONS + "option_upload_photo.png"; } }
        public static string BUTTON_ALBUM_PHOTO { get { return BASE_BUTTONS + "option_album_photo.png"; } }
        public static string BUTTON_EDIT_PHOTO { get { return BASE_BUTTONS + "edit_photo.png"; } }
        public static string BUTTON_EDIT_PHOTO_H { get { return BASE_BUTTONS + "edit_photo_h.png"; } }
        public static string BUTTON_EDIT_PHOTO_CLICK { get { return BASE_BUTTONS + "edit_photo_click.png"; } }
        public static string BUTTON_UPLOAD_FROM_COMPUTER { get { return BASE_BUTTONS + "from_computer.png"; } }
        public static string BUTTON_UPLOAD_FROM_COMPUTER_H { get { return BASE_BUTTONS + "from_computer_h.png"; } }
        public static string BUTTON_UPLOAD_FROM_COMPUTER_CLICK { get { return BASE_BUTTONS + "from_computer_click.png"; } }
        public static string BUTTON_TAKE_PHOTO { get { return BASE_BUTTONS + "take_photo.png"; } }
        public static string BUTTON_TAKE_PHOTO_H { get { return BASE_BUTTONS + "take_photo_h.png"; } }
        public static string BUTTON_TAKE_PHOTO_CLICK { get { return BASE_BUTTONS + "take_photo_click.png"; } }
        public static string COVER_IMAGE_BACKGROUND { get { return BASE + "cover_image_box.png"; } }
        public static string NEW_STATUS_OPTIONS { get { return BASE_BUTTONS + "upload.png"; } }
        public static string NEW_STATUS_OPTIONS_H { get { return BASE_BUTTONS + "upload_h.png"; } }
        public static string NEW_STATUS_IMAGE_UPLOAD_CROSS { get { return BASE_BUTTONS + "cross.png"; } }
        public static string NEW_STATUS_IMAGE_UPLOAD_CROSS_H { get { return BASE_BUTTONS + "cross_h.png"; } }
        public static string NEW_STATUS_EMOTICONS { get { return BASE_BUTTONS + "sts_emoticon.png"; } }
        public static string NEW_STATUS_EMOTICONS_H { get { return BASE_BUTTONS + "sts_emoticon_h.png"; } }
        public static string NEW_STATUS_LOCATION { get { return BASE_BUTTONS + "sts_location.png"; } }
        public static string NEW_STATUS_LOCATION_H { get { return BASE_BUTTONS + "sts_location_h.png"; } }
        public static string NEW_STATUS_TAG { get { return BASE_BUTTONS + "sts_tag.png"; } }
        public static string NEW_STATUS_TAG_H { get { return BASE_BUTTONS + "sts_tag_h.png"; } }
        public static string NEW_STATUS_MUSIC { get { return BASE_BUTTONS + "music.png"; } }
        public static string NEW_STATUS_MUSIC_H { get { return BASE_BUTTONS + "music_h.png"; } }
        public static string NEW_STATUS_WRITE_POST_ICON { get { return BASE + "new_status_write_icon.png"; } }
        public static string BUTTON_VIDEO_FEED { get { return BASE_BUTTONS + "video_feed.png"; } }
        public static string BUTTON_VIDEO_FEED_H { get { return BASE_BUTTONS + "video_feed_h.png"; } }
        public static string BUTTON_CREATE_CIRCLE_ICON { get { return BASE_BUTTONS + "create_circle_icon.png"; } }
        public static string CELEBRITY_DISCOVER_COUNTRY_ICON { get { return BASE + "disLctn.png"; } }
        public static string CELEBRITY_DISCOVER_CATEGORY_ICON { get { return BASE + "category.png"; } }

        public static string NEW_STATUS_MEDIA_ALBUM_LIST { get { return BASE_BUTTONS + "media_album_list.png"; } }
        public static string NEW_STATUS_MEDIA_ALBUM_LIST_H { get { return BASE_BUTTONS + "media_album_list_H.png"; } }
        public static string MEDIA_ADD_ALBUM_LIST_VIEW { get { return BASE_BUTTONS + "addMedia_album.png"; } }
        public static string MEDIA_ADD_ALBUM_LIST_VIEW_H { get { return BASE_BUTTONS + "addMedia_album_h.png"; } }
        public static string MEDIA_ALBUM_LIST_VIEW { get { return BASE_BUTTONS + "albumMedia_list.png"; } }
        public static string MEDIA_ALBUM_LIST_VIEW_H { get { return BASE_BUTTONS + "albumMedia_list_h.png"; } }
        public static string MEDIA_FULL_SCREEN_VIEW { get { return BASE_BUTTONS + "full_screen.png"; } }
        public static string MEDIA_FULL_SCREEN_VIEW_H { get { return BASE_BUTTONS + "full_screen_h.png"; } }
        public static string MEDIA_FULL_SCREEN_VIEW_SELECTED { get { return BASE_BUTTONS + "full_screen_selected.png"; } }
        public static string MEDIA_EXIT_FULL_SCREEN_VIEW { get { return BASE_BUTTONS + "exit_screen.png"; } }
        public static string MEDIA_EXIT_FULL_SCREEN_VIEW_H { get { return BASE_BUTTONS + "exit_screen_h.png"; } }
        public static string MEDIA_EXIT_FULL_SCREEN_VIEW_SELECTED { get { return BASE_BUTTONS + "exit_screen_selected.png"; } }
        public static string MEDIA_NEW_WINDOW { get { return BASE_BUTTONS + "new_window.png"; } }
        public static string MEDIA_NEW_WINDOW_H { get { return BASE_BUTTONS + "new_window_h.png"; } }
        public static string MEDIA_NEW_WINDOW_SELECTED { get { return BASE_BUTTONS + "new_window_selected.png"; } }
        public static string MEDIA_NEW_WINDOW_BACK { get { return BASE_BUTTONS + "new_window_back.png"; } }
        public static string MEDIA_NEW_WINDOW_BACK_H { get { return BASE_BUTTONS + "new_window_back_h.png"; } }
        public static string MEDIA_NEW_WINDOW_BACK_SELECTED { get { return BASE_BUTTONS + "new_window_back_selected.png"; } }
        public static string MEDIA_VIEWS_COUNT { get { return BASE + "media_views.png"; } }
        public static string CELEBRITY_FOLLOWING_ICON { get { return BASE_BUTTONS + "cel_flw_normal.png"; } }
        public static string CELEBRITY_FOLLOWING_ICON_P { get { return BASE_BUTTONS + "cel_flw_click.png"; } }

        public static string MEDIA_VIEW_PREVIOUS_ICON { get { return BASE_BUTTONS + "preMedia.png"; } }
        public static string MEDIA_VIEW_PREVIOUS_ICON_H { get { return BASE_BUTTONS + "preMedia_h.png"; } }
        public static string MEDIA_VIEW_PREVIOUS_ICON_D { get { return BASE_BUTTONS + "preMedia_d.png"; } }
        public static string MEDIA_VIEW_PREVIOUS_ICON_P { get { return BASE_BUTTONS + "preMedia_selected.png"; } }
        public static string MEDIA_VIEW_NEXT_ICON { get { return BASE_BUTTONS + "nextMedia.png"; } }
        public static string MEDIA_VIEW_NEXT_ICON_H { get { return BASE_BUTTONS + "nextMedia_h.png"; } }
        public static string MEDIA_VIEW_NEXT_ICON_D { get { return BASE_BUTTONS + "nextMedia_d.png"; } }
        public static string MEDIA_VIEW_NEXT_ICON_P { get { return BASE_BUTTONS + "nextMediat_selected.png"; } }
        public static string MEDIA_VIEW_PLAY_ICON { get { return BASE_BUTTONS + "play.png"; } }
        public static string MEDIA_VIEW_PLAY_ICON_H { get { return BASE_BUTTONS + "play_h.png"; } }
        public static string MEDIA_VIEW_PLAY_LARGE { get { return BASE_BUTTONS + "play_large.png"; } }
        public static string MEDIA_VIEW_PLAY_LARGE_H { get { return BASE_BUTTONS + "play_large_h.png"; } }
        public static string MEDIA_VIEW_PLAY_ICON_P { get { return BASE_BUTTONS + "play_selected.png"; } }
        public static string MEDIA_VIEW_PAUSE_ICON { get { return BASE_BUTTONS + "pause.png"; } }
        public static string MEDIA_VIEW_PAUSE_ICON_H { get { return BASE_BUTTONS + "pause_h.png"; } }
        public static string MEDIA_VIEW_PAUSE_ICON_P { get { return BASE_BUTTONS + "pause_selected.png"; } }
        public static string MEDIA_VIEW_VOLUME_ICON { get { return BASE_BUTTONS + "volume.png"; } }

        public static string CLOSE { get { return BASE_BUTTONS + "close.png"; } }
        public static string CLOSE_H { get { return BASE_BUTTONS + "close_h.png"; } }
        public static string CLOSE_TRANSPARENT { get { return BASE_BUTTONS + "close_transparent.png"; } }
        public static string STICKER_REMOVE { get { return BASE_BUTTONS + "sticker_remove.png"; } }
        public static string STICKER_REMOVE_H { get { return BASE_BUTTONS + "sticker_remove_h.png"; } }
        public static string STICKER_RECENT_ICON { get { return BASE + "recent_sticker_icon.png"; } }
        public static string BUTTON_EXPAND { get { return BASE_BUTTONS + "expand.png"; } }
        public static string BUTTON_EXPAND_H { get { return BASE_BUTTONS + "expand_h.png"; } }
        public static string BUTTON_CLOSE_BOLD { get { return BASE_BUTTONS + "close_bold.png"; } }
        public static string BUTTON_CLOSE_BOLD_H { get { return BASE_BUTTONS + "close_bold_h.png"; } }

        public static string CHAT_POPUP_CLOSE_BOLD { get { return BASE_BUTTONS + "chat_popup_msg_cross.png"; } }
        public static string CHAT_POPUP_CLOSE_BOLD_H { get { return BASE_BUTTONS + "chat_popup_msg_cross_h.png"; } }
        public static string CHAT_POPUP_ID { get { return BASE_BUTTONS + "caht_popup_msg_id.png"; } }

        public static string BUTTON_CLOSE_BOLD_WHITE { get { return BASE_BUTTONS + "close_bold_white.png"; } }
        public static string BUTTON_CLOSE_SMALL_H { get { return BASE_BUTTONS + "close_small_h.png"; } }
        public static string BUTTON_CLOSE_SMALL_WHITE { get { return BASE_BUTTONS + "close_small_white.png"; } }
        public static string BUTTON_SETTING_MINI { get { return BASE_BUTTONS + "setting_mini.png"; } }
        public static string BUTTON_SETTING_MINI_H { get { return BASE_BUTTONS + "setting_mini_h.png"; } }
        public static string BUTTON_SETTING_MINI_SELECTED { get { return BASE_BUTTONS + "setting_mini_selected.png"; } }
        public static string BUTTON_ABOUT_SETTING_MINI { get { return BASE_BUTTONS + "about_setting_mini.png"; } }
        public static string BUTTON_ABOUT_SETTING_MINI_H { get { return BASE_BUTTONS + "about_setting_mini_h.png"; } }
        public static string BUTTON_ABOUT_SETTING_MINI_SELECTED { get { return BASE_BUTTONS + "about_setting_mini_selected.png"; } }
        public static string BUTTON_UPLOAD_FROM_DIRECTORY { get { return BASE_BUTTONS + "directory_upload.png"; } }
        public static string BUTTON_UPLOAD_FROM_DIRECTORY_H { get { return BASE_BUTTONS + "directory_upload_h.png"; } }
        public static string BUTTON_CAMERA { get { return BASE_BUTTONS + "cam_upload.png"; } }
        public static string BUTTON_CAMERA_H { get { return BASE_BUTTONS + "cam_upload_h.png"; } }
        public static string BUTTON_CAMERA_WHITE { get { return BASE + "cam_upload_white.png"; } }
        public static string BUTTON_MARK_AS_READ { get { return BASE_BUTTONS + "radio_button.png"; } }
        public static string BUTTON_MARK_AS_READ_H { get { return BASE_BUTTONS + "radio_button_select.png"; } }
        public static string BUTTON_NOTIFICATION_SELECT { get { return BASE_BUTTONS + "tick.png"; } }//offnet.png
        public static string BUTTON_NOTIFICATION_SELECT_H { get { return BASE_BUTTONS + "tick_h.png"; } }
        public static string BUTTON_NOTIFICATION_SELECTED { get { return BASE_BUTTONS + "tick_h2.png"; } }
        public static string RADIO_BUTTON_CHAT_BG { get { return BASE_BUTTONS + "radiobtn_chat_bg.png"; } }
        public static string RADIO_BUTTON_CHAT_BG_SELECTED { get { return BASE_BUTTONS + "radio_btn_selectedchat_bg.png"; } }
        public static string CIRCLE_OUTSIDE_PROFILE_PIC { get { return BASE + "cricle_outside_profile_pic.png"; } }
        public static string CIRCLE_OUTSIDE_PROFILE_PIC_MYNAMEPANEL { get { return BASE + "cricle_outside_profile_pic_mynamepanel.png"; } }
        public static string CIRCLE_OUTSIDE_PROFILE_PIC_PROFILE { get { return BASE + "cricle_outside_profile_pic_profile.png"; } }

        public static string CALL_INCOMING { get { return BASE + "callhistory_incoming.png"; } }
        public static string CALL_OUTGOING { get { return BASE + "callhistory_outgoing.png"; } }
        public static string CALL_MISSED { get { return BASE + "callhistory_missed.png"; } }
        public static string CALL_FROM_LOG { get { return BASE + "log_call.png"; } }
        public static string CALL_FROM_LOG_H { get { return BASE + "log_call_h.png"; } }
        public static string CALL_FROM_LOG_SELECTED { get { return BASE + "log_call_selected.png"; } }
        public static string DIALPAD_CALL { get { return BASE + "diapd_call.png"; } }
        public static string DIALPAD_CALL_H { get { return BASE + "diapd_call_h.png"; } }
        public static string DIALPAD_CALL_SELECTED { get { return BASE + "diapd_call_selected.png"; } }

        public static string VIDEO_CALL_FROM_LOG { get { return BASE + "log_video_call.png"; } }
        public static string VIDEO_CALL_FROM_LOG_H { get { return BASE + "log_video_call_h.png"; } }
        public static string VIDEO_CALL_FROM_LOG_SELECTED { get { return BASE + "log_video_call_selected.png"; } }

        public static string CHAT_INCOMING { get { return BASE + "chat_incoming.png"; } }
        public static string CHAT_OUTGOING { get { return BASE + "chat_outgoing.png"; } }
        public static string SEARCH_IMAGE { get { return BASE + "search_img.png"; } }
        public static string SPECIAL_SEARCH_IMAGE { get { return BASE + "special_search.png"; } }
        public static string UNKNOWN_IMAGE { get { return BASE + "unknown.png"; } }
        public static string PROFILE_IMAGE_BOX_UNKNOWN { get { return BASE + "profile_image_box.png"; } }
        public static string UNKNOWN_IMAGE_LARGE { get { return BASE + "unknown_large.png"; } }
        public static string VERIFY { get { return BASE + "verify.png"; } }
        public static string CELEBRITY_DEFAULT_IMAGE { get { return BASE + "celebrity_default_image.jpg"; } }
        public static string ROOM_DEFAULT_IMAGE { get { return BASE + "room_default_image.png"; } }


        public static string MENU_MOOD { get { return BASE_MENU + "menu_mood.png"; } }
        public static string MENU_SIGN_OUT { get { return BASE_MENU + "menu_sign_out.png"; } }
        public static string MENU_CLOSE { get { return BASE_MENU + "menu_close.png"; } }
        public static string MENU_CHECK_FOR_UPDATE { get { return BASE_MENU + "menu_check_for_update.png"; } }
        public static string MENU_SETTINGS { get { return BASE_MENU + "menu_settings.png"; } }
        public static string MENU_HELP_DESK { get { return BASE_MENU + "menu_help.png"; } }
        public static string MENU_INFO { get { return BASE_MENU + "menu_info.png"; } }
        public static string PRIVACE_POLICY { get { return BASE_MENU + "menu_privacy.png"; } }
        public static string CONTACT_US_ACTIVE { get { return BASE_MENU + "menu_contact_active.png"; } }
        public static string CONTACT_US_INACTIVE { get { return BASE_MENU + "menu_contact_inactive.png"; } }
        public static string ABOUIT_RINGID { get { return BASE_MENU + "menu_about_ringid.png"; } }

        public static string STATUS_ONLINE { get { return BASE_STATUS + "status_online.png"; } }
        public static string STATUS_OFFLINE { get { return BASE_STATUS + "status_offline.png"; } }
        public static string STATUS_HOVER { get { return BASE_STATUS + "status_hover.png"; } }
        public static string STATUS_DONOT_DISTURB { get { return BASE_STATUS + "status_dont_disturb.png"; } }
        public static string ONLINE_DESKTOP { get { return BASE_STATUS + "online_desktop.png"; } }
        public static string ONLINE_ANDROID { get { return BASE_STATUS + "online_android.png"; } }
        public static string ONLINE_IOS { get { return BASE_STATUS + "online_ios.png"; } }
        public static string ONLINE_WEB { get { return BASE_STATUS + "online_web.png"; } }
        public static string ONLINE_WINDOWS { get { return BASE_STATUS + "online_windows.png"; } }
        public static string OFFLINE { get { return BASE_STATUS + "offline.png"; } }
        public static string AWAY { get { return BASE_STATUS + "away.png"; } }
        public static string POPUP_STATUS { get { return BASE + "popup_status.png"; } }
        public static string NO_INTERNET_AVAILABLE { get { return BASE_STATUS + "no_internet.png"; } }
        public static string CHECK_BOX { get { return BASE + "check_box.png"; } }
        public static string CHECK_BOX_SLECTED { get { return BASE + "check_box_selected.png"; } }

        public static string SETTING_MINI { get { return BASE + "setting_mini.png"; } }
        public static string SETTING_MINI_H { get { return BASE + "setting_mini_h.png"; } }
        public static string STAR { get { return BASE + "star.png"; } }
        public static string STAR_H { get { return BASE + "star_h.png"; } }
        public static string STAR_F { get { return BASE + "star_f.png"; } }
        public static string CIRCLE_IMAGE_IN_LIST { get { return BASE + "group_mini.png"; } }
        public static string TICK_MARK { get { return BASE + "tick_mark.png"; } }
        public static string TICK_MARK_WHITE { get { return BASE + "tick_white.png"; } }
        public static string PROFILE_IMAGE_BOX { get { return BASE + "profile_image_box.png"; } }


        public static string EMO_UP_ARROW { get { return BASE_BUTTONS + "emo_up_arrow.png"; } }
        public static string EMO_UP_ARROW_H { get { return BASE_BUTTONS + "emo_up_arrow_h.png"; } }
        public static string EMO_DOWN_ARROW { get { return BASE_BUTTONS + "emo_down_arrow.png"; } }
        public static string EMO_DOWN_ARROW_H { get { return BASE_BUTTONS + "emo_down_arrow_h.png"; } }
        public static string STICKER_ARRANGE { get { return BASE_BUTTONS + "sticker_arrange.png"; } }
        public static string STICKER_ARRANGE_H { get { return BASE_BUTTONS + "sticker_arrange_h.png"; } }

        public static string DOWN_ARROW { get { return BASE + "down_arrow.png"; } }
        public static string DOWN_ARROW_H { get { return BASE + "down_arrow_h.png"; } }
        public static string EDIT_ALBUM_SONG_ARROW { get { return BASE + "album_edit.png"; } }
        public static string RIGHT_ARROW { get { return BASE_BUTTONS + "right_arrow.png"; } }
        public static string RIGHT_ARROW_H { get { return BASE_BUTTONS + "right_arrow_h.png"; } }
        public static string RIGHT_MINI_ARROW { get { return BASE + "right_mini_arrow.png"; } }
        public static string RIGHT_MINI_ARROW_H { get { return BASE + "right_mini_arrow_h.png"; } }
        public static string LEFT_ARROW { get { return BASE_BUTTONS + "left_arrow.png"; } }
        public static string LEFT_ARROW_H { get { return BASE_BUTTONS + "left_arrow_h.png"; } }
        public static string ALBUM_IMAGE_SELECTION_BOX { get { return BASE_BUTTONS + "album_image_selection.png"; } }

        /*FEED*/
        public static string NO_FEED_ANIMATION
        {
            get
            {
                return BASE_LOADERS + "no_feed_animation.gif";
            }
        }

        public static string RECORD_ANIMATION { get { return BASE_LOADERS + "record.gif"; } }
        public static string PLAY_RECORD_VIDEO { get { return BASE_CHAT + "play_rec_vdo.png"; } }
        public static string PLAY_RECORD_VIDEO_H { get { return BASE_CHAT + "play_rec_vdo_h.png"; } }
        /********CALL*******/
        public static string MESSAGE { get { return BASE_CALL + "messege.png"; } }
        public static string MESSAGE_H { get { return BASE_CALL + "messege_h.png"; } }

        public static string CHAT_WITH_CALLING_FRIEND { get { return BASE_CALL + "chat.png"; } }
        public static string CHAT_WITH_CALLING_FRIEND_H { get { return BASE_CALL + "chat_h.png"; } }
        public static string ANSWER_WITH_VOICE_ONLY { get { return BASE_CALL + "answer_with_voice_Only.png"; } }
        public static string ANSWER_WITH_VOICE_ONLY_H { get { return BASE_CALL + "answer_with_voice_Only_h.png"; } }

        public static string IGNORE_CALL { get { return BASE_CALL + "ignore.png"; } }
        public static string IGNORE_CALL_H { get { return BASE_CALL + "ignore_h.png"; } }
        public static string INCOMING_CALL_ANIMATION { get { return BASE_CALL + "incoming_call_animation.gif"; } }
        //   public static string OUTGOING_CALL_ANIMATION { get { return BASE_CALL + "outgoingCallAnim.gif"; } }
        public static string CALL_ACCEPT { get { return BASE_CALL + "receive.png"; } }
        public static string CALL_ACCEPT_H { get { return BASE_CALL + "receive_h.png"; } }
        public static string CALL_HOLD { get { return BASE_CALL + "hold.png"; } }
        public static string CALL_HOLD_H { get { return BASE_CALL + "hold_h.png"; } }
        public static string CALL_UNHOLD { get { return BASE_CALL + "unhold.png"; } }
        public static string CALL_UNHOLD_H { get { return BASE_CALL + "unhold_h.png"; } }
        public static string CALL_MUTE { get { return BASE_CALL + "mute.png"; } }
        public static string CALL_MUTE_H { get { return BASE_CALL + "mute_h.png"; } }
        public static string CALL_UMMUTE { get { return BASE_CALL + "unmute.png"; } }
        public static string CALL_UMMUTE_H { get { return BASE_CALL + "unmute_h.png"; } }
        public static string BUTTON_VOLUME { get { return BASE_CALL + "speker.png"; } }
        public static string BUTTON_VOLUME_H { get { return BASE_CALL + "speker_h.png"; } }

        public static string BUTTON_REC_VIDEO { get { return BASE_CALL + "vdo_recv.png"; } }
        public static string BUTTON_REC_VIDEO_H { get { return BASE_CALL + "vdo_recv_h.png"; } }

        public static string BUTTON_VIDEO { get { return BASE_CALL + "video.png"; } }
        public static string BUTTON_VIDEO_H { get { return BASE_CALL + "video_h.png"; } }

        public static string BUTTON_STOP_VIDEO { get { return BASE_CALL + "video_off.png"; } }
        public static string BUTTON_STOP_VIDEO_H { get { return BASE_CALL + "video_off_h.png"; } }
        public static string CALL_END { get { return BASE_CALL + "end_call.png"; } }
        public static string CALL_END_H { get { return BASE_CALL + "end_call_h.png"; } }
        public static string CALL_TOP_BAR_INCOMING { get { return BASE_CALL + "call_top_bar_incoming.png"; } }
        public static string CALL_TOP_BAR_OUTGOING { get { return BASE_CALL + "call_top_bar_outgoing.png"; } }

        public static string CALL_NET0 { get { return BASE_CALL + "net0.png"; } }
        public static string CALL_NET1 { get { return BASE_CALL + "net1.png"; } }
        public static string CALL_NET2 { get { return BASE_CALL + "net2.png"; } }
        public static string CALL_NET3 { get { return BASE_CALL + "net3.png"; } }
        public static string CALL_NET4 { get { return BASE_CALL + "net4.png"; } }


        public static string ACCEPT_CALL_MINI { get { return BASE_CALL + "accept_call_mini.png"; } }
        public static string ACCEPT_CALL_MINI_H { get { return BASE_CALL + "accept_call_mini_h.png"; } }
        public static string BUSY_MSG_MINI { get { return BASE_CALL + "busy_msg_mini.png"; } }
        public static string BUSY_MSG_MINI_H { get { return BASE_CALL + "busy_msg_mini_h.png"; } }
        public static string END_CALL_MINI { get { return BASE_CALL + "end_call_mini.png"; } }
        public static string END_CALL_MINI_H { get { return BASE_CALL + "end_call_mini_h.png"; } }
        public static string REJECT_END_CALL_MINI { get { return BASE_CALL + "reject_end_call_mini.png"; } }
        public static string REJECT_END_CALL_MINI_H { get { return BASE_CALL + "reject_end_call_mini_h.png"; } }
        //public static string CALL_RING_ID { get { return BASE_CALL + "call_ringid.png"; } }

        /********CALL*******/

        /********** Settings ***************/
        public static string CALL_SETTINGS { get { return BASE_SETTINGS + "call-settings.png"; } }
        public static string CALL_SETTINGS_H { get { return BASE_SETTINGS + "call-settings-h.png"; } }
        public static string CHAT_BACKGROUND { get { return BASE_SETTINGS + "change-BG.png"; } }
        public static string CHAT_BACKGROUND_H { get { return BASE_SETTINGS + "change-BG-h.png"; } }
        public static string CUSTOMIZE_MSG { get { return BASE_SETTINGS + "customise-msg.png"; } }
        public static string CUSTOMIZE_MSG_H { get { return BASE_SETTINGS + "customise-msg-h.png"; } }
        public static string IM_SETTINGS { get { return BASE_SETTINGS + "im_settings.png"; } }
        public static string IM_SETTINGS_H { get { return BASE_SETTINGS + "im_settings_h.png"; } }
        public static string HISTORY { get { return BASE_SETTINGS + "history.png"; } }
        public static string HISTORY_H { get { return BASE_SETTINGS + "history-h.png"; } }
        public static string PASSWORD { get { return BASE_SETTINGS + "password.png"; } }
        public static string PASSWORD_H { get { return BASE_SETTINGS + "password-h.png"; } }
        public static string PRIVACY { get { return BASE_SETTINGS + "privacy.png"; } }
        public static string PRIVACY_H { get { return BASE_SETTINGS + "privacy-h.png"; } }
        public static string BLOCKED { get { return BASE_SETTINGS + "blocked.png"; } }
        public static string BLOCKED_H { get { return BASE_SETTINGS + "blocked-h.png"; } }
        public static string RING_SETTINGS { get { return BASE_SETTINGS + "setting.png"; } }
        public static string RING_SETTINGS_H { get { return BASE_SETTINGS + "setting_h.png"; } }
        public static string RECOVERY_SETTINGS { get { return BASE_SETTINGS + "recovery.png"; } }
        public static string RECOVERY_SETTINGS_H { get { return BASE_SETTINGS + "recovery_h.png"; } }
        public static string USER_PREFER { get { return BASE_SETTINGS + "user.png"; } }
        public static string USER_PREFER_H { get { return BASE_SETTINGS + "user-h.png"; } }
        public static string CALL_DIVERT { get { return BASE_SETTINGS + "call-divart.png"; } }
        public static string CALL_DIVERT_H { get { return BASE_SETTINGS + "call-divart-h.png"; } }
        public static string AUDIO { get { return BASE_SETTINGS + "audio.png"; } }
        public static string AUDIO_H { get { return BASE_SETTINGS + "audio-h.png"; } }
        public static string VOLUME { get { return BASE_SETTINGS + "microphone.png"; } }
        public static string SPEAKER { get { return BASE_SETTINGS + "speaker.png"; } }
        public static string SPEAKER_TEST { get { return BASE_SETTINGS + "speaker_test.png"; } }
        public static string SPEAKER_TEST_H { get { return BASE_SETTINGS + "speaker_test_h.png"; } }
        public static string SPEAKER_TEST_SELECTED { get { return BASE_SETTINGS + "speaker_test_selected.png"; } }
        public static string VIDEO_SETTINGS { get { return BASE_SETTINGS + "video_settings.png"; } }
        public static string VIDEO_SETTINGS_H { get { return BASE_SETTINGS + "video_settings_h.png"; } }

        /************************************/

        //CHAT

        public static string CHAT_EDITED { get { return BASE_CHAT + "chat_edited.png"; } }
        public static string CHAT_EMOTICON { get { return BASE_CHAT + "chat_emoticon.png"; } }
        public static string CHAT_EMOTICON_H { get { return BASE_CHAT + "chat_emoticon_h.png"; } }
        public static string CHAT_MORE_OPTION_BG { get { return BASE_CHAT + "chat_more_option_bg.png"; } }
        public static string CHAT_RECENT { get { return BASE_CHAT + "chat_recent.png"; } }
        public static string CHAT_RECENT_H { get { return BASE_CHAT + "chat_recent_h.png"; } }
        public static string CHAT_SEND { get { return BASE_CHAT + "chat_send.png"; } }
        public static string CHAT_SEND_H { get { return BASE_CHAT + "chat_send_h.png"; } }
        public static string CHAT_STICKER { get { return BASE_CHAT + "chat_sticker.png"; } }
        public static string CHAT_STICKER_H { get { return BASE_CHAT + "chat_sticker_h.png"; } }
        public static string MINUS { get { return BASE_CHAT + "minus.png"; } }
        public static string MINUS_H { get { return BASE_CHAT + "minus_h.png"; } }
        public static string OFF { get { return BASE_CHAT + "off.png"; } }
        public static string ON { get { return BASE_CHAT + "on.png"; } }
        public static string PLUS { get { return BASE_CHAT + "plus.png"; } }
        public static string PLUS_H { get { return BASE_CHAT + "plus_h.png"; } }
        public static string RECORD_PAUSE { get { return BASE_CHAT + "record_pause.png"; } }
        public static string RECORD_PAUSE_H { get { return BASE_CHAT + "record_pause_h.png"; } }
        public static string RECORD_RESUME { get { return BASE_CHAT + "record_resume.png"; } }
        public static string RECORD_RESUME_H { get { return BASE_CHAT + "record_resume_h.png"; } }
        public static string RECORD_SEND { get { return BASE_CHAT + "record_send.png"; } }
        public static string RECORD_SEND_H { get { return BASE_CHAT + "record_send_h.png"; } }
        public static string RECORD_START { get { return BASE_CHAT + "record_start.png"; } }
        public static string RECORD_START_H { get { return BASE_CHAT + "record_start_h.png"; } }
        public static string RECORD_STOP { get { return BASE_CHAT + "record_stop.png"; } }
        public static string RECORD_STOP_H { get { return BASE_CHAT + "record_stop_h.png"; } }
        public static string WATCH { get { return BASE_CHAT + "watch.png"; } }
        public static string WATCH_H { get { return BASE_CHAT + "watch_h.png"; } }
        public static string EMOTICON_POPUP_DOWN_ARROW { get { return BASE + "emoticon_popup_down_arrow.png"; } }
        public static string EMOTICON_POPUP_UP_ARROW { get { return BASE + "emoticon_popup_up_arrow.png"; } }
        public static string EMOTICON_POPUP_NO_ARROW { get { return BASE + "emoticon_popup_no_arrow.png"; } }
        public static string POPUP_CHAT_RECENT { get { return BASE_CHAT + "popup_chat_recent.png"; } }
        public static string CHAT_DEFAULT_AUDIO { get { return BASE_CHAT + "chat_default_audio.png"; } }
        public static string CHAT_DEFAULT_FILE { get { return BASE_CHAT + "chat_default_file.png"; } }
        public static string CHAT_DEFAULT_FILE_DOWNLOAD { get { return BASE_CHAT + "chat_default_file_download.png"; } }
        public static string CHAT_DEFAULT_FILE_UPLOAD { get { return BASE_CHAT + "chat_default_file_upload.png"; } }
        public static string CHAT_DEFAULT_VIDEO { get { return BASE_CHAT + "chat_default_video.png"; } }
        public static string CHAT_DEFAULT_IMAGE { get { return BASE_CHAT + "chat_default_image.png"; } }
        public static string RECORDER_CLOSE { get { return BASE_CHAT + "recorder_close.png"; } }
        public static string RECORDER_CLOSE_H { get { return BASE_CHAT + "recorder_close_h.png"; } }
        public static string INCLUDE_VOICE { get { return BASE_CHAT + "include_voice.png"; } }
        public static string INCLUDE_VOICE_H { get { return BASE_CHAT + "include_voice_h.png"; } }
        public static string INCLUDE_VOICE_SELECTED { get { return BASE_CHAT + "include_voice_selected.png"; } }
        public static string SEND_CHAT_IMAGE { get { return BASE_CHAT + "send_chat_image.png"; } }
        public static string SEND_CHAT_IMAGE_H { get { return BASE_CHAT + "send_chat_image_h.png"; } }
        public static string SEND_CHAT_IMAGE_SELECTED { get { return BASE_CHAT + "send_chat_image_selected.png"; } }
        public static string CHAT_RESUME_BIG { get { return BASE_CHAT + "chat_resume_big.png"; } }
        public static string CHAT_RESUME_BIG_H { get { return BASE_CHAT + "chat_resume_big_h.png"; } }
        public static string CHAT_RESUME_BIG_SELECTED { get { return BASE_CHAT + "chat_resume_big_selected.png"; } }
        public static string ADD_FROM_WEBCAM { get { return BASE_CHAT + "add_from_webcam.png"; } }
        public static string ADD_FROM_DIRECTORY { get { return BASE_CHAT + "add_from_directory.png"; } }
        public static string ADD_FROM_WEBCAM_H { get { return BASE_CHAT + "add_from_webcam_h.png"; } }
        public static string ADD_FROM_DIRECTORY_H { get { return BASE_CHAT + "add_from_directory_h.png"; } }
        public static string CHAT_MULTIMEDIA { get { return BASE_CHAT + "chat_multimedia.png"; } }
        public static string CHAT_MULTIMEDIA_H { get { return BASE_CHAT + "chat_multimedia_h.png"; } }
        public static string ADD_STICKER { get { return BASE_CHAT + "add_sticker.png"; } }
        public static string ADD_STICKER_H { get { return BASE_CHAT + "add_sticker_h.png"; } }
        public static string STICKER_DOWNLOAD { get { return BASE + "sticker_download.png"; } }
        public static string STICKER_DOWNLOAD_H { get { return BASE + "sticker_download_h.png"; } }
        public static string CHAT_MEDIA_PLAY { get { return BASE_CHAT + "chat_media_play.png"; } }
        public static string CHAT_MEDIA_PLAY_H { get { return BASE_CHAT + "chat_media_play_h.png"; } }
        public static string CHAT_MEDIA_PLAY_DISABLE { get { return BASE_CHAT + "chat_media_play_disable.png"; } }
        public static string CHAT_MEDIA_PAUSE { get { return BASE_CHAT + "chat_media_pause.png"; } }
        public static string CHAT_MEDIA_PAUSE_H { get { return BASE_CHAT + "chat_media_pause_h.png"; } }
        public static string CHAT_MEDIA_PAUSE_DISABLE { get { return BASE_CHAT + "chat_media_pause_disable.png"; } }
        public static string HAND_GRAB { get { return BASE_CHAT + "hand_grab.ico"; } }
        public static string HAND_RELEASE { get { return BASE_CHAT + "hand_release.ico"; } }
        public static string IMAGE_ORIGINAL_SIZE { get { return BASE_CHAT + "image_original_size.png"; } }
        public static string IMAGE_ORIGINAL_SIZE_H { get { return BASE_CHAT + "image_original_size_h.png"; } }
        public static string IMAGE_ZOOM { get { return BASE_CHAT + "image_zoom.png"; } }
        public static string IMAGE_ZOOM_H { get { return BASE_CHAT + "image_zoom_h.png"; } }
        public static string RECORD_EXPAND { get { return BASE_CHAT + "record_expand.png"; } }
        public static string RECORD_EXPAND_H { get { return BASE_CHAT + "record_expand_h.png"; } }
        public static string RECORD_NORMAL { get { return BASE_CHAT + "record_normal.png"; } }
        public static string RECORD_NORMAL_H { get { return BASE_CHAT + "record_normal_h.png"; } }
        public static string LOVE { get { return BASE_CHAT + "love.png"; } }
        public static string LOVE_H { get { return BASE_CHAT + "love_h.png"; } }
        public static string CHAT_LOG_PLUS { get { return BASE_CHAT + "chat_log_plus.png"; } }
        public static string CHAT_LOG_PLUS_H { get { return BASE_CHAT + "chat_log_plus_h.png"; } }
        public static string CHAT_EMOTICON_LARGE { get { return BASE_CHAT + "chat_emoticon_large.png"; } }
        public static string CHAT_EMOTICON_LARGE_H { get { return BASE_CHAT + "chat_emoticon_large_h.png"; } }
        public static string DELIVERED { get { return BASE_CHAT + "delivered.png"; } }
        public static string SEEN { get { return BASE_CHAT + "seen.png"; } }
        public static string ADD_GROUP_MEMBER { get { return BASE_BUTTONS + "add_group_member.png"; } }
        public static string ADD_GROUP_MEMBER_H { get { return BASE_BUTTONS + "add_group_member_h.png"; } }
        public static string EDIT_INFO { get { return BASE + "edit_info.png"; } }

        public static string LINK_BUTTON_PREVIOUS_IMAGE { get { return BASE_BUTTONS + "link_prev.png"; } }
        public static string LINK_BUTTON_PREVIOUS_IMAGE_H { get { return BASE_BUTTONS + "link_prev_h.png"; } }
        public static string LINK_BUTTON_PREVIOUS_IMAGE_P { get { return BASE_BUTTONS + "link_prev_select.png"; } }
        public static string LINK_BUTTON_NEXT_IMAGE { get { return BASE_BUTTONS + "link_next.png"; } }
        public static string LINK_BUTTON_NEXT_IMAGE_H { get { return BASE_BUTTONS + "link_next_h.png"; } }
        public static string LINK_BUTTON_NEXT_IMAGE_P { get { return BASE_BUTTONS + "link_next_select.png"; } }
        public static string LINK_VIDEO_ICON { get { return BASE + "Link_Video_Icon.png"; } }
        public static string LINK_VIDEO_ICON_H { get { return BASE + "Link_Video_Icon_h.png"; } }
        public static string LINK_MESSAGE_ICON { get { return BASE + "link_mg_icon.png"; } }
        public static string INFO_BIG { get { return BASE + "info_big.png"; } }
        public static string PHOTOS { get { return BASE + "photos.png"; } }
        public static string MV { get { return BASE + "music_video.png"; } }

        public static string LIST_VIEW_ALBUM { get { return BASE_BUTTONS + "list_view.png"; } }
        public static string LIST_VIEW_ALBUM_H { get { return BASE_BUTTONS + "list_view_h.png"; } }
        public static string THUMBNAIL_VIEW { get { return BASE_BUTTONS + "thumbnail_view.png"; } }
        public static string THUMBNAIL_VIEW_H { get { return BASE_BUTTONS + "thumbnail_view_h.png"; } }
        public static string UPLOAD_MUSIC_AND_VIDEO_ICON { get { return BASE_BUTTONS + "uploadMusic.png"; } }
        public static string UPLOAD_MUSIC_AND_VIDEO_ICON_H { get { return BASE_BUTTONS + "uploadMusic_h.png"; } }
        public static string UPLOAD_MUSIC_AND_VIDEO_ICON_SELECTED { get { return BASE_BUTTONS + "uploadMusic_selected.png"; } }
        public static string COMMENT_UPLOAD_ICON { get { return BASE_BUTTONS + "cmnt_upload.png"; } }
        public static string COMMENT_UPLOAD_ICON_H { get { return BASE_BUTTONS + "cmnt_upload_h.png"; } }
        public static string DISCOVER_SEARCH_ICON { get { return BASE_BUTTONS + "discover_search.png"; } }
        public static string DISCOVER_SEARCH_ICON_H { get { return BASE_BUTTONS + "discover_search_h.png"; } }
        public static string DISCOVER_SEARCH_ICON_CLICKED { get { return BASE_BUTTONS + "discover_search_click.png"; } }
        public static string MEDIA_UP_SCROLL_ICON { get { return BASE_BUTTONS + "media_up.png"; } }
        public static string MEDIA_UP_SCROLL_ICON_H { get { return BASE_BUTTONS + "media_up_h.png"; } }

        public static string MEDIA_ADD_TO_ALBUM_ICON { get { return BASE + "add_to_album_Media.png"; } }
        public static string MEDIA_ADD_TO_ALBUM_ORANGE_ICON { get { return BASE + "add_to_orange_album.png"; } }
        public static string MEDIA_SHARE_ORANGE_ICON { get { return BASE + "share_media_orange_icon.png"; } }
        public static string SINGLE_MEDIA_SHARE_ICON { get { return BASE + "share_media_icon.png"; } }
        //public static string MEDIA_DOWNLOAD_ICON { get { return BASE + "download.png"; } }
        public static string MEDIA_DOWNLOAD_ORANGE_ICON { get { return BASE + "download_orange.png"; } }
        public static string MEDIA_DOWNLOADING_ICON { get { return BASE + "downloading.png"; } }
        public static string MEDIA_WAITING_ICON { get { return BASE + "download_waiting.png"; } }
        public static string MEDIA_DOWNLOADED_ICON { get { return BASE + "downloaded.png"; } }

        public static string MEDIA_SHARE_ICON { get { return BASE + "shareMedia.png"; } }
        public static string MEDIA_UPLOAD_WAITING_ICON { get { return BASE + "waiting.jpg"; } }

        public static string DEFAULT_AUDIO_IMAGE { get { return BASE + "default_audio_image.jpg"; } }
        public static string DEFAULT_AUDIO_SMALL { get { return BASE + "default_audio_small.png"; } }
        public static string DEFAULT_VIDEO_IMAGE { get { return BASE + "default_video_image.png"; } }
        public static string DEFAULT_VIDEO_SMALL { get { return BASE + "default_video_small.png"; } }
        public static string NEW_STATUS_VALIDITY_ICON { get { return BASE_BUTTONS + "validity.png"; } }
        public static string NEW_STATUS_VALIDITY_ICON_H { get { return BASE_BUTTONS + "validity_h.png"; } }
        public static string NEW_STATUS_VALIDITY_ICON_SELECTED { get { return BASE_BUTTONS + "validity_selected.png"; } }
        public static string NEW_STATUS_AUDIO_ICON { get { return BASE + "audio.png"; } }
        public static string NEW_STATUS_VIDEO_ICON { get { return BASE + "video.png"; } }
        public static string POPUP_VALIDITY_UP_ARROW { get { return BASE + "validity_popup_up_arrow.png"; } }
        public static string POPUP_VALIDITY_DOWN_ARROW { get { return BASE + "validity_popup_down_arrow.png"; } }
        public static string MUSIC_AND_VIDEO_TAG_ICON { get { return BASE + "music_tag.png"; } }
        public static string MEDIA_CLOUD_HASH_TAG_IMAGE { get { return BASE + "cloudHashTag_Icon.png"; } }
        public static string NEWSPORTAL_DEFAULT_IMAGE { get { return BASE + "NewsPortalDefaultImage.png"; } }

        public static string HASHTAG_ALL_IMAGE_ICON { get { return BASE + "hashTag_all.png"; } }
        public static string HASHTAG_ALBUM_IMAGE_ICON { get { return BASE + "hashTag_album.png"; } }
        public static string HASHTAG_IMAGE_ICON { get { return BASE + "hashTag_Icon.png"; } }
        public static string HASHTAG_SONG_IMAGE_ICON { get { return BASE + "hashTag_song.png"; } }
        public static string MEDIA_NEXT_SEARCH_DEATAILS { get { return BASE_BUTTONS + "media_next.png"; } }
        public static string MEDIA_NEXT_SEARCH_DEATAILS_H { get { return BASE_BUTTONS + "media_next_h.png"; } }

        public static string RECOVERY_OPTION_FACEBOOK { get { return BASE + "recovery_fb.jpg"; } }
        public static string RECOVERY_OPTION_TWITTER { get { return BASE + "recovery_twitter.jpg"; } }

        public static string CREATE_GROUP { get { return BASE + "create_group.png"; } }
        public static string GROUP_NAME_EDIT { get { return BASE + "group_name_edit.png"; } }
        public static string GROUP_NAME_EDIT_OK { get { return BASE + "group_name_edit_ok.png"; } }
        public static string GROUP_NAME_EDIT_OK_H { get { return BASE + "group_name_edit_ok_h.png"; } }
        public static string GROUP_NAME_EDIT_CANCEL { get { return BASE + "group_name_edit_cancel.png"; } }
        public static string GROUP_NAME_EDIT_CANCEL_H { get { return BASE + "group_name_edit_cancel_h.png"; } }

        public static string ADD_CHAT_BG { get { return BASE_CHAT + "add_chat_bg.png"; } }
        public static string ADD_CHAT_BG_H { get { return BASE_CHAT + "add_chat_bg_h.png"; } }
        public static string CHAT_BG_BACK { get { return BASE_CHAT + "chat_bg_back.png"; } }
        public static string CHAT_BG_BACK_H { get { return BASE_CHAT + "chat_bg_back_H.png"; } }
        public static string CHAT_BG_NEXT { get { return BASE_CHAT + "chat_bg_next.png"; } }
        public static string CHAT_BG_NEXT_H { get { return BASE_CHAT + "chat_bg_next_h.png"; } }
        public static string CHAT_BG_SELECT { get { return BASE_CHAT + "chat_bg_select.png"; } }
        public static string CHAT_BG_DESELECT { get { return BASE_CHAT + "chat_bg_deselect.png"; } }
        public static string CHAT_BG_DESELECT_H { get { return BASE_CHAT + "chat_bg_deselect_h.png"; } }
        public static string CHAT_BG_DOWNLOAD { get { return BASE_CHAT + "chat_bg_download.png"; } }
        public static string CHAT_BG_DOWNLOAD_H { get { return BASE_CHAT + "chat_bg_download_h.png"; } }
        public static string CHAT_FILE_UPLOAD { get { return BASE_CHAT + "chat_file_upload.png"; } }
        public static string FILE_TRANFER_CANCEL { get { return BASE_CHAT + "file_transfer_cancel.png"; } }
        public static string FILE_TRANFER_CANCEL_H { get { return BASE_CHAT + "file_transfer_cancel_h.png"; } }
        public static string GROUP_SETTINGS { get { return BASE_BUTTONS + "group_settings.png"; } }
        public static string GROUP_SETTINGS_H { get { return BASE_BUTTONS + "group_settings_h.png"; } }
        public static string CHAT_PLAY { get { return BASE_CHAT + "chat_play.png"; } }
        public static string CHAT_PLAY_H { get { return BASE_CHAT + "chat_play_h.png"; } }
        public static string CHAT_PAUSE { get { return BASE_CHAT + "chat_pause.png"; } }
        public static string CHAT_PAUSE_H { get { return BASE_CHAT + "chat_pause_h.png"; } }
        public static string CHAT_AUDIO { get { return BASE_CHAT + "chat_audio.png"; } }
        public static string DEFAULT_RING_MEDIA_IMAGE { get { return BASE_CHAT + "default_ring_media_image.jpg"; } }

        public static string FEED_1_IMAGE { get { return BASE_MENU + "feed_1.png"; } }
        public static string FEED_1_IMAGE_H { get { return BASE_MENU + "feed_1_h.png"; } }
        public static string FEED_2_IMAGE { get { return BASE_MENU + "feed_2.png"; } }
        public static string FEED_2_IMAGE_H { get { return BASE_MENU + "feed_2_h.png"; } }
        public static string FEED_3_IMAGE { get { return BASE_MENU + "feed_3.png"; } }
        public static string FEED_3_IMAGE_H { get { return BASE_MENU + "feed_3_h.png"; } }

        public static string BACK_IMAGE_ICON { get { return BASE_BUTTONS + "back_icon.png"; } }
        public static string BACK_IMAGE_ICON_H { get { return BASE_BUTTONS + "back_icon_h.png"; } }
        public static string BACK_IMAGE_ICON_P { get { return BASE_BUTTONS + "back_icon_p.png"; } }

        public static string HDOT { get { return BASE_BUTTONS + "HDot.png"; } }
        public static string HDOT_H { get { return BASE_BUTTONS + "HDot_h.png"; } }
        public static string VDOT { get { return BASE_BUTTONS + "VDot.png"; } }
        public static string VDOT_H { get { return BASE_BUTTONS + "VDot_h.png"; } }
        public static string HDOT_SELECTED { get { return BASE_BUTTONS + "HDot_selected.png"; } }
        public static string VDOT_SELECTED { get { return BASE_BUTTONS + "VDot_selected.png"; } }
        public static string UP_LOADING_ICON { get { return BASE_BUTTONS + "Up_loading.png"; } }
        public static string UP_LOADING_ICON_H { get { return BASE_BUTTONS + "Up_loading_h.png"; } }

        public static string UP_ARROW { get { return BASE + "up.png"; } }
        public static string UP_ARROW_H { get { return BASE + "up_h.png"; } }

        public static string UPDATE_VERSION_ICON { get { return BASE + "update.png"; } }
        public static string PHOTO_LOADING { get { return BASE + "photo_loading.png"; } }
        public static string PHOTO_ALBUM_COVER_IMAGE { get { return BASE + "photo_album_default.png"; } }

        public static string UPLOAD { get { return BASE + "upload.png"; } }
        public static string UPLOAD_H { get { return BASE + "upload_h.png"; } }
        public static string UPLOAD_POPUP_UP_ARROW { get { return BASE + "upload_popup_up_arrow.png"; } }
        public static string DOWNLOAD { get { return BASE + "download.png"; } }
        public static string BUTTON_HD
        { get { return BASE_BUTTONS + "hd.png"; } }

        public static string BUTTON_HD_H
        { get { return BASE_BUTTONS + "hd_h.png"; } }

        #region GIF
        public static string LOADER_CLOCK { get { return BASE_LOADERS + "waiting.gif"; } }
        public static string LOADER_PRGOGRESS
        {
            get
            {
                return BASE_LOADERS + "calling.gif";
            }
        }
        public static string LOADER_SMALL { get { return BASE_LOADERS + "Loading_30x30.gif"; } }
        public static string LOADER_MEDIUM { get { return BASE_LOADERS + "Loading_40x40.gif"; } }
        public static string LOADER_100 { get { return BASE_LOADERS + "Loading_100.gif"; } }
        public static string LOADING_RINGID_FEED { get { return BASE_LOADERS + "LOADING_RINGID_FEED.gif"; } }
        public static string SPLASH_SCREEN_LOADER { get { return BASE_LOADERS + "splash_screen.gif"; } }
        public static string TYPING { get { return BASE_LOADERS + "typing.gif"; } }
        public static string LOADER_LIKE_ANIMATION { get { return BASE_LOADERS + "like_animation.gif"; } }
        public static string LOADER_FEED { get { return BASE_LOADERS + "loader.gif"; } }
        public static string LOADER_FEED_CYCLE { get { return BASE_LOADERS + "loader_cycle.gif"; } }
        public static string LOADER_FEED_CYCLE_MINI { get { return BASE_LOADERS + "loader_cycle_30.gif"; } }
        public static string LOADER_CYCLE_MINI_BG_BLACK { get { return BASE_LOADERS + "loader_cycle_blackBG.gif"; } }//loader_cycle_blackBG
        public static string LOADER_CHAT_HISTORY { get { return BASE_LOADERS + "loader_chat_history.gif"; } }
        public static string LOADER_PLAYER_BUFFER { get { return BASE_LOADERS + "player_buffer.gif"; } }
        public static string LOADER_PLAYER_MUSIC { get { return BASE_LOADERS + "player_music.gif"; } }
        public static string LOADER_SIGN_IN { get { return BASE_LOADERS + "signIn_loader.gif"; } }
        public static string LOADER_DOWNLOAD_SMALL { get { return BASE_LOADERS + "download_loader.gif"; } }
        public static string MORE_LOADER { get { return BASE_LOADERS + "more_loader.gif"; } }
        public static string OUTGOING_ANIMATION { get { return BASE_LOADERS + "outgoingCallAnim.gif"; } }//outgoingCallAnim.gif/calling.gif
        public static string LOADER_OFFLINE { get { return BASE_LOADERS + "offline_loader.gif"; } }
        public static string UPLOAD_DOWNLOAD { get { return BASE + "upload_download.gif"; } }
        public static string UPLOADING { get { return BASE + "uploading.gif"; } }
        public static string DOWNLOADING { get { return BASE + "downloading.gif"; } }
        #endregion
        #region "Full Image view"
        public static string BUTTON_BACK_IMAGE
        {
            get
            {
                return BASE_BUTTONS + "back.png";
            }
        }
        public static string BUTTON_BACK_IMAGE_H
        {
            get
            {
                return BASE_BUTTONS + "back_h.png";
            }
        }
        public static string BUTTON_BACK_IMAGE_P
        {
            get
            {
                return BASE_BUTTONS + "back_p.png";
            }
        }
        public static string BUTTON_NEXT_IMAGE
        {
            get
            {
                return BASE_BUTTONS + "next.png";
            }
        }
        public static string BUTTON_NEXT_IMAGE_H
        {
            get
            {
                return BASE_BUTTONS + "next_h.png";
            }
        }
        public static string BUTTON_NEXT_IMAGE_P
        {
            get
            {
                return BASE_BUTTONS + "next_p.png";
            }
        }
        #endregion

        //wallet
        public static string WALLET { get { return BASE + "wallet.png"; } }
        public static string WALLET_H { get { return BASE + "wallet-h.png"; } }
        public static string WALLET_SELECTED { get { return BASE + "wallet-selected.png"; } }
        public static string GOLD { get { return BASE + "gold.png"; } }
        public static string SILVER { get { return BASE + "silver.png"; } }
        public static string BRONZE { get { return BASE + "bronze.png"; } }
        public static string COIN_EXCHANGE { get { return BASE_BUTTONS + "coin_exchange.png"; } }
        public static string COIN_EXCHANGE_H { get { return BASE_BUTTONS + "coin_exchange_h.png"; } }
        public static string WALLET_COIN { get { return BASE + "wallet_coin.png"; } }
        public static string WALLET_DOUBLE_COIN { get { return BASE + "wallet_double_coin.png"; } }
        public static string WALLET_CHECK_IN { get { return BASE + "wallet_check_in.png"; } }
        public static string WALLET_CHECK_IN_GIFT { get { return BASE + "wallet_check_in_gift.png"; } }
        public static string WALLET_CHECK_IN_SUCCESS { get { return BASE + "wallet_check_in_success.png"; } }
        public static string WALLET_INVITE { get { return BASE + "wallet_invite.png"; } }
        public static string WALLET_INVITE_H { get { return BASE + "wallet_invite_h.png"; } }
        public static string WALLET_MORE_SHARE { get { return BASE + "wallet_more_share.png"; } }
        public static string WALLET_MORE_SHARE_H { get { return BASE + "wallet_more_share_h.png"; } }
        public static string WALLET_ALREADY_CHECKED_IN { get { return BASE + "wallet_already_checked_in_l.png"; } }
        public static string WALLET_MOBILE_BANKING { get { return BASE + "wallet_mobile_banking.png"; } }
        public static string WALLET_INTERNET_BANKING { get { return BASE + "wallet_internet_banking.png"; } }
        public static string WALLET_BANK_CARDS { get { return BASE + "wallet_bank_cards.png"; } }
        public static string WALLET_RIGHT_ARROW { get { return BASE + "wallet_arrow_right_35x35.png"; } }
        public static string WALLET_SSL { get { return BASE + "wallet_ssl.jpg"; } }
        public static string WALLET_PAYPAL { get { return BASE + "wallet_paypal.jpg"; } }

        // Stream
        public static string COIN_COUNT { get { return BASE_STREAM + "coin_count.png"; } }
        public static string COIN_COUNT_H { get { return BASE_STREAM + "coin_count_h.png"; } }
        public static string COIN_RIGHT_ARROW { get { return BASE_STREAM + "coin_right_arrow.png"; } }
        public static string COIN_RIGHT_ARROW_H { get { return BASE_STREAM + "coin_right_arrow_h.png"; } }
        public static string COIN_SMALL { get { return BASE_STREAM + "coin_small.png"; } }
        public static string COIN_LARGE { get { return BASE_STREAM + "coin_large.png"; } }

        public static string EYE { get { return BASE_STREAM + "eye.png"; } }
        public static string EYE_H { get { return BASE_STREAM + "eye_h.png"; } }
        public static string GIFT { get { return BASE_STREAM + "gift.png"; } }
        public static string GIFT_H { get { return BASE_STREAM + "gift_h.png"; } }
        public static string GO_LIVE { get { return BASE_STREAM + "go_live.png"; } }
        public static string GO_LIVE_H { get { return BASE_STREAM + "go_live_h.png"; } }
        public static string GO_MY_CHANNEL { get { return BASE_STREAM + "go_my_channel.png"; } }
        public static string GO_MY_CHANNEL_H { get { return BASE_STREAM + "go_my_channel_h.png"; } }
        public static string SEND { get { return BASE_STREAM + "send.png"; } }
        public static string SEND_H { get { return BASE_STREAM + "send_h.png"; } }
        public static string SELECT_CAMERA { get { return BASE_STREAM + "select_camera.png"; } }
        public static string SELECT_CAMERA_H { get { return BASE_STREAM + "select_camera_h.png"; } }
        public static string SELECT_MONITOR { get { return BASE_STREAM + "select_monitor.png"; } }
        public static string SELECT_MONITOR_H { get { return BASE_STREAM + "select_monitor_h.png"; } }
        public static string SELECT_APP { get { return BASE_STREAM + "select_app.png"; } }
        public static string SELECT_APP_H { get { return BASE_STREAM + "select_app_h.png"; } }
        public static string SOURCE_CAMERA_WHITE { get { return BASE_STREAM + "source_camera_white.png"; } }
        public static string SOURCE_CAMERA_WHITE_H { get { return BASE_STREAM + "source_camera_white_h.png"; } }
        public static string SOURCE_MONITOR_WHITE { get { return BASE_STREAM + "source_monitor_white.png"; } }
        public static string SOURCE_MONITOR_WHITE_H { get { return BASE_STREAM + "source_monitor_white_h.png"; } }
        public static string SOURCE_APP_WHITE { get { return BASE_STREAM + "source_app_white.png"; } }
        public static string SOURCE_APP_WHITE_H { get { return BASE_STREAM + "source_app_white_h.png"; } }
        public static string SOURCE_CAMERA_ORANGE { get { return BASE_STREAM + "source_camera_orange.png"; } }
        public static string SOURCE_CAMERA_ORANGE_H { get { return BASE_STREAM + "source_camera_orange_h.png"; } }
        public static string SOURCE_MONITOR_ORANGE { get { return BASE_STREAM + "source_monitor_orange.png"; } }
        public static string SOURCE_MONITOR_ORANGE_H { get { return BASE_STREAM + "source_monitor_orange_h.png"; } }
        public static string SOURCE_APP_ORANGE { get { return BASE_STREAM + "source_app_orange.png"; } }
        public static string SOURCE_APP_ORANGE_H { get { return BASE_STREAM + "source_app_orange_h.png"; } }
        public static string STREAM_CANCEL_WHITE { get { return BASE_STREAM + "stream_cancel_white.png"; } }
        public static string STREAM_CANCEL_WHITE_H { get { return BASE_STREAM + "stream_cancel_white_h.png"; } }
        public static string STREAM_SHARE_WHITE { get { return BASE_STREAM + "stream_share_white.png"; } }
        public static string STREAM_SHARE_WHITE_H { get { return BASE_STREAM + "stream_share_white_h.png"; } }
        public static string STREAM_LOCATION_WHITE { get { return BASE_STREAM + "stream_location_white.png"; } }
        public static string STREAM_LOCATION_WHITE_H { get { return BASE_STREAM + "stream_location_white_h.png"; } }
        public static string STREAM_MIC_WHITE { get { return BASE_STREAM + "stream_mic_white.png"; } }
        public static string STREAM_MIC_WHITE_H { get { return BASE_STREAM + "stream_mic_white_h.png"; } }
        public static string TIME { get { return BASE_STREAM + "time.png"; } }
        public static string TIME_H { get { return BASE_STREAM + "time_h.png"; } }
        public static string TWITTER_WHITE { get { return BASE_STREAM + "twitter_white.png"; } }
        public static string TWITTER_WHITE_H { get { return BASE_STREAM + "twitter_white_h.png"; } }
        public static string FACEBOOK_WHITE { get { return BASE_STREAM + "facebook_white.png"; } }
        public static string FACEBOOK_WHITE_H { get { return BASE_STREAM + "facebook_white_h.png"; } }
        public static string STREAM_CURSOR { get { return BASE_STREAM + "stream_cursor.ico"; } }
        public static string STREAM_CATEGORY_DOWN { get { return BASE_STREAM + "category_arrow_down.png"; } }
        public static string DEFAULT_APP_ICON { get { return BASE_STREAM + "default_app_icon.png"; } }
        public static string STREAM_CATEGORY_SELECT { get { return BASE_STREAM + "stream_category_select.png"; } }
        public static string STREAM_CHAT_OFF { get { return BASE_STREAM + "stream_chat_off.png"; } }
        public static string STREAM_CHAT_ON { get { return BASE_STREAM + "stream_chat_on.png"; } }
        public static string STREAM_FULL_VIEW { get { return BASE_STREAM + "stream_full_view.png"; } }
        public static string STREAM_FULL_VIEW_H { get { return BASE_STREAM + "stream_full_view_h.png"; } }
        public static string STREAM_CHANNEL_DISCOVER { get { return BASE_STREAM + "stream_channel_discover.png"; } }
        public static string STREAM_CHANNEL_DISCOVER_H { get { return BASE_STREAM + "stream_channel_discover_h.png"; } }
        public static string STREAM_CHANNEL_RELOAD { get { return BASE_STREAM + "stream_channel_reload.png"; } }
        public static string STREAM_CHANNEL_RELOAD_H { get { return BASE_STREAM + "stream_channel_reload_h.png"; } }
        public static string CHANNEL_INFO { get { return BASE_STREAM + "channel_info.png"; } }
        public static string CHANNEL_INFO_H { get { return BASE_STREAM + "channel_info_h.png"; } }
        public static string CHANNEL_INFO_SMALL { get { return BASE_STREAM + "channel_info_small.png"; } }
        public static string CHANNEL_INFO_SMALL_H { get { return BASE_STREAM + "channel_info_small_h.png"; } }
        public static string STREAMING_LOAD_ICON { get { return BASE_STREAM + "streaming_load_icon.png"; } }
        public static string CHANNEL_SETTINGS_HD { get { return BASE_STREAM + "channel_settings_hd.png"; } }
        public static string CHANNEL_SETTINGS_HD_H { get { return BASE_STREAM + "channel_settings_hd_h.png"; } }
        public static string CHANNEL_SETTINGS_SD { get { return BASE_STREAM + "channel_settings_sd.png"; } }
        public static string CHANNEL_SETTINGS_SD_H { get { return BASE_STREAM + "channel_settings_sd_h.png"; } }
        public static string STREAM_FOLLOWING { get { return BASE_STREAM + "stream_following.png"; } }
        public static string STREAM_FOLLOWING_H { get { return BASE_STREAM + "stream_following_h.png"; } }
        public static string RECENT_LIVE { get { return BASE_STREAM + "recent_live.png"; } }

        public static string CHANNEL_PROFILE_UNKNOWN { get { return BASE_CHANNEL + "channel_pro_image_unknown.png"; } }
        public static string CHANNEL_PROFILE_IMAGE_NORMAL { get { return BASE_CHANNEL + "channel_pro_Image_normal.png"; } }
        public static string CHANNEL_COVER_IMAGE_H { get { return BASE_CHANNEL + "cover_photo_upload_h.png"; } }
        public static string CHANNEL_PROFILE_IMAGE_CROSS { get { return BASE_CHANNEL + "channel_pro_img_cancel.png"; } }
        public static string CHANNEL_PROFILE_IMAGE_CROSS_H { get { return BASE_CHANNEL + "channel_pro_image_cancel_h.png"; } }

        public static string TEMP_DOLLAR { get { return BASE_STREAM + "dollar.png"; } }

        public static string CONFIRMATIONS_CROSS { get { return BASE_CONFIRMATIONS + "cross.png"; } }
        public static string CONFIRMATIONS_INFO { get { return BASE_CONFIRMATIONS + "info.png"; } }
        public static string CONFIRMATIONS_QUESTION { get { return BASE_CONFIRMATIONS + "question.png"; } }
        public static string CONFIRMATIONS_WARNING { get { return BASE_CONFIRMATIONS + "warning.png"; } }
        //window buttons
        public static string WINDOW_BACK_TO_MAIN { get { return BASE_WINDOW + "back_to_main.png"; } }
        public static string WINDOW_BACK_TO_MAIN_H { get { return BASE_WINDOW + "back_to_main_h.png"; } }
        public static string WINDOW_BACK_TO_MAIN_P { get { return BASE_WINDOW + "back_to_main_p.png"; } }
        public static string WINDOW_EXIT_FULL_SCREEN { get { return BASE_WINDOW + "exit_full_screen.png"; } }
        public static string WINDOW_EXIT_FULL_SCREEN_H { get { return BASE_WINDOW + "exit_full_screen_h.png"; } }
        public static string WINDOW_EXIT_FULL_SCREEN_P { get { return BASE_WINDOW + "exit_full_screen_p.png"; } }
        public static string WINDOW_FULL_SCREEN { get { return BASE_WINDOW + "full_screen.png"; } }
        public static string WINDOW_FULL_SCREEN_H { get { return BASE_WINDOW + "full_screen_h.png"; } }
        public static string WINDOW_FULL_SCREEN_P { get { return BASE_WINDOW + "full_screen_p.png"; } }
        public static string SHOW_NEW_WINDOW { get { return BASE_WINDOW + "new_window.png"; } }
        public static string SHOW_NEW_WINDOW_H { get { return BASE_WINDOW + "new_window_h.png"; } }
        public static string SHOW_NEW_WINDOW_P { get { return BASE_WINDOW + "new_window_p.png"; } }
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace View.Constants
{
    public class SocialMediaConstants
    {
        public const int SOCIAL_MEDIA_FACEBOOK = 1;      
        public static readonly string FACEBOOK_CLIENT_ID = "477404429103451"; 
        public static readonly string FACEBOOK_SUCCESS_LINK = "https://www.facebook.com/connect/login_success.html";
        public static readonly string FACEBOOK_GRAPH_URL = "https://graph.facebook.com/";
        public static readonly string FACEBOOK_APPLICATION_SECRTE = "6f6fef8701d4b084b5151092ea74bea9";
        public static readonly string FACEBOOK_OAuthUrl = "https://graph.facebook.com/oauth/authorize?";
        public static readonly string FACEBOOK_FeedDialogUrl = "https://www.facebook.com/dialog/feed?";

        public static readonly string GOOGLE_MAP_API_KEY = "AIzaSyCHl88HAklaOu6Q0TSfX5N5eA0vjdlBNuE";
        public static readonly string TWITTER_CONSUMER_KEY = "vAMQyxKXbADWa8tmUrBPf8CP1";
        public static readonly string TWITTER_CONSUMER_SECRET = "7mnPQDU1clTM4jFI9M9xTyDRQYaNBVAKTt6StDH1GbUuNHlJSH";
        public static readonly string DIGITS_CONSUMER_KEY = "wnZ1SukFsxdnr22U4PLiS3T9J"; 

        public static readonly string YOUTUBE_EMBEDDING_URL = "https://www.youtube-nocookie.com/embed/{0}?autoplay=1&rel=0&showinfo=0&fs=0";
        public static readonly string YOUTUBE_NORMAL_URL = "https://www.youtube.com/watch?v={0}";
        public static readonly string YOUTUBE_URL_PATTERN = @"youtu(?:\.be|be\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)";
    }
}

﻿
using System.IO;
using View.ViewModel;
using System;
using View.BindingModels;
using View.Utility.Notification;
using View.Utility.RingPlayer;
using View.Utility.Circle;
using View.Utility.Chat;
using View.Utility.Stream;
using View.Utility.Channel;
namespace View.Constants
{
    public class RingIDSettings
    {
        public static string APP_NAME { get { return "ringID"; } }
        public static string APP_FOLDER = @Environment.CurrentDirectory;
        public static int FRAME_DEFAULT_WIDTH { get { return 990; } }
        public static int FRAME_DEFAULT_HEIGHT { get { return 720; } }
        public static int WINDOW_WIDTH_CALL { get { return 350; } }
        public static int WINDOW_HEIGHT_CALL { get { return 450; } }
        public static int MAIN_RIGHT_CONTENT_WIDTH { get { return 630; } }
        public static int LEFT_FRIEND_LIST_WIDTH { get { return 260; } }
        public static int RIGHT_PANEL_WIDTH { get { return 240; } }
        public static int RIGHT_MENUBAR_WIDTH { get { return 50; } }
        public static int MAIN_MENU_HEIGHT { get { return 50; } }
        public static int MY_NAME_PANEL_HEIGHT { get { return 108; } }

        public static int AVOTAR_HEIGHT { get { return 70; } }
        public static int PROFILE_PIC_DISPLAY_SIZE { get { return 95; } }
        public static int COVER_PIC_DISPLAY_WIDTH { get { return 630; } }
        public static int COVER_PIC_DISPLAY_HEIGHT { get { return 180; } }
        public static int COVER_PIC_DISPLAY_HEIGHT_WITH_BORDER_HEIGHT { get { return 181; } }
        public static int ANONYMOUS_PIC_UPLOAD_SCREEN_WIDTH { get { return 170; } }
        public static int ANONYMOUS_PIC_UPLOAD_SCREEN_HEIGHT { get { return 170; } }
        public static int ANONYMOUS_PIC_MAXIMUM_WIDTH { get { return 400; } }
        public static int COVER_PIC_UPLOAD_SCREEN_WIDTH = 740;
        public static int COVER_PIC_UPLOAD_SCREEN_HEIGHT = 280;

        public static int COVER_PIC_MAXIMUM_WIDTH { get { return 1480; } }
        public static int FRIEND_MF_LCTN_TAB_PANEL_HEIGHT { get { return 220; } }

        /*Profile Pic*/
        public static int PROFILE_PIC_SCREEN_WIDTH { get { return 512; } }
        public static int PROFILE_PIC_CROP_SCREEN_WIDTH_HEIGHT { get { return 320; } }
        public static int PROFILE_PIC_SCALABLE_WIDTH { get { return 720; } }
        public static int PROFILE_PIC_MINIMUM_WIDTH { get { return 100; } }
        public static int PROFILE_PIC_MINIMUM_HEIGHT { get { return 100; } }

        public static string TEMP_PROFILE_IMAGE_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "tempprofileimages";
            }
        }

        public static string TEMP_COVER_IMAGE_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "tempcoverimages";
            }
        }

        public static string TEMP_BOOK_IMAGE_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "tempbookimages";
            }
        }

        public static string TEMP_COMMENT_IMAGE_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "tempcommentimages";
            }
        }

        public static string TEMP_IM_SHARED_FILES_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "IM Shared Files";
            }
        }
        //public static string TEMP_FEED_FILES_FOLDER
        //{
        //    get
        //    {
        //        return APP_FOLDER + Path.DirectorySeparatorChar + "tempfeedfiles";
        //    }
        //}
        public static string TEMP_CHAT_BACKGROUND_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "tempchatbackground";
            }
        }
        public static string TEMP_CHAT_FILES_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "tempchatfiles";
            }
        }
        public static string TEMP_CHAT_FILES_TRANSFER_FOLDER
        {
            get
            {
                return RingIDSettings.TEMP_CHAT_FILES_FOLDER + Path.DirectorySeparatorChar + "FileTransfer";
            }
        }
        public static string TEMP_CAM_IMAGE_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "tempcamfiles";
            }
        }
        public static string RESOURCE_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "resources";
            }
        }
        public static string STICKER_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "stickers";
            }
        }
        public static string CHAT_BG_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "chatbg";
            }
        }
        public static string EMOTICON_FOLDER { get { return RESOURCE_FOLDER + Path.DirectorySeparatorChar + "emoticons"; } }
        public static string AUDIO_FOLDER
        {
            get
            {
                return RESOURCE_FOLDER + Path.DirectorySeparatorChar + "audio";
            }
        }

        public static string TEMP_DOING_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "tempdoingimages";
            }
        }
        public static string TEMP_DOING_MINI_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "tempdoingimages" + Path.DirectorySeparatorChar + Models.Constants.DefaultSettings.D_MINI;
            }
        }
        public static string TEMP_DOING_FULL_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "tempdoingimages" + Path.DirectorySeparatorChar + Models.Constants.DefaultSettings.D_FULL;
            }
        }
        public static string TEMP_MEDIA_IMAGES_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "tempmediaimages";
            }
        }
        public static string TEMP_NEWSPORTAL_IMAGES_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "tempnewsportalimages";
            }
        }
        public static string TEMP_PAGES_IMAGES_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "temppagesimages";
            }
        }
        public static string TEMP_CELEBRITY_IMAGES_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "tempcelebrityimages";
            }
        }
        public static string TEMP_MEDIAPAGE_IMAGES_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "tempmediapageimages";
            }
        }
        public static string TEMP_MAP_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "tempmapimages";
            }
        }
        public static string TEMP_DOWNLOADED_MEDIA_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "tempdownloadedmedias";
            }
        }
        public static string TEMP_MEDIA_TOOLKIT
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + @"tempmediatools";
            }
        }

        public static string STREAM_GIFT_FOLDER
        {
            get
            {
                return APP_FOLDER + Path.DirectorySeparatorChar + "gift";
            }
        }

        #region Sql scripts
        public static string SCRIPT_EMOTICON
        {
            get
            {
                return RESOURCE_FOLDER + Path.DirectorySeparatorChar + "scripts" + Path.DirectorySeparatorChar + "emoticons_insert.db";
            }
        }
        public static string SCRIPT_INSTANCE_MESSAGE
        {
            get
            {
                return RESOURCE_FOLDER + Path.DirectorySeparatorChar + "scripts" + Path.DirectorySeparatorChar + "instant_messages_insert.db";
            }
        }
        public static string SCRIPT_EVENT
        {
            get
            {
                return RESOURCE_FOLDER + Path.DirectorySeparatorChar + "scripts" + Path.DirectorySeparatorChar + "event_insert.db";
            }
        }
        //     public static string SCRIPT_EMOTICON = RESOURCE_FOLDER + Path.DirectorySeparatorChar + "scripts" + Path.DirectorySeparatorChar + "emoticons_insert.db";
        //  public static string SCRIPT_INSTANCE_MESSAGE = RESOURCE_FOLDER + Path.DirectorySeparatorChar + "scripts" + Path.DirectorySeparatorChar + "instant_messages_insert.db";
        #endregion

        public static RingIDViewModel MainViewModel { get { return RingIDViewModel.Instance; } }

        public static WelcomeScreenViewModel WelcomeScreenViewModel { get { return WelcomeScreenViewModel.Instance; } }

        public static ChangableTexts ChangableTexts { get { return RingIDViewModel.Instance.ChangableTexts; } }

        public static VMCircle CircleViewModel { get { return VMCircle.Instance; } }

        public static VMNotification NotificationViewModel { get { return VMNotification.Instance; } }

        //public static RingPlayerViewModel RingPlayerVM { get { return RingPlayerViewModel.Instance; } }

        public static ChatViewModel ChatViewModel { get { return ChatViewModel.Instance; } }

        public static StreamViewModel StreamViewModel { get { return StreamViewModel.Instance; } }

        public static ChannelViewModel ChannelViewModel { get { return ChannelViewModel.Instance; } }

        public static View.Utility.Wallet.WalletViewModel WalletVM { get { return View.Utility.Wallet.WalletViewModel.Instance; } }

        public static Utility.Wallet.WalletCommandModel WalletCM { get { return Utility.Wallet.WalletCommandModel.Instance; } }

        public static NewsFeedViewModel NewsFeedViewModel { get { return NewsFeedViewModel.Instance; } }

        public static OtherViewsVM OtherViewsVM { get { return OtherViewsVM.Instance; } }

        public static CommentViewModel CommentViewModel { get { return CommentViewModel.Instance; } }

        public static System.Drawing.Color[] COLORS = { 
                                                          System.Drawing.Color.FromArgb(245,132,131), //"#f58484"
                                                          System.Drawing.Color.FromArgb(245,211,132), //"#f5d384"
                                                          System.Drawing.Color.FromArgb(175,230,124), //"#afe67c"
                                                          System.Drawing.Color.FromArgb(125,232,152), //"#7de898"
                                                          System.Drawing.Color.FromArgb(132,202,245), //"#84caf5"
                                                          System.Drawing.Color.FromArgb(132,143,245), //"#848ff5"
                                                          System.Drawing.Color.FromArgb(232,132,245), //"#e884f5"
                                                          System.Drawing.Color.FromArgb(245,132,166), //"#f584a6"
                                                      };
    }
}

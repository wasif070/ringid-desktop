﻿
namespace View.Constants
{

    public static class MiddlePanelConstants
    {
        public const int NULL = 0;
        public const int TypeAllFeeds = 1;
        public const int TypeMyProfile = 2;
        public const int TypeFriendChatCall = 3;
        public const int TypeFriendProfile = 4;
        public const int TypeGroupChatCall = 5;
        public const int TypeGroupChatSettings = 6;
        public const int TypeAddFriend = 7;
        public const int TypeDialer = 8;
        public const int TypeCircleMain = 9;
        public const int TypeMyProfilePictureChange = 10;
        public const int TypeMyCoverPictureChange = 11;
        public const int TypeSingleFeedDetails = 12;
        public const int TypeCirclePanel = 13;
        public const int TypeCreateCirclePanel = 14;
        public const int TypeMyAlbum = 15;
        public const int TypeCallMainUI = 16;
        public const int TypeMusicAndVideo = 17;
        public const int TypeMediaFeed = 18;
        public const int TypeFriendChatSettings = 19;
        public const int TypeStickerMarket = 20;
        public const int TypeGroupList = 21;
        public const int TypeNewsPortal = 22;
        public const int TypeRoomList = 23;
        public const int TypeRoomChat = 24;
        public const int TypeRoomChatSettings = 25;
        public const int TypeRoomMemberList = 26;
        public const int TypeNewsPortalProfile = 27;
        public const int TypePages = 28;
        public const int TypePageProfile = 29;
        public const int TypeSavedFeed = 30;
        public const int TypeCelebrity = 31;
        public const int TypeCelebrityProfile = 32;
        public const int TypeMediaCloud = 33;
        public const int TypeMediaCloudProfile = 34;
        public const int TypeFollowingList = 35;
        public const int TypeMarketStickerDetails = 36;
        public const int TypeWallet = 37;
        public const int TypeStreamAndChannel = 38;
        public const int TypeMyChannel = 39;

        public const int TypeMediaCloudSearch = 51;
        public const int TypeNewsPortalSearch = 52;
        public const int TypePagesSearch = 53;
        public const int TypeCelebritySearch = 54;
    }

    public static class WelcomePanelConstants
    {
        public const int TypeWelcome = 0;
        public const int TypeSignup = 1;
        public const int TypeSignin = 2;
        public const int TypeForgotPassword = 3;
        public const int TypeSignupNamePassword = 4;
        public const int TypeSignupSuccess = 5;
        public const int TypeFacebook = 6;
        public const int TypeTwitter = 7;
        public const int TypeForgotPasswordCodeVerify = 8;
        public const int TypePasswordReset = 9;
        public const int TypeProfile = 10;
        public const int TypeDigitsVerification = 11;
        public const int TypeSigninSuccess = 12;
        public const int TypeSigninFaild = 13;
    }

    public static class MediaSearchIndexConstants
    {
        public const int TypeRecentTrending = -2;
        public const int TypeSearchSuggestions = -1;
        public const int TypeFullSearchAll = 0;//o througt 3 indexes
        public const int TypeFullSearchMedia = 1;
        public const int TypeFullSearchAlbums = 2;
        public const int TypeFullSearchTags = 3;
    }

    public static class StreamAndChannelConstants
    {
        public const int TypeStreamAndChannelMainPanel = 0;
        public const int TypeStreamDiscoveryPanel = 1;
        public const int TypeChannelDiscoveryPanel = 2;
        public const int TypeStreamFollowingPanel = 3;
        public const int TypeStreamAndChannelMoreListPanel = 4;
        public const int TypeChannelDetailsPanel = 5;
    }

    public static class MyChannelConstants
    {
        public const int TypeMyChannelAndFollowingPanel = 0;
        public const int TypeMyChannelCreatePanel = 1;
        public const int TypeMyChannelMainPanel = 2;
        public const int TypeMyChannelDetailsPanel = 3;
    }

    public enum FriendFilterEnum : int
    {
        TypeAllFriends = 0,
        TypeFav = 1,
        TypeTop = 2,
        TypeRecentlyAdded = 3,
        Official = 4
    }

    public enum FriendshipStatus : int
    {
        Default = 0,
        IsFriend = 1,
        Incoming = 2,
        Outgoing = 3
    }

    public static class ZIndexConstants
    {
        public const int PopUpViewWrapper = 1;
        public const int BasicImageViewWrapper = 2;
        public const int BasicMediaViewWrapper = 3;
        public const int LikeListViewWrapper = 4;
        public const int MediaListFromAlbumClickWrapper = 5;
        public const int ucCommentPortalPagesImageView = 6;
        public const int ucShareViewWrapper = 7;
        public const int KeptAlwaysMaxZIndex = 10;
    }
}

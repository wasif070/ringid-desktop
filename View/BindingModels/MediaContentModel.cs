﻿using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using View.Utility.DataContainer;
using View.ViewModel;

namespace View.BindingModels
{
    public class MediaContentModel : INotifyPropertyChanged
    {
        public MediaContentModel()
        {
            CurrentInstance = this;
        }
        public void LoadData(JObject jObject)
        {
            //int index = 0;
            if (jObject[JsonKeys.MediaList] != null)
            {
                this.MediaList = new ObservableCollection<SingleMediaModel>();
                //this.FeedMediaList = new ObservableCollection<SingleMediaModel>();
                JArray array = (JArray)jObject[JsonKeys.MediaList];
                foreach (JObject singleMedia in array)
                {
                    Guid contentid = (Guid)singleMedia[JsonKeys.ContentId];
                    SingleMediaModel singleMediaModel = null;
                    if (!MediaDataContainer.Instance.ContentModels.TryGetValue(contentid, out singleMediaModel))
                    {
                        singleMediaModel = new SingleMediaModel();
                        MediaDataContainer.Instance.ContentModels[contentid] = singleMediaModel;
                    }
                    singleMediaModel.LoadData(singleMedia, (int)jObject[JsonKeys.MediaType]);
                    MediaList.Add(singleMediaModel);
                    //if (index < 2)
                    //{
                    //    FeedMediaList.Add(singleMediaModel);
                    //}
                    //index++;
                }
            }
            if (jObject[JsonKeys.AlbumName] != null)
            {
                this.AlbumName = (string)jObject[JsonKeys.AlbumName];
            }
            if (jObject[JsonKeys.MediaAlbumImageURL] != null)
            {
                this.AlbumImageUrl = (string)jObject[JsonKeys.MediaAlbumImageURL];
            }
            if (jObject[JsonKeys.CoverImageUrl] != null)
            {
                this.AlbumImageUrl = (string)jObject[JsonKeys.CoverImageUrl];
            }
            if (jObject[JsonKeys.MediaType] != null)
            {
                this.MediaType = (int)jObject[JsonKeys.MediaType];
            }
            if (jObject[JsonKeys.AlbumId] != null)
            {
                this.AlbumId = (Guid)jObject[JsonKeys.AlbumId];
            }
            if (jObject[JsonKeys.UserTableID] != null)
            {
                this.UserTableID = (long)jObject[JsonKeys.UserTableID];
            }
            if (jObject[JsonKeys.MediaItemsCount] != null)
            {
                this.TotalMediaCount = (int)jObject[JsonKeys.MediaItemsCount];
            }
            else if (jObject[JsonKeys.MemberOrMediaCount] != null)
            {
                this.TotalMediaCount = (int)jObject[JsonKeys.MemberOrMediaCount];
            }
            if (jObject[JsonKeys.Privacy] != null)
            {
                this.MediaPrivacy = (int)jObject[JsonKeys.Privacy];
            }
            if (jObject[JsonKeys.ImageAlbumType] != null)
            {
                IsStaticAlbum = ((int)jObject[JsonKeys.ImageAlbumType] == 1);
            }
        }

        private ObservableCollection<SingleMediaModel> _FeedMediaList;
        public ObservableCollection<SingleMediaModel> FeedMediaList
        {
            get { return _FeedMediaList; }
            set
            {
                if (_FeedMediaList == value) return;
                _FeedMediaList = value;
                OnPropertyChanged("FeedMediaList");
            }
        }

        private ObservableCollection<SingleMediaModel> _MediaList;
        public ObservableCollection<SingleMediaModel> MediaList
        {
            get { return _MediaList; }
            set
            {
                if (_MediaList == value) return;
                _MediaList = value;
                OnPropertyChanged("MediaList");
            }
        }
        private int _MediaType;
        public int MediaType
        {
            get { return _MediaType; }
            set
            {
                if (_MediaType == value) return;
                _MediaType = value;
                OnPropertyChanged("MediaType");
            }
        }
        private bool _IsLoaderOn = false;
        public bool IsLoaderOn
        {
            get { return _IsLoaderOn; }
            set
            {
                if (_IsLoaderOn == value) return;
                _IsLoaderOn = value;
                OnPropertyChanged("IsLoaderOn");
            }
        }
        private Guid _AlbumId;
        public Guid AlbumId
        {
            get { return _AlbumId; }
            set
            {
                if (_AlbumId == value) return;
                _AlbumId = value;
                OnPropertyChanged("AlbumId");
            }
        }
        private bool _IsStaticAlbum;
        public bool IsStaticAlbum
        {
            get { return _IsStaticAlbum; }
            set
            {
                if (_IsStaticAlbum == value) return;
                _IsStaticAlbum = value;
                OnPropertyChanged("IsStaticAlbum");
            }
        }
        private long _UserTableId;
        public long UserTableID
        {
            get { return _UserTableId; }
            set
            {
                if (_UserTableId == value) return;
                _UserTableId = value;
                OnPropertyChanged("UserTableID");
            }
        }

        private string _AlbumName;
        public string AlbumName
        {
            get { return _AlbumName; }
            set
            {
                if (_AlbumName == value) return;
                _AlbumName = value;
                OnPropertyChanged("AlbumName");
            }
        }
        private string _AlbumImageUrl = string.Empty;
        public string AlbumImageUrl
        {
            get { return _AlbumImageUrl; }
            set
            {
                if (_AlbumImageUrl == value) return;
                _AlbumImageUrl = value;
                OnPropertyChanged("AlbumImageUrl");
            }
        }
        private bool _DownloadFailed = false;
        public bool DownloadFailed
        {
            get
            {
                return
                    _DownloadFailed;
            }
            set
            {
                if (value == _DownloadFailed)
                    return;
                _DownloadFailed = value;
                this.OnPropertyChanged("DownloadFailed");
            }
        }
        private int _TotalMediaCount;
        public int TotalMediaCount
        {
            get { return _TotalMediaCount; }
            set
            {
                if (_TotalMediaCount == value) return;
                _TotalMediaCount = value;
                OnPropertyChanged("TotalMediaCount");
            }
        }
        private MediaContentModel _CurrentInstance;
        public MediaContentModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                if (value == _CurrentInstance)
                    return;
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }
        private Visibility _panelVisibility;
        public Visibility PanelVisibility
        {
            get
            {
                return _panelVisibility;
            }
            set
            {
                _panelVisibility = value;
                this.OnPropertyChanged("PanelVisibility");
            }
        }

        private int _mediaPrivacy;
        public int MediaPrivacy
        {
            get
            {
                return _mediaPrivacy;
            }
            set
            {
                if (value == _mediaPrivacy)
                {
                    return;
                }
                _mediaPrivacy = value;
                this.OnPropertyChanged("MediaPrivacy");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

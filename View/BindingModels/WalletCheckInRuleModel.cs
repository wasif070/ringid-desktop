﻿using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace View.BindingModels
{
    public class WalletCheckInRuleModel : INotifyPropertyChanged
    {
        private int dailyCheckInDayTypeID = 0;
        public int DailyCheckInDayTypeID
        {
            get { return dailyCheckInDayTypeID; }
            set { dailyCheckInDayTypeID = value; this.onPropertyChanged("DailyCheckInDayTypeID"); }
        }

        private int coinID = 0;
        public int CoinID
        {
            get { return coinID; }
            set { coinID = value; this.onPropertyChanged("CoinID"); }
        }

        private int coinQuantity = 0;
        public int CoinQuantity
        {
            get { return coinQuantity; }
            set { coinQuantity = value; this.onPropertyChanged("CoinQuantity"); }
        }

        private int dayNumber = 0;
        public int DayNumber
        {
            get { return dayNumber; }
            set { dayNumber = value; this.onPropertyChanged("DayNumber"); }
        }

        public static WalletCheckInRuleModel LoadDataFromJson(JObject dto)
        {
            WalletCheckInRuleModel model = new WalletCheckInRuleModel();
            try
            {
                if (dto[WalletConstants.RSP_KEY_DAILY_CHECK_IN_TYPE_ID] != null)
                {
                    model.DailyCheckInDayTypeID = (int)dto[WalletConstants.RSP_KEY_DAILY_CHECK_IN_TYPE_ID];
                }
                if (dto[WalletConstants.RSP_KEY_COIN_ID] != null)
                {
                    model.CoinID = (int)dto[WalletConstants.RSP_KEY_COIN_ID];
                }
                if (dto[WalletConstants.RSP_KEY_COIN_QUANTITY] != null)
                {
                    model.CoinQuantity = (int)dto[WalletConstants.RSP_KEY_COIN_QUANTITY];
                }
                if (dto[WalletConstants.RSP_KEY_DAY_NUMBER] != null)
                {
                    model.DayNumber = (int)dto[WalletConstants.RSP_KEY_DAY_NUMBER];
                }
            }
            catch (Exception)
            {
                model.DailyCheckInDayTypeID = WalletConstants.CHECK_IN_TYPE_DEFAULT;
                model.CoinID = 0;
                model.CoinQuantity = 0;
                model.DayNumber = 0;
                //throw;
            }
            return model;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void onPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

﻿using Models.Entity;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace View.BindingModels
{
    public class SingleWorkModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChangedNotify(string methodName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(methodName));
            }
        }

        private string _WPosition;
        public string WPosition
        {
            get
            {
                return _WPosition;
            }
            set
            {
                if (value == _WPosition)
                    return;
                _WPosition = value;
                this.OnPropertyChangedNotify("WPosition");
            }
        }
        private string _WDescription;
        public string WDescription
        {
            get
            {
                return _WDescription;
            }
            set
            {
                if (value == _WDescription)
                    return;
                _WDescription = value;
                this.OnPropertyChangedNotify("WDescription");
            }
        }
        private string _WCompanyName;
        public string WCompanyName
        {
            get
            {
                return _WCompanyName;
            }
            set
            {
                if (value == _WCompanyName)
                    return;
                _WCompanyName = value;
                this.OnPropertyChangedNotify("WCompanyName");
            }
        }
        private string _WCity;
        public string WCity
        {
            get
            {
                return _WCity;
            }
            set
            {
                if (value == _WCity)
                    return;
                _WCity = value;
                this.OnPropertyChangedNotify("WCity");
            }
        }
        private long _WFromTime;
        public long WFromTime
        {
            get
            {
                return _WFromTime;
            }
            set
            {
                if (value == _WFromTime)
                    return;
                _WFromTime = value;
                this.OnPropertyChangedNotify("WFromTime");
            }
        }
        private long _WToTime;
        public long WToTime
        {
            get
            {
                return _WToTime;
            }
            set
            {
                if (value == _WToTime)
                    return;
                _WToTime = value;
                this.OnPropertyChangedNotify("WToTime");
            }
        }

        private Guid _WId;
        public Guid WId
        {
            get
            {
                return _WId;
            }
            set
            {
                if (value == _WId)
                    return;
                _WId = value;
                this.OnPropertyChangedNotify("WId");
            }
        }

        ///new properties
        private string _WFromTime_ViewMode;
        public string WFromTime_ViewMode
        {
            get
            {
                return _WFromTime_ViewMode;
            }
            set
            {
                if (value == _WFromTime_ViewMode)
                    return;
                _WFromTime_ViewMode = value;
                this.OnPropertyChangedNotify("WFromTime_ViewMode");
            }
        }
        private string _WToTime_ViewMode;
        public string WToTime_ViewMode
        {
            get
            {
                return _WToTime_ViewMode;
            }
            set
            {
                if (value == _WToTime_ViewMode)
                    return;
                _WToTime_ViewMode = value;
                this.OnPropertyChangedNotify("WToTime_ViewMode");
            }
        }
        private string _WFromTime_EditMode;
        public string WFromTime_EditMode
        {
            get
            {
                return _WFromTime_EditMode;
            }
            set
            {
                if (value == _WFromTime_EditMode)
                    return;
                _WFromTime_EditMode = value;
                this.OnPropertyChangedNotify("WFromTime_EditMode");
            }
        }
        private string _WToTime_EditMode;
        public string WToTime_EditMode
        {
            get
            {
                return _WToTime_EditMode;
            }
            set
            {
                if (value == _WToTime_EditMode)
                    return;
                _WToTime_EditMode = value;
                this.OnPropertyChangedNotify("WToTime_EditMode");
            }
        }
        /*private Visibility _WVisibilityEditMode;
        public Visibility WVisibilityEditMode
        {
            get
            {
                return _WVisibilityEditMode;
            }
            set
            {
                if (value == _WVisibilityEditMode)
                    return;
                _WVisibilityEditMode = value;
                this.OnPropertyChangedNotify("WVisibilityEditMode");
            }
        }*/

        private Visibility _SettingAboutButtonVisibilty = Visibility.Hidden;
        public Visibility SettingAboutButtonVisibilty
        {
            get
            {
                return _SettingAboutButtonVisibilty;
            }
            set
            {
                if (value == _SettingAboutButtonVisibilty)
                    return;
                _SettingAboutButtonVisibilty = value;
                this.OnPropertyChangedNotify("SettingAboutButtonVisibilty");
            }
        }

        private Visibility _WVisibilityViewMode;
        public Visibility WVisibilityViewMode
        {
            get
            {
                return _WVisibilityViewMode;
            }
            set
            {
                if (value == _WVisibilityViewMode)
                    return;
                _WVisibilityViewMode = value;
                this.OnPropertyChangedNotify("WVisibilityViewMode");
            }
        }
        private bool _IsCurrentlyWorkingChecked;
        public bool IsCurrentlyWorkingChecked
        {
            get
            {
                return _IsCurrentlyWorkingChecked;
            }
            set
            {
                if (value == _IsCurrentlyWorkingChecked)
                    return;
                _IsCurrentlyWorkingChecked = value;
                this.OnPropertyChangedNotify("IsCurrentlyWorkingChecked");
            }
        }
        private Visibility _WVisibilityDate;
        public Visibility WVisibilityDate
        {
            get
            {
                return _WVisibilityDate;
            }
            set
            {
                if (value == _WVisibilityDate)
                    return;
                _WVisibilityDate = value;
                this.OnPropertyChangedNotify("WVisibilityDate");
            }
        }
        
        //Company errorString
        private string _WCompanyErrorString = string.Empty;
        public string WCompanyErrorString
        {
            get
            {
                return _WCompanyErrorString;
            }
            set
            {
                if (value == _WCompanyErrorString)
                    return;
                _WCompanyErrorString = value;
                this.OnPropertyChangedNotify("WCompanyErrorString");
            }
        }
        //Position errorString
        private string _WPostionErrorString = string.Empty;
        public string WPostionErrorString
        {
            get
            {
                return _WPostionErrorString;
            }
            set
            {
                if (value == _WPostionErrorString)
                    return;
                _WPostionErrorString = value;
                this.OnPropertyChangedNotify("WPostionErrorString");
            }
        }
        //FromDate errorsTring
        private string _WFromDateErrorString = string.Empty;
        public string WFromDateErrorString
        {
            get
            {
                return _WFromDateErrorString;
            }
            set
            {
                if (value == _WFromDateErrorString)
                    return;
                _WFromDateErrorString = value;
                this.OnPropertyChangedNotify("WFromDateErrorString");
            }
        }
        //ToDate errorString
        private string _WToDateErrorString = string.Empty;
        public string WToDateErrorString
        {
            get
            {
                return _WToDateErrorString;
            }
            set
            {
                if (value == _WToDateErrorString)
                    return;
                _WToDateErrorString = value;
                this.OnPropertyChangedNotify("WToDateErrorString");
            }
        }
        public void LoadData(WorkDTO work)
        {
            this.WId = work.Id;
            this.WCity = work.City;
            this.WPosition = work.Position;
            this.WCompanyName = work.CompanyName;
            this.WDescription = work.Description;
            //this.WVisibilityEditMode = Visibility.Collapsed;
            this.WVisibilityViewMode = Visibility.Visible;
            //this.WErrorString = "";
            this.WCompanyErrorString = string.Empty;
            this.WPostionErrorString = string.Empty;
            this.WFromDateErrorString = string.Empty;
            this.WToDateErrorString = string.Empty;
            this.WFromTime = work.FromTime;
            this.WToTime = work.ToTime;
            if (work.FromTime == 0)
            {
                this.WVisibilityDate = Visibility.Collapsed;
            }
            else
            {
                this.WVisibilityDate = Visibility.Visible;
                DateTime ft = ModelUtility.DateTimeFromMillisSince1970(work.FromTime);
                this.WFromTime_EditMode = ft.ToShortDateString();
                this.WFromTime_ViewMode = ft.ToString("dd-MMM-yyyy");
                if (work.ToTime == 0)
                {
                    this.IsCurrentlyWorkingChecked = true;
                    this.WToTime_EditMode = DateTime.Now.ToShortDateString();
                    this.WToTime_ViewMode = "Present";
                }
                else
                {
                    this.IsCurrentlyWorkingChecked = false;
                    DateTime tt = ModelUtility.DateTimeFromMillisSince1970(work.ToTime);
                    this.WToTime_EditMode = tt.ToShortDateString();
                    this.WToTime_ViewMode = tt.ToString("dd-MMM-yyyy");
                }
            }
        }

    }
}

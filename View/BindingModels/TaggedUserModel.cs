﻿using Models.Entity;
using System.ComponentModel;

namespace View.BindingModels
{
    public class TaggedUserModel : BaseUserProfileModel
    {
        //public TaggedUserModel()
        //{
        //    CurrentInstance = this;
        //}
        //private string _FullName;
        //public string FullName
        //{
        //    get { return _FullName; }
        //    set
        //    {
        //        if (value == _FullName)
        //            return;
        //        _FullName = value;
        //        this.OnPropertyChanged("FullName");
        //    }
        //}

        //private string _ProfileImage;
        //public string ProfileImage
        //{
        //    get { return _ProfileImage; }
        //    set
        //    {
        //        if (value == _ProfileImage)
        //            return;
        //        _ProfileImage = value;
        //        this.OnPropertyChanged("ProfileImage");
        //    }
        //}

        private int _Index;
        public int Index
        {
            get { return _Index; }
            set
            {
                if (value == _Index)
                    return;

                _Index = value;
                this.OnPropertyChanged("Index");
            }
        }

        //private long _UserTableId;
        //public long UserTableID
        //{
        //    get { return _UserTableId; }
        //    set
        //    {
        //        if (value == _UserTableId)
        //            return;

        //        _UserTableId = value;
        //        this.OnPropertyChanged("UserTableID");
        //    }
        //}

        //private long _UserIdentity;
        //public long UserIdentity
        //{
        //    get { return _UserIdentity; }
        //    set
        //    {
        //        if (value == _UserIdentity)
        //            return;
        //        _UserIdentity = value;
        //        this.OnPropertyChanged("UserIdentity");
        //    }
        //}

        //private TaggedUserModel _CurrentInstance;
        //public TaggedUserModel CurrentInstance
        //{
        //    get { return _CurrentInstance; }
        //    set
        //    {
        //        if (value == _CurrentInstance)
        //            return;
        //        _CurrentInstance = value;
        //        this.OnPropertyChanged("CurrentInstance");
        //    }
        //}
        //public event PropertyChangedEventHandler PropertyChanged;
        //public void OnPropertyChanged(string propertyName)
        //{
        //    PropertyChangedEventHandler handler = PropertyChanged;
        //    if (handler != null)
        //    {
        //        handler(this, new PropertyChangedEventArgs(propertyName));
        //    }
        //}

        //public void LoadData(StatusTagDTO statusTagDTo)
        //{
        //    FullName = statusTagDTo.FullName;
        //    UserTableID = statusTagDTo.UserTableID;
        //    //UserIdentity = statusTagDTo.UserIdentity;
        //    Index = statusTagDTo.Index;
        //}
    }
}

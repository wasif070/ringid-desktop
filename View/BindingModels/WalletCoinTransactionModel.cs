﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace View.BindingModels
{
    public class WalletCoinTransactionModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) { handler(this, new PropertyChangedEventArgs(propertyName)); }
        }

        private long transactionID = 0;
        public long TransactionID
        {
            get { return transactionID; }
            set { transactionID = value; this.OnPropertyChanged("TransactionID"); }
        }

        private long transactionDate = 0;
        public long TransactionDate
        {
            get { return transactionDate; }
            set { transactionDate = value; this.OnPropertyChanged("TransactionDate"); }
        }

        private int transactionTypeID = 0;
        public int TransactionTypeID
        {
            get { return transactionTypeID; }
            set { transactionTypeID = value; this.OnPropertyChanged("TransactionTypeID"); }
        }

        private int amount = 0;
        public int Amount
        {
            get { return amount; }
            set { amount = value; this.OnPropertyChanged("Amount"); }
        }

        private long otherUserID = 0;
        public long OtherUserID
        {
            get { return otherUserID; }
            set { otherUserID = value; this.OnPropertyChanged("OtherUserID"); }
        }

        private string transactionRemarks = string.Empty;
        public string TransactionRemarks
        {
            get { return transactionRemarks; }
            set { transactionRemarks = value; this.OnPropertyChanged("TransactionRemarks"); }
        }

        private int coinID = 0;
        public int CoinID
        {
            get { return coinID; }
            set { coinID = value; this.OnPropertyChanged("CoinID"); }
        }

        private string ringID = string.Empty;
        public string RingID
        {
            get { return ringID; }
            set { ringID = value; this.OnPropertyChanged("RingID"); }
        }

        private UserShortInfoModel shortInfoModel = new UserShortInfoModel();
        public UserShortInfoModel ShortInfoModel
        {
            get { return shortInfoModel; }
            set { shortInfoModel = value; this.OnPropertyChanged("ShortInfoModel"); }
        }

    }
}

﻿using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace View.BindingModels
{
    public class AlbumModel : INotifyPropertyChanged
    {
        #region "Property"

        private Guid _AlbumId;
        public Guid AlbumId
        {
            get
            {
                return _AlbumId;
            }
            set
            {
                if (value == _AlbumId)
                    return;

                _AlbumId = value;
                this.OnPropertyChanged("AlbumId");
            }
        }

        private string _AlbumCoverImageUrl;
        public string AlbumCoverImageUrl
        {
            get
            {
                return _AlbumCoverImageUrl;
            }
            set
            {
                if (value == _AlbumCoverImageUrl)
                    return;

                _AlbumCoverImageUrl = value;
                this.OnPropertyChanged("AlbumCoverImageUrl");
            }
        }

        private string _AlbumName;
        public string AlbumName
        {
            get
            {
                return _AlbumName;
            }
            set
            {
                if (value == _AlbumName)
                    return;

                _AlbumName = value;
                this.OnPropertyChanged("AlbumName");
            }
        }

        private int _totalImagesInAlbum;
        public int TotalImagesInAlbum
        {
            get
            {
                return _totalImagesInAlbum;
            }
            set
            {
                if (value == _totalImagesInAlbum)
                    return;
                _totalImagesInAlbum = value;
                this.OnPropertyChanged("TotalImagesInAlbum");
            }
        }

        private ObservableCollection<ImageModel> _ImageList;
        public ObservableCollection<ImageModel> ImageList
        {
            get { return _ImageList; }
            set
            {
                if (_ImageList == value) return;
                _ImageList = value;
                OnPropertyChanged("ImageList");
            }
        }
        
        private long _UserTableId;
        public long UserTableID
        {
            get { return _UserTableId; }
            set
            {
                if (_UserTableId == value) return;
                _UserTableId = value;
                OnPropertyChanged("UserTableID");
            }
        }
        
        private bool _IsLoadMore = false;
        public bool IsLoadMore
        {
            get
            {
                return _IsLoadMore;
            }
            set
            {
                _IsLoadMore = value;
                this.OnPropertyChanged("IsLoadMore");
            }
        }

        private AlbumModel _CurrentInstance;
        public AlbumModel CurrentInstance
        {
            get
            {
                return _CurrentInstance;
            }
            set
            {
                if (value == _CurrentInstance)
                    return;
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }

        private int _albumPrivacy;
        public int AlbumPrivacy
        {
            get
            {
                return _albumPrivacy;
            }
            set
            {
                if(value == _albumPrivacy)
                {
                    return;
                }
                _albumPrivacy = value;
                this.OnPropertyChanged("AlbumPrivacy");
            }
        }

        private int _imageAlbumType;
        public int ImageAlbumType
        {
            get
            {
                return _imageAlbumType;
            }
            set
            {
                if(value == _imageAlbumType)
                {
                    return;
                }
                _imageAlbumType = value;
                this.OnPropertyChanged("ImageAlbumType");

            }
        }
        private bool _NoImageFound = false;
        public bool NoImageFound
        {
            get
            {
                return _NoImageFound;
            }
            set
            {
                if (value == _NoImageFound)
                    return;
                _NoImageFound = value;
                this.OnPropertyChanged("NoImageFound");
            }
        }

        #endregion

        #region "Constructor"
        public AlbumModel()
        {
            CurrentInstance = this;            
        }

        public AlbumModel(JObject jObject)
        {
            CurrentInstance = this;
            LoadData(jObject);
        }
        #endregion

        #region "Utility Methods"
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void LoadData(JObject jObject)
        {
            ImageList = new ObservableCollection<ImageModel>();
            ImageList.Add(new ImageModel() { IsLoadMore = true });

            if (jObject[JsonKeys.AlbumId] != null)
                AlbumId = (Guid)jObject[JsonKeys.AlbumId];
            if (jObject[JsonKeys.AlbumName] != null)
                AlbumName = (string)jObject[JsonKeys.AlbumName];
            if (jObject[JsonKeys.MemberOrMediaCount] != null)
                TotalImagesInAlbum = (int)jObject[JsonKeys.MemberOrMediaCount];
            if (jObject[JsonKeys.CoverImageUrl] != null)
                AlbumCoverImageUrl = (string)jObject[JsonKeys.CoverImageUrl];
            if (jObject[JsonKeys.Privacy] != null)
                AlbumPrivacy = (int)jObject[JsonKeys.Privacy];
            if (jObject[JsonKeys.ImageAlbumType] != null)
                ImageAlbumType = (int)jObject[JsonKeys.ImageAlbumType];
        }
        #endregion
    }
}

﻿using Models.Entity;
using System;
using System.ComponentModel;
using System.Windows;

namespace View.BindingModels
{
    public class SingleSkillModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChangedNotify(string methodName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(methodName));
            }
        }
        private string _SSkill;
        public string SSkill
        {
            get
            {
                return _SSkill;
            }
            set
            {
                if (value == _SSkill)
                    return;
                _SSkill = value;
                this.OnPropertyChangedNotify("SSkill");
            }
        }
        private string _SDescription;
        public string SDescription
        {
            get
            {
                return _SDescription;
            }
            set
            {
                if (value == _SDescription)
                    return;
                _SDescription = value;
                this.OnPropertyChangedNotify("SDescription");
            }
        }
        private Guid _SKId;
        public Guid SKId
        {
            get
            {
                return _SKId;
            }
            set
            {
                if (value == _SKId)
                    return;
                _SKId = value;
                this.OnPropertyChangedNotify("SKId");
            }
        }
        /*private Visibility _SVisibilityEditMode;
        public Visibility SVisibilityEditMode
        {
            get
            {
                return _SVisibilityEditMode;
            }
            set
            {
                if (value == _SVisibilityEditMode)
                    return;
                _SVisibilityEditMode = value;
                this.OnPropertyChangedNotify("SVisibilityEditMode");
            }
        }*/
        private Visibility _SettingAboutButtonVisibilty = Visibility.Hidden;
        public Visibility SettingAboutButtonVisibilty
        {
            get
            {
                return _SettingAboutButtonVisibilty;
            }
            set
            {
                if (value == _SettingAboutButtonVisibilty)
                    return;
                _SettingAboutButtonVisibilty = value;
                this.OnPropertyChangedNotify("SettingAboutButtonVisibilty");
            }
        }
        private Visibility _SVisibilityViewMode;
        public Visibility SVisibilityViewMode
        {
            get
            {
                return _SVisibilityViewMode;
            }
            set
            {
                if (value == _SVisibilityViewMode)
                    return;
                _SVisibilityViewMode = value;
                this.OnPropertyChangedNotify("SVisibilityViewMode");
            }
        }
        private string _SErrorString = string.Empty;
        public string SErrorString
        {
            get
            {
                return _SErrorString;
            }
            set
            {
                if (value == _SErrorString)
                    return;
                _SErrorString = value;
                this.OnPropertyChangedNotify("SErrorString");
            }
        }

        public void LoadData(SkillDTO skill)
        {
            this.SKId = skill.Id;
            this.SSkill = skill.SkillName;
            this.SDescription = skill.Description;
            //this.SVisibilityEditMode = Visibility.Collapsed;
            this.SVisibilityViewMode = Visibility.Visible;
            this.SErrorString = "";
        }
    }
}

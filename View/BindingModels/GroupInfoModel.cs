﻿using Models.Constants;
using Models.DAO;
using Models.Entity;
using View.Utility;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using View.ViewModel;
using System.Diagnostics;
using log4net;
using System.Runtime;
using System.Collections.Concurrent;
using System.Windows;
using System.Windows.Media;

namespace View.BindingModels
{

    public class GroupInfoModel : INotifyPropertyChanged, IMemberChanged
    {
        private readonly ILog log = LogManager.GetLogger(typeof(GroupInfoModel).Name);

        #region Constructor

        public GroupInfoModel()
        {
            _CurrentInstance = this;
        }

        public GroupInfoModel(GroupInfoDTO groupInfoDTO)
            : this()
        {
            LoadData(groupInfoDTO);
        }

        #endregion Constructor

        #region Property

        private GroupInfoModel _CurrentInstance;
        public GroupInfoModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }

        private long _GroupID;
        public long GroupID
        {
            get { return _GroupID; }
            set
            {
                if (value == _GroupID)
                    return;

                _GroupID = value;
                this.OnPropertyChanged("GroupID");
            }
        }

        private string _GroupName;
        public string GroupName
        {
            get { return _GroupName; }
            set
            {
                if (value == _GroupName)
                    return;

                _GroupName = value;
                this.OnPropertyChanged("GroupName");
            }
        }

        private int _NumberOfMembers;
        public int NumberOfMembers
        {
            get { return _NumberOfMembers; }
            set
            {
                if (value == _NumberOfMembers)
                    return;

                _NumberOfMembers = value;
                this.OnPropertyChanged("NumberOfMembers");
            }
        }

        private string _GroupProfileImage;
        public string GroupProfileImage
        {
            get { return _GroupProfileImage; }
            set
            {
                if (value == _GroupProfileImage)
                    return;

                _GroupProfileImage = value;
                this.OnPropertyChanged("GroupProfileImage");
            }
        }

        private bool _ImSoundEnabled = true;
        public bool ImSoundEnabled
        {
            get { return _ImSoundEnabled; }
            set
            {
                if (value == _ImSoundEnabled)
                    return;

                _ImSoundEnabled = value;
                this.OnPropertyChanged("ImSoundEnabled");
            }
        }

        private bool _ImNotificationEnabled = true;
        public bool ImNotificationEnabled
        {
            get { return _ImNotificationEnabled; }
            set
            {
                if (value == _ImNotificationEnabled)
                    return;

                _ImNotificationEnabled = value;
                this.OnPropertyChanged("ImNotificationEnabled");
            }
        }

        private bool _ImPopupEnabled = true;
        public bool ImPopupEnabled
        {
            get { return _ImPopupEnabled; }
            set
            {
                if (value == _ImPopupEnabled)
                    return;

                _ImPopupEnabled = value;
                this.OnPropertyChanged("ImPopupEnabled");
            }
        }

        private bool _IsPartial;
        public bool IsPartial
        {
            get { return _IsPartial; }
            set
            {
                if (value == _IsPartial)
                    return;

                _IsPartial = value;
                this.OnPropertyChanged("IsPartial");
            }
        }

        private string _ChatBgUrl;
        public string ChatBgUrl
        {
            get { return _ChatBgUrl; }
            set
            {
                if (value == _ChatBgUrl)
                    return;

                _ChatBgUrl = value;
                this.OnPropertyChanged("ChatBgUrl");
            }
        }

        private string _EventChatBgUrl;
        public string EventChatBgUrl
        {
            get { return _EventChatBgUrl; }
            set
            {
                if (value == _EventChatBgUrl)
                    return;

                _EventChatBgUrl = value;
                this.OnPropertyChanged("EventChatBgUrl");
            }
        }

        private string _ChatMinPacketID;
        public string ChatMinPacketID
        {
            get { return _ChatMinPacketID; }
            set
            {
                if (value == _ChatMinPacketID)
                    return;

                _ChatMinPacketID = value;
                this.OnPropertyChanged("ChatMinPacketID");
            }
        }

        private string _ChatMaxPacketID;
        public string ChatMaxPacketID
        {
            get { return _ChatMaxPacketID; }
            set
            {
                if (value == _ChatMaxPacketID)
                    return;

                _ChatMaxPacketID = value;
                this.OnPropertyChanged("ChatMaxPacketID");
            }
        }

        private int _AccessType;
        public int AccessType
        {
            get { return _AccessType; }
            set
            {
                if (value == _AccessType)
                    return;

                _AccessType = value;
                this.OnPropertyChanged("AccessType");
            }
        }

        private bool _ChatHistoryNotFound;
        public bool ChatHistoryNotFound
        {
            get { return _ChatHistoryNotFound; }
            set
            {
                if (value == _ChatHistoryNotFound)
                    return;

                _ChatHistoryNotFound = value;
                this.OnPropertyChanged("ChatHistoryNotFound");
            }
        }

        private bool _IsCallChatPanelOpened;
        public bool IsCallChatPanelOpened
        {
            get { return _IsCallChatPanelOpened; }
            set
            {
                if (value == _IsCallChatPanelOpened)
                    return;

                _IsCallChatPanelOpened = value;
                this.OnPropertyChanged("IsCallChatPanelOpened");
            }
        }

        private bool _IsProfilePanelOpened;
        public bool IsProfilePanelOpened
        {
            get { return _IsProfilePanelOpened; }
            set
            {
                if (value == _IsProfilePanelOpened)
                    return;

                _IsProfilePanelOpened = value;
                this.OnPropertyChanged("IsProfilePanelOpened");
            }
        }
        
        private bool _SingleGroupPanelFocusable = false;
        public bool SingleGroupPanelFocusable
        {
            get
            {
                return _SingleGroupPanelFocusable;
            }

            set
            {
                _SingleGroupPanelFocusable = value;
                OnPropertyChanged("SingleGroupPanelFocusable");
            }
        }

        private ObservableDictionary<long, GroupMemberInfoModel> _MemberInfoDictionary = new ObservableDictionary<long, GroupMemberInfoModel>();
        public ObservableDictionary<long, GroupMemberInfoModel> MemberInfoDictionary
        {
            get { return _MemberInfoDictionary; }
            set { _MemberInfoDictionary = value; }
        }

        private ObservableCollection<GroupMemberInfoModel> _MemberList = new ObservableCollection<GroupMemberInfoModel>();
        public ObservableCollection<GroupMemberInfoModel> MemberList
        {
            get { return _MemberList; }
            set { _MemberList = value; }
        }

        private ObservableCollection<GroupMemberInfoModel> _AllMemberList = new ObservableCollection<GroupMemberInfoModel>();
        public ObservableCollection<GroupMemberInfoModel> AllMemberList
        {
            get { return _AllMemberList; }
            set { _AllMemberList = value; }
        }

        private ObservableCollection<GroupMemberInfoModel> _AdminMemberList = new ObservableCollection<GroupMemberInfoModel>();
        public ObservableCollection<GroupMemberInfoModel> AdminMemberList
        {
            get { return _AdminMemberList; }
            set { _AdminMemberList = value; }
        }

        #endregion Property

        #region Utility Method

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event MemberChangedEventHandler MemberChanged;
        public void OnMemberChanged(GroupMemberInfoModel model, NotifyCollectionChangedAction type)
        {
            MemberChangedEventHandler handler = MemberChanged;
            if (handler != null)
            {
                handler(this, new MemberChangedEventArgs(model, type));
            }
        }

        public void LoadData(GroupInfoDTO groupInfoDTO)
        {
            if (groupInfoDTO != null)
            {
                this.GroupID = groupInfoDTO.GroupID;
                this.GroupName = groupInfoDTO.GroupName;
                this.NumberOfMembers = groupInfoDTO.NumberOfMembers;
                this.GroupProfileImage = groupInfoDTO.GroupProfileImage;
                this.IsPartial = groupInfoDTO.IsPartial;
                this.ChatBgUrl = groupInfoDTO.ChatBgUrl;
                this.ChatMinPacketID = groupInfoDTO.ChatMinPacketID;
                this.ChatMaxPacketID = groupInfoDTO.ChatMaxPacketID;
                this.ImSoundEnabled = groupInfoDTO.ImSoundEnabled;
                this.ImNotificationEnabled = groupInfoDTO.ImNotificationEnabled;
                this.ImPopupEnabled = groupInfoDTO.ImPopupEnabled;
                if (MemberChanged == null)
                {
                    MemberChanged += MemberInformationChanged;
                }
            }
        }

        public void LoadMemberData(GroupMemberInfoDTO memberInfoDTO)
        {
            try
            {
                GroupMemberInfoModel memberModel = MemberInfoDictionary.TryGetValue(memberInfoDTO.UserTableID);
                if (memberModel == null)
                {
                    memberModel = new GroupMemberInfoModel(memberInfoDTO, MemberChanged);
                    MemberInfoDictionary[memberInfoDTO.UserTableID] = memberModel;
                }
                else
                {
                    memberModel.LoadData(memberInfoDTO);
                }

                if (memberModel.BasicInfoModel == null)
                {
                    memberModel.BasicInfoModel = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(memberModel.UserTableID, memberModel.RingID, memberModel.FullName);
                }
            }
            catch (Exception ex)
            {
                if (DefaultSettings.DEBUG)
                    log.Error("Error: LoadMemberData() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void AddMemberData(GroupMemberInfoDTO memberInfoDTO)
        {
            try
            {
                GroupMemberInfoModel memberModel = new GroupMemberInfoModel(memberInfoDTO, MemberChanged);
                MemberInfoDictionary[memberModel.UserTableID] = memberModel;

                if (memberModel.BasicInfoModel == null)
                {
                    memberModel.BasicInfoModel = RingIDViewModel.Instance.GetUserBasicInfoModelNotNullable(memberModel.UserTableID, memberModel.RingID, memberModel.FullName);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: LoadMemberData() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public GroupMemberInfoModel RemoveMemberData(long userIdentity)
        {
            GroupMemberInfoModel model = null;
            try
            {
                if (MemberInfoDictionary.ContainsKey(userIdentity))
                {
                    model = MemberInfoDictionary.TryGetValue(userIdentity);
                    model.MemberAccessType = ChatConstants.MEMBER_TYPE_NOT_MEMBER;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: RemoveMemberData() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            return model;
        }

        public void RemoveMemberData(GroupMemberInfoModel model)
        {
            model.MemberAccessType = ChatConstants.MEMBER_TYPE_NOT_MEMBER; 
        }

        private void MemberInformationChanged(object sender, MemberChangedEventArgs e)
        {
            try
            {
                switch (e.ChangeType)
                {
                    case NotifyCollectionChangedAction.Replace:
                    case NotifyCollectionChangedAction.Add:
                        {
                            if (e.MemberModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                            {
                                AccessType = e.MemberModel.MemberAccessType;
                            }

                            if (IsCallChatPanelOpened)
                            {
                                bool state = MemberList.Where(P => P.UserTableID == e.MemberModel.UserTableID).FirstOrDefault() != null;
                                if (state)
                                {
                                    if (e.MemberModel.MemberAccessType == ChatConstants.MEMBER_TYPE_NOT_MEMBER)
                                    {
                                        MemberList.InvokeRemove(e.MemberModel);
                                    }
                                }
                                else
                                {
                                    if (e.MemberModel.MemberAccessType != ChatConstants.MEMBER_TYPE_NOT_MEMBER)
                                    {
                                        if (e.MemberModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                                        {
                                            MemberList.InvokeInsert(0, e.MemberModel);
                                        }
                                        else
                                        {
                                            GroupMemberInfoModel tempModel = MemberList.FirstOrDefault();
                                            if (tempModel != null && tempModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                                            {
                                                MemberList.InvokeInsert(1, e.MemberModel);
                                            }
                                            else
                                            {
                                                MemberList.InvokeInsert(0, e.MemberModel);
                                            }
                                        }
                                    }
                                }
                            }

                            if (IsProfilePanelOpened)
                            {
                                bool state = AdminMemberList.Where(P => P.UserTableID == e.MemberModel.UserTableID).FirstOrDefault() != null;
                                if (e.MemberModel.MemberAccessType == ChatConstants.MEMBER_TYPE_ADMIN)
                                {
                                    if (!state)
                                    {
                                        AdminMemberList.InvokeAdd(e.MemberModel);
                                    }
                                }
                                else
                                {
                                    if (state)
                                    {
                                        AdminMemberList.InvokeRemove(e.MemberModel);
                                    }
                                }

                                state = AllMemberList.Where(P => P.UserTableID == e.MemberModel.UserTableID).FirstOrDefault() != null;
                                if (e.MemberModel.MemberAccessType == ChatConstants.MEMBER_TYPE_NOT_MEMBER)
                                {
                                    if (state)
                                    {
                                        AllMemberList.InvokeRemove(e.MemberModel);
                                    }
                                }
                                else
                                {
                                    if (!state)
                                    {
                                        AllMemberList.InvokeAdd(e.MemberModel);
                                    }
                                }
                            }
                        }
                        break;
                    //case NotifyCollectionChangedAction.Remove:
                    //    {
                    //        if (e.MemberModel.UserTableID == DefaultSettings.LOGIN_TABLE_ID)
                    //        {
                    //            AccessType = ChatConstants.MEMBER_TYPE_NOT_MEMBER;
                    //        }
                    //        if (IsCallChatPanelOpened)
                    //        {
                    //            MemberList.InvokeRemove(e.MemberModel);
                    //        }
                    //        if (IsProfilePanelOpened)
                    //        {
                    //            AdminMemberList.InvokeRemove(e.MemberModel);
                    //            AllMemberList.InvokeRemove(e.MemberModel);
                    //        }
                    //    }
                    //    break;
                }
            }
            catch (Exception ex)
            {
                if (DefaultSettings.DEBUG)
                    log.Error("Error: MemberInformationChanged() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        #endregion Utility Method
    }

    public class GroupMemberInfoModel : INotifyPropertyChanged
    {

        #region Constructor

        public GroupMemberInfoModel()
        {
            _CurrentInstance = this;
        }

        public GroupMemberInfoModel(GroupMemberInfoDTO groupMemberInfoDTO, MemberChangedEventHandler memberChanged)
            : this()
        {
            this.MemberChanged = memberChanged;
            LoadData(groupMemberInfoDTO);
        }

        #endregion Constructor

        #region Property

        private GroupMemberInfoModel _CurrentInstance;
        public GroupMemberInfoModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }

        private UserBasicInfoModel _BasicInfoModel;
        public UserBasicInfoModel BasicInfoModel
        {
            get { return _BasicInfoModel; }
            set
            {
                _BasicInfoModel = value;
                this.OnPropertyChanged("BasicInfoModel");
            }
        }

        private long _UserTableID;
        public long UserTableID
        {
            get { return _UserTableID; }
            set
            {
                if (value == _UserTableID)
                    return;

                _UserTableID = value;
                this.OnPropertyChanged("UserTableID");
            }
        }

        private long _RingID;
        public long RingID
        {
            get { return _RingID; }
            set
            {
                if (value == _RingID)
                    return;

                _RingID = value;
                this.OnPropertyChanged("RingID");
            }
        }

        private string _FullName;
        public string FullName
        {
            get { return _FullName; }
            set
            {
                if (value == _FullName)
                    return;

                _FullName = value;
                this.OnPropertyChanged("FullName");
            }
        }

        private int _MemberAccessType;
        public int MemberAccessType
        {
            get { return _MemberAccessType; }
            set
            {
                int prevType = _MemberAccessType;
                if (value == prevType)
                    return;

                _MemberAccessType = value;
                this.OnPropertyChanged("MemberAccessType");
                this.OnMemberChanged(prevType);
            }
        }

        private long _MemberAddedBy;
        public long MemberAddedBy
        {
            get { return _MemberAddedBy; }
            set
            {
                if (value == _MemberAddedBy)
                    return;

                _MemberAddedBy = value;
                this.OnPropertyChanged("MemberAddedBy");
            }
        }

        private long _UpdateTime;
        public long UpdateTime
        {
            get { return _UpdateTime; }
            set
            {
                if (value == _UpdateTime)
                    return;

                _UpdateTime = value;
                this.OnPropertyChanged("UpdateTime");
            }
        }

        private bool _IsChecked;
        public bool IsChecked
        {
            get { return _IsChecked; }
            set
            {
                if (value == _IsChecked)
                    return;

                _IsChecked = value;
                this.OnPropertyChanged("IsChecked");
            }
        }

        private bool _IsReadyForAction = true;
        public bool IsReadyForAction
        {
            get { return _IsReadyForAction; }
            set
            {
                if (value == _IsReadyForAction)
                    return;

                _IsReadyForAction = value;
                this.OnPropertyChanged("IsReadyForAction");
            }
        }

        #endregion Property

        #region Utility Method

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event MemberChangedEventHandler MemberChanged;
        public void OnMemberChanged(int prevType)
        {
            MemberChangedEventHandler handler = MemberChanged;
            if (handler != null)
            {
                handler(this, new MemberChangedEventArgs(this, NotifyCollectionChangedAction.Replace, prevType));
            }
        }

        public void LoadData(GroupMemberInfoDTO groupMemberInfoDTO)
        {
            if (groupMemberInfoDTO != null)
            {
                this.UserTableID = groupMemberInfoDTO.UserTableID;
                this.RingID = groupMemberInfoDTO.RingID;
                this.FullName = groupMemberInfoDTO.FullName;
                this.MemberAccessType = groupMemberInfoDTO.MemberAccessType;
                this.MemberAddedBy = groupMemberInfoDTO.MemberAddedBy;
            }
        }

        #endregion Utility Method

    }

    public class MemberChangedEventArgs : EventArgs
    {

        public MemberChangedEventArgs(GroupMemberInfoModel memberModel, NotifyCollectionChangedAction changeType)
        {
            this.MemberModel = memberModel;
            this.ChangeType = changeType;
        }

        public MemberChangedEventArgs(GroupMemberInfoModel memberModel, NotifyCollectionChangedAction changeType, int prevAccessType)
            : this(memberModel, changeType)
        {
            this.PrevAccessType = prevAccessType;
        }

        public GroupMemberInfoModel MemberModel { get; set; }
        public NotifyCollectionChangedAction ChangeType { get; set; }
        public int PrevAccessType { get; set; }

    }

    public interface IMemberChanged
    {
        event MemberChangedEventHandler MemberChanged;
    }

    public delegate void MemberChangedEventHandler(object sender, MemberChangedEventArgs e);

}

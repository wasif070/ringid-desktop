<<<<<<< HEAD
﻿using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Threading;
using View.Utility;

namespace View.BindingModels
{
    public class ChannelModel : INotifyPropertyChanged
    {

        #region Constructor

        public ChannelModel()
        {
        }

        public ChannelModel(ChannelDTO channelDTO)
        {
            this.LoadData(channelDTO);
        }

        #endregion Constructor

        #region Property

        private Guid _ChannelID;
        public Guid ChannelID
        {
            get { return _ChannelID; }
            set
            {
                if (value == _ChannelID)
                    return;

                _ChannelID = value;
                this.OnPropertyChanged("ChannelID");
            }
        }

        private long _OwnerID;
        public long OwnerID
        {
            get { return _OwnerID; }
            set
            {
                if (value == _OwnerID)
                    return;

                _OwnerID = value;
                this.OnPropertyChanged("OwnerID");
            }
        }

        private long _PublisherID;
        public long PublisherID
        {
            get { return _PublisherID; }
            set
            {
                if (value == _PublisherID)
                    return;

                _PublisherID = value;
                this.OnPropertyChanged("PublisherID");
            }
        }

        private string _Title = string.Empty;
        public string Title
        {
            get { return _Title; }
            set
            {
                if (value == _Title)
                    return;

                _Title = value;
                this.OnPropertyChanged("Title");
            }
        }

        private string _Description = string.Empty;
        public string Description
        {
            get { return _Description; }
            set
            {
                if (value == _Description)
                    return;

                _Description = value;
                this.OnPropertyChanged("Description");
            }
        }

        private string _ProfileImage;
        public string ProfileImage
        {
            get { return _ProfileImage; }
            set
            {
                if (value == _ProfileImage)
                    return;

                _ProfileImage = value;
                this.OnPropertyChanged("ProfileImage");
            }
        }

        private int _ProfileImageWidth;
        public int ProfileImageWidth
        {
            get { return _ProfileImageWidth; }
            set
            {
                if (value == _ProfileImageWidth)
                    return;

                _ProfileImageWidth = value;
                this.OnPropertyChanged("ProfileImageWidth");
            }
        }

        private int _ProfileImageHeight;
        public int ProfileImageHeight
        {
            get { return _ProfileImageHeight; }
            set
            {
                if (value == _ProfileImageHeight)
                    return;

                _ProfileImageHeight = value;
                this.OnPropertyChanged("ProfileImageHeight");
            }
        }

        private Guid _ProfileImageID;
        public Guid ProfileImageID
        {
            get { return _ProfileImageID; }
            set
            {
                if (value == _ProfileImageID)
                    return;

                _ProfileImageID = value;
                this.OnPropertyChanged("ProfileImageID");
            }
        }

        private string _CoverImage;
        public string CoverImage
        {
            get { return _CoverImage; }
            set
            {
                if (value == _CoverImage)
                    return;

                _CoverImage = value;
                this.OnPropertyChanged("CoverImage");
            }
        }

        private int _CoverImageWidth;
        public int CoverImageWidth
        {
            get { return _CoverImageWidth; }
            set
            {
                if (value == _CoverImageWidth)
                    return;

                _CoverImageWidth = value;
                this.OnPropertyChanged("CoverImageWidth");
            }
        }

        private int _CoverImageHeight;
        public int CoverImageHeight
        {
            get { return _CoverImageHeight; }
            set
            {
                if (value == _CoverImageHeight)
                    return;

                _CoverImageHeight = value;
                this.OnPropertyChanged("CoverImageHeight");
            }
        }

        private Guid _CoverImageID;
        public Guid CoverImageID
        {
            get { return _CoverImageID; }
            set
            {
                if (value == _CoverImageID)
                    return;

                _CoverImageID = value;
                this.OnPropertyChanged("CoverImageID");
            }
        }

        private int _CoverImageX;
        public int CoverImageX
        {
            get { return _CoverImageX; }
            set
            {
                if (value == _CoverImageX)
                    return;

                _CoverImageX = value;
                this.OnPropertyChanged("CoverImageX");
            }
        }

        private int _CoverImageY;
        public int CoverImageY
        {
            get { return _CoverImageY; }
            set
            {
                if (value == _CoverImageY)
                    return;

                _CoverImageY = value;
                this.OnPropertyChanged("CoverImageY");
            }
        }

        private int _ChannelStatus;
        public int ChannelStatus
        {
            get { return _ChannelStatus; }
            set
            {
                if (value == _ChannelStatus)
                    return;

                _ChannelStatus = value;
                this.OnPropertyChanged("ChannelStatus");
            }
        }

        private int _ChannelType;
        public int ChannelType
        {
            get { return _ChannelType; }
            set
            {
                if (value == _ChannelType)
                    return;

                _ChannelType = value;
                this.OnPropertyChanged("ChannelType");
            }
        }

        private long _SubscriberCount;
        public long SubscriberCount
        {
            get { return _SubscriberCount; }
            set
            {
                if (value == _SubscriberCount)
                    return;

                _SubscriberCount = value;
                this.OnPropertyChanged("SubscriberCount");
            }
        }

        private long _SubscriptionTime;
        public long SubscriptionTime
        {
            get { return _SubscriptionTime; }
            set
            {
                if (value == _SubscriptionTime)
                    return;

                _SubscriptionTime = value;
                this.OnPropertyChanged("SubscriptionTime");
            }
        }

        private long _ViewCount;
        public long ViewCount
        {
            get { return _ViewCount; }
            set
            {
                if (value == _ViewCount)
                    return;

                _ViewCount = value;
                this.OnPropertyChanged("ViewCount");
            }
        }

        private long _CreationTime;
        public long CreationTime
        {
            get { return _CreationTime; }
            set
            {
                if (value == _CreationTime)
                    return;

                _CreationTime = value;
                this.OnPropertyChanged("CreationTime");
            }
        }

        private string _Country = string.Empty;
        public string Country
        {
            get { return _Country; }
            set
            {
                if (value == _Country)
                    return;

                _Country = value;
                this.OnPropertyChanged("Country");
            }
        }

        private string _ChannelIP;
        public string ChannelIP
        {
            get { return _ChannelIP; }
            set
            {
                if (value == _ChannelIP)
                    return;

                _ChannelIP = value;
                this.OnPropertyChanged("ChannelIP");
            }
        }

        private int _ChannelPort;
        public int ChannelPort
        {
            get { return _ChannelPort; }
            set
            {
                if (value == _ChannelPort)
                    return;

                _ChannelPort = value;
                this.OnPropertyChanged("ChannelPort");
            }
        }

        private string _StreamIP;
        public string StreamIP
        {
            get { return _StreamIP; }
            set
            {
                if (value == _StreamIP)
                    return;

                _StreamIP = value;
                this.OnPropertyChanged("StreamIP");
            }
        }

        private int _StreamPort;
        public int StreamPort
        {
            get { return _StreamPort; }
            set
            {
                if (value == _StreamPort)
                    return;

                _StreamPort = value;
                this.OnPropertyChanged("StreamPort");
            }
        }

        private bool _IsViewOpened;
        public bool IsViewOpened
        {
            get { return _IsViewOpened; }
            set
            {
                if (value == _IsViewOpened)
                    return;

                _IsViewOpened = value;
                this.OnPropertyChanged("IsViewOpened");
            }
        }

        private bool _IsProcessing;
        public bool IsProcessing
        {
            get { return _IsProcessing; }
            set
            {
                if (value == _IsProcessing)
                    return;

                _IsProcessing = value;
                this.OnPropertyChanged("IsProcessing");
            }
        }

        private bool _IsCombinedView = false;
        public bool IsCombinedView
        {
            get { return _IsCombinedView; }
        }

        private int _ViewType = SettingsConstants.TYPE_CHANNEL;
        public int ViewType
        {
            get { return _ViewType; }
        }

        private ObservableCollection<ChannelCategoryModel> _CategoryList = new ObservableCollection<ChannelCategoryModel>();
        public ObservableCollection<ChannelCategoryModel> CategoryList
        {
            get { return _CategoryList; }
            set
            {
                if (value == _CategoryList)
                    return;

                _CategoryList = value;
                this.OnPropertyChanged("CategoryList");
            }
        }

        private ObservableCollection<ChannelMediaModel> _MediaList = new ObservableCollection<ChannelMediaModel>();
        public ObservableCollection<ChannelMediaModel> MediaList
        {
            get { return _MediaList; }
            set
            {
                if (value == _MediaList)
                    return;

                _MediaList = value;
                this.OnPropertyChanged("MediaList");
            }
        }

        #endregion Property

        #region Utility Method

        public void LoadData(ChannelDTO channelDTO)
        {
            this.ChannelID = channelDTO.ChannelID;
            this.OwnerID = channelDTO.OwnerID;
            this.Title = channelDTO.Title;
            this.Description = channelDTO.Description;
            this.ProfileImage = channelDTO.ProfileImage;
            this.ProfileImageWidth = channelDTO.ProfileImageWidth;
            this.ProfileImageHeight = channelDTO.ProfileImageHeight;
            this.ProfileImageID = channelDTO.ProfileImageID;
            this.CoverImage = channelDTO.CoverImage;
            this.CoverImageWidth = channelDTO.CoverImageWidth;
            this.CoverImageHeight = channelDTO.CoverImageHeight;
            this.CoverImageID = channelDTO.CoverImageID;
            this.CoverImageX = channelDTO.CoverImageX;
            this.CoverImageY = channelDTO.CoverImageY;
            this.ChannelStatus = channelDTO.ChannelStatus;
            this.ChannelType = channelDTO.ChannelType;
            this.SubscriberCount = channelDTO.SubscriberCount;
            this.SubscriptionTime = channelDTO.SubscriptionTime;
            this.ViewCount = channelDTO.ViewCount;
            this.CreationTime = channelDTO.CreationTime;
            this.Country = channelDTO.Country;
            this.ChannelIP = channelDTO.ChannelIP;
            this.ChannelPort = channelDTO.ChannelPort;
            this.StreamIP = channelDTO.StreamIP;
            this.StreamPort = channelDTO.StreamPort;

            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                this.CategoryList.Clear();
                if (channelDTO.CategoryList != null)
                {
                    foreach (ChannelCategoryDTO ctgDTO in channelDTO.CategoryList)
                    {
                        this.CategoryList.Add(new ChannelCategoryModel(ctgDTO));
                    }
                }

                this.MediaList.Clear();
                if (channelDTO.MediaList != null)
                {
                    foreach (ChannelMediaDTO mediaDTO in channelDTO.MediaList)
                    {
                        this.MediaList.Add(new ChannelMediaModel(mediaDTO));
                    }
                }
            }, DispatcherPriority.Send);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method

    }

    public class ChannelCategoryModel : INotifyPropertyChanged
    {

        #region Constructor

        public ChannelCategoryModel()
        {
        }

        public ChannelCategoryModel(ChannelCategoryDTO categoryDTO)
        {
            this.LoadData(categoryDTO);
        }

        #endregion Constructor

        #region Property

        private int _CategoryID;
        public int CategoryID
        {
            get { return _CategoryID; }
            set
            {
                if (value == _CategoryID)
                    return;

                _CategoryID = value;
                this.OnPropertyChanged("CategoryID");
            }
        }

        private string _CategoryName;
        public string CategoryName
        {
            get { return _CategoryName; }
            set
            {
                if (value == _CategoryName)
                    return;

                _CategoryName = value;
                this.OnPropertyChanged("CategoryName");
            }
        }

        #endregion Property

        #region Utility Method

        public void LoadData(ChannelCategoryDTO categoryDTO)
        {
            this.CategoryID = categoryDTO.CategoryID;
            this.CategoryName = categoryDTO.CategoryName;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method

    }

    public class ChannelMediaModel : INotifyPropertyChanged
    {

        #region Constructor

        public ChannelMediaModel()
        {
        }

        public ChannelMediaModel(ChannelMediaDTO mediaDTO)
        {
            this.LoadData(mediaDTO);
        }

        #endregion Constructor

        #region Property

        private Guid _ChannelID;
        public Guid ChannelID
        {
            get { return _ChannelID; }
            set
            {
                if (value == _ChannelID)
                    return;

                _ChannelID = value;
                this.OnPropertyChanged("ChannelID");
            }
        }

        private long _OwnerID;
        public long OwnerID
        {
            get { return _OwnerID; }
            set
            {
                if (value == _OwnerID)
                    return;

                _OwnerID = value;
                this.OnPropertyChanged("OwnerID");
            }
        }

        private string _Title;
        public string Title
        {
            get { return _Title; }
            set
            {
                if (value == _Title)
                    return;

                _Title = value;
                this.OnPropertyChanged("Title");
            }
        }

        private string _Artist;
        public string Artist
        {
            get { return _Artist; }
            set
            {
                if (value == _Artist)
                    return;

                _Artist = value;
                this.OnPropertyChanged("Artist");
            }
        }

        private string _Description;
        public string Description
        {
            get { return _Description; }
            set
            {
                if (value == _Description)
                    return;

                _Description = value;
                this.OnPropertyChanged("Description");
            }
        }

        private Guid _MediaID;
        public Guid MediaID
        {
            get { return _MediaID; }
            set
            {
                if (value == _MediaID)
                    return;

                _MediaID = value;
                this.OnPropertyChanged("MediaID");
            }
        }

        private int _MediaType;
        public int MediaType
        {
            get { return _MediaType; }
            set
            {
                if (value == _MediaType)
                    return;

                _MediaType = value;
                this.OnPropertyChanged("MediaType");
            }
        }

        private string _MediaUrl;
        public string MediaUrl
        {
            get { return _MediaUrl; }
            set
            {
                if (value == _MediaUrl)
                    return;

                _MediaUrl = value;
                this.OnPropertyChanged("MediaUrl");
            }
        }

        private string _ThumbImageUrl;
        public string ThumbImageUrl
        {
            get { return _ThumbImageUrl; }
            set
            {
                if (value == _ThumbImageUrl)
                    return;

                _ThumbImageUrl = value;
                this.OnPropertyChanged("ThumbImageUrl");
            }
        }

        private int _ThumbImageWidth;
        public int ThumbImageWidth
        {
            get { return _ThumbImageWidth; }
            set
            {
                if (value == _ThumbImageWidth)
                    return;

                _ThumbImageWidth = value;
                this.OnPropertyChanged("ThumbImageWidth");
            }
        }

        private int _ThumbImageHeight;
        public int ThumbImageHeight
        {
            get { return _ThumbImageHeight; }
            set
            {
                if (value == _ThumbImageHeight)
                    return;

                _ThumbImageHeight = value;
                this.OnPropertyChanged("ThumbImageHeight");
            }
        }

        private long _Duration;
        public long Duration
        {
            get { return _Duration; }
            set
            {
                if (value == _Duration)
                    return;

                _Duration = value;
                this.OnPropertyChanged("Duration");
            }
        }

        private int _MediaStatus;
        public int MediaStatus
        {
            get { return _MediaStatus; }
            set
            {
                if (value == _MediaStatus)
                    return;

                _MediaStatus = value;
                this.OnPropertyChanged("MediaStatus");
            }
        }

        private int _ChannelType;
        public int ChannelType
        {
            get { return _ChannelType; }
            set
            {
                if (value == _ChannelType)
                    return;

                _ChannelType = value;
                this.OnPropertyChanged("ChannelType");
            }
        }

        private long _StartTime;
        public long StartTime
        {
            get { return _StartTime; }
            set
            {
                if (value == _StartTime)
                    return;

                _StartTime = value;
                this.OnPropertyChanged("StartTime");
            }
        }

        private long _EndTime;
        public long EndTime
        {
            get { return _EndTime; }
            set
            {
                if (value == _EndTime)
                    return;

                _EndTime = value;
                this.OnPropertyChanged("EndTime");
            }
        }

        private bool _IsViewOpened;
        public bool IsViewOpened
        {
            get { return _IsViewOpened; }
            set
            {
                if (value == _IsViewOpened)
                    return;

                _IsViewOpened = value;
                this.OnPropertyChanged("IsViewOpened");
            }
        }

        private bool _IsChecked;
        public bool IsChecked
        {
            get { return _IsChecked; }
            set
            {
                if (value == _IsChecked)
                    return;

                _IsChecked = value;
                this.OnPropertyChanged("IsChecked");
            }
        }

        #endregion Property

        #region Utility Method

        public void LoadData(ChannelMediaDTO mediaDTO)
        {
            this.ChannelID = mediaDTO.ChannelID;
            this.OwnerID = mediaDTO.OwnerID;
            this.Title = mediaDTO.Title;
            this.Artist = mediaDTO.Artist;
            this.Description = mediaDTO.Description;
            this.MediaID = mediaDTO.MediaID;
            this.MediaType = mediaDTO.MediaType;
            this.MediaUrl = mediaDTO.MediaUrl;
            this.ThumbImageUrl = mediaDTO.ThumbImageUrl;
            this.ThumbImageWidth = mediaDTO.ThumbImageWidth;
            this.ThumbImageHeight = mediaDTO.ThumbImageHeight;
            this.Duration = mediaDTO.Duration;
            this.MediaStatus = mediaDTO.MediaStatus;
            this.ChannelType = mediaDTO.ChannelType;
            this.StartTime = mediaDTO.StartTime;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method

    }

    public class ChannelLoadStatusModel : INotifyPropertyChanged
    {
        public ChannelLoadStatusModel() { }

        private bool _IsFeaturedLoading = false;
        public bool IsFeaturedLoading
        {
            get { return _IsFeaturedLoading; }
            set
            {
                if (value == _IsFeaturedLoading) { return; }
                _IsFeaturedLoading = value;
                this.OnPropertyChanged("IsFeaturedLoading");
            }
        }

        private bool _IsMostViewLoading = false;
        public bool IsMostViewLoading
        {
            get { return _IsMostViewLoading; }
            set
            {
                if (value == _IsMostViewLoading) { return; }
                _IsMostViewLoading = value;
                this.OnPropertyChanged("IsMostViewLoading");
            }
        }

        private bool _IsSearchLoading = false;
        public bool IsSearchLoading
        {
            get { return _IsSearchLoading; }
            set
            {
                if (value == _IsSearchLoading) { return; }
                _IsSearchLoading = value;
                this.OnPropertyChanged("IsSearchLoading");
            }
        }

        private bool _IsMyChannelLoading = false;
        public bool IsMyChannelLoading
        {
            get { return _IsMyChannelLoading; }
            set
            {
                if (value == _IsMyChannelLoading) { return; }
                _IsMyChannelLoading = value;
                this.OnPropertyChanged("IsMyChannelLoading");
            }
        }

        private bool _IsFollowingLoading = false;
        public bool IsFollowingLoading
        {
            get { return _IsFollowingLoading; }
            set
            {
                if (value == _IsFollowingLoading) { return; }
                _IsFollowingLoading = value;
                this.OnPropertyChanged("IsFollowingLoading");
            }
        }

        private bool _IsPlaylistLoading = false;
        public bool IsPlaylistLoading
        {
            get { return _IsPlaylistLoading; }
            set
            {
                if (value == _IsPlaylistLoading) { return; }
                _IsPlaylistLoading = value;
                this.OnPropertyChanged("IsPlaylistLoading");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) { handler(this, new PropertyChangedEventArgs(propertyName)); }
        }
    }
}
=======
﻿using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Threading;
using View.Utility;

namespace View.BindingModels
{
    public class ChannelModel : INotifyPropertyChanged
    {

        #region Constructor

        public ChannelModel()
        {
        }

        public ChannelModel(ChannelDTO channelDTO)
        {
            this.LoadData(channelDTO);
        }

        #endregion Constructor

        #region Property

        private Guid _ChannelID;
        public Guid ChannelID
        {
            get { return _ChannelID; }
            set
            {
                if (value == _ChannelID)
                    return;

                _ChannelID = value;
                this.OnPropertyChanged("ChannelID");
            }
        }

        private long _OwnerID;
        public long OwnerID
        {
            get { return _OwnerID; }
            set
            {
                if (value == _OwnerID)
                    return;

                _OwnerID = value;
                this.OnPropertyChanged("OwnerID");
            }
        }

        private long _PublisherID;
        public long PublisherID
        {
            get { return _PublisherID; }
            set
            {
                if (value == _PublisherID)
                    return;

                _PublisherID = value;
                this.OnPropertyChanged("PublisherID");
            }
        }

        private string _Title = string.Empty;
        public string Title
        {
            get { return _Title; }
            set
            {
                if (value == _Title)
                    return;

                _Title = value;
                this.OnPropertyChanged("Title");
            }
        }

        private string _Description = string.Empty;
        public string Description
        {
            get { return _Description; }
            set
            {
                if (value == _Description)
                    return;

                _Description = value;
                this.OnPropertyChanged("Description");
            }
        }

        private string _ProfileImage;
        public string ProfileImage
        {
            get { return _ProfileImage; }
            set
            {
                if (value == _ProfileImage)
                    return;

                _ProfileImage = value;
                this.OnPropertyChanged("ProfileImage");
            }
        }

        private int _ProfileImageWidth;
        public int ProfileImageWidth
        {
            get { return _ProfileImageWidth; }
            set
            {
                if (value == _ProfileImageWidth)
                    return;

                _ProfileImageWidth = value;
                this.OnPropertyChanged("ProfileImageWidth");
            }
        }

        private int _ProfileImageHeight;
        public int ProfileImageHeight
        {
            get { return _ProfileImageHeight; }
            set
            {
                if (value == _ProfileImageHeight)
                    return;

                _ProfileImageHeight = value;
                this.OnPropertyChanged("ProfileImageHeight");
            }
        }

        private Guid _ProfileImageID;
        public Guid ProfileImageID
        {
            get { return _ProfileImageID; }
            set
            {
                if (value == _ProfileImageID)
                    return;

                _ProfileImageID = value;
                this.OnPropertyChanged("ProfileImageID");
            }
        }

        private string _CoverImage;
        public string CoverImage
        {
            get { return _CoverImage; }
            set
            {
                if (value == _CoverImage)
                    return;

                _CoverImage = value;
                this.OnPropertyChanged("CoverImage");
            }
        }

        private int _CoverImageWidth;
        public int CoverImageWidth
        {
            get { return _CoverImageWidth; }
            set
            {
                if (value == _CoverImageWidth)
                    return;

                _CoverImageWidth = value;
                this.OnPropertyChanged("CoverImageWidth");
            }
        }

        private int _CoverImageHeight;
        public int CoverImageHeight
        {
            get { return _CoverImageHeight; }
            set
            {
                if (value == _CoverImageHeight)
                    return;

                _CoverImageHeight = value;
                this.OnPropertyChanged("CoverImageHeight");
            }
        }

        private Guid _CoverImageID;
        public Guid CoverImageID
        {
            get { return _CoverImageID; }
            set
            {
                if (value == _CoverImageID)
                    return;

                _CoverImageID = value;
                this.OnPropertyChanged("CoverImageID");
            }
        }

        private int _CoverImageX;
        public int CoverImageX
        {
            get { return _CoverImageX; }
            set
            {
                if (value == _CoverImageX)
                    return;

                _CoverImageX = value;
                this.OnPropertyChanged("CoverImageX");
            }
        }

        private int _CoverImageY;
        public int CoverImageY
        {
            get { return _CoverImageY; }
            set
            {
                if (value == _CoverImageY)
                    return;

                _CoverImageY = value;
                this.OnPropertyChanged("CoverImageY");
            }
        }

        private int _ChannelStatus;
        public int ChannelStatus
        {
            get { return _ChannelStatus; }
            set
            {
                if (value == _ChannelStatus)
                    return;

                _ChannelStatus = value;
                this.OnPropertyChanged("ChannelStatus");
            }
        }

        private int _ChannelType;
        public int ChannelType
        {
            get { return _ChannelType; }
            set
            {
                if (value == _ChannelType)
                    return;

                _ChannelType = value;
                this.OnPropertyChanged("ChannelType");
            }
        }

        private long _SubscriberCount;
        public long SubscriberCount
        {
            get { return _SubscriberCount; }
            set
            {
                if (value == _SubscriberCount)
                    return;

                _SubscriberCount = value;
                this.OnPropertyChanged("SubscriberCount");
            }
        }

        private long _SubscriptionTime;
        public long SubscriptionTime
        {
            get { return _SubscriptionTime; }
            set
            {
                if (value == _SubscriptionTime)
                    return;

                _SubscriptionTime = value;
                this.OnPropertyChanged("SubscriptionTime");
            }
        }

        private long _ViewCount;
        public long ViewCount
        {
            get { return _ViewCount; }
            set
            {
                if (value == _ViewCount)
                    return;

                _ViewCount = value;
                this.OnPropertyChanged("ViewCount");
            }
        }

        private long _CreationTime;
        public long CreationTime
        {
            get { return _CreationTime; }
            set
            {
                if (value == _CreationTime)
                    return;

                _CreationTime = value;
                this.OnPropertyChanged("CreationTime");
            }
        }

        private string _Country = string.Empty;
        public string Country
        {
            get { return _Country; }
            set
            {
                if (value == _Country)
                    return;

                _Country = value;
                this.OnPropertyChanged("Country");
            }
        }

        private string _ChannelIP;
        public string ChannelIP
        {
            get { return _ChannelIP; }
            set
            {
                if (value == _ChannelIP)
                    return;

                _ChannelIP = value;
                this.OnPropertyChanged("ChannelIP");
            }
        }

        private int _ChannelPort;
        public int ChannelPort
        {
            get { return _ChannelPort; }
            set
            {
                if (value == _ChannelPort)
                    return;

                _ChannelPort = value;
                this.OnPropertyChanged("ChannelPort");
            }
        }

        private string _StreamIP;
        public string StreamIP
        {
            get { return _StreamIP; }
            set
            {
                if (value == _StreamIP)
                    return;

                _StreamIP = value;
                this.OnPropertyChanged("StreamIP");
            }
        }

        private int _StreamPort;
        public int StreamPort
        {
            get { return _StreamPort; }
            set
            {
                if (value == _StreamPort)
                    return;

                _StreamPort = value;
                this.OnPropertyChanged("StreamPort");
            }
        }

        private bool _IsViewOpened;
        public bool IsViewOpened
        {
            get { return _IsViewOpened; }
            set
            {
                if (value == _IsViewOpened)
                    return;

                _IsViewOpened = value;
                this.OnPropertyChanged("IsViewOpened");
            }
        }

        private bool _IsProcessing;
        public bool IsProcessing
        {
            get { return _IsProcessing; }
            set
            {
                if (value == _IsProcessing)
                    return;

                _IsProcessing = value;
                this.OnPropertyChanged("IsProcessing");
            }
        }

        private bool _IsCombinedView = false;
        public bool IsCombinedView
        {
            get { return _IsCombinedView; }
        }

        private int _ViewType = SettingsConstants.TYPE_CHANNEL;
        public int ViewType
        {
            get { return _ViewType; }
        }

        private ObservableCollection<ChannelCategoryModel> _CategoryList = new ObservableCollection<ChannelCategoryModel>();
        public ObservableCollection<ChannelCategoryModel> CategoryList
        {
            get { return _CategoryList; }
            set
            {
                if (value == _CategoryList)
                    return;

                _CategoryList = value;
                this.OnPropertyChanged("CategoryList");
            }
        }

        private ObservableCollection<ChannelMediaModel> _MediaList = new ObservableCollection<ChannelMediaModel>();
        public ObservableCollection<ChannelMediaModel> MediaList
        {
            get { return _MediaList; }
            set
            {
                if (value == _MediaList)
                    return;

                _MediaList = value;
                this.OnPropertyChanged("MediaList");
            }
        }

        #endregion Property

        #region Utility Method

        public void LoadData(ChannelDTO channelDTO)
        {
            this.ChannelID = channelDTO.ChannelID;
            this.OwnerID = channelDTO.OwnerID;
            this.Title = channelDTO.Title;
            this.Description = channelDTO.Description;
            this.ProfileImage = channelDTO.ProfileImage;
            this.ProfileImageWidth = channelDTO.ProfileImageWidth;
            this.ProfileImageHeight = channelDTO.ProfileImageHeight;
            this.ProfileImageID = channelDTO.ProfileImageID;
            this.CoverImage = channelDTO.CoverImage;
            this.CoverImageWidth = channelDTO.CoverImageWidth;
            this.CoverImageHeight = channelDTO.CoverImageHeight;
            this.CoverImageID = channelDTO.CoverImageID;
            this.CoverImageX = channelDTO.CoverImageX;
            this.CoverImageY = channelDTO.CoverImageY;
            this.ChannelStatus = channelDTO.ChannelStatus;
            this.ChannelType = channelDTO.ChannelType;
            this.SubscriberCount = channelDTO.SubscriberCount;
            this.SubscriptionTime = channelDTO.SubscriptionTime;
            this.ViewCount = channelDTO.ViewCount;
            this.CreationTime = channelDTO.CreationTime;
            this.Country = channelDTO.Country;
            this.ChannelIP = channelDTO.ChannelIP;
            this.ChannelPort = channelDTO.ChannelPort;
            this.StreamIP = channelDTO.StreamIP;
            this.StreamPort = channelDTO.StreamPort;

            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                this.CategoryList.Clear();
                if (channelDTO.CategoryList != null)
                {
                    foreach (ChannelCategoryDTO ctgDTO in channelDTO.CategoryList)
                    {
                        this.CategoryList.Add(new ChannelCategoryModel(ctgDTO));
                    }
                }

                this.MediaList.Clear();
                if (channelDTO.MediaList != null)
                {
                    foreach (ChannelMediaDTO mediaDTO in channelDTO.MediaList)
                    {
                        this.MediaList.Add(new ChannelMediaModel(mediaDTO));
                    }
                }
            }, DispatcherPriority.Send);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method

    }

    public class ChannelCategoryModel : INotifyPropertyChanged
    {

        #region Constructor

        public ChannelCategoryModel()
        {
        }

        public ChannelCategoryModel(ChannelCategoryDTO categoryDTO)
        {
            this.LoadData(categoryDTO);
        }

        #endregion Constructor

        #region Property

        private int _CategoryID;
        public int CategoryID
        {
            get { return _CategoryID; }
            set
            {
                if (value == _CategoryID)
                    return;

                _CategoryID = value;
                this.OnPropertyChanged("CategoryID");
            }
        }

        private string _CategoryName;
        public string CategoryName
        {
            get { return _CategoryName; }
            set
            {
                if (value == _CategoryName)
                    return;

                _CategoryName = value;
                this.OnPropertyChanged("CategoryName");
            }
        }

        #endregion Property

        #region Utility Method

        public void LoadData(ChannelCategoryDTO categoryDTO)
        {
            this.CategoryID = categoryDTO.CategoryID;
            this.CategoryName = categoryDTO.CategoryName;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method

    }

    public class ChannelMediaModel : INotifyPropertyChanged
    {

        #region Constructor

        public ChannelMediaModel()
        {
        }

        public ChannelMediaModel(ChannelMediaDTO mediaDTO)
        {
            this.LoadData(mediaDTO);
        }

        #endregion Constructor

        #region Property

        private Guid _ChannelID;
        public Guid ChannelID
        {
            get { return _ChannelID; }
            set
            {
                if (value == _ChannelID)
                    return;

                _ChannelID = value;
                this.OnPropertyChanged("ChannelID");
            }
        }

        private long _OwnerID;
        public long OwnerID
        {
            get { return _OwnerID; }
            set
            {
                if (value == _OwnerID)
                    return;

                _OwnerID = value;
                this.OnPropertyChanged("OwnerID");
            }
        }

        private string _Title;
        public string Title
        {
            get { return _Title; }
            set
            {
                if (value == _Title)
                    return;

                _Title = value;
                this.OnPropertyChanged("Title");
            }
        }

        private string _Artist;
        public string Artist
        {
            get { return _Artist; }
            set
            {
                if (value == _Artist)
                    return;

                _Artist = value;
                this.OnPropertyChanged("Artist");
            }
        }

        private string _Description;
        public string Description
        {
            get { return _Description; }
            set
            {
                if (value == _Description)
                    return;

                _Description = value;
                this.OnPropertyChanged("Description");
            }
        }

        private Guid _MediaID;
        public Guid MediaID
        {
            get { return _MediaID; }
            set
            {
                if (value == _MediaID)
                    return;

                _MediaID = value;
                this.OnPropertyChanged("MediaID");
            }
        }

        private int _MediaType;
        public int MediaType
        {
            get { return _MediaType; }
            set
            {
                if (value == _MediaType)
                    return;

                _MediaType = value;
                this.OnPropertyChanged("MediaType");
            }
        }

        private string _MediaUrl;
        public string MediaUrl
        {
            get { return _MediaUrl; }
            set
            {
                if (value == _MediaUrl)
                    return;

                _MediaUrl = value;
                this.OnPropertyChanged("MediaUrl");
            }
        }

        private string _ThumbImageUrl;
        public string ThumbImageUrl
        {
            get { return _ThumbImageUrl; }
            set
            {
                if (value == _ThumbImageUrl)
                    return;

                _ThumbImageUrl = value;
                this.OnPropertyChanged("ThumbImageUrl");
            }
        }

        private int _ThumbImageWidth;
        public int ThumbImageWidth
        {
            get { return _ThumbImageWidth; }
            set
            {
                if (value == _ThumbImageWidth)
                    return;

                _ThumbImageWidth = value;
                this.OnPropertyChanged("ThumbImageWidth");
            }
        }

        private int _ThumbImageHeight;
        public int ThumbImageHeight
        {
            get { return _ThumbImageHeight; }
            set
            {
                if (value == _ThumbImageHeight)
                    return;

                _ThumbImageHeight = value;
                this.OnPropertyChanged("ThumbImageHeight");
            }
        }

        private long _Duration;
        public long Duration
        {
            get { return _Duration; }
            set
            {
                if (value == _Duration)
                    return;

                _Duration = value;
                this.OnPropertyChanged("Duration");
            }
        }

        private int _MediaStatus;
        public int MediaStatus
        {
            get { return _MediaStatus; }
            set
            {
                if (value == _MediaStatus)
                    return;

                _MediaStatus = value;
                this.OnPropertyChanged("MediaStatus");
            }
        }

        private int _ChannelType;
        public int ChannelType
        {
            get { return _ChannelType; }
            set
            {
                if (value == _ChannelType)
                    return;

                _ChannelType = value;
                this.OnPropertyChanged("ChannelType");
            }
        }

        private long _StartTime;
        public long StartTime
        {
            get { return _StartTime; }
            set
            {
                if (value == _StartTime)
                    return;

                _StartTime = value;
                this.OnPropertyChanged("StartTime");
            }
        }

        private long _EndTime;
        public long EndTime
        {
            get { return _EndTime; }
            set
            {
                if (value == _EndTime)
                    return;

                _EndTime = value;
                this.OnPropertyChanged("EndTime");
            }
        }

        private bool _IsViewOpened;
        public bool IsViewOpened
        {
            get { return _IsViewOpened; }
            set
            {
                if (value == _IsViewOpened)
                    return;

                _IsViewOpened = value;
                this.OnPropertyChanged("IsViewOpened");
            }
        }

        private bool _IsChecked;
        public bool IsChecked
        {
            get { return _IsChecked; }
            set
            {
                if (value == _IsChecked)
                    return;

                _IsChecked = value;
                this.OnPropertyChanged("IsChecked");
            }
        }

        #endregion Property

        #region Utility Method

        public void LoadData(ChannelMediaDTO mediaDTO)
        {
            this.ChannelID = mediaDTO.ChannelID;
            this.OwnerID = mediaDTO.OwnerID;
            this.Title = mediaDTO.Title;
            this.Artist = mediaDTO.Artist;
            this.Description = mediaDTO.Description;
            this.MediaID = mediaDTO.MediaID;
            this.MediaType = mediaDTO.MediaType;
            this.MediaUrl = mediaDTO.MediaUrl;
            this.ThumbImageUrl = mediaDTO.ThumbImageUrl;
            this.ThumbImageWidth = mediaDTO.ThumbImageWidth;
            this.ThumbImageHeight = mediaDTO.ThumbImageHeight;
            this.Duration = mediaDTO.Duration;
            this.MediaStatus = mediaDTO.MediaStatus;
            this.ChannelType = mediaDTO.ChannelType;
            this.StartTime = mediaDTO.StartTime;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method

    }

    public class ChannelLoadStatusModel : INotifyPropertyChanged
    {
        public ChannelLoadStatusModel() { }

        private bool _IsFeaturedLoading = false;
        public bool IsFeaturedLoading
        {
            get { return _IsFeaturedLoading; }
            set
            {
                if (value == _IsFeaturedLoading) { return; }
                _IsFeaturedLoading = value;
                this.OnPropertyChanged("IsFeaturedLoading");
            }
        }

        private bool _IsMostViewLoading = false;
        public bool IsMostViewLoading
        {
            get { return _IsMostViewLoading; }
            set
            {
                if (value == _IsMostViewLoading) { return; }
                _IsMostViewLoading = value;
                this.OnPropertyChanged("IsMostViewLoading");
            }
        }

        private bool _IsSearchLoading = false;
        public bool IsSearchLoading
        {
            get { return _IsSearchLoading; }
            set
            {
                if (value == _IsSearchLoading) { return; }
                _IsSearchLoading = value;
                this.OnPropertyChanged("IsSearchLoading");
            }
        }

        private bool _IsMyChannelLoading = false;
        public bool IsMyChannelLoading
        {
            get { return _IsMyChannelLoading; }
            set
            {
                if (value == _IsMyChannelLoading) { return; }
                _IsMyChannelLoading = value;
                this.OnPropertyChanged("IsMyChannelLoading");
            }
        }

        private bool _IsFollowingLoading = false;
        public bool IsFollowingLoading
        {
            get { return _IsFollowingLoading; }
            set
            {
                if (value == _IsFollowingLoading) { return; }
                _IsFollowingLoading = value;
                this.OnPropertyChanged("IsFollowingLoading");
            }
        }

        private bool _IsPlaylistLoading = false;
        public bool IsPlaylistLoading
        {
            get { return _IsPlaylistLoading; }
            set
            {
                if (value == _IsPlaylistLoading) { return; }
                _IsPlaylistLoading = value;
                this.OnPropertyChanged("IsPlaylistLoading");
            }
        }

        private bool _IsUploadedMediaLoading = false;
        public bool IsUploadedMediaLoading
        {
            get { return _IsUploadedMediaLoading; }
            set
            {
                if (value == _IsUploadedMediaLoading) { return; }
                _IsUploadedMediaLoading = value;
                this.OnPropertyChanged("IsUploadedMediaLoading");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) { handler(this, new PropertyChangedEventArgs(propertyName)); }
        }
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

﻿using imsdkwrapper;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using VirtualPanel.Controls;

namespace View.BindingModels
{
    public class RoomMemberModel : INotifyPropertyChanged, IHeightMeasurer
    {
        private readonly ILog log = LogManager.GetLogger(typeof(RoomMemberModel).Name);

        #region Constructor

        public RoomMemberModel()
        {
            _CurrentInstance = this;
        }

        public RoomMemberModel(BasePublicChatMemberDTO anonymousDTO)
            : this()
        {
            LoadData(anonymousDTO);
        }

        #endregion Constructor

        #region Property

        private RoomMemberModel _CurrentInstance;
        public RoomMemberModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }

        private long _FakeID;
        public long FakeID
        {
            get { return _FakeID; }
            set
            {
                if (value == _FakeID)
                    return;

                _FakeID = value;
                this.OnPropertyChanged("FakeID");
            }
        }
        private long _MemberID;
        public long MemberID
        {
            get { return _MemberID; }
            set
            {
                if (value == _MemberID)
                    return;

                _MemberID = value;
                this.OnPropertyChanged("MemberID");
            }
        }
        private long _RingID;
        public long RingID
        {
            get { return _RingID; }
            set
            {
                if (value == _RingID)
                    return;

                _RingID = value;
                this.OnPropertyChanged("RingID");
            }
        }

        private string _FullName;
        public string FullName
        {
            get { return _FullName; }
            set
            {
                if (value == _FullName)
                    return;

                _FullName = value;
                this.OnPropertyChanged("FullName");
            }
        }

        private string _ProfileImageUrl;
        public string ProfileImageUrl
        {
            get { return _ProfileImageUrl; }
            set
            {
                if (value == _ProfileImageUrl)
                    return;

                _ProfileImageUrl = value;
                this.OnPropertyChanged("ProfileImageUrl");
            }
        }

        private long _AddedTime;
        public long AddedTime
        {
            get { return _AddedTime; }
            set
            {
                if (value == _AddedTime)
                    return;

                _AddedTime = value;
                this.OnPropertyChanged("AddedTime");
            }
        }

        #endregion Property

        #region Utility Method

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void LoadData(BasePublicChatMemberDTO anonymousDTO)
        {
            if (anonymousDTO != null)
            {
                this.FakeID = anonymousDTO.FakeID;
                this.MemberID = anonymousDTO.MemberID;
                this.RingID = anonymousDTO.RingID;
                this.FullName = anonymousDTO.FullName;
                this.ProfileImageUrl = anonymousDTO.ProfileUrl;
                this.AddedTime = anonymousDTO.AddedTime;
            }
        }

        #endregion Utility Method

        public double GetEstimatedHeight(double availableWidth, VirtualViewType viewType)
        {
            double value = 0;
            try
            {
                switch (viewType)
                {
                    case VirtualViewType.ROOM_MEMBERS_PANEL__MEMBER_LIST:
                        value = 50;
                        break;
                    default:
                        value = 55;
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: GetEstimatedHeight() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            return value;
        }
    }

}

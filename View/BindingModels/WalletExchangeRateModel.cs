﻿using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace View.BindingModels
{
    public class WalletExchangeRateModel : INotifyPropertyChanged
    {
        private int coinId = 0;
        public int CoinId
        {
            get { return coinId; }
            set
            {
                if (value == coinId)
                    return;
                coinId = value;
                this.OnPropertyChanged("CoinId");
            }
        }

        private string coinName = string.Empty;
        public string CoinName
        {
            get { return coinName; }
            set
            {
                if (value == coinName)
                    return;
                coinName = value;
                this.OnPropertyChanged("CoinName");
            }
        }

        private string currencyName = string.Empty;
        public string CurrencyName
        {
            get { return currencyName; }
            set { currencyName = value; this.OnPropertyChanged("CurrencyName"); }
        }

        private string currencyISOCode = string.Empty;
        public string CurrencyISOCode
        {
            get { return currencyISOCode; }
            set
            {
                if (value == currencyISOCode)
                    return;
                currencyISOCode = value;
                this.OnPropertyChanged("CurrencyISOCode");
            }
        }

        private double exchangeRate = 0.0;
        public double ExchangeRate
        {
            get { return exchangeRate; }
            set
            {
                if (value == exchangeRate)
                    return;
                exchangeRate = value;
                this.OnPropertyChanged("ExchangeRate");
            }
        }

        public static WalletExchangeRateModel LoadDataFromJson(JObject jObject)
        {
            WalletExchangeRateModel model = new WalletExchangeRateModel();
            try
            {
                if (jObject[JsonKeys.CurrencyName] != null)
                {
                    model.CurrencyName = (string)jObject[JsonKeys.CurrencyName];
                }
                if (jObject[JsonKeys.CurrencyISOCode] != null)
                {
                    model.CurrencyISOCode = (string)jObject[JsonKeys.CurrencyISOCode];
                }
                if (jObject[JsonKeys.ExchangeRate] != null)
                {
                    model.ExchangeRate = (double)jObject[JsonKeys.ExchangeRate];
                }
            }
            catch (Exception)
            {
                model.CurrencyISOCode = string.Empty;
                //throw;
            }
            return model;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

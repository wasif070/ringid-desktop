﻿using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using View.Utility.DataContainer;
using View.Utility.Feed;
using View.ViewModel;
using View.Constants;
using View.Utility;
using log4net;
using View.Utility.Auth;

namespace View.BindingModels
{
    public class FeedModel : INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(FeedModel).Name);
        public FeedModel()
        {
            CurrentInstance = this;
        }

        public void LoadData(JObject jObject)
        {
            try
            {
                this.FeedType = 2;// 0 for new, 1 for TOP_LOADING, 2 for feed, 3 for load more
                if (jObject[JsonKeys.WallOwner] != null)
                {
                    WallOrContentOwner = new BaseUserProfileModel((JObject)jObject[JsonKeys.WallOwner]);
                }
                if (jObject[JsonKeys.GuestWriter] != null)
                {
                    JObject jObj = (JObject)jObject[JsonKeys.GuestWriter];
                    long utId = (int)jObj[JsonKeys.UserTableID];
                    PostOwner = (utId == DefaultSettings.LOGIN_TABLE_ID) ? RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel : new BaseUserProfileModel(jObj);
                }
                else
                {
                    PostOwner = (WallOrContentOwner.UserTableID == DefaultSettings.LOGIN_TABLE_ID) ? RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel : WallOrContentOwner;
                }
                //PostOwner = (jObject[JsonKeys.GuestWriter] != null) ? new BaseUserProfileModel((JObject)jObject[JsonKeys.GuestWriter]) : WallOrContentOwner;
                if (jObject[JsonKeys.NewsPortalFeedInfo] != null)
                {
                    this.NewsModel = new NewsModel();
                    NewsModel.LoadData((JObject)jObject[JsonKeys.NewsPortalFeedInfo]);
                }
                if (jObject[JsonKeys.NewsfeedId] != null)
                {
                    this.NewsfeedId = (Guid)jObject[JsonKeys.NewsfeedId];
                }
                if (jObject[JsonKeys.Status] != null)
                {
                    this.Status = (string)jObject[JsonKeys.Status];
                }
                else
                    this.Status = "";
                if (jObject[JsonKeys.Time] != null)
                {
                    this.Time = (long)jObject[JsonKeys.Time];
                }
                if (jObject[JsonKeys.SavedTime] != null)
                {
                    this.SavedTime = (long)jObject[JsonKeys.SavedTime];
                }
                if (jObject[JsonKeys.FeedPrivacy] != null)
                {
                    this.Privacy = (int)jObject[JsonKeys.FeedPrivacy];
                }
                if (jObject[JsonKeys.ActualTime] != null)
                {
                    this.ActualTime = (long)jObject[JsonKeys.ActualTime];
                }
                if (jObject[JsonKeys.Validity] != null)
                {
                    this.Validity = (int)jObject[JsonKeys.Validity];
                }
                if (jObject[JsonKeys.BookPostType] != null)
                {
                    this.BookPostType = (short)jObject[JsonKeys.BookPostType];
                }
                //if (jObject[JsonKeys.NumberOfComments] != null)
                //{
                //    this.NumberOfComments = (long)jObject[JsonKeys.NumberOfComments];
                //}
                //if (jObject[JsonKeys.NumberOfLikes] != null)
                //{
                //    this.NumberOfLikes = (long)jObject[JsonKeys.NumberOfLikes];
                //}
                //if (jObject[JsonKeys.NumberOfShares] != null)
                //{
                //    this.NumberOfShares = (long)jObject[JsonKeys.NumberOfShares];
                //}
                //if (jObject[JsonKeys.ILike] != null)
                //{
                //    this.ILike = (short)jObject[JsonKeys.ILike];
                //}
                //if (jObject[JsonKeys.IComment] != null)
                //{
                //    this.IComment = (short)jObject[JsonKeys.IComment];
                //}
                //if (jObject[JsonKeys.IShare] != null)
                //{
                //    this.IShare = (short)jObject[JsonKeys.IShare];
                //}
                if (jObject[JsonKeys.SubType] != null)
                {
                    this.FeedSubType = (short)jObject[JsonKeys.SubType];
                }
                if (jObject[JsonKeys.FeedCategory] != null)
                {
                    this.FeedCategory = (short)jObject[JsonKeys.FeedCategory];
                }
                if (jObject[JsonKeys.IsSaved] != null)
                {
                    this.IsSaved = (bool)jObject[JsonKeys.IsSaved];
                }
                if (jObject[JsonKeys.IsEdited] != null)
                {
                    this.Edited = (bool)jObject[JsonKeys.IsEdited];
                }
                if (jObject[JsonKeys.ShowContinue] != null)
                {
                    this.ShowContinue = (bool)jObject[JsonKeys.ShowContinue];
                }
                if (jObject[JsonKeys.LatestCommenter] != null)
                {
                    Activist = new BaseUserProfileModel((JObject)jObject[JsonKeys.LatestCommenter]);
                    ActivityType = 2;
                }
                else if (jObject[JsonKeys.LatestSharer] != null)
                {
                    Activist = new BaseUserProfileModel((JObject)jObject[JsonKeys.LatestSharer]);
                    ActivityType = 3;
                }
                else if (jObject[JsonKeys.LatestLiker] != null)
                {
                    Activist = new BaseUserProfileModel((JObject)jObject[JsonKeys.LatestLiker]);
                    ActivityType = 1;
                }
                /////
                if (jObject[JsonKeys.LinkDetails] != null)
                {
                    JObject linkDetails = new JObject();
                    linkDetails = (JObject)jObject[JsonKeys.LinkDetails];

                    LinkModel = new LinkInformationModel();
                    LinkModel.PreviewUrl = (string)linkDetails[JsonKeys.LinkURL];

                    if (linkDetails[JsonKeys.LinkDescription] != null)
                    {
                        LinkModel.PreviewDesc = (string)linkDetails[JsonKeys.LinkDescription];
                    }
                    if (linkDetails[JsonKeys.LinkDomain] != null)
                    {
                        LinkModel.PreviewDomain = (string)linkDetails[JsonKeys.LinkDomain];
                    }
                    if (linkDetails[JsonKeys.LinkImageURL] != null)
                    {
                        LinkModel.PreviewImgUrl = (string)linkDetails[JsonKeys.LinkImageURL];
                    }
                    if (linkDetails[JsonKeys.LinkTitle] != null)
                    {
                        LinkModel.PreviewTitle = (string)linkDetails[JsonKeys.LinkTitle];
                    }
                    if (linkDetails[JsonKeys.LinkType] != null)
                    {
                        LinkModel.PreviewLinkType = (int)linkDetails[JsonKeys.LinkType];
                    }
                }
                ////////
                if (jObject[JsonKeys.TotalImage] != null)
                {
                    this.TotalImage = (int)jObject[JsonKeys.TotalImage];
                }
                else if (jObject[JsonKeys.ImageInCollection] != null)
                {
                    this.TotalImage = (short)jObject[JsonKeys.ImageInCollection];
                }

                if (jObject[JsonKeys.LocationDetails] != null)
                {
                    JObject location = (JObject)jObject[JsonKeys.LocationDetails];
                    if (location[JsonKeys.Location] != null && ((string)location[JsonKeys.Location]).Length > 0)
                    {
                        if (this.LocationModel == null) LocationModel = new LocationModel();
                        LocationModel.LocationName = (string)location[JsonKeys.Location];
                    }
                    if (location[JsonKeys.Latitude] != null)
                    {
                        double d = (double)location[JsonKeys.Latitude];
                        if (d != 9999.0)
                        {
                            if (this.LocationModel == null) LocationModel = new LocationModel();
                            LocationModel.Latitude = d;
                        }
                    }
                    if (location[JsonKeys.Longitude] != null)
                    {
                        double d = (double)location[JsonKeys.Longitude];
                        if (d != 9999.0)
                        {
                            if (this.LocationModel == null) LocationModel = new LocationModel();
                            LocationModel.Longitude = d;
                        }
                    }
                }
                if (LocationModel != null)
                {
                    if (LocationModel.Latitude != 0 && LocationModel.Longitude != 0 && !(LinkModel != null || BookPostType == SettingsConstants.FEED_TYPE_IMAGE || ParentFeed != null))
                    { //(BookPostType == SettingsConstants.FEED_TYPE_SINGLE_IMAGE || BookPostType == SettingsConstants.FEED_TYPE_SINGLE_IMAGE_WITH_ALBUM || BookPostType == SettingsConstants.FEED_TYPE_MULTIPLE_IMAGE_WITH_ALBUM)
                        LocationModel.ImageUrl = "http://maps.googleapis.com/maps/api/staticmap?"
                         + "center=" + LocationModel.Latitude + "," + LocationModel.Longitude
                         + "&key=" + SocialMediaConstants.GOOGLE_MAP_API_KEY
                         + "&size=600x170"
                         + "&markers=size:mid%7Ccolor:red%7C" + LocationModel.Latitude + "," + LocationModel.Longitude
                         + "&zoom=15"
                         + "&maptype=roadmap"
                         + "&sensor=false";
                        //locationModel.OnPropertyChanged("CurrentInstance");
                    }
                    else
                        LocationModel.ImageUrl = "";
                }
                if (jObject[JsonKeys.StatusTags] != null)
                {
                    JArray array = (JArray)jObject[JsonKeys.StatusTags];
                    StatusTags = new ObservableCollection<TaggedUserModel>();
                    foreach (JObject obj in array)
                    {
                        TaggedUserModel statusTagModel = new TaggedUserModel();
                        if (obj[JsonKeys.FullName] != null) statusTagModel.FullName = (string)obj[JsonKeys.FullName];
                        if (obj[JsonKeys.Position] != null) statusTagModel.Index = (int)obj[JsonKeys.Position];
                        if (obj[JsonKeys.UserTableID] != null) statusTagModel.UserTableID = (long)obj[JsonKeys.UserTableID];
                        StatusTags.Add(statusTagModel);
                    }
                }
                if (jObject[JsonKeys.FriendsTagList] != null)
                {
                    JArray array = (JArray)jObject[JsonKeys.FriendsTagList];
                    TaggedFriendsList = new ObservableCollection<BaseUserProfileModel>();
                    foreach (JObject obj in array)
                    {
                        if (obj[JsonKeys.UserTableID] != null && obj[JsonKeys.Name] != null)
                        {
                            string tfn = (string)obj[JsonKeys.Name];
                            long tutId = (long)obj[JsonKeys.UserTableID];
                            long tuId = (obj[JsonKeys.UserIdentity] != null) ? (long)obj[JsonKeys.UserIdentity] : 0;
                            string tPrIm = (obj[JsonKeys.ProfileImage] != null) ? (string)obj[JsonKeys.ProfileImage] : string.Empty;
                            BaseUserProfileModel baseModel = new BaseUserProfileModel { FullName = tfn, UserTableID = tutId, ProfileImage = tPrIm, UserIdentity = tuId, ProfileType = SettingsConstants.PROFILE_TYPE_GENERAL };
                            if (FirstTagProfile == null)
                            {
                                FirstTagProfile = baseModel;
                                //todo use GetUserProfile when generalized
                            }
                            else if (SecondTagProfile == null && array.Count == 2)
                            {
                                SecondTagProfile = baseModel;
                            }
                            TaggedFriendsList.Add(baseModel);
                        }
                    }
                    if (array.Count > 2)
                    {
                        ExtraTagCount = (array.Count - 1) + " others";
                    }
                }
                if (jObject[JsonKeys.TotalTaggedFriends] != null)
                {
                    short totaltg = (short)jObject[JsonKeys.TotalTaggedFriends];
                    //if (totaltg > 2 && SecondTagProfile != null) ExtraTagCount = (totaltg - 2).ToString();
                    //else 
                    if (SecondTagProfile == null && totaltg > 2 && FirstTagProfile != null) ExtraTagCount = (totaltg - 1) + " others";
                }
                if (jObject[JsonKeys.DoingList] != null)
                {
                    JArray array = (JArray)jObject[JsonKeys.DoingList];
                    foreach (JObject obj in array)
                    {
                        if (obj[JsonKeys.Name] != null)
                        {
                            this.DoingActivity = (string)obj[JsonKeys.Name];//temporarily "Feeling " + 
                            this.DoingId = (long)obj[JsonKeys.Id];

                            if (obj[JsonKeys.Url] != null)
                                this.DoingImageUrl = (string)obj[JsonKeys.Url];
                            else if (obj[JsonKeys.Id] != null)
                            {
                                long id = (long)obj[JsonKeys.Id];
                                DoingDTO dto = null;
                                if (NewsFeedDictionaries.Instance.DOING_DICTIONARY.TryGetValue(id, out dto))
                                {
                                    this.DoingImageUrl = dto.ImgUrl;
                                }
                            }
                        }
                    }
                }

                if (jObject[JsonKeys.AlbumDetails] != null)
                {
                    JObject albumDetails = (JObject)jObject[JsonKeys.AlbumDetails];
                    this.TotalImage = (int)albumDetails[JsonKeys.MemberOrMediaCount];
                    if (albumDetails[JsonKeys.ImageAlbumType] != null) IsStaticAlbum = ((int)albumDetails[JsonKeys.ImageAlbumType] == 1);
                    //
                    Guid ALBId = (albumDetails[JsonKeys.AlbumId] != null) ? (Guid)albumDetails[JsonKeys.AlbumId] : Guid.Empty;
                    string ALBNm = (albumDetails[JsonKeys.AlbumName] != null) ? (string)albumDetails[JsonKeys.AlbumName] : string.Empty;
                    int ALBMdaT = (albumDetails[JsonKeys.MediaType] != null) ? (int)albumDetails[JsonKeys.MediaType] : 0;
                    //
                    if (albumDetails[JsonKeys.ImageList] != null)
                    {
                        int imageType = (albumDetails[JsonKeys.ImageType] != null) ? (int)albumDetails[JsonKeys.ImageType] : 0;
                        JArray imageList = (JArray)albumDetails[JsonKeys.ImageList];
                        ImageList = new ObservableCollection<ImageModel>();
                        FeedImageList = new ObservableCollection<ImageModel>();
                        foreach (JObject singleImage in imageList)
                        {
                            ImageModel imgModel;
                            Guid imageId = (singleImage[JsonKeys.ImageId] != null) ? (Guid)singleImage[JsonKeys.ImageId] : Guid.Empty;
                            if (!ImageDataContainer.Instance.ImageModelDictionary.TryGetValue(imageId, out imgModel))
                            {
                                imgModel = new ImageModel();
                                ImageDataContainer.Instance.ImageModelDictionary[imageId] = imgModel;
                            }
                            if ((imageList.Count > 1 || BookPostType == 3 || BookPostType == 6 || BookPostType == 9)
                                && imgModel.LikeCommentShare == null)
                            {
                                imgModel.LikeCommentShare = new LikeCommentShareModel();
                                imgModel.LikeCommentShare.LoadData(singleImage);
                            }
                            imgModel.ImageType = imageType;
                            imgModel.NewsFeedId = NewsfeedId;
                            imgModel.LoadData(singleImage);
                            imgModel.SetMissingInfo(singleImage);
                            imgModel.UserShortInfoModel = PostOwner;
                            imgModel.AlbumId = ALBId; imgModel.AlbumName = ALBNm;
                            //
                            if (!string.IsNullOrEmpty(imgModel.ImageUrl))
                            {
                                if (imgModel.ImageType == SettingsConstants.TYPE_PROFILE_IMAGE)
                                {
                                    string convertedUrl = (HelperMethods.ConvertUrlToName(imgModel.ImageUrl, ImageUtility.IMG_600)).Replace('/', '_');
                                    imgModel.ImageLocation = RingIDSettings.TEMP_PROFILE_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                                }
                                else if (imgModel.ImageType == SettingsConstants.TYPE_COVER_IMAGE)
                                {
                                    string convertedUrl = (HelperMethods.ConvertUrlToName(imgModel.ImageUrl, ImageUtility.IMG_600)).Replace('/', '_');
                                    imgModel.ImageLocation = RingIDSettings.TEMP_COVER_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                                }
                                else
                                {
                                    string convertedUrl = (HelperMethods.ConvertUrlToName(imgModel.ImageUrl, ImageUtility.IMG_600)).Replace('/', '_');
                                    imgModel.ImageLocation = RingIDSettings.TEMP_BOOK_IMAGE_FOLDER + System.IO.Path.DirectorySeparatorChar + convertedUrl;
                                }
                            }
                            //
                            ImageList.Add(imgModel);
                        }
                        ImageList = new ObservableCollection<ImageModel>(ImageList.OrderBy(i => i.ImageId));
                        //ImageList.Sort((x, y) => x.ImageId.CompareTo(y.ImageId));
                        int maxImg = (ImageList.Count > 3) ? 3 : ImageList.Count;
                        for (int index = 0; index < maxImg; index++)
                        {
                            ImageModel imgMdl = ImageList[index];
                            imgMdl.FeedImageInfoModel = new FeedImageInfo();
                            imgMdl.FeedImageInfoModel.Width = imgMdl.ImageWidth;
                            imgMdl.FeedImageInfoModel.Height = imgMdl.ImageHeight;
                            FeedImageList.Add(imgMdl);
                            if (index == 2 && TotalImage > FeedImageList.Count)
                            {
                                imgMdl.MoreImagesCount = "+" + (TotalImage - FeedImageList.Count);
                            }
                        }
                        ScaleFeedImages scaler = new ScaleFeedImages(FeedImageList, ActualTime, 600);
                        ImageViewHeight = scaler.ImageViewHeight;
                        if (FeedImageList.Count == 1 && BookPostType != 3 && BookPostType != 6 && BookPostType != 9)
                        {
                            SingleImageFeedModel = FeedImageList.ElementAt(0);
                            if (SingleImageFeedModel.LikeCommentShare == null)
                                SingleImageFeedModel.LikeCommentShare = new LikeCommentShareModel();
                            LikeCommentShare = SingleImageFeedModel.LikeCommentShare;
                            SingleImageFeedModel.LikeCommentShare.LoadData(jObject);
                        }
                        else SingleImageFeedModel = null;
                        //
                        if (NewsModel != null) NewsModel.NewsPortalFeedtype = SettingsConstants.PORTALFEED_TYPE_IMAGE;
                    }
                    else if (albumDetails[JsonKeys.MediaList] != null)
                    {
                        if (MediaContent == null) MediaContent = new MediaContentModel();
                        MediaContent.MediaList = new ObservableCollection<SingleMediaModel>();
                        MediaContent.FeedMediaList = new ObservableCollection<SingleMediaModel>();
                        MediaContent.AlbumId = ALBId; MediaContent.AlbumName = ALBNm; MediaContent.MediaType = ALBMdaT;
                        JArray mediaLst = (JArray)albumDetails[JsonKeys.MediaList];
                        MediaContent.TotalMediaCount = (jObject[JsonKeys.TotalAudioOrVideoCount] != null) ? (int)jObject[JsonKeys.TotalAudioOrVideoCount] : MediaContent.MediaList.Count;
                        //if (albumDetails[JsonKeys.MediaType] != null) MediaContent.MediaType = (int)albumDetails[JsonKeys.MediaType];
                        //if (albumDetails[JsonKeys.AlbumId] != null) MediaContent.AlbumId = (Guid)albumDetails[JsonKeys.AlbumId];

                        int index = 0;
                        foreach (JObject singleMedia in mediaLst)
                        {
                            Guid contentId = (Guid)singleMedia[JsonKeys.ContentId];
                            SingleMediaModel singleMediaModel = null;
                            if (!MediaDataContainer.Instance.ContentModels.TryGetValue(contentId, out singleMediaModel))
                            {
                                singleMediaModel = new SingleMediaModel();
                                MediaDataContainer.Instance.ContentModels[contentId] = singleMediaModel;
                            }
                            if ((mediaLst.Count > 1 || BookPostType == 3 || BookPostType == 6 || BookPostType == 9)
                                && singleMediaModel.LikeCommentShare == null)
                            {
                                singleMediaModel.LikeCommentShare = new LikeCommentShareModel();
                                singleMediaModel.LikeCommentShare.LoadData(singleMedia);
                            }
                            singleMediaModel.LoadData(singleMedia, MediaContent.MediaType, true);
                            if (singleMediaModel.MediaOwner == null)
                                singleMediaModel.MediaOwner = PostOwner;
                            singleMediaModel.NewsFeedId = NewsfeedId;
                            MediaContent.MediaList.Add(singleMediaModel);
                            if (index < 2)
                            {
                                MediaContent.FeedMediaList.Add(singleMediaModel);
                            }
                            index++;
                            singleMediaModel.AlbumId = ALBId; singleMediaModel.AlbumName = ALBNm; singleMediaModel.MediaType = ALBMdaT;
                        }
                        MediaContent.MediaList.OrderBy(i => i.ContentId);
                        if (MediaContent.TotalMediaCount > 2)
                        {
                            ExtraMediaCount = "+" + (MediaContent.TotalMediaCount - 2) + " More";
                        }
                        if (MediaContent.TotalMediaCount == 1 && BookPostType != 3 && BookPostType != 6 && BookPostType != 9)
                        {
                            //if (PostOwner.ProfileType == SettingsConstants.PROFILE_TYPE_NEWSPORTAL || PostOwner.ProfileType == SettingsConstants.PROFILE_TYPE_PAGES)
                            //{
                            //    PortalMediaModel = MediaContent.MediaList.ElementAt(0); //TODO
                            //}
                            //else 
                            //if (BookPostType != 3 && BookPostType != 6 && BookPostType != 9)
                            //{
                            SingleMediaFeedModel = MediaContent.MediaList.ElementAt(0);
                            if (SingleMediaFeedModel.LikeCommentShare == null)
                                SingleMediaFeedModel.LikeCommentShare = new LikeCommentShareModel();
                            LikeCommentShare = SingleMediaFeedModel.LikeCommentShare;
                            SingleMediaFeedModel.LikeCommentShare.LoadData(jObject);
                            // }
                        }
                        else SingleMediaFeedModel = null;
                        if (NewsModel != null) NewsModel.NewsPortalFeedtype = SettingsConstants.PORTALFEED_TYPE_MEDIA;
                    }
                }
                if (LikeCommentShare == null) { LikeCommentShare = new LikeCommentShareModel(); }
                LikeCommentShare.LoadData(jObject);
                //if (MediaContent != null && MediaContent.TotalMediaCount > 1)
                //{
                //    foreach (var item in MediaContent.MediaList)
                //    {
                //        if (item.LikeCommentShare == null)
                //        {

                //        }
                //    }
                //}
                //if (ImageList != null && ImageList.Count > 1)
                //{
                //    foreach (var item in ImageList)
                //    {
                //        if (item.LikeCommentShare == null)
                //        {

                //        }
                //    }
                //}
                if (jObject[JsonKeys.OriginalFeed] != null)
                {
                    JObject parentFeedJObj = (JObject)jObject[JsonKeys.OriginalFeed];
                    Guid parentNewsfeedId = (Guid)parentFeedJObj[JsonKeys.NewsfeedId];
                    FeedDataContainer.Instance.InsertIntoSharedFeedIds(parentNewsfeedId, NewsfeedId);
                    if (FeedDataContainer.Instance.FeedModels.ContainsKey(parentNewsfeedId))
                    {
                        ParentFeed = FeedDataContainer.Instance.FeedModels[parentNewsfeedId];
                        //ParentFeed.LoadLikeCommentShareUpdatedData(parentFeedJObj);
                        ParentFeed.LoadData(parentFeedJObj);
                    }
                    else
                    {
                        ParentFeed = new FeedModel();
                        FeedDataContainer.Instance.FeedModels[parentNewsfeedId] = ParentFeed;
                        ParentFeed.LoadData(parentFeedJObj);
                    }
                    WallOrContentOwner = ParentFeed.PostOwner;
                }
                if (jObject[JsonKeys.WhoShareList] != null)
                {
                    List<Guid> extIds = null;
                    if (!FeedDataContainer.Instance.SharedNewsfeedIds.TryGetValue(NewsfeedId, out extIds))
                    {
                        extIds = new List<Guid>();
                        FeedDataContainer.Instance.SharedNewsfeedIds[NewsfeedId] = extIds;
                    }
                    WhoShareList = new ObservableCollection<FeedHolderModel>();
                    JArray whoShareListArr = (JArray)jObject[JsonKeys.WhoShareList];
                    foreach (JObject obj in whoShareListArr)
                    {
                        Guid whoShareNewsfeedId = (Guid)obj[JsonKeys.NewsfeedId];
                        if (!extIds.Contains(whoShareNewsfeedId))
                            extIds.Add(whoShareNewsfeedId);
                        FeedModel whoShareFeedModel = null;
                        if (FeedDataContainer.Instance.FeedModels.ContainsKey(whoShareNewsfeedId))
                        {
                            whoShareFeedModel = FeedDataContainer.Instance.FeedModels[whoShareNewsfeedId];
                            whoShareFeedModel.LoadLikeCommentShareUpdatedData(obj);
                        }
                        else
                        {
                            whoShareFeedModel = new FeedModel();
                            FeedDataContainer.Instance.FeedModels[whoShareNewsfeedId] = whoShareFeedModel;
                            //whoShareFeedModel.LoadSharedFeedData(obj);
                            whoShareFeedModel.FeedType = 2;
                            if (obj[JsonKeys.NewsfeedId] != null)
                            {
                                whoShareFeedModel.NewsfeedId = (Guid)obj[JsonKeys.NewsfeedId];
                            }
                            whoShareFeedModel.Status = (obj[JsonKeys.Status] != null) ? (string)obj[JsonKeys.Status] : "";
                            if (obj[JsonKeys.ActualTime] != null)
                            {
                                whoShareFeedModel.ActualTime = (long)obj[JsonKeys.ActualTime];
                            }
                            if (obj[JsonKeys.WallOwner] != null)
                            {
                                JObject jObj = (JObject)obj[JsonKeys.WallOwner];
                                long utId = (int)jObj[JsonKeys.UserTableID];
                                //whoShareFeedModel.WallOrContentOwner = 
                                whoShareFeedModel.PostOwner = (utId == DefaultSettings.LOGIN_TABLE_ID) ? RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel : new BaseUserProfileModel(jObj);
                            }
                            whoShareFeedModel.FeedPanelType = 10;
                        }
                        //if (ParentFeed == null && whoShareFeedModel.ParentFeed != null)
                        //    ParentFeed = whoShareFeedModel.ParentFeed;
                        //else if (ParentFeed != null && whoShareFeedModel.ParentFeed == null)
                        //if (whoShareFeedModel.Status.Equals("S8I"))
                        //{

                        //}
                        if (whoShareFeedModel.ParentFeed == null)
                        {
                            whoShareFeedModel.ParentFeed = this;
                            whoShareFeedModel.WallOrContentOwner = this.PostOwner;
                        }
                        whoShareFeedModel.Privacy = SettingsConstants.PRIVACY_PUBLIC;
                        if (whoShareFeedModel.LikeCommentShare == null)
                            whoShareFeedModel.LikeCommentShare = new LikeCommentShareModel();
                        if (!WhoShareList.Any(P => P.FeedId == whoShareFeedModel.NewsfeedId))
                        {
                            FeedHolderModel holder = new FeedHolderModel(2);
                            holder.FeedId = whoShareFeedModel.NewsfeedId;
                            holder.Feed = whoShareFeedModel;
                            //holder.FeedPanelType = 15; //confusion
                            holder.ShortModel = false;
                            WhoShareList.Add(holder);
                        }
                        WhoShareList = new ObservableCollection<FeedHolderModel>(WhoShareList.OrderBy(i => i.Feed.Time));
                    }
                    if (WhoShareList.Count == 0 || (LikeCommentShare != null && LikeCommentShare.NumberOfShares == 0))
                        ShowMoreSharesState = 0;
                    else if (WhoShareList.Count > 0 && LikeCommentShare != null && WhoShareList.Count < LikeCommentShare.NumberOfShares)
                        ShowMoreSharesState = 2;
                }
                //if (Status.Equals("S8I"))
                //{

                //}
                if (ParentFeed != null)
                {
                    FeedPanelType = 1;
                }
                else if (NewsModel != null && this.PostOwner.ProfileType == SettingsConstants.PROFILE_TYPE_NEWSPORTAL)
                {
                    FeedPanelType = 7;
                }
                else if (NewsModel != null && this.PostOwner.ProfileType == SettingsConstants.PROFILE_TYPE_PAGES)
                {
                    FeedPanelType = 11;
                }
                else if (NewsModel != null && this.PostOwner.ProfileType == SettingsConstants.PROFILE_TYPE_MUSICPAGE)
                {
                    FeedPanelType = 14;
                }
                else if (this.PostOwner.ProfileType == SettingsConstants.PROFILE_TYPE_CELEBRITY)
                {
                    FeedPanelType = 12;
                }
                else if (SingleImageFeedModel != null)
                    FeedPanelType = 3;
                else if (SingleMediaFeedModel != null)
                    FeedPanelType = 4;
                else if (ImageList != null && ImageList.Count > 0)
                    FeedPanelType = 2;
                else if (MediaContent != null)
                    FeedPanelType = 5;
                else if (LinkModel != null)
                    FeedPanelType = 8;
                else if (LocationModel != null && LocationModel.Latitude != 0 && LocationModel.Longitude != 0)
                    FeedPanelType = 9;
                else
                    FeedPanelType = 10;
            }
            catch (Exception ex)
            {
                log.Debug("Error Message : " + ex.Message + "Stack Trace" + ex.StackTrace);
            }
        }

        public void LoadLikeCommentShareUpdatedData(JObject jObject)
        {
            if (jObject[JsonKeys.Status] != null)
            {
                this.Status = (string)jObject[JsonKeys.Status];
            }
            else
                this.Status = "";
            //TODO tagged int &ext
            //if (jObject[JsonKeys.NumberOfComments] != null)
            //{
            //    this.NumberOfComments = (long)jObject[JsonKeys.NumberOfComments];
            //}
            //if (jObject[JsonKeys.NumberOfLikes] != null)
            //{
            //    this.NumberOfLikes = (long)jObject[JsonKeys.NumberOfLikes];
            //}
            //if (jObject[JsonKeys.NumberOfShares] != null)
            //{
            //    this.NumberOfShares = (long)jObject[JsonKeys.NumberOfShares];
            //}
            //if (jObject[JsonKeys.ILike] != null)
            //{
            //    this.ILike = (short)jObject[JsonKeys.ILike];
            //}
            //if (jObject[JsonKeys.IComment] != null)
            //{
            //    this.IComment = (short)jObject[JsonKeys.IComment];
            //}
            //if (jObject[JsonKeys.IShare] != null)
            //{
            //    this.IShare = (short)jObject[JsonKeys.IShare];
            //}
            if (jObject[JsonKeys.IsSaved] != null)
            {
                this.IsSaved = (bool)jObject[JsonKeys.IsSaved];
            }
            //this.Privacy = SettingsConstants.PRIVACY_PUBLIC;
        }

        private ObservableCollection<CommentModel> _CommentList = new ObservableCollection<CommentModel>();
        public ObservableCollection<CommentModel> CommentList
        {
            get
            {
                return _CommentList;
            }
            set
            {
                _CommentList = value;
                this.OnPropertyChanged("CommentList");
            }
        }
        private BaseUserProfileModel _Activist;
        public BaseUserProfileModel Activist
        {
            get { return _Activist; }
            set
            {
                if (value == _Activist)
                    return;
                _Activist = value;
                this.OnPropertyChanged("Activist");
            }
        }
        private int _ActivityType; // 1= Like, 2= Comment, 3= share
        public int ActivityType
        {
            get { return _ActivityType; }
            set
            {
                if (value == _ActivityType)
                    return;
                _ActivityType = value;
                this.OnPropertyChanged("ActivityType");
            }
        }
        private BaseUserProfileModel _PostOwner;
        public BaseUserProfileModel PostOwner
        {
            get { return _PostOwner; }
            set
            {
                if (value == _PostOwner)
                    return;
                _PostOwner = value;
                this.OnPropertyChanged("PostOwner");
            }
        }
        private BaseUserProfileModel _WallOrContentOwner; //wall owner or share content's owner (nt shared feed,shared feed owner is from feedmodel.parentfeed.postowner)
        public BaseUserProfileModel WallOrContentOwner
        {
            get { return _WallOrContentOwner; }
            set
            {
                if (value == _WallOrContentOwner)
                    return;
                _WallOrContentOwner = value;
                this.OnPropertyChanged("WallOrContentOwner");
            }
        }
        private Guid _NewsfeedId;
        public Guid NewsfeedId
        {
            get { return _NewsfeedId; }
            set
            {
                if (value == _NewsfeedId)
                    return;
                _NewsfeedId = value;
                this.OnPropertyChanged("NewsfeedId");
            }
        }
        private string _Status;
        public string Status
        {
            get { return _Status; }
            set
            {
                if (value == _Status)
                    return;
                _Status = value;
                this.OnPropertyChanged("Status");
            }
        }

        private long _ActualTime;
        public long ActualTime
        {
            get { return _ActualTime; }
            set
            {
                if (value == _ActualTime)
                    return;
                _ActualTime = value;
                this.OnPropertyChanged("ActualTime");
            }
        }
        private long _Time;
        public long Time
        {
            get { return _Time; }
            set
            {
                if (value == _Time)
                    return;
                _Time = value;
                this.OnPropertyChanged("Time");
            }
        }
        private long _SavedTime;
        public long SavedTime
        {
            get { return _SavedTime; }
            set
            {
                if (value == _SavedTime)
                    return;
                _SavedTime = value;
                this.OnPropertyChanged("SavedTime");
            }
        }
        //private long _NumberOfComments;
        //public long NumberOfComments
        //{
        //    get { return _NumberOfComments; }
        //    set
        //    {
        //        if (value == _NumberOfComments)
        //            return;
        //        _NumberOfComments = value;
        //        this.OnPropertyChanged("NumberOfComments");
        //    }
        //}
        private bool _NoMoreComments = false;
        public bool NoMoreComments
        {
            get { return _NoMoreComments; }
            set
            {
                //if (value == _NoMoreComments)
                //    return;
                _NoMoreComments = value;
                this.OnPropertyChanged("NoMoreComments");
            }
        }
        private int _ShowNextCommentsVisible = 0; //0=next cm,prev cm hide& cmnPanel hide// 1=next cm,prev cm show, cmn show, //2=next cm,prev cm hide, cmn show, 
        public int ShowNextCommentsVisible
        {
            get { return _ShowNextCommentsVisible; }
            set
            {
                if (value == _ShowNextCommentsVisible)
                    return;
                _ShowNextCommentsVisible = value;
                this.OnPropertyChanged("ShowNextCommentsVisible");
            }
        }
        private int _ShowMoreSharesState = 0;  // 0=both Text & loader collapsed,  1= only Text collapsed,loader visible,  2= only loader collapsed, text visible
        public int ShowMoreSharesState
        {
            get { return _ShowMoreSharesState; }
            set
            {
                if (value == _ShowMoreSharesState)
                    return;
                _ShowMoreSharesState = value;
                this.OnPropertyChanged("ShowMoreSharesState");
            }
        }
        //private long _NumberOfShares;
        //public long NumberOfShares
        //{
        //    get { return _NumberOfShares; }
        //    set
        //    {
        //        if (value == _NumberOfShares)
        //            return;
        //        _NumberOfShares = value;
        //        this.OnPropertyChanged("NumberOfShares");
        //    }
        //}
        //private long _NumberOfLikes;
        //public long NumberOfLikes
        //{
        //    get { return _NumberOfLikes; }
        //    set
        //    {
        //        if (value == _NumberOfLikes)
        //            return;
        //        _NumberOfLikes = value;
        //        this.OnPropertyChanged("NumberOfLikes");
        //    }
        //}
        private ObservableCollection<ImageModel> _ImageList;
        public ObservableCollection<ImageModel> ImageList
        {
            get { return _ImageList; }
            set
            {
                if (value == _ImageList)
                    return;
                _ImageList = value;
                this.OnPropertyChanged("ImageList");
            }
        }
        private ObservableCollection<ImageModel> _FeedImageList;
        public ObservableCollection<ImageModel> FeedImageList
        {
            get { return _FeedImageList; }
            set
            {
                if (value == _FeedImageList)
                    return;
                _FeedImageList = value;
                this.OnPropertyChanged("FeedImageList");
            }
        }
        //private short _ILike;
        //public short ILike
        //{
        //    get { return _ILike; }
        //    set
        //    {
        //        // if (value == _ILike)
        //        //    return;
        //        _ILike = value;
        //        this.OnPropertyChanged("ILike");
        //    }
        //}
        //private short _IComment;
        //public short IComment
        //{
        //    get { return _IComment; }
        //    set
        //    {
        //        if (value == _IComment)
        //            return;
        //        _IComment = value;
        //        this.OnPropertyChanged("IComment");
        //    }
        //}
        //private short _IShare;
        //public short IShare
        //{
        //    get { return _IShare; }
        //    set
        //    {
        //        if (value == _IShare)
        //            return;
        //        _IShare = value;
        //        this.OnPropertyChanged("IShare");
        //    }
        //}
        private LikeCommentShareModel _LikeCommentShare;
        public LikeCommentShareModel LikeCommentShare
        {
            get { return _LikeCommentShare; }
            set
            {
                if (value == _LikeCommentShare)
                    return;
                _LikeCommentShare = value;
                this.OnPropertyChanged("LikeCommentShare");
            }
        }
        private int _TotalImage;
        public int TotalImage
        {
            get { return _TotalImage; }
            set
            {
                if (value == _TotalImage)
                    return;
                _TotalImage = value;
                this.OnPropertyChanged("TotalImage");
            }
        }
        private short _BookPostType;
        public short BookPostType
        {
            get { return _BookPostType; }
            set
            {
                if (value == _BookPostType)
                    return;
                _BookPostType = value;
                this.OnPropertyChanged("BookPostType");
            }
        }
        private short _FeedType;
        //FeedType -1 storagefeed//0 for newstatus//1 for loader added but loaderpanel invisible// 2 for feed// 3 for loadergif visible nomoretxtpnl inv
        //4= nomoretxtpnl visible, loadergif invisible//5= reload btn visible
        public short FeedType
        {
            get { return _FeedType; }
            set
            {
                if (value == _FeedType)
                    return;
                _FeedType = value;
                this.OnPropertyChanged("FeedType");
            }
        }
        private short _FeedPanelType = 0;
        public short FeedPanelType
        {
            get { return _FeedPanelType; }
            set
            {
                if (value == _FeedPanelType)
                    return;
                _FeedPanelType = value;
                this.OnPropertyChanged("FeedPanelType");
            }
        }
        private short _FeedSubType;
        public short FeedSubType
        {
            get { return _FeedSubType; }
            set
            {
                if (value == _FeedSubType)
                    return;
                _FeedSubType = value;
                this.OnPropertyChanged("FeedSubType");
            }
        }
        private short _FeedCategory;
        public short FeedCategory
        {
            get { return _FeedCategory; }
            set
            {
                if (value == _FeedCategory)
                    return;
                _FeedCategory = value;
                this.OnPropertyChanged("FeedCategory");
            }
        }
        private bool _ShowContinue;
        public bool ShowContinue
        {
            get { return _ShowContinue; }
            set
            {
                if (value == _ShowContinue)
                    return;
                _ShowContinue = value;
                this.OnPropertyChanged("ShowContinue");
            }
        }
        private SingleMediaModel _SingleMediaFeedModel;
        public SingleMediaModel SingleMediaFeedModel
        {
            get { return _SingleMediaFeedModel; }
            set
            {
                if (value == _SingleMediaFeedModel)
                    return;
                _SingleMediaFeedModel = value;
                this.OnPropertyChanged("SingleMediaFeedModel");
            }
        }
        //private SingleMediaModel _PortalMediaModel;
        //public SingleMediaModel PortalMediaModel
        //{
        //    get { return _PortalMediaModel; }
        //    set
        //    {
        //        if (value == _PortalMediaModel)
        //            return;
        //        _PortalMediaModel = value;
        //        this.OnPropertyChanged("PortalMediaModel");
        //    }
        //}
        private ImageModel _SingleImageFeedModel;
        public ImageModel SingleImageFeedModel
        {
            get { return _SingleImageFeedModel; }
            set
            {
                if (value == _SingleImageFeedModel)
                    return;
                _SingleImageFeedModel = value;
                this.OnPropertyChanged("SingleImageFeedModel");
            }
        }

        private NewsModel _NewsModel;
        public NewsModel NewsModel
        {
            get { return _NewsModel; }
            set
            {
                if (value == _NewsModel)
                    return;
                _NewsModel = value;
                this.OnPropertyChanged("NewsModel");
            }
        }
        private int _Privacy;
        public int Privacy
        {
            get { return _Privacy; }
            set
            {
                if (value == _Privacy)
                    return;

                _Privacy = value;
                this.OnPropertyChanged("Privacy");
            }
        }
        private bool _IsEditMode = false;
        public bool IsEditMode
        {
            get { return _IsEditMode; }
            set
            {
                if (value == _IsEditMode)
                    return;
                _IsEditMode = value;
                this.OnPropertyChanged("IsEditMode");
            }
        }
        private LocationModel _locationModel;
        public LocationModel LocationModel
        {
            get { return _locationModel; }
            set
            {
                if (value == _locationModel)
                    return;
                _locationModel = value;
                this.OnPropertyChanged("LocationModel");
            }
        }

        private double _ImageViewHeight;
        public double ImageViewHeight
        {
            get { return _ImageViewHeight; }
            set
            {
                if (value == _ImageViewHeight) return;
                _ImageViewHeight = value;
                this.OnPropertyChanged("ImageViewHeight");
            }
        }
        private int _Validity;
        public int Validity
        {
            get { return _Validity; }
            set
            {
                if (value == _Validity)
                    return;

                _Validity = value;
                this.OnPropertyChanged("Validity");
            }
        }
        private FeedModel _ParentFeed;
        public FeedModel ParentFeed
        {
            get { return _ParentFeed; }
            set
            {
                if (value == _ParentFeed)
                    return;
                _ParentFeed = value;
                this.OnPropertyChanged("ParentFeed");
            }
        }

        private ObservableCollection<FeedHolderModel> _WhoShareList;
        public ObservableCollection<FeedHolderModel> WhoShareList
        {
            get { return _WhoShareList; }
            set
            {
                if (value == _WhoShareList)
                    return;
                _WhoShareList = value;
                this.OnPropertyChanged("WhoShareList");
            }
        }
        private string _DoingActivity = "";
        public string DoingActivity
        {
            get { return _DoingActivity; }
            set
            {
                if (value == _DoingActivity)
                    return;
                _DoingActivity = value;
                this.OnPropertyChanged("DoingActivity");
            }
        }
        private string _DoingImageUrl = "";
        public string DoingImageUrl
        {
            get { return _DoingImageUrl; }
            set
            {
                if (value == _DoingImageUrl)
                    return;
                _DoingImageUrl = value;
                this.OnPropertyChanged("DoingImageUrl");
            }
        }

        private long _doingId = 0;
        public long DoingId
        {
            get
            {
                return _doingId;
            }
            set
            {
                if (value == _doingId)
                {
                    return;
                }
                _doingId = value;
                this.OnPropertyChanged("DoingId");
            }
        }

        private LinkInformationModel _LinkModel;
        public LinkInformationModel LinkModel
        {
            get { return _LinkModel; }
            set
            {
                if (value == _LinkModel)
                    return;
                _LinkModel = value;
                this.OnPropertyChanged("LinkModel");
            }
        }
        private string _ExtraMediaCount = null;
        public string ExtraMediaCount
        {
            get { return _ExtraMediaCount; }
            set
            {
                if (value == _ExtraMediaCount)
                    return;
                _ExtraMediaCount = value;
                this.OnPropertyChanged("ExtraMediaCount");
            }
        }
        private ObservableCollection<BaseUserProfileModel> _TaggedFriendsList;
        public ObservableCollection<BaseUserProfileModel> TaggedFriendsList
        {
            get { return _TaggedFriendsList; }
            set
            {
                if (value == _TaggedFriendsList)
                    return;
                _TaggedFriendsList = value;
                this.OnPropertyChanged("TaggedFriendsList");
            }
        }
        private BaseUserProfileModel _FirstTagProfile;
        public BaseUserProfileModel FirstTagProfile
        {
            get { return _FirstTagProfile; }
            set
            {
                if (value == _FirstTagProfile)
                    return;
                _FirstTagProfile = value;
                this.OnPropertyChanged("FirstTagProfile");
            }
        }
        private BaseUserProfileModel _SecondTagProfile;
        public BaseUserProfileModel SecondTagProfile
        {
            get { return _SecondTagProfile; }
            set
            {
                if (value == _SecondTagProfile)
                    return;
                _SecondTagProfile = value;
                this.OnPropertyChanged("SecondTagProfile");
            }
        }
        private string _ExtraTagCount = string.Empty; //after 2
        public string ExtraTagCount
        {
            get { return _ExtraTagCount; }
            set
            {
                if (value == _ExtraTagCount)
                    return;
                _ExtraTagCount = value;
                this.OnPropertyChanged("ExtraTagCount");
            }
        }
        private bool _IsStaticAlbum = false;
        public bool IsStaticAlbum
        {
            get { return _IsStaticAlbum; }
            set
            {
                if (value == _IsStaticAlbum)
                    return;
                _IsStaticAlbum = value;
                this.OnPropertyChanged("IsStaticAlbum");
            }
        }
        private MediaContentModel _MediaContent;
        public MediaContentModel MediaContent
        {
            get { return _MediaContent; }
            set
            {
                if (value == _MediaContent)
                    return;
                _MediaContent = value;
                this.OnPropertyChanged("MediaContent");
            }
        }
        private ObservableCollection<TaggedUserModel> _StatusTags;
        public ObservableCollection<TaggedUserModel> StatusTags
        {
            get { return _StatusTags; }
            set
            {
                if (value == _StatusTags)
                    return;
                _StatusTags = value;
                this.OnPropertyChanged("StatusTags");
            }
        }
        private FeedModel _CurrentInstance;
        public FeedModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                if (value == _CurrentInstance)
                    return;
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }
        private bool _IsShareListVisible = false;
        public bool IsShareListVisible
        {
            get { return _IsShareListVisible; }
            set
            {
                if (value == _IsShareListVisible)
                    return;

                _IsShareListVisible = value;
                this.OnPropertyChanged("IsShareListVisible");
            }
        }
        private bool _IsSaved;
        public bool IsSaved
        {
            get { return _IsSaved; }
            set
            {
                if (value == _IsSaved)
                    return;

                _IsSaved = value;
                this.OnPropertyChanged("IsSaved");
            }
        }
        private bool _Edited = false;
        public bool Edited
        {
            get { return _Edited; }
            set
            {
                if (value == _Edited)
                    return;
                _Edited = value;
                this.OnPropertyChanged("Edited");
            }
        }

        #region "singleFeed"
        private Visibility _popupLikeLoaderVisibility = Visibility.Collapsed;
        public Visibility PopupLikeLoaderVisibility
        {
            get
            {
                return _popupLikeLoaderVisibility;
            }
            set
            {
                if (value == _popupLikeLoaderVisibility)
                    return;
                _popupLikeLoaderVisibility = value;
                OnPropertyChanged("PopupLikeLoaderVisibility");
            }
        }

        private string _popupLikeLoader;
        public string PopupLikeLoader
        {
            get
            {
                return _popupLikeLoader;
            }
            set
            {
                if (value == _popupLikeLoader)
                    return;
                _popupLikeLoader = value;
                OnPropertyChanged("PopupLikeLoader");
            }
        }
        #endregion
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
﻿using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel;
using View.ViewModel;

namespace View.BindingModels
{
    public class HashTagModel : INotifyPropertyChanged
    {
        public void LoadData(JObject jObject)
        {
            if (jObject[JsonKeys.MediaItemsCount] != null)
            {
                this.NoOfItems = (int)jObject[JsonKeys.MediaItemsCount];
            }
            //else if (jObject[JsonKeys.Weight] != null)
            //{
            //    this.NoOfItems = (int)jObject[JsonKeys.Weight];
            //}
                
            if (jObject[JsonKeys.HashTagId] != null)
            {
                this.HashTagId = (long)jObject[JsonKeys.HashTagId];
            }
            if (jObject[JsonKeys.HashTagName] != null)
            {
                this.HashTagSearchKey = (string)jObject[JsonKeys.HashTagName];
            }
            else if (jObject[JsonKeys.MediaSearchKey] != null)
            {
                this.HashTagSearchKey = (string)jObject[JsonKeys.MediaSearchKey];
            }
            if(jObject[JsonKeys.Weight] != null)
            {
                this.Weight = (int)jObject[JsonKeys.Weight];
            }
        }
        public void LoadData(HashTagDTO dto)
        {
            this.HashTagId = dto.HashTagID;
            this.HashTagSearchKey = dto.HashTagSearchKey;
            this.NoOfItems = dto.NoOfItems;
            this.Weight = dto.Weight;
        }

        private long _HashTagId;
        public long HashTagId
        {
            get { return _HashTagId; }
            set
            {
                if (value == _HashTagId)
                    return;

                _HashTagId = value;
                this.OnPropertyChanged("HashTagId");
            }
        }

        private int _NoOfItems;
        public int NoOfItems
        {
            get { return _NoOfItems; }
            set
            {
                if (value == _NoOfItems)
                    return;

                _NoOfItems = value;
                this.OnPropertyChanged("NoOfItems");
            }
        }

        private string _HashTagSearchKey = string.Empty;
        public string HashTagSearchKey
        {
            get { return _HashTagSearchKey; }
            set
            {
                if (value == _HashTagSearchKey)
                    return;

                _HashTagSearchKey = value;
                this.OnPropertyChanged("HashTagSearchKey");
            }
        }

        private bool _RemoveEnabled = true;
        public bool RemoveEnabled
        {
            get { return _RemoveEnabled; }
            set
            {
                if (value == _RemoveEnabled)
                    return;

                _RemoveEnabled = value;
                this.OnPropertyChanged("RemoveEnabled");
            }
        }

        private short _TagType = 1;
        public short TagType
        {
            get { return _TagType; }
            set
            {
                if (value == _TagType)
                    return;
                _TagType = value;
                this.OnPropertyChanged("TagType");
            }
        }

        private int _weight;
        public int Weight
        {
            get
            {
                return _weight;
            }
            set
            {
                if(value == _weight)
                {
                    return;
                }
                _weight = value;
                this.OnPropertyChanged("Weight");
            }
        }


        private ObservableCollection<SingleMediaModel> _MediaList;
        public ObservableCollection<SingleMediaModel> MediaList
        {
            get { return _MediaList; }
            set
            {
                if (_MediaList == value) return;
                _MediaList = value;
                OnPropertyChanged("MediaList");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

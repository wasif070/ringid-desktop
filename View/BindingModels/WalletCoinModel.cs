﻿using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace View.BindingModels
{
    public class WalletCoinModel : INotifyPropertyChanged
    {
        private long userTableId = 0;
        public long UserTableID
        {
            get
            {
                return userTableId;
            }
            set
            {
                if (value == userTableId)
                    return;

                userTableId = value;
                this.OnPropertyChanged("UserTableID");
            }
        }
        private int coinId = 0;
        public int CoinId
        {
            get { return coinId; }
            set
            {
                if (value == coinId)
                    return;
                coinId = value;
                this.OnPropertyChanged("CoinId");
            }
        }
        
        private string coinName = string.Empty;
        public string CoinName
        {
            get { return coinName; }
            set
            {
                if (value == coinName)
                    return;
                coinName = value;
                this.OnPropertyChanged("CoinName");
            }
        }

        private int quantity = 0;
        public int Quantity
        {
            get { return quantity; }
            set
            {
                if (value == quantity)
                    return;
                quantity = value;
                this.OnPropertyChanged("Quantity");
            }
        }
        private int availableQuantity = 0;
        public int AvailableQuantity
        {
            get { return availableQuantity; }
            set
            {
                if (value == availableQuantity)
                    return;
                availableQuantity = value;
                this.OnPropertyChanged("AvailableQuantity");
            }
        }
        private int totalQuantity = 0;
        public int TotalQuantity
        {
            get { return totalQuantity; }
            set
            {
                if (value == totalQuantity)
                    return;
                totalQuantity = value;
                this.OnPropertyChanged("TotalQuantity");
            }
        }

        public static WalletCoinModel GetDataFromJson(JObject jObject)
        {
            WalletCoinModel model = new WalletCoinModel();
            try
            {
                if (jObject[WalletConstants.RSP_KEY_COIN_NAME] != null)
                {
                    model.CoinName = (string)jObject[WalletConstants.RSP_KEY_COIN_NAME];
                }
                if (jObject[WalletConstants.RSP_KEY_QUANTITY] != null)
                {
                    model.Quantity = (int)jObject[WalletConstants.RSP_KEY_QUANTITY];
                }
                if (jObject[WalletConstants.RSP_KEY_COIN_ID] != null)
                {
                    model.CoinId = (int)jObject[WalletConstants.RSP_KEY_COIN_ID];
                }
                //current status -- there is only one kind of coin id and that is 1
                model.CoinId = WalletConstants.COIN_ID_GOLD;
            }
            catch (Exception)
            {
                model.CoinId = WalletConstants.COIN_ID_DEFAULT;
                //throw;
            }
            return model;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

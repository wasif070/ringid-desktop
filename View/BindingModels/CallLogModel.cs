﻿using Models.Entity;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using View.Utility;

namespace View.BindingModels
{
    public class CallLogModel : INotifyPropertyChanged
    {

        private bool _ForOnlyChatView = false;

        #region Constructor

        public CallLogModel(bool forOnlyChatView = false)
        {
            this._ForOnlyChatView = forOnlyChatView;
            this._CurrentInstance = this;
        }

        public CallLogModel(CallLogDTO callLogDTO, bool forOnlyChatView = false)
        {
            this._ForOnlyChatView = forOnlyChatView;
            this._CurrentInstance = this;
            LoadData(callLogDTO);
        }

        #endregion Constructor

        #region Property

        private CallLogModel _CurrentInstance;
        public CallLogModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }

        private string _CallID;
        public string CallID
        {
            get { return _CallID; }
            set
            {
                if (value == _CallID)
                    return;

                _CallID = value;
                this.OnPropertyChanged("CallID");
            }
        }

        private int _CallCategory;
        public int CallCategory
        {
            get { return _CallCategory; }
            set
            {
                if (value == _CallCategory)
                    return;

                _CallCategory = value;
                this.OnPropertyChanged("CallCategory");
            }
        }

        private int _CallType;
        public int CallType
        {
            get { return _CallType; }
            set
            {
                if (value == _CallType)
                    return;

                _CallType = value;
                this.OnPropertyChanged("CallType");
            }
        }

        private long _FriendTableID;
        public long FriendTableID
        {
            get { return _FriendTableID; }
            set
            {
                if (value == _FriendTableID)
                    return;

                _FriendTableID = value;
                this.OnPropertyChanged("FriendTableID");
            }
        }

        private long _CallDuration;
        public long CallDuration
        {
            get { return _CallDuration; }
            set
            {
                if (value == _CallDuration)
                    return;

                _CallDuration = value;
                this.OnPropertyChanged("CallDuration");
            }
        }
        
        private long _CallingTime;
        public long CallingTime
        {
            get { return _CallingTime; }
            set
            {
                if (value == _CallingTime)
                    return;

                _CallingTime = value;
                this.OnPropertyChanged("CallingTime");
            }
        }

        private string _Message;
        public string Message
        {
            get { return _Message; }
            set
            {
                if (value == _Message)
                    return;

                _Message = value;
                this.OnPropertyChanged("Message");
            }
        }

        private bool _IsUnread = false;
        public bool IsUnread
        {
            get { return _IsUnread; }
            set
            {
                if (value == _IsUnread)
                    return;

                _IsUnread = value;
                this.OnPropertyChanged("IsUnread");
            }
        }

        private bool _IsChecked = false;
        public bool IsChecked
        {
            get { return _IsChecked; }
            set
            {
                if (value == _IsChecked)
                    return;

                _IsChecked = value;
                this.OnPropertyChanged("IsChecked");
            }
        }

        private string _CallLogID;
        public string CallLogID
        {
            get { return _CallLogID; }
            set
            {
                if (value == _CallLogID)
                    return;

                _CallLogID = value;
                this.OnPropertyChanged("CallLogID");
            }
        }

        private long _LastCallingTime;
        public long LastCallingTime
        {
            get { return _LastCallingTime; }
            set
            {
                if (value == _LastCallingTime)
                    return;

                _LastCallingTime = value;
                this.OnPropertyChanged("LastCallingTime");
            }
        }

        private ObservableDictionary<string, long> _CallIDs;
        public ObservableDictionary<string, long> CallIDs
        {
            get { return _CallIDs; }
            private set
            {
                if (value == _CallIDs)
                    return;

                _CallIDs = value;
                this.OnPropertyChanged("CallIDs");
            }
        }

        #endregion Property

        #region Utility Method

        public void LoadData(CallLogDTO callLogDTO)
        {
            this.CallID = callLogDTO.CallID;
            this.CallCategory = callLogDTO.CallCategory;
            this.CallType = callLogDTO.CallType;
            this.FriendTableID = callLogDTO.FriendTableID;
            this.CallDuration = callLogDTO.CallDuration;
            this.CallingTime = callLogDTO.CallingTime;
            this.Message = callLogDTO.Message;
            this.IsUnread = callLogDTO.IsUnread; 
            this.CallLogID = callLogDTO.CallLogID;

            if (!this._ForOnlyChatView)
            {

                this.LastCallingTime = callLogDTO.LastCallingTime > 0 ? callLogDTO.LastCallingTime : callLogDTO.CallingTime;

                if (this.CallIDs == null)
                {
                    this.CallIDs = new ObservableDictionary<string, long>();
                }
                else
                {
                    this.CallIDs.Clear();
                }

                if (callLogDTO.CallIDs != null && callLogDTO.CallIDs.Count > 0)
                {
                    foreach (KeyValuePair<string, long> pair in callLogDTO.CallIDs.ToArray())
                    {
                        this.CallIDs[pair.Key] = pair.Value;
                    }
                }
                else if (this.CallID != null)
                {
                    this.CallIDs[this.CallID] = this.CallingTime;
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods
    }
}

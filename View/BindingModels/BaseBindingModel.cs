﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace View.BindingModels
{
    public class BaseBindingModel : INotifyPropertyChanged
    {
        /// <summary>
        /// This is the base binding model class. Every binding model class should inherit this class where INotifyPropertyChanged is needed.
        /// It implements INotifyPropertyChanged class, which helps to notify UI in case of property changes.
        /// </summary>
        protected virtual void SetProperty<T>(ref T member, T val, string propertyName)
        {
            if (object.Equals(member, val))
            {
                return;
            }
            member = val;
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}

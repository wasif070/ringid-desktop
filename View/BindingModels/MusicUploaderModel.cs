﻿using System;
using System.ComponentModel;
using System.Windows.Media.Imaging;

namespace View.BindingModels
{
    public class MusicUploaderModel : INotifyPropertyChanged
    {
        public MusicUploaderModel()
        {
            CurrentInstance = this;
            System.GC.SuppressFinalize(this);
        }
        public Guid ContentId;
        private int _Pecentage;
        public int Pecentage
        {
            get { return _Pecentage; }
            set
            {
                if (value == _Pecentage)
                    return;

                _Pecentage = value;
                this.OnPropertyChanged("Pecentage");
            }
        }

        private string _AudioTitle = null;
        public string AudioTitle
        {
            get { return _AudioTitle; }
            set
            {
                if (value == _AudioTitle)
                    return;

                _AudioTitle = value;
                this.OnPropertyChanged("AudioTitle");
            }
        }
        private string _AudioArtist = null;
        public string AudioArtist
        {
            get { return _AudioArtist; }
            set
            {
                if (value == _AudioArtist)
                    return;

                _AudioArtist = value;
                this.OnPropertyChanged("AudioArtist");
            }
        }
        private int _ImageWidth;
        public int ImageWidth
        {
            get { return _ImageWidth; }
            set
            {
                if (value == _ImageWidth)
                    return;

                _ImageWidth = value;
                this.OnPropertyChanged("ImageWidth");
            }
        }
        private int _ImageHeight;
        public int ImageHeight
        {
            get { return _ImageHeight; }
            set
            {
                if (value == _ImageHeight)
                    return;

                _ImageHeight = value;
                this.OnPropertyChanged("ImageHeight");
            }
        }
        private long _AudioDuration;
        public long AudioDuration
        {
            get { return _AudioDuration; }
            set
            {
                if (value == _AudioDuration)
                    return;

                _AudioDuration = value;
                this.OnPropertyChanged("AudioDuration");
            }
        }
        private bool _TitleEnabled = true;
        public bool TitleEnabled
        {
            get { return _TitleEnabled; }
            set
            {
                if (value == _TitleEnabled)
                    return;

                _TitleEnabled = value;
                this.OnPropertyChanged("TitleEnabled");
            }
        }
        private bool _IsUploadedInAudioServer = false;
        public bool IsUploadedInAudioServer
        {
            get { return _IsUploadedInAudioServer; }
            set
            {
                if (value == _IsUploadedInAudioServer)
                    return;

                _IsUploadedInAudioServer = value;
                this.OnPropertyChanged("IsUploadedInAudioServer");
            }
        }
        private bool _IsUploading = false;
        public bool IsUploading
        {
            get { return _IsUploading; }
            set
            {
                if (value == _IsUploading)
                    return;

                _IsUploading = value;
                this.OnPropertyChanged("IsUploading");
            }
        }
        private bool _IsImageFound = false;
        public bool IsImageFound
        {
            get { return _IsImageFound; }
            set
            {
                if (value == _IsImageFound)
                    return;

                _IsImageFound = value;
                this.OnPropertyChanged("IsImageFound");
            }
        }
        private string _FilePath = string.Empty;
        public string FilePath
        {
            get { return _FilePath; }
            set
            {
                if (value == _FilePath)
                    return;

                _FilePath = value;
                this.OnPropertyChanged("FilePath");
            }
        }
        private long _FileSize;
        public long FileSize
        {
            get { return _FileSize; }
            set
            {
                if (value == _FileSize)
                    return;

                _FileSize = value;
                this.OnPropertyChanged("FileSize");
            }
        }
        private string _ThumbUrl = null;
        public string ThumbUrl
        {
            get { return _ThumbUrl; }
            set
            {
                if (value == _ThumbUrl)
                    return;
                _ThumbUrl = value;
                this.OnPropertyChanged("ThumbUrl");
            }
        }
        private string _StreamUrl = null;
        public string StreamUrl
        {
            get { return _StreamUrl; }
            set
            {
                if (value == _StreamUrl)
                    return;
                _StreamUrl = value;
                this.OnPropertyChanged("StreamUrl");
            }
        }
        //private byte[] _ArtImagePostData;
        //public byte[] ArtImagePostData
        //{
        //    get { return _ArtImagePostData; }
        //    set
        //    {
        //        if (value == _ArtImagePostData)
        //            return;

        //        _ArtImagePostData = value;
        //        this.OnPropertyChanged("PostData");
        //    }
        //}
        //private BitmapImage _ArtImage;
        //public BitmapImage ArtImage
        //{
        //    get { return _ArtImage; }
        //    set
        //    {
        //        if (value == _ArtImage)
        //            return;

        //        _ArtImage = value;
        //        this.OnPropertyChanged("ArtImage");
        //    }
        //}
        private MusicUploaderModel _CurrentInstance;
        public MusicUploaderModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                if (value == _CurrentInstance)
                    return;
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }
        #region Utility Method
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion Utility Method
    }
}

<<<<<<< HEAD
﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using View.Constants;
using View.Utility;
using View.ViewModel;
using System.Linq;
using View.Utility.WPFMessageBox;
using Models.Stores;
using Models.DAO;
using View.UI.PopUp;
using View.UI;
using Newtonsoft.Json.Linq;

namespace View.BindingModels
{
    public class SingleMediaModel : INotifyPropertyChanged
    {
        private readonly ILog log = LogManager.GetLogger(typeof(SingleMediaModel).Name);

        public SingleMediaModel()
        {
            CurrentInstance = this;
        }

        public void LoadData(JObject jObject, int mediaType, bool PlayedFromFeed = false)
        {
            this.PlayedFromFeed = PlayedFromFeed;
            if (jObject[JsonKeys.Artist] != null)
                this.Artist = (string)jObject[JsonKeys.Artist];

            if (jObject[JsonKeys.ContentId] != null)
                this.ContentId = (Guid)jObject[JsonKeys.ContentId];

            if (jObject[JsonKeys.AlbumId] != null)
                this.AlbumId = (Guid)jObject[JsonKeys.AlbumId];

            if (jObject[JsonKeys.AlbumName] != null)
                this.AlbumName = (string)jObject[JsonKeys.AlbumName];

            if (jObject[JsonKeys.Title] != null)
                this.Title = (string)jObject[JsonKeys.Title];

            if (jObject[JsonKeys.StreamUrl] != null)
                this.StreamUrl = (string)jObject[JsonKeys.StreamUrl];

            if (mediaType > 0 && MediaType == 0)
                this.MediaType = mediaType;
            else if (jObject[JsonKeys.MediaType] != null)
            {
                int mdaT = (int)jObject[JsonKeys.MediaType];
                if (mdaT > 0 && MediaType == 0)
                    this.MediaType = mdaT;
            }
            if (jObject[JsonKeys.ThumbUrl] != null)
            {
                this.ThumbUrl = (string)jObject[JsonKeys.ThumbUrl];
                if (string.IsNullOrWhiteSpace(this.ThumbUrl)) this.ThumbUrl = null;
            }
            if (this.MediaType == 2 && !string.IsNullOrEmpty(this.StreamUrl) && string.IsNullOrEmpty(this.ThumbUrl))
                this.ThumbUrl = this.StreamUrl.Replace(".mp4", ".jpg");

            if (jObject[JsonKeys.ThumbImageHeight] != null)
                this.ThumbImageHeight = (int)jObject[JsonKeys.ThumbImageHeight];

            if (jObject[JsonKeys.ThumbImageWidth] != null)
                this.ThumbImageWidth = (int)jObject[JsonKeys.ThumbImageWidth];

            if (jObject[JsonKeys.MediaDuration] != null)
                this.Duration = (long)jObject[JsonKeys.MediaDuration];

            //if (jObject[JsonKeys.LikeCount] != null)
            //    this.LikeCount = (long)jObject[JsonKeys.LikeCount];

            //if (jObject[JsonKeys.CommentCount] != null)
            //    this.CommentCount = (short)jObject[JsonKeys.CommentCount];

            if (jObject[JsonKeys.AccessCount] != null)
                this.AccessCount = (short)jObject[JsonKeys.AccessCount];

            //if (jObject[JsonKeys.ILike] != null)
            //    this.ILike = (short)jObject[JsonKeys.ILike];

            //if (jObject[JsonKeys.IComment] != null)
            //    this.IComment = (short)jObject[JsonKeys.IComment];

            //if (jObject[JsonKeys.IShare] != null)
            //    this.IShare = (short)jObject[JsonKeys.IShare];

            //if (jObject[JsonKeys.NumberOfShares] != null)
            //    this.ShareCount = (long)jObject[JsonKeys.NumberOfShares];

            if (jObject[JsonKeys.UserTableID] != null)
                this.UserTableID = (long)jObject[JsonKeys.UserTableID];

            //if (jObject[JsonKeys.FullName] != null)
            //    this.FullName = (string)jObject[JsonKeys.FullName];

            //if (jObject[JsonKeys.ProfileImage] != null)
            //    this.ProfileImage = (string)jObject[JsonKeys.ProfileImage];
            if (MediaOwner == null && jObject[JsonKeys.UserTableID] != null)
            {
                MediaOwner = new BaseUserProfileModel(jObject);
            }
            if (jObject[JsonKeys.MediaPrivacy] != null)
                this.Privacy = (int)jObject[JsonKeys.MediaPrivacy];
        }
        public void LoadDataFromDetails(JObject jObject, int mediaType, bool PlayedFromFeed = false)
        {
            this.PlayedFromFeed = PlayedFromFeed;
            if (jObject[JsonKeys.Artist] != null)
                this.Artist = (string)jObject[JsonKeys.Artist];

            if (jObject[JsonKeys.ContentId] != null)
                this.ContentId = (Guid)jObject[JsonKeys.ContentId];

            if (jObject[JsonKeys.AlbumId] != null)
                this.AlbumId = (Guid)jObject[JsonKeys.AlbumId];

            if (jObject[JsonKeys.AlbumName] != null)
                this.AlbumName = (string)jObject[JsonKeys.AlbumName];

            if (jObject[JsonKeys.Title] != null)
                this.Title = (string)jObject[JsonKeys.Title];

            if (jObject[JsonKeys.StreamUrl] != null)
                this.StreamUrl = (string)jObject[JsonKeys.StreamUrl];

            if (mediaType > 0 && MediaType == 0)
                this.MediaType = mediaType;
            else if (jObject[JsonKeys.MediaType] != null)
            {
                int mdaT = (int)jObject[JsonKeys.MediaType];
                if (mdaT > 0 && MediaType == 0)
                    this.MediaType = mdaT;
            }
            if (jObject[JsonKeys.ThumbUrl] != null)
            {
                this.ThumbUrl = (string)jObject[JsonKeys.ThumbUrl];
                if (string.IsNullOrWhiteSpace(this.ThumbUrl)) this.ThumbUrl = null;
            }
            if (this.MediaType == 2 && !string.IsNullOrEmpty(this.StreamUrl) && string.IsNullOrEmpty(this.ThumbUrl))
                this.ThumbUrl = this.StreamUrl.Replace(".mp4", ".jpg");

            if (jObject[JsonKeys.ThumbImageHeight] != null)
                this.ThumbImageHeight = (int)jObject[JsonKeys.ThumbImageHeight];

            if (jObject[JsonKeys.ThumbImageWidth] != null)
                this.ThumbImageWidth = (int)jObject[JsonKeys.ThumbImageWidth];

            if (jObject[JsonKeys.MediaDuration] != null)
                this.Duration = (long)jObject[JsonKeys.MediaDuration];

            //if (jObject[JsonKeys.LikeCount] != null)
            //    this.LikeCount = (long)jObject[JsonKeys.LikeCount];

            //if (jObject[JsonKeys.CommentCount] != null)
            //    this.CommentCount = (short)jObject[JsonKeys.CommentCount];

            //if (jObject[JsonKeys.AccessCount] != null)
            //    this.AccessCount = (short)jObject[JsonKeys.AccessCount];

            //if (jObject[JsonKeys.ILike] != null)
            //    this.ILike = (short)jObject[JsonKeys.ILike];

            //if (jObject[JsonKeys.IComment] != null)
            //    this.IComment = (short)jObject[JsonKeys.IComment];

            //if (jObject[JsonKeys.IShare] != null)
            //    this.IShare = (short)jObject[JsonKeys.IShare];

            //if (jObject[JsonKeys.NumberOfShares] != null)
            //    this.ShareCount = (long)jObject[JsonKeys.NumberOfShares];

            if (jObject[JsonKeys.UserTableID] != null)
                this.UserTableID = (long)jObject[JsonKeys.UserTableID];

            //if (jObject[JsonKeys.FullName] != null)
            //    this.FullName = (string)jObject[JsonKeys.FullName];

            //if (jObject[JsonKeys.ProfileImage] != null)
            //    this.ProfileImage = (string)jObject[JsonKeys.ProfileImage];
            if (MediaOwner == null && jObject[JsonKeys.UserTableID] != null)
            {
                MediaOwner = new BaseUserProfileModel(jObject);
            }
            if (jObject[JsonKeys.MediaPrivacy] != null)
                this.Privacy = (int)jObject[JsonKeys.MediaPrivacy];
        }
        private string _ThumbUrl = string.Empty;
        public string ThumbUrl
        {
            get { return _ThumbUrl; }
            set
            {
                if (value == _ThumbUrl)
                    return;
                _ThumbUrl = value;
                this.OnPropertyChanged("ThumbUrl");
            }
        }
        private string _StreamUrl;
        public string StreamUrl
        {
            get { return _StreamUrl; }
            set
            {
                if (value == _StreamUrl)
                    return;
                _StreamUrl = value;
                this.OnPropertyChanged("StreamUrl");
            }
        }
        private BaseUserProfileModel _MediaOwner;
        public BaseUserProfileModel MediaOwner
        {
            get { return _MediaOwner; }
            set
            {
                if (value == _MediaOwner)
                    return;
                _MediaOwner = value;
                this.OnPropertyChanged("MediaOwner");
            }
        }
        //private string _ProfileImage;
        //public string ProfileImage
        //{
        //    get { return _ProfileImage; }
        //    set
        //    {
        //        if (value == _ProfileImage)
        //            return;
        //        _ProfileImage = value;
        //        this.OnPropertyChanged("ProfileImage");
        //    }
        //}
        //private string _FullName;
        //public string FullName
        //{
        //    get { return _FullName; }
        //    set
        //    {
        //        if (value == _FullName)
        //            return;
        //        _FullName = value;
        //        this.OnPropertyChanged("FullName");
        //    }
        //}
        private Guid _NewsFeedId;
        public Guid NewsFeedId
        {
            get { return _NewsFeedId; }
            set
            {
                if (value == _NewsFeedId)
                    return;

                _NewsFeedId = value;
                this.OnPropertyChanged("NewsFeedId");
            }
        }
        private string _Title = string.Empty;
        public string Title
        {
            get { return _Title; }
            set
            {
                if (value == _Title)
                    return;
                _Title = value;
                this.OnPropertyChanged("Title");
            }
        }
        private string _Artist = string.Empty;
        public string Artist
        {
            get { return _Artist; }
            set
            {
                if (value == _Artist)
                    return;
                _Artist = value;
                this.OnPropertyChanged("Artist");
            }
        }
        private long _Duration;
        public long Duration
        {
            get { return _Duration; }
            set
            {
                if (value == _Duration)
                    return;

                _Duration = value;
                this.OnPropertyChanged("Duration");
            }
        }
        private int _ThumbImageWidth;
        public int ThumbImageWidth
        {
            get { return _ThumbImageWidth; }
            set
            {
                if (value == _ThumbImageWidth)
                    return;

                _ThumbImageWidth = value;
                this.OnPropertyChanged("ThumbImageWidth");
            }
        }
        private int _ThumbImageHeight;
        public int ThumbImageHeight
        {
            get { return _ThumbImageHeight; }
            set
            {
                if (value == _ThumbImageHeight)
                    return;

                _ThumbImageHeight = value;
                this.OnPropertyChanged("ThumbImageHeight");
            }
        }
        private int _Privacy;
        public int Privacy
        {
            get { return _Privacy; }
            set
            {
                if (value == _Privacy)
                    return;

                _Privacy = value;
                this.OnPropertyChanged("Privacy");
            }
        }
        private Guid _ContentId;
        public Guid ContentId
        {
            get { return _ContentId; }
            set
            {
                if (value == _ContentId)
                    return;

                _ContentId = value;
                this.OnPropertyChanged("ContentId");
            }
        }
        private long _UserTableId;
        public long UserTableID
        {
            get { return _UserTableId; }
            set
            {
                if (value == _UserTableId)
                    return;

                _UserTableId = value;
                this.OnPropertyChanged("UserTableID");
            }
        }
        private int _DownloadState;
        public int DownloadState
        {
            get { return _DownloadState; }
            set
            {
                if (value == _DownloadState)
                    return;
                _DownloadState = value;
                //HandlePopupPausedResume();
                this.OnPropertyChanged("DownloadState");
            }
        }
        private int _DownloadProgress;
        public int DownloadProgress
        {
            get { return _DownloadProgress; }
            set
            {
                if (value == _DownloadProgress)
                    return;

                _DownloadProgress = value;
                this.OnPropertyChanged("DownloadProgress");
            }
        }
        private long _DownloadTime;
        public long DownloadTime
        {
            get { return _DownloadTime; }
            set
            {
                if (value == _DownloadTime)
                    return;

                _DownloadTime = value;
                this.OnPropertyChanged("DownloadTime");
            }
        }
        private long _LastPlayedTime;
        public long LastPlayedTime
        {
            get { return _LastPlayedTime; }
            set
            {
                if (value == _LastPlayedTime)
                    return;

                _LastPlayedTime = value;
                this.OnPropertyChanged("LastPlayedTime");
            }
        }
        //private short _ILike;
        //public short ILike
        //{
        //    get { return _ILike; }
        //    set
        //    {
        //        // if (value == _ILike)
        //        //    return;
        //        _ILike = value;
        //        this.OnPropertyChanged("ILike");
        //    }
        //}
        //private short _IComment;
        //public short IComment
        //{
        //    get { return _IComment; }
        //    set
        //    {
        //        if (value == _IComment)
        //            return;
        //        _IComment = value;
        //        this.OnPropertyChanged("IComment");
        //    }
        //}
        //private short _IShare;
        //public short IShare
        //{
        //    get { return _IShare; }
        //    set
        //    {
        //        //if (value == _IShare)
        //        //    return;
        //        _IShare = value;
        //        this.OnPropertyChanged("IShare");
        //    }
        //}
        private short _AccessCount;
        public short AccessCount
        {
            get { return _AccessCount; }
            set
            {
                if (value == _AccessCount)
                    return;
                _AccessCount = value;
                this.OnPropertyChanged("AccessCount");
            }
        }
        //private long _LikeCount;
        //public long LikeCount
        //{
        //    get { return _LikeCount; }
        //    set
        //    {
        //        if (value == _LikeCount)
        //            return;
        //        _LikeCount = value;
        //        this.OnPropertyChanged("LikeCount");
        //    }
        //}
        //private long _ShareCount;
        //public long ShareCount
        //{
        //    get { return _ShareCount; }
        //    set
        //    {
        //        if (value == _ShareCount)
        //            return;
        //        _ShareCount = value;
        //        this.OnPropertyChanged("ShareCount");
        //    }
        //}
        //private long _CommentCount;
        //public long CommentCount
        //{
        //    get { return _CommentCount; }
        //    set
        //    {
        //        if (value == _CommentCount)
        //            return;
        //        _CommentCount = value;
        //        this.OnPropertyChanged("CommentCount");
        //    }
        //}
        private LikeCommentShareModel _LikeCommentShare;
        public LikeCommentShareModel LikeCommentShare
        {
            get { return _LikeCommentShare; }
            set
            {
                if (value == _LikeCommentShare)
                    return;
                _LikeCommentShare = value;
                this.OnPropertyChanged("LikeCommentShare");
            }
        }
        public string _AlbumName = string.Empty;
        public string AlbumName
        {
            get { return _AlbumName; }
            set
            {
                if (_AlbumName == value) return;
                _AlbumName = value;
                OnPropertyChanged("AlbumName");
            }
        }

        public string hdURL;
        public string HDURL
        {
            get { return hdURL; }
            set
            {
                if (hdURL == value) return;
                hdURL = value;
                OnPropertyChanged("HDURL");
            }
        }

        private int _MediaType;
        public int MediaType
        {
            get { return _MediaType; }
            set
            {
                if (_MediaType == value) return;
                _MediaType = value;
                OnPropertyChanged("MediaType");
            }
        }
        private Guid _AlbumId;
        public Guid AlbumId
        {
            get { return _AlbumId; }
            set
            {
                if (_AlbumId == value) return;
                _AlbumId = value;
                OnPropertyChanged("AlbumId");
            }
        }
        private long _Time;
        public long Time
        {
            get { return _Time; }
            set
            {
                if (_Time == value) return;
                _Time = value;
                OnPropertyChanged("Time");
            }
        }
        private bool _playVisible;
        public bool PlayVisible
        {
            get
            {
                return
                    _playVisible;
            }
            set
            {
                if (value == _playVisible)
                    return;
                _playVisible = value;
                this.OnPropertyChanged("PlayVisible");
            }
        }
        private bool _NoImageFound = false;
        public bool NoImageFound
        {
            get
            {
                return
                    _NoImageFound;
            }
            set
            {
                if (value == _NoImageFound)
                    return;
                _NoImageFound = value;
                this.OnPropertyChanged("NoImageFound");
            }
        }
        private bool _PlayedFromFeed = false;
        public bool PlayedFromFeed
        {
            get
            {
                return
                    _PlayedFromFeed;
            }
            set
            {
                if (value == _PlayedFromFeed)
                    return;
                _PlayedFromFeed = value;
                this.OnPropertyChanged("PlayedFromFeed");
            }
        }
        private bool _IsFromLocalDirectory = false;
        public bool IsFromLocalDirectory
        {
            get
            {
                return
                    _IsFromLocalDirectory;
            }
            set
            {
                if (value == _IsFromLocalDirectory)
                    return;
                _IsFromLocalDirectory = value;
                this.OnPropertyChanged("IsFromLocalDirectory");
            }
        }
        private bool _shareOptionsPopup;
        public bool ShareOptionsPopup
        {
            get { return _shareOptionsPopup; }
            set
            {
                if (value == _shareOptionsPopup)
                    return;
                _shareOptionsPopup = value;
                this.OnPropertyChanged("ShareOptionsPopup");
            }
        }

        private Guid _ChannelId = Guid.Empty;
        public Guid ChannelId
        {
            get { return _ChannelId; }
            set
            {
                if (value == _ChannelId)
                    return;
                _ChannelId = value;
                this.OnPropertyChanged("ChannelId");
            }
        }

        private SingleMediaModel _CurrentInstance;
        public SingleMediaModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                if (value == _CurrentInstance)
                    return;
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        //#region Downloader Event
        ////private void downloader_StateChanged(object sender, EventArgs e)
        ////{
        ////    Downloader.CanStart;
        ////    Downloader.CanStop;
        ////    Downloader.CanPause;
        ////    Downloader.CanResume;
        ////}

        //private void downloader_CalculationFileSize(object sender, Int32 fileNr)
        //{
        //    // if (DefaultSettings.DEBUG)
        //    //     log.Info(String.Format("Calculating file sizes - file {0} of {1}", fileNr, Downloader.Files.Count));
        //}

        //private void downloader_ProgressChanged(object sender, EventArgs e)
        //{
        //    // if (Downloader.SupportsProgress)
        //    //  {
        //    DownloadProgress = (int)Downloader.CurrentFilePercentage();
        //    var model = RingIDViewModel.Instance.UploadingModelList.FirstOrDefault(p => p.ContentId == ContentId);
        //    if (model != null)
        //    {
        //        model.TotalUploadedInPercentage = DownloadProgress;
        //    }
        //    //  if (DefaultSettings.DEBUG)                  
        //    //      log.Info(String.Format("Downloaded {0} of {1} ({2}%)", Downloader.CurrentFileProgress, Downloader.CurrentFileSize, Downloader.CurrentFilePercentage()));
        //    //  }
        //}

        //#region "Properties"

        //public UCDownloadOrAddToAlbumPopUpWrapper DownloadOrAddToAlbumPopUpWrapper
        //{
        //    get
        //    {
        //        return MainSwitcher.PopupController.DownloadOrAddToAlbumPopUpWrapper;
        //    }
        //}

        //public UCFeedDownloadPauseCancelPopup FeedDownloadPauseCancelPopup
        //{
        //    get
        //    {
        //        return MainSwitcher.PopupController.FeedDownloadPauseCancelPopup;
        //    }
        //}

        //#endregion"Properties"

        //private void downloader_FileDownloadStarted(object sender, EventArgs e)
        //{
        //    if (Downloader.CurrentFileSize > 0)
        //    {
        //        //AddMediaToDownloadsTab();
        //        InsertOrUpdateDownloadMedia(StatusConstants.MEDIA_DOWNLOADING_STATE);
        //    }
        //    else
        //    {
        //        Downloader.Stop();
        //    }
        //    //if (DefaultSettings.DEBUG)
        //    //{
        //    //    log.Info(String.Format("Downloading {0}", Downloader.CurrentFile.Path));
        //    //    log.Info(String.Format("File size: {0}", FileDownloader.FormatSizeBinary(Downloader.CurrentFileSize)));
        //    //    log.Info(String.Format("Saving to {0}\\{1}", Downloader.LocalDirectory, Downloader.CurrentFile.Name));
        //    //}
        //}

        //private void downloader_FileDownloadPaused(object sender, EventArgs e)
        //{
        //    InsertOrUpdateDownloadMedia(StatusConstants.MEDIA_DOWNLOAD_PAUSE_STATE, DownloadProgress);
        //}

        //private void downloader_FileDownloadResumed(object sender, EventArgs e)
        //{
        //    InsertOrUpdateDownloadMedia(StatusConstants.MEDIA_DOWNLOADING_STATE, DownloadProgress);
        //}

        //private void downloader_FileDownloadCanceled(object sender, EventArgs e)
        //{
        //    //if (DefaultSettings.DEBUG)
        //    //    log.Info("Download(s) canceled");
        //    DeleteDownloadMedia(ContentId);
        //}


        //private void downloader_FileDownloadFailed(object sender, Exception ex)
        //{
        //    //Downloader.Stop();
        //    DeleteDownloadMedia(ContentId);
        //    CustomMessageBox.ShowError(ex.Message);
        //}

        //private void downloader_FileDownloadCompleted(object sender, EventArgs e)
        //{
        //    //if (DefaultSettings.DEBUG)
        //    //    log.Info(String.Format("Download complete, downloaded {0} files.", Downloader.Files.Count));

        //    InsertOrUpdateDownloadMedia(StatusConstants.MEDIA_DOWNLOAD_COMPLETED_STATE);
        //    var model = RingIDViewModel.Instance.UploadingModelList.FirstOrDefault(p => p.ContentId == ContentId);
        //    RingIDViewModel.Instance.UploadingModelList.InvokeRemove(model);
        //    if (DownloadOrAddToAlbumPopUpWrapper.ucUploadingPopup != null)
        //    {
        //        DownloadOrAddToAlbumPopUpWrapper.ucUploadingPopup.ClosePopUp();
        //    }
        //}

        ////private void downloader_CancelRequested(object sender, EventArgs e)
        ////{
        ////    if (DefaultSettings.DEBUG)
        ////        log.Info("Canceling downloads...");
        ////}

        ////private void downloader_DeletingFilesAfterCancel(object sender, EventArgs e)
        ////{
        ////    //if (DefaultSettings.DEBUG)
        ////    //    log.Info("Canceling downloads - deleting files...");
        ////}



        //public void InsertOrUpdateDownloadMedia(int state, int downloadProgress = 0)
        //{
        //    DownloadState = state;
        //    DownloadProgress = downloadProgress;
        //    DownloadTime = Models.Utility.ModelUtility.CurrentTimeMillisLocal();

        //    //GetSingleMediaDto.DownloadState = DownloadState;
        //    //GetSingleMediaDto.DownloadProgress = DownloadProgress;
        //    //GetSingleMediaDto.DownloadTime = Models.Utility.ModelUtility.CurrentTimeMillisLocal();

        //    //lock (MediaDictionaries.Instance.DOWNLOAD_MEDIAS)
        //    //{
        //    //    MediaDictionaries.Instance.DOWNLOAD_MEDIAS[GetSingleMediaDto.ContentId] = GetSingleMediaDto;
        //    //}
        //    if (!RingIDViewModel.Instance.MyDownloads.Any(P => P.ContentId == ContentId))
        //        RingIDViewModel.Instance.MyDownloads.InvokeAdd(this);

        //    MediaDAO.Instance.InsertIntoDownloadedMediasTable(this.ContentId, this.UserTableID, this.Title, this.Artist, this.StreamUrl, this.ThumbUrl, this.Duration, this.ThumbImageWidth, this.ThumbImageHeight, this.MediaType, this.AlbumId, this.AlbumName, this.AccessCount, this.DownloadState, this.DownloadTime, this.DownloadProgress);
        //    //InsertIntoDownloadMedia(GetSingleMediaDto);
        //}

        //public void DeleteDownloadMedia(long mediaContentId)
        //{
        //    DownloadState = StatusConstants.MEDIA_DOWNLOAD_NOT_START_STATE;
        //    DownloadProgress = 0;

        //    //lock (MediaDictionaries.Instance.DOWNLOAD_MEDIAS)
        //    //{
        //    //    MediaDictionaries.Instance.DOWNLOAD_MEDIAS.Remove(mediaContentId);
        //    //}
        //    MediaDAO.Instance.DeleteFromDownloadedMediasTable(mediaContentId);

        //    if (Downloader.Files != null && Downloader.Files.Count() > 1)
        //    {
        //        System.IO.File.Delete(Downloader.LocalDirectory + "\\" + Downloader.Files[0].Name);
        //    }

        //    var model = RingIDViewModel.Instance.UploadingModelList.FirstOrDefault(p => p.ContentId == ContentId);
        //    if (model != null)
        //    {
        //        model.IsUploadFailed = true;
        //    }
        //}

        //#endregion
        //#region "Media Command"
        //private ICommand _FeedMediaLeftButtonCommand;
        //public ICommand FeedMediaLeftButtonCommand
        //{
        //    get
        //    {
        //        if (_FeedMediaLeftButtonCommand == null)
        //        {
        //            _FeedMediaLeftButtonCommand = new RelayCommand(param => MediaUtility.OnFeedMediaLeftClick(param, this.ContentId));
        //        }
        //        return _FeedMediaLeftButtonCommand;
        //    }
        //}
        //#endregion
        //private void HandlePopupPausedResume()
        //{
        //    Application.Current.Dispatcher.BeginInvoke(() =>
        //    {
        //        if (_DownloadState != StatusConstants.MEDIA_DOWNLOADING_STATE && FeedDownloadPauseCancelPopup.popupPausedResume.IsOpen)
        //        {
        //            FeedDownloadPauseCancelPopup.popupPausedResume.IsOpen = false;
        //        }
        //    });
        //}

        //private int _DownloadingProgress;
        //public int DownloadingProgress
        //{
        //    get { return _DownloadingProgress; }
        //    set
        //    {
        //        if (value == _DownloadingProgress)
        //            return;

        //        _DownloadingProgress = value;
        //        this.OnPropertyChanged("DownloadingProgress");
        //    }
        //}
        //public FileDownloader Downloader;
        //public FileDownloader Downloader
        //{
        //    get { return _Downloader; }
        //    set
        //    {
        //        if (value == _Downloader)
        //            return;
        //        _Downloader = value;
        //        this.OnPropertyChanged("Downloader");
        //    }
        //}

        //private SingleMediaDTO singleMediaDTO;
        //public SingleMediaDTO GetSingleMediaDto
        //{
        //    get
        //    {
        //        return singleMediaDTO;
        //    }
        //}
        //public SingleMediaDTO GetDTOFromModel()
        //{
        //    SingleMediaDTO dto = new SingleMediaDTO();
        //    dto.ContentId = this.ContentId;
        //    dto.UserTableID = this.UserTableID;
        //    dto.AlbumId = this.AlbumId;
        //    dto.AlbumName = this.AlbumName;
        //    dto.Title = this.Title;
        //    dto.StreamUrl = this.StreamUrl;
        //    dto.ThumbImageHeight = this.ThumbImageHeight;
        //    dto.ThumbImageWidth = this.ThumbImageWidth;
        //    dto.Duration = this.Duration;
        //    dto.MediaType = this.MediaType;
        //    dto.LikeCount = (short)this.LikeCount;
        //    dto.CommentCount = this.CommentCount;
        //    dto.AccessCount = this.AccessCount;
        //    dto.ILike = this.ILike;
        //    dto.IComment = this.IComment;
        //    dto.IShare = this.IShare;
        //    dto.ShareCount = (short)this.ShareCount;
        //    dto.FullName = this.FullName;
        //    dto.ProfileImage = this.ProfileImage;
        //    dto.Privacy = this.Privacy;
        //    dto.ContentId = this.ContentId;
        //    return dto;
        //}
        //public void LoadData(SingleMediaDTO singleMediaDTO)
        //{
        //    try
        //    {
        //        //SingleMediaDTO dto = null;
        //        //MediaDictionaries.Instance.DOWNLOAD_MEDIAS.TryGetValue(singleMediaDTO.ContentId, out dto);
        //        //if (dto != null)
        //        //{
        //        //    singleMediaDTO.DownloadState = dto.DownloadState;
        //        //    singleMediaDTO.DownloadProgress = dto.DownloadProgress;
        //        //    singleMediaDTO.DownloadTime = dto.DownloadTime;
        //        //    lock (MediaDictionaries.Instance.DOWNLOAD_MEDIAS)
        //        //        MediaDictionaries.Instance.DOWNLOAD_MEDIAS[singleMediaDTO.ContentId] = singleMediaDTO;
        //        //}
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }

        //    ThumbUrl = singleMediaDTO.ThumbUrl;
        //    StreamUrl = singleMediaDTO.StreamUrl;
        //    Title = singleMediaDTO.Title;
        //    Artist = singleMediaDTO.Artist;
        //    Duration = singleMediaDTO.Duration;
        //    ThumbImageWidth = singleMediaDTO.ThumbImageWidth;
        //    ThumbImageHeight = singleMediaDTO.ThumbImageHeight;
        //    ContentId = singleMediaDTO.ContentId;
        //    ILike = singleMediaDTO.ILike;
        //    IComment = singleMediaDTO.IComment;
        //    Privacy = singleMediaDTO.Privacy;
        //    AccessCount = singleMediaDTO.AccessCount;
        //    LikeCount = singleMediaDTO.LikeCount;
        //    CommentCount = singleMediaDTO.CommentCount;
        //    MediaType = singleMediaDTO.MediaType;
        //    AlbumId = singleMediaDTO.AlbumId;
        //    AlbumName = singleMediaDTO.AlbumName;
        //    UserTableID = singleMediaDTO.UserTableID;
        //    LastPlayedTime = singleMediaDTO.LastPlayedTime;
        //    DownloadState = singleMediaDTO.DownloadState;
        //    DownloadTime = singleMediaDTO.DownloadTime;
        //    ProfileImage = singleMediaDTO.ProfileImage;
        //    FullName = singleMediaDTO.FullName;
        //    IShare = singleMediaDTO.IShare;
        //    ShareCount = singleMediaDTO.ShareCount;
        //    if (singleMediaDTO.NewsFeedId > 0) NewsFeedId = singleMediaDTO.NewsFeedId;
        //    CurrentInstance = this;
        //}
    }
}
=======
﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using View.Constants;
using View.Utility;
using View.ViewModel;
using System.Linq;
using View.Utility.WPFMessageBox;
using Models.Stores;
using Models.DAO;
using View.UI.PopUp;
using View.UI;
using Newtonsoft.Json.Linq;

namespace View.BindingModels
{
    public class SingleMediaModel : INotifyPropertyChanged
    {
        private readonly ILog log = LogManager.GetLogger(typeof(SingleMediaModel).Name);

        public SingleMediaModel()
        {
            CurrentInstance = this;
        }

        public void LoadData(JObject jObject, int mediaType, bool PlayedFromFeed = false)
        {
            this.PlayedFromFeed = PlayedFromFeed;
            if (jObject[JsonKeys.Artist] != null)
                this.Artist = (string)jObject[JsonKeys.Artist];

            if (jObject[JsonKeys.ContentId] != null)
                this.ContentId = (Guid)jObject[JsonKeys.ContentId];

            if (jObject[JsonKeys.AlbumId] != null)
                this.AlbumId = (Guid)jObject[JsonKeys.AlbumId];

            if (jObject[JsonKeys.AlbumName] != null)
                this.AlbumName = (string)jObject[JsonKeys.AlbumName];

            if (jObject[JsonKeys.Title] != null)
                this.Title = (string)jObject[JsonKeys.Title];

            if (jObject[JsonKeys.StreamUrl] != null)
                this.StreamUrl = (string)jObject[JsonKeys.StreamUrl];

            if (mediaType > 0 && MediaType == 0)
                this.MediaType = mediaType;
            else if (jObject[JsonKeys.MediaType] != null)
            {
                int mdaT = (int)jObject[JsonKeys.MediaType];
                if (mdaT > 0 && MediaType == 0)
                    this.MediaType = mdaT;
            }
            if (jObject[JsonKeys.ThumbUrl] != null)
            {
                this.ThumbUrl = (string)jObject[JsonKeys.ThumbUrl];
                if (string.IsNullOrWhiteSpace(this.ThumbUrl)) this.ThumbUrl = null;
            }
            if (this.MediaType == 2 && !string.IsNullOrEmpty(this.StreamUrl) && string.IsNullOrEmpty(this.ThumbUrl))
                this.ThumbUrl = this.StreamUrl.Replace(".mp4", ".jpg");

            if (jObject[JsonKeys.ThumbImageHeight] != null)
                this.ThumbImageHeight = (int)jObject[JsonKeys.ThumbImageHeight];

            if (jObject[JsonKeys.ThumbImageWidth] != null)
                this.ThumbImageWidth = (int)jObject[JsonKeys.ThumbImageWidth];

            if (jObject[JsonKeys.MediaDuration] != null)
                this.Duration = (long)jObject[JsonKeys.MediaDuration];

            //if (jObject[JsonKeys.LikeCount] != null)
            //    this.LikeCount = (long)jObject[JsonKeys.LikeCount];

            //if (jObject[JsonKeys.CommentCount] != null)
            //    this.CommentCount = (short)jObject[JsonKeys.CommentCount];

            if (jObject[JsonKeys.AccessCount] != null)
                this.AccessCount = (short)jObject[JsonKeys.AccessCount];

            //if (jObject[JsonKeys.ILike] != null)
            //    this.ILike = (short)jObject[JsonKeys.ILike];

            //if (jObject[JsonKeys.IComment] != null)
            //    this.IComment = (short)jObject[JsonKeys.IComment];

            //if (jObject[JsonKeys.IShare] != null)
            //    this.IShare = (short)jObject[JsonKeys.IShare];

            //if (jObject[JsonKeys.NumberOfShares] != null)
            //    this.ShareCount = (long)jObject[JsonKeys.NumberOfShares];

            if (jObject[JsonKeys.UserTableID] != null)
                this.UserTableID = (long)jObject[JsonKeys.UserTableID];

            //if (jObject[JsonKeys.FullName] != null)
            //    this.FullName = (string)jObject[JsonKeys.FullName];

            //if (jObject[JsonKeys.ProfileImage] != null)
            //    this.ProfileImage = (string)jObject[JsonKeys.ProfileImage];
            if ((MediaOwner == null || (string.IsNullOrEmpty(MediaOwner.FullName))) && jObject[JsonKeys.UserTableID] != null)
            {
                MediaOwner = new BaseUserProfileModel(jObject);
            }
            if (jObject[JsonKeys.MediaPrivacy] != null)
                this.Privacy = (int)jObject[JsonKeys.MediaPrivacy];
        }
        public void LoadDataFromDetails(JObject jObject, int mediaType, bool PlayedFromFeed = false)
        {
            this.PlayedFromFeed = PlayedFromFeed;
            if (jObject[JsonKeys.Artist] != null)
                this.Artist = (string)jObject[JsonKeys.Artist];

            if (jObject[JsonKeys.ContentId] != null)
                this.ContentId = (Guid)jObject[JsonKeys.ContentId];

            if (jObject[JsonKeys.AlbumId] != null)
                this.AlbumId = (Guid)jObject[JsonKeys.AlbumId];

            if (jObject[JsonKeys.AlbumName] != null)
                this.AlbumName = (string)jObject[JsonKeys.AlbumName];

            if (jObject[JsonKeys.Title] != null)
                this.Title = (string)jObject[JsonKeys.Title];

            if (jObject[JsonKeys.StreamUrl] != null)
                this.StreamUrl = (string)jObject[JsonKeys.StreamUrl];

            if (mediaType > 0 && MediaType == 0)
                this.MediaType = mediaType;
            else if (jObject[JsonKeys.MediaType] != null)
            {
                int mdaT = (int)jObject[JsonKeys.MediaType];
                if (mdaT > 0 && MediaType == 0)
                    this.MediaType = mdaT;
            }
            if (jObject[JsonKeys.ThumbUrl] != null)
            {
                this.ThumbUrl = (string)jObject[JsonKeys.ThumbUrl];
                if (string.IsNullOrWhiteSpace(this.ThumbUrl)) this.ThumbUrl = null;
            }
            if (this.MediaType == 2 && !string.IsNullOrEmpty(this.StreamUrl) && string.IsNullOrEmpty(this.ThumbUrl))
                this.ThumbUrl = this.StreamUrl.Replace(".mp4", ".jpg");

            if (jObject[JsonKeys.ThumbImageHeight] != null)
                this.ThumbImageHeight = (int)jObject[JsonKeys.ThumbImageHeight];

            if (jObject[JsonKeys.ThumbImageWidth] != null)
                this.ThumbImageWidth = (int)jObject[JsonKeys.ThumbImageWidth];

            if (jObject[JsonKeys.MediaDuration] != null)
                this.Duration = (long)jObject[JsonKeys.MediaDuration];

            //if (jObject[JsonKeys.LikeCount] != null)
            //    this.LikeCount = (long)jObject[JsonKeys.LikeCount];

            //if (jObject[JsonKeys.CommentCount] != null)
            //    this.CommentCount = (short)jObject[JsonKeys.CommentCount];

            //if (jObject[JsonKeys.AccessCount] != null)
            //    this.AccessCount = (short)jObject[JsonKeys.AccessCount];

            //if (jObject[JsonKeys.ILike] != null)
            //    this.ILike = (short)jObject[JsonKeys.ILike];

            //if (jObject[JsonKeys.IComment] != null)
            //    this.IComment = (short)jObject[JsonKeys.IComment];

            //if (jObject[JsonKeys.IShare] != null)
            //    this.IShare = (short)jObject[JsonKeys.IShare];

            //if (jObject[JsonKeys.NumberOfShares] != null)
            //    this.ShareCount = (long)jObject[JsonKeys.NumberOfShares];

            if (jObject[JsonKeys.UserTableID] != null)
                this.UserTableID = (long)jObject[JsonKeys.UserTableID];

            //if (jObject[JsonKeys.FullName] != null)
            //    this.FullName = (string)jObject[JsonKeys.FullName];

            //if (jObject[JsonKeys.ProfileImage] != null)
            //    this.ProfileImage = (string)jObject[JsonKeys.ProfileImage];
            if (MediaOwner == null && jObject[JsonKeys.UserTableID] != null)
            {
                MediaOwner = new BaseUserProfileModel(jObject);
            }
            if (jObject[JsonKeys.MediaPrivacy] != null)
                this.Privacy = (int)jObject[JsonKeys.MediaPrivacy];
        }
        private string _ThumbUrl = string.Empty;
        public string ThumbUrl
        {
            get { return _ThumbUrl; }
            set
            {
                if (value == _ThumbUrl)
                    return;
                _ThumbUrl = value;
                this.OnPropertyChanged("ThumbUrl");
            }
        }
        private string _StreamUrl;
        public string StreamUrl
        {
            get { return _StreamUrl; }
            set
            {
                if (value == _StreamUrl)
                    return;
                _StreamUrl = value;
                this.OnPropertyChanged("StreamUrl");
            }
        }
        private BaseUserProfileModel _MediaOwner;
        public BaseUserProfileModel MediaOwner
        {
            get { return _MediaOwner; }
            set
            {
                if (value == _MediaOwner)
                    return;
                _MediaOwner = value;
                this.OnPropertyChanged("MediaOwner");
            }
        }
        //private string _ProfileImage;
        //public string ProfileImage
        //{
        //    get { return _ProfileImage; }
        //    set
        //    {
        //        if (value == _ProfileImage)
        //            return;
        //        _ProfileImage = value;
        //        this.OnPropertyChanged("ProfileImage");
        //    }
        //}
        //private string _FullName;
        //public string FullName
        //{
        //    get { return _FullName; }
        //    set
        //    {
        //        if (value == _FullName)
        //            return;
        //        _FullName = value;
        //        this.OnPropertyChanged("FullName");
        //    }
        //}
        private Guid _NewsFeedId;
        public Guid NewsFeedId
        {
            get { return _NewsFeedId; }
            set
            {
                if (value == _NewsFeedId)
                    return;

                _NewsFeedId = value;
                this.OnPropertyChanged("NewsFeedId");
            }
        }
        private string _Title = string.Empty;
        public string Title
        {
            get { return _Title; }
            set
            {
                if (value == _Title)
                    return;
                _Title = value;
                this.OnPropertyChanged("Title");
            }
        }
        private string _Artist = string.Empty;
        public string Artist
        {
            get { return _Artist; }
            set
            {
                if (value == _Artist)
                    return;
                _Artist = value;
                this.OnPropertyChanged("Artist");
            }
        }
        private long _Duration;
        public long Duration
        {
            get { return _Duration; }
            set
            {
                if (value == _Duration)
                    return;

                _Duration = value;
                this.OnPropertyChanged("Duration");
            }
        }
        private int _ThumbImageWidth;
        public int ThumbImageWidth
        {
            get { return _ThumbImageWidth; }
            set
            {
                if (value == _ThumbImageWidth)
                    return;

                _ThumbImageWidth = value;
                this.OnPropertyChanged("ThumbImageWidth");
            }
        }
        private int _ThumbImageHeight;
        public int ThumbImageHeight
        {
            get { return _ThumbImageHeight; }
            set
            {
                if (value == _ThumbImageHeight)
                    return;

                _ThumbImageHeight = value;
                this.OnPropertyChanged("ThumbImageHeight");
            }
        }
        private int _Privacy;
        public int Privacy
        {
            get { return _Privacy; }
            set
            {
                if (value == _Privacy)
                    return;

                _Privacy = value;
                this.OnPropertyChanged("Privacy");
            }
        }
        private Guid _ContentId;
        public Guid ContentId
        {
            get { return _ContentId; }
            set
            {
                if (value == _ContentId)
                    return;

                _ContentId = value;
                this.OnPropertyChanged("ContentId");
            }
        }
        private long _UserTableId;
        public long UserTableID
        {
            get { return _UserTableId; }
            set
            {
                if (value == _UserTableId)
                    return;

                _UserTableId = value;
                this.OnPropertyChanged("UserTableID");
            }
        }
        private int _DownloadState;
        public int DownloadState
        {
            get { return _DownloadState; }
            set
            {
                if (value == _DownloadState)
                    return;
                _DownloadState = value;
                //HandlePopupPausedResume();
                this.OnPropertyChanged("DownloadState");
            }
        }
        private int _DownloadProgress;
        public int DownloadProgress
        {
            get { return _DownloadProgress; }
            set
            {
                if (value == _DownloadProgress)
                    return;

                _DownloadProgress = value;
                this.OnPropertyChanged("DownloadProgress");
            }
        }
        private long _DownloadTime;
        public long DownloadTime
        {
            get { return _DownloadTime; }
            set
            {
                if (value == _DownloadTime)
                    return;

                _DownloadTime = value;
                this.OnPropertyChanged("DownloadTime");
            }
        }
        private long _LastPlayedTime;
        public long LastPlayedTime
        {
            get { return _LastPlayedTime; }
            set
            {
                if (value == _LastPlayedTime)
                    return;

                _LastPlayedTime = value;
                this.OnPropertyChanged("LastPlayedTime");
            }
        }
        //private short _ILike;
        //public short ILike
        //{
        //    get { return _ILike; }
        //    set
        //    {
        //        // if (value == _ILike)
        //        //    return;
        //        _ILike = value;
        //        this.OnPropertyChanged("ILike");
        //    }
        //}
        //private short _IComment;
        //public short IComment
        //{
        //    get { return _IComment; }
        //    set
        //    {
        //        if (value == _IComment)
        //            return;
        //        _IComment = value;
        //        this.OnPropertyChanged("IComment");
        //    }
        //}
        //private short _IShare;
        //public short IShare
        //{
        //    get { return _IShare; }
        //    set
        //    {
        //        //if (value == _IShare)
        //        //    return;
        //        _IShare = value;
        //        this.OnPropertyChanged("IShare");
        //    }
        //}
        private short _AccessCount;
        public short AccessCount
        {
            get { return _AccessCount; }
            set
            {
                if (value == _AccessCount)
                    return;
                _AccessCount = value;
                this.OnPropertyChanged("AccessCount");
            }
        }
        //private long _LikeCount;
        //public long LikeCount
        //{
        //    get { return _LikeCount; }
        //    set
        //    {
        //        if (value == _LikeCount)
        //            return;
        //        _LikeCount = value;
        //        this.OnPropertyChanged("LikeCount");
        //    }
        //}
        //private long _ShareCount;
        //public long ShareCount
        //{
        //    get { return _ShareCount; }
        //    set
        //    {
        //        if (value == _ShareCount)
        //            return;
        //        _ShareCount = value;
        //        this.OnPropertyChanged("ShareCount");
        //    }
        //}
        //private long _CommentCount;
        //public long CommentCount
        //{
        //    get { return _CommentCount; }
        //    set
        //    {
        //        if (value == _CommentCount)
        //            return;
        //        _CommentCount = value;
        //        this.OnPropertyChanged("CommentCount");
        //    }
        //}
        private LikeCommentShareModel _LikeCommentShare;
        public LikeCommentShareModel LikeCommentShare
        {
            get { return _LikeCommentShare; }
            set
            {
                if (value == _LikeCommentShare)
                    return;
                _LikeCommentShare = value;
                this.OnPropertyChanged("LikeCommentShare");
            }
        }
        public string _AlbumName = string.Empty;
        public string AlbumName
        {
            get { return _AlbumName; }
            set
            {
                if (_AlbumName == value) return;
                _AlbumName = value;
                OnPropertyChanged("AlbumName");
            }
        }

        public string hdURL;
        public string HDURL
        {
            get { return hdURL; }
            set
            {
                if (hdURL == value) return;
                hdURL = value;
                OnPropertyChanged("HDURL");
            }
        }

        private int _MediaType;
        public int MediaType
        {
            get { return _MediaType; }
            set
            {
                if (_MediaType == value) return;
                _MediaType = value;
                OnPropertyChanged("MediaType");
            }
        }
        private Guid _AlbumId;
        public Guid AlbumId
        {
            get { return _AlbumId; }
            set
            {
                if (_AlbumId == value) return;
                _AlbumId = value;
                OnPropertyChanged("AlbumId");
            }
        }
        private long _Time;
        public long Time
        {
            get { return _Time; }
            set
            {
                if (_Time == value) return;
                _Time = value;
                OnPropertyChanged("Time");
            }
        }
        private bool _playVisible;
        public bool PlayVisible
        {
            get
            {
                return
                    _playVisible;
            }
            set
            {
                if (value == _playVisible)
                    return;
                _playVisible = value;
                this.OnPropertyChanged("PlayVisible");
            }
        }
        private bool _NoImageFound = false;
        public bool NoImageFound
        {
            get
            {
                return
                    _NoImageFound;
            }
            set
            {
                if (value == _NoImageFound)
                    return;
                _NoImageFound = value;
                this.OnPropertyChanged("NoImageFound");
            }
        }
        private bool _PlayedFromFeed = false;
        public bool PlayedFromFeed
        {
            get
            {
                return
                    _PlayedFromFeed;
            }
            set
            {
                if (value == _PlayedFromFeed)
                    return;
                _PlayedFromFeed = value;
                this.OnPropertyChanged("PlayedFromFeed");
            }
        }
        private bool _IsFromLocalDirectory = false;
        public bool IsFromLocalDirectory
        {
            get
            {
                return
                    _IsFromLocalDirectory;
            }
            set
            {
                if (value == _IsFromLocalDirectory)
                    return;
                _IsFromLocalDirectory = value;
                this.OnPropertyChanged("IsFromLocalDirectory");
            }
        }
        private bool _shareOptionsPopup;
        public bool ShareOptionsPopup
        {
            get { return _shareOptionsPopup; }
            set
            {
                if (value == _shareOptionsPopup)
                    return;
                _shareOptionsPopup = value;
                this.OnPropertyChanged("ShareOptionsPopup");
            }
        }

        private Guid _ChannelId = Guid.Empty;
        public Guid ChannelId
        {
            get { return _ChannelId; }
            set
            {
                if (value == _ChannelId)
                    return;
                _ChannelId = value;
                this.OnPropertyChanged("ChannelId");
            }
        }

        private SingleMediaModel _CurrentInstance;
        public SingleMediaModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                if (value == _CurrentInstance)
                    return;
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        //#region Downloader Event
        ////private void downloader_StateChanged(object sender, EventArgs e)
        ////{
        ////    Downloader.CanStart;
        ////    Downloader.CanStop;
        ////    Downloader.CanPause;
        ////    Downloader.CanResume;
        ////}

        //private void downloader_CalculationFileSize(object sender, Int32 fileNr)
        //{
        //    // if (DefaultSettings.DEBUG)
        //    //     log.Info(String.Format("Calculating file sizes - file {0} of {1}", fileNr, Downloader.Files.Count));
        //}

        //private void downloader_ProgressChanged(object sender, EventArgs e)
        //{
        //    // if (Downloader.SupportsProgress)
        //    //  {
        //    DownloadProgress = (int)Downloader.CurrentFilePercentage();
        //    var model = RingIDViewModel.Instance.UploadingModelList.FirstOrDefault(p => p.ContentId == ContentId);
        //    if (model != null)
        //    {
        //        model.TotalUploadedInPercentage = DownloadProgress;
        //    }
        //    //  if (DefaultSettings.DEBUG)                  
        //    //      log.Info(String.Format("Downloaded {0} of {1} ({2}%)", Downloader.CurrentFileProgress, Downloader.CurrentFileSize, Downloader.CurrentFilePercentage()));
        //    //  }
        //}

        //#region "Properties"

        //public UCDownloadOrAddToAlbumPopUpWrapper DownloadOrAddToAlbumPopUpWrapper
        //{
        //    get
        //    {
        //        return MainSwitcher.PopupController.DownloadOrAddToAlbumPopUpWrapper;
        //    }
        //}

        //public UCFeedDownloadPauseCancelPopup FeedDownloadPauseCancelPopup
        //{
        //    get
        //    {
        //        return MainSwitcher.PopupController.FeedDownloadPauseCancelPopup;
        //    }
        //}

        //#endregion"Properties"

        //private void downloader_FileDownloadStarted(object sender, EventArgs e)
        //{
        //    if (Downloader.CurrentFileSize > 0)
        //    {
        //        //AddMediaToDownloadsTab();
        //        InsertOrUpdateDownloadMedia(StatusConstants.MEDIA_DOWNLOADING_STATE);
        //    }
        //    else
        //    {
        //        Downloader.Stop();
        //    }
        //    //if (DefaultSettings.DEBUG)
        //    //{
        //    //    log.Info(String.Format("Downloading {0}", Downloader.CurrentFile.Path));
        //    //    log.Info(String.Format("File size: {0}", FileDownloader.FormatSizeBinary(Downloader.CurrentFileSize)));
        //    //    log.Info(String.Format("Saving to {0}\\{1}", Downloader.LocalDirectory, Downloader.CurrentFile.Name));
        //    //}
        //}

        //private void downloader_FileDownloadPaused(object sender, EventArgs e)
        //{
        //    InsertOrUpdateDownloadMedia(StatusConstants.MEDIA_DOWNLOAD_PAUSE_STATE, DownloadProgress);
        //}

        //private void downloader_FileDownloadResumed(object sender, EventArgs e)
        //{
        //    InsertOrUpdateDownloadMedia(StatusConstants.MEDIA_DOWNLOADING_STATE, DownloadProgress);
        //}

        //private void downloader_FileDownloadCanceled(object sender, EventArgs e)
        //{
        //    //if (DefaultSettings.DEBUG)
        //    //    log.Info("Download(s) canceled");
        //    DeleteDownloadMedia(ContentId);
        //}


        //private void downloader_FileDownloadFailed(object sender, Exception ex)
        //{
        //    //Downloader.Stop();
        //    DeleteDownloadMedia(ContentId);
        //    CustomMessageBox.ShowError(ex.Message);
        //}

        //private void downloader_FileDownloadCompleted(object sender, EventArgs e)
        //{
        //    //if (DefaultSettings.DEBUG)
        //    //    log.Info(String.Format("Download complete, downloaded {0} files.", Downloader.Files.Count));

        //    InsertOrUpdateDownloadMedia(StatusConstants.MEDIA_DOWNLOAD_COMPLETED_STATE);
        //    var model = RingIDViewModel.Instance.UploadingModelList.FirstOrDefault(p => p.ContentId == ContentId);
        //    RingIDViewModel.Instance.UploadingModelList.InvokeRemove(model);
        //    if (DownloadOrAddToAlbumPopUpWrapper.ucUploadingPopup != null)
        //    {
        //        DownloadOrAddToAlbumPopUpWrapper.ucUploadingPopup.ClosePopUp();
        //    }
        //}

        ////private void downloader_CancelRequested(object sender, EventArgs e)
        ////{
        ////    if (DefaultSettings.DEBUG)
        ////        log.Info("Canceling downloads...");
        ////}

        ////private void downloader_DeletingFilesAfterCancel(object sender, EventArgs e)
        ////{
        ////    //if (DefaultSettings.DEBUG)
        ////    //    log.Info("Canceling downloads - deleting files...");
        ////}



        //public void InsertOrUpdateDownloadMedia(int state, int downloadProgress = 0)
        //{
        //    DownloadState = state;
        //    DownloadProgress = downloadProgress;
        //    DownloadTime = Models.Utility.ModelUtility.CurrentTimeMillisLocal();

        //    //GetSingleMediaDto.DownloadState = DownloadState;
        //    //GetSingleMediaDto.DownloadProgress = DownloadProgress;
        //    //GetSingleMediaDto.DownloadTime = Models.Utility.ModelUtility.CurrentTimeMillisLocal();

        //    //lock (MediaDictionaries.Instance.DOWNLOAD_MEDIAS)
        //    //{
        //    //    MediaDictionaries.Instance.DOWNLOAD_MEDIAS[GetSingleMediaDto.ContentId] = GetSingleMediaDto;
        //    //}
        //    if (!RingIDViewModel.Instance.MyDownloads.Any(P => P.ContentId == ContentId))
        //        RingIDViewModel.Instance.MyDownloads.InvokeAdd(this);

        //    MediaDAO.Instance.InsertIntoDownloadedMediasTable(this.ContentId, this.UserTableID, this.Title, this.Artist, this.StreamUrl, this.ThumbUrl, this.Duration, this.ThumbImageWidth, this.ThumbImageHeight, this.MediaType, this.AlbumId, this.AlbumName, this.AccessCount, this.DownloadState, this.DownloadTime, this.DownloadProgress);
        //    //InsertIntoDownloadMedia(GetSingleMediaDto);
        //}

        //public void DeleteDownloadMedia(long mediaContentId)
        //{
        //    DownloadState = StatusConstants.MEDIA_DOWNLOAD_NOT_START_STATE;
        //    DownloadProgress = 0;

        //    //lock (MediaDictionaries.Instance.DOWNLOAD_MEDIAS)
        //    //{
        //    //    MediaDictionaries.Instance.DOWNLOAD_MEDIAS.Remove(mediaContentId);
        //    //}
        //    MediaDAO.Instance.DeleteFromDownloadedMediasTable(mediaContentId);

        //    if (Downloader.Files != null && Downloader.Files.Count() > 1)
        //    {
        //        System.IO.File.Delete(Downloader.LocalDirectory + "\\" + Downloader.Files[0].Name);
        //    }

        //    var model = RingIDViewModel.Instance.UploadingModelList.FirstOrDefault(p => p.ContentId == ContentId);
        //    if (model != null)
        //    {
        //        model.IsUploadFailed = true;
        //    }
        //}

        //#endregion
        //#region "Media Command"
        //private ICommand _FeedMediaLeftButtonCommand;
        //public ICommand FeedMediaLeftButtonCommand
        //{
        //    get
        //    {
        //        if (_FeedMediaLeftButtonCommand == null)
        //        {
        //            _FeedMediaLeftButtonCommand = new RelayCommand(param => MediaUtility.OnFeedMediaLeftClick(param, this.ContentId));
        //        }
        //        return _FeedMediaLeftButtonCommand;
        //    }
        //}
        //#endregion
        //private void HandlePopupPausedResume()
        //{
        //    Application.Current.Dispatcher.BeginInvoke(() =>
        //    {
        //        if (_DownloadState != StatusConstants.MEDIA_DOWNLOADING_STATE && FeedDownloadPauseCancelPopup.popupPausedResume.IsOpen)
        //        {
        //            FeedDownloadPauseCancelPopup.popupPausedResume.IsOpen = false;
        //        }
        //    });
        //}

        //private int _DownloadingProgress;
        //public int DownloadingProgress
        //{
        //    get { return _DownloadingProgress; }
        //    set
        //    {
        //        if (value == _DownloadingProgress)
        //            return;

        //        _DownloadingProgress = value;
        //        this.OnPropertyChanged("DownloadingProgress");
        //    }
        //}
        //public FileDownloader Downloader;
        //public FileDownloader Downloader
        //{
        //    get { return _Downloader; }
        //    set
        //    {
        //        if (value == _Downloader)
        //            return;
        //        _Downloader = value;
        //        this.OnPropertyChanged("Downloader");
        //    }
        //}

        //private SingleMediaDTO singleMediaDTO;
        //public SingleMediaDTO GetSingleMediaDto
        //{
        //    get
        //    {
        //        return singleMediaDTO;
        //    }
        //}
        //public SingleMediaDTO GetDTOFromModel()
        //{
        //    SingleMediaDTO dto = new SingleMediaDTO();
        //    dto.ContentId = this.ContentId;
        //    dto.UserTableID = this.UserTableID;
        //    dto.AlbumId = this.AlbumId;
        //    dto.AlbumName = this.AlbumName;
        //    dto.Title = this.Title;
        //    dto.StreamUrl = this.StreamUrl;
        //    dto.ThumbImageHeight = this.ThumbImageHeight;
        //    dto.ThumbImageWidth = this.ThumbImageWidth;
        //    dto.Duration = this.Duration;
        //    dto.MediaType = this.MediaType;
        //    dto.LikeCount = (short)this.LikeCount;
        //    dto.CommentCount = this.CommentCount;
        //    dto.AccessCount = this.AccessCount;
        //    dto.ILike = this.ILike;
        //    dto.IComment = this.IComment;
        //    dto.IShare = this.IShare;
        //    dto.ShareCount = (short)this.ShareCount;
        //    dto.FullName = this.FullName;
        //    dto.ProfileImage = this.ProfileImage;
        //    dto.Privacy = this.Privacy;
        //    dto.ContentId = this.ContentId;
        //    return dto;
        //}
        //public void LoadData(SingleMediaDTO singleMediaDTO)
        //{
        //    try
        //    {
        //        //SingleMediaDTO dto = null;
        //        //MediaDictionaries.Instance.DOWNLOAD_MEDIAS.TryGetValue(singleMediaDTO.ContentId, out dto);
        //        //if (dto != null)
        //        //{
        //        //    singleMediaDTO.DownloadState = dto.DownloadState;
        //        //    singleMediaDTO.DownloadProgress = dto.DownloadProgress;
        //        //    singleMediaDTO.DownloadTime = dto.DownloadTime;
        //        //    lock (MediaDictionaries.Instance.DOWNLOAD_MEDIAS)
        //        //        MediaDictionaries.Instance.DOWNLOAD_MEDIAS[singleMediaDTO.ContentId] = singleMediaDTO;
        //        //}
        //    }
        //    catch (Exception e)
        //    {
        //        log.Error(e.Message + "\n" + e.StackTrace + "==>" + e.Message);
        //    }

        //    ThumbUrl = singleMediaDTO.ThumbUrl;
        //    StreamUrl = singleMediaDTO.StreamUrl;
        //    Title = singleMediaDTO.Title;
        //    Artist = singleMediaDTO.Artist;
        //    Duration = singleMediaDTO.Duration;
        //    ThumbImageWidth = singleMediaDTO.ThumbImageWidth;
        //    ThumbImageHeight = singleMediaDTO.ThumbImageHeight;
        //    ContentId = singleMediaDTO.ContentId;
        //    ILike = singleMediaDTO.ILike;
        //    IComment = singleMediaDTO.IComment;
        //    Privacy = singleMediaDTO.Privacy;
        //    AccessCount = singleMediaDTO.AccessCount;
        //    LikeCount = singleMediaDTO.LikeCount;
        //    CommentCount = singleMediaDTO.CommentCount;
        //    MediaType = singleMediaDTO.MediaType;
        //    AlbumId = singleMediaDTO.AlbumId;
        //    AlbumName = singleMediaDTO.AlbumName;
        //    UserTableID = singleMediaDTO.UserTableID;
        //    LastPlayedTime = singleMediaDTO.LastPlayedTime;
        //    DownloadState = singleMediaDTO.DownloadState;
        //    DownloadTime = singleMediaDTO.DownloadTime;
        //    ProfileImage = singleMediaDTO.ProfileImage;
        //    FullName = singleMediaDTO.FullName;
        //    IShare = singleMediaDTO.IShare;
        //    ShareCount = singleMediaDTO.ShareCount;
        //    if (singleMediaDTO.NewsFeedId > 0) NewsFeedId = singleMediaDTO.NewsFeedId;
        //    CurrentInstance = this;
        //}
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

﻿using Models.Entity;
using System.ComponentModel;
using System.Windows;

namespace View.BindingModels
{
    public class RoomCategoryModel : INotifyPropertyChanged
    {

        private string _CategoryName;
        public string CategoryName
        {
            get { return _CategoryName; }
            set
            {
                if (value == _CategoryName)
                    return;

                _CategoryName = value;
                this.OnPropertyChanged("CategoryName");
            }
        }

        private int _CategoryType;
        public int CategoryType
        {
            get { return _CategoryType; }
            set
            {
                if (value == _CategoryType)
                    return;

                _CategoryType = value;
                this.OnPropertyChanged("CategoryType");
            }
        }

        private bool _Subscribed;
        public bool Subscribed
        {
            get { return _Subscribed; }
            set
            {
                if (value == _Subscribed)
                    return;

                _Subscribed = value;
                this.OnPropertyChanged("Subscribed");
            }
        }

        private bool _TempSelected = true;
        public bool TempSelected
        {
            get { return _TempSelected; }
            set
            {
                if (value == _TempSelected)
                    return;

                _TempSelected = value;
                this.OnPropertyChanged("TempSelected");
            }
        }
        private Visibility _VisibilityInCountrySearch = Visibility.Visible;
        public Visibility VisibilityInCountrySearch
        {
            get { return _VisibilityInCountrySearch; }
            set
            {
                if (value == _VisibilityInCountrySearch)
                    return;
                _VisibilityInCountrySearch = value;
                this.OnPropertyChanged("VisibilityInCountrySearch");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

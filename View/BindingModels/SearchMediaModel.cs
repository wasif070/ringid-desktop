﻿using Models.Entity;
using System.ComponentModel;

namespace View.BindingModels
{
    public class SearchMediaModel : INotifyPropertyChanged
    {
        public void LoadData(SearchMediaDTO dto)
        {
            this.SearchSuggestion = dto.SearchSuggestion;
            this.SearchType = dto.SearchType;
            this.UpdateTime = dto.UpdateTime;
        }

        private long _UpdateTime;
        public long UpdateTime
        {
            get { return _UpdateTime; }
            set
            {
                if (value == _UpdateTime)
                    return;

                _UpdateTime = value;
                this.OnPropertyChanged("UpdateTime");
            }
        }

        private int _SearchType;
        public int SearchType
        {
            get { return _SearchType; }
            set
            {
                if (value == _SearchType)
                    return;

                _SearchType = value;
                this.OnPropertyChanged("SearchType");
            }
        }

        private string _SearchSuggestion = string.Empty;
        public string SearchSuggestion
        {
            get { return _SearchSuggestion; }
            set
            {
                if (value == _SearchSuggestion)
                    return;

                _SearchSuggestion = value;
                this.OnPropertyChanged("SearchSuggestion");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

<<<<<<< HEAD
﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Models.Stores;
using View.Utility;

namespace View.BindingModels
{   

    public class EmoticonModel : INotifyPropertyChanged
    {

        #region Constructor

        public EmoticonModel()
        {
        }

        public EmoticonModel(EmoticonDTO emoticonDTO)
        {
            LoadData(emoticonDTO);
        }

        #endregion Constructor

        #region Property

        private int _ID;
        public int ID
        {
            get { return _ID; }
            set
            {
                if (value == _ID)
                    return;

                _ID = value;
                this.OnPropertyChanged("ID");
            }
        }

        private string _Name = String.Empty;
        public string Name
        {
            get { return _Name; }
            set
            {
                if (value == _Name)
                    return;

                _Name = value;
                this.OnPropertyChanged("Name");
            }
        }

        private string _Symbol = String.Empty;
        public string Symbol
        {
            get { return _Symbol; }
            set
            {
                if (value == _Symbol)
                    return;

                _Symbol = value;
                this.OnPropertyChanged("Symbol");
            }
        }

        private string _Url;
        public string Url
        {
            get { return _Url; }
            set
            {
                if (value == _Url)
                    return;

                _Url = value;
                this.OnPropertyChanged("Url");
            }
        }

        private int _Type;
        public int Type
        {
            get { return _Type; }
            set
            {
                if (value == _Type)
                    return;

                _Type = value;
                this.OnPropertyChanged("Type");
            }
        }

        #endregion Property

        #region Utility Method

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void LoadData(EmoticonDTO emoticonDTO)
        {
            if (emoticonDTO != null)
            {
                this.ID = emoticonDTO.ID;
                this.Name = emoticonDTO.Name;
                this.Url = emoticonDTO.Url;
                this.Symbol = emoticonDTO.Symbol;
                this.Type = emoticonDTO.Type;
            }
        }

        #endregion Utility Method
    }

    public class MarketStickerCategoryModel : INotifyPropertyChanged
    {
        #region Constructor

        public MarketStickerCategoryModel(MarketStickerCategoryDTO categoryDTO)
        {
            UpdateData(categoryDTO);
        }

        #endregion


        #region Property

        private int _StickerCategoryID;
        public int StickerCategoryID
        {
            get { return _StickerCategoryID; }
            set
            {
                if (value == _StickerCategoryID)
                    return;

                _StickerCategoryID = value;
                this.OnPropertyChanged("StickerCategoryID");
            }
        }

        private string _StickerCategoryName;
        public string StickerCategoryName
        {
            get { return _StickerCategoryName; }
            set
            {
                if (value == _StickerCategoryName)
                    return;

                _StickerCategoryName = value;
                this.OnPropertyChanged("StickerCategoryName");
            }
        }

        private int _StickerCollectionID;
        public int StickerCollectionID
        {
            get { return _StickerCollectionID; }
            set
            {
                if (value == _StickerCollectionID)
                    return;

                _StickerCollectionID = value;
                this.OnPropertyChanged("StickerCollectionID");
            }
        }

        private string _CategoryBannerImage;
        public string CategoryBannerImage
        {
            get { return _CategoryBannerImage; }
            set
            {
                if (value == _CategoryBannerImage)
                    return;

                _CategoryBannerImage = value;
                this.OnPropertyChanged("CategoryBannerImage");
            }
        }

        private string _Icon;
        public string Icon
        {
            get { return _Icon; }
            set
            {
                _Icon = value;
                this.OnPropertyChanged("Icon");
            }
        }

        private string _DetailImage;
        public string DetailImage
        {
            get { return _DetailImage; }
            set
            {
                if (value == _DetailImage)
                    return;

                _DetailImage = value;
                this.OnPropertyChanged("DetailImage");
            }
        }

        private string _ThumbDetailImage;
        public string ThumbDetailImage
        {
            get { return _ThumbDetailImage; }
            set
            {
                if (value == _ThumbDetailImage)
                    return;

                _ThumbDetailImage = value;
                this.OnPropertyChanged("ThumbDetailImage");
            }
        }

        public bool _IsFree = false;
        public bool IsFree
        {
            get { return _IsFree; }
            set
            {
                if (value == _IsFree)
                    return;

                _IsFree = value;
                this.OnPropertyChanged("IsFree");
            }
        }

        public bool _IsNew = false;
        public bool IsNew
        {
            get { return _IsNew; }
            set
            {
                if (value == _IsNew)
                    return;

                _IsNew = value;
                this.OnPropertyChanged("IsNew");
            }
        }


        public bool _IsDownloadRunning = false;
        public bool IsDownloadRunning
        {
            get { return _IsDownloadRunning; }
            set
            {
                if (value == _IsDownloadRunning)
                    return;

                _IsDownloadRunning = value;
                this.OnPropertyChanged("IsDownloadRunning");
            }
        }

        public bool _Downloaded = false;
        public bool Downloaded
        {
            get { return _Downloaded; }
            set
            {
                if (value == _Downloaded)
                    return;

                _Downloaded = value;
                this.OnPropertyChanged("Downloaded");
            }
        }

        public bool _IsDefault = false;
        public bool IsDefault
        {
            get { return _IsDefault; }
            set
            {
                if (value == _IsDefault)
                    return;

                _IsDefault = value;
                this.OnPropertyChanged("IsDefault");
            }
        }

        public bool _IsIconDownloading = false;
        public bool IsIconDownloading
        {
            get { return _IsIconDownloading; }
            set
            {
                if (value == _IsIconDownloading)
                    return;

                _IsIconDownloading = value;
                this.OnPropertyChanged("IsIconDownloading");
            }
        }

        public bool _IsMarketIconDownloading = false;
        public bool IsMarketIconDownloading
        {
            get { return _IsMarketIconDownloading; }
            set
            {
                if (value == _IsMarketIconDownloading)
                    return;

                _IsMarketIconDownloading = value;
                this.OnPropertyChanged("IsMarketIconDownloading");
            }
        }

        public bool _IsDetailImageDownloading = false;
        public bool IsDetailImageDownloading
        {
            get { return _IsDetailImageDownloading; }
            set
            {
                if (value == _IsDetailImageDownloading)
                    return;

                _IsDetailImageDownloading = value;
                this.OnPropertyChanged("IsDetailImageDownloading");
            }
        }

        public bool _IsThumbDetailImageDownloading = false;
        public bool IsThumbDetailImageDownloading
        {
            get { return _IsThumbDetailImageDownloading; }
            set
            {
                if (value == _IsThumbDetailImageDownloading)
                    return;

                _IsThumbDetailImageDownloading = value;
                this.OnPropertyChanged("IsThumbDetailImageDownloading");
            }
        }

        private bool _IsNewStickerSeen = false;
        public bool IsNewStickerSeen
        {
            get { return _IsNewStickerSeen; }
            set
            {
                if (value == _IsNewStickerSeen) return;
                _IsNewStickerSeen = value;
                this.OnPropertyChanged("IsNewStickerSeen");
            }
        }

        private int _DownloadPercentage;
        public int DownloadPercentage
        {
            get { return _DownloadPercentage; }
            set
            {
                _DownloadPercentage = value;
                this.OnPropertyChanged("DownloadPercentage");
            }
        }


        private ObservableCollection<MarkertStickerImagesModel> _ImageList = new ObservableCollection<MarkertStickerImagesModel>();
        public ObservableCollection<MarkertStickerImagesModel> ImageList
        {
            get { return _ImageList; }
            set
            {
                if (_ImageList == value) return;
                _ImageList = value;
                this.OnPropertyChanged("ImageList");
            }
        }

        private long _LastUsedDate;
        public long LastUsedDate
        {
            get { return _LastUsedDate; }
            set
            {
                if (value == _LastUsedDate)
                    return;

                _LastUsedDate = value;
                this.OnPropertyChanged("LastUsedDate");
            }
        }

        private long _DownloadTime;
        public long DownloadTime
        {
            get { return _DownloadTime; }
            set
            {
                if (value == _DownloadTime)
                    return;

                _DownloadTime = value;
                this.OnPropertyChanged("DownloadTime");
            }
        }

        private double _CalculationValue;
        public double CalculationValue
        {
            get { return _CalculationValue; }
            set { _CalculationValue = value; }
        }

        private int _Rank;
        public int Rank
        {
            get { return _Rank; }
            set
            {
                if (value == _Rank)
                    return;

                _Rank = value;
                this.OnPropertyChanged("Rank");
            }
        }

        private bool _IsSelected  = false;
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                if (value == _IsSelected)
                    return;

                _IsSelected = value;
                this.OnPropertyChanged("IsSelected");
            }
        }

        private bool _IsSelectedInComment = false;
        public bool IsSelectedInComment
        {
            get { return _IsSelectedInComment; }
            set
            {
                if (value == _IsSelectedInComment)
                    return;
                
                _IsSelectedInComment = value;
                this.OnPropertyChanged("IsSelectedInComment");
            }
        }

        //public bool _IsTop = false;
        //public bool IsTop
        //{
        //    get { return _IsTop; }
        //    set
        //    {
        //        if (value == _IsTop)
        //            return;

        //        _IsTop = value;
        //        this.OnPropertyChanged("IsTop");
        //    }
        //}

        #endregion

        #region Utility

        public void UpdateData(MarketStickerCategoryDTO categoryDTO)
        {
            if (categoryDTO != null)
            {
                this.StickerCategoryID = categoryDTO.sCtId;
                this.StickerCollectionID = categoryDTO.sClId;
                this.StickerCategoryName = categoryDTO.sctName;
                this.CategoryBannerImage = categoryDTO.cgBnrImg;
                this.Rank = categoryDTO.Rnk;
                this.DetailImage = categoryDTO.DetailImg;
                this.ThumbDetailImage = categoryDTO.ThumbDetailImage;
                this.IsNew = categoryDTO.cgNw;
                this.Icon = categoryDTO.Icon;
                //this.Prize = categoryDTO.Prz;
                this.IsFree = categoryDTO.free;
                //this.IsTop = categoryDTO.IsTop;
                //this.Description = categoryDTO.Dcpn;
                this.Downloaded = categoryDTO.Downloaded;
                this.DownloadTime = categoryDTO.DownloadTime;
                //this.SortOrder = categoryDTO.SortOrder;
                //this.IsVisible = categoryDTO.IsVisible;
                this.IsDefault = categoryDTO.IsDefault;
                this.LastUsedDate = categoryDTO.LastUsedDate;
                this.IsNewStickerSeen = categoryDTO.IsNewStickerSeen;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }

    public class MarketStickerCollectionModel : INotifyPropertyChanged
    {
        #region Constructor

        public MarketStickerCollectionModel(MarketStickerCollectionDTO collectionDTO)
        {
            UpdateData(collectionDTO);
        }

        #endregion

        #region Property

        private string _StickerCollectionName;
        public string StickerCollectionName
        {
            get { return _StickerCollectionName; }
            set
            {
                if (value == _StickerCollectionName)
                    return;

                _StickerCollectionName = value;
                this.OnPropertyChanged("StickerCollectionName");
            }
        }

        private int _StickerCollectionID;
        public int StickerCollectionID
        {
            get { return _StickerCollectionID; }
            set
            {
                if (value == _StickerCollectionID)
                    return;

                _StickerCollectionID = value;
                this.OnPropertyChanged("StickerCollectionID");
            }
        }

        private string _CategoryBannerImage;
        public string CategoryBannerImage
        {
            get { return _CategoryBannerImage; }
            set
            {
                if (value == _CategoryBannerImage)
                    return;

                _CategoryBannerImage = value;
                this.OnPropertyChanged("CategoryBannerImage");
            }
        }

        private ObservableCollection<MarketStickerCategoryModel> _CategoryList = new ObservableCollection<MarketStickerCategoryModel>();
        public ObservableCollection<MarketStickerCategoryModel> CategoryList
        {
            get { return _CategoryList; }
            set
            {
                if (value == _CategoryList)
                    return;

                _CategoryList = value;
                this.OnPropertyChanged("CategoryList");
            }
        }

        #endregion

        #region Utility

        public void UpdateData(MarketStickerCollectionDTO collectionDTO)
        {
            this.StickerCollectionID = collectionDTO.sClId;
            this.StickerCollectionName = collectionDTO.name;
            this.CategoryBannerImage = collectionDTO.bnrImg;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion


    }

    public class MarkertStickerImagesModel : INotifyPropertyChanged
    {
        #region Constructor
        public MarkertStickerImagesModel(MarkertStickerImagesDTO ImgDto)
        {
            UpdateData(ImgDto);
        }
        #endregion

        #region Property

        private int _ImageID;
        public int ImageID
        {
            get { return _ImageID; }
            set
            {
                if (value == _ImageID)
                    return;

                _ImageID = value;
                this.OnPropertyChanged("ImageID");
            }
        }

        private int _StickerCategoryID;
        public int StickerCategoryID
        {
            get { return _StickerCategoryID; }
            set
            {
                if (value == _StickerCategoryID)
                    return;

                _StickerCategoryID = value;
                this.OnPropertyChanged("StickerCategoryID");
            }
        }

        private int _StickerCollectionID;
        public int StickerCollectionID
        {
            get { return _StickerCollectionID; }
            set
            {
                if (value == _StickerCollectionID)
                    return;

                _StickerCollectionID = value;
                this.OnPropertyChanged("StickerCollectionID");
            }
        }

        private string _ImageUrl;
        public string ImageUrl
        {
            get { return _ImageUrl; }
            set
            {
                if (value == _ImageUrl)
                    return;

                _ImageUrl = value;
                this.OnPropertyChanged("ImageUrl");
            }
        }

        public bool _IsRecent;
        public bool IsRecent
        {
            get { return _IsRecent; }
            set
            {
                _IsRecent = value;
                this.OnPropertyChanged("IsRecent");
            }
        }

        private bool _SingleStickerImageFocusable = false;
        public bool SingleStickerImageFocusable
        {
            get
            {
                return _SingleStickerImageFocusable;
            }

            set
            {
                _SingleStickerImageFocusable = value;
                OnPropertyChanged("SingleStickerImageFocusable");
            }
        }

        #endregion

        #region Utility

        public void UpdateData(MarkertStickerImagesDTO imgDTO)
        {
            this.ImageID = imgDTO.imId;
            this.StickerCategoryID = imgDTO.sCtId;
            this.StickerCollectionID = imgDTO.sClId;
            this.ImageUrl = imgDTO.imUrl;
            this.IsRecent = imgDTO.IsRecent;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public override string ToString()
        {
            return this.StickerCollectionID.ToString() + Path.AltDirectorySeparatorChar + this.StickerCategoryID.ToString() + Path.AltDirectorySeparatorChar + this.ImageUrl;
        }

        #endregion

    }

    public class MarketStickerLanguageModel : INotifyPropertyChanged
    {
        #region Constructor

        public MarketStickerLanguageModel(MarketStickerLanguageyDTO cntryDTO)
        {
            UpdateData(cntryDTO);
        }

        #endregion

        #region Property

        //private int _CountryId;
        //public int CountryId
        //{
        //    get { return _CountryId; }
        //    set
        //    {
        //        if (value == _CountryId)
        //            return;

        //        _CountryId = value;
        //        this.OnPropertyChanged("CountryId");
        //    }
        //}        

        //private string _CountryCode;
        //public string CountryCode
        //{
        //    get { return _CountryCode; }
        //    set
        //    {
        //        if (value == _CountryCode)
        //            return;

        //        _CountryCode = value;
        //        this.OnPropertyChanged("CountryCode");
        //    }
        //}

        //private string _CountrySymbol;
        //public string CountrySymbol
        //{
        //    get { return _CountrySymbol; }
        //    set
        //    {
        //        if (value == _CountrySymbol)
        //            return;

        //        _CountrySymbol = value;
        //        this.OnPropertyChanged("CountrySymbol");
        //    }
        //}

        private string _CountryName;
        public string CountryName
        {
            get { return _CountryName; }
            set
            {
                if (value == _CountryName)
                    return;

                _CountryName = value;
                this.OnPropertyChanged("CountryName");
            }
        }

        private ObservableCollection<MarketStickerCategoryModel> _CategoryList = new ObservableCollection<MarketStickerCategoryModel>();
        public ObservableCollection<MarketStickerCategoryModel> CategoryList
        {
            get { return _CategoryList; }
            set
            {
                if (value == _CategoryList)
                    return;

                _CategoryList = value;
                this.OnPropertyChanged("CategoryList");
            }
        }

        #endregion

        #region Utility

        public void UpdateData(MarketStickerLanguageyDTO cntryDTO)
        {
            //this.CountryId = cntryDTO.id;
            this.CountryName = cntryDTO.name;
            //this.CountryCode = cntryDTO.code;
            //this.CountrySymbol = cntryDTO.countrySymbol;
            //this.CountryName = cntryDTO.name;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
}
=======
﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Models.Stores;
using View.Utility;

namespace View.BindingModels
{

    public class EmoticonModel : INotifyPropertyChanged
    {

        #region Constructor

        public EmoticonModel()
        {
        }

        public EmoticonModel(EmoticonDTO emoticonDTO)
        {
            LoadData(emoticonDTO);
        }

        #endregion Constructor

        #region Property

        private int _ID;
        public int ID
        {
            get { return _ID; }
            set
            {
                if (value == _ID)
                    return;

                _ID = value;
                this.OnPropertyChanged("ID");
            }
        }

        private string _Name = String.Empty;
        public string Name
        {
            get { return _Name; }
            set
            {
                if (value == _Name)
                    return;

                _Name = value;
                this.OnPropertyChanged("Name");
            }
        }

        private string _Symbol = String.Empty;
        public string Symbol
        {
            get { return _Symbol; }
            set
            {
                if (value == _Symbol)
                    return;

                _Symbol = value;
                this.OnPropertyChanged("Symbol");
            }
        }

        private string _Url;
        public string Url
        {
            get { return _Url; }
            set
            {
                if (value == _Url)
                    return;

                _Url = value;
                this.OnPropertyChanged("Url");
            }
        }

        private int _Type;
        public int Type
        {
            get { return _Type; }
            set
            {
                if (value == _Type)
                    return;

                _Type = value;
                this.OnPropertyChanged("Type");
            }
        }

        #endregion Property

        #region Utility Method

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void LoadData(EmoticonDTO emoticonDTO)
        {
            if (emoticonDTO != null)
            {
                this.ID = emoticonDTO.ID;
                this.Name = emoticonDTO.Name;
                this.Url = emoticonDTO.Url;
                this.Symbol = emoticonDTO.Symbol;
                this.Type = emoticonDTO.Type;
            }
        }

        #endregion Utility Method
    }

    public class MarketStickerDynamicCategoryModel : INotifyPropertyChanged
    {
        #region Constructor

        public MarketStickerDynamicCategoryModel()
        {
        }

        public MarketStickerDynamicCategoryModel(MarketStickerDynamicCategoryDTO dynamicCategoryDTO)
        {
            LoadData(dynamicCategoryDTO);
        }

        #endregion

        #region Property

        private int _DynamicCategoryId;
        public int DynamicCategoryId
        {
            get { return _DynamicCategoryId; }
            set
            {
                if (value == _DynamicCategoryId)
                    return;

                _DynamicCategoryId = value;
                this.OnPropertyChanged("DynamicCategoryId");
            }
        }

        private string _DynamicCategoryName;
        public string DynamicCategoryName
        {
            get { return _DynamicCategoryName; }
            set
            {
                if (value == _DynamicCategoryName)
                    return;

                _DynamicCategoryName = value;
                this.OnPropertyChanged("DynamicCategoryName");
            }
        }

        private int _DynamicCategoryShape;
        public int DynamicCategoryShape
        {
            get { return _DynamicCategoryShape; }
            set
            {
                if (value == _DynamicCategoryShape)
                    return;

                _DynamicCategoryShape = value;
                this.OnPropertyChanged("DynamicCategoryShape");
            }
        }

        private List<MarketStickerCategoryModel> _DynamicCategoryBannerList = new List<MarketStickerCategoryModel>();
        public List<MarketStickerCategoryModel> DynamicCategoryBannerList
        {
            get { return _DynamicCategoryBannerList; }
            set
            {
                _DynamicCategoryBannerList = value;
                this.OnPropertyChanged("DynamicCategoryBannerList");
            }
        }

        private List<MarketStickerCategoryModel> _DynamicCategoryList = new List<MarketStickerCategoryModel>();
        public List<MarketStickerCategoryModel> DynamicCategoryList
        {
            get { return _DynamicCategoryList; }
            set
            {
                _DynamicCategoryList = value;
                this.OnPropertyChanged("DynamicCategoryList");
            }
        }

        #endregion Property

        #region Utility

        public void LoadData(MarketStickerDynamicCategoryDTO dynamicCategoryDTO)
        {
            this.DynamicCategoryId = dynamicCategoryDTO.id;
            this.DynamicCategoryName = dynamicCategoryDTO.name;
            this.DynamicCategoryShape = dynamicCategoryDTO.shp;
            //if (dynamicCategoryDTO.banner != null) 
            //{ 
            //foreach (MarketStickerCategoryDTO ctgDTO in dynamicCategoryDTO.banner)
            //{
            //    MarketStickerCategoryModel ctgModel = new 
            //}
            //}
            //this.DynamicCategoryBannerList = dynamicCategoryDTO.banner;           
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility
    }

    public class MarketStickerCategoryModel : INotifyPropertyChanged
    {
        #region Constructor

        public MarketStickerCategoryModel(MarketStickerCategoryDTO categoryDTO)
        {
            UpdateData(categoryDTO);
        }

        #endregion


        #region Property

        private int _StickerCategoryID;
        public int StickerCategoryID
        {
            get { return _StickerCategoryID; }
            set
            {
                if (value == _StickerCategoryID)
                    return;

                _StickerCategoryID = value;
                this.OnPropertyChanged("StickerCategoryID");
            }
        }

        private string _StickerCategoryName;
        public string StickerCategoryName
        {
            get { return _StickerCategoryName; }
            set
            {
                if (value == _StickerCategoryName)
                    return;

                _StickerCategoryName = value;
                this.OnPropertyChanged("StickerCategoryName");
            }
        }

        private int _StickerCollectionID;
        public int StickerCollectionID
        {
            get { return _StickerCollectionID; }
            set
            {
                if (value == _StickerCollectionID)
                    return;

                _StickerCollectionID = value;
                this.OnPropertyChanged("StickerCollectionID");
            }
        }

        private string _CategoryBannerImage;
        public string CategoryBannerImage
        {
            get { return _CategoryBannerImage; }
            set
            {
                if (value == _CategoryBannerImage)
                    return;

                _CategoryBannerImage = value;
                this.OnPropertyChanged("CategoryBannerImage");
            }
        }

        private string _Icon;
        public string Icon
        {
            get { return _Icon; }
            set
            {
                _Icon = value;
                this.OnPropertyChanged("Icon");
            }
        }

        private string _DetailImage;
        public string DetailImage
        {
            get { return _DetailImage; }
            set
            {
                if (value == _DetailImage)
                    return;

                _DetailImage = value;
                this.OnPropertyChanged("DetailImage");
            }
        }

        private string _ThumbDetailImage;
        public string ThumbDetailImage
        {
            get { return _ThumbDetailImage; }
            set
            {
                if (value == _ThumbDetailImage)
                    return;

                _ThumbDetailImage = value;
                this.OnPropertyChanged("ThumbDetailImage");
            }
        }

        public bool _IsFree = false;
        public bool IsFree
        {
            get { return _IsFree; }
            set
            {
                if (value == _IsFree)
                    return;

                _IsFree = value;
                this.OnPropertyChanged("IsFree");
            }
        }

        public bool _IsNew = false;
        public bool IsNew
        {
            get { return _IsNew; }
            set
            {
                if (value == _IsNew)
                    return;

                _IsNew = value;
                this.OnPropertyChanged("IsNew");
            }
        }


        public bool _IsDownloadRunning = false;
        public bool IsDownloadRunning
        {
            get { return _IsDownloadRunning; }
            set
            {
                if (value == _IsDownloadRunning)
                    return;

                _IsDownloadRunning = value;
                this.OnPropertyChanged("IsDownloadRunning");
            }
        }

        public bool _Downloaded = false;
        public bool Downloaded
        {
            get { return _Downloaded; }
            set
            {
                if (value == _Downloaded)
                    return;

                _Downloaded = value;
                this.OnPropertyChanged("Downloaded");
            }
        }

        public bool _IsDefault = false;
        public bool IsDefault
        {
            get { return _IsDefault; }
            set
            {
                if (value == _IsDefault)
                    return;

                _IsDefault = value;
                this.OnPropertyChanged("IsDefault");
            }
        }

        public bool _IsIconDownloading = false;
        public bool IsIconDownloading
        {
            get { return _IsIconDownloading; }
            set
            {
                if (value == _IsIconDownloading)
                    return;

                _IsIconDownloading = value;
                this.OnPropertyChanged("IsIconDownloading");
            }
        }

        public bool _IsMarketIconDownloading = false;
        public bool IsMarketIconDownloading
        {
            get { return _IsMarketIconDownloading; }
            set
            {
                if (value == _IsMarketIconDownloading)
                    return;

                _IsMarketIconDownloading = value;
                this.OnPropertyChanged("IsMarketIconDownloading");
            }
        }

        public bool _IsDetailImageDownloading = false;
        public bool IsDetailImageDownloading
        {
            get { return _IsDetailImageDownloading; }
            set
            {
                if (value == _IsDetailImageDownloading)
                    return;

                _IsDetailImageDownloading = value;
                this.OnPropertyChanged("IsDetailImageDownloading");
            }
        }

        public bool _IsThumbDetailImageDownloading = false;
        public bool IsThumbDetailImageDownloading
        {
            get { return _IsThumbDetailImageDownloading; }
            set
            {
                if (value == _IsThumbDetailImageDownloading)
                    return;

                _IsThumbDetailImageDownloading = value;
                this.OnPropertyChanged("IsThumbDetailImageDownloading");
            }
        }

        private bool _IsNewStickerSeen = false;
        public bool IsNewStickerSeen
        {
            get { return _IsNewStickerSeen; }
            set
            {
                if (value == _IsNewStickerSeen) return;
                _IsNewStickerSeen = value;
                this.OnPropertyChanged("IsNewStickerSeen");
            }
        }

        private int _DownloadPercentage;
        public int DownloadPercentage
        {
            get { return _DownloadPercentage; }
            set
            {
                _DownloadPercentage = value;
                this.OnPropertyChanged("DownloadPercentage");
            }
        }


        private ObservableCollection<MarkertStickerImagesModel> _ImageList = new ObservableCollection<MarkertStickerImagesModel>();
        public ObservableCollection<MarkertStickerImagesModel> ImageList
        {
            get { return _ImageList; }
            set
            {
                if (_ImageList == value) return;
                _ImageList = value;
                this.OnPropertyChanged("ImageList");
            }
        }

        private long _LastUsedDate;
        public long LastUsedDate
        {
            get { return _LastUsedDate; }
            set
            {
                if (value == _LastUsedDate)
                    return;

                _LastUsedDate = value;
                this.OnPropertyChanged("LastUsedDate");
            }
        }

        private long _DownloadTime;
        public long DownloadTime
        {
            get { return _DownloadTime; }
            set
            {
                if (value == _DownloadTime)
                    return;

                _DownloadTime = value;
                this.OnPropertyChanged("DownloadTime");
            }
        }

        private double _CalculationValue;
        public double CalculationValue
        {
            get { return _CalculationValue; }
            set { _CalculationValue = value; }
        }

        private int _Rank;
        public int Rank
        {
            get { return _Rank; }
            set
            {
                if (value == _Rank)
                    return;

                _Rank = value;
                this.OnPropertyChanged("Rank");
            }
        }

        private bool _IsSelected = false;
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                if (value == _IsSelected)
                    return;

                _IsSelected = value;
                this.OnPropertyChanged("IsSelected");
            }
        }

        private bool _IsSelectedInComment = false;
        public bool IsSelectedInComment
        {
            get { return _IsSelectedInComment; }
            set
            {
                if (value == _IsSelectedInComment)
                    return;

                _IsSelectedInComment = value;
                this.OnPropertyChanged("IsSelectedInComment");
            }
        }

        //public bool _IsTop = false;
        //public bool IsTop
        //{
        //    get { return _IsTop; }
        //    set
        //    {
        //        if (value == _IsTop)
        //            return;

        //        _IsTop = value;
        //        this.OnPropertyChanged("IsTop");
        //    }
        //}

        #endregion

        #region Utility

        public void UpdateData(MarketStickerCategoryDTO categoryDTO)
        {
            if (categoryDTO != null)
            {
                this.StickerCategoryID = categoryDTO.sCtId;
                this.StickerCollectionID = categoryDTO.sClId;
                this.StickerCategoryName = categoryDTO.sctName;
                this.CategoryBannerImage = categoryDTO.cgBnrImg;
                this.Rank = categoryDTO.Rnk;
                this.DetailImage = categoryDTO.DetailImg;
                this.ThumbDetailImage = categoryDTO.ThumbDetailImage;
                this.IsNew = categoryDTO.cgNw;
                this.Icon = categoryDTO.Icon;
                //this.Prize = categoryDTO.Prz;
                this.IsFree = categoryDTO.free;
                //this.IsTop = categoryDTO.IsTop;
                //this.Description = categoryDTO.Dcpn;
                this.Downloaded = categoryDTO.Downloaded;
                this.DownloadTime = categoryDTO.DownloadTime;
                //this.SortOrder = categoryDTO.SortOrder;
                //this.IsVisible = categoryDTO.IsVisible;
                this.IsDefault = categoryDTO.IsDefault;
                this.LastUsedDate = categoryDTO.LastUsedDate;
                this.IsNewStickerSeen = categoryDTO.IsNewStickerSeen;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }

    public class MarketStickerCollectionModel : INotifyPropertyChanged
    {
        #region Constructor

        public MarketStickerCollectionModel(MarketStickerCollectionDTO collectionDTO)
        {
            UpdateData(collectionDTO);
        }

        #endregion

        #region Property

        private string _StickerCollectionName;
        public string StickerCollectionName
        {
            get { return _StickerCollectionName; }
            set
            {
                if (value == _StickerCollectionName)
                    return;

                _StickerCollectionName = value;
                this.OnPropertyChanged("StickerCollectionName");
            }
        }

        private int _StickerCollectionID;
        public int StickerCollectionID
        {
            get { return _StickerCollectionID; }
            set
            {
                if (value == _StickerCollectionID)
                    return;

                _StickerCollectionID = value;
                this.OnPropertyChanged("StickerCollectionID");
            }
        }

        private string _CategoryBannerImage;
        public string CategoryBannerImage
        {
            get { return _CategoryBannerImage; }
            set
            {
                if (value == _CategoryBannerImage)
                    return;

                _CategoryBannerImage = value;
                this.OnPropertyChanged("CategoryBannerImage");
            }
        }

        private ObservableCollection<MarketStickerCategoryModel> _CategoryList = new ObservableCollection<MarketStickerCategoryModel>();
        public ObservableCollection<MarketStickerCategoryModel> CategoryList
        {
            get { return _CategoryList; }
            set
            {
                if (value == _CategoryList)
                    return;

                _CategoryList = value;
                this.OnPropertyChanged("CategoryList");
            }
        }

        #endregion

        #region Utility

        public void UpdateData(MarketStickerCollectionDTO collectionDTO)
        {
            this.StickerCollectionID = collectionDTO.sClId;
            this.StickerCollectionName = collectionDTO.name;
            this.CategoryBannerImage = collectionDTO.bnrImg;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion


    }

    public class MarkertStickerImagesModel : INotifyPropertyChanged
    {
        #region Constructor
        public MarkertStickerImagesModel(MarkertStickerImagesDTO ImgDto)
        {
            UpdateData(ImgDto);
        }
        #endregion

        #region Property

        private int _ImageID;
        public int ImageID
        {
            get { return _ImageID; }
            set
            {
                if (value == _ImageID)
                    return;

                _ImageID = value;
                this.OnPropertyChanged("ImageID");
            }
        }

        private int _StickerCategoryID;
        public int StickerCategoryID
        {
            get { return _StickerCategoryID; }
            set
            {
                if (value == _StickerCategoryID)
                    return;

                _StickerCategoryID = value;
                this.OnPropertyChanged("StickerCategoryID");
            }
        }

        private int _StickerCollectionID;
        public int StickerCollectionID
        {
            get { return _StickerCollectionID; }
            set
            {
                if (value == _StickerCollectionID)
                    return;

                _StickerCollectionID = value;
                this.OnPropertyChanged("StickerCollectionID");
            }
        }

        private string _ImageUrl;
        public string ImageUrl
        {
            get { return _ImageUrl; }
            set
            {
                if (value == _ImageUrl)
                    return;

                _ImageUrl = value;
                this.OnPropertyChanged("ImageUrl");
            }
        }

        public bool _IsRecent;
        public bool IsRecent
        {
            get { return _IsRecent; }
            set
            {
                _IsRecent = value;
                this.OnPropertyChanged("IsRecent");
            }
        }

        private bool _SingleStickerImageFocusable = false;
        public bool SingleStickerImageFocusable
        {
            get
            {
                return _SingleStickerImageFocusable;
            }

            set
            {
                _SingleStickerImageFocusable = value;
                OnPropertyChanged("SingleStickerImageFocusable");
            }
        }

        #endregion

        #region Utility

        public void UpdateData(MarkertStickerImagesDTO imgDTO)
        {
            this.ImageID = imgDTO.imId;
            this.StickerCategoryID = imgDTO.sCtId;
            this.StickerCollectionID = imgDTO.sClId;
            this.ImageUrl = imgDTO.imUrl;
            this.IsRecent = imgDTO.IsRecent;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public override string ToString()
        {
            return this.StickerCollectionID.ToString() + Path.AltDirectorySeparatorChar + this.StickerCategoryID.ToString() + Path.AltDirectorySeparatorChar + this.ImageUrl;
        }

        #endregion

    }

    public class MarketStickerLanguageModel : INotifyPropertyChanged
    {
        #region Constructor

        public MarketStickerLanguageModel(MarketStickerLanguageyDTO cntryDTO)
        {
            UpdateData(cntryDTO);
        }

        #endregion

        #region Property

        //private int _CountryId;
        //public int CountryId
        //{
        //    get { return _CountryId; }
        //    set
        //    {
        //        if (value == _CountryId)
        //            return;

        //        _CountryId = value;
        //        this.OnPropertyChanged("CountryId");
        //    }
        //}        

        //private string _CountryCode;
        //public string CountryCode
        //{
        //    get { return _CountryCode; }
        //    set
        //    {
        //        if (value == _CountryCode)
        //            return;

        //        _CountryCode = value;
        //        this.OnPropertyChanged("CountryCode");
        //    }
        //}

        //private string _CountrySymbol;
        //public string CountrySymbol
        //{
        //    get { return _CountrySymbol; }
        //    set
        //    {
        //        if (value == _CountrySymbol)
        //            return;

        //        _CountrySymbol = value;
        //        this.OnPropertyChanged("CountrySymbol");
        //    }
        //}

        private string _CountryName;
        public string CountryName
        {
            get { return _CountryName; }
            set
            {
                if (value == _CountryName)
                    return;

                _CountryName = value;
                this.OnPropertyChanged("CountryName");
            }
        }

        private ObservableCollection<MarketStickerCategoryModel> _CategoryList = new ObservableCollection<MarketStickerCategoryModel>();
        public ObservableCollection<MarketStickerCategoryModel> CategoryList
        {
            get { return _CategoryList; }
            set
            {
                if (value == _CategoryList)
                    return;

                _CategoryList = value;
                this.OnPropertyChanged("CategoryList");
            }
        }

        #endregion

        #region Utility

        public void UpdateData(MarketStickerLanguageyDTO cntryDTO)
        {
            //this.CountryId = cntryDTO.id;
            this.CountryName = cntryDTO.name;
            //this.CountryCode = cntryDTO.code;
            //this.CountrySymbol = cntryDTO.countrySymbol;
            //this.CountryName = cntryDTO.name;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

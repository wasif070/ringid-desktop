﻿using Models.Constants;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Threading;
using View.ViewModel;
using System.Linq;

namespace View.BindingModels
{
    public class FeedHolderModel : INotifyPropertyChanged
    {
        public FeedHolderModel(short feedType)
        {
            this.FeedType = feedType;
        }
        private FeedModel _Feed;
        public FeedModel Feed
        {
            get { return _Feed; }
            set
            {
                if (value == _Feed)
                    return;
                _Feed = value;
                this.OnPropertyChanged("Feed");
            }
        }
        private bool _ShortModel = true;
        public bool ShortModel
        {
            get { return _ShortModel; }
            set
            {
                if (value == _ShortModel)
                    return;
                _ShortModel = value;
                this.OnPropertyChanged("ShortModel");
            }
        }
        private int _ShortModelType = SettingsConstants.FEED_BLANK_VIEW;
        public int ShortModelType
        {
            get { return _ShortModelType; }
            set
            {
                if (value == _ShortModelType)
                    return;
<<<<<<< HEAD
                if (value == SettingsConstants.FEED_FULL_VIEW) 
                    LoadView(true);
                else if (value == SettingsConstants.FEED_PARTIAL_VIEW) 
                    LoadView(false);
=======
                //if (value == SettingsConstants.FEED_FULL_VIEW)
                //    LoadView(true);
                //else if (value == SettingsConstants.FEED_PARTIAL_VIEW)
                //    LoadView(false);
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
                _ShortModelType = value;
                this.OnPropertyChanged("ShortModelType");
            }
        }
        private string _ExtraSharerCount = string.Empty; //after 2, if 2nd is null
        public string ExtraSharerCount
        {
            get { return _ExtraSharerCount; }
            set
            {
                if (value == _ExtraSharerCount)
                    return;
                _ExtraSharerCount = value;
                this.OnPropertyChanged("ExtraSharerCount");
            }
        }
        private BaseUserProfileModel _FirstSharer;
        public BaseUserProfileModel FirstSharer
        {
            get { return _FirstSharer; }
            set
            {
                if (value == _FirstSharer)
                    return;
                _FirstSharer = value;
                this.OnPropertyChanged("FirstSharer");
            }
        }
        private BaseUserProfileModel _SecondSharer;
        public BaseUserProfileModel SecondSharer
        {
            get { return _SecondSharer; }
            set
            {
                if (value == _SecondSharer)
                    return;
                _SecondSharer = value;
                this.OnPropertyChanged("SecondSharer");
            }
        }
        private int _SequenceForAllFeeds;
        public int SequenceForAllFeeds
        {
            get { return _SequenceForAllFeeds; }
            set
            {
                if (value == _SequenceForAllFeeds)
                    return;
                _SequenceForAllFeeds = value;
                this.OnPropertyChanged("SequenceForAllFeeds");
            }
        }
        private int _FeedPanelType = 0;
        public int FeedPanelType
        {
            get { return _FeedPanelType; }
            set
            {
                if (value == _FeedPanelType)
                    return;
                _FeedPanelType = value;
                this.OnPropertyChanged("FeedPanelType");
            }
        }
        private int _EstimatedHeight = 500;
        public int EstimatedHeight
        {
            get { return _EstimatedHeight; }
            set
            {
                if (value == _EstimatedHeight)
                    return;
                _EstimatedHeight = value;
                this.OnPropertyChanged("EstimatedHeight");
            }
        }
        //private string _ShortModelName;
        //public string ShortModelName
        //{
        //    get { return _ShortModelName; }
        //    set
        //    {
        //        if (value == _ShortModelName)
        //            return;
        //        _ShortModelName = value;
        //        this.OnPropertyChanged("ShortModelName");
        //    }
        //}
        public Guid FeedId;
        public bool BottomLoad = true;
        //public long FeedId
        //{
        //    get { return _FeedId; }
        //    set
        //    {
        //        if (value == _FeedId)
        //            return;
        //        _FeedId = value;
        //        this.OnPropertyChanged("FeedId");
        //    }
        //}
        private short _FeedType;
        //FeedType -1 storagefeed//0 for newstatus//1 for loader added but loaderpanel invisible// 2 for feed// 3 for loadergif visible nomoretxtpnl inv
        //4= nomoretxtpnl visible, loadergif invisible//5= reload btn visible
        public short FeedType
        {
            get { return _FeedType; }
            set
            {
                if (value == _FeedType)
                    return;
                _FeedType = value;
                this.OnPropertyChanged("FeedType");
            }
        }
        public void SelectViewTemplate()
        {
            if (this.Feed.ParentFeed != null && this.Feed.ParentFeed.WhoShareList != null && this.Feed.ParentFeed.WhoShareList.Count > 1)
            {
                this.FeedPanelType = 20;
                this.FirstSharer = this.Feed.ParentFeed.WhoShareList[0].Feed.PostOwner;
                if (this.Feed.ParentFeed.WhoShareList.Count == 2)
                    this.SecondSharer = this.Feed.ParentFeed.WhoShareList[1].Feed.PostOwner;
                else
                    this.ExtraSharerCount = (this.Feed.ParentFeed.LikeCommentShare.NumberOfShares - 1) + " others";
                //
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    FeedHolderModel prevExtModel = NewsFeedViewModel.Instance.AllFeedsViewCollection.Where(P => (P.Feed != null && P.Feed.ParentFeed != null && P.Feed.ParentFeed.NewsfeedId == this.Feed.ParentFeed.NewsfeedId)).FirstOrDefault();
                    if (prevExtModel != null)
                    {
                        NewsFeedViewModel.Instance.AllFeedsViewCollection.Remove(prevExtModel);
                        NewsFeedViewModel.Instance.CurrentFeedIds.Remove(prevExtModel.FeedId);
                    }
                }, DispatcherPriority.Send);
            }
            else
            {
                this.FeedPanelType = this.Feed.FeedPanelType;
            }
        }
        public void LoadView(bool full)
        {
<<<<<<< HEAD
            if (this.Feed != null && this.Feed.FeedImageList != null && this.Feed.FeedImageList.Count > 0)
                foreach (var item in this.Feed.FeedImageList)
                {
                    item.IsOpenedInFeed = full;
                }
=======
            if (this.Feed != null)
            {
                //if (Feed.Status.Equals("share parent image"))
                //{
                //    bool current = Feed.ParentFeed.FeedImageList[0].IsOpenedInFeed;
                //    bool next = full;
                //}
                if (this.Feed.FeedImageList != null && this.Feed.FeedImageList.Count > 0)
                    foreach (var item in this.Feed.FeedImageList)
                    {
                        item.IsOpenedInFeed = full;
                    }
                if (this.Feed.ParentFeed != null && this.Feed.ParentFeed.FeedImageList != null && this.Feed.ParentFeed.FeedImageList.Count > 0)
                    foreach (var item in this.Feed.ParentFeed.FeedImageList)
                    {
                        item.IsOpenedInFeed = full;
                    }
            }
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

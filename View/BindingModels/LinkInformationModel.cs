﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace View.BindingModels
{
    public class LinkInformationModel : INotifyPropertyChanged
    {
        public LinkInformationModel()
        {
            CurrentInstance = this;
        }
        private string _PreviewImgUrl = "";
        public string PreviewImgUrl
        {
            get { return _PreviewImgUrl; }
            set
            {
                if (value == _PreviewImgUrl)
                    return;
                _PreviewImgUrl = value;
                this.OnPropertyChanged("PreviewImgUrl");
            }
        }
        private string _PreviewDesc = "";
        public string PreviewDesc
        {
            get { return _PreviewDesc; }
            set
            {
                if (value == _PreviewDesc)
                    return;
                _PreviewDesc = value;
                this.OnPropertyChanged("PreviewDesc");
            }
        }
        private string _PreviewDomain = "";
        public string PreviewDomain
        {
            get { return _PreviewDomain; }
            set
            {
                if (value == _PreviewDomain)
                    return;
                _PreviewDomain = value;
                this.OnPropertyChanged("PreviewDomain");
            }
        }
        private string _PreviewUrl = "";
        public string PreviewUrl
        {
            get { return _PreviewUrl; }
            set
            {
                if (value == _PreviewUrl)
                    return;
                _PreviewUrl = value;
                this.OnPropertyChanged("PreviewUrl");
            }
        }
        private string _PreviewTitle = "";
        public string PreviewTitle
        {
            get { return _PreviewTitle; }
            set
            {
                if (value == _PreviewTitle)
                    return;
                _PreviewTitle = value;
                this.OnPropertyChanged("PreviewTitle");
            }
        }
        private int _PreviewLinkType;
        public int PreviewLinkType
        {
            get { return _PreviewLinkType; }
            set
            {
                if (value == _PreviewLinkType)
                    return;
                _PreviewLinkType = value;
                this.OnPropertyChanged("PreviewLinkType");
            }
        }
        private LinkInformationModel _CurrentInstance;
        public LinkInformationModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                if (value == _CurrentInstance)
                    return;
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

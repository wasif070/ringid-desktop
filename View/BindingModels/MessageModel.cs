﻿using imsdkwrapper;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using View.Utility;
using View.Utility.Chat;
using View.ViewModel;

namespace View.BindingModels
{
    public class MessageModel : INotifyPropertyChanged
    {
        private bool _ForOnlyChatView = false;

        #region Constructor

        public MessageModel(bool forOnlyChatView = false)
        {
            this._ForOnlyChatView = forOnlyChatView;
            this._CurrentInstance = this;
        }

        public MessageModel(MessageDTO messageDTO, bool forOnlyChatView = false)
        {
            this._ForOnlyChatView = forOnlyChatView;
            this._CurrentInstance = this;
            this.LoadData(messageDTO);
        }

        #endregion Constructor

        #region Property

        private MessageModel _CurrentInstance;
        public MessageModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }

        private MessageViewModel _ViewModel = new MessageViewModel();
        public MessageViewModel ViewModel
        {
            get { return _ViewModel; }
        }

        private LastStatusModel _LastStatus;
        public LastStatusModel LastStatus
        {
            get { return _LastStatus; }
            set
            {
                if (value == _LastStatus)
                    return;

                _LastStatus = value;
                this.OnPropertyChanged("LastStatus");
            }
        }

        private ObservableDictionary<long, GroupMemberInfoModel> _SeenByList = new ObservableDictionary<long, GroupMemberInfoModel>();
        public ObservableDictionary<long, GroupMemberInfoModel> SeenByList
        {
            get { return _SeenByList; }
            set
            {
                if (value == _SeenByList)
                    return;

                _SeenByList = value;
                this.OnPropertyChanged("SeenByList");
            }
        }

        private ObservableCollection<RoomMemberModel> _LikeMemberList = new ObservableCollection<RoomMemberModel>();
        public ObservableCollection<RoomMemberModel> LikeMemberList
        {
            get { return _LikeMemberList; }
            set
            {
                if (value == _LikeMemberList)
                    return;

                _LikeMemberList = value;
                this.OnPropertyChanged("LikeMemberList");
            }
        }

        private long _MessageID;
        public long MessageID
        {
            get { return _MessageID; }
            set
            {
                if (value == _MessageID)
                    return;

                _MessageID = value;
                this.OnPropertyChanged("MessageID");
            }
        }

        private int _PacketType;
        public int PacketType
        {
            get { return _PacketType; }
            set
            {
                if (value == _PacketType)
                    return;

                _PacketType = value;
                this.OnPropertyChanged("PacketType");
            }
        }

        private string _PacketID;
        public string PacketID
        {
            get { return _PacketID; }
            set
            {
                if (value == _PacketID)
                    return;

                _PacketID = value;
                this.OnPropertyChanged("PacketID");
            }
        }

        private long _SenderTableID;
        public long SenderTableID
        {
            get { return _SenderTableID; }
            set
            {
                if (value == _SenderTableID)
                    return;

                _SenderTableID = value;
                this.OnPropertyChanged("SenderTableID");
            }
        }

        private string _FullName;
        public string FullName
        {
            get { return _FullName; }
            set
            {
                if (value == _FullName)
                    return;

                _FullName = value;
                this.OnPropertyChanged("FullName");
            }
        }

        private string _ProfileImage;
        public string ProfileImage
        {
            get { return _ProfileImage; }
            set
            {
                if (value == _ProfileImage)
                    return;

                _ProfileImage = value;
                this.OnPropertyChanged("ProfileImage");
            }
        }

        private long _FriendTableID;
        public long FriendTableID
        {
            get { return _FriendTableID; }
            set
            {
                if (value == _FriendTableID)
                    return;

                _FriendTableID = value;
                this.OnPropertyChanged("FriendTableID");
            }
        }

        private long _GroupID;
        public long GroupID
        {
            get { return _GroupID; }
            set
            {
                if (value == _GroupID)
                    return;

                _GroupID = value;
                this.OnPropertyChanged("GroupID");
            }
        }

        private string _RoomID;
        public string RoomID
        {
            get { return _RoomID; }
            set
            {
                if (value == _RoomID)
                    return;

                _RoomID = value;
                this.OnPropertyChanged("RoomID");
            }
        }

        private string _Message;
        public string Message
        {
            get { return _Message; }
            set
            {
                if (value == _Message)
                    return;

                _Message = value;
                this.OnPropertyChanged("Message");
            }
        }

        private string _OriginalMessage;
        public string OriginalMessage
        {
            get { return _OriginalMessage; }
            set
            {
                if (value == _OriginalMessage)
                    return;

                _OriginalMessage = value;
                this.OnPropertyChanged("OriginalMessage");
            }
        }

        private long _MessageDate;
        public long MessageDate
        {
            get { return _MessageDate; }
            set
            {
                if (value == _MessageDate)
                    return;

                _MessageDate = value;
                this.OnPropertyChanged("MessageDate");
            }
        }

        private long _MessageDelieverDate;
        public long MessageDelieverDate
        {
            get { return _MessageDelieverDate; }
            set
            {
                if (value == _MessageDelieverDate)
                    return;

                _MessageDelieverDate = value;
                this.OnPropertyChanged("MessageDelieverDate");
            }
        }

        private long _MessageSeenDate;
        public long MessageSeenDate
        {
            get { return _MessageSeenDate; }
            set
            {
                if (value == _MessageSeenDate)
                    return;

                _MessageSeenDate = value;
                this.OnPropertyChanged("MessageSeenDate");
            }
        }

        private long _MessageViewDate;
        public long MessageViewDate
        {
            get { return _MessageViewDate; }
            set
            {
                if (value == _MessageViewDate)
                    return;

                _MessageViewDate = value;
                this.OnPropertyChanged("MessageViewDate");
            }
        }

        private MessageType _MessageType;
        public MessageType MessageType
        {
            get { return _MessageType; }
            set
            {
                if (value == _MessageType)
                    return;

                _MessageType = value;
                this.OnPropertyChanged("MessageType");
            }
        }

        private int _LikeCount;
        public int LikeCount
        {
            get { return _LikeCount; }
            set
            {
                if (value == _LikeCount)
                    return;

                _LikeCount = value;
                this.OnPropertyChanged("LikeCount");
            }
        }

        private bool _ILike;
        public bool ILike
        {
            get { return _ILike; }
            set
            {
                if (value == _ILike)
                    return;

                _ILike = value;
                this.OnPropertyChanged("ILike");
            }
        }

        private bool _IReport;
        public bool IReport
        {
            get { return _IReport; }
            set
            {
                if (value == _IReport)
                    return;

                _IReport = value;
                this.OnPropertyChanged("IReport");
            }
        }

        private bool _IsUnread = false;
        public bool IsUnread
        {
            get { return _IsUnread; }
            set
            {
                if (value == _IsUnread)
                    return;

                _IsUnread = value;
                this.OnPropertyChanged("IsUnread");
            }
        }

        private int _Timeout;
        public int Timeout
        {
            get { return _Timeout; }
            set
            {
                if (value == _Timeout)
                    return;

                _Timeout = value;
                this.OnPropertyChanged("Timeout");
            }
        }

        private int _Presence;
        public int Presence
        {
            get { return _Presence; }
            set
            {
                if (value == _Presence)
                    return;

                _Presence = value;
                this.OnPropertyChanged("Presence");
            }
        }

        private int _Device;
        public int Device
        {
            get { return _Device; }
            set
            {
                if (value == _Device)
                    return;

                _Device = value;
                this.OnPropertyChanged("Device");
            }
        }

        private string _Caption;
        public string Caption
        {
            get { return _Caption; }
            set
            {
                if (value == _Caption)
                    return;

                _Caption = value;
                this.OnPropertyChanged("Caption");
            }
        }

        public string _LinkUrl;
        public string LinkUrl
        {
            get { return _LinkUrl; }
            set
            {
                if (value == _LinkUrl)
                    return;

                _LinkUrl = value;
                this.OnPropertyChanged("LinkUrl");
            }
        }

        private string _LinkTitle;
        public string LinkTitle
        {
            get { return _LinkTitle; }
            set
            {
                if (value == _LinkTitle)
                    return;

                _LinkTitle = value;
                this.OnPropertyChanged("LinkTitle");
            }
        }

        private string _LinkImageUrl;
        public string LinkImageUrl
        {
            get { return _LinkImageUrl; }
            set
            {
                if (value == _LinkImageUrl)
                    return;

                _LinkImageUrl = value;
                this.OnPropertyChanged("LinkImageUrl");
            }
        }

        private string _LinkDescription;
        public string LinkDescription
        {
            get { return _LinkDescription; }
            set
            {
                if (value == _LinkDescription)
                    return;

                _LinkDescription = value;
                this.OnPropertyChanged("LinkDescription");
            }
        }

        private int _Width;
        public int Width
        {
            get { return _Width; }
            set
            {
                if (value == _Width)
                    return;

                _Width = value;
                this.OnPropertyChanged("Width");
            }
        }

        private int _Height;
        public int Height
        {
            get { return _Height; }
            set
            {
                if (value == _Height)
                    return;

                _Height = value;
                this.OnPropertyChanged("Height");
            }
        }

        private int _Duration;
        public int Duration
        {
            get { return _Duration; }
            set
            {
                if (value == _Duration)
                    return;

                _Duration = value;
                this.OnPropertyChanged("Duration");
            }
        }

        private long _FileSize;
        public long FileSize
        {
            get { return _FileSize; }
            set
            {
                if (value == _FileSize)
                    return;

                _FileSize = value;
                this.OnPropertyChanged("FileSize");
            }
        }

        private long _FileID;
        public long FileID
        {
            get { return _FileID; }
            set
            {
                if (value == _FileID)
                    return;

                _FileID = value;
                this.OnPropertyChanged("FileID");
            }
        }

        private int _FileStatus;
        public int FileStatus
        {
            get { return _FileStatus; }
            set
            {
                if (value == _FileStatus)
                    return;

                _FileStatus = value;
                this.OnPropertyChanged("FileStatus");
            }
        }

        private string _FileMenifest;
        public string FileMenifest
        {
            get { return _FileMenifest; }
            set
            {
                if (value == _FileMenifest)
                    return;

                _FileMenifest = value;
                this.OnPropertyChanged("FileMenifest");
            }
        }

        private int _MediaType;
        public int MediaType
        {
            get { return _MediaType; }
            set
            {
                if (value == _MediaType)
                    return;

                _MediaType = value;
                this.OnPropertyChanged("MediaType");
            }
        }

        private string _MediaTitle;
        public string MediaTitle
        {
            get { return _MediaTitle; }
            set
            {
                if (value == _MediaTitle)
                    return;

                _MediaTitle = value;
                this.OnPropertyChanged("MediaTitle");
            }
        }

        private string _MediaAlbum;
        public string MediaAlbum
        {
            get { return _MediaAlbum; }
            set
            {
                if (value == _MediaAlbum)
                    return;

                _MediaAlbum = value;
                this.OnPropertyChanged("MediaAlbum");
            }
        }

        private string _MediaArtist;
        public string MediaArtist
        {
            get { return _MediaArtist; }
            set
            {
                if (value == _MediaArtist)
                    return;

                _MediaArtist = value;
                this.OnPropertyChanged("MediaArtist");
            }
        }

        private float _Latitude;
        public float Latitude
        {
            get { return _Latitude; }
            private set
            {
                if (value == _Latitude)
                    return;

                _Latitude = value;
                this.OnPropertyChanged("Latitude");
            }
        }

        private float _Longitude;
        public float Longitude
        {
            get { return _Longitude; }
            private set
            {
                if (value == _Longitude)
                    return;

                _Longitude = value;
                this.OnPropertyChanged("Longitude");
            }
        }

        private UserBasicInfoModel _SharedContact;
        public UserBasicInfoModel SharedContact
        {
            get { return _SharedContact; }
            private set
            {
                if (value == _SharedContact)
                    return;

                _SharedContact = value;
                this.OnPropertyChanged("SharedContact");
            }
        }

        private int _OffsetPosition;
        public int OffsetPosition
        {
            get { return _OffsetPosition; }
            set
            {
                if (value == _OffsetPosition)
                    return;

                _OffsetPosition = value;
                this.OnPropertyChanged("OffsetPosition");
            }
        }

        private int _IsSecretVisible;
        public int IsSecretVisible
        {
            get { return _IsSecretVisible; }
            set
            {
                if (value == _IsSecretVisible)
                    return;

                _IsSecretVisible = value;
                this.OnPropertyChanged("IsSecretVisible");
            }
        }

        private UserShortInfoModel _FriendInfoModel;
        public UserShortInfoModel FriendInfoModel
        {
            get { return _FriendInfoModel; }
            set
            {
                if (value == _FriendInfoModel)
                    return;

                _FriendInfoModel = value;
                this.OnPropertyChanged("FriendInfoModel");
            }
        }

        private int _Status;
        public int Status
        {
            get { return _Status; }
            set
            {
                if (value == _Status)
                    return;

                _Status = value;
                this.OnPropertyChanged("Status");
                if (_IsMessageOpened)
                {
                    this.OnChatOpened();
                }
            }
        }

        private MessageType _PrevMessageType;
        public MessageType PrevMessageType
        {
            get { return _PrevMessageType; }
            set
            {
                if (value == _PrevMessageType)
                    return;

                _PrevMessageType = value;
                this.OnPropertyChanged("PrevMessageType");
            }
        }

        private bool _IsSecretChat;
        public bool IsSecretChat
        {
            get { return _IsSecretChat; }
            set
            {
                if (value == _IsSecretChat)
                    return;

                _IsSecretChat = value;
                this.OnPropertyChanged("IsSecretChat");
            }
        }

        private Int32 _Index;
        public Int32 Index
        {
            get { return _Index; }
            set
            {
                if (value == _Index)
                    return;

                _Index = value;
                this.OnPropertyChanged("Index");
            }
        }

        private bool _FromFriend;
        public bool FromFriend
        {
            get { return _FromFriend; }
            set
            {
                if (value == _FromFriend)
                    return;

                _FromFriend = value;
                this.OnPropertyChanged("FromFriend");
            }
        }

        private bool _IsMessageOpened;
        public bool IsMessageOpened
        {
            get { return _IsMessageOpened; }
            set
            {
                if (value == _IsMessageOpened)
                    return;

                _IsMessageOpened = value;
                this.OnPropertyChanged("IsMessageOpened");
                if (_IsMessageOpened)
                {
                    ImageDictionaries.Instance.CHAT_DOWNLOAD_PROCESSOR_QUEUE.AddItem(new ChatDownloadProcessor(this, true));
                }
            }
        }

        private bool _IsPreviewOpened;
        public bool IsPreviewOpened
        {
            get { return _IsPreviewOpened; }
            set
            {
                if (value == _IsPreviewOpened)
                    return;

                _IsPreviewOpened = value;
                this.OnPropertyChanged("IsPreviewOpened");
            }
        }

        private bool _IsFileOpened;
        public bool IsFileOpened
        {
            get { return _IsFileOpened; }
            set
            {
                if (value == _IsFileOpened)
                    return;

                _IsFileOpened = value;
                this.OnPropertyChanged("IsFileOpened");
            }
        }

        private bool _IsDownloadRunning;
        public bool IsDownloadRunning
        {
            get { return _IsDownloadRunning; }
            set
            {
                if (value == _IsDownloadRunning)
                    return;

                _IsDownloadRunning = value;
                this.OnPropertyChanged("IsDownloadRunning");
            }
        }

        private int _MultiMediaState;
        public int MultiMediaState
        {
            get { return _MultiMediaState; }
            set
            {
                if (value == _MultiMediaState)
                    return;

                _MultiMediaState = value;
                this.OnPropertyChanged("MultiMediaState");
                this.OnAudioPlayStateChange();
            }
        }

        private StreamMessageModel _StreamMsgModel;
        public StreamMessageModel StreamMsgModel
        {
            get { return _StreamMsgModel; }
            set
            {
                if (value == _StreamMsgModel) return;
                _StreamMsgModel = value;
                this.OnPropertyChanged("StreamMsgModel");
            }
        }

        #endregion Property

        #region Utility Method

        public void LoadData(MessageDTO messageDTO)
        {
            this.MessageID = messageDTO.MessageID;
            this.PacketType = messageDTO.PacketType;
            this.PacketID = messageDTO.PacketID;
            this.SenderTableID = messageDTO.SenderTableID;
            this.FullName = messageDTO.FullName;
            this.FriendTableID = messageDTO.FriendTableID;
            this.GroupID = messageDTO.GroupID;
            this.RoomID = messageDTO.RoomID;
            this.OriginalMessage = messageDTO.OriginalMessage;
            this.Message = messageDTO.Message;
            this.MessageDate = messageDTO.MessageDate;
            this.Timeout = messageDTO.Timeout;
            this.MessageDelieverDate = messageDTO.MessageDelieverDate;
            this.MessageSeenDate = messageDTO.MessageSeenDate;
            this.MessageViewDate = messageDTO.MessageViewDate;
            this.MessageType = (MessageType)messageDTO.MessageType;            
            this.PrevMessageType = (MessageType)messageDTO.MessageType;
            this.LikeCount = messageDTO.LikeCount;
            this.ILike = messageDTO.ILike;
            this.IReport = messageDTO.IReport;
            this.IsUnread = messageDTO.IsUnread;
            this.Presence = messageDTO.Presence;
            this.Device = messageDTO.Device;
            this.Caption = messageDTO.Caption;
            this.LinkUrl = messageDTO.LinkUrl;
            this.LinkTitle = messageDTO.LinkTitle;
            this.LinkImageUrl = messageDTO.LinkImageUrl;
            this.LinkDescription = messageDTO.LinkDescription;
            this.Width = messageDTO.Width;
            this.Height = messageDTO.Height;
            this.Duration = messageDTO.Duration;
            this.FileSize = messageDTO.FileSize;
            this.FileID = messageDTO.FileID;
            this.FileMenifest = messageDTO.FileMenifest;
            this.FileStatus = messageDTO.FileStatus;
            this.MediaType = messageDTO.MediaType;
            this.MediaTitle = messageDTO.MediaTitle;
            this.MediaAlbum = messageDTO.MediaAlbum;
            this.MediaArtist = messageDTO.MediaArtist;
            this.MediaType = messageDTO.MediaType;
            this.MediaType = messageDTO.MediaType;
            this.Latitude = messageDTO.Latitude;
            this.Longitude = messageDTO.Longitude;
            if (messageDTO.SharedTableID > 0)
            {
                SharedContact = RingIDViewModel.Instance.GetUserBasicInfoModelFromServerNotNullable(messageDTO.SharedTableID, messageDTO.SharedRingID, messageDTO.SharedFullName, messageDTO.SharedProfileImage);
            }
            this.IsSecretVisible = messageDTO.IsSecretVisible;
            this.IsSecretChat = messageDTO.Timeout > 0;
            this.FullName = messageDTO.FullName;
            this.ProfileImage = messageDTO.ProfileImage;
            this.FromFriend = messageDTO.SenderTableID != DefaultSettings.LOGIN_TABLE_ID;

            if (this._ForOnlyChatView)
            {
                this.ViewModel.LoadData(this);
                this.Status = messageDTO.Status;
                this.ViewModel.LoadEvent(this);
            }
            else
            {
                this.Status = messageDTO.Status;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event AddingNewEventHandler ChatOpened;
        public void OnChatOpened()
        {
            AddingNewEventHandler handler = ChatOpened;
            if (handler != null)
            {
                handler(this, new AddingNewEventArgs(this));
            }
        }

        public event AddingNewEventHandler AudioPlayStateChange;
        public void OnAudioPlayStateChange()
        {
            AddingNewEventHandler handler = AudioPlayStateChange;
            if (handler != null)
            {
                handler(this, new AddingNewEventArgs(this));
            }
        }

        #endregion Utility Methods
    }

    public class StreamMessageModel : INotifyPropertyChanged
    {
        private int _MessageType;
        public int MessageType
        {
            get { return _MessageType; }
            set
            {
                if (value == _MessageType) return;
                _MessageType = value;
                this.OnPropertyChanged("MessageType");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

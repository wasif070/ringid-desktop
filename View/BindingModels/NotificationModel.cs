﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using View.ViewModel;

namespace View.BindingModels
{
    public class NotificationModel : INotifyPropertyChanged
    {
        #region Constructor

        //public NotificationModel()
        //{
        //    CurrentInstance = this;
        //}

        //public NotificationModel(NotificationDTO notificationDTO)
        //{
        //    CurrentInstance = this;
        //    LoadData(notificationDTO);
        //}

        //public NotificationModel(bool showPanelSeeMore)
        //{
        //    CurrentInstance = this;
        //    this.ShowPanelSeeMore = showPanelSeeMore;
        //}

        #endregion

        #region Property

        //private NotificationModel _CurrentInstance;
        //public NotificationModel CurrentInstance
        //{
        //    get { return _CurrentInstance; }
        //    set
        //    {
        //        if (value == _CurrentInstance)
        //            return;
        //        _CurrentInstance = value;
        //        this.OnPropertyChanged("CurrentInstance");
        //    }
        //}

        private Guid notificationID = Guid.Empty;
        public Guid NotificationID
        {
            get { return notificationID; }
            set {notificationID = value; this.OnPropertyChanged("NotificationID"); }
        }

        private List<Guid> deleteNotificationIDs = new List<Guid>();
        public List<Guid> DeleteNotificationIDs
        {
            get { return deleteNotificationIDs; }
            set { deleteNotificationIDs = value; this.OnPropertyChanged("DeleteNotificationIDs"); }
        }

        private long updateTime = 0;
        public long UpdateTime
        {
            get { return updateTime; }
            set { updateTime = value; this.OnPropertyChanged("UpdateTime"); }
        }

        private int notificationType = 0;
        public int NotificationType
        {
            get { return notificationType; }
            set { notificationType = value; this.OnPropertyChanged("NotificationType"); }
        }

        private int activityID = 0;
        public int ActivityID
        {
            get { return activityID; }
            set { activityID = value; this.OnPropertyChanged("ActivityID"); }
        }

        private int messageType = 0;
        public int MessageType
        {
            get { return messageType; }
            set { messageType = value; this.OnPropertyChanged("MessageType"); }
        }

        private long numberOfLikeOrComment = 0;
        public long NumberOfLikeOrComment
        {
            get { return numberOfLikeOrComment; }
            set { numberOfLikeOrComment = value; this.OnPropertyChanged("NumberOfLikeOrComment"); }
        }

        private Guid newsfeedID = Guid.Empty;
        public Guid NewsfeedID
        {
            get { return newsfeedID; }
            set { newsfeedID = value; this.OnPropertyChanged("NewsfeedID"); }
        }

        private Guid imageID = Guid.Empty;
        public Guid ImageID
        {
            get { return imageID; }
            set { imageID = value; this.OnPropertyChanged("ImageID"); }
        }

        private Guid commentID = Guid.Empty;
        public Guid CommentID
        {
            get { return commentID; }
            set { commentID = value; this.OnPropertyChanged("CommentID"); }
        }

        private bool isRead = false;
        public bool IsRead
        {
            get { return isRead; }
            set { isRead = value; this.OnPropertyChanged("IsRead"); }
        }

        //private int _TotalNotification = 0;
        //public int TotalNotification
        //{
        //    get { return _TotalNotification; }
        //    set
        //    {
        //        if (value == _TotalNotification) return;
        //        _TotalNotification = value; this.OnPropertyChanged("TotalNotification");
        //    }
        //}

        //private bool _ShowPanelSeeMore;
        //public bool ShowPanelSeeMore
        //{
        //    get { return _ShowPanelSeeMore; }
        //    set
        //    {
        //        if (value == _ShowPanelSeeMore) return;
        //        _ShowPanelSeeMore = value; this.OnPropertyChanged("ShowPanelSeeMore");
        //    }
        //}
        private bool isChecked = false;
        public bool IsChecked
        {
            get { return isChecked; }
            set { isChecked = value; this.OnPropertyChanged("IsChecked"); }
        }

        private UserShortInfoModel userShortInfoModel = new UserShortInfoModel();
        public UserShortInfoModel UserShortInfoModel
        {
            get { return userShortInfoModel; }
            set { userShortInfoModel = value; this.OnPropertyChanged("UserShortInfoModel"); }
        }
        #endregion


        #region Utility Methods

        //public void LoadData(NotificationDTO notificationDTO)
        //{
        //    if (notificationDTO != null)
        //    {
        //        this.NotificationID = notificationDTO.ID;
        //        this.DeleteNotificationIDs = new List<Guid>();
        //        this.DeleteNotificationIDs.AddRange(notificationDTO.PreviousIds);
        //        this.UserShortInfoModel = new UserShortInfoModel();
        //        this.UserShortInfoModel.FullName = notificationDTO.FriendName;
        //        this.UserShortInfoModel.UserTableID = notificationDTO.FriendTableID;
        //        this.UserShortInfoModel.UserIdentity = notificationDTO.FriendRingID;
        //        this.UserShortInfoModel.ProfileImage = notificationDTO.ImageUrl;
        //        this.UpdateTime = notificationDTO.UpdateTime;
        //        this.NotificationType = notificationDTO.NotificationType;
        //        this.ActivityID = notificationDTO.ActivityID;
        //        this.MessageType = notificationDTO.MessageType;
        //        this.NumberOfLikeOrComment = notificationDTO.NumberOfLikeOrComment;
        //        this.NewsfeedID = notificationDTO.NewsfeedID;
        //        this.ImageID = notificationDTO.ImageID;
        //        this.CommentID = notificationDTO.CommentID;
        //        this.IsRead = notificationDTO.IsRead;
        //        //this.ShowPanelSeeMore = false;
        //    }
        //}

        public static NotificationModel LoadDataFromDTO(NotificationDTO notificationDTO)
        {
            NotificationModel model = new NotificationModel();
            try
            {
                if (notificationDTO != null)
                {
                    model.NotificationID = notificationDTO.ID;
                    model.DeleteNotificationIDs = new List<Guid>();
                    model.DeleteNotificationIDs.AddRange(notificationDTO.PreviousIds);
                    model.UserShortInfoModel = new UserShortInfoModel();
                    model.UserShortInfoModel.FullName = notificationDTO.FriendName;
                    model.UserShortInfoModel.UserTableID = notificationDTO.FriendTableID;
                    model.UserShortInfoModel.UserIdentity = notificationDTO.FriendRingID;
                    model.UserShortInfoModel.ProfileImage = notificationDTO.ImageUrl;
                    model.UpdateTime = notificationDTO.UpdateTime;
                    model.NotificationType = notificationDTO.NotificationType;
                    model.ActivityID = notificationDTO.ActivityID;
                    model.MessageType = notificationDTO.MessageType;
                    model.NumberOfLikeOrComment = notificationDTO.NumberOfLikeOrComment;
                    model.NewsfeedID = notificationDTO.NewsfeedID;
                    model.ImageID = notificationDTO.ImageID;
                    model.CommentID = notificationDTO.CommentID;
                    model.IsRead = notificationDTO.IsRead;
                }
                else
                {
                    model.NotificationID = Guid.Empty;
                }
            }
            catch (Exception)
            {
                model.NotificationID = Guid.Empty;
            }
            return model;
        }
        

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}

﻿using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace View.BindingModels
{
    public class WalletCoinBundleModel : INotifyPropertyChanged
    {
        private int bundleTypeID = 0;
        public int BundleTypeID
        {
            get { return bundleTypeID; }
            set { bundleTypeID = value; this.OnPropertyChanged("BundleTypeID"); }
        }

        private string bundleTypeName = string.Empty;
        public string BundleTypeName
        {
            get { return bundleTypeName; }
            set { bundleTypeName = value; this.OnPropertyChanged("BundleTypeName"); }
        }

        private int coinID = 0;
        public int CoinID
        {
            get { return coinID; }
            set { coinID = value; this.OnPropertyChanged("CoinID"); }
        }

        private string coinName = string.Empty;
        public string CoinName
        {
            get { return coinName; }
            set { coinName = value; this.OnPropertyChanged("CoinName"); }
        }

        private string currencyISOCode = string.Empty;
        public string CurrencyISOCode
        {
            get { return currencyISOCode; }
            set { currencyISOCode = value; this.OnPropertyChanged("CurrencyISOCode"); }
        }        

        private int networkID = 0;
        public int NetworkID
        {
            get { return networkID; }
            set { networkID = value; this.OnPropertyChanged("NetworkID"); }
        }

        private int coinBundleID = 0;
        public int CoinBundleID
        {
            get { return coinBundleID; }
            set { coinBundleID = value; this.OnPropertyChanged("CoinBundleID"); }
        }

        private int coinQuantity = 0;
        public int CoinQuantity
        {
            get { return coinQuantity; }
            set { coinQuantity = value; this.OnPropertyChanged("CoinQuantity"); }
        }

        private int freeCoinQuantity = 0;
        public int FreeCoinQuantity
        {
            get { return freeCoinQuantity; }
            set { freeCoinQuantity = value; this.OnPropertyChanged("FreeCoinQuantity"); }
        }

        private string customMessage = string.Empty;
        public string CustomMessage
        {
            get { return customMessage; }
            set { customMessage = value; this.OnPropertyChanged("CustomMessage"); }
        }

        private double price = 0.0;
        public double Price
        {
            get { return price; }
            set { price = value; this.OnPropertyChanged("Price"); }
        }        

        private bool active = false;
        public bool Active
        {
            get { return active; }
            set { active = value; this.OnPropertyChanged("Active"); }
        }

        private double exchangeRate = 0.0;
        public double ExchangeRate
        {
            get { return exchangeRate; }
            set { exchangeRate = value; this.OnPropertyChanged("ExchangeRate"); }
        }

        private bool isApplicableForFirstTime = false;
        public bool IsApplicableForFirstTime
        {
            get { return isApplicableForFirstTime; }
            set { isApplicableForFirstTime = value; this.OnPropertyChanged("IsApplicableForFirstTime"); }
        }

        #region key
        //public const string keyBundleTypeID = "bundleTypeId";
        //public const string keyBundleTypeName = "bundleTypeName";
        //public const string keyCoinID = "coinId";
        //public const string keyCoinName = "coinName";
        //public const string keyCurrencyISOCode = "currencyISOCode";
        //public const string keyNetworkId = "networkId";
        //public const string keyCoinBundleId = "coinBundleId";
        //public const string keyCoinQuantity = "coinQuantity";
        //public const string keyFreeCoinQuantity = "freeCoinQuantity";
        //public const string keyCustomMessage = "customMessage";
        //public const string keyPrice = "price";
        //public const string keyActive = "active";
        //public const string keyIsApplicableForFirstTime = "isApplicableForFirstTime";
        //public const string keyExchangeRate = "exchangeRate";
        //public const string keyCoinBundles = "coinBundle";
        #endregion

        public static WalletCoinBundleModel LoadDataFromJson(JObject jObject)
        {
            WalletCoinBundleModel model = new WalletCoinBundleModel();
            try
            {
                if (jObject[WalletConstants.RSP_KEY_BUNDLE_TYPE_ID] != null)
                {
                    model.BundleTypeID = (int)jObject[WalletConstants.RSP_KEY_BUNDLE_TYPE_ID];
                }
                if (jObject[WalletConstants.RSP_KEY_BUNDLE_TYPE_NAME] != null)
                {
                    model.BundleTypeName = (string)jObject[WalletConstants.RSP_KEY_BUNDLE_TYPE_NAME];
                }
                if (jObject[WalletConstants.RSP_KEY_COIN_ID] != null)
                {
                    model.CoinID = (int)jObject[WalletConstants.RSP_KEY_COIN_ID];
                }
                if (jObject[WalletConstants.RSP_KEY_COIN_NAME] != null)
                {
                    model.CoinName = (string)jObject[WalletConstants.RSP_KEY_COIN_NAME];
                }
                if (jObject[WalletConstants.RSP_KEY_CURRENCY_ISO_CODE] != null)
                {
                    model.CurrencyISOCode = (string)jObject[WalletConstants.RSP_KEY_CURRENCY_ISO_CODE];
                }
                if (jObject[WalletConstants.RSP_KEY_NETWORK_ID] != null)
                {
                    model.NetworkID = (int)jObject[WalletConstants.RSP_KEY_NETWORK_ID];
                }
                if (jObject[WalletConstants.RSP_KEY_COIN_BUNDLE_ID] != null)
                {
                    model.CoinBundleID = (int)jObject[WalletConstants.RSP_KEY_COIN_BUNDLE_ID];
                }
                if (jObject[WalletConstants.RSP_KEY_COIN_QUANTITY] != null)
                {
                    model.CoinQuantity = (int)jObject[WalletConstants.RSP_KEY_COIN_QUANTITY];
                }
                if (jObject[WalletConstants.RSP_KEY_FREE_COIN_QUANTITY] != null)
                {
                    model.FreeCoinQuantity = (int)jObject[WalletConstants.RSP_KEY_FREE_COIN_QUANTITY];
                }
                if (jObject[WalletConstants.RSP_KEY_CUSTOM_MESSAGE] != null)
                {
                    model.CustomMessage = jObject[WalletConstants.RSP_KEY_CUSTOM_MESSAGE].ToString();
                }
                if (jObject[WalletConstants.RSP_KEY_BUNDLE_PRICE] != null)
                {
                    model.Price = (double)jObject[WalletConstants.RSP_KEY_BUNDLE_PRICE];
                }
                if (jObject[WalletConstants.RSP_KEY_ACTIVE] != null)
                {
                    model.Active = (bool)jObject[WalletConstants.RSP_KEY_ACTIVE];
                }
                if (jObject[WalletConstants.RSP_KEY_IS_APPLICABLE_FOR_FIRST_TIME] != null)
                {
                    model.IsApplicableForFirstTime = (bool)jObject[WalletConstants.RSP_KEY_IS_APPLICABLE_FOR_FIRST_TIME];
                }
                if (jObject[WalletConstants.RSP_KEY_EXCHANGE_RATE] != null)
                {
                    model.ExchangeRate = (double)jObject[WalletConstants.RSP_KEY_EXCHANGE_RATE];
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Coin Bundle Parse Error ==> " + ex.Message + Environment.NewLine +  ex.StackTrace);
            }
            return model;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

﻿using Models.Constants;
using Models.Entity;
using Models.Utility;
using System;
using System.ComponentModel;
using System.Windows;

namespace View.BindingModels
{
    public class SingleBasicModelSet : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChangedNotify(string methodName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(methodName));
            }
        }
        public SingleBasicModelSet()
        {
            BirthDate = MarriageDate = DateTime.MinValue;
        }
        //public SingleBasicModelSet(long val)
        //{
        //    //BirthDate = MarriageDate = val;
        //}
        public void SetMySingleBasicSetToDefaultSettingsUserProfile()
        {
            FullName = DefaultSettings.userProfile.FullName;
            Gender = string.IsNullOrEmpty(DefaultSettings.userProfile.Gender) ? "" : DefaultSettings.userProfile.Gender;
            //Gender_EditMode = string.IsNullOrEmpty(DefaultSettings.userProfile.Gender) ? "Select a Gender" : DefaultSettings.userProfile.Gender;
            HomeCity = string.IsNullOrEmpty(DefaultSettings.userProfile.HomeCity) ? "" : DefaultSettings.userProfile.HomeCity;
            // HomeCity_EditMode = string.IsNullOrEmpty(DefaultSettings.userProfile.HomeCity) ? "" : DefaultSettings.userProfile.HomeCity;
            CurrentCity = string.IsNullOrEmpty(DefaultSettings.userProfile.CurrentCity) ? "" : DefaultSettings.userProfile.CurrentCity;
            //CurrentCity_EditMode = string.IsNullOrEmpty(DefaultSettings.userProfile.CurrentCity) ? "" : DefaultSettings.userProfile.CurrentCity;
            AboutMe = string.IsNullOrEmpty(DefaultSettings.userProfile.AboutMe) ? "" : DefaultSettings.userProfile.AboutMe;
            //AboutMe_EditMode = string.IsNullOrEmpty(DefaultSettings.userProfile.AboutMe) ? "" : DefaultSettings.userProfile.AboutMe;
            BirthDate = DefaultSettings.userProfile.BirthDate;
            MarriageDate = DefaultSettings.userProfile.MarriageDate;
            //BirthDay_EditMode = (DefaultSettings.userProfile.BirthDay == 1) ? "" : ModelUtility.DateTimeFromMillisSince1970(DefaultSettings.userProfile.BirthDay).ToShortDateString();
            //BirthDay_ViewMode = (DefaultSettings.userProfile.BirthDay == 1) ? "N/A" : ModelUtility.DateTimeFromMillisSince1970(DefaultSettings.userProfile.BirthDay).ToShortDateString();
            //MarriageDay_EditMode = (DefaultSettings.userProfile.MarriageDay == 1) ? "" : ModelUtility.DateTimeFromMillisSince1970(DefaultSettings.userProfile.MarriageDay).ToShortDateString();
            //MarriageDay_ViewMode = (DefaultSettings.userProfile.MarriageDay == 1) ? "N/A" : ModelUtility.DateTimeFromMillisSince1970(DefaultSettings.userProfile.MarriageDay).ToShortDateString();

            Email = DefaultSettings.userProfile.Email;
            MobilePhone = DefaultSettings.userProfile.MobileNumber;
            MobilePhoneDialingCode = DefaultSettings.userProfile.MobileDialingCode;
            EditModeVisibility = Visibility.Collapsed;
            ViewModeVisibility = Visibility.Visible;
            // MobileVerifiedVisibility = (DefaultSettings.userProfile.IsMobileNumberVerified == 1) ? Visibility.Visible : Visibility.Collapsed;
            //MailVerifiedVisibility = (DefaultSettings.userProfile.IsEmailVerified == 1) ? Visibility.Visible : Visibility.Collapsed;
            FacebookValidated = DefaultSettings.userProfile.FacebookValidated;
            TwitterValidated = DefaultSettings.userProfile.TwitterValidated;
        }

        public void LoadData(UserBasicInfoDTO user)
        {
            FullName = user.FullName;
            Gender = user.Gender;
            HomeCity = user.HomeCity;
            CurrentCity = user.CurrentCity;
            AboutMe = user.AboutMe;
            BirthDate = user.BirthDate;
            MarriageDate = user.MarriageDate;
            //BirthDay_ViewMode = (user.BirthDay == 1) ? "" : ModelUtility.DateTimeFromMillisSince1970(user.BirthDay).ToShortDateString();
            //MarriageDay_ViewMode = (user.MarriageDay == 1) ? "" : ModelUtility.DateTimeFromMillisSince1970(user.MarriageDay).ToShortDateString();

            Email = user.Email;
            MobilePhone = (string.IsNullOrEmpty(user.MobileNumber) || string.IsNullOrEmpty(user.MobileDialingCode)) ? "" : user.MobileDialingCode + "-" + user.MobileNumber;
        }

        private string _FullName;
        public string FullName
        {
            get { return _FullName; }
            set
            {
                if (value == _FullName)
                    return;

                _FullName = value;
                this.OnPropertyChangedNotify("FullName");
            }
        }

        private string _Gender;
        public string Gender
        {
            get { return _Gender; }
            set
            {
                if (value == _Gender)
                    return;

                _Gender = value;
                this.OnPropertyChangedNotify("Gender");
            }
        }

        private string _CurrentCity;
        public string CurrentCity
        {
            get { return _CurrentCity; }
            set
            {
                if (value == _CurrentCity)
                    return;

                _CurrentCity = value;
                this.OnPropertyChangedNotify("CurrentCity");
            }
        }

        private string _HomeCity;
        public string HomeCity
        {
            get { return _HomeCity; }
            set
            {
                if (value == _HomeCity)
                    return;

                _HomeCity = value;
                this.OnPropertyChangedNotify("HomeCity");
            }
        }

        private string _AboutMe;
        public string AboutMe
        {
            get { return _AboutMe; }
            set
            {
                if (value == _AboutMe)
                    return;

                _AboutMe = value;
                this.OnPropertyChangedNotify("AboutMe");
            }
        }

        private DateTime _BirthDate;
        public DateTime BirthDate
        {
            get { return _BirthDate; }
            set
            {
                if (value == _BirthDate)
                    return;

                _BirthDate = value;
                this.OnPropertyChangedNotify("BirthDate");
            }
        }

        private DateTime _MarriageDate;
        public DateTime MarriageDate
        {
            get { return _MarriageDate; }
            set
            {
                if (value == _MarriageDate)
                    return;

                _MarriageDate = value;
                this.OnPropertyChangedNotify("MarriageDate");
            }
        }

        private string _Email;
        public string Email
        {
            get { return _Email; }
            set
            {
                if (value == _Email)
                    return;

                _Email = value;
                this.OnPropertyChangedNotify("Email");
            }
        }

        private string _MobilePhone;
        public string MobilePhone
        {
            get { return _MobilePhone; }
            set
            {
                if (value == _MobilePhone)
                    return;

                _MobilePhone = value;
                this.OnPropertyChangedNotify("MobilePhone");
            }
        }

        private string _MobilePhoneDialingCode;
        public string MobilePhoneDialingCode
        {
            get { return _MobilePhoneDialingCode; }
            set
            {
                if (value == _MobilePhoneDialingCode)
                    return;

                _MobilePhoneDialingCode = value;
                this.OnPropertyChangedNotify("MobilePhoneDialingCode");
            }
        }

        private Visibility _EditModeVisibility = Visibility.Collapsed;
        public Visibility EditModeVisibility
        {
            get
            {
                return _EditModeVisibility;
            }
            set
            {
                if (value == _EditModeVisibility)
                    return;
                _EditModeVisibility = value;
                this.OnPropertyChangedNotify("EditModeVisibility");
            }
        }

        private Visibility _ViewModeVisibility = Visibility.Visible;
        public Visibility ViewModeVisibility
        {
            get
            {
                return _ViewModeVisibility;
            }
            set
            {
                if (value == _ViewModeVisibility)
                    return;
                _ViewModeVisibility = value;
                this.OnPropertyChangedNotify("ViewModeVisibility");
            }
        }

        private int _FacebookValidated = 0;
        public int FacebookValidated { get { return _FacebookValidated; } set { _FacebookValidated = value; this.OnPropertyChangedNotify("FacebookValidated"); } }

        private int _TwitterValidated = 0;
        public int TwitterValidated { get { return _TwitterValidated; } set { _TwitterValidated = value; this.OnPropertyChangedNotify("TwitterValidated"); } }

    }
}

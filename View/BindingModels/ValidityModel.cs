﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace View.BindingModels
{
    public class ValidityModel : INotifyPropertyChanged
    {
        private string _ValidityString;
        public string ValidityString
        {
            get { return _ValidityString; }
            set
            {
                if (value == _ValidityString)
                    return;
                _ValidityString = value;
                this.OnPropertyChanged("ValidityString");
            }
        }

        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (value == _isSelected)
                    return;
                _isSelected = value;
                this.OnPropertyChanged("IsSelected");
            }
        }

        public ValidityModel(string str, bool isSelected)
        {
            this.ValidityString = str;
            this.IsSelected = isSelected;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        
    }
}

﻿using Models.Entity;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace View.BindingModels
{
    public class ActivityModel : INotifyPropertyChanged
    {

        #region Constructor

        public ActivityModel()
        {
            _CurrentInstance = this;
        }

        public ActivityModel(ActivityDTO activityDTO)
        {
            _CurrentInstance = this;
            LoadData(activityDTO);
        }

        #endregion Constructor

        #region Property

        private ActivityModel _CurrentInstance;
        public ActivityModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }

        private ActivityMemberModel _ActivityByMemberModel;
        public ActivityMemberModel ActivityByMemberModel
        {
            get { return _ActivityByMemberModel; }
            set
            {
                _ActivityByMemberModel = value;
                this.OnPropertyChanged("ActivityByMemberModel");
            }
        }

        private long _ID;
        public long ID
        {
            get { return _ID; }
            set
            {
                if (value == _ID)
                    return;

                _ID = value;
                this.OnPropertyChanged("ID");
            }
        }

        private long _ActivityBy;
        public long ActivityBy
        {
            get { return _ActivityBy; }
            set
            {
                if (value == _ActivityBy)
                    return;

                _ActivityBy = value;
                this.OnPropertyChanged("ActivityBy");
            }
        }

        private long _FriendTableID;
        public long FriendTableID
        {
            get { return _FriendTableID; }
            set
            {
                if (value == _FriendTableID)
                    return;

                _FriendTableID = value;
                this.OnPropertyChanged("FriendTableID");
            }
        }

        private long _GroupID;
        public long GroupID
        {
            get { return _GroupID; }
            set
            {
                if (value == _GroupID)
                    return;

                _GroupID = value;
                this.OnPropertyChanged("GroupID");
            }
        }

        private int _ActivityType;
        public int ActivityType
        {
            get { return _ActivityType; }
            set
            {
                if (value == _ActivityType)
                    return;

                _ActivityType = value;
                this.OnPropertyChanged("ActivityType");
            }
        }

        private int _MessageType;
        public int MessageType
        {
            get { return _MessageType; }
            set
            {
                if (value == _MessageType)
                    return;

                _MessageType = value;
                this.OnPropertyChanged("MessageType");
            }
        }

        private long _UpdateTime;
        public long UpdateTime
        {
            get { return _UpdateTime; }
            set
            {
                if (value == _UpdateTime)
                    return;

                _UpdateTime = value;
                this.OnPropertyChanged("UpdateTime");
            }
        }

        private ObservableCollection<ActivityMemberModel> _MemberList = new ObservableCollection<ActivityMemberModel>();
        public ObservableCollection<ActivityMemberModel> MemberList
        {
            get { return _MemberList; }
            set
            {
                if (value == _MemberList)
                    return;

                _MemberList = value;
                this.OnPropertyChanged("MemberList");
            }
        }

        private string _GroupName;
        public string GroupName
        {
            get { return _GroupName; }
            set
            {
                if (value == _GroupName)
                    return;

                _GroupName = value;
                this.OnPropertyChanged("GroupName");
            }
        }

        private string _GroupProfileImage;
        public string GroupProfileImage
        {
            get { return _GroupProfileImage; }
            set
            {
                if (value == _GroupProfileImage)
                    return;

                _GroupProfileImage = value;
                this.OnPropertyChanged("GroupProfileImage");
            }
        }

        private string _PacketID;
        public string PacketID
        {
            get { return _PacketID; }
            set
            {
                if (value == _PacketID)
                    return;

                _PacketID = value;
                this.OnPropertyChanged("PacketID");
            }
        }

        private bool _IsUnread = false;
        public bool IsUnread
        {
            get { return _IsUnread; }
            set
            {
                if (value == _IsUnread)
                    return;

                _IsUnread = value;
                this.OnPropertyChanged("IsUnread");
            }
        }

        #endregion Property

        #region Utility Method

        public void LoadData(ActivityDTO activityDTO)
        {
            this.ID = activityDTO.ID;
            this.ActivityBy = activityDTO.ActivityBy;
            this.FriendTableID = activityDTO.FriendTableID;
            this.GroupID = activityDTO.GroupID;
            this.ActivityType = activityDTO.ActivityType;
            this.MessageType = activityDTO.MessageType;
            this.UpdateTime = activityDTO.UpdateTime;
            this.GroupName = activityDTO.GroupName;
            this.GroupProfileImage = activityDTO.GroupProfileImage;
            this.PacketID = activityDTO.PacketID;
            this.IsUnread = activityDTO.IsUnread;
            this.MemberList.Clear();
            if (activityDTO.MemberList != null && activityDTO.MemberList.Count > 0)
            {
                foreach (ActivityMemberDTO memberDTO in activityDTO.MemberList)
                {
                    this.MemberList.Add(new ActivityMemberModel(memberDTO));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods
    }

    public class ActivityMemberModel : INotifyPropertyChanged
    {

        #region Constructor

        public ActivityMemberModel()
        {
            _CurrentInstance = this;
        }

        public ActivityMemberModel(ActivityMemberDTO activityMemberDTO)
        {
            _CurrentInstance = this;
            LoadData(activityMemberDTO);
        }

        #endregion Constructor

        #region Property

        private ActivityMemberModel _CurrentInstance;
        public ActivityMemberModel CurrentInstance
        {
            get { return _CurrentInstance; }
        }

        private long _UserTableID;
        public long UserTableID
        {
            get { return _UserTableID; }
            set
            {
                if (value == _UserTableID)
                    return;

                _UserTableID = value;
                this.OnPropertyChanged("UserTableID");
            }
        }

        private long _RingID;
        public long RingID
        {
            get { return _RingID; }
            set
            {
                if (value == _RingID)
                    return;

                _RingID = value;
                this.OnPropertyChanged("RingID");
            }
        }

        private String _FullName;
        public String FullName
        {
            get { return _FullName; }
            set
            {
                if (value == _FullName)
                    return;

                _FullName = value;
                this.OnPropertyChanged("FullName");
            }
        }

        private int _MemberAccessType;
        public int MemberAccessType
        {
            get { return _MemberAccessType; }
            set
            {
                if (value == _MemberAccessType)
                    return;

                _MemberAccessType = value;
                this.OnPropertyChanged("MemberAccessType");
            }
        }

        #endregion Property

        #region Utility Method

        public void LoadData(ActivityMemberDTO activityMemberDTO)
        {
            this.UserTableID = activityMemberDTO.UID;
            this.RingID = activityMemberDTO.RID;
            this.MemberAccessType = activityMemberDTO.ACCESS;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods
    }
}

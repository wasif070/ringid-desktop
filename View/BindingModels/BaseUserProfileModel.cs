﻿using Models.Constants;
using Newtonsoft.Json.Linq;
using System.ComponentModel;

namespace View.BindingModels
{
    public class BaseUserProfileModel : INotifyPropertyChanged
    {
        public BaseUserProfileModel()
        {
            CurrentInstance = this;
        }
        public BaseUserProfileModel(JObject jObj)
        {
            FullName = (jObj[JsonKeys.FullName] != null) ? (string)jObj[JsonKeys.FullName] : string.Empty;
            UserTableID = (jObj[JsonKeys.UserTableID] != null) ? (long)jObj[JsonKeys.UserTableID] : 0;
            ProfileImage = (jObj[JsonKeys.ProfileImage] != null) ? (string)jObj[JsonKeys.ProfileImage] : string.Empty;
            ProfileType = (jObj[JsonKeys.ProfileType] != null) ? (int)jObj[JsonKeys.ProfileType] : 1;
            CurrentInstance = this;
        }
        private long _UserTableId;//=grpId for circle
        public long UserTableID
        {
            get { return _UserTableId; }
            set
            {
                if (value == _UserTableId)
                    return;

                _UserTableId = value;
                this.OnPropertyChanged("UserTableID");
            }
        }
        private int _ProfileType = 1; //userbasicinfomodel default
        public int ProfileType
        {
            get { return _ProfileType; }
            set
            {
                if (value == _ProfileType)
                    return;
                _ProfileType = value;
                this.OnPropertyChanged("ProfileType");
            }
        }
        private string _FullName;
        public string FullName
        {
            get { return _FullName; }
            set
            {
                if (value == _FullName)
                    return;

                _FullName = value;
                this.OnPropertyChanged("FullName");
            }
        }
        private string _ProfileImage;
        public string ProfileImage
        {
            get { return _ProfileImage; }
            set
            {
                if (value == _ProfileImage)
                    return;

                _ProfileImage = value;
                this.OnPropertyChanged("ProfileImage");
            }
        }
        private long _UserIdentity;
        public long UserIdentity
        {
            get { return _UserIdentity; }
            set
            {
                if (value == _UserIdentity)
                    return;

                _UserIdentity = value;
                this.OnPropertyChanged("UserIdentity");
            }
        }
        private bool _IsProfileHidden;
        public bool IsProfileHidden
        {
            get { return _IsProfileHidden; }
            set
            {
                if (value == _IsProfileHidden)
                    return;

                _IsProfileHidden = value;
                this.OnPropertyChanged("IsProfileHidden");
            }
        }
        private bool _IsSpecialContact;
        public bool IsSpecialContact
        {
            get { return _IsSpecialContact; }
            set
            {
                if (value == _IsSpecialContact)
                    return;

                _IsSpecialContact = value;
                this.OnPropertyChanged("IsSpecialContact");
            }
        }
        private BaseUserProfileModel _CurrentInstance;
        public BaseUserProfileModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                if (value == _CurrentInstance)
                    return;
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

﻿using System.ComponentModel;

namespace View.BindingModels
{
    public class RecoveryInfoModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private string _Info;
        public string Info
        {
            get
            {
                return _Info;
            }
            set
            {
                _Info = value;
                this.OnPropertyChanged("Info");
            }
        }
        private bool _IsInfoSelected = false;
        public bool IsInfoSelected
        {
            get
            {
                return _IsInfoSelected;
            }
            set
            {
                _IsInfoSelected = value;
                this.OnPropertyChanged("IsInfoSelected");
            }
        }
    }
}

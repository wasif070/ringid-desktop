﻿using callsdkwrapper;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using View.Utility;

namespace View.BindingModels
{
    public class StreamModel : INotifyPropertyChanged
    {

        public StreamModel() { }

        public StreamModel(StreamDTO strmDTO)
        {
            LoadData(strmDTO);
        }

        public void LoadData(StreamDTO streamingDTO)
        {
            this.StreamID = streamingDTO.StreamID;
            this.UserTableID = streamingDTO.UserTableID;
            this.UserName = streamingDTO.UserName;
            this.UserType = streamingDTO.UserType;
            this.ProfileImage = streamingDTO.ProfileImage;
            this.Longitude = streamingDTO.Longitude;
            this.Latitude = streamingDTO.Latitude;
            this.StartTime = streamingDTO.StartTime;
            this.EndTime = streamingDTO.EndTime;
            this.ViewCount = streamingDTO.ViewCount;
            this.LikeCount = streamingDTO.LikeCount;
            this.Title = streamingDTO.Title;
            this.GiftOn = streamingDTO.GiftOn;
            this.Country = streamingDTO.Country;
            this.DeviceCategory = streamingDTO.DeviceCategory;
            this.ChatServerIP = streamingDTO.ChatServerIP;
            this.ChatServerPort = streamingDTO.ChatServerPort;
            this.PublisherServerIP = streamingDTO.PublisherServerIP;
            this.PublisherServerPort = streamingDTO.PublisherServerPort;
            this.ViewerServerIP = streamingDTO.ViewerServerIP;
            this.ViewerServerPort = streamingDTO.ViewerServerPort;
            this.Distance = streamingDTO.Distance != null ? streamingDTO.Distance : this.Distance;

            Application.Current.Dispatcher.BeginInvoke(() =>
            {
                this.CategoryList.Clear();
                if (streamingDTO.CategoryList != null)
                {
                    foreach (StreamCategoryDTO streamCtgDTO in streamingDTO.CategoryList)
                    {
                        StreamCategoryModel catModel = new StreamCategoryModel();
                        catModel.CategoryID = streamCtgDTO.CategoryID;
                        catModel.CategoryName = streamCtgDTO.CategoryName;
                        this.CategoryList.Add(catModel);
                    }
                }
            }, DispatcherPriority.Send);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) { handler(this, new PropertyChangedEventArgs(propertyName)); }
        }

        #region Utility

        #endregion

        #region Property

        private long _UserTableID;
        public long UserTableID
        {
            get { return _UserTableID; }
            set
            {
                if (value == _UserTableID) return;
                _UserTableID = value;
                this.OnPropertyChanged("UserTableID");
            }
        }

        private string _UserName;
        public string UserName
        {
            get { return _UserName; }
            set
            {
                if (value == _UserName) return;
                _UserName = value;
                this.OnPropertyChanged("UserName");
            }
        }

        private int _UserType;
        public int UserType
        {
            get { return _UserType; }
            set
            {
                if (value == _UserType) return;
                _UserType = value;
                this.OnPropertyChanged("UserType");
            }
        }

        private string _ProfileImage;
        public string ProfileImage
        {
            get { return _ProfileImage; }
            set
            {
                if (value == _ProfileImage) return;
                _ProfileImage = value;
                this.OnPropertyChanged("ProfileImage");
            }
        }

        private float _Longitude;
        public float Longitude
        {
            get { return _Longitude; }
            set
            {
                if (value == _Longitude) return;
                _Longitude = value;
                this.OnPropertyChanged("Longitude");
            }
        }

        private float _Latitude;
        public float Latitude
        {
            get { return _Latitude; }
            set
            {
                if (value == _Latitude) return;
                _Latitude = value;
                this.OnPropertyChanged("Latitude");
            }
        }

        private long _StartTime;
        public long StartTime
        {
            get { return _StartTime; }
            set
            {
                if (value == _StartTime) return;
                _StartTime = value;
                this.OnPropertyChanged("StartTime");
            }
        }

        private long _EndTime;
        public long EndTime
        {
            get { return _EndTime; }
            set
            {
                if (value == _EndTime) return;
                _EndTime = value;
                this.OnPropertyChanged("EndTime");
            }
        }

        private long _ViewCount;
        public long ViewCount
        {
            get { return _ViewCount; }
            set
            {
                if (value == _ViewCount) return;
                _ViewCount = value;
                this.OnPropertyChanged("ViewCount");
            }
        }

        private long _LikeCount;
        public long LikeCount
        {
            get { return _LikeCount; }
            set
            {
                if (value == _LikeCount) return;
                _LikeCount = value;
                this.OnPropertyChanged("LikeCount");
            }
        }

        private string _Title;
        public string Title
        {
            get { return _Title; }
            set
            {
                if (value == _Title) return;
                _Title = value;
                this.OnPropertyChanged("Title");
            }
        }

        private Guid _StreamID;
        public Guid StreamID
        {
            get { return _StreamID; }
            set
            {
                if (value == _StreamID) return;
                _StreamID = value;
                this.OnPropertyChanged("StreamID");
            }
        }

        private ObservableCollection<StreamCategoryModel> _CategoryList = new ObservableCollection<StreamCategoryModel>();
        public ObservableCollection<StreamCategoryModel> CategoryList
        {
            get { return _CategoryList; }
            set
            {
                if (value == _CategoryList) return;
                _CategoryList = value;
                this.OnPropertyChanged("CategoryList");
            }
        }

        private string _ChatServerIP;
        public string ChatServerIP
        {
            get { return _ChatServerIP; }
            set
            {
                if (value == _ChatServerIP) return;
                _ChatServerIP = value;
                this.OnPropertyChanged("ChatServerIP");
            }
        }

        private int _ChatServerPort;
        public int ChatServerPort
        {
            get { return _ChatServerPort; }
            set
            {
                if (value == _ChatServerPort) return;
                _ChatServerPort = value;
                this.OnPropertyChanged("ChatServerPort");
            }
        }

        private string _PublisherServerIP;
        public string PublisherServerIP
        {
            get { return _PublisherServerIP; }
            set
            {
                if (value == _PublisherServerIP) return;
                _PublisherServerIP = value;
                this.OnPropertyChanged("PublisherServerIP");
            }
        }

        private int _PublisherServerPort;
        public int PublisherServerPort
        {
            get { return _PublisherServerPort; }
            set
            {
                if (value == _PublisherServerPort) return;
                _PublisherServerPort = value;
                this.OnPropertyChanged("PublisherServerPort");
            }
        }

        private string _ViewerServerIP;
        public string ViewerServerIP
        {
            get { return _ViewerServerIP; }
            set
            {
                if (value == _ViewerServerIP) return;
                _ViewerServerIP = value;
                this.OnPropertyChanged("ViewerServerIP");
            }
        }

        private int _ViewerServerPort;
        public int ViewerServerPort
        {
            get { return _ViewerServerPort; }
            set
            {
                if (value == _ViewerServerPort) return;
                _ViewerServerPort = value;
                this.OnPropertyChanged("ViewerServerPort");
            }
        }

        private bool _ChatOn;
        public bool ChatOn
        {
            get { return _ChatOn; }
            set
            {
                if (value == _ChatOn) return;
                _ChatOn = value;
                this.OnPropertyChanged("ChatOn");
            }
        }

        private bool _GiftOn;
        public bool GiftOn
        {
            get { return _GiftOn; }
            set
            {
                if (value == _GiftOn) return;
                _GiftOn = value;
                this.OnPropertyChanged("GiftOn");
            }
        }

        private string _Country;
        public string Country
        {
            get { return _Country; }
            set
            {
                if (value == _Country) return;
                _Country = value;
                this.OnPropertyChanged("Country");
            }
        }

        private int _DeviceCategory;
        public int DeviceCategory
        {
            get { return _DeviceCategory; }
            set
            {
                if (value == _DeviceCategory) return;
                _DeviceCategory = value;
                this.OnPropertyChanged("DeviceCategory");
            }
        }

        private double? _Distance;
        public double? Distance
        {
            get { return _Distance; }
            set
            {
                if (value == _Distance) return;
                _Distance = value;
                this.OnPropertyChanged("Distance");
            }
        }

        private bool _IsStreamOpened;
        public bool IsStreamOpened
        {
            get { return _IsStreamOpened; }
            set
            {
                if (value == _IsStreamOpened) return;
                _IsStreamOpened = value;
                this.OnPropertyChanged("IsStreamOpened");
            }
        }

        private bool _IsViewOpened;
        public bool IsViewOpened
        {
            get { return _IsViewOpened; }
            set
            {
                if (value == _IsViewOpened) 
                    return;

                _IsViewOpened = value;
                this.OnPropertyChanged("IsViewOpened");
            }
        }

        private bool _IsCombinedView = false;
        public bool IsCombinedView
        {
            get { return _IsCombinedView; }
        }

        private int _ViewType = SettingsConstants.TYPE_STREAM;
        public int ViewType
        {
            get { return _ViewType; }
        }

        #endregion

    }

    public class StreamCategoryModel : INotifyPropertyChanged
    {
        private int _CategoryID;
        public int CategoryID
        {
            get { return _CategoryID; }
            set
            {
                if (value == _CategoryID) return;
                _CategoryID = value;
                this.OnPropertyChanged("CategoryID");
            }
        }

        private string _CategoryName = string.Empty;
        public string CategoryName
        {
            get { return _CategoryName; }
            set
            {
                if (value == _CategoryName) return;
                _CategoryName = value;
                this.OnPropertyChanged("CategoryName");
            }
        }

        private int _FollowingCount;
        public int FollowingCount
        {
            get { return _FollowingCount; }
            set
            {
                if (value == _FollowingCount) return;
                _FollowingCount = value;
                this.OnPropertyChanged("FollowingCount");
            }
        }

        private bool _IsSelected = false;
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                if (value == _IsSelected) return;
                _IsSelected = value;
                this.OnPropertyChanged("IsSelected");
            }
        }

        private bool _IsChecked = false;
        public bool IsChecked
        {
            get { return _IsChecked; }
            set
            {
                if (value == _IsChecked) return;
                _IsChecked = value;
                this.OnPropertyChanged("IsChecked");
            }
        }

        private bool _IsExpanded = false;
        public bool IsExpanded
        {
            get { return _IsExpanded; }
            set
            {
                if (value == _IsExpanded) return;
                _IsExpanded = value;
                this.OnPropertyChanged("IsExpanded");
            }
        }

        private ObservableCollection<StreamModel> _StreamList = null;
        public ObservableCollection<StreamModel> StreamList
        {
            get { return _StreamList; }
            set
            {
                if (value == _StreamList) return;
                _StreamList = value;
                this.OnPropertyChanged("StreamList");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) { handler(this, new PropertyChangedEventArgs(propertyName)); }
        }
    }

    public class StreamUserModel : INotifyPropertyChanged
    {
        public StreamUserModel() { }

        public StreamUserModel(StreamUserDTO userDTO)
        {
            LoadData(userDTO);
        }

        public void LoadData(StreamUserDTO userDTO)
        {
            this.UserTableID = userDTO.UserTableID;
            this.UserName = userDTO.UserName;
            this.ProfileImage = userDTO.ProfileImage;
            this.FollowingCount = userDTO.FollowingCount;
            this.FollowerCount = userDTO.FollowerCount;
            this.CoinCount = userDTO.CoinCount;
            this.ViewCount = userDTO.ViewCount;
            this.Country = userDTO.Country;
            this.IsFollowing = userDTO.IsFollowing;
            this.PublisherID = userDTO.PublisherID;
            this.AddedTime = userDTO.AddedTime;
        }

        public void LoadData(StreamUserModel contributorModel)
        {
            this.UserTableID = contributorModel.UserTableID;
            this.UserName = !string.IsNullOrEmpty(contributorModel.UserName) ? contributorModel.UserName : string.Empty;
            this.ProfileImage = !string.IsNullOrEmpty(contributorModel.ProfileImage) ? contributorModel.ProfileImage : string.Empty;
            this.Contribution = contributorModel.Contribution;
        }

        private long _UserTableID;
        public long UserTableID
        {
            get { return _UserTableID; }
            set
            {
                if (value == _UserTableID) { return; }
                _UserTableID = value;
                this.OnPropertyChanged("UserTableID");
            }
        }

        private string _UserName;
        public string UserName
        {
            get { return _UserName; }
            set
            {
                if (value == _UserName) { return; }
                _UserName = value;
                this.OnPropertyChanged("UserName");
            }
        }

        private string _ProfileImage;
        public string ProfileImage
        {
            get { return _ProfileImage; }
            set
            {
                if (value == _ProfileImage) { return; }
                _ProfileImage = value;
                this.OnPropertyChanged("ProfileImage");
            }
        }

        private long _FollowingCount;
        public long FollowingCount
        {
            get { return _FollowingCount; }
            set
            {
                if (value == _FollowingCount) { return; }
                _FollowingCount = value;
                this.OnPropertyChanged("FollowingCount");
            }
        }

        private long _FollowerCount;
        public long FollowerCount
        {
            get { return _FollowerCount; }
            set
            {
                if (value == _FollowerCount) { return; }
                _FollowerCount = value;
                this.OnPropertyChanged("FollowerCount");
            }
        }

        private long _CoinCount;
        public long CoinCount
        {
            get { return _CoinCount; }
            set
            {
                if (value == _CoinCount) return;
                _CoinCount = value;
                this.OnPropertyChanged("CoinCount");
            }
        }

        private long _ViewCount;
        public long ViewCount
        {
            get { return _ViewCount; }
            set
            {
                if (value == _ViewCount) return;
                _ViewCount = value;
                this.OnPropertyChanged("ViewCount");
            }
        }

        private string _Country;
        public string Country
        {
            get { return _Country; }
            set
            {
                if (value == _Country) return;
                _Country = value;
                this.OnPropertyChanged("Country");
            }
        }

        private bool _IsFollowing;
        public bool IsFollowing
        {
            get { return _IsFollowing; }
            set
            {
                if (value == _IsFollowing) return;
                _IsFollowing = value;
                this.OnPropertyChanged("IsFollowing");
            }
        }

        private long _AddedTime;
        public long AddedTime
        {
            get { return _AddedTime; }
            set
            {
                if (value == _AddedTime) return;
                _AddedTime = value;
                this.OnPropertyChanged("AddedTime");
            }
        }

        private long _PublisherID;
        public long PublisherID
        {
            get { return _PublisherID; }
            set
            {
                if (value == _PublisherID) return;
                _PublisherID = value;
                this.OnPropertyChanged("PublisherID");
            }
        }

        private long _Contribution;
        public long Contribution
        {
            get { return _Contribution; }
            set
            {
                if (value == _Contribution) return;
                _Contribution = value;
                this.OnPropertyChanged("Contribution");
            }
        }

        private int _ContributionType;
        public int ContributionType
        {
            get { return _ContributionType; }
            set
            {
                if (value == _ContributionType) return;
                _ContributionType = value;
                this.OnPropertyChanged("ContributionType");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) { handler(this, new PropertyChangedEventArgs(propertyName)); }
        }

    }

    public class AppInfoModel : INotifyPropertyChanged
    {
        public AppInfoModel() { }

        private int _AppHandler;
        public int AppHandler
        {
            get { return _AppHandler; }
            set
            {
                if (value == _AppHandler) { return; }
                _AppHandler = value;
                this.OnPropertyChanged("AppHandler");
            }
        }

        private string _AppTitle;
        public string AppTitle
        {
            get { return _AppTitle; }
            set
            {
                if (value == _AppTitle) { return; }
                _AppTitle = value;
                this.OnPropertyChanged("AppTitle");
            }
        }

        private string _AppName;
        public string AppName
        {
            get { return _AppName; }
            set
            {
                if (value == _AppName) { return; }
                _AppName = value;
                this.OnPropertyChanged("AppName");
            }
        }

        private string _AppCaption;
        public string AppCaption
        {
            get { return _AppCaption; }
            set
            {
                if (value == _AppCaption) { return; }
                _AppCaption = value;
                this.OnPropertyChanged("AppCaption");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) { handler(this, new PropertyChangedEventArgs(propertyName)); }
        }
    }

    public class StreamRenderModel : INotifyPropertyChanged
    {
        public StreamRenderModel() { }

        private ImageSource _RenderSource = null;
        public ImageSource RenderSource
        {
            get
            {
                return _RenderSource;
            }
            set
            {
                _RenderSource = value;
                OnPropertyChanged("RenderSource");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) { handler(this, new PropertyChangedEventArgs(propertyName)); }
        }
    }

    public class StreamLoadStatusModel : INotifyPropertyChanged
    {
        public StreamLoadStatusModel() { }

        private bool _IsFeaturedLoading = false;
        public bool IsFeaturedLoading
        {
            get { return _IsFeaturedLoading; }
            set
            {
                if (value == _IsFeaturedLoading) { return; }
                _IsFeaturedLoading = value;
                this.OnPropertyChanged("IsFeaturedLoading");
            }
        }

        private bool _IsRecentLoading = false;
        public bool IsRecentLoading
        {
            get { return _IsRecentLoading; }
            set
            {
                if (value == _IsRecentLoading) { return; }
                _IsRecentLoading = value;
                this.OnPropertyChanged("IsRecentLoading");
            }
        }

        private bool _IsNearByLoading = false;
        public bool IsNearByLoading
        {
            get { return _IsNearByLoading; }
            set
            {
                if (value == _IsNearByLoading) { return; }
                _IsNearByLoading = value;
                this.OnPropertyChanged("IsNearByLoading");
            }
        }

        private bool _IsMostViewLoading = false;
        public bool IsMostViewLoading
        {
            get { return _IsMostViewLoading; }
            set
            {
                if (value == _IsMostViewLoading) { return; }
                _IsMostViewLoading = value;
                this.OnPropertyChanged("IsMostViewLoading");
            }
        }

        private bool _IsSearchLoading = false;
        public bool IsSearchLoading
        {
            get { return _IsSearchLoading; }
            set
            {
                if (value == _IsSearchLoading) { return; }
                _IsSearchLoading = value;
                this.OnPropertyChanged("IsSearchLoading");
            }
        }

        private bool _IsFollowingLoading = false;
        public bool IsFollowingLoading
        {
            get { return _IsFollowingLoading; }
            set
            {
                if (value == _IsFollowingLoading) { return; }
                _IsFollowingLoading = value;
                this.OnPropertyChanged("IsFollowingLoading");
            }
        }

        private bool _IsCategoryWiseCountLoading = false;
        public bool IsCategoryWiseCountLoading
        {
            get { return _IsCategoryWiseCountLoading; }
            set
            {
                if (value == _IsCategoryWiseCountLoading) { return; }
                _IsCategoryWiseCountLoading = value;
                this.OnPropertyChanged("IsCategoryWiseCountLoading");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) { handler(this, new PropertyChangedEventArgs(propertyName)); }
        }
    }
}

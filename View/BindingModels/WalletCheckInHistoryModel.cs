﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace View.BindingModels
{
    public class WalletCheckInHistoryModel : INotifyPropertyChanged
    {
        #region constants
        //public const int CHECKED_IN_TODAY = 1;
        //public const int CHECKED_IN_PREVIOUS = 2;
        //public const int GIFT_DAY = 3;
        //public const int UPCOMING = 4;
        //public const int RECENT_MISSED = 5;
        //public const int BREAK_CYCLE_DAY = 6;
        #endregion
        private string reportingDate = string.Empty;
        public string ReportingDate
        {
            get 
            { 
                return reportingDate; 
            }
            set 
            { 
                reportingDate = value; 
                this.onPropertyChanged("ReportingDate"); 
                ReportingDay = !string.IsNullOrEmpty(reportingDate)? Utility.Wallet.WalletViewModel.Instance.SetDayNumber( Convert.ToDateTime(reportingDate).DayOfWeek.ToString().ToLower()) : 0; 
            }
        }

        private bool checkIn = false;
        public bool CheckIn
        {
            get { return checkIn; }
            set { checkIn = value; this.onPropertyChanged("CheckIn"); }
        }

        public int ReportingDay
        {
            get;
            private set;
        }

        private int dayCheckInType = 0;
        public int DayCheckInType
        {
            //todays check in          1
            //previous day check in    2
            //gift day                 3
            //upcoming                 4
            //missed day               5
            //break cycle day          6
            get { return dayCheckInType;}
            set { dayCheckInType = value; this.onPropertyChanged("DayCheckInType"); }
        }

        #region key
        public const string keyReportingDate = "reportingDate";
        public const string keyCheckIn = "CheckIn";
        public const string keyCheckInHistory = "dailyCheckinHistory";
        #endregion

        public static WalletCheckInHistoryModel LoadDataFromJson(JObject dto)
        {
            WalletCheckInHistoryModel model = new WalletCheckInHistoryModel();
            try
            {
                if (dto[keyReportingDate] != null)
                {
                    model.ReportingDate = (string)dto[keyReportingDate];
                }
                if (dto[keyCheckIn] != null)
                {
                    model.CheckIn = (int)dto[keyCheckIn] == 1 ? true : false;
                }
            }
            catch (Exception)
            {
                model.ReportingDate = string.Empty;
                //throw;
            }
            return model;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void onPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

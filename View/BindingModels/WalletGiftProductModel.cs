﻿using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace View.BindingModels
{
    public class WalletGiftProductModel : INotifyPropertyChanged
    {
        private int productID = 0;
        public int ProductID
        {
            get { return productID; }
            set { productID = value; this.OnPropertyChanged("ProductID"); }
        }

        private string productName = string.Empty;
        public string ProductName
        {
            get { return productName; }
            set { productName = value; this.OnPropertyChanged("ProductName"); }
        }

        private int productTypeID = 0;
        public int ProductTypeID
        {
            get { return productTypeID; }
            set { productTypeID = value; this.OnPropertyChanged("ProductTypeID"); }
        }

        private int productCategoryID = 0;
        public int ProductCategoryID
        {
            get { return productCategoryID; }
            set { productCategoryID = value; this.OnPropertyChanged("ProductCategoryID"); }
        }

        private int productPriceCoinID = 0;
        public int ProductPriceCoinID
        {
            get { return productPriceCoinID; }
            set { productPriceCoinID = value; this.OnPropertyChanged("ProductPriceCoinID"); }
        }

        private int productPriceCoinQuantity = 0;
        public int ProductPriceCoinQuantity
        {
            get { return productPriceCoinQuantity; }
            set { productPriceCoinQuantity = value; this.OnPropertyChanged("ProductPriceCoinQuantity"); }
        }

        private double productPrice = 0.0;
        public double ProductPrice
        {
            get { return productPrice; }
            set { productPrice = value; this.OnPropertyChanged("ProductPrice"); }
        }

        private string productIcon = string.Empty;
        public string ProductIcon
        {
            get { return productIcon; }
            set { productIcon = value; this.OnPropertyChanged("ProductIcon"); }
        }

        private string _productUrl = string.Empty;
        public string ProductUrl
        {
            get { return _productUrl; }
            set { _productUrl = value; this.OnPropertyChanged("ProductUrl"); }
        }

        private bool _IsNeedToDisplay = false;
        public bool IsNeedToDisplay
        {
            get { return _IsNeedToDisplay; }
            set 
            {
                _IsNeedToDisplay = value;
                this.OnPropertyChanged("IsNeedToDisplay");
            }
        }

        private int _GiftDisplayingTime = 3000;
        public int GiftDisplayingTime
        {
            get { return _GiftDisplayingTime; }
            set
            {
                _GiftDisplayingTime = value;
                this.OnPropertyChanged("GiftDisplayingTime");
            }
        }

        private bool active = false;
        public bool Active
        {
            get { return active; }
            set { active = value; this.OnPropertyChanged("IsApplicableForAllStores"); }
        }

        private bool isApplicableForAllStores = false;
        public bool IsApplicableForAllStores
        {
            get { return isApplicableForAllStores; }
            set { isApplicableForAllStores = value; this.OnPropertyChanged("IsApplicableForAllStores"); }
        }

        private bool isSelected = false;
        public bool IsSelected
        {
            get { return isSelected; }
            set 
            {
                isSelected = value;
                this.OnPropertyChanged("IsSelected"); 
            }
        }

        private string _giftFilePath = string.Empty;
        public string GiftFilePath
        {
            get { return _giftFilePath; }
            set { _giftFilePath = value; this.OnPropertyChanged("GiftFilePath"); }
        }

        private long _GiftSenderTableId;
        public long GiftSenderTableId
        {
            get { return _GiftSenderTableId; }
            set { _GiftSenderTableId = value; this.OnPropertyChanged("GiftSenderTableId"); }
        }
        private string _GiftSenderName = string.Empty;
        public string GiftSenderName
        {
            get { return _GiftSenderName; }
            set { _GiftSenderName = value; this.OnPropertyChanged("GiftSenderName"); }
        }

        private string _GiftSenderProfileImage = string.Empty;
        public string GiftSenderProfileImage
        {
            get { return _GiftSenderProfileImage; }
            set { _GiftSenderProfileImage = value; this.OnPropertyChanged("GiftSenderProfileImage"); }
        }

        public static WalletGiftProductModel LoadDataFromJson(JObject jObject)
        {
            WalletGiftProductModel model = new WalletGiftProductModel();
            try
            {
                if (jObject[WalletConstants.RSP_KEY_PRODUCT_ID] != null)
                {
                    model.ProductID = (int)jObject[WalletConstants.RSP_KEY_PRODUCT_ID];
                }
                if (jObject[WalletConstants.RSP_KEY_PRODUCT_NAME] != null)
                {
                    model.ProductName = (string)jObject[WalletConstants.RSP_KEY_PRODUCT_NAME];
                }
                if (jObject[WalletConstants.RSP_KEY_PRODUCT_TYPE_ID] != null)
                {
                    model.ProductTypeID = (int)jObject[WalletConstants.RSP_KEY_PRODUCT_TYPE_ID];
                }
                if (jObject[WalletConstants.RSP_KEY_PRODUCT_CATEGORY_ID] != null)
                {
                    model.ProductCategoryID = (int)jObject[WalletConstants.RSP_KEY_PRODUCT_CATEGORY_ID];
                }
                if (jObject[WalletConstants.RSP_KEY_PRODUCT_PRICE_COIN_ID] != null)
                {
                    model.ProductPriceCoinID = (int)jObject[WalletConstants.RSP_KEY_PRODUCT_PRICE_COIN_ID];
                }
                if (jObject[WalletConstants.RSP_KEY_PRODUCT_PRICE_COIN_QUANTITY] != null)
                {
                    model.ProductPriceCoinQuantity = (int)jObject[WalletConstants.RSP_KEY_PRODUCT_PRICE_COIN_QUANTITY];
                }
                if (jObject[WalletConstants.RSP_KEY_PRODUCT_PRICE] != null)
                {
                    model.ProductPrice = (double)jObject[WalletConstants.RSP_KEY_PRODUCT_PRICE];
                }
                if (jObject[WalletConstants.RSP_KEY_PRODUCT_ICON] != null)
                {
                    model.ProductIcon = (string)jObject[WalletConstants.RSP_KEY_PRODUCT_ICON];
                }
                if (jObject[WalletConstants.RSP_KEY_ACTIVE] != null)
                {
                    model.Active = (bool)jObject[WalletConstants.RSP_KEY_ACTIVE];
                }
                if (jObject[WalletConstants.RSP_KEY_IS_APPLICABLE_FOR_ALL_STORES] != null)
                {
                    model.IsApplicableForAllStores = (bool)jObject[WalletConstants.RSP_KEY_IS_APPLICABLE_FOR_ALL_STORES];
                }
                //test purpose
                model.ProductIcon = "http://38.127.68.51:8084/ringmarket/icon.png";
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Gift Product Parse Error ==> " + ex.Message + Environment.NewLine + ex.StackTrace);
            }
            return model;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public static JObject MakeGiftObjects(WalletGiftProductModel gift)
        {
            JObject giftObj = new JObject();
            giftObj[WalletConstants.RSP_KEY_PRODUCT_ID] = gift.ProductID;
            giftObj[WalletConstants.RSP_KEY_QUANTITY] = 1;
            return giftObj;
        }
    }
}

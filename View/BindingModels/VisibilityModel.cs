﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace View.BindingModels
{
    public class VisibilityModel : INotifyPropertyChanged
    {

        #region Constructor


        public VisibilityModel()
        {
        }

        #endregion Constructor

        #region Property

        //private Visibility _IsVisible = Visibility.Visible;
        //public Visibility IsVisible
        //{
        //    get { return _IsVisible; }
        //    set
        //    {
        //        if (value == _IsVisible)
        //            return;

        //        _IsVisible = value;
        //        this.OnPropertyChanged("IsVisible");
        //    }
        //}

        private Visibility _IsVisibleInSuggestion = Visibility.Visible;
        public Visibility IsVisibleInSuggestion
        {
            get { return _IsVisibleInSuggestion; }
            set
            {
                if (value == _IsVisibleInSuggestion)
                    return;

                _IsVisibleInSuggestion = value;

                this.OnPropertyChanged("IsVisibleInSuggestion");
            }
        }

        private Visibility _IsVisibleInPeopleUmayKnow = Visibility.Collapsed;
        public Visibility IsVisibleInPeopleUmayKnow
        {
            get { return _IsVisibleInPeopleUmayKnow; }
            set
            {
                if (value == _IsVisibleInPeopleUmayKnow)
                    return;

                _IsVisibleInPeopleUmayKnow = value;

                this.OnPropertyChanged("IsVisibleInPeopleUmayKnow");
            }
        }

        //private Visibility _IsVisibleInPending = Visibility.Visible;
        //public Visibility IsVisibleInPending
        //{
        //    get { return _IsVisibleInPending; }
        //    set
        //    {
        //        if (value == _IsVisibleInPending)
        //            return;

        //        _IsVisibleInPending = value;
        //        this.OnPropertyChanged("IsVisibleInPending");
        //    }
        //}

        private Visibility _IsVisibleInFriendsFriendList = Visibility.Visible;
        public Visibility IsVisibleInFriendsFriendList
        {
            get { return _IsVisibleInFriendsFriendList; }
            set
            {
                if (value == _IsVisibleInFriendsFriendList)
                    return;

                _IsVisibleInFriendsFriendList = value;
                this.OnPropertyChanged("IsVisibleInFriendsFriendList");
            }
        }

        private Visibility _IsVisibleInMyFriendList = Visibility.Visible;
        public Visibility IsVisibleInMyFriendList
        {
            get { return _IsVisibleInMyFriendList; }
            set
            {
                if (value == _IsVisibleInMyFriendList)
                    return;

                _IsVisibleInMyFriendList = value;
                this.OnPropertyChanged("IsVisibleInMyFriendList");
            }
        }
        //private Visibility _IsVisibleInCallSetting = Visibility.Visible;
        //public Visibility IsVisibleInCallsetting
        //{
        //    get { return _IsVisibleInCallSetting; }
        //    set
        //    {
        //        if (value == _IsVisibleInCallSetting)
        //            return;

        //        _IsVisibleInCallSetting = value;
        //        this.OnPropertyChanged("IsVisibleInCallsetting");
        //    }
        //}

        //private Visibility _IsVisibleInBlockedContact = Visibility.Visible;
        //public Visibility IsVisibleInBlockedContact
        //{
        //    get { return _IsVisibleInBlockedContact; }
        //    set
        //    {
        //        if (value == _IsVisibleInBlockedContact)
        //            return;

        //        _IsVisibleInBlockedContact = value;
        //        this.OnPropertyChanged("IsVisibleInBlockedContact");
        //    }
        //}
        private Visibility _IsVisibleInCircleMemberList = Visibility.Visible;
        public Visibility IsVisibleInCircleMemberList
        {
            get { return _IsVisibleInCircleMemberList; }
            set
            {
                if (value == _IsVisibleInCircleMemberList)
                    return;

                _IsVisibleInCircleMemberList = value;
                this.OnPropertyChanged("IsVisibleInCircleMemberList");
            }
        }
        //private Visibility _IsVisibleInDialer = Visibility.Visible;
        //public Visibility IsVisibleInDialer
        //{
        //    get { return _IsVisibleInDialer; }
        //    set
        //    {
        //        if (value == _IsVisibleInDialer)
        //            return;

        //        _IsVisibleInDialer = value;

        //        this.OnPropertyChanged("IsVisibleInDialer");
        //    }
        //}

        //private Visibility _IsVisibleInAddFriend = Visibility.Visible;
        //public Visibility IsVisibleInAddFriend
        //{
        //    get { return _IsVisibleInAddFriend; }
        //    set
        //    {
        //        if (value == _IsVisibleInAddFriend)
        //            return;

        //        _IsVisibleInAddFriend = value;
        //        this.OnPropertyChanged("IsVisibleInAddFriend");
        //    }
        //}

        private bool _ShowActionButton = true;
        public bool ShowActionButton
        {
            get { return _ShowActionButton; }
            set
            {
                if (value == _ShowActionButton)
                    return;
                _ShowActionButton = value;
                this.OnPropertyChanged("ShowActionButton");
            }
        }

        private Visibility _ShowLoading = Visibility.Hidden;
        public Visibility ShowLoading
        {
            get { return _ShowLoading; }
            set
            {
                if (value == _ShowLoading)
                    return;
                _ShowLoading = value;
                this.OnPropertyChanged("ShowLoading");
            }
        }

        private Visibility _IsVisibleInAddGroupMember = Visibility.Visible;
        public Visibility IsVisibleInAddGroupMember
        {
            get { return _IsVisibleInAddGroupMember; }
            set
            {
                if (value == _IsVisibleInAddGroupMember)
                    return;

                _IsVisibleInAddGroupMember = value;
                this.OnPropertyChanged("IsVisibleInAddGroupMember");
            }
        }

        private bool _IsAddGroupMember = false;
        public bool IsAddGroupMember
        {
            get { return _IsAddGroupMember; }
            set
            {
                if (value == _IsAddGroupMember)
                    return;
                _IsAddGroupMember = value;
                this.OnPropertyChanged("IsAddGroupMember");
            }
        }

        #endregion Property


        #region Utility Methods

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods
    }
}

﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace View.BindingModels
{
    public class WelcomeScreenViewModel : INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(WelcomeScreenViewModel).Name);
        private static WelcomeScreenViewModel _Instance;

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public static WelcomeScreenViewModel Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new WelcomeScreenViewModel();
                }
                return _Instance;
            }
        }

        //private bool _IsComponentEnabled = true;
        //public bool IsComponentEnabled
        //{
        //    get { return _IsComponentEnabled; }
        //    set
        //    {
        //        if (_IsComponentEnabled == value)
        //        {
        //            return;
        //        }
        //        _IsComponentEnabled = value;
        //        OnPropertyChanged("IsComponentEnabled");
        //    }
        //}
    }
}

﻿using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace View.BindingModels
{
    public class WalletReferralRuleListModel : INotifyPropertyChanged
    {
        //public const string keyRuleItemID = "ruleItemId";
        //public const string keyRuleName = "ruleName";
        //public const string keyCoinQuantity = "coinQuantity";
        //public const string keyRulesList = "rulesList";

        private int ruleItemID = 0;
        public int RuleItemID
        {
            get { return ruleItemID; }
            set { ruleItemID = value; this.OnPropertyChanged("RuleID"); }
        }

        private string ruleName = string.Empty;
        public string RuleName
        {
            get { return ruleName; }
            set { ruleName = value; this.OnPropertyChanged("RuleName"); }
        }

        private string description = string.Empty;
        public string Description
        {
            get { return description; }
            set { description = value; this.OnPropertyChanged("Description"); }
        }


        private int coinQuantity = 0;
        public int CoinQuantity
        {
            get { return coinQuantity; }
            set { coinQuantity = value; this.OnPropertyChanged("CoinQuantity"); }
        }

        public static WalletReferralRuleListModel LoadDataFromJson(JObject jObject)
        {
            WalletReferralRuleListModel model = new WalletReferralRuleListModel();
            try
            {
                if (jObject[WalletConstants.RSP_KEY_RULE_ITEM_ID] != null)
                {
                    model.RuleItemID = (int)jObject[WalletConstants.RSP_KEY_RULE_ITEM_ID];
                }
                if (jObject[WalletConstants.RSP_KEY_RULE_NAME] != null)
                {
                    model.RuleName = (string)jObject[WalletConstants.RSP_KEY_RULE_NAME];
                }
                if (jObject[WalletConstants.RSP_KEY_RULE_DESCRIPTION] !=null)
                {
                    model.Description = (string)jObject[WalletConstants.RSP_KEY_RULE_DESCRIPTION];
                }
                if (jObject[WalletConstants.RSP_KEY_COIN_QUANTITY] != null)
                {
                    model.CoinQuantity = (int)jObject[WalletConstants.RSP_KEY_COIN_QUANTITY];
                }
            }
            catch (Exception ex)
            {
                model.RuleItemID = 0;
                System.Diagnostics.Debug.WriteLine("Referral Rule Parse Error ==> " + ex.Message + Environment.NewLine + ex.StackTrace);
            }
            return model;
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

﻿using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.ComponentModel;

namespace View.BindingModels
{
    public class CelebrityModel : BaseUserProfileModel
    {
        public CelebrityModel()
        {
            CurrentInstance = this;
        }
        public void LoadData(JObject jObject)
        {
            if (jObject[JsonKeys.FullName] != null)
            {
                this.FullName = (string)jObject[JsonKeys.FullName];
            }
            if (jObject[JsonKeys.UserTableID] != null)
            {
                this.UserTableID = (long)jObject[JsonKeys.UserTableID];
            }
            if (jObject[JsonKeys.ProfileImage] != null)
            {
                this.ProfileImage = (string)jObject[JsonKeys.ProfileImage];
            }
            if (jObject[JsonKeys.CoverImage] != null)
            {
                this.CoverImage = (string)jObject[JsonKeys.CoverImage];
            }
            if (jObject[JsonKeys.UserIdentity] != null)
            {
                this.UserIdentity = (long)jObject[JsonKeys.UserIdentity];
            }
            if (jObject[JsonKeys.RingId] != null)
            {
                this.UserIdentity = (long)jObject[JsonKeys.RingId];
            }
            if (jObject[JsonKeys.IsUserFeedHidden] != null)
            {
                this.IsProfileHidden = (bool)jObject[JsonKeys.IsUserFeedHidden];
            }
            if (jObject[JsonKeys.Followercount] != null)
            {
                this.SubscriberCount = (int)jObject[JsonKeys.Followercount];
            }
            if (jObject[JsonKeys.PostCount] != null)
            {
                this.PostCount = (int)jObject[JsonKeys.PostCount];
            }
            if (jObject[JsonKeys.CelebrityCountry] != null)
            {
                this.CelebrityCountry = (string)jObject[JsonKeys.CelebrityCountry];
            }
            if (jObject[JsonKeys.IsFollower] != null)
            {
                this.IsSubscribed = (bool)jObject[JsonKeys.IsFollower];
            }
            if (jObject[JsonKeys.OnlineTime] != null)
            {
                this.OnlineTime = (long)jObject[JsonKeys.OnlineTime];
            }
            if (jObject[JsonKeys.MoodMessage] != null)
            {
                this.MoodMessage = (string)jObject[JsonKeys.MoodMessage];
            }
            if (jObject[JsonKeys.Category] != null)
            {
                JArray categoryArray = (JArray)jObject[JsonKeys.Category];
                List<string> CelebrityCatNameArray = new List<string>();
                foreach (var jObj in categoryArray)
                {
                    string temp = (string)jObj;
                    CelebrityCatNameArray.Add(temp);
                }
                CelebrityCategoryName = string.Join(", ", CelebrityCatNameArray.ToArray());
            }
            if (jObject[JsonKeys.CelebrityInfo] != null)
            {
                JObject celebInfo = (JObject)jObject[JsonKeys.CelebrityInfo];
                if (celebInfo[JsonKeys.Followercount] != null)
                {
                    this.SubscriberCount = (int)celebInfo[JsonKeys.Followercount];
                }
                if (celebInfo[JsonKeys.PostCount] != null)
                {
                    this.PostCount = (int)celebInfo[JsonKeys.PostCount];
                }
                if (celebInfo[JsonKeys.CelebrityCountry] != null)
                {
                    this.CelebrityCountry = (string)celebInfo[JsonKeys.CelebrityCountry];
                }
                if (celebInfo[JsonKeys.IsFollower] != null)
                {
                    this.IsSubscribed = (bool)celebInfo[JsonKeys.IsFollower];
                }
                if (celebInfo[JsonKeys.OnlineTime] != null)
                {
                    this.OnlineTime = (long)celebInfo[JsonKeys.OnlineTime];
                }
                if (celebInfo[JsonKeys.MoodMessage] != null)
                {
                    this.MoodMessage = (string)celebInfo[JsonKeys.MoodMessage];
                }
                if (celebInfo[JsonKeys.Category] != null)
                {
                    JArray categoryArray = (JArray)celebInfo[JsonKeys.Category];
                    List<string> CelebrityCatNameArray = new List<string>();
                    foreach (var jObj in categoryArray)
                    {
                        string temp = (string)jObj;
                        CelebrityCatNameArray.Add(temp);
                    }
                    CelebrityCategoryName = string.Join(", ", CelebrityCatNameArray.ToArray());
                }
            }
        }
        public void LoadData(CelebrityDTO dto)
        {
            UserTableID = dto.UserTableID;
            UserIdentity = dto.RingID;
            FullName = dto.CelebrityName;
            SubscriberCount = dto.SubscriberCount;
            PostCount = dto.PostCount;
            ProfileImage = dto.ProfileImage;
            CelebrityCountry = dto.CelebrityCountry;
            IsSubscribed = dto.IsSubscribed;
            CoverImage = dto.CoverImage;
            OnlineTime = dto.OnlineTime;
            MoodMessage = dto.MoodMessage;

            if (dto.CelebrityCatNameArray != null)
                CelebrityCategoryName = string.Join(", ", dto.CelebrityCatNameArray.ToArray());
            IsProfileHidden = dto.IsProfileHidden;
        }
        //private long _UserIdentity;
        //public long UserIdentity
        //{
        //    get { return _UserIdentity; }
        //    set
        //    {
        //        if (value == _UserIdentity)
        //            return;

        //        _UserIdentity = value;
        //        this.OnPropertyChanged("UserIdentity");
        //    }
        //}
        //private bool _IsProfileHidden;
        //public bool IsProfileHidden
        //{
        //    get { return _IsProfileHidden; }
        //    set
        //    {
        //        if (value == _IsProfileHidden)
        //            return;

        //        _IsProfileHidden = value;
        //        this.OnPropertyChanged("IsProfileHidden");
        //    }
        //}
        //private long _UserTableId;
        //public long UserTableID
        //{
        //    get { return _UserTableId; }
        //    set
        //    {
        //        if (value == _UserTableId)
        //            return;

        //        _UserTableId = value;
        //        this.OnPropertyChanged("UserTableID");
        //    }
        //}
        //private string _CelebrityName;
        //public string CelebrityName
        //{
        //    get { return _CelebrityName; }
        //    set
        //    {
        //        if (value == _CelebrityName)
        //            return;
        //        _CelebrityName = value;
        //        this.OnPropertyChanged("CelebrityName");
        //    }
        //}
        private bool _IsSubscribed;
        public bool IsSubscribed
        {
            get { return _IsSubscribed; }
            set
            {
                if (value == _IsSubscribed)
                    return;
                _IsSubscribed = value;
                this.OnPropertyChanged("IsSubscribed");
            }
        }
        private string _CelebrityCountry;
        public string CelebrityCountry
        {
            get { return _CelebrityCountry; }
            set
            {
                if (value == _CelebrityCountry)
                    return;
                _CelebrityCountry = value;
                this.OnPropertyChanged("CelebrityCountry");
            }
        }
        private long _OnlineTime;
        public long OnlineTime
        {
            get { return _OnlineTime; }
            set
            {
                if (value == _OnlineTime)
                    return;

                _OnlineTime = value;
                this.OnPropertyChanged("OnlineTime");
            }
        }
        private string _MoodMessage;
        public string MoodMessage
        {
            get { return _MoodMessage; }
            set
            {
                if (value == _MoodMessage)
                    return;
                _MoodMessage = value;
                this.OnPropertyChanged("MoodMessage");
            }
        }
        private string _CelebrityCategoryName;
        public string CelebrityCategoryName
        {
            get { return _CelebrityCategoryName; }
            set
            {
                if (value == _CelebrityCategoryName)
                    return;
                _CelebrityCategoryName = value;
                this.OnPropertyChanged("CelebrityCategoryName");
            }
        }
        private int _SubscriberCount;
        public int SubscriberCount
        {
            get { return _SubscriberCount; }
            set
            {
                if (value == _SubscriberCount)
                    return;

                _SubscriberCount = value;
                this.OnPropertyChanged("SubscriberCount");
            }
        }

        private int _PostCount;
        public int PostCount
        {
            get { return _PostCount; }
            set
            {
                if (value == _PostCount)
                    return;

                _PostCount = value;
                this.OnPropertyChanged("PostCount");
            }
        }
        //private string _ProfileImage;
        //public string ProfileImage
        //{
        //    get { return _ProfileImage; }
        //    set
        //    {
        //        if (value == _ProfileImage)
        //            return;

        //        _ProfileImage = value;
        //        this.OnPropertyChanged("ProfileImage");
        //    }
        //}
        private string _CoverImage;
        public string CoverImage
        {
            get { return _CoverImage; }
            set
            {
                if (value == _CoverImage)
                    return;

                _CoverImage = value;
                this.OnPropertyChanged("CoverImage");
            }
        }
        private bool _IsSortByPopularity;
        public bool IsSortByPopularity
        {
            get { return _IsSortByPopularity; }
            set
            {
                if (value == _IsSortByPopularity)
                    return;
                _IsSortByPopularity = value;
                this.OnPropertyChanged("IsSortByPopularity");
            }
        }

        private bool _IsExcludeRemovedList;
        public bool IsExcludeRemovedList
        {
            get { return _IsExcludeRemovedList; }
            set
            {
                if (value == _IsExcludeRemovedList)
                    return;
                _IsExcludeRemovedList = value;
                this.OnPropertyChanged("IsExcludeRemovedList");
            }
        }

        private bool _IsRightMarginOff;
        public bool IsRightMarginOff
        {
            get { return _IsRightMarginOff; }
            set
            {
                if (value == _IsRightMarginOff)
                    return;

                _IsRightMarginOff = value;
                this.OnPropertyChanged("IsRightMarginOff");
            }
        }

        private bool _IsLoadMore = false;
        public bool IsLoadMore
        {
            get
            {
                return _IsLoadMore;
            }
            set
            {
                _IsLoadMore = value;
                this.OnPropertyChanged("IsLoadMore");
            }

        }
        //private CelebrityModel _CurrentInstance;
        //public CelebrityModel CurrentInstance
        //{
        //    get { return _CurrentInstance; }
        //    set
        //    {
        //        if (value == _CurrentInstance)
        //            return;

        //        _CurrentInstance = value;
        //        this.OnPropertyChanged("CurrentInstance");
        //    }
        //}
        //public event PropertyChangedEventHandler PropertyChanged;
        //public void OnPropertyChanged(string propertyName)
        //{
        //    PropertyChangedEventHandler handler = PropertyChanged;
        //    if (handler != null)
        //    {
        //        handler(this, new PropertyChangedEventArgs(propertyName));
        //    }
        //}
    }
}

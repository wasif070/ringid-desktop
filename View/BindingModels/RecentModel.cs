﻿using Models.Constants;
using Models.Entity;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using View.Utility;
using View.Utility.Chat;

namespace View.BindingModels
{
    public class RecentModel : INotifyPropertyChanged
    {
        private bool _ForOnlyChatView = false;

        #region Constructor

        public RecentModel(bool forOnlyChatView = false)
        {
            this._ForOnlyChatView = forOnlyChatView;
            this._CurrentInstance = this;
        }

        public RecentModel(RecentDTO recentDTO, bool forOnlyChatView = false)
        {
            this._ForOnlyChatView = forOnlyChatView;
            this._CurrentInstance = this;
            this.LoadData(recentDTO);
        }

        #endregion Constructor

        #region Property

        private RecentModel _CurrentInstance;
        public RecentModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }

        private string _ContactID;
        public string ContactID
        {
            get { return _ContactID; }
            set
            {
                if (value == _ContactID)
                    return;

                _ContactID = value;
                this.OnPropertyChanged("ContactID");
            }
        }

        private long _FriendTableID;
        public long FriendTableID
        {
            get { return _FriendTableID; }
            set
            {
                if (value == _FriendTableID)
                    return;

                _FriendTableID = value;
                this.OnPropertyChanged("FriendTableID");
            }
        }

        private long _GroupID;
        public long GroupID
        {
            get { return _GroupID; }
            set
            {
                if (value == _GroupID)
                    return;

                _GroupID = value;
                this.OnPropertyChanged("GroupID");
            }
        }

        private string _RoomID;
        public string RoomID
        {
            get { return _RoomID; }
            set
            {
                if (value == _RoomID)
                    return;

                _RoomID = value;
                this.OnPropertyChanged("RoomID");
            }
        }

        private int _ContactType;
        public int ContactType
        {
            get { return _ContactType; }
            private set
            {
                if (value == _ContactType)
                    return;

                _ContactType = value;
                this.OnPropertyChanged("ContactType");
            }
        }

        private int _Type;
        public int Type
        {
            get { return _Type; }
            private set
            {
                if (value == _Type)
                    return;

                _Type = value;
                this.OnPropertyChanged("Type");
            }
        }

        private long _Time;
        public long Time
        {
            get { return _Time; }
            set
            {
                if (value == _Time)
                    return;

                _Time = value;
                this.OnPropertyChanged("Time");
            }
        }

        private DateTime _DateTime;
        public DateTime DateTime
        {
            get { return _DateTime; }
            set
            {
                if (value == _DateTime)
                    return;

                _DateTime = value;
                this.OnPropertyChanged("DateTime");
            }
        }

        private CallLogModel _CallLog;
        public CallLogModel CallLog
        {
            get { return _CallLog; }
            private set
            {
                if (value == _CallLog)
                    return;

                _CallLog = value;
                this.OnPropertyChanged("CallLog");
            }
        }

        private MessageModel _Message;
        public MessageModel Message
        {
            get { return _Message; }
            private set
            {
                if (value == _Message)
                    return;

                _Message = value;
                this.OnPropertyChanged("Message");
            }
        }

        private ActivityModel _Activity;
        public ActivityModel Activity
        {
            get { return _Activity; }
            private set
            {
                if (value == _Activity)
                    return;

                _Activity = value;
                this.OnPropertyChanged("Activity");
            }
        }

        private string _UniqueKey;
        public string UniqueKey
        {
            get { return _UniqueKey; }
            private set
            {
                if (value == _UniqueKey)
                    return;

                _UniqueKey = value;
                this.OnPropertyChanged("UniqueKey");
            }
        }

        private bool _IsProcessed = false;
        public bool IsProcessed
        {
            get { return _IsProcessed; }
            set
            {
                if (value == _IsProcessed)
                    return;

                _IsProcessed = value;
                this.OnPropertyChanged("IsProcessed");
            }
        }

        private UserShortInfoModel _FriendInfoModel;
        public UserShortInfoModel FriendInfoModel
        {
            get { return _FriendInfoModel; }
            set
            {
                if (value == _FriendInfoModel)
                    return;

                _FriendInfoModel = value;
                this.OnPropertyChanged("FriendInfoModel");
            }
        }

        private GroupInfoModel _GroupInfoModel;
        public GroupInfoModel GroupInfoModel
        {
            get { return _GroupInfoModel; }
            set
            {
                if (value == _GroupInfoModel)
                    return;

                _GroupInfoModel = value;
                this.OnPropertyChanged("GroupInfoModel");
            }
        }

        private RoomModel _RoomModel;
        public RoomModel RoomModel
        {
            get { return _RoomModel; }
            set
            {
                if (value == _RoomModel)
                    return;

                _RoomModel = value;
                this.OnPropertyChanged("RoomModel");
            }
        }

        private ObservableDictionary<string, long> _UnreadList;
        public ObservableDictionary<string, long> UnreadList
        {
            get { return _UnreadList; }
            set
            {
                if (value == _UnreadList)
                    return;

                _UnreadList = value;
                this.OnPropertyChanged("UnreadList");
            }
        }

        private long  _PopupLifeTime;
        public long PopupLifeTime
        {
            get { return _PopupLifeTime; }
            set
            {
                if (value == _PopupLifeTime)
                    return;

                _PopupLifeTime = value;
                this.OnPropertyChanged("PopupLifeTime");
            }
        }

        private bool _IsRecentOpened;
        public bool IsRecentOpened
        {
            get { return _IsRecentOpened; }
            set
            {
                if (value == _IsRecentOpened)
                    return;

                _IsRecentOpened = value;
                this.OnPropertyChanged("IsRecentOpened");
            }
        }

        private int _MinViewHeight;
        public int MinViewHeight
        {
            get { return _MinViewHeight; }
            set
            {
                if (value == _MinViewHeight)
                    return;

                _MinViewHeight = value;
                this.OnPropertyChanged("MinViewHeight");
            }
        }

        private int _PrevViewHeight;
        public int PrevViewHeight
        {
            get { return _PrevViewHeight; }
            set
            {
                if (value == _PrevViewHeight)
                    return;

                _PrevViewHeight = value;
                this.OnPropertyChanged("PrevViewHeight");
            }
        }

        private bool _IsChecked;
        public bool IsChecked
        {
            get { return _IsChecked; }
            set
            {
                if (value == _IsChecked)
                    return;

                _IsChecked = value;
                this.OnPropertyChanged("IsChecked");
            }
        }

        private bool _IsCheckedVisible;
        public bool IsCheckedVisible
        {
            get { return _IsCheckedVisible; }
            set
            {
                if (value == _IsCheckedVisible)
                    return;

                _IsCheckedVisible = value;
                this.OnPropertyChanged("IsCheckedVisible");
            }
        }

        #endregion Property

        #region Utility Method

        public void LoadData(RecentDTO recentDTO)
        {
            if (recentDTO != null)
            {
                this.ContactID = recentDTO.ContactID;
                this.FriendTableID = recentDTO.FriendTableID;
                this.GroupID = recentDTO.GroupID;
                this.RoomID = recentDTO.RoomID;
                if (recentDTO.Message != null)
                {
                    if (this.Message == null)
                    {
                        this.Message = new MessageModel(recentDTO.Message, _ForOnlyChatView);
                    }
                    else
                    {
                        this.Message.LoadData(recentDTO.Message);
                    }

                    this.UniqueKey = this.Message.PacketID;
                    this.Time = this.Message.MessageDate;
                    if (this.Message.FriendTableID > 0)
                    {
                        this.ContactType = ChatConstants.TYPE_FRIEND;
                        this.Type = ChatConstants.SUBTYPE_FRIEND_CHAT;
                    }
                    else if (this.Message.GroupID > 0)
                    {
                        this.GroupID = this.GroupID;
                        this.ContactType = ChatConstants.TYPE_GROUP;
                        this.Type = ChatConstants.SUBTYPE_GROUP_CHAT;
                    }
                    else if (!String.IsNullOrWhiteSpace(this.Message.RoomID))
                    {
                        this.ContactType = ChatConstants.TYPE_ROOM;
                        this.Type = ChatConstants.SUBTYPE_ROOM_CHAT;
                    }

                    this.MinViewHeight = ChatHelpers.CHAT_VIEW_HEIGHT[(int)Message.MessageType][0] + ChatHelpers.CHAT_VIEW_HEIGHT[(int)Message.MessageType][1];
                    this.PrevViewHeight = this.MinViewHeight;
                }
                else if (recentDTO.CallLog != null)
                {
                    if (this.CallLog == null)
                    {
                        this.CallLog = new CallLogModel(recentDTO.CallLog, _ForOnlyChatView);
                    }
                    else
                    {
                        this.CallLog.LoadData(recentDTO.CallLog);
                    }

                    this.UniqueKey = this.CallLog.CallID;
                    this.Time = this.CallLog.CallingTime;
                    this.ContactType = ChatConstants.TYPE_FRIEND;
                    this.Type = ChatConstants.SUBTYPE_CALL_LOG;
                    this.MinViewHeight = ChatHelpers.CALL_VIEW_HEIGHT;
                    this.PrevViewHeight = this.MinViewHeight;
                }
                else if (recentDTO.Activity != null)
                {
                    if (this.Activity == null)
                    {
                        this.Activity = new ActivityModel(recentDTO.Activity);
                    }
                    else
                    {
                        this.Activity.LoadData(recentDTO.Activity);
                    }

                    this.UniqueKey = this.Activity.PacketID;
                    this.Time = this.Activity.UpdateTime;
                    this.ContactType = ChatConstants.TYPE_GROUP;
                    this.Type = ChatConstants.SUBTYPE_GROUP_ACTIVITY;
                    this.MinViewHeight = ChatHelpers.ACTIVITY_VIEW_HEIGHT;
                    this.PrevViewHeight = this.MinViewHeight;
                }
                else if (recentDTO.Time > 0)
                {
                    this.UniqueKey = String.Empty;
                    this.Time = recentDTO.Time;
                    this.ContactType = ChatConstants.TYPE_COMMON;
                    this.Type = ChatConstants.SUBTYPE_DATE_TITLE;
                    this.MinViewHeight = ChatHelpers.TIME_VIEW_HEIGHT;
                    this.PrevViewHeight = this.MinViewHeight;
                }

                this.DateTime = ModelUtility.GetLocalDateTime(this.Time);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods
    }
}

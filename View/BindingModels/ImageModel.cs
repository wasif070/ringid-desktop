﻿using Models.Entity;
using System.ComponentModel;
using System.Windows.Input;
using View.Utility;
using View.Utility.Feed;
using View.ViewModel;
using Newtonsoft.Json.Linq;
using Models.Constants;
using System;
using System.Windows.Media;
using System.Collections.ObjectModel;
using View.Utility.DataContainer;

namespace View.BindingModels
{
    public class ImageModel : INotifyPropertyChanged
    {
        #region Constructor

        public ImageModel()
        {
            CurrentInstance = this;
        }

        //public ImageModel(ImageDTO imageDTO)
        //{
        //    CurrentInstance = this;
        //    LoadData(imageDTO);
        //}
        public ImageModel(JObject jObject)
        {
            CurrentInstance = this;
            LoadData(jObject);
            if (NewsFeedId != Guid.Empty)
            {
                FeedModel fm = null;
                if (FeedDataContainer.Instance.FeedModels.TryGetValue(NewsFeedId, out fm))
                    LikeCommentShare = fm.LikeCommentShare;
            }
        }
        #endregion Constructor

        #region Property

        private BaseUserProfileModel _UserShortInfoModel;
        public BaseUserProfileModel UserShortInfoModel
        {
            get { return _UserShortInfoModel; }
            set
            {
                if (value == _UserShortInfoModel)
                    return;
                _UserShortInfoModel = value;
                //this.OnPropertyChanged("UserShortInfoModel");
            }
        }

        private FeedImageInfo _FeedImageInfoModel;
        public FeedImageInfo FeedImageInfoModel
        {
            get
            {
                return _FeedImageInfoModel;
            }
            set
            {
                if (value == _FeedImageInfoModel)
                    return;

                _FeedImageInfoModel = value;
                this.OnPropertyChanged("FeedImageInfoModel");
            }
        }

        private long _FriendUserTableID;
        public long FriendUserTableID
        {
            get
            {
                return _FriendUserTableID;
            }
            set
            {
                if (value == _FriendUserTableID)
                    return;

                _FriendUserTableID = value;
                this.OnPropertyChanged("FriendUserTableID");
            }
        }

        private Guid _NewsFeedId;
        public Guid NewsFeedId
        {
            get
            {
                return _NewsFeedId;
            }
            set
            {
                _NewsFeedId = value;
            }
        }

        private Guid _AlbumId;
        public Guid AlbumId
        {
            get
            {
                return _AlbumId;
            }
            set
            {
                if (value == _AlbumId)
                    return;

                _AlbumId = value;
                this.OnPropertyChanged("AlbumId");
            }
        }

        private string _ImageUrl;
        public string ImageUrl
        {
            get
            {
                return _ImageUrl;
            }
            set
            {
                if (value == _ImageUrl)
                    return;

                _ImageUrl = value;
                this.OnPropertyChanged("ImageUrl");
            }
        }

        private string _ImageLocation;
        public string ImageLocation
        {
            get
            {
                return _ImageLocation;
            }
            set
            {
                if (value == _ImageLocation)
                    return;

                _ImageLocation = value;
                this.OnPropertyChanged("ImageLocation");
            }
        }

        private string _AlbumName;
        public string AlbumName
        {
            get
            {
                return _AlbumName;
            }
            set
            {
                if (value == _AlbumName)
                    return;

                _AlbumName = value;
                this.OnPropertyChanged("AlbumName");
            }
        }

        private string _Caption = string.Empty;
        public string Caption
        {
            get
            {
                return _Caption;
            }
            set
            {
                if (value == _Caption)
                    return;

                _Caption = value;
                this.OnPropertyChanged("Caption");
            }
        }
        private LikeCommentShareModel _LikeCommentShare;
        public LikeCommentShareModel LikeCommentShare
        {
            get { return _LikeCommentShare; }
            set
            {
                if (value == _LikeCommentShare)
                    return;
                _LikeCommentShare = value;
                this.OnPropertyChanged("LikeCommentShare");
            }
        }
        private Guid _ImageId;
        public Guid ImageId
        {
            get
            {
                return _ImageId;
            }
            set
            {
                if (value == _ImageId)
                    return;

                _ImageId = value;
                this.OnPropertyChanged("ImageId");
            }
        }

        private long _Time;
        public long Time
        {
            get
            {
                return _Time;
            }
            set
            {
                if (value == _Time)
                    return;
                _Time = value;
                this.OnPropertyChanged("Time");
            }
        }

        private double _ImageHeight;
        public double ImageHeight
        {
            get
            {
                return _ImageHeight;
            }
            set
            {
                if (value == _ImageHeight)
                    return;
                _ImageHeight = value;
                this.OnPropertyChanged("ImageHeight");
            }
        }

        private double _ImageWidth;
        public double ImageWidth
        {
            get
            {
                return _ImageWidth;
            }
            set
            {
                if (value == _ImageWidth)
                    return;
                _ImageWidth = value;
                this.OnPropertyChanged("ImageWidth");
            }
        }

        private int _ImageType;
        public int ImageType
        {
            get
            {
                return _ImageType;
            }
            set
            {
                if (value == _ImageType)
                    return;
                _ImageType = value;
                this.OnPropertyChanged("ImageType");
            }
        }

        private int _Serial;
        public int Serial
        {
            get
            {
                return _Serial;
            }
            set
            {
                if (value == _Serial)
                    return;
                _Serial = value;
                this.OnPropertyChanged("Serial");
            }
        }

        private int _Privacy;
        public int Privacy
        {
            get { return _Privacy; }
            set
            {
                if (value == _Privacy)
                    return;

                _Privacy = value;
                this.OnPropertyChanged("Privacy");
            }
        }
        //private long _NumberOfLikes;
        //public long NumberOfLikes
        //{
        //    get
        //    {
        //        return _NumberOfLikes;
        //    }
        //    set
        //    {
        //        if (value == _NumberOfLikes)
        //            return;
        //        _NumberOfLikes = value;
        //        this.OnPropertyChanged("NumberOfLikes");
        //    }
        //}

        //private int _ILike;
        //public int ILike
        //{
        //    get
        //    {
        //        return _ILike;
        //    }
        //    set
        //    {
        //        if (value == _ILike)
        //            return;
        //        _ILike = value;
        //        this.OnPropertyChanged("ILike");
        //    }
        //}

        //private int _IComment;
        //public int IComment
        //{
        //    get
        //    {
        //        return _IComment;
        //    }
        //    set
        //    {
        //        if (value == _IComment)
        //            return;
        //        _IComment = value;
        //        this.OnPropertyChanged("IComment");
        //    }
        //}

        //private long _NumberOfComments;
        //public long NumberOfComments
        //{
        //    get
        //    {
        //        return _NumberOfComments;
        //    }
        //    set
        //    {
        //        if (value == _NumberOfComments)
        //            return;
        //        _NumberOfComments = value;
        //        this.OnPropertyChanged("NumberOfComments");
        //    }
        //}

        private int _totalImagesInAlbum;
        public int TotalImagesInAlbum
        {
            get
            {
                return _totalImagesInAlbum;
            }
            set
            {
                if (value == _totalImagesInAlbum || value == 0)
                    return;
                _totalImagesInAlbum = value;
                this.OnPropertyChanged("TotalImagesInAlbum");
            }
        }

        private int _TotalImages;
        public int TotalImage
        {
            get
            {
                return _TotalImages;
            }
            set
            {
                if (value == _TotalImages)
                    return;
                _TotalImages = value;
                this.OnPropertyChanged("TotalImages");
            }
        }

        private bool _NoImageFound = false;
        public bool NoImageFound
        {
            get
            {
                return _NoImageFound;
            }
            set
            {
                if (value == _NoImageFound)
                    return;
                _NoImageFound = value;
                this.OnPropertyChanged("NoImageFound");
            }
        }

        private string _MoreImagesCount;
        public string MoreImagesCount
        {
            get
            {
                return _MoreImagesCount;
            }
            set
            {
                if (value == _MoreImagesCount)
                    return;
                _MoreImagesCount = value;
                this.OnPropertyChanged("MoreImagesCount");
            }
        }
        private long _UserTableID;
        public long UserTableID
        {
            get
            {
                return _UserTableID;
            }
            set
            {
                if (value == _UserTableID)
                    return;
                _UserTableID = value;
                this.OnPropertyChanged("UserTableID");
            }
        }
        private long _UserIdentity;
        public long UserIdentity
        {
            get
            {
                return _UserIdentity;
            }
            set
            {
                if (value == _UserIdentity)
                    return;
                _UserIdentity = value;
                this.OnPropertyChanged("UserIdentity");
            }
        }
        private string _FullName;
        public string FullName
        {
            get
            {
                return _FullName;
            }
            set
            {
                if (value == _FullName)
                    return;
                _FullName = value;
                this.OnPropertyChanged("FullName");
            }
        }
        private long _FriendId;
        public long FriendId
        {
            get
            {
                return _FriendId;
            }
            set
            {
                if (value == _FriendId)
                    return;
                _FriendId = value;
                this.OnPropertyChanged("FriendId");
            }
        }
        //  private long _UserIdentity;
        //public long UserIdentity
        //{
        //    get
        //    {
        //        return _UserIdentity;
        //    }
        //    set
        //    {
        //        if (value == _UserIdentity)
        //            return;
        //        _UserIdentity = value;
        //        this.OnPropertyChanged("UserIdentity");
        //    }
        //}

        private bool _IsSelected = false;
        public bool IsSelected
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                _IsSelected = value;
                this.OnPropertyChanged("IsSelected");
            }
        }

        private bool _IsLoadMore = false;
        public bool IsLoadMore
        {
            get
            {
                return _IsLoadMore;
            }
            set
            {
                _IsLoadMore = value;
                this.OnPropertyChanged("IsLoadMore");
            }
        }

        private bool _IsIMG300Downloading = false;
        public bool IsIMG300Downloading
        {
            get
            {
                return _IsIMG300Downloading;
            }
            set
            {
                if (value == _IsIMG300Downloading)
                    return;
                _IsIMG300Downloading = value;
                this.OnPropertyChanged("IsIMG300Downloading");
            }
        }

        private Stretch _StrechValue = Stretch.None;
        public Stretch StrechValue
        {
            get
            {
                return _StrechValue;
            }
            set
            {
                if (value == _StrechValue)
                    return;
                _StrechValue = value;
                this.OnPropertyChanged("StrechValue");
            }
        }
        private bool _IsOpenedInFeed = false;
        public bool IsOpenedInFeed
        {
            get
            {
                return _IsOpenedInFeed;
            }
            set
            {
                if (value == _IsOpenedInFeed)
                    return;
                _IsOpenedInFeed = value;
                this.OnPropertyChanged("IsOpenedInFeed");
            }
        }
        private ImageModel _CurrentInstance;
        public ImageModel CurrentInstance
        {
            get
            {
                return _CurrentInstance;
            }
            set
            {
                if (value == _CurrentInstance)
                    return;
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }
        #endregion

        #region Utility Methods

        public void LoadData(JObject jObject)
        {
            if (jObject[JsonKeys.ImageId] != null)
                this.ImageId = (Guid)jObject[JsonKeys.ImageId];
            if (jObject[JsonKeys.ImageUrl] != null)
                this.ImageUrl = (string)jObject[JsonKeys.ImageUrl];
            if (jObject[JsonKeys.ImageCaption] != null)
                this.Caption = (string)jObject[JsonKeys.ImageCaption];
            if (jObject[JsonKeys.ImageHeight] != null)
                this.ImageHeight = (int)jObject[JsonKeys.ImageHeight];
            if (jObject[JsonKeys.ImageWidth] != null)
                this.ImageWidth = (int)jObject[JsonKeys.ImageWidth];
            if (jObject[JsonKeys.ImageType] != null)
                this.ImageType = (int)jObject[JsonKeys.ImageType];
            if (jObject[JsonKeys.Serial] != null)
                this.Serial = (int)jObject[JsonKeys.Serial];
            //if (jObject[JsonKeys.NumberOfLikes] != null)
            //    this.NumberOfLikes = (long)jObject[JsonKeys.NumberOfLikes];
            //if (jObject[JsonKeys.ILike] != null)
            //    this.ILike = (int)jObject[JsonKeys.ILike];
            //if (jObject[JsonKeys.NumberOfComments] != null)
            //    this.NumberOfComments = (long)jObject[JsonKeys.NumberOfComments];
            //if (jObject[JsonKeys.IComment] != null)
            //    this.IComment = (int)jObject[JsonKeys.IComment];
            if (jObject[JsonKeys.AlbumId] != null)
                this.AlbumId = (Guid)jObject[JsonKeys.AlbumId];
            if (jObject[JsonKeys.AlbumName] != null)
                this.AlbumName = (string)jObject[JsonKeys.AlbumName];
            if (jObject[JsonKeys.Time] != null)
                this.Time = (long)jObject[JsonKeys.Time];
            if (this.Time == 0 && jObject[JsonKeys.UpdateTime] != null)
                this.Time = (long)jObject[JsonKeys.UpdateTime];
            if (jObject[JsonKeys.FriendId] != null)
                this.FriendId = (long)jObject[JsonKeys.FriendId];
            if (jObject[JsonKeys.UserTableID] != null)
                this.UserTableID = (long)jObject[JsonKeys.UserTableID];
            if (jObject[JsonKeys.FullName] != null)
                this.FullName = (string)jObject[JsonKeys.FullName];
            if (jObject[JsonKeys.MediaPrivacy] != null)
                this.Privacy = (int)jObject[JsonKeys.MediaPrivacy];
            if (jObject[JsonKeys.TotalNotificationCount] != null)
            {
                this.TotalImagesInAlbum = (int)jObject[JsonKeys.TotalNotificationCount];
            }
            if (jObject[JsonKeys.ImageDetails] != null)
            {
                JObject imageDetails = (JObject)jObject[JsonKeys.ImageDetails];
                if (imageDetails[JsonKeys.ImageId] != null)
                    this.ImageId = (Guid)imageDetails[JsonKeys.ImageId];
                if (imageDetails[JsonKeys.ImageUrl] != null)
                    this.ImageUrl = (string)imageDetails[JsonKeys.ImageUrl];
                if (imageDetails[JsonKeys.ImageCaption] != null)
                    this.Caption = (string)imageDetails[JsonKeys.ImageCaption];
                if (imageDetails[JsonKeys.ImageHeight] != null)
                    this.ImageHeight = (int)imageDetails[JsonKeys.ImageHeight];
                if (imageDetails[JsonKeys.ImageWidth] != null)
                    this.ImageWidth = (int)imageDetails[JsonKeys.ImageWidth];
                if (imageDetails[JsonKeys.ImageType] != null)
                    this.ImageType = (int)imageDetails[JsonKeys.ImageType];
                if (imageDetails[JsonKeys.Serial] != null)
                    this.Serial = (int)imageDetails[JsonKeys.Serial];
                //if (imageDetails[JsonKeys.NumberOfLikes] != null)
                //    this.NumberOfLikes = (long)imageDetails[JsonKeys.NumberOfLikes];
                //if (imageDetails[JsonKeys.ILike] != null)
                //    this.ILike = (int)imageDetails[JsonKeys.ILike];
                //if (imageDetails[JsonKeys.NumberOfComments] != null)
                //    this.NumberOfComments = (long)imageDetails[JsonKeys.NumberOfComments];
                //if (imageDetails[JsonKeys.IComment] != null)
                //    this.IComment = (int)imageDetails[JsonKeys.IComment];
                if (imageDetails[JsonKeys.AlbumId] != null)
                    this.AlbumId = (Guid)imageDetails[JsonKeys.AlbumId];
                if (imageDetails[JsonKeys.AlbumName] != null)
                    this.AlbumName = (string)imageDetails[JsonKeys.AlbumName];
                if (imageDetails[JsonKeys.Time] != null)
                    this.Time = (long)imageDetails[JsonKeys.Time];
                if (this.Time == 0 && imageDetails[JsonKeys.UpdateTime] != null)
                    this.Time = (long)imageDetails[JsonKeys.UpdateTime];
                if (imageDetails[JsonKeys.FriendId] != null)
                    this.FriendId = (long)imageDetails[JsonKeys.FriendId];
                if (imageDetails[JsonKeys.UserTableID] != null)
                    this.UserTableID = (long)imageDetails[JsonKeys.UserTableID];
                if (imageDetails[JsonKeys.FullName] != null)
                    this.FullName = (string)imageDetails[JsonKeys.FullName];
                if (imageDetails[JsonKeys.MediaPrivacy] != null)
                    this.Privacy = (int)imageDetails[JsonKeys.MediaPrivacy];
                if (imageDetails[JsonKeys.TotalNotificationCount] != null)
                {
                    this.TotalImagesInAlbum = (int)imageDetails[JsonKeys.TotalNotificationCount];
                }
            }
            if (LikeCommentShare != null)
                LikeCommentShare.LoadData(jObject);
        }

        //public void LoadData(ImageDTO imageDTO)
        //{
        //    if (imageDTO != null)
        //    {
        //        this.FriendUserTableID = imageDTO.FriendTableId;
        //        this.AlbumId = imageDTO.AlbumId;
        //        this.AlbumName = imageDTO.AlbumName;
        //        this.ImageUrl = imageDTO.ImageUrl;
        //        this.ImageId = imageDTO.ImageId;
        //        this.Caption = imageDTO.Caption;
        //        this.Time = imageDTO.Time;
        //        this.ImageHeight = imageDTO.ImageHeight;
        //        this.ImageWidth = imageDTO.ImageWidth;
        //        this.ImageType = imageDTO.ImageType;
        //        this.NumberOfLikes = imageDTO.NumberOfLikes;
        //        this.ILike = imageDTO.iLike;
        //        this.NumberOfComments = imageDTO.NumberOfComments;
        //        this.IComment = imageDTO.iComment;
        //        //this.TotalImagesInAlbum = imageDTO.TotalImagesInAlbum;
        //        this.TotalImage = imageDTO.TotalImage;
        //        this.Privacy = imageDTO.Privacy;
        //        if (imageDTO.NewsFeedId > 0)
        //            this.NewsFeedId = imageDTO.NewsFeedId;
        //        //this.FullName = imageDTO.FullName;
        //        //this.UserIdentity = imageDTO.UserIdentity;
        //        if (UserShortInfoModel == null) this.UserShortInfoModel = RingIDViewModel.Instance.GetUserShortInfoModelNotNullable(imageDTO.UserTableID, 0, imageDTO.FullName);
        //        //CurrentInstance = this;
        //    }
        //}
        public void SetMissingInfo(JObject jObject)
        {
            //Currently set uid and fullname
            if (this.UserIdentity <= 0 && string.IsNullOrEmpty(this.FullName))
            {
                if (jObject[JsonKeys.UserIdentity] != null)
                {
                    this.UserIdentity = (long)jObject[JsonKeys.UserIdentity];
                }
                if (jObject[JsonKeys.FullName] != null)
                {
                    this.FullName = (string)jObject[JsonKeys.FullName];
                }
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

        #region "Image Command"
        private ICommand _feedImageRightButtonCommand;
        public ICommand FeedImageRightButtonCommand
        {
            get
            {
                if (_feedImageRightButtonCommand == null)
                {
                    _feedImageRightButtonCommand = new RelayCommand(param => ImageUtility.OnFeedImageLeftClick(this.NoImageFound, this.ImageId, this.NewsFeedId));
                }
                return _feedImageRightButtonCommand;
            }
        }
        //private ICommand _feedImageRightButtonCommand;
        //public ICommand FeedImageRightButtonCommand
        //{
        //    get
        //    {
        //        if (_feedImageRightButtonCommand == null)
        //        {
        //            _feedImageRightButtonCommand = new RelayCommand(param => ImageUtility.OnFeedImageLeftClick(param, this.NoImageFound, this.ImageId));
        //        }
        //        return _feedImageRightButtonCommand;
        //    }
        //}
        private ICommand _portalImageCommand;
        public ICommand PortalImageCommand
        {
            get
            {
                if (_portalImageCommand == null)
                {
                    _portalImageCommand = new RelayCommand(param => ImageUtility.ShowImageViewFromPortalPages((ObservableCollection<ImageModel>)param, this.ImageId, true));
                }
                return _portalImageCommand;
            }
        }
        //private void OnFeedImageLeftClick(object parameter)
        //{
        //    if (!this.NoImageFound)
        //    {
        //        if (UCGuiRingID.Instance != null)
        //        {
        //            if (parameter is List<ImageModel>)
        //            {

        //                List<ImageModel> model = (List<ImageModel>)parameter;
        //                int SelectedIndex1 = 0;
        //                List<ImageModel> _TempList = new List<ImageModel>();
        //                if (model.Count > 0)
        //                {
        //                    long nfid = model.ElementAt(0).NewsFeedId;

        //                    int totalImageInAlbum = 1;
        //                    if (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS.ContainsKey(nfid) && NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[nfid].TotalImage > model.Count)
        //                    {
        //                        totalImageInAlbum = NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS[nfid].TotalImage;
        //                        new ShowContinueImageFeed(nfid);
        //                    }
        //                    else
        //                    {
        //                        totalImageInAlbum = model.Count;
        //                    }
        //                    foreach (ImageModel feedImaginfo in model)
        //                    {
        //                        feedImaginfo.TotalImagesInAlbum = totalImageInAlbum;
        //                        _TempList.Add(feedImaginfo);
        //                        if (feedImaginfo.ImageId == this.ImageId)
        //                        {
        //                            if (UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView == null)
        //                            {
        //                                UCGuiRingID.Instance.ucBasicImageViewWrapper.AddUCBasicImageViewToWrapperPanel();
        //                            }
        //                            UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.SelectedIndex1 = SelectedIndex1;
        //                        }
        //                        SelectedIndex1++;
        //                    }
        //                }
        //                UCGuiRingID.Instance.ucBasicImageViewWrapper.SetUserModel(_TempList, _TempList[UCGuiRingID.Instance.ucBasicImageViewWrapper.ucBasicImageView.SelectedIndex1]);
        //            }
        //        }
        //    }
        //}
        #endregion
    }
}

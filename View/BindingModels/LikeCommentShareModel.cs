﻿using Models.Constants;
using Newtonsoft.Json.Linq;
using System.ComponentModel;

namespace View.BindingModels
{
    public class LikeCommentShareModel : INotifyPropertyChanged
    {
        public LikeCommentShareModel()
        {
            CurrentInstance = this;
        }
        public void LoadData(JObject jObject)
        {
            if (jObject[JsonKeys.NumberOfComments] != null)
            {
                this.NumberOfComments = (long)jObject[JsonKeys.NumberOfComments];
            }
            else if (jObject[JsonKeys.CommentCount] != null)
            {
                this.NumberOfComments = (long)jObject[JsonKeys.CommentCount];
            }
            if (jObject[JsonKeys.NumberOfLikes] != null)
            {
                this.NumberOfLikes = (long)jObject[JsonKeys.NumberOfLikes];
            }
            else if (jObject[JsonKeys.LikeCount] != null)
            {
                this.NumberOfLikes = (long)jObject[JsonKeys.LikeCount];
            }
            if (jObject[JsonKeys.NumberOfShares] != null)
            {
                this.NumberOfShares = (long)jObject[JsonKeys.NumberOfShares];
            }
            if (jObject[JsonKeys.ILike] != null)
            {
                this.ILike = (short)jObject[JsonKeys.ILike];
            }
            if (jObject[JsonKeys.IComment] != null)
            {
                this.IComment = (short)jObject[JsonKeys.IComment];
            }
            if (jObject[JsonKeys.IShare] != null)
            {
                this.IShare = (short)jObject[JsonKeys.IShare];
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private LikeCommentShareModel _CurrentInstance;
        public LikeCommentShareModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                if (value == _CurrentInstance)
                    return;
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }
        private long _NumberOfComments;
        public long NumberOfComments
        {
            get { return _NumberOfComments; }
            set
            {
                if (value == _NumberOfComments)
                    return;
                _NumberOfComments = value;
                this.OnPropertyChanged("NumberOfComments");
            }
        }
        private long _NumberOfShares;
        public long NumberOfShares
        {
            get { return _NumberOfShares; }
            set
            {
                if (value == _NumberOfShares)
                    return;
                _NumberOfShares = value;
                this.OnPropertyChanged("NumberOfShares");
            }
        }
        private long _NumberOfLikes;
        public long NumberOfLikes
        {
            get { return _NumberOfLikes; }
            set
            {
                if (value == _NumberOfLikes)
                    return;
                _NumberOfLikes = value;
                this.OnPropertyChanged("NumberOfLikes");
            }
        }
        private short _ILike;
        public short ILike
        {
            get { return _ILike; }
            set
            {
                if (value == _ILike)
                    return;
                _ILike = value;
                this.OnPropertyChanged("ILike");
            }
        }
        private short _IComment;
        public short IComment
        {
            get { return _IComment; }
            set
            {
                if (value == _IComment)
                    return;
                _IComment = value;
                this.OnPropertyChanged("IComment");
            }
        }
        private short _IShare;
        public short IShare
        {
            get { return _IShare; }
            set
            {
                if (value == _IShare)
                    return;
                _IShare = value;
                this.OnPropertyChanged("IShare");
            }
        }
    }
}

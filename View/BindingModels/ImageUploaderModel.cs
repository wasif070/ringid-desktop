﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

namespace View.BindingModels
{
    public class ImageUploaderModel : INotifyPropertyChanged
    {
        #region Constructor

        public ImageUploaderModel()
        {
            CurrentInstance = this;
        }

        #endregion Constructor

        #region Property
        public Guid ImageId = Guid.Empty;
        private BitmapImage _CopiedBitmapImage;
        public BitmapImage CopiedBitmapImage
        {
            get { return _CopiedBitmapImage; }
            set
            {
                if (value == _CopiedBitmapImage)
                    return;

                _CopiedBitmapImage = value;
            }
        }
        private int _Height;
        public int Height
        {
            get { return _Height; }
            set
            {
                if (value == _Height)
                    return;

                _Height = value;
                this.OnPropertyChanged("Height");
            }
        }
        private int _Width;
        public int Width
        {
            get { return _Width; }
            set
            {
                if (value == _Width)
                    return;

                _Width = value;
                this.OnPropertyChanged("Width");
            }
        }
        private int _ImageType;
        public int ImageType
        {
            get { return _ImageType; }
            set
            {
                if (value == _ImageType)
                    return;

                _ImageType = value;
                this.OnPropertyChanged("ImageType");
            }
        }
        private string _ImageUrl = null;
        public string ImageUrl
        {
            get { return _ImageUrl; }
            set
            {
                if (value == _ImageUrl)
                    return;

                _ImageUrl = value;
                this.OnPropertyChanged("ImageUrl");
            }
        }
        private bool _IsUploadedInImageServer = false;
        public bool IsUploadedInImageServer
        {
            get { return _IsUploadedInImageServer; }
            set
            {
                if (value == _IsUploadedInImageServer)
                    return;

                _IsUploadedInImageServer = value;
                this.OnPropertyChanged("IsUploadedInImageServer");
            }
        }
        private bool _IsUploading = false;
        public bool IsUploading
        {
            get { return _IsUploading; }
            set
            {
                if (value == _IsUploading)
                    return;

                _IsUploading = value;
                this.OnPropertyChanged("IsUploading");
            }
        }
        private string _FilePath = string.Empty;
        public string FilePath
        {
            get { return _FilePath; }
            set
            {
                if (value == _FilePath)
                    return;

                _FilePath = value;
                this.OnPropertyChanged("FilePath");
            }
        }
        private long _FileSize;
        public long FileSize
        {
            get { return _FileSize; }
            set
            {
                if (value == _FileSize)
                    return;

                _FileSize = value;
                this.OnPropertyChanged("FileSize");
            }
        }
        private bool _IsFromUpload = true;
        public bool IsFromUpload
        {
            get { return _IsFromUpload; }
            set
            {
                if (value == _IsFromUpload)
                    return;

                _IsFromUpload = value;
                this.OnPropertyChanged("IsFromUpload");
            }
        }
        private bool _CaptionEnabled = true;
        public bool CaptionEnabled
        {
            get { return _CaptionEnabled; }
            set
            {
                if (value == _CaptionEnabled)
                    return;

                _CaptionEnabled = value;
                this.OnPropertyChanged("CaptionEnabled");
            }
        }
        private bool _RemoveEnabled = true;
        public bool RemoveEnabled
        {
            get { return _RemoveEnabled; }
            set
            {
                if (value == _RemoveEnabled)
                    return;

                _RemoveEnabled = value;
                this.OnPropertyChanged("RemoveEnabled");
            }
        }
        private string _ImageCaption = string.Empty;
        public string ImageCaption
        {
            get { return _ImageCaption; }
            set
            {
                if (value == _ImageCaption)
                    return;

                _ImageCaption = value;
                this.OnPropertyChanged("ImageCaption");
            }
        }
        private ImageUploaderModel _CurrentInstance;
        public ImageUploaderModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                if (value == _CurrentInstance)
                    return;
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }

        private int _Pecentage;
        public int Pecentage
        {
            get { return _Pecentage; }
            set
            {
                if (value == _Pecentage)
                    return;

                _Pecentage = value;
                this.OnPropertyChanged("Pecentage");
            }
        }

        //private Bitmap _Img;
        //public Bitmap Img
        //{
        //    get { return _Img; }
        //    set
        //    {
        //        if (value == _Img)
        //            return;

        //        _Img = value;
        //        this.OnPropertyChanged("Img");
        //    }
        //}
        #endregion Property

        #region Utility Method

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Method
    }
}

﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace View.BindingModels
{
    public class WalletContributorModel : BaseUserProfileModel
    {
        private int coinQuantity;
        public int CoinQuantity
        {
            get { return coinQuantity; }
            set { coinQuantity = value; this.OnPropertyChanged("CoinQuantity"); }
        }

        #region key
        //public const string keyFromUserID = "fromUserId";
        //public const string keyQuantity = "quantity";
        #endregion

        public static WalletContributorModel LoadDataFromJson(Newtonsoft.Json.Linq.JObject jObject)
        {
            WalletContributorModel model = new WalletContributorModel();
            try
            {
                if (jObject[WalletConstants.RSP_KEY_CONTRIBUTOR_USER_ID] != null)
                {
                    model.UserTableID = (long)jObject[WalletConstants.RSP_KEY_CONTRIBUTOR_USER_ID];
                }
                if (jObject[JsonKeys.UserIdentity] != null)
                {
                    model.UserIdentity = (long)jObject[JsonKeys.UserIdentity];
                }
                if (jObject[JsonKeys.FullName] != null)
                {
                    model.FullName = (string)jObject[JsonKeys.FullName];
                }
                if (jObject[JsonKeys.ProfileImage] != null)
                {
                    model.ProfileImage = (string)jObject[JsonKeys.ProfileImage];
                }
                if (jObject[WalletConstants.RSP_KEY_QUANTITY] != null)
                {
                    model.CoinQuantity = (int)jObject[WalletConstants.RSP_KEY_QUANTITY];
                }
            }
            catch (Exception)
            {
                model.UserTableID = -1;
            }
            return model;
        }
        
    }
}

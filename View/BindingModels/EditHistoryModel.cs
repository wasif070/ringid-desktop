﻿using System.Collections.ObjectModel;
using System.ComponentModel;

namespace View.BindingModels
{
    public class EditHistoryModel : INotifyPropertyChanged
    {

        private string _EditedValue;
        public string EditedValue
        {
            get { return _EditedValue; }
            set
            {
                if (value == _EditedValue)
                    return;

                _EditedValue = value;
                this.OnPropertyChanged("EditedValue");
            }
        }

        private int _EditedId;
        public int EditedId
        {
            get { return _EditedId; }
            set
            {
                if (value == _EditedId)
                    return;

                _EditedId = value;
                this.OnPropertyChanged("EditedId");
            }
        }

        private long _EditedTime;
        public long EditedTime
        {
            get { return _EditedTime; }
            set
            {
                if (value == _EditedTime)
                    return;

                _EditedTime = value;
                this.OnPropertyChanged("EditedTime");
            }
        }

        private bool _IsStatus;//status or comment
        public bool IsStatus
        {
            get { return _IsStatus; }
            set
            {
                if (value == _IsStatus)
                    return;

                _IsStatus = value;
                this.OnPropertyChanged("IsStatus");
            }
        }

        private ObservableCollection<TaggedUserModel> _TaggedUsers;
        public ObservableCollection<TaggedUserModel> TaggedUsers
        {
            get { return _TaggedUsers; }
            set
            {
                if (value == _TaggedUsers)
                    return;
                _TaggedUsers = value;
                this.OnPropertyChanged("TaggedUsers");
            }
        }
        private BaseUserProfileModel _ShortInfoModel;
        public BaseUserProfileModel ShortInfoModel
        {
            get { return _ShortInfoModel; }
            set
            {
                if (value == _ShortInfoModel)
                    return;
                _ShortInfoModel = value;
                //this.OnPropertyChanged("ShortInfoModel");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

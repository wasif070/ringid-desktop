﻿using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System;
using System.ComponentModel;

namespace View.BindingModels
{
    public class NewsPortalModel : BaseUserProfileModel
    {
        public NewsPortalModel()
        {
            CurrentInstance = this;
        }
        public void LoadData(JObject jObject)
        {
            if (jObject[JsonKeys.FullName] != null)
            {
                this.FullName = (string)jObject[JsonKeys.FullName];
            }
            if (jObject[JsonKeys.ProfileImage] != null)
            {
                this.ProfileImage = (string)jObject[JsonKeys.ProfileImage];
            }
            if (jObject[JsonKeys.CoverImage] != null)
            {
                this.CoverImage = (string)jObject[JsonKeys.CoverImage];
            }
            if (UserTableID == 0 && jObject[JsonKeys.UserTableID] != null)
            {
                this.UserTableID = (long)jObject[JsonKeys.UserTableID];
            }
            if (UserTableID == 0 && jObject[JsonKeys.PageId] != null)
            {
                this.UserTableID = (long)jObject[JsonKeys.PageId];
            }
            if (jObject[JsonKeys.UserIdentity] != null)
            {
                this.UserIdentity = (long)jObject[JsonKeys.UserIdentity];
            }
            if (jObject[JsonKeys.IsUserFeedHidden] != null)
            {
                this.IsProfileHidden = (bool)jObject[JsonKeys.IsUserFeedHidden];
            }
            if (jObject[JsonKeys.NewsPortalSlogan] != null)
            {
                this.PortalSlogan = (string)jObject[JsonKeys.NewsPortalSlogan];
            }
            if (jObject[JsonKeys.PageId] != null)
            {
                this.PortalId = (long)jObject[JsonKeys.PageId];
            }
            if (jObject[JsonKeys.Subscribe] != null)
            {
                this.IsSubscribed = (bool)jObject[JsonKeys.Subscribe];
            }
            if (jObject[JsonKeys.SubscriberCount] != null)
            {
                this.SubscriberCount = (int)jObject[JsonKeys.SubscriberCount];
            }
            if (jObject[JsonKeys.NewsPortalCatName] != null)
            {
                this.PortalCatName = (string)jObject[JsonKeys.NewsPortalCatName];
            }
            if (jObject[JsonKeys.NewsPortalCatId] != null)
            {
                this.PortalCatId = (Guid)jObject[JsonKeys.NewsPortalCatId];
            }
            if (jObject[JsonKeys.NewsPortalDTO] != null)
            {
                JObject NpDto = (JObject)jObject[JsonKeys.NewsPortalDTO];
                if (NpDto[JsonKeys.NewsPortalSlogan] != null)
                {
                    this.PortalSlogan = (string)NpDto[JsonKeys.NewsPortalSlogan];
                }
                if (NpDto[JsonKeys.PageId] != null)
                {
                    this.PortalId = (long)NpDto[JsonKeys.PageId];
                }
                if (NpDto[JsonKeys.Subscribe] != null)
                {
                    this.IsSubscribed = (bool)NpDto[JsonKeys.Subscribe];
                }
                if (NpDto[JsonKeys.NewsPortalCatName] != null)
                {
                    this.PortalCatName = (string)NpDto[JsonKeys.NewsPortalCatName];
                }
                if (NpDto[JsonKeys.NewsPortalCatId] != null)
                {
                    this.PortalCatId = (Guid)NpDto[JsonKeys.NewsPortalCatId];
                }
                if (NpDto[JsonKeys.SubscriberCount] != null)
                {
                    this.SubscriberCount = (int)NpDto[JsonKeys.SubscriberCount];
                }
                if (NpDto[JsonKeys.FullName] != null)
                {
                    this.FullName = (string)NpDto[JsonKeys.FullName];
                }
                if (UserTableID == 0 && NpDto[JsonKeys.UserTableID] != null)
                {
                    this.UserTableID = (long)NpDto[JsonKeys.UserTableID];
                }
                if (NpDto[JsonKeys.UserIdentity] != null)
                {
                    this.UserIdentity = (long)NpDto[JsonKeys.UserIdentity];
                }
                if (NpDto[JsonKeys.ProfileImage] != null)
                {
                    this.ProfileImage = (string)NpDto[JsonKeys.ProfileImage];
                }
            }
        }
        //public void LoadData(NewsPortalDTO dto)
        //{
        //    PortalName = dto.PortalName;
        //    PortalId = dto.PortalId;
        //    UserTableID = dto.UserTableID;
        //    SubscriberCount = dto.SubscriberCount;
        //    ProfileImage = dto.ProfileImage;
        //    PortalSlogan = dto.PortalSlogan;
        //    PortalCatName = dto.PortalCatName;
        //    PortalCatId = dto.PortalCatId;
        //    IsSubscribed = dto.IsSubscribed;
        //    CoverImage = dto.CoverImage;
        //    UserIdentity = dto.RingID;
        //    IsProfileHidden = dto.IsProfileHidden;
        //}
        //private string _PortalName;
        //public string PortalName
        //{
        //    get { return _PortalName; }
        //    set
        //    {
        //        if (value == _PortalName)
        //            return;
        //        _PortalName = value;
        //        this.OnPropertyChanged("PortalName");
        //    }
        //}
        private long _PortalId;
        public long PortalId
        {
            get { return _PortalId; }
            set
            {
                if (value == _PortalId)
                    return;

                _PortalId = value;
                this.OnPropertyChanged("PortalId");
            }
        }
        //private bool _IsProfileHidden;
        //public bool IsProfileHidden
        //{
        //    get { return _IsProfileHidden; }
        //    set
        //    {
        //        if (value == _IsProfileHidden)
        //            return;

        //        _IsProfileHidden = value;
        //        this.OnPropertyChanged("IsProfileHidden");
        //    }
        //}
        private bool _IsSubscribed;
        public bool IsSubscribed
        {
            get { return _IsSubscribed; }
            set
            {
                if (value == _IsSubscribed)
                    return;
                _IsSubscribed = value;
                this.OnPropertyChanged("IsSubscribed");
            }
        }
        private string _PortalSlogan;
        public string PortalSlogan
        {
            get { return _PortalSlogan; }
            set
            {
                if (value == _PortalSlogan)
                    return;
                _PortalSlogan = value;
                this.OnPropertyChanged("PortalSlogan");
            }
        }
        //private long _UserTableId;
        //public long UserTableID
        //{
        //    get { return _UserTableId; }
        //    set
        //    {
        //        if (value == _UserTableId)
        //            return;

        //        _UserTableId = value;
        //        this.OnPropertyChanged("UserTableID");
        //    }
        //}
        //private long _UserIdentity;
        //public long UserIdentity
        //{
        //    get { return _UserIdentity; }
        //    set
        //    {
        //        if (value == _UserIdentity)
        //            return;

        //        _UserIdentity = value;
        //        this.OnPropertyChanged("UserIdentity");
        //    }
        //}
        private Guid _PortalCatId;
        public Guid PortalCatId
        {
            get { return _PortalCatId; }
            set
            {
                if (value == _PortalCatId)
                    return;

                _PortalCatId = value;
                this.OnPropertyChanged("PortalCatId");
            }
        }
        private string _PortalCatName;
        public string PortalCatName
        {
            get { return _PortalCatName; }
            set
            {
                if (value == _PortalCatName)
                    return;
                _PortalCatName = value;
                this.OnPropertyChanged("PortalCatName");
            }
        }
        private int _SubscriberCount;
        public int SubscriberCount
        {
            get { return _SubscriberCount; }
            set
            {
                if (value == _SubscriberCount)
                    return;

                _SubscriberCount = value;
                this.OnPropertyChanged("SubscriberCount");
            }
        }

        //private string _ProfileImage;
        //public string ProfileImage
        //{
        //    get { return _ProfileImage; }
        //    set
        //    {
        //        if (value == _ProfileImage)
        //            return;

        //        _ProfileImage = value;
        //        this.OnPropertyChanged("ProfileImage");
        //    }
        //}
        private string _CoverImage;
        public string CoverImage
        {
            get { return _CoverImage; }
            set
            {
                if (value == _CoverImage)
                    return;

                _CoverImage = value;
                this.OnPropertyChanged("CoverImage");
            }
        }
        private bool _IsRightMarginOff;
        public bool IsRightMarginOff
        {
            get { return _IsRightMarginOff; }
            set
            {
                if (value == _IsRightMarginOff)
                    return;

                _IsRightMarginOff = value;
                this.OnPropertyChanged("IsRightMarginOff");
            }
        }
        //private NewsPortalModel _CurrentInstance;
        //public NewsPortalModel CurrentInstance
        //{
        //    get { return _CurrentInstance; }
        //    set
        //    {
        //        if (value == _CurrentInstance)
        //            return;

        //        _CurrentInstance = value;
        //        this.OnPropertyChanged("CurrentInstance");
        //    }
        //}
        private bool _IsContextMenuOpened = false;
        public bool IsContextMenuOpened
        {
            get { return _IsContextMenuOpened; }
            set
            {
                System.Diagnostics.Debug.WriteLine("ContextMenu ....=>");
                if (value == _IsContextMenuOpened) return;
                _IsContextMenuOpened = value;
                this.OnPropertyChanged("IsContextMenuOpened");
            }
        }
        //public event PropertyChangedEventHandler PropertyChanged;
        //public void OnPropertyChanged(string propertyName)
        //{
        //    PropertyChangedEventHandler handler = PropertyChanged;
        //    if (handler != null)
        //    {
        //        handler(this, new PropertyChangedEventArgs(propertyName));
        //    }
        //}
    }
}

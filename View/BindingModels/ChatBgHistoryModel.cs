﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace View.BindingModels
{
    public class ChatBgHistoryModel : INotifyPropertyChanged
    {
        #region Constructor

        public ChatBgHistoryModel()
        {
            _CurrentInstance = this;
        }

        public ChatBgHistoryModel(ChatBgImageDTO bgDTO)
        {
            _CurrentInstance = this;
            LoadData(bgDTO);
        }

        #endregion Constructor

        #region Property


        private ChatBgHistoryModel _CurrentInstance;
        public ChatBgHistoryModel CurrentInstance
        {
            get { return _CurrentInstance; }
        }

        private int _Id;
        public int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
                this.OnPropertyChanged("Id");
            }
        }

        private string _ChatBgUrl;
        public string ChatBgUrl
        {
            get { return _ChatBgUrl; }
            set
            {
                if (value == _ChatBgUrl)
                    return;

                _ChatBgUrl = value;
                this.OnPropertyChanged("ChatBgUrl");
            }
        }

         private long _UpdateTime;
         public long UpdateTime
         {
             get { return _UpdateTime; }
             set
             {
                 if (value == _UpdateTime)
                     return;

                 _UpdateTime = value;
                 this.OnPropertyChanged("UpdateTime");
             }
         }

         private bool _IsChecked;
         public bool IsChecked
         {
             get { return _IsChecked; }
             set
             {
                 if (value == _IsChecked)
                     return;

                 _IsChecked = value;
                 this.OnPropertyChanged("IsChecked");
             }
         }

         private bool _IsDownloaded;
         public bool IsDownloaded
         {
             get { return _IsDownloaded; }
             set
             {
                 if (value == _IsDownloaded)
                     return;

                 _IsDownloaded = value;
                 this.OnPropertyChanged("IsDownloaded");
             }
         }

         private string _ThemeColor;
         public string ThemeColor
         {
             get { return _ThemeColor; }
             set
             {
                 if (value == _ThemeColor)
                     return;

                 _ThemeColor = value;
                 this.OnPropertyChanged("ThemeColor");
             }
         }

         private bool _IsAction;
         public bool IsAction
         {
             get { return _IsAction; }
             set
             {
                 _IsAction = value;
                 this.OnPropertyChanged("IsAction");
             }
         }

        #endregion Property

        #region Utility Method

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void LoadData(ChatBgImageDTO bgDTO)
        {
            if (bgDTO != null)
            {
                this.Id = bgDTO.id;
                this.ChatBgUrl = bgDTO.name;
                this.ThemeColor = bgDTO.themeColor;
                this.IsDownloaded = bgDTO.IsDownloaded;
                this.UpdateTime = bgDTO.UpdateTime;
            }
        }

        #endregion Utility Method

    }
}

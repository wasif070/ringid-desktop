﻿using Models.Entity;
using View.Utility;
using System.ComponentModel;
using log4net;
using System.Collections.ObjectModel;
using imsdkwrapper;
using Models.Constants;
using System;

namespace View.BindingModels
{

    public class RoomModel : INotifyPropertyChanged
    {
        private readonly ILog log = LogManager.GetLogger(typeof(RoomModel).Name);

        #region Constructor

        public RoomModel()
        {
        }

        public RoomModel(RoomDTO roomDTO)
            : this()
        {
            LoadData(roomDTO);
        }

        public RoomModel(BaseRoomDTO roomDTO)
            : this()
        {
            LoadData(roomDTO);
        }

        #endregion Constructor

        #region Property

        private string _RoomID;
        public string RoomID
        {
            get { return _RoomID; }
            set
            {
                if (value == _RoomID)
                    return;

                _RoomID = value;
                this.OnPropertyChanged("RoomID");
            }
        }

        private string _RoomName;
        public string RoomName
        {
            get { return _RoomName; }
            set
            {
                if (value == _RoomName)
                    return;

                _RoomName = value;
                this.OnPropertyChanged("RoomName");
            }
        }

        private string _RoomProfileImage;
        public string RoomProfileImage
        {
            get { return _RoomProfileImage; }
            set
            {
                if (value == _RoomProfileImage)
                    return;

                _RoomProfileImage = value;
                this.OnPropertyChanged("RoomProfileImage");
            }
        }

        private int _NumberOfMembers;
        public int NumberOfMembers
        {
            get { return _NumberOfMembers; }
            set
            {
                if (value == _NumberOfMembers)
                    return;

                _NumberOfMembers = value;
                this.OnPropertyChanged("NumberOfMembers");
            }
        }

        private bool _ImSoundEnabled = true;
        public bool ImSoundEnabled
        {
            get { return _ImSoundEnabled; }
            set
            {
                if (value == _ImSoundEnabled)
                    return;

                _ImSoundEnabled = value;
                this.OnPropertyChanged("ImSoundEnabled");
            }
        }

        private bool _ImNotificationEnabled = true;
        public bool ImNotificationEnabled
        {
            get { return _ImNotificationEnabled; }
            set
            {
                if (value == _ImNotificationEnabled)
                    return;

                _ImNotificationEnabled = value;
                this.OnPropertyChanged("ImNotificationEnabled");
            }
        }

        private bool _ImPopupEnabled = true;
        public bool ImPopupEnabled
        {
            get { return _ImPopupEnabled; }
            set
            {
                if (value == _ImPopupEnabled)
                    return;

                _ImPopupEnabled = value;
                this.OnPropertyChanged("ImPopupEnabled");
            }
        }

        private bool _IsPartial;
        public bool IsPartial
        {
            get { return _IsPartial; }
            set
            {
                if (value == _IsPartial)
                    return;

                _IsPartial = value;
                this.OnPropertyChanged("IsPartial");
            }
        }

        private string _ChatBgUrl;
        public string ChatBgUrl
        {
            get { return _ChatBgUrl; }
            set
            {
                if (value == _ChatBgUrl)
                    return;

                _ChatBgUrl = value;
                this.OnPropertyChanged("ChatBgUrl");
            }
        }

        private string _EventChatBgUrl;
        public string EventChatBgUrl
        {
            get { return _EventChatBgUrl; }
            set
            {
                if (value == _EventChatBgUrl)
                    return;

                _EventChatBgUrl = value;
                this.OnPropertyChanged("EventChatBgUrl");
            }
        }

        private string _ChatMinPacketID;
        public string ChatMinPacketID
        {
            get { return _ChatMinPacketID; }
            set
            {
                if (value == _ChatMinPacketID)
                    return;

                _ChatMinPacketID = value;
                this.OnPropertyChanged("ChatMinPacketID");
            }
        }

        private string _ChatMaxPacketID;
        public string ChatMaxPacketID
        {
            get { return _ChatMaxPacketID; }
            set
            {
                if (value == _ChatMaxPacketID)
                    return;

                _ChatMaxPacketID = value;
                this.OnPropertyChanged("ChatMaxPacketID");
            }
        }

        private bool _ChatHistoryNotFound;
        public bool ChatHistoryNotFound
        {
            get { return _ChatHistoryNotFound; }
            set
            {
                if (value == _ChatHistoryNotFound)
                    return;

                _ChatHistoryNotFound = value;
                this.OnPropertyChanged("ChatHistoryNotFound");
            }
        }

        private string _PagingState;
        public string PagingState
        {
            get { return _PagingState; }
            set
            {
                if (value == _PagingState)
                    return;

                _PagingState = value;
                this.OnPropertyChanged("PagingState");
            }
        }

        private bool _ChatMemberNotFound;
        public bool ChatMemberNotFound
        {
            get { return _ChatMemberNotFound; }
            set
            {
                if (value == _ChatMemberNotFound)
                    return;

                _ChatMemberNotFound = value;
                this.OnPropertyChanged("ChatMemberNotFound");
            }
        }

        private bool _FromDB;
        public bool FromDB
        {
            get { return _FromDB; }
            set
            {
                if (value == _FromDB)
                    return;

                _FromDB = value;
                this.OnPropertyChanged("FromDB");
            }
        }

        private ObservableCollection<RoomMemberModel> _MemberInfoList = new ObservableCollection<RoomMemberModel>();
        public ObservableCollection<RoomMemberModel> MemberInfoList
        {
            get { return _MemberInfoList; }
            set { _MemberInfoList = value; }
        }

        private ObservableCollection<MessageModel> _MessageList = new ObservableCollection<MessageModel>();
        public ObservableCollection<MessageModel> MessageList
        {
            get { return _MessageList; }
            set
            {
                _MessageList = value;
                this.OnPropertyChanged("MessageList");
            }
        }

        #endregion Property

        #region Utility Method

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void LoadData(RoomDTO roomDTO)
        {
            if (roomDTO != null)
            {
                this.RoomID = roomDTO.RoomID;
                this.RoomName = roomDTO.RoomName;
                this.RoomProfileImage = roomDTO.RoomProfileImage;
                this.NumberOfMembers = roomDTO.NumberOfMembers;
                this.IsPartial = roomDTO.IsPartial;
                this.ChatBgUrl = roomDTO.ChatBgUrl;
                this.ChatMinPacketID = roomDTO.ChatMinPacketID;
                this.ChatMaxPacketID = roomDTO.ChatMaxPacketID;
                this.ImSoundEnabled = roomDTO.ImSoundEnabled;
                this.ImNotificationEnabled = roomDTO.ImNotificationEnabled;
                this.ImPopupEnabled = roomDTO.ImPopupEnabled;
            }
        }

        public void LoadData(BaseRoomDTO roomDTO)
        {
            if (roomDTO != null)
            {
                this.RoomID = roomDTO.RoomID;
                this.RoomName = roomDTO.RoomName;
                this.RoomProfileImage = roomDTO.RoomImageUrl;
                this.NumberOfMembers = roomDTO.NumberOfMember;
            }
        }

        #endregion Utility Method
    }

    public class RoomRequestModel : INotifyPropertyChanged
    {
        private readonly ILog log = LogManager.GetLogger(typeof(RoomRequestModel).Name);

        #region Property

        private string _CategoryName = String.Empty;
        public string CategoryName
        {
            get { return _CategoryName; }
            set
            {
                if (value == _CategoryName)
                    return;

                _CategoryName = value;
                this.OnPropertyChanged("CategoryName");
            }
        }

        private string _CountryName = String.Empty;
        public string CountryName
        {
            get { return _CountryName; }
            set
            {
                if (value == _CountryName)
                    return;

                _CountryName = value;
                this.OnPropertyChanged("CountryName");
            }
        }

        private string _SearchText = String.Empty;
        public string SearchText
        {
            get { return _SearchText; }
            set
            {
                if (value == _SearchText)
                    return;

                _SearchText = value;
                this.OnPropertyChanged("SearchText");
            }
        }

        private string _PrevSearchText = String.Empty;
        public string PrevSearchText
        {
            get { return _PrevSearchText; }
            set
            {
                if (value == _PrevSearchText)
                    return;

                _PrevSearchText = value;
                this.OnPropertyChanged("PrevSearchText");
            }
        }

        private int _StartIndex = 0;
        public int StartIndex
        {
            get { return _StartIndex; }
            set
            {
                if (value == _StartIndex)
                    return;

                _StartIndex = value;
                this.OnPropertyChanged("StartIndex");
            }
        }

        private bool _NoMoreRoom = false;
        public bool NoMoreRoom
        {
            get { return _NoMoreRoom; }
            set
            {
                if (value == _NoMoreRoom)
                    return;

                _NoMoreRoom = value;
                this.OnPropertyChanged("NoMoreRoom");
            }
        }

        private bool _IsActive = true;
        public bool IsActive
        {
            get { return _IsActive; }
            set
            {
                if (value == _IsActive)
                    return;

                _IsActive = value;
                this.OnPropertyChanged("IsActive");
            }
        }

        #endregion Property

        #region Utility Method

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        #endregion Utility Method
    }

}

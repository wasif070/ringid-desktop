﻿using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System.ComponentModel;

namespace View.BindingModels
{
    public class PageInfoModel : BaseUserProfileModel
    {
        public PageInfoModel()
        {
            CurrentInstance = this;
        }
        public void LoadData(JObject jObject)
        {
            if (jObject[JsonKeys.FullName] != null)
            {
                this.FullName = (string)jObject[JsonKeys.FullName];
            }
            if (jObject[JsonKeys.ProfileImage] != null)
            {
                this.ProfileImage = (string)jObject[JsonKeys.ProfileImage];
            }
            if (jObject[JsonKeys.CoverImage] != null)
            {
                this.CoverImage = (string)jObject[JsonKeys.CoverImage];
            }
            if (jObject[JsonKeys.UserTableID] != null)
            {
                this.UserTableID = (long)jObject[JsonKeys.UserTableID];
            }
            if (jObject[JsonKeys.UserIdentity] != null)
            {
                this.UserIdentity = (long)jObject[JsonKeys.UserIdentity];
            }
            if (jObject[JsonKeys.IsUserFeedHidden] != null)
            {
                this.IsProfileHidden = (bool)jObject[JsonKeys.IsUserFeedHidden];
            }
            if (jObject[JsonKeys.NewsPortalSlogan] != null)
            {
                this.PageSlogan = (string)jObject[JsonKeys.NewsPortalSlogan];
            }
            if (jObject[JsonKeys.PageId] != null)
            {
                this.PageId = (long)jObject[JsonKeys.PageId];
            }
            if (jObject[JsonKeys.Subscribe] != null)
            {
                this.IsSubscribed = (bool)jObject[JsonKeys.Subscribe];
            }
            if (jObject[JsonKeys.SubscriberCount] != null)
            {
                this.SubscriberCount = (int)jObject[JsonKeys.SubscriberCount];
            }
            if (jObject[JsonKeys.NewsPortalCatName] != null)
            {
                this.PageCatName = (string)jObject[JsonKeys.NewsPortalCatName];
            }
            if (jObject[JsonKeys.NewsPortalCatId] != null)
            {
                this.PageCatId = (long)jObject[JsonKeys.NewsPortalCatId];
            }
            if (jObject[JsonKeys.NewsPortalDTO] != null)
            {
                JObject NpDto = (JObject)jObject[JsonKeys.NewsPortalDTO];
                if (NpDto[JsonKeys.NewsPortalSlogan] != null)
                {
                    this.PageSlogan = (string)NpDto[JsonKeys.NewsPortalSlogan];
                }
                if (NpDto[JsonKeys.PageId] != null)
                {
                    this.PageId = (long)NpDto[JsonKeys.PageId];
                }
                if (NpDto[JsonKeys.Subscribe] != null)
                {
                    this.IsSubscribed = (bool)NpDto[JsonKeys.Subscribe];
                }
                if (NpDto[JsonKeys.NewsPortalCatName] != null)
                {
                    this.PageCatName = (string)NpDto[JsonKeys.NewsPortalCatName];
                }
                if (NpDto[JsonKeys.NewsPortalCatId] != null)
                {
                    this.PageCatId = (long)NpDto[JsonKeys.NewsPortalCatId];
                }
                if (NpDto[JsonKeys.SubscriberCount] != null)
                {
                    this.SubscriberCount = (int)NpDto[JsonKeys.SubscriberCount];
                }
                if (NpDto[JsonKeys.FullName] != null)
                {
                    this.FullName = (string)NpDto[JsonKeys.FullName];
                }
                if (NpDto[JsonKeys.UserTableID] != null)
                {
                    this.UserTableID = (long)NpDto[JsonKeys.UserTableID];
                }
                if (NpDto[JsonKeys.UserIdentity] != null)
                {
                    this.UserIdentity = (long)NpDto[JsonKeys.UserIdentity];
                }
                if (NpDto[JsonKeys.ProfileImage] != null)
                {
                    this.ProfileImage = (string)NpDto[JsonKeys.ProfileImage];
                }
            }
        }
        //public void LoadData(PageInfoDTO dto)
        //{
        //    PageId = dto.PageId;
        //    UserTableID = dto.UserTableID;
        //    PageName = dto.PageName;
        //    SubscriberCount = dto.SubscriberCount;
        //    ProfileImage = dto.ProfileImage;
        //    PageSlogan = dto.PageSlogan;
        //    PageCatName = dto.PageCatName;
        //    PageCatId = dto.PageCatId;
        //    IsSubscribed = dto.IsSubscribed;
        //    CoverImage = dto.CoverImage;
        //    UserIdentity = dto.RingID;
        //    IsProfileHidden = dto.IsProfileHidden;
        //}
        private long _PageId;
        public long PageId
        {
            get { return _PageId; }
            set
            {
                if (value == _PageId)
                    return;
                _PageId = value;
                this.OnPropertyChanged("PageId");
            }
        }
        //private long _UserTableId;
        //public long UserTableID
        //{
        //    get { return _UserTableId; }
        //    set
        //    {
        //        if (value == _UserTableId)
        //            return;
        //        _UserTableId = value;
        //        this.OnPropertyChanged("UserTableID");
        //    }
        //}
        //private bool _IsProfileHidden;
        //public bool IsProfileHidden
        //{
        //    get { return _IsProfileHidden; }
        //    set
        //    {
        //        if (value == _IsProfileHidden)
        //            return;

        //        _IsProfileHidden = value;
        //        this.OnPropertyChanged("IsProfileHidden");
        //    }
        //}
        //private long _UserIdentity;
        //public long UserIdentity
        //{
        //    get { return _UserIdentity; }
        //    set
        //    {
        //        if (value == _UserIdentity)
        //            return;

        //        _UserIdentity = value;
        //        this.OnPropertyChanged("UserIdentity");
        //    }
        //}
        //private string _PageName;
        //public string PageName
        //{
        //    get { return _PageName; }
        //    set
        //    {
        //        if (value == _PageName)
        //            return;
        //        _PageName = value;
        //        this.OnPropertyChanged("PageName");
        //    }
        //}
        private bool _IsSubscribed;
        public bool IsSubscribed
        {
            get { return _IsSubscribed; }
            set
            {
                if (value == _IsSubscribed)
                    return;
                _IsSubscribed = value;
                this.OnPropertyChanged("IsSubscribed");
            }
        }
        private string _PageSlogan;
        public string PageSlogan
        {
            get { return _PageSlogan; }
            set
            {
                if (value == _PageSlogan)
                    return;
                _PageSlogan = value;
                this.OnPropertyChanged("PageSlogan");
            }
        }

        private long _PageCatId;
        public long PageCatId
        {
            get { return _PageCatId; }
            set
            {
                if (value == _PageCatId)
                    return;

                _PageCatId = value;
                this.OnPropertyChanged("PageCatId");
            }
        }
        private string _PageCatName;
        public string PageCatName
        {
            get { return _PageCatName; }
            set
            {
                if (value == _PageCatName)
                    return;
                _PageCatName = value;
                this.OnPropertyChanged("PageCatName");
            }
        }
        private int _SubscriberCount;
        public int SubscriberCount
        {
            get { return _SubscriberCount; }
            set
            {
                if (value == _SubscriberCount)
                    return;

                _SubscriberCount = value;
                this.OnPropertyChanged("SubscriberCount");
            }
        }

        //private string _ProfileImage;
        //public string ProfileImage
        //{
        //    get { return _ProfileImage; }
        //    set
        //    {
        //        if (value == _ProfileImage)
        //            return;

        //        _ProfileImage = value;
        //        this.OnPropertyChanged("ProfileImage");
        //    }
        //}
        private string _CoverImage;
        public string CoverImage
        {
            get { return _CoverImage; }
            set
            {
                if (value == _CoverImage)
                    return;

                _CoverImage = value;
                this.OnPropertyChanged("CoverImage");
            }
        }
        private bool _IsRightMarginOff;
        public bool IsRightMarginOff
        {
            get { return _IsRightMarginOff; }
            set
            {
                if (value == _IsRightMarginOff)
                    return;

                _IsRightMarginOff = value;
                this.OnPropertyChanged("IsRightMarginOff");
            }
        }
        private bool _IsContextMenuOpened = false;
        public bool IsContextMenuOpened
        {

            get { return _IsContextMenuOpened; }
            set
            {
                System.Diagnostics.Debug.WriteLine("ContextMenu ....=>");
                if (value == _IsContextMenuOpened) return;
                _IsContextMenuOpened = value;
                this.OnPropertyChanged("IsContextMenuOpened");
            }
        }
        //private PageInfoModel _CurrentInstance;
        //public PageInfoModel CurrentInstance
        //{
        //    get { return _CurrentInstance; }
        //    set
        //    {
        //        if (value == _CurrentInstance)
        //            return;

        //        _CurrentInstance = value;
        //        this.OnPropertyChanged("CurrentInstance");
        //    }
        //}
        //public event PropertyChangedEventHandler PropertyChanged;
        //public void OnPropertyChanged(string propertyName)
        //{
        //    PropertyChangedEventHandler handler = PropertyChanged;
        //    if (handler != null)
        //    {
        //        handler(this, new PropertyChangedEventArgs(propertyName));
        //    }
        //}
    }
}

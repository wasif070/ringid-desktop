﻿using System.ComponentModel;
using System.Windows.Media.Imaging;

namespace View.BindingModels
{
    public class CommonUploaderModel : INotifyPropertyChanged
    {
        public CommonUploaderModel()
        {
            CurrentInstance = this;
        }

        private int _UploadType;//3=image, 5=audio// 6=video //8=sticker //9= record
        public int UploadType
        {
            get { return _UploadType; }
            set
            {
                if (value == _UploadType)
                    return;
                _UploadType = value;
                this.OnPropertyChanged("UploadType");
            }
        }
        private long _FileSize;
        public long FileSize
        {
            get { return _FileSize; }
            set
            {
                if (value == _FileSize)
                    return;

                _FileSize = value;
                this.OnPropertyChanged("FileSize");
            }
        }
        private string _FilePath = string.Empty;
        public string FilePath
        {
            get { return _FilePath; }
            set
            {
                if (value == _FilePath)
                    return;

                _FilePath = value;
                this.OnPropertyChanged("FilePath");
            }
        }
        private long _FileDuration; //for audiovideo
        public long FileDuration
        {
            get { return _FileDuration; }
            set
            {
                if (value == _FileDuration)
                    return;

                _FileDuration = value;
                this.OnPropertyChanged("FileDuration");
            }
        }
        private string _FileTitle = string.Empty; //for audiovideo
        public string FileTitle
        {
            get { return _FileTitle; }
            set
            {
                if (value == _FileTitle)
                    return;

                _FileTitle = value;
                this.OnPropertyChanged("FileTitle");
            }
        }
        private string _FileArtist = string.Empty;//for audiovideo
        public string FileArtist
        {
            get { return _FileArtist; }
            set
            {
                if (value == _FileArtist)
                    return;

                _FileArtist = value;
                this.OnPropertyChanged("FileArtist");
            }
        }
        private bool _IsThumbImageFound = false; //for audio
        public bool IsThumbImageFound
        {
            get { return _IsThumbImageFound; }
            set
            {
                if (value == _IsThumbImageFound)
                    return;

                _IsThumbImageFound = value;
                this.OnPropertyChanged("IsThumbImageFound");
            }
        }
        private int _FileOriginType; //for image, MessageType.IMAGEFROMCAMERA, fromGALLERY, fromPC
        public int FileOriginType
        {
            get { return _FileOriginType; }
            set
            {
                if (value == _FileOriginType)
                    return;

                _FileOriginType = value;
                this.OnPropertyChanged("FileOriginType");
            }
        }
        private int _FileWidth; //for image
        public int FileWidth
        {
            get { return _FileWidth; }
            set
            {
                if (value == _FileWidth)
                    return;

                _FileWidth = value;
                this.OnPropertyChanged("FileWidth");
            }
        }
        private int _FileHeight;//for image
        public int FileHeight
        {
            get { return _FileHeight; }
            set
            {
                if (value == _FileHeight)
                    return;

                _FileHeight = value;
                this.OnPropertyChanged("FileHeight");
            }
        }
        private BitmapImage _ThumbnailBmp = null; //for video previmage, for image, copied image
        public BitmapImage ThumbnailBmp
        {
            get { return _ThumbnailBmp; }
            set
            {
                _ThumbnailBmp = value;
                this.OnPropertyChanged("ThumbnailBmp");
            }
        }
        //private string _StickerPath = string.Empty;//for sticker 1//1
        //public string StickerPath
        //{
        //    get { return _StickerPath; }
        //    set
        //    {
        //        if (value == _StickerPath)
        //            return;

        //        _StickerPath = value;
        //        this.OnPropertyChanged("StickerPath");
        //    }
        //}
        ////////uploading
        private bool _IsUploading = false;
        public bool IsUploading
        {
            get { return _IsUploading; }
            set
            {
                if (value == _IsUploading)
                    return;

                _IsUploading = value;
                this.OnPropertyChanged("IsUploading");
            }
        }
        private int _Pecentage;
        public int Pecentage
        {
            get { return _Pecentage; }
            set
            {
                if (value == _Pecentage)
                    return;

                _Pecentage = value;
                this.OnPropertyChanged("Pecentage");
            }
        }

        ////////uploadED
        private bool _IsUploadedInServer = false;
        public bool IsUploadedInServer
        {
            get { return _IsUploadedInServer; }
            set
            {
                if (value == _IsUploadedInServer)
                    return;

                _IsUploadedInServer = value;
                this.OnPropertyChanged("IsUploadedInServer");
            }
        }
        //private string _UploadedImageUrl = null; //for image
        //public string UploadedImageUrl
        //{
        //    get { return _UploadedImageUrl; }
        //    set
        //    {
        //        if (value == _UploadedImageUrl)
        //            return;

        //        _UploadedImageUrl = value;
        //        this.OnPropertyChanged("UploadedImageUrl");
        //    }
        //}
        private string _UploadedThumbUrl = string.Empty; //for audiovideo
        public string UploadedThumbUrl
        {
            get { return _UploadedThumbUrl; }
            set
            {
                if (value == _UploadedThumbUrl)
                    return;
                _UploadedThumbUrl = value;
                this.OnPropertyChanged("UploadedThumbUrl");
            }
        }
        //private string _UploadedStreamUrl = null; //for audiovideo
        //public string UploadedStreamUrl
        //{
        //    get { return _UploadedStreamUrl; }
        //    set
        //    {
        //        if (value == _UploadedStreamUrl)
        //            return;
        //        _UploadedStreamUrl = value;
        //        this.OnPropertyChanged("UploadedStreamUrl");
        //    }
        //}
        private string _UploadedUrl = string.Empty; //for audiovideo-streamUrl, for imag-imageUrl, for sticker-stickerpath
        public string UploadedUrl
        {
            get { return _UploadedUrl; }
            set
            {
                if (value == _UploadedUrl)
                    return;
                _UploadedUrl = value;
                this.OnPropertyChanged("UploadedUrl");
            }
        }
        private CommonUploaderModel _CurrentInstance;
        public CommonUploaderModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                if (value == _CurrentInstance)
                    return;
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

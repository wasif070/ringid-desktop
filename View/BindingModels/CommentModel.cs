﻿using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel;
using View.ViewModel;
using System.Linq;
using System;
using System.Windows;

namespace View.BindingModels
{
    public class CommentModel : INotifyPropertyChanged
    {
        public CommentModel()
        {
            CurrentInstance = this;
        }
        public void LoadCommenter(JObject jObject)
        {
            long utId = 0;
            if (jObject[JsonKeys.UserTableID] != null) utId = (long)jObject[JsonKeys.UserTableID];
            if (utId == 0 && jObject[JsonKeys.WUserTableId] != null) utId = (long)jObject[JsonKeys.WUserTableId];
            if (utId == 0 && jObject[JsonKeys.ActivistId] != null) utId = (long)jObject[JsonKeys.ActivistId];

            long uId = (jObject[JsonKeys.UserIdentity] != null) ? (long)jObject[JsonKeys.UserIdentity] : 0;

            if (jObject[JsonKeys.FullName] != null)
            {
                string prImg = (jObject[JsonKeys.ProfileImage] != null) ? (string)jObject[JsonKeys.ProfileImage] : "";
                string fn = (string)jObject[JsonKeys.FullName];
                if (UserShortInfoModel == null)
                {
                    UserShortInfoModel = RingIDViewModel.Instance.GetUserShortInfoModelNotNullable(utId, uId, fn, prImg);
                }
            }
            else if (UserShortInfoModel == null)
                UserShortInfoModel = RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel;
        }
        public void LoadData(JObject jObject)
        {
            if (UserShortInfoModel == null)
            {
                LoadCommenter(jObject);
            }
            if (jObject[JsonKeys.Comment] != null)
            {
                Comment = (string)jObject[JsonKeys.Comment];
            }
            if (jObject[JsonKeys.Time] != null)
            {
                Time = (long)jObject[JsonKeys.Time];
            }
            else if (jObject[JsonKeys.UpdateTime] != null)
            {
                Time = (long)jObject[JsonKeys.UpdateTime];
            }
            if (jObject[JsonKeys.ShowContinue] != null)
            {
                ShowContinue = (bool)jObject[JsonKeys.ShowContinue];
            }
            if (jObject[JsonKeys.ILikeComment] != null)
            {
                ILikeComment = (short)jObject[JsonKeys.ILikeComment];
            }
            if (jObject[JsonKeys.TotalLikeComment] != null)
            {
                TotalLikeComment = (int)jObject[JsonKeys.TotalLikeComment];
            }
            if (jObject[JsonKeys.Url] != null)
            {
                MediaOrImageUrl = (string)jObject[JsonKeys.Url];
            }
            else MediaOrImageUrl = null;
            if (jObject[JsonKeys.UrlType] != null)
            {
                UrlType = (int)jObject[JsonKeys.UrlType];
            }
            if (jObject[JsonKeys.CommentTags] != null)
            {
                TaggedUsers = new ObservableCollection<TaggedUserModel>();
                JArray array = (JArray)jObject[JsonKeys.CommentTags];
                foreach (JObject obj in array)
                {
                    TaggedUserModel singleTagModel = new TaggedUserModel();
                    if (obj[JsonKeys.UserIdentity] != null) singleTagModel.UserIdentity = (long)obj[JsonKeys.UserIdentity];//not needed eventually
                    if (obj[JsonKeys.UserTableID] != null) singleTagModel.UserTableID = (long)obj[JsonKeys.UserTableID];
                    if (obj[JsonKeys.FullName] != null) singleTagModel.FullName = (string)obj[JsonKeys.FullName];
                    if (obj[JsonKeys.Position] != null) singleTagModel.Index = (int)obj[JsonKeys.Position];
                    if (obj[JsonKeys.ProfileImage] != null) singleTagModel.ProfileImage = (string)obj[JsonKeys.ProfileImage]; //not needed eventually
                    TaggedUsers.Add(singleTagModel);
                }
                TaggedUsers = new ObservableCollection<TaggedUserModel>(TaggedUsers.OrderByDescending(a => a.Index));
                //TaggedUsers.OrderBy(a => a.Index);
            }
            else TaggedUsers = null; //editcomment

            IsEditMode = false;
            if (jObject[JsonKeys.IsEdited] != null)
            {
                Edited = (bool)jObject[JsonKeys.IsEdited];
            }
            if (jObject[JsonKeys.ThumbUrlComment] != null)
            {
                ThumbUrl = (string)jObject[JsonKeys.ThumbUrlComment];
            }
            else ThumbUrl = null;
            if (jObject[JsonKeys.MediaDuration] != null)
            {
                Duration = (long)jObject[JsonKeys.MediaDuration];
            }
            else if (jObject[JsonKeys.Duration] != null)
            {
                Duration = (long)jObject[JsonKeys.Duration];
            }
        }

        private BaseUserProfileModel _UserShortInfoModel;
        public BaseUserProfileModel UserShortInfoModel
        {
            get { return _UserShortInfoModel; }
            set
            {
                if (value == _UserShortInfoModel)
                    return;
                _UserShortInfoModel = value;
                //this.OnPropertyChanged("UserShortInfoModel");
            }
        }

        private Guid _CommentId;
        public Guid CommentId
        {
            get { return _CommentId; }
            set
            {
                if (value == _CommentId)
                    return;
                _CommentId = value;
                this.OnPropertyChanged("CommentId");
            }
        }

        private string _Comment;
        public string Comment
        {
            get { return _Comment; }
            set
            {
                if (value == _Comment)
                    return;
                _Comment = value;
                this.OnPropertyChanged("Comment");
            }
        }

        private long _Time;
        public long Time
        {
            get { return _Time; }
            set
            {
                if (value == _Time)
                    return;
                _Time = value;
                this.OnPropertyChanged("Time");
            }
        }

        private long _TotalLikeComment;
        public long TotalLikeComment
        {
            get { return _TotalLikeComment; }
            set
            {
                if (value == _TotalLikeComment)
                    return;
                _TotalLikeComment = value;
                this.OnPropertyChanged("TotalLikeComment");
            }
        }

        private long _Duration;
        public long Duration
        {
            get { return _Duration; }
            set
            {
                if (value == _Duration)
                    return;
                _Duration = value;
                this.OnPropertyChanged("Duration");
            }
        }

        private string _ThumbUrl;
        public string ThumbUrl
        {
            get { return _ThumbUrl; }
            set
            {
                if (value == _ThumbUrl)
                    return;
                _ThumbUrl = value;
                this.OnPropertyChanged("ThumbUrl");
            }
        }

        private short _ILikeComment;
        public short ILikeComment
        {
            get { return _ILikeComment; }
            set
            {
                // if (value == _ILikeComment)
                //    return;
                _ILikeComment = value;
                this.OnPropertyChanged("ILikeComment");
            }
        }

        private bool _ShowContinue;
        public bool ShowContinue
        {
            get { return _ShowContinue; }
            set
            {
                if (value == _ShowContinue)
                    return;
                _ShowContinue = value;
                this.OnPropertyChanged("ShowContinue");
            }
        }

        private ObservableCollection<TaggedUserModel> _TaggedUsers;
        public ObservableCollection<TaggedUserModel> TaggedUsers
        {
            get { return _TaggedUsers; }
            set
            {
                if (value == _TaggedUsers)
                    return;
                _TaggedUsers = value;
                this.OnPropertyChanged("TaggedUsers");
            }
        }
        ///
        private bool _IsEditMode = false;
        public bool IsEditMode
        {
            get { return _IsEditMode; }
            set
            {
                // if (value == _IsEditMode)
                //    return;
                _IsEditMode = value;
                this.OnPropertyChanged("IsEditMode");
            }
        }

        //private bool _IsButtonsEnabled = true;
        //public bool IsButtonsEnabled
        //{
        //    get { return _IsButtonsEnabled; }
        //    set
        //    {
        //        if (value == _IsButtonsEnabled)
        //            return;
        //        _IsButtonsEnabled = value;
        //        this.OnPropertyChanged("IsButtonsEnabled");
        //    }
        //}

        private bool _Edited = false;
        public bool Edited
        {
            get { return _Edited; }
            set
            {
                if (value == _Edited)
                    return;
                _Edited = value;
                this.OnPropertyChanged("Edited");
            }
        }

        private long _PostOwnerUtId;
        public long PostOwnerUtId
        {
            get { return _PostOwnerUtId; }
            set
            {
                if (value == _PostOwnerUtId)
                    return;
                _PostOwnerUtId = value;
                this.OnPropertyChanged("PostOwnerUtId");
            }
        }

        private Guid _NewsfeedId;
        public Guid NewsfeedId
        {
            get { return _NewsfeedId; }
            set
            {
                if (value == _NewsfeedId)
                    return;
                _NewsfeedId = value;
                this.OnPropertyChanged("NewsfeedId");
            }
        }

        private Guid _ImageId;
        public Guid ImageId
        {
            get { return _ImageId; }
            set
            {
                if (value == _ImageId)
                    return;
                _ImageId = value;
                this.OnPropertyChanged("ImageId");
            }
        }

        private Guid _ContentId;
        public Guid ContentId
        {
            get { return _ContentId; }
            set
            {
                if (value == _ContentId)
                    return;
                _ContentId = value;
                this.OnPropertyChanged("ContentId");
            }
        }

        private string _MediaOrImageUrl;
        public string MediaOrImageUrl
        {
            get { return _MediaOrImageUrl; }
            set
            {
                if (value == _MediaOrImageUrl)
                    return;
                _MediaOrImageUrl = value;
                this.OnPropertyChanged("MediaOrImageUrl");
            }
        }

        private int _UrlType;
        public int UrlType
        {
            get { return _UrlType; }
            set
            {
                if (value == _UrlType)
                    return;
                _UrlType = value;
                this.OnPropertyChanged("UrlType");
            }
        }

        private CommentModel _CurrentInstance;
        public CommentModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                if (value == _CurrentInstance)
                    return;
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }
        private Visibility _popupLikeLoaderVisibility = Visibility.Collapsed;
        public Visibility PopupLikeLoaderVisibility
        {
            get
            {
                return _popupLikeLoaderVisibility;
            }
            set
            {
                if (value == _popupLikeLoaderVisibility)
                    return;
                _popupLikeLoaderVisibility = value;
                OnPropertyChanged("PopupLikeLoaderVisibility");
            }
        }

        private string _popupLikeLoader;
        public string PopupLikeLoader
        {
            get
            {
                return _popupLikeLoader;
            }
            set
            {
                if (value == _popupLikeLoader)
                    return;
                _popupLikeLoader = value;
                OnPropertyChanged("PopupLikeLoader");
            }
        }
        //private Visibility _PlaceholderVisibility;
        //public Visibility PlaceholderVisibility
        //{
        //    get
        //    {
        //        return _PlaceholderVisibility;
        //    }
        //    set
        //    {
        //        if (value == _PlaceholderVisibility)
        //            return;
        //        _PlaceholderVisibility = value;
        //        OnPropertyChanged("PlaceholderVisibility");
        //    }
        //}
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        //private long _UserIdentity;
        //public long UserIdentity
        //{
        //    get { return _UserIdentity; }
        //    set
        //    {
        //        if (value == _UserIdentity)
        //            return;
        //        _UserIdentity = value;
        //        this.OnPropertyChanged("UserIdentity");
        //    }
        //}
        //private string _FullName;
        //public string FullName
        //{
        //    get { return _FullName; }
        //    set
        //    {
        //        if (value == _FullName)
        //            return;

        //        _FullName = value;
        //        this.OnPropertyChanged("FullName");
        //    }
        //}
        //private string _ProfileImage;
        //public string ProfileImage
        //{
        //    get { return _ProfileImage; }
        //    set
        //    {
        //        if (value == _ProfileImage)
        //            return;

        //        _ProfileImage = value;
        //        this.OnPropertyChanged("ProfileImage");
        //    }
        //}
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace View.BindingModels
{
    public class ReasonModel : INotifyPropertyChanged
    {
        public ReasonModel(int id, string str)
        {
            this.ReasonId = id;
            this.ReasonString = str;
        }

        private int _reasonId;
        public int ReasonId
        {
            get { return _reasonId; }
            set
            {
                if (value == _reasonId)
                    return;
                _reasonId = value;
                this.OnPropertyChanged("ReasonId");
            }
        }
        private string _reasonString;
        public string ReasonString
        {
            get { return _reasonString; }
            set
            {
                if (value == _reasonString)
                    return;
                _reasonString = value;
                this.OnPropertyChanged("ReasonString");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
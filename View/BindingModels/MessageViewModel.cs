﻿using imsdkwrapper;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using View.Utility;
using View.Utility.Chat;
using View.Utility.Chat.Service;

namespace View.BindingModels
{
    public class MessageViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        #region Constructor

        public MessageViewModel() { }

        #endregion Constructor

        #region Property

        private CustomizeTimer _SecretDeleteTimer = null;
        public CustomizeTimer SecretDeleteTimer
        {
            get { return _SecretDeleteTimer; }
            private set { _SecretDeleteTimer = value; }
        }

        private CustomizeTimer _SecretCounterTimer = null;
        public CustomizeTimer SecretCounterTimer
        {
            get { return _SecretCounterTimer; }
            private set { _SecretCounterTimer = value; }
        }

        private int _ViewType;
        public int ViewType
        {
            get { return _ViewType; }
            set
            {
                if (value == _ViewType)
                    return;

                _ViewType = value;
                this.OnPropertyChanged("ViewType");
            }
        }

        private bool _IsFriendChat;
        public bool IsFriendChat
        {
            get { return _IsFriendChat; }
            set
            {
                if (value == _IsFriendChat)
                    return;

                _IsFriendChat = value;
                this.OnPropertyChanged("IsFriendChat");
            }
        }

        private bool _IsGroupChat;
        public bool IsGroupChat
        {
            get { return _IsGroupChat; }
            set
            {
                if (value == _IsGroupChat)
                    return;

                _IsGroupChat = value;
                this.OnPropertyChanged("IsGroupChat");
            }
        }

        private bool _IsRoomChat;
        public bool IsRoomChat
        {
            get { return _IsRoomChat; }
            set
            {
                if (value == _IsRoomChat)
                    return;

                _IsRoomChat = value;
                this.OnPropertyChanged("IsRoomChat");
            }
        }

        private bool _IsPreviewMessage;
        public bool IsPreviewMessage
        {
            get { return _IsPreviewMessage; }
            private set
            {
                if (value == _IsPreviewMessage)
                    return;

                _IsPreviewMessage = value;
                this.OnPropertyChanged("IsFileMessage");
            }
        }

        private bool _IsMediaMessage;
        public bool IsMediaMessage
        {
            get { return _IsMediaMessage; }
            private set
            {
                if (value == _IsMediaMessage)
                    return;

                _IsMediaMessage = value;
                this.OnPropertyChanged("IsMediaMessage");
            }
        }

        private bool _HasCaption;
        public bool HasCaption
        {
            get { return _HasCaption; }
            private set
            {
                if (value == _HasCaption)
                    return;

                _HasCaption = value;
                this.OnPropertyChanged("HasCaption");
            }
        }

        private bool _IsEditMode;
        public bool IsEditMode
        {
            get { return _IsEditMode; }
            set
            {
                if (value == _IsEditMode)
                    return;

                _IsEditMode = value;
                this.OnPropertyChanged("IsEditMode");
            }
        }

        private bool _ForceStopSecretTimer;
        public bool ForceStopSecretTimer
        {
            get { return _ForceStopSecretTimer; }
            set
            {
                if (value == _ForceStopSecretTimer)
                    return;

                _ForceStopSecretTimer = value;
                this.OnPropertyChanged("ForceStopSecretTimer");
            }
        }

        private int _SecretCount;
        public int SecretCount
        {
            get { return _SecretCount; }
            set
            {
                if (value == _SecretCount)
                    return;

                _SecretCount = value;
                this.OnPropertyChanged("SecretCount");
            }
        }

        private bool _IsUploadRunning;
        public bool IsUploadRunning
        {
            get { return _IsUploadRunning; }
            set
            {
                if (value == _IsUploadRunning)
                    return;

                _IsUploadRunning = value;
                this.OnPropertyChanged("IsUploadRunning");
            }
        }

        private int _DownloadPercentage;
        public int DownloadPercentage
        {
            get { return _DownloadPercentage; }
            set
            {
                _DownloadPercentage = value;
                this.OnPropertyChanged("DownloadPercentage");
            }
        }

        private int _UploadPercentage;
        public int UploadPercentage
        {
            get { return _UploadPercentage; }
            set
            {
                _UploadPercentage = value;
                this.OnPropertyChanged("UploadPercentage");
            }
        }

        private long _FileProgressInSize;
        public long FileProgressInSize
        {
            get { return _FileProgressInSize; }
            set
            {
                _FileProgressInSize = value;
                this.OnPropertyChanged("FileProgressInSize");
            }
        }

        private bool _IsForceDownload;
        public bool IsForceDownload
        {
            get { return _IsForceDownload; }
            set
            {
                _IsForceDownload = value;
                this.OnPropertyChanged("IsForceDownload");
            }
        }

        private int _ImageViewWidth;
        public int ImageViewWidth
        {
            get { return _ImageViewWidth; }
            set
            {
                if (value == _ImageViewWidth)
                    return;

                _ImageViewWidth = value;
                this.OnPropertyChanged("ImageViewWidth");
            }
        }

        private int _ImageViewHeight;
        public int ImageViewHeight
        {
            get { return _ImageViewHeight; }
            set
            {
                if (value == _ImageViewHeight)
                    return;

                _ImageViewHeight = value;
                this.OnPropertyChanged("ImageViewHeight");
            }
        }

        private long _PrevIdentity;
        public long PrevIdentity
        {
            get { return _PrevIdentity; }
            set
            {
                if (value == _PrevIdentity)
                    return;

                _PrevIdentity = value;
                this.OnPropertyChanged("PrevIdentity");
            }
        }

        private bool _IsSamePrevAndCurrID;
        public bool IsSamePrevAndCurrID
        {
            get { return _IsSamePrevAndCurrID; }
            set
            {
                if (value == _IsSamePrevAndCurrID)
                    return;

                _IsSamePrevAndCurrID = value;
                this.OnPropertyChanged("IsSamePrevAndCurrID");
            }
        }

        private double _RotateAngle;
        public double RotateAngle
        {
            get { return _RotateAngle; }
            set
            {
                if (value == _RotateAngle)
                    return;

                _RotateAngle = value;
                this.OnPropertyChanged("RotateAngle");
            }
        }

        private long _LastViewDate;
        public long LastViewDate
        {
            get { return _LastViewDate; }
            set
            {
                if (value == _LastViewDate)
                    return;

                _LastViewDate = value;
                this.OnPropertyChanged("LastViewDate");
            }
        }

        private String _SelectedCharacters;
        public String SelectedCharacters
        {
            get { return _SelectedCharacters; }
            set
            {
                if (value == _SelectedCharacters)
                    return;

                _SelectedCharacters = value;
                this.OnPropertyChanged("SelectedCharacters");
            }
        }

        private bool _IsEditable;
        public bool IsEditable
        {
            get { return _IsEditable; }
            set
            {
                if (value == _IsEditable)
                    return;

                _IsEditable = value;
                this.OnPropertyChanged("IsEditable");
            }
        }

        private bool _IsFileQueued;
        public bool IsFileQueued
        {
            get { return _IsFileQueued; }
            set
            {
                if (value == _IsFileQueued)
                    return;

                _IsFileQueued = value;
                this.OnPropertyChanged("IsFileQueued");
            }
        }

        private bool _IsProcessing;
        public bool IsProcessing
        {
            get { return _IsProcessing; }
            set
            {
                if (value == _IsProcessing)
                    return;

                _IsProcessing = value;
                this.OnPropertyChanged("IsProcessing");
            }
        }

        private bool _LikeMemberNotFound;
        public bool LikeMemberNotFound
        {
            get { return _LikeMemberNotFound; }
            set
            {
                if (value == _LikeMemberNotFound)
                    return;

                _LikeMemberNotFound = value;
                this.OnPropertyChanged("LikeMemberNotFound");
            }
        }

        private bool _IsAudioOpened;
        public bool IsAudioOpened
        {
            get { return _IsAudioOpened; }
            set
            {
                if (value == _IsAudioOpened)
                    return;

                _IsAudioOpened = value;
                this.OnPropertyChanged("IsAudioOpened");
            }
        }

        #endregion Property

        #region Utility Method

        public void LoadData(MessageModel messageModel)
        {
            this.IsFriendChat = messageModel.FriendTableID > 0;
            this.IsGroupChat = messageModel.GroupID > 0;
            this.IsRoomChat = !String.IsNullOrWhiteSpace(messageModel.RoomID);

            this.LoadViewStructure(messageModel);
            this.LoadViewType(messageModel);
            this.LoadViewTimer(messageModel);
        }

        public void LoadViewStructure(MessageModel messageModel)
        {
            switch (messageModel.MessageType)
            {
                case MessageType.IMAGE_FILE_FROM_GALLERY:
                case MessageType.IMAGE_FILE_FROM_CAMERA:
                    {
                        this.ImageViewWidth = 160 + 50;
                        this.ImageViewHeight = ChatHelpers.CHAT_VIEW_HEIGHT[(int)messageModel.MessageType][0];
                        if (messageModel.Width > 0 && messageModel.Height > 0)
                        {
                            float ration = (float)messageModel.Width / messageModel.Height;
                            if (ration > 1.1500)
                            {
                                int estWidth = (int)(ration * ChatHelpers.CHAT_VIEW_HEIGHT[(int)messageModel.MessageType][0]);
                                this.ImageViewWidth = estWidth > 300 ? 300 : estWidth;
                            }
                            else if (ration < 0.85000)
                            {
                                int estHeight = (int)((1 / ration) * 160);
                                this.ImageViewWidth = 160 + 50;
                                this.ImageViewHeight = estHeight > 250 ? 250 : estHeight;
                            }
                        }
                        this.IsPreviewMessage = true;
                        this.IsMediaMessage = true;
                    }
                    break;
                case MessageType.VIDEO_FILE:
                    {
                        this.ImageViewWidth = 255;
                        this.ImageViewHeight = ChatHelpers.CHAT_VIEW_HEIGHT[(int)messageModel.MessageType][0];
                        this.IsPreviewMessage = true;
                        this.IsMediaMessage = true;
                    }
                    break;
                case MessageType.AUDIO_FILE:
                    {
                        this.ImageViewWidth = 300;
                        this.ImageViewHeight = ChatHelpers.CHAT_VIEW_HEIGHT[(int)messageModel.MessageType][0];
                        this.IsPreviewMessage = false;
                        this.IsMediaMessage = true;
                        this.IsAudioOpened = !messageModel.IsSecretChat;
                    }
                    break;
                case MessageType.DOWNLOAD_STICKER_MESSAGE:
                    {
                        this.ImageViewWidth = 0;
                        this.ImageViewHeight = ChatHelpers.CHAT_VIEW_HEIGHT[(int)messageModel.MessageType][0];
                        this.IsPreviewMessage = true;
                    }
                    break;
                case MessageType.LOCATION_MESSAGE:
                    {
                        this.ImageViewWidth = 340;
                        this.ImageViewHeight = ChatHelpers.CHAT_VIEW_HEIGHT[(int)messageModel.MessageType][0];
                        this.IsPreviewMessage = true;
                    }
                    break;
                case MessageType.FILE_STREAM:
                    {
                        this.ImageViewWidth = 120;
                        this.ImageViewHeight = ChatHelpers.CHAT_VIEW_HEIGHT[(int)messageModel.MessageType][0];
                        this.IsPreviewMessage = false;
                    }
                    break;
                case MessageType.CONTACT_SHARE:
                    {
                        this.ImageViewWidth = 120;
                        this.ImageViewHeight = ChatHelpers.CHAT_VIEW_HEIGHT[(int)messageModel.MessageType][0];
                        this.IsPreviewMessage = true;
                    }
                    break;
                case MessageType.LINK_MESSAGE:
                    this.ImageViewWidth = 0;
                    this.ImageViewHeight = 0;
                    if (String.IsNullOrWhiteSpace(messageModel.LinkImageUrl) || this.IsEditMode)
                    {
                        this.IsPreviewMessage = false;
                    }
                    else
                    {
                        this.IsPreviewMessage = true;
                    }
                    break;
                case MessageType.RING_MEDIA_MESSAGE:
                    this.ImageViewWidth = 255;
                    this.ImageViewHeight = ChatHelpers.CHAT_VIEW_HEIGHT[(int)messageModel.MessageType][0];
                    if (!String.IsNullOrWhiteSpace(messageModel.LinkImageUrl))
                    {
                        this.IsPreviewMessage = true;
                    }
                    else
                    {
                        this.IsPreviewMessage = false;
                    }
                    break;
                default:
                    this.ImageViewWidth = 0;
                    this.ImageViewHeight = 0;
                    this.IsPreviewMessage = false;
                    break;
            }
        }

        public void LoadViewType(MessageModel messageModel)
        {
            switch ((MessageType)messageModel.MessageType)
            {
                case MessageType.PLAIN_MESSAGE:
                case MessageType.EMOTICON_MESSAGE:
                    ViewType = ChatConstants.CHAT_PLAIN_EMOTICON_VIEW;
                    break;
                case MessageType.DOWNLOAD_STICKER_MESSAGE:
                    ViewType = ChatConstants.CHAT_STICKER_VIEW;
                    break;
                case MessageType.DELETE_MESSAGE:
                    ViewType = ChatConstants.CHAT_DELETE_VIEW;
                    messageModel.ChatOpened -= ChatHelpers.MessageViewModel_OnChatOpened;
                    messageModel.AudioPlayStateChange -= ChatHelpers.MediaPlayer_AudioPlayStateChange;
                    break;
                case MessageType.IMAGE_FILE_FROM_GALLERY:
                case MessageType.IMAGE_FILE_FROM_CAMERA:
                    HasCaption = !String.IsNullOrWhiteSpace(messageModel.Caption);
                    ViewType = ChatConstants.CHAT_IMAGE_VIEW;
                    break;
                case MessageType.AUDIO_FILE:
                    ViewType = ChatConstants.CHAT_AUDIO_VIEW;
                    break;
                case MessageType.VIDEO_FILE:
                    ViewType = ChatConstants.CHAT_VIDEO_VIEW;
                    break;
                case MessageType.FILE_STREAM:
                    ViewType = ChatConstants.CHAT_FILE_VIEW;
                    break;
                case MessageType.LINK_MESSAGE:
                    if (IsEditMode)
                    {
                        ViewType = ChatConstants.CHAT_PLAIN_EMOTICON_VIEW;
                    }
                    else
                    {
                        ViewType = ChatConstants.CHAT_LINK_VIEW;
                    }
                    break;
                case MessageType.LOCATION_MESSAGE:
                    ViewType = ChatConstants.CHAT_LOCATION_VIEW;
                    break;
                case MessageType.RING_MEDIA_MESSAGE:
                    ViewType = ChatConstants.CHAT_RING_MEDIA_VIEW;
                    break;
                case MessageType.CONTACT_SHARE:
                    ViewType = ChatConstants.CHAT_CONTACT_VIEW;
                    break;
            }
        }

        public void LoadViewTimer(MessageModel messageModel)
        {
            if (messageModel.IsSecretChat)
            {
                if (messageModel.FromFriend && messageModel.MessageType != MessageType.AUDIO_FILE && messageModel.MessageType != MessageType.VIDEO_FILE)
                {
                    this.CreateDeleteTimer(messageModel);
                }
                else
                {
                    this.DistroyDeleteTimer();
                }

                if (!messageModel.FromFriend)
                {
                    this.CreateSecretCounterTimer(messageModel);
                }
                else
                {
                    this.DistroySecretCounterTimer();
                }
            }
            else
            {
                this.DistroyDeleteTimer();
                this.DistroySecretCounterTimer();
            }
        }

        public void LoadEvent(MessageModel messageModel)
        {
            if (messageModel.MessageType != MessageType.DELETE_MESSAGE)
            {
                if (!messageModel.ViewModel.IsRoomChat)
                {
                    messageModel.ChatOpened -= ChatHelpers.MessageViewModel_OnChatOpened;
                    messageModel.ChatOpened += ChatHelpers.MessageViewModel_OnChatOpened;
                }

                if (messageModel.MessageType == MessageType.AUDIO_FILE)
                {
                    messageModel.AudioPlayStateChange -= ChatHelpers.MediaPlayer_AudioPlayStateChange;
                    messageModel.AudioPlayStateChange += ChatHelpers.MediaPlayer_AudioPlayStateChange;
                }
            }
            else
            {
                messageModel.ChatOpened -= ChatHelpers.MessageViewModel_OnChatOpened;
                messageModel.AudioPlayStateChange -= ChatHelpers.MediaPlayer_AudioPlayStateChange;
            }
        }

        public void CreateDeleteTimer(MessageModel messageModel)
        {
            if (SecretDeleteTimer == null)
            {
                SecretDeleteTimer = new CustomizeTimer();
                SecretDeleteTimer.IntervalInSecond = 1;
                SecretDeleteTimer.Tag = messageModel;
                SecretDeleteTimer.Tick += ChatHelpers.SecretDeleteTimer_Tick;
            }
        }

        public void DistroyDeleteTimer()
        {
            if (SecretDeleteTimer != null)
            {
                SecretDeleteTimer.Tick -= ChatHelpers.SecretDeleteTimer_Tick;
                SecretDeleteTimer.Stop();
                SecretDeleteTimer = null;
                SecretCount = 0;
            }
        }

        public void CreateSecretCounterTimer(MessageModel messageModel)
        {
            if (SecretCounterTimer == null)
            {
                SecretCounterTimer = new CustomizeTimer();
                SecretCounterTimer.IntervalInSecond = 1;
                SecretCounterTimer.Tag = messageModel;
                SecretCounterTimer.Tick += ChatHelpers.SecretCounterTimer_Tick;
            }
        }

        public void DistroySecretCounterTimer()
        {
            if (SecretCounterTimer != null)
            {
                SecretCounterTimer.Tick -= ChatHelpers.SecretCounterTimer_Tick;
                SecretCounterTimer.Stop();
                SecretCounterTimer = null;
            }
        }

        public void StartVideoDeleteTimer(MessageModel messageModel)
        {
            long diff = messageModel.Duration - messageModel.OffsetPosition;
            if (messageModel.IsSecretChat && messageModel.FromFriend && diff > 0)
            {
                if (SecretDeleteTimer == null)
                {
                    SecretDeleteTimer = new CustomizeTimer();
                    SecretDeleteTimer.IntervalInSecond = 1;
                    SecretDeleteTimer.Tag = messageModel;
                    SecretDeleteTimer.Tick += ChatHelpers.SecretDeleteTimer_Tick;
                }
                SecretCount = (int)diff;
                SecretDeleteTimer.Start();
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion Utility Methods

    }

    public class LastStatusModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string _DeliveredPacketID;
        public string DeliveredPacketID
        {
            get { return _DeliveredPacketID; }
            set
            {
                if (value == _DeliveredPacketID)
                    return;

                _DeliveredPacketID = value;
                this.OnPropertyChanged("DeliveredPacketID");
            }
        }

        private long _LastDeliveredDate;
        public long LastDeliveredDate
        {
            get { return _LastDeliveredDate; }
            set
            {
                if (value == _LastDeliveredDate)
                    return;

                _LastDeliveredDate = value;
                this.OnPropertyChanged("LastDeliveredDate");
            }
        }

        private string _SeenPacketID;
        public string SeenPacketID
        {
            get { return _SeenPacketID; }
            set
            {
                if (value == _SeenPacketID)
                    return;

                _SeenPacketID = value;
                this.OnPropertyChanged("SeenPacketID");
            }
        }

        private long _LastSeenDate;
        public long LastSeenDate
        {
            get { return _LastSeenDate; }
            set
            {
                if (value == _LastSeenDate)
                    return;

                _LastSeenDate = value;
                this.OnPropertyChanged("LastSeenDate");
            }
        }

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void Reset()
        {
            this.LastDeliveredDate = 0;
            this.LastSeenDate = 0;
            this.DeliveredPacketID = null;
            this.SeenPacketID = null;
        }
    }
}

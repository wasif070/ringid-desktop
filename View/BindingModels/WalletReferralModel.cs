﻿using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace View.BindingModels
{
    public class WalletReferralModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private long userIdentity = 0;
        public long UserIdentity
        {
            get { return userIdentity; }
            set { userIdentity = value; this.OnPropertyChanged("UserIdentity"); }
        }

        private long userTableID = 0;
        public long UserTableID
        {
            get { return userTableID; }
            set { userTableID = value; this.OnPropertyChanged("UserTableID"); }
        }


        private string fullName = string.Empty;
        public string FullName
        {
            get { return fullName; }
            set { fullName = value; this.OnPropertyChanged("FullName"); }
        }

        private string  gender = string.Empty;
        public string  Gender
        {
            get { return gender; }
            set { gender = value; this.OnPropertyChanged("Gender"); }
        }

        private string profileImage = string.Empty;
        public string ProfileImage
        {
            get { return profileImage; }
            set { profileImage = value; this.OnPropertyChanged("ProfileImage"); }
        }

        private long profileImageID = 0;
        public long ProfileImageID
        {
            get { return profileImageID; }
            set { profileImageID = value; this.OnPropertyChanged("ProfileImageID"); }
        }        

        private bool accepted = false;
        public bool Accepted
        {
            get { return accepted; }
            set { accepted = value; this.OnPropertyChanged("Accepted"); }
        }

        private int matchedBy = 0;
        public int MatchedBy
        {
            get { return matchedBy; }
            set { matchedBy = value; this.OnPropertyChanged("MatchedBy"); }
        }

        private long updateTime = 0;
        public long UpdateTime
        {
            get { return updateTime; }
            set { updateTime = value; this.OnPropertyChanged("UpdateTime"); }
        }

        private int reasonCode = 0;
        public int ReasonCode
        {
            get { return reasonCode; }
            set { reasonCode = value; this.OnPropertyChanged("ReasonCode"); }
        }

        private int callAccess = 1;
        public int CallAccess
        {
            get { return callAccess; }
            set { callAccess = value; this.OnPropertyChanged("CallAccess"); }
        }

        private int chatAccess = 1;
        public int ChatAccess
        {
            get { return chatAccess; }
            set { chatAccess = value; this.OnPropertyChanged("ChatAccess"); }
        }

        private int feedAccess = 1;
        public int FeedAccess
        {
            get { return feedAccess; }
            set { feedAccess = value; this.OnPropertyChanged("FeedAccess"); }
        }

        private int contactType = 0;
        public int ContactType
        {
            get { return contactType; }
            set { contactType = value; this.OnPropertyChanged("ContactType"); }
        }

        private int numberOfMutualFriend = 0;
        public int NumberOfMutualFriend
        {
            get { return numberOfMutualFriend; }
            set { numberOfMutualFriend = value; this.OnPropertyChanged("NumberOfMutualFriend"); }
        }

        private int friendshipStatus = 0;
        public int FriendshipStatus
        {
            get { return friendshipStatus; }
            set { friendshipStatus = value; this.OnPropertyChanged("FriendshipStatus"); }
        }

        public static WalletReferralModel LoadData(JObject jObj)
        {
            WalletReferralModel model = new WalletReferralModel();
            try
            {
                if (jObj[JsonKeys.UserIdentity] != null)
                {
                    model.UserIdentity = (long)jObj[JsonKeys.UserIdentity];
                }
                if (jObj[JsonKeys.UserTableID] !=null)
                {
                    model.UserTableID = (long)jObj[JsonKeys.UserTableID];
                }
                if (jObj[JsonKeys.FullName] != null)
                {
                    model.FullName = (string)jObj[JsonKeys.FullName];
                }
                if (jObj[JsonKeys.Gender] != null)
                {
                    model.Gender = (string)jObj[JsonKeys.Gender];
                }
                if (jObj[JsonKeys.FriendshipStatus] != null)
                {
                    model.FriendshipStatus = (int)jObj[JsonKeys.FriendshipStatus];
                }
                if (jObj[JsonKeys.ProfileImage] != null)
                {
                    model.ProfileImage = (string)jObj[JsonKeys.ProfileImage];
                }
                if (jObj[JsonKeys.ProfileImageId] != null)
                {
                    model.ProfileImageID = (long)jObj[JsonKeys.ProfileImageId];
                }
                if (jObj[JsonKeys.ContactType] != null)
                {
                    model.ContactType = (int)jObj[JsonKeys.ContactType];
                }
                if (jObj[JsonKeys.NumberOfMutualFriend] != null)
                {
                    model.NumberOfMutualFriend = (int)jObj[JsonKeys.NumberOfMutualFriend];
                }
                if (jObj[JsonKeys.Accept] != null)
                {
                    model.Accepted = (bool)jObj[JsonKeys.Accept];
                }
                if (jObj[JsonKeys.UpdateTime] != null)
                {
                    model.UpdateTime = (long)jObj[JsonKeys.UpdateTime];
                }
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(typeof(WalletReferralModel).Name).Error("WALLET REFERRAL MODEL ==> " + ex.Message + Environment.NewLine + ex.StackTrace);
            }
            return model;
        }

    }
}

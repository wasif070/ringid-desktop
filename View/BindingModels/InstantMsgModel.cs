﻿using Models.Entity;
using System.ComponentModel;
using System.Windows;

namespace View.BindingModels
{
    public class InstantMsgModel : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChangedNotify(string methodName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(methodName));
            }
        }

        public void Onload(InstantMessageDTO ins)
        {
            this.InstantMessage = ins.InstantMessage;
            this.VisEditDelete = (ins.MsgType == 1) ? Visibility.Visible : Visibility.Hidden;
            CurrentInstance = this;
        }

        private InstantMsgModel _CurrentInstance;
        public InstantMsgModel CurrentInstance
        {
            get
            {
                return _CurrentInstance;
            }
            set
            {
                if (value == _CurrentInstance)
                    return;
                _CurrentInstance = value;
                this.OnPropertyChangedNotify("CurrentInstance");
            }
        }


        private string _InstantMessage;
        public string InstantMessage
        {
            get
            {
                return _InstantMessage;
            }
            set
            {
                if (value == _InstantMessage)
                    return;
                _InstantMessage = value;
                this.OnPropertyChangedNotify("InstantMessage");
            }
        }
        private Visibility _VisEditDelete;
        public Visibility VisEditDelete
        {
            get
            {
                return _VisEditDelete;
            }
            set
            {
                if (value == _VisEditDelete)
                    return;
                _VisEditDelete = value;
                this.OnPropertyChangedNotify("VisEditDelete");
            }
        }
    }
}

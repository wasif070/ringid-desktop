﻿using System;
using System.ComponentModel;
using log4net;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using VirtualPanel.Controls;

namespace View.BindingModels
{
    public class UserBasicInfoModel : INotifyPropertyChanged
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(UserBasicInfoModel).Name);

        #region Constructor
        public UserBasicInfoDTO basicInfoDTO;

        public UserBasicInfoModel()
        {
            _CurrentInstance = this;
        }

        public UserBasicInfoModel(UserBasicInfoDTO basicInfoDTO)
        {
            _CurrentInstance = this;
            this.basicInfoDTO = basicInfoDTO;
            LoadData(basicInfoDTO);
        }

        #endregion Constructor

        #region Property

        private UserBasicInfoModel _CurrentInstance;
        public UserBasicInfoModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set { _CurrentInstance = value; this.OnPropertyChanged("CurrentInstance"); }
        }

        private int _placeHolderType = 1;
        public int PlaceHolderType
        {
            get { return _placeHolderType; }
            set { _placeHolderType = value; this.OnPropertyChanged("Type"); }
        }

        private int _friendListType = -1;
        public int FriendListType
        {
            get { return _friendListType; }
            set
            {
                if (value == _friendListType) return;
                _friendListType = value; this.OnPropertyChanged("FriendListType");
            }
        }

        private VisibilityModel _VisibilityModel = new VisibilityModel();
        public VisibilityModel VisibilityModel
        {
            get { return _VisibilityModel; }
        }

        private UserShortInfoModel _ShortInfoModel = new UserShortInfoModel();
        public UserShortInfoModel ShortInfoModel
        {
            get { return _ShortInfoModel; }
        }

        private long _PivotId = 0;
        public long PivotId
        {
            get { return _PivotId; }
            set { _PivotId = value; this.OnPropertyChanged("PivotId"); }
        }

        private string _Gender;
        public string Gender
        {
            get { return _Gender; }
            set
            {
                if (value == _Gender) return;
                _Gender = value; this.OnPropertyChanged("Gender");
            }
        }

        private string _Email;
        public string Email
        {
            get { return _Email; }
            set
            {
                if (value == _Email) return;
                _Email = value; this.OnPropertyChanged("Email");
            }
        }

        private string _MobilePhone;
        public string MobilePhone
        {
            get { return _MobilePhone; }
            set
            {
                if (value == _MobilePhone) return;
                _MobilePhone = value; this.OnPropertyChanged("MobilePhone");
            }
        }

        private string _MobilePhoneDialingCode;
        public string MobilePhoneDialingCode
        {
            get { return _MobilePhoneDialingCode; }
            set
            {
                if (value == _MobilePhoneDialingCode) return;
                _MobilePhoneDialingCode = value; this.OnPropertyChanged("MobilePhoneDialingCode");
            }
        }

        private double _FavoriteRank;
        public double FavoriteRank
        {
            get { return _FavoriteRank; }
            set
            {
                if (value == _FavoriteRank) return;
                _FavoriteRank = value; this.OnPropertyChanged("FavoriteRank");
            }
        }

        private long _NumberOfChats;
        public long NumberOfChats
        {
            get { return _NumberOfChats; }
            set
            {
                if (value == _NumberOfChats) return;
                _NumberOfChats = value; this.OnPropertyChanged("NumberOfChats");
            }
        }

        private long _NumberOfCalls;
        public long NumberOfCalls
        {
            get { return _NumberOfCalls; }
            set
            {
                if (value == _NumberOfCalls) return;
                _NumberOfCalls = value; this.OnPropertyChanged("NumberOfCalls");
            }
        }

        private int _MatchedBy;
        public int MatchedBy
        {
            get { return _MatchedBy; }
            set
            {
                if (value == _MatchedBy) return;
                _MatchedBy = value; this.OnPropertyChanged("MatchedBy");
            }
        }

        private long _NumberOfMutualFriends;
        public long NumberOfMutualFriends
        {
            get { return _NumberOfMutualFriends; }
            set
            {
                if (value == _NumberOfMutualFriends) return;
                _NumberOfMutualFriends = value; this.OnPropertyChanged("NumberOfMutualFriends");
            }
        }

        private string _CurrentCity;
        public string CurrentCity
        {
            get { return _CurrentCity; }
            set
            {
                if (value == _CurrentCity) return;
                _CurrentCity = value; this.OnPropertyChanged("CurrentCity");
            }
        }

        private string _HomeCity;
        public string HomeCity
        {
            get { return _HomeCity; }
            set
            {
                if (value == _HomeCity) return;
                _HomeCity = value; this.OnPropertyChanged("HomeCity");
            }
        }

        /*private long _MarriageDay;
        public long MarriageDay
        {
            get { return _MarriageDay; }
            set
            {
                if (value == _MarriageDay) return;
                _MarriageDay = value; this.OnPropertyChanged("MarriageDay");
            }
        }*/

        private DateTime _MarriageDate;
        public DateTime MarriageDate
        {
            get { return _MarriageDate; }
            set
            {
                if (value == _MarriageDate) return;
                _MarriageDate = value; this.OnPropertyChanged("MarriageDate");
            }
        }

        private string _AboutMe;
        public string AboutMe
        {
            get { return _AboutMe; }
            set
            {
                if (value == _AboutMe) return;
                _AboutMe = value; this.OnPropertyChanged("AboutMe");
            }
        }

        private long _ContactAddedTime;
        public long ContactAddedTime
        {
            get { return _ContactAddedTime; }
            set
            {
                if (value == _ContactAddedTime) return;
                _ContactAddedTime = value; OnPropertyChanged("ContactAddedTime");
            }
        }
       
        private long _ContactUpdateTime;
        public long ContactUpdateTime
        {
            get { return _ContactUpdateTime; }
            set
            {
                if (value == _ContactUpdateTime) return;
                _ContactUpdateTime = value; OnPropertyChanged("ContactUpdateTime");
            }
        }
     
        private long _UpdateTime;
        public long UpdateTime
        {
            get { return _UpdateTime; }
            set
            {
                if (value == _UpdateTime) return;
                _UpdateTime = value; OnPropertyChanged("UpdateTime");
            }
        }

        private int _ContactAddedReadUnread;
        public int ContactAddedReadUnread
        {
            get { return _ContactAddedReadUnread; }
            set
            {
                if (value == _ContactAddedReadUnread) return;
                _ContactAddedReadUnread = value; OnPropertyChanged("ContactAddedReadUnread");
            }
        }

        private int _IncomingStatus;
        public int IncomingStatus
        {
            get { return _IncomingStatus; }
            set
            {
                if (value == _IncomingStatus) return;
                _IncomingStatus = value; OnPropertyChanged("IncomingStatus");
            }
        }

        private int _IsEmailVerified;
        public int IsEmailVerified
        {
            get { return _IsEmailVerified; }
            set
            {
                if (value == _IsEmailVerified) return;
                _IsEmailVerified = value; this.OnPropertyChanged("IsEmailVerified");
            }
        }

        private int _IsMobileNumberVerified;
        public int IsMobileNumberVerified
        {
            get { return _IsMobileNumberVerified; }
            set
            {
                if (value == _IsMobileNumberVerified) return;
                _IsMobileNumberVerified = value; this.OnPropertyChanged("IsMobileNumberVerified");
            }
        }

        private int _CropImageX;
        public int CropImageX
        {
            get { return _CropImageX; }
            set
            {
                if (value == _CropImageX) return;
                _CropImageX = value; this.OnPropertyChanged("CropImageX");
            }
        }

        private int _CropImageY;
        public int CropImageY
        {
            get { return _CropImageY; }
            set
            {
                if (value == _CropImageY) return;
                _CropImageY = value; this.OnPropertyChanged("CropImageY");
            }
        }

        private string _CoverImage;
        public string CoverImage
        {
            get { return _CoverImage; }
            set
            {
                if (value == _CoverImage) return;
                _CoverImage = value; this.OnPropertyChanged("CoverImage");
            }
        }

        /*private long _BirthDay;
        public long BirthDay
        {
            get { return _BirthDay; }
            set
            {
                if (value == _BirthDay) return;
                _BirthDay = value; this.OnPropertyChanged("BirthDay");
            }
        }*/

        private DateTime _BirthDate;
        public DateTime BirthDate
        {
            get { return _BirthDate; }
            set
            {
                if (value == _BirthDate) return;
                _BirthDate = value; this.OnPropertyChanged("BirthDate");
            }
        }

        private string _Country;
        public string Country
        {
            get { return _Country; }
            set
            {
                if (value == _Country) return;
                _Country = value; this.OnPropertyChanged("Country");
            }
        }

        private int _EmailPrivacy;
        public int EmailPrivacy
        {
            get { return _EmailPrivacy; }
            set
            {
                if (value == _EmailPrivacy) return;
                _EmailPrivacy = value; this.OnPropertyChanged("EmailPrivacy");
            }
        }

        private int _MobilePrivacy;
        public int MobilePrivacy
        {
            get { return _MobilePrivacy; }
            set
            {
                if (value == _MobilePrivacy) return;
                _MobilePrivacy = value; this.OnPropertyChanged("MobilePrivacy");
            }
        }

        private int _ProfileImagePrivacy;
        public int ProfileImagePrivacy
        {
            get { return _ProfileImagePrivacy; }
            set
            {
                if (value == _ProfileImagePrivacy) return;
                _ProfileImagePrivacy = value; this.OnPropertyChanged("ProfileImagePrivacy");
            }
        }

        private int _CoverImagePrivacy;
        public int CoverImagePrivacy
        {
            get { return _CoverImagePrivacy; }
            set
            {
                if (value == _CoverImagePrivacy) return;
                _CoverImagePrivacy = value; this.OnPropertyChanged("CoverImagePrivacy");
            }
        }

        private int _BirthDayPrivacy;
        public int BirthDayPrivacy
        {
            get { return _BirthDayPrivacy; }
            set
            {
                if (value == _BirthDayPrivacy) return;
                _BirthDayPrivacy = value; this.OnPropertyChanged("BirthDayPrivacy");
            }
        }

        private Guid _ProfileImageId;
        public Guid ProfileImageId
        {
            get { return _ProfileImageId; }
            set
            {
                if (value == _ProfileImageId) return;
                _ProfileImageId = value; this.OnPropertyChanged("ProfileImageId");
            }
        }

        private Guid _CoverImageId;
        public Guid CoverImageId
        {
            get { return _CoverImageId; }
            set
            {
                if (value == _CoverImageId) return;
                _CoverImageId = value; this.OnPropertyChanged("CoverImageId");
            }
        }

        private string _DeviceToken;
        public string DeviceToken
        {
            get { return _DeviceToken; }
            set
            {
                if (value == _DeviceToken) return;
                _DeviceToken = value; this.OnPropertyChanged("DeviceToken");
            }
        }

        private int _CallAccess;
        public int CallAccess
        {
            get { return _CallAccess; }
            set
            {
                if (value == _CallAccess) return;
                _CallAccess = value; this.OnPropertyChanged("CallAccess");
            }
        }

        private int _ChatAccess;
        public int ChatAccess
        {
            get { return _ChatAccess; }
            set
            {
                if (value == _ChatAccess) return;
                _ChatAccess = value; this.OnPropertyChanged("ChatAccess");
            }
        }

        private int _FeedAccess;
        public int FeedAccess
        {
            get { return _FeedAccess; }
            set
            {
                if (value == _FeedAccess) return;
                _FeedAccess = value; this.OnPropertyChanged("FeedAccess");
            }
        }

        private int _AnonymousCallAccess;
        public int AnonymousCallAccess
        {
            get { return _AnonymousCallAccess; }
            set
            {
                if (value == _AnonymousCallAccess) return;
                _AnonymousCallAccess = value; this.OnPropertyChanged("AnonymousCallAccess");
            }
        }

        private int _AnonymousChatAccess;
        public int AnonymousChatAccess
        {
            get { return _AnonymousChatAccess; }
            set
            {
                if (value == _AnonymousChatAccess) return;
                _AnonymousChatAccess = value; this.OnPropertyChanged("AnonymousChatAccess");
            }
        }

        private long _CircleId;
        public long CircleId
        {
            get { return _CircleId; }
            set
            {
                _CircleId = value;
                this.OnPropertyChanged("CircleId");
            }
        }

        private bool _IsAdmin;
        public bool IsAdmin
        {
            get { return _IsAdmin; }
            set
            {
                if (value == _IsAdmin) return;
                _IsAdmin = value; this.OnPropertyChanged("IsAdmin");
            }
        }

        private int _CircleMembershipStatus;
        public int CircleMembershipStatus
        {
            get { return _CircleMembershipStatus; }
            set { _CircleMembershipStatus = value; this.OnPropertyChanged("CircleMembershipStatus"); }
        }

        private string _ChatBgUrl;
        public string ChatBgUrl
        {
            get { return _ChatBgUrl; }
            set
            {
                if (value == _ChatBgUrl) return;
                _ChatBgUrl = value; this.OnPropertyChanged("ChatBgUrl");
            }
        }

        private string _EventChatBgUrl;
        public string EventChatBgUrl
        {
            get { return _EventChatBgUrl; }
            set
            {
                if (value == _EventChatBgUrl) return;
                _EventChatBgUrl = value; this.OnPropertyChanged("EventChatBgUrl");
            }
        }

        private string _ChatMinPacketID;
        public string ChatMinPacketID
        {
            get { return _ChatMinPacketID; }
            set
            {
                if (value == _ChatMinPacketID) return;
                _ChatMinPacketID = value; this.OnPropertyChanged("ChatMinPacketID");
            }
        }

        private string _ChatMaxPacketID;
        public string ChatMaxPacketID
        {
            get { return _ChatMaxPacketID; }
            set
            {
                if (value == _ChatMaxPacketID) return;
                _ChatMaxPacketID = value; this.OnPropertyChanged("ChatMaxPacketID");
            }
        }

        private bool _ChatHistoryNotFound;
        public bool ChatHistoryNotFound
        {
            get { return _ChatHistoryNotFound; }
            set
            {
                if (value == _ChatHistoryNotFound) return;
                _ChatHistoryNotFound = value; this.OnPropertyChanged("ChatHistoryNotFound");
            }
        }

        private int _FacebookValidated;
        public int FacebookValidated { get { return _FacebookValidated; } set { _FacebookValidated = value; this.OnPropertyChanged("FacebookValidated"); } }

        private int _TwitterValidated;
        public int TwitterValidated { get { return _TwitterValidated; } set { _TwitterValidated = value; this.OnPropertyChanged("TwitterValidated"); } }

        private int _AllowIncomingFriendRequest;
        public int AllowIncomingFriendRequest
        {
            get { return _AllowIncomingFriendRequest; }
            set
            {
                if (value == _AllowIncomingFriendRequest) return;
                _AllowIncomingFriendRequest = value; this.OnPropertyChanged("AllowIncomingFriendRequest");
            }
        }

        private int _AllowFriendsToAddMe;
        public int AllowFriendsToAddMe
        {
            get { return _AllowFriendsToAddMe; }
            set
            {
                if (value == _AllowFriendsToAddMe) return;
                _AllowFriendsToAddMe = value; this.OnPropertyChanged("AllowFriendsToAddMe");
            }
        }
        private int _AutoAddFriends;
        public int AutoAddFriends
        {
            get { return _AutoAddFriends; }
            set
            {
                if (value == _AutoAddFriends) return;
                _AutoAddFriends = value; this.OnPropertyChanged("AutoAddFriends");
            }
        }

        private int _NotificationValidity;
        public int NotificationValidity
        {
            get { return _NotificationValidity; }
            set
            {
                if (value == _NotificationValidity) return;
                _NotificationValidity = value; this.OnPropertyChanged("NotificationValidity");
            }
        }

        private bool _IsAccessChanging;
        public bool IsAccessChanging
        {
            get { return _IsAccessChanging; }
            set
            {
                if (value == _IsAccessChanging) return;
                _IsAccessChanging = value; this.OnPropertyChanged("IsAccessChanging");
            }
        }

        #endregion Property

        #region Utility Method

        public void LoadData(UserBasicInfoDTO basicInfoDTO)
        {
            if (basicInfoDTO != null)
            {
                this.ShortInfoModel.UserIdentity = basicInfoDTO.RingID;
                this.ShortInfoModel.UserTableID = basicInfoDTO.UserTableID;
                this.ShortInfoModel.FullName = basicInfoDTO.FullName;
                this.ShortInfoModel.Presence = basicInfoDTO.Presence;
                this.ShortInfoModel.LastOnlineTime = basicInfoDTO.LastOnlineTime;
                this.ShortInfoModel.ProfileImage = basicInfoDTO.ProfileImage;
                this.ShortInfoModel.Device = basicInfoDTO.Device;
                this.ShortInfoModel.FriendShipStatus = basicInfoDTO.FriendShipStatus;
                this.ShortInfoModel.Mood = basicInfoDTO.Mood;
                this.ShortInfoModel.IsSecretVisible = basicInfoDTO.IsSecretVisible;
                this.ShortInfoModel.ContactType = basicInfoDTO.ContactType;
                this.ShortInfoModel.IsSpecialContact = (basicInfoDTO.ContactType == SettingsConstants.SPECIAL_CONTACT || basicInfoDTO.UserTableID == DefaultSettings.RINGID_OFFICIAL_UTID);
                this.ShortInfoModel.ImSoundEnabled = basicInfoDTO.ImSoundEnabled;
                this.ShortInfoModel.ImNotificationEnabled = basicInfoDTO.ImNotificationEnabled;
                this.ShortInfoModel.ImPopupEnabled = basicInfoDTO.ImPopupEnabled;
                this.Gender = basicInfoDTO.Gender;
                this.Email = basicInfoDTO.Email;
                this.MobilePhone = basicInfoDTO.MobileNumber;
                this.MobilePhoneDialingCode = basicInfoDTO.MobileDialingCode;
                this.FavoriteRank = basicInfoDTO.FavoriteRank;
                this.NumberOfChats = basicInfoDTO.NumberOfChats;
                this.NumberOfCalls = basicInfoDTO.NumberOfCalls;
                this.MatchedBy = basicInfoDTO.MatchedBy;
                this.NumberOfMutualFriends = basicInfoDTO.NumberOfMutualFriends;
                this.CurrentCity = basicInfoDTO.CurrentCity;
                this.HomeCity = basicInfoDTO.HomeCity;
                //this.MarriageDay = basicInfoDTO.MarriageDay;
                this.MarriageDate = basicInfoDTO.MarriageDate;
                this.AboutMe = basicInfoDTO.AboutMe;
                this.IsEmailVerified = basicInfoDTO.IsEmailVerified;
                this.IsMobileNumberVerified = basicInfoDTO.IsMobileNumberVerified;
                this.CropImageX = basicInfoDTO.CropImageX;
                this.CropImageY = basicInfoDTO.CropImageY;
                this.CoverImage = basicInfoDTO.CoverImage;
                //this.BirthDay = basicInfoDTO.BirthDay;
                this.BirthDate = basicInfoDTO.BirthDate;
                this.Country = basicInfoDTO.Country;
                this.EmailPrivacy = basicInfoDTO.EmailPrivacy;
                this.MobilePrivacy = basicInfoDTO.MobilePrivacy;
                this.ProfileImagePrivacy = basicInfoDTO.ProfileImagePrivacy;
                this.CoverImagePrivacy = basicInfoDTO.CoverImagePrivacy;
                this.BirthDayPrivacy = basicInfoDTO.BirthdayPrivacy;
                this.ProfileImageId = basicInfoDTO.ProfileImageId;
                this.CoverImageId = basicInfoDTO.CoverImageId;
                this.DeviceToken = basicInfoDTO.DeviceToken;
                this.CallAccess = basicInfoDTO.CallAccess;
                this.ChatAccess = basicInfoDTO.ChatAccess;
                this.FeedAccess = basicInfoDTO.FeedAccess;
                this.AnonymousCallAccess = basicInfoDTO.AnonymousCallAccess;
                this.AnonymousChatAccess = basicInfoDTO.AnonymousChatAccess;
                this.ChatBgUrl = basicInfoDTO.ChatBgUrl;
                this.ChatMinPacketID = basicInfoDTO.ChatMinPacketID;
                this.ChatMaxPacketID = basicInfoDTO.ChatMaxPacketID;
                this.ContactAddedTime = basicInfoDTO.ContactAddedTime;
                this.ContactUpdateTime = basicInfoDTO.ContactUpdateTime;
                this.UpdateTime = basicInfoDTO.UpdateTime;
                this.ContactAddedReadUnread = basicInfoDTO.ContactAddedReadUnread;
                this.IncomingStatus = basicInfoDTO.IncomingStatus;
                this.FacebookValidated = basicInfoDTO.FacebookValidated;
                this.TwitterValidated = basicInfoDTO.TwitterValidated;
                this.AllowIncomingFriendRequest = basicInfoDTO.AllowIncomingFriendRequest;
                this.AllowFriendsToAddMe = basicInfoDTO.AllowFriendsToAddMe;
                this.AutoAddFriends = basicInfoDTO.AutoAddFriends;
                this.NotificationValidity = basicInfoDTO.NotificationValidity;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) { handler(this, new PropertyChangedEventArgs(propertyName)); }
        }

        #endregion Utility Methods

        public CircleMemberDTO GetCircleMemberDTOFromModel(long cId, long ut)
        {
            CircleMemberDTO circleMember = new CircleMemberDTO();
            circleMember.CircleId = cId;
            circleMember.UpdateTime = ut;
            circleMember.RingID = this.ShortInfoModel.UserIdentity;
            circleMember.IsAdmin = IsAdmin;
            circleMember.UserTableID = this.ShortInfoModel.UserTableID;
            circleMember.FullName = this.ShortInfoModel.FullName;
            return circleMember;
        }

        public void LoadDataFromCircleMember(CircleMemberDTO circleMember)
        {
            this.ShortInfoModel.UserIdentity = circleMember.RingID;
            this.ShortInfoModel.UserTableID = circleMember.UserTableID;
            if (!string.IsNullOrEmpty(circleMember.FullName)) this.ShortInfoModel.FullName = circleMember.FullName;
            if (!string.IsNullOrEmpty(circleMember.ProfileImage)) this.ShortInfoModel.ProfileImage = circleMember.ProfileImage;
            CircleId = circleMember.CircleId;
            IsAdmin = circleMember.IsAdmin;
        }

        public double GetEstimatedHeight(double availableWidth, VirtualViewType viewType)
        {
            double value = 0;
            try
            {
                switch (viewType)
                {
                    case VirtualViewType.GROUP_CREATE_PANEL__FRIEND_LIST:
                        value = 50;
                        break;
                    case VirtualViewType.MEDIA_SHARE_RECENT_LIST_PANEL__RECENT_LIST:
                        value = 50;
                        break;
                    case VirtualViewType.CALL_DIVERT_PANEL_LIST:
                        value = 57;
                        break;
                    case VirtualViewType.STATUS_TAG_PANEL_LIST:
                        value = 51;
                        break;
                    case VirtualViewType.GROUP_CREATE_PANEL_FROM_CHAT_CALL_PANEL__FRIEND_LIST:
                        value = 49;
                        break;
                    case VirtualViewType.ROOM_MEMBERS_PANEL__MEMBER_LIST:
                        value = 50;
                        break;
                    case VirtualViewType.CONTACT_PANEL__CONTACT_SHARE_LIST:
                        value = 50;
                        break;
                    default:
                        value = 55;
                        break;
                }
            }
            catch (Exception ex) { log.Error("Error: GetEstimatedHeight() ==> " + ex.Message + "\n" + ex.StackTrace); }
            return value;
        }
    }

    public class UserShortInfoModel : BaseUserProfileModel
    {
        #region Constructor

        public UserShortInfoModel()
        {
            //_CurrentInstance = this;
        }

        public UserShortInfoModel(UserShortInfoModel basicInfoDTO)
        {
            //_CurrentInstance = this;
            LoadData(basicInfoDTO);
        }

        #endregion Constructor

        #region Property

        //private UserShortInfoModel _CurrentInstance;
        //public UserShortInfoModel CurrentInstance
        //{
        //    get { return _CurrentInstance; }
        //    set
        //    {
        //        _CurrentInstance = value;
        //        this.OnPropertyChanged("CurrentInstance");
        //    }
        //}


        //private long _UserIdentity;
        //public long UserIdentity
        //{
        //    get { return _UserIdentity; }
        //    set
        //    {
        //        if (value == _UserIdentity)
        //            return;

        //        _UserIdentity = value;
        //        this.OnPropertyChanged("UserIdentity");
        //    }
        //}

        //private long _UserTableId;
        //public long UserTableID
        //{
        //    get { return _UserTableId; }
        //    set
        //    {
        //        if (value == _UserTableId)
        //            return;

        //        _UserTableId = value;
        //        this.OnPropertyChanged("UserTableID");
        //    }
        //}

        //private string _FullName;
        //public string FullName
        //{
        //    get { return _FullName; }
        //    set
        //    {
        //        if (value == _FullName)
        //            return;

        //        _FullName = value;
        //        this.OnPropertyChanged("FullName");
        //    }
        //}

        //private string _ProfileImage;
        //public string ProfileImage
        //{
        //    get { return _ProfileImage; }
        //    set
        //    {
        //        if (value == _ProfileImage)
        //            return;

        //        _ProfileImage = value;
        //        this.OnPropertyChanged("ProfileImage");
        //    }
        //}

        private int _FriendShipStatus;
        public int FriendShipStatus
        {
            get { return _FriendShipStatus; }
            set
            {
                if (value == _FriendShipStatus)
                    return;

                _FriendShipStatus = value;
                this.OnPropertyChanged("FriendShipStatus");
            }
        }

        private int _Presence;
        public int Presence
        {
            get { return _Presence; }
            set
            {
                if (value == _Presence)
                    return;

                _Presence = value;
                this.OnPropertyChanged("Presence");
            }
        }

        private int _Device;
        public int Device
        {
            get { return _Device; }
            set
            {
                if (value == _Device)
                    return;

                _Device = value;
                this.OnPropertyChanged("Device");
            }
        }

        //private bool _IsProfileHidden;
        //public bool IsProfileHidden
        //{
        //    get { return _IsProfileHidden; }
        //    set
        //    {
        //        if (value == _IsProfileHidden)
        //            return;

        //        _IsProfileHidden = value;
        //        this.OnPropertyChanged("IsProfileHidden");
        //    }
        //}

        private long _LastOnlineTime;
        public long LastOnlineTime
        {
            get { return _LastOnlineTime; }
            set
            {
                if (value == _LastOnlineTime)
                    return;

                _LastOnlineTime = value;
                this.OnPropertyChanged("LastOnlineTime");
            }
        }
        private bool _IsVisibleInTagList = false;
        public bool IsVisibleInTagList
        {
            get { return _IsVisibleInTagList; }
            set
            {
                if (value == _IsVisibleInTagList)
                    return;

                _IsVisibleInTagList = value;
                this.OnPropertyChanged("IsVisibleInTagList");
            }
        }

        private int _IsSecretVisible = 1;
        public int IsSecretVisible
        {
            get { return _IsSecretVisible; }
            set
            {
                if (value == _IsSecretVisible)
                    return;

                _IsSecretVisible = value;
                this.OnPropertyChanged("IsSecretVisible");
            }
        }

        private int _Mood;
        public int Mood
        {
            get { return _Mood; }
            set
            {
                if (value == _Mood)
                    return;

                _Mood = value;
                this.OnPropertyChanged("Mood");
            }
        }

        private int _ContactType;
        public int ContactType
        {
            get { return _ContactType; }
            set
            {
                if (value == _ContactType)
                    return;

                _ContactType = value;
                this.OnPropertyChanged("ContactType");
            }
        }

        private bool _ImSoundEnabled = true;
        public bool ImSoundEnabled
        {
            get { return _ImSoundEnabled; }
            set
            {
                if (value == _ImSoundEnabled)
                    return;

                _ImSoundEnabled = value;
                this.OnPropertyChanged("ImSoundEnabled");
            }
        }

        private bool _ImNotificationEnabled = true;
        public bool ImNotificationEnabled
        {
            get { return _ImNotificationEnabled; }
            set
            {
                if (value == _ImNotificationEnabled)
                    return;

                _ImNotificationEnabled = value;
                this.OnPropertyChanged("ImNotificationEnabled");
            }
        }

        private bool _ImPopupEnabled = true;
        public bool ImPopupEnabled
        {
            get { return _ImPopupEnabled; }
            set
            {
                if (value == _ImPopupEnabled)
                    return;

                _ImPopupEnabled = value;
                this.OnPropertyChanged("ImPopupEnabled");
            }
        }

        #endregion Property

        #region Utility Method

        public void LoadData(UserShortInfoModel shortInfoDTO, UserBasicInfoDTO dto = null)
        {
            if (shortInfoDTO != null)
            {
                this.UserIdentity = shortInfoDTO.UserIdentity;
                this.UserTableID = shortInfoDTO.UserTableID;
                this.FullName = shortInfoDTO.FullName;
                this.Presence = shortInfoDTO.Presence;
                this.Device = shortInfoDTO.Device;
                this.FriendShipStatus = shortInfoDTO.FriendShipStatus;
                this.ContactType = shortInfoDTO.ContactType;
                this.IsSpecialContact = (shortInfoDTO.ContactType == SettingsConstants.SPECIAL_CONTACT || shortInfoDTO.UserTableID == DefaultSettings.RINGID_OFFICIAL_UTID);
                this.IsProfileHidden = shortInfoDTO.IsProfileHidden;
                //this.ContactAddedTime = shortInfoDTO.ContactAddedTime;
                //this.ContactAddedReadUnread = shortInfoDTO.ContactAddedReadUnread;
            }
            else if (dto != null)
            {
                this.UserIdentity = dto.RingID;
                this.ProfileImage = dto.ProfileImage;
                this.UserTableID = dto.UserTableID;
                this.FullName = dto.FullName;
                this.Presence = dto.Presence;
                this.Device = dto.Device;
                this.FriendShipStatus = dto.FriendShipStatus;
                this.ContactType = dto.ContactType;
                this.IsSpecialContact = (dto.ContactType == SettingsConstants.SPECIAL_CONTACT || dto.UserTableID == DefaultSettings.RINGID_OFFICIAL_UTID);
                this.IsProfileHidden = dto.IsProfileHidden;
                //this.ContactAddedTime = dto.ContactAddedTime;
                //this.ContactAddedReadUnread = dto.ContactAddedReadUnread;
            }
        }

        public void LoadFromTransactionHistory(JObject txnHistoryObject)
        {
            if (txnHistoryObject[JsonKeys.OtherUserID] != null)
            {
                this.UserTableID = (long)txnHistoryObject[JsonKeys.OtherUserID];
            }
            if (txnHistoryObject[JsonKeys.UserIdentity] != null)
            {
                this.UserIdentity = (long)txnHistoryObject[JsonKeys.UserIdentity];
            }
            if (txnHistoryObject[JsonKeys.FullName] != null)
            {
                this.FullName = (string)txnHistoryObject[JsonKeys.FullName];
            }
            if (txnHistoryObject[JsonKeys.ProfileImage] != null)
            {
                this.ProfileImage = (string)txnHistoryObject[JsonKeys.ProfileImage];
            }
        }


        #endregion Utility Methods
    }

    public class BlockedNonFriendModel : INotifyPropertyChanged
    {
        public BlockedNonFriendModel()
        {

        }

        private long _UserTableID;
        public long UserTableID
        {
            get { return _UserTableID; }
            set
            {
                if (value == _UserTableID) return;
                _UserTableID = value; this.OnPropertyChanged("UserTableID");
            }
        }

        private bool _IsBlockedByMe = false;
        public bool IsBlockedByMe
        {
            get { return _IsBlockedByMe; }
            set
            {
                if (value == _IsBlockedByMe) return;
                _IsBlockedByMe = value; this.OnPropertyChanged("IsBlockedByMe");
            }
        }

        private bool _IsBlockedByFriend = false;
        public bool IsBlockedByFriend
        {
            get { return _IsBlockedByFriend; }
            set
            {
                if (value == _IsBlockedByFriend) return;
                _IsBlockedByFriend = value; this.OnPropertyChanged("IsBlockedByFriend");
            }
        }

        private bool _IsFriendRegDenied = false;
        public bool IsFriendRegDenied
        {
            get { return _IsFriendRegDenied; }
            set
            {
                if (value == _IsFriendRegDenied) return;
                _IsFriendRegDenied = value; this.OnPropertyChanged("IsFriendRegDenied");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

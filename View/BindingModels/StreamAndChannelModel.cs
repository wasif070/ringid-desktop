﻿using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace View.BindingModels
{
    public class StreamAndChannelModel : INotifyPropertyChanged
    {

        #region Constructor

        public StreamAndChannelModel() { }

        public StreamAndChannelModel(StreamDTO streamDTO)
        {
            LoadData(streamDTO);
        }

        public StreamAndChannelModel(ChannelDTO channelDTO)
        {
            LoadData(channelDTO);
        }

        #endregion Constructor

        #region Property

        private StreamModel _Stream;
        public StreamModel Stream
        {
            get { return _Stream; }
            set
            {
                if (value == _Stream) return;
                _Stream = value;
                this.OnPropertyChanged("Stream");
            }
        }

        private ChannelModel _Channel;
        public ChannelModel Channel
        {
            get { return _Channel; }
            set
            {
                if (value == _Channel) return;
                _Channel = value;
                this.OnPropertyChanged("Channel");
            }
        }

        private bool _IsCombinedView = true;
        public bool IsCombinedView
        {
            get { return _IsCombinedView; }
        }

        private int _ViewType;
        public int ViewType
        {
            get { return _ViewType; }
            private set
            {
                if (value == _ViewType) return;
                _ViewType = value;
                this.OnPropertyChanged("ViewType");
            }
        }

        private bool _IsViewOpened;
        public bool IsViewOpened
        {
            get { return _IsViewOpened; }
            set
            {
                if (value == _IsViewOpened)
                    return;

                _IsViewOpened = value;
                this.OnPropertyChanged("IsViewOpened");
            }
        }

        #endregion Property

        public void LoadData(StreamDTO streamDTO)
        {
            this.ViewType = SettingsConstants.TYPE_STREAM;
            if (Stream == null)
            {
                Stream = new StreamModel();
            }
            Stream.LoadData(streamDTO);
        }

        public void LoadData(ChannelDTO channelDTO)
        {
            this.ViewType = SettingsConstants.TYPE_CHANNEL;
            if (Channel == null)
            {
                Channel = new ChannelModel();
            }
            Channel.LoadData(channelDTO);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #region Utility

        #endregion

    }

    public class ParamModel : INotifyPropertyChanged
    {
        public ParamModel() { }

        private string _Title;
        public string Title
        {
            get { return _Title; }
            set
            {
                if (value == _Title) { return; }
                _Title = value;
                this.OnPropertyChanged("Title");
            }
        }

        private int _ActionType;
        public int ActionType
        {
            get { return _ActionType; }
            set
            {
                if (value == _ActionType) { return; }
                _ActionType = value;
                this.OnPropertyChanged("ActionType");
            }
        }

        private object _Param;
        public object Param
        {
            get { return _Param; }
            set
            {
                if (value == _Param) { return; }
                _Param = value;
                this.OnPropertyChanged("Param");
            }
        }

        private int _PrevViewType;
        public int PrevViewType
        {
            get { return _PrevViewType; }
            set
            {
                if (value == _PrevViewType) { return; }
                _PrevViewType = value;
                this.OnPropertyChanged("PrevViewType");
            }
        }

        private object _PrevParam;
        public object PrevParam
        {
            get { return _PrevParam; }
            set
            {
                if (value == _PrevParam) { return; }
                _PrevParam = value;
                this.OnPropertyChanged("PrevParam");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) { handler(this, new PropertyChangedEventArgs(propertyName)); }
        }
    }

}


﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using View.UI.Settings;
using View.ViewModel;

namespace View.BindingModels
{
    public class ChatBgImageModel : INotifyPropertyChanged
    {
        #region Constructor

        public ChatBgImageModel()
        {
            _CurrentInstance = this;
        }

        public ChatBgImageModel(ChatBgImageDTO imagesDTO)
        {
            _CurrentInstance = this;
            LoadData(imagesDTO);
        }

        #endregion Constructor

        #region Property


        private ChatBgImageModel _CurrentInstance;
        public ChatBgImageModel CurrentInstance
        {
            get { return _CurrentInstance; }
        }

        private int _ID;
        public int ID
        {
            get { return _ID; }
            set
            {
                if (value == _ID)
                    return;

                _ID = value;
                this.OnPropertyChanged("ID");
            }
        }

        private string _Name;
        public string Name
        {
            get { return _Name; }
            set
            {
                if (value == _Name)
                    return;

                _Name = value;
                this.OnPropertyChanged("Name");
            }
        }

        private string _ThemeColor;
        public string ThemeColor
        {
            get { return _ThemeColor; }
            set
            {
                if (value == _ThemeColor)
                    return;

                _ThemeColor = value;
                this.OnPropertyChanged("ThemeColor");
            }
        }

        private bool _IsChecked;
        public bool IsChecked
        {
            get { return _IsChecked; }
            set
            {
                if (value == _IsChecked)
                    return;

                _IsChecked = value;
                this.OnPropertyChanged("IsChecked");
            }
        }

        private bool _IsDownloading;
        public bool IsDownloading
        {
            get { return _IsDownloading; }
            set
            {
                if (value == _IsDownloading)
                    return;

                _IsDownloading = value;
                this.OnPropertyChanged("IsDownloading");
            }
        }

        private int _DownloadPercentage;
        public int DownloadPercentage
        {
            get { return _DownloadPercentage; }
            set
            {
                if (value == _DownloadPercentage)
                    return;

                _DownloadPercentage = value;
                this.OnPropertyChanged("DownloadPercentage");
            }
        }

         private bool _IsDownloadCompleted;
         public bool IsDownloadCompleted
        {
            get { return _IsDownloadCompleted; }
            set
            {
                if (value == _IsDownloadCompleted)
                    return;

                _IsDownloadCompleted = value;
                this.OnPropertyChanged("IsDownloadCompleted");
            }
        }

        #endregion Property

        #region Utility Method

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void LoadData(ChatBgImageDTO imagesDTO)
        {
            if (imagesDTO != null)
            {
                this.ID = imagesDTO.id;
                this.Name = imagesDTO.name;
                this.ThemeColor = imagesDTO.themeColor;
            }
        }

        #endregion Utility Method

    }
}

﻿using Models.Constants;
using Models.Entity;
using System.ComponentModel;
using View.Constants;

namespace View.BindingModels
{
    public class LocationModel : BaseBindingModel
    {
        public LocationModel()
        {
            CurrentInstance = this;
        }

        private string _locationName = string.Empty;
        public string LocationName
        {
            get
            {
                return _locationName;
            }
            set
            {
                SetProperty(ref _locationName, value, "LocationName");
            }
        }

        private string _locationId;
        public string LocationId
        {
            get
            {
                return _locationId;
            }
            set
            {
                SetProperty(ref _locationId, value, "LocationId");
            }
        }

        private string _imageUrl;
        public string ImageUrl
        {
            get
            {
                return _imageUrl;
            }
            set
            {
                SetProperty(ref _imageUrl, value, "ImageUrl");
            }
        }

        private double _latitude;
        public double Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                SetProperty(ref _latitude, value, "Latitude");
            }
        }

        private double _longitude;
        public double Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                SetProperty(ref _longitude, value, "Longitude");
            }
        }

        /// <summary>
        /// 0 for location name in location
        /// 1 for image uri in location
        /// </summary>
        private int _type;
        public int Type
        {
            get
            {
                return _type;
            }
            set
            {
                SetProperty(ref _type, value, "Type");
            }
        }

        private LocationModel _currentInstance;
        public LocationModel CurrentInstance
        {
            get
            {
                return _currentInstance;
            }
            set
            {
                SetProperty(ref _currentInstance, value, "CurrentInstance");
            }
        }

        public LocationDTO GetLocationDTOFromModel()
        {
            LocationDTO location = new LocationDTO();
            location.LocationName = this.LocationName;
            location.Latitude = this.Latitude;
            location.Longitude = this.Longitude;
            return location;
        }

        public void LoadData(LocationDTO location, bool MapOff = false)
        {
            if (location != null)
            {
                this.LocationName = location.LocationName;
                this.Latitude = location.Latitude;
                this.Longitude = location.Longitude;
                if ((Latitude != 0 || Longitude != 0) && !MapOff)
                {
                    this.ImageUrl = "http://maps.googleapis.com/maps/api/staticmap?"
                     + "center=" + Latitude + "," + Longitude
                     + "&key=" + SocialMediaConstants.GOOGLE_MAP_API_KEY
                     + "&size=600x170"
                     + "&markers=size:mid%7Ccolor:red%7C" + Latitude + "," + Longitude
                     + "&zoom=15"
                     + "&maptype=roadmap"
                     + "&sensor=false";
                }
                else
                    this.ImageUrl = "";
            }
        }
    }
}

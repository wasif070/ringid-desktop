﻿using Models.Entity;
using Models.Utility;
using System;
using System.ComponentModel;
using System.Windows;
namespace View.BindingModels
{
    public class SingleEducationModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChangedNotify(string methodName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(methodName));
            }
        }
        private string _EInstName;
        public string EInstName
        {
            get
            {
                return _EInstName;
            }
            set
            {
                if (value == _EInstName)
                    return;
                _EInstName = value;
                this.OnPropertyChangedNotify("EInstName");
            }
        }
        /*private SingleEducationModel _CurrentInstance;
        public SingleEducationModel CurrentInstance
        {
            get
            {
                return _CurrentInstance;
            }
            set
            {
                if (value == _CurrentInstance)
                    return;
                _CurrentInstance = value;
                this.OnPropertyChangedNotify("CurrentInstance");
            }
        }*/
        private string _EConcentration;
        public string EConcentration
        {
            get
            {
                return _EConcentration;
            }
            set
            {
                if (value == _EConcentration)
                    return;
                _EConcentration = value;
                this.OnPropertyChangedNotify("EConcentration");
            }
        }
        private string _EDescription;
        public string EDescription
        {
            get
            {
                return _EDescription;
            }
            set
            {
                if (value == _EDescription)
                    return;
                _EDescription = value;
                this.OnPropertyChangedNotify("EDescription");
            }
        }
        private string _EDegree;
        public string EDegree
        {
            get
            {
                return _EDegree;
            }
            set
            {
                if (value == _EDegree)
                    return;
                _EDegree = value;
                this.OnPropertyChangedNotify("EDegree");
            }
        }
        ////
        private long _EFromTime;
        public long EFromTime
        {
            get
            {
                return _EFromTime;
            }
            set
            {
                if (value == _EFromTime)
                    return;
                _EFromTime = value;
                this.OnPropertyChangedNotify("EFromTime");
            }
        }
        private long _EToTime;
        public long EToTime
        {
            get
            {
                return _EToTime;
            }
            set
            {
                if (value == _EToTime)
                    return;
                _EToTime = value;
                this.OnPropertyChangedNotify("EToTime");
            }
        }

        private Guid _EId;
        public Guid EId
        {
            get
            {
                return _EId;
            }
            set
            {
                if (value == _EId)
                    return;
                _EId = value;
                this.OnPropertyChangedNotify("EId");
            }
        }

        ///neE properties
        private string _EFromTime_ViewMode;
        public string EFromTime_ViewMode
        {
            get
            {
                return _EFromTime_ViewMode;
            }
            set
            {
                if (value == _EFromTime_ViewMode)
                    return;
                _EFromTime_ViewMode = value;
                this.OnPropertyChangedNotify("EFromTime_ViewMode");
            }
        }
        private string _EToTime_ViewMode;
        public string EToTime_ViewMode
        {
            get
            {
                return _EToTime_ViewMode;
            }
            set
            {
                if (value == _EToTime_ViewMode)
                    return;
                _EToTime_ViewMode = value;
                this.OnPropertyChangedNotify("EToTime_ViewMode");
            }
        }
        private string _EFromTime_EditMode;
        public string EFromTime_EditMode
        {
            get
            {
                return _EFromTime_EditMode;
            }
            set
            {
                if (value == _EFromTime_EditMode)
                    return;
                _EFromTime_EditMode = value;
                this.OnPropertyChangedNotify("EFromTime_EditMode");
            }
        }
        private string _EToTime_EditMode;
        public string EToTime_EditMode
        {
            get
            {
                return _EToTime_EditMode;
            }
            set
            {
                if (value == _EToTime_EditMode)
                    return;
                _EToTime_EditMode = value;
                this.OnPropertyChangedNotify("EToTime_EditMode");
            }
        }
        /*private Visibility _EVisibilityEditMode;
        public Visibility EVisibilityEditMode
        {
            get
            {
                return _EVisibilityEditMode;
            }
            set
            {
                if (value == _EVisibilityEditMode)
                    return;
                _EVisibilityEditMode = value;
                this.OnPropertyChangedNotify("EVisibilityEditMode");
            }
        }*/

        private Visibility _SettingAboutButtonVisibilty = Visibility.Collapsed;
        public Visibility SettingAboutButtonVisibilty
        {
            get
            {
                return _SettingAboutButtonVisibilty;
            }
            set
            {
                if (value == _SettingAboutButtonVisibilty)
                    return;
                _SettingAboutButtonVisibilty = value;
                this.OnPropertyChangedNotify("SettingAboutButtonVisibilty");
            }
        }
        
        private Visibility _EVisibilityViewMode;
        public Visibility EVisibilityViewMode
        {
            get
            {
                return _EVisibilityViewMode;
            }
            set
            {
                if (value == _EVisibilityViewMode)
                    return;
                _EVisibilityViewMode = value;
                this.OnPropertyChangedNotify("EVisibilityViewMode");
            }
        }
        private bool _IsCurrentlyEChecked;
        public bool IsCurrentlyEChecked
        {
            get
            {
                return _IsCurrentlyEChecked;
            }
            set
            {
                if (value == _IsCurrentlyEChecked)
                    return;
                _IsCurrentlyEChecked = value;
                this.OnPropertyChangedNotify("IsCurrentlyEChecked");
            }
        }
        private Visibility _EVisibilityDate;
        public Visibility EVisibilityDate
        {
            get
            {
                return _EVisibilityDate;
            }
            set
            {
                if (value == _EVisibilityDate)
                    return;
                _EVisibilityDate = value;
                this.OnPropertyChangedNotify("EVisibilityDate");
            }
        }

        /*private string _EErrorString = string.Empty;
        public string EErrorString
        {
            get
            {
                return _EErrorString;
            }
            set
            {
                if (value == _EErrorString)
                    return;
                _EErrorString = value;
                this.OnPropertyChangedNotify("EErrorString");
            }
        }*/
        //EVisibilityDegConcBoth
        private Visibility _EVisibilityDegConcBoth = Visibility.Collapsed;
        public Visibility EVisibilityDegConcBoth
        {
            get
            {
                return _EVisibilityDegConcBoth;
            }
            set
            {
                if (value == _EVisibilityDegConcBoth)
                    return;
                _EVisibilityDegConcBoth = value;
                this.OnPropertyChangedNotify("EVisibilityDegConcBoth");
            }
        }
        private Visibility _EVisibilityGraducated;
        public Visibility EVisibilityGraducated
        {
            get
            {
                return _EVisibilityGraducated;
            }
            set
            {
                if (value == _EVisibilityGraducated)
                    return;
                _EVisibilityGraducated = value;
                this.OnPropertyChangedNotify("EVisibilityGraducated");
            }
        }
        private bool _EIsGraducated;
        public bool EIsGraducated
        {
            get
            {
                return _EIsGraducated;
            }
            set
            {
                if (value == _EIsGraducated)
                    return;
                _EIsGraducated = value;
                this.OnPropertyChangedNotify("EIsGraducated");
            }
        }

        private int _EAttendedForIndex;
        public int EAttendedForIndex
        {
            get
            {
                return _EAttendedForIndex;
            }
            set
            {
                if (value == _EAttendedForIndex)
                    return;
                _EAttendedForIndex = value;
                this.OnPropertyChangedNotify("EAttendedForIndex");
            }
        }
        
        //InstituteName errorString
        private string _EInstituteNameErrorString = string.Empty;
        public string EInstituteNameErrorString
        {
            get
            {
                return _EInstituteNameErrorString;
            }
            set
            {
                if (value == _EInstituteNameErrorString)
                    return;
                _EInstituteNameErrorString = value;
                this.OnPropertyChangedNotify("EInstituteNameErrorString");
            }
        }
        //Degree errorString
        private string _EDegreeErrorString = string.Empty;
        public string EDegreeErrorString
        {
            get
            {
                return _EDegreeErrorString;
            }
            set
            {
                if (value == _EDegreeErrorString)
                    return;
                _EDegreeErrorString = value;
                this.OnPropertyChangedNotify("EDegreeErrorString");
            }
        }
        //Concentration errorString
        private string _EConcentrationErrorString = string.Empty;
        public string EConcentrationErrorString
        {
            get
            {
                return _EConcentrationErrorString;
            }
            set
            {
                if (value == _EConcentrationErrorString)
                    return;
                _EConcentrationErrorString = value;
                this.OnPropertyChangedNotify("EConcentrationErrorString");
            }
        }
        //FromDate errorsTring
        private string _EFromDateErrorString = string.Empty;
        public string EFromDateErrorString
        {
            get
            {
                return _EFromDateErrorString;
            }
            set
            {
                if (value == _EFromDateErrorString)
                    return;
                _EFromDateErrorString = value;
                this.OnPropertyChangedNotify("EFromDateErrorString");
            }
        }
        //ToDate errorString
        private string _EToDateErrorString = string.Empty;
        public string EToDateErrorString
        {
            get
            {
                return _EToDateErrorString;
            }
            set
            {
                if (value == _EToDateErrorString)
                    return;
                _EToDateErrorString = value;
                this.OnPropertyChangedNotify("EToDateErrorString");
            }
        }

        public void LoadData(EducationDTO education)
        {

            this.EId = education.Id;
            this.EInstName = education.SchoolName;
            this.EConcentration = education.Concentration;
            this.EDegree = education.Degree;
            this.EDescription = education.Description;
            this.EFromTime = education.FromTime;
            this.EToTime = education.ToTime;
            if (!string.IsNullOrEmpty(education.Degree) && !string.IsNullOrEmpty(education.Concentration))
            {
                this.EVisibilityDegConcBoth = Visibility.Visible;
            }
            else
            {
                this.EVisibilityDegConcBoth = Visibility.Collapsed;
            }
            //this.EVisibilityEditMode = Visibility.Collapsed;
            this.EVisibilityViewMode = Visibility.Visible;
            //this.EErrorString = "";
            this.EInstituteNameErrorString = string.Empty;
            this.EDegreeErrorString = string.Empty;
            this.EConcentrationErrorString = string.Empty;
            this.EFromDateErrorString = string.Empty;
            this.EToDateErrorString = string.Empty;

            if (education.FromTime == 0)
            {
                this.EVisibilityDate = Visibility.Collapsed;
            }
            else
            {
                this.EVisibilityDate = Visibility.Visible;
                DateTime ft = ModelUtility.DateTimeFromMillisSince1970(education.FromTime);
                this.EFromTime_EditMode = ft.ToShortDateString();
                this.EFromTime_ViewMode = ft.ToString("dd-MMM-yyyy");
                if (education.ToTime == 0)
                {
                    this.IsCurrentlyEChecked = true;
                    this.EToTime_EditMode = DateTime.Now.ToShortDateString();
                    this.EToTime_ViewMode = "Present";
                }
                else
                {
                    this.IsCurrentlyEChecked = false;
                    DateTime tt = ModelUtility.DateTimeFromMillisSince1970(education.ToTime);
                    this.EToTime_EditMode = tt.ToShortDateString();
                    this.EToTime_ViewMode = tt.ToString("dd-MMM-yyyy");
                }
            }
            if (education.Graduated) { this.EIsGraducated = true; this.EVisibilityGraducated = Visibility.Visible; }
            else { this.EIsGraducated = false; this.EVisibilityGraducated = Visibility.Collapsed; }
            this.EAttendedForIndex = education.AttendedFor - 1;
            //CurrentInstance = this;
        }

    }
}

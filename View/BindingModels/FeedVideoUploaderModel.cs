﻿using System;
using System.ComponentModel;
using System.Windows.Media.Imaging;

namespace View.BindingModels
{
    public class FeedVideoUploaderModel : INotifyPropertyChanged
    {
        public FeedVideoUploaderModel()
        {
            CurrentInstance = this;
            System.GC.SuppressFinalize(this);
        }
        public Guid ContentId;
        private string _FilePath = string.Empty;
        public string FilePath
        {
            get { return _FilePath; }
            set
            {
                if (value == _FilePath)
                    return;

                _FilePath = value;
                this.OnPropertyChanged("FilePath");
            }
        }
        private long _FileSize;
        public long FileSize
        {
            get { return _FileSize; }
            set
            {
                if (value == _FileSize)
                    return;

                _FileSize = value;
                this.OnPropertyChanged("FileSize");
            }
        }
        private string _VideoTitle = null;
        public string VideoTitle
        {
            get { return _VideoTitle; }
            set
            {
                if (value == _VideoTitle)
                    return;

                _VideoTitle = value;
                this.OnPropertyChanged("VideoTitle");
            }
        }
        private int _Pecentage;
        public int Pecentage
        {
            get { return _Pecentage; }
            set
            {
                if (value == _Pecentage)
                    return;

                _Pecentage = value;
                this.OnPropertyChanged("Pecentage");
            }
        }
        private string _VideoArtist = null;
        public string VideoArtist
        {
            get { return _VideoArtist; }
            set
            {
                if (value == _VideoArtist)
                    return;

                _VideoArtist = value;
                this.OnPropertyChanged("VideoArtist");
            }
        }
        private long _VideoDuration;
        public long VideoDuration
        {
            get { return _VideoDuration; }
            set
            {
                if (value == _VideoDuration)
                    return;

                _VideoDuration = value;
                this.OnPropertyChanged("VideoDuration");
            }
        }
        private string _StreamUrl = null;
        public string StreamUrl
        {
            get { return _StreamUrl; }
            set
            {
                if (value == _StreamUrl)
                    return;
                _StreamUrl = value;
                this.OnPropertyChanged("StreamUrl");
            }
        }
        private string _ThumbUrl = null;
        public string ThumbUrl
        {
            get { return _ThumbUrl; }
            set
            {
                if (value == _ThumbUrl)
                    return;
                _ThumbUrl = value;
                this.OnPropertyChanged("ThumbUrl");
            }
        }

        //private int _ThumbImageWidth;
        //public int ThumbImageWidth
        //{
        //    get { return _ThumbImageWidth; }
        //    set
        //    {
        //        if (value == _ThumbImageWidth)
        //            return;

        //        _ThumbImageWidth = value;
        //        this.OnPropertyChanged("ThumbImageWidth");
        //    }
        //}
        //private int _ThumbImageHeight;
        //public int ThumbImageHeight
        //{
        //    get { return _ThumbImageHeight; }
        //    set
        //    {
        //        if (value == _ThumbImageHeight)
        //            return;

        //        _ThumbImageHeight = value;
        //        this.OnPropertyChanged("ThumbImageHeight");
        //    }
        //}
        private bool _IsRecorded = false;
        public bool IsRecorded { get { return _IsRecorded; } set { _IsRecorded = value; this.OnPropertyChanged("IsRecorded"); } }

        private bool _TitleEnabled = true;
        public bool TitleEnabled
        {
            get { return _TitleEnabled; }
            set
            {
                if (value == _TitleEnabled)
                    return;

                _TitleEnabled = value;
                this.OnPropertyChanged("TitleEnabled");
            }
        }
        private bool _IsUploadedInVideoServer = false;
        public bool IsUploadedInVideoServer
        {
            get { return _IsUploadedInVideoServer; }
            set
            {
                if (value == _IsUploadedInVideoServer)
                    return;

                _IsUploadedInVideoServer = value;
                this.OnPropertyChanged("IsUploadedInVideoServer");
            }
        }
        private bool _IsUploading = false;
        public bool IsUploading
        {
            get { return _IsUploading; }
            set
            {
                if (value == _IsUploading)
                    return;

                _IsUploading = value;
                this.OnPropertyChanged("IsUploading");
            }
        }
        private BitmapImage _VideoThumbnail = null;
        public BitmapImage VideoThumbnail
        {
            get { return _VideoThumbnail; }
            set { _VideoThumbnail = value; this.OnPropertyChanged("VideoThumbnail"); }
        }

        private int _ThumbImageWidth;
        public int ThumbImageWidth
        {
            get { return _ThumbImageWidth; }
            set
            {
                if (value == _ThumbImageWidth)
                    return;

                _ThumbImageWidth = value;
                this.OnPropertyChanged("ThumbImageWidth");
            }
        }

        private int _ThumbImageHeight;
        public int ThumbImageHeight
        {
            get { return _ThumbImageHeight; }
            set
            {
                if (value == _ThumbImageHeight)
                    return;

                _ThumbImageHeight = value;
                this.OnPropertyChanged("ThumbImageHeight");
            }
        }

        private FeedVideoUploaderModel _CurrentInstance;
        public FeedVideoUploaderModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                if (value == _CurrentInstance)
                    return;
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }
        #region Utility Method
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion Utility Method
    }
}

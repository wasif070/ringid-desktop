﻿using System.ComponentModel;
using System.Windows;
using View.Constants;

namespace View.BindingModels
{
    public class CountryCodeModel : INotifyPropertyChanged
    {
        public CountryCodeModel(string cn, string cd)
        {
            CountryName = cn;
            CountryCode = cd;
            FlagUri = ImageLocation.BASE_COUNTRY + cn + ".png";
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private string _CountryName;
        public string CountryName
        {
            get { return _CountryName; }
            set
            {
                if (value == _CountryName)
                    return;
                _CountryName = value;
                this.OnPropertyChanged("CountryName");
            }
        }

        private string _CountryCode;
        public string CountryCode
        {
            get { return _CountryCode; }
            set
            {
                if (value == _CountryCode)
                    return;
                _CountryCode = value;
                this.OnPropertyChanged("CountryCode");
            }
        }

        private string _FlagUri;
        public string FlagUri
        {
            get { return _FlagUri; }
            set
            {
                if (value == _FlagUri)
                    return;
                _FlagUri = value;
                this.OnPropertyChanged("FlagUri");
            }
        }

        private bool _TempSelected = false;
        public bool TempSelected
        {
            get { return _TempSelected; }
            set
            {
                if (value == _TempSelected)
                    return;
                _TempSelected = value;
                this.OnPropertyChanged("TempSelected");
            }
        }

        private Visibility _VisibilityInCountrySearch = Visibility.Visible;
        public Visibility VisibilityInCountrySearch
        {
            get { return _VisibilityInCountrySearch; }
            set
            {
                if (value == _VisibilityInCountrySearch)
                    return;
                _VisibilityInCountrySearch = value;
                this.OnPropertyChanged("VisibilityInCountrySearch");
            }
        }
    }
}

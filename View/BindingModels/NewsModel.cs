﻿using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System;
using System.ComponentModel;

namespace View.BindingModels
{
    public class NewsModel : INotifyPropertyChanged
    {
        public void LoadData(JObject jObject)
        {
            if (jObject[JsonKeys.NewsPortalCategoryId] != null)
            {
                this.NewsCategoryId = (Guid)jObject[JsonKeys.NewsPortalCategoryId];
            }
            if (jObject[JsonKeys.NewsCategoryId] != null)
            {
                this.NewsCategoryId = (Guid)jObject[JsonKeys.NewsCategoryId];
            }
            if (jObject[JsonKeys.NewsShortDescription] != null)
            {
                this.NewsShortDescription = (string)jObject[JsonKeys.NewsShortDescription];
            }
            if (jObject[JsonKeys.NewsDescription] != null)
            {
                this.NewsDescription = (string)jObject[JsonKeys.NewsDescription];
            }
            if (jObject[JsonKeys.Id] != null)
            {
                this.NewsId = (Guid)jObject[JsonKeys.Id];
            }
            if (jObject[JsonKeys.NewsPortalTitle] != null)
            {
                this.NewsTitle = (string)jObject[JsonKeys.NewsPortalTitle];
            }
            if (jObject[JsonKeys.NewsPortalUrl] != null)
            {
                this.NewsUrl = (string)jObject[JsonKeys.NewsPortalUrl];
            }
            //if (jObject[JsonKeys.NewsPortalFeedtype] != null)
            //{
            //    this.NewsPortalFeedtype = (int)jObject[JsonKeys.NewsPortalFeedtype];
            //}
            if (jObject[JsonKeys.NewsPortalExUrlOp] != null)
            {
                this.ExUrlOp = (bool)jObject[JsonKeys.NewsPortalExUrlOp];
            }
            if (jObject[JsonKeys.PageInfo] != null)
            {
                JObject pageInfo = (JObject)jObject[JsonKeys.PageInfo];
                if (pageInfo[JsonKeys.PageId] != null)
                {
                    this.PId = (long)pageInfo[JsonKeys.PageId];
                }
                if (pageInfo[JsonKeys.FullName] != null)
                {
                    this.PName = (string)pageInfo[JsonKeys.FullName];
                }
                if (pageInfo[JsonKeys.SubscriberCount] != null)
                {
                    this.SCount = (int)pageInfo[JsonKeys.SubscriberCount];
                }
            }
        }
        //public void LoadData(NewsDTO dto)
        //{
        //    this.NewsUrl = dto.NewsUrl;
        //    this.NewsId = dto.NewsId;
        //    this.NewsCategoryId = dto.NewsCategoryId;
        //    this.ExUrlOp = dto.ExUrlOp;
        //    this.NewsTitle = dto.NewsTitle;
        //    this.NewsShortDescription = dto.NewsShortDescription;
        //    this.NewsDescription = dto.NewsDescription;
        //    this.NewsPortalFeedtype = dto.NewsPortalFeedtype;
        //    //this.IsSaved = dto.IsSaved;
        //}

        private string _NewsUrl;
        public string NewsUrl
        {
            get { return _NewsUrl; }
            set
            {
                if (value == _NewsUrl)
                    return;

                _NewsUrl = value;
                this.OnPropertyChanged("NewsUrl");
            }
        }

        private string _NewsShortDescription;
        public string NewsShortDescription
        {
            get { return _NewsShortDescription; }
            set
            {
                if (value == _NewsShortDescription)
                    return;

                _NewsShortDescription = value;
                this.OnPropertyChanged("NewsShortDescription");
            }
        }

        private string _NewsDescription;
        public string NewsDescription
        {
            get { return _NewsDescription; }
            set
            {
                if (value == _NewsDescription)
                    return;

                _NewsDescription = value;
                this.OnPropertyChanged("NewsDescription");
            }
        }

        private string _NewsTitle;
        public string NewsTitle
        {
            get { return _NewsTitle; }
            set
            {
                if (value == _NewsTitle)
                    return;

                _NewsTitle = value;
                this.OnPropertyChanged("NewsTitle");
            }
        }
        private int _NewsPortalFeedtype = 2;//text
        public int NewsPortalFeedtype
        {
            get { return _NewsPortalFeedtype; }
            set
            {
                if (value == _NewsPortalFeedtype)
                    return;

                _NewsPortalFeedtype = value;
                this.OnPropertyChanged("NewsPortalFeedtype");
            }
        }
        private bool _ExUrlOp;
        public bool ExUrlOp
        {
            get { return _ExUrlOp; }
            set
            {
                if (value == _ExUrlOp)
                    return;

                _ExUrlOp = value;
                this.OnPropertyChanged("ExUrlOp");
            }
        }
        //private bool _IsSaved;
        //public bool IsSaved
        //{
        //    get { return _IsSaved; }
        //    set
        //    {
        //        if (value == _IsSaved)
        //            return;

        //        _IsSaved = value;
        //        this.OnPropertyChanged("IsSaved");
        //    }
        //}
        private Guid _NewsCategoryId;
        public Guid NewsCategoryId
        {
            get { return _NewsCategoryId; }
            set
            {
                if (value == _NewsCategoryId)
                    return;

                _NewsCategoryId = value;
                this.OnPropertyChanged("NewsCategoryId");
            }
        }

        private Guid _NewsId;
        public Guid NewsId
        {
            get { return _NewsId; }
            set
            {
                if (value == _NewsId)
                    return;
                _NewsId = value;
                this.OnPropertyChanged("NewsId");
            }
        }
        private long _PId;
        public long PId
        {
            get { return _PId; }
            set
            {
                if (value == _PId)
                    return;
                _PId = value;
                this.OnPropertyChanged("PId");
            }
        }
        private string _PName;
        public string PName
        {
            get { return _PName; }
            set
            {
                if (value == _PName)
                    return;
                _PName = value;
                this.OnPropertyChanged("PName");
            }
        }
        private int _SCount;
        public int SCount
        {
            get { return _SCount; }
            set
            {
                if (value == _SCount)
                    return;
                _SCount = value;
                this.OnPropertyChanged("SCount");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

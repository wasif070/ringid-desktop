﻿using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System;
using System.ComponentModel;

namespace View.BindingModels
{
    public class MusicPageModel : BaseUserProfileModel
    {
        public MusicPageModel()
        {
            CurrentInstance = this;
        }
        public void LoadData(JObject jObject)
        {
            if (jObject[JsonKeys.FullName] != null)
            {
                this.FullName = (string)jObject[JsonKeys.FullName];
            }
            if (jObject[JsonKeys.ProfileImage] != null)
            {
                this.ProfileImage = (string)jObject[JsonKeys.ProfileImage];
            }
            if (jObject[JsonKeys.CoverImage] != null)
            {
                this.CoverImage = (string)jObject[JsonKeys.CoverImage];
            }
            if (UserTableID == 0 && jObject[JsonKeys.UserTableID] != null)
            {
                this.UserTableID = (long)jObject[JsonKeys.UserTableID];
            }
            if (UserTableID == 0 && jObject[JsonKeys.PageId] != null)
            {
                this.UserTableID = (long)jObject[JsonKeys.PageId];
            }
            if (jObject[JsonKeys.UserIdentity] != null)
            {
                this.UserIdentity = (long)jObject[JsonKeys.UserIdentity];
            }
            if (jObject[JsonKeys.IsUserFeedHidden] != null)
            {
                this.IsProfileHidden = (bool)jObject[JsonKeys.IsUserFeedHidden];
            }
            if (jObject[JsonKeys.UserIdentity] != null)
            {
                this.UserIdentity = (long)jObject[JsonKeys.UserIdentity];
            }
            if (jObject[JsonKeys.NewsPortalSlogan] != null)
            {
                this.PageSlogan = (string)jObject[JsonKeys.NewsPortalSlogan];
            }
            if (jObject[JsonKeys.PageId] != null)
            {
                this.MusicPageId = (long)jObject[JsonKeys.PageId];
            }
            if (jObject[JsonKeys.Subscribe] != null)
            {
                this.IsSubscribed = (bool)jObject[JsonKeys.Subscribe];
            }
            if (jObject[JsonKeys.SubscriberCount] != null)
            {
                this.SubscriberCount = (int)jObject[JsonKeys.SubscriberCount];
            }
            if (jObject[JsonKeys.NewsPortalCatName] != null)
            {
                this.PageCatName = (string)jObject[JsonKeys.NewsPortalCatName];
            }
            if (jObject[JsonKeys.NewsPortalCatId] != null)
            {
                this.PageCatId = (Guid)jObject[JsonKeys.NewsPortalCatId];
            }
            if (jObject[JsonKeys.NewsPortalDTO] != null)
            {
                JObject NpDto = (JObject)jObject[JsonKeys.NewsPortalDTO];
                if (NpDto[JsonKeys.NewsPortalSlogan] != null)
                {
                    this.PageSlogan = (string)NpDto[JsonKeys.NewsPortalSlogan];
                }
                if (NpDto[JsonKeys.PageId] != null)
                {
                    this.MusicPageId = (long)NpDto[JsonKeys.PageId];
                }
                if (NpDto[JsonKeys.Subscribe] != null)
                {
                    this.IsSubscribed = (bool)NpDto[JsonKeys.Subscribe];
                }
                if (NpDto[JsonKeys.NewsPortalCatName] != null)
                {
                    this.PageCatName = (string)NpDto[JsonKeys.NewsPortalCatName];
                }
                if (NpDto[JsonKeys.NewsPortalCatId] != null)
                {
                    this.PageCatId = (Guid)NpDto[JsonKeys.NewsPortalCatId];
                }
                if (NpDto[JsonKeys.SubscriberCount] != null)
                {
                    this.SubscriberCount = (int)NpDto[JsonKeys.SubscriberCount];
                }
                if (UserTableID == 0 && NpDto[JsonKeys.UserTableID] != null)
                {
                    this.UserTableID = (long)NpDto[JsonKeys.UserTableID];
                }
            }
        }

        private long _MusicPageId;
        public long MusicPageId
        {
            get { return _MusicPageId; }
            set
            {
                if (value == _MusicPageId)
                    return;
                _MusicPageId = value;
                this.OnPropertyChanged("MusicPageId");
            }
        }

        private bool _IsSubscribed;
        public bool IsSubscribed
        {
            get { return _IsSubscribed; }
            set
            {
                //if (value == _IsSubscribed)
                //    return;
                _IsSubscribed = value;
                this.OnPropertyChanged("IsSubscribed");
            }
        }

        private int _SubscriberCount;
        public int SubscriberCount
        {
            get { return _SubscriberCount; }
            set
            {
                if (value == _SubscriberCount)
                    return;

                _SubscriberCount = value;
                this.OnPropertyChanged("SubscriberCount");
            }
        }

        private string _CoverImage;
        public string CoverImage
        {
            get { return _CoverImage; }
            set
            {
                if (value == _CoverImage)
                    return;

                _CoverImage = value;
                this.OnPropertyChanged("CoverImage");
            }
        }

        private string _PageSlogan;
        public string PageSlogan
        {
            get { return _PageSlogan; }
            set
            {
                if (value == _PageSlogan)
                    return;
                _PageSlogan = value;
                this.OnPropertyChanged("PageSlogan");
            }
        }

        private Guid _PageCatId;
        public Guid PageCatId
        {
            get { return _PageCatId; }
            set
            {
                if (value == _PageCatId)
                    return;

                _PageCatId = value;
                this.OnPropertyChanged("PageCatId");
            }
        }

        private string _PageCatName;
        public string PageCatName
        {
            get { return _PageCatName; }
            set
            {
                if (value == _PageCatName)
                    return;
                _PageCatName = value;
                this.OnPropertyChanged("PageCatName");
            }
        }

        private bool _IsRightMarginOff;
        public bool IsRightMarginOff
        {
            get { return _IsRightMarginOff; }
            set
            {
                if (value == _IsRightMarginOff)
                    return;

                _IsRightMarginOff = value;
                this.OnPropertyChanged("IsRightMarginOff");
            }
        }

        private bool _IsContextMenuOpened = false;
        public bool IsContextMenuOpened
        {
            get { return _IsContextMenuOpened; }
            set
            {
                System.Diagnostics.Debug.WriteLine("ContextMenu ....=>");
                if (value == _IsContextMenuOpened) return;
                _IsContextMenuOpened = value;
                this.OnPropertyChanged("IsContextMenuOpened");
            }
        }
    }
}

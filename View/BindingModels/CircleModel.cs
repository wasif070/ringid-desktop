﻿using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;

namespace View.BindingModels
{
    public class CircleModel : BaseUserProfileModel
    {
        public void LoadData(JObject jObject)
        {
            CurrentInstance = this;
            if (jObject[JsonKeys.GroupName] != null)
                this.FullName = (string)jObject[JsonKeys.GroupName];
            if (jObject[JsonKeys.GroupId] != null)
                this.UserTableID = (long)jObject[JsonKeys.GroupId];
            if (jObject[JsonKeys.SuperAdmin] != null)
            {
                this.SuperAdmin = (long)jObject[JsonKeys.SuperAdmin];
                if (this.SuperAdmin == DefaultSettings.LOGIN_RING_ID)
                {
                    this.VisibilityLeaveCircle = Visibility.Collapsed;
                    this.VisibilityDeleteCircle = Visibility.Visible;
                }
                else
                {
                    this.VisibilityLeaveCircle = Visibility.Visible;
                    this.VisibilityDeleteCircle = Visibility.Collapsed;
                }
            }
            if (jObject[JsonKeys.UpdateTime] != null)
                this.UpdateTime = (long)jObject[JsonKeys.UpdateTime];
            if (jObject[JsonKeys.UserTableID] != null)
                this.UserTableID = (long)jObject[JsonKeys.UserTableID];
            if (jObject[JsonKeys.IntegerStatus] != null)
                this.IntegerStatus = (short)jObject[JsonKeys.IntegerStatus];
            if (jObject[JsonKeys.MemberOrMediaCount] != null)
                this.MemberCount = (int)jObject[JsonKeys.MemberOrMediaCount];
            if (jObject[JsonKeys.MembershipType] != null)
                this.MembershipStatus = (int)jObject[JsonKeys.MembershipType];
            if (jObject[JsonKeys.AdminCount] != null)
                this.AdminCount = (int)jObject[JsonKeys.AdminCount];
            if (jObject[JsonKeys.IsHidden] != null)
                this.IsProfileHidden = (bool)jObject[JsonKeys.IsHidden];
        }
        public void LoadData(CircleDTO circle)
        {
            UserTableID = circle.CircleId;
            FullName = circle.CircleName;
            SuperAdmin = circle.SuperAdmin;
            UpdateTime = circle.UpdateTime;
            //UserTableID = circle.UserTableID;
            IntegerStatus = circle.IntegerStatus;
            MemberCount = circle.MemberCount;
            AdminCount = circle.AdminCount;
            MembershipStatus = circle.MembershipStatus;
            IsProfileHidden = circle.IsProfileHidden;
            if (circle.SuperAdmin == DefaultSettings.LOGIN_RING_ID)
            {
                VisibilityLeaveCircle = Visibility.Collapsed;
                VisibilityDeleteCircle = Visibility.Visible;
            }
            else
            {
                VisibilityLeaveCircle = Visibility.Visible;
                VisibilityDeleteCircle = Visibility.Collapsed;
            }

            CurrentInstance = this;
        }

        public CircleDTO GetCircleDTO(CircleModel circleModel)
        {
            CircleDTO circleDTO = new CircleDTO();
            circleDTO.CircleName = circleModel.FullName;
            circleDTO.CircleId = circleModel.UserTableID;
            circleDTO.SuperAdmin = circleModel.SuperAdmin;
            circleDTO.UpdateTime = circleModel.UpdateTime;
            circleDTO.UserTableID = circleModel.UserTableID;
            circleDTO.IntegerStatus = circleModel.IntegerStatus;
            circleDTO.MemberCount = circleModel.MemberCount;
            circleDTO.AdminCount = circleModel.AdminCount;
            circleDTO.MembershipStatus = circleModel.MembershipStatus;
            return circleDTO;
        }

        //private long _CircleId;
        //public long CircleId
        //{
        //    get { return _CircleId; }
        //    set
        //    {
        //        if (value == _CircleId)
        //            return;
        //        _CircleId = value;
        //        this.OnPropertyChanged("CircleId");
        //    }
        //}
        //private string _CircleName;
        //public string CircleName
        //{
        //    get { return _CircleName; }
        //    set
        //    {
        //        if (value == _CircleName)
        //            return;
        //        _CircleName = value;
        //        this.OnPropertyChanged("CircleName");
        //    }
        //}
        //private bool _IsProfileHidden;
        //public bool IsProfileHidden
        //{
        //    get { return _IsProfileHidden; }
        //    set
        //    {
        //        if (value == _IsProfileHidden)
        //            return;

        //        _IsProfileHidden = value;
        //        this.OnPropertyChanged("IsProfileHidden");
        //    }
        //}
        private long _SuperAdmin;
        public long SuperAdmin
        {
            get { return _SuperAdmin; }
            set
            {
                if (value == _SuperAdmin)
                    return;
                _SuperAdmin = value;
                this.OnPropertyChanged("SuperAdmin");
            }
        }
        private long _UpdateTime;
        public long UpdateTime
        {
            get { return _UpdateTime; }
            set
            {
                if (value == _UpdateTime)
                    return;

                _UpdateTime = value;
                this.OnPropertyChanged("UpdateTime");
            }
        }
        //private long _UserTableId;
        //public long UserTableID
        //{
        //    get { return _UserTableId; }
        //    set
        //    {
        //        if (value == _UserTableId)
        //            return;

        //        _UserTableId = value;
        //        this.OnPropertyChanged("UserTableID");
        //    }
        //}
        private short _IntegerStatus;
        public short IntegerStatus
        {
            get { return _IntegerStatus; }
            set
            {
                if (value == _IntegerStatus)
                    return;
                _IntegerStatus = value;
                this.OnPropertyChanged("IntegerStatus");
            }
        }
        private int _MemberCount;
        public int MemberCount
        {
            get { return _MemberCount; }
            set
            {
                if (value == _MemberCount)
                    return;
                _MemberCount = value;
                this.OnPropertyChanged("MemberCount");
            }
        }

        private int _MembershipStatus;
        public int MembershipStatus
        {
            get { return _MembershipStatus; }
            set
            {
                if (value == _MembershipStatus)
                    return;
                _MembershipStatus = value;
                this.OnPropertyChanged("MembershipStatus");
            }
        }

        private bool _SingleCirclePanelFocusable = false;
        public bool SingleCirclePanelFocusable
        {
            get
            {
                return _SingleCirclePanelFocusable;
            }

            set
            {
                _SingleCirclePanelFocusable = value;
                OnPropertyChanged("SingleCirclePanelFocusable");
            }
        }
        private Visibility _VisibilityLeaveCircle;
        public Visibility VisibilityLeaveCircle
        {
            get { return _VisibilityLeaveCircle; }
            set
            {
                if (value == _VisibilityLeaveCircle)
                    return;

                _VisibilityLeaveCircle = value;
                this.OnPropertyChanged("VisibilityLeaveCircle");
            }
        }
        private Visibility _VisibilityDeleteCircle;
        public Visibility VisibilityDeleteCircle
        {
            get { return _VisibilityDeleteCircle; }
            set
            {
                if (value == _VisibilityDeleteCircle)
                    return;

                _VisibilityDeleteCircle = value;
                this.OnPropertyChanged("VisibilityDeleteCircle");
            }
        }
        private CircleModel _CurrentInstance;
        public CircleModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                if (value == _CurrentInstance)
                    return;
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }

        private int _AdminCount;

        public int AdminCount
        {
            get { return _AdminCount; }
            set { _AdminCount = value; this.OnPropertyChanged("AdminCount"); }
        }


        //public event PropertyChangedEventHandler PropertyChanged;
        //public void OnPropertyChanged(string propertyName)
        //{
        //    PropertyChangedEventHandler handler = PropertyChanged;
        //    if (handler != null)
        //    {
        //        handler(this, new PropertyChangedEventArgs(propertyName));
        //    }
        //}
    }
}

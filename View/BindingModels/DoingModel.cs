﻿using Models.Constants;
using Models.Entity;
using System.ComponentModel;
using System.Windows;

namespace View.BindingModels
{
    public class DoingModel : INotifyPropertyChanged
    {
        public void LoadData(DoingDTO doing)
        {
            UpdateTime = doing.UpdateTime;
            ID = doing.ID;
            ImgUrl = doing.ImgUrl;
            Name = doing.Name; //"Feeling " + 
            Category = doing.Category;
            SortId = doing.SortId;
            CurrentInstance = this;
        }
        private string _ImgUrl;
        public string ImgUrl
        {
            get { return _ImgUrl; }
            set
            {
                if (value == _ImgUrl)
                    return;
                _ImgUrl = value;
                this.OnPropertyChanged("ImgUrl");
            }
        }
        private string _Name;
        public string Name
        {
            get { return _Name; }
            set
            {
                if (value == _Name)
                    return;
                _Name = value;
                this.OnPropertyChanged("Name");
            }
        }
        private long _UpdateTime;
        public long UpdateTime
        {
            get { return _UpdateTime; }
            set
            {
                if (value == _UpdateTime)
                    return;

                _UpdateTime = value;
                this.OnPropertyChanged("UpdateTime");
            }
        }

        private int _Category;
        public int Category
        {
            get { return _Category; }
            set
            {
                if (value == _Category)
                    return;

                _Category = value;
                this.OnPropertyChanged("Category");
            }
        }
        private int _SortId;
        public int SortId
        {
            get { return _SortId; }
            set
            {
                if (value == _SortId)
                    return;

                _SortId = value;
                this.OnPropertyChanged("SortId");
            }
        }
        private Visibility _VisibilityInSearch = Visibility.Visible;
        public Visibility VisibilityInSearch
        {
            get { return _VisibilityInSearch; }
            set
            {
                if (value == _VisibilityInSearch)
                    return;
                _VisibilityInSearch = value;
                this.OnPropertyChanged("VisibilityInSearch");
            }
        }
        private long _ID;
        public long ID
        {
            get { return _ID; }
            set
            {
                if (value == _ID)
                    return;

                _ID = value;
                this.OnPropertyChanged("ID");
            }
        }
        private DoingModel _CurrentInstance;
        public DoingModel CurrentInstance
        {
            get { return _CurrentInstance; }
            set
            {
                if (value == _CurrentInstance)
                    return;
                _CurrentInstance = value;
                this.OnPropertyChanged("CurrentInstance");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}

<<<<<<< HEAD
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace View.BindingModels
{
    public class UploadingModel : INotifyPropertyChanged
    {
        private string _UploadingContent;
        public string UploadingContent
        {
            get { return _UploadingContent; }
            set
            {
                if (value == _UploadingContent)
                    return;
                _UploadingContent = value;
                this.OnPropertyChanged("UploadingContent");
            }
        }

        private int _TotalUploadedInPercentage;
        public int TotalUploadedInPercentage
        {
            get { return _TotalUploadedInPercentage; }
            set
            {
                if (value == _TotalUploadedInPercentage)
                    return;
                _TotalUploadedInPercentage = value;
                this.OnPropertyChanged("TotalUploadedInPercentage");
            }
        }

        private bool _IsUploadFailed;
        public bool IsUploadFailed
        {
            get { return _IsUploadFailed; }
            set
            {
                if (value == _IsUploadFailed)
                    return;
                _IsUploadFailed = value;
                this.OnPropertyChanged("IsUploadFailed");
            }
        }

        private bool _IsDownload;
        public bool IsDownload
        {
            get { return _IsDownload; }
            set
            {
                if (value == _IsDownload)
                    return;
                _IsDownload = value;
                this.OnPropertyChanged("IsDownload");
            }
        }

        private Guid _ContentId;
        public Guid ContentId
        {
            get { return _ContentId; }
            set
            {
                if (value == _ContentId)
                    return;

                _ContentId = value;
                this.OnPropertyChanged("ContentId");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
=======
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace View.BindingModels
{
    public class UploadingModel : BaseBindingModel
    {
        private string _uploadingContent;
        public string UploadingContent
        {
            get
            {
                return _uploadingContent;
            }
            set
            {
                SetProperty(ref _uploadingContent, value, "UploadingContent");
            }
        }

        private int _totalUploadedInPercentage;
        public int TotalUploadedInPercentage
        {
            get
            {
                return _totalUploadedInPercentage;
            }
            set
            {
                SetProperty(ref _totalUploadedInPercentage, value, "TotalUploadedInPercentage");
            }
        }

        private bool _isUploadFailed;
        public bool IsUploadFailed
        {
            get
            {
                return _isUploadFailed;
            }
            set
            {
                SetProperty(ref _isUploadFailed, value, "IsUploadFailed");
            }
        }

        private bool _isDownload;
        public bool IsDownload
        {
            get
            {
                return _isDownload;
            }
            set
            {
                SetProperty(ref _isDownload, value, "IsDownload");
            }
        }

        private Guid _contentId;
        public Guid ContentId
        {
            get
            {
                return _contentId;
            }
            set
            {
                SetProperty(ref _contentId, value, "ContentId");
            }
        }
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

﻿using Models.Constants;
using Models.Entity;
using Models.Utility;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Data.SQLite;
using Models.Stores;
using System.Threading;
using log4net;
using Models.Utility.Chat;
using Newtonsoft.Json;

namespace Models.DAO
{
    public class RecentChatCallActivityDAO
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(RecentChatCallActivityDAO).Name);

        public static List<RecentDTO> LoadRecentChatCallActivityListByDays(long? groupId, long? friendId, int day, int? limit, List<string> ignoreUniqueIDList = null, bool isAscending = false)
        {
            long time = ModelUtility.GetStartTimeMillisLocal(DateTime.Now) - ((int)day * SettingsConstants.MILISECONDS_IN_DAY);
            return LoadRecentChatCallActivityListByDays(groupId, friendId, time, limit, ignoreUniqueIDList, isAscending);
        }

        public static List<RecentDTO> LoadRecentChatCallActivityListByDays(long? groupId, long? friendId, long time, int? limit, List<string> ignoreUniqueIDList = null, bool isAscending = false)
        {
            List<RecentDTO> recentList = new List<RecentDTO>();
            string query = String.Empty;
            string ignoreUniqueIDs = String.Empty;

            if (limit != null && limit <= 0)
            {
                return recentList;
            }

            if (ignoreUniqueIDList != null && ignoreUniqueIDList.Count > 0)
            {
                ignoreUniqueIDs = String.Join<string>(",", ignoreUniqueIDList.Select(P => "'" + P + "'").ToList());
            }

            try
            {
                CreateDBandTables CreateDB = new CreateDBandTables();
                CreateDB.ConnectToDatabase();

                if (friendId != null && friendId > 0)
                {
                    query = "SELECT * FROM "
                            + "    ( "
                            + "        SELECT "
                            + "            " + DBConstants.FRIEND_TABLE_ID + ", "
                            + "            " + DBConstants.SENDER_TABLE_ID + ", "
                            + "            " + DBConstants.PACKET_TYPE + ", "
                            + "            " + DBConstants.MESSAGE_TYPE + ", "
                            + "            " + DBConstants.MESSAGE + ", "
                            + "            " + DBConstants.MESSAGE_DATE + ", "
                            + "            " + DBConstants.MESSAGE_DELIEVER_DATE + ", "
                            + "            " + DBConstants.MESSAGE_SEEN_DATE + ", "
                            + "            " + DBConstants.MESSAGE_VIEW_DATE + ", "
                            + "            " + DBConstants.STATUS + ", "
                            + "            " + DBConstants.FILE_ID + ", "
                            + "            " + DBConstants.FILE_STATUS + ", "
                            + "            " + DBConstants.PACKET_ID + ", "
                            + "            " + DBConstants.IS_UNREAD + ", "
                            + "            " + ChatConstants.SUBTYPE_FRIEND_CHAT + " AS " + DBConstants.TABLE_TYPE + ", "
                            + "            " + DBConstants.TIME_OUT + ", "
                            + "            " + DBConstants.IS_SECRET_VISIBLE + ", "
                            + "            0 AS " + DBConstants.CL_CALL_DURATION + " "
                            + "        FROM "
                            + "        " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                            + "        WHERE "
                            + "        " + DBConstants.MESSAGE_DATE + " > " + time + " AND "
                            + "        length(" + DBConstants.PACKET_ID + ") > " + 0 + " AND "
                            + "        " + DBConstants.FRIEND_TABLE_ID + " = " + friendId + " "
                            + "        UNION "
                            + "        SELECT "
                            + "            " + DBConstants.FRIEND_TABLE_ID + ", "
                            + "            '' AS " + DBConstants.SENDER_TABLE_ID + ", "
                            + "            " + DBConstants.CL_CALL_CATEGORY + " AS " + DBConstants.PACKET_TYPE + ", "
                            + "            " + DBConstants.CL_CALL_TYPE + " AS " + DBConstants.MESSAGE_TYPE + ", "
                            + "            " + DBConstants.MESSAGE + ", "
                            + "            " + DBConstants.CL_CALLING_TIME + " AS " + DBConstants.MESSAGE_DATE + ", "
                            + "            0 AS " + DBConstants.MESSAGE_DELIEVER_DATE + ", "
                            + "            0 AS " + DBConstants.MESSAGE_SEEN_DATE + ", "
                            + "            0 AS " + DBConstants.MESSAGE_VIEW_DATE + ", "
                            + "            0 AS " + DBConstants.STATUS + ", "
                            + "            0 AS " + DBConstants.FILE_ID + ", "
                            + "            0 AS " + DBConstants.FILE_STATUS + ", "
                            + "            " + DBConstants.CALL_ID + " AS " + DBConstants.PACKET_ID + ", "
                            + "            " + DBConstants.IS_UNREAD + ", "
                            + "            " + ChatConstants.SUBTYPE_CALL_LOG + " AS " + DBConstants.TABLE_TYPE + ", "
                            + "            0 AS " + DBConstants.TIME_OUT + ", "
                            + "            0 AS " + DBConstants.IS_SECRET_VISIBLE + ", "
                            + "            " + DBConstants.CL_CALL_DURATION + " "
                            + "        FROM "
                            + "        " + DBConstants.TABLE_RING_CALL_LOG + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                            + "        WHERE "
                            + "        " + DBConstants.CL_CALLING_TIME + " > " + time + " AND "
                            + "        " + DBConstants.FRIEND_TABLE_ID + " = " + friendId + " "
                            + "    ) AS vTable "
                            + "    " + (!String.IsNullOrEmpty(ignoreUniqueIDs) ? "WHERE " + DBConstants.PACKET_ID + " NOT IN (" + ignoreUniqueIDs + ") " : " ")
                            + "    ORDER BY vTable." + DBConstants.MESSAGE_DATE + (isAscending ? " ASC" : " DESC ")
                            + "   " + (limit != null ? " LIMIT " + limit.ToString() : "");
                }
                else if (groupId != null && groupId > 0)
                {
                    query = "SELECT * FROM "
                            + "    ( "
                            + "        SELECT "
                            + "            " + DBConstants.GROUP_ID + ", "
                            + "            " + DBConstants.SENDER_TABLE_ID + ", "
                            + "            " + DBConstants.PACKET_TYPE + ", "
                            + "            " + DBConstants.MESSAGE_TYPE + ", "
                            + "            " + DBConstants.MESSAGE + ", "
                            + "            " + DBConstants.MESSAGE_DATE + ", "
                            + "            " + DBConstants.MESSAGE_DELIEVER_DATE + ", "
                            + "            " + DBConstants.MESSAGE_SEEN_DATE + ", "
                            + "            " + DBConstants.MESSAGE_VIEW_DATE + ", "
                            + "            " + DBConstants.STATUS + ", "
                            + "            " + DBConstants.FILE_ID + ", "
                            + "            " + DBConstants.FILE_STATUS + ", "
                            + "            " + DBConstants.PACKET_ID + ", "
                            + "            " + DBConstants.IS_UNREAD + ", "
                            + "            " + ChatConstants.SUBTYPE_GROUP_CHAT + " AS " + DBConstants.TABLE_TYPE + ", "
                            + "            0 AS " + DBConstants.ACTIVITY_TYPE + ", "
                            + "            '' AS " + DBConstants.GROUP_NAME + ", "
                            + "            '' AS " + DBConstants.GROUP_PROFILE_IMAGE + ", "
                            + "            '' AS " + DBConstants.MEMBER_LIST + " "
                            + "        FROM "
                            + "        " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                            + "        WHERE "
                            + "        " + DBConstants.MESSAGE_DATE + " > " + time + " AND "
                            + "        length(" + DBConstants.PACKET_ID + ") > " + 0 + " AND "
                            + "        " + DBConstants.GROUP_ID + " = " + groupId + " "
                            + "        UNION "
                            + "        SELECT "
                            + "            " + DBConstants.GROUP_ID + ", "
                            + "            " + DBConstants.ACTIVITY_BY + " AS " + DBConstants.SENDER_TABLE_ID + ", "
                            + "            0 AS " + DBConstants.PACKET_TYPE + ", "
                            + "            " + DBConstants.MESSAGE_TYPE + ", "
                            + "            '' AS " + DBConstants.MESSAGE + ", "
                            + "            " + DBConstants.UPDATE_TIME + " AS " + DBConstants.MESSAGE_DATE + ", "
                            + "            0 AS " + DBConstants.MESSAGE_DELIEVER_DATE + ", "
                            + "            0 AS " + DBConstants.MESSAGE_SEEN_DATE + ", "
                            + "            0 AS " + DBConstants.MESSAGE_VIEW_DATE + ", "
                            + "            0 AS " + DBConstants.STATUS + ", "
                            + "            0 AS " + DBConstants.FILE_ID + ", "
                            + "            0 AS " + DBConstants.FILE_STATUS + ", "
                            + "            " + DBConstants.PACKET_ID + ", "
                            + "            " + DBConstants.IS_UNREAD + ", "
                            + "            " + ChatConstants.SUBTYPE_GROUP_ACTIVITY + " AS " + DBConstants.TABLE_TYPE + ", "
                            + "            " + DBConstants.ACTIVITY_TYPE + ", "
                            + "            " + DBConstants.GROUP_NAME + ", "
                            + "            " + DBConstants.GROUP_PROFILE_IMAGE + ", "
                            + "            " + DBConstants.MEMBER_LIST + " "
                            + "        FROM "
                            + "        " + DBConstants.TABLE_RING_ACTIVITY + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                            + "        WHERE "
                            + "        " + DBConstants.UPDATE_TIME + " > " + time + " AND "
                            + "        " + DBConstants.GROUP_ID + " = " + groupId + " "
                            + "    ) AS vTable "
                            + "    " + (!String.IsNullOrEmpty(ignoreUniqueIDs) ? "WHERE " + DBConstants.PACKET_ID + " NOT IN (" + ignoreUniqueIDs + ") " : " ")
                            + "    ORDER BY vTable." + DBConstants.MESSAGE_DATE + (isAscending ? " ASC" : " DESC ")
                            + "    " + (limit != null ? " LIMIT " + limit.ToString() : "");
                }

                using (SQLiteCommand command = new SQLiteCommand(query, CreateDB.GetConnection()))
                {
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        int tableType = dr.GetInt32(14);
                        switch (tableType)
                        {
                            case ChatConstants.SUBTYPE_FRIEND_CHAT:
                                recentList.Add(MakeFriendChatObject(dr));
                                break;
                            case ChatConstants.SUBTYPE_CALL_LOG:
                                recentList.Add(MakeCallLogObject(dr));
                                break;
                            case ChatConstants.SUBTYPE_GROUP_CHAT:
                                recentList.Add(MakeGroupChatObject(dr));
                                break;
                            case ChatConstants.SUBTYPE_GROUP_ACTIVITY:
                                recentList.Add(MakeGroupActivityObject(dr));
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in LoadRecentChatCallActivityListByDays() ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
            finally
            {

            }

            return recentList;
        }

        public static List<RecentDTO> LoadRecentChatCallActivityListByScroll(long? groupId, long? friendId, int day, int? limit, int scrollType, List<string> ignoreUniqueIDList = null)
        {
            long time = ModelUtility.GetStartTimeMillisLocal(DateTime.Now) - ((int)day * SettingsConstants.MILISECONDS_IN_DAY);
            return LoadRecentChatCallActivityListByScroll(groupId, friendId, time, limit, scrollType, ignoreUniqueIDList);
        }

        public static List<RecentDTO> LoadRecentChatCallActivityListByScroll(long? groupId, long? friendId, long time, int? limit, int scrollType, List<string> ignoreUniqueIDList = null)
        {
            List<RecentDTO> recentList = new List<RecentDTO>();
            string query = String.Empty;
            string ignoreUniqueIDs = String.Empty;

            if (limit != null && limit <= 0)
            {
                return recentList;
            }

            if (ignoreUniqueIDList != null && ignoreUniqueIDList.Count > 0)
            {
                ignoreUniqueIDs = String.Join<string>(",", ignoreUniqueIDList.Select(P => "'" + P + "'").ToList());
            }

            long minTime = DateTime.UtcNow.ChangeTime(0, 0, 0, 0).CurrentTimeMillis() - (ChatConstants.DAY_365_DAYS * SettingsConstants.MILISECONDS_IN_DAY);
            try
            {
                CreateDBandTables CreateDB = new CreateDBandTables();
                CreateDB.ConnectToDatabase();

                if (friendId != null && friendId > 0)
                {
                    query = "SELECT * FROM "
                            + "    ( "
                            + "        SELECT "
                            + "            " + DBConstants.FRIEND_TABLE_ID + ", "
                            + "            " + DBConstants.SENDER_TABLE_ID + ", "
                            + "            " + DBConstants.PACKET_TYPE + ", "
                            + "            " + DBConstants.MESSAGE_TYPE + ", "
                            + "            " + DBConstants.MESSAGE + ", "
                            + "            " + DBConstants.MESSAGE_DATE + ", "
                            + "            " + DBConstants.MESSAGE_DELIEVER_DATE + ", "
                            + "            " + DBConstants.MESSAGE_SEEN_DATE + ", "
                            + "            " + DBConstants.MESSAGE_VIEW_DATE + ", "
                            + "            " + DBConstants.STATUS + ", "
                            + "            " + DBConstants.FILE_ID + ", "
                            + "            " + DBConstants.FILE_STATUS + ", "
                            + "            " + DBConstants.PACKET_ID + ", "
                            + "            " + DBConstants.IS_UNREAD + ", "
                            + "            " + ChatConstants.SUBTYPE_FRIEND_CHAT + " AS " + DBConstants.TABLE_TYPE + ", "
                            + "            " + DBConstants.TIME_OUT + ", "
                            + "            " + DBConstants.IS_SECRET_VISIBLE + ", "
                            + "            0 AS " + DBConstants.CL_CALL_DURATION + " "
                            + "        FROM "
                            + "        " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                            + "        WHERE "
                            + "        " + DBConstants.MESSAGE_DATE + " >= " + minTime + " AND "
                            + "        length(" + DBConstants.PACKET_ID + ") > " + 0 + " AND "
                            + "        " + (scrollType == ChatConstants.HISTORY_UP ? (DBConstants.MESSAGE_DATE + " < " + time) : (DBConstants.MESSAGE_DATE + " > " + time)) + " AND "
                            + "        " + DBConstants.FRIEND_TABLE_ID + " = " + friendId + " "
                            + "        UNION "
                            + "        SELECT "
                            + "            " + DBConstants.FRIEND_TABLE_ID + ", "
                            + "            '' AS " + DBConstants.SENDER_TABLE_ID + ", "
                            + "            " + DBConstants.CL_CALL_CATEGORY + " AS " + DBConstants.PACKET_TYPE + ", "
                            + "            " + DBConstants.CL_CALL_TYPE + " AS " + DBConstants.MESSAGE_TYPE + ", "
                            + "            " + DBConstants.MESSAGE + ", "
                            + "            " + DBConstants.CL_CALLING_TIME + " AS " + DBConstants.MESSAGE_DATE + ", "
                            + "            0 AS " + DBConstants.MESSAGE_DELIEVER_DATE + ", "
                            + "            0 AS " + DBConstants.MESSAGE_SEEN_DATE + ", "
                            + "            0 AS " + DBConstants.MESSAGE_VIEW_DATE + ", "
                            + "            0 AS " + DBConstants.STATUS + ", "
                            + "            0 AS " + DBConstants.FILE_ID + ", "
                            + "            0 AS " + DBConstants.FILE_STATUS + ", "
                            + "            " + DBConstants.CALL_ID + " AS " + DBConstants.PACKET_ID + ", "
                            + "            " + DBConstants.IS_UNREAD + ", "
                            + "            " + ChatConstants.SUBTYPE_CALL_LOG + " AS " + DBConstants.TABLE_TYPE + ", "
                            + "            0 AS " + DBConstants.TIME_OUT + ", "
                            + "            0 AS " + DBConstants.IS_SECRET_VISIBLE + ", "
                            + "            " + DBConstants.CL_CALL_DURATION + " "
                            + "        FROM "
                            + "        " + DBConstants.TABLE_RING_CALL_LOG + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                            + "        WHERE "
                            + "        " + DBConstants.CL_CALLING_TIME + " >= " + minTime + " AND "
                            + "        " + (scrollType == ChatConstants.HISTORY_UP ? (DBConstants.CL_CALLING_TIME + " < " + time) : (DBConstants.CL_CALLING_TIME + " > " + time)) + " AND "
                            + "        " + DBConstants.FRIEND_TABLE_ID + " = " + friendId + " "
                            + "    ) AS vTable "
                            + "    " + (!String.IsNullOrEmpty(ignoreUniqueIDs) ? "WHERE " + DBConstants.PACKET_ID + " NOT IN (" + ignoreUniqueIDs + ") " : " ")
                            + "    ORDER BY vTable." + DBConstants.MESSAGE_DATE + " " + (scrollType == ChatConstants.HISTORY_UP ? "DESC" : "ASC") + " "
                            + "    " + (limit != null ? " LIMIT " + limit.ToString() : "");
                }
                else if (groupId != null && groupId > 0)
                {
                    query = "SELECT * FROM "
                            + "    ( "
                            + "        SELECT "
                            + "            " + DBConstants.GROUP_ID + ", "
                            + "            " + DBConstants.SENDER_TABLE_ID + ", "
                            + "            " + DBConstants.PACKET_TYPE + ", "
                            + "            " + DBConstants.MESSAGE_TYPE + ", "
                            + "            " + DBConstants.MESSAGE + ", "
                            + "            " + DBConstants.MESSAGE_DATE + ", "
                            + "            " + DBConstants.MESSAGE_DELIEVER_DATE + ", "
                            + "            " + DBConstants.MESSAGE_SEEN_DATE + ", "
                            + "            " + DBConstants.MESSAGE_VIEW_DATE + ", "
                            + "            " + DBConstants.STATUS + ", "
                            + "            " + DBConstants.FILE_ID + ", "
                            + "            " + DBConstants.FILE_STATUS + ", "
                            + "            " + DBConstants.PACKET_ID + ", "
                            + "            " + DBConstants.IS_UNREAD + ", "
                            + "            " + ChatConstants.SUBTYPE_GROUP_CHAT + " AS " + DBConstants.TABLE_TYPE + ", "
                            + "            0 AS " + DBConstants.ACTIVITY_TYPE + ", "
                            + "            '' AS " + DBConstants.GROUP_NAME + ", "
                            + "            '' AS " + DBConstants.GROUP_PROFILE_IMAGE + ", "
                            + "            '' AS " + DBConstants.MEMBER_LIST + " "
                            + "        FROM "
                            + "        " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                            + "        WHERE "
                            + "        " + DBConstants.MESSAGE_DATE + " >= " + minTime + " AND "
                            + "        length(" + DBConstants.PACKET_ID + ") > " + 0 + " AND "
                            + "        " + (scrollType == ChatConstants.HISTORY_UP ? (DBConstants.MESSAGE_DATE + " < " + time) : (DBConstants.MESSAGE_DATE + " > " + time)) + " AND "
                            + "        " + DBConstants.GROUP_ID + " = " + groupId + " "
                            + "        UNION "
                            + "        SELECT "
                            + "            " + DBConstants.GROUP_ID + ", "
                            + "            " + DBConstants.ACTIVITY_BY + " AS " + DBConstants.SENDER_TABLE_ID + ", "
                            + "            0 AS " + DBConstants.PACKET_TYPE + ", "
                            + "            " + DBConstants.MESSAGE_TYPE + ", "
                            + "            '' AS " + DBConstants.MESSAGE + ", "
                            + "            " + DBConstants.UPDATE_TIME + " AS " + DBConstants.MESSAGE_DATE + ", "
                            + "            0 AS " + DBConstants.MESSAGE_DELIEVER_DATE + ", "
                            + "            0 AS " + DBConstants.MESSAGE_SEEN_DATE + ", "
                            + "            0 AS " + DBConstants.MESSAGE_VIEW_DATE + ", "
                            + "            0 AS " + DBConstants.STATUS + ", "
                            + "            0 AS " + DBConstants.FILE_ID + ", "
                            + "            0 AS " + DBConstants.FILE_STATUS + ", "
                            + "            " + DBConstants.PACKET_ID + ", "
                            + "            " + DBConstants.IS_UNREAD + ", "
                            + "            " + ChatConstants.SUBTYPE_GROUP_ACTIVITY + " AS " + DBConstants.TABLE_TYPE + ", "
                            + "            " + DBConstants.ACTIVITY_TYPE + ", "
                            + "            " + DBConstants.GROUP_NAME + ", "
                            + "            " + DBConstants.GROUP_PROFILE_IMAGE + ", "
                            + "            " + DBConstants.MEMBER_LIST + " "
                            + "        FROM "
                            + "        " + DBConstants.TABLE_RING_ACTIVITY + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                            + "        WHERE "
                            + "        " + DBConstants.UPDATE_TIME + " >= " + minTime + " AND "
                            + "        " + (scrollType == ChatConstants.HISTORY_UP ? (DBConstants.UPDATE_TIME + " < " + time) : (DBConstants.UPDATE_TIME + " > " + time)) + " AND "
                            + "        " + DBConstants.GROUP_ID + " = " + groupId + " "
                            + "    ) AS vTable "
                            + "    " + (!String.IsNullOrEmpty(ignoreUniqueIDs) ? "WHERE " + DBConstants.PACKET_ID + " NOT IN (" + ignoreUniqueIDs + ") " : " ")
                            + "    ORDER BY vTable." + DBConstants.MESSAGE_DATE + " " + (scrollType == ChatConstants.HISTORY_UP ? "DESC" : "ASC") + " "
                            + "    " + (limit != null ? " LIMIT " + limit.ToString() : "");
                }

                using (SQLiteCommand command = new SQLiteCommand(query, CreateDB.GetConnection()))
                {
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        int tableType = dr.GetInt32(14);
                        switch (tableType)
                        {
                            case ChatConstants.SUBTYPE_FRIEND_CHAT:
                                recentList.Add(MakeFriendChatObject(dr));
                                break;
                            case ChatConstants.SUBTYPE_CALL_LOG:
                                recentList.Add(MakeCallLogObject(dr));
                                break;
                            case ChatConstants.SUBTYPE_GROUP_CHAT:
                                recentList.Add(MakeGroupChatObject(dr));
                                break;
                            case ChatConstants.SUBTYPE_GROUP_ACTIVITY:
                                recentList.Add(MakeGroupActivityObject(dr));
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in LoadRecentChatCallActivityListByScroll() ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
            finally
            {

            }

            return recentList;
        }

        private static RecentDTO MakeFriendChatObject(SQLiteDataReader dr)
        {
            MessageDTO entity = new MessageDTO();
            entity.FriendTableID = dr.GetInt64(0);
            entity.SenderTableID = dr.GetInt64(1);
            entity.PacketType = dr.GetInt32(2);
            entity.MessageType = dr.GetInt32(3);
            entity.OriginalMessage = dr.GetString(4);
            entity.MessageDate = dr.GetInt64(5);
            entity.MessageDelieverDate = dr.GetInt64(6);
            entity.MessageSeenDate = dr.GetInt64(7);
            entity.MessageViewDate = dr.GetInt64(8);
            entity.Status = dr.GetInt32(9);
            entity.FileID = dr.GetInt64(10);
            entity.FileStatus = dr.GetInt32(11);
            entity.PacketID = dr.GetString(12);
            entity.IsUnread = dr.GetBoolean(13);
            entity.Timeout = dr.GetInt32(15);
            entity.IsSecretVisible = dr.GetInt32(16);
            ChatJSONParser.ParseMessage(entity);

            RecentDTO recent = new RecentDTO();
            recent.FriendTableID = entity.FriendTableID;
            recent.ContactID = entity.FriendTableID.ToString();
            recent.Time = entity.MessageDate;
            recent.Message = entity;
            return recent;
        }

        private static RecentDTO MakeCallLogObject(SQLiteDataReader dr)
        {
            CallLogDTO entity = new CallLogDTO();
            entity.FriendTableID = dr.GetInt64(0);
            entity.CallCategory = dr.GetInt32(2);
            entity.CallType = dr.GetInt32(3);
            entity.Message = dr.GetString(4);
            entity.CallingTime = dr.GetInt64(5);
            entity.CallID = dr.GetString(12);
            entity.IsUnread = dr.GetBoolean(13);
            entity.CallDuration = dr.GetInt64(17);
            entity.CallLogID = entity.FriendTableID + "-" + entity.CallCategory + "-" + entity.CallType;

            RecentDTO recent = new RecentDTO();
            recent.FriendTableID = entity.FriendTableID;
            recent.ContactID = entity.FriendTableID.ToString();
            recent.Time = entity.CallingTime;
            recent.CallLog = entity;
            return recent;
        }

        private static RecentDTO MakeGroupChatObject(SQLiteDataReader dr)
        {
            MessageDTO entity = new MessageDTO();
            entity.GroupID = dr.GetInt64(0);
            entity.SenderTableID = dr.GetInt64(1);
            entity.PacketType = dr.GetInt32(2);
            entity.MessageType = dr.GetInt32(3);
            entity.OriginalMessage = dr.GetString(4);
            entity.MessageDate = dr.GetInt64(5);
            entity.MessageDelieverDate = dr.GetInt64(6);
            entity.MessageSeenDate = dr.GetInt64(7);
            entity.MessageViewDate = dr.GetInt64(8);
            entity.Status = dr.GetInt32(9);
            entity.FileID = dr.GetInt64(10);
            entity.FileStatus = dr.GetInt32(11);
            entity.PacketID = dr.GetString(12);
            entity.IsUnread = dr.GetBoolean(13);
            ChatJSONParser.ParseMessage(entity);

            RecentDTO recent = new RecentDTO();
            recent.GroupID = entity.GroupID;
            recent.ContactID = entity.GroupID.ToString();
            recent.Time = entity.MessageDate;
            recent.Message = entity;
            return recent;
        }

        private static RecentDTO MakeGroupActivityObject(SQLiteDataReader dr)
        {
            ActivityDTO entity = new ActivityDTO();
            entity.GroupID = dr.GetInt64(0);
            entity.ActivityBy = dr.GetInt64(1);
            entity.MessageType = dr.GetInt32(3);
            entity.UpdateTime = dr.GetInt64(5);
            entity.PacketID = dr.GetString(12);
            entity.IsUnread = dr.GetBoolean(13);
            entity.ActivityType = dr.GetInt32(15);
            entity.GroupName = dr.GetString(16);
            entity.GroupProfileImage = dr.GetString(17);
            string memberList = dr.GetString(18);
            if (!String.IsNullOrWhiteSpace(memberList))
            {
                try
                {
                    JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                    entity.MemberList = oSerializer.Deserialize<List<ActivityMemberDTO>>(memberList);
                }
                catch (Exception)
                {
                    entity.MemberList = new List<ActivityMemberDTO>();
                }
            }

            RecentDTO recent = new RecentDTO();
            recent.GroupID = entity.GroupID;
            recent.ContactID = entity.GroupID.ToString();
            recent.Time = entity.UpdateTime;
            recent.Activity = entity;
            return recent;
        }

        public static MessageDTO GetMessageDTOByPacketID(long? friendId, long? groupId, string packetId, long fileId = 0)
        {
            MessageDTO entity = null;
            String query = String.Empty;
            try
            {
                CreateDBandTables CreateDB = new CreateDBandTables();
                CreateDB.ConnectToDatabase();

                if (friendId != null && friendId > 0)
                {
                    query = "SELECT "
                                + "            " + DBConstants.FRIEND_TABLE_ID + ", "
                                + "            " + DBConstants.SENDER_TABLE_ID + ", "
                                + "            " + DBConstants.PACKET_TYPE + ", "
                                + "            " + DBConstants.MESSAGE_TYPE + ", "
                                + "            " + DBConstants.MESSAGE + ", "
                                + "            " + DBConstants.MESSAGE_DATE + ", "
                                + "            " + DBConstants.MESSAGE_DELIEVER_DATE + ", "
                                + "            " + DBConstants.MESSAGE_SEEN_DATE + ", "
                                + "            " + DBConstants.MESSAGE_VIEW_DATE + ", "
                                + "            " + DBConstants.STATUS + ", "
                                + "            " + DBConstants.FILE_ID + ", "
                                + "            " + DBConstants.FILE_STATUS + ", "
                                + "            " + DBConstants.PACKET_ID + ", "
                                + "            " + DBConstants.IS_UNREAD + ", "
                                + "            " + ChatConstants.SUBTYPE_FRIEND_CHAT + " AS " + DBConstants.TABLE_TYPE + ", "
                                + "            " + DBConstants.TIME_OUT + ", "
                                + "            " + DBConstants.IS_SECRET_VISIBLE + " "
                                + " FROM " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " WHERE "
                                + (fileId > 0 ? DBConstants.FILE_ID + " = " + fileId : DBConstants.PACKET_ID + " = '" + packetId + "'") + " AND "
                                + DBConstants.FRIEND_TABLE_ID + " = " + friendId;
                }
                else if (groupId != null && groupId > 0)
                {
                    query = "SELECT "
                                + "            " + DBConstants.GROUP_ID + ", "
                                + "            " + DBConstants.SENDER_TABLE_ID + ", "
                                + "            " + DBConstants.PACKET_TYPE + ", "
                                + "            " + DBConstants.MESSAGE_TYPE + ", "
                                + "            " + DBConstants.MESSAGE + ", "
                                + "            " + DBConstants.MESSAGE_DATE + ", "
                                + "            " + DBConstants.MESSAGE_DELIEVER_DATE + ", "
                                + "            " + DBConstants.MESSAGE_SEEN_DATE + ", "
                                + "            " + DBConstants.MESSAGE_VIEW_DATE + ", "
                                + "            " + DBConstants.STATUS + ", "
                                + "            " + DBConstants.FILE_ID + ", "
                                + "            " + DBConstants.FILE_STATUS + ", "
                                + "            " + DBConstants.PACKET_ID + ", "
                                + "            " + DBConstants.IS_UNREAD + ", "
                                + "            " + ChatConstants.SUBTYPE_GROUP_CHAT + " AS " + DBConstants.TABLE_TYPE + " "
                                + " FROM " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " WHERE "
                                + (fileId > 0 ? DBConstants.FILE_ID + " = " + fileId : DBConstants.PACKET_ID + " = '" + packetId + "'") + " AND "
                                + DBConstants.GROUP_ID + " = " + groupId;
                }

                using (SQLiteCommand command = new SQLiteCommand(query, CreateDB.GetConnection()))
                {
                    SQLiteDataReader dr = command.ExecuteReader();
                    if (dr.Read())
                    {
                        int tableType = dr.GetInt32(14);
                        switch (tableType)
                        {
                            case ChatConstants.SUBTYPE_FRIEND_CHAT:
                                {
                                    RecentDTO recentDTO = MakeFriendChatObject(dr);
                                    entity = recentDTO.Message;
                                }
                                break;
                            case ChatConstants.SUBTYPE_GROUP_CHAT:
                                {
                                    RecentDTO recentDTO = MakeGroupChatObject(dr);
                                    entity = recentDTO.Message;
                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in GetMessageDTOByPacketID() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            finally
            {

            }

            return entity;
        }

        public static List<MessageDTO> GetMessageStatusByPacketIDs(long friendId, long groupId, List<string> packetIds)
        {
            List<MessageDTO> list = new List<MessageDTO>();
            String query = String.Empty;
            try
            {
                string pIDs = string.Empty;

                if (packetIds != null)
                {
                    foreach (string packetId in packetIds)
                    {
                        pIDs += "'" + packetId + "',";
                    }
                }

                if (!String.IsNullOrWhiteSpace(pIDs))
                {
                    pIDs = pIDs.Substring(0, pIDs.Length - 1);

                    CreateDBandTables CreateDB = new CreateDBandTables();
                    CreateDB.ConnectToDatabase();

                    if (friendId > 0)
                    {
                        query = "SELECT "
                                    + "   " + DBConstants.FRIEND_TABLE_ID + ", "
                                    + "   " + DBConstants.SENDER_TABLE_ID + ", "
                                    + "   " + DBConstants.STATUS + ", "
                                    + "   " + DBConstants.PACKET_ID + ", "
                                    + "   " + DBConstants.MESSAGE_TYPE + ", "
                                    + "   " + DBConstants.MESSAGE_DELIEVER_DATE + ", "
                                    + "   " + DBConstants.MESSAGE_SEEN_DATE + ", "
                                    + "   " + DBConstants.MESSAGE_VIEW_DATE + " "
                                    + " FROM " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                    + " WHERE "
                                    + DBConstants.PACKET_ID + " IN (" + pIDs + ")" + " AND "
                                    + DBConstants.FRIEND_TABLE_ID + " = " + friendId;
                    }
                    else if (groupId > 0)
                    {
                        query = "SELECT "
                                    + "   " + DBConstants.GROUP_ID + ", "
                                    + "   " + DBConstants.SENDER_TABLE_ID + ", "
                                    + "   " + DBConstants.STATUS + ", "
                                    + "   " + DBConstants.PACKET_ID + ", "
                                    + "   " + DBConstants.MESSAGE_TYPE + ", "
                                    + "   " + DBConstants.MESSAGE_DELIEVER_DATE + ", "
                                    + "   " + DBConstants.MESSAGE_SEEN_DATE + ", "
                                    + "   " + DBConstants.MESSAGE_VIEW_DATE + " "
                                    + " FROM " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                    + " WHERE "
                                    + DBConstants.PACKET_ID + " IN (" + pIDs + ")" + " AND "
                                    + DBConstants.GROUP_ID + " = " + groupId;
                    }

                    if (!String.IsNullOrWhiteSpace(query))
                    {
                        using (SQLiteCommand command = new SQLiteCommand(query, CreateDB.GetConnection()))
                        {
                            SQLiteDataReader dr = command.ExecuteReader();
                            while (dr.Read())
                            {
                                MessageDTO entity = new MessageDTO();
                                if (friendId > 0)
                                {
                                    entity.FriendTableID = dr.GetInt64(0);
                                }
                                else if (groupId > 0)
                                {
                                    entity.GroupID = dr.GetInt64(0);
                                }
                                entity.SenderTableID = dr.GetInt64(1);
                                entity.Status = dr.GetInt32(2);
                                entity.PacketID = dr.GetString(3);
                                entity.MessageType = dr.GetInt32(4);
                                entity.MessageDelieverDate = dr.GetInt64(5);
                                entity.MessageSeenDate = dr.GetInt64(6);
                                entity.MessageViewDate = dr.GetInt64(7);
                                list.Add(entity);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in GetMessageStatusByPacketIDs() ==> " + ex.Message + "\n" + ex.StackTrace);
            }
            finally
            {

            }

            return list;
        }

        public static List<string> GetFeedMessagePacketIDs(long friendIdentity, List<string> packetIds)
        {
            List<string> list = new List<string>();
            String query = String.Empty;

            try
            {
                if (packetIds.Count > 0)
                {
                    string pIDs = string.Empty;
                    foreach (string packetId in packetIds)
                    {
                        pIDs += "'" + packetId + "',";
                    }
                    if (!String.IsNullOrWhiteSpace(pIDs))
                    {
                        pIDs = pIDs.Substring(0, pIDs.Length - 1);

                        CreateDBandTables CreateDB = new CreateDBandTables();
                        CreateDB.ConnectToDatabase();

                        query = "SELECT " + DBConstants.PACKET_ID + " FROM " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                    + " WHERE "
                                    + DBConstants.PACKET_TYPE + " = " + ChatConstants.PACKET_TYPE_FEED_MESSAGE + " AND "
                                    + DBConstants.FRIEND_TABLE_ID + " = " + friendIdentity + " AND "
                                    + DBConstants.PACKET_ID + " IN (" + pIDs + ")";

                        using (SQLiteCommand command = new SQLiteCommand(query, CreateDB.GetConnection()))
                        {
                            SQLiteDataReader dr = command.ExecuteReader();
                            while (dr.Read())
                            {
                                list.Add(dr.GetString(0));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in GetFeedMessagePacketIDs() ==> " + ex.Message + "\n" + ex.StackTrace + ". Query = " + query);
            }

            return list;
        }

        public static List<RecentDTO> LoadRecentCallContactList(int day = ChatConstants.DAY_365_DAYS, int? offset = null, int? limit = null)
        {
            String query = String.Empty;
            long minTime = ModelUtility.GetStartTimeMillisLocal(DateTime.Now) - (day * SettingsConstants.MILISECONDS_IN_DAY);
            List<RecentDTO> recentList = new List<RecentDTO>();

            try
            {
                CreateDBandTables CreateDB = new CreateDBandTables();
                CreateDB.ConnectToDatabase();

                query =
                " SELECT " +
                "    vTABLE." + DBConstants.FRIEND_TABLE_ID + ", " +
                "    vTABLE." + DBConstants.CL_CALL_CATEGORY + ", " +
                "    vTABLE." + DBConstants.CL_CALL_TYPE + ", " +
                "    MAX(vTABLE." + DBConstants.CL_CALLING_TIME + ") AS " + DBConstants.CL_CALLING_TIME + ", " +
                "    MIN(vTABLE." + DBConstants.CL_CALLING_TIME + ") AS LAST_" + DBConstants.CL_CALLING_TIME + ", " +
                "    GROUP_CONCAT('''' || vTABLE." + DBConstants.CALL_ID + " || '''' || ':' || vTABLE." + DBConstants.CL_CALLING_TIME + ") AS CALL_IDS " +
                " FROM " +
                " ( " +
                "    SELECT " + DBConstants.FRIEND_TABLE_ID + ", " + DBConstants.CALL_ID + ", " + DBConstants.CL_CALLING_TIME + ", " + DBConstants.CL_CALL_CATEGORY + ", " + DBConstants.CL_CALL_TYPE + ", " +
                "    ( " +
                "        SELECT t1." + DBConstants.CL_CALLING_TIME + " FROM " + DBConstants.TABLE_RING_CALL_LOG + DefaultSettings.LOGIN_TABLE_ID.ToString() + " t1 " +
                "        WHERE " +
                "           t1." + DBConstants.CL_CALLING_TIME + " < t2." + DBConstants.CL_CALLING_TIME + " AND " +
                "           (t1." + DBConstants.FRIEND_TABLE_ID + " != t2." + DBConstants.FRIEND_TABLE_ID + " OR t1." + DBConstants.CL_CALL_CATEGORY + " != t2." + DBConstants.CL_CALL_CATEGORY + " OR t1." + DBConstants.CL_CALL_TYPE + " != t2." + DBConstants.CL_CALL_TYPE + ") " +
                "        ORDER BY " + DBConstants.CL_CALLING_TIME + " DESC LIMIT 1 " +
                "    ) AS GROUP_SEQ FROM " + DBConstants.TABLE_RING_CALL_LOG + DefaultSettings.LOGIN_TABLE_ID.ToString() + " t2 " +
                "    ORDER BY " + DBConstants.CL_CALLING_TIME + " DESC " +
                " ) AS vTABLE " +
                " WHERE vTABLE." + DBConstants.CL_CALLING_TIME + " >= " + minTime + " " +
                " GROUP BY vTABLE." + DBConstants.FRIEND_TABLE_ID + ", vTABLE." + DBConstants.CL_CALL_CATEGORY + ", vTABLE." + DBConstants.CL_CALL_TYPE + ", vTABLE.GROUP_SEQ " +
                " ORDER BY vTABLE." + DBConstants.CL_CALLING_TIME + " DESC "
                + (offset != null && limit != null ? ("LIMIT " + offset + ", " + limit) : ""); ;

                using (SQLiteCommand command = new SQLiteCommand(query, CreateDB.GetConnection()))
                {
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        string tempValue = dr.GetString(5);
                        if (!String.IsNullOrWhiteSpace(tempValue))
                        {
                            CallLogDTO callLogDTO = new CallLogDTO();
                            callLogDTO.FriendTableID = dr.GetInt64(0);
                            callLogDTO.CallCategory = dr.GetInt32(1);
                            callLogDTO.CallType = dr.GetInt32(2);
                            callLogDTO.CallingTime = dr.GetInt64(3);
                            callLogDTO.CallLogID = callLogDTO.FriendTableID + "-" + callLogDTO.CallCategory + "-" + callLogDTO.CallType;
                            callLogDTO.LastCallingTime = dr.GetInt64(4);
                            callLogDTO.CallIDs = JsonConvert.DeserializeObject<Dictionary<string, long>>("{" + tempValue + "}"); ;

                            RecentDTO recentDTO = new RecentDTO();
                            recentDTO.FriendTableID = callLogDTO.FriendTableID;
                            recentDTO.ContactID = callLogDTO.FriendTableID.ToString();
                            recentDTO.Time = callLogDTO.CallingTime;
                            recentDTO.CallLog = callLogDTO;
                            recentList.Add(recentDTO);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error occured during LoadRecentCallContactList. Exception :: ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
            return recentList;
        }

        public static List<RecentDTO> LoadRecentChatContactList(long friendIdentity = 0, long groupID = 0, string roomID = null, int day = ChatConstants.DAY_365_DAYS, int? offset = null, int? limit = null)
        {
            String query = String.Empty;
            long minTime = ModelUtility.GetStartTimeMillisLocal(DateTime.Now) - (day * SettingsConstants.MILISECONDS_IN_DAY);
            List<RecentDTO> recentList = new List<RecentDTO>();
            try
            {
                CreateDBandTables CreateDB = new CreateDBandTables();
                CreateDB.ConnectToDatabase();

                query = "SELECT * FROM ( "
                    + "        SELECT "
                    + "             vTABLE." + DBConstants.FRIEND_TABLE_ID + ", "
                    + "             0 AS " + DBConstants.GROUP_ID + ", "
                    + "             '' AS " + DBConstants.ROOM_ID + ", "
                    + "             " + DBConstants.SENDER_TABLE_ID + ", "
                    + "             " + DBConstants.MESSAGE + ", "
                    + "             vTABLE." + DBConstants.MESSAGE_DATE + ", "
                    + "             vTABLE." + DBConstants.PACKET_ID + ", "
                    + "             " + DBConstants.MESSAGE_TYPE + " "
                    + "        FROM " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "

                    + "        INNER JOIN  (SELECT "
                    + "                 " + DBConstants.FRIEND_TABLE_ID + ", "
                    + "                 " + DBConstants.PACKET_ID + ", "
                    + "                 MAX(" + DBConstants.MESSAGE_DATE + ") AS  " + DBConstants.MESSAGE_DATE + " "
                    + "             FROM "
                    + "             " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                    + "             WHERE "
                    + "                 " + (friendIdentity > 0 ? DBConstants.FRIEND_TABLE_ID + " = " + friendIdentity + " AND " : (groupID > 0 || !String.IsNullOrWhiteSpace(roomID) ? "1 = 0 AND " : ""))
                    + "                 " + DBConstants.MESSAGE_TYPE + " <> " + ChatConstants.TYPE_DELETE_MSG + " AND "
                    + "                 " + DBConstants.MESSAGE_DATE + " >= " + minTime + " "
                    + "             GROUP BY " + DBConstants.FRIEND_TABLE_ID + " "
                    + "        ) AS vTABLE ON "
                    + "        vTABLE." + DBConstants.FRIEND_TABLE_ID + " = " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.FRIEND_TABLE_ID + " AND "
                    + "        vTABLE." + DBConstants.PACKET_ID + " = " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.PACKET_ID + " AND "
                    + "        vTABLE." + DBConstants.MESSAGE_DATE + " = " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.MESSAGE_DATE + " "

                    + "        UNION "

                    + "        SELECT "
                    + "             0 AS " + DBConstants.FRIEND_TABLE_ID + ", "
                    + "             vTABLE." + DBConstants.GROUP_ID + ", "
                    + "             '' AS " + DBConstants.ROOM_ID + ", "
                    + "             " + DBConstants.SENDER_TABLE_ID + ", "
                    + "             " + DBConstants.MESSAGE + ", "
                    + "             vTABLE." + DBConstants.MESSAGE_DATE + ", "
                    + "             vTABLE." + DBConstants.PACKET_ID + ", "
                    + "             " + DBConstants.MESSAGE_TYPE + " "
                    + "        FROM " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                    + "        INNER JOIN  (SELECT "
                    + "                 " + DBConstants.GROUP_ID + ", "
                    + "                 " + DBConstants.PACKET_ID + ", "
                    + "                 MAX(" + DBConstants.MESSAGE_DATE + ") AS " + DBConstants.MESSAGE_DATE + " "
                    + "             FROM "
                    + "             " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                    + "             WHERE "
                    + "                 " + (groupID > 0 ? DBConstants.GROUP_ID + " = " + groupID + " AND " : (friendIdentity > 0 || !String.IsNullOrWhiteSpace(roomID) ? "1 = 0 AND " : ""))
                    + "                 " + DBConstants.MESSAGE_TYPE + " <> " + ChatConstants.TYPE_DELETE_MSG + " AND "
                    + "                 " + DBConstants.MESSAGE_DATE + " >= " + minTime + " "
                    + "             GROUP BY " + DBConstants.GROUP_ID + " "
                    + "         ) AS vTABLE ON "
                    + "         vTABLE." + DBConstants.GROUP_ID + " = " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.GROUP_ID + " AND "
                    + "         vTABLE." + DBConstants.PACKET_ID + " = " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.PACKET_ID + " AND "
                    + "         vTABLE." + DBConstants.MESSAGE_DATE + " = " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.MESSAGE_DATE + " "

                    + "        UNION "

                    + "        SELECT "
                    + "             0 AS " + DBConstants.FRIEND_TABLE_ID + ", "
                    + "             0 AS " + DBConstants.GROUP_ID + ", "
                    + "             vTABLE." + DBConstants.ROOM_ID + ", "
                    + "             " + DBConstants.SENDER_TABLE_ID + ", "
                    + "             " + DBConstants.MESSAGE + ", "
                    + "             vTABLE." + DBConstants.MESSAGE_DATE + ", "
                    + "             vTABLE." + DBConstants.PACKET_ID + ", "
                    + "             " + DBConstants.MESSAGE_TYPE + " "
                    + "        FROM " + DBConstants.TABLE_RING_ROOM_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                    + "        INNER JOIN  (SELECT "
                    + "                 " + DBConstants.ROOM_ID + ", "
                    + "                 " + DBConstants.PACKET_ID + ", "
                    + "                 MAX(" + DBConstants.MESSAGE_DATE + ") AS " + DBConstants.MESSAGE_DATE + " "
                    + "             FROM "
                    + "             " + DBConstants.TABLE_RING_ROOM_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                    + "             WHERE "
                    + "                 " + (!String.IsNullOrWhiteSpace(roomID) ? DBConstants.ROOM_ID + " = '" + roomID + "' AND " : (friendIdentity > 0 || groupID > 0 ? "1 = 0 AND " : ""))
                    + "                 " + DBConstants.MESSAGE_TYPE + " <> " + ChatConstants.TYPE_DELETE_MSG + " AND "
                    + "                 " + DBConstants.MESSAGE_DATE + " >= " + minTime + " "
                    + "             GROUP BY " + DBConstants.ROOM_ID + " "
                    + "         ) AS vTABLE ON "
                    + "         vTABLE." + DBConstants.ROOM_ID + " = " + DBConstants.TABLE_RING_ROOM_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.ROOM_ID + " AND "
                    + "         vTABLE." + DBConstants.PACKET_ID + " = " + DBConstants.TABLE_RING_ROOM_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.PACKET_ID + " AND "
                    + "         vTABLE." + DBConstants.MESSAGE_DATE + " = " + DBConstants.TABLE_RING_ROOM_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.MESSAGE_DATE + " "
                    + ") AS TEMP_TABLE "
                    + "ORDER BY  TEMP_TABLE." + DBConstants.MESSAGE_DATE + " DESC "
                    + (offset != null && limit != null ? ("LIMIT " + offset + ", " + limit) : "");

                using (SQLiteCommand command = new SQLiteCommand(query, CreateDB.GetConnection()))
                {
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        MessageDTO entity = new MessageDTO();
                        entity.FriendTableID = dr.GetInt64(0);
                        entity.GroupID = dr.GetInt64(1);
                        entity.RoomID = dr.GetString(2);
                        entity.SenderTableID = dr.GetInt64(3);
                        entity.OriginalMessage = dr.GetString(4);
                        entity.MessageDate = dr.GetInt64(5);
                        entity.PacketID = dr.GetString(6);
                        entity.MessageType = dr.GetInt32(7);
                        ChatJSONParser.ParseMessage(entity);
                        entity.Message = entity.Message == null ? String.Empty : entity.Message;

                        RecentDTO recentDTO = new RecentDTO();
                        if (entity.FriendTableID > 0)
                        {
                            recentDTO.FriendTableID = entity.FriendTableID;
                            recentDTO.ContactID = entity.FriendTableID.ToString();
                        }
                        else if (entity.GroupID > 0)
                        {
                            recentDTO.GroupID = entity.GroupID;
                            recentDTO.ContactID = entity.GroupID.ToString();
                        }
                        else
                        {
                            recentDTO.RoomID = entity.RoomID;
                            recentDTO.ContactID = entity.RoomID;
                        }
                        recentDTO.Time = entity.MessageDate;
                        recentDTO.Message = entity;
                        recentList.Add(recentDTO);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error occured during loadRecentChatContactList. Exception :: ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
            return recentList;
        }

        public static void DeleteChatHistory(long friendIdentity, long groupID)
        {
            string query = String.Empty;
            try
            {
                CreateDBandTables CreateDB = new CreateDBandTables();
                CreateDB.ConnectToDatabase();

                if (friendIdentity > 0)
                {
                    query = "DELETE FROM  " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString()
                            + " WHERE "
                            + DBConstants.FRIEND_TABLE_ID + "=" + friendIdentity + "";
                    CreateDB.ExecuteDBQurey(query);
                }
                else if (groupID > 0)
                {
                    query = "DELETE FROM  " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString()
                            + " WHERE "
                            + DBConstants.GROUP_ID + "=" + groupID + "";
                    CreateDB.ExecuteDBQurey(query);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error occured during DeletechatHistoryFromDB(long friendIdentity, long GroupID). Exception :: " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
        }

        public static void DeleteChatHistory(long friendIdentity, long groupID, List<string> packetIDs)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    string pIDs = string.Empty;
                    foreach (string packetID in packetIDs)
                    {
                        pIDs += "'" + packetID + "',";
                    }

                    if (!String.IsNullOrWhiteSpace(pIDs))
                    {
                        pIDs = pIDs.Substring(0, pIDs.Length - 1);

                        CreateDBandTables CreateDB = new CreateDBandTables();
                        CreateDB.ConnectToDatabase();

                        if (friendIdentity > 0)
                        {
                            query = "DELETE FROM  " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID
                                    + " WHERE "
                                    + DBConstants.PACKET_ID + " IN (" + pIDs + ") AND "
                                    + DBConstants.FRIEND_TABLE_ID + "=" + friendIdentity + "";
                            CreateDB.ExecuteDBQurey(query);
                        }
                        else if (groupID > 0)
                        {
                            query = "DELETE FROM  " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                    + " WHERE "
                                    + DBConstants.PACKET_ID + " IN (" + pIDs + ") AND "
                                    + DBConstants.GROUP_ID + "=" + groupID + "";
                            CreateDB.ExecuteDBQurey(query);
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error occured during DeletechatHistoryFromDB(long friendIdentity, long groupID, List<string> packetIDs). Exception :: " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

        public static void DeleteChatHistoryByMessageType(long friendIdentity, long groupID, List<string> packetIDs)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    string pIDs = string.Empty;
                    foreach (string packetID in packetIDs)
                    {
                        pIDs += "'" + packetID + "',";
                    }

                    if (!String.IsNullOrWhiteSpace(pIDs))
                    {
                        pIDs = pIDs.Substring(0, pIDs.Length - 1);

                        CreateDBandTables CreateDB = new CreateDBandTables();
                        CreateDB.ConnectToDatabase();

                        if (friendIdentity > 0)
                        {
                            query = "UPDATE " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID + " "
                                    + " SET "
                                    + DBConstants.MESSAGE_TYPE + "=" + ChatConstants.TYPE_DELETE_MSG + ", "
                                    + DBConstants.IS_UNREAD + "=" + ModelUtility.IsBoolean(false) + ", "
                                    + DBConstants.STATUS + "=" + ChatConstants.STATUS_DELETED + " "
                                    + " WHERE "
                                    + DBConstants.PACKET_ID + " IN (" + pIDs + ") AND "
                                    + DBConstants.FRIEND_TABLE_ID + "=" + friendIdentity + "";
                            CreateDB.ExecuteDBQurey(query);
                        }
                        else if (groupID > 0)
                        {
                            query = "UPDATE " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                                    + " SET "
                                    + DBConstants.MESSAGE_TYPE + "=" + ChatConstants.TYPE_DELETE_MSG + ", "
                                    + DBConstants.IS_UNREAD + "=" + ModelUtility.IsBoolean(false) + ", "
                                    + DBConstants.STATUS + "=" + ChatConstants.STATUS_DELETED + " "
                                    + " WHERE "
                                    + DBConstants.PACKET_ID + " IN (" + pIDs + ") AND "
                                    + DBConstants.GROUP_ID + "=" + groupID + "";
                            CreateDB.ExecuteDBQurey(query);
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error occured during DeletechatHistoryFromDB(long friendIdentity, long GroupID, List<string> packetIDs). Exception :: " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

        public static void DeleteCallHistory(long friendId, List<string> callIDs)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    string pIDs = string.Empty;
                    foreach (string id in callIDs)
                    {
                        pIDs += "'" + id + "',";
                    }

                    if (!String.IsNullOrWhiteSpace(pIDs))
                    {
                        pIDs = pIDs.Substring(0, pIDs.Length - 1);

                        CreateDBandTables CreateDB = new CreateDBandTables();
                        CreateDB.ConnectToDatabase();

                        query = "DELETE FROM  " + DBConstants.TABLE_RING_CALL_LOG + DefaultSettings.LOGIN_TABLE_ID
                                + " WHERE "
                                + DBConstants.CALL_ID + " IN (" + pIDs + ") AND "
                                + DBConstants.FRIEND_TABLE_ID + "=" + friendId + "";
                        CreateDB.ExecuteDBQurey(query);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error occured during DeleteCallHistory(List<string> callIDs, long friendId). Exception :: " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

        public static void DeleteCallChatHistoryByDate(long time)
        {
            string query = String.Empty;
            try
            {
                CreateDBandTables CreateDB = new CreateDBandTables();
                CreateDB.ConnectToDatabase();
                query = "DELETE FROM  " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString()
                            + " WHERE "
                            + DBConstants.MESSAGE_DATE + " < " + time + "";
                CreateDB.ExecuteDBQurey(query);
                query = "DELETE FROM  " + DBConstants.TABLE_RING_CALL_LOG + DefaultSettings.LOGIN_TABLE_ID.ToString()
                            + " WHERE "
                            + DBConstants.CL_CALLING_TIME + " < " + time + " ";
                CreateDB.ExecuteDBQurey(query);
                query = "DELETE FROM  " + DBConstants.TABLE_RING_ACTIVITY + DefaultSettings.LOGIN_TABLE_ID.ToString()
                            + " WHERE "
                            + DBConstants.UPDATE_TIME + " < " + time + " ";
                CreateDB.ExecuteDBQurey(query);
                query = "DELETE FROM  " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString()
                             + " WHERE "
                             + DBConstants.MESSAGE_DATE + " < " + time + " ";
                CreateDB.ExecuteDBQurey(query);
                query = "DELETE FROM  " + DBConstants.TABLE_RING_ROOM_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString()
                             + " WHERE "
                             + DBConstants.MESSAGE_DATE + " < " + time + " ";
                CreateDB.ExecuteDBQurey(query);
            }
            catch (Exception ex)
            {
                log.Error("Error occured during DeleteCallChatHistoryFromDBbyDate. Exception :: " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
        }

        public static void DeleteActivityHistory(long groupID, List<string> packetIDs)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    string pIDs = string.Empty;
                    foreach (string packetID in packetIDs)
                    {
                        pIDs += "'" + packetID + "',";
                    }

                    if (!String.IsNullOrWhiteSpace(pIDs))
                    {
                        pIDs = pIDs.Substring(0, pIDs.Length - 1);

                        CreateDBandTables CreateDB = new CreateDBandTables();
                        CreateDB.ConnectToDatabase();

                        query = "DELETE FROM  " + DBConstants.TABLE_RING_ACTIVITY + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                    + " WHERE "
                                    + DBConstants.PACKET_ID + " IN (" + pIDs + ") AND "
                                    + DBConstants.GROUP_ID + "=" + groupID + "";
                        CreateDB.ExecuteDBQurey(query);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error occured during DeleteActivityHistory(long groupID, List<string> packetIDs). Exception :: " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

        public static void UpdateChatMessageStatus(long friendIdentity, long groupID, string packetID, int status, long messageDeliverDate, long messageSeenDate, long messageViewDate)
        {
            new Thread(() =>
            {
                UpdateChatMessageStatusInSameProcess(friendIdentity, groupID, packetID, status, messageDeliverDate, messageDeliverDate, messageViewDate);
            }).Start();
        }

        public static void UpdateChatMessageStatusInSameProcess(long friendIdentity, long groupID, string packetID, int status, long messageDeliverDate, long messageSeenDate, long messageViewDate)
        {
            string query = String.Empty;
            try
            {
                if (friendIdentity > 0)
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                                 + " SET "
                                 + DBConstants.STATUS + "=" + status + " "
                                 + (messageDeliverDate > 0 ? ", " + DBConstants.MESSAGE_DELIEVER_DATE + "=" + messageDeliverDate + " " : "")
                                 + (messageSeenDate > 0 ? ", " + DBConstants.MESSAGE_SEEN_DATE + "=" + messageSeenDate + " " : "")
                                 + (messageViewDate > 0 ? ", " + DBConstants.MESSAGE_VIEW_DATE + "=" + messageViewDate + " " : "")
                                 + " WHERE "
                                 + DBConstants.FRIEND_TABLE_ID + "=" + friendIdentity + " AND "
                                 + DBConstants.PACKET_ID + "='" + packetID + "'";
                }
                else if (groupID > 0)
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                                + " SET "
                                + DBConstants.STATUS + "=" + status + " "
                                + (messageDeliverDate > 0 ? ", " + DBConstants.MESSAGE_DELIEVER_DATE + "=" + messageDeliverDate + " " : "")
                                + (messageSeenDate > 0 ? ", " + DBConstants.MESSAGE_SEEN_DATE + "=" + messageSeenDate + " " : "")
                                + (messageViewDate > 0 ? ", " + DBConstants.MESSAGE_VIEW_DATE + "=" + messageViewDate + " " : "")
                                + " WHERE "
                                + DBConstants.GROUP_ID + "=" + groupID + " AND "
                                + DBConstants.PACKET_ID + "='" + packetID + "'";
                }

                if (!String.IsNullOrWhiteSpace(query))
                {
                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();
                    createDB.ExecuteDBQurey(query);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error occured during UpdateChatMessageStatusInSameProcess. Exception :: " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
        }

        public static void UpdateChatMessageFileStatus(long friendIdentity, long groupID, string packetID, int status)
        {
            new Thread(() =>
            {
                UpdateChatMessageFileStatusInSameProcess(friendIdentity, groupID, packetID, status);
            }).Start();
        }

        public static void UpdateChatMessageFileStatusInSameProcess(long friendIdentity, long groupID, string packetID, int status)
        {
            string query = String.Empty;
            try
            {
                if (friendIdentity > 0)
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                                 + " SET "
                                 + DBConstants.FILE_STATUS + "=" + status + " "
                                 + " WHERE "
                                 + DBConstants.FRIEND_TABLE_ID + "=" + friendIdentity + " AND "
                                 + DBConstants.PACKET_ID + "='" + packetID + "'";
                }
                else if (groupID > 0)
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                                + " SET "
                                + DBConstants.FILE_STATUS + "=" + status + " "
                                + " WHERE "
                                + DBConstants.GROUP_ID + "=" + groupID + " AND "
                                + DBConstants.PACKET_ID + "='" + packetID + "'";
                }

                if (!String.IsNullOrWhiteSpace(query))
                {
                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();
                    createDB.ExecuteDBQurey(query);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error occured during UpdateChatMessageStatusInSameProcess. Exception :: " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
        }

        public static List<string> GetMessagePacketIDsWithIncompleteStatus(long friendIdentity)
        {
            List<string> pacaketIds = new List<string>();
            string query = string.Empty;
            try
            {
                long time = ModelUtility.GetStartTimeMillisLocal(DateTime.Now) - ((int)ChatConstants.DAY_30_DAYS * SettingsConstants.MILISECONDS_IN_DAY);

                if (friendIdentity > 0)
                {
                    query = "SELECT " + DBConstants.PACKET_ID + " FROM " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " WHERE "
                                + DBConstants.SENDER_TABLE_ID + " = " + DefaultSettings.LOGIN_TABLE_ID + " AND "
                                + DBConstants.FRIEND_TABLE_ID + " = " + friendIdentity + " AND "
                                + DBConstants.MESSAGE_TYPE + " > " + ChatConstants.TYPE_BLANK_MSG + " AND "
                                + DBConstants.MESSAGE_DATE + " > " + time + " AND "
                                + "("
                                + "(" + DBConstants.TIME_OUT + " = 0 AND (" + DBConstants.STATUS + " = " + ChatConstants.STATUS_SENT + " OR " + DBConstants.STATUS + " = " + ChatConstants.STATUS_DELIVERED + ")) "
                                + " OR "
                                + "(" + DBConstants.TIME_OUT + " > 0 AND (" + DBConstants.STATUS + " = " + ChatConstants.STATUS_SENT + " OR " + DBConstants.STATUS + " = " + ChatConstants.STATUS_DELIVERED + " OR " + DBConstants.STATUS + " = " + ChatConstants.STATUS_SEEN + "))"
                                + ")";

                    CreateDBandTables CreateDB = new CreateDBandTables();
                    CreateDB.ConnectToDatabase();

                    using (SQLiteCommand command = new SQLiteCommand(query, CreateDB.GetConnection()))
                    {
                        SQLiteDataReader dr = command.ExecuteReader();
                        while (dr.Read())
                        {
                            pacaketIds.Add(dr.GetString(dr.GetOrdinal(DBConstants.PACKET_ID)));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error occured during GetPacketIDWithIncompleteStatus. Exception :: ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
            return pacaketIds;
        }

        public static List<RecentDTO> GetUnreadChatList(int day = ChatConstants.DAY_365_DAYS)
        {
            String query = String.Empty;
            long minTime = ModelUtility.GetStartTimeMillisLocal(DateTime.Now) - (day * SettingsConstants.MILISECONDS_IN_DAY);
            List<RecentDTO> recentList = new List<RecentDTO>();
            try
            {
                CreateDBandTables CreateDB = new CreateDBandTables();
                CreateDB.ConnectToDatabase();

                query = "      SELECT "
                    + "             " + DBConstants.FRIEND_TABLE_ID + ", "
                    + "             0 AS " + DBConstants.GROUP_ID + ", "
                    + "             " + DBConstants.MESSAGE_DATE + ", "
                    + "             " + DBConstants.PACKET_ID + " "
                    + "        FROM " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                    + "        WHERE "
                    + "                 " + DBConstants.IS_UNREAD + " = " + ModelUtility.IsBoolean(true) + " AND "
                    + "                 " + DBConstants.MESSAGE_DATE + " >= " + minTime + " "
                    + "        UNION "
                    + "        SELECT "
                    + "             0 AS " + DBConstants.FRIEND_TABLE_ID + ", "
                    + "             " + DBConstants.GROUP_ID + ", "
                    + "             " + DBConstants.MESSAGE_DATE + ", "
                    + "             " + DBConstants.PACKET_ID + " "
                    + "        FROM " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                    + "        WHERE "
                    + "                 " + DBConstants.IS_UNREAD + " = " + ModelUtility.IsBoolean(true) + " AND "
                    + "                 " + DBConstants.MESSAGE_DATE + " >= " + minTime + " "
                    + "        ORDER BY " + DBConstants.FRIEND_TABLE_ID + " ASC, " + DBConstants.GROUP_ID + " ASC, " + DBConstants.MESSAGE_DATE + " DESC";

                using (SQLiteCommand command = new SQLiteCommand(query, CreateDB.GetConnection()))
                {
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        MessageDTO entity = new MessageDTO();
                        entity.FriendTableID = dr.GetInt64(0);
                        entity.GroupID = dr.GetInt64(1);
                        RecentDTO recentDTO = new RecentDTO();
                        if (entity.FriendTableID > 0)
                        {
                            recentDTO.ContactID = entity.FriendTableID.ToString();
                        }
                        else if (entity.GroupID > 0)
                        {
                            recentDTO.ContactID = entity.GroupID.ToString();
                        }
                        recentDTO.Time = dr.GetInt64(2);
                        recentDTO.UniqueKey = dr.GetString(3);
                        recentList.Add(recentDTO);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error occured during GetUnreadChatList. Exception :: ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
            return recentList;
        }

        public static List<RecentDTO> GetUnreadCallList(int day = ChatConstants.DAY_365_DAYS)
        {
            String query = String.Empty;
            long minTime = ModelUtility.GetStartTimeMillisLocal(DateTime.Now) - (day * SettingsConstants.MILISECONDS_IN_DAY);
            List<RecentDTO> recentList = new List<RecentDTO>();
            try
            {
                CreateDBandTables CreateDB = new CreateDBandTables();
                CreateDB.ConnectToDatabase();

                query = "      SELECT "
                    + "             " + DBConstants.FRIEND_TABLE_ID + ", "
                    + "             " + DBConstants.CL_CALLING_TIME + ", "
                    + "             " + DBConstants.CALL_ID + " "
                    + "        FROM " + DBConstants.TABLE_RING_CALL_LOG + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                    + "        WHERE "
                    + "                 " + DBConstants.IS_UNREAD + " = " + ModelUtility.IsBoolean(true) + " AND "
                    + "                 " + DBConstants.CL_CALLING_TIME + " >= " + minTime + " ";

                using (SQLiteCommand command = new SQLiteCommand(query, CreateDB.GetConnection()))
                {
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        MessageDTO entity = new MessageDTO();
                        entity.FriendTableID = dr.GetInt64(0);
                        RecentDTO recentDTO = new RecentDTO();
                        recentDTO.ContactID = entity.FriendTableID.ToString();
                        recentDTO.Time = dr.GetInt64(1);
                        recentDTO.UniqueKey = dr.GetString(2);
                        recentList.Add(recentDTO);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error occured during GetUnreadCallList. Exception :: ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
            return recentList;
        }

        public static ConcurrentDictionary<string, ConcurrentDictionary<string, int>> GetUniqueKeysGroupByContactIDs(long time)
        {
            String query = String.Empty;
            ConcurrentDictionary<string, ConcurrentDictionary<string, int>> recentList = new ConcurrentDictionary<string, ConcurrentDictionary<string, int>>();
            try
            {
                CreateDBandTables CreateDB = new CreateDBandTables();
                CreateDB.ConnectToDatabase();

                query = "SELECT "
                    + "     CONTACT_ID, "
                    + "     GROUP_CONCAT('''' || " + DBConstants.PACKET_ID + " || ''' : ' || TYPE) AS UNIQUE_KEYS "
                    + " FROM "
                    + " ( "
                    + "      SELECT "
                    + "           CAST(" + DBConstants.FRIEND_TABLE_ID + " AS TEXT) AS CONTACT_ID, "
                    + "           " + DBConstants.PACKET_ID + ", "
                    + "           " + DBConstants.MESSAGE_DATE + " AS " + DBConstants.MESSAGE_DATE + ", "
                    + "           1 AS TYPE "
                    + "      FROM " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                    + "      WHERE "
                    + "           " + DBConstants.MESSAGE_DATE + " < " + time + " "
                    + "      UNION "
                    + "      SELECT "
                    + "           CAST(" + DBConstants.FRIEND_TABLE_ID + " AS TEXT) AS CONTACT_ID, "
                    + "           " + DBConstants.CALL_ID + ", "
                    + "           " + DBConstants.CL_CALLING_TIME + " AS " + DBConstants.MESSAGE_DATE + ", "
                    + "           3 AS TYPE "
                    + "      FROM " + DBConstants.TABLE_RING_CALL_LOG + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                    + "      WHERE "
                    + "           " + DBConstants.CL_CALLING_TIME + " < " + time + " "
                    + "      UNION "
                    + "      SELECT "
                    + "           CAST(" + DBConstants.GROUP_ID + " AS TEXT) AS CONTACT_ID, "
                    + "           " + DBConstants.PACKET_ID + ", "
                    + "           " + DBConstants.MESSAGE_DATE + " AS " + DBConstants.MESSAGE_DATE + ", "
                    + "           2 AS TYPE "
                    + "      FROM " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                    + "      WHERE "
                    + "             " + DBConstants.MESSAGE_DATE + " < " + time + " "
                    + "      UNION "
                    + "      SELECT "
                    + "           CAST(" + DBConstants.GROUP_ID + " AS TEXT) AS CONTACT_ID, "
                    + "           " + DBConstants.PACKET_ID + ", "
                    + "           " + DBConstants.UPDATE_TIME + " AS " + DBConstants.MESSAGE_DATE + ", "
                    + "           5 AS TYPE "
                    + "      FROM " + DBConstants.TABLE_RING_ACTIVITY + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                    + "      WHERE "
                    + "           " + DBConstants.UPDATE_TIME + " < " + time + " "
                    + "      UNION "
                    + "      SELECT "
                    + "           " + DBConstants.ROOM_ID + " AS CONTACT_ID, "
                    + "           " + DBConstants.PACKET_ID + ", "
                    + "           " + DBConstants.MESSAGE_DATE + " AS " + DBConstants.MESSAGE_DATE + ", "
                    + "           6 AS TYPE "
                    + "      FROM " + DBConstants.TABLE_RING_ROOM_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                    + "      WHERE "
                    + "           " + DBConstants.MESSAGE_DATE + " < " + time + " "
                    + "      ORDER BY " + DBConstants.MESSAGE_DATE + " DESC "
                    + " ) "
                    + " GROUP BY CONTACT_ID";

                using (SQLiteCommand command = new SQLiteCommand(query, CreateDB.GetConnection()))
                {
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        string contactID = dr.GetString(0);
                        string tempValue = dr.GetString(1);
                        if (!String.IsNullOrWhiteSpace(contactID) && !String.IsNullOrWhiteSpace(tempValue))
                        {
                            recentList[contactID] = JsonConvert.DeserializeObject<ConcurrentDictionary<string, int>>("{" + tempValue + "}");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error occured during GetUniqueKeysGroupByContactIDs. Exception :: ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
            return recentList;
        }

        public static void MakeChatAsRead(long friendIdentity, long groupID, List<string> packetIds)
        {
            if (packetIds.Count > 0)
            {
                new Thread(() =>
                {
                    string query = String.Empty;
                    try
                    {
                        string pIDs = string.Empty;
                        foreach (string packetId in packetIds)
                        {
                            pIDs += "'" + packetId + "',";
                        }
                        pIDs = pIDs.Substring(0, pIDs.Length - 1);

                        if (friendIdentity > 0)
                        {
                            query = "UPDATE " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                                         + " SET "
                                         + DBConstants.IS_UNREAD + "=" + ModelUtility.IsBoolean(false) + " "
                                         + " WHERE "
                                         + DBConstants.FRIEND_TABLE_ID + "=" + friendIdentity + " AND "
                                         + DBConstants.PACKET_ID + " IN (" + pIDs + ")";
                        }
                        else if (groupID > 0)
                        {
                            query = "UPDATE " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                                        + " SET "
                                        + DBConstants.IS_UNREAD + "=" + ModelUtility.IsBoolean(false) + " "
                                        + " WHERE "
                                        + DBConstants.GROUP_ID + "=" + groupID + " AND "
                                        + DBConstants.PACKET_ID + " IN (" + pIDs + ")";
                        }

                        if (!String.IsNullOrWhiteSpace(query))
                        {
                            CreateDBandTables createDB = new CreateDBandTables();
                            createDB.ConnectToDatabase();
                            createDB.ExecuteDBQurey(query);
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("Error occured during MakeChatAsRead. Exception :: " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                    }
                }).Start();
            }
        }

        public static void MakeCallAsRead(long friendIdentity, List<string> packetIds)
        {
            if (packetIds.Count > 0)
            {
                new Thread(() =>
                {
                    string query = String.Empty;
                    try
                    {
                        string pIDs = string.Empty;
                        foreach (string packetId in packetIds)
                        {
                            pIDs += "'" + packetId + "',";
                        }
                        pIDs = pIDs.Substring(0, pIDs.Length - 1);

                        query = "UPDATE " + DBConstants.TABLE_RING_CALL_LOG + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                                     + " SET "
                                     + DBConstants.IS_UNREAD + "=" + ModelUtility.IsBoolean(false) + " "
                                     + " WHERE "
                                     + DBConstants.FRIEND_TABLE_ID + "=" + friendIdentity + " AND "
                                     + DBConstants.CALL_ID + " IN (" + pIDs + ")";

                        CreateDBandTables createDB = new CreateDBandTables();
                        createDB.ConnectToDatabase();
                        createDB.ExecuteDBQurey(query);
                    }
                    catch (Exception ex)
                    {
                        log.Error("Error occured during MakeCallAsRead. Exception :: " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                    }
                }).Start();
            }
        }

        public static void InsertMessageDTO(MessageDTO messageDTO)
        {
            if (messageDTO.FriendTableID > 0)
            {
                List<MessageDTO> msgList = new List<MessageDTO>();
                msgList.Add(messageDTO);

                new InsertIntoFriendMessageTable(msgList).Preocess();
            }
            else if (messageDTO.GroupID > 0)
            {
                List<MessageDTO> msgList = new List<MessageDTO>();
                msgList.Add(messageDTO);

                new InsertIntoGroupMessageTable(msgList).Preocess();
            }
            else if (!String.IsNullOrWhiteSpace(messageDTO.RoomID))
            {
                new InsertIntoRoomMessageTable(messageDTO).Preocess();
            }
        }

        public static void InsertCallLogDTO(CallLogDTO callLogDTO)
        {
            List<CallLogDTO> callLogList = new List<CallLogDTO>();
            callLogList.Add(callLogDTO);
            new InsertIntoCallLogTable(callLogList).Preocess();
        }

    }
}

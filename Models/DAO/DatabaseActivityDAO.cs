﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
using System.Text;
namespace Models.DAO
{
    public class DatabaseActivityDAO
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(DatabaseActivityDAO).Name);

        public CreateDBandTables createDBandTables;
        public DatabaseActivityDAO()
        {
            createDBandTables = new CreateDBandTables();
        }

        public void FetchRingAllUsersSettings()
        {
            SettingsConstants.VALUE_RINGID_USERINFO_UT = -1;
            SettingsConstants.VALUE_RINGID_CONTACT_UT = -1;
            SettingsConstants.VALUE_RINGID_GROUP_UT = 0;
            SettingsConstants.VALUE_RINGID_BLOCK_UNBLOCK_UT = 0;
            SettingsConstants.VALUE_RINGID_CIRCLE_UT = 0;
            SettingsConstants.VALUE_RINGID_CIRCLE_MEMBER_UT = 0;
            SettingsConstants.VALUE_RINGID_CALL_LOG_UT = -1;
            SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER = -1;
            SettingsConstants.VALUE_RINGID_PLAYER_DEVICE_NUMBER = -1;
            SettingsConstants.VALUE_RINGID_SPEAKER_VOLUME = 1F;
            SettingsConstants.VALUE_RINGID_SEPARATE_CHAT_VIEW = false;
            SettingsConstants.VALUE_RINGID_ALERT_SOUND = true;
            SettingsConstants.VALUE_RINGID_IM_SOUND = true;
            SettingsConstants.VALUE_RINGID_IM_NOTIFICATION = true;
            SettingsConstants.VALUE_RINGID_IM_POPUP = true;
            SettingsConstants.VALUE_RINGID_IM_AUTO_DOWNLOAD = false;
            SettingsConstants.VALUE_RINGID_NEWS_FEED_EXPIRY = AppConstants.DEFAULT_VALIDITY;
            //SettingsConstants.VALUE_RINGID_IS_CHAT_LOG_SYNC = false;
            SettingsConstants.VALUE_WALLET_PIN_NUMBER = string.Empty;
            SettingsConstants.VALUE_RINGID_CHAT_LOG_OFFSET = 0;
            SettingsConstants.VALUE_RINGID_IS_NO_MORE_CHAT_LOG = false;
            SettingsConstants.VALUE_WALLET_DWELLING_TIME_IN_MINUTES = 0;
            SettingsConstants.VALUE_WALLET_DWELLING_DATE = "1970-01-01";

            try
            {
                String query = "SELECT count(*) FROM sqlite_master WHERE type='table' AND name='" + DBConstants.TABLE_RING_LOGIN_SETTINGS + "'";
                using (SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection()))
                {
                    if (Convert.ToInt32(command.ExecuteScalar()) > 0)
                    {
                        query = "SELECT * FROM  " + DBConstants.TABLE_RING_ALL_USERS_SETTINGS + " WHERE " + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID + "";
                        using (SQLiteCommand cmd = new SQLiteCommand(query, createDBandTables.GetConnection()))
                        {
                            SQLiteDataReader dr = cmd.ExecuteReader();
                            while (dr.Read())
                            {
                                String key = dr.GetString(1);
                                String value = dr.GetString(2);
                                if (key != null && value != null)
                                {
                                    switch (key)
                                    {
                                        case SettingsConstants.KEY_RINGID_USERINFO_UT:
                                            {
                                                SettingsConstants.VALUE_RINGID_USERINFO_UT = ParseStringToLong(value, -1);
                                            }
                                            break;
                                        case SettingsConstants.KEY_RINGID_CONTACT_UT:
                                            {
                                                SettingsConstants.VALUE_RINGID_CONTACT_UT = ParseStringToLong(value, -1);
                                            }
                                            break;
                                        case SettingsConstants.KEY_RINGID_GROUP_UT:
                                            {
                                                SettingsConstants.VALUE_RINGID_GROUP_UT = ParseStringToLong(value, 0);
                                            }
                                            break;
                                        case SettingsConstants.KEY_RINGID_BLOCK_UNBLOCK_UT:
                                            {
                                                SettingsConstants.VALUE_RINGID_BLOCK_UNBLOCK_UT = ParseStringToLong(value, 0);
                                            }
                                            break;
                                        case SettingsConstants.KEY_RINGID_CIRCLE_UT:
                                            {
                                                SettingsConstants.VALUE_RINGID_CIRCLE_UT = ParseStringToLong(value, 0);
                                            }
                                            break;

                                        case SettingsConstants.KEY_RINGID_CIRCLE_MEMBER_UT:
                                            {
                                                SettingsConstants.VALUE_RINGID_CIRCLE_MEMBER_UT = ParseStringToLong(value, 0);
                                            }
                                            break;
                                        case SettingsConstants.KEY_RINGID_CALL_LOG_UT:
                                            SettingsConstants.VALUE_RINGID_CALL_LOG_UT = ParseStringToLong(value, 0);
                                            break;
                                        case SettingsConstants.KEY_RINGID_RECORDER_DEVICE_NUMBER:
                                            SettingsConstants.VALUE_RINGID_RECORDER_DEVICE_NUMBER = ParseStringToInt(value, -1);
                                            break;
                                        case SettingsConstants.KEY_RINGID_PLAYER_DEVICE_NUMBER:
                                            SettingsConstants.VALUE_RINGID_PLAYER_DEVICE_NUMBER = ParseStringToInt(value, -1);
                                            break;
                                        case SettingsConstants.KEY_RINGID_SPEAKER_VOLUME:
                                            SettingsConstants.VALUE_RINGID_SPEAKER_VOLUME = ParseStringToFloat(value, 0);
                                            break;
                                        case SettingsConstants.KEY_RINGID_SEPARATE_CHAT_VIEW:
                                            SettingsConstants.VALUE_RINGID_SEPARATE_CHAT_VIEW = ParseStringToBoolean(value, false);
                                            break;
                                        case SettingsConstants.KEY_RINGID_STICKER_VIEW:
                                            SettingsConstants.VALUE_RINGID_STICKER_VIEW = ParseStringToBoolean(value, false);
                                            break;
                                        case SettingsConstants.KEY_RINGID_ALERT_SOUND:
                                            SettingsConstants.VALUE_RINGID_ALERT_SOUND = ParseStringToBoolean(value, false);
                                            break;
                                        case SettingsConstants.KEY_RINGID_IM_SOUND: SettingsConstants.VALUE_RINGID_IM_SOUND = ParseStringToBoolean(value, false);
                                            break;
                                        case SettingsConstants.KEY_RINGID_IM_NOTIFICATION: SettingsConstants.VALUE_RINGID_IM_NOTIFICATION = ParseStringToBoolean(value, false);
                                            break;
                                        case SettingsConstants.KEY_RINGID_IM_POPUP: SettingsConstants.VALUE_RINGID_IM_POPUP = ParseStringToBoolean(value, false);
                                            break;
                                        case SettingsConstants.KEY_RINGID_IM_AUTO_DOWNLOAD: SettingsConstants.VALUE_RINGID_IM_AUTO_DOWNLOAD = ParseStringToBoolean(value, false);
                                            break;
                                        case SettingsConstants.KEY_RINGID_NEWS_FEED_EXPIRY: SettingsConstants.VALUE_RINGID_NEWS_FEED_EXPIRY = ParseStringToInt(value, -1);
                                            break;
                                        case SettingsConstants.KEY_RINGID_CHAT_LOG_OFFSET: SettingsConstants.VALUE_RINGID_CHAT_LOG_OFFSET = ParseStringToInt(value, 0);
                                            break;
                                        case SettingsConstants.KEY_RINGID_IS_NO_MORE_CHAT_LOG: SettingsConstants.VALUE_RINGID_IS_NO_MORE_CHAT_LOG = ParseStringToBoolean(value, false);
                                            break;
                                        case SettingsConstants.KEY_WALLET_PIN_NUMBER: SettingsConstants.VALUE_WALLET_PIN_NUMBER = value;
                                            break;
                                        case SettingsConstants.KEY_RINGID_WEBCAM_DEVICE_ID:
                                            if (value != null && value.Trim().Length > 0) SettingsConstants.VALUE_RINGID_WEBCAM_DEVICE_ID = value;
                                            break;
                                        case SettingsConstants.KEY_WALLET_DWELLING_TIME_IN_MINUTES:
                                            SettingsConstants.VALUE_WALLET_DWELLING_TIME_IN_MINUTES = ParseStringToInt(value, 0);
                                            System.Diagnostics.Debug.WriteLine("DWELLING TIME ==> " + SettingsConstants.VALUE_WALLET_DWELLING_TIME_IN_MINUTES);
                                            break;
                                        case SettingsConstants.KEY_WALLET_DWELLING_DATE:
                                            if (value != null && value.Trim().Length > 0) SettingsConstants.VALUE_WALLET_DWELLING_DATE = value;
                                            System.Diagnostics.Debug.WriteLine("DWELLING DATE ==> " + SettingsConstants.VALUE_WALLET_DWELLING_DATE);
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }

                if (DefaultSettings.DEBUG)
                {
                    log.Info("\nUSERINFO_UT.>" + SettingsConstants.VALUE_RINGID_USERINFO_UT +
                        "\nCONTACT_UT.>" + SettingsConstants.VALUE_RINGID_CONTACT_UT +
                        "\nCIRCLE_UT.> " + SettingsConstants.VALUE_RINGID_CIRCLE_UT +
                        "\nCIRCLE_MEMBER_UT.>" + SettingsConstants.VALUE_RINGID_CIRCLE_MEMBER_UT +
                        "\nGROUP_UT.> " + SettingsConstants.VALUE_RINGID_GROUP_UT +
                        "\nBLOCK_UNBLOCK_UT.> " + SettingsConstants.VALUE_RINGID_BLOCK_UNBLOCK_UT +
                        "\nCALL_LOG_UT.>" + SettingsConstants.VALUE_RINGID_CALL_LOG_UT +
                        "\nWEBCAM_DEVICE_ID.>" + SettingsConstants.VALUE_RINGID_WEBCAM_DEVICE_ID +
                        "\nSEPARATE_CHAT_VIEW.>" + SettingsConstants.VALUE_RINGID_SEPARATE_CHAT_VIEW +
                        "\nALERT_SOUND.>" + SettingsConstants.VALUE_RINGID_ALERT_SOUND +
                        "\nIM_SOUND.>" + SettingsConstants.VALUE_RINGID_IM_SOUND +
                        "\nIM_NOTIFICATION.>" + SettingsConstants.VALUE_RINGID_IM_NOTIFICATION +
                        "\nIM_POPUP.>" + SettingsConstants.VALUE_RINGID_IM_POPUP +
                        "\nIM_AUTO_DOWNLOAD.>" + SettingsConstants.VALUE_RINGID_IM_AUTO_DOWNLOAD +
                        "\nNEWS_FEED_EXPIRY.>" + SettingsConstants.VALUE_RINGID_NEWS_FEED_EXPIRY);
                }
                SettingsConstants.VALUE_RINGID_LAST_LOGIN_TIME = DateTime.Now.Millisecond;
            }
            catch (Exception ex)
            {
                log.Error("Error occured during fetching RingAllUsersSettings from DB. Exception :: " + ex.Message);
            }
        }


        private int ParseStringToInt(string s, int defaultVal)
        {
            try
            {
                return Int32.Parse(s);
            }
            catch (Exception)
            {
                return defaultVal;
            }
        }
        private long ParseStringToLong(string s, long defaultVal)
        {
            try
            {
                return long.Parse(s);
            }
            catch (Exception)
            {
                return defaultVal;
            }
        }

        private float ParseStringToFloat(string s, float defaultVal)
        {
            try
            {
                return float.Parse(s);
            }
            catch (Exception)
            {
                return defaultVal;
            }
        }

        private bool ParseStringToBoolean(string s, bool defaultVal)
        {
            try
            {
                return Boolean.Parse(s);
            }
            catch (Exception)
            {
                return defaultVal;
            }
        }

        public void FetchRingSignInSettingsBeforeLogin()
        {
            DefaultSettings.VALUE_NEW_USER_NAME = 0;
            DefaultSettings.PREVIOUS_USER_NAME = 0;
            DefaultSettings.VALUE_LOGIN_USER_NAME = "";
            DefaultSettings.VALUE_LOGIN_USER_TYPE = 0;
            DefaultSettings.VALUE_LOGIN_USER_PASSWORD = "";
            DefaultSettings.VALUE_LOGIN_AUTO_SIGNIN = 1;
            DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD = 1;
            DefaultSettings.VALUE_DOING_LIST_UT = 0;
            DefaultSettings.VALUE_LOGIN_USER_INFO = null;
            DefaultSettings.VALUE_APP_INSTALLED_VERSION = AppConfig.DESKTOP_FIRST_REALEASE_VERSION;
            DefaultSettings.IS_VERSION_VALUE_EXIST = false;
            DefaultSettings.DEVICE_UNIQUE_ID = string.Empty;
            try
            {
                String query = "SELECT count(*) FROM sqlite_master WHERE type='table' AND name='" + DBConstants.TABLE_RING_LOGIN_SETTINGS + "'";
                using (SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection()))
                {
                    if (Convert.ToInt32(command.ExecuteScalar()) > 0)
                    {
                        query = "SELECT * FROM  " + DBConstants.TABLE_RING_LOGIN_SETTINGS;
                        using (SQLiteCommand cmd = new SQLiteCommand(query, createDBandTables.GetConnection()))
                        {
                            SQLiteDataReader dr = cmd.ExecuteReader();
                            while (dr.Read())
                            {
                                String key = dr.GetString(0);
                                String value = dr.GetString(1);
                                if (key != null && value != null)
                                {
                                    switch (key)
                                    {
                                        case DefaultSettings.KEY_LOGIN_USER_NAME:
                                            {
                                                long id = 0;
                                                try { id = long.Parse(value); }
                                                catch (Exception) { }

                                                DefaultSettings.PREVIOUS_USER_NAME = id;
                                                DefaultSettings.VALUE_LOGIN_USER_NAME = value;
                                            }
                                            break;
                                        case DefaultSettings.KEY_LOGIN_USER_TYPE:
                                            {
                                                DefaultSettings.VALUE_LOGIN_USER_TYPE = ParseStringToInt(value, SettingsConstants.RINGID_LOGIN);
                                            }
                                            break;
                                        case DefaultSettings.KEY_LOGIN_USER_PASSWORD:
                                            {
                                                String pass = value;
                                                String newPass = HelperMethodsModel.EncryptPassword(pass);
                                                DefaultSettings.VALUE_LOGIN_USER_PASSWORD = newPass;
                                            }
                                            break;
                                        case DefaultSettings.KEY_LOGIN_SAVE_PASSWORD:
                                            {
                                                DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD = ParseStringToInt(value, 1);
                                            }
                                            break;
                                        case DefaultSettings.KEY_DOING_LIST_UT:
                                            {
                                                DefaultSettings.VALUE_DOING_LIST_UT = ParseStringToLong(value, 0);
                                            }
                                            break;
                                        case DefaultSettings.KEY_LOGIN_AUTO_SIGNIN:
                                            {
                                                DefaultSettings.VALUE_LOGIN_AUTO_SIGNIN = ParseStringToInt(value, 1);
                                            }
                                            break;
                                        case DefaultSettings.KEY_LOGIN_USER_INFO:
                                            {
                                                if (!string.IsNullOrEmpty(value))
                                                {
                                                    try
                                                    {
                                                        DefaultSettings.VALUE_LOGIN_USER_INFO = JObject.Parse(value);
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        DefaultSettings.VALUE_LOGIN_USER_INFO = null;
                                                        log.Error(ex.Message + "\n" + ex.StackTrace + "==>" + ex.Message);
                                                    }
                                                }
                                            }
                                            break;
                                        case DefaultSettings.KEY_MOBILE_DIALING_CODE:
                                            {
                                                DefaultSettings.VALUE_MOBILE_DIALING_CODE = value;
                                            }
                                            break;
                                        case DefaultSettings.KEY_NEW_USER_NAME:
                                            {
                                                long id = 0;
                                                try { id = long.Parse(value); }
                                                catch (Exception) { }
                                                DefaultSettings.VALUE_NEW_USER_NAME = id;
                                            }
                                            break;
                                        case DefaultSettings.KEY_APP_INSTALLED_VERSION:
                                            {
                                                DefaultSettings.VALUE_APP_INSTALLED_VERSION = value;
                                                DefaultSettings.IS_VERSION_VALUE_EXIST = true;
                                            }
                                            break;
                                        case DefaultSettings.KEY_DEVICE_UNIQUE_ID:
                                            {
                                                DefaultSettings.DEVICE_UNIQUE_ID = value;
                                            }
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("Error occured during fetching RingLoginSetting from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void FetchContactList()
        {
            try
            {
                String query = "SELECT "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.USER_TABLE_ID + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.RING_ID + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.FULL_NAME + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.FRIENDSHIP_STATUS + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.PROFILE_IMAGE + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.PROFILE_IMAGE_ID + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.COVER_IMAGE + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.COVER_IMAGE_ID + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.COVER_IMAGE_X + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.COVER_IMAGE_Y + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.PROFILE_IMAGE_PRIVACY + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.COVER_IMAGE_PRIVACY + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.DEVICE_TOKEN + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.CONTACT_TYPE + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.UPDATE_TIME + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.MATCHED_BY + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.NUMBER_OF_CALLS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.NUMBER_OF_MUTUAL_FRIENDS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.NUMBER_OF_CHATS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.FAVORITE_RANK + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CALL_ACCESS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_ACCESS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.FEED_ACCESS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_MIN_PACKET_ID + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_MAX_PACKET_ID + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IS_SECRET_VISIBLE + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_BG_URL + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CONTACT_ADDED_TIME + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CONTACT_UT + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CONTACT_READ_UNREAD + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.INCOMING_STATUS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_SOUND + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_NOTIFICATION + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_POPUP + " "
                    + " FROM " + DBConstants.TABLE_RING_USER_BASIC_INFO + " "
                    + " LEFT JOIN " + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + " ON "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.USER_TABLE_ID + "=" + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.USER_TABLE_ID + " ";
                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                SQLiteDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    UserBasicInfoDTO entity = new UserBasicInfoDTO();
                    entity.UserTableID = (long)dr.GetValue(dr.GetOrdinal(DBConstants.USER_TABLE_ID));
                    entity.RingID = (long)dr.GetValue(dr.GetOrdinal(DBConstants.RING_ID));
                    entity.FullName = dr.GetString(dr.GetOrdinal(DBConstants.FULL_NAME));
                    entity.Presence = StatusConstants.PRESENCE_OFFLINE;
                    entity.FriendShipStatus = dr[DBConstants.FRIENDSHIP_STATUS] != System.DBNull.Value ? dr.GetInt32(dr.GetOrdinal(DBConstants.FRIENDSHIP_STATUS)) : 0;
                    entity.ProfileImage = dr.GetString(dr.GetOrdinal(DBConstants.PROFILE_IMAGE));
                    entity.ProfileImageId = Guid.Parse(dr.GetString(dr.GetOrdinal(DBConstants.PROFILE_IMAGE_ID)));
                    entity.CoverImage = dr.GetString(dr.GetOrdinal(DBConstants.COVER_IMAGE));
                    entity.CoverImageId = Guid.Parse(dr.GetString(dr.GetOrdinal(DBConstants.COVER_IMAGE_ID)));
                    entity.CropImageX = dr.GetInt32(dr.GetOrdinal(DBConstants.COVER_IMAGE_X));
                    entity.CropImageY = dr.GetInt32(dr.GetOrdinal(DBConstants.COVER_IMAGE_Y));
                    entity.ProfileImagePrivacy = (short)dr.GetValue(dr.GetOrdinal(DBConstants.PROFILE_IMAGE_PRIVACY));
                    entity.CoverImagePrivacy = (short)dr.GetValue(dr.GetOrdinal(DBConstants.COVER_IMAGE_PRIVACY));
                    entity.DeviceToken = dr.GetString(dr.GetOrdinal(DBConstants.DEVICE_TOKEN));
                    entity.ContactType = (short)dr.GetValue(dr.GetOrdinal(DBConstants.CONTACT_TYPE));

                    entity.UpdateTime = dr[DBConstants.UPDATE_TIME] != System.DBNull.Value ? (long)dr.GetValue(dr.GetOrdinal(DBConstants.UPDATE_TIME)) : 0;
                    entity.MatchedBy = dr[DBConstants.MATCHED_BY] != System.DBNull.Value ? dr.GetInt32(dr.GetOrdinal(DBConstants.MATCHED_BY)) : 0;
                    entity.NumberOfCalls = dr[DBConstants.NUMBER_OF_CALLS] != System.DBNull.Value ? (long)dr.GetValue(dr.GetOrdinal(DBConstants.NUMBER_OF_CALLS)) : 0;
                    entity.NumberOfMutualFriends = dr[DBConstants.NUMBER_OF_MUTUAL_FRIENDS] != System.DBNull.Value ? (long)dr.GetValue(dr.GetOrdinal(DBConstants.NUMBER_OF_MUTUAL_FRIENDS)) : 0;
                    entity.NumberOfChats = dr[DBConstants.NUMBER_OF_CHATS] != System.DBNull.Value ? (long)dr.GetValue(dr.GetOrdinal(DBConstants.NUMBER_OF_CHATS)) : 0;
                    entity.FavoriteRank = dr[DBConstants.FAVORITE_RANK] != System.DBNull.Value ? dr.GetDouble(dr.GetOrdinal(DBConstants.FAVORITE_RANK)) : 0;
                    entity.CallAccess = dr[DBConstants.CALL_ACCESS] != System.DBNull.Value ? (short)dr.GetValue(dr.GetOrdinal(DBConstants.CALL_ACCESS)) : 0;
                    entity.ChatAccess = dr[DBConstants.CHAT_ACCESS] != System.DBNull.Value ? (short)dr.GetValue(dr.GetOrdinal(DBConstants.CHAT_ACCESS)) : 0;
                    entity.FeedAccess = dr[DBConstants.FEED_ACCESS] != System.DBNull.Value ? (short)dr.GetValue(dr.GetOrdinal(DBConstants.FEED_ACCESS)) : 0;
                    entity.ChatMinPacketID = dr[DBConstants.CHAT_MIN_PACKET_ID] != System.DBNull.Value ? dr.GetString(dr.GetOrdinal(DBConstants.CHAT_MIN_PACKET_ID)) : String.Empty;
                    entity.ChatMaxPacketID = dr[DBConstants.CHAT_MAX_PACKET_ID] != System.DBNull.Value ? dr.GetString(dr.GetOrdinal(DBConstants.CHAT_MAX_PACKET_ID)) : String.Empty;
                    entity.IsSecretVisible = dr[DBConstants.IS_SECRET_VISIBLE] != System.DBNull.Value ? dr.GetInt32(dr.GetOrdinal(DBConstants.IS_SECRET_VISIBLE)) : 1;
                    entity.ChatBgUrl = dr[DBConstants.CHAT_BG_URL] != System.DBNull.Value ? (string)dr.GetValue(dr.GetOrdinal(DBConstants.CHAT_BG_URL)) : String.Empty;
                    entity.ContactAddedTime = dr[DBConstants.CONTACT_ADDED_TIME] != System.DBNull.Value ? (long)dr.GetValue(dr.GetOrdinal(DBConstants.CONTACT_ADDED_TIME)) : 0;
                    entity.ContactUpdateTime = dr[DBConstants.CONTACT_UT] != System.DBNull.Value ? (long)dr.GetValue(dr.GetOrdinal(DBConstants.CONTACT_UT)) : 0;
                    entity.ContactAddedReadUnread = dr[DBConstants.CONTACT_READ_UNREAD] != System.DBNull.Value ? (short)dr.GetValue(dr.GetOrdinal(DBConstants.CONTACT_READ_UNREAD)) : (short)0;
                    entity.IncomingStatus = dr[DBConstants.INCOMING_STATUS] != System.DBNull.Value ? dr.GetInt32(dr.GetOrdinal(DBConstants.INCOMING_STATUS)) : 1;
                    entity.ImNotificationEnabled = dr[DBConstants.IM_NOTIFICATION] != System.DBNull.Value ? dr.GetBoolean(dr.GetOrdinal(DBConstants.IM_NOTIFICATION)) : false;
                    entity.ImPopupEnabled = dr[DBConstants.IM_POPUP] != System.DBNull.Value ? dr.GetBoolean(dr.GetOrdinal(DBConstants.IM_POPUP)) : false;
                    entity.ImSoundEnabled = dr[DBConstants.IM_SOUND] != System.DBNull.Value ? dr.GetBoolean(dr.GetOrdinal(DBConstants.IM_SOUND)) : false;
                    //FriendDictionaries.Instance.UTID_UID_DICTIONARY[entity.UserTableId] = entity.UserIdentity;
                    //  FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY[entity.UserTableId] = entity;
                    FriendDictionaries.Instance.AddOrUPdateInBasicInfoDictionary(entity.UserTableID, entity, true);

                }
            }
            catch (Exception e)
            {
                log.Error("Error occured during fetching Contract List from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void FetchMediaSearchList()
        {
            try
            {
                String query = "SELECT * FROM " + DBConstants.TABLE_RING_MEDIA_SEARCH + " WHERE " + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID + "";
                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                SQLiteDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    SearchMediaDTO searchMedia = new SearchMediaDTO();
                    searchMedia.SearchSuggestion = dr.GetString(dr.GetOrdinal(DBConstants.MEDIA_SEARCH_SUGGESTION));
                    searchMedia.SearchType = dr.GetInt32(dr.GetOrdinal(DBConstants.MEDIA_MEDIA_TYPE));
                    searchMedia.UpdateTime = (long)dr.GetValue(dr.GetOrdinal(DBConstants.UPDATE_TIME));
                    lock (MediaDictionaries.Instance.MEDIA_SEARCHES)
                    {
                        MediaDictionaries.Instance.MEDIA_SEARCHES.Add(searchMedia);
                    }
                }
                lock (MediaDictionaries.Instance.MEDIA_SEARCHES)
                {
                    MediaDictionaries.Instance.MEDIA_SEARCHES.Sort((x, y) => y.SearchSuggestion.Length.CompareTo(x.SearchSuggestion.Length));
                }
            }
            catch (Exception e)
            {
                log.Error("Error occured during FetchMediaSearchList from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void CreateRingHiddenPostsTable()
        {
            try
            {
                string sqlCreate = "CREATE TABLE IF NOT EXISTS " + DBConstants.TABLE_RING_HIDDEN_POSTS_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                 + "("
                                 + DBConstants.HIDDEN_POST + " BIGINT NOT NULL DEFAULT 0,"
                                 + " UNIQUE (" + DBConstants.HIDDEN_POST + ")"
                                 + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
            }
            catch (Exception e)
            {
                log.Error("Error occured during CreateRingHiddenPostsTable from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void CreateRingHiddenPostsUserTable()
        {
            try
            {
                string sqlCreate = "CREATE TABLE IF NOT EXISTS " + DBConstants.TABLE_RING_HIDDEN_POSTS_USERS_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                 + "("
                                 + DBConstants.HIDDEN_POST_USER + " BIGINT NOT NULL DEFAULT 0,"
                                 + " UNIQUE (" + DBConstants.HIDDEN_POST_USER + ")"
                                 + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
            }
            catch (Exception e)
            {
                log.Error("Error occured during CreateRingHiddenPostsUserTable from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void FetchHiddenPosts()
        {
            try
            {
                string query = "SELECT * FROM " + DBConstants.TABLE_RING_HIDDEN_POSTS_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString();
                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                SQLiteDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    lock (NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS)
                    {
                        NewsFeedDictionaries.Instance.HIDDEN_NEWS_FEEDS.Add((long)dr.GetValue(dr.GetOrdinal(DBConstants.HIDDEN_POST)));
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("Error occured during FetchHiddenPosts from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void InsertIntoHiddenPosts(long newsFeedId)
        {
            try
            {
                CreateDBandTables createDBandTables = new CreateDBandTables();
                createDBandTables.ConnectToDatabase();
                string sqlQuery = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_HIDDEN_POSTS_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " ( "
                                + DBConstants.HIDDEN_POST
                                + " ) "
                                + " VALUES"
                                + " ("
                                + newsFeedId
                                + ")";
                createDBandTables.ExecuteDBQurey(sqlQuery);

            }
            catch (Exception e)
            {
                log.Error("Error occured during InsertIntoHiddenPosts from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void FetchHiddenPostsFromUsers()
        {
            try
            {
                string query = "SELECT * FROM " + DBConstants.TABLE_RING_HIDDEN_POSTS_USERS_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString();
                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                SQLiteDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    lock (NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS)
                    {
                        NewsFeedDictionaries.Instance.HIDDEN_FEEDS_USERS.Add((long)dr.GetValue(dr.GetOrdinal(DBConstants.HIDDEN_POST_USER)));
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("Error occured during FetchHiddenPosts from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void InsertIntoHiddenPostsOfUsers(long userIdentity)
        {
            try
            {
                CreateDBandTables createDBandTables = new CreateDBandTables();
                createDBandTables.ConnectToDatabase();
                string sqlQuery = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_HIDDEN_POSTS_USERS_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " ( "
                                + DBConstants.HIDDEN_POST_USER
                                + " ) "
                                + " VALUES"
                                + " ("
                                + userIdentity
                                + ")";
                createDBandTables.ExecuteDBQurey(sqlQuery);

            }
            catch (Exception e)
            {
                log.Error("Error occured during InsertIntoHiddenPostsOfUsers from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void RemoveFromHiddenPostsOfUsers(long userIdentity)
        {
            try
            {
                CreateDBandTables createDBandTables = new CreateDBandTables();
                createDBandTables.ConnectToDatabase();
                string sqlQuery = "DELETE FROM " + DBConstants.TABLE_RING_HIDDEN_POSTS_USERS_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " WHERE "
                                + DBConstants.HIDDEN_POST_USER + " = " + userIdentity;
                createDBandTables.ExecuteDBQurey(sqlQuery);
            }
            catch (Exception e)
            {
                log.Error("Error occured during RemoveFromHiddenPostsOfUsers from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void FetchDoingTable()
        {
            try
            {
                String query = "SELECT * FROM " + DBConstants.TABLE_RING_DOING;
                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                SQLiteDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    DoingDTO dto = new DoingDTO();
                    dto.ID = (long)dr.GetValue(dr.GetOrdinal(DBConstants.ID));
                    dto.Category = (int)dr.GetValue(dr.GetOrdinal(DBConstants.DOING_CATEGORY));
                    dto.SortId = (int)dr.GetValue(dr.GetOrdinal(DBConstants.DOING_SORTID));
                    dto.Name = dr.GetString(dr.GetOrdinal(DBConstants.DOING_NAME));
                    dto.ImgUrl = dr.GetString(dr.GetOrdinal(DBConstants.DOING_URL));
                    dto.UpdateTime = (long)dr.GetValue(dr.GetOrdinal(DBConstants.DOING_UPDATE_TIME));
                    //if (dto.UpdateTime > DefaultSettings.VALUE_DOING_LIST_UT)
                    //{
                    //    DefaultSettings.VALUE_DOING_LIST_UT = dto.UpdateTime;
                    //    DatabaseActivityDAO.Instance.SaveDoingUpdateTimeIntoDB();
                    //}
                    lock (NewsFeedDictionaries.Instance.DOING_DICTIONARY)
                    {
                        if (NewsFeedDictionaries.Instance.DOING_DICTIONARY.ContainsKey(dto.ID))
                            NewsFeedDictionaries.Instance.DOING_DICTIONARY[dto.ID] = dto;
                        else
                        {
                            NewsFeedDictionaries.Instance.DOING_DICTIONARY.Add(dto.ID, dto);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("Error occured during fetching doing activity from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public long GetCircleMaxUpdateTime(long circleId)
        {
            long ut = 0;

            try
            {
                string query = string.Format("SELECT MAX({0}) AS {0} FROM {1} WHERE {2}=@loginUserId AND {3}=@circleId",
                                                  DBConstants.UPDATE_TIME, DBConstants.TABLE_RING_CIRCLE_MEMBERS_LIST, DBConstants.LOGIN_TABLE_ID, DBConstants.CIRCLE_ID);
                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                command.Parameters.AddWithValue("@loginUserId", Convert.ToInt32(DefaultSettings.LOGIN_TABLE_ID));
                command.Parameters.AddWithValue("@circleId", Convert.ToInt32(circleId));
                SQLiteDataReader reader = command.ExecuteReader();
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        ut = (long)reader.GetValue(reader.GetOrdinal(DBConstants.UPDATE_TIME));
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ERROR ==> " + e.Message + "\n" + e.StackTrace);
            }
            return ut;
        }

        public List<CircleDTO> FetchAllCircles()
        {
            string query = string.Empty;
            List<CircleDTO> tempCircleList = new List<CircleDTO>();
            try
            {
                query = string.Format("SELECT * FROM {0} WHERE {1} = @loginUserId AND {2} = 0 AND {3} > 0", DBConstants.TABLE_RING_CIRCLE_LIST, DBConstants.LOGIN_TABLE_ID, DBConstants.INTEGER_STATUS, DBConstants.MEMBER_COUNT);
                using (SQLiteCommand getCommand = new SQLiteCommand(createDBandTables.GetConnection()))
                {
                    getCommand.CommandText = query;
                    getCommand.Parameters.AddWithValue("@loginUserId", Convert.ToInt64(DefaultSettings.LOGIN_TABLE_ID));
                    SQLiteDataReader reader = getCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        CircleDTO dto = new CircleDTO();
                        dto.CircleId = reader.GetInt64(reader.GetOrdinal("CIRCLE_ID"));
                        dto.CircleName = reader.GetString(reader.GetOrdinal("CIRCLE_NAME"));
                        dto.SuperAdmin = reader.GetInt64(reader.GetOrdinal("SUPER_ADMIN"));
                        dto.MemberCount = reader.GetInt32(reader.GetOrdinal("MEMBER_COUNT"));
                        dto.UpdateTime = reader.GetInt64(reader.GetOrdinal("UPDATE_TIME"));
                        dto.UserTableID = reader.GetInt32(reader.GetOrdinal("USER_TABLE_ID"));
                        dto.IntegerStatus = reader.GetInt16(reader.GetOrdinal("INTEGER_STATUS"));
                        dto.AdminCount = reader.GetInt32(reader.GetOrdinal("ADMIN_COUNT"));
                        tempCircleList.Add(dto);
                        //lock (RingDictionaries.Instance.CIRCLE_LIST_DICTIONARY)
                        //{
                        //    if (!RingDictionaries.Instance.CIRCLE_LIST_DICTIONARY.ContainsKey(dto.CircleId))
                        //    {
                        //        RingDictionaries.Instance.CIRCLE_LIST_DICTIONARY.Add(dto.CircleId, dto);
                        //    }
                        //    else
                        //    {
                        //        if (dto.UpdateTime >= RingDictionaries.Instance.CIRCLE_LIST_DICTIONARY[dto.CircleId].UpdateTime)
                        //        {
                        //            RingDictionaries.Instance.CIRCLE_LIST_DICTIONARY[dto.CircleId] = dto;
                        //        }
                        //    }
                        //}
                    }
                }
                //if (RingDictionaries.Instance.CIRCLE_LIST_DICTIONARY.Count > 0)
                //{
                //    DefaultSettings.ALL_CIRCLE_FETCHED = true;
                //}
                if (tempCircleList.Count > 0)
                {
                    DefaultSettings.ALL_CIRCLE_FETCHED = true;
                }
            }
            catch (Exception e)
            {
                log.Error("ERROR at FetchingAllCircles==> " + e.Message + "\n" + e.StackTrace);
            }
            return tempCircleList;
        }

        public void FetchEmoticonList()
        {
            try
            {
                List<string> symList = new List<string>();
                String query = "SELECT * FROM " + DBConstants.TABLE_RING_EMOTICONS;
                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                SQLiteDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    EmoticonDTO entity = new EmoticonDTO();
                    entity.ID = dr.GetInt32(dr.GetOrdinal(DBConstants.ID));
                    entity.Name = dr.GetString(dr.GetOrdinal(DBConstants.EMO_NAME));
                    entity.Symbol = dr.GetString(dr.GetOrdinal(DBConstants.EMO_SYMBOL));
                    entity.Type = dr.GetInt32(dr.GetOrdinal(DBConstants.EMO_TYPE));
                    entity.Url = dr.GetString(dr.GetOrdinal(DBConstants.EMO_URL));
                    DefaultDictionaries.Instance.EMOTICON_DICTIONARY[entity.Symbol] = entity;

                    string sym = entity.Symbol;
                    sym = sym.Replace("|", "\\|");
                    sym = sym.Replace("(", "\\(");
                    sym = sym.Replace(")", "\\)");
                    sym = sym.Replace("*", "\\*");
                    sym = sym.Replace("$", "\\$");
                    sym = sym.Replace("/", "\\/");
                    sym = sym.Replace("^", "\\^");
                    sym = sym.Replace("?", "\\?");
                    //sym = sym.Replace(";", "\\;");
                    symList.Add(sym);

                }

                symList = symList.OrderByDescending(P => P).ToList();
                foreach (string sym in symList)
                {
                    if (!String.IsNullOrEmpty(DefaultSettings.EMOTION_REGULAR_EXPRESSION))
                    {
                        DefaultSettings.EMOTION_REGULAR_EXPRESSION = DefaultSettings.EMOTION_REGULAR_EXPRESSION + sym + "|";
                    }
                    else
                    {
                        DefaultSettings.EMOTION_REGULAR_EXPRESSION = sym + "|";
                    }
                }

            }
            catch (Exception e)
            {
                log.Error("Error occured during fetching Emoticon List from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void FetchDateEventList()
        {
            try
            {
                DateTime date = DateTime.Now;
                String query = "SELECT * FROM " + DBConstants.TABLE_RING_DATE_EVENT
                    + " WHERE "
                    + DBConstants.DATE_EVENT_MONTH + " >= " + date.Month + " AND "
                    + DBConstants.DATE_EVENT_DAY + " >= " + date.Day;

                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                SQLiteDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    DateEventDTO entity = new DateEventDTO();
                    entity.Day = dr.GetInt32(dr.GetOrdinal(DBConstants.DATE_EVENT_DAY));
                    entity.Month = dr.GetInt32(dr.GetOrdinal(DBConstants.DATE_EVENT_MONTH));
                    entity.Url = dr.GetString(dr.GetOrdinal(DBConstants.DATE_EVENT_URL));
                    if (!String.IsNullOrWhiteSpace(entity.Url))
                    {
                        ChatDictionaries.Instance.DATE_EVENT_DICTIONARY[entity.Day + "-" + entity.Month] = entity;
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("Error occured during fetching Date Event List from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void CreateContactListTable()
        {
            String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " (" + ""
                                + DBConstants.USER_TABLE_ID + "    BIGINT,"
                                + DBConstants.RING_ID + "    BIGINT,"
                                + DBConstants.FRIENDSHIP_STATUS + "    SMALLINT,"
                                + DBConstants.UPDATE_TIME + "  BIGINT NOT NULL DEFAULT 0,"
                                + DBConstants.MATCHED_BY + "  SMALLINT NOT NULL DEFAULT 0,"
                                + DBConstants.NUMBER_OF_MUTUAL_FRIENDS + "  BIGINT NOT NULL DEFAULT 0,"
                                + DBConstants.NUMBER_OF_CALLS + "  BIGINT NOT NULL DEFAULT 0,"
                                + DBConstants.NUMBER_OF_CHATS + "  BIGINT NOT NULL DEFAULT 0,"
                                + DBConstants.FAVORITE_RANK + "  DOUBLE NOT NULL DEFAULT 0,"
                                + DBConstants.CALL_ACCESS + " SMALLINT NOT NULL DEFAULT 0,"
                                + DBConstants.CHAT_ACCESS + " SMALLINT NOT NULL DEFAULT 0,"
                                + DBConstants.FEED_ACCESS + " SMALLINT NOT NULL DEFAULT 0,"
                                + DBConstants.CHAT_MIN_PACKET_ID + "  VARCHAR(64) NOT NULL,"
                                + DBConstants.CHAT_MAX_PACKET_ID + "  VARCHAR(64) NOT NULL,"
                                + DBConstants.IS_SECRET_VISIBLE + "  SMALLINT NOT NULL DEFAULT 1,"
                                + DBConstants.CHAT_BG_URL + "  VARCHAR(250),"
                                + DBConstants.CONTACT_ADDED_TIME + "    BIGINT NOT NULL DEFAULT 0,"
                                + DBConstants.CONTACT_UT + "    BIGINT NOT NULL DEFAULT 0,"
                                + DBConstants.CONTACT_READ_UNREAD + "    SMALLINT NOT NULL DEFAULT 0,"
                                + DBConstants.INCOMING_STATUS + "    INTEGER NOT NULL DEFAULT 0,"
                                + DBConstants.IM_SOUND + "    BOOLEAN NOT NULL DEFAULT 1,"
                                + DBConstants.IM_NOTIFICATION + "    BOOLEAN NOT NULL DEFAULT 1,"
                                + DBConstants.IM_POPUP + "    BOOLEAN NOT NULL DEFAULT 1,"
                                + " UNIQUE (" + DBConstants.USER_TABLE_ID + ")"
                                + ")";
            createDBandTables.ExecuteDBQurey(sqlCreate);

            sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_CONTACT_LIST_USER_TABLE_ID, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.USER_TABLE_ID + ")";
            createDBandTables.ExecuteDBQurey(sqlCreate);
        }

        public void CreateFriendChatTable()
        {
            try
            {
                String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                    + " ("
                                    + DBConstants.ID + "  INTEGER PRIMARY KEY   AUTOINCREMENT,"
                                    + DBConstants.FRIEND_TABLE_ID + "   BIGINT,"
                                    + DBConstants.SENDER_TABLE_ID + "  BIGINT,"
                                    + DBConstants.PACKET_TYPE + "  INTEGER,"
                                    + DBConstants.MESSAGE_TYPE + "  INTEGER,"
                                    + DBConstants.MESSAGE + "   VARCHAR(32000),"
                                    + DBConstants.MESSAGE_DATE + "  BIGINT,"
                                    + DBConstants.MESSAGE_DELIEVER_DATE + "  BIGINT,"
                                    + DBConstants.MESSAGE_SEEN_DATE + "  BIGINT,"
                                    + DBConstants.MESSAGE_VIEW_DATE + "  BIGINT,"
                                    + DBConstants.STATUS + "  INTEGER,"
                                    + DBConstants.FILE_ID + "  BIGINT NOT NULL DEFAULT 0,"
                                    + DBConstants.FILE_STATUS + "  INTEGER NOT NULL DEFAULT 0,"
                                    + DBConstants.PACKET_ID + "   VARCHAR(64) NOT NULL,"
                                    + DBConstants.IS_UNREAD + "  BOOLEAN NOT NULL DEFAULT FALSE,"
                                    + DBConstants.TIME_OUT + "  INTEGER,"
                                    + DBConstants.IS_SECRET_VISIBLE + "  SMALLINT NOT NULL DEFAULT 0,"
                                    + " UNIQUE (" + DBConstants.FRIEND_TABLE_ID + ", " + DBConstants.PACKET_ID + "))";
                createDBandTables.ExecuteDBQurey(sqlCreate);
                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_FRIEND_CHAT_FRIEND_TABLE_ID, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.FRIEND_TABLE_ID + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_FRIEND_CHAT_PACKET_ID, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.PACKET_ID + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_FRIEND_CHAT_MESSAGE_DATE, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.MESSAGE_DATE + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_FRIEND_CHAT_MESSAGE_TYPE, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.MESSAGE_TYPE + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_FRIEND_CHAT_FILE_ID, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_FRIEND_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.FILE_ID + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
            }
            catch (Exception e)
            {
                log.Error("Error occured during Creating Friend Chat Table from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void CreateGroupChatTable()
        {
            try
            {
                String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + "  ("
                                + DBConstants.ID + "   INTEGER PRIMARY KEY   AUTOINCREMENT,"
                                + DBConstants.GROUP_ID + "  BIGINT NOT NULL,"
                                + DBConstants.SENDER_TABLE_ID + "  BIGINT,"
                                + DBConstants.PACKET_TYPE + "  INTEGER,"
                                + DBConstants.MESSAGE_TYPE + "  INTEGER,"
                                + DBConstants.MESSAGE + "   VARCHAR(32000),"
                                + DBConstants.MESSAGE_DATE + "  BIGINT,"
                                + DBConstants.MESSAGE_DELIEVER_DATE + "  BIGINT,"
                                + DBConstants.MESSAGE_SEEN_DATE + "  BIGINT,"
                                + DBConstants.MESSAGE_VIEW_DATE + "  BIGINT,"
                                + DBConstants.STATUS + "  INTEGER,"
                                + DBConstants.FILE_ID + "  BIGINT NOT NULL DEFAULT 0,"
                                + DBConstants.FILE_STATUS + "  INTEGER NOT NULL DEFAULT 0,"
                                + DBConstants.PACKET_ID + "   VARCHAR(64) NOT NULL,"
                                + DBConstants.IS_UNREAD + "  BOOLEAN NOT NULL DEFAULT FALSE,"
                                + " UNIQUE (" + DBConstants.GROUP_ID + ", " + DBConstants.PACKET_ID + "))";
                createDBandTables.ExecuteDBQurey(sqlCreate);
                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_GROUP_CHAT_GROUP_ID, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.GROUP_ID + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_GROUP_CHAT_PACKET_ID, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.PACKET_ID + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_GROUP_CHAT_MESSAGE_DATE, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.MESSAGE_DATE + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_GROUP_CHAT_MESSAGE_TYPE, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.MESSAGE_TYPE + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_GROUP_CHAT_FILE_ID, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.FILE_ID + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
            }
            catch (Exception e)
            {
                log.Error("Error occured during Creating Friend Chat Table from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void CreateRoomChatTable()
        {
            try
            {
                String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_ROOM_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + "  ("
                                + DBConstants.ID + "   INTEGER PRIMARY KEY   AUTOINCREMENT,"
                                + DBConstants.ROOM_ID + "  VARCHAR(64) NOT NULL DEFAULT '',"
                                + DBConstants.SENDER_TABLE_ID + "  BIGINT,"
                                + DBConstants.PACKET_TYPE + "  INTEGER,"
                                + DBConstants.MESSAGE_TYPE + "  INTEGER,"
                                + DBConstants.MESSAGE + "   VARCHAR(32000),"
                                + DBConstants.MESSAGE_DATE + "  BIGINT,"
                                + DBConstants.MESSAGE_DELIEVER_DATE + "  BIGINT,"
                                + DBConstants.MESSAGE_SEEN_DATE + "  BIGINT,"
                                + DBConstants.MESSAGE_VIEW_DATE + "  BIGINT,"
                                + DBConstants.STATUS + "  INTEGER,"
                                + DBConstants.FILE_ID + "  BIGINT NOT NULL DEFAULT 0,"
                                + DBConstants.FILE_STATUS + "  INTEGER NOT NULL DEFAULT 0,"
                                + DBConstants.PACKET_ID + "   VARCHAR(64) NOT NULL,"
                                + DBConstants.IS_UNREAD + "  BOOLEAN NOT NULL DEFAULT FALSE,"
                                + DBConstants.FULL_NAME + "   VARCHAR(200),"
                                + DBConstants.PROFILE_IMAGE + "   VARCHAR(256),"
                                + " UNIQUE (" + DBConstants.ROOM_ID + ", " + DBConstants.PACKET_ID + "))";
                createDBandTables.ExecuteDBQurey(sqlCreate);
                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_ROOM_CHAT_ROOM_ID, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_ROOM_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.ROOM_ID + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_ROOM_CHAT_PACKET_ID, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_ROOM_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.PACKET_ID + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_ROOM_CHAT_MESSAGE_DATE, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_ROOM_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.MESSAGE_DATE + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_ROOM_CHAT_MESSAGE_TYPE, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_ROOM_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.MESSAGE_TYPE + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
            }
            catch (Exception e)
            {
                log.Error("Error occured during Creating Room Chat Table from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void CreateCallLogTable()
        {
            try
            {
                String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_CALL_LOG + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                    + " ("
                                    + DBConstants.CALL_ID + " VARCHAR(64) NOT NULL,"
                                    + DBConstants.FRIEND_TABLE_ID + " BIGINT,"
                                    + DBConstants.CL_CALL_CATEGORY + " SMALLINT,"
                                    + DBConstants.CL_CALL_TYPE + " SMALLINT,"
                                    + DBConstants.CL_CALL_DURATION + " BIGINT,"
                                    + DBConstants.CL_CALLING_TIME + " BIGINT,"
                                    + DBConstants.MESSAGE + " VARCHAR(250),"
                                    + DBConstants.IS_UNREAD + "  BOOLEAN NOT NULL DEFAULT FALSE,"
                                    + " UNIQUE (" + DBConstants.FRIEND_TABLE_ID + ", " + DBConstants.CALL_ID + ")"
                                    + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);

                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_CALL_LOG_FRIEND_TABLE_ID, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_CALL_LOG + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.FRIEND_TABLE_ID + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_CALL_LOG_CALLING_TIME, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_CALL_LOG + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.CL_CALLING_TIME + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_CALL_LOG_CALL_ID, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_CALL_LOG + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.CALL_ID + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
            }
            catch (Exception e)
            {
                log.Error("Error occured during Creating Call Log Table from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void CreateActivityTable()
        {
            try
            {
                String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_ACTIVITY + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                    + " ("
                                    + DBConstants.ID + "  INTEGER PRIMARY KEY   AUTOINCREMENT,"
                                    + DBConstants.FRIEND_TABLE_ID + " BIGINT,"
                                    + DBConstants.GROUP_ID + " BIGINT,"
                                    + DBConstants.ACTIVITY_TYPE + " SMALLINT,"
                                    + DBConstants.MESSAGE_TYPE + " SMALLINT,"
                                    + DBConstants.ACTIVITY_BY + " BIGINT,"
                                    + DBConstants.UPDATE_TIME + " BIGINT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEMBER_LIST + " VARCHAR(3100),"
                                    + DBConstants.GROUP_NAME + " VARCHAR(500),"
                                    + DBConstants.GROUP_PROFILE_IMAGE + " VARCHAR(250),"
                                    + DBConstants.PACKET_ID + "   VARCHAR(64) NOT NULL,"
                                    + DBConstants.IS_UNREAD + "  BOOLEAN NOT NULL DEFAULT FALSE,"
                                    + " UNIQUE (" + DBConstants.PACKET_ID + "))";
                createDBandTables.ExecuteDBQurey(sqlCreate);

                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_ACTIVITY_FRIEND_TABLE_ID, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_ACTIVITY + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.FRIEND_TABLE_ID + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_ACTIVITY_GROUP_ID, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_ACTIVITY + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.GROUP_ID + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_ACTIVITY_UPDATE_TIME, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_ACTIVITY + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.UPDATE_TIME + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
            }
            catch (Exception e)
            {
                log.Error("Error occured during Creating Activity Table from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }

        }

        public void CreateGroupListTable()
        {
            try
            {
                String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                    + " ("
                                    + DBConstants.GROUP_ID + " BIGINT,"
                                    + DBConstants.GROUP_NAME + " VARCHAR(500),"
                                    + DBConstants.GROUP_PROFILE_IMAGE + " VARCHAR(250),"
                                    + DBConstants.NUMBER_OF_MEMBERS + "  INTEGER NOT NULL DEFAULT 0,"
                                    + DBConstants.CHAT_MIN_PACKET_ID + "  VARCHAR(64) NOT NULL,"
                                    + DBConstants.CHAT_MAX_PACKET_ID + "  VARCHAR(64) NOT NULL,"
                                    + DBConstants.CHAT_BG_URL + "  VARCHAR(250),"
                                    + DBConstants.IS_PARTIAL + " BOOLEAN NOT NULL DEFAULT FALSE,"
                                    + DBConstants.IM_SOUND + "    BOOLEAN NOT NULL DEFAULT 1,"
                                    + DBConstants.IM_NOTIFICATION + "    BOOLEAN NOT NULL DEFAULT 1,"
                                    + DBConstants.IM_POPUP + "    BOOLEAN NOT NULL DEFAULT 1,"
                                    + "  UNIQUE (" + DBConstants.GROUP_ID + ")"
                                    + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);

                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_GROUP_LIST_GROUP_ID, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.GROUP_ID + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
            }
            catch (Exception e)
            {
                log.Error("Error occured during Creating Group List Table from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void CreateGroupMemberListTable()
        {
            try
            {
                String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_GROUP_MEMBER_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                    + " ("
                                    + DBConstants.GROUP_ID + " BIGINT,"
                                    + DBConstants.USER_TABLE_ID + " BIGINT,"
                                    + DBConstants.RING_ID + " BIGINT,"
                                    + DBConstants.FULL_NAME + " VARCHAR(250),"
                                    + DBConstants.MEMBER_ACCESS_TYPE + "  SMALLINT,"
                                    + DBConstants.MEMBER_ADDED_BY + " BIGINT,"
                                    + " UNIQUE (" + DBConstants.GROUP_ID + ", " + DBConstants.USER_TABLE_ID + ")"
                                    + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);

                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_GROUP_MEMBER_LIST_GROUP_ID, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_GROUP_MEMBER_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.GROUP_ID + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_GROUP_MEMBER_LIST_TABLE_ID, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_GROUP_MEMBER_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.USER_TABLE_ID + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
            }
            catch (Exception e)
            {
                log.Error("Error occured during Creating Group Member List Table from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void CreateRoomListTable()
        {
            try
            {
                String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                    + " ("
                                    + DBConstants.ROOM_ID + " VARCHAR(64) NOT NULL DEFAULT '',"
                                    + DBConstants.ROOM_NAME + " VARCHAR(200) NOT NULL DEFAULT '',"
                                    + DBConstants.ROOM_IMAGE + " VARCHAR(256),"
                                    + DBConstants.NUMBER_OF_MEMBERS + " INTEGER NOT NULL DEFAULT 0,"
                                    + DBConstants.CHAT_MIN_PACKET_ID + "  VARCHAR(64) NOT NULL,"
                                    + DBConstants.CHAT_MAX_PACKET_ID + "  VARCHAR(64) NOT NULL,"
                                    + DBConstants.CHAT_BG_URL + "  VARCHAR(250),"
                                    + DBConstants.IS_PARTIAL + " BOOLEAN NOT NULL DEFAULT FALSE,"
                                    + DBConstants.IM_SOUND + "    BOOLEAN NOT NULL DEFAULT 1,"
                                    + DBConstants.IM_NOTIFICATION + "    BOOLEAN NOT NULL DEFAULT 1,"
                                    + DBConstants.IM_POPUP + "    BOOLEAN NOT NULL DEFAULT 1,"
                                    + "  UNIQUE (" + DBConstants.ROOM_ID + ")"
                                    + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);

                sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_ROOM_LIST_ROOM_ID, DefaultSettings.LOGIN_TABLE_ID.ToString()) + " ON " + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + " (" + DBConstants.ROOM_ID + ")";
                createDBandTables.ExecuteDBQurey(sqlCreate);
            }
            catch (Exception e)
            {
                log.Error("Error occured during Creating Room List Table from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void UpdateTableAndPrevData()
        {
            string prevVersion = DefaultSettings.VALUE_APP_INSTALLED_VERSION;
            List<string> queryList = new List<string>();
            List<string> tableList = new List<string>();

            try
            {
                if (String.Compare(AppConfig.DESKTOP_VALID_REALEASE_VERSION, prevVersion) > 0)
                {
                    String query = "SELECT name FROM sqlite_master WHERE type='table' AND name LIKE 'RING_%'";
                    using (SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection()))
                    {
                        SQLiteDataReader dataReader = command.ExecuteReader();
                        while (dataReader.Read())
                        {
                            string name = dataReader.GetString(dataReader.GetOrdinal("name"));
                            if (!name.Equals(DBConstants.TABLE_RING_LOGIN_SETTINGS))
                            {
                                tableList.Add(name);
                            }
                        }
                        dataReader.Close();
                        dataReader.Dispose();

                        foreach (string name in tableList)
                        {
                            command.CommandText = "DROP TABLE '" + name + "'";
                            command.ExecuteNonQuery();
                        }
                    }
                    DefaultSettings.VALUE_APP_INSTALLED_VERSION = AppConfig.DESKTOP_REALEASE_VERSION;
                    SaveAppInstalledVersion();
                    return;
                }
                else if (String.Compare(AppConfig.DESKTOP_REALEASE_VERSION, prevVersion) <= 0)
                {
                    DefaultSettings.VALUE_APP_INSTALLED_VERSION = AppConfig.DESKTOP_REALEASE_VERSION;
                    SaveAppInstalledVersion();
                    return;
                }

                prevVersion = "Models.Resources.UpdateScripts." + prevVersion + ".sql";
                string currVersion = "Models.Resources.UpdateScripts." + AppConfig.DESKTOP_REALEASE_VERSION + ".sql";

                var assembly = Assembly.GetExecutingAssembly();
                List<string> sqlResourceList = assembly.GetManifestResourceNames().Where(P => P.EndsWith(".sql") && String.Compare(P, currVersion) <= 0 && String.Compare(P, prevVersion) > 0).ToList();
                List<long> loginUserList = new List<long>();

                if (sqlResourceList.Count > 0)
                {
                    try
                    {
                        String query = "SELECT name FROM sqlite_master WHERE type='table'";
                        using (SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection()))
                        {
                            SQLiteDataReader dataReader = command.ExecuteReader();
                            while (dataReader.Read())
                            {
                                tableList.Add(dataReader.GetString(dataReader.GetOrdinal("name")));
                            }

                            if (tableList.Contains(DBConstants.TABLE_RING_ALL_USERS_SETTINGS))
                            {
                                query = "SELECT " + DBConstants.LOGIN_TABLE_ID + " FROM  " + DBConstants.TABLE_RING_ALL_USERS_SETTINGS + " GROUP BY " + DBConstants.LOGIN_TABLE_ID;
                                using (SQLiteCommand cmd = new SQLiteCommand(query, createDBandTables.GetConnection()))
                                {
                                    SQLiteDataReader dr = cmd.ExecuteReader();
                                    while (dr.Read())
                                    {
                                        loginUserList.Add((long)dr.GetValue(dr.GetOrdinal(DBConstants.LOGIN_TABLE_ID)));
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        log.Error("Error occured during fetching Already logged in userlist from DB. Exception :: " + e.Message + "\n" + e.StackTrace);
                    }

                    foreach (string embeddedResource in sqlResourceList)
                    {
                        using (Stream stream = assembly.GetManifestResourceStream(embeddedResource))
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            string[] tempArray = reader.ReadToEnd().Split(';').Select(P => P.Trim()).Where(P => P.Length > 0).ToArray();
                            for (int idx = 0; idx < tempArray.Length; idx += 2)
                            {
                                string tableName = tempArray[idx];
                                string query = tempArray[idx + 1];

                                if (tableName.Contains("{0}"))
                                {
                                    foreach (long userId in loginUserList)
                                    {
                                        tableName = String.Format(tableName, userId);
                                        if (tableList.Contains(tableName))
                                        {
                                            queryList.Add(query.Replace("{0}", userId.ToString()));
                                        }
                                    }
                                }
                                else
                                {
                                    if (tableList.Contains(tableName))
                                    {
                                        queryList.Add(query);
                                    }
                                }
                            }
                        }
                    }

                    if (queryList.Count > 0)
                    {
                        try
                        {
                            createDBandTables.InsertOrUpdateCommandNeedtoRollback(queryList);
                            DefaultSettings.VALUE_APP_INSTALLED_VERSION = AppConfig.DESKTOP_REALEASE_VERSION;
                            SaveAppInstalledVersion();
                        }
                        catch (Exception ex)
                        {
                            log.Error("Error occured during select table name from DB. Exception :: " + ex.Message + "\n" + ex.StackTrace + ", Query => " + String.Join(";", queryList));
                        }
                    }
                    else
                    {
                        DefaultSettings.VALUE_APP_INSTALLED_VERSION = AppConfig.DESKTOP_REALEASE_VERSION;
                        SaveAppInstalledVersion();
                    }
                }
                else
                {
                    DefaultSettings.VALUE_APP_INSTALLED_VERSION = AppConfig.DESKTOP_REALEASE_VERSION;
                    SaveAppInstalledVersion();
                }
            }
            catch (Exception e)
            {
                log.Error("Error occured during update table and previous data in DB. Exception :: " + e.Message + "\n" + e.StackTrace);
            }
        }

        public void SaveLoginInfointoDB(string info)
        {
            IList<InfoKeyValue> infoKeyValues = new List<InfoKeyValue>();
            if (DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD == 0)
            {
                //DefaultSettings.VALUE_LOGIN_AUTO_SIGNIN = 0;
                DefaultSettings.VALUE_LOGIN_USER_TYPE = 0;
                DefaultSettings.VALUE_LOGIN_USER_NAME = string.Empty;
            }
            infoKeyValues.Add(new InfoKeyValue(DefaultSettings.KEY_LOGIN_USER_TYPE, DefaultSettings.VALUE_LOGIN_USER_TYPE + ""));
            infoKeyValues.Add(new InfoKeyValue(DefaultSettings.KEY_LOGIN_USER_NAME, DefaultSettings.VALUE_LOGIN_USER_NAME.ToString()));
            infoKeyValues.Add(new InfoKeyValue(DefaultSettings.KEY_MOBILE_DIALING_CODE, DefaultSettings.VALUE_MOBILE_DIALING_CODE));
            infoKeyValues.Add(new InfoKeyValue(DefaultSettings.KEY_LOGIN_USER_PASSWORD, HelperMethodsModel.EncryptPassword(DefaultSettings.VALUE_LOGIN_USER_PASSWORD)));

            infoKeyValues.Add(new InfoKeyValue(DefaultSettings.KEY_LOGIN_SAVE_PASSWORD, DefaultSettings.VALUE_LOGIN_SAVE_PASSWORD + ""));
            infoKeyValues.Add(new InfoKeyValue(DefaultSettings.KEY_LOGIN_AUTO_SIGNIN, DefaultSettings.VALUE_LOGIN_AUTO_SIGNIN + ""));
            infoKeyValues.Add(new InfoKeyValue(DefaultSettings.KEY_LOGIN_USER_INFO, info));
            new InsertRingLoginSetting(infoKeyValues).Start();
        }
        public void SaveNewUserIDintoDB()
        {
            IList<InfoKeyValue> infoKeyValues = new List<InfoKeyValue>();
            infoKeyValues.Add(new InfoKeyValue(DefaultSettings.KEY_NEW_USER_NAME, DefaultSettings.VALUE_NEW_USER_NAME.ToString()));
            new InsertRingLoginSetting(infoKeyValues).Start();
        }
        public void SaveAppInstalledVersion()
        {
            IList<InfoKeyValue> infoKeyValues = new List<InfoKeyValue>();
            infoKeyValues.Add(new InfoKeyValue(DefaultSettings.KEY_APP_INSTALLED_VERSION, DefaultSettings.VALUE_APP_INSTALLED_VERSION));
            new InsertRingLoginSetting(infoKeyValues).Run();
        }
        public void DeleteNewUserIDfromDB()
        {
            IList<InfoKeyValue> infoKeyValues = new List<InfoKeyValue>();
            infoKeyValues.Add(new InfoKeyValue(DefaultSettings.KEY_NEW_USER_NAME, StatusConstants.STATUS_DELETED));
            new InsertRingLoginSetting(infoKeyValues).Start();
        }
        public void SaveChangedPasswordIntoDB()
        {
            IList<InfoKeyValue> infoKeyValues = new List<InfoKeyValue>();
            infoKeyValues.Add(new InfoKeyValue(DefaultSettings.KEY_LOGIN_USER_PASSWORD, HelperMethodsModel.EncryptPassword(DefaultSettings.VALUE_LOGIN_USER_PASSWORD)));
            new InsertRingLoginSetting(infoKeyValues).Start();
        }
        public void SaveDoingUpdateTimeIntoDB()
        {
            IList<InfoKeyValue> infoKeyValues = new List<InfoKeyValue>();
            infoKeyValues.Add(new InfoKeyValue(DefaultSettings.KEY_DOING_LIST_UT, DefaultSettings.VALUE_DOING_LIST_UT + ""));
            new InsertRingLoginSetting(infoKeyValues).Start();
        }
        public void SaveDeviceUniqueIDintoDB()
        {
            IList<InfoKeyValue> infoKeyValues = new List<InfoKeyValue>();
            infoKeyValues.Add(new InfoKeyValue(DefaultSettings.KEY_DEVICE_UNIQUE_ID, DefaultSettings.DEVICE_UNIQUE_ID.ToString()));
            new InsertRingLoginSetting(infoKeyValues).Start();
        }
    }
}

﻿using log4net;
using Models.Constants;
using System;
using System.Data.SQLite;
using System.Threading;

namespace Models.DAO
{
    public class InsertIntoNotificationCountTable
    {
        private readonly ILog log = LogManager.GetLogger(typeof(InsertIntoNotificationCountTable).Name);

        public InsertIntoNotificationCountTable()
        {
           new Thread(new ThreadStart(Run)).Start();
        }

        private void Run()
        {
            string query = String.Empty;
            try
            {
                CreateDBandTables createDBandTables = new CreateDBandTables();
                createDBandTables.ConnectToDatabase();


                int RowCount = 0;
                query = "SELECT COUNT(*) FROM  " + DBConstants.TABLE_RING_NOTIFICATION_COUNT
                        + " WHERE "
                        + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID + "";
                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                RowCount = Convert.ToInt32(command.ExecuteScalar());
                if (RowCount < 1)
                {
                    //insert
                    query = "INSERT INTO " + DBConstants.TABLE_RING_NOTIFICATION_COUNT
                        + " ("
                        + DBConstants.LOGIN_TABLE_ID + ","
                        + DBConstants.COUNT_CALL_NOTIFICATION + ","
                        + DBConstants.COUNT_CHAT_NOTIFICATION + ","
                        + DBConstants.COUNT_ALL_NOTIFICATION + ","
                        + DBConstants.COUNT_ADD_FRIEND_NOTIFICATION
                        + ")"
                        + "VALUES"
                        + " ("
                        + DefaultSettings.LOGIN_TABLE_ID + ","
                        + AppConstants.CALL_NOTIFICATION_COUNT + ","
                        + AppConstants.CHAT_NOTIFICATION_COUNT + ","
                        + AppConstants.ALL_NOTIFICATION_COUNT + ","
                        + AppConstants.ADD_FRIEND_NOTIFICATION_COUNT
                         + ")";
                    createDBandTables.ExecuteDBQurey(query);
                }
                else
                {
                    //update
                    query = "UPDATE " + DBConstants.TABLE_RING_NOTIFICATION_COUNT
                        + " SET "
                        + DBConstants.COUNT_CALL_NOTIFICATION + " = " + AppConstants.CALL_NOTIFICATION_COUNT + ","
                        + DBConstants.COUNT_CHAT_NOTIFICATION + " = " + AppConstants.CHAT_NOTIFICATION_COUNT + ","
                        + DBConstants.COUNT_ALL_NOTIFICATION + " = " + AppConstants.ALL_NOTIFICATION_COUNT + ","
                        + DBConstants.COUNT_ADD_FRIEND_NOTIFICATION + " = " + AppConstants.ADD_FRIEND_NOTIFICATION_COUNT 
                        + " WHERE "
                        + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID;
                    createDBandTables.ExecuteDBQurey(query);
                }


            }
            catch (Exception e)
            {
                log.Error("SQLException in InsertIntoNotificationCountTable class ==> " + e.Message + "\n" + e.StackTrace + ", Query = " + query);
            }
        }
    }
}

﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Models.DAO
{
    public class InsertIntoRoomMessageTable
    {
        private readonly ILog log = LogManager.GetLogger(typeof(InsertIntoRoomMessageTable).Name);

        private MessageDTO _MessageDTO;

        public InsertIntoRoomMessageTable(MessageDTO messageDTO)
        {
            this._MessageDTO = messageDTO;
        }

        public void Preocess()
        {
            string insertQuery = String.Empty;
            string updateQuery = String.Empty;
            string query = String.Empty;

            try
            {
                if (_MessageDTO != null)
                {
                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();

                    query = "UPDATE " + DBConstants.TABLE_RING_ROOM_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                                + " SET "
                                + DBConstants.PACKET_TYPE + " = " + _MessageDTO.PacketType + ", "
                                + DBConstants.MESSAGE_TYPE + " = " + _MessageDTO.MessageType + ", "
                                + DBConstants.MESSAGE + " = '" + ModelUtility.IsExceded(_MessageDTO.OriginalMessage, 32000) + "', "
                                + DBConstants.MESSAGE_DATE + " = " + _MessageDTO.MessageDate + ", "
                                + DBConstants.MESSAGE_DELIEVER_DATE + " = " + _MessageDTO.MessageDelieverDate + ", "
                                + DBConstants.MESSAGE_SEEN_DATE + " = " + _MessageDTO.MessageSeenDate + ", "
                                + DBConstants.MESSAGE_VIEW_DATE + " = " + _MessageDTO.MessageViewDate + ", "
                                + DBConstants.STATUS + " = " + _MessageDTO.Status + ", "
                                + DBConstants.FILE_ID + " = " + _MessageDTO.FileID + ", "
                                + DBConstants.FILE_STATUS + " = " + _MessageDTO.FileStatus + ", "
                                + DBConstants.PACKET_ID + " = '" + _MessageDTO.PacketID + "', "
                                + DBConstants.IS_UNREAD + " = " + ModelUtility.IsBoolean(false) + ", "
                                + DBConstants.FULL_NAME + " = '" + ModelUtility.IsExceded(_MessageDTO.FullName, 200) + "', "
                                + DBConstants.PROFILE_IMAGE + " = '" + ModelUtility.IsExceded(_MessageDTO.ProfileImage, 256) + "'"
                                 + " WHERE "
                                 + DBConstants.ROOM_ID + "='" + _MessageDTO.RoomID + "' AND "
                                 + DBConstants.SENDER_TABLE_ID + "=" + _MessageDTO.SenderTableID + " ";

                    using (SQLiteCommand commandUpdate = new SQLiteCommand(query, createDB.GetConnection()))
                    {
                        if (commandUpdate.ExecuteNonQuery() == 0)
                        {
                            query = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_ROOM_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " ( "
                                + DBConstants.ROOM_ID + ", "
                                + DBConstants.SENDER_TABLE_ID + ", "
                                + DBConstants.PACKET_TYPE + ", "
                                + DBConstants.MESSAGE_TYPE + ", "
                                + DBConstants.MESSAGE + ", "
                                + DBConstants.MESSAGE_DATE + ", "
                                + DBConstants.MESSAGE_DELIEVER_DATE + ", "
                                + DBConstants.MESSAGE_SEEN_DATE + ", "
                                + DBConstants.MESSAGE_VIEW_DATE + ", "
                                + DBConstants.STATUS + ", "
                                + DBConstants.FILE_ID + ", "
                                + DBConstants.FILE_STATUS + ", "
                                + DBConstants.PACKET_ID + ", "
                                + DBConstants.IS_UNREAD + ", "
                                + DBConstants.FULL_NAME + ", "
                                + DBConstants.PROFILE_IMAGE + ""
                                + " ) "
                                + " SELECT "
                                + "'" + ModelUtility.IsExceded(_MessageDTO.RoomID, 50) + "', "
                                + _MessageDTO.SenderTableID + ", "
                                + _MessageDTO.PacketType + ", "
                                + _MessageDTO.MessageType + ", "
                                + "'" + ModelUtility.IsExceded(_MessageDTO.OriginalMessage, 32000) + "', "
                                + _MessageDTO.MessageDate + ", "
                                + _MessageDTO.MessageDelieverDate + ", "
                                + _MessageDTO.MessageSeenDate + ", "
                                + _MessageDTO.MessageViewDate + ", "
                                + _MessageDTO.Status + ", "
                                + _MessageDTO.FileID + ", "
                                + _MessageDTO.FileStatus + ", "
                                + "'" + _MessageDTO.PacketID + "', "
                                + ModelUtility.IsBoolean(false) + ", "
                                + "'" + ModelUtility.IsExceded(_MessageDTO.FullName, 200) + "', "
                                + "'" + ModelUtility.IsExceded(_MessageDTO.ProfileImage, 256) + "' ";

                            using (SQLiteCommand commandInsert = new SQLiteCommand(query, createDB.GetConnection())) 
                            {
                                commandInsert.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in InsertOrUpdate() ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
        }


    }
}

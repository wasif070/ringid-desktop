﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading;

namespace Models.DAO
{
    public class ContactListDAO
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ContactListDAO).Name);

        public static UserBasicInfoDTO FetchContactListByFriendIdentity(long friendIdentity)
        {
            log.Info("Fetching from Contact List Table .............");

            UserBasicInfoDTO entity = null;
            String query = String.Empty;
            try
            {
                CreateDBandTables createDBandTables = new CreateDBandTables();
                createDBandTables.ConnectToDatabase();

                query = "SELECT "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.USER_TABLE_ID + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.RING_ID + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.FULL_NAME + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.FRIENDSHIP_STATUS + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.PROFILE_IMAGE + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.PROFILE_IMAGE_ID + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.COVER_IMAGE + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.COVER_IMAGE_ID + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.COVER_IMAGE_X + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.COVER_IMAGE_Y + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.PROFILE_IMAGE_PRIVACY + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.COVER_IMAGE_PRIVACY + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.DEVICE_TOKEN + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.UPDATE_TIME + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.MATCHED_BY + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.NUMBER_OF_CALLS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.NUMBER_OF_MUTUAL_FRIENDS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.NUMBER_OF_CHATS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.FAVORITE_RANK + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CALL_ACCESS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_ACCESS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.FEED_ACCESS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_MIN_PACKET_ID + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_MAX_PACKET_ID + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IS_SECRET_VISIBLE + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_BG_URL + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CONTACT_ADDED_TIME + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CONTACT_UT + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CONTACT_READ_UNREAD + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.INCOMING_STATUS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_SOUND + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_NOTIFICATION + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_POPUP + " "
                    + " FROM " + DBConstants.TABLE_RING_USER_BASIC_INFO + " "
                    + " LEFT JOIN " + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + " ON "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.USER_TABLE_ID + "=" + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.USER_TABLE_ID + " "
                    + " WHERE "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.USER_TABLE_ID + "=" + friendIdentity + "";

                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                SQLiteDataReader dr = command.ExecuteReader();
                if (dr.Read())
                {
                    entity = new UserBasicInfoDTO();
                    entity.UserTableID = (long)dr.GetValue(dr.GetOrdinal(DBConstants.USER_TABLE_ID));
                    entity.RingID = (long)dr.GetValue(dr.GetOrdinal(DBConstants.RING_ID));
                    entity.FullName = dr.GetString(dr.GetOrdinal(DBConstants.FULL_NAME));
                    entity.Presence = StatusConstants.PRESENCE_OFFLINE;
                    entity.FriendShipStatus = dr.GetInt32(dr.GetOrdinal(DBConstants.FRIENDSHIP_STATUS));
                    entity.ProfileImage = dr.GetString(dr.GetOrdinal(DBConstants.PROFILE_IMAGE));
                    entity.ProfileImageId = (Guid)dr.GetValue(dr.GetOrdinal(DBConstants.PROFILE_IMAGE_ID));
                    entity.CoverImage = dr.GetString(dr.GetOrdinal(DBConstants.COVER_IMAGE));
                    entity.CoverImageId = (Guid)dr.GetValue(dr.GetOrdinal(DBConstants.COVER_IMAGE_ID));
                    entity.CropImageX = dr.GetInt32(dr.GetOrdinal(DBConstants.COVER_IMAGE_X));
                    entity.CropImageY = dr.GetInt32(dr.GetOrdinal(DBConstants.COVER_IMAGE_Y));
                    entity.ProfileImagePrivacy = (short)dr.GetValue(dr.GetOrdinal(DBConstants.PROFILE_IMAGE_PRIVACY));
                    entity.CoverImagePrivacy = (short)dr.GetValue(dr.GetOrdinal(DBConstants.COVER_IMAGE_PRIVACY));
                    entity.DeviceToken = dr.GetString(dr.GetOrdinal(DBConstants.DEVICE_TOKEN));
                    entity.UpdateTime = (long)dr.GetValue(dr.GetOrdinal(DBConstants.UPDATE_TIME));
                    entity.MatchedBy = dr.GetInt32(dr.GetOrdinal(DBConstants.MATCHED_BY));
                    entity.NumberOfCalls = (long)dr.GetValue(dr.GetOrdinal(DBConstants.NUMBER_OF_CALLS));
                    entity.NumberOfMutualFriends = (long)dr.GetValue(dr.GetOrdinal(DBConstants.NUMBER_OF_MUTUAL_FRIENDS));
                    entity.NumberOfChats = (long)dr.GetValue(dr.GetOrdinal(DBConstants.NUMBER_OF_CHATS));
                    entity.FavoriteRank = dr.GetDouble(dr.GetOrdinal(DBConstants.FAVORITE_RANK));
                    entity.CallAccess = (short)dr.GetValue(dr.GetOrdinal(DBConstants.CALL_ACCESS));
                    entity.ChatAccess = (short)dr.GetValue(dr.GetOrdinal(DBConstants.CHAT_ACCESS));
                    entity.FeedAccess = (short)dr.GetValue(dr.GetOrdinal(DBConstants.FEED_ACCESS));
                    entity.ChatMinPacketID = dr.GetString(dr.GetOrdinal(DBConstants.CHAT_MIN_PACKET_ID));
                    entity.ChatMaxPacketID = dr.GetString(dr.GetOrdinal(DBConstants.CHAT_MAX_PACKET_ID));
                    entity.IsSecretVisible = dr.GetInt32(dr.GetOrdinal(DBConstants.IS_SECRET_VISIBLE));
                    entity.ChatBgUrl = (string)dr.GetValue(dr.GetOrdinal(DBConstants.CHAT_BG_URL));
                    entity.ContactAddedTime = (long)dr.GetValue(dr.GetOrdinal(DBConstants.CONTACT_ADDED_TIME));
                    entity.ContactUpdateTime = (long)dr.GetValue(dr.GetOrdinal(DBConstants.CONTACT_UT));
                    entity.ContactAddedReadUnread = (short)dr.GetValue(dr.GetOrdinal(DBConstants.CONTACT_READ_UNREAD));
                    entity.IncomingStatus = dr.GetInt32(dr.GetOrdinal(DBConstants.INCOMING_STATUS));
                    entity.ImNotificationEnabled = dr.GetBoolean(dr.GetOrdinal(DBConstants.IM_NOTIFICATION));
                    entity.ImPopupEnabled = dr.GetBoolean(dr.GetOrdinal(DBConstants.IM_POPUP));
                    entity.ImSoundEnabled = dr.GetBoolean(dr.GetOrdinal(DBConstants.IM_SOUND));
                }
            }
            catch (Exception ex)
            {
                log.Error("Error occured during fetching Contract List from DB. Exception ::  ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }

            return entity;
        }

        public static List<UserBasicInfoDTO> FetchBlockedContactList()
        {
            log.Info("Fetching Blocked Contacts from Contact List Table .............");

            List<UserBasicInfoDTO> listBlockedContacts = new List<UserBasicInfoDTO>();
            String query = String.Empty;
            try
            {
                CreateDBandTables createDBandTables = new CreateDBandTables();
                createDBandTables.ConnectToDatabase();

                query = "SELECT "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.USER_TABLE_ID + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.RING_ID + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.FULL_NAME + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.FRIENDSHIP_STATUS + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.PROFILE_IMAGE + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.PROFILE_IMAGE_ID + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.COVER_IMAGE + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.COVER_IMAGE_ID + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.COVER_IMAGE_X + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.COVER_IMAGE_Y + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.PROFILE_IMAGE_PRIVACY + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.COVER_IMAGE_PRIVACY + ", "
                    + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.DEVICE_TOKEN + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.UPDATE_TIME + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.MATCHED_BY + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.NUMBER_OF_CALLS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.NUMBER_OF_MUTUAL_FRIENDS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.NUMBER_OF_CHATS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.FAVORITE_RANK + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CALL_ACCESS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_ACCESS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.FEED_ACCESS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_MIN_PACKET_ID + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_MAX_PACKET_ID + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IS_SECRET_VISIBLE + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_BG_URL + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CONTACT_ADDED_TIME + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CONTACT_UT + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CONTACT_READ_UNREAD + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.INCOMING_STATUS + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_SOUND + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_NOTIFICATION + ", "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_POPUP + " "
                    + " FROM " + DBConstants.TABLE_RING_USER_BASIC_INFO + " "
                    + " LEFT JOIN " + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + " ON "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.USER_TABLE_ID + "=" + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.USER_TABLE_ID + " "
                    + " LEFT JOIN " + DBConstants.TABLE_RING_BLOCKED_NON_FRIEND + " ON "
                    + DBConstants.TABLE_RING_BLOCKED_NON_FRIEND + "." + DBConstants.USER_TABLE_ID + "=" + DBConstants.TABLE_RING_USER_BASIC_INFO + "." + DBConstants.USER_TABLE_ID + " "
                    + " WHERE "
                    + "(("
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.FRIENDSHIP_STATUS + "=" + StatusConstants.FRIENDSHIP_STATUS_ACCEPTED + " AND "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CALL_ACCESS + "=" + StatusConstants.TYPE_ACCESS_BLOCKED + " AND "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_ACCESS + "=" + StatusConstants.TYPE_ACCESS_BLOCKED + " AND "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.FEED_ACCESS + "=" + StatusConstants.TYPE_ACCESS_BLOCKED
                    + ") OR ( "
                    + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.FRIENDSHIP_STATUS + "<>" + StatusConstants.FRIENDSHIP_STATUS_ACCEPTED + " AND "
                    + DBConstants.TABLE_RING_BLOCKED_NON_FRIEND + "." + DBConstants.IS_BLOCKED_BY_ME + "=" + ModelUtility.IsBoolean(true)
                    + "))";

                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                SQLiteDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    UserBasicInfoDTO entity = new UserBasicInfoDTO();
                    entity.UserTableID = (long)dr.GetValue(dr.GetOrdinal(DBConstants.USER_TABLE_ID));
                    entity.RingID = (long)dr.GetValue(dr.GetOrdinal(DBConstants.RING_ID));
                    entity.FullName = dr.GetString(dr.GetOrdinal(DBConstants.FULL_NAME));
                    entity.Presence = StatusConstants.PRESENCE_OFFLINE;
                    entity.FriendShipStatus = dr.GetInt32(dr.GetOrdinal(DBConstants.FRIENDSHIP_STATUS));
                    entity.ProfileImage = dr.GetString(dr.GetOrdinal(DBConstants.PROFILE_IMAGE));
                    entity.ProfileImageId = (Guid)dr.GetValue(dr.GetOrdinal(DBConstants.PROFILE_IMAGE_ID));
                    entity.CoverImage = dr.GetString(dr.GetOrdinal(DBConstants.COVER_IMAGE));
                    entity.CoverImageId = (Guid)dr.GetValue(dr.GetOrdinal(DBConstants.COVER_IMAGE_ID));
                    entity.CropImageX = dr.GetInt32(dr.GetOrdinal(DBConstants.COVER_IMAGE_X));
                    entity.CropImageY = dr.GetInt32(dr.GetOrdinal(DBConstants.COVER_IMAGE_Y));
                    entity.ProfileImagePrivacy = (short)dr.GetValue(dr.GetOrdinal(DBConstants.PROFILE_IMAGE_PRIVACY));
                    entity.CoverImagePrivacy = (short)dr.GetValue(dr.GetOrdinal(DBConstants.COVER_IMAGE_PRIVACY));
                    entity.DeviceToken = dr.GetString(dr.GetOrdinal(DBConstants.DEVICE_TOKEN));
                    entity.UpdateTime = (long)dr.GetValue(dr.GetOrdinal(DBConstants.UPDATE_TIME));
                    entity.MatchedBy = dr.GetInt32(dr.GetOrdinal(DBConstants.MATCHED_BY));
                    entity.NumberOfCalls = (long)dr.GetValue(dr.GetOrdinal(DBConstants.NUMBER_OF_CALLS));
                    entity.NumberOfMutualFriends = (long)dr.GetValue(dr.GetOrdinal(DBConstants.NUMBER_OF_MUTUAL_FRIENDS));
                    entity.NumberOfChats = (long)dr.GetValue(dr.GetOrdinal(DBConstants.NUMBER_OF_CHATS));
                    entity.FavoriteRank = dr.GetDouble(dr.GetOrdinal(DBConstants.FAVORITE_RANK));
                    entity.CallAccess = (short)dr.GetValue(dr.GetOrdinal(DBConstants.CALL_ACCESS));
                    entity.ChatAccess = (short)dr.GetValue(dr.GetOrdinal(DBConstants.CHAT_ACCESS));
                    entity.FeedAccess = (short)dr.GetValue(dr.GetOrdinal(DBConstants.FEED_ACCESS));
                    entity.ChatMinPacketID = dr.GetString(dr.GetOrdinal(DBConstants.CHAT_MIN_PACKET_ID));
                    entity.ChatMaxPacketID = dr.GetString(dr.GetOrdinal(DBConstants.CHAT_MAX_PACKET_ID));
                    entity.IsSecretVisible = dr.GetInt32(dr.GetOrdinal(DBConstants.IS_SECRET_VISIBLE));
                    entity.ChatBgUrl = (string)dr.GetValue(dr.GetOrdinal(DBConstants.CHAT_BG_URL));
                    entity.ContactAddedTime = (long)dr.GetValue(dr.GetOrdinal(DBConstants.CONTACT_ADDED_TIME));
                    entity.ContactUpdateTime = (long)dr.GetValue(dr.GetOrdinal(DBConstants.CONTACT_UT));
                    entity.ContactAddedReadUnread = (short)dr.GetValue(dr.GetOrdinal(DBConstants.CONTACT_READ_UNREAD));
                    entity.IncomingStatus = dr.GetInt32(dr.GetOrdinal(DBConstants.INCOMING_STATUS));
                    entity.ImNotificationEnabled = dr.GetBoolean(dr.GetOrdinal(DBConstants.IM_NOTIFICATION));
                    entity.ImPopupEnabled = dr.GetBoolean(dr.GetOrdinal(DBConstants.IM_POPUP));
                    entity.ImSoundEnabled = dr.GetBoolean(dr.GetOrdinal(DBConstants.IM_SOUND));
                    listBlockedContacts.Add(entity);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error occured during fetching Blocked List from DB. Exception ::  ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }

            return listBlockedContacts;
        }

        public static void UpdateChatBackground(long friendIdentity, string chatBgUrl)
        {
            new Thread(() =>
            {
                string query = String.Empty;

                try
                {
                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();

                    query = "UPDATE " + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " SET "
                                + DBConstants.CHAT_BG_URL + "='" + ModelUtility.IsNull(chatBgUrl) + "' "
                                + " WHERE "
                                + DBConstants.USER_TABLE_ID + "=" + friendIdentity + " ";

                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in UpdateChatBackground() ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

        public static void UpdateChatHistoryTime(long friendIdentity, string chatMinPacketID, string chatMaxPacketID)
        {
            string query = String.Empty;

            try
            {
                CreateDBandTables createDB = new CreateDBandTables();
                createDB.ConnectToDatabase();

                query = "UPDATE " + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                        + " SET "
                        + DBConstants.CHAT_MIN_PACKET_ID + "='" + chatMinPacketID + "', "
                        + DBConstants.CHAT_MAX_PACKET_ID + "='" + chatMaxPacketID + "' "
                        + " WHERE "
                        + DBConstants.USER_TABLE_ID + "=" + friendIdentity + " ";


                createDB.ExecuteDBQurey(query);
            }
            catch (Exception ex)
            {
                log.Error("SQLException in UpdateChatHistoryTime() ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
        }

        public static void UpdateHideSecretTimer(long friendIdentity, int hideSecretTimer)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();

                    query = "UPDATE " + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                            + " SET "
                            + DBConstants.IS_SECRET_VISIBLE + "=" + hideSecretTimer + " "
                            + " WHERE "
                            + DBConstants.USER_TABLE_ID + "=" + friendIdentity + " ";

                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in UpdateHideSecretTimer() ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }


        public static void UpdateContactRanking(UserBasicInfoDTO basicInfoDTO)
        {
            new Thread(() =>
            {
                UpdateContactRankingWithoutThread(basicInfoDTO);
            }).Start();
        }

        public static void UpdateContactRankingWithoutThread(UserBasicInfoDTO basicInfoDTO)
        {
            string insertUpdateQuery = String.Empty;
            string query = String.Empty;
            try
            {
                CreateDBandTables createDB = new CreateDBandTables();
                createDB.ConnectToDatabase();

                query = "UPDATE " + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                            + " SET "
                            + DBConstants.NUMBER_OF_CALLS + "=" + basicInfoDTO.NumberOfCalls + ","
                            + DBConstants.NUMBER_OF_CHATS + "=" + basicInfoDTO.NumberOfChats + ","
                            + DBConstants.FAVORITE_RANK + "=" + basicInfoDTO.FavoriteRank + ""
                            + " WHERE "
                            + DBConstants.USER_TABLE_ID + "=" + basicInfoDTO.UserTableID + " ";
                createDB.ExecuteDBQurey(query);
            }
            catch (Exception ex)
            {
                log.Error("SQLException in UpdateFriendRanking(UserBasicInfoDTO basicInfoDTO) ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
        }

        public static void UpdateIMNotification(long friendIdentity, bool isEnable)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " SET "
                                + DBConstants.IM_NOTIFICATION + "=" + ModelUtility.IsBoolean(isEnable) + " "
                                + " WHERE "
                                + DBConstants.USER_TABLE_ID + "=" + friendIdentity + "";

                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();
                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in UpdateIMNotification() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

        public static void UpdateIMSound(long friendIdentity, bool isEnable)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " SET "
                                + DBConstants.IM_SOUND + "=" + ModelUtility.IsBoolean(isEnable) + " "
                                + " WHERE "
                                + DBConstants.USER_TABLE_ID + "=" + friendIdentity + "";

                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();
                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in UpdateIMSound() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

        public static void UpdateIMPopUp(long tableID, bool isEnable)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " SET "
                                + DBConstants.IM_POPUP + "=" + ModelUtility.IsBoolean(isEnable) + " "
                                + " WHERE "
                                + DBConstants.USER_TABLE_ID + "=" + tableID + "";

                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();
                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in UpdateIMPopUp() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

    }
}

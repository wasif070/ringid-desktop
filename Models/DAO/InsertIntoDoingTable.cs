﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading;

namespace Models.DAO
{
    public class InsertIntoDoingTable
    {
        readonly ILog log = LogManager.GetLogger(typeof(InsertIntoDoingTable).Name);
        private List<DoingDTO> doingDtos;
        public InsertIntoDoingTable(List<DoingDTO> doingDtos)
        {
            this.doingDtos = doingDtos;
            Thread thread = new Thread(new ThreadStart(Run));
            thread.Name = this.GetType().Name;
            thread.Start();
        }

        private void Run()
        {
            string query = String.Empty;
            try
            {
                CreateDBandTables createDBandTables = new CreateDBandTables();
                createDBandTables.ConnectToDatabase();
                foreach (DoingDTO dto in doingDtos)
                {
                    query = "SELECT COUNT(*) FROM  " + DBConstants.TABLE_RING_DOING + " WHERE "
                                + DBConstants.ID + "=" + dto.ID;
                    SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                    int RowCount = Convert.ToInt32(command.ExecuteScalar());
                    if (RowCount < 1)
                    {
                        query = "INSERT INTO " + DBConstants.TABLE_RING_DOING
                            + " (" + DBConstants.ID + "," + DBConstants.DOING_CATEGORY + "," + DBConstants.DOING_SORTID + "," + DBConstants.DOING_NAME + "," + DBConstants.DOING_URL + "," + DBConstants.DOING_UPDATE_TIME + ")"
                            + "VALUES"
                            + "("
                            + dto.ID + ","
                            + dto.Category + ","
                            + dto.SortId + ","
                            + "'" + dto.Name + "',"
                            + "'" + dto.ImgUrl + "',"
                            + dto.UpdateTime + ""
                            + ")";
                        createDBandTables.ExecuteDBQurey(query);
                    }
                    else
                    {
                        query = "UPDATE " + DBConstants.TABLE_RING_DOING
                                + " SET "
                                + DBConstants.DOING_CATEGORY + "=" + dto.Category + ","
                                + DBConstants.DOING_SORTID + "=" + dto.SortId + ","
                                + DBConstants.DOING_NAME + "='" + dto.Name + "',"
                                + DBConstants.DOING_URL + "='" + dto.ImgUrl + "',"
                                + DBConstants.DOING_UPDATE_TIME + "=" + dto.UpdateTime
                                + " WHERE " + DBConstants.ID + "=" + dto.ID + "";
                        createDBandTables.ExecuteDBQurey(query);
                    }
                }
                // new InsertIntoRingUserSettings();
            }
            catch (Exception e)
            {
                log.Error("SQLException in InsertIntoDoingTable class ==> " + e.Message + "\n" + e.StackTrace + ", Query = " + query);
            }
        }
    }
}

﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Models.DAO
{
    public class DeleteFromContactListTable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(DeleteFromContactListTable).Name);

        private long _UserID;
        public DeleteFromContactListTable(long userID)
        {
            this._UserID = userID;
            Thread thread = new Thread(new ThreadStart(Run));
            thread.Name = this.GetType().Name;
            thread.Start();
        }
        public void Run()
        {
            String query = String.Empty;
            String query1 = String.Empty;
            String query2 = String.Empty;

            try
            {
                CreateDBandTables createDBandTables = new CreateDBandTables();
                createDBandTables.ConnectToDatabase();
                try
                {
                    query = "DELETE FROM  " + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID
                                    + " WHERE "
                                    + DBConstants.USER_TABLE_ID + "=" + _UserID;
                    createDBandTables.ExecuteDBQurey(query);
                }
                catch (Exception e) { log.Error("SQLException in DeleteFromContactListTable class ==> " + e.Message + "\n" + e.StackTrace + ", Query = " + query); }
            }
            catch (Exception e)
            {
                log.Error("SQLException in DeleteFromContactListTable class ==> " + e.Message + "\n" + e.StackTrace);
            }
        }
    }
}

﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Data.SQLite;

namespace Models.DAO
{
    public class InstantMessagesDAO
    {
        readonly ILog log = LogManager.GetLogger(typeof(InstantMessagesDAO).Name);

        private CreateDBandTables createDBandTables;
        public InstantMessagesDAO()
        {
            createDBandTables = new CreateDBandTables();
            createDBandTables.ConnectToDatabase();
        }
        private static InstantMessagesDAO _Instance;
        public static InstantMessagesDAO Instance
        {
            get { return _Instance = _Instance ?? new InstantMessagesDAO(); }
            set { _Instance = value; }
        }

        public IList<InstantMessageDTO> GetInstantMessagesFromDB()
        {
            String query = String.Empty;
            IList<InstantMessageDTO> instantMessages = new List<InstantMessageDTO>();
            try
            {
                query = "SELECT * FROM  " + DBConstants.TABLE_RING_INSTANT_MESSAGES;
                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                SQLiteDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    InstantMessageDTO instantMessageDTO = new InstantMessageDTO();
                    instantMessageDTO.InstantMessage = dr.GetString(dr.GetOrdinal(DBConstants.INS_MSG));
                    instantMessageDTO.MsgType = dr.GetInt32(dr.GetOrdinal(DBConstants.INS_MSG_TYPE));
                    instantMessages.Add(instantMessageDTO);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error occured during fetching GetInstantMessagesFromDB from DB. Exception :: " + ex.Message + "\n" + ex.StackTrace + ", Query" + query);
            }
            return instantMessages;
        }

        public void DeleteInstantMessagesFromDB(IList<InstantMessageDTO> insMsgs)
        {
            String query = String.Empty;
            try
            {
                foreach (InstantMessageDTO ins in insMsgs)
                {
                    query = "DELETE FROM  " + DBConstants.TABLE_RING_INSTANT_MESSAGES + " WHERE " + DBConstants.INS_MSG + "='" + ins.InstantMessage
                            + "' AND " + DBConstants.INS_MSG_TYPE + "=" + ins.MsgType + "";
                    createDBandTables.ExecuteDBQurey(query);
                }
            }
            catch (Exception ex)
            {
                log.Error("Error occured during DeleteInstantMessagesFromDB from DB. Exception :: " + ex.Message + "\n" + ex.StackTrace + ", Query" + query);
            }
        }
        public void DeleteSingleInstantMessageFromDB(InstantMessageDTO ins)
        {
            String query = String.Empty;
            try
            {
                query = "DELETE FROM  " + DBConstants.TABLE_RING_INSTANT_MESSAGES + " WHERE " + DBConstants.INS_MSG + "='" + ins.InstantMessage
                        + "' AND " + DBConstants.INS_MSG_TYPE + "=" + ins.MsgType + "";
                createDBandTables.ExecuteDBQurey(query);
            }
            catch (Exception ex)
            {
                log.Error("Error occured during DeleteSingleInstantMessageFromDB from DB. Exception :: " + ex.Message + "\n" + ex.StackTrace + ", Query" + query);
            }
        }

        public void InsertSingleInstantMessagetoDB(InstantMessageDTO insMsg)
        {
            String query = String.Empty;
            try
            {
                query = "INSERT INTO " + DBConstants.TABLE_RING_INSTANT_MESSAGES
                                + " (" + DBConstants.INS_MSG + "," + DBConstants.INS_MSG_TYPE + ")"
                                + " VALUES "
                                + " ("
                                + "'" + insMsg.InstantMessage + "',"
                                + "" + insMsg.MsgType + ""
                                + ")";
                createDBandTables.ExecuteDBQurey(query);
            }
            catch (Exception ex)
            {
                log.Error("Error occured during InsertSingleInstantMessagetoDB from DB. Exception :: " + ex.Message + "\n" + ex.StackTrace + ", Query" + query);
            }
        }
    }
}

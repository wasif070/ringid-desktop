﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Models.DAO
{
    public class InsertIntoRoomListTable
    {
        private readonly ILog log = LogManager.GetLogger(typeof(InsertIntoRoomListTable).Name);

        Thread t = null;
        public delegate void CompleteHandler();
        public event CompleteHandler OnComplete;

        public InsertIntoRoomListTable(List<RoomDTO> roomDTOs)
        {
            t = new Thread(param => InsertOrUpdate(roomDTOs));
            t.Name = this.GetType().Name;
        }

        public void Start()
        {
            if (t != null)
            {
                t.Start();
            }
        }

        private void InsertOrUpdate(List<RoomDTO> roomDTOs)
        {
            string insertUpdateQuery = String.Empty;
            string deleteQuery = String.Empty;
            string updateQuery = String.Empty;
            string query = String.Empty;
            try
            {
                if (roomDTOs != null && roomDTOs.Count > 0)
                {
                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();

                    insertUpdateQuery = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " ( "
                                + DBConstants.ROOM_ID + ", "
                                + DBConstants.ROOM_NAME + ", "
                                + DBConstants.ROOM_IMAGE + ", "
                                + DBConstants.NUMBER_OF_MEMBERS + ", "
                                + DBConstants.CHAT_MIN_PACKET_ID + ", "
                                + DBConstants.CHAT_MAX_PACKET_ID + ", "
                                + DBConstants.CHAT_BG_URL + ", "
                                + DBConstants.IS_PARTIAL + ", "
                                + DBConstants.IM_SOUND + ", "
                                + DBConstants.IM_NOTIFICATION + ", "
                                + DBConstants.IM_POPUP + " "
                                + " ) ";

                    deleteQuery = "DELETE FROM " + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " WHERE ";

                    int prevTransType = 1;

                    roomDTOs.ForEach(i =>
                        {
                            if (i.IntegerStatus == StatusConstants.STATUS_DELETED)
                            {
                                if (prevTransType == 0)
                                {
                                    query = query.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' }) + "; ";
                                }
                                query += deleteQuery
                                    + DBConstants.ROOM_ID + "='" + i.RoomID + "'; ";
                                prevTransType = 1;
                            }
                            else
                            {
                                if (prevTransType == 1)
                                {
                                    query += insertUpdateQuery;
                                }

                                query += "SELECT "
                                + "'" + ModelUtility.IsNull(i.RoomID) + "', "
                                + "'" + ModelUtility.IsExceded(i.RoomName, 200) + "', "
                                + "'" + ModelUtility.IsExceded(i.RoomProfileImage, 256) + "', "
                                + "" + i.NumberOfMembers + ", "
                                + "'" + ModelUtility.IsNull(i.ChatMinPacketID) + "', "
                                + "'" + ModelUtility.IsNull(i.ChatMaxPacketID) + "', "
                                + "'" + ModelUtility.IsNull(i.ChatBgUrl) + "', "
                                + ModelUtility.IsBoolean(i.IsPartial) + ", "
                                + ModelUtility.IsBoolean(i.ImSoundEnabled) + ", "
                                + ModelUtility.IsBoolean(i.ImNotificationEnabled) + ", "
                                + ModelUtility.IsBoolean(i.ImPopupEnabled) + " "
                                + " UNION ";
                                prevTransType = 0;
                            }
                        }
                    );

                    if (prevTransType == 0)
                    {
                        query = query.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' }) + "; ";
                    }

                    createDB.ExecuteDBQurey(query);
                    //createDB.ExecuteDBQurey("BEGIN; " + query + " END;");
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in InsertOrUpdate() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Room Query = " + query);
            }

            if (OnComplete != null)
            {
                OnComplete();
            }
        }


    }
}

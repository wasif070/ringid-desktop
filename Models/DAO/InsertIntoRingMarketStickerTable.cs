﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Models.DAO
{
    public class InsertIntoRingMarketStickerTable
    {
        private readonly ILog log = LogManager.GetLogger(typeof(InsertIntoRingMarketStickerTable).Name);

        Thread t = null;


        public InsertIntoRingMarketStickerTable(List<StickerCollectionDTO> collectionDTOs)
        {
            t = new Thread(param => InsertOrUpdate(collectionDTOs));
            t.Name = this.GetType().Name;
            t.Start();
        }

        public InsertIntoRingMarketStickerTable(List<MarketStickerCategoryDTO> categoryDTOs)
        {
            t = new Thread(param => InsertOrUpdate(categoryDTOs));
            t.Name = this.GetType().Name;
            t.Start();
        }

        public InsertIntoRingMarketStickerTable(List<MarkertStickerImagesDTO> imagesDTOs)
        {
            t = new Thread(param => InsertOrUpdate(imagesDTOs));
            t.Name = this.GetType().Name;
            t.Start();
        }        

        private void InsertOrUpdate(List<StickerCollectionDTO> collectionsDTOs)
        {
            string query = String.Empty;
            int queryCounter = 0;
            try
            {
                if (collectionsDTOs != null && collectionsDTOs.Count > 0)
                {
                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();

                    string initQuery = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_MARKET_STICKER_COLLECTION
                                    + " ( "
                                    + DBConstants.COLLECTION_ID + ", "
                                    + DBConstants.LOGIN_TABLE_ID + ", "
                                    + DBConstants.COLLECTION_NAME + ", "
                                    + DBConstants.BANNER_IMAGE + ", "
                                    + DBConstants.THEME_COLOR + " "
                                    + " ) ";

                    collectionsDTOs.ForEach(i =>
                    {
                        if (queryCounter == 0)
                        {
                            query += initQuery;
                        }
                        queryCounter++;

                        query += "SELECT "
                            + i.SClId + ", "
                            + "" + DefaultSettings.LOGIN_TABLE_ID + ", "
                            + "'" + ModelUtility.IsExceded(i.Name, 150) + "', "
                            + "'" + ModelUtility.IsNull(i.BnrImg) + "', "
                            + "'" + ModelUtility.IsNull(i.ThmColr) + "' "
                            + " UNION ";

                        if (queryCounter == 500)
                        {
                            query = query.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' }) + "; ";
                            queryCounter = 0;
                        }
                    }
                    );

                    if (queryCounter > 0)
                    {
                        query = query.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' });
                    }
                    createDB.ExecuteDBQurey(query);
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in InsertOrUpdate(StickerCollectionDTO) class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
        }

        private void InsertOrUpdate(List<MarketStickerCategoryDTO> categoryDTOs)
        {
            string query = String.Empty;
            int queryCounter = 0;

            try
            {
                if (categoryDTOs != null && categoryDTOs.Count > 0)
                {
                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();

                    string initQuery = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_MARKET_STICKER_CATEGORY
                                + " ( "
                                + DBConstants.CATEGORY_ID + ", "
                                + DBConstants.LOGIN_TABLE_ID + ", "
                                + DBConstants.CATEGORY_NAME + ", "
                                + DBConstants.COLLECTION_ID + ", "
                                + DBConstants.BANNER_IMAGE + ", "
                                + DBConstants.IS_BANNER_VISIBLE + ", "
                                + DBConstants.RANK + ", "
                                + DBConstants.DETAIL_IMAGE + ", "
                                + DBConstants.IS_NEW + ", "
                                + DBConstants.IS_TOP + ", "
                                + DBConstants.ICON + ", "
                                + DBConstants.PRIZE + ", "
                                + DBConstants.IS_FREE + ", "
                                + DBConstants.DESCRIPTION + ", "
                                + DBConstants.IS_DOWNLOADED + ", "
                                + DBConstants.DOWNLOAD_TIME + ", "
                                + DBConstants.SORT_ORDER + ", "
                                + DBConstants.IS_VISIBLE + ", "
                                + DBConstants.IS_DEFAULT + ", "
                                + DBConstants.LAST_USED_DATE + ", "
                                + DBConstants.NEW_STICKER_IS_SEEN + " "
                                + " ) ";

                    categoryDTOs.ForEach(i =>
                        {
                            if (queryCounter == 0)
                            {
                                query += initQuery;
                            }
                            queryCounter++;

                            query += "SELECT "
                                + i.sCtId + ", "
                                + "" + DefaultSettings.LOGIN_TABLE_ID + ", "
                                + "'" + ModelUtility.IsNull(i.sctName) + "', "
                                + i.sClId + ", "
                                + "'" + ModelUtility.IsNull(i.cgBnrImg) + "', "
                                + ModelUtility.IsBoolean(i.IsBannerVisible) + ", "
                                + i.Rnk + ", "
                                + "'" + ModelUtility.IsNull(i.DetailImg) + "', "
                                + ModelUtility.IsBoolean(i.cgNw) + ", "
                                + ModelUtility.IsBoolean(i.IsTop) + ", "
                                + "'" + ModelUtility.IsNull(i.Icon) + "', "
                                + i.Prz + ", "
                                + ModelUtility.IsBoolean(i.free) + ", "
                                + "'" + ModelUtility.IsNull(i.Dcpn) + "', "
                                + ModelUtility.IsBoolean(i.Downloaded) + ", "
                                + i.DownloadTime + ", "
                                + i.SortOrder + ", "
                                + ModelUtility.IsBoolean(i.IsVisible) + ", "
                                + ModelUtility.IsBoolean(i.IsDefault) + ", "
                                + i.LastUsedDate + ", "
                                + ModelUtility.IsBoolean(i.IsNewStickerSeen) + " "
                                + " UNION ";

                            if (queryCounter == 500)
                            {
                                query = query.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' }) + "; ";
                                queryCounter = 0;
                            }
                        }
                    );

                    if (queryCounter > 0)
                    {
                        query = query.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' }) + "; ";
                    }

                    createDB.ExecuteDBQurey(query);
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in InsertOrUpdate(StickerCategoryDTO) class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
        }

        private void InsertOrUpdate(List<MarkertStickerImagesDTO> imagesDTOs)
        {
            string query = String.Empty;
            int queryCounter = 0;
            try
            {
                if (imagesDTOs != null && imagesDTOs.Count > 0)
                {
                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();

                    string initQuery = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_MARKET_STICKER
                                    + " ( "
                                    + DBConstants.IMAGE_ID + ", "
                                    + DBConstants.LOGIN_TABLE_ID + ", "
                                    + DBConstants.COLLECTION_ID + ", "
                                    + DBConstants.CATEGORY_ID + ", "
                                    + DBConstants.IMAGE_URL + ", "
                                    + DBConstants.IS_RECENT + " "
                                    + " ) ";

                    imagesDTOs.ForEach(i =>
                    {
                        if (queryCounter == 0)
                        {
                            query += initQuery;
                        }
                        queryCounter++;
                        
                        query += "SELECT "
                            + i.imId + ", "
                            + "" + DefaultSettings.LOGIN_TABLE_ID + ", "
                            + i.sClId + ", "
                            + i.sCtId + ", "
                            + "'" + ModelUtility.IsNull(i.imUrl) + "', "
                            + ModelUtility.IsBoolean(i.IsRecent)
                            + " UNION ";

                        if (queryCounter == 500)
                        {
                            query = query.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' }) + "; ";
                            queryCounter = 0;
                        }
                    });

                    if (queryCounter > 0)
                    {
                        query = query.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' });
                    }
                    createDB.ExecuteDBQurey(query);
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in InsertOrUpdate(StickerImagesDTO) class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
        }

    }
}

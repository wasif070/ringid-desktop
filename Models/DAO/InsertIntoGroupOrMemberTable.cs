﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Models.DAO
{
    public class InsertIntoGroupOrMemberTable
    {
        private readonly ILog log = LogManager.GetLogger(typeof(InsertIntoGroupOrMemberTable).Name);

        Thread t = null;
        public delegate void CompleteHandler();
        public event CompleteHandler OnComplete;

        public InsertIntoGroupOrMemberTable(List<GroupInfoDTO> groupInfoDTOs)
        {
            t = new Thread(param => InsertOrUpdate(groupInfoDTOs, null));
            t.Name = this.GetType().Name;
        }

        public InsertIntoGroupOrMemberTable(List<GroupMemberInfoDTO> groupMemberInfoDTOs)
        {
            t = new Thread(param => InsertOrUpdate(null, groupMemberInfoDTOs));
            t.Name = this.GetType().Name;
        }

        public InsertIntoGroupOrMemberTable(List<GroupInfoDTO> groupInfoDTOs, List<GroupMemberInfoDTO> groupMemberInfoDTOs)
        {
            t = new Thread(param => InsertOrUpdate(groupInfoDTOs, groupMemberInfoDTOs));
            t.Name = this.GetType().Name;
        }

        public void Start()
        {
            if (t != null)
            {
                t.Start();
            }
        }

        private void InsertOrUpdate(List<GroupInfoDTO> groupInfoDTOs, List<GroupMemberInfoDTO> memberInfoDTOs)
        {
            string insertUpdateQuery = String.Empty;
            string deleteQuery = String.Empty;
            string updateQuery = String.Empty;
            string groupQuery = String.Empty;
            string memberQuery = String.Empty;
            try
            {
                if ((groupInfoDTOs != null && groupInfoDTOs.Count > 0) || (memberInfoDTOs != null && memberInfoDTOs.Count > 0))
                {
                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();

                    if (groupInfoDTOs != null && groupInfoDTOs.Count > 0)
                    {
                        insertUpdateQuery = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                    + " ( "
                                    + DBConstants.GROUP_ID + ", "
                                    + DBConstants.GROUP_NAME + ", "
                                    + DBConstants.GROUP_PROFILE_IMAGE + ", "
                                    + DBConstants.NUMBER_OF_MEMBERS + ", "
                                    + DBConstants.CHAT_MIN_PACKET_ID + ", "
                                    + DBConstants.CHAT_MAX_PACKET_ID + ", "
                                    + DBConstants.CHAT_BG_URL + ", "
                                    + DBConstants.IS_PARTIAL + ", "
                                    + DBConstants.IM_SOUND + ", "
                                    + DBConstants.IM_NOTIFICATION + ", "
                                    + DBConstants.IM_POPUP + " "
                                    + " ) ";

                        deleteQuery = "DELETE FROM " + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                    + " WHERE ";

                        int prevTransType = 1;

                        groupInfoDTOs.ForEach(i =>
                        {
                            if (i.IntegerStatus == StatusConstants.STATUS_DELETED)
                            {
                                if (prevTransType == 0)
                                {
                                    groupQuery = groupQuery.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' }) + "; ";
                                }
                                groupQuery += deleteQuery
                                    + DBConstants.PACKET_ID + "='" + i.GroupID + "'; ";
                                prevTransType = 1;
                            }
                            else
                            {
                                if (prevTransType == 1)
                                {
                                    groupQuery += insertUpdateQuery;
                                }

                                groupQuery += "SELECT "
                                + i.GroupID + ", "
                                + "'" + ModelUtility.IsExceded(i.GroupName, 500) + "', "
                                + "'" + ModelUtility.IsNull(i.GroupProfileImage) + "', "
                                + i.NumberOfMembers + ", "
                                + "'" + ModelUtility.IsNull(i.ChatMinPacketID) + "', "
                                + "'" + ModelUtility.IsNull(i.ChatMaxPacketID) + "', "
                                + "'" + ModelUtility.IsNull(i.ChatBgUrl) + "', "
                                + ModelUtility.IsBoolean(i.IsPartial) + ", "
                                + ModelUtility.IsBoolean(i.ImSoundEnabled) + ", "
                                + ModelUtility.IsBoolean(i.ImNotificationEnabled) + ", "
                                + ModelUtility.IsBoolean(i.ImPopupEnabled) + " "
                                + " UNION ";
                                prevTransType = 0;
                            }
                        }
                        );

                        if (prevTransType == 0)
                        {
                            groupQuery = groupQuery.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' }) + "; ";
                        }
                    }

                    if (memberInfoDTOs != null && memberInfoDTOs.Count > 0)
                    {
                        insertUpdateQuery = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_GROUP_MEMBER_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                    + " ( "
                                    + DBConstants.GROUP_ID + ", "
                                    + DBConstants.USER_TABLE_ID + ", "
                                    + DBConstants.RING_ID + ", "
                                    + DBConstants.FULL_NAME + ", "
                                    + DBConstants.MEMBER_ACCESS_TYPE + ", "
                                    + DBConstants.MEMBER_ADDED_BY + " "
                                    + " ) ";

                        updateQuery = "UPDATE " + DBConstants.TABLE_RING_GROUP_MEMBER_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                    + " SET ";

                        //deleteQuery = "DELETE FROM " + DBConstants.TABLE_RING_GROUP_MEMBER_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                        //            + " WHERE ";

                        int prevTransType = 1;

                        memberInfoDTOs.ForEach(i =>
                        {
                            if (i.IntegerStatus == StatusConstants.STATUS_UPDATED)
                            {
                                if (prevTransType == 0)
                                {
                                    memberQuery = memberQuery.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' }) + "; ";
                                }
                                memberQuery += updateQuery
                                    + DBConstants.MEMBER_ACCESS_TYPE + "=" + i.MemberAccessType + " "
                                    + " WHERE "
                                    + DBConstants.USER_TABLE_ID + "=" + i.UserTableID + " AND "
                                    + DBConstants.GROUP_ID + "='" + i.GroupID + "'; ";
                                prevTransType = 1;
                            }
                            else if (i.IntegerStatus == StatusConstants.STATUS_DELETED)
                            {
                                if (prevTransType == 0)
                                {
                                    memberQuery = memberQuery.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' }) + "; ";
                                }
                                memberQuery += updateQuery
                                    + DBConstants.MEMBER_ACCESS_TYPE + "=" + ChatConstants.MEMBER_TYPE_NOT_MEMBER + " "
                                    + " WHERE "
                                    + DBConstants.USER_TABLE_ID + "=" + i.UserTableID + " AND "
                                    + DBConstants.GROUP_ID + "='" + i.GroupID + "'; ";
                                prevTransType = 1;
                            }
                            else
                            {
                                if (prevTransType == 1)
                                {
                                    memberQuery += insertUpdateQuery;
                                }

                                memberQuery += "SELECT "
                                + i.GroupID + ", "
                                + i.UserTableID + ", "
                                + i.RingID + ", "
                                + "'" + ModelUtility.IsNull(i.FullName) + "', "
                                + i.MemberAccessType + ", "
                                + i.MemberAddedBy + " "
                                + " UNION ";
                                prevTransType = 0;
                            }
                        }
                        );

                        if (prevTransType == 0)
                        {
                            memberQuery = memberQuery.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' }) + "; ";
                        }
                    }

                    createDB.ExecuteDBQurey(groupQuery + memberQuery);
                    //createDB.ExecuteDBQurey("BEGIN; " + groupQuery + memberQuery + " END;");
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in InsertOrUpdate() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Group Query = " + groupQuery + ", Member Query = " + memberQuery);
            }

            if (OnComplete != null)
            {
                OnComplete();
            }
        }


    }
}

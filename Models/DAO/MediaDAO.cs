﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading;

namespace Models.DAO
{
    public class MediaDAO
    {
        private readonly ILog log = LogManager.GetLogger(typeof(MediaDAO).Name);
        private CreateDBandTables createDBandTables;
        private static MediaDAO _Instance;
        public static MediaDAO Instance
        {
            get { return _Instance = _Instance ?? new MediaDAO(); }
            set { _Instance = value; }
        }
        public MediaDAO()
        {
            createDBandTables = new CreateDBandTables();
            createDBandTables.ConnectToDatabase();
        }

        public DataTable FetchRecentMediaList()
        {
            String query = String.Empty;
            try
            {
                query = "SELECT * FROM " + DBConstants.TABLE_RING_RECENT_MEDIA
                    + " WHERE " + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID + ""
                    + " ORDER BY " + DBConstants.MEDIA_PLAYING_TIME + " DESC ";
                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                SQLiteDataAdapter at = new SQLiteDataAdapter(command);
                DataTable dt = new DataTable();
                at.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                log.Error("SQLException in FetchRecentMediaList() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
            return null;
        }
        public DataTable FetchDownloadMediaList()
        {
            String query = String.Empty;
            try
            {
                query = "SELECT * FROM " + DBConstants.TABLE_RING_DOWNLOAD_MEDIA
                    + " WHERE " + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID + ""
                    + " ORDER BY " + DBConstants.MEDIA_DOWNLOAD_TIME + " DESC ";
                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                SQLiteDataAdapter at = new SQLiteDataAdapter(command);
                DataTable dt = new DataTable();
                at.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                log.Error("SQLException in FetchDownloadMediaList() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
            return null;
        }
        public void InsertIntoRecentMediasTable(Guid contentId, long UserTableID, string Title, string Artist, string streamUrl, string thumbUrl, long duration, int thW, int thH, int mdaTp, Guid albmId, string albmNm, int viewCnt, long lastTm)
        {
            try
            {
                new Thread(delegate()
                {
                    string query = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_RECENT_MEDIA
                                + " ( "
                                    + DBConstants.LOGIN_TABLE_ID + ", "
                                    + DBConstants.USER_TABLE_ID + ", "
                                    + DBConstants.MEDIA_CONTENTID + ", "
                                    + DBConstants.MEDIA_TITLE + ", "
                                    + DBConstants.MEDIA_ARTIST + ", "
                                    + DBConstants.MEDIA_STREAM_URL + ", "
                                    + DBConstants.MEDIA_THUMBURL + ", "
                                    + DBConstants.MEDIA_DURAION + ", "
                                    + DBConstants.MEDIA_THUMB_IMAGE_WIDTH + ", "
                                    + DBConstants.MEDIA_THUMB_IMAGE_HEIGHT + ", "
                                    + DBConstants.MEDIA_ACCESS_COUNT + ", "
                                    + DBConstants.MEDIA_LIKE_COUNT + ", "
                                    + DBConstants.MEDIA_COMMENT_COUNT + ", "
                                    + DBConstants.MEDIA_ILIKE + ", "
                                    + DBConstants.MEDIA_ICOMMENT + ", "
                                    + DBConstants.MEDIA_MEDIA_TYPE + ", "
                                    + DBConstants.MEDIA_ALBUM_ID + ", "
                                    + DBConstants.MEDIA_ALBUM_NAME + ", "
                                    + DBConstants.MEDIA_PLAYING_TIME + ""
                                + " ) ";

                    query += "SELECT "
                        + "" + DefaultSettings.LOGIN_TABLE_ID + ", "
                        + "" + UserTableID + ", "
                        + "'" + contentId.ToString() + "', "
                        + "'" + ModelUtility.IsNull(Title) + "', "
                        + "'" + ModelUtility.IsNull(Artist) + "', "
                        + "'" + ModelUtility.IsNull(streamUrl) + "', "
                        + "'" + ModelUtility.IsNull(thumbUrl) + "', "
                        + "" + duration + ", "
                        + "'" + thW + "', "
                        + "'" + thH + "', "
                        + "" + viewCnt + ", "
                        + "" + 0 + ", "
                        + "" + 0 + ", "
                        + "" + 0 + ", "
                        + "" + 0 + ", "
                        + "" + mdaTp + ", "
                        + "'" + albmId.ToString() + "', "
                        + "'" + ModelUtility.IsNull(albmNm) + "', "
                        + "" + lastTm + ""
                        + " UNION ";

                    query = query.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' });
                    createDBandTables.ExecuteDBQurey(query);
                }).Start();
            }
            catch (Exception ex)
            {
                log.Error("SQLException in InsertOrUpdate() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = ");
            }
        }
        public void InsertIntoDownloadedMediasTable(Guid contentId, long UserTableID, string Title, string Artist, string streamUrl, string ThumbUrl, long duration, int thW, int thH, int mdaTp, Guid albmId, string albmNm, int viewCnt, int dwSt, long dwTm, int dwPrg)
        {
            try
            {
                new Thread(delegate()
                {
                    string query = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_DOWNLOAD_MEDIA
                                + " ( "
                                    + DBConstants.LOGIN_TABLE_ID + ", "
                                    + DBConstants.USER_TABLE_ID + ", "
                                    + DBConstants.MEDIA_CONTENTID + ", "
                                    + DBConstants.MEDIA_TITLE + ", "
                                    + DBConstants.MEDIA_ARTIST + ", "
                                    + DBConstants.MEDIA_STREAM_URL + ", "
                                    + DBConstants.MEDIA_THUMBURL + ", "
                                    + DBConstants.MEDIA_DURAION + ", "
                                    + DBConstants.MEDIA_THUMB_IMAGE_WIDTH + ", "
                                    + DBConstants.MEDIA_THUMB_IMAGE_HEIGHT + ", "
                                    + DBConstants.MEDIA_ACCESS_COUNT + ", "
                                    + DBConstants.MEDIA_LIKE_COUNT + ", "
                                    + DBConstants.MEDIA_COMMENT_COUNT + ", "
                                    + DBConstants.MEDIA_ILIKE + ", "
                                    + DBConstants.MEDIA_ICOMMENT + ", "
                                    + DBConstants.MEDIA_MEDIA_TYPE + ", "
                                    + DBConstants.MEDIA_ALBUM_ID + ", "
                                    + DBConstants.MEDIA_ALBUM_NAME + ", "
                                    + DBConstants.MEDIA_DOWNLOAD_STATE + ", "
                                    + DBConstants.MEDIA_DOWNLOAD_PROGRESS + ", "
                                    + DBConstants.MEDIA_DOWNLOAD_TIME + ""
                                    + " ) ";

                    query += "SELECT "
                        + "" + DefaultSettings.LOGIN_TABLE_ID + ", "
                        + "" + UserTableID + ", "
                        + "'" + contentId.ToString() + "', "
                        + "'" + ModelUtility.IsNull(Title) + "', "
                        + "'" + ModelUtility.IsNull(Artist) + "', "
                        + "'" + ModelUtility.IsNull(streamUrl) + "', "
                        + "'" + ModelUtility.IsNull(ThumbUrl) + "', "
                        + "" + duration + ", "
                        + "'" + thW + "', "
                        + "'" + thH + "', "
                        + "" + viewCnt + ", "
                        + "" + 0 + ", "
                        + "" + 0 + ", "
                        + "" + 0 + ", "
                        + "" + 0 + ", "
                        + "" + mdaTp + ", "
                        + "'" + albmId.ToString() + "', "
                        + "'" + ModelUtility.IsNull(albmNm) + "', "
                        + "" + dwSt + ","
                        + "" + dwPrg + ","
                        + "" + dwTm + ""
                        + " UNION ";

                    query = query.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' });
                    createDBandTables.ExecuteDBQurey(query);
                }).Start();
            }
            catch (Exception ex)
            {
                log.Error("SQLException in InsertOrUpdateDownload() class ==> " + ex.Message + "\n" + ex.StackTrace + " " + ex.Message + ", Query = ");
            }
        }
        public void DeleteFromRecentMediasTable(Guid contentId)
        {
            try
            {
                new Thread(delegate()
                {
                    string query = "DELETE FROM  " + DBConstants.TABLE_RING_RECENT_MEDIA
                                               + " WHERE "
                                               + DBConstants.MEDIA_CONTENTID + "='" + contentId.ToString() + "' AND "
                                               + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID + "";
                    createDBandTables.ExecuteDBQurey(query);

                }).Start();
            }
            catch (Exception ex)
            {
                log.Error("SQLException in  () class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = ");
            }
        }
        public void DeleteFromDownloadedMediasTable(Guid contentId)
        {
            try
            {
                new Thread(delegate()
                {
                    string query = "DELETE FROM  " + DBConstants.TABLE_RING_DOWNLOAD_MEDIA
                                                              + " WHERE "
                                                              + DBConstants.MEDIA_CONTENTID + "='" + contentId.ToString() + "' AND "
                                                              + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID + "";
                    createDBandTables.ExecuteDBQurey(query);
                }).Start();
            }
            catch (Exception ex)
            {
                log.Error("SQLException in  () class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = ");
            }
        }
        //public void InsertIntoRingRecentMedia(SingleMediaDTO singleMediaDto)
        //{
        //    Thread t = new Thread(param => InsertOrUpdate(singleMediaDto));
        //    t.Name = this.GetType().Name;
        //    t.Start();
        //}
        //private void InsertOrUpdate(SingleMediaDTO singleMediaDto)
        //{
        //    string query = String.Empty;
        //    try
        //    {
        //        if (singleMediaDto != null)
        //        {
        //            //CreateDBandTables createDB = new CreateDBandTables();
        //            //createDB.ConnectToDatabase();
        //            query = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_RECENT_MEDIA
        //                        + " ( "
        //                            + DBConstants.LOGIN_TABLE_ID + ", "
        //                            + DBConstants.USER_TABLE_ID + ", "
        //                            + DBConstants.MEDIA_CONTENTID + ", "
        //                            + DBConstants.MEDIA_TITLE + ", "
        //                            + DBConstants.MEDIA_ARTIST + ", "
        //                            + DBConstants.MEDIA_STREAM_URL + ", "
        //                            + DBConstants.MEDIA_THUMBURL + ", "
        //                            + DBConstants.MEDIA_DURAION + ", "
        //                            + DBConstants.MEDIA_THUMB_IMAGE_WIDTH + ", "
        //                            + DBConstants.MEDIA_THUMB_IMAGE_HEIGHT + ", "
        //                            + DBConstants.MEDIA_ACCESS_COUNT + ", "
        //                            + DBConstants.MEDIA_LIKE_COUNT + ", "
        //                            + DBConstants.MEDIA_COMMENT_COUNT + ", "
        //                            + DBConstants.MEDIA_ILIKE + ", "
        //                            + DBConstants.MEDIA_ICOMMENT + ", "
        //                            + DBConstants.MEDIA_MEDIA_TYPE + ", "
        //                            + DBConstants.MEDIA_ALBUM_ID + ", "
        //                            + DBConstants.MEDIA_ALBUM_NAME + ", "
        //                            + DBConstants.MEDIA_PLAYING_TIME + ""
        //                        + " ) ";

        //            //singleMediaDtos.ForEach(i =>
        //            //{
        //            query += "SELECT "
        //                + "" + DefaultSettings.LOGIN_TABLE_ID + ", "
        //                + "" + singleMediaDto.UserTableID + ", "
        //                + "" + singleMediaDto.ContentId + ", "
        //                + "'" + ModelUtility.IsNull(singleMediaDto.Title) + "', "
        //                + "'" + ModelUtility.IsNull(singleMediaDto.Artist) + "', "
        //                + "'" + ModelUtility.IsNull(singleMediaDto.StreamUrl) + "', "
        //                + "'" + ModelUtility.IsNull(singleMediaDto.ThumbUrl) + "', "
        //                + "" + singleMediaDto.Duration + ", "
        //                + "'" + singleMediaDto.ThumbImageWidth + "', "
        //                + "'" + singleMediaDto.ThumbImageHeight + "', "
        //                + "" + singleMediaDto.AccessCount + ", "
        //                + "" + singleMediaDto.LikeCount + ", "
        //                + "" + singleMediaDto.CommentCount + ", "
        //                + "" + singleMediaDto.ILike + ", "
        //                + "" + singleMediaDto.IComment + ", "
        //                + "" + singleMediaDto.MediaType + ", "
        //                + "" + singleMediaDto.AlbumId + ", "
        //                + "'" + ModelUtility.IsNull(singleMediaDto.AlbumName) + "', "
        //                + "" + singleMediaDto.LastPlayedTime + ""
        //                + " UNION ";
        //            //});

        //            query = query.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' });
        //            createDBandTables.ExecuteDBQurey(query);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("SQLException in InsertOrUpdate() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
        //    }

        //}
        //public void DeleteFromRecentMedia(long contentId)
        //{
        //    Thread t = new Thread(param => DeleFromRecentMedia(contentId));
        //    t.Name = this.GetType().Name;
        //    t.Start();
        //}
        //private void DeleFromRecentMedia(long contentId)
        //{
        //    string query = String.Empty;
        //    try
        //    {
        //        query = "DELETE FROM  " + DBConstants.TABLE_RING_RECENT_MEDIA
        //                                  + " WHERE "
        //                                  + DBConstants.MEDIA_CONTENTID + "=" + contentId + " AND "
        //                                  + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID + "";
        //        createDBandTables.ExecuteDBQurey(query);
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("SQLException in deleteFromRecentMedia  ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
        //    }
        //    //finally
        //    //{
        //    //    t = null;
        //    //}
        //}
        //public void InsertIntoDownloadMedia(SingleMediaDTO singleMediaDto)
        //{
        //    Thread t = new Thread(param => InsertOrUpdateDownload(singleMediaDto));
        //    t.Name = this.GetType().Name;
        //    t.Start();
        //}

        //private void InsertOrUpdateDownload(SingleMediaDTO singleMediaDto)
        //{
        //    string query = String.Empty;
        //    try
        //    {
        //        if (singleMediaDto != null)
        //        {
        //            query = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_DOWNLOAD_MEDIA
        //                        + " ( "
        //                            + DBConstants.LOGIN_TABLE_ID + ", "
        //                            + DBConstants.USER_TABLE_ID + ", "
        //                            + DBConstants.MEDIA_CONTENTID + ", "
        //                            + DBConstants.MEDIA_TITLE + ", "
        //                            + DBConstants.MEDIA_ARTIST + ", "
        //                            + DBConstants.MEDIA_STREAM_URL + ", "
        //                            + DBConstants.MEDIA_THUMBURL + ", "
        //                            + DBConstants.MEDIA_DURAION + ", "
        //                            + DBConstants.MEDIA_THUMB_IMAGE_WIDTH + ", "
        //                            + DBConstants.MEDIA_THUMB_IMAGE_HEIGHT + ", "
        //                            + DBConstants.MEDIA_ACCESS_COUNT + ", "
        //                            + DBConstants.MEDIA_LIKE_COUNT + ", "
        //                            + DBConstants.MEDIA_COMMENT_COUNT + ", "
        //                            + DBConstants.MEDIA_ILIKE + ", "
        //                            + DBConstants.MEDIA_ICOMMENT + ", "
        //                            + DBConstants.MEDIA_MEDIA_TYPE + ", "
        //                            + DBConstants.MEDIA_ALBUM_ID + ", "
        //                            + DBConstants.MEDIA_ALBUM_NAME + ", "
        //                            + DBConstants.MEDIA_DOWNLOAD_STATE + ", "
        //                            + DBConstants.MEDIA_DOWNLOAD_PROGRESS + ", "
        //                            + DBConstants.MEDIA_DOWNLOAD_TIME + ""
        //                            + " ) ";

        //            query += "SELECT "
        //                + "" + DefaultSettings.LOGIN_TABLE_ID + ", "
        //                + "" + singleMediaDto.UserTableID + ", "
        //                + "" + singleMediaDto.ContentId + ", "
        //                + "'" + ModelUtility.IsNull(singleMediaDto.Title) + "', "
        //                + "'" + ModelUtility.IsNull(singleMediaDto.Artist) + "', "
        //                + "'" + ModelUtility.IsNull(singleMediaDto.StreamUrl) + "', "
        //                + "'" + ModelUtility.IsNull(singleMediaDto.ThumbUrl) + "', "
        //                + "" + singleMediaDto.Duration + ", "
        //                + "'" + singleMediaDto.ThumbImageWidth + "', "
        //                + "'" + singleMediaDto.ThumbImageHeight + "', "
        //                + "" + singleMediaDto.AccessCount + ", "
        //                + "" + singleMediaDto.LikeCount + ", "
        //                + "" + singleMediaDto.CommentCount + ", "
        //                + "" + singleMediaDto.ILike + ", "
        //                + "" + singleMediaDto.IComment + ", "
        //                + "" + singleMediaDto.MediaType + ", "
        //                + "" + singleMediaDto.AlbumId + ", "
        //                + "'" + ModelUtility.IsNull(singleMediaDto.AlbumName) + "', "
        //                + "" + singleMediaDto.DownloadState + ","
        //                + "" + singleMediaDto.DownloadProgress + ","
        //                + "" + singleMediaDto.DownloadTime + ""
        //                + " UNION ";
        //            //});

        //            query = query.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' });
        //            createDBandTables.ExecuteDBQurey(query);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("SQLException in InsertOrUpdateDownload() class ==> " + ex.Message + "\n" + ex.StackTrace + " " + ex.Message + ", Query = " + query);
        //    }

        //}
        //public void DeleteFromDownloadMedia(long contentId)
        //{
        //    Thread t = new Thread(param => DelFromDownloadMedia(contentId));
        //    t.Name = this.GetType().Name;
        //    t.Start();
        //}
        //private void DelFromDownloadMedia(long contentId)
        //{
        //    string query = String.Empty;
        //    try
        //    {
        //        query = "DELETE FROM  " + DBConstants.TABLE_RING_DOWNLOAD_MEDIA
        //                                  + " WHERE "
        //                                  + DBConstants.MEDIA_CONTENTID + "=" + contentId + " AND "
        //                                  + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID + "";
        //        createDBandTables.ExecuteDBQurey(query);
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("SQLException in DeleteFromDownloadMedia  ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
        //    }
        //}
    }
}

﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading;

namespace Models.DAO
{
    public class ChatBgHistoryDAO
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(ChatBgHistoryDAO).Name);

        public static void SaveChatBgHistoryFromThread(ChatBgImageDTO bgImageDTO)
        {
            new Thread(() => SaveChatBgHistory(bgImageDTO)).Start();
        }

        public static void SaveChatBgHistory(ChatBgImageDTO bgImageDTO)
        {
            string query = String.Empty;
            try
            {
                CreateDBandTables createDBandTables = new CreateDBandTables();
                createDBandTables.ConnectToDatabase();

                query = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_CHAT_BG_LIST
                        + " ( "
                        + DBConstants.LOGIN_TABLE_ID + ", "
                        + DBConstants.ID + ", "
                        + DBConstants.CHAT_BG_URL + ", "
                        + DBConstants.THEME_COLOR + ", "
                        + DBConstants.IS_DOWNLOADED + ", "
                        + DBConstants.UPDATE_TIME + " "
                        + " ) "
                        + " SELECT "
                        + " " + DefaultSettings.LOGIN_TABLE_ID + ", "
                        + " " + bgImageDTO.id + ", "
                        + " '" + ModelUtility.IsNull(bgImageDTO.name) + "', "
                        + " '" + ModelUtility.IsNull(bgImageDTO.themeColor) + "', "
                        + " " + ModelUtility.IsBoolean(bgImageDTO.IsDownloaded) + ", "
                        + " " + bgImageDTO.UpdateTime + " ";

                createDBandTables.ExecuteDBQurey(query);
            }
            catch (Exception ex)
            {
                log.Error("SQLException in InsertChatBgHistory class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
        }

        public static List<ChatBgImageDTO> GetChatBgHistoryDTOs()
        {
            string query = String.Empty;
            List<ChatBgImageDTO> bgDTOs = new List<ChatBgImageDTO>();
            try
            {
                CreateDBandTables CreateDB = new CreateDBandTables();
                CreateDB.ConnectToDatabase();

                query = "SELECT " + DBConstants.ID + ", " + DBConstants.CHAT_BG_URL + ", " + DBConstants.THEME_COLOR + ", " + DBConstants.IS_DOWNLOADED + ", " + DBConstants.UPDATE_TIME + " FROM " + DBConstants.TABLE_RING_CHAT_BG_LIST
                                        + " WHERE "
                                        + DBConstants.LOGIN_TABLE_ID + " = " + DefaultSettings.LOGIN_TABLE_ID;
                                        //+ " ORDER BY " + DBConstants.UPDATE_TIME + " DESC";
                using (SQLiteCommand command = new SQLiteCommand(query, CreateDB.GetConnection()))
                {
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        ChatBgImageDTO bgDTO = new ChatBgImageDTO();
                        bgDTO.id = dr.GetInt32(0);
                        bgDTO.name = dr.GetString(1);
                        bgDTO.themeColor = dr.GetString(2);
                        bgDTO.IsDownloaded = dr.GetBoolean(3);
                        bgDTO.UpdateTime = dr.GetInt64(4);
                        bgDTOs.Add(bgDTO);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in GetChatBgHistoryDTOs() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
            return bgDTOs;
        }

        public static void UpdateChatBgLastUsedTime(string chatBgUrl, long updateTime)
        {
            new Thread(() =>
            {
                string query = String.Empty;

                try
                {
                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();

                    query = "UPDATE " + DBConstants.TABLE_RING_CHAT_BG_LIST
                                + " SET "
                                + DBConstants.UPDATE_TIME + "=" + updateTime + " "
                                + " WHERE "
                                + DBConstants.LOGIN_TABLE_ID + " = " + DefaultSettings.LOGIN_TABLE_ID + " AND "
                                + DBConstants.CHAT_BG_URL + "='" + ModelUtility.IsNull(chatBgUrl) + "' ";

                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in UpdateChatBackground() ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

    }
}

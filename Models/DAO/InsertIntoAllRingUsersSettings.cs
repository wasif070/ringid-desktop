﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading;

namespace Models.DAO
{
    public class InsertIntoAllRingUsersSettings
    {
        private readonly ILog log = LogManager.GetLogger(typeof(InsertIntoAllRingUsersSettings).Name);
        private Func<int> OnComplete;

        public void StartProcess(Dictionary<string, string> settingsDictionary, Func<int> OnComplete = null)
        {
            new Thread(() =>
            {
                this.OnComplete = OnComplete;
                string query = String.Empty;
                try
                {
                    CreateDBandTables createDBandTables = new CreateDBandTables();
                    createDBandTables.ConnectToDatabase();
                    int prevTransType = 1;

                    query = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_ALL_USERS_SETTINGS
                                       + "	("
                                       + DBConstants.LOGIN_TABLE_ID + ","
                                       + " " + DBConstants.SETTINGS_KEY + ", "
                                       + " " + DBConstants.SETTINGS_VALUE + " "
                                       + " ) ";

                    if (settingsDictionary != null)
                    {
                        foreach (KeyValuePair<string, string> userValue in settingsDictionary)
                        {
                            if (!string.IsNullOrEmpty(userValue.Key))
                            {
                                query += "SELECT "
                                + "" + DefaultSettings.LOGIN_TABLE_ID + ","
                                + "'" + userValue.Key + "',"
                                + "'" + userValue.Value + "'"
                                + " UNION ";
                                prevTransType = 0;
                            }
                        }

                        if (prevTransType == 0)
                        {
                            query = query.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' }) + "; ";
                        }
                        createDBandTables.ExecuteDBQurey(query);
                    }
                }
                catch (Exception e)
                {
                    log.Error("SQLException in InsertIntoRingUserSettings class ==> " + e.Message + "\n" + e.StackTrace + ", Query = " + query);
                }
                finally
                {
                    if (OnComplete != null)
                    {
                        OnComplete();
                    }
                }
            }).Start();
        }

        public void UpadateSingleValue(string key, string value)
        {
            string query = string.Empty;
            if (!string.IsNullOrEmpty(key))
            {
                new Thread(() =>
               {
                   try
                   {
                       CreateDBandTables createDBandTables = new CreateDBandTables();
                       createDBandTables.ConnectToDatabase();
                       query = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_ALL_USERS_SETTINGS
                                           + "	("
                                           + DBConstants.LOGIN_TABLE_ID + ","
                                           + " " + DBConstants.SETTINGS_KEY + ", "
                                           + " " + DBConstants.SETTINGS_VALUE + " "
                                           + " ) "
                                           + " VALUES"
                                           + " ("
                                           + "" + DefaultSettings.LOGIN_TABLE_ID + ","
                                           + "'" + key + "',"
                                           + "'" + value + "'"
                                           + ")";
                       createDBandTables.ExecuteDBQurey(query);
                   }
                   catch (Exception e)
                   {
                       log.Error("SQLException in UpadateSingleValue class ==> " + e.Message + "\n" + e.StackTrace + ", Query = " + query);
                   }
               }).Start();
            }
        }
    }
}

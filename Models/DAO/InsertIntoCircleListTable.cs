﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Threading;

namespace Models.DAO
{
    public class InsertIntoCircleListTable
    {
        readonly ILog log = LogManager.GetLogger(typeof(InsertIntoCircleListTable).Name);
        private List<CircleDTO> circleDtos;
        private CircleDTO circleDTO;

        public InsertIntoCircleListTable(List<CircleDTO> circleDtos)
        {
            this.circleDtos = circleDtos;
            Thread thread = new Thread(new ThreadStart(ProcessAll));
            thread.Name = this.GetType().Name;
            thread.Start();
        }

        public InsertIntoCircleListTable(CircleDTO circleDTO)
        {
            this.circleDTO = circleDTO;
            Thread thread = new Thread(new ThreadStart(Process));
            thread.Name = this.GetType().Name;
            thread.Start();
        }

        private void ProcessAll()
        {
            foreach (CircleDTO dto in circleDtos)
            {
                this.circleDTO = dto;
                this.Process();
            }
        }

        private void Process()
        {
            CreateDBandTables createDBandTables = new CreateDBandTables();
            createDBandTables.ConnectToDatabase();
            string query = string.Empty;
            try
            {

                query = string.Format("SELECT * FROM {0} WHERE {1} = @loginTableId AND {2} = @circleId", DBConstants.TABLE_RING_CIRCLE_LIST, DBConstants.LOGIN_TABLE_ID, DBConstants.CIRCLE_ID);
                SQLiteCommand command = new SQLiteCommand(createDBandTables.GetConnection());
                command.CommandText = query;
                command.Parameters.AddWithValue("@loginTableId", DefaultSettings.LOGIN_TABLE_ID);
                command.Parameters.AddWithValue("@circleId", circleDTO.CircleId);

                SQLiteDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    //update
                    long ut = 0;
                    while (reader.Read()) { ut = reader.GetInt32(reader.GetOrdinal(DBConstants.UPDATE_TIME)); }
                    if (circleDTO.UpdateTime > ut)
                    {
                        try
                        {
                            using (SQLiteCommand updateCommand = new SQLiteCommand(createDBandTables.GetConnection()))
                            {
                                query = string.Format("UPDATE {0} SET {1} = @updateTime, {2} = @superAdmin, {3} = @memberCount, {4} = @circleName, {5} = @userTableId, {6} = @integerStatus, {7} = @adminCount WHERE {8} = @loginTableId AND {9} = @circleId",
                                                                                     DBConstants.TABLE_RING_CIRCLE_LIST,
                                                                                     DBConstants.UPDATE_TIME, 
                                                                                     DBConstants.SUPER_ADMIN, 
                                                                                     DBConstants.MEMBER_COUNT,
                                                                                     DBConstants.CIRCLE_NAME, 
                                                                                     DBConstants.USER_TABLE_ID, 
                                                                                     DBConstants.INTEGER_STATUS, 
                                                                                     DBConstants.ADMIN_COUNT,
                                                                                     DBConstants.LOGIN_TABLE_ID, 
                                                                                     DBConstants.CIRCLE_ID);
                                updateCommand.CommandText = query;
                                updateCommand.Parameters.AddWithValue("@updateTime", Convert.ToInt64(circleDTO.UpdateTime));
                                updateCommand.Parameters.AddWithValue("@superAdmin", Convert.ToInt32(circleDTO.SuperAdmin));
                                updateCommand.Parameters.AddWithValue("@memberCount", Convert.ToInt32(circleDTO.MemberCount));
                                updateCommand.Parameters.AddWithValue("@circleName", Models.Utility.ModelUtility.IsNull(circleDTO.CircleName));
                                updateCommand.Parameters.AddWithValue("@userTableId", Convert.ToInt32(circleDTO.UserTableID));
                                updateCommand.Parameters.AddWithValue("@integerStatus", Convert.ToInt32(circleDTO.IntegerStatus));
                                updateCommand.Parameters.AddWithValue("@loginTableId", Convert.ToInt32(DefaultSettings.LOGIN_TABLE_ID));
                                updateCommand.Parameters.AddWithValue("@circleId", Convert.ToInt32(circleDTO.CircleId));
                                updateCommand.Parameters.AddWithValue("@adminCount", Convert.ToInt32(circleDTO.AdminCount));

                                updateCommand.ExecuteNonQuery();
                            }
                        }
                        catch (Exception ex)
                        {
                            log.Error("Update CIRCLE LIST ==> " + ex.Message + "\n" + ex.StackTrace + query);
                            //   if (createDBandTables.IsConnectionOpen) createDBandTables.CloseConnection();
                        }
                    }
                }
                else
                {
                    //fresh insert
                    try
                    {
                        using (SQLiteCommand insCommand = new SQLiteCommand(createDBandTables.GetConnection()))
                        {
                            query = string.Format("INSERT OR REPLACE INTO {0} ( {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9} ) VALUES ( @loginTableId, @circleId, @circleName, @superAdmin, @memberCount, @updateTime, @userTableId, @integerStatus, @adminCount )",
                                                                    DBConstants.TABLE_RING_CIRCLE_LIST,
                                                                    DBConstants.LOGIN_TABLE_ID, 
                                                                    DBConstants.CIRCLE_ID, 
                                                                    DBConstants.CIRCLE_NAME,
                                                                    DBConstants.SUPER_ADMIN, 
                                                                    DBConstants.MEMBER_COUNT, 
                                                                    DBConstants.UPDATE_TIME, 
                                                                    DBConstants.USER_TABLE_ID, 
                                                                    DBConstants.INTEGER_STATUS, 
                                                                    DBConstants.ADMIN_COUNT);
                            insCommand.CommandText = query;
                            insCommand.Parameters.AddWithValue("@loginTableId", Convert.ToInt32(DefaultSettings.LOGIN_TABLE_ID));
                            insCommand.Parameters.AddWithValue("@circleId", Convert.ToInt32(circleDTO.CircleId));
                            insCommand.Parameters.AddWithValue("@circleName", circleDTO.CircleName);
                            insCommand.Parameters.AddWithValue("@superAdmin", Convert.ToInt32(circleDTO.SuperAdmin));
                            insCommand.Parameters.AddWithValue("@memberCount", Convert.ToInt32(circleDTO.MemberCount));
                            insCommand.Parameters.AddWithValue("@updateTime", Convert.ToInt64(circleDTO.UpdateTime));
                            insCommand.Parameters.AddWithValue("@userTableId", Convert.ToInt32(circleDTO.UserTableID));
                            insCommand.Parameters.AddWithValue("@integerStatus", Convert.ToInt32(circleDTO.IntegerStatus));
                            insCommand.Parameters.AddWithValue("@adminCount", Convert.ToInt32(circleDTO.AdminCount));

                            insCommand.ExecuteNonQuery();
                        }

                    }
                    catch (Exception ex)
                    {
                        log.Error("Insert CIRCLE LIST ==> " + ex.Message + "\n" + ex.StackTrace + query);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("ERROR CIRCLE LIST ==> " + ex.Message + "\n" + ex.StackTrace + query);
                //   if (createDBandTables.IsConnectionOpen) createDBandTables.CloseConnection();
            }
        }

    }
}

﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Models.DAO
{
    public class DeleteFromCircleMemberListTable
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(DeleteFromCircleMemberListTable).Name);
        private long circleId;
        

        public DeleteFromCircleMemberListTable(long circleId, List<CircleMemberDTO> listToDelete)
        {
            this.circleId = circleId;
            Thread thread = new Thread(param => Preocess(listToDelete));
            thread.Name = this.GetType().Name;
            thread.Start();
        }
        
        private void Preocess(List<CircleMemberDTO> listToDelete)
        {
            String query = String.Empty;
            try
            {
                CreateDBandTables createDBandTables = new CreateDBandTables();
                createDBandTables.ConnectToDatabase();
                foreach (CircleMemberDTO dto in listToDelete)
                {
                    if (dto.UserTableID > 0)
                        query = "DELETE FROM  " + DBConstants.TABLE_RING_CIRCLE_MEMBERS_LIST + " WHERE " + DBConstants.CIRCLE_ID + "=" + circleId
                                    + " AND " + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID + ""
                                    + " AND " + DBConstants.USER_TABLE_ID + "=" + dto.UserTableID + "";
                    else
                        query = "DELETE FROM  " + DBConstants.TABLE_RING_CIRCLE_MEMBERS_LIST + " WHERE " + DBConstants.CIRCLE_ID + "=" + circleId
                                    + " AND " + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID + "";
                    createDBandTables.ExecuteDBQurey(query);
                }
            }
            catch (Exception e)
            {
              log.Error("SQLException in DeleteFromCircleMemberListTable class ==> " + e.Message + "\n" + e.StackTrace + ", Query = " + query);
            }
        }






















        private void Run()
        {
       

        }
    }
}

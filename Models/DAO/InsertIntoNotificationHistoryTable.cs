﻿using Models.Constants;
using Models.Entity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data.SQLite;
using log4net;

namespace Models.DAO
{
    public class InsertIntoNotificationHistoryTable
    {
        private readonly ILog log = LogManager.GetLogger(typeof(InsertIntoNotificationHistoryTable).Name);
        private List<NotificationDTO> notificationList;

        public InsertIntoNotificationHistoryTable(List<NotificationDTO> notificationList)
        {
            this.notificationList = notificationList;
            Thread thread = new Thread(new ThreadStart(Run));
            thread.Start();
        }

        private void Run()
        {
            string query = String.Empty;

            try
            {
                CreateDBandTables createDBandTables = new CreateDBandTables();
                createDBandTables.ConnectToDatabase();
                if (notificationList != null)
                {
                    foreach (NotificationDTO entity in notificationList)
                    {
                        int RowCount = 0;
                        //string frndNm = entity.FriendName.Replace("'", "''");
                        query = "SELECT COUNT(*) FROM  " + DBConstants.TABLE_RING_NOTIFICATION_HISTORY
                                + " WHERE "
                                + DBConstants.ID + "= '" + entity.ID.ToString() + "' AND "
                                + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID + "";
                        SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                        RowCount = Convert.ToInt32(command.ExecuteScalar());
                        int isRead = (entity.IsRead == false) ? 0 : 1;
                        if (RowCount < 1)
                        {
                            //insert
                            try
                            {
                                //   createDBandTables.ExecuteDBQurey(query);
                                query = string.Format(@"INSERT OR REPLACE INTO {0} ( {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14} ) VALUES 
                                                                                   ( @userTableId, @id, @friendId, @ut, @nType, @acId, @nfId, @imgId, @imgUrl, @cmntId, @mType, @fName, @loc, @isRead )",
                                           DBConstants.TABLE_RING_NOTIFICATION_HISTORY,
                                           DBConstants.LOGIN_TABLE_ID, DBConstants.ID,
                                           DBConstants.NOTIFICATION_FRIENDID, DBConstants.NOTIFICATION_UT,
                                           DBConstants.NOTIFICATION_N_TYPE, DBConstants.NOTIFICATION_ACTIVITY_ID,
                                           DBConstants.NOTIFICATION_NEWSFEED_ID, DBConstants.NOTIFICATION_IMAGE_ID,
                                           DBConstants.NOTIFICATION_IMAGE_URL, DBConstants.NOTIFICATION_COMMENT_ID,
                                           DBConstants.NOTIFICATION_MESSAGE_TYPE, DBConstants.NOTIFICATION_FRIEND_NAME,
                                           DBConstants.NOTIFICATION_NUMBER_OF_LIKECOMMENT, DBConstants.NOTIFICATION_IS_READ);
                                using (SQLiteCommand insCmd = new SQLiteCommand(createDBandTables.GetConnection()))
                                {
                                    insCmd.CommandText = query;
                                    insCmd.Parameters.AddWithValue("@userTableId", Convert.ToInt32(DefaultSettings.LOGIN_TABLE_ID));
                                    //insCmd.Parameters.AddWithValue("@id", Convert.ToInt32(entity.ID));
                                    insCmd.Parameters.AddWithValue("@id", entity.ID.ToString());
                                    insCmd.Parameters.AddWithValue("@friendId", Convert.ToInt32(entity.FriendTableID));
                                    insCmd.Parameters.AddWithValue("@ut", Convert.ToInt64(entity.UpdateTime));
                                    insCmd.Parameters.AddWithValue("@nType", Convert.ToInt32(entity.NotificationType));
                                    insCmd.Parameters.AddWithValue("@acId", Convert.ToInt32(entity.ActivityID));
                                    //insCmd.Parameters.AddWithValue("@nfId", Convert.ToInt32(entity.NewsfeedID));
                                    insCmd.Parameters.AddWithValue("@nfId", entity.NewsfeedID.ToString());
                                    //insCmd.Parameters.AddWithValue("@imgId", Convert.ToInt32(entity.ImageID));
                                    insCmd.Parameters.AddWithValue("@imgId", entity.ImageID.ToString());
                                    insCmd.Parameters.AddWithValue("@imgUrl", entity.ImageUrl ?? String.Empty);
                                    //insCmd.Parameters.AddWithValue("@cmntId", Convert.ToInt32(entity.CommentID));
                                    insCmd.Parameters.AddWithValue("@cmntId", entity.CommentID.ToString());
                                    insCmd.Parameters.AddWithValue("@mType", Convert.ToInt32(entity.MessageType));
                                    insCmd.Parameters.AddWithValue("@fName", entity.FriendName);
                                    insCmd.Parameters.AddWithValue("@loc", Convert.ToInt32(entity.NumberOfLikeOrComment));
                                    insCmd.Parameters.AddWithValue("@isRead", Convert.ToInt32(entity.IsRead));

                                    insCmd.ExecuteNonQuery();
                                }
                            }
                            catch (Exception)
                            {
                                //update
                                query = string.Format(@"UPDATE {0}
                                                                SET {1} = @friendId,
                                                                    {2} = @ut,
                                                                    {3} = @nType,
                                                                    {4} = @acId,
                                                                    {5} = @nfId,
                                                                    {6} = @imgId,
                                                                    {7} = @imgUrl,
                                                                    {8} = @cmntId,
                                                                    {9} = @mType,
                                                                    {10} = @fName,
                                                                    {11} = @loc,
                                                                    {12} = @isRead
                                                              WHERE {13} = @userTableId AND {14} = @id",
                                                              DBConstants.TABLE_RING_NOTIFICATION_HISTORY,
                                                              DBConstants.NOTIFICATION_FRIENDID, DBConstants.NOTIFICATION_UT,
                                                              DBConstants.NOTIFICATION_N_TYPE, DBConstants.NOTIFICATION_ACTIVITY_ID,
                                                              DBConstants.NOTIFICATION_NEWSFEED_ID, DBConstants.NOTIFICATION_IMAGE_ID,
                                                              DBConstants.NOTIFICATION_IMAGE_URL, DBConstants.NOTIFICATION_COMMENT_ID,
                                                              DBConstants.NOTIFICATION_MESSAGE_TYPE, DBConstants.NOTIFICATION_FRIEND_NAME,
                                                              DBConstants.NOTIFICATION_NUMBER_OF_LIKECOMMENT, DBConstants.NOTIFICATION_IS_READ,
                                                              DBConstants.LOGIN_TABLE_ID, DBConstants.ID);
                                using (SQLiteCommand updateCmd = new SQLiteCommand(createDBandTables.GetConnection()))
                                {
                                    updateCmd.CommandText = query;
                                    updateCmd.Parameters.AddWithValue("@friendId", Convert.ToInt32(entity.FriendTableID));
                                    updateCmd.Parameters.AddWithValue("@ut", Convert.ToInt64(entity.UpdateTime));
                                    updateCmd.Parameters.AddWithValue("@nType", Convert.ToInt32(entity.NotificationType));
                                    updateCmd.Parameters.AddWithValue("@acId", Convert.ToInt32(entity.ActivityID));
                                    //updateCmd.Parameters.AddWithValue("@nfId", Convert.ToInt32(entity.NewsfeedID));
                                    updateCmd.Parameters.AddWithValue("@nfId", entity.NewsfeedID.ToString());
                                    //updateCmd.Parameters.AddWithValue("@imgId", Convert.ToInt32(entity.ImageID));
                                    updateCmd.Parameters.AddWithValue("@imgId", entity.ImageID.ToString());
                                    updateCmd.Parameters.AddWithValue("@imgUrl", entity.ImageUrl ?? String.Empty);
                                    //updateCmd.Parameters.AddWithValue("@cmntId", Convert.ToInt32(entity.CommentID));
                                    updateCmd.Parameters.AddWithValue("@cmntId", entity.CommentID.ToString());
                                    updateCmd.Parameters.AddWithValue("@mType", Convert.ToInt32(entity.MessageType));
                                    updateCmd.Parameters.AddWithValue("@fName", entity.FriendName);
                                    updateCmd.Parameters.AddWithValue("@loc", Convert.ToInt32(entity.NumberOfLikeOrComment));
                                    updateCmd.Parameters.AddWithValue("@isRead", Convert.ToInt32(entity.IsRead));
                                    updateCmd.Parameters.AddWithValue("@userTableId", Convert.ToInt32(DefaultSettings.LOGIN_TABLE_ID));
                                    //                                    updateCmd.Parameters.AddWithValue("@id", Convert.ToInt32(entity.ID));
                                    updateCmd.Parameters.AddWithValue("@id", entity.ID.ToString());

                                    updateCmd.ExecuteNonQuery();
                                }
                            }
                        }
                        else
                        {
                            //update
                            query = string.Format(@"UPDATE {0}
                                                                SET {1} = @friendId,
                                                                    {2} = @ut,
                                                                    {3} = @nType,
                                                                    {4} = @acId,
                                                                    {5} = @nfId,
                                                                    {6} = @imgId,
                                                                    {7} = @imgUrl,
                                                                    {8} = @cmntId,
                                                                    {9} = @mType,
                                                                    {10} = @fName,
                                                                    {11} = @loc,
                                                                    {12} = @isRead
                                                              WHERE {13} = @userTableId AND {14} = @id",
                                                              DBConstants.TABLE_RING_NOTIFICATION_HISTORY,
                                                              DBConstants.NOTIFICATION_FRIENDID, DBConstants.NOTIFICATION_UT,
                                                              DBConstants.NOTIFICATION_N_TYPE, DBConstants.NOTIFICATION_ACTIVITY_ID,
                                                              DBConstants.NOTIFICATION_NEWSFEED_ID, DBConstants.NOTIFICATION_IMAGE_ID,
                                                              DBConstants.NOTIFICATION_IMAGE_URL, DBConstants.NOTIFICATION_COMMENT_ID,
                                                              DBConstants.NOTIFICATION_MESSAGE_TYPE, DBConstants.NOTIFICATION_FRIEND_NAME,
                                                              DBConstants.NOTIFICATION_NUMBER_OF_LIKECOMMENT, DBConstants.NOTIFICATION_IS_READ,
                                                              DBConstants.LOGIN_TABLE_ID, DBConstants.ID);
                            using (SQLiteCommand updateCmd = new SQLiteCommand(createDBandTables.GetConnection()))
                            {
                                updateCmd.CommandText = query;
                                updateCmd.Parameters.AddWithValue("@friendId", Convert.ToInt32(entity.FriendTableID));
                                updateCmd.Parameters.AddWithValue("@ut", Convert.ToInt64(entity.UpdateTime));
                                updateCmd.Parameters.AddWithValue("@nType", Convert.ToInt32(entity.NotificationType));
                                updateCmd.Parameters.AddWithValue("@acId", Convert.ToInt32(entity.ActivityID));
                                //                                updateCmd.Parameters.AddWithValue("@nfId", Convert.ToInt32(entity.NewsfeedID));
                                updateCmd.Parameters.AddWithValue("@nfId", entity.NewsfeedID.ToString());
                                //updateCmd.Parameters.AddWithValue("@imgId", Convert.ToInt32(entity.ImageID));
                                updateCmd.Parameters.AddWithValue("@imgId", entity.ImageID.ToString());
                                updateCmd.Parameters.AddWithValue("@imgUrl", entity.ImageUrl ?? String.Empty);
                                //updateCmd.Parameters.AddWithValue("@cmntId", Convert.ToInt32(entity.CommentID));
                                updateCmd.Parameters.AddWithValue("@cmntId", entity.CommentID.ToString());
                                updateCmd.Parameters.AddWithValue("@mType", Convert.ToInt32(entity.MessageType));
                                updateCmd.Parameters.AddWithValue("@fName", entity.FriendName);
                                updateCmd.Parameters.AddWithValue("@loc", Convert.ToInt32(entity.NumberOfLikeOrComment));
                                updateCmd.Parameters.AddWithValue("@isRead", Convert.ToInt32(entity.IsRead));
                                updateCmd.Parameters.AddWithValue("@userTableId", Convert.ToInt32(DefaultSettings.LOGIN_TABLE_ID));
                                //                                updateCmd.Parameters.AddWithValue("@id", Convert.ToInt32(entity.ID));
                                updateCmd.Parameters.AddWithValue("@id", entity.ID.ToString());

                                updateCmd.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("SQLException in InsertIntoNotificationHistoryTable class ==> " + e.Message + "\n" + e.StackTrace + ", Query = " + query);
            }
        }

    }
}

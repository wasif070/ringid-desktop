﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;

namespace Models.DAO
{
    public class NotificationHistoryDAO
    {
        readonly ILog log = LogManager.GetLogger(typeof(NotificationHistoryDAO).Name);

        private CreateDBandTables createDBandTables;
        public NotificationHistoryDAO()
        {
            createDBandTables = new CreateDBandTables();
            createDBandTables.ConnectToDatabase();
        }
        private static NotificationHistoryDAO _Instance;
        public static NotificationHistoryDAO Instance
        {
            get { return _Instance = _Instance ?? new NotificationHistoryDAO(); }
            set { _Instance = value; }
        }
        /*
        public List<NotificationDTO> LoadNotificationHistoryList(int start, int limit)
        {
            string query = String.Empty;
            List<NotificationDTO> notificationList = new List<NotificationDTO>();
            try
            {
                query = "SELECT * FROM "
                             + DBConstants.TABLE_RING_NOTIFICATION_HISTORY + " "
                             + "WHERE "
                             + DBConstants.LOGIN_USER_ID + " = " + DefaultSettings.LOGIN_USER_ID + " "
                             + "ORDER BY "
                             + DBConstants.NOTIFICATION_UT + " DESC"
                             + (limit > 0 ? (" OFFSET " + start + " ROWS FETCH FIRST " + limit + " ROWS ONLY") : "");

                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                SQLiteDataReader row = command.ExecuteReader();
                while (row.Read())
                {
                    NotificationDTO entity = new NotificationDTO();
                    entity.ID = Convert.ToInt64(row[DBConstants.ID]);
                    entity.FriendID = Convert.ToInt64(row[DBConstants.NOTIFICATION_FRIENDID]);
                    entity.UpdateTime = Convert.ToInt64(row[DBConstants.NOTIFICATION_UT]);
                    entity.NotificationType = Convert.ToInt32(row[DBConstants.NOTIFICATION_N_TYPE]);
                    entity.ActivityID = Convert.ToInt32(row[DBConstants.NOTIFICATION_ACTIVITY_ID]);
                    entity.NewsfeedID = Convert.ToInt64(row[DBConstants.NOTIFICATION_NEWSFEED_ID]);
                    entity.ImageID = Convert.ToInt64(row[DBConstants.NOTIFICATION_IMAGE_ID]);
                    entity.CommentID = Convert.ToInt64(row[DBConstants.NOTIFICATION_COMMENT_ID]);
                    entity.MessageType = Convert.ToInt32(row[DBConstants.NOTIFICATION_MESSAGE_TYPE]);
                    entity.FriendName = Convert.ToString(row[DBConstants.NOTIFICATION_FRIEND_NAME]);
                    entity.NumberOfLikeOrComment = Convert.ToInt32(row[DBConstants.NOTIFICATION_NUMBER_OF_LIKECOMMENT]);
                    entity.IsRead = Convert.ToBoolean(row[DBConstants.NOTIFICATION_IS_READ]);

                    // notificationList.Add(entity);

                    if (!RingDictionaries.Instance.NOTIFICATION_DICTIONARY.ContainsKey(entity.ID))
                    {
                        RingDictionaries.Instance.NOTIFICATION_DICTIONARY.Add(entity.ID, entity);
                    }
                    else
                    {

                        RingDictionaries.Instance.NOTIFICATION_DICTIONARY[entity.ID] = entity;
                    }

                }


            }
            catch (Exception e)
            {
                log.Error("SQLException in LoadNotificationHistoryList class ==> " + e.Message + "\n" + e.StackTrace + "==>" + e.Message + ", Query = " + query);
            }
            finally
            {
                LoadMaxTimeFromNotificationHistory();
                LoadMinTimeFromNotificationHistory();
            }

            return notificationList;
        }
        */
        public void LoadNotificationFromDB(long updateTime, bool firstTimeFetch = false)
        {
            //List<NotificationDTO> list = new List<NotificationDTO>();
            string query = string.Format("SELECT * FROM {0} WHERE {1} = @userTableId AND {2} < @updateTime ORDER BY {2} DESC LIMIT 10", DBConstants.TABLE_RING_NOTIFICATION_HISTORY, DBConstants.LOGIN_TABLE_ID, DBConstants.NOTIFICATION_UT);
            try
            {
                SQLiteCommand command = new SQLiteCommand(createDBandTables.GetConnection());
                command.CommandText = query;
                command.Parameters.AddWithValue("@userTableId", Convert.ToInt64(DefaultSettings.LOGIN_TABLE_ID));
                command.Parameters.AddWithValue("@updateTime", Convert.ToInt64(updateTime));

                SQLiteDataReader row = command.ExecuteReader();

                if (row.HasRows)
                {
                    while (row.Read())
                    {
                        //System.Diagnostics.Debug.WriteLine("GUID IMGID ==>" + Convert.ToString(row[DBConstants.NOTIFICATION_IMAGE_ID]));
                        NotificationDTO entity = new NotificationDTO();
                        //entity.ID = Convert.ToInt64(row[DBConstants.ID]);
                        entity.ID = Guid.Parse(Convert.ToString(row[DBConstants.ID]));
                        entity.FriendTableID = Convert.ToInt64(row[DBConstants.NOTIFICATION_FRIENDID]);
                        entity.UpdateTime = Convert.ToInt64(row[DBConstants.NOTIFICATION_UT]);
                        entity.NotificationType = Convert.ToInt32(row[DBConstants.NOTIFICATION_N_TYPE]);
                        entity.ActivityID = Convert.ToInt32(row[DBConstants.NOTIFICATION_ACTIVITY_ID]);
                        entity.NewsfeedID = Guid.Parse(Convert.ToString(row[DBConstants.NOTIFICATION_NEWSFEED_ID])); 
                        entity.ImageID = Guid.Parse(Convert.ToString((row[DBConstants.NOTIFICATION_IMAGE_ID])));
                        entity.ImageUrl = Convert.ToString(row[DBConstants.NOTIFICATION_IMAGE_URL]);
                        entity.CommentID = Guid.Parse(Convert.ToString(row[DBConstants.NOTIFICATION_COMMENT_ID]));
                        entity.MessageType = Convert.ToInt32(row[DBConstants.NOTIFICATION_MESSAGE_TYPE]);
                        entity.FriendName = Convert.ToString(row[DBConstants.NOTIFICATION_FRIEND_NAME]);
                        entity.NumberOfLikeOrComment = Convert.ToInt32(row[DBConstants.NOTIFICATION_NUMBER_OF_LIKECOMMENT]);
                        entity.IsRead = Convert.ToBoolean(row[DBConstants.NOTIFICATION_IS_READ]);
                        entity.PreviousIds = new List<Guid>();

                        if (!RingDictionaries.Instance.NOTIFICATION_LISTS.Any(x => x.ID == entity.ID))
                        {
                            RingDictionaries.Instance.NOTIFICATION_LISTS.Add(entity);
                        }
                        AppConstants.NOTIFICATION_MIN_UT = entity.UpdateTime;
                    }
                    if (firstTimeFetch)
                    {
                        int dbUnreads = 0;
                        foreach (var item in RingDictionaries.Instance.NOTIFICATION_LISTS)
                        {
                            if (item.IsRead)
                            {
                                break;
                            }
                            dbUnreads++;
                        }
                        AppConstants.ALL_NOTIFICATION_COUNT = dbUnreads;
                    }
                    //NotificationDTO dto = RingDictionaries.Instance.NOTIFICATION_LISTS.Where(x => x.UpdateTime == RingDictionaries.Instance.NOTIFICATION_LISTS.Max(y => y.UpdateTime)).First();
                    //dto.IsRead = true;
                    //List<NotificationDTO> list = new List<NotificationDTO>();
                    //list.Add(dto);
                    //new InsertIntoNotificationHistoryTable(list);
                }
                else
                {
                    AppConstants.NOTIFICATION_ALL_FETCHED_FROM_DB = true;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error Select from Notification History " + ex.Message + "\n" + ex.StackTrace);
            }
            finally
            {
                LoadMaxTimeFromNotificationHistory();
                //LoadMinTimeFromNotificationHistory();
            }
            //  return list;
        }

        public void LoadMaxTimeFromNotificationHistory()
        {
            string query = String.Empty;
            try
            {
                query = "SELECT MAX(" + DBConstants.NOTIFICATION_UT + ") AS TEMP FROM "
                             + DBConstants.TABLE_RING_NOTIFICATION_HISTORY + " "
                             + "WHERE "
                             + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID;

                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                SQLiteDataReader row = command.ExecuteReader();
                while (row.Read() && !string.IsNullOrEmpty(row["TEMP"].ToString()))
                {
                    AppConstants.NOTIFICATION_MAX_UT = Convert.ToInt64(row["TEMP"]);
                }
            }
            catch (Exception e)
            {
                log.Error("Exception in LoadMaxTimeFromNotificationHistory==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message + ", Query = " + query);
            }
        }

        public void LoadMinTimeFromNotificationHistory()
        {
            string query = String.Empty;
            try
            {
                query = "SELECT MIN(" + DBConstants.NOTIFICATION_UT + ") AS TEMP FROM "
                             + DBConstants.TABLE_RING_NOTIFICATION_HISTORY + " "
                             + "WHERE "
                             + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID;

                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                SQLiteDataReader row = command.ExecuteReader();
                while (row.Read() && !string.IsNullOrEmpty(row["TEMP"].ToString()))
                {
                    AppConstants.NOTIFICATION_MIN_UT = Convert.ToInt64(row["TEMP"]);
                }
            }
            catch (Exception e)
            {
                log.Error("Exception in LoadMinTimeFromNotificationHistory==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message + ", Query = " + query);
            }
        }

        public long GetMinTimeFromNotificationHistory()
        {
            long ut = 0;
            string query = String.Empty;
            try
            {
                query = "SELECT MIN(" + DBConstants.NOTIFICATION_UT + ") AS TEMP FROM "
                             + DBConstants.TABLE_RING_NOTIFICATION_HISTORY + " "
                             + "WHERE "
                             + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID;

                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                SQLiteDataReader row = command.ExecuteReader();
                while (row.Read() && !string.IsNullOrEmpty(row["TEMP"].ToString()))
                {
                    ut = Convert.ToInt64(row["TEMP"]);
                }
            }
            catch (Exception e)
            {
                ut = AppConstants.NOTIFICATION_MIN_UT;
                log.Error("Exception in LoadMinTimeFromNotificationHistory==>" + e.Message + "\n" + e.StackTrace + "==>" + e.Message + ", Query = " + query);
            }
            return ut;
        }

        public void DeleteNotification(Guid nfId)
        {
            try
            {
                string query = string.Format("DELETE FROM {0} WHERE {1} = @userTableId AND {2} = @notificationId",
                                              DBConstants.TABLE_RING_NOTIFICATION_HISTORY, DBConstants.LOGIN_TABLE_ID, DBConstants.ID);
                SQLiteCommand command = new SQLiteCommand(createDBandTables.GetConnection());
                command.CommandText = query;
                command.Parameters.AddWithValue("@userTableId", DefaultSettings.LOGIN_TABLE_ID);
                command.Parameters.AddWithValue("@notificationId", nfId.ToString());

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                log.Error("Error Delete notification ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
    }
}

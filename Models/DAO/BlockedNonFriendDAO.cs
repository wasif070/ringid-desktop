﻿using log4net;
using Models.Constants;
using Models.Utility;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading;

namespace Models.DAO
{
    public class BlockedNonFriendDAO
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(BlockedNonFriendDAO).Name);

        public static void SaveBlockedNonFriendFromThread(long userId, bool blockByMe, bool blockByFriend)
        {
            new Thread(() => SaveBlockedNonFriend(userId, blockByMe, blockByFriend)).Start();
        }

        public static void SaveBlockedNonFriend(long userId, bool blockByMe, bool blockByFriend)
        {
            string query = String.Empty;
            try
            {
                CreateDBandTables createDBandTables = new CreateDBandTables();
                createDBandTables.ConnectToDatabase();

                query = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_BLOCKED_NON_FRIEND
                        + " ( "
                        + DBConstants.LOGIN_TABLE_ID + ", "
                        + DBConstants.USER_TABLE_ID + ", "
                        + DBConstants.IS_BLOCKED_BY_ME + ", "
                        + DBConstants.IS_BLOCKED_BY_FRIEND + " "
                        + " ) "
                        + " SELECT "
                        + " " + DefaultSettings.LOGIN_TABLE_ID + ", "
                        + " " + userId + ", "
                        + " " + ModelUtility.IsBoolean(blockByMe) + ", "
                        + " " + ModelUtility.IsBoolean(blockByFriend) + " ";

                createDBandTables.ExecuteDBQurey(query);
            }
            catch (Exception ex)
            {
                log.Error("SQLException in SaveBlockedNonFriend() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
        }

        public static ConcurrentDictionary<long, bool[]> GetBlockedNonFriends()
        {
            string query = String.Empty;
            ConcurrentDictionary<long, bool[]> list = new ConcurrentDictionary<long, bool[]>();
            try
            {
                CreateDBandTables CreateDB = new CreateDBandTables();
                CreateDB.ConnectToDatabase();

                query = "SELECT " 
                        + DBConstants.USER_TABLE_ID + ", " 
                        + DBConstants.IS_BLOCKED_BY_ME + ", " 
                        + DBConstants.IS_BLOCKED_BY_FRIEND 
                        + " FROM " 
                        + DBConstants.TABLE_RING_BLOCKED_NON_FRIEND
                        + " WHERE "
                        + DBConstants.LOGIN_TABLE_ID + " = " + DefaultSettings.LOGIN_TABLE_ID;
                using (SQLiteCommand command = new SQLiteCommand(query, CreateDB.GetConnection()))
                {
                    SQLiteDataReader dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        list[dr.GetInt64(0)] = new bool[2] { dr.GetBoolean(1), dr.GetBoolean(2) };
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in GetBlockedNonFriends() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
            return list;
        }

    }
}

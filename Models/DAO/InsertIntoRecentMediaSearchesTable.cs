﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Data.SQLite;
using System.Threading;

namespace Models.DAO
{
    public class InsertIntoRecentMediaSearchesTable
    {
        //private string sqlInsert, sqlUpdate, sqlSelect;

        private readonly ILog log = LogManager.GetLogger(typeof(InsertIntoRecentMediaSearchesTable).Name);

        private SearchMediaDTO searchMedia;
        public InsertIntoRecentMediaSearchesTable(SearchMediaDTO searchMedia)
        {
            this.searchMedia = searchMedia;
            Thread thread = new Thread(new ThreadStart(Run));
            thread.Name = this.GetType().Name;
            thread.Start();
        }
        private void Run()
        {
            string query = String.Empty;
            try
            {
                CreateDBandTables createDBandTables = new CreateDBandTables();
                createDBandTables.ConnectToDatabase();

                query = "SELECT COUNT(*) FROM  " + DBConstants.TABLE_RING_MEDIA_SEARCH + " WHERE "
                        + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID + " AND " + DBConstants.MEDIA_SEARCH_SUGGESTION + "='" + searchMedia.SearchSuggestion + "'";
                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                int RowCount = Convert.ToInt32(command.ExecuteScalar());
                if (RowCount == 1)
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_MEDIA_SEARCH
                            + " SET "
                            + DBConstants.MEDIA_MEDIA_TYPE + "=" + searchMedia.SearchType + ","
                            + DBConstants.UPDATE_TIME + "=" + searchMedia.UpdateTime
                            + " WHERE "
                            + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID + " AND " + DBConstants.MEDIA_SEARCH_SUGGESTION + "='" + searchMedia.SearchSuggestion + "'";
                    createDBandTables.ExecuteDBQurey(query);
                }
                else
                {
                    query = "INSERT INTO " + DBConstants.TABLE_RING_MEDIA_SEARCH
                             + " (" + DBConstants.LOGIN_TABLE_ID + "," + DBConstants.MEDIA_SEARCH_SUGGESTION
                             + "," + DBConstants.MEDIA_MEDIA_TYPE + "," + DBConstants.UPDATE_TIME + ")"
                             + "VALUES"
                             + "("
                             + DefaultSettings.LOGIN_TABLE_ID + ","
                             + "'" + searchMedia.SearchSuggestion + "',"
                             + searchMedia.SearchType + ","
                             + searchMedia.UpdateTime + ""
                             + ")";
                    createDBandTables.ExecuteDBQurey(query);
                    query = "SELECT COUNT(*) FROM  " + DBConstants.TABLE_RING_MEDIA_SEARCH + " WHERE "
                            + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID;
                    SQLiteCommand command2 = new SQLiteCommand(query, createDBandTables.GetConnection());
                    int RowCount2 = Convert.ToInt32(command.ExecuteScalar());
                    if (RowCount2 > 10)
                    {
                        query = "DELETE FROM  " + DBConstants.TABLE_RING_MEDIA_SEARCH
                            + " WHERE "
                            + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID + ""
                            + " ORDER BY " + DBConstants.UPDATE_TIME
                            + "LIMIT 1";
                        createDBandTables.ExecuteDBQurey(query);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("SQLException in InsertIntoRecentMediaSearchesTable class ==> " + e.Message + "\n" + e.StackTrace + ", Query = " + query);
            }
        }
    }
}

﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Models.DAO
{
    public class InsertIntoUserBasicInfoTable
    {
        private readonly ILog log = LogManager.GetLogger(typeof(InsertIntoUserBasicInfoTable).Name);

        Thread t = null;
        public delegate void CompleteHandler();
        public event CompleteHandler OnComplete;
        private Func<int> OnFuncComplete;

        bool doSave;
        public InsertIntoUserBasicInfoTable(List<UserBasicInfoDTO> basicInfoDTOs, bool doSave = false, Func<int> OnFuncComplete = null)
        {
            this.OnFuncComplete = OnFuncComplete;
            t = new Thread(param => Preocess(basicInfoDTOs));
            t.Name = this.GetType().Name;
            this.doSave = doSave;
        }

        public void Start()
        {
            if (t != null)
            {
                t.Start();
            }
        }

        private void Preocess(List<UserBasicInfoDTO> basicInfoDTOs)
        {
            try
            {
                List<long> userIDList = new List<long>();
                if (basicInfoDTOs
                    != null && basicInfoDTOs.Count > 0)
                {
                    List<UserBasicInfoDTO> infoDTOs = new List<UserBasicInfoDTO>();
                    foreach (UserBasicInfoDTO infoDTO in basicInfoDTOs)
                    {
                        if (!userIDList.Contains(infoDTO.RingID))
                        {
                            //Console.WriteLine("Insering++" + infoDTO.UserIdentity);
                            infoDTOs.Add(infoDTO);
                            if (infoDTOs.Count >= 500)
                            {
                                InsertOrUpdate(infoDTOs);
                                infoDTOs = new List<UserBasicInfoDTO>();
                            }
                            userIDList.Add(infoDTO.RingID);
                        }

                    }
                    userIDList.Clear();
                    if (infoDTOs.Count > 0)
                    {
                        InsertOrUpdate(infoDTOs);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in Preocess() ==> " + ex.Message + "\n" + ex.StackTrace);
            }

            if (OnComplete != null)
            {
                if (basicInfoDTOs != null && basicInfoDTOs.Count > 1)
                {
                    Thread.Sleep(100);
                }
                else
                {
                    Thread.Sleep(15);
                }
                OnComplete();
            }
        }

        private void InsertOrUpdate(List<UserBasicInfoDTO> basicInfoDTOs)
        {
            string query = String.Empty;

            try
            {
                if (basicInfoDTOs != null && basicInfoDTOs.Count > 0)
                {
                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();

                    query = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_USER_BASIC_INFO
                                + " ( "
                                + DBConstants.USER_TABLE_ID + ", "
                                + DBConstants.RING_ID + ", "
                                + DBConstants.FULL_NAME + ", "
                                + DBConstants.PROFILE_IMAGE + ", "
                                + DBConstants.PROFILE_IMAGE_ID + ", "
                                + DBConstants.COVER_IMAGE + ", "
                                + DBConstants.COVER_IMAGE_ID + ", "
                                + DBConstants.COVER_IMAGE_X + ", "
                                + DBConstants.COVER_IMAGE_Y + ", "
                                + DBConstants.PROFILE_IMAGE_PRIVACY + ", "
                                + DBConstants.COVER_IMAGE_PRIVACY + ", "
                                + DBConstants.DEVICE_TOKEN + ", "
                                + DBConstants.CONTACT_TYPE + " "
                                + " ) ";

                    basicInfoDTOs.ForEach(i =>
                        {

                            query += "SELECT "
                            + i.UserTableID + ", "
                            + i.RingID + ", "
                            + "'" + ModelUtility.IsNull(i.FullName) + "', "
                            + "'" + ModelUtility.IsNull(i.ProfileImage) + "', "
                            + "'" + i.ProfileImageId.ToString() + "', "
                            + "'" + ModelUtility.IsNull(i.CoverImage) + "', "
                            + "'" + i.CoverImageId.ToString() + "', "
                            + i.CropImageX + ", "
                            + i.CropImageY + ", "
                            + i.ProfileImagePrivacy + ", "
                            + i.CoverImagePrivacy + ", "
                            + "'" + ModelUtility.IsNull(i.DeviceToken) + "', "
                            + i.ContactType + " "
                            + " UNION ";
                        }
                    );
                    query = query.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' }) + "; ";
                    //createDB.ExecuteDBQurey(query);

                    //query = String.Empty;
                    query += "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_CONTACT_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " ( "
                                + DBConstants.USER_TABLE_ID + ", "
                                + DBConstants.RING_ID + ", "
                                + DBConstants.FRIENDSHIP_STATUS + ", "
                                + DBConstants.UPDATE_TIME + ", "
                                + DBConstants.MATCHED_BY + ", "
                                + DBConstants.NUMBER_OF_MUTUAL_FRIENDS + ", "
                                + DBConstants.NUMBER_OF_CALLS + ", "
                                + DBConstants.NUMBER_OF_CHATS + ", "
                                + DBConstants.FAVORITE_RANK + ", "
                                + DBConstants.CALL_ACCESS + ", "
                                + DBConstants.CHAT_ACCESS + ", "
                                + DBConstants.FEED_ACCESS + ", "
                                + DBConstants.CHAT_MIN_PACKET_ID + ", "
                                + DBConstants.CHAT_MAX_PACKET_ID + ", "
                                + DBConstants.IS_SECRET_VISIBLE + ", "
                                + DBConstants.CHAT_BG_URL + ", "
                                + DBConstants.CONTACT_ADDED_TIME + ", "
                                + DBConstants.CONTACT_UT + ", "
                                + DBConstants.CONTACT_READ_UNREAD + ", "
                                + DBConstants.INCOMING_STATUS + ", "
                                + DBConstants.IM_SOUND + ", "
                                + DBConstants.IM_NOTIFICATION + ", "
                                + DBConstants.IM_POPUP + " "
                                + " ) ";

                    basicInfoDTOs.ForEach(i =>
                    {

                        query += "SELECT "
                        + i.UserTableID + ", "
                        + i.RingID + ", "
                        + (i.Delete == StatusConstants.STATUS_DELETED ? 0 : i.FriendShipStatus) + ", "
                        + i.UpdateTime + ", "
                        + i.MatchedBy + ", "
                        + i.NumberOfMutualFriends + ", "
                        + i.NumberOfCalls + ", "
                        + i.NumberOfChats + ", "
                        + i.FavoriteRank + ", "
                        + i.CallAccess + ", "
                        + i.ChatAccess + ", "
                        + i.FeedAccess + ", "
                        + "'" + ModelUtility.IsNull(i.ChatMinPacketID) + "', "
                        + "'" + ModelUtility.IsNull(i.ChatMaxPacketID) + "', "
                        + i.IsSecretVisible + ", "
                        + "'" + ModelUtility.IsNull(i.ChatBgUrl) + "', "
                        + i.ContactAddedTime + ", "
                        + i.ContactUpdateTime + ", "
                        + i.ContactAddedReadUnread + ", "
                        + i.IncomingStatus + ", "
                        + ModelUtility.IsBoolean(i.ImSoundEnabled) + ", "
                        + ModelUtility.IsBoolean(i.ImNotificationEnabled) + ", "
                        + ModelUtility.IsBoolean(i.ImPopupEnabled) + " "
                        + " UNION ";
                    }
                    );
                    query = query.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' }) + "; ";
                    createDB.ExecuteDBQurey(query);

                    if (doSave)
                    {
                        basicInfoDTOs.ForEach(i =>
                        {
                            if (i.UpdateTime > Models.Constants.SettingsConstants.VALUE_RINGID_USERINFO_UT)
                            {
                                Models.Constants.SettingsConstants.VALUE_RINGID_USERINFO_UT = i.UpdateTime;
                            }
                            if (i.ContactUpdateTime > Models.Constants.SettingsConstants.VALUE_RINGID_CONTACT_UT)
                            {
                                Models.Constants.SettingsConstants.VALUE_RINGID_CONTACT_UT = i.ContactUpdateTime;
                            }
                        });
                        Dictionary<string, string> temDictionary = new Dictionary<string,string>();
                        temDictionary[Models.Constants.SettingsConstants.KEY_RINGID_USERINFO_UT] =  Models.Constants.SettingsConstants.VALUE_RINGID_USERINFO_UT.ToString();
                        temDictionary[Models.Constants.SettingsConstants.KEY_RINGID_CONTACT_UT] =  Models.Constants.SettingsConstants.VALUE_RINGID_CONTACT_UT.ToString();

                        new InsertIntoAllRingUsersSettings().StartProcess(temDictionary, ()=>
                        {
                            if (OnFuncComplete != null) 
                            {
                                OnFuncComplete();
                            }
                            return 0;
                        });
                    }
                    else 
                    {
                        if (OnFuncComplete != null)
                        {
                            OnFuncComplete();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in InsertOrUpdate(List<UserBasicInfoDTO> basicInfoDTOs) ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
        }

    }
}

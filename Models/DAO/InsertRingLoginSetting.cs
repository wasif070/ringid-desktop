﻿using log4net;
using Models.Constants;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Data.SQLite;

namespace Models.DAO
{
    public class InsertRingLoginSetting
    {
        readonly ILog log = LogManager.GetLogger(typeof(InsertRingLoginSetting).Name);
        private IList<InfoKeyValue> infoKeyValues;
        bool isRunning = false;
        //private string sqlInsert, sqlUpdate, sqlSelect;
        public static object lockObj = new object();

        public InsertRingLoginSetting(IList<InfoKeyValue> infoKeyValues)
        {
            this.infoKeyValues = infoKeyValues;
        }

        public void Start()
        {
            if (!isRunning)
            {
                Thread thread = new Thread(new ThreadStart(Run));
                thread.Name = this.GetType().Name;
                thread.Start();
            }

        }

        public void Run()
        {
            lock (lockObj)
            {
                isRunning = true;
                String sqlQuery = String.Empty;
                try
                {
                    CreateDBandTables createDBandTables = new CreateDBandTables();
                    createDBandTables.ConnectToDatabase();

                    foreach (InfoKeyValue infoKeyValue in infoKeyValues)
                    {
                        if (infoKeyValue.Status == StatusConstants.STATUS_DELETED)
                        {
                            sqlQuery = "DELETE FROM  " + DBConstants.TABLE_RING_LOGIN_SETTINGS
                                  + " WHERE " + DBConstants.LOGIN_KEY + "='" + Models.Utility.ModelUtility.IsNull(infoKeyValue.Key) + "'";
                            createDBandTables.ExecuteDBQurey(sqlQuery);
                        }
                        else
                        {
                            sqlQuery = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_LOGIN_SETTINGS
                                + " ( " + DBConstants.LOGIN_KEY + "," + DBConstants.LOGIN_VALUE + ")"
                                + " SELECT '" + infoKeyValue.Key + "','" + Models.Utility.ModelUtility.IsNull(infoKeyValue.Val) + "'";
                            createDBandTables.ExecuteDBQurey(sqlQuery);
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in InsertRingLoginSetting class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + sqlQuery);
                }
                finally
                {
                    isRunning = false;
                }
            }
        }
    }
}

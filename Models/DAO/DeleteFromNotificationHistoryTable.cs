﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Models.DAO
{
    public class DeleteFromNotificationHistoryTable
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(DeleteFromNotificationHistoryTable).Name);

        private Guid notificationId;
        private List<Guid> notificationIds;

        public DeleteFromNotificationHistoryTable(Guid notificationId)
        {
            this.notificationId = notificationId;
            Thread thread = new Thread(new ThreadStart(Run));
            thread.Start();
        }

        public DeleteFromNotificationHistoryTable(List<Guid> notificationIds)
        {
            this.notificationIds = notificationIds;
            Thread thread = new Thread(new ThreadStart(DeleteMultiple));
            thread.Start();
        }

        private void DeleteMultiple()
        {
            string query = string.Empty;
            string values = string.Empty;
            try
            {
                foreach (var id in notificationIds)
                {
                    values += string.IsNullOrEmpty(values) ? string.Empty : ",";
                    values += string.Format("'{0}'", id.ToString());
                }
                query = string.Format("DELETE FROM {0} WHERE {1} IN ({2}) AND {3} = {4}",
                                        DBConstants.TABLE_RING_NOTIFICATION_HISTORY,
                                        DBConstants.ID, values,
                                        DBConstants.LOGIN_TABLE_ID, DefaultSettings.LOGIN_TABLE_ID);

                CreateDBandTables createDBandTables = new CreateDBandTables();
                createDBandTables.ConnectToDatabase();
                createDBandTables.ExecuteDBQurey(query);
            }
            catch (Exception e)
            {
                log.Error("ERROR DELETE MULTIPLE NOTIFICATION ==> " + e.StackTrace);
            }
        }

        private void Run()
        {
            String query = String.Empty;
            try
            {
                CreateDBandTables createDBandTables = new CreateDBandTables();
                createDBandTables.ConnectToDatabase();

                query = "DELETE FROM  " + DBConstants.TABLE_RING_NOTIFICATION_HISTORY
                            + " WHERE "
                            + DBConstants.ID + "= '" + notificationId.ToString() + "' AND "
                            + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID + "";
                createDBandTables.ExecuteDBQurey(query);

            }
            catch (Exception e)
            {
                log.Error("SQLException in DeleteFromNotificationHistoryTable class ==> " + e.Message + "\n" + e.StackTrace + ", Query = " + query);
            }

        }
    }
}

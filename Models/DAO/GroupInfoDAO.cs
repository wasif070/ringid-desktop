﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Utility;
using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Threading;

namespace Models.DAO
{
    public class GroupInfoDAO
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(GroupInfoDAO).Name);

        public static List<GroupInfoDTO> GetAllGroupInfo(long? groupID = null)
        {
            string masterQuery = String.Empty;
            string detailQuery = String.Empty;

            List<GroupInfoDTO> masterDTOs = new List<GroupInfoDTO>();
            try
            {
                CreateDBandTables CreateDB = new CreateDBandTables();
                CreateDB.ConnectToDatabase();

                masterQuery = "SELECT "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.GROUP_ID + ", "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.GROUP_NAME + ", "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.GROUP_PROFILE_IMAGE + ", "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.NUMBER_OF_MEMBERS + ", "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IS_PARTIAL + ", "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_MIN_PACKET_ID + ", "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_MAX_PACKET_ID + ", "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_BG_URL + ", "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_SOUND + ", "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_NOTIFICATION + ", "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_POPUP + " "
                        + " FROM " + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                        + (groupID != null ? " WHERE " + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.GROUP_ID + " = " + groupID : "");

                using (SQLiteCommand masterCommand = new SQLiteCommand(masterQuery, CreateDB.GetConnection()))
                {
                    SQLiteDataReader masterDr = masterCommand.ExecuteReader();
                    while (masterDr.Read())
                    {
                        GroupInfoDTO groupInfoDTO = new GroupInfoDTO();
                        groupInfoDTO.GroupID = masterDr.GetInt64(0);
                        groupInfoDTO.GroupName = masterDr.GetString(1);
                        groupInfoDTO.GroupProfileImage = masterDr.GetString(2);
                        groupInfoDTO.NumberOfMembers = masterDr.GetInt32(3);
                        groupInfoDTO.IsPartial = masterDr.GetBoolean(4);
                        groupInfoDTO.ChatMinPacketID = masterDr.GetString(5);
                        groupInfoDTO.ChatMaxPacketID = masterDr.GetString(6);
                        groupInfoDTO.ChatBgUrl = masterDr.GetString(7);
                        groupInfoDTO.ImSoundEnabled = masterDr.GetBoolean(8);
                        groupInfoDTO.ImNotificationEnabled = masterDr.GetBoolean(9);
                        groupInfoDTO.ImPopupEnabled = masterDr.GetBoolean(10);
                        masterDTOs.Add(groupInfoDTO);
                    }

                    masterDTOs.ForEach(i =>
                    {
                        try
                        {
                            detailQuery = "SELECT "
                                    + DBConstants.GROUP_ID + ", "
                                    + DBConstants.USER_TABLE_ID + ", "
                                    + DBConstants.RING_ID + ", "
                                    + DBConstants.FULL_NAME + ", "
                                    + DBConstants.MEMBER_ACCESS_TYPE + ", "
                                    + DBConstants.MEMBER_ADDED_BY + " "
                                    + " FROM " + DBConstants.TABLE_RING_GROUP_MEMBER_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                    + " WHERE "
                                    + DBConstants.GROUP_ID + " = " + i.GroupID;

                            using (SQLiteCommand detailCommand = new SQLiteCommand(detailQuery, CreateDB.GetConnection()))
                            {
                                SQLiteDataReader detailDr = detailCommand.ExecuteReader();
                                while (detailDr.Read())
                                {
                                    GroupMemberInfoDTO groupMemberInfoDTO = new GroupMemberInfoDTO();
                                    groupMemberInfoDTO.GroupID = i.GroupID;
                                    groupMemberInfoDTO.UserTableID = detailDr.GetInt64(1);
                                    groupMemberInfoDTO.RingID = detailDr.GetInt64(2);
                                    groupMemberInfoDTO.FullName = detailDr.GetString(3);
                                    groupMemberInfoDTO.MemberAccessType = detailDr.GetInt16(4);
                                    groupMemberInfoDTO.MemberAddedBy = detailDr.GetInt64(5);
                                    i.MemberList.Add(groupMemberInfoDTO);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            log.Error("SQLException in GetAllGroupInfo() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + detailQuery);
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in GetAllGroupInfo() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + masterQuery);
            }

            return masterDTOs;
        }

        public static GroupInfoDTO GetGroupInfoDTOByGroupID(long groupID)
        {
            string masterQuery = String.Empty;
            try
            {
                CreateDBandTables CreateDB = new CreateDBandTables();
                CreateDB.ConnectToDatabase();

                masterQuery = "SELECT "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.GROUP_ID + ", "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.GROUP_NAME + ", "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.GROUP_PROFILE_IMAGE + ", "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.NUMBER_OF_MEMBERS + ", "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IS_PARTIAL + ", "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_MIN_PACKET_ID + ", "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_MAX_PACKET_ID + ", "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_BG_URL + ", "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_SOUND + ", "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_NOTIFICATION + ", "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_POPUP + " "
                        + " FROM " + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                        + " WHERE "
                        + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.GROUP_ID + " = " + groupID + " ";

                using (SQLiteCommand command = new SQLiteCommand(masterQuery, CreateDB.GetConnection()))
                {
                    SQLiteDataReader masterDr = command.ExecuteReader();
                    if (masterDr.Read())
                    {
                        GroupInfoDTO groupInfoDTO = new GroupInfoDTO();
                        groupInfoDTO.GroupID = masterDr.GetInt64(0);
                        groupInfoDTO.GroupName = masterDr.GetString(1);
                        groupInfoDTO.GroupProfileImage = masterDr.GetString(2);
                        groupInfoDTO.NumberOfMembers = masterDr.GetInt32(3);
                        groupInfoDTO.IsPartial = masterDr.GetBoolean(4);
                        groupInfoDTO.ChatMinPacketID = masterDr.GetString(5);
                        groupInfoDTO.ChatMaxPacketID = masterDr.GetString(6);
                        groupInfoDTO.ChatBgUrl = masterDr.GetString(7);
                        groupInfoDTO.ImSoundEnabled = masterDr.GetBoolean(8);
                        groupInfoDTO.ImNotificationEnabled = masterDr.GetBoolean(9);
                        groupInfoDTO.ImPopupEnabled = masterDr.GetBoolean(10);
                        return groupInfoDTO;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in GetGroupInfoDTOByGroupID() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + masterQuery);
            }
            return null;
        }

        public static List<GroupInfoDTO> GetGroupInfoDTOsByGroupIDs(List<long> groupIDs)
        {
            List<GroupInfoDTO> groupInfoDTOs = new List<GroupInfoDTO>();
            if (groupIDs.Count > 0)
            {
                string masterQuery = String.Empty;

                try
                {
                    CreateDBandTables CreateDB = new CreateDBandTables();
                    CreateDB.ConnectToDatabase();

                    masterQuery = "SELECT "
                            + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.GROUP_ID + ", "
                            + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.GROUP_NAME + ", "
                            + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.GROUP_PROFILE_IMAGE + ", "
                            + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.NUMBER_OF_MEMBERS + ", "
                            + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IS_PARTIAL + ", "
                            + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_MIN_PACKET_ID + ", "
                            + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_MAX_PACKET_ID + ", "
                            + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_BG_URL + ", "
                            + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_SOUND + ", "
                            + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_NOTIFICATION + ", "
                            + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_POPUP + " "
                            + " FROM " + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + " "
                            + " WHERE "
                            + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.GROUP_ID + " IN (" + String.Join(",", groupIDs) + ") ";

                    using (SQLiteCommand masterCommand = new SQLiteCommand(masterQuery, CreateDB.GetConnection()))
                    {
                        SQLiteDataReader masterDr = masterCommand.ExecuteReader();
                        while (masterDr.Read())
                        {
                            GroupInfoDTO groupInfoDTO = new GroupInfoDTO();
                            groupInfoDTO.GroupID = masterDr.GetInt64(0);
                            groupInfoDTO.GroupName = masterDr.GetString(1);
                            groupInfoDTO.GroupProfileImage = masterDr.GetString(2);
                            groupInfoDTO.NumberOfMembers = masterDr.GetInt32(3);
                            groupInfoDTO.IsPartial = masterDr.GetBoolean(4);
                            groupInfoDTO.ChatMinPacketID = masterDr.GetString(5);
                            groupInfoDTO.ChatMaxPacketID = masterDr.GetString(6);
                            groupInfoDTO.ChatBgUrl = masterDr.GetString(7);
                            groupInfoDTO.ImSoundEnabled = masterDr.GetBoolean(8);
                            groupInfoDTO.ImNotificationEnabled = masterDr.GetBoolean(9);
                            groupInfoDTO.ImPopupEnabled = masterDr.GetBoolean(10);
                            groupInfoDTOs.Add(groupInfoDTO);
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in GetGroupInfoDTOsByGroupIDs() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + masterQuery);
                }
            }
            return groupInfoDTOs;
        }

        public static List<GroupMemberInfoDTO> GetAllGroupMemberListByGroupID(long groupID)
        {
            string detailQuery = String.Empty;
            List<GroupMemberInfoDTO> masterDTOs = new List<GroupMemberInfoDTO>();
            try
            {
                CreateDBandTables CreateDB = new CreateDBandTables();
                CreateDB.ConnectToDatabase();

                detailQuery = "SELECT "
                                    + DBConstants.GROUP_ID + ", "
                                    + DBConstants.USER_TABLE_ID + ", "
                                    + DBConstants.RING_ID + ", "
                                    + DBConstants.FULL_NAME + ", "
                                    + DBConstants.MEMBER_ACCESS_TYPE + ", "
                                    + DBConstants.MEMBER_ADDED_BY + " "
                                    + " FROM " + DBConstants.TABLE_RING_GROUP_MEMBER_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                    + " WHERE "
                                    + DBConstants.GROUP_ID + " = " + groupID;
                using (SQLiteCommand command = new SQLiteCommand(detailQuery, CreateDB.GetConnection()))
                {
                    SQLiteDataReader detailDr = command.ExecuteReader();
                    while (detailDr.Read())
                    {
                        GroupMemberInfoDTO groupMemberInfoDTO = new GroupMemberInfoDTO();
                        groupMemberInfoDTO.GroupID = groupID;
                        groupMemberInfoDTO.UserTableID = detailDr.GetInt64(1);
                        groupMemberInfoDTO.RingID = detailDr.GetInt64(2);
                        groupMemberInfoDTO.FullName = detailDr.GetString(3);
                        groupMemberInfoDTO.MemberAccessType = detailDr.GetInt16(4);
                        groupMemberInfoDTO.MemberAddedBy = detailDr.GetInt64(5);
                        masterDTOs.Add(groupMemberInfoDTO);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in GetAllGroupMemberListByGroupID() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + detailQuery);
            }
            return masterDTOs;
        }

        public static void UpdateGroupName(long groupID, string groupName)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " SET "
                                + DBConstants.GROUP_NAME + "='" + ModelUtility.IsNull(groupName) + "' "
                                + " WHERE "
                                + DBConstants.GROUP_ID + "=" + groupID + "";

                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();
                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in UpdateGroupName() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

        public static void UpdateGroupProfileImage(long groupID, string imageUrl)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " SET "
                                + DBConstants.GROUP_PROFILE_IMAGE + "='" + ModelUtility.IsNull(imageUrl) + "' "
                                + " WHERE "
                                + DBConstants.GROUP_ID + "=" + groupID + "";

                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();
                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in UpdateGroupProfileImage() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

        public static void UpdateGroupNameProfileImageAndNumberOfMember(long groupID, string groupName = null, string imageUrl = null, int? numberOfMember = null)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    bool hasGroupName = !String.IsNullOrWhiteSpace(groupName);
                    bool hasGroupImage = !String.IsNullOrWhiteSpace(imageUrl);
                    bool hasGroupMemberCount = numberOfMember != null;
                    if (hasGroupName || hasGroupImage || hasGroupMemberCount)
                    {
                        query = "UPDATE " + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " SET "
                                + (hasGroupName ? DBConstants.GROUP_NAME + "='" + ModelUtility.IsNull(groupName) + "' " + (hasGroupImage || hasGroupMemberCount ? "AND " : "") : "")
                                + (hasGroupImage ? DBConstants.GROUP_PROFILE_IMAGE + "='" + ModelUtility.IsNull(imageUrl) + "'" + (hasGroupMemberCount ? "AND " : "") : "")
                                + (hasGroupMemberCount ? DBConstants.NUMBER_OF_MEMBERS + "=" + numberOfMember + " " : "")
                                + " WHERE "
                                + DBConstants.GROUP_ID + "=" + groupID + ""; ;

                        CreateDBandTables createDB = new CreateDBandTables();
                        createDB.ConnectToDatabase();
                        createDB.ExecuteDBQurey(query);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in UpdateGroupNameProfileImageAndNumberOfMember() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

        public static void UpdateGroupMemberAccessType(long groupID, long memberID, int accessType)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_GROUP_MEMBER_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " SET "
                                + DBConstants.MEMBER_ACCESS_TYPE + "=" + accessType + " "
                                + " WHERE "
                                + DBConstants.USER_TABLE_ID + "=" + memberID + " AND "
                                + DBConstants.GROUP_ID + "=" + groupID + "";

                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();
                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in UpdateGroupMemberAccessType() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

        public static void UpdateNumberOfMembers(long groupID, int numberOfMembers)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " SET "
                                + DBConstants.NUMBER_OF_MEMBERS + "=" + numberOfMembers + " "
                                + " WHERE "
                                + DBConstants.GROUP_ID + "=" + groupID + "";

                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();
                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in UpdateNumberOfMembers() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

        public static void UpdateChatBackground(long groupID, string chatBgUrl)
        {
            new Thread(() =>
            {
                string query = String.Empty;

                try
                {
                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();

                    query = "UPDATE " + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " SET "
                                + DBConstants.CHAT_BG_URL + "='" + ModelUtility.IsNull(chatBgUrl) + "' "
                                + " WHERE "
                                + DBConstants.GROUP_ID + "=" + groupID + " ";

                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in UpdateChatBackground() ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

        public static void UpdateChatHistoryTime(long groupID, string chatMinPacketID, string chatMaxPacketID)
        {
            string query = String.Empty;

            try
            {
                CreateDBandTables createDB = new CreateDBandTables();
                createDB.ConnectToDatabase();

                query = "UPDATE " + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                        + " SET "
                        + DBConstants.CHAT_MIN_PACKET_ID + "='" + chatMinPacketID + "', "
                        + DBConstants.CHAT_MAX_PACKET_ID + "='" + chatMaxPacketID + "' "
                        + " WHERE "
                        + DBConstants.GROUP_ID + "=" + groupID + " ";

                createDB.ExecuteDBQurey(query);
            }
            catch (Exception ex)
            {
                log.Error("SQLException in UpdateChatHistoryTime() ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
        }

        public static void UpdateIMNotification(long groupID, bool isEnable)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " SET "
                                + DBConstants.IM_NOTIFICATION + "=" + ModelUtility.IsBoolean(isEnable) + " "
                                + " WHERE "
                                + DBConstants.GROUP_ID + "=" + groupID + "";

                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();
                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in UpdateIMNotification() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

        public static void UpdateIMSound(long groupID, bool isEnabled)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " SET "
                                + DBConstants.IM_SOUND + "=" + ModelUtility.IsBoolean(isEnabled) + " "
                                + " WHERE "
                                + DBConstants.GROUP_ID + "=" + groupID + "";

                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();
                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in UpdateIMSound() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

        public static void UpdateIMPopUp(long groupID, bool isEnabled)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_GROUP_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " SET "
                                + DBConstants.IM_POPUP + "=" + ModelUtility.IsBoolean(isEnabled) + " "
                                + " WHERE "
                                + DBConstants.GROUP_ID + "=" + groupID + "";

                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();
                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in UpdateIMPopUp() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

    }
}

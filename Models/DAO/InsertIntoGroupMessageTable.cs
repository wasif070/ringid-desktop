﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Models.DAO
{
    public class InsertIntoGroupMessageTable
    {
        private readonly ILog log = LogManager.GetLogger(typeof(InsertIntoGroupMessageTable).Name);

        Thread t = null;
        public delegate void CompleteHandler();
        public event CompleteHandler OnComplete;
        private List<MessageDTO> _MessageDTOs;

        public InsertIntoGroupMessageTable(List<MessageDTO> messageDTOs)
        {
            this._MessageDTOs = messageDTOs;
        }

        public void Start()
        {
            t = new Thread(param => Preocess());
            t.Name = this.GetType().Name;
            t.Start();
        }

        internal void Preocess()
        {
            try
            {
                if (this._MessageDTOs != null && this._MessageDTOs.Count > 0)
                {
                    this._MessageDTOs = this._MessageDTOs.ToList();
                    if (this._MessageDTOs.Count > 500)
                    {
                        List<MessageDTO> msgDTOs = new List<MessageDTO>();
                        foreach (MessageDTO msg in this._MessageDTOs)
                        {
                            msgDTOs.Add(msg);
                            if (msgDTOs.Count >= 500)
                            {
                                InsertOrUpdate(msgDTOs);
                                msgDTOs = new List<MessageDTO>();
                            }
                        }

                        if (msgDTOs.Count > 0)
                        {
                            InsertOrUpdate(msgDTOs);
                        }
                    }
                    else
                    {
                        InsertOrUpdate(_MessageDTOs);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in Preocess() ==> " + ex.Message + "\n" + ex.StackTrace);
            }

            if (OnComplete != null)
            {
                Thread.Sleep(100);
                OnComplete();
            }
        }

        private void InsertOrUpdate(List<MessageDTO> messageDTOs)
        {
            string insertUpdateQuery = String.Empty;
            string updateQuery = String.Empty;
            string query = String.Empty;

            try
            {
                if (messageDTOs != null && messageDTOs.Count > 0)
                {
                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();

                    insertUpdateQuery = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_GROUP_CHAT + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " ( "
                                + DBConstants.GROUP_ID + ", "
                                + DBConstants.SENDER_TABLE_ID + ", "
                                + DBConstants.PACKET_TYPE + ", "
                                + DBConstants.MESSAGE_TYPE + ", "
                                + DBConstants.MESSAGE + ", "
                                + DBConstants.MESSAGE_DATE + ", "
                                + DBConstants.MESSAGE_DELIEVER_DATE + ", "
                                + DBConstants.MESSAGE_SEEN_DATE + ", "
                                + DBConstants.MESSAGE_VIEW_DATE + ", "
                                + DBConstants.STATUS + ", "
                                + DBConstants.FILE_ID + ", "
                                + DBConstants.FILE_STATUS + ", "
                                + DBConstants.PACKET_ID + ", "
                                + DBConstants.IS_UNREAD + " "
                                + " ) ";

                    int prevTransType = 1;

                    messageDTOs.ForEach(i =>
                    {
                        if (prevTransType == 1)
                        {
                            query += insertUpdateQuery;
                        }

                        query += "SELECT "
                        + i.GroupID + ", "
                        + i.SenderTableID + ", "
                        + i.PacketType + ", "
                        + i.MessageType + ", "
                        + "'" + ModelUtility.IsExceded(i.OriginalMessage, 32000) + "', "
                        + i.MessageDate + ", "
                        + i.MessageDelieverDate + ", "
                        + i.MessageSeenDate + ", "
                        + i.MessageViewDate + ", "
                        + i.Status + ", "
                        + i.FileID + ", "
                        + i.FileStatus + ", "
                        + "'" + i.PacketID + "', "
                        + ModelUtility.IsBoolean(i.IsUnread) + " "
                        + " UNION ";
                        prevTransType = 0;
                    });

                    if (prevTransType == 0)
                    {
                        query = query.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' }) + "; ";
                    }
                    createDB.ExecuteDBQurey(query);
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in InsertOrUpdate() ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
        }


    }
}

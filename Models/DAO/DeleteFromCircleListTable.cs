﻿using log4net;
using Models.Constants;
using System;
using System.Threading;

namespace Models.DAO
{
    public class DeleteFromCircleListTable
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(DeleteFromCircleListTable).Name);

        private long circleId;
        public DeleteFromCircleListTable(long circleId)
        {
            this.circleId = circleId;
            Thread thread = new Thread(new ThreadStart(Run));
            thread.Start();
        }

        private void Run()
        {
            String query = String.Empty;
            try
            {
                CreateDBandTables createDBandTables = new CreateDBandTables();
                createDBandTables.ConnectToDatabase();

                query = "DELETE FROM  " + DBConstants.TABLE_RING_CIRCLE_LIST
                            + " WHERE "
                            + DBConstants.CIRCLE_ID + "=" + circleId + " AND "
                            + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID + "";
                createDBandTables.ExecuteDBQurey(query);
            }
            catch (Exception)
            {
               
            }

        }
    }
}

﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Threading;

namespace Models.DAO
{
    public class InsertIntoCircleMemberTable
    {
        readonly ILog log = LogManager.GetLogger(typeof(InsertIntoCircleMemberTable).Name);
        //private List<CircleMemberDTO> circleMemberDtos;
        //private bool IsFromTmpDictionary = false;
        private CircleMemberDTO circleMemberDTO;
        private List<CircleMemberDTO> insertList;
        
        public InsertIntoCircleMemberTable(List<CircleMemberDTO> insertList)
        {
            this.insertList = insertList;
            Thread thread = new Thread(new ThreadStart(ProcessAllDTO));
            thread.Name = this.GetType().Name;
            thread.Start();
        }

        private void ProcessAllDTO()
        {
            foreach (CircleMemberDTO dto in insertList)
            {
                this.circleMemberDTO = dto;
                this.ProcessDTO();
            }
        }

        public InsertIntoCircleMemberTable(CircleMemberDTO circleMemberDTO)
        {
            this.circleMemberDTO = circleMemberDTO;
            this.ProcessDTO();
        }

        private void ProcessDTO()
        {
            string query = string.Empty;
            try
            {
                CreateDBandTables createDBandTables = new CreateDBandTables();
                createDBandTables.ConnectToDatabase();

                query = string.Format("SELECT * FROM {0} WHERE {1} = @loginTableId AND {2} = @circleId AND {3} = @tableId",
                                        DBConstants.TABLE_RING_CIRCLE_MEMBERS_LIST,
                                        DBConstants.LOGIN_TABLE_ID,
                                        DBConstants.CIRCLE_ID,
                                        DBConstants.USER_TABLE_ID);

                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                command.CommandText = query;
                command.Parameters.AddWithValue("@loginTableId", Convert.ToInt32(DefaultSettings.LOGIN_TABLE_ID));
                command.Parameters.AddWithValue("@circleId", Convert.ToInt32(circleMemberDTO.CircleId));
                command.Parameters.AddWithValue("@tableId", Convert.ToInt32(circleMemberDTO.UserTableID));
                SQLiteDataReader dataReader = command.ExecuteReader();

                if (dataReader.HasRows == false) // no row so insert if ists != 0
                {
                    try
                    {
                        using (SQLiteCommand insCommand = new SQLiteCommand(createDBandTables.GetConnection()))
                        {
                            insCommand.CommandText = string.Format("INSERT OR REPLACE INTO {0} ({1}, {2}, {3}, {4}, {5}, {6}, {7} ) VALUES ( @loginTableId, @circleId, @admin, @ringId, @updateTime, @tableId, @integerStatus)",
                                                                    DBConstants.TABLE_RING_CIRCLE_MEMBERS_LIST, DBConstants.LOGIN_TABLE_ID, DBConstants.CIRCLE_ID, DBConstants.ADMIN, DBConstants.RING_ID,
                                                                    DBConstants.UPDATE_TIME, DBConstants.USER_TABLE_ID, DBConstants.INTEGER_STATUS);
                            insCommand.Parameters.AddWithValue("@loginTableId", Convert.ToInt32(DefaultSettings.LOGIN_TABLE_ID));
                            insCommand.Parameters.AddWithValue("@circleId", Convert.ToInt32(circleMemberDTO.CircleId));
                            insCommand.Parameters.AddWithValue("@admin", Convert.ToInt32((circleMemberDTO.IsAdmin ? 1 : 0)));
                            insCommand.Parameters.AddWithValue("@ringId", Convert.ToInt32(circleMemberDTO.RingID));
                            insCommand.Parameters.AddWithValue("@updateTime", Convert.ToInt64(circleMemberDTO.UpdateTime));
                            insCommand.Parameters.AddWithValue("@tableId", Convert.ToInt32(circleMemberDTO.UserTableID));
                            insCommand.Parameters.AddWithValue("@integerStatus", Convert.ToInt32(circleMemberDTO.IntegerStatus));

                            insCommand.ExecuteNonQuery();
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("Error Insert Into Circle Member List ==> " + ex.Message + "\n" + ex.StackTrace);
                    }
                }
                else // row exist update if ut higher or delete if ists = 1
                {
                    long ut = 0;
                    while (dataReader.Read()) { ut = dataReader.GetInt32(dataReader.GetOrdinal(DBConstants.UPDATE_TIME)); }
                    if (circleMemberDTO.UpdateTime > ut) // check if ut higher then update
                    {
                        try
                        {
                            using (SQLiteCommand updateCommand = new SQLiteCommand(createDBandTables.GetConnection()))
                            {
                                query = string.Format("UPDATE {0} SET {1} = @updateTime, {2} = @admin, {3} = @ringId, {4} = @integerStatus WHERE {5} = @loginTableId AND {6} = @circleId AND {7} = @tableId",
                                                              DBConstants.TABLE_RING_CIRCLE_MEMBERS_LIST,
                                                              DBConstants.UPDATE_TIME,
                                                              DBConstants.ADMIN,
                                                              DBConstants.RING_ID,
                                                              DBConstants.INTEGER_STATUS,
                                                              DBConstants.LOGIN_TABLE_ID,
                                                              DBConstants.CIRCLE_ID,
                                                              DBConstants.USER_TABLE_ID);
                                updateCommand.CommandText = query;
                                updateCommand.Parameters.AddWithValue("@updateTime", Convert.ToInt64(circleMemberDTO.UpdateTime));
                                updateCommand.Parameters.AddWithValue("@admin", Convert.ToInt32(circleMemberDTO.IsAdmin ? 1 : 0));
                                updateCommand.Parameters.AddWithValue("@ringId", Convert.ToInt32(circleMemberDTO.RingID));
                                updateCommand.Parameters.AddWithValue("@integerStatus", Convert.ToInt32(circleMemberDTO.IntegerStatus));
                                updateCommand.Parameters.AddWithValue("@loginTableId", Convert.ToInt32(DefaultSettings.LOGIN_TABLE_ID));
                                updateCommand.Parameters.AddWithValue("@circleId", Convert.ToInt32(circleMemberDTO.CircleId));
                                updateCommand.Parameters.AddWithValue("@tableId", Convert.ToInt32(circleMemberDTO.UserTableID));

                                updateCommand.ExecuteNonQuery();
                            }
                        }
                        catch (Exception ex)
                        {
                            log.Error("SQLException in InsertIntoCircleMemberTable class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in InsertIntoCircleMemberTable class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
        }
    }
}

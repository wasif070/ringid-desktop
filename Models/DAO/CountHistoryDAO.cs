﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.DAO
{
    public class CountHistoryDAO
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CountHistoryDAO).Name);

        private CreateDBandTables createDBandTables;
        public CountHistoryDAO()
        {
            createDBandTables = new CreateDBandTables();
            createDBandTables.ConnectToDatabase();
        }
        private static CountHistoryDAO _Instance;
        public static CountHistoryDAO Instance
        {
            get { return _Instance = _Instance ?? new CountHistoryDAO(); }
            set { _Instance = value; }
        }

        public void LoadCountValuesOfNotifications()
        {
            string query = String.Empty;
            try
            {
                query = "SELECT * FROM "
                       + DBConstants.TABLE_RING_NOTIFICATION_COUNT + " "
                       + "WHERE "
                       + DBConstants.LOGIN_TABLE_ID + " = " + DefaultSettings.LOGIN_TABLE_ID + " ";

                SQLiteCommand command = new SQLiteCommand(query, createDBandTables.GetConnection());
                SQLiteDataReader row = command.ExecuteReader();
                while (row.Read())
                {
                    //AppConstants.CALL_NOTIFICATION_COUNT = Convert.ToInt32(row[DBConstants.COUNT_CALL_NOTIFICATION]);
                    //AppConstants.CHAT_NOTIFICATION_COUNT = Convert.ToInt32(row[DBConstants.COUNT_CHAT_NOTIFICATION]);
                    AppConstants.ALL_NOTIFICATION_COUNT = Convert.ToInt32(row[DBConstants.COUNT_ALL_NOTIFICATION]);
                    AppConstants.ADD_FRIEND_NOTIFICATION_COUNT = Convert.ToInt32(row[DBConstants.COUNT_ADD_FRIEND_NOTIFICATION]);
                }

            }
            catch (Exception e)
            {
                log.Error("Exception in LoadCountValuesOfNotifications ==> " + e.Message + "\n" + e.StackTrace + ", Query = " + query);
            }
        }
    }
}

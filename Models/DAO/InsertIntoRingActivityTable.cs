﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Models.DAO
{
    public class InsertIntoRingActivityTable
    {

        private readonly ILog log = LogManager.GetLogger(typeof(InsertIntoRingActivityTable).Name);

        Thread t = null;
        public delegate void CompleteHandler();
        public event CompleteHandler OnComplete;
        private List<ActivityDTO> _ActivityDTOs;

        public InsertIntoRingActivityTable(List<ActivityDTO> activityDTOs)
        {
            this._ActivityDTOs = activityDTOs;
        }

        public void Start()
        {
            t = new Thread(param => Preocess());
            t.Name = this.GetType().Name;
            t.Start();
        }

        public void Run()
        {
            Preocess();
        }

        internal void Preocess()
        {
            try
            {
                if (this._ActivityDTOs != null && this._ActivityDTOs.Count > 0)
                {
                    this._ActivityDTOs = this._ActivityDTOs.ToList();
                    if (this._ActivityDTOs.Count > 500)
                    {
                        List<ActivityDTO> msgDTOs = new List<ActivityDTO>();
                        foreach (ActivityDTO msg in this._ActivityDTOs)
                        {
                            msgDTOs.Add(msg);
                            if (msgDTOs.Count >= 500)
                            {
                                InsertOrUpdate(msgDTOs);
                                msgDTOs = new List<ActivityDTO>();
                            }
                        }

                        if (msgDTOs.Count > 0)
                        {
                            InsertOrUpdate(msgDTOs);
                        }
                    }
                    else
                    {
                        InsertOrUpdate(_ActivityDTOs);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in Preocess() ==> " + ex.Message + "\n" + ex.StackTrace);
            }

            if (OnComplete != null)
            {
                Thread.Sleep(100);
                OnComplete();
            }
        }

        private void InsertOrUpdate(List<ActivityDTO> activityDTOs)
        {
            string insertUpdateQuery = String.Empty;
            string deleteQuery = String.Empty;
            string query = String.Empty;

            try
            {
                if (activityDTOs != null && activityDTOs.Count > 0)
                {
                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();
                    query = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_ACTIVITY + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " ( "
                                + DBConstants.FRIEND_TABLE_ID + ", "
                                + DBConstants.GROUP_ID + ", "
                                + DBConstants.ACTIVITY_TYPE + ", "
                                + DBConstants.MESSAGE_TYPE + ", "
                                + DBConstants.ACTIVITY_BY + ", "
                                + DBConstants.MEMBER_LIST + ", "
                                + DBConstants.GROUP_NAME + ", "
                                + DBConstants.GROUP_PROFILE_IMAGE + ", "
                                + DBConstants.UPDATE_TIME + ", "
                                + DBConstants.PACKET_ID + ", "
                                + DBConstants.IS_UNREAD + " "
                                + " ) ";

                    activityDTOs.ForEach(i =>
                    {
                        string members = String.Empty;
                        if (i.MemberList != null && i.MemberList.Count > 0)
                        {
                            try
                            {
                                JavaScriptSerializer oSerializer = new JavaScriptSerializer();
                                members = oSerializer.Serialize(i.MemberList);
                            }
                            catch (Exception)
                            {
                            }
                        }

                        query += "SELECT "
                            + "" + i.FriendTableID + ", "
                            + "" + i.GroupID + ", "
                            + "" + i.ActivityType + ", "
                            + "" + i.MessageType + ", "
                            + "" + i.ActivityBy + ", "
                            + "'" + ModelUtility.IsNull(members) + "', "
                            + "'" + ModelUtility.IsNull(i.GroupName) + "', "
                            + "'" + ModelUtility.IsNull(i.GroupProfileImage) + "', "
                            + "" + i.UpdateTime + ", "
                            + "'" + ModelUtility.IsNull(i.PacketID) + "', "
                            + "" + ModelUtility.IsBoolean(i.IsUnread) + " "
                            + " UNION ";
                    });

                    query = query.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' });
                    createDB.ExecuteDBQurey(query);
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in InsertOrUpdate() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
        }

    }
}

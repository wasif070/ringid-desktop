﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Text;
using log4net;
using Models.Constants;
namespace Models.DAO
{
    public class CreateDBandTables
    {
        readonly ILog log = LogManager.GetLogger(typeof(CreateDBandTables).Name);
        private static SQLiteConnection DBConnection;
        public SQLiteConnection GetConnection()
        {
            return DBConnection;
        }

        private void ShowErrorLog(string msg)
        {
            log.Error(msg);
        }

        public void CreateNewDatabase(string directory)
        {
            try
            {
                if (!File.Exists(directory + DBConstants.DB_NAME))
                {
                    SQLiteConnection.CreateFile(directory + DBConstants.DB_NAME);
                }
                DefaultSettings.DB_FILE = directory + DBConstants.DB_NAME;
            }
            catch (Exception ex)
            {
                ShowErrorLog("Can not create database  ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public bool IsConnectionOpen { get { return (DBConnection != null); } }

        // Creates a connection with our database file.

        public bool ConnectToDatabase()
        {
            if (DBConnection == null && !string.IsNullOrEmpty(DefaultSettings.DB_FILE))
            {
                try
                {
                    DBConnection = new SQLiteConnection("Data Source=" + DefaultSettings.DB_FILE + ";Version=3;");
                    DBConnection.SetPassword("desktopDB");
                    DBConnection.Open();
                }
                catch (Exception ex)
                {
                    ShowErrorLog("Error in Opening Database ==> " + ex.Message + "\n" + ex.StackTrace);
                    DBConnection.Close();
                    DBConnection = null;
                    return false;
                }
            }
            return true;
        }

        public void CloseConnection()
        {
            if (DBConnection != null)
            {
                try
                {
                    DBConnection.Dispose();
                    DBConnection.Close();
                }
                catch (Exception ex)
                {
                    ShowErrorLog("Error in Closing Database ==> " + ex.Message + "\n" + ex.StackTrace);
                }
            }
        }

        public SQLiteConnection GetConnectWithDatabase()
        {
            return new SQLiteConnection("Data Source=" + DefaultSettings.DB_FILE + ";Version=3;");
        }

        #region Create Database and Tables
        public void CreateAllTables()
        {
            /*
            16 tables for ringid desktopApp
             */
            try
            {
                CreateEmotionsTable();
                CreateInstantMessagesTable();
                CreateRingMarketStickerCollectionTable();
                CreateRingMarketStickerCategoriesTable();
                CreateRingMarketStickerTable();
                CreateRingAllUsersSettings();
                CreateUserBasicInfoTable();
                CreateCircleListTable();
                CreateCircleMemberListTable();
                CreateNotificationHistoryTable();
                CreateCallChatAddFriendNotificationCountTable();
                CreateDoingTable();
                CreateRecentMediaTable();
                CreateMediaSearchTable();
                CreateDownloadMediaTable();
                CreateChatBgListTable();
                CreateBlockedNonFriendTable();
                CreateRoomCategoryTable();
                CreateRingEventTable();
            }
            catch (Exception ex)
            {
                ShowErrorLog("Can not create tables==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void ExecuteDBQurey(string queryString)
        {
            using (SQLiteCommand command = new SQLiteCommand(queryString, DBConnection))
            {
                command.ExecuteNonQuery();
            }
        }

        public void InsertOrUpdateCommand(string queryString)
        {
            #region inserOrUpdate
            SQLiteConnection sqlConnection = null;
            SQLiteCommand cmd = null;
            SQLiteTransaction tr = null;
            try
            {
                sqlConnection = GetConnectWithDatabase();
                sqlConnection.Open();
                tr = sqlConnection.BeginTransaction();
                cmd = sqlConnection.CreateCommand();
                cmd.Transaction = tr;
                cmd.CommandText = queryString;
                cmd.ExecuteNonQuery();
                tr.Commit();
            }
            catch (SqliteException)
            {
                if (tr != null)
                {
                    try
                    {
                        tr.Rollback();

                    }
                    catch (SqliteException ex2)
                    {
                        ShowErrorLog(ex2.ToString());
                    }
                    finally
                    {
                        tr.Dispose();
                    }
                }
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }

                if (tr != null)
                {
                    tr.Dispose();
                }

                if (sqlConnection != null)
                {
                    try
                    {
                        sqlConnection.Close();

                    }
                    catch (SqliteException ex)
                    {
                        log.Error(ex.Message + "\n" + ex.StackTrace);
                    }
                    finally
                    {
                        sqlConnection.Dispose();
                    }
                }
            }
            #endregion
        }

        public void InsertOrUpdateCommandNeedtoRollback(List<string> queryStringList)
        {
            #region inserOrUpdate
            //SQLiteConnection sqlConnection = null;
            SQLiteCommand cmd = null;
            SQLiteTransaction tr = null;
            try
            {
                //sqlConnection = GetConnectWithDatabase();
                //sqlConnection.Open();
                tr = DBConnection.BeginTransaction();
                cmd = DBConnection.CreateCommand();
                cmd.Transaction = tr;
                foreach (string item in queryStringList)
                {
                    cmd.CommandText = item;
                    cmd.ExecuteNonQuery();
                }
                tr.Commit();
            }
            catch (SqliteException ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace);

                if (tr != null)
                {
                    try
                    {
                        tr.Rollback();

                    }
                    catch (SqliteException ex2)
                    {
                        ShowErrorLog(ex2.ToString());

                    }
                    finally
                    {
                        tr.Dispose();
                    }
                }
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }

                if (tr != null)
                {
                    tr.Dispose();
                }

                //if (sqlConnection != null)
                //{
                //    try
                //    {
                //        sqlConnection.Close();

                //    }
                //    catch (SqliteException ex)
                //    {
                //        log.Error(ex.Message + "\n" + ex.StackTrace);
                //    }
                //    finally
                //    {
                //        sqlConnection.Dispose();
                //    }
                //}
            }
            #endregion
        }

        public void CreateRingLoginSettingTable()
        {
            String sqlCreate = "CREATE TABLE IF NOT EXISTS " + DBConstants.TABLE_RING_LOGIN_SETTINGS
                                     + " ("
                                     + DBConstants.LOGIN_KEY + " VARCHAR(50) NOT NULL UNIQUE,"
                                     + DBConstants.LOGIN_VALUE + " VARCHAR(10000),"
                                     + " UNIQUE (" + DBConstants.LOGIN_KEY + ")"
                                     + ")";
            ExecuteDBQurey(sqlCreate);
        }

        public void CreateOthersSettingTable()
        {
            String sqlCreate = "CREATE TABLE IF NOT EXISTS " + DBConstants.TABLE_RING_OTHERS_SETTINGS
                                     + " ("
                                     + DBConstants.SETTINGS_KEY + " VARCHAR(50) NOT NULL,"
                                     + DBConstants.EMO_TYPE + " SMALLINT NOT NULL, "
                                     + " UNIQUE (" + DBConstants.SETTINGS_KEY + ")"
                                     + ")";
            ExecuteDBQurey(sqlCreate);
        }

        private void CreateEmotionsTable()
        {
            String sqlCreate = "CREATE TABLE IF NOT EXISTS " + DBConstants.TABLE_RING_EMOTICONS
                            + "("
                            + DBConstants.ID + " BIGINT PRIMARY KEY NOT NULL,"
                            + DBConstants.EMO_NAME + " VARCHAR(50) NOT NULL,"
                            + DBConstants.EMO_SYMBOL + " VARCHAR(50) NOT NULL UNIQUE,"
                            + DBConstants.EMO_URL + " VARCHAR(50) NOT NULL,"
                            + DBConstants.EMO_TYPE + " SMALLINT NOT NULL"
                            + ")";
            ExecuteDBQurey(sqlCreate);
        }

        public void CreateInstantMessagesTable()
        {
            String sqlCreate = "CREATE TABLE  IF NOT EXISTS " + DBConstants.TABLE_RING_INSTANT_MESSAGES
                                + "("
                                + DBConstants.INS_MSG + " VARCHAR(200) NOT NULL UNIQUE,"
                                + DBConstants.INS_MSG_TYPE + " SMALLINT"
                                + ")";
            ExecuteDBQurey(sqlCreate);
        }

        private void CreateRingMarketStickerCollectionTable()
        {
            String sqlCreate = "CREATE TABLE IF NOT EXISTS " + DBConstants.TABLE_RING_MARKET_STICKER_COLLECTION
                               + "("
                               + DBConstants.LOGIN_TABLE_ID + " BIGINT,"
                               + DBConstants.COLLECTION_ID + " INTEGER,"
                               + DBConstants.COLLECTION_NAME + " VARCHAR(150),"
                               + DBConstants.BANNER_IMAGE + " VARCHAR(100),"
                               + DBConstants.THEME_COLOR + " VARCHAR(100),"
                               + " UNIQUE (" + DBConstants.COLLECTION_ID + ", " + DBConstants.LOGIN_TABLE_ID + ")"
                               + ")";
            ExecuteDBQurey(sqlCreate);
        }

        private void CreateRingMarketStickerCategoriesTable()
        {
            String sqlCreate = "CREATE TABLE IF NOT EXISTS " + DBConstants.TABLE_RING_MARKET_STICKER_CATEGORY
                               + "("
                               + DBConstants.LOGIN_TABLE_ID + " BIGINT,"
                               + DBConstants.CATEGORY_ID + " INTEGER,"
                               + DBConstants.CATEGORY_NAME + " VARCHAR(150),"
                               + DBConstants.COLLECTION_ID + " INTEGER,"
                               + DBConstants.BANNER_IMAGE + " VARCHAR(100),"
                               + DBConstants.IS_BANNER_VISIBLE + " BOOLEAN NOT NULL DEFAULT 0,"
                               + DBConstants.RANK + " INTEGER,"
                               + DBConstants.DETAIL_IMAGE + " VARCHAR(100),"
                               + DBConstants.IS_NEW + " BOOLEAN NOT NULL DEFAULT 0,"
                               + DBConstants.ICON + " VARCHAR(100),"
                               + DBConstants.PRIZE + " FLOAT,"
                               + DBConstants.IS_FREE + " BOOLEAN NOT NULL DEFAULT 0,"
                               + DBConstants.IS_TOP + " BOOLEAN NOT NULL DEFAULT 0,"
                               + DBConstants.DESCRIPTION + " VARCHAR(1000),"
                               + DBConstants.IS_DOWNLOADED + " BOOLEAN NOT NULL DEFAULT 0,"
                               + DBConstants.DOWNLOAD_TIME + " BIGINT NOT NULL DEFAULT 0,"
                               + DBConstants.SORT_ORDER + " INTEGER NOT NULL DEFAULT 0,"
                               + DBConstants.IS_VISIBLE + " BOOLEAN NOT NULL DEFAULT 1,"
                               + DBConstants.IS_DEFAULT + " BOOLEAN NOT NULL DEFAULT 0,"
                               + DBConstants.LAST_USED_DATE + " BIGINT NOT NULL DEFAULT 0,"
                               + DBConstants.NEW_STICKER_IS_SEEN + " BOOLEAN NOT NULL DEFAULT 0,"
                               + " UNIQUE (" + DBConstants.CATEGORY_ID + ", " + DBConstants.LOGIN_TABLE_ID + ")"
                               + ")";
            ExecuteDBQurey(sqlCreate);
        }

        private void CreateRingMarketStickerTable()
        {
            String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_MARKET_STICKER
                                 + "("
                                 + DBConstants.LOGIN_TABLE_ID + " BIGINT,"
                                 + DBConstants.IMAGE_ID + " INTEGER,"
                                 + DBConstants.IMAGE_URL + " VARCHAR(50),"
                                 + DBConstants.COLLECTION_ID + " INTEGER NOT NULL DEFAULT 0,"
                                 + DBConstants.CATEGORY_ID + " INTEGER,"
                                 + DBConstants.IS_RECENT + " BOOLEAN NOT NULL DEFAULT 0,"
                                 + " UNIQUE (" + DBConstants.IMAGE_ID + ", " + DBConstants.CATEGORY_ID + ", " + DBConstants.LOGIN_TABLE_ID + ")"
                                 + ")";
            ExecuteDBQurey(sqlCreate);
        }

        private void CreateRingAllUsersSettings()
        {
            String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_ALL_USERS_SETTINGS
                               + "("
                               + DBConstants.LOGIN_TABLE_ID + " BIGINT,"
                               + DBConstants.SETTINGS_KEY + " VARCHAR(150) NOT NULL,"
                               + DBConstants.SETTINGS_VALUE + " VARCHAR(50) NOT NULL,"
                               + "  UNIQUE (" + DBConstants.LOGIN_TABLE_ID + ", " + DBConstants.SETTINGS_KEY
                               + "))";
            ExecuteDBQurey(sqlCreate);
        }

        private void CreateUserBasicInfoTable()
        {
            String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_USER_BASIC_INFO
                                + "  (" + ""
                                + DBConstants.USER_TABLE_ID + "    BIGINT,"
                                + DBConstants.RING_ID + "    BIGINT,"
                                + DBConstants.FULL_NAME + "    VARCHAR(200),"
                                + DBConstants.PROFILE_IMAGE + "   VARCHAR(250),"
                                + DBConstants.PROFILE_IMAGE_ID + "   VARCHAR(128),"
                                + DBConstants.COVER_IMAGE + "   VARCHAR(250),"
                                + DBConstants.COVER_IMAGE_ID + "    VARCHAR(128),"
                                + DBConstants.COVER_IMAGE_X + "    BIGINT,"
                                + DBConstants.COVER_IMAGE_Y + "    BIGINT,"
                                + DBConstants.PROFILE_IMAGE_PRIVACY + "    SMALLINT,"
                                + DBConstants.COVER_IMAGE_PRIVACY + "    SMALLINT,"
                                + DBConstants.DEVICE_TOKEN + "    VARCHAR(1100),"
                                + DBConstants.CONTACT_TYPE + "    SMALLINT,"
                                + " UNIQUE (" + DBConstants.USER_TABLE_ID + ")"
                                + ")";
            ExecuteDBQurey(sqlCreate);

            sqlCreate = "CREATE INDEX IF NOT EXISTS " + DBConstants.INDEX_USER_BASIC_INFO_USER_TABLE_ID + " ON " + DBConstants.TABLE_RING_USER_BASIC_INFO + " (" + DBConstants.USER_TABLE_ID + ")";
            ExecuteDBQurey(sqlCreate);

        }

        private void CreateCircleListTable()
        {
            try
            {
                string sqlCreateTable = string.Format("CREATE TABLE IF NOT EXISTS {0} ( {1} BIGINT, {2} BIGINT, {3} VARCHAR(100), {4} BIGINT, {5} INTEGER, {6} BIGINT DEFAULT 0, {7} BIGINT, {8} SMALLINT DEFAULT 0, {9} INTEGER NOT NULL DEFAULT 0, UNIQUE ({1}, {2}) )",
                                                       DBConstants.TABLE_RING_CIRCLE_LIST, //0
                                                       DBConstants.LOGIN_TABLE_ID,          //1
                                                       DBConstants.CIRCLE_ID,              //2
                                                       DBConstants.CIRCLE_NAME,            //3
                                                       DBConstants.SUPER_ADMIN,            //4
                                                       DBConstants.MEMBER_COUNT,           //5
                                                       DBConstants.UPDATE_TIME,            //6
                                                       DBConstants.USER_TABLE_ID,          //7
                                                       DBConstants.INTEGER_STATUS,          //8
                                                       DBConstants.ADMIN_COUNT              //9
                                                     );
                ExecuteDBQurey(sqlCreateTable);
            }
            catch (Exception)
            {
                //  log.Error("CREATE CIRCLE TABLE ==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void CreateCircleMemberListTable()
        {
            try
            {
                string sqlCreateTable = string.Format(@"CREATE TABLE IF NOT EXISTS {0}
                                                       ( 
                                                        {1} BIGINT NOT NULL,
                                                        {2} BIGINT NOT NULL,
                                                        {3} BIGINT NOT NULL,
                                                        {4} BIGINT,
                                                        {5} BOOLEAN,
                                                        {6} SMALLINT DEFAULT 0,
                                                        {7} BIGINT NOT NULL DEFAULT 0,
                                                        UNIQUE ({1}, {2}, {3})
                                                        )",
                                                          DBConstants.TABLE_RING_CIRCLE_MEMBERS_LIST, //{0}
                                                          DBConstants.LOGIN_TABLE_ID,                  //{1}
                                                          DBConstants.CIRCLE_ID,                      //{2}
                                                          DBConstants.USER_TABLE_ID,                  //{4}
                                                          DBConstants.RING_ID,                  //{3}
                                                          DBConstants.ADMIN,                          //{5}
                                                          DBConstants.INTEGER_STATUS,                 //{6}
                                                          DBConstants.UPDATE_TIME);                   //{7}
                ExecuteDBQurey(sqlCreateTable);
            }
            catch
            {

            }
        }

        private void CreateNotificationHistoryTable()
        {
            String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_NOTIFICATION_HISTORY
                                + "("
                                + DBConstants.ID + " VARCHAR(128),"
                                + DBConstants.LOGIN_TABLE_ID + " BIGINT,"
                                + DBConstants.NOTIFICATION_FRIENDID + " BIGINT,"
                                + DBConstants.NOTIFICATION_UT + " BIGINT,"
                                + DBConstants.NOTIFICATION_N_TYPE + " SMALLINT,"
                                + DBConstants.NOTIFICATION_ACTIVITY_ID + " BIGINT,"
                                + DBConstants.NOTIFICATION_NEWSFEED_ID + " VARCHAR(128),"
                                + DBConstants.NOTIFICATION_IMAGE_ID + " VARCHAR(128),"
                                + DBConstants.NOTIFICATION_IMAGE_URL + " VARCHAR(256) DEFAULT '',"
                                + DBConstants.NOTIFICATION_COMMENT_ID + " VARCHAR(128),"
                                + DBConstants.NOTIFICATION_MESSAGE_TYPE + " SMALLINT,"
                                + DBConstants.NOTIFICATION_FRIEND_NAME + " VARCHAR(200),"
                                + DBConstants.NOTIFICATION_NUMBER_OF_LIKECOMMENT + " BIGINT,"
                                + DBConstants.NOTIFICATION_IS_READ + " BOOLEAN,"
                                + "  UNIQUE (" + DBConstants.ID + ", " + DBConstants.LOGIN_TABLE_ID + "))";
            ExecuteDBQurey(sqlCreate);
        }

        private void CreateCallChatAddFriendNotificationCountTable()
        {
            String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_NOTIFICATION_COUNT
                    + "("
                    + DBConstants.LOGIN_TABLE_ID + " BIGINT,"
                    + DBConstants.COUNT_CALL_NOTIFICATION + " INTEGER,"
                    + DBConstants.COUNT_CHAT_NOTIFICATION + " INTEGER,"
                    + DBConstants.COUNT_ALL_NOTIFICATION + " INTEGER,"
                    + DBConstants.COUNT_ADD_FRIEND_NOTIFICATION + " INTEGER,"
                    + "  UNIQUE (" + DBConstants.LOGIN_TABLE_ID + ")"
                    + ")";
            ExecuteDBQurey(sqlCreate);
        }

        public void CreateDoingTable()
        {
            String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_DOING
                               + "("
                               + DBConstants.ID + " BIGINT PRIMARY KEY NOT NULL,"
                               + DBConstants.DOING_CATEGORY + " INT,"
                               + DBConstants.DOING_SORTID + " INT,"
                               + DBConstants.DOING_NAME + " VARCHAR(100),"
                               + DBConstants.DOING_URL + " VARCHAR(250),"
                               + DBConstants.DOING_UPDATE_TIME + " BIGINT"
                               + ")";
            ExecuteDBQurey(sqlCreate);
        }

        private void CreateMediaSearchTable()
        {
            String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_MEDIA_SEARCH
                                + "("
                                + DBConstants.LOGIN_TABLE_ID + " BIGINT,"
                                + DBConstants.MEDIA_SEARCH_SUGGESTION + " VARCHAR(100),"
                                + DBConstants.MEDIA_MEDIA_TYPE + " INT NOT NULL DEFAULT 0,"
                                + DBConstants.UPDATE_TIME + " BIGINT NOT NULL DEFAULT 0,"
                                + "  UNIQUE (" + DBConstants.LOGIN_TABLE_ID + ", " + DBConstants.MEDIA_SEARCH_SUGGESTION + ")"
                                + ")";
            ExecuteDBQurey(sqlCreate);
        }

        public void CreateRecentMediaTable()
        {
            try
            {
                String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_RECENT_MEDIA
                     + " (" + ""
                                    + DBConstants.LOGIN_TABLE_ID + "    BIGINT,"
                                    + DBConstants.USER_TABLE_ID + "    BIGINT,"
                                    + DBConstants.MEDIA_CONTENTID + " VARCHAR(128) PRIMARY KEY NOT NULL,"
                                    + DBConstants.MEDIA_TITLE + "  VARCHAR(250),"
                                    + DBConstants.MEDIA_ARTIST + "  VARCHAR(250),"
                                    + DBConstants.MEDIA_STREAM_URL + "  VARCHAR(250),"
                                    + DBConstants.MEDIA_THUMBURL + "  VARCHAR(250),"
                                    + DBConstants.MEDIA_DURAION + " BIGINT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEDIA_THUMB_IMAGE_WIDTH + " INT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEDIA_THUMB_IMAGE_HEIGHT + " INT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEDIA_ACCESS_COUNT + " SMALLINT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEDIA_LIKE_COUNT + " SMALLINT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEDIA_COMMENT_COUNT + " SMALLINT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEDIA_ILIKE + " SMALLINT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEDIA_ICOMMENT + " SMALLINT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEDIA_MEDIA_TYPE + " INT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEDIA_ALBUM_ID + " VARCHAR(128) NOT NULL,"
                                    + DBConstants.MEDIA_ALBUM_NAME + "  VARCHAR(250),"
                                    + DBConstants.MEDIA_PLAYING_TIME + "  BIGINT NOT NULL DEFAULT 0,"
                                    + " UNIQUE (" + DBConstants.LOGIN_TABLE_ID + ", " + DBConstants.MEDIA_CONTENTID + "))";

                ExecuteDBQurey(sqlCreate);
                //sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_RECENT_MEDIA_USERIDENTITY, DefaultSettings.LOGIN_USER_ID.ToString()) + " ON " + DBConstants.TABLE_RING_RECENT_MEDIA + DefaultSettings.LOGIN_USER_ID.ToString() + " (" + DBConstants.LOGIN_USER_ID + ")";
                //ExecuteDBQurey(sqlCreate);
            }
            catch (Exception ex)
            {
                log.Error("Error in Creating " + DBConstants.TABLE_RING_RECENT_MEDIA + "Table" + ex.Message);
            }
        }

        public void CreateDownloadMediaTable()
        {
            try
            {
                String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_DOWNLOAD_MEDIA
                     + " (" + ""
                                    + DBConstants.LOGIN_TABLE_ID + "    BIGINT,"
                                    + DBConstants.USER_TABLE_ID + "    BIGINT,"
                                    + DBConstants.MEDIA_CONTENTID + " VARCHAR(128) PRIMARY KEY NOT NULL,"
                                    + DBConstants.MEDIA_TITLE + "  VARCHAR(250),"
                                    + DBConstants.MEDIA_ARTIST + "  VARCHAR(250),"
                                    + DBConstants.MEDIA_STREAM_URL + "  VARCHAR(250),"
                                    + DBConstants.MEDIA_THUMBURL + "  VARCHAR(250),"
                                    + DBConstants.MEDIA_DURAION + " BIGINT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEDIA_THUMB_IMAGE_WIDTH + " INT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEDIA_THUMB_IMAGE_HEIGHT + " INT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEDIA_ACCESS_COUNT + " SMALLINT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEDIA_LIKE_COUNT + " SMALLINT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEDIA_COMMENT_COUNT + " SMALLINT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEDIA_ILIKE + " SMALLINT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEDIA_ICOMMENT + " SMALLINT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEDIA_MEDIA_TYPE + " INT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEDIA_ALBUM_ID + " VARCHAR(128) NOT NULL,"
                                    + DBConstants.MEDIA_ALBUM_NAME + "  VARCHAR(250),"
                                    + DBConstants.MEDIA_DOWNLOAD_STATE + "  SMALLINT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEDIA_DOWNLOAD_PROGRESS + " INT NOT NULL DEFAULT 0,"
                                    + DBConstants.MEDIA_DOWNLOAD_TIME + "  BIGINT NOT NULL DEFAULT 0,"
                                    + " UNIQUE (" + DBConstants.LOGIN_TABLE_ID + ", " + DBConstants.MEDIA_CONTENTID + "))";

                ExecuteDBQurey(sqlCreate);
                //sqlCreate = "CREATE INDEX IF NOT EXISTS " + String.Format(DBConstants.INDEX_RECENT_MEDIA_USERIDENTITY, DefaultSettings.LOGIN_USER_ID.ToString()) + " ON " + DBConstants.TABLE_RING_RECENT_MEDIA + DefaultSettings.LOGIN_USER_ID.ToString() + " (" + DBConstants.LOGIN_USER_ID + ")";
                //ExecuteDBQurey(sqlCreate);
            }
            catch (Exception ex)
            {
                log.Error("Error in Creating " + DBConstants.TABLE_RING_DOWNLOAD_MEDIA + "Table" + ex.Message);
            }
        }

        private void CreateChatBgListTable()
        {
            String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_CHAT_BG_LIST
                                + "("
                                + DBConstants.LOGIN_TABLE_ID + " BIGINT NOT NULL,"
                                + DBConstants.ID + " INT NOT NULL DEFAULT 0,"
                                + DBConstants.CHAT_BG_URL + " VARCHAR(100) NOT NULL,"
                                + DBConstants.THEME_COLOR + " VARCHAR(50) NOT NULL DEFAULT '',"
                                + DBConstants.IS_DOWNLOADED + "    BOOLEAN NOT NULL DEFAULT 0,"
                                + DBConstants.UPDATE_TIME + " BIGINT NOT NULL DEFAULT 0,"
                                + "  UNIQUE (" + DBConstants.LOGIN_TABLE_ID + ", " + DBConstants.CHAT_BG_URL + ")"
                                + ")";
            ExecuteDBQurey(sqlCreate);
        }

        public void CreateRingEventTable()
        {
            String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_DATE_EVENT
                                + " ("
                                + DBConstants.DATE_EVENT_DAY + " SMALLINT NOT NULL DEFAULT 0,"
                                + DBConstants.DATE_EVENT_MONTH + " SMALLINT NOT NULL DEFAULT 0,"
                                + DBConstants.DATE_EVENT_URL + " VARCHAR(250),"
                                + " UNIQUE (" + DBConstants.DATE_EVENT_DAY + ", " + DBConstants.DATE_EVENT_MONTH + ")"
                                + ")";
            ExecuteDBQurey(sqlCreate);
        }

        public void CreateBlockedNonFriendTable()
        {
            String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_BLOCKED_NON_FRIEND
                                + " ("
                                + DBConstants.LOGIN_TABLE_ID + "  BIGINT NOT NULL,"
                                + DBConstants.USER_TABLE_ID + "  BIGINT,"
                                + DBConstants.IS_BLOCKED_BY_ME + "  BOOLEAN NOT NULL DEFAULT 0,"
                                + DBConstants.IS_BLOCKED_BY_FRIEND + "  BOOLEAN NOT NULL DEFAULT 0,"
                                + " UNIQUE (" + DBConstants.LOGIN_TABLE_ID + ", " + DBConstants.USER_TABLE_ID + ")"
                                + ")";
            ExecuteDBQurey(sqlCreate);
        }

        public void CreateRoomCategoryTable()
        {
            String sqlCreate = "CREATE TABLE IF NOT EXISTS  " + DBConstants.TABLE_RING_ROOM_CATEGORY
                                + " ("
                                + DBConstants.LOGIN_TABLE_ID + "  BIGINT NOT NULL,"
                                + DBConstants.CATEGORY_NAME + " VARCHAR(100) NOT NULL,"
                                + " UNIQUE (" + DBConstants.LOGIN_TABLE_ID + ", " + DBConstants.CATEGORY_NAME + ")"
                                + ")";
            ExecuteDBQurey(sqlCreate);
        }

        public void InsertDefaultData(string scriptEmoPath, string scriptIMPath, string scriptEvtPath)
        {
            try
            {
                string selectSqlEmoticon = "SELECT COUNT(*) FROM  " + DBConstants.TABLE_RING_EMOTICONS;
                SQLiteCommand command = new SQLiteCommand(selectSqlEmoticon, DBConnection);
                int RowCount = Convert.ToInt32(command.ExecuteScalar());
                if (RowCount < 1)
                {
                    if (File.Exists(scriptEmoPath))
                    {
                        string qry = File.ReadAllText(scriptEmoPath);
                        ExecuteDBQurey(qry);
                    }
                }
                string selectSqlIM = "SELECT COUNT(*) FROM  " + DBConstants.TABLE_RING_INSTANT_MESSAGES;
                command = new SQLiteCommand(selectSqlIM, DBConnection);
                int RowCountIM = Convert.ToInt32(command.ExecuteScalar());
                if (RowCountIM < 1)
                {
                    if (File.Exists(scriptIMPath))
                    {
                        string qry = File.ReadAllText(scriptIMPath);
                        ExecuteDBQurey(qry);
                    }
                }
                if (File.Exists(scriptEvtPath))
                {
                    string qry = File.ReadAllText(scriptEvtPath);
                    ExecuteDBQurey(qry);
                }
            }
            catch (Exception ex)
            {
                log.Error("Exception in InsertDefaultData() ::  ==> " + ex.Message + "\n" + ex.StackTrace);
            }
        }
        #endregion

        #region helpers
        private string ConvertStringArrayToString(string[] array)
        {
            StringBuilder builder = new StringBuilder();
            foreach (string value in array)
            {
                builder.Append(value);
                builder.Append(" ");
            }
            return builder.ToString();
        }
        #endregion
    }
}

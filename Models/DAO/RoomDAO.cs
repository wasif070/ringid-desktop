﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Utility;
using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Threading;

namespace Models.DAO
{
    public class RoomDAO
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(RoomDAO).Name);

        public static List<RoomDTO> GetAllRoom(string roomID = null)
        {
            string masterQuery = String.Empty;
            string detailQuery = String.Empty;

            List<RoomDTO> masterDTOs = new List<RoomDTO>();
            try
            {
                CreateDBandTables CreateDB = new CreateDBandTables();
                CreateDB.ConnectToDatabase();

                masterQuery = "SELECT "
                        + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.ROOM_ID + ", "
                        + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.ROOM_NAME + ", "
                        + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.ROOM_IMAGE + ", "
                        + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.NUMBER_OF_MEMBERS + ", "
                        + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IS_PARTIAL + ", "
                        + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_MIN_PACKET_ID + ", "
                        + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_MAX_PACKET_ID + ", "
                        + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.CHAT_BG_URL + ", "
                        + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_SOUND + ", "
                        + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_NOTIFICATION + ", "
                        + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.IM_POPUP + " "
                        + " FROM " + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                        + (!String.IsNullOrWhiteSpace(roomID) ? " WHERE " + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString() + "." + DBConstants.ROOM_ID + " = '" + roomID + "'" : "");

                using (SQLiteCommand masterCommand = new SQLiteCommand(masterQuery, CreateDB.GetConnection()))
                {
                    SQLiteDataReader masterDr = masterCommand.ExecuteReader();
                    while (masterDr.Read())
                    {
                        RoomDTO roomDTO = new RoomDTO();
                        roomDTO.RoomID = masterDr.GetString(0);
                        roomDTO.RoomName = masterDr.GetString(1);
                        roomDTO.RoomProfileImage = masterDr.GetString(2);
                        roomDTO.NumberOfMembers = masterDr.GetInt32(3);
                        roomDTO.IsPartial = masterDr.GetBoolean(4);
                        roomDTO.ChatMinPacketID = masterDr.GetString(5);
                        roomDTO.ChatMaxPacketID = masterDr.GetString(6);
                        roomDTO.ChatBgUrl = masterDr.GetString(7);
                        roomDTO.ImSoundEnabled = masterDr.GetBoolean(8);
                        roomDTO.ImNotificationEnabled = masterDr.GetBoolean(9);
                        roomDTO.ImPopupEnabled = masterDr.GetBoolean(10);
                        masterDTOs.Add(roomDTO);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in GetAllRoom() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + masterQuery);
            }

            return masterDTOs;
        }

        public static void UpdateNumberOfMembers(string roomID, int numberOfMembers)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " SET "
                                + DBConstants.NUMBER_OF_MEMBERS + "=" + numberOfMembers + " "
                                + " WHERE "
                                + DBConstants.ROOM_ID + "='" + roomID + "'";

                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();
                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in UpdateNumberOfMembers() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }


        public static void UpdateChatBackground(string roomID, string chatBgUrl)
        {
            new Thread(() =>
            {
                string query = String.Empty;

                try
                {
                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();

                    query = "UPDATE " + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " SET "
                                + DBConstants.CHAT_BG_URL + "='" + ModelUtility.IsNull(chatBgUrl) + "' "
                                + " WHERE "
                                + DBConstants.ROOM_ID + "='" + roomID + "'";

                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in UpdateChatBackground() ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

        public static void UpdateIMNotification(string roomID, bool isEnable)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " SET "
                                + DBConstants.IM_NOTIFICATION + "=" + ModelUtility.IsBoolean(isEnable) + " "
                                + " WHERE "
                                + DBConstants.ROOM_ID + "='" + roomID + "'";

                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();
                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in UpdateIMNotification() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

        public static void UpdateIMSound(string roomID, bool isEnabled)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " SET "
                                + DBConstants.IM_SOUND + "=" + ModelUtility.IsBoolean(isEnabled) + " "
                                + " WHERE "
                                + DBConstants.ROOM_ID + "='" + roomID + "'";

                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();
                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in UpdateIMSound() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

        public static void UpdateIMPopUp(string roomID, bool isEnabled)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_ROOM_LIST + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " SET "
                                + DBConstants.IM_POPUP + "=" + ModelUtility.IsBoolean(isEnabled) + " "
                                + " WHERE "
                                + DBConstants.ROOM_ID + "='" + roomID + "'";

                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();
                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in UpdateIMPopUp() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

        public static List<string> GetAllRoomCategory()
        {
            string masterQuery = String.Empty;

            List<string> categoryList = new List<string>();
            try
            {
                CreateDBandTables CreateDB = new CreateDBandTables();
                CreateDB.ConnectToDatabase();

                masterQuery = "SELECT " + DBConstants.CATEGORY_NAME + " FROM " + DBConstants.TABLE_RING_ROOM_CATEGORY
                    + " WHERE " + DBConstants.LOGIN_TABLE_ID + "=" + DefaultSettings.LOGIN_TABLE_ID + " "
                    + " ORDER BY " + DBConstants.CATEGORY_NAME;

                using (SQLiteCommand masterCommand = new SQLiteCommand(masterQuery, CreateDB.GetConnection()))
                {
                    SQLiteDataReader masterDr = masterCommand.ExecuteReader();
                    while (masterDr.Read())
                    {
                        categoryList.Add(masterDr.GetString(0));
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in GetAllRoomCategory() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + masterQuery);
            }

            return categoryList;
        }

        public static void SaveRoomCategory(List<string> categoryList)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    if (categoryList != null && categoryList.Count > 0)
                    {
                        CreateDBandTables createDB = new CreateDBandTables();
                        createDB.ConnectToDatabase();

                        query = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_ROOM_CATEGORY
                                    + " ( "
                                    + DBConstants.LOGIN_TABLE_ID + ", "
                                    + DBConstants.CATEGORY_NAME + " "
                                    + " ) ";

                        categoryList.ForEach(i =>
                        {
                            query += "SELECT "
                            + "" + DefaultSettings.LOGIN_TABLE_ID + ", "
                            + "'" + ModelUtility.IsExceded(i, 100) + "' "
                            + " UNION ";
                        }
                        );

                        query = query.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' }) + "; ";
                        createDB.ExecuteDBQurey(query);
                        //createDB.ExecuteDBQurey("BEGIN; " + query + " END;");
                    }
                }
                catch (Exception ex)
                {
                    log.Error("SQLException in SaveRoomCategory() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Room Query = " + query);
                }
            }).Start();
        }

    }
}

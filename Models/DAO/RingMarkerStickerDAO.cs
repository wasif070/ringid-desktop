﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Utility;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Threading;
using Models.Stores;

namespace Models.DAO
{
    public class RingMarkerStickerDAO
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(RingMarkerStickerDAO).Name);

        public static List<MarketStickerCategoryDTO> GetAllRingMarketSticker()
        {

            string masterQuery = String.Empty;
            string detailQuery = String.Empty;
            List<MarketStickerCategoryDTO> masterDTOs = new List<MarketStickerCategoryDTO>();
            List<MarkertStickerImagesDTO> recentImageDTOs = new List<MarkertStickerImagesDTO>();
            try
            {
                CreateDBandTables CreateDB = new CreateDBandTables();
                CreateDB.ConnectToDatabase();

                masterQuery = "SELECT * FROM " + DBConstants.TABLE_RING_MARKET_STICKER_CATEGORY
                            + " WHERE "
                            + DBConstants.LOGIN_TABLE_ID + " = " + DefaultSettings.LOGIN_TABLE_ID
                            + " ORDER BY " + DBConstants.DOWNLOAD_TIME + " DESC, " + DBConstants.CATEGORY_ID + " ASC";

                using (SQLiteCommand masterCommand = new SQLiteCommand(masterQuery, CreateDB.GetConnection()))
                {
                    SQLiteDataReader masterDr = masterCommand.ExecuteReader();
                    while (masterDr.Read())
                    {
                        MarketStickerCategoryDTO categoryDTO = new MarketStickerCategoryDTO();
                        categoryDTO.sCtId = masterDr.GetInt32(masterDr.GetOrdinal(DBConstants.CATEGORY_ID));
                        categoryDTO.sClId = masterDr.GetInt32(masterDr.GetOrdinal(DBConstants.COLLECTION_ID));
                        categoryDTO.sctName = masterDr.GetString(masterDr.GetOrdinal(DBConstants.CATEGORY_NAME));
                        categoryDTO.cgBnrImg = masterDr.GetString(masterDr.GetOrdinal(DBConstants.BANNER_IMAGE));
                        categoryDTO.IsBannerVisible = masterDr.GetBoolean(masterDr.GetOrdinal(DBConstants.IS_BANNER_VISIBLE));
                        categoryDTO.Dcpn = masterDr.GetString(masterDr.GetOrdinal(DBConstants.DESCRIPTION));
                        categoryDTO.DetailImg = masterDr.GetString(masterDr.GetOrdinal(DBConstants.DETAIL_IMAGE));
                        categoryDTO.Icon = masterDr.GetString(masterDr.GetOrdinal(DBConstants.ICON));
                        categoryDTO.Downloaded = masterDr.GetBoolean(masterDr.GetOrdinal(DBConstants.IS_DOWNLOADED));
                        categoryDTO.free = masterDr.GetBoolean(masterDr.GetOrdinal(DBConstants.IS_FREE));
                        categoryDTO.cgNw = masterDr.GetBoolean(masterDr.GetOrdinal(DBConstants.IS_NEW));
                        categoryDTO.IsTop = masterDr.GetBoolean(masterDr.GetOrdinal(DBConstants.IS_TOP));
                        categoryDTO.Prz = masterDr.GetFloat(masterDr.GetOrdinal(DBConstants.PRIZE));
                        categoryDTO.Rnk = masterDr.GetInt32(masterDr.GetOrdinal(DBConstants.RANK));
                        categoryDTO.DownloadTime = masterDr.GetInt64(masterDr.GetOrdinal(DBConstants.DOWNLOAD_TIME));
                        categoryDTO.SortOrder = masterDr.GetInt32(masterDr.GetOrdinal(DBConstants.SORT_ORDER));
                        categoryDTO.IsVisible = masterDr.GetBoolean(masterDr.GetOrdinal(DBConstants.IS_VISIBLE));
                        categoryDTO.IsDefault = masterDr.GetBoolean(masterDr.GetOrdinal(DBConstants.IS_DEFAULT));
                        categoryDTO.LastUsedDate = masterDr.GetInt64(masterDr.GetOrdinal(DBConstants.LAST_USED_DATE));
                        categoryDTO.IsNewStickerSeen = masterDr.GetBoolean(masterDr.GetOrdinal(DBConstants.NEW_STICKER_IS_SEEN));
                        masterDTOs.Add(categoryDTO);
                    }

                    masterDTOs.ForEach(i =>
                    {
                        try
                        {
                            detailQuery = "SELECT * FROM " + DBConstants.TABLE_RING_MARKET_STICKER
                                                + " WHERE "
                                                + DBConstants.LOGIN_TABLE_ID + " = " + DefaultSettings.LOGIN_TABLE_ID + " AND "
                                                + DBConstants.CATEGORY_ID + " = " + i.sCtId;

                            using (SQLiteCommand detailCommand = new SQLiteCommand(detailQuery, CreateDB.GetConnection()))
                            {
                                SQLiteDataReader detailDr = detailCommand.ExecuteReader();
                                while (detailDr.Read())
                                {
                                    MarkertStickerImagesDTO imageDTO = new MarkertStickerImagesDTO();
                                    imageDTO.sClId = detailDr.GetInt32(detailDr.GetOrdinal(DBConstants.COLLECTION_ID));
                                    imageDTO.sCtId = detailDr.GetInt32(detailDr.GetOrdinal(DBConstants.CATEGORY_ID));
                                    imageDTO.imId = detailDr.GetInt32(detailDr.GetOrdinal(DBConstants.IMAGE_ID));
                                    imageDTO.imUrl = detailDr.GetString(detailDr.GetOrdinal(DBConstants.IMAGE_URL));
                                    imageDTO.IsRecent = detailDr.GetBoolean(detailDr.GetOrdinal(DBConstants.IS_RECENT));
                                    i.ImagesList.Add(imageDTO);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            log.Error("SQLException in GetAllRingMarketSticker() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + detailQuery);
                        }
                    });

                    try
                    {
                        detailQuery = "SELECT * FROM " + DBConstants.TABLE_RING_MARKET_STICKER
                                            + " WHERE "
                                            + DBConstants.LOGIN_TABLE_ID + " = " + DefaultSettings.LOGIN_TABLE_ID + " AND "
                                            + DBConstants.IS_RECENT + " = " + ModelUtility.IsBoolean(true);

                        using (SQLiteCommand detailCommand = new SQLiteCommand(detailQuery, CreateDB.GetConnection()))
                        {
                            SQLiteDataReader detailDr = detailCommand.ExecuteReader();
                            while (detailDr.Read())
                            {
                                MarkertStickerImagesDTO imageDTO = new MarkertStickerImagesDTO();
                                imageDTO.sClId = detailDr.GetInt32(detailDr.GetOrdinal(DBConstants.COLLECTION_ID));
                                imageDTO.sCtId = detailDr.GetInt32(detailDr.GetOrdinal(DBConstants.CATEGORY_ID));
                                imageDTO.imId = detailDr.GetInt32(detailDr.GetOrdinal(DBConstants.IMAGE_ID));
                                imageDTO.imUrl = detailDr.GetString(detailDr.GetOrdinal(DBConstants.IMAGE_URL));
                                imageDTO.IsRecent = detailDr.GetBoolean(detailDr.GetOrdinal(DBConstants.IS_RECENT));
                                recentImageDTOs.Add(imageDTO);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("SQLException in GetAllRingMarketSticker() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + detailQuery);
                    }

                    if (recentImageDTOs.Count > 0)
                    {
                        MarketStickerCategoryDTO categoryDTO = new MarketStickerCategoryDTO();
                        categoryDTO.sCtId = 0;
                        categoryDTO.sClId = 0;
                        categoryDTO.sctName = "Recent";
                        categoryDTO.Downloaded = true;
                        categoryDTO.DownloadTime = 0;
                        categoryDTO.SortOrder = 0;
                        categoryDTO.IsVisible = true;
                        categoryDTO.IsDefault = false;
                        categoryDTO.LastUsedDate = 0;
                        masterDTOs.Insert(0, categoryDTO);
                        categoryDTO.ImagesList.AddRange(recentImageDTOs);
                        
                    }                   
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in GetAllRingMarketSticker() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + masterQuery);
            }

            return masterDTOs;
        }

        public static List<MarketStickerCollectionDTO> GetRingMarketStickerCollection()
        {

            string masterQuery = String.Empty;
            List<MarketStickerCollectionDTO> masterDTOs = new List<MarketStickerCollectionDTO>();
            try
            {
                CreateDBandTables CreateDB = new CreateDBandTables();
                CreateDB.ConnectToDatabase();

                masterQuery = "SELECT * FROM " + DBConstants.TABLE_RING_MARKET_STICKER_COLLECTION
                            + " WHERE "
                            + DBConstants.LOGIN_TABLE_ID + " = " + DefaultSettings.LOGIN_TABLE_ID
                            + " ORDER BY " + DBConstants.COLLECTION_NAME + " ASC";

                using (SQLiteCommand masterCommand = new SQLiteCommand(masterQuery, CreateDB.GetConnection()))
                {
                    SQLiteDataReader masterDr = masterCommand.ExecuteReader();
                    while (masterDr.Read())
                    {
                        MarketStickerCollectionDTO collectionDTO = new MarketStickerCollectionDTO();
                        collectionDTO.sClId = masterDr.GetInt32(masterDr.GetOrdinal(DBConstants.COLLECTION_ID));
                        collectionDTO.name = masterDr.GetString(masterDr.GetOrdinal(DBConstants.COLLECTION_NAME));
                        collectionDTO.bnrImg = masterDr.GetString(masterDr.GetOrdinal(DBConstants.BANNER_IMAGE));
                        collectionDTO.ThmColr = masterDr.GetString(masterDr.GetOrdinal(DBConstants.THEME_COLOR));
                        masterDTOs.Add(collectionDTO);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in GetRingMarketStickerCollection() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + masterQuery);
            }

            return masterDTOs;
        }

        public static void UpdateStickerCategoryDownloadStatus(int categoryID, bool isDownloaded, long downloadTime = 0)
        {
            string query = String.Empty;
            try
            {
                query = "UPDATE " + DBConstants.TABLE_RING_MARKET_STICKER_CATEGORY + " "
                                 + " SET "
                                 + DBConstants.IS_DOWNLOADED + "=" + ModelUtility.IsBoolean(isDownloaded) + ", "
                                 + DBConstants.DOWNLOAD_TIME + "=" + (isDownloaded ? downloadTime : 0) + " "
                                 + " WHERE "
                                 + DBConstants.LOGIN_TABLE_ID + " = " + DefaultSettings.LOGIN_TABLE_ID + " AND "
                                 + DBConstants.CATEGORY_ID + "=" + categoryID;

                CreateDBandTables createDB = new CreateDBandTables();
                createDB.ConnectToDatabase();
                createDB.ExecuteDBQurey(query);
            }
            catch (Exception ex)
            {
                log.Error("Error occured during UpdateStickerCategoryDownloadStatus. Exception :: " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
        }

        public static void UpdateStickerCategoryLastUsedDate(int categoryID, long lastUsedDate)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_MARKET_STICKER_CATEGORY + " "
                                     + " SET "
                                     + DBConstants.LAST_USED_DATE + "=" + lastUsedDate + " "
                                     + " WHERE "
                                     + DBConstants.LOGIN_TABLE_ID + " = " + DefaultSettings.LOGIN_TABLE_ID + " AND "
                                     + DBConstants.CATEGORY_ID + "=" + categoryID;

                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();
                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("Error occured during UpdateStickerCategoryDownloadStatus. Exception :: " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

        public static void UpdateStickerRecent(int categoryID, int imageID)
        {
            new Thread(() =>
            {
                string query = String.Empty;
                try
                {
                    query = "UPDATE " + DBConstants.TABLE_RING_MARKET_STICKER + " "
                                     + " SET "
                                     + DBConstants.IS_RECENT + "=" + ModelUtility.IsBoolean(true) + " "
                                     + " WHERE "
                                     + DBConstants.LOGIN_TABLE_ID + " = " + DefaultSettings.LOGIN_TABLE_ID + " AND "
                                     + DBConstants.CATEGORY_ID + "=" + categoryID + " AND "
                                     + DBConstants.IMAGE_ID + "=" + imageID;

                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();
                    createDB.ExecuteDBQurey(query);
                }
                catch (Exception ex)
                {
                    log.Error("Error occured during UpdateStickerRecent. Exception :: " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
                }
            }).Start();
        }

    }
}

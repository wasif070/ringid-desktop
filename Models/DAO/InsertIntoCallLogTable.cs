﻿using log4net;
using Models.Constants;
using Models.Entity;
using Models.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Models.DAO
{
    public class InsertIntoCallLogTable
    {

        private readonly ILog log = LogManager.GetLogger(typeof(InsertIntoCallLogTable).Name);

        Thread t = null;
        public delegate void CompleteHandler();
        public event CompleteHandler OnComplete;
        private List<CallLogDTO> _CallLogDTOs;

        public InsertIntoCallLogTable(List<CallLogDTO> callLogDTOs)
        {
            this._CallLogDTOs = callLogDTOs;
        }

        public void Start()
        {
            t = new Thread(param => Preocess());
            t.Name = this.GetType().Name;
            t.Start();
        }

        public void Run()
        {
            Preocess();
        }

        internal void Preocess()
        {
            try
            {
                if (this._CallLogDTOs != null && this._CallLogDTOs.Count > 0)
                {
                    this._CallLogDTOs = this._CallLogDTOs.ToList();
                    if (this._CallLogDTOs.Count > 500)
                    {
                        List<CallLogDTO> msgDTOs = new List<CallLogDTO>();
                        foreach (CallLogDTO msg in this._CallLogDTOs)
                        {
                            msgDTOs.Add(msg);
                            if (msgDTOs.Count >= 500)
                            {
                                InsertOrUpdate(msgDTOs);
                                msgDTOs = new List<CallLogDTO>();
                            }
                        }

                        if (msgDTOs.Count > 0)
                        {
                            InsertOrUpdate(msgDTOs);
                        }
                    }
                    else
                    {
                        InsertOrUpdate(_CallLogDTOs);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in Preocess() ==> " + ex.Message + "\n" + ex.StackTrace);
            }

            if (OnComplete != null)
            {
                Thread.Sleep(100);
                OnComplete();
            }
        }

        private void InsertOrUpdate(List<CallLogDTO> callLogDTOs)
        {
            string insertUpdateQuery = String.Empty;
            string deleteQuery = String.Empty;
            string query = String.Empty;

            try
            {
                if (callLogDTOs != null && callLogDTOs.Count > 0)
                {
                    CreateDBandTables createDB = new CreateDBandTables();
                    createDB.ConnectToDatabase();
                    query = "INSERT OR REPLACE INTO " + DBConstants.TABLE_RING_CALL_LOG + DefaultSettings.LOGIN_TABLE_ID.ToString()
                                + " ( "
                                + DBConstants.CALL_ID + ", "
                                + DBConstants.FRIEND_TABLE_ID + ", "
                                + DBConstants.CL_CALL_CATEGORY + ", "
                                + DBConstants.CL_CALL_TYPE + ", "
                                + DBConstants.CL_CALL_DURATION + ", "
                                + DBConstants.MESSAGE + ", "
                                + DBConstants.CL_CALLING_TIME + ", "
                                + DBConstants.IS_UNREAD + " "
                                + " ) ";

                    callLogDTOs.ForEach(i =>
                    {
                        query += "SELECT "
                            + "'" + i.CallID + "', "
                            + "" + i.FriendTableID + ", "
                            + "" + i.CallCategory + ", "
                            + "" + i.CallType + ", "
                            + "" + i.CallDuration + ", "
                            + "'" + ModelUtility.IsNull(i.Message) + "', "
                            + "" + i.CallingTime + ", "
                            + "" + ModelUtility.IsBoolean(i.IsUnread) + " "
                            + " UNION ";
                    });

                    query = query.TrimEnd(new char[] { 'U', 'N', 'I', 'O', 'N', ' ' });
                    createDB.ExecuteDBQurey(query);
                }
            }
            catch (Exception ex)
            {
                log.Error("SQLException in InsertOrUpdate() class ==> " + ex.Message + "\n" + ex.StackTrace + ", Query = " + query);
            }
        }

    }
}

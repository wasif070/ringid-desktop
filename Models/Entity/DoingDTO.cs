﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Entity
{
    public class DoingDTO
    {
        public long ID { get; set; }
        public long UpdateTime { get; set; }
        public int Category { get; set; }
        public string Name { get; set; }
        public string ImgUrl { get; set; }
        public int SortId { get; set; }
    }
}

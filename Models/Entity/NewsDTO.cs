﻿
namespace Models.Entity
{
    public class NewsDTO
    {
        public string NewsUrl { get; set; }
        public long NewsId { get; set; }
        public bool ExUrlOp { get; set; }
        public int NewsCategoryId { get; set; }
        public string NewsTitle { get; set; }
        public string NewsShortDescription { get; set; }
        public string NewsDescription { get; set; }

        public int NewsPortalFeedtype { get; set; }

        //common pagetype feed info
        public long PId { get; set; }
        public string PName { get; set; }
        public int SCount { get; set; }
    }
}

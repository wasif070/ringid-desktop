﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Entity
{
    public class FriendInfoDTO
    {
        private List<long> _FriendList = new List<long>();
        public List<long> FriendList
        {
            get { return _FriendList; }
            set { _FriendList = value; }
        }
        public int TotalFriend { get; set; }        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Entity
{
    public class SettingsDTO
    {
        public int SettingsName { get; set; }
        public int SettingsValue { get; set; }
    }
}

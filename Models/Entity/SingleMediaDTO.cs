﻿
namespace Models.Entity
{
    public class SingleMediaDTO
    {
        public long ContentId { get; set; }
        public string Title { get; set; }
        public string Artist { get; set; }
        public string StreamUrl { get; set; }
        public string ThumbUrl { get; set; }
        public long Duration { get; set; }
        public int ThumbImageWidth { get; set; }
        public int ThumbImageHeight { get; set; }

        //details of a media
        public short AccessCount { get; set; }
        public short LikeCount { get; set; }
        public long CommentCount { get; set; }
        public short ShareCount { get; set; }
        public short ILike { get; set; }
        public short IComment { get; set; }
        public short IShare { get; set; }

        //from head
        public int MediaType { get; set; }
        public long AlbumId { get; set; }
        public string AlbumName { get; set; }
        public long UserTableID { get; set; }
        public long NewsFeedId { get; set; }
        public string FullName { get; set; }
        public string ProfileImage { get; set; }

        //future
        public long Time { get; set; }
        public long LastPlayedTime { get; set; }

        //Download
        public int DownloadState { get; set; }
        public int DownloadProgress { get; set; }
        public long DownloadTime { get; set; }

        public int Privacy { get; set; }
    }
}
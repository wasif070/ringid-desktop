﻿using Models.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Entity
{
    public class HashTagDTO
    {
        public HashTagDTO()
        {
            HashTagInsertionTime = ModelUtility.CurrentTimeMillis();
        }
        public long HashTagID { get; set; }
        public int NoOfItems { get; set; }
        public string HashTagSearchKey { get; set; }
        public long HashTagInsertionTime { get; set; }
        public List<SingleMediaDTO> MediaList { get; set; }
        public int Weight { get; set; }
    }
}

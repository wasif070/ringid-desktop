﻿using Models.Constants;
using Newtonsoft.Json.Linq;


namespace Models.Entity
{
    public class CommunicationPortsDTO
    {

        public CommunicationPortsDTO(string json)
        {
            if (!string.IsNullOrEmpty(json))
            {
                JObject jObject = JObject.Parse(json);
                AuthServerIP = (string)jObject[JsonKeys.AuthServerIP];
                ComPort = (int)jObject[JsonKeys.ComPort];
                RingID = (string)jObject[JsonKeys.RingId];
                IsSuccess = (bool)jObject[JsonKeys.IsSuccess];
            }
            else
            {
                AuthServerIP = null;
                ComPort = 0;
                RingID = null;
                IsSuccess = false;
            }

        }
        public override string ToString()
        {
            return "AuthServerIP==>" + AuthServerIP + " ComPort==>" + ComPort + " RingID==>" + RingID;
        }

        private string authServerIP;

        public string AuthServerIP
        {
            get { return authServerIP; }
            set { authServerIP = value; }
        }
        private int comPort;

        public int ComPort
        {
            get { return comPort; }
            set { comPort = value; }
        }

        private string ringID;

        public string RingID
        {
            get { return ringID; }
            set { ringID = value; }
        }
        private bool isSuccess;

        public bool IsSuccess
        {
            get { return isSuccess; }
            set { isSuccess = value; }
        }


    }
}

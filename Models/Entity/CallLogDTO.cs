﻿
using System.Collections.Generic;
namespace Models.Entity
{
    public class CallLogDTO
    {

        public string CallID { get; set; }
        public int CallCategory { get; set; } //audio = 1, video = 2
        public int CallType { get; set; }//outgoing=1,incoming=2,else missed
        public long FriendTableID { get; set; }
        public long CallDuration { get; set; }
        public long CallingTime { get; set; }
        public string Message { get; set; }
        public bool IsUnread { get; set; }
        public long LastCallingTime { get; set; }
        public string CallLogID { get; set; }
        public Dictionary<string, long> CallIDs { get; set; }

    }
}

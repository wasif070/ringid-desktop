﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Models.Entity
{
    public class MessageDTO
    {

        public long MessageID { get; set; }
        public int PacketType { get; set; }
        public string PacketID { get; set; }
        public long SenderTableID { get; set; }
        public long FriendTableID { get; set; }
        public long GroupID { get; set; }
        public string RoomID { get; set; }
        public long PublisherID { get; set; }
        public string Message { get; set; }
        public string OriginalMessage { get; set; }
        public long MessageDate { get; set; }
        public long MessageDelieverDate { get; set; }
        public long MessageSeenDate { get; set; }
        public long MessageViewDate { get; set; }
        public int MessageType { get; set; }
        public int Timeout { get; set; }
        public int Presence { get; set; }
        public int Mood { get; set; }
        public int Device { get; set; }
        public string DeviceToken { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public int IsSecretVisible { get; set; }
        public int ChatBindingPort { get; set; }
        public int NumberOfPacket { get; set; }
        public int SequenceNumber { get; set; }
        public int ApplicationType { get; set; }
        public int RemotePushType { get; set; }
        public string Caption { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Duration { get; set; }
        public string GroupName { get; set; }
        public string GroupImage { get; set; }
        public string FullName { get; set; }
        public string ProfileImage { get; set; }
        public int NumberOfMembers { get; set; }
        public long MemberAddedBy { get; set; }
        public long ActivityType { get; set; }
        public int LikeCount { get; set; }
        public bool ILike { get; set; }
        public bool IReport { get; set; }
        public bool IsUnread { get; set; }
        public List<MessageDTO> MessageList { get; set; }

        public string LinkUrl { get; set; }
        public string LinkTitle { get; set; }
        public string LinkImageUrl { get; set; }
        public string LinkDescription { get; set; }

        public long ServerDate { get; set; }
        public long DateDifference { get; set; }
        public int Status { get; set; }
        public int ChatRegisterPort { get; set; }
        public string ChatServerIP { get; set; }
        public byte[] DataByte { get; set; }
        public long FileID { get; set; }
        public long FileSize { get; set; }
        public string FileMenifest { get; set; }
        public int FileStatus { get; set; }
        public bool IsFileCancelled { get; set; }
        public bool IsChatMessage { get; set; }

        public int MediaType { get; set; }
        public string MediaTitle { get; set; }
        public string MediaAlbum { get; set; }
        public string MediaArtist { get; set; }

        public long SharedTableID { get; set; }
        public long SharedRingID { get; set; }
        public string SharedFullName { get; set; }
        public string SharedProfileImage { get; set; }
        public StreamMessageDTO StreamMsgDTO { get; set; }

        public override string ToString()
        {
            return "MessageBaseDTO ["
                    + "pT=" + PacketType + ", pId=" + PacketID + ", fId=" + FriendTableID + ", grpId=" + GroupID + ", rID=" + RoomID + ", pbID=" + PublisherID
                    + ", sId=" + SenderTableID + ", mT=" + MessageType + ", sts=" + Status + ", mD=" + MessageDate
                    + ", tOut=" + Timeout + ", tV=" + IsSecretVisible + ", aT=" + ActivityType + ", addBy=" + MemberAddedBy
                    + ", grpMbrCnt=" + NumberOfMembers + ", msg=" + Message + ", fN=" + FullName + ", grpNm=" + GroupName
                    + ", grpImg=" + GroupImage + ", seqNo=" + SequenceNumber + ", nop=" + NumberOfPacket + ", lC=" + LikeCount + ", iL=" + ILike + ", iR=" + IReport
                    + ", flId=" + FileID + ", flMf=" + FileMenifest + ", cbp=" + ChatBindingPort + ", srvD=" + ServerDate + ", dDiff=" + DateDifference
                    + "]";
        }

        public MessageDTO Clone()
        {
            return (MessageDTO)CopyTo(typeof(MessageDTO));
        }

        public Object CopyTo(Type targetClass)
        {
            Object instance = null;
            try
            {
                instance = Activator.CreateInstance(targetClass);
                PropertyInfo[] sourceProperties = this.GetType().GetProperties();
                List<PropertyInfo> destProperties = instance.GetType().GetProperties().ToList();

                foreach (PropertyInfo sourceProperty in sourceProperties)
                {
                    int index = -1;
                    int curr = -1;

                    foreach (PropertyInfo destProperty in destProperties)
                    {
                        curr++;
                        if (destProperty.Name == sourceProperty.Name && destProperty.PropertyType.IsAssignableFrom(sourceProperty.PropertyType))
                        {
                            destProperty.SetValue(instance, sourceProperty.GetValue(this, null), null);
                            index = curr;
                            break;
                        }
                    }

                    if (index > -1)
                    {
                        destProperties.RemoveAt(index);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in MessageDTO class in returning targetClass==>" + ex.Message + "\n" + ex.StackTrace);
            }
            return instance;
        }
    }

    public class StreamMessageDTO
    {
        public int MessageType { get; set; }
    }
}

<<<<<<< HEAD
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Entity
{

    public class StickerDTO
    {

        public bool Sucs { get; set; }
        public int nc { get; set; }
        public List<StickerCollectionDTO> StickerCollectionsList { get; set; }
        public List<StickerCategoryDTO> TopCategoriesList { get; set; }
        public List<StickerCategoryDTO> StNewCategoriesList { get; set; }
        public List<StickerCategoryDTO> FreeCategoriesList { get; set; }
        public List<StickerCategoryDTO> CategoriesList { get; set; }
        public List<StickerImagesDTO> ImagesList { get; set; }
        public List<StickerCategoryDTO> SingleCategoryInfo { get; set; }
        public List<BannerDTO> BannerList { get; set; }
        public List<int> ids { get; set; }
    }

    public class StickerCollectionDTO
    {

        public int SClId { get; set; }
        public string Name { get; set; }
        public string BnrImg { get; set; }
        public string ThmColr { get; set; }

    }

    public class StickerCategoryDTO
    {

        public int SCtId { get; set; }
        public string SctName { get; set; }
        public int SClId { get; set; }
        public string CgBnrImg { get; set; }
        public int Rnk { get; set; }
        public string DtlImg { get; set; }
        public bool CgNw { get; set; }
        public string Icn { get; set; }
        public float Prz { get; set; }
        public bool Free { get; set; }
        public string Dcpn { get; set; }
        public bool Downloaded { get; set; }
        public long DownloadTime { get; set; }
        public int SortOrder { get; set; }
        public bool IsDefault { get; set; }
        public long LastUsedDate { get; set; }
        public bool IsTop { get; set; }
        public bool IsBannerVisible { get; set; }

        private List<StickerImagesDTO> _ImagesList = new List<StickerImagesDTO>();
        public List<StickerImagesDTO> ImagesList
        {
            get { return _ImagesList; }
            set { _ImagesList = value; }
        }

        public void UpdateDTO(StickerCategoryDTO categoryDTO)
        {
            this.SctName = categoryDTO.SctName;
            this.CgBnrImg = categoryDTO.CgBnrImg;
            this.Rnk = categoryDTO.Rnk;
            this.DtlImg = categoryDTO.DtlImg;
            this.CgNw = categoryDTO.CgNw;
            this.Icn = categoryDTO.Icn;
            this.Prz = categoryDTO.Prz;
            this.Free = categoryDTO.Free;
            this.Dcpn = categoryDTO.Dcpn;
        }
    }

    public class StickerImagesDTO
    {

        public int ImId { get; set; }
        public int SCtId { get; set; }
        public int SClId { get; set; }
        public string ImUrl { get; set; }

        public bool _IsRecent = false;
        public bool IsRecent
        {
            get { return _IsRecent; }
            set { _IsRecent = value; }
        }
    }

    public class BannerDTO
    {
        public int SClId { get; set; }
        public int SCtId { get; set; }
    }

    public class MarketStickerDTO
    {

        public bool sucs { get; set; }

        public List<int> pCat { get; set; }
        public List<int> fCat { get; set; }
        public List<int> nCat { get; set; }
        public List<int> tCat { get; set; }
        public List<int> allCat { get; set; }
        
        public List<MarketStickerCategoryDTO> plrBnr { get; set; }
        public List<MarketStickerCategoryDTO> nwBnr { get; set; }
        public List<MarketStickerCategoryDTO> allBnr { get; set; }

        public List<MarketStickerCategoryDTO> catList { get; set; }
        public List<MarkertStickerImagesDTO> ImagesList { get; set; }
        public List<MarketStickerCollectionDTO> colList { get; set; }
        public List<MarketStickerLanguageyDTO> langLst { get; set; }
    }

    public class MarketStickerCategoryDTO
    {

        public int sCtId { get; set; }
        public int sClId { get; set; }
        public string sctName { get; set; }
        public bool cgNw { get; set; }
        public bool free { get; set; }
        public bool Downloaded { get; set; }
        public bool IsDefault { get; set; }
        public long DownloadTime { get; set; }
        public bool IsBannerVisible { get; set; }
        public int Rnk { get; set; }
        public bool IsTop { get; set; }
        public float Prz { get; set; }
        public string Dcpn { get; set; }
        public int SortOrder { get; set; }
        public bool _IsVisible = true;
        public bool IsVisible
        {
            get { return _IsVisible; }
            set { _IsVisible = value; }
        }
        public long LastUsedDate { get; set; }

        private bool _IsNewStickerSeen = false;
        public bool IsNewStickerSeen
        {
            get { return _IsNewStickerSeen; }
            set { _IsNewStickerSeen = value; }
        }

        public string _Icon = "icon.png";
        public string Icon
        {
            get { return _Icon; }
            set { _Icon = value; }
        }

        public string _cgBnrImg = "banner.png";
        public string cgBnrImg
        {
            get { return _cgBnrImg; }
            set { _cgBnrImg = value; }
        }

        public string _DetailImg = "details.png";
        public string DetailImg
        {
            get { return _DetailImg; }
            set { _DetailImg = value; }
        }

        private string _ThumbDetailImage = "thumbdetails.png";
        public string ThumbDetailImage
        {
            get { return _ThumbDetailImage; }
            set { _ThumbDetailImage = value; }
        }

        private List<MarkertStickerImagesDTO> _ImagesList = new List<MarkertStickerImagesDTO>();
        public List<MarkertStickerImagesDTO> ImagesList
        {
            get { return _ImagesList; }
            set { _ImagesList = value; }
        }

        public void UpdateNewStickerCategoryDTO(MarketStickerCategoryDTO marketCtgDTO)
        {

            this.sCtId = marketCtgDTO.sCtId;
            this.sClId = marketCtgDTO.sClId;
            this.sctName = marketCtgDTO.sctName;
            this.cgNw = marketCtgDTO.cgNw;
            this.free = marketCtgDTO.free;
            this.Rnk = marketCtgDTO.Rnk;
            this.IsNewStickerSeen = marketCtgDTO.IsNewStickerSeen;
        }

        public void UpdateMarketDTOByCategoryDTO(StickerCategoryDTO categoryDTO)
        {

            this.sCtId = categoryDTO.SCtId;
            this.sClId = categoryDTO.SClId;
            this.sctName = categoryDTO.SctName;
            this.cgNw = categoryDTO.CgNw;
            this.free = categoryDTO.Free;
            this.Rnk = categoryDTO.Rnk;
        }
    }

    public class MarketStickerCollectionDTO
    {

        public int sClId { get; set; }
        public string name { get; set; }
        public string bnrImg { get; set; }
        public string ThmColr { get; set; }
        public List<int> catIds { get; set; }
    }

    public class MarkertStickerImagesDTO
    {
        public int imId { get; set; }
        public int sCtId { get; set; }
        public int sClId { get; set; }
        public string imUrl { get; set; }

        public bool _IsRecent = false;
        public bool IsRecent
        {
            get { return _IsRecent; }
            set { _IsRecent = value; }
        }
    }

    public class MarketStickerLanguageyDTO
    {
        //public int id { get; set; }
        //public string code { get; set; }
        //public string countrySymbol { get; set; }
        public string name { get; set; }
        public List<int> catIds { get; set; }
    }
}
=======
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Entity
{

    public class StickerDTO
    {

        public bool Sucs { get; set; }
        public int nc { get; set; }
        public List<StickerCollectionDTO> StickerCollectionsList { get; set; }
        public List<StickerCategoryDTO> TopCategoriesList { get; set; }
        public List<StickerCategoryDTO> StNewCategoriesList { get; set; }
        public List<StickerCategoryDTO> FreeCategoriesList { get; set; }
        public List<StickerCategoryDTO> CategoriesList { get; set; }
        public List<StickerImagesDTO> ImagesList { get; set; }
        public List<StickerCategoryDTO> SingleCategoryInfo { get; set; }
        public List<BannerDTO> BannerList { get; set; }
        public List<int> ids { get; set; }
    }

    public class StickerCollectionDTO
    {

        public int SClId { get; set; }
        public string Name { get; set; }
        public string BnrImg { get; set; }
        public string ThmColr { get; set; }

    }

    public class StickerCategoryDTO
    {

        public int SCtId { get; set; }
        public string SctName { get; set; }
        public int SClId { get; set; }
        public string CgBnrImg { get; set; }
        public int Rnk { get; set; }
        public string DtlImg { get; set; }
        public bool CgNw { get; set; }
        public string Icn { get; set; }
        public float Prz { get; set; }
        public bool Free { get; set; }
        public string Dcpn { get; set; }
        public bool Downloaded { get; set; }
        public long DownloadTime { get; set; }
        public int SortOrder { get; set; }
        public bool IsDefault { get; set; }
        public long LastUsedDate { get; set; }
        public bool IsTop { get; set; }
        public bool IsBannerVisible { get; set; }

        private List<StickerImagesDTO> _ImagesList = new List<StickerImagesDTO>();
        public List<StickerImagesDTO> ImagesList
        {
            get { return _ImagesList; }
            set { _ImagesList = value; }
        }

        public void UpdateDTO(StickerCategoryDTO categoryDTO)
        {
            this.SctName = categoryDTO.SctName;
            this.CgBnrImg = categoryDTO.CgBnrImg;
            this.Rnk = categoryDTO.Rnk;
            this.DtlImg = categoryDTO.DtlImg;
            this.CgNw = categoryDTO.CgNw;
            this.Icn = categoryDTO.Icn;
            this.Prz = categoryDTO.Prz;
            this.Free = categoryDTO.Free;
            this.Dcpn = categoryDTO.Dcpn;
        }
    }

    public class StickerImagesDTO
    {

        public int ImId { get; set; }
        public int SCtId { get; set; }
        public int SClId { get; set; }
        public string ImUrl { get; set; }

        public bool _IsRecent = false;
        public bool IsRecent
        {
            get { return _IsRecent; }
            set { _IsRecent = value; }
        }
    }

    public class BannerDTO
    {
        public int SClId { get; set; }
        public int SCtId { get; set; }
    }

    public class MarketStickerDTO
    {

        public bool sucs { get; set; }

        public List<MarketStickerDynamicCategoryDTO> dCatList { get; set; }
        public List<int> pCat { get; set; }
        public List<int> fCat { get; set; }
        public List<int> nCat { get; set; }
        public List<int> tCat { get; set; }
        public List<int> allCat { get; set; }

        public List<MarketStickerCategoryDTO> plrBnr { get; set; }
        public List<MarketStickerCategoryDTO> nwBnr { get; set; }
        public List<MarketStickerCategoryDTO> allBnr { get; set; }

        public List<MarketStickerCategoryDTO> catList { get; set; }
        public List<MarkertStickerImagesDTO> ImagesList { get; set; }
        public List<MarketStickerCollectionDTO> colList { get; set; }
        public List<MarketStickerLanguageyDTO> langLst { get; set; }
    }

    public class MarketStickerDynamicCategoryDTO
    {
        public int id { get; set; }
        public string name { get; set; }
        public int shp { get; set; }
        public List<MarketStickerCategoryDTO> banner { get; set; }
        public List<int> catIds { get; set; }
    }

    public class MarketStickerCategoryDTO
    {

        public int sCtId { get; set; }
        public int sClId { get; set; }
        public string sctName { get; set; }
        public bool cgNw { get; set; }
        public bool free { get; set; }
        public bool Downloaded { get; set; }
        public bool IsDefault { get; set; }
        public long DownloadTime { get; set; }
        public bool IsBannerVisible { get; set; }
        public int Rnk { get; set; }
        public bool IsTop { get; set; }
        public float Prz { get; set; }
        public string Dcpn { get; set; }
        public int SortOrder { get; set; }
        public bool _IsVisible = true;
        public bool IsVisible
        {
            get { return _IsVisible; }
            set { _IsVisible = value; }
        }
        public long LastUsedDate { get; set; }

        private bool _IsNewStickerSeen = false;
        public bool IsNewStickerSeen
        {
            get { return _IsNewStickerSeen; }
            set { _IsNewStickerSeen = value; }
        }

        public string _Icon = "icon.png";
        public string Icon
        {
            get { return _Icon; }
            set { _Icon = value; }
        }

        public string _cgBnrImg = "banner.png";
        public string cgBnrImg
        {
            get { return _cgBnrImg; }
            set { _cgBnrImg = value; }
        }

        public string _DetailImg = "details.png";
        public string DetailImg
        {
            get { return _DetailImg; }
            set { _DetailImg = value; }
        }

        private string _ThumbDetailImage = "thumbdetails.png";
        public string ThumbDetailImage
        {
            get { return _ThumbDetailImage; }
            set { _ThumbDetailImage = value; }
        }

        private List<MarkertStickerImagesDTO> _ImagesList = new List<MarkertStickerImagesDTO>();
        public List<MarkertStickerImagesDTO> ImagesList
        {
            get { return _ImagesList; }
            set { _ImagesList = value; }
        }

        public void UpdateNewStickerCategoryDTO(MarketStickerCategoryDTO marketCtgDTO)
        {

            this.sCtId = marketCtgDTO.sCtId;
            this.sClId = marketCtgDTO.sClId;
            this.sctName = marketCtgDTO.sctName;
            this.cgNw = marketCtgDTO.cgNw;
            this.free = marketCtgDTO.free;
            this.Rnk = marketCtgDTO.Rnk;
            this.IsNewStickerSeen = marketCtgDTO.IsNewStickerSeen;
        }

        public void UpdateMarketDTOByCategoryDTO(StickerCategoryDTO categoryDTO)
        {

            this.sCtId = categoryDTO.SCtId;
            this.sClId = categoryDTO.SClId;
            this.sctName = categoryDTO.SctName;
            this.cgNw = categoryDTO.CgNw;
            this.free = categoryDTO.Free;
            this.Rnk = categoryDTO.Rnk;
        }
    }

    public class MarketStickerCollectionDTO
    {

        public int sClId { get; set; }
        public string name { get; set; }
        public string bnrImg { get; set; }
        public string ThmColr { get; set; }
        public List<int> catIds { get; set; }
    }

    public class MarkertStickerImagesDTO
    {
        public int imId { get; set; }
        public int sCtId { get; set; }
        public int sClId { get; set; }
        public string imUrl { get; set; }

        public bool _IsRecent = false;
        public bool IsRecent
        {
            get { return _IsRecent; }
            set { _IsRecent = value; }
        }
    }

    public class MarketStickerLanguageyDTO
    {
        //public int id { get; set; }
        //public string code { get; set; }
        //public string countrySymbol { get; set; }
        public string name { get; set; }
        public List<int> catIds { get; set; }
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

﻿using System.Collections.Generic;

namespace Models.Entity
{
    public class MediaContentDTO
    {
        public List<SingleMediaDTO> MediaList { get; set; }
        public int MediaType { get; set; }
        public long AlbumId { get; set; }
        public long UserTableID { get; set; }


        // for AlbumContent DTO
        public string AlbumName { get; set; }
        public string AlbumImageUrl { get; set; }
        public int TotalMediaCount { get; set; }
    }
}

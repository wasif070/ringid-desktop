﻿using System;

namespace Models.Entity
{
    public class EducationDTO
    {
        public Guid Id { get; set; }
        public long UpdateTime { get; set; }
        public string SchoolName { get; set; }
        public string Concentration { get; set; }
        public string Description { get; set; }
        public int AttendedFor { get; set; } //1=College, 2=Graduate School
        public string Degree { get; set; }
        public bool Graduated { get; set; }
        public bool IsSchool { get; set; } //if you add a college then 0, else for school 1
        public long FromTime { get; set; }
        public long ToTime { get; set; }
    }
}

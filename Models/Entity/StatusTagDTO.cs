﻿
namespace Models.Entity
{
    public class StatusTagDTO
    {
        public string FullName { get; set; }
        public long UserTableID { get; set; }
        // public long UserIdentity { get; set; }
        public int Index { get; set; }
    }
}

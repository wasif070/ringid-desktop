﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Entity
{
    public class DigitsData
    {
        public bool IsSccess { set; get; }
        public string MobileDialingCode { set; get; }
        public string PhoneNumber { set; get; }
    }
}

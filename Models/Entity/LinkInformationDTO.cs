﻿using System.Collections.Generic;

namespace Models.Entity
{
    public class LinkInformationDTO
    {
        public string Url { get; set; }
        public string Title { get; set; }
        public List<string> ImageUrls { get; set; }
        public string Domain { get; set; }
        public string Description { get; set; }
        public int MediaType { get; set; } //music=1,video=2, others=0
    }
}

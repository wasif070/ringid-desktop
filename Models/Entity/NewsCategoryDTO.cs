﻿
namespace Models.Entity
{
    public class NewsCategoryDTO
    {
        public string CategoryName { get; set; }
        public int CategoryId { get; set; }
        public int CategoryType { get; set; }
        public bool Subscribed { get; set; }
    }
}

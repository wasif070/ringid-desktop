﻿
namespace Models.Entity
{
    public class CommentDTO
    {
        public long CommentId { get; set; }
        public string Comment { get; set; }
        public long Time { get; set; }
        public long UserIdentity { get; set; }
        public string FullName { get; set; }
        public string ProfileImage { get; set; }
        public bool ShowContinue { get; set; }
        public int TotalLikeComment { get; set; }
        public short ILikeComment { get; set; }
    }
}

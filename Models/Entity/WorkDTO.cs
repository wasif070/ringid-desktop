﻿using System;

namespace Models.Entity
{
    public class WorkDTO
    {
        public Guid Id { get; set; }
        public long UpdateTime { get; set; }
        public string Position { get; set; }
        public string Description { get; set; }
        public string CompanyName { get; set; }
        public string City { get; set; }
        public long FromTime { get; set; }
        public long ToTime { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Entity
{
    public class SignedInInfoDTO
    {
        public SignedInInfoDTO()
        {
            errorMsg = null;
            versionMsg = string.Empty;
            isDownloadMandatory = false;
            gotResponseFromServer = false;
            reasonCode = 0;
            ReqeustCancelled = false;
        }
        public bool isDownloadMandatory { get; set; }
        public bool isSuccess { get; set; }
        public string errorMsg { get; set; }
        public string versionMsg { get; set; }
        public bool gotResponseFromServer { get; set; }
        public int reasonCode { get; set; }
        public bool ReqeustCancelled { get; set; }
    }
}

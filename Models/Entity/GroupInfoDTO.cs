﻿using Models.Constants;
using System;
using System.Collections.Generic;

namespace Models.Entity
{
    public class GroupInfoDTO
    {
        public GroupInfoDTO()
        {
            this.ImSoundEnabled = SettingsConstants.VALUE_RINGID_IM_SOUND;
            this.ImNotificationEnabled = SettingsConstants.VALUE_RINGID_IM_NOTIFICATION;
            this.ImPopupEnabled = SettingsConstants.VALUE_RINGID_IM_POPUP;
        }

        public long GroupID { get; set; }
        public string GroupName { get; set; }
        public string GroupProfileImage { get; set; }
        public bool IsPartial { get; set; }
        public int IntegerStatus { get; set; }
        public int NumberOfMembers { get; set; }
        public string ChatMinPacketID { get; set; }
        public string ChatMaxPacketID { get; set; }
        public string ChatBgUrl { get; set; }
        public bool ImSoundEnabled { get; set; }
        public bool ImNotificationEnabled { get; set; }
        public bool ImPopupEnabled { get; set; }

        private List<GroupMemberInfoDTO> _MemberList = new List<GroupMemberInfoDTO>();
        public List<GroupMemberInfoDTO> MemberList
        {
            get { return _MemberList; }
            set { _MemberList = value; }
        }
    }

    public class GroupMemberInfoDTO
    {
        public long GroupID { get; set; }
        public long UserTableID { get; set; }
        public long RingID { get; set; }
        public string FullName { get; set; }
        public int MemberAccessType { get; set; }
        public long MemberAddedBy { get; set; }
        public int IntegerStatus { get; set; }
    }

}

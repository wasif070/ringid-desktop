﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Entity
{
    public class DateEventDTO
    {
        public DateEventDTO()
        {
        }

        public DateEventDTO(int day, int month, string url)
        {
            this.Day = day;
            this.Month = month;
            this.Url = url;
        }

        public DateEventDTO(DateEventDTO eventDTO)
        {
            this.Day = eventDTO.Day;
            this.Month = eventDTO.Month;
            this.Url = eventDTO.Url;
        }

        public int Day { get; set; }
        public int Month { get; set; }
        public string Url { get; set; }
    }
}

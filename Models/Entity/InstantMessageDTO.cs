﻿
namespace Models.Entity
{
    public class InstantMessageDTO
    {
        public int MsgType { get; set; } //0 for default,1 for customized msgs
        public string InstantMessage { get; set; }
    }
}

﻿
namespace Models.Entity
{
    public class MusicPageDTO
    {
        public long UserTableID;
        public long RingID;
        public string MusicPageName;
        public bool IsSubscribed;
        public int SubscriberCount;
        public string ProfileImage;
        public string CoverImage;
        public long MusicPageId { get; set; }
        public string PageSlogan { get; set; }
        public long PageCatId { get; set; }
        public string PageCatName { get; set; }
        public bool IsProfileHidden { get; set; }
    }
}

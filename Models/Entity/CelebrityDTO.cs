﻿
using System.Collections.Generic;
namespace Models.Entity
{
    public class CelebrityDTO
    {
        public long RingID { get; set; } //ringid, celebrityid
        public long UserTableID { get; set; }
        public string CelebrityName { get; set; }
        public string ProfileImage { get; set; }
        public string CoverImage { get; set; }
        public int SubscriberCount { get; set; }
        public int PostCount { get; set; }
        public bool IsSubscribed { get; set; }
        public string CelebrityCountry { get; set; }
        public List<string> CelebrityCatNameArray { get; set; }
        public string CelebrityCategoryName { get; set; }
        public long OnlineTime { get; set; }
        public string MoodMessage { get; set; }
        public bool IsProfileHidden { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Entity
{
    public class CircleMemberDTO
    {
        public long PivotId { get; set; }
        public long RingID { get; set; }
        public string FullName { get; set; }
        public bool IsAdmin { get; set; }
        public long CircleId { get; set; }
        public long UpdateTime { get; set; }
        public long UserTableID { get; set; }
        public short IntegerStatus { get; set; }
        public string ProfileImage { get; set; }
    }
}

﻿
using System;
namespace Models.Entity
{
    public class ImageDTO
    {
        private long fndId;
        public long FriendTableId
        {
            get { return fndId; }
            set { fndId = value; }
        }

        private string albId;
        public string AlbumId
        {
            get { return albId; }
            set { albId = value; }
        }

        private string iurl;
        public string ImageUrl
        {
            get { return iurl; }
            set { iurl = value; }
        }

        private string albn;
        public string AlbumName
        {
            get { return albn; }
            set { albn = value; }
        }

        private string cptn;
        public string Caption
        {
            get { return cptn; }
            set { cptn = value; }
        }

        private long imgId;
        public long ImageId
        {
            get { return imgId; }
            set { imgId = value; }
        }

        public Guid NewsFeedId { get; set; }

        public int Privacy { get; set; }

        private long tm;
        public long Time
        {
            get { return tm; }
            set { tm = value; }
        }

        private int ih;
        public int ImageHeight
        {
            get { return ih; }
            set { ih = value; }
        }

        private int iw;
        public int ImageWidth
        {
            get { return iw; }
            set { iw = value; }
        }

        private int imT;
        public int ImageType
        {
            get { return imT; }
            set { imT = value; }
        }

        private long nl;
        public long NumberOfLikes
        {
            get { return nl; }
            set { nl = value; }
        }

        private int il;
        public int iLike
        {
            get { return il; }
            set { il = value; }
        }

        private int ic;
        public int iComment
        {
            get { return ic; }
            set { ic = value; }
        }

        private long nc;
        public long NumberOfComments
        {
            get { return nc; }
            set { nc = value; }
        }

        private int timg;
        public int TotalImagesInAlbum
        {
            get { return timg; }
            set { timg = value; }
        }

        private int tim;
        public int TotalImage
        {
            get { return tim; }
            set { tim = value; }
        }

        private long utid;
        public long UserTableID
        {
            get { return utid; }
            set { utid = value; }
        }

        private string fn;
        public string FullName
        {
            get { return fn; }
            set { fn = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Entity
{
    public class CallerDTO : ICloneable
    {
        public long UserTableID { set; get; }
        public string CallServerIP { set; get; }
        public int CallRegPort { set; get; }
        public int Presence { set; get; }
        public int Mood { set; get; }
        public int Device { set; get; }
        public string CallID { set; get; }
        public long Time { set; get; }
        public string DeviceToken { set; get; }
        public string FullName { set; get; }
        //public bool IsDivertedCall { set; get; }
        public int ApplicationType { set; get; }
        public int VoiceBindingPort { set; get; }
        public string ConnectedWith { set; get; }
        public string ErrorMessage { set; get; }
        public int ReasonCode { set; get; }
        public string ProfileImage { set; get; }
        /*please calT (1=VoiceCall and 2=VideoCall ) parameter with 174*/
        public int calT { set; get; }
        public bool IsIncomming { set; get; }
        public string BusyMessage { set; get; }
        public string CallEndMessage { set; get; }
        public string BusyMessageToSend { set; get; }
        public int VideoCommunicationPort { set; get; }
        public int P2PCall { set; get; }
        //   public bool VideoCallInitiateByMe { set; get; }
        //    public int AnswerWithVideo { set; get; }
        // public bool IsAudioP2PEnabled { set; get; }
        // public bool IsVideoP2PEnabled { set; get; }
        //  public bool CloseWithOutWarning { set; get; }
        public bool IsSuccess { get; set; }

        int remotePushType = 1;

        public int RemotePushType
        {
            get { return remotePushType; }
            set { remotePushType = value; }
        }

        bool muted = false;

        public bool Muted
        {
            get { return muted; }
            set { muted = value; }
        }

        int voiceRegisterd = -1;

        public int VoiceRegisterd
        {
            get { return voiceRegisterd; }
            set { voiceRegisterd = value; }
        }

        long callSessionID;

        public long CallSessionID
        {
            get { return callSessionID; }
            set { callSessionID = value; }
        }

        //bool isCallEnded = false;

        //public bool IsCallEnded
        //{
        //    get { return isCallEnded; }
        //    set { isCallEnded = value; }
        //}

        bool isNeedToPlayEndCallSound = true;

        public bool IsNeedToPlayEndCallSound
        {
            get { return isNeedToPlayEndCallSound; }
            set { isNeedToPlayEndCallSound = value; }
        }

        bool isApplicationClosed = false;

        public bool IsApplicationClosed
        {
            get { return isApplicationClosed; }
            set { isApplicationClosed = value; }
        }

        //bool isNeedEdToSendCancel = true;

        //public bool IsNeedEdToSendCancel
        //{
        //    get { return isNeedEdToSendCancel; }
        //    set { isNeedEdToSendCancel = value; }
        //}

        #region ICloneable Members

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
    }
}

﻿using Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Entity
{
    public class RoomDTO
    {

        public string RoomID { get; set; }
        public string RoomProfileImage { get; set; }
        public string RoomName { get; set; }
        public bool IsPartial { get; set; }
        public string ChatMinPacketID { get; set; }
        public string ChatMaxPacketID { get; set; }
        public string ChatBgUrl { get; set; }
        public int IntegerStatus { get; set; }
        public int NumberOfMembers { get; set; }
        public bool ImSoundEnabled { get; set; }
        public bool ImNotificationEnabled { get; set; }
        public bool ImPopupEnabled { get; set; }
        
    }
}

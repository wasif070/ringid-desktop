﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Utility;

namespace Models.Entity
{
    public class ChatInitiatorDTO
    {
        public int ActionType { get; set; }
        public long FriendTableID { get; set; }
        public long GroupID { get; set; }
        public string ChatServerIP { get; set; }
        public int ChatRegisterPort { get; set; }
        public string FullName { get; set; }
        public string ProfileImage { get; set; }
        public int Device { get; set; }
        public int Presence { get; set; }
        public int Mood { get; set; }
        public string DeviceToken { get; set; }
        public long LastOnlineTime { get; set; }
        public int ApplicationType { get; set; }
        public int RemotePushType { get; set; }
        public List<long> BlockByList { get; set; }
        public List<long> AnonymousBlockByList { get; set; }
        public int ReasonCode { get; set; }
        public long ServerPacketID { get; set; }
        public long Time { get; set; }

        public override string ToString()
        {
            return "ChatInitiatorDTO ["
                    + "aT=" + ActionType + ", spID=" + ServerPacketID + ", fId=" + FriendTableID + ", gId=" + GroupID + ", fn=" + FullName + ", IP=" + ChatServerIP
                    + ", Port=" + ChatRegisterPort + ", dvc=" + Device + ", Prsc=" + Presence + ", md=" + Mood + ", lot=" + LastOnlineTime + ", tm=" + Time + ", aT=" + ApplicationType
                    + ", rpt=" + RemotePushType + ", rc=" + ReasonCode + ", dt=" + DeviceToken + ", bb=" + BlockByList.ToArrayString() + ", abb=" + AnonymousBlockByList.ToArrayString() 
                    + "]";
        }
    }
}

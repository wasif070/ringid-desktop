﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Models.Entity
{

    //{"nfId":4130,"uId":"2110012548","utId":888,"sts":"","tm":1441285902820,"at":1441285902820,"type":1,"nc":0,"nl":0,"ns":0,"il":0,"ic":0,"is":0,
    //"sucs":true,"fn":"Arafat","afn":"","prIm":"2110012548/1441289274404.jpg","prImPr":1,
    //"imageList":[{"imgId":3901,"iurl":"2110012548/1441285899734.jpg","ih":225,"iw":300,"imT":2,"albId":"profileimages","albn":"Profile Photos","tm":1441285902815,"nl":0,"il":0,"ic":0,"nc":0}],
    //"sfId":0,"tim":1,"grpId":0,"actvt":0,"auId":0,"imc":0,"sc":false}
    /*{"seq":"1/10","newsFeedList":
[
{"nfId":4215,"uId":"2110010086","fndId":"2110011654","utId":71,"sts":"1234","tm":1441601468053,"at":1441601468053,"type":2,"nc":0,"nl":0,"ns":0,"il":0,"ic":0,"is":0,"sucs":true,
"fn":"sirat samyoun","ffn":"sam6211","afn":"","prIm":"2110010086/1441278166174.jpg","prImPr":1,"fprIm":"2110011654/1439970835114.jpg","fprImPr":1,"sfId":0,"grpId":0,"actvt":0,"auId":0,"imc":0,"sc":false},

{"nfId":4208,"uId":"2110010086","utId":71,"sts":"","tm":1441598111963,"at":1441598111963,"type":3,"nc":0,"nl":0,"ns":0,"il":0,"ic":0,"is":0,"sucs":true,
"fn":"sirat samyoun","afn":"","prIm":"2110010086/1441278166174.jpg","prImPr":1,"imageList":[somethgin],"sfId":0,"tim":6,"grpId":0,"actvt":0,"auId":0,"imc":6,"sc":false}

],"sucs":true,"lmt":20,"rc":0}*/
    //[Serializable]
    public class FeedDTO
    {
        //[XmlElement("NewsfeedId")]
        public Guid NewsfeedId { get; set; }

        //[XmlElement("UserIdentity")]
        public long RingID { get; set; }

        //[XmlElement("UserTableID")]
        public long UserTableID { get; set; }

        //[XmlElement("Status")]
        public string Status { get; set; }

        //[XmlElement("Time")]
        public long Time { get; set; }

        //[XmlElement("ActualTime")]
        public long ActualTime { get; set; }

        //[XmlElement("BookPostType")]
        public short BookPostType { get; set; }

        //[XmlElement("NumberOfComments")]
        public long NumberOfComments { get; set; }

        //[XmlElement("NumberOfLikes")]
        public long NumberOfLikes { get; set; }

        //[XmlElement("NumberOfShares")]
        public long NumberOfShares { get; set; }

        //[XmlElement("ILike")]
        public short ILike { get; set; }

        //[XmlElement("IComment")]
        public short IComment { get; set; }

        //[XmlElement("IShare")]
        public short IShare { get; set; }

        //[XmlElement("sucs")]
        public bool sucs { get; set; }

        //[XmlElement("FullName")]
        public string FullName { get; set; }

        //[XmlElement("ActivistFirstName")]
        public string ActivistFirstName { get; set; }

        public short SuperType { get; set; }

        //[XmlElement("FeedSubType")]
        public short FeedSubType { get; set; }

        //[XmlElement("FeedCategory")]
        public short FeedCategory { get; set; }

        //[XmlElement("ProfileImage")]
        public string ProfileImage { get; set; }

        //[XmlElement("ProfileImagePrivacy")]
        public short ProfileImagePrivacy { get; set; }

        //[XmlElement("ImageList")]
        public List<ImageDTO> ImageList { get; set; }

        //[XmlElement("SharedFeedId")]
        public long SharedFeedId { get; set; }

        //[XmlElement("TotalImage")]
        public int TotalImage { get; set; }

        //[XmlElement("GroupId")]
        public long GroupId { get; set; }

        //[XmlElement("CircleName")]
        public string CircleName { get; set; }

        //[XmlElement("Activity")]
        public short Activity { get; set; }

        //[XmlElement("ActivistId")]
        public long ActivistId { get; set; }

        //[XmlElement("ImageInCollection")]
        public short ImageInCollection { get; set; }

        //[XmlElement("ShowContinue")]
        public bool ShowContinue { get; set; }

        //[XmlElement("LinkMediaType")]
        public int LinkMediaType { get; set; }

        //[XmlElement("FriendId")]
        public long FriendUserTableId { get; set; }

        //[XmlElement("FriendFirstName")]
        public string FriendFirstName { get; set; }

        //[XmlElement("FriendProfileImage")]
        public string FriendProfileImage { get; set; }

        //[XmlElement("FriendProfileImagePrivacy")]
        public short FriendProfileImagePrivacy { get; set; }

        //[XmlElement("locationDTO")]
        public LocationDTO locationDTO { get; set; }

        public NewsDTO NewsInfo { get; set; }

        //[XmlElement("DoingActivity")]
        public string DoingActivity { get; set; }

        //[XmlElement("DoingImageUrl")]
        public string DoingImageUrl { get; set; }

        //[XmlElement("ParentFeed")]
        public FeedDTO ParentFeed
        {
            get;
            set;
        }

        //[XmlElement("WhoShare")]
        public FeedDTO WhoShare { get; set; }

        //[XmlElement("WhoShareList")]
        public List<FeedDTO> WhoShareList { get; set; }

        //[XmlElement("PreviewImgUrl")]
        public string PreviewImgUrl { get; set; }

        //[XmlElement("PreviewUrl")]
        public string PreviewUrl { get; set; }

        //[XmlElement("PreviewDesc")]
        public string PreviewDesc { get; set; }

        //[XmlElement("PreviewDomain")]
        public string PreviewDomain { get; set; }

        //[XmlElement("PreviewTitle")]
        public string PreviewTitle { get; set; }

        //[XmlElement("TotalTaggedFriends")]
        public short TotalTaggedFriends { get; set; }

        //[XmlElement("TaggedFriendsList")]
        public List<UserBasicInfoDTO> TaggedFriendsList { get; set; }

        //[XmlElement("MediaContent")]
        public MediaContentDTO MediaContent;

        //[XmlElement("StatusTags")]
        public List<StatusTagDTO> StatusTags { get; set; }

        //[XmlElement("Privacy")]
        public int Privacy { get; set; }

        public long FeedViewCount { get; set; }

        public bool IsSaved { get; set; }

        public bool Edited { get; set; }

        public int Validity { get; set; }

    }
}

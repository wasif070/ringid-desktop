﻿
using Models.Constants;
using System;
namespace Models.Entity
{
    public class UserBasicInfoDTO
    {
        public UserBasicInfoDTO()
        {
            this.ImSoundEnabled = SettingsConstants.VALUE_RINGID_IM_SOUND;
            this.ImNotificationEnabled = SettingsConstants.VALUE_RINGID_IM_NOTIFICATION;
            this.ImPopupEnabled = SettingsConstants.VALUE_RINGID_IM_POPUP;
        }

        private long uId;

        public long RingID
        {
            get { return uId; }
            set { uId = value; }
        }
        private bool admin;

        public bool Admin
        {
            get { return admin; }
            set { admin = value; }
        }
        private int psnc;

        public int Presence
        {
            get { return psnc; }
            set { psnc = value; }
        }
        private string fn;

        public string FullName
        {
            get { return fn; }
            set { fn = value; }
        }
        private string gr;

        public string Gender
        {
            get { return gr; }
            set { gr = value; }
        }
        private string mbl;

        public string MobileNumber
        {
            get { return mbl; }
            set { mbl = value; }
        }
        private long ut;

        public long UpdateTime
        {
            get { return ut; }
            set { ut = value; }
        }
        private bool isProfileHidden;

        public bool IsProfileHidden
        {
            get { return isProfileHidden; }
            set { isProfileHidden = value; }
        }
        private int del;

        public int Delete
        {
            get { return del; }
            set { del = value; }
        }
        private string prIm;

        public string ProfileImage
        {
            get { return prIm; }
            set { prIm = value; }
        }

        private string el;

        public string Email
        {
            get { return el; }
            set { el = value; }
        }

        private string smid;
        public string SocialMediaId
        {
            get { return smid; }
            set { smid = value; }
        }
        //private string it;
        //public string InputToken
        //{
        //    get { return it; }
        //    set { it = value; }
        //}
        //private string wim;

        //public string Wim
        //{
        //    get { return wim; }
        //    set { wim = value; }
        //}
        private string cnty;

        public string Country
        {
            get { return cnty; }
            set { cnty = value; }
        }
        private int frnS;

        public int FriendShipStatus
        {
            get { return frnS; }
            set { frnS = value; }
        }
        private string mblDc;

        public string MobileDialingCode
        {
            get { return mblDc; }
            set { mblDc = value; }
        }
        private int smP;

        public int SmsServerPort
        {
            get { return smP; }
            set { smP = value; }
        }
        private string smIp;

        public string SmsServerIp
        {
            get { return smIp; }
            set { smIp = value; }
        }
        //private long blc;

        //public long Blc
        //{
        //    get { return blc; }
        //    set { blc = value; }
        //}
        private long bDay;

        public long BirthDay
        {
            get { return bDay; }
            set { bDay = value; }
        }
        private short[] pvc;

        public short[] Privacy
        {
            get { return pvc; }
            set { pvc = value; }
        }
        private short emailPrivacy;

        public short EmailPrivacy
        {
            get { return emailPrivacy; }
            set { emailPrivacy = value; }
        }
        private short mobilePrivacy;

        public short MobilePrivacy
        {
            get { return mobilePrivacy; }
            set { mobilePrivacy = value; }
        }
        private short profileImagePrivacy;

        public short ProfileImagePrivacy
        {
            get { return profileImagePrivacy; }
            set { profileImagePrivacy = value; }
        }
        private short birthdayPrivacy;

        public short BirthdayPrivacy
        {
            get { return birthdayPrivacy; }
            set { birthdayPrivacy = value; }
        }
        private short coverImagePrivacy;

        public short CoverImagePrivacy
        {
            get { return coverImagePrivacy; }
            set { coverImagePrivacy = value; }
        }
        private long divertedFriendIdentity;
        public long DivertedFriendIdentity
        {
            get { return divertedFriendIdentity; }
            set { divertedFriendIdentity = value; }
        }
        //private string re;

        //public string RingEmail
        //{
        //    get { return re; }
        //    set { re = value; }
        //}
        private int emVsn;

        public int EmoticonVersion
        {
            get { return emVsn; }
            set { emVsn = value; }
        }
        //private int fnfHd;

        //public int FnfHeaderVersion
        //{
        //    get { return fnfHd; }
        //    set { fnfHd = value; }
        //}
        private long cut;

        public long ContactUpdateTime
        {
            get { return cut; }
            set { cut = value; }
        }
        //private string dNo;

        //public string DialledNumber
        //{
        //    get { return dNo; }
        //    set { dNo = value; }
        //}
        private int dvc;

        public int Device
        {
            get { return dvc; }
            set { dvc = value; }
        }
        private string dt;

        public string DeviceToken
        {
            get { return dt; }
            set { dt = value; }
        }
        private string cIm;

        public string CoverImage
        {
            get { return cIm; }
            set { cIm = value; }
        }
        private Guid prImId;

        public Guid ProfileImageId
        {
            get { return prImId; }
            set { prImId = value; }
        }
        private Guid cImId;

        public Guid CoverImageId
        {
            get { return cImId; }
            set { cImId = value; }
        }
        private int cimX = 0;

        public int CropImageX
        {
            get { return cimX; }
            set { cimX = value; }
        }
        private int cimY = 0;

        public int CropImageY
        {
            get { return cimY; }
            set { cimY = value; }
        }
        //private string nm;

        //public string Nm
        //{
        //    get { return nm; }
        //    set { nm = value; }
        //}
        private int iev;

        public int IsEmailVerified
        {
            get { return iev; }
            set { iev = value; }
        }
        private int imnv;

        public int IsMobileNumberVerified
        {
            get { return imnv; }
            set { imnv = value; }
        }
        private long utId;

        public long UserTableID
        {
            get { return utId; }
            set { utId = value; }
        }
        //private int ct;

        //public int ContactType
        //{
        //    get { return ct; }
        //    set { ct = value; }
        //}
        //private bool incomingNotification;

        //public bool IncomingNotification
        //{
        //    get { return incomingNotification; }
        //    set { incomingNotification = value; }
        //}
        //private int bv; //blocked

        //public int BlockedValue
        //{
        //    get { return bv; }
        //    set { bv = value; }
        //}
        //private int nct;

        //public int NewContactType
        //{
        //    get { return nct; }
        //    set { nct = value; }
        //}
        //private bool iscr;

        //public bool IsChangeRequester
        //{
        //    get { return iscr; }
        //    set { iscr = value; }
        //}
        private string cc;

        public string CurrentCity
        {
            get { return cc; }
            set { cc = value; }
        }
        private string hc;

        public string HomeCity
        {
            get { return hc; }
            set { hc = value; }
        }
        private string am;

        public string AboutMe
        {
            get { return am; }
            set { am = value; }
        }
        private long mDay;

        public long MarriageDay
        {
            get { return mDay; }
            set { mDay = value; }
        }
        private long lot;

        public long LastOnlineTime
        {
            get { return lot; }
            set { lot = value; }
        }
        private long nmf; //NumberOfMutualFriends

        public long NumberOfMutualFriends
        {
            get { return nmf; }
            set { nmf = value; }
        }
        //private int cfs; //commonFriendSuggestion

        //public int CommonFriendsSuggestion
        //{
        //    get { return cfs; }
        //    set { cfs = value; }
        //}
        //private int pns; //PhoneNumberSuggestion

        //public int PhoneNumberSuggestion
        //{
        //    get { return pns; }
        //    set { pns = value; }
        //}
        //private int cls; //contact list suggestion

        //public int ContactListSuggestion
        //{
        //    get { return cls; }
        //    set { cls = value; }
        //}
        private int mb; //matched by

        public int MatchedBy
        {
            get { return mb; }
            set { mb = value; }
        }
        private long numberOfChats; //NumberOfChats

        public long NumberOfChats
        {
            get { return numberOfChats; }
            set { numberOfChats = value; }
        }
        private long numberOfCalls; //NumberOfCalls

        public long NumberOfCalls
        {
            get { return numberOfCalls; }
            set { numberOfCalls = value; }
        }
        private long cft;
        public long CallForwardingText
        {

            get { return cft; }
            set { cft = value; }
        }
        private double favoriteRank; // 

        public double FavoriteRank
        {
            get { return favoriteRank; }
            set { favoriteRank = value; }
        }

        private bool isFavouriteTemp;

        public bool IsFavouriteTemp
        {
            get { return isFavouriteTemp; }
            set { isFavouriteTemp = value; }
        }

        private bool isTopTemp;

        public bool IsTopTemp
        {
            get { return isTopTemp; }
            set { isTopTemp = value; }
        }

        private int mood;
        public int Mood
        {
            get { return mood; }
            set { mood = value; }
        }

        private int cla;
        public int CallAccess
        {
            get { return cla; }
            set { cla = value; }
        }

        private int chta;
        public int ChatAccess
        {
            get { return chta; }
            set { chta = value; }
        }

        private int fda;
        public int FeedAccess
        {
            get { return fda; }
            set { fda = value; }
        }

        private int anc;
        public int AnonymousCallAccess
        {
            get { return anc; }
            set { anc = value; }
        }

        private int ancht;
        public int AnonymousChatAccess
        {
            get { return ancht; }
            set { ancht = value; }
        }

        private string _ChatMinPacketID;
        public string ChatMinPacketID
        {
            get { return _ChatMinPacketID; }
            set { _ChatMinPacketID = value; }
        }

        private string _ChatMaxPacketID;
        public string ChatMaxPacketID
        {
            get { return _ChatMaxPacketID; }
            set { _ChatMaxPacketID = value; }
        }

        private string _ChatBgUrl;
        public string ChatBgUrl
        {
            get { return _ChatBgUrl; }
            set { _ChatBgUrl = value; }
        }

        private int _IsSecretVisible = 1;
        public int IsSecretVisible
        {
            get { return _IsSecretVisible; }
            set { _IsSecretVisible = value; }
        }

        private int fbvld;
        public int FacebookValidated { get { return fbvld; } set { fbvld = value; } }
        private int twtrvld;
        public int TwitterValidated { get { return twtrvld; } set { twtrvld = value; } }

        private int contactType;

        public int ContactType
        {
            get { return contactType; }
            set { contactType = value; }
        }
        private int _friendListType = -1;
        public int FriendListType
        {
            get { return _friendListType; }
            set { _friendListType = value; }
        }

        private long contactAddedTime;
        public long ContactAddedTime
        {
            get { return contactAddedTime; }
            set { contactAddedTime = value; }
        }

        private int _ContactAddedReadUnread;
        public int ContactAddedReadUnread
        {
            get { return _ContactAddedReadUnread; }
            set { _ContactAddedReadUnread = value; }
        }

        private int _IncomingStatus;
        public int IncomingStatus
        {
            get { return _IncomingStatus; }
            set { _IncomingStatus = value; }
        }

        //Friend Settings
        private int _AllowIncomingFriendRequest;
        public int AllowIncomingFriendRequest
        {
            get { return _AllowIncomingFriendRequest; }
            set { _AllowIncomingFriendRequest = value; }
        }
        private int _AllowFriendsToAddMe;
        public int AllowFriendsToAddMe
        {
            get { return _AllowFriendsToAddMe; }
            set { _AllowFriendsToAddMe = value; }
        }
        private int _AutoAddFriends;
        public int AutoAddFriends
        {
            get { return _AutoAddFriends; }
            set { _AutoAddFriends = value; }
        }

        private bool _ImSoundEnabled;
        public bool ImSoundEnabled
        {
            get { return _ImSoundEnabled; }
            set { _ImSoundEnabled = value; }
        }

        private bool _ImNotificationEnabled;
        public bool ImNotificationEnabled
        {
            get { return _ImNotificationEnabled; }
            set { _ImNotificationEnabled = value; }
        }

        private bool _ImPopupEnabled;
        public bool ImPopupEnabled
        {
            get { return _ImPopupEnabled; }
            set { _ImPopupEnabled = value; }
        }

        private int _NotificationValidity;
        public int NotificationValidity
        {
            get { return _NotificationValidity; }
            set { _NotificationValidity = value; }
        }

        public Guid AlbumId { get; set; }

        public DateTime BirthDate { get; set; }

        public DateTime MarriageDate { get; set; }
    }
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Entity
{
    public class ActivityDTO
    {

        public long ID { get; set; }
        public long ActivityBy { get; set; }
        public long FriendTableID { get; set; }
        public long GroupID { get; set; }
        public int ActivityType { get; set; }
        public int MessageType { get; set; }
        public long UpdateTime { get; set; }
        public string GroupName { get; set; }
        public string GroupProfileImage { get; set; }
        public string PacketID { get; set; }
        public bool IsUnread { get; set; }
        private List<ActivityMemberDTO> _MemberList = new List<ActivityMemberDTO>();
        public List<ActivityMemberDTO> MemberList
        {
            get { return _MemberList; }
            set { _MemberList = value; }
        }

    }

    public class ActivityMemberDTO
    {
        public long UID { get; set; }
        public long RID { get; set; }
        public int ACCESS { get; set; }
    }
}

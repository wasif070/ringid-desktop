﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Entity
{
    public class ChannelDTO
    {
        public Guid ChannelID { get; set; }
        public long OwnerID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ProfileImage { get; set; }
        public int ProfileImageWidth { get; set; }
        public int ProfileImageHeight { get; set; }
        public Guid ProfileImageID { get; set; }
        public string CoverImage { get; set; }
        public int CoverImageWidth { get; set; }
        public int CoverImageHeight { get; set; }
        public Guid CoverImageID { get; set; }
        public int CoverImageX { get; set; }
        public int CoverImageY { get; set; }
        public int ChannelStatus { get; set; }
        public int ChannelType { get; set; }
        public long SubscriberCount { get; set; }
        public long SubscriptionTime { get; set; }
        public long ViewCount { get; set; }
        public long CreationTime { get; set; }
        public string Country { get; set; }
        public string ChannelIP { get; set; }
        public int ChannelPort { get; set; }
        public string StreamIP { get; set; }
        public int StreamPort { get; set; }
        public List<ChannelCategoryDTO> CategoryList { get; set; }
        public List<ChannelMediaDTO> MediaList { get; set; }
    }

    public class ChannelCategoryDTO
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
    }

    public class ChannelMediaDTO
    {
        public Guid ChannelID { get; set; }
        public long OwnerID { get; set; }
        public string Title { get; set; }
        public string Artist { get; set; }
        public string Description { get; set; }
        public Guid MediaID { get; set; }
        public int MediaType { get; set; }
        public string MediaUrl { get; set; }
        public string ThumbImageUrl { get; set; }
        public int ThumbImageWidth { get; set; }
        public int ThumbImageHeight { get; set; }
        public long Duration { get; set; }
        public int MediaStatus { get; set; }
        public int ChannelType { get; set; }
        public long StartTime { get; set; }
    }

}

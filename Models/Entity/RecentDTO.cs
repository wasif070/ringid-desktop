﻿using System;
using System.Collections.Generic;

namespace Models.Entity
{
    public class RecentDTO
    {
        public RecentDTO() {}

        public string ContactID { get; set; }
        public long FriendTableID { get; set; }
        public long GroupID { get; set; }
        private string _RoomID = String.Empty;
        public string RoomID
        {
            get { return _RoomID; }
            set { _RoomID = value; }
        }
        public int ContactType { get; set; }
        public int Type { get; set; }
        public long Time { get; set; }
        public string UniqueKey { get; set; }
        public string HistoryDateName { get; set; }
        public CallLogDTO CallLog { get; set; }
        public MessageDTO Message { get; set; }
        public ActivityDTO Activity { get; set; }
        public bool NeedForcelyLoad { get; set; }
        public bool IsMoveToButtom { get; set; }
    }
}

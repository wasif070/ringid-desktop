﻿using System;

namespace Models.Entity
{
    public class SkillDTO
    {
        public Guid Id { get; set; }
        public long UpdateTime { get; set; }
        public string SkillName { get; set; }
        public string Description { get; set; }
    }
}

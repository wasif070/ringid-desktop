﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Models.Entity
{
    public class ChatBgImageDTO
    {
        public int id { get; set; }
        public string name { get; set; }
        public string themeColor { get; set; }
        public bool IsDownloaded { get; set; }
        public long UpdateTime { get; set; }
    }
}

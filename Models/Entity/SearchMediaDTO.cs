﻿
namespace Models.Entity
{
    public class SearchMediaDTO
    {
        public string SearchSuggestion { get; set; }
        public int SearchType { get; set; }
        public long UpdateTime { get; set; }
    }
}

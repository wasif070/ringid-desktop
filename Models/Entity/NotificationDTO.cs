﻿using Models.Constants;
using Models.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Entity
{
    public class NotificationDTO
    {
        public Guid ID { get; set; }
        public long FriendTableID { get; set; }
        public long FriendRingID { get; set; }
        public long UpdateTime { get; set; }
        public int NotificationType { get; set; }
        public int ActivityID { get; set; }
        public int MessageType { get; set; }
        public string FriendName { get; set; }
        public long NumberOfLikeOrComment { get; set; }
        public Guid NewsfeedID { get; set; }
        public Guid ImageID { get; set; }
        public Guid CommentID { get; set; }
        public string ImageUrl { get; set; }
        public bool IsRead { get; set; }
        public List<Guid> PreviousIds { get; set; }

        public static NotificationDTO FindExistingDTO(NotificationDTO dto)
        {
            if (dto == null) return null;
            NotificationDTO item = null;

            switch (dto.MessageType)
            {
                case StatusConstants.MESSAGE_LIKE_STATUS:
                case StatusConstants.MESSAGE_LIKE_COMMENT:
                case StatusConstants.MESSAGE_ADD_STATUS_COMMENT:
                case StatusConstants.MESSAGE_SHARE_STATUS:
                    lock (RingDictionaries.Instance.NOTIFICATION_LISTS)
                    {
                        item = RingDictionaries.Instance.NOTIFICATION_LISTS.Where(x => x.NewsfeedID == dto.NewsfeedID && x.MessageType == dto.MessageType).FirstOrDefault();
                    }
                    break;
                case StatusConstants.MESSAGE_LIKE_IMAGE:
                case StatusConstants.MESSAGE_LIKE_IMAGE_COMMENT:
                case StatusConstants.MESSAGE_IMAGE_COMMENT:
                case StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA:
                case StatusConstants.MESSAGE_LIKE_AUDIO_MEDIA_COMMENT:
                case StatusConstants.MESSAGE_AUDIO_MEDIA_VIEW:
                case StatusConstants.MESSAGE_AUDIO_MEDIA_COMMENT:
                case StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA:
                case StatusConstants.MESSAGE_LIKE_VIDEO_MEDIA_COMMENT:
                case StatusConstants.MESSAGE_VIDEO_MEDIA_COMMENT:
                case StatusConstants.MESSAGE_VIDEO_MEDIA_VIEW:
                    lock (RingDictionaries.Instance.NOTIFICATION_LISTS)
                    {
                        item = RingDictionaries.Instance.NOTIFICATION_LISTS.Where(x => x.ImageID == dto.ImageID && x.MessageType == dto.MessageType).FirstOrDefault();
                    }
                    break;
                case StatusConstants.MESSAGE_ADD_CIRCLE_MEMBER:
                    lock (RingDictionaries.Instance.NOTIFICATION_LISTS)
                    {
                        item = RingDictionaries.Instance.NOTIFICATION_LISTS.Where(x => x.ActivityID == dto.ActivityID && x.MessageType == dto.MessageType).FirstOrDefault();
                    }
                    break;
                case StatusConstants.MESSAGE_NOTIFICATION_LIVE:
                    lock (RingDictionaries.Instance.NOTIFICATION_LISTS)
                    {
                        item = RingDictionaries.Instance.NOTIFICATION_LISTS.Where(x => x.ID == dto.ID && x.MessageType == dto.MessageType).FirstOrDefault();
                    }
                    break;
                default:
                    break;
            }
            return item;
        }
    }
}

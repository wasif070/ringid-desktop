﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Entity
{
    public class StreamDTO
    {
        public Guid StreamID { get; set; }
        public long UserTableID { get; set; }
        public string UserName { get; set; }
        public int UserType { get; set; }
        public string ProfileImage { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public long StartTime { get; set; }
        public long EndTime { get; set; }
        public long ViewCount { get; set; }
        public long LikeCount { get; set; }
        public bool ChatOn { get; set; }
        public bool GiftOn { get; set; }
        public string Title { get; set; }
        public string Country { get; set; }
        public int DeviceCategory { get; set; }
        public double? Distance { get; set; }
        public List<StreamCategoryDTO> CategoryList { get; set; }
        public string ChatServerIP { get; set; }
        public int ChatServerPort { get; set; }
        public string PublisherServerIP { get; set; }
        public int PublisherServerPort { get; set; }
        public string ViewerServerIP { get; set; }
        public int ViewerServerPort { get; set; }

    }

    public class StreamCategoryDTO
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public int FollowingCount { get; set; }
    }

    public class StreamUserDTO
    {
        public long UserTableID { get; set; }
        public string UserName { get; set; }
        public string ProfileImage { get; set; }
        public long FollowingCount { get; set; }
        public long FollowerCount { get; set; }
        public long CoinCount { get; set; }
        public long ViewCount { get; set; }
        public string Country { get; set; }
        public bool IsFollowing { get; set; }
        public long PublisherID { get; set; }
        public long AddedTime { get; set; }
    }

    public class StreamServerDTO
    {
        public Guid StreamID { get; set; }
        public long ServerID { get; set; }
        public string ServerIP { get; set; }
        public int RegisterPort { get; set; }
    }
}

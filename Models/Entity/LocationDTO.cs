﻿
namespace Models.Entity
{
    public class LocationDTO
    {
        public string LocationName { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}

﻿
namespace Models.Entity
{
    public class InfoKeyValue
    {
        public InfoKeyValue(string k, string v)
        {
            Key = k;
            Val = v;
        }
        public InfoKeyValue(string k,int st)
        {
            Key = k;
            Status = st;
        }
        private string key;

        public string Key
        {
            get { return key; }
            set { key = value; }
        }

        private string val;

        public string Val
        {
            get { return val; }
            set { val = value; }
        }

        private int status;

        public int Status
        {
            get { return status; }
            set { status = value; }
        }

    }
}

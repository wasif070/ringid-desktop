﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Models.Entity
{
    public class ChatBgDTO
    {
        public bool sucs { get; set;}
        public List<ChatBgImageDTO> chatBgImageList { get; set; }
    }
}

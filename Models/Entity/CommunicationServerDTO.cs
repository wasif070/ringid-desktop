﻿using Models.Constants;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Entity
{
    public class CommunicationServerDTO
    {
        public CommunicationServerDTO(string json)
        {
            if (!string.IsNullOrEmpty(json))
            {
                JObject jObject = JObject.Parse(json);

                AuthServer = (string)jObject[JsonKeys.AuthServer];
                ImageServer = (string)jObject[JsonKeys.ImageServer];
                ImageServerResource = (string)jObject[JsonKeys.ImageServerResource];
                StickerServer = (string)jObject[JsonKeys.StickerServer];
                StickerServerResource = (string)jObject[JsonKeys.StickerServerResource];
                VodServer = (string)jObject[JsonKeys.VODServer];
                VodServerResource = (string)jObject[JsonKeys.VODServerResource];
                BaseWebURL = (string)jObject[JsonKeys.BaseWebURL];
                IsSuccess = (bool)jObject[JsonKeys.Success];
                WalletPaymentGateway = (string)jObject["paymentGateway"];
                DefaultSettings.RINGID_OFFICIAL_UTID = string.IsNullOrEmpty((string)jObject[JsonKeys.UserTableID]) ? 0 : (long)jObject[JsonKeys.UserTableID];
            }
            else
            {
                AuthServer = null;
                ImageServer = null;
                ImageServerResource = null;
                StickerServer = null;
                StickerServerResource = null;
                VodServer = null;
                VodServerResource = null;
                BaseWebURL = null;
                IsSuccess = false;
                WalletPaymentGateway = null;
                DefaultSettings.RINGID_OFFICIAL_UTID = 0;
            }

        }
        public override string ToString()
        {
            return "AuthServer==>" + AuthServer + " ImageServer==>" + ImageServer + " ImageServerResource==>" + ImageServerResource + " StickerServer==>" + StickerServer + " StickerServerResource==>" + StickerServerResource + " VodServer==>" + VodServer + " VodServerResource==>" + VodServerResource + "BaseWebURL==>" + BaseWebURL;
        }

        private string authServer;
        public string AuthServer
        {
            get { return authServer; }
            set { authServer = value; }
        }

        private string imageServer;
        public string ImageServer
        {
            get { return imageServer; }
            set { imageServer = value; }
        }
        private string imageServerResource;
        public string ImageServerResource
        {
            get { return imageServerResource; }
            set { imageServerResource = value; }
        }
        private string stickerServer;
        public string StickerServer
        {
            get { return stickerServer; }
            set { stickerServer = value; }
        }
        private string stickerServerResource;
        public string StickerServerResource
        {
            get { return stickerServerResource; }
            set { stickerServerResource = value; }
        }
        private string vodServer;
        public string VodServer
        {
            get { return vodServer; }
            set { vodServer = value; }
        }
        private string vodServerResource;
        public string VodServerResource
        {
            get { return vodServerResource; }
            set { vodServerResource = value; }
        }
        private string baseWebURL;
        public string BaseWebURL
        {
            get { return baseWebURL; }
            set { baseWebURL = value; }
        }
        private bool isSuccess;
        public bool IsSuccess
        {
            get { return isSuccess; }
            set { isSuccess = value; }
        }
        
        private string walletPaymentGateway;
        public string WalletPaymentGateway
        {
            get { return walletPaymentGateway; }
            set { walletPaymentGateway = value; }
        }


    }
}


﻿
namespace Models.Entity
{
    public class PageInfoDTO
    {
        public long PageId { get; set; }
        public long UserTableID { get; set; }
        public string PageName { get; set; }
        public string ProfileImage { get; set; }
        public int SubscriberCount { get; set; }
        public bool IsSubscribed { get; set; }
        public string PageSlogan { get; set; }
        //
        public long PageCatId { get; set; }
        public string PageCatName { get; set; }

        public string CoverImage { get; set; }

        public long RingID { get; set; }
        public bool IsProfileHidden { get; set; }
    }
}

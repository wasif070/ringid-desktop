﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Entity
{
    public class CircleDTO
    {
        public string CircleName { get; set; }
        public long CircleId { get; set; }
        public long SuperAdmin { get; set; }
        public long UpdateTime { get; set; }
        public long UserTableID { get; set; }
        public short IntegerStatus { get; set; }
        public int MemberCount { get; set; }
        public int MembershipStatus { get; set; }
        public int AdminCount { get; set; }
        public bool IsProfileHidden { get; set; }
    }
}

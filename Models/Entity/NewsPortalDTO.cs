﻿
namespace Models.Entity
{
    public class NewsPortalDTO
    {
        public string PortalName { get; set; }
        public string ProfileImage { get; set; }
        public int SubscriberCount { get; set; }
        public long UserTableID { get; set; }
        public bool IsSubscribed { get; set; }
        public string PortalSlogan { get; set; }
        //
        public long PortalId { get; set; }
        public long PortalCatId { get; set; }
        public string PortalCatName { get; set; }

        public string CoverImage { get; set; }

        public long RingID { get; set; }
        public bool IsProfileHidden { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Entity
{
    public class ChatFileDTO
    {

        public ChatFileDTO() { }

        public ChatFileDTO(ChatFileDTO chatFileDTO) 
        {
            this.LoadData(chatFileDTO);
        }

        public void LoadData(ChatFileDTO chatFileDTO)
        {
            this.ContactID = chatFileDTO.ContactID;
            this.FriendTableID = chatFileDTO.FriendTableID;
            this.GroupID = chatFileDTO.GroupID;
            this.RoomID = chatFileDTO.RoomID;
            this.SenderTableID = chatFileDTO.SenderTableID;
            this.PacketID = chatFileDTO.PacketID;
            this.Message = chatFileDTO.Message;
            this.MessageType = chatFileDTO.MessageType;
            this.FileID = chatFileDTO.FileID;
            this.Status = chatFileDTO.Status;
            this.UploadUrl = chatFileDTO.UploadUrl;
            this.ErrorMsg = chatFileDTO.ErrorMsg;
            this.File = chatFileDTO.File;
            this.FileSize = chatFileDTO.FileSize;
            this.FileMenifest = chatFileDTO.FileMenifest;
            this.RecentModel = chatFileDTO.RecentModel;
            this.MessageDTO = chatFileDTO.MessageDTO;
            this.IsDownstream = chatFileDTO.IsDownstream;
        }

        public long ContactID { get; set; }
        public long FriendTableID { get; set; }
        public long GroupID { get; set; }
        public string RoomID { get; set; }
        public long SenderTableID { get; set; }
        public string PacketID { get; set; }
        public string Message { get; set; }
        public int MessageType { get; set; }
        public long FileID { get; set; }
        public int Status { get; set; }
        public string UploadUrl { get; set; }
        public string ErrorMsg { get; set; }
        public string File { get; set; }
        public long FileSize { get; set; }
        public string FileMenifest { get; set; }
        public object RecentModel { get; set; }
        public MessageDTO MessageDTO { get; set; }
        public bool IsDownstream { get; set; }
    }
}

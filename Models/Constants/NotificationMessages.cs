﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Constants
{
    public static class NotificationMessages
    {
        private static readonly string are_you_sure = "Are you sure ";
        public static readonly string INVALID_RINGID = "The ringID you entered does not exist. Please check your ringID and try again.";
        public static readonly string INVALID_MOBILE = "The phone number you entered doesn’t appear to belong to an account. Please check your phone number and try again.";
        public static readonly string INVALID_EMAIL = "The email ID you entered doesn’t appear to belong to an account. Please check your email ID and try again.";
        public static readonly string INVALID_FACEBOOK = "The Facebook ID you entered doesn’t appear to belong to an account. Please check your Facebook ID and try again.";
        public static readonly string FB_NOT_SAME = "Your associated Facebook Id and this Id aren't same";
        public static readonly string TWITTER_NOT_SAME = "Your associated Twitter ID and this Id aren't same";
        public static readonly string FB_ADDED_SUCCESS_MESSAGE = "Your Facebook ID has been added successfully!";
        public static readonly string TW_ADDED_SUCCESS_MESSAGE = "Your Twitter ID has been added successfully!";
        public static readonly string INVALID_TWITTER = "The Twitter ID you entered doesn’t appear to belong to an account. Please check your Twitter ID and try again.";
        public static readonly string EMAIL_ALREADY_IN_USE = "This E-mail address is associated with another ringID";
        public static readonly string ALREADY_HAVE_A_RINGID = "You already have a ringID!";
        public static readonly string NETWORK_PROBLEM = "Please check your network status and try again";
        public static readonly string SIGN_OUT_NOTIFICAITON = are_you_sure + "you want to sign out from ringID?";
        public static readonly string SIGN_OUT_DESCRIPTION = "Selecting OK will log you out from ringID. Please note you will not be able to receive notifications until the next time you sign in.";
        public static readonly string CALL_TERMINATE = "Previous call will be terminated?";
        public static readonly string ERROR_IN_UPLOAD_NOTIFICAITON = "Cannot upload image";
        public static readonly string NO_LONGER_FRIEND_NOTIFICAITON = "Sorry! This friend is no longer in your contacts";
        public static readonly string NO_LONGER_IN_GROUP_NOTIFICAITON = "Sorry! No longer in this group";
        public static readonly string CALL_LIMIT_NOTIFICATION = "You have reached your call limit for today";
        public static readonly string NO_FRIEND = "No friend";
        public static readonly string NOT_SLECTE_FRIEND = "Friend has not been selected";
        public static readonly string OFFLINE_NOTIFICATION = "You are currently offline";
        public static readonly string CIRCLE_CREATE_SUCCESS = "Circle created successfully";
        public static readonly string CIRCLE_EDIT_SUCCESS = "Circle edited successfully";
        public static readonly string DELETE_SUCCESS = " Deleted successfully";
        public static readonly string INVALID_CIRCLE_NAME = "Please enter valid circle name";
        public static readonly string NO_CIRCLE_MEMBER = "You need members in this circle";
        public static readonly string CAN_NOT_UPLOAD_IMAGE = "Cannot upload image";
        public static readonly string CAN_NOT_UPLOAD_FILE = "Cannot upload file!";
        public static readonly string CAN_NOT_FOUND_IMAGE = "Image not found.";
        public static readonly string FILE_NOT_FOUND = "File not found.";
        public static readonly string NEW_MEMBER_ADDED = "New members added successfully";
        public static readonly string CIRCLE_EDITED_SUCCESSFULLY = "Circle has been edited successfully";
        public static readonly string CAN_NOT_CREATE_CIRCLE = "Cannot create circle";
        public static readonly string CAN_NOT_CREATE_GROUP = "Cannot create group";
        public static readonly string INCOMING_REQUEST = "Incoming request";
        public static readonly string PENDING_REQUEST = "Pending request";
        public static readonly string SERVER_ERROR = "Communication server not responding";
        public static readonly string REQUEST_TIME_OUT = "Request time out!";

        public const string NO_PERMISSION = "You do not have permission to chat with this user";
        public const string NON_FRIEND_ANONYMOUS_CHAT_ALLOW_WARNING_TITLE = "Chat Access is Disabled!";
        public const string NON_FRIEND_ANONYMOUS_CHAT_ALLOW_WARNING = "You have disabled chat access for non friends. Completing this action will enable chat access for non friends. Do you want to continue?";
        public const string UNBLOCK_WARNING_TITLE = "{0} is blocked!";
        public const string UNBLOCK_WARNING = "You have blocked {0} earlier. Completing this action will unblock {0}, are you sure you want to complete this action?";
        public const string UNBLOCK_WARNING_CHAT_TITLE = "Chat Access is Disabled!";
        public const string UNBLOCK_WARNING_CHAT = "You have disabled {0} from Chat Access. Completing this action will enable Chat Access. Do you want to continue?";
        public const string UNBLOCK_WARNING_CALL_TITLE = "Call Access is Disabled!";
        public const string UNBLOCK_WARNING_CALL = "You have disabled {0} from Call Access. Completing this action will enable Call Access. Do you want to continue?";

        public static readonly string FRIEND_ACCESS_PREVENT_WARNING = are_you_sure + "you want to block {0}? Blocking will prevent the user from {1} access.";
        public static readonly string BLOCKING_WILL_PREVENT_ACCESS = "Blocking will prevent the user from {0} access.";

        public static readonly string FRIEND_BLOCK_WARNING = are_you_sure + "you want to block \"{0}\"?";
        public static readonly string NON_FRIEND_BLOCK_WARNING = are_you_sure + "you want to block \"{0}\"?";
        public static readonly string BLOCKING_CANCELLED_OUTGOING_REQUEST = "Blocking will be cancelled outgoing request.";
        public static readonly string NON_FRIEND_OUTGOING_BLOCK_WARNING = are_you_sure + "you want to block {0}?" + BLOCKING_CANCELLED_OUTGOING_REQUEST;

        public const string NO_GROUP_PERMISSION = "You do not have permission to chat in this group. Because, you are not a member of this group.";
        public const string GROUP_NON_OWNER_LEAVE_PERMISSION = "If you leave from this Group, you will stop receiving messages. Would you like to mute instead?";
        public const string GROUP_OWNER_LEAVE_PERMISSION = "If you leave from this Group, you will stop receiving messages and also you have to make another group owner. Would you like to mute instead?";
        public const string GROUP_NAME_ENTER_MESSAGE = "You can provide name for this group or skip to continue with default group name.";
        public const string GROUP_NAME_ENTER_NEW_GROUP_MESSAGE = "You can create group with name or skip to create with default group name.";
        public const string GROUP_MEMBER_LIMIT_EXITED = "Group can't have more than {0} members.";
        public const string STICKER_DOWNLOAD_FAILED = "Failed to download sticker {0}!";
        public const string CHAT_CONTENT_DOWNLOAD_FAILED = "Failed to download chat {0}!";
        public const string CHAT_REPORT_TITLE = "Send Report";
        public static readonly string CHAT_REPORT_WARNING = are_you_sure + "you want to send report against this message?";
        public const string NOTHING_TO_POST = "Nothing to post !";
        public static readonly string CANCEL_OUTGOING_REQUEST = are_you_sure + "you want to cancel your sent friend request?";
        public static readonly string CANCEL_INCOMING_REQUEST = are_you_sure + "you want to cancel this friend request?";
        public static readonly string REMOVE_FROM_FRIEND = are_you_sure + "you want to remove {0} from your friendlist?";
        public static readonly string REMOVE_FROM_SUGGESTION = are_you_sure + "you want to remove {0} from your suggestion list?";
        public static readonly string FAILDED_TRY_AGAIN = "Failed! Try again!";
        public static readonly string FAILED_TRY_WITH_NETWORK_CHECK = "Please try after some time or check your internet connection!";
        public static readonly string NO_RECOVERY_SUGGESTION_FOUND = "No recovery suggestion found!";
        public static readonly string FAILED_TO_RESET_PASSWORD = "Password reset failed!";
        public static readonly string PASSWORD_RESET_COMPLETED = "Password reset completed!";
        public static readonly string PLEASE_WAIT = "Please wait...";
        public static readonly string UNFOLLOW_CONFIRMATION = "Are you sure you want to Unfollow this \"{0}\"?";
        public static readonly string FOLLOW_CONFIRMATION = "Are you sure you want to follow this \"{0}\"?";
        public static readonly string SURE_WANT_TO = "Are you sure you want to {0}?";
        /*SIGN UP REQUEST*/
        public static readonly string ATLEAST_4_CHARACTER_NEEDED = " Name must be at least 4 character long";
        public static readonly string START_WITH_LETTER = "Name must start with a letter";
        public static readonly string CAN_CONTAINS_ONLY = " Password can only contain alphabets and underscore ";
        public static readonly string FIRST_NAMME = "Please enter first name";
        public static readonly string LAST_NAME = "Please enter last name";
        public static readonly string MUST_HAVE_6_CHAR = " must be of at least 6 character";
        public static readonly string COMMA_NOT_ALLOWED = "Comma(,) is not allowed ";
        public static readonly string CAN_NOT_CANTAIN = " can't contain ";
        public static readonly string PASSWORD_MISSMATCH = "Password does not match";
        public static readonly string VALID_PHONE = "Please enter your phone number";
        public static readonly string CHOOSE_CALLING_CODE = "Please specify calling code";
        public static readonly string CHOOSE_COUNTRY = "Please select a country";
        public static readonly string CHOOSE_GENDER = "Please select your gender";
        public static readonly string DOWNLOAD_NEW_VERSION = "Please download the new version of ringID";
        public static readonly string MAX_STATUS_LENGTH_OVER = "Number of characters should be less than 400.";
        //public static readonly string MAX_FILE_SIZE = "File size must be less than {0}MB";
        public static readonly string MAX_FILE_TRANSFER_SIZE = "File size must be less than {0}GB";
        public static readonly string ANOTHER_RECORDING_RUNNING = "Another recording is already running. Please, complete or cancel previous one to start new recording";
        public static readonly string WEBCAM_NOT_FOUND = "No webcam found in your system!";
        public static readonly string WEBCAM_ALREADY_USING = "Another program might be using your webcam!";
        public static readonly string LOGGED_IN_FROM_ANOTHER_DEVICE_HEADER = " Signed Out!";
        public static readonly string LOGGED_IN_FROM_ANOTHER_DEVICE = "You are signed in from another device.";
        public static readonly string SESSION_INVALID_HEADER = " Signed Out!";
        public static readonly string SESSION_INVALID_BODY = "Your session has been expired, please login again.";
        //"Failed! Please enter your details correctly!"
        public static readonly string PLEASE_ENTER_DETAILS_CORRECTLY = "Failed! Please enter your details correctly!";

        /**/
        /*NOTIFICATION MESSAGE*/
        public static readonly string LIKES_STRING = "like";
        public static readonly string MSG_UPDATE_PROFILE_IMAGE = "changed profile picture";
        public static readonly string MSG_UPDATE_COVER_IMAGE = "changed cover photo";
        public static readonly string MSG_ADD_FRIEND = "wants to be your friend";
        public static readonly string MSG_ACCEPT_FRIEND = " accepted your friend request";
        public static readonly string MSG_ADD_CIRCLE_MEMBER = "added you in {0}";
        public static readonly string MSG_ADD_STATUS_COMMENT = "commented on your post";
        public static readonly string MSG_LIKE_STATUS = LIKES_STRING + "d your post";
        public static readonly string MSG_LIKE_COMMENT = LIKES_STRING + "d your comment";
        public static readonly string MSG_ADD_COMMENT_ON_COMMENT = "commented on a post where you have commented";
        public static readonly string MSG_SHARE_STATUS = "shared your post";
        public static readonly string MSG_LIKE_IMAGE = LIKES_STRING + "d your photo";
        public static readonly string MSG_IMAGE_COMMENT = "commented on your photo";
        public static readonly string MSG_LIKE_IMAGE_COMMENT = LIKES_STRING + "d your comment in a photo";
        public static readonly string MSG_ACCEPT_FRIEND_ACCESS = "accepted your request for complete profile";
        public static readonly string MSG_UPGRADE_FRIEND_ACCESS = "upgraded friend access";
        public static readonly string MSG_DOWNGRADE_FRIEND_ACCESS = "downgraded friend access";
        // public static readonly string MSG_NOW_FRIEND = "added you as a friend";
        public static readonly string MESSAGE_LIKE_AUDIO_MEDIA = "liked your music";
        public static readonly string MESSAGE_AUDIO_MEDIA_COMMENT = "commented on your music";
        public static readonly string MESSAGE_LIKE_AUDIO_MEDIA_COMMENT = "liked your comment in a music";
        public static readonly string MESSAGE_AUDIO_MEDIA_VIEW = "viewed your music";
        public static readonly string MESSAGE_LIKE_VIDEO_MEDIA = "liked your video";
        public static readonly string MESSAGE_VIDEO_MEDIA_COMMENT = "commented on your video";
        public static readonly string MESSAGE_LIKE_VIDEO_MEDIA_COMMENT = "liked your comment in a video";
        public static readonly string MESSAGE_VIDEO_MEDIA_VIEW = "viewed your video";
        public static readonly string MESSAGE_YOU_HAVE_BEEN_TAGGED = "tagged you";
        public static readonly string CONTENT_DOES_NOT_EXIST = "This content does not exist";
        public static readonly string ALREADY_SHARED_MESSAGE = "You have already shared this.";


        public const string NOTIFICATION_SHOW_MORE = "Show More";
        public static readonly string NOTIFICATION_LOADING = "Loading...";
        public static readonly string NOTIFICATION_NO_MORE_NOTIFICATION = "";
        /**/
        public static readonly string VIDEO_CALL_NOTIFICATION = "Video Calling is currently not supported on Desktop. This feature will be available soon.";
        public static readonly string ROUTE_CALL_NOTIFICATION = "Making free calls to landlines and mobile numbers is currently not supported on Desktop. This feature will be available soon.";
        public static readonly string SMS_NOTIFICATION = "Sending free SMS is currently not supported on Desktop. This feature will be available soon.";
        public static readonly string RING_MAIL_NOTIFICATION = "Secure Ring Mail is currently not supported on Desktop. This feature will be available soon.";
        public static readonly string FRIEND_ACCESS_NOTIFICAITON = "You have a request of changing friend information access permission to '{0}'. " + are_you_sure + "you want to accept?";
        //   public static readonly string INTERNET_UNAVAILABLE = "Internet unavailable";
        public static readonly string INTERNET_UNAVAILABLE = "No internet connection!";
        public static readonly string INVALID_SESSION = "Invalid Session";
        public static readonly string CAN_NOT_PROCESS = "Can not process";
        public static readonly string ACCESS_DENIED = "Permission Denied";
        public static readonly string TEXT_OFFLINE = "Offline";
        public static readonly string TEXT_FRIEND_AWAY = "Friend is Away!";
        public static readonly string TEXT_FRIEND_OFFLINE = "User Offline!";
        public static readonly string TEXT_DO_NOT_DISTURB = "Do Not Disturb!";
        public static readonly string TEXT_NO_BINDING_PORT = "No voice binding port";
        public static readonly string TEXT_CALL_FAILED = "Can not call right now";
        public static readonly string TEXT_CALLING = "Calling...";
        public static readonly string TEXT_INCOMING_CALL = "Incoming call";
        public static readonly string TEXT_INCOMING_VIDEO_CALL = "Incoming video call";
        public static readonly string TEXT_RINGING = "Ringing...";
        public static readonly string TEXT_AUTHENTICATING = "Authenticating...";
        public static readonly string TEXT_SENDING_PUSH = "Sending push";
        public static readonly string TEXT_CONNECTING = "Connecting...";
        public static readonly string TEXT_DIVERTED_CALL = "Diverted call";
        public static readonly string TEXT_USER_BUSY = "User Busy";
        public static readonly string TEXT_USER_IN_CALL = "User in Call";
        public static readonly string TEXT_NO_ANSWER = "No Answer";
        public static readonly string TEXT_NO_Response = "No Response";
        public static readonly string TEXT_USER_UNREACHABLE = "User currently unreachable";
        public static readonly string TEXT_CANCELED = "Cancelled";
        public static readonly string TEXT_CALL_END = "Call Ended";
        public static readonly string TEXT_CALL_DROPPED = "Call Droped";
        public static readonly string TEXT_CALL_MUTED = "Call Muted";
        public static readonly string TEXT_CALL_FAILED_REASON = "Call Failed";
        public static readonly string TEXT_CALL_ESTABLISHED = "Call Established";
        public static readonly string TEXT_AUDIO_PROBLEM = "Audio device problem";
        public static readonly string TEXT_MICROPHONE_PROBLEM = "Microphone problem";
        public static readonly string TEXT_AUDIO_PROBLEM_FRIEND = "Contact have audio device problem";
        public static readonly string TEXT_USER_LOGGED_OUT = "User logged out";
        /*Settings*/
        public static readonly string HISTORY_SETTINGS_HEADER_TEXT = "Deleting history will result in the removal of all call and chat logs within the selected time period.";
        public static readonly string ADD_DIVERT_NUMBER_TEXT = "Activating the call divert option will divert all your calls to the assigned number.";
        public static readonly string REMOVE_DIVERT_TEXT = "Deactivating the call divert option will allow incoming calls to your number. ";
        public static readonly string BLOCKED_CONTACTS_TEXT = "Stop someone contacting you.";
        /*Settings*/
        public static readonly string TEXT_PROFILE_IMAGE = "Profile Pictures";
        public static readonly string TEXT_COVER_IMAGE = "Cover Photos";
        public static readonly string TEXT_FEED_IMAGE = "Feed Photos";
        public static readonly string TEXT_ALBUM_IMAGE = "Album Photos";
        public static readonly string TEXT_PROFILE_PHOTO = "Update Profile Picture";
        public static readonly string TEXT_COVER_PHOTO = "Update Cover Photo";
        public static readonly string TEXT_MY_ALBUMS = "My Albums";

        /*FAIL MESSAGES*/
        public static readonly string MSG_ADDFRIEND_FAIL = "Failed to add friend! ";
        public static readonly string MSG_UNFRIEND_FAIL = "Failed to unfriend! ";
        public static readonly string MSG_REMOVE_FROM_SUGGESTION = "Failed to remove from suggestion";
        public static readonly string MSG_ACCEPT_REQUEST_FAIL = "Failed to Accept Request! ";
        public static readonly string CALL_ON_HOLD = "Call on Hold";
        public static readonly string VIDEO_UPLOAD_FAILED = "Failed to upload video!";

        /* CIRCLE */
        //public static readonly string MSG_CREATE_CIRCLE = "Your circle  {0} created successfully.";
        public static readonly string MSG_CREATE_CIRCLE = "Your circle {0} created successfully.";
        public static readonly string REMOVE_FROM_CIRCLE = are_you_sure + "you want to remove {0}?";
        public static readonly string MAKE_CIRCLE_ADMIN = are_you_sure + "you want to make {0} an Admin?";
        public static readonly string REMOVE_CIRCLE_ADMIN = are_you_sure + "you want to remove {0} from Admins?";
        public static readonly string CIRCLE_FRIEND_ADD_ONLY = "Only Friends can be added!";
        public static readonly string CIRCLE_MEMBER_ALREADY_ADDED = "Member already Added!";
        public static readonly string CIRCLE_NO_MEMBER_ADDED = "No member added!";
        public static readonly string CIRCLE_MEMBER_ADDED_SUCCESS = "Successfully added member!";
        public static readonly string CIRCLE_MEMBER_ADD_NO_ACCESS = "You have no authority to add group member!";
        public static readonly string CIRCLE_FRIEND_ALREADY_ADDED = "Friend is already group member!";
        public static readonly string CIRCLE_CREATE_NO_NAME = "Please Enter a name for the Circle!";
        public static readonly string CIRCLE_CREATE_MAX_NAME_LENGTH = "Please Enter a Circle Name within maximum 60 characters!";
        public static readonly string CIRCLE_CREATE_REQUIRE_MEMBER_ADD = "Please add members to create a group!";
        public static readonly string CIRCLE_DELETE_CONFIRMATION_MESSAGE = are_you_sure + "you want to delete {0}?";
        public static readonly string CIRCLE_LEAVE_CONFIRMATION_MESSAGE = are_you_sure + "you want to leave {0}?";
        public static readonly string CIRCLE_NOT_ACCESSIBLE = "Circle Not accessible!";
        public static readonly string CIRCLE_MEMBER_FETCHING_FAIL = "Unable to load circle members!";

        public const string MNU_ALL_FRIENDS = "All Friends";
        public const string MNU_TOP_FRIENDS = "Top Friends";
        public const string MNU_FAV_FRIENDS = "Favorite Friends";
        public const string MNU_BLOCK = "Blocked Access";
        public const string MNU_RECENTLY_ADDED = "Recently Added";
        public const string MNU_OFFICIAL = "ringID Official";

        public static readonly string NO_VIDEO_ON_HOLD = "Call on hold.";
        public static readonly string ALREADY_IN_CALL = "You are already in a call with someone.";
        public static readonly string CAN_NOT_CALL_RIGHT_NOW = "Can not make a call right now!";
        //web cam
        public static readonly string NO_WEBCAM = "No camera detected on your device.";
        public static readonly string NO_WEBCAM_TITLE = "No webcam!";
        //MY PROFILE ABOUT
        /*  Basic Info Error */

        public static readonly string ABOUT_NO_CHANGE = "No change!";
        public static readonly string ABOUT_NOT_AVAILABLE = "N/A";
        public static readonly string ABOUT_START_DAY_MUST_EARLY = "Start date must be earlier!";
        public static readonly string ABOUT_START_DATE_REQUIRED = "Must provide from time!";
        public static readonly string ABOUT_END_DATE_REQUIRED = "Must provide to time!";
        public static readonly string ABOUT_END_DATE_MUST_GREATER_THAN_START_DATE = "To time must be later than from time!";
        public static readonly string ABOUT_START_DATE_END_DATE_REQUIRED_OR_NONE = "Must provide from time & to time both or none!";

        public static readonly string BIE_BIRTHDAY_AGE_LIMIT_EXCEEDED = "Birthday must be set at least 13 years earlier from Current Year!";
        public static readonly string BIE_BIRTHDAY_SET_FUTURE = "Birthday can't be in future!";
        public static readonly string BIE_HOME_CITY_BLANK = "Home City can't be set to blank!";
        public static readonly string BIE_CURRENT_CITY_BLANK = "Current City can't be set to blank!";
        public static readonly string BIE_ABOUT_ME_BLANK = "About Me can't be set to blank!";
        public static readonly string BIE_FULL_NAME_BLANK = "Name can't be set to blank!";
        public static readonly string BIE_FULL_NAME_EXCEEDS_LIMIT = "Please Enter a Name within maximum {0} characters!";
        public static string BIE_FULL_NAME_LESS_THAN_MINIMUM_LIMIT = "Please Enter a Name of minimum 2 characters!";
        public static string BIE_FULL_NAME_DOES_NOT_START_WITH_ALPHANUMERIC = "Name must start with alphanumeric character";
        public static readonly string BIE_MARRIAGE_DATE_LIMIT_EXCEEDED = "Marriage Day can be set in future within maximum 20 years from Current Year!";
        public static readonly string BIE_MARRIAGE_DATE_SET_BEFORE_BIRTHDAY = "Marriage Day can't be before birthday!";

        public static readonly string BI_SMS_SENDING_LIMIT_EXCEEDED = "Your sms sending request has been over.";
        public static readonly string BIR_CHANGE_VERIFIED_MAIL_ASK = "Change your mail? Your current email is already verified.";
        public static readonly string BIR_CHANGE_VERIFIED_PHONE_NUMBER_ASK = "Change your Mobile Number? Your current Number is already verified.";
        public static readonly string BIR_INVALID_EMAIL_ADDRESS = "Please enter a valid email address!";
        public static readonly string BIR_INVALID_PHONE_NUMBER = "Please enter a valid mobile number!";

        public static readonly string BIR_VERIFICATION_CODE_REQUIRED = "Please enter verification code!";
        //public static readonly string BIP_COMAPNY_NAME_OR_POSITION_REQUIRED = "Must add a company name or a position";
        public static readonly string BIP_ASK_DELETE_PROFESSION = "Delete this work?";

        public static readonly string PC_COMAPNY_NAME_REQUIRED = "Must add a company name!";
        public static readonly string PC_POSITION_REQUIRED = "Must add a position!";
        public static readonly string PC_ASK_DELETE_PROFESSION = "Delete this work?";


        public static readonly string ED_SCHOOL_NAME_REQUIRED = "Must add a institute name!";
        public static string ED_DEGREE_REQUIRED = "Must add a degree name";
        public static string ED_CENCENTRATION_REQUIRED = "Must add field of study";
        public static string ED_ASK_DELETE_EDUCATION = "Delete this Education?";

        /*Skill*/
        public static readonly string SKILL_CREATE_NO_NAME = "Must provide a Skill Name";
        public static readonly string FAILED_CONFIRMATION_MESSAGE_TITLE = "Failed!";

        public static readonly string ADD_TO_MUSIC_ALBUM = "Add to Music Album";
        public static readonly string ADD_TO_VIDEO_ALBUM = "Add to Video Album";

        public static readonly string DOWNLOAD_TO_MUSIC_ALBUM = "Download to Music Album";
        public static readonly string DOWNLOAD_TO_VIDEO_ALBUM = "Download to Video Album";
        public static readonly string ADDING_MEDIA_CONFIRMATION = "Media has been added succesfully !!";
        public static readonly string ADDING_MEDIA_FAILED = "This media already exists in your album";
        public static readonly string MEDIA_MUSIC = "Playback";//Song
        public static readonly string SINGLE_MEDIA_MUSIC = "music ?";
        public static readonly string MEDIA_MUSICS = "Playbacks";//Songs
        public static readonly string MEDIA_VIDEO = "Playback";//Video
        public static readonly string SINGLE_MEDIA_VIDEO = "video ?";
        public static readonly string MEDIA_VIDEOS = "Playbacks";//Videos

        public static readonly string DELETE_CONFIRMATIONS = "Are you sure you want to delete {0}?";
        public static readonly string LEAVE_CONFIRMATIONS = "Are you sure you want to leave from {0}?";
        public static readonly string HIDE_CONFIRMATIONS = "Are you sure you want to hide {0}?";
        public static readonly string TITLE_CONFIRMATIONS = "{0} confirmation!";

        public static readonly string HEADER_DELETE_NOTIFICATION = "Delete Notification";
        public static readonly string HEADER_DELETE_NOTIFICATIONS = "Delete Notifications";
        public static readonly string HEADER_CIRCLE_LEAVE = "Leave Circle";
        public static readonly string HEADER_CIRCLE_DELETE = "Delete Circle";
        public static readonly string HEADER_CIRCLE_REMOVE_MEMBER = "Remove Member";
        public static readonly string HEADER_CIRCLE_REMOVE_ADMIN = "Remove Admin";
        public static readonly string HEADER_CIRCLE_MAKE_ADMIN = "Make Admin";
        public static readonly string PHONE_NUMBER_NOTIFICAITON = "You already have a ringID account with this phone number";

        public static readonly string NO_NEED_UPGRADE_MESSAGE = "You already have the latest version of ringID installed.";
        public static readonly string NO_UPDATER = "Please uninstall this ringID version and install the new version from the ringID website.";
        public static readonly string CONTACT_NOT_FOUND = "Contact did not found";

        public static readonly string UPDATE_VERSION_TEXT = "There's a new version of ringID available. Update now to get the latest features and improvements.";
        public static readonly string CURRENT_VERSION_TEXT_WITH_VERSION_NUMBER = "Your Current Version is {0}";
        public static readonly string UPDATE_VERSION_TEXT_WITH_VERSION_NUMBER = "There's a new version of ringID({0}) available. Update now to get the latest features and improvements.";

        public static readonly string MEDIA_TYPE;
        public static readonly string REMOVE_FROM_DOWNLOAD_MEDIA_TEXT = are_you_sure + "you want to remove this downloaded ";

        //wallet
        public static readonly string COIN_TRANSFER_SUCCESS = "Coin transferred successfully!";
        public static readonly string WALLET_NETWORK_ERROR = "An error occured. Please try again!";
        public static readonly string TRANSACTION_ERROR = "Unable to complete transaction!";
        public static readonly string TRANSFER_ERROR = "Unable to transfer!";
        public static readonly string TRANSFER_REFERRAL_ERROR = "Unable to transfer! Please select a valid user!";
        public static readonly string TRANSFER_AMOUNT_ERROR = "Unable to transfer! Invalid amount!";
        public static readonly string REFERRAL_ERROR = "Please select a valid user!";
        public static readonly string AMOUNT_ERROR = "Invalid amount!";
        public static readonly string WALLET_PIN_NUMBER_BLANK = "Please provide pin number!";
        public static readonly string WALLET_PIN_NUMBER_LENGTH_ERROR = "Pin number must be of 4 digits!";
        public static readonly string WALLET_PIN_NUMBER_NUMERIC_ERROR = "Pin number must be numeric!";
        public static readonly string WALLET_INCORRECT_USER_PASSWORD = "Incorrect Password!";
        public static readonly string WALLET_CONFIRMATION_PIN_NOT_PROVIDED = "Please provide confirmation pin number!";
        public static readonly string WALLET_PIN_NUMBERS_NOT_SAME = "Both pin number must be same!";
        public static readonly string WALLET_INCORRECT_PIN_NUMBER = "Incorrect pin number!";
        public static readonly string WALLET_REFERRAL_SENDING_SUCCESS = "Referral sent successfully!";
        public static readonly string WALLET_REFERRAL_SENDING_ERROR = "Unable to send request!";
        public static readonly string WALLET_REFERRAL_SENDING_FAIL = "Unable to send request! Please try again!";
        public static readonly string WALLET_REFERRAL_ACCEPT_SUCCESS = "You have accepted {0} ({1})'s request!";
        public static readonly string WALLET_REFERRAL_REJECT_SUCCESS = "You have rejected {0} ({1})'s request!";
        public static readonly string WALLET_REFERRAL_PROCESSING_FAIL = "Unable to process! Please try again!";

        //Stream
        public static string STREAM_TERMS_CONDITIONS_1 = "ringID Live respects the sentiment of users and encourages non discriminative user interaction. Behaviors like use of drugs/alcohol, smoking, pornography, nudity, vulgarism are strongly prohibited.";
        public static string STREAM_TERMS_CONDITIONS_2 = "Moderators will review around the clock and violation of the terms of use may lead to account being banned or any other penalty";

        //
        public static string SUCCESS_CONFIRMATION_MESSAGE_TITLE = "Success!";
        public static string MEDIA_ALREADY_EXIST_IN_ALBUM = "Media Already Exists in the Album!";
        public static string MEDIA_ALREADY_EXIST_IN_ALBUM_TITLE = "Already Exists!";
        public static string ADD_TO_MEDIA_ALBUM_FAILED_MESSAGE = "Failed to Add Media to the Album!";
        public static string PUBLIC_MEDIA_ADD_MESSAGE = "Only Public Media can be Added!";
        public static string PAUSED_MEDIA_FILE_NOT_FOUND = "Paused Media file not found";
        public static string FAILED_MEDIA_SHARE_FACEBOOK_MESSAGE = "Media link can not be shared to facebook right now!";
        public static string FAILED_MEDIA_SHARE_FACEBOOK_MESSAGE_TITLE = "Facebook share";
        public static string COPY_MEDIA_LINK_SUCCESS_MESSAGE = "Media link copied to clipboard!";
        public static string COPY_MEDIA_LINK_FAILED_MESSAGE = "Media link can not be copied right now!";
        public static string COPY_MEDIA_LINK_MESSAGE_TITLE = "Copy url";
        public static string DELETE_MEDIA_SUCCESS_MESSAGE = "Successfully Deleted this Media!";
        public static string DELETE_MEDIA_MESSAGE_TITLE = "Delete";
        public static string MEDIA_ADDED_SUCCESS_MESSAGE = "Media added Successfully!";
        public static string MEDIA_ADDED_MESSAGE_TITLE = "Add media";
        public static string MEDIA_ADDED_FAILED_MESSAGE = "Failed to add media";
        public static string NETWORK_MESSAGE_TITLE = "Network";
        public static string NO_MEDIA_FOUND_TEXT = "No Media found for";
        public static string NO_RECENT_FOUND_TEXT = "No Recent Media Found";
        public static string REPORT_MEDIA_MESSAGE_TITLE = "Report media";
        public static string REPORT_FEED_MESSAGE_TITLE = "Report feed";
        public static string REPORT_SUCCESSFUL_MESSAGE_TITLE = "Report successful";
        public static string REPORT_SUCCESSFUL_MESSAGE = "Thank you for submitting report. Your report will be reviewed soon.";
        public static string REPORT_FAILED_MESSAGE_TITILE = "Report failed";
        public static string REPORT_FAILED_MESSAGE = "Can not report right now, please try again later!";
        public static string REPORT_TYPE_SELECT_ALERT_MESSAGE = "Must select a report type!";
        public static string SINGLE_FEED_HIDE_MESSAGE_TITLE = "Hide";
        public static string SINGLE_FEED_HIDE_MESSAGE = "You won't see this feed in News Feed.";
        public static string SINGLE_FEED_HIDE_USER_MESSAGE = "You won't see feeds from {0} anymore.";
        public static string SINGLE_FEED_UNSAVE_MESSAGE_TITLE = "Unsave";
        public static string SINGLE_FEED_UNSAVE_MESSAGE = "Unsave this Feed";
        public static string ADD_FEED_MESSAGE_TITLE = "Add feed";
        public static string FAILED_ADD_FEED_MESSAGE = "Failed to Add Feed!";
        public static string UNSUBSCRIBED_SUCCESSFUL_MESSAGE = "Successfully unsubscribed from this Portal!";
        public static string SUBSCRIBED_TO_CATEGORIES_SUCCESSFUL_MESSAGE = "Successfully subscribed to the chosen categories of this Portal!";
        public static string SUBSCRIBED_SUCCESSFUL_MESSAGE = "Successfully subscribed to this Portal!";
        public static string UNSUBSCRIBED_PAGE_SUCCESSFUL_MESSAGE = "Successfully unsubscribed from this Page!";
        public static string SUBSCRIBED_PAGE_TO_CATEGORIES_SUCCESSFUL_MESSAGE = "Successfully subscribed to the chosen categories of this Page!";
        public static string SUBSCRIBED_PAGE_SUCCESSFUL_MESSAGE = "Successfully subscribed to this Page!";
        public static string UNSUBSCRIBED_MUSIC_PAGE_SUCCESSFUL_MESSAGE = "Successfully unsubscribed from this Music Page!";
        public static string SUBSCRIBED_MUSIC_PAGE_TO_CATEGORIES_SUCCESSFUL_MESSAGE = "Successfully subscribed to the chosen categories of this Media Page!";
        public static string SUBSCRIBED_MUSIC_PAGE_SUCCESSFUL_MESSAGE = "Successfully subscribed to this Media Page!";
        public static string MESSAGE_YOUTUBEVIEWER_BROWSER_DEPRECATED = "Your Internet Explorer version is deprecated! Please upgrade or use another browser and set it as default.";
        public static string MESSAGE_YOUTUBEVIEWER_FILE_PLAYING_FAILED = "The video you are trying to view maybe missing or corrupted!";
    }
}

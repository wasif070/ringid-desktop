<<<<<<< HEAD
﻿using System;

namespace Models.Constants
{
    public class AppConstants
    {
        /*request type
        1st byte + broken type+ packet.........
        */
        public const int SINGLE_PACKET = 0;
        public const int BROKEN_PACKET = 1;

        public const int REQUEST_TYPE_KEEP_ALIVE = 1;
        public const int REQUEST_TYPE_CONFIRMATION = 2;
        public const int REQUEST_TYPE_AUTH = 3;
        public const int REQUEST_TYPE_REQUEST = 5;
        public const int REQUEST_TYPE_UPDATE = 4;
        public const int REQUEST_TYPE_CALL = 6;
        public const int REQUEST_TYPE_CHAT = 7;

        /**/
        public const int CLIENT_DATA_SIZE = 460;
        public const int HEADER_SIZE = 12;
        public const int DATA_SIZE = 500;
        public const int DATA_OFFSET = 1;

        public const int CONS_FULL_NAME = 1;
        //public const int CONS_LAST_NAME = 2;
        public const int CONS_GENDER = 3;
        public const int CONS_COUNTRY = 4;
        public const int CONS_MOBILE_PHONE = 5;
        public const int CONS_EMAIL = 6;
        public const int ACTION_ADD_NUMBER_OR_EMAIL = 214;
        public const int CONS_PRESENCE = 7;
        public const int CONS_WHAT_IS_IN_UR_MIND = 8;
        public const int CONS_FRIENDSHIP_STATUS = 9;
        public const int CONS_CALLING_CODE = 10;
        public const int CONS_PREV_PASS = 11;
        public const int CONS_NEW_PASS = 12;
        public const int CONS_PROFILE_IMAGE = 13;
        public const int CONS_BIRTH_DAY = 14;
        public const int CONS_COVER_IMAGE = 15;
        public const int CONS_PRIVACY = 16;
        /**/
        public const int CONS_CURRENT_CITY = 18;
        public const int CONS_HOME_CITY = 19;
        public const int CONS_MARRIAGE_DAY = 20;
        public const int CONS_ABOUT_ME = 21;
        //-------------------------------------- changes made by reefat[13.03.2014] (start) --------------------------------------
        //--------------- revised call type
        public const int TYPE_CONFIRMATION = 200;
        /*
         * Client<<------->>Server[<100]
         */
        public const int NEWS_FEED_TYPE_IMAGE = 1;
        public const int NEWS_FEED_TYPE_STATUS = 2;
        public const int NEWS_FEED_TYPE_MULTIPLE_IMAGE = 3;
        public const int NEWS_FEED_TYPE_ALBUM = 4;
        public const int TYPE_INVALID_LOGIN_SESSION = 19;
        public const int TYPE_SIGN_IN = 20; // "signin";    
        public const int TYPE_MY_PROFILE_DETAILS = 21; // "my profile details";
        public const int TYPE_SIGN_OUT = 22; //"signout";    
        public const int TYPE_CONTACT_LIST = 23; // "contactList";
        public const int TYPE_USER_ID_AVAILABLE = 24; // "userIdentityAvailable";
        public const int TYPE_USER_PROFILE = 25; // "user_profile";    
        public const int TYPE_KEEP_ALIVE = 26; // "keepAlive";
        public const int TYPE_MY_SETTINGS_VALUES = 28; //mysettings
        public const int TYPE_CONTACT_UTIDS = 29;
        public const int TYPE_SUGGESTION_IDS = 31;
        public const int TYPE_USERS_DETAILS = 32;
        public const int TYPE_REMOVE_SUGGESTION = 33;
        public const int TYPE_CONTACT_SEARCH = 34; // "contactSearch";
        public const int TYPE_CALL_LOG = 35; // "callLog";
        public const int TYPE_PROFILE_IMAGE_TEXT = 37; // "profileImage";
        public const int TYPE_ADD_ADDRESSBOOK_FRIEND = 38; // "addAddressBookFriend";
        public const int TYPE_PASSWORD_RECOVERY = 39; // "passwordRecovery";
        public const int TYPE_PASSWORD_RESET = 40; // "passwordReset";
        public const int TYPE_EMOTICONS_ALL = 41; // "emoticons_all";
        public const int TYPE_EMOTICONS = 42; // "emoticons";
        public const int TYPE_REMOVE_PROFILE_IMAGE = 43; // "removeProfileImage";
        public const int TYPE_BALANCE = 44; // "balance";
        public const int TYPE_CREATE_CIRCLE = 50;//"create_group";
        public const int TYPE_NEW_CIRCLE = 51;//"new_group";
        public const int TYPE_CIRCLE_DETAILS = 52;
        public const int TYPE_LEAVE_CIRCLE = 53;// "leave_group";
        public const int TYPE_SEND_SMS = 61;//”send_sms”
        public const int TYPE_UPDATE_PROFILE_IMAGE = 63;
        public const int TYPE_CIRCLE_LIST = 70;
        public const int TYPE_ADDRESS_BOOK = 71;
        public const int TYPE_PRIVACY_SETTINGS = 74;   //"privacy_settings"
        public const int TYPE_MULTIPLE_SESSION_WARNING = 75;
        public const int TYPE_SESSION_VALIDATION = 76;
        public const int TYPE_SERVER_REPORT = 77;
        public const int TYPE_PRESENCE = 78;
        public const int TYPE_UNWANTED_LOGIN = 79;

        /*
         * 81,82 and 83 has bben defined for SMS action types
         * Please do not use them here
         * public const int TYPE_FRIEND_SMS = 81;
         * public const int TYPE_CIRCLE_SMS = 82;
         * public const int TYPE_KNOW_SMS_STATUS = 83;
         * 
         */
        public const int TYPE_UPDATE_CONTACT_ACCESS = 82;
        public const int TYPE_GET_IM_OFFLINE_SERVER = 83;
        public const int TYPE_COMMENTS_FOR_STATUS = 84;
        public const int TYPE_UPLOAD_ALBUM_IMAGE = 85;
        public const int TYPE_STICKERS = 86;
        public const int TYPE_MEDIA_FEED = 87;
        public const int TYPE_NEWS_FEED = 88;
        public const int TYPE_COMMENTS_FOR_IMAGE = 89;
        public const int TYPE_UPDATE_UPLOAD_ALBUM_IMAGE = 90;
        public const int TYPE_STICKERS_BACKGROUND_IMAGES = 91;
        public const int TYPE_LIKES_FOR_STATUS = 92;
        public const int TYPE_LIKES_FOR_IMAGE = 93;
        public const int TYPE_MY_BOOK = 94;
        public const int TYPE_USER_DETAILS = 95;
        public const int TYPE_MY_ALBUMS = 96;
        public const int TYPE_MY_ALBUM_IMAGES = 97;
        public const int TYPE_FRIENDS_PRESENCE_LIST = 98;
        public const int TYPE_CIRCLE_MEMBERS_LIST = 99;
        public const int TYPE_SEARCH_CIRCLE_MEMBER = 101;
        public const int TYPE_SEND_MOBILE_PASSCODE = 100;
        public const int TYPE_UPDATE_COVER_IMAGE = 103;
        public const int TYPE_REMOVE_COVER_IMAGE = 104;
        public const int TYPE_UPDATE_FRIEND_GROUP = 105;
        public const int TYPE_YOU_MAY_KNOW_LIST = 106;
        //public const int TYPE_FRIEND_ALL_DETAILS = 107;
        public const int TYPE_FRIEND_CONTACT_LIST = 107;
        public const int TYPE_FRIEND_ALBUMS = 108;
        public const int TYPE_FRIEND_ALBUM_IMAGES = 109;
        public const int TYPE_FRIEND_NEWSFEED = 110;
        public const int TYPE_MY_NOTIFICATIONS = 111;
        public const int TYPE_DELETE_MY_NOTIFICATIONS = 112;
        public const int TYPE_SINGLE_NOTIFICATION = 113;
        public const int TYPE_SINGLE_STATUS_NOTIFICATION = 114;
        /**/
        public const int TYPE_SINGLE_FEED_SHARE_LIST = 115;
        public const int TYPE_LIST_LIKES_OF_COMMENT = 116;
        public const int TYPE_MULTIPLE_IMAGE_POST = 117;
        public const int TYPE_MUTUAL_FRIENDS = 118;
        public const int TYPE_UPDATE_TAG_NAME = 119;
        /*
         * Client<<----->>Server<<----->>Client [>100 and <200]     
         */
        public const int TYPE_IMAGE_DETAILS = 121;
        //public const int TYPE_ADD_COMMENT_ON_COMMENT = 122;
        public const int TYPE_LIKE_COMMENT = 123;
        //public const int TYPE_REMOVE_COMMENT_ON_COMMENT = 124;
        public const int TYPE_UNLIKE_COMMENT = 125;
        public const int TYPE_ADD_PROFILE_DETAILS = 126;
        /**/
        public const int TYPE_ADD_FRIEND = 127; //"add_friend";
        public const int TYPE_DELETE_FRIEND = 128; // "delete_friend";
        public const int TYPE_ACCEPT_FRIEND = 129; //"accept_friend";
        public const int TYPE_CHANGE_PASSWORD = 130; // "change_password";
        public const int TYPE_CALL_TRANSFER = 132; // "call_start";
        public const int TYPE_CALL_DIVERT = 133; // "call_end";
        public const int TYPE_MULTIPLE_FRIEND_PRESENCE_INFO = 136;
        public const int TYPE_ACTION_GET_FULL_COMMENT = 137;
        public const int TYPE_ACTION_GET_MORE_FEED_IMAGE = 139;
        public const int TYPE_ACTION_GET_MORE_FEED_MEDIA = 140;
        public const int TYPE_DELETE_CIRCLE = 152;// "delete_group";
        public const int TYPE_REMOVE_CIRCLE_MEMBER = 154;  //"remove_group_member"; 
        public const int TYPE_ADD_CIRCLE_MEMBER = 156;// "add_group_member";
        public const int TYPE_EDIT_CIRCLE_MEMBER = 158;//  "edit_group_member";
        public const int TYPE_ADD_CONTACT_LIST_FRIEND = 172;
        public const int TYPE_ADD_MULTIPLE_FRIEND = 173; //"add_multiple_friend";
        public const int TYPE_SEND_REGISTER = 174; // "want to start a call";
        public const int TYPE_DELETE_ALBUM_IMAGE = 176;
        public const int TYPE_ADD_STATUS = 177;
        public const int TYPE_EDIT_STATUS = 178;
        public const int TYPE_DELETE_STATUS = 179;
        //public const int TYPE_ADD_IMAGE_COMMENT = 180;
        public const int TYPE_ADD_STATUS_COMMENT = 181;
        public const int TYPE_DELETE_IMAGE_COMMENT = 182;
        public const int TYPE_DELETE_STATUS_COMMENT = 183;
        public const int TYPE_LIKE_STATUS = 184;
        public const int TYPE_LIKE_UNLIKE_IMAGE = 185;
        public const int TYPE_UNLIKE_STATUS = 186;
        public const int TYPE_HIDE_UNHIDE_FEED = 187;
        public const int TYPE_EDIT_STATUS_COMMENT = 189;
        public const int TYPE_ONILE_OFFLINE_STATUS = 190;
        public const int TYPE_SHARE_STATUS = 191;
        public const int TYPE_ACTION_CHANGE_MOOD = 193;
        public const int TYPE_EDIT_IMAGE_COMMENT = 194;
        public const int TYPE_RESET_NOTIFICATION_COUNTER = 195;
        public const int TYPE_IMAGE_COMMENT_LIKES = 196;
        public const int TYPE_LIKE_UNLIKE_IMAGE_COMMENT = 197;
        public const int TYPE_CIRCLE_NEWSFEED = 198;
        public const int TYPE_SINGLE_FRIEND_PRESENCE_INFO = 199;
        public const int TYPE_DEACTIVATE_ACCOUNT = 202;
        public const int TYPE_HIDE_UNHIDE_USER_FEED = 203;
        public const int TYPE_UNKNWON_PROFILE_INFO = 204;
        public const int TYPE_DIGITS_VERIFY = 208;
        public const int TYPE_UPDATE_DIGITS_VERIFY = 209;
        public const int TYPE_FRIEND_CONTACT_LIST_V_141 = 211;
        public const int TYPE_ACTION_VERIFY_MY_NUMBER = 212;
        public const int TYPE_VERIFY_NUMBER_OR_EMAIL = 215;
        public const int TYPE_UPDATE_TOGGLE_SETTINGS = 216;
        public const int TYPE_SEND_RECOVERED_PASSCODE = 217;
        public const int TYPE_VERIFY_RECOVERED_PASSCODE = 218;
        public const int TYPE_RESET_PASSWORD = 219;
        public const int TYPE_SEND_EMAIL_PASSCODE = 220;
        public const int TYPE_ADD_VERIFY_EMAIL = 221;
        public const int TYPE_RECOVERY_SUGGESTION = 222;
        public const int TYPTE_MISS_CALL_LIST = 224;
        public const int TYPE_ADD_WORK = 227;
        public const int TYPE_UPDATE_WORK = 228;
        public const int TYPE_REMOVE_WORK = 229;
        public const int TYPE_LIST_WORKS_EDUCATIONS_SKILLS = 230;
        public const int TYPE_ADD_EDUCATION = 231;
        public const int TYPE_UPDATE_EDUCATION = 232;
        public const int TYPE_REMOVE_EDUCATION = 233;
        public const int TYPE_LIST_WORK = 234;
        public const int TYPE_LIST_EDUCATION = 235;
        public const int TYPE_LIST_SKILL = 236;
        public const int TYPE_ADD_SKILL = 237;
        public const int TYPE_UPDATE_SKILL = 238;
        public const int TYPE_REMOVE_SKILL = 239;
        public const int TYPE_BLOCK_UNBLOCK_FRIEND = 243;
        public const int TYPE_CHANGE_FRIEND_ACCESS = 244;
        public const int TYPE_ACCEPT_FRIEND_ACCESS = 245;
        public const int TYPE_DELETE_PHOTO = 246;
        public const int TYPE_FRIEND_MIGRATION = 247;
        public const int TYPE_WHO_SHARES_LIST = 249;
        public const int TYPE_MEDIA_SHARE_LIST = 250;
        public const int TYPE_ADD_MEDIA_ALBUM = 253;
        public const int TYPE_UPDATE_MEDIA_ALBUM = 254;
        public const int TYPE_DELETE_MEDIA_ALBUM = 255;
        public const int TYPE_MEDIA_ALBUM_LIST = 256;
        public const int TYPE_MEDIA_ALBUM_DETAILS = 257;
        public const int TYPE_IMAGE_ALBUM_LIST = 96;
        public const int TYPE_ADD_ALBUM_COVER_IMAGE = 1012;
        public const int TYPE_ADD_MEDIA_CONTENT = 258;
        public const int TYPE_EDIT_MEDIA_CONTENT = 259;
        public const int TYPE_DELETE_MEDIA_CONTENT = 260;
        public const int TYPE_MEDIA_ALBUM_CONTENT_LIST = 261;
        public const int TYPE_MEDIA_CONTENT_DETAILS = 262;
        public const int TYPE_LIKE_UNLIKE_MEDIA = 264;
        //public const int TYPE_ADD_COMMENT_ON_MEDIA = 265;
        public const int TYPE_EDIT_COMMENT_ON_MEDIA = 266;
        public const int TYPE_DELETE_COMMENT_ON_MEDIA = 267;
        public const int TYPE_LIKE_UNLIKE_MEDIA_COMMENT = 268;
        public const int TYPE_MEDIA_LIKE_LIST = 269;
        public const int TYPE_MEDIA_COMMENT_LIST = 270;
        public const int TYPE_MEDIACOMMENT_LIKE_LIST = 271;
        public const int TYPE_INCREASE_MEDIA_CONTENT_VIEW_COUNT = 272;
        public const int TYPE_VIEW_DOING_LIST = 273;
        public const int TYPE_VIEW_TAGGED_LIST = 274;
        public const int TYPE_ADD_SOCIAL_MEDIA_ID = 276;

        //Media
        public const int TYPE_MEDIA_SUGGESTION = 277;
        public const int TYPE_MEDIA_CONTENTS_BASED_ON_KEYWORD = 278;
        public const int TYPE_HASHTAG_MEDIA_CONTENTS = 279;
        public const int TYPE_HASHTAG_SUGGESTION = 280;
        public const int TYPE_SEARCH_TRENDS = 281;
        public const int TYPE_STORE_CONTACT_LIST = 284;

        //Celebrity
        public const int TYPE_CELEBRITY_FEED = 288;
        public const int TYPE_FOLLOW_UNFOLLOW_CELEBRITY = 287;
        public const int TYPE_CELEBRITY_LIST = 286;
        public const int TYPE_CELEBRITY_CATEGORY_LIST = 285;

        /*News Portal*/
        public const int TYPE_NEWSPORTAL_ALLTYPELIST = 291;
        public const int TYPE_NEWSPORTAL_CATEGORIES_LIST = 294;
        public const int TYPE_NEWSPORTAL_FEED = 295;
        public const int TYPE_NEWSPORTAL_SUBSCRIBE_UNSUBSCRIBE = 296;
        public const int TYPE_NEWSPORTAL_LIST = 299;
        public const int TYPE_SAVE_UNSAVE_NEWSFEED = 300;
        public const int TYPE_NEWSPORTAL_BREAKING_FEED = 302;
        public const int TYPE_BUSINESSPAGE_BREAKING_FEED = 303;
        public const int TYPE_BUSINESS_PAGE_FEED = 306;
        //public const int TYPE_MEDIA_PAGE_FEED = 307;
        public const int TYPE_MEDIA_PAGE_TRENDING_FEED = 308;
        public const int TYPE_ALL_SAVED_FEEDS = 309;
        public const int TYPE_HIDE_UNHIDE_CIRCLE_FEED = 310;
        /*
         * Client<<----->>Server<<----->>Client [>100 and <200]     
         */
        //public const int TYPE_UPDATE_COMMENT_ON_COMMENT = 322;
        public const int TYPE_UPDATE_LIKE_COMMENT = 323;
        //public const int TYPE_UPDATE_REMOVE_COMMENT_ON_COMMENT = 324;
        public const int TYPE_UPDATE_UNLIKE_COMMENT = 325;
        public const int TYPE_UPDATE_ADD_FRIEND = 327; //"add_friend";    
        public const int TYPE_UPDATE_DELETE_FRIEND = 328; // "delete_friend";    
        public const int TYPE_UPDATE_ACCEPT_FRIEND = 329; //"accept_friend";
        public const int TYPE_UPDATE_CHANGE_PASSWORD = 330; // "change_password";
        public const int TYPE_UPDATE_CALL_TRANSFER = 332; // "call_start";
        public const int TYPE_UPDATE_CALL_END = 333; // "call_end";
        public const int TYPE_UPDATE_MULTIPLE_FRIEND_PRESENCE_INFO = 336;
        public const int TYPE_UPDATE_DELETE_CIRCLE = 352;// "delete_group";
        public const int TYPE_UPDATE_REMOVE_CIRCLE_MEMBER = 354;  //"remove_group_member"; 
        public const int TYPE_UPDATE_ADD_CIRCLE_MEMBER = 356;// "add_group_member";
        public const int TYPE_UPDATE_EDIT_CIRCLE_MEMBER = 358;//  "edit_group_member";
        public const int TYPE_UPDATE_ADD_ADDRESS_BOOK_FRIEND = 372;
        public const int TYPE_UPDATE_ADD_MULTIPLE_FRIEND = 373; //"add_multiple_friend";
        public const int TYPE_UPDATE_SEND_REGISTER = 374; // "want to start a call";
        public const int TYPE_UPDATE_START_CHAT = 375; // "want to start a call";
        public const int TYPE_UPDATE_DELETE_ALBUM_IMAGE = 376;
        public const int TYPE_UPDATE_ADD_STATUS = 377;
        public const int TYPE_UPDATE_EDIT_STATUS = 378;
        public const int TYPE_UPDATE_DELETE_STATUS = 379;
        public const int TYPE_UPDATE_ADD_IMAGE_COMMENT = 380;
        public const int TYPE_UPDATE_ADD_STATUS_COMMENT = 381;
        public const int TYPE_UPDATE_DELETE_IMAGE_COMMENT = 382;
        public const int TYPE_UPDATE_DELETE_STATUS_COMMENT = 383;
        public const int TYPE_UPDATE_LIKE_STATUS = 384;
        public const int TYPE_UPDATE_LIKE_UNLIKE_IMAGE = 385;
        public const int TYPE_UPDATE_UNLIKE_STATUS = 386;
        public const int TYPE_UPDATE_EDIT_STATUS_COMMENT = 389;
        public const int TYPE_UPDATE_SHARE_STATUS = 391;
        public const int TYPE_UPDATE_EDIT_IMAGE_COMMENT = 394;
        public const int TYPE_UPDATE_LIKE_UNLIKE_IMAGE_COMMENT = 397;
        public const int TYPE_UPDATE_DEACTIVATE_ACCOUNT = 402;
        public const int TYPE_UPDATE_CHANGE_FRIEND_ACCESS = 444;
        public const int TYPE_UPDATE_ACCEPT_FRIEND_ACCESS = 445;
        public const int TYPE_UPDATE_FRIEND_MIGRATION = 447;

        public const int TYPE_UPDATE_LIKEUNLIKE_MEDIA = 464;
        public const int TYPE_UPDATE_COMMENT_MEDIA = 465;
        public const int TYPE_UPDATE_EDITCOMMENT_MEDIA = 466;
        public const int TYPE_UPDATE_DELETECOMMENT_MEDIA = 467;
        public const int TYPE_UPDATE_LIKEUNLIKECOMMENT_MEDIA = 468;
        public const int TYPE_UPDATE_STORE_CONTACT_LIST = 484;

        public const int TYPE_SPAM_REASON_LIST = 1001;
        public const int TYPE_SPAM_REPORT = 1002;
        //others constants
        //  public const String CALLEE_FRIEND_ID = "CALLEE_FRIEND_ID";
        public const String TERMINATION_RULE = "IP PORT";
        //    /*PRIVACY*/
        public const int PRIVACY_ONLY_ME = 1;
        public const int PRIVACY_FRIENDSTOSHOW = 5;
        public const int PRIVACY_FRIENDSTOHIDE = 10;
        public const int PRIVACY_FRIENDSONLY = 15;
        public const int PRIVACY_FRIENDSOFFRIENDS = 20;
        public const int PRIVACY_PUBLIC = 25;

        /**/
        public const int STATUS_IN_PROGRESS = 201;
        public const int STATUS_SENT = 202;
        //public const int STATUS_NOT_ENOUGH_BALANCE = 204;
        /* SMS History */
        public const int HISTORY_STATUS_IN_PROGRESS = 0;
        public const int HISTORY_STATUS_FAILED = 1;
        public const int HISTORY_STATUS_SENT = 2;
        public const int HISTORY_STATUS_DELETED = 3;
        /*chat*/
        public const int TYPE_START_FRIEND_CHAT = 175; // "want to start friend chat";
        public const int TYPE_START_GROUP_CHAT = 134; // "want to start group chat";
        public const int TYPE_ADD_GROUP_MEMBER = 135; // "want to add group member in chat";
        public const int TYPE_UPDATE_START_FRIEND_CHAT = 375; // "want to start friend chat";
        public const int TYPE_UPDATE_START_GROUP_CHAT = 334; // "want to start group chat";
        public const int TYPE_UPDATE_ADD_GROUP_MEMBER = 335; // "want to add group member in chat";

        public const int TYPE_NEWSFEED_EDIT_HISTORY_DETAILS = 1017;
        public const int TYPE_NEWSFEED_EDIT_HISTORY_LIST = 1016;
        public const int TYPE_COMMENT_EDIT_HISTORY_LIST = 1021;
        public const int ACTION_MERGED_LIKE_UNLIKE_COMMENT = 1123;
        public const int ACTION_MERGED_LIKES_LIST_OF_COMMENT = 1116;
        public const int ACTION_MERGED_COMMENTS_LIST = 1084;
        public const int ACTION_MERGED_DELETE_COMMENT = 1183;
        public const int ACTION_MERGED_UPDATE_ADD_COMMENT = 1381;
        public const int ACTION_MERGED_UPDATE_DELETE_COMMENT = 1383;
        public const int ACTION_MERGED_UPDATE_LIKE_UNLIKE = 1384;
        public const int ACTION_UPDATE_LIKE_UNLIKE_COMMENT = 1323;
        /*WALLET*/
        public const int TYPE_GET_WALLET_INFORMATION = 1026;
        public const int TYPE_GET_COIN_EXCHANGE_RATE_INFORMATION = 1027;
        public const int TYPE_TRANSFER_AVAILABLE_BONUS_COIN_TO_WALLET = 1028;
        public const int TYPE_TRANSFER_COIN_USER_TO_USER_WALLET = 1029;
        public const int TYPE_GET_CURRENCY_LIST = 1030;
        public const int TYPE_GET_TRANSACTION_HISTORY = 1031;
        public const int TYPE_ADD_REFERRER = 1036;
        public const int TYPE_ACCEPT_OR_DECLINE_REFERRAL_REQUEST = 10382; // 12-11-2016
        public const int TYPE_UPDATE_REFERRAL_REQUEST = 1037;
        public const int TYPE_GET_COIN_BUNDLE_LIST = 1038;
        public const int TYPE_REFERRAL_NETWORK_SUMMARY = 1041;
        public const int TYPE_MY_REFERRALS_LIST = 1042;
        public const int TYPE_GET_TOP_CONTRIBUTORS = 1044;
        public const int TYPE_GET_COIN_EARNING_RULE = 1045;
        public const int TYPE_PAYMENT_RECEIVED = 1046;
        public const int TYPE_GET_GIFT_PRODUCTS = 1039;
        public const int TYPE_SKIP_REFERRER = 1047;
        public const int TYPE_DAILY_CHECK_IN = 1049;
        public const int TYPE_DAILY_CHECKIN_HISTORY = 1054;
        public const int TYPE_DAILY_CHECKIN_RULES = 1055;
        public const int TYPE_DAILY_TASK_DWELL_TIME = 1050;
        public const int TYPE_PURCHASE_COIN = 1043;
        public const int TYPE_SEND_GIFT = 1040;
        public const int TYPE_RECEIVED_GIFT = 1052;
        public const int TYPE_SHARE_TO_SOCIAL_MEDIA = 1051;
        public const int TYPE_FOLLOW_UNFOLLOW_USER = 1063;
        public const int TYPE_UPDATE_FOLLOW_UNFOLLOW_USER = 1064;
        
        //public const int TYPE_UPDATE_ACCEPT_OR_DECLINE_REFERRAL_REQUEST = 1039; // 12-11-2016
        /*notification max ut*/

        /* Start Live Stream */
        public const int TYPE_INITIALIZE_LIVE_STREAM = 2000;
        public const int TYPE_START_LIVE_STREAM = 2001;
        public const int TYPE_END_LIVE_STREAM = 2002;
        public const int TYPE_GET_LIVE_STREAMING_DETAILS = 2003;
        public const int TYPE_GET_FEATURED_LIVE_STREAMS = 2004;
        public const int TYPE_GET_RECENT_STREAMS = 2005;
        public const int TYPE_UPDATE_LIVE_STREAM = 2006;
        public const int TYPE_GET_STREAM_CATEGORY_LIST = 2007;
        public const int TYPE_GET_MOST_VIEWED_STREAMS = 2008;
        public const int TYPE_GET_NEAREST_STREAMS = 2009;
        public const int TYPE_ACTION_SEARCH_LIVE_STREAMS = 2010;
        public const int TYPE_ACTION_LIVE_STREAMING_USER_DETAILS = 2011;
        public const int TYPE_UPDATE_STREAMING_LIKE_COUNT = 2012;
        public const int TYPE_GET_FOLLOWING_STREAMS = 2013;
        public const int TYPE_GET_CATEGORY_WISE_STREAM_COUNT = 2014;

        /* End Live Stream */

        /* Start Channel */

        public const int TYPE_CREATE_CHANNEL = 2015;
        public const int TYPE_GET_CHANNEL_CATEGORY_LIST = 2016;
        public const int TYPE_UPDATE_CHANNEL_INFO = 2017;
        public const int TYPE_ACTIVATE_DEACTIVATE_CHANNEL = 2018;
        public const int TYPE_GET_FOLLOWING_CHANNEL_LIST = 2019;
        public const int TYPE_GET_MOST_VIEWED_CHANNEL_LIST = 2020;
        public const int TYPE_GET_OWN_CHANNEL_LIST = 2021;
        public const int TYPE_SUBSCRIBE_UNSUBSCRIBE_CHANNEL = 2022;
        public const int TYPE_GET_CHANNEL_DETAILS = 2023;
        public const int TYPE_ADD_CHANNEL_PROFILE_IMAGE = 2024;
        public const int TYPE_ADD_CHANNEL_COVER_IMAGE = 2025;
        public const int TYPE_ADD_MEDIA_TO_CHANNEL = 2026;
        public const int TYPE_GET_CHANNEL_MEDIA_LIST = 2028;
        public const int TYPE_UPDATE_CHANNEL_MEDIA_STATUS = 2029;
        public const int TYPE_GET_FEATURED_CHANNEL_LIST = 2030;
        public const int TYPE_UPDATE_CHANNEL_MEDIA_INFO = 2031;
        public const int TYPE_ACTION_SEARCH_CHANNEL_LIST = 2032;
        public const int TYPE_GET_CHANNEL_PLAYLIST = 2033;
        public const int TYPE_GET_CHANNEL_MEDIA_ITEM = 2034;

        /* End Channel */

        public static long NOTIFICATION_MAX_UT = 0;
        public static long NOTIFICATION_MIN_UT = 0;
        public static long NOTIFICATION_LAST_MAX_UT = 0;

        public static bool NOTIFICATION_ALL_FETCHED_FROM_DB = false;

        public static int CALL_NOTIFICATION_COUNT = 0;
        public static int CHAT_NOTIFICATION_COUNT = 0;
        public static int ALL_NOTIFICATION_COUNT = 0;
        public static int ADD_FRIEND_NOTIFICATION_COUNT = 0;

        //Device Platform Constants
        public const int PLATFORM_DESKTOP = 1;
        public const int PLATFORM_ANDROID = 2;
        public const int PLATFORM_IPHONE = 3;
        public const int PLATFORM_WINDOWS = 4;
        public const int PLATFORM_WEB = 5;

        //Device Categoryy
        public const int CATEGORY_PC = 1;
        public const int CATEGORY_MOBILE = 3;
        public const int CATEGORY_WEB = 5;

        /*friend feed activitys*/
        public const short NEWSFEED_LIKED = 1;
        public const short NEWSFEED_COMMENTED = 2;
        public const short NEWSFEED_SHARED = 3;

        /*streaming*/
        public const int AUDIO_CHANNEL = 1;
        public const int VIDEO_CHANNEL = 2;
        public const int TV_CHANNEL = 3;
        public const int LIVE_STREAM = 4;

        /* TIME INTERVALS */
        public const long TEN_MINUTES = 600000; // 5 minutes
        public const long FIVE_MINUTES = 300000; // 5 minutes

        //Validity
        public const int DEFAULT_VALIDITY = 0; // represents unlimited
        public const int VALIDITY_ONE_DAY = 1;
        public const string UNLIMITED_VALIDITY_STRING = "Unlimited";

        //Default Thumb

        public const int DEFAULT_THUMB_HEIGHT = 150;
        public const int DEFAULT_THUMB_WIDTH = 150;

        public const int EDIT_WALL_TYPE = -100;

    }
}
=======
﻿using System;

namespace Models.Constants
{
    public class AppConstants
    {
        /*request type
        1st byte + broken type+ packet.........
        */
        public const int SINGLE_PACKET = 0;
        public const int BROKEN_PACKET = 1;

        public const int REQUEST_TYPE_KEEP_ALIVE = 1;
        public const int REQUEST_TYPE_CONFIRMATION = 2;
        public const int REQUEST_TYPE_AUTH = 3;
        public const int REQUEST_TYPE_REQUEST = 5;
        public const int REQUEST_TYPE_UPDATE = 4;
        public const int REQUEST_TYPE_CALL = 6;
        public const int REQUEST_TYPE_CHAT = 7;

        /**/
        public const int CLIENT_DATA_SIZE = 460;
        public const int HEADER_SIZE = 12;
        public const int DATA_SIZE = 500;
        public const int DATA_OFFSET = 1;

        public const int CONS_FULL_NAME = 1;
        //public const int CONS_LAST_NAME = 2;
        public const int CONS_GENDER = 3;
        public const int CONS_COUNTRY = 4;
        public const int CONS_MOBILE_PHONE = 5;
        public const int CONS_EMAIL = 6;
        public const int ACTION_ADD_NUMBER_OR_EMAIL = 214;
        public const int CONS_PRESENCE = 7;
        public const int CONS_WHAT_IS_IN_UR_MIND = 8;
        public const int CONS_FRIENDSHIP_STATUS = 9;
        public const int CONS_CALLING_CODE = 10;
        public const int CONS_PREV_PASS = 11;
        public const int CONS_NEW_PASS = 12;
        public const int CONS_PROFILE_IMAGE = 13;
        public const int CONS_BIRTH_DAY = 14;
        public const int CONS_COVER_IMAGE = 15;
        public const int CONS_PRIVACY = 16;
        /**/
        public const int CONS_CURRENT_CITY = 18;
        public const int CONS_HOME_CITY = 19;
        public const int CONS_MARRIAGE_DAY = 20;
        public const int CONS_ABOUT_ME = 21;
        //-------------------------------------- changes made by reefat[13.03.2014] (start) --------------------------------------
        //--------------- revised call type
        public const int TYPE_CONFIRMATION = 200;
        /*
         * Client<<------->>Server[<100]
         */
        public const int NEWS_FEED_TYPE_IMAGE = 1;
        public const int NEWS_FEED_TYPE_STATUS = 2;
        public const int NEWS_FEED_TYPE_MULTIPLE_IMAGE = 3;
        public const int NEWS_FEED_TYPE_ALBUM = 4;
        public const int TYPE_INVALID_LOGIN_SESSION = 19;
        public const int TYPE_SIGN_IN = 20; // "signin";    
        public const int TYPE_MY_PROFILE_DETAILS = 21; // "my profile details";
        public const int TYPE_SIGN_OUT = 22; //"signout";    
        public const int TYPE_CONTACT_LIST = 23; // "contactList";
        public const int TYPE_USER_ID_AVAILABLE = 24; // "userIdentityAvailable";
        public const int TYPE_USER_PROFILE = 25; // "user_profile";    
        public const int TYPE_KEEP_ALIVE = 26; // "keepAlive";
        public const int TYPE_MY_SETTINGS_VALUES = 28; //mysettings
        public const int TYPE_CONTACT_UTIDS = 29;
        public const int TYPE_SUGGESTION_IDS = 31;
        public const int TYPE_USERS_DETAILS = 32;
        public const int TYPE_REMOVE_SUGGESTION = 33;
        public const int TYPE_CONTACT_SEARCH = 34; // "contactSearch";
        public const int TYPE_CALL_LOG = 35; // "callLog";
        public const int TYPE_PROFILE_IMAGE_TEXT = 37; // "profileImage";
        public const int TYPE_ADD_ADDRESSBOOK_FRIEND = 38; // "addAddressBookFriend";
        public const int TYPE_PASSWORD_RECOVERY = 39; // "passwordRecovery";
        public const int TYPE_PASSWORD_RESET = 40; // "passwordReset";
        public const int TYPE_EMOTICONS_ALL = 41; // "emoticons_all";
        public const int TYPE_EMOTICONS = 42; // "emoticons";
        public const int TYPE_REMOVE_PROFILE_IMAGE = 43; // "removeProfileImage";
        public const int TYPE_BALANCE = 44; // "balance";
        public const int TYPE_CREATE_CIRCLE = 50;//"create_group";
        public const int TYPE_NEW_CIRCLE = 51;//"new_group";
        public const int TYPE_CIRCLE_DETAILS = 52;
        public const int TYPE_LEAVE_CIRCLE = 53;// "leave_group";
        public const int TYPE_SEND_SMS = 61;//”send_sms”
        public const int TYPE_UPDATE_PROFILE_IMAGE = 63;
        public const int TYPE_CIRCLE_LIST = 70;
        public const int TYPE_ADDRESS_BOOK = 71;
        public const int TYPE_PRIVACY_SETTINGS = 74;   //"privacy_settings"
        public const int TYPE_MULTIPLE_SESSION_WARNING = 75;
        public const int TYPE_SESSION_VALIDATION = 76;
        public const int TYPE_SERVER_REPORT = 77;
        public const int TYPE_PRESENCE = 78;
        public const int TYPE_UNWANTED_LOGIN = 79;

        /*
         * 81,82 and 83 has bben defined for SMS action types
         * Please do not use them here
         * public const int TYPE_FRIEND_SMS = 81;
         * public const int TYPE_CIRCLE_SMS = 82;
         * public const int TYPE_KNOW_SMS_STATUS = 83;
         * 
         */
        public const int TYPE_UPDATE_CONTACT_ACCESS = 82;
        public const int TYPE_GET_IM_OFFLINE_SERVER = 83;
        public const int TYPE_COMMENTS_FOR_STATUS = 84;
        public const int TYPE_UPLOAD_ALBUM_IMAGE = 85;
        public const int TYPE_STICKERS = 86;
        public const int TYPE_MEDIA_FEED = 87;
        public const int TYPE_NEWS_FEED = 88;
        public const int TYPE_COMMENTS_FOR_IMAGE = 89;
        public const int TYPE_UPDATE_UPLOAD_ALBUM_IMAGE = 90;
        public const int TYPE_STICKERS_BACKGROUND_IMAGES = 91;
        public const int TYPE_LIKES_FOR_STATUS = 92;
        public const int TYPE_LIKES_FOR_IMAGE = 93;
        public const int TYPE_MY_BOOK = 94;
        public const int TYPE_USER_DETAILS = 95;
        public const int TYPE_MY_ALBUMS = 96;
        public const int TYPE_MY_ALBUM_IMAGES = 97;
        public const int TYPE_FRIENDS_PRESENCE_LIST = 98;
        public const int TYPE_CIRCLE_MEMBERS_LIST = 99;
        public const int TYPE_SEARCH_CIRCLE_MEMBER = 101;
        public const int TYPE_SEND_MOBILE_PASSCODE = 100;
        public const int TYPE_UPDATE_COVER_IMAGE = 103;
        public const int TYPE_REMOVE_COVER_IMAGE = 104;
        public const int TYPE_UPDATE_FRIEND_GROUP = 105;
        public const int TYPE_YOU_MAY_KNOW_LIST = 106;
        //public const int TYPE_FRIEND_ALL_DETAILS = 107;
        public const int TYPE_FRIEND_CONTACT_LIST = 107;
        public const int TYPE_FRIEND_ALBUMS = 108;
        public const int TYPE_FRIEND_ALBUM_IMAGES = 109;
        public const int TYPE_FRIEND_NEWSFEED = 110;
        public const int TYPE_MY_NOTIFICATIONS = 111;
        public const int TYPE_DELETE_MY_NOTIFICATIONS = 112;
        public const int TYPE_SINGLE_NOTIFICATION = 113;
        public const int TYPE_SINGLE_STATUS_NOTIFICATION = 114;
        /**/
        public const int TYPE_SINGLE_FEED_SHARE_LIST = 115;
        public const int TYPE_LIST_LIKES_OF_COMMENT = 116;
        public const int TYPE_MULTIPLE_IMAGE_POST = 117;
        public const int TYPE_MUTUAL_FRIENDS = 118;
        public const int TYPE_UPDATE_TAG_NAME = 119;
        /*
         * Client<<----->>Server<<----->>Client [>100 and <200]     
         */
        public const int TYPE_IMAGE_DETAILS = 121;
        //public const int TYPE_ADD_COMMENT_ON_COMMENT = 122;
        public const int TYPE_LIKE_COMMENT = 123;
        //public const int TYPE_REMOVE_COMMENT_ON_COMMENT = 124;
        public const int TYPE_UNLIKE_COMMENT = 125;
        public const int TYPE_ADD_PROFILE_DETAILS = 126;
        /**/
        public const int TYPE_ADD_FRIEND = 127; //"add_friend";
        public const int TYPE_DELETE_FRIEND = 128; // "delete_friend";
        public const int TYPE_ACCEPT_FRIEND = 129; //"accept_friend";
        public const int TYPE_CHANGE_PASSWORD = 130; // "change_password";
        public const int TYPE_CALL_TRANSFER = 132; // "call_start";
        public const int TYPE_CALL_DIVERT = 133; // "call_end";
        public const int TYPE_MULTIPLE_FRIEND_PRESENCE_INFO = 136;
        public const int TYPE_ACTION_GET_FULL_COMMENT = 137;
        public const int TYPE_ACTION_GET_MORE_FEED_IMAGE = 139;
        public const int TYPE_ACTION_GET_MORE_FEED_MEDIA = 140;
        public const int TYPE_DELETE_CIRCLE = 152;// "delete_group";
        public const int TYPE_REMOVE_CIRCLE_MEMBER = 154;  //"remove_group_member"; 
        public const int TYPE_ADD_CIRCLE_MEMBER = 156;// "add_group_member";
        public const int TYPE_EDIT_CIRCLE_MEMBER = 158;//  "edit_group_member";
        public const int TYPE_ADD_CONTACT_LIST_FRIEND = 172;
        public const int TYPE_ADD_MULTIPLE_FRIEND = 173; //"add_multiple_friend";
        public const int TYPE_SEND_REGISTER = 174; // "want to start a call";
        public const int TYPE_DELETE_ALBUM_IMAGE = 176;
        public const int TYPE_ADD_STATUS = 177;
        public const int TYPE_EDIT_STATUS = 178;
        public const int TYPE_DELETE_STATUS = 179;
        //public const int TYPE_ADD_IMAGE_COMMENT = 180;
        public const int TYPE_ADD_STATUS_COMMENT = 181;
        public const int TYPE_DELETE_IMAGE_COMMENT = 182;
        public const int TYPE_DELETE_STATUS_COMMENT = 183;
        public const int TYPE_LIKE_STATUS = 184;
        public const int TYPE_LIKE_UNLIKE_IMAGE = 185;
        public const int TYPE_UNLIKE_STATUS = 186;
        public const int TYPE_HIDE_UNHIDE_FEED = 187;
        public const int TYPE_EDIT_STATUS_COMMENT = 189;
        public const int TYPE_ONILE_OFFLINE_STATUS = 190;
        public const int TYPE_SHARE_STATUS = 191;
        public const int TYPE_ACTION_CHANGE_MOOD = 193;
        public const int TYPE_EDIT_IMAGE_COMMENT = 194;
        public const int TYPE_RESET_NOTIFICATION_COUNTER = 195;
        public const int TYPE_IMAGE_COMMENT_LIKES = 196;
        public const int TYPE_LIKE_UNLIKE_IMAGE_COMMENT = 197;
        public const int TYPE_CIRCLE_NEWSFEED = 198;
        public const int TYPE_SINGLE_FRIEND_PRESENCE_INFO = 199;
        public const int TYPE_DEACTIVATE_ACCOUNT = 202;
        public const int TYPE_HIDE_UNHIDE_USER_FEED = 203;
        public const int TYPE_UNKNWON_PROFILE_INFO = 204;
        public const int TYPE_DIGITS_VERIFY = 208;
        public const int TYPE_UPDATE_DIGITS_VERIFY = 209;
        public const int TYPE_FRIEND_CONTACT_LIST_V_141 = 211;
        public const int TYPE_ACTION_VERIFY_MY_NUMBER = 212;
        public const int TYPE_VERIFY_NUMBER_OR_EMAIL = 215;
        public const int TYPE_UPDATE_TOGGLE_SETTINGS = 216;
        public const int TYPE_SEND_RECOVERED_PASSCODE = 217;
        public const int TYPE_VERIFY_RECOVERED_PASSCODE = 218;
        public const int TYPE_RESET_PASSWORD = 219;
        public const int TYPE_SEND_EMAIL_PASSCODE = 220;
        public const int TYPE_ADD_VERIFY_EMAIL = 221;
        public const int TYPE_RECOVERY_SUGGESTION = 222;
        public const int TYPTE_MISS_CALL_LIST = 224;
        public const int TYPE_ADD_WORK = 227;
        public const int TYPE_UPDATE_WORK = 228;
        public const int TYPE_REMOVE_WORK = 229;
        public const int TYPE_LIST_WORKS_EDUCATIONS_SKILLS = 230;
        public const int TYPE_ADD_EDUCATION = 231;
        public const int TYPE_UPDATE_EDUCATION = 232;
        public const int TYPE_REMOVE_EDUCATION = 233;
        public const int TYPE_LIST_WORK = 234;
        public const int TYPE_LIST_EDUCATION = 235;
        public const int TYPE_LIST_SKILL = 236;
        public const int TYPE_ADD_SKILL = 237;
        public const int TYPE_UPDATE_SKILL = 238;
        public const int TYPE_REMOVE_SKILL = 239;
        public const int TYPE_BLOCK_UNBLOCK_FRIEND = 243;
        public const int TYPE_CHANGE_FRIEND_ACCESS = 244;
        public const int TYPE_ACCEPT_FRIEND_ACCESS = 245;
        public const int TYPE_DELETE_PHOTO = 246;
        public const int TYPE_FRIEND_MIGRATION = 247;
        public const int TYPE_WHO_SHARES_LIST = 249;
        public const int TYPE_MEDIA_SHARE_LIST = 250;
        public const int TYPE_ADD_MEDIA_ALBUM = 253;
        public const int TYPE_UPDATE_MEDIA_ALBUM = 254;
        public const int TYPE_DELETE_MEDIA_ALBUM = 255;
        public const int TYPE_MEDIA_ALBUM_LIST = 256;
        public const int TYPE_MEDIA_ALBUM_DETAILS = 257;
        public const int TYPE_IMAGE_ALBUM_LIST = 96;
        public const int TYPE_ADD_ALBUM_COVER_IMAGE = 1012;
        public const int TYPE_ADD_MEDIA_CONTENT = 258;
        public const int TYPE_EDIT_MEDIA_CONTENT = 259;
        public const int TYPE_DELETE_MEDIA_CONTENT = 260;
        public const int TYPE_MEDIA_ALBUM_CONTENT_LIST = 261;
        public const int TYPE_MEDIA_CONTENT_DETAILS = 262;
        public const int TYPE_LIKE_UNLIKE_MEDIA = 264;
        //public const int TYPE_ADD_COMMENT_ON_MEDIA = 265;
        public const int TYPE_EDIT_COMMENT_ON_MEDIA = 266;
        public const int TYPE_DELETE_COMMENT_ON_MEDIA = 267;
        public const int TYPE_LIKE_UNLIKE_MEDIA_COMMENT = 268;
        public const int TYPE_MEDIA_LIKE_LIST = 269;
        public const int TYPE_MEDIA_COMMENT_LIST = 270;
        public const int TYPE_MEDIACOMMENT_LIKE_LIST = 271;
        public const int TYPE_INCREASE_MEDIA_CONTENT_VIEW_COUNT = 272;
        public const int TYPE_VIEW_DOING_LIST = 273;
        public const int TYPE_VIEW_TAGGED_LIST = 274;
        public const int TYPE_ADD_SOCIAL_MEDIA_ID = 276;

        //Media
        public const int TYPE_MEDIA_SUGGESTION = 277;
        public const int TYPE_MEDIA_CONTENTS_BASED_ON_KEYWORD = 278;
        public const int TYPE_HASHTAG_MEDIA_CONTENTS = 279;
        public const int TYPE_HASHTAG_SUGGESTION = 280;
        public const int TYPE_SEARCH_TRENDS = 281;
        public const int TYPE_STORE_CONTACT_LIST = 284;

        //Celebrity
        public const int TYPE_CELEBRITY_FEED = 288;
        public const int TYPE_FOLLOW_UNFOLLOW_CELEBRITY = 287;
        public const int TYPE_CELEBRITY_LIST = 286;
        public const int TYPE_CELEBRITY_CATEGORY_LIST = 285;

        /*News Portal*/
        public const int TYPE_NEWSPORTAL_ALLTYPELIST = 291;
        public const int TYPE_NEWSPORTAL_CATEGORIES_LIST = 294;
        public const int TYPE_NEWSPORTAL_FEED = 295;
        public const int TYPE_NEWSPORTAL_SUBSCRIBE_UNSUBSCRIBE = 296;
        public const int TYPE_NEWSPORTAL_LIST = 299;
        public const int TYPE_SAVE_UNSAVE_NEWSFEED = 300;
        public const int TYPE_NEWSPORTAL_BREAKING_FEED = 302;
        public const int TYPE_BUSINESSPAGE_BREAKING_FEED = 303;
        public const int TYPE_BUSINESS_PAGE_FEED = 306;
        //public const int TYPE_MEDIA_PAGE_FEED = 307;
        public const int TYPE_MEDIA_PAGE_TRENDING_FEED = 308;
        public const int TYPE_ALL_SAVED_FEEDS = 309;
        public const int TYPE_HIDE_UNHIDE_CIRCLE_FEED = 310;
        /*
         * Client<<----->>Server<<----->>Client [>100 and <200]     
         */
        //public const int TYPE_UPDATE_COMMENT_ON_COMMENT = 322;
        public const int TYPE_UPDATE_LIKE_COMMENT = 323;
        //public const int TYPE_UPDATE_REMOVE_COMMENT_ON_COMMENT = 324;
        public const int TYPE_UPDATE_UNLIKE_COMMENT = 325;
        public const int TYPE_UPDATE_ADD_FRIEND = 327; //"add_friend";    
        public const int TYPE_UPDATE_DELETE_FRIEND = 328; // "delete_friend";    
        public const int TYPE_UPDATE_ACCEPT_FRIEND = 329; //"accept_friend";
        public const int TYPE_UPDATE_CHANGE_PASSWORD = 330; // "change_password";
        public const int TYPE_UPDATE_CALL_TRANSFER = 332; // "call_start";
        public const int TYPE_UPDATE_CALL_END = 333; // "call_end";
        public const int TYPE_UPDATE_MULTIPLE_FRIEND_PRESENCE_INFO = 336;
        public const int TYPE_UPDATE_DELETE_CIRCLE = 352;// "delete_group";
        public const int TYPE_UPDATE_REMOVE_CIRCLE_MEMBER = 354;  //"remove_group_member"; 
        public const int TYPE_UPDATE_ADD_CIRCLE_MEMBER = 356;// "add_group_member";
        public const int TYPE_UPDATE_EDIT_CIRCLE_MEMBER = 358;//  "edit_group_member";
        public const int TYPE_UPDATE_ADD_ADDRESS_BOOK_FRIEND = 372;
        public const int TYPE_UPDATE_ADD_MULTIPLE_FRIEND = 373; //"add_multiple_friend";
        public const int TYPE_UPDATE_SEND_REGISTER = 374; // "want to start a call";
        public const int TYPE_UPDATE_START_CHAT = 375; // "want to start a call";
        public const int TYPE_UPDATE_DELETE_ALBUM_IMAGE = 376;
        public const int TYPE_UPDATE_ADD_STATUS = 377;
        public const int TYPE_UPDATE_EDIT_STATUS = 378;
        public const int TYPE_UPDATE_DELETE_STATUS = 379;
        public const int TYPE_UPDATE_ADD_IMAGE_COMMENT = 380;
        public const int TYPE_UPDATE_ADD_STATUS_COMMENT = 381;
        public const int TYPE_UPDATE_DELETE_IMAGE_COMMENT = 382;
        public const int TYPE_UPDATE_DELETE_STATUS_COMMENT = 383;
        public const int TYPE_UPDATE_LIKE_STATUS = 384;
        public const int TYPE_UPDATE_LIKE_UNLIKE_IMAGE = 385;
        public const int TYPE_UPDATE_UNLIKE_STATUS = 386;
        public const int TYPE_UPDATE_EDIT_STATUS_COMMENT = 389;
        public const int TYPE_UPDATE_SHARE_STATUS = 391;
        public const int TYPE_UPDATE_EDIT_IMAGE_COMMENT = 394;
        public const int TYPE_UPDATE_LIKE_UNLIKE_IMAGE_COMMENT = 397;
        public const int TYPE_UPDATE_DEACTIVATE_ACCOUNT = 402;
        public const int TYPE_UPDATE_CHANGE_FRIEND_ACCESS = 444;
        public const int TYPE_UPDATE_ACCEPT_FRIEND_ACCESS = 445;
        public const int TYPE_UPDATE_FRIEND_MIGRATION = 447;

        public const int TYPE_UPDATE_LIKEUNLIKE_MEDIA = 464;
        public const int TYPE_UPDATE_COMMENT_MEDIA = 465;
        public const int TYPE_UPDATE_EDITCOMMENT_MEDIA = 466;
        public const int TYPE_UPDATE_DELETECOMMENT_MEDIA = 467;
        public const int TYPE_UPDATE_LIKEUNLIKECOMMENT_MEDIA = 468;
        public const int TYPE_UPDATE_STORE_CONTACT_LIST = 484;

        public const int TYPE_SPAM_REASON_LIST = 1001;
        public const int TYPE_SPAM_REPORT = 1002;
        //others constants
        //  public const String CALLEE_FRIEND_ID = "CALLEE_FRIEND_ID";
        public const String TERMINATION_RULE = "IP PORT";
        //    /*PRIVACY*/
        public const int PRIVACY_ONLY_ME = 1;
        public const int PRIVACY_FRIENDSTOSHOW = 5;
        public const int PRIVACY_FRIENDSTOHIDE = 10;
        public const int PRIVACY_FRIENDSONLY = 15;
        public const int PRIVACY_FRIENDSOFFRIENDS = 20;
        public const int PRIVACY_PUBLIC = 25;

        /**/
        public const int STATUS_IN_PROGRESS = 201;
        public const int STATUS_SENT = 202;
        //public const int STATUS_NOT_ENOUGH_BALANCE = 204;
        /* SMS History */
        public const int HISTORY_STATUS_IN_PROGRESS = 0;
        public const int HISTORY_STATUS_FAILED = 1;
        public const int HISTORY_STATUS_SENT = 2;
        public const int HISTORY_STATUS_DELETED = 3;
        /*chat*/
        public const int TYPE_START_FRIEND_CHAT = 175; // "want to start friend chat";
        public const int TYPE_START_GROUP_CHAT = 134; // "want to start group chat";
        public const int TYPE_ADD_GROUP_MEMBER = 135; // "want to add group member in chat";
        public const int TYPE_UPDATE_START_FRIEND_CHAT = 375; // "want to start friend chat";
        public const int TYPE_UPDATE_START_GROUP_CHAT = 334; // "want to start group chat";
        public const int TYPE_UPDATE_ADD_GROUP_MEMBER = 335; // "want to add group member in chat";

        public const int TYPE_NEWSFEED_EDIT_HISTORY_DETAILS = 1017;
        public const int TYPE_NEWSFEED_EDIT_HISTORY_LIST = 1016;
        public const int TYPE_COMMENT_EDIT_HISTORY_LIST = 1021;
        public const int ACTION_MERGED_LIKE_UNLIKE_COMMENT = 1123;
        public const int ACTION_MERGED_LIKES_LIST_OF_COMMENT = 1116;
        public const int ACTION_MERGED_COMMENTS_LIST = 1084;
        public const int ACTION_MERGED_DELETE_COMMENT = 1183;
        public const int ACTION_MERGED_UPDATE_ADD_COMMENT = 1381;
        public const int ACTION_MERGED_UPDATE_DELETE_COMMENT = 1383;
        public const int ACTION_MERGED_UPDATE_LIKE_UNLIKE = 1384;
        public const int ACTION_UPDATE_LIKE_UNLIKE_COMMENT = 1323;
        /*WALLET*/
        public const int TYPE_GET_WALLET_INFORMATION = 1026;
        public const int TYPE_GET_COIN_EXCHANGE_RATE_INFORMATION = 1027;
        public const int TYPE_TRANSFER_AVAILABLE_BONUS_COIN_TO_WALLET = 1028;
        public const int TYPE_TRANSFER_COIN_USER_TO_USER_WALLET = 1029;
        public const int TYPE_GET_CURRENCY_LIST = 1030;
        public const int TYPE_GET_TRANSACTION_HISTORY = 1031;
        public const int TYPE_ADD_REFERRER = 1036;
        public const int TYPE_ACCEPT_OR_DECLINE_REFERRAL_REQUEST = 10382; // 12-11-2016
        public const int TYPE_UPDATE_REFERRAL_REQUEST = 1037;
        public const int TYPE_GET_COIN_BUNDLE_LIST = 1038;
        public const int TYPE_REFERRAL_NETWORK_SUMMARY = 1041;
        public const int TYPE_MY_REFERRALS_LIST = 1042;
        public const int TYPE_GET_TOP_CONTRIBUTORS = 1044;
        public const int TYPE_GET_COIN_EARNING_RULE = 1045;
        public const int TYPE_PAYMENT_RECEIVED = 1046;
        public const int TYPE_GET_GIFT_PRODUCTS = 1039;
        public const int TYPE_SKIP_REFERRER = 1047;
        public const int TYPE_DAILY_CHECK_IN = 1049;
        public const int TYPE_DAILY_CHECKIN_HISTORY = 1054;
        public const int TYPE_DAILY_CHECKIN_RULES = 1055;
        public const int TYPE_DAILY_TASK_DWELL_TIME = 1050;
        public const int TYPE_PURCHASE_COIN = 1043;
        public const int TYPE_SEND_GIFT = 1040;
        public const int TYPE_RECEIVED_GIFT = 1052;
        public const int TYPE_SHARE_TO_SOCIAL_MEDIA = 1051;
        public const int TYPE_FOLLOW_UNFOLLOW_USER = 1063;
        public const int TYPE_UPDATE_FOLLOW_UNFOLLOW_USER = 1064;
        
        //public const int TYPE_UPDATE_ACCEPT_OR_DECLINE_REFERRAL_REQUEST = 1039; // 12-11-2016
        /*notification max ut*/

        /* Start Live Stream */
        public const int TYPE_INITIALIZE_LIVE_STREAM = 2000;
        public const int TYPE_START_LIVE_STREAM = 2001;
        public const int TYPE_END_LIVE_STREAM = 2002;
        public const int TYPE_GET_LIVE_STREAMING_DETAILS = 2003;
        public const int TYPE_GET_FEATURED_LIVE_STREAMS = 2004;
        public const int TYPE_GET_RECENT_STREAMS = 2005;
        public const int TYPE_UPDATE_LIVE_STREAM = 2006;
        public const int TYPE_GET_STREAM_CATEGORY_LIST = 2007;
        public const int TYPE_GET_MOST_VIEWED_STREAMS = 2008;
        public const int TYPE_GET_NEAREST_STREAMS = 2009;
        public const int TYPE_ACTION_SEARCH_LIVE_STREAMS = 2010;
        public const int TYPE_ACTION_LIVE_STREAMING_USER_DETAILS = 2011;
        public const int TYPE_UPDATE_STREAMING_LIKE_COUNT = 2012;
        public const int TYPE_GET_FOLLOWING_STREAMS = 2013;
        public const int TYPE_GET_CATEGORY_WISE_STREAM_COUNT = 2014;

        /* End Live Stream */

        /* Start Channel */

        public const int TYPE_CREATE_CHANNEL = 2015;
        public const int TYPE_GET_CHANNEL_CATEGORY_LIST = 2016;
        public const int TYPE_UPDATE_CHANNEL_INFO = 2017;
        public const int TYPE_ACTIVATE_DEACTIVATE_CHANNEL = 2018;
        public const int TYPE_GET_FOLLOWING_CHANNEL_LIST = 2019;
        public const int TYPE_GET_MOST_VIEWED_CHANNEL_LIST = 2020;
        public const int TYPE_GET_OWN_CHANNEL_LIST = 2021;
        public const int TYPE_SUBSCRIBE_UNSUBSCRIBE_CHANNEL = 2022;
        public const int TYPE_GET_CHANNEL_DETAILS = 2023;
        public const int TYPE_ADD_CHANNEL_PROFILE_IMAGE = 2024;
        public const int TYPE_ADD_CHANNEL_COVER_IMAGE = 2025;
        public const int TYPE_ADD_UPLOADED_CHANNEL_MEDIA = 2026;
        public const int TYPE_GET_UPLOADED_CHANNEL_MEDIA = 2027;
        public const int TYPE_GET_CHANNEL_MEDIA_LIST = 2028;
        public const int TYPE_UPDATE_CHANNEL_MEDIA_STATUS = 2029;
        public const int TYPE_GET_FEATURED_CHANNEL_LIST = 2030;
        public const int TYPE_UPDATE_CHANNEL_MEDIA_INFO = 2031;
        public const int TYPE_ACTION_SEARCH_CHANNEL_LIST = 2032;
        public const int TYPE_GET_CHANNEL_PLAYLIST = 2033;
        public const int TYPE_GET_CHANNEL_MEDIA_ITEM = 2034;
        public const int TYPE_DELETE_UPLOADED_CHANNEL_MEDIA = 2039;

        /* End Channel */

        public static long NOTIFICATION_MAX_UT = 0;
        public static long NOTIFICATION_MIN_UT = 0;
        public static long NOTIFICATION_LAST_MAX_UT = 0;

        public static bool NOTIFICATION_ALL_FETCHED_FROM_DB = false;

        public static int CALL_NOTIFICATION_COUNT = 0;
        public static int CHAT_NOTIFICATION_COUNT = 0;
        public static int ALL_NOTIFICATION_COUNT = 0;
        public static int ADD_FRIEND_NOTIFICATION_COUNT = 0;

        //Device Platform Constants
        public const int PLATFORM_DESKTOP = 1;
        public const int PLATFORM_ANDROID = 2;
        public const int PLATFORM_IPHONE = 3;
        public const int PLATFORM_WINDOWS = 4;
        public const int PLATFORM_WEB = 5;

        //Device Categoryy
        public const int CATEGORY_PC = 1;
        public const int CATEGORY_MOBILE = 3;
        public const int CATEGORY_WEB = 5;

        /*friend feed activitys*/
        public const short NEWSFEED_LIKED = 1;
        public const short NEWSFEED_COMMENTED = 2;
        public const short NEWSFEED_SHARED = 3;

        /*streaming*/
        public const int AUDIO_CHANNEL = 1;
        public const int VIDEO_CHANNEL = 2;
        public const int TV_CHANNEL = 3;
        public const int LIVE_STREAM = 4;

        /* TIME INTERVALS */
        public const long TEN_MINUTES = 600000; // 5 minutes
        public const long FIVE_MINUTES = 300000; // 5 minutes

        //Validity
        public const int DEFAULT_VALIDITY = 0; // represents unlimited
        public const int VALIDITY_ONE_DAY = 1;
        public const string UNLIMITED_VALIDITY_STRING = "Unlimited";

        //Default Thumb

        public const int DEFAULT_THUMB_HEIGHT = 150;
        public const int DEFAULT_THUMB_WIDTH = 150;

        public const int EDIT_WALL_TYPE = -100;

    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

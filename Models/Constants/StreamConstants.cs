﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Constants
{
    public static class StreamConstants
    {
        public const int CHANNEL_IN = 1;
        public const int CHANNEL_OUT = 2;

        public const int MICROPHONE_SPEAKER_MIX = 0;
        public const int MICROPHONE = 1;
        public const int SPEAKER = 2;

        public const int MONITOR_SCREEN = 1;
        public const int WEB_CAMERA = 2;
        public const int APP_SCREEN = 3;

        public const int FRAME_WIDTH = 640;
        public const int FRAME_HEIGHT = 480;

        public const int STREAM_TYPE_NEARBY = 1;
        public const int STREAM_TYPE_COUNTRY = 2;
        public const int STREAM_TYPE_FOLLOWING = 4;

        public const int STREAM_SEARCH_TYPE_ALL = 1;
        public const int STREAM_SEARCH_TYPE_COUNTRY = 2;

        public const int STREAM_RECENT_CATEGORY_BY_ALL = 1;
        public const int STREAM_RECENT_CATEGORY_BY_PAGINATION = 2;

        public const int STREAM_BACK_TO_HOME = 1;
        public const int STREAM_BACK_TO_SEARCH = 2;
        public const int STREAM_BACK_TO_COUNTRY = 3;

        public const int STREAM_TOP_SCROLL = 1;
        public const int STREAM_BOTTOM_SCROLL = 2;

        public static string STREAM_GIFT_EXTENSION_PNG = ".png";
        public static string STREAM_GIFT_EXTENSION_GIF = ".gif";

        public const int STREAM_CALL_INCOMING = 1;
        public const int STREAM_CALL_OUTGOING = 2;
        public const int STREAM_CALL_CONNECTED = 3;
        public const int STREAM_CALL_DISCONNECTED = 0;

        public const int STREAMING_FINISHED = 0;
        public const int STREAMING_CONNECTING = 1;
        public const int STREAMING_RECONNECTING = 2;
        public const int STREAMING_CONNECTED = 3;
        public const int STREAMING_POOR_NERWORK = 4;
        public const int STREAMING_INTERRUPTED = 5;
        public const int STREAMING_NO_DATA = 6;


        public static int PLAIN_MESSAGE = 2;
        public static int GIFT_MESSAGE = 21;
        public static int LIKE_MESSAGE = 22;
        public static int FOLLOW_MESSAGE = 23;
        public static int SHARE_MESSAGE = 24;

    }
}
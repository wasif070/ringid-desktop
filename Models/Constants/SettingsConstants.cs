﻿
namespace Models.Constants
{
    public class SettingsConstants
    {
        public const double FORCE_FAVOURITE_VALUE = 10000000.00;
        public const long MILISECONDS_IN_DAY = 86400000;

        //settings constants
        public static long VALUE_RINGID_USERINFO_UT = -1;
        public static long VALUE_RINGID_CONTACT_UT = -1;
        public static long VALUE_RINGID_CIRCLE_UT = 0;
        public static long VALUE_RINGID_CIRCLE_MEMBER_UT = 0;
        public static long VALUE_RINGID_GROUP_UT = 0;
        public static long VALUE_RINGID_BLOCK_UNBLOCK_UT = 0;
        public static long VALUE_RINGID_CALL_LOG_UT = -1;
        public static long VALUE_RINGID_SMS_LOG_UT = -1;
        public static long VALUE_RINGID_LAST_LOGIN_TIME = -1;
        public static int VALUE_RINGID_RECORDER_DEVICE_NUMBER = -1;
        public static float VALUE_RINGID_SPEAKER_VOLUME = 1.0F;
        public static int VALUE_RINGID_PLAYER_DEVICE_NUMBER = -1;
        public static string VALUE_RINGID_WEBCAM_DEVICE_ID = string.Empty;
        public static bool VALUE_RINGID_ALERT_SOUND = true;
        public static bool VALUE_RINGID_IM_SOUND = true;
        public static bool VALUE_RINGID_SEPARATE_CHAT_VIEW = false;
        public static bool VALUE_RINGID_STICKER_VIEW = true;
        public static bool VALUE_RINGID_IM_NOTIFICATION = true;
        public static bool VALUE_RINGID_IM_POPUP = true;
        public static bool VALUE_RINGID_IM_AUTO_DOWNLOAD = true;
        public static int VALUE_RINGID_NEWS_FEED_EXPIRY = AppConstants.DEFAULT_VALIDITY;
        //public static bool VALUE_RINGID_IS_CHAT_LOG_SYNC = false;
        public static int VALUE_RINGID_CHAT_LOG_OFFSET = 0;
        public static bool VALUE_RINGID_IS_NO_MORE_CHAT_LOG = false;
        public static string VALUE_WALLET_PIN_NUMBER = string.Empty;
        public static int VALUE_WALLET_DWELLING_TIME_IN_MINUTES = 0;
        public static string VALUE_WALLET_DWELLING_DATE = "1970-01-01";

        public const string KEY_RINGID_USERINFO_UT = "VALUE_RINGID_USERINFO_UT";
        public const string KEY_RINGID_CONTACT_UT = "VALUE_RINGID_CONTACT_UT";
        public const string KEY_RINGID_CIRCLE_UT = "VALUE_RINGID_CIRCLE_UT";
        public const string KEY_RINGID_CIRCLE_MEMBER_UT = "VALUE_RINGID_CIRCLE_MEMBER_UT";
        public const string KEY_RINGID_GROUP_UT = "VALUE_RINGID_GROUP_UT";
        public const string KEY_RINGID_BLOCK_UNBLOCK_UT = "VALUE_RINGID_BLOCK_UNBLOCK_UT";
        public const string KEY_RINGID_CALL_LOG_UT = "VALUE_RINGID_CALL_LOG_UT";
        public const string KEY_RINGID_SMS_LOG_UT = "VALUE_RINGID_SMS_LOG_UT";
        public const string KEY_RINGID_LAST_LOGIN_TIME = "VALUE_RINGID_LAST_LOGIN_TIME";
        public const string KEY_RINGID_RECORDER_DEVICE_NUMBER = "VALUE_RINGID_RECORDER_DEVICE_NUMBER";
        public const string KEY_RINGID_SPEAKER_VOLUME = "VALUE_RINGID_SPEAKER_VOLUME";
        public const string KEY_RINGID_PLAYER_DEVICE_NUMBER = "VALUE_RINGID_PLAYER_DEVICE_NUMBER";
        public const string KEY_RINGID_WEBCAM_DEVICE_ID = "VALUE_RINGID_WEBCAM_DEVICE_ID";
        public const string KEY_RINGID_ALERT_SOUND = "VALUE_RINGID_ALERT_SOUND";
        public const string KEY_RINGID_IM_SOUND = "VALUE_RINGID_IM_SOUND";
        public const string KEY_RINGID_SEPARATE_CHAT_VIEW = "VALUE_RINGID_SEPARATE_CHAT_VIEW";
        public const string KEY_RINGID_STICKER_VIEW = "VALUE_RINGID_STICKER_VIEW";
        public const string KEY_RINGID_IM_NOTIFICATION = "VALUE_RINGID_IM_NOTIFICATION";
        public const string KEY_RINGID_IM_POPUP = "VALUE_RINGID_IM_POPUP";
        public const string KEY_RINGID_IM_AUTO_DOWNLOAD = "VALUE_RINGID_IM_AUTO_DOWNLOAD";
        public const string KEY_RINGID_NEWS_FEED_EXPIRY = "VALUE_RINGID_NEWS_FEED_EXPIRY";
        public const string KEY_RINGID_IS_CHAT_LOG_SYNC = "VALUE_RINGID_IS_CHAT_LOG_SYNC";
        public const string KEY_WALLET_PIN_NUMBER = "VALUE_WALLET_PIN_NUMBER";
        public const string KEY_RINGID_CHAT_LOG_OFFSET = "VALUE_RINGID_CHAT_LOG_OFFSET";
        public const string KEY_RINGID_IS_NO_MORE_CHAT_LOG = "VALUE_RINGID_IS_NO_MORE_CHAT_LOG";
        public const string KEY_WALLET_DWELLING_TIME_IN_MINUTES = "VALUE_WALLET_DWELLING_TIME_IN_MINUTES";
        public const string KEY_WALLET_DWELLING_DATE = "VALUE_WALLET_DWELLING_DATE";

        //public static long VALUE_RINGID_USERINFO_UT = -1;
        //public static long VALUE_RINGID_CONTACT_UT = -1;
        //public static long VALUE_RINGID_GROUP_UT = 0;
        //public static long VALUE_RINGID_CIRCLE_MEMBER_UT = 0;
        //public static long VALUE_RINGID_CIRCLE_UT = 0;
        //public static long VALUE_RINGID_BLOCK_UNBLOCK_UT = 0;
        //public static long VALUE_RINGID_CALL_LOG_UT = -1;
        //public static long VALUE_RINGID_SMS_LOG_UT = -1;
        //public static long VALUE_RINGID_LAST_LOGIN_TIME = -1;
        //public static int VALUE_RINGID_RECORDER_DEVICE_NUMBER = -1;
        //public static float VALUE_RINGID_SPEAKER_VOLUME = 1.0F;
        //public static int VALUE_RINGID_PLAYER_DEVICE_NUMBER = -1;
        //public static string VALUE_RINGID_WEBCAM_DEVICE_ID = string.Empty;
        //public static bool VALUE_RINGID_ALERT_SOUND = true;
        //public static bool VALUE_RINGID_IM_SOUND = true;
        //public static bool VALUE_RINGID_SEPARATE_CHAT_VIEW = false;
        //public static bool VALUE_RINGID_IM_NOTIFICATION = true;
        //public static bool VALUE_RINGID_IM_POPUP = true;
        //public static bool VALUE_RINGID_IM_AUTO_DOWNLOAD = true;
        //public static int VALUE_RINGID_NEWS_FEED_EXPIRY = -1;
        //public static bool VALUE_RINGID_IS_CHAT_LOG_SYNC = false;



        public const int APP_TYPE_RINGID_FULL = 1;
        public const int APP_TYPE_RINGID_LITE = 2;

        public const int TYPE_FEED_COMMENT = 1;
        public const int TYPE_IMAGE_COMMENT = 2;
        public const int TYPE_MEDIA_COMMENT = 3;

        public const int RINGID_LOGIN = 1;
        public const int MOBILE_LOGIN = 2;
        public const int EMAIL_LOGIN = 3;
        public const int FACEBOOK_LOGIN = 4;
        public const int TWITTER_LOGIN = 5;

        public const int UPLOADTYPE_MUSIC = 5;
        public const int UPLOADTYPE_IMAGE = 1;
        public const int UPLOADTYPE_VIDEO = 6;
        public const int UPLOADTYPE_STICKER = 8;
        public const int UPLOADTYPE_RECORD = 9;

        //public const int SUPERTYPE_GENERAL = 1;
        //public const int SUPERTYPE_CELEBRITY = 2;
        //public const int SUPERTYPE_NEWSPORTAL = 3;
        //public const int SUPERTYPE_PAGE = 4;
        //public const int SUPERTYPE_MUSICPAGE = 5;
        //public const int SUPERTYPE_CIRCLE = 6;

        public const short MEDIA_FEED_TYPE_ALL = 0;
        public const short MEDIA_FEED_TYPE_TRENDING = 1;

        public const int TYPE_NORMAL_BOOK_IMAGE = 1;
        public const int TYPE_PROFILE_IMAGE = 2;
        public const int TYPE_COVER_IMAGE = 3;
        public const int TYPE_GROUP_IMAGE = 99;
        public const int TYPE_IMAGE_IN_COMMENT = -1;

        public const int TYPE_FROM_SIGNUP = 1;
        public const int TYPE_FROM_SIGNIN = 2;
        public const int TYPE_FROM_PROFILE = 3;
        public const int TYPE_FROM_RECOVERY = 4;

        //CHAT CONSTANTS
        public const int WALL_TYPE_OWN = 1001;
        public const int WALL_TYPE_FRIEND = 1005;
        public const int WALL_TYPE_GROUP = 1009;
        public const int WALL_TYPE_MEDIA_PAGE = 1013;
        public const int WALL_TYPE_BUSINESS_PAGE = 1017;
        public const int WALL_TYPE_NEWSPORTAL = 1021;

        //WALL OWNER TYPE
        public const int WALL_OWNER_TYPE_DEFAULT_USER = 1;
        public const int WALL_OWNER_TYPE_SPECIAL_CONTACT = 2;
        public const int WALL_OWNER_TYPE_CELEBRITY = 10;
        public const int WALL_OWNER_TYPE_NEWSPORTAL = 15;
        public const int WALL_OWNER_TYPE_MEDIA_PAGE = 20;
        public const int WALL_OWNER_TYPE_BUSINESS_PAGE = 25;
        public const int WALL_OWNER_TYPE_GROUP = 99;

        public const int FEED_TYPE_TEXT = 1;

        public const int FEED_TYPE_IMAGE = 2;
        public const int FEED_TYPE_AUDIO = 5;
        public const int FEED_TYPE_VIDEO = 6;

        public const int FEED_TYPE_SINGLE_IMAGE = 2;
        public const int FEED_TYPE_SINGLE_IMAGE_WITH_ALBUM = 3;
        public const int FEED_TYPE_MULTIPLE_IMAGE_WITH_ALBUM = 4;
        public const int FEED_TYPE_SINGLE_AUDIO = 5;
        public const int FEED_TYPE_SINGLE_AUDIO_WITH_ALBUM = 6;
        public const int FEED_TYPE_MULTIPLE_AUDIO_WITH_ALBUM = 7;
        public const int FEED_TYPE_SINGLE_VIDEO = 8;
        public const int FEED_TYPE_SINGLE_VIDEO_WITH_ALBUM = 9;
        public const int FEED_TYPE_MULTIPLE_VIDEO_WITH_ALBUM = 10;

        //Media
        public const int MEDIA_SEARCH_TYPE_ALL = 0;
        public const int MEDIA_SEARCH_TYPE_SONGS = 1;
        public const int MEDIA_SEARCH_TYPE_ALBUMS = 2;
        public const int MEDIA_SEARCH_TYPE_TAGS = 3;
        public const int MEDIA_SEARCH_TYPE_TOP = 4;

        public const int PRIVACY_ONLY_ME = 1;
        public const int PRIVACY_SP_FRIEND2SHOW = 5;
        public const int PRIVACY_SP_FRIEND2HIDE = 10;
        public const int PRIVACY_FRIEND = 15;
        public const int PRIVACY_FOF = 20;
        public const int PRIVACY_PUBLIC = 25;


        public const int MEDIA_TYPE_AUDIO = 1;
        public const int MEDIA_TYPE_VIDEO = 2;
        public const int MEDIA_TYPE_IMAGE = 3;

        public const int TYPE_STREAM_AND_CHANNEL = 0;
        public const int TYPE_STREAM = 1;
        public const int TYPE_CHANNEL = 2;

        //Special Contact
        public const int SPECIAL_CONTACT = 3;

        //Spcecial Feed
        public const int GENERAL_FEED = 0;
        public const int SPECIAL_FEED = 1;
        public const int NEWSPORTAL_FEED = 3;
        // Recently Added
        public static int RECENT_FRND_UNREAD = 0;
        public static int RECENT_FRND_READ = 1;

        public const int PROFILE_TYPE_GENERAL = 1;  //all or my or frnd
        public const int PROFILE_TYPE_SPECIAL_CONTACT = 2;  //all or my or frnd
        public const int PROFILE_TYPE_CELEBRITY = 10;
        public const int PROFILE_TYPE_NEWSPORTAL = 15;
        public const int PROFILE_TYPE_PAGES = 25;
        public const int PROFILE_TYPE_MUSICPAGE = 20;
        public const int PROFILE_TYPE_CIRCLE = 99;

        //public const int PAGECATEGORY_TYPE_NEWSPORTAL = 3;
        //public const int PAGECATEGORY_TYPE_PAGES = 4;
        //public const int PAGECATEGORY_TYPE_MUSICPAGE = 5;
        //public const int PAGECATEGORY_TYPE_CELEBRITY = 6;

        public const int CHANNEL_TYPE_SEARCH = -1;//custom
        public const int CHANNEL_TYPE_BOTH = 0;
        public const int CHANNEL_TYPE_DISCOVER = 1;
        public const int CHANNEL_TYPE_FOLLOWING = 2;

        public const int CELEBRITY_TYPE_DISCOVER = 1;
        public const int CELEBRITY_TYPE_POPULAR = 2;
        public const int CELEBRITY_TYPE_FOLLOWING = 3;

        public const int PORTALFEED_TYPE_IMAGE = 1;
        public const int PORTALFEED_TYPE_TEXT = 2;
        //public const int PORTALFEED_TYPE_MULTIPLEIMAGE = 3;
        //public const int PORTALFEED_TYPE_AUDIO = 5;
        public const int PORTALFEED_TYPE_MEDIA = 6;

        public const int FEED_FIRSTTIME = 1;
        public const int FEED_ADDORUPDATE = 2;
        public const int FEED_REGULAR = 0;

        public const int RESPONSE_SUCCESS = 1;
        public const int RESPONSE_NOTSUCCESS = 2;
        public const int NO_RESPONSE = 0;

        //Download Image Type
        public const int IMAGE_TYPE_PROFILE = 1;
        public const int IMAGE_TYPE_COVER = 2;
        public const int IMAGE_TYPE_CHAT_PROFILE = 3;
        public const int IMAGE_TYPE_ROOM = 4;
        public const int IMAGE_TYPE_ROOM_MEMBER = 5;


        public const int ROOT_CONTENT_TYPE_FEED = 1001;
        public const int ROOT_CONTENT_TYPE_ALBUM = 1002;

        public const int ACTIVITY_ON_STATUS = 1;
        public const int ACTIVITY_ON_IMAGE = 2;
        public const int ACTIVITY_ON_MULTIMEDIA = 3;
        public const int ACTIVITY_ON_ALBUMS = 4;

        public const int IMAGE_TYPE_STREAM = 6;
        public const int IMAGE_TYPE_CHANNEL = 7;

        public const int FEED_TYPE_STORAGE_FEED = -1;
        public const int FEED_TYPE_NEW_STATUS = 0;
        public const int FEED_TYPE_LOADER_ADDED_BUT_INVISIBLE = 1;
        public const int FEED_TYPE_NORMAL_FEED = 2;
        public const int FEED_TYPE_LOADER_VISIBLE_NO_MORE_TEXT_PANEL_INVISIBLE = 3;
        public const int FEED_TYPE_NO_MORE_TEXT_PANEL_VISIBLE_LOADER_INVISIBLE = 4;
        public const int FEED_TYPE_RELOAD_BUTTON_VISIBLE = 5;


        public const int FEED_FULL_VIEW = 2;
        public const int FEED_PARTIAL_VIEW = 1;
        public const int FEED_BLANK_VIEW = 0;

        public const int MAX_BANK_FEEDS = 2;

    }
}

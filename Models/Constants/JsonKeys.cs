<<<<<<< HEAD
﻿using System;

namespace Models.Constants
{
    public class JsonKeys
    {
        //user and serrion
        public const String UserIdentity = "uId";
        public const String SessionId = "sId";

        public const String LoginType = "lt";
        public const String RingId = "ringID";
        public const String InputToken = "it";
        public const String SocialMediaId = "smid";
        public const String SocialMediaType = "smt";

        //index of headers
        public const String IndexOfHeaders = "inOH";
        public const String FullName = "fn";
        public const String LastName = "ln";
        public const String Gender = "gr";
        public const String Country = "cnty";
        public const String CountryDiscover = "cntr";
        public const String MobilePhone = "mbl";
        public const String DialingCode = "mblDc";//"mblDc";
        public const String Email = "el";
        public const String Presence = "psnc";  // 1 = offline, 2 = online, 3 = away
        public const String Mood = "mood"; // ALIVE = 1; DONT_DISTURB = 2; BUSY = 3; INVISIBLE = 4;
        public const String WhatIsInYourMind = "wim";
        public const String FriendshipStatus = "frnS";
        public const String OldPassword = "oPw";
        public const String NewPassword = "nPw";
        public const String ProfileImage = "prIm";
        public const String CoverImage = "cIm";
        public const String CropImageX = "cimX";
        public const String CropImageY = "cimY";
        public const String ProfileImageId = "prImId";
        public const String CoverImageId = "cImId";
        public const String BirthDay = "bDay";
        public const String BirthMonth = "bMnth";
        public const String BirthYear = "bYr";
        public const String RingEmail = "re";
        public const String UpdateTime = "ut";
        public const String ContactUpdateTime = "cut";
        public const String ContactUpdate = "cu";
        public const String GroupMemberUpdateTime = "gpmut";
        public const String IntegerStatus = "ists";
        public const String Deleted = "del";
        public const String TotalFriend = "tf";
        public const String NumberOfMutualFriend = "nmf";
        public const String UserTableID = "utId";
        public const String UserTableIDs = "utIDs";
        public const String UpdateOnly = "uo";
        public const String ContactType = "ct";
        public const String BlockValue = "bv";
        //public const String NewContactType = "nct";
        //public const String IsChangeRequested = "iscr";
        public const String Accept = "acpt";
        public const string Contacts = "contacts";
        public const string Count = "count";

        public const String NotificationValidity = "nvldt";
        public const String CurrentCity = "cc";
        public const String HomeCity = "hc";
        public const String MarriageDay = "mDay";
        public const String MarriageMonth = "mMnth";
        public const String MarriageYear = "mYr";
        public const String AboutMe = "am";

        public const String CommonFriendSuggestion = "cfs";
        public const String PhoneNumberSuggestion = "pns";
        public const String ContactListSuggestion = "cls";

        //Work and Education Parameters 
        public const String WorkList = "workList";
        public const String EducationList = "educationList";
        public const String SkillList = "skillList";
        public const string WorkId = "wId";
        public const String WorkObj = "wrk";
        public const String CompanyName = "cnm";
        public const String Position = "pstn";
        public const String Description = "desc";
        public const String City = "ct";
        public const String FromTime = "ft";
        public const String ToTime = "tt";
        public const string SchoolId = "schlId";
        public const String SchoolName = "scl";
        public const String AttendedFor = "af";
        public const String Degree = "dgr";
        public const String Graduated = "grtd";
        public const String IsSchool = "iss";
        public const String Concentration = "cntn";
        public const String EducationObj = "edu";
        public const string SkillId = "skillId";
        public const String Skill = "skl";
        public const String SkillObj = "skill";
        public const string UUID = "uuId";
        public const string UuID = "uuid";

        public const String Name = "nm";
        //public const String AdditionaInfo = "adInfo";
        //public const String NumberOrEmail = "noe";
        //public const String IsVerified = "ivf";
        public const String IsMyNumberVerified = "imnv";
        public const String IsEmailVerified = "iev";
        public const String WebLoginEnabled = "wle";
        public const String PCLoginEnabled = "ple";
        public const String MobileOrEmail = "mblOrEml";
        public const String SettingsName = "sn";
        public const String SettingsValue = "sv";
        public const String RecoverBy = "rb";
        public const String Suggestions = "sgtns";

        public const String NotificationType = "nt";
        public const String NotificationMessageType = "mt";
        public const String ActionId = "acId";
        public const String FriendName = "fndN";
        public const String DeleteNotificationIds = "ntIDLst";//"ids";
        public const String TotalNotificationCount = "tn";
        public const String NotificationScrollPosition = "scl";
        public const String LikeOrComment = "loc";
        public const String NotificationList = "nList";
        //app globals
        public const String ChatServerIp = "chIp"; //chatServerIp
        public const String SmsServerIp = "smIp"; //smsServerIp
        public const String SmsServerPort = "smP"; //smsServerPort
        public const String MailServerIp = "ms";
        public const String MailServerPort = "mp";
        public const String Balance = "blc"; //balance
        public const String ChatRegistrationPort = "chRp"; //registrationPort
        public const String ConfirmationPort = "chCp"; //confirmationPort
        public const String ChatHistoryPort = "chHp"; //chatHistoryPort
        public const String FriendChatPort = "fChp"; //friendChatPort
        public const String GroupChatPort = "gChp"; //groupChatPort
        public const String LastOnlineTime = "lot";

        public const String MailIp = "mailIP";
        public const String SmtpPort = "smtpPrt";
        public const String ImapPort = "imapPrt";
        public const String ImageServerIp = "imgIP";
        public const String MarketServerIp = "mrktIP";
        public const String OfflineServerIp = "oIP";
        public const String OfflineServerPrt = "oPrt";

        public const String OriginalFeed = "orgFd";
        public const String WhoShareList = "whShrLst";
        //public const String WhoShare = "whoShare";
        public const String NumberOfHeaders = "nOfHd"; //noOfHeaders
        public const String FriendId = "fndId"; //friendId
        public const String FutId = "futId"; //futId
        public const String FutIds = "futIds"; //futIds
        public const String FriendFirstName = "ffn"; //FriendFirstName
        //public const String FriendLastName = "fln"; //FriendLastName
        public const String FriendProfileImage = "fprIm";
        public const String FriendProfileImagePrivacy = "fprImPr";
        public const String GuestId = "gstId";
        public const String WallOwnerId = "wOnrId";
        public const string OwnerId = "onrId";
        public const String SpecificFriendsToShowOrHide = "spcFrnds";

        public const String UserName = "usrNm"; //userName
        public const String Password = "usrPw"; //password
        public const String Version = "vsn"; //version
        public const String SearchParam = "schPm"; //searchParam
        public const String SearchCategory = "scat"; //SearchCategory
        public const String Category = "cat";
        public const String SearchedContacList = "searchedcontaclist";
        public const String Limit = "lmt";
        public const String VersionMessage = "vsnMg"; //versionMessage
        public const String DownloadMandatory = "dwnMnd"; //downloadMandatory
        public const String Device = "dvc"; //device
        public const String DeviceCategory = "dvcc"; //device category
        public const String DeviceId = "did"; //Unique Device ID (UDID)
        public const String DeviceToken = "dt"; //winUri
        public const String IsPickedMobileNumber = "ispc"; //is my number is picked by application? 1:0;
        public const String IsPickedEmail = "isepc";
        public const String VerificationCode = "vc"; //former passcode
        public const String EmailVerificationCode = "evc";
        public const String FnfHeaderVersion = "fnfHd"; //fnfHeaderVersion
        public const String Type = "type"; //type
        public const String Success = "sucs"; //success
        public const String IsSuccess = "success";
        public const String Message = "mg"; //message
        public const String MSG = "msg"; //message
        public const String Action = "actn"; //action
        public const String TempId = "tmpId"; //tempId
        public const String PacketId = "pckId"; //packetId
        public const String Sequence = "seq"; //seq
        public const String PacketIdFromServer = "pckFs"; //packetIdFromServer
        public const String PacketType = "pt"; //packetType
        public const String MessageDate = "mDate"; //messageDate    
        public const String PushUrl = "wUrl";//push notification uri
        public const String Url = "url";//push notification uri
        public const String TotalRecords = "tr";//totalRecords
        //public const String Latitude = "la";
        //public const String Longitude = "lo";
        public const String Timeout = "tout";
        public const String LastStatus = "lsts";
        public const String UserFound = "uf";
        public const String MutualFriendIDs = "mfIDs";
        public const String ReasonCode = "rc";
        public const String Tag = "tg";
        public const String TagId = "tid";
        public const String TagOwnerId = "twid";
        public const String TagName = "tgn";
        public const String NewTag = "ntg";
        public const String FriendIds = "fids";
        public const String TagList = "tagList";
        public const String TagUpdateTime = "tut";
        public const String FriendList = "frndList";
        public const String Friends = "frnds";
        public const String UIDs = "uIds";

        //groups
        public const String GroupId = "grpId";
        public const String UpdateBalance = "uBl";
        //public const String DialingCode = "dCD";
        public const String EmoticonUrl = "emoUrl";
        public const String GroupName = "gNm";
        public const String SuperAdmin = "sAd";
        public const String MemberIds = "mIds";
        public const String OldMemberIds = "oMIds";
        public const String NewMemberIds = "nMIds";
        public const String Admin = "admin"; // unchanged

        //reset password
        public const String SecretCode = "secCo";
        public const String NewPasswordRe = "nwPw"; //newPassword, used for resetting password (WTH!)

        //sms
        public const String StatusCode = "sCode";
        public const String LogStartLimit = "st";



        //privacy
        public const String Privacy = "pvc";
        public const String EmailPrivacy = "ePr";
        public const String MobilePhonePrivacy = "mblPhPr";
        public const String ProfileImagePrivacy = "prImPr";
        public const String BirthdayPrivacy = "bdPr";
        public const String CoverImagePrivacy = "cImPr";

        //emoticons
        public const String EmoticonVersion = "emVsn";

        public const String StartLimit = "st";
        public const String PostLimit = "lmt";

        //book
        public const String PostFeed = "pst";
        public const String ImageId = "imgId";
        public const String ImageIds = "imgIds";
        public const String ImageHeight = "ih";
        public const String ImageWidth = "iw";
        public const String ImageUrl = "iurl";
        public const String ImageCaption = "cptn";
        public const String ImageType = "imT";
        public const String Serial = "sl";
        public const String AlbumId = "albId";
        public const String AlbumName = "albn";
        public const String ImageAlbumType = "albT";
        public const String CoverImageUrl = "cvrURL";
        public const String isSameAlbum = "issa";
        public const String NumberOfComments = "nc";
        public const String NumberOfLikes = "nl";
        public const String NumberOfShares = "ns";
        public const String Time = "tm";
        public const String ActualTime = "at";
        public const String Id = "id";
        public const String BookPostType = "type";
        public const String Status = "sts";
        public const String NewsfeedId = "nfId";
        public const String SharedFeedId = "sfId";
        public const String ShareFeedOwnerUtId = "sfwOnrId";
        public const String ILike = "il";
        public const String IComment = "ic";
        public const String IShare = "is";
        public const String ImageDetails = "imgDtls";
        public const String Liked = "lkd";
        public const String Comment = "cmn";
        public const String CommentId = "cmnId";
        public const String AlbumCoverImage = "cvImg";
        public const String ILikeComment = "il";
        public const String TotalLikeComment = "tl";
        public const String Location = "lctn";
        public const String Latitude = "lat";
        public const String Longitude = "lng";
        public const String LocationDetails = "loctn";

        public const String Scroll = "scl";
        public const String TotalImage = "tim"; //image uploaded in a post
        public const String TotalShare = "ts";
        public const String ImageInCollection = "imc"; //total album image
        public const String Validity = "vldt"; //for newsfeed
        public const String TotalImagesInAlbum = "timg";//received with profile, cover collections for ImageViewer 
        public const String ImageAlbumList = "albumList";
        public const String ImageAlbumId = "albId";
        public const String Activity = "actvt";
        public const String ActivityType = "actT";
        public const String ActivistId = "auId";
        public const String ActivistFirstName = "afn";
        //public const String ActivistLastName = "aln";

        public const String StickerId = "stkId";
        public const String StickerBackgroundImage = "bkImgQ";
        public const String StickerCategoryName = "ctgNm";
        public const String StickerCategoryId = "ctgId";

        // for sticker market
        public const String StickerCatId = "sCtId";
        public const String StickerCatName = "sctName";
        public const String CollectionId = "sClId";
        public const String CategoryBannerImage = "cgBnrImg";
        public const String Rank = "rnk";
        public const String DetailImage = "dtlImg";
        public const String CategoryNew = "cgNw";
        public const String Icon = "icn";
        public const String Price = "prz";
        public const String Free = "free";
        public const String StickerDescription = "dcpn";
        public const String DefaultItem = "defaultItem";

        public const String Shares = "shares";
        public const String CollectionName = "name";
        //public const String CollectionTitle = "title";
        public const String CollectionBannerImage = "bnrImg";
        public const String ThemeColor = "thmColor";
        public const String TextColor = "txtColor";

        public const String StickerImageId = "imId";
        public const String StickerImageUrl = "imUrl";

        public const String WindowsPhoneNotificationUri = "winURI";
        public const String PushNotificationEnabled = "pen";

        public const String AuthServer = "ac";
        public const String ImageServer = "ims";
        public const String ImageServerResource = "imsres";
        public const String StickerServer = "stmapi";
        public const String StickerServerResource = "stmres";
        public const String VODServer = "vodapi";
        public const String VODServerResource = "vodres";

        public const String AuthServerIP = "authServerIP";
        public const String AuthServerPort = "authPort";
        public const String ComPort = "comPort";
        public const String CallForwardText = "cft";
        public const String UserDetails = "userDetails";
        public const String ContactList = "contactList";

        public const String AppType = "apt";
        public const String MatchedBy = "mb";

        public const string FeedAccess = "fda";  //Feed Access
        public const string CallAccess = "cla";  //Call Access
        public const string ChatAccess = "chta"; //Chat Access
        public const string AnonymousCallAccess = "anc";  //Anonymous Call Access
        public const string AnonymousChatAccess = "ancht";  //Anonymous Chat Access

        public const string ContactUtIdList = "idList";
        public const string RID = "rid";
        public const string FriendDTO = "fndDTO";

        public const string ContactIds = "contactIds";
        public const string NewsFeedList = "newsFeedList";
        public const string NewsFeed = "newsFeed";
        public const string ShowContinue = "sc";
        public const string Likes = "likes";
        public const string Comments = "comments";
        public const string CommentType = "cmntT";
        public const string CommentDTO = "cmntDTO";
        public const string GroupList = "groupList";
        public const string MemberOrMediaCount = "mc";
        public const string GroupMembers = "groupMembers";
        public const string RemovedMembers = "removedMembers";

        public const string LinkDetails = "lnk";
        public const string LinkTitle = "lnkTtl";
        public const string LinkURL = "lnkURL";
        public const string LinkDescription = "lnkDsc";
        public const string LinkImageURL = "lnlImgURL";
        public const string LinkDomain = "lnkDmn";
        public const string LinkType = "lnkT";

        public const string TotalTaggedFriends = "tfc";
        public const string TaggedFriendUtIds = "tFrndIds";
        public const string RemovedTagsFriendUtIds = "rTFrndIds";
        public const string FriendsTagList = "fTgLst";

        public const string DoingList = "mdLst";
        public const string DoingActivityIds = "fActvtIds";
        public const string MoodIds = "mdIds";
        public const string DoingSort = "wgt";

        public const string ContentId = "cntntId";
        public const string Title = "ttl";
        public const string Artist = "artst";
        public const string StreamUrl = "strmURL";
        public const string ThumbUrl = "thmbURL";
        public const string ThumbUrlComment = "thmbUrl";
        public const string ThumbImageHeight = "tih";
        public const string ThumbImageWidth = "tiw";
        public const string MediaDuration = "drtn";
        public const string MediaType = "mdaT";
        public const string MediaList = "mdaLst";
        public const string MediaIds = "mdaIds";
        public const string MediaContentDTO = "mdaCntntDTO";
        public const string MediaContentList = "mdaCntntLst";
        public const string MediaAlbumImageURL = "imgURL";
        public const string MediaAlbumList = "mediaAlbumList";

        public const string AccessCount = "ac";
        public const string LikeCount = "lc";
        public const string CommentCount = "cc";

        public const string Facebook = "fb";
        public const string Twitter = "twtr";


        public const string FacebookValidated = "fbvld";
        public const string TwitterValidated = "twtrvld";


        public const string StatusTags = "stsTags";

        public const string MembershipType = "mt";

        //call keys
        public const String SwitchIp = "swIp"; //switchIp
        public const String SwitchPort = "swPr"; //switchPort
        public const string P2PCall = "p2p";
        public const string RemotePushType = "rpt";
        public const string BlockBy = "bb";
        public const string AnonymousBlockBy = "abb";
        public const string Duration = "dunt"; //duration
        public const string CallId = "callID"; //callId
        public const string TCall = "tCall"; //callId
        public const string ConnectedWith = "cw"; //ConnectedWith
        public const String IsDivertedCall = "idc"; //IsDivertedCall
        public const string CallType = "calT";
        //call log
        public const string CallLog = "callLog";
        public const string CallDuration = "calD";
        public const string CallingTime = "caTm";
        public const string PhoneNo = "phN";
        //Media
        public const string MediaSuggestion = "sgstn";
        public const string MediaHashTagId = "ukey";
        public const string MediaSuggestionType = "sugt";
        public const string MediaSearchKey = "sk";
        public const string HashTagList = "htgList";
        public const string HValue = "val";
        public const string PivotId = "pvtid";
        public const string MediaItemsCount = "mdc";
        public const string HashTagName = "htn";
        public const string HashTagId = "htid";
        public const string MediaPrivacy = "mpvc";
        public const string FeedPrivacy = "fpvc";
        public const string SubType = "stp";
        //public const string SuperType = "spType";
        public const string WallType = "wType";
        public const string WallOwnerType = "wOnrT";
        public const string GuestWriterType = "gWtrT";
        public const string FeedCategory = "fc";
        public const string SharingPostContentIds = "cntntIDLst";
        public const string AdminCount = "admnc";
        public const string MemberCategoryCircle = "mcat";
        public const string MediaTrendingFeed = "mtf"; //mtf=1 for media trends , mtf=0 for all media feeds
        //Friend Settings
        public const string AllowIncomingFriendRequest = "aifr";
        public const string AllowFriendsToAddMe = "afam";
        public const string AutoAddFriends = "aaf";

        public const string SpamType = "spmt"; //SpamType(SPAM_USER/SPAM_FEED/SPAM_IMAGE/SPAM_MEDIA_CONTENT)
        public const string SpamReasonList = "rsnLst"; //Reason List
        public const string SpamReason = "rsn"; //Reason
        public const string SpamId = "spmid";
        public const string SpamReasonId = "rsnid";

        public const string IsDigits = "idgt";
        public const string BaseWebURL = "web";

        public const string NewsPortalFeedInfo = "npFeedInfo";
        public const string NewsPortalUrl = "nUrl";
        public const string NewsPortalExUrlOp = "exUrlOp";
        public const string NewsPortalCategoryId = "npCatId";
        public const string NewsCategoryId = "nCatId";
        public const string NewsPortalTitle = "nTtl";
        //public const string NewsPortalDescription = "nDesc";// 
        public const string NewsShortDescription = "nSDctn";
        public const string NewsDescription = "nDctn";
        public const string NewsPortalDTO = "npDTO";
        public const string NewsPortalFeedtype = "npFeedType";
        public const string IsNewsPortal = "isnp";
        public const string NewsPortalCategoryList = "nCatList";
        public const string NewsPortalList = "npList";
        public const string NewsPortalUtid = "npID";
        public const string PageId = "pId";
        public const string PageInfo = "pInfo";
        public const string IsUserFeedHidden = "isufhdn";
        public const string IsHidden = "ishdn";
        public const string NewsPortalCategoryName = "catName";
        public const string Subscribe = "subsc";
        public const string SubscribeType = "subscType";
        public const string SubscriberCount = "subCount";
        public const string SubscriptionTime = "subTime";
        public const string SubscribedIDs = "npSubIds";
        public const string UnsubscribedIDs = "npUnSubIds";
        public const string NewsPortalSlogan = "nPslgn";
        public const string NewsPortalCatName = "nPCatName";
        public const string NewsPortalCatId = "nPCatId";
        public const string IsFeaturedFeed = "isffeed";
        public const string SubUnsubscrbType = "subscType";
        public const string FeedIdsList = "feedIdList";
        public const string IdsList = "IDLst";
        public const string UtIdsList = "utIDLst";
        public const string Hide = "hid";
        public const string Option = "optn";
        public const string FeedIds = "feedIds";
        public const string IsSaved = "svd";
        public const string ProfileType = "pType";
        public const string BreakingNewsFeeds = "brNPFeeds";
        public const string MutualFriendCount = "wmfc";
        public const string NumberOfMutualFriendCount = "mfc";
        //Celebrity
        public const string CelebrityListType = "clt";
        public const string IsFollower = "isFollower";
        public const string OnlineTime = "olt";
        public const string MoodMessage = "mmsg";
        public const string Followercount = "fcount";
        public const string PostCount = "pcount";
        public const string CelebrityCountry = "cntr";
        public const string CelebrityCategoryId = "catId";
        public const string CelebrityCategoryName = "catName";
        public const string IsExcludeRemovedList = "iserl";
        public const string IsSortByPopularity = "issbp";
        public const string CelebrityList = "celList";
        public const string CelebrityInfo = "celebInfo";
        public const string Flw = "flw";
        public const string Follow = "follow";
        public const string CelebrityCatList = "celCatList";
        //Wallet
        public const string WSuccess = "success";
        public const string WUserTableId = "userId";
        public const string WInfo = "info";
        public const string MyCoin = "my_coin";
        public const string BonusCoin = "bonus_coin";
        public const string Quantity = "quantity";
        public const string AvailableQuantity = "available_quantity";
        public const string TotalQuantity = "total_quantity";
        public const string ExchangeRate = "exchangeRate";
        public const string CoinId = "coinId";
        public const string CoinName = "coinName";
        public const string CurrencyISOCode = "currencyISOCode";
        public const string CurrencyName = "currencyName";
        public const string TransactionHistory = "transactionHistory";
        public const string TransactionID = "transaction_id";
        public const string TransactionDate = "transaction_date";
        public const string TransactionTypeID = "transaction_type_id";
        public const string TransactionAmount = "amount";
        public const string OtherUserID = "other_user_id";
        public const string TransactionType = "trT";
        public const string TransactionRemarks = "transaction_Remarks";
        public const string PendingRequests = "pReqs";
        public const string UserRanks = "usrRnk";
        public const string ReferralList = "reflist";
        public const string ReferralSummary = "refSum";
        public const string MyReferralStat = "myRefSts";
        public const string ReferralID = "refId";
        public const string ReferralAcceptedStatus = "accepted";
        public const string ReferralAddedTime = "addTime";
        public const string ReferralGoldMemberCount = "1";
        public const string ReferralSilverMemberCount = "2";
        public const string RequestCoinID = "cnId";
        public const string TransfereeUtID = "toUtId";
        public const string TransactionQuantity = "qty";
        public const string TransactionRemark = "rmk";

        public const string UrlType = "uType";
        public const string CommentTags = "cmntTags";
        public const string IsEdited = "edtd";
        public const string CommentTagList = "cmnTgLst";
        public const string EditTime = "et";
        public const string List = "list";

        public const string AlbumCount = "albc";
        public const string ImageList = "imageList";
        public const string EditedImageList = "edtimglst";
        public const string RemovedImageIds = "rmviids";
        public const string AddedMediaList = "nmlst";
        public const string RemovedMediaIds = "rmvmids";
        public const string EditedMediaList = "edtmlst";
        public const string SavedTime = "stm";

        public const string PivotUUID = "pvtUUID";
        public const string PivotTime = "pvtTm";
        public const string RootContentType = "rtCntntType";
        public const string GuestWriter = "gstWtr";
        public const string WallOwner = "wlOnr";
        public const string LatestLiker = "lkr";
        public const string LatestCommenter = "cmntr";
        public const string LatestSharer = "shrer";
        public const string NpUUID = "npuuid";
        public const string SharedFeedList = "shrdFdLst";
        public const string TotalMediaCount = "mdaC";
        public const string CommenterID = "cmntrId";

        public const string ProfileImageHeight = "prImH";
        public const string ProfileImageWidth = "prImW";
        public const string CoverImageHeight = "cImH";
        public const string CoverImageWidth = "cImW";

        // Live Stream

        public const string StreamId = "streamId";
        public const string StreamCategoryId = "catId";
        public const string StreamCategoryList = "catList";
        public const string StreamLagList = "tagList";
        public const string StreamHashTagId = "hTagID";
        public const string StreamHashTag = "hTag";
        public const string StreamChatON = "chtOn";
        public const string StreamGiftON = "giftOn";
        public const string StreamDetails = "streamDetails";
        public const string StreamFullName = "fn";
        public const string StreamStartTime = "stTm";
        public const string StreamEndTime = "endTm";
        public const string StreamCoinCount = "cnc";
        public const string StreamViewerCount = "vwc";
        public const string StreamFollowingCount = "followingCount";
        public const string StreamFollowerCount = "followerCount";
        public const string StreamChatServerIp = "chatServerIp";
        public const string StreamChatServerPort = "chatServerPort";
        public const string StreamPublisherServerIp = "streamingServerIp";
        public const string StreamPublisherServerPort = "streamingServerPort";
        public const string StreamViewerServerIp = "viewerServerIp";
        public const string StreamViewerServerPort = "viewerServerPort";
        public const string StreamServerDTO = "strmSrvrDto";

        public const string StreamLongitude = "lon";
        public const string StreamSearchParm = "schPm";
        public const string StreamDetailsList = "streamDetailsList";
        public const string StreamCountry = "cntr";
        public const string StreamDistance = "dist";
        public const string StreamIsFollowing = "isFollowing";
        public const string StreamChatOn = "chtOn";
        public const string StreamGiftOn = "giftOn";
        public const string StreamUser = "user";
        public const string StreamScroll = "scl";
        public const string StreamIsFriendOnly = "isFnd";

        //Channel

        public const string ChannelDTO = "cdto";
        public const string ChannelID = "chnlId";
        public const string ChannelOwnerID = "ownerId";
        public const string ChannelCatetoryList = "chnlCats";
        public const string ChannelCatetoryIDs = "chnlCatIds";
        public const string ChannelStatus = "chnlSts";
        public const string ChannelType = "chType";
        public const string ChannelMediaList = "chnlMedia";
        public const string ChannelMediaArtist = "artist";
        public const string ChannelMediaID = "mediaId";
        public const string ChannelMediaType = "mediaType";
        public const string ChannelMediaUrl = "mediaUrl";
        public const string ChannelThumbImageUrl = "thmbImgUrl";
        public const string ChannelThumbImageWidth = "thmbImgW";
        public const string ChannelThumbImageHeight = "thmbImgH";
        public const string ChannelList = "chnlList";
        public const string ChannelSubscrbUnSubscrbValue = "subVal";
        public const string ChannelMediaStatus = "chMSts";
        public const string ChannelMediaIds = "chnlMediaIds";
        public const string ChannelUserType = "userType";
        public const string ChannelMediaStartTime = "stTime";
        public const string ChannelMediaDTO = "cmdto";
        public const string ChannelCreationTime = "ctm";
        public const string IsFromChannel = "isChnl";
        public const string ChannelIP = "chnlIp";
        public const string ChannelPort = "chnlPrt";
        public const string StreamIP = "strmIP";
        public const string StreamPort = "strmPrt";

        public const string AlbumsList = "albLst";
        public const string NtUUID = "uuid";
        public const string Weight = "weight";
        public const string AlbumDetails = "albmDtls";
        public const string TotalAudioOrVideoCount = "tmc";
        public const string Edited = "edt";
        public const string PageTitle = "pttl";

        public const string RequestCancelled = "reqCancelled";
        public const string NoResponseFromServer = "noResponse";
    }
}
=======
﻿using System;

namespace Models.Constants
{
    public class JsonKeys
    {
        //user and serrion
        public const String UserIdentity = "uId";
        public const String SessionId = "sId";

        public const String LoginType = "lt";
        public const String RingId = "ringID";
        public const String InputToken = "it";
        public const String SocialMediaId = "smid";
        public const String SocialMediaType = "smt";

        //index of headers
        public const String IndexOfHeaders = "inOH";
        public const String FullName = "fn";
        public const String LastName = "ln";
        public const String Gender = "gr";
        public const String Country = "cnty";
        public const String CountryDiscover = "cntr";
        public const String MobilePhone = "mbl";
        public const String DialingCode = "mblDc";//"mblDc";
        public const String Email = "el";
        public const String Presence = "psnc";  // 1 = offline, 2 = online, 3 = away
        public const String Mood = "mood"; // ALIVE = 1; DONT_DISTURB = 2; BUSY = 3; INVISIBLE = 4;
        public const String WhatIsInYourMind = "wim";
        public const String FriendshipStatus = "frnS";
        public const String OldPassword = "oPw";
        public const String NewPassword = "nPw";
        public const String ProfileImage = "prIm";
        public const String CoverImage = "cIm";
        public const String CropImageX = "cimX";
        public const String CropImageY = "cimY";
        public const String ProfileImageId = "prImId";
        public const String CoverImageId = "cImId";
        public const String BirthDay = "bDay";
        public const String BirthMonth = "bMnth";
        public const String BirthYear = "bYr";
        public const String RingEmail = "re";
        public const String UpdateTime = "ut";
        public const String ContactUpdateTime = "cut";
        public const String ContactUpdate = "cu";
        public const String GroupMemberUpdateTime = "gpmut";
        public const String IntegerStatus = "ists";
        public const String Deleted = "del";
        public const String TotalFriend = "tf";
        public const String NumberOfMutualFriend = "nmf";
        public const String UserTableID = "utId";
        public const String UserTableIDs = "utIDs";
        public const String UpdateOnly = "uo";
        public const String ContactType = "ct";
        public const String BlockValue = "bv";
        //public const String NewContactType = "nct";
        //public const String IsChangeRequested = "iscr";
        public const String Accept = "acpt";
        public const string Contacts = "contacts";
        public const string Count = "count";

        public const String NotificationValidity = "nvldt";
        public const String CurrentCity = "cc";
        public const String HomeCity = "hc";
        public const String MarriageDay = "mDay";
        public const String MarriageMonth = "mMnth";
        public const String MarriageYear = "mYr";
        public const String AboutMe = "am";

        public const String CommonFriendSuggestion = "cfs";
        public const String PhoneNumberSuggestion = "pns";
        public const String ContactListSuggestion = "cls";

        //Work and Education Parameters 
        public const String WorkList = "workList";
        public const String EducationList = "educationList";
        public const String SkillList = "skillList";
        public const string WorkId = "wId";
        public const String WorkObj = "wrk";
        public const String CompanyName = "cnm";
        public const String Position = "pstn";
        public const String Description = "desc";
        public const String City = "ct";
        public const String FromTime = "ft";
        public const String ToTime = "tt";
        public const string SchoolId = "schlId";
        public const String SchoolName = "scl";
        public const String AttendedFor = "af";
        public const String Degree = "dgr";
        public const String Graduated = "grtd";
        public const String IsSchool = "iss";
        public const String Concentration = "cntn";
        public const String EducationObj = "edu";
        public const string SkillId = "skillId";
        public const String Skill = "skl";
        public const String SkillObj = "skill";
        public const string UUID = "uuId";
        public const string UuID = "uuid";

        public const String Name = "nm";
        //public const String AdditionaInfo = "adInfo";
        //public const String NumberOrEmail = "noe";
        //public const String IsVerified = "ivf";
        public const String IsMyNumberVerified = "imnv";
        public const String IsEmailVerified = "iev";
        public const String WebLoginEnabled = "wle";
        public const String PCLoginEnabled = "ple";
        public const String MobileOrEmail = "mblOrEml";
        public const String SettingsName = "sn";
        public const String SettingsValue = "sv";
        public const String RecoverBy = "rb";
        public const String Suggestions = "sgtns";

        public const String NotificationType = "nt";
        public const String NotificationMessageType = "mt";
        public const String ActionId = "acId";
        public const String FriendName = "fndN";
        public const String DeleteNotificationIds = "ntIDLst";//"ids";
        public const String TotalNotificationCount = "tn";
        public const String NotificationScrollPosition = "scl";
        public const String LikeOrComment = "loc";
        public const String NotificationList = "nList";
        //app globals
        public const String ChatServerIp = "chIp"; //chatServerIp
        public const String SmsServerIp = "smIp"; //smsServerIp
        public const String SmsServerPort = "smP"; //smsServerPort
        public const String MailServerIp = "ms";
        public const String MailServerPort = "mp";
        public const String Balance = "blc"; //balance
        public const String ChatRegistrationPort = "chRp"; //registrationPort
        public const String ConfirmationPort = "chCp"; //confirmationPort
        public const String ChatHistoryPort = "chHp"; //chatHistoryPort
        public const String FriendChatPort = "fChp"; //friendChatPort
        public const String GroupChatPort = "gChp"; //groupChatPort
        public const String LastOnlineTime = "lot";

        public const String MailIp = "mailIP";
        public const String SmtpPort = "smtpPrt";
        public const String ImapPort = "imapPrt";
        public const String ImageServerIp = "imgIP";
        public const String MarketServerIp = "mrktIP";
        public const String OfflineServerIp = "oIP";
        public const String OfflineServerPrt = "oPrt";

        public const String OriginalFeed = "orgFd";
        public const String WhoShareList = "whShrLst";
        //public const String WhoShare = "whoShare";
        public const String NumberOfHeaders = "nOfHd"; //noOfHeaders
        public const String FriendId = "fndId"; //friendId
        public const String FutId = "futId"; //futId
        public const String FutIds = "futIds"; //futIds
        public const String FriendFirstName = "ffn"; //FriendFirstName
        //public const String FriendLastName = "fln"; //FriendLastName
        public const String FriendProfileImage = "fprIm";
        public const String FriendProfileImagePrivacy = "fprImPr";
        public const String GuestId = "gstId";
        public const String WallOwnerId = "wOnrId";
        public const string OwnerId = "onrId";
        public const String SpecificFriendsToShowOrHide = "spcFrnds";

        public const String UserName = "usrNm"; //userName
        public const String Password = "usrPw"; //password
        public const String Version = "vsn"; //version
        public const String SearchParam = "schPm"; //searchParam
        public const String SearchCategory = "scat"; //SearchCategory
        public const String Category = "cat";
        public const String SearchedContacList = "searchedcontaclist";
        public const String Limit = "lmt";
        public const String VersionMessage = "vsnMg"; //versionMessage
        public const String DownloadMandatory = "dwnMnd"; //downloadMandatory
        public const String Device = "dvc"; //device
        public const String DeviceCategory = "dvcc"; //device category
        public const String DeviceId = "did"; //Unique Device ID (UDID)
        public const String DeviceToken = "dt"; //winUri
        public const String IsPickedMobileNumber = "ispc"; //is my number is picked by application? 1:0;
        public const String IsPickedEmail = "isepc";
        public const String VerificationCode = "vc"; //former passcode
        public const String EmailVerificationCode = "evc";
        public const String FnfHeaderVersion = "fnfHd"; //fnfHeaderVersion
        public const String Type = "type"; //type
        public const String Success = "sucs"; //success
        public const String IsSuccess = "success";
        public const String Message = "mg"; //message
        public const String MSG = "msg"; //message
        public const String Action = "actn"; //action
        public const String TempId = "tmpId"; //tempId
        public const String PacketId = "pckId"; //packetId
        public const String Sequence = "seq"; //seq
        public const String PacketIdFromServer = "pckFs"; //packetIdFromServer
        public const String PacketType = "pt"; //packetType
        public const String MessageDate = "mDate"; //messageDate    
        public const String PushUrl = "wUrl";//push notification uri
        public const String Url = "url";//push notification uri
        public const String TotalRecords = "tr";//totalRecords
        //public const String Latitude = "la";
        //public const String Longitude = "lo";
        public const String Timeout = "tout";
        public const String LastStatus = "lsts";
        public const String UserFound = "uf";
        public const String MutualFriendIDs = "mfIDs";
        public const String ReasonCode = "rc";
        public const String Tag = "tg";
        public const String TagId = "tid";
        public const String TagOwnerId = "twid";
        public const String TagName = "tgn";
        public const String NewTag = "ntg";
        public const String FriendIds = "fids";
        public const String TagList = "tagList";
        public const String TagUpdateTime = "tut";
        public const String FriendList = "frndList";
        public const String Friends = "frnds";
        public const String UIDs = "uIds";

        //groups
        public const String GroupId = "grpId";
        public const String UpdateBalance = "uBl";
        //public const String DialingCode = "dCD";
        public const String EmoticonUrl = "emoUrl";
        public const String GroupName = "gNm";
        public const String SuperAdmin = "sAd";
        public const String MemberIds = "mIds";
        public const String OldMemberIds = "oMIds";
        public const String NewMemberIds = "nMIds";
        public const String Admin = "admin"; // unchanged

        //reset password
        public const String SecretCode = "secCo";
        public const String NewPasswordRe = "nwPw"; //newPassword, used for resetting password (WTH!)

        //sms
        public const String StatusCode = "sCode";
        public const String LogStartLimit = "st";



        //privacy
        public const String Privacy = "pvc";
        public const String EmailPrivacy = "ePr";
        public const String MobilePhonePrivacy = "mblPhPr";
        public const String ProfileImagePrivacy = "prImPr";
        public const String BirthdayPrivacy = "bdPr";
        public const String CoverImagePrivacy = "cImPr";

        //emoticons
        public const String EmoticonVersion = "emVsn";

        public const String StartLimit = "st";
        public const String PostLimit = "lmt";

        //book
        public const String PostFeed = "pst";
        public const String ImageId = "imgId";
        public const String ImageIds = "imgIds";
        public const String ImageHeight = "ih";
        public const String ImageWidth = "iw";
        public const String ImageUrl = "iurl";
        public const String ImageCaption = "cptn";
        public const String ImageType = "imT";
        public const String Serial = "sl";
        public const String AlbumId = "albId";
        public const String AlbumName = "albn";
        public const String ImageAlbumType = "albT";
        public const String CoverImageUrl = "cvrURL";
        public const String isSameAlbum = "issa";
        public const String NumberOfComments = "nc";
        public const String NumberOfLikes = "nl";
        public const String NumberOfShares = "ns";
        public const String Time = "tm";
        public const String ActualTime = "at";
        public const String Id = "id";
        public const String BookPostType = "type";
        public const String Status = "sts";
        public const String NewsfeedId = "nfId";
        public const String SharedFeedId = "sfId";
        public const String ShareFeedOwnerUtId = "sfwOnrId";
        public const String ILike = "il";
        public const String IComment = "ic";
        public const String IShare = "is";
        public const String ImageDetails = "imgDtls";
        public const String Liked = "lkd";
        public const String Comment = "cmn";
        public const String CommentId = "cmnId";
        public const String AlbumCoverImage = "cvImg";
        public const String ILikeComment = "il";
        public const String TotalLikeComment = "tl";
        public const String Location = "lctn";
        public const String Latitude = "lat";
        public const String Longitude = "lng";
        public const String LocationDetails = "loctn";

        public const String Scroll = "scl";
        public const String TotalImage = "tim"; //image uploaded in a post
        public const String TotalShare = "ts";
        public const String ImageInCollection = "imc"; //total album image
        public const String Validity = "vldt"; //for newsfeed
        public const String TotalImagesInAlbum = "timg";//received with profile, cover collections for ImageViewer 
        public const String ImageAlbumList = "albumList";
        public const String ImageAlbumId = "albId";
        public const String Activity = "actvt";
        public const String ActivityType = "actT";
        public const String ActivistId = "auId";
        public const String ActivistFirstName = "afn";
        //public const String ActivistLastName = "aln";

        public const String StickerId = "stkId";
        public const String StickerBackgroundImage = "bkImgQ";
        public const String StickerCategoryName = "ctgNm";
        public const String StickerCategoryId = "ctgId";

        // for sticker market
        public const String StickerCatId = "sCtId";
        public const String StickerCatName = "sctName";
        public const String CollectionId = "sClId";
        public const String CategoryBannerImage = "cgBnrImg";
        public const String Rank = "rnk";
        public const String DetailImage = "dtlImg";
        public const String CategoryNew = "cgNw";
        public const String Icon = "icn";
        public const String Price = "prz";
        public const String Free = "free";
        public const String StickerDescription = "dcpn";
        public const String DefaultItem = "defaultItem";

        public const String Shares = "shares";
        public const String CollectionName = "name";
        //public const String CollectionTitle = "title";
        public const String CollectionBannerImage = "bnrImg";
        public const String ThemeColor = "thmColor";
        public const String TextColor = "txtColor";

        public const String StickerImageId = "imId";
        public const String StickerImageUrl = "imUrl";

        public const String WindowsPhoneNotificationUri = "winURI";
        public const String PushNotificationEnabled = "pen";

        public const String AuthServer = "ac";
        public const String ImageServer = "ims";
        public const String ImageServerResource = "imsres";
        public const String StickerServer = "stmapi";
        public const String StickerServerResource = "stmres";
        public const String VODServer = "vodapi";
        public const String VODServerResource = "vodres";

        public const String AuthServerIP = "authServerIP";
        public const String AuthServerPort = "authPort";
        public const String ComPort = "comPort";
        public const String CallForwardText = "cft";
        public const String UserDetails = "userDetails";
        public const String ContactList = "contactList";

        public const String AppType = "apt";
        public const String MatchedBy = "mb";

        public const string FeedAccess = "fda";  //Feed Access
        public const string CallAccess = "cla";  //Call Access
        public const string ChatAccess = "chta"; //Chat Access
        public const string AnonymousCallAccess = "anc";  //Anonymous Call Access
        public const string AnonymousChatAccess = "ancht";  //Anonymous Chat Access

        public const string ContactUtIdList = "idList";
        public const string RID = "rid";
        public const string FriendDTO = "fndDTO";

        public const string ContactIds = "contactIds";
        public const string NewsFeedList = "newsFeedList";
        public const string NewsFeed = "newsFeed";
        public const string ShowContinue = "sc";
        public const string Likes = "likes";
        public const string Comments = "comments";
        public const string CommentType = "cmntT";
        public const string CommentDTO = "cmntDTO";
        public const string GroupList = "groupList";
        public const string MemberOrMediaCount = "mc";
        public const string GroupMembers = "groupMembers";
        public const string RemovedMembers = "removedMembers";

        public const string LinkDetails = "lnk";
        public const string LinkTitle = "lnkTtl";
        public const string LinkURL = "lnkURL";
        public const string LinkDescription = "lnkDsc";
        public const string LinkImageURL = "lnlImgURL";
        public const string LinkDomain = "lnkDmn";
        public const string LinkType = "lnkT";

        public const string TotalTaggedFriends = "tfc";
        public const string TaggedFriendUtIds = "tFrndIds";
        public const string RemovedTagsFriendUtIds = "rTFrndIds";
        public const string FriendsTagList = "fTgLst";

        public const string DoingList = "mdLst";
        public const string DoingActivityIds = "fActvtIds";
        public const string MoodIds = "mdIds";
        public const string DoingSort = "wgt";

        public const string ContentId = "cntntId";
        public const string Title = "ttl";
        public const string Artist = "artst";
        public const string StreamUrl = "strmURL";
        public const string ThumbUrl = "thmbURL";
        public const string ThumbUrlComment = "thmbUrl";
        public const string ThumbImageHeight = "tih";
        public const string ThumbImageWidth = "tiw";
        public const string MediaDuration = "drtn";
        public const string MediaType = "mdaT";
        public const string MediaList = "mdaLst";
        public const string MediaIds = "mdaIds";
        public const string MediaContentDTO = "mdaCntntDTO";
        public const string MediaContentList = "mdaCntntLst";
        public const string MediaAlbumImageURL = "imgURL";
        public const string MediaAlbumList = "mediaAlbumList";

        public const string AccessCount = "ac";
        public const string LikeCount = "lc";
        public const string CommentCount = "cc";

        public const string Facebook = "fb";
        public const string Twitter = "twtr";


        public const string FacebookValidated = "fbvld";
        public const string TwitterValidated = "twtrvld";


        public const string StatusTags = "stsTags";

        public const string MembershipType = "mt";

        //call keys
        public const String SwitchIp = "swIp"; //switchIp
        public const String SwitchPort = "swPr"; //switchPort
        public const string P2PCall = "p2p";
        public const string RemotePushType = "rpt";
        public const string BlockBy = "bb";
        public const string AnonymousBlockBy = "abb";
        public const string Duration = "dunt"; //duration
        public const string CallId = "callID"; //callId
        public const string TCall = "tCall"; //callId
        public const string ConnectedWith = "cw"; //ConnectedWith
        public const String IsDivertedCall = "idc"; //IsDivertedCall
        public const string CallType = "calT";
        //call log
        public const string CallLog = "callLog";
        public const string CallDuration = "calD";
        public const string CallingTime = "caTm";
        public const string PhoneNo = "phN";
        //Media
        public const string MediaSuggestion = "sgstn";
        public const string MediaHashTagId = "ukey";
        public const string MediaSuggestionType = "sugt";
        public const string MediaSearchKey = "sk";
        public const string HashTagList = "htgList";
        public const string HValue = "val";
        public const string PivotId = "pvtid";
        public const string MediaItemsCount = "mdc";
        public const string HashTagName = "htn";
        public const string HashTagId = "htid";
        public const string MediaPrivacy = "mpvc";
        public const string FeedPrivacy = "fpvc";
        public const string SubType = "stp";
        //public const string SuperType = "spType";
        public const string WallType = "wType";
        public const string WallOwnerType = "wOnrT";
        public const string GuestWriterType = "gWtrT";
        public const string FeedCategory = "fc";
        public const string SharingPostContentIds = "cntntIDLst";
        public const string AdminCount = "admnc";
        public const string MemberCategoryCircle = "mcat";
        public const string MediaTrendingFeed = "mtf"; //mtf=1 for media trends , mtf=0 for all media feeds
        //Friend Settings
        public const string AllowIncomingFriendRequest = "aifr";
        public const string AllowFriendsToAddMe = "afam";
        public const string AutoAddFriends = "aaf";

        public const string SpamType = "spmt"; //SpamType(SPAM_USER/SPAM_FEED/SPAM_IMAGE/SPAM_MEDIA_CONTENT)
        public const string SpamReasonList = "rsnLst"; //Reason List
        public const string SpamReason = "rsn"; //Reason
        public const string SpamId = "spmid";
        public const string SpamReasonId = "rsnid";

        public const string IsDigits = "idgt";
        public const string BaseWebURL = "web";

        public const string NewsPortalFeedInfo = "npFeedInfo";
        public const string NewsPortalUrl = "nUrl";
        public const string NewsPortalExUrlOp = "exUrlOp";
        public const string NewsPortalCategoryId = "npCatId";
        public const string NewsCategoryId = "nCatId";
        public const string NewsPortalTitle = "nTtl";
        //public const string NewsPortalDescription = "nDesc";// 
        public const string NewsShortDescription = "nSDctn";
        public const string NewsDescription = "nDctn";
        public const string NewsPortalDTO = "npDTO";
        public const string NewsPortalFeedtype = "npFeedType";
        public const string IsNewsPortal = "isnp";
        public const string NewsPortalCategoryList = "nCatList";
        public const string NewsPortalList = "npList";
        public const string NewsPortalUtid = "npID";
        public const string PageId = "pId";
        public const string PageInfo = "pInfo";
        public const string IsUserFeedHidden = "isufhdn";
        public const string IsHidden = "ishdn";
        public const string NewsPortalCategoryName = "catName";
        public const string Subscribe = "subsc";
        public const string SubscribeType = "subscType";
        public const string SubscriberCount = "subCount";
        public const string SubscriptionTime = "subTime";
        public const string SubscribedIDs = "npSubIds";
        public const string UnsubscribedIDs = "npUnSubIds";
        public const string NewsPortalSlogan = "nPslgn";
        public const string NewsPortalCatName = "nPCatName";
        public const string NewsPortalCatId = "nPCatId";
        public const string IsFeaturedFeed = "isffeed";
        public const string SubUnsubscrbType = "subscType";
        public const string FeedIdsList = "feedIdList";
        public const string IdsList = "IDLst";
        public const string UtIdsList = "utIDLst";
        public const string Hide = "hid";
        public const string Option = "optn";
        public const string FeedIds = "feedIds";
        public const string IsSaved = "svd";
        public const string ProfileType = "pType";
        public const string BreakingNewsFeeds = "brNPFeeds";
        public const string MutualFriendCount = "wmfc";
        public const string NumberOfMutualFriendCount = "mfc";
        //Celebrity
        public const string CelebrityListType = "clt";
        public const string IsFollower = "isFollower";
        public const string OnlineTime = "olt";
        public const string MoodMessage = "mmsg";
        public const string Followercount = "fcount";
        public const string PostCount = "pcount";
        public const string CelebrityCountry = "cntr";
        public const string CelebrityCategoryId = "catId";
        public const string CelebrityCategoryName = "catName";
        public const string IsExcludeRemovedList = "iserl";
        public const string IsSortByPopularity = "issbp";
        public const string CelebrityList = "celList";
        public const string CelebrityInfo = "celebInfo";
        public const string Flw = "flw";
        public const string Follow = "follow";
        public const string CelebrityCatList = "celCatList";
        //Wallet
        public const string WSuccess = "success";
        public const string WUserTableId = "userId";
        public const string WInfo = "info";
        public const string MyCoin = "my_coin";
        public const string BonusCoin = "bonus_coin";
        public const string Quantity = "quantity";
        public const string AvailableQuantity = "available_quantity";
        public const string TotalQuantity = "total_quantity";
        public const string ExchangeRate = "exchangeRate";
        public const string CoinId = "coinId";
        public const string CoinName = "coinName";
        public const string CurrencyISOCode = "currencyISOCode";
        public const string CurrencyName = "currencyName";
        public const string TransactionHistory = "transactionHistory";
        public const string TransactionID = "transaction_id";
        public const string TransactionDate = "transaction_date";
        public const string TransactionTypeID = "transaction_type_id";
        public const string TransactionAmount = "amount";
        public const string OtherUserID = "other_user_id";
        public const string TransactionType = "trT";
        public const string TransactionRemarks = "transaction_Remarks";
        public const string PendingRequests = "pReqs";
        public const string UserRanks = "usrRnk";
        public const string ReferralList = "reflist";
        public const string ReferralSummary = "refSum";
        public const string MyReferralStat = "myRefSts";
        public const string ReferralID = "refId";
        public const string ReferralAcceptedStatus = "accepted";
        public const string ReferralAddedTime = "addTime";
        public const string ReferralGoldMemberCount = "1";
        public const string ReferralSilverMemberCount = "2";
        public const string RequestCoinID = "cnId";
        public const string TransfereeUtID = "toUtId";
        public const string TransactionQuantity = "qty";
        public const string TransactionRemark = "rmk";

        public const string UrlType = "uType";
        public const string CommentTags = "cmntTags";
        public const string IsEdited = "edtd";
        public const string CommentTagList = "cmnTgLst";
        public const string EditTime = "et";
        public const string List = "list";

        public const string AlbumCount = "albc";
        public const string ImageList = "imageList";
        public const string EditedImageList = "edtimglst";
        public const string RemovedImageIds = "rmviids";
        public const string AddedMediaList = "nmlst";
        public const string RemovedMediaIds = "rmvmids";
        public const string EditedMediaList = "edtmlst";
        public const string SavedTime = "stm";

        public const string PivotUUID = "pvtUUID";
        public const string PivotTime = "pvtTm";
        public const string RootContentType = "rtCntntType";
        public const string GuestWriter = "gstWtr";
        public const string WallOwner = "wlOnr";
        public const string LatestLiker = "lkr";
        public const string LatestCommenter = "cmntr";
        public const string LatestSharer = "shrer";
        public const string NpUUID = "npuuid";
        public const string SharedFeedList = "shrdFdLst";
        public const string TotalMediaCount = "mdaC";
        public const string CommenterID = "cmntrId";

        public const string ProfileImageHeight = "prImH";
        public const string ProfileImageWidth = "prImW";
        public const string CoverImageHeight = "cImH";
        public const string CoverImageWidth = "cImW";

        // Live Stream

        public const string StreamId = "streamId";
        public const string StreamCategoryId = "catId";
        public const string StreamCategoryList = "catList";
        public const string StreamLagList = "tagList";
        public const string StreamHashTagId = "hTagID";
        public const string StreamHashTag = "hTag";
        public const string StreamChatON = "chtOn";
        public const string StreamGiftON = "giftOn";
        public const string StreamDetails = "streamDetails";
        public const string StreamFullName = "fn";
        public const string StreamStartTime = "stTm";
        public const string StreamEndTime = "endTm";
        public const string StreamCoinCount = "cnc";
        public const string StreamViewerCount = "vwc";
        public const string StreamFollowingCount = "followingCount";
        public const string StreamFollowerCount = "followerCount";
        public const string StreamChatServerIp = "chatServerIp";
        public const string StreamChatServerPort = "chatServerPort";
        public const string StreamPublisherServerIp = "streamingServerIp";
        public const string StreamPublisherServerPort = "streamingServerPort";
        public const string StreamViewerServerIp = "viewerServerIp";
        public const string StreamViewerServerPort = "viewerServerPort";
        public const string StreamServerDTO = "strmSrvrDto";

        public const string StreamLongitude = "lon";
        public const string StreamSearchParm = "schPm";
        public const string StreamDetailsList = "streamDetailsList";
        public const string StreamCountry = "cntr";
        public const string StreamDistance = "dist";
        public const string StreamIsFollowing = "isFollowing";
        public const string StreamChatOn = "chtOn";
        public const string StreamGiftOn = "giftOn";
        public const string StreamUser = "user";
        public const string StreamScroll = "scl";
        public const string StreamIsFriendOnly = "isFnd";

        //Channel

        public const string ChannelDTO = "cdto";
        public const string ChannelID = "chnlId";
        public const string ChannelOwnerID = "ownerId";
        public const string ChannelCatetoryList = "chnlCats";
        public const string ChannelCatetoryIDs = "chnlCatIds";
        public const string ChannelStatus = "chnlSts";
        public const string ChannelType = "chType";
        public const string ChannelMediaList = "chnlMedia";
        public const string ChannelMediaArtist = "artist";
        public const string ChannelMediaID = "mediaId";
        public const string ChannelMediaType = "mediaType";
        public const string ChannelMediaUrl = "mediaUrl";
        public const string ChannelThumbImageUrl = "thmbImgUrl";
        public const string ChannelThumbImageWidth = "thmbImgW";
        public const string ChannelThumbImageHeight = "thmbImgH";
        public const string ChannelList = "chnlList";
        public const string ChannelSubscrbUnSubscrbValue = "subVal";
        public const string ChannelMediaStatus = "chMSts";
        public const string ChannelMediaIds = "chnlMediaIds";
        public const string ChannelUserType = "userType";
        public const string ChannelMediaStartTime = "stTime";
        public const string ChannelMediaDTO = "cmdto";
        public const string ChannelCreationTime = "ctm";
        public const string IsFromChannel = "isChnl";
        public const string ChannelIP = "chnlIp";
        public const string ChannelPort = "chnlPrt";
        public const string StreamIP = "strmIP";
        public const string StreamPort = "strmPrt";

        public const string AlbumsList = "albLst";
        public const string NtUUID = "uuid";
        public const string Weight = "weight";
        public const string AlbumDetails = "albmDtls";
        public const string TotalAudioOrVideoCount = "tmc";
        public const string Edited = "edt";
        public const string PageTitle = "pttl";

        public const string RequestCancelled = "reqCancelled";
        public const string NoResponseFromServer = "noResponse";
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

﻿
namespace Models.Constants
{
    public static class StatusConstants
    {
        public const int PRESENCE_OFFLINE = 1;
        public const int PRESENCE_ONLINE = 2;
        public const int PRESENCE_AWAY = 3;

        public const int MOOD_OFFLINE = 0;
        public const int MOOD_ALIVE = 1;
        public const int MOOD_DONT_DISTURB = 2;
        public const int MOOD_BUSY = 3;
        public const int MOOD_INVISIBLE = 4;

        /**/
        public const int FRIENDSHIP_STATUS_ACCEPTED = 1;
        public const int FRIENDSHIP_STATUS_INCOMING = 2;
        public const int FRIENDSHIP_STATUS_PENDING = 3;
        public const int FRIENDSHIP_STATUS_UNKNOWN = 0;
        public const short STATUS_DELETED = 1;
        public const short STATUS_UPDATED = 2;
        public const short STATUS_INSERTED = 3;

        // Contct Access
        //public const int ACCESS_CHAT_CALL = 1;
        //public const int ACCESS_FULL = 2;

        public const int SEARCH_BY_ALL = 0;
        public const int SEARCH_BY_NAME = 1;
        public const int SEARCH_BY_PHONE = 2;
        public const int SEARCH_BY_EMAIL = 3;
        public const int SEARCH_BY_RINGID = 4;
        public const int SEARCH_BY_LOCATION = 5;

        //Notification Type
        public const int NOTIFICATION_CONTACT = 1;
        public const int NOTIFICATION_BOOK = 2;
        public const int NOTIFICATION_SMS = 3;
        public const int NOTIFICATION_CIRCLE = 4;
        public const int NOTIFICATION_PROFILE = 5;
        //Notification Message Type
        public const int MESSAGE_UPDATE_PROFILE_IMAGE = 1;// Example: FrinedName(fndN) updated his profile photo.
        public const int MESSAGE_UPDATE_COVER_IMAGE = 2; //Example: FrinedName(fndN) updated his cover photo.
        public const int MESSAGE_ADD_FRIEND = 3; //Example: FrinedName(fndN) wants to be friends with you.
        public const int MESSAGE_ACCEPT_FRIEND = 4; //Example: FrinedName(fndN) has accepted your friend request.
        public const int MESSAGE_ADD_CIRCLE_MEMBER = 5; //Example: FrinedName(fndN) added you in groupName(Using groupId need to find groupName).
        public const int MESSAGE_ADD_STATUS_COMMENT = 6; //Example: FriendName(fndN) commented on your status. or Example: FriendName(fndN) & previousFriendName Commented on your status.
        public const int MESSAGE_LIKE_STATUS = 7; //Example: FriendName(fndN) liked your status. or Example: FriendName(fndN) & previousFriendName liked your status.
        public const int MESSAGE_LIKE_COMMENT = 8; //
        public const int MESSAGE_ADD_COMMENT_ON_COMMENT = 9;
        public const int MESSAGE_SHARE_STATUS = 10;
        public const int MESSAGE_LIKE_IMAGE = 11; //
        public const int MESSAGE_IMAGE_COMMENT = 12;
        public const int MESSAGE_LIKE_IMAGE_COMMENT = 13;
        public const int MESSAGE_ACCEPT_FRIEND_ACCESS = 15;
        public const int MESSAGE_UPGRADE_FRIEND_ACCESS = 14;
        public const int MESSAGE_DOWNGRADE_FRIEND_ACCESS = 16;
        //public const int MESSAGE_NOW_FRIEND = 17;
        public const int MESSAGE_LIKE_AUDIO_MEDIA = 17;
        public const int MESSAGE_AUDIO_MEDIA_COMMENT = 18;
        public const int MESSAGE_LIKE_AUDIO_MEDIA_COMMENT = 19;
        public const int MESSAGE_AUDIO_MEDIA_VIEW = 20;
        public const int MESSAGE_LIKE_VIDEO_MEDIA = 21;
        public const int MESSAGE_VIDEO_MEDIA_COMMENT = 22;
        public const int MESSAGE_LIKE_VIDEO_MEDIA_COMMENT = 23;
        public const int MESSAGE_VIDEO_MEDIA_VIEW = 24;
        public const int MESSAGE_NOTIFICATION_WITH_TAG = 25;
        public const int MESSAGE_NOTIFICATION_STATUS_TAG = 26;
        public const int MESSAGE_NOTIFICATION_COMMENT_TAG = 27;
        public const int MESSAGE_NOTIFICATION_LIVE = 51;

        public const int WEB_LOGIN_ENABLED = 1;
        public const int PC_LOGIN_ENABLED = 2;
        public const int COMMON_FRIENDS_SUGGESTION = 3;
        public const int PHONE_NUMBER_SUGGESTION = 4;
        public const int CONTACT_LIST_SUGGESTION = 5;
        public const int FULL_ACCESS = 0;//Custom
        public const int CALL_ACCESS = 6;
        public const int CHAT_ACCESS = 7;
        public const int FEED_ACCESS = 8;
        public const int ANONYMOUS_CALL = 9;
        public const int ANONYMOUS_CHAT = 10;
        public const int ALLOW_INCOMING_FRIEND_REQUEST = 11;
        public const int ALLOW_FRIENDS_TO_ADD_ME = 12;
        public const int AUTO_ADD_FRIENDS = 13;

        public const int CHAT_CALL_EVERYONE = 1;
        public const int CHAT_CALL_FRIENDS_ONLY = 0;

        public const int TYPE_ACCESS_BLOCKED = 0;
        public const int TYPE_ACCESS_UNBLOCKED = 1;

        public const int LOADING_PANEL_INVISIBILE = 0;
        public const int LOADING_TEXT_VISIBLE = 1;
        public const int LOADING_GIF_VISIBLE = 2;
        public const int LOADING_NO_MORE_FEED_VISIBLE = 3;

        public const int MEDIA_INIT_STATE = 0;
        public const int MEDIA_PLAY_STATE = 1;
        public const int MEDIA_PAUSE_STATE = 2;

        public const int MEDIA_DOWNLOAD_NOT_START_STATE = 0;
        public const int MEDIA_DOWNLOADING_STATE = 1;
        public const int MEDIA_DOWNLOAD_PAUSE_STATE = 2;
        public const int MEDIA_DOWNLOAD_COMPLETED_STATE = 3;

        public const int IMAGE_DEFAULT = 0;
        public const int IMAGE_LANDSCAPE = 1;
        public const int IMAGE_PORTRAIT = 2;


        public const int CIRCLE_MEMBER = 0;
        public const int CIRCLE_ADMIN = 1;
        public const int CIRCLE_SUPER_ADMIN = 2;

        public const int CIRCLE_MEMBER_NORMAL_SCROLL = 0;
        public const int CIRCLE_MEMBER_ADMIN_SCROLL = 1;
        public const int CIRCLE_MEMBER_NORMAL_SEARCH = 2;

        public const int INCOMING_REQUEST_READ = 1;
        public const int INCOMING_REQUEST_UNREAD = 0;

        public const int AIFR_AFAM_AAD_SET = 0;
        public const int AIFR_AFAM_AAD_UNSET = 1;

        public const int SPAM_USER = 1;
        public const int SPAM_FEED = 2;
        public const int SPAM_IMAGE = 3;
        public const int SPAM_MEDIA_CONTENT = 4;

        public const int SEARCH_FRIENDLIST = 1;
        public const int SEARCH_ADD_FRIEND_OR_DIALPAD = 2;

        //Wallet Constants
        public const int WALLET_USER_RANK_GOLD = 1;
        public const int WALLET_USER_RANK_SILVER = 2;

        public const int WALLET_TRANSACTION_TYPE_SENT = 1;
        public const int WALLET_TRANSACTION_TYPE_RECEIVED = 2;
        public const int WALLET_TRANSACTION_TYPE_BONUS_MIGRATION = 3;

        public const int NAVIGATE_FROM_FEED = 1;
        public const int NAVIGATE_FROM_PROFILE = 2;
        public const int NAVIGATE_FROM_NOTIFICATION = 3;
        public const int NAVIGATE_FROM_NEWSPORTAL = 4;
        public const int NAVIGATE_FROM_COMMENT = 5;

        public static readonly int SCROLL_LATEST = 1;
        public static readonly int SCROLL_OLDER = 2;

        public const int NO_DATA = 0;
        public const int NO_DATA_AND_LOADING = 1;
        public const int HAS_DATA = 2;
        public const int HAS_DATA_AND_LOADING = 3;
    }
}

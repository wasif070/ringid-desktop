﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Constants
{
    public class StartUpConstatns
    {
        public const string ARGUMENT_STARTUP = "-startup";
        public const string ARGUMENT_RESTART = "-restart";
        public const string ARGUMENT_MULTIPLELOGIN = "-multiplelogin";
        public const string ARGUMENT_SESSIONINVALID = "-sessioninvalid";
        public const string ARGUMENT_SIGNIN_FAILD = "-signInFailed";
        public const string ARGUMENT_EXIT_FROM_ANOTHER_PROCESS = "-exitFromAnotherProc";
        public const string ARGUMENT_RESTART_FROM_ANOTHER_PROCESS = "-restartFromAnotherProc";
    }
}

﻿using Models.Entity;
using System.IO;
using Newtonsoft.Json.Linq;
using System;

namespace Models.Constants
{
    public class DefaultSettings
    {
        public static string DB_FILE = string.Empty;
        public static int KEEP_ALIVE_TIME = 30000;
        public static string PROFILE_IMAGE_ALBUM_ID = "profileimages";
        public static string COVER_IMAGE_ALBUM_ID = "coverimages";
        public static string FEED_IMAGE_ALBUM_ID = "default";
        public static string TEMP_PROFILE_IMAGE_FOLDER = "tempprofileimages";
        public static string TEMP_COVER_IMAGE_FOLDER = "tempcoverimages";
        public static string TEMP_BOOK_IMAGE_FOLDER = "tempbookimages";
        public static string TEMP_CHAT_FILES_FOLDER = "tempchatfiles";
        public static string TEMP_BROKEN_FILES_FOLDER = "tempbrokenfiles";
        public static string RESOURCE_FOLDER = "resources" + Path.AltDirectorySeparatorChar;
        public static string EMOTICON_FOLDER = "resources" + Path.AltDirectorySeparatorChar + "emoticons";
        public static string LARGE_EMOTICON_FOLDER = "resources" + Path.AltDirectorySeparatorChar + "large_emoticons";
        public static string STICKER_FOLDER = "resources" + Path.AltDirectorySeparatorChar + "stickers";
        public static string SCRIPT_FOLDER = "resources" + Path.AltDirectorySeparatorChar + "scripts";
        public static string CHAT_BG_FOLDER = "resources" + Path.AltDirectorySeparatorChar + "chatbg";
        public static JObject VALUE_LOGIN_USER_INFO = null;
        public static WorkDTO TEMP_WorkObject = null;
        public static EducationDTO TEMP_EducationObject = null;
        public static SkillDTO TEMP_SkillObject = null;
        public static string EMOTION_REGULAR_EXPRESSION = String.Empty;
        public const string HYPER_LINK_EXPRESSION = @"\b(?:(https?|ftp|file)://|www\.)[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?";
        public const string ONLY_HTTPS_HYPER_LINK_EXPRESSION = @"\b(?:(https?)://|www\.)[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?";
        public const string TAG_EXPRESSION = @" ?\{.*?\}";
        public const string GUID_EXPRESSION = @"{([0-9a-zA-Z]+-[0-9a-zA-Z]+-[0-9a-zA-Z]+-[0-9a-zA-Z]+-[0-9a-zA-Z]+)}";
        public static string D_MINI = "dmini";
        public static string D_MIDUM = "dmid";
        public static string D_FULL = "dfull";
        public static UserBasicInfoDTO userProfile = null;
        public static string DefaultCountry = null;
        //public static int USER_PREV_STATUS = 0; //Mood Offline
        public static string RING_OFFLINE_IM_IP = "";
        public static int RING_OFFLINE_IM_PORT = 0;
        public static int RING_CHAT_BUFFER_SIZE = 2048;
        public static int RING_CHAT_BROKEN_PACKET_SIZE = 512;
        public static int RING_CHAT_FILE_CHUNK_SIZE = 5120;//10240//5120
        public static int RING_CHAT_LOCAL_PORT = 9876;
        public static bool FRIEND_LIST_LOADED = false;
        public static bool FRIEND_LIST_LOADED_FROM_DB = false;
        public static int MEMORY_MAX_LIMIT = 200; //MB
        public static int FEED_MAX_LIMIT = 100;
        public static int MEMORY_RELEASE_LIMIT = 10;
        public static bool MEMORY_RELEASE_TOP_MODELS_FOR_FEED_TYPE_ALL = false;
        public static bool MEMORY_RELEASE_TOP_MODELS_FOR_FEED_TYPE_MY = false;
        public static long MEMORY_RELEASE_TOP_MODELS_FOR_FEED_TYPE_FRIEND = 0;
        public static long MEMORY_RELEASE_TOP_MODELS_FOR_FEED_TYPE_CIRCLE = 0;


        public const int FEED_TYPE_MY = 2;
        public const int FEED_TYPE_FRIEND = 3;
        public const int FEED_TYPE_CIRCLE = 4;
        public const int FEED_TYPE_MEDIA = 105;//turn to nothing
        public const int FEED_TYPE_MEDIA_TRENDING = 106; //turn to nothing
        public const int FEED_TYPE_NEWSPORTAL = 7;
        public const int FEED_TYPE_NEWSPORTAL_PROFILE = 8;
        public const int FEED_TYPE_NEWSPORTAL_SAVED = 9;
        public const int FEED_TYPE_PAGE = 10;
        public const int FEED_TYPE_PAGE_PROFILE = 11;
        public const int FEED_TYPE_PAGE_SAVED = 12;
        public const int FEED_TYPE_CELEBRITY = 13;
        public const int FEED_TYPE_CELEBRITY_PROFILE = 14;
        public const int FEED_TYPE_CELEBRITY_SAVED = 15;
        public const int FEED_TYPE_MUSIC = 16;
        public const int FEED_TYPE_MUSIC_PROFILE = 17;
        public const int FEED_TYPE_MUSIC_SAVED = 18;

        //1-9 All////
        public const int FEED_TYPE_ALL = 1;
        public const int FEED_TYPE_ALL_NEWSPORTAL = 2;
        public const int FEED_TYPE_ALL_MEDIA = 3;
        //10-19 Profiles////
        public const int FEED_TYPE_PROFILE_MY = 11;
        public const int FEED_TYPE_PROFILE_NEWSPORTAL = 12;
        public const int FEED_TYPE_PROFILE_MEDIA = 13;
        public const int FEED_TYPE_PROFILE_FRIEND = 14;
        ///20-30 saved////
        public const int FEED_TYPE_SAVED_ALL = 20;
        public const int FEED_TYPE_SAVED_PAGE = 21;
        public const int FEED_TYPE_SAVED_MEDIAPAGE = 22;
        public const int FEED_TYPE_SAVED_CELEB = 23;
        public const int FEED_TYPE_SAVED_CIRCLE = 24;
        public const int FEED_TYPE_SAVED_NEWSPORTAL = 25;
        //others////


        //end
        public const int FEED_TYPE_CIRCLE_ALL = 25;
        public const int FEED_TYPE_DETAILS = 26;
        public const int FEED_TYPE_SHARED = 27;

        public static int MEGA_BYTE = 1024 * 1024;
        //public static long MAX_CHAT_FILE_SIZE = 5345279;
        public static long MAX_CHAT_FILE_TRANSFER_SIZE = 1073741824L;
        public static int MIN_CHAT_BG_WIDTH = 1920;
        public static int MIN_CHAT_BG_HEIGHT = 1080;
        public static int MAX_GROUP_MEMBER_LIMIT = 99;
        public static bool DEBUG = true;

        /*
         * saved in dat file names
         */
        public static long RINGID_OFFICIAL_UTID = 0;
        //public static long RINGID_OFFICIAL_ID = 2110000001;
        //public static long LOGIN_USER_ID = 0;
        //public static long LOGIN_USER_TABLE_ID = 0;
        public static long LOGIN_RING_ID = 0;
        public static long LOGIN_TABLE_ID = 0;
        public static string LOGIN_USER_PROFILE_IMAGE = "";
        public static string LOGIN_SESSIONID = null;
        public static bool IS_FIRST_TIME_LOGIN = true;
        public static int INITIAL_SUGSTNS_COUNT = 0;
        public static long PREVIOUS_USER_NAME = 0;

        public static string ENTER_PRESS_BEFORE_LOGIN = "";
        public const string KEY_NEW_USER_NAME = "new.user.name";
        public static long VALUE_NEW_USER_NAME = 0;
        public const string KEY_LOGIN_USER_NAME = "login.user.name";
        public static string VALUE_LOGIN_USER_NAME = "";
        public const string KEY_LOGIN_USER_TYPE = "login.user.type";
        public static int VALUE_LOGIN_USER_TYPE = 0;
        public const string KEY_LOGIN_USER_PASSWORD = "login.user.password";
        public static string VALUE_LOGIN_USER_PASSWORD = "";
        public const string KEY_LOGIN_AUTO_SIGNIN = "login.auto.signin";
        public static int VALUE_LOGIN_AUTO_SIGNIN = 1;
        public const string KEY_LOGIN_SAVE_PASSWORD = "login.save.password";
        public static int VALUE_LOGIN_SAVE_PASSWORD = 1;
        public const string KEY_DEVICE_UNIQUE_ID = "device.unique.id";
        public static string DEVICE_UNIQUE_ID = string.Empty;
        //public static bool VAL_LOGIN_SAVE_PASSWORD = true;
        //public static string KEY_LOGIN_AUTO_START = "login.auto.start";
        public static int VALUE_LOGIN_AUTO_START = 1;
        public const string KEY_LOGIN_USER_INFO = "login.user.info";
        public const string KEY_MOBILE_DIALING_CODE = "mobile.dialing.code";
        public static string VALUE_MOBILE_DIALING_CODE = null;
        //public static string KEY_SOCIAL_MEDIA_ID = "social.media.id";
        //public static string VALUE_SOCIAL_MEDIA_ID = null;
        public const string KEY_DOING_LIST_UT = "doing.list.ut";
        public static long VALUE_DOING_LIST_UT = 0;
        public const string KEY_APP_INSTALLED_VERSION = "app.installed.version";
        public static string VALUE_APP_INSTALLED_VERSION = AppConfig.DESKTOP_FIRST_REALEASE_VERSION;
        public static bool IS_VERSION_VALUE_EXIST = false;
        public static long? CALL_FORWARDIN_TXT = null;
        public static bool CurrentDivertAdd = false;
        public static bool CurrentDivertCancel = false;
        public static string CALL_DIVERT_FRIEND_ID = null;
        public static bool isFriendListNeedToChange = true;
        //spublic static bool NoTopSticker = false;
        public static string[] PRIVACYTYPE_ARRAY = new string[] { "Everyone", "Only Friends", "Only Me" };
        public static string[] GENDER_ARRAY = new string[] { "Male", "Female" };
        public static int[] HISTORY_ARRAY_DAY = new int[] { -1, 0, 1, 2, 6, 14, 29, 89 };
        public static string[,] COUNTRY_MOBILE_CODE = new string[,] { 
        {"ringID", ""},//+878 21
        {"E-mail", ""},
        {"Afghanistan", "+93"},
        {"Albania", "+355"},
        {"Algeria", "+213"},
        {"American Samoa", "+1684"},
        {"Andorra", "+376"},
        {"Angola", "+244"},
        {"Anguilla", "+1264"},
        {"Antarctica", "+672"},
        {"Antigua and Barbuda", "+1268"},
        {"Argentina", "+54"},
        {"Armenia", "+374"},
        {"Aruba", "+297"},
        {"Australia", "+61"},
        {"Austria", "+43"},
        {"Azerbaijan", "+994"},
        {"Bahamas", "+1242"},
        {"Bahrain", "+973"},
        {"Bangladesh", "+880"},
        {"Barbados", "+1246"},
        {"Belarus", "+375"},
        {"Belgium", "+32"},
        {"Belize", "+501"},
        {"Benin", "+229"},
        {"Bermuda", "+1441"},
        {"Bhutan", "+975"},
        {"Bolivia", "+591"},
        {"Bonaire, Sint Eustatius", "+599"},
        {"Bosnia and Herzegovina", "+387"},
        {"Botswana", "+267"},
        {"Brazil", "+55"},
        {"British Virgin Islands", "+1284"},
        {"Brunei", "+673"},
        {"Bulgaria", "+359"},
        {"Burkina Faso", "+226"},
        {"Burundi", "+257"},
        {"Cambodia", "+855"},
        {"Cameroon", "+237"},
        {"Canada", "+1"},
        {"Cape Verde", "+238"},
        {"Cayman Islands", "+1345"},
        {"Central African Republic", "+236"},
        {"Chad", "+235"},
        {"Chile", "+56"},
        {"China", "+86"},
        {"Colombia", "+57"},
        {"Comoros", "+269"},
        {"Congo", "+242"},
        {"Congo (DRC)", "+243"},
        {"Cook Islands", "+682"},
        {"Costa Rica", "+506"},
        {"C'te d'Ivoire", "+225"},
        {"Croatia", "+385"},
        {"Cuba", "+53"},
        {"Cyprus", "+357"},
        {"Czech Republic", "+420"},
        {"Denmark", "+45"},
        {"Djibouti", "+253"},
        {"Dominica", "+1767"},
        {"Dominican Republic", "+18"},
        {"Ecuador", "+593"},
        {"Egypt", "+20"},
        {"El Salvador", "+503"},
        {"Equatorial Guinea", "+240"},
        {"Eritrea", "+291"},
        {"Estonia", "+372"},
        {"Ethiopia", "+251"},
        {"Falkland Islands", "+500"},
        {"Faroe Islands", "+298"},
        {"Fiji", "+679"},
        {"Finland", "+358"},
        {"France", "+33"},
        {"French Guiana", "+594"},
        {"French Polynesia", "+689"},
        {"Gabon", "+241"},
        {"Gambia", "+220"},
        {"Georgia", "+995"},
        {"Germany", "+49"},
        {"Ghana", "+233"},
        {"Gibraltar", "+350"},
        {"Greece", "+30"},
        {"Greenland", "+299"},
        {"Grenada", "+1473"},
        {"Guadeloupe", "+590"},
        {"Guam", "+1671"},
        {"Guatemala", "+502"},
        {"Guinea", "+224"},
        {"Guinea Bissau", "+245"},
        {"Guyana", "+592"},
        {"Haiti", "+509"},
        {"Holy See (Vatican City)", "+3"},
        {"Honduras", "+504"},
        //     {"Hong Kong SAR", "+852"},
        {"Hungary", "+36"},
        {"Iceland", "+354"},
        {"India", "+91"},
        {"Indonesia", "+62"},
        {"Iran", "+98"},
        {"Iraq", "+964"},
        {"Ireland", "+353"},
        {"Israel", "+972"},
        {"Italy", "+39"},
        {"Jamaica", "+1876"},
        {"Japan", "+81"},
        {"Jordan", "+962"},
        {"Kazakhstan", "+7"},
        {"Kenya", "+254"},
        {"Kiribati", "+686"},
        {"Korea", "+82"},
        {"Kuwait", "+965"},
        {"Kyrgyzstan", "+996"},
        {"Laos", "+856"},
        {"Latvia", "+371"},
        {"Lebanon", "+961"},
        {"Lesotho", "+266"},
        {"Liberia", "+231"},
        {"Libya", "+218"},
        {"Liechtenstein", "+423"},
        {"Lithuania", "+370"},
        {"Luxembourg", "+352"},
        {"Macao SAR", "+853"},
        {"Macedonia, FYRO", "+389"},
        {"Madagascar", "+261"},
        {"Malawi", "+265"},
        {"Malaysia", "+60"},
        {"Maldives", "+960"},
        {"Mali", "+223"},
        {"Malta", "+356"},
        {"Marshall Islands", "+692"},
        {"Martinique", "+596"},
        {"Mauritania", "+222"},
        {"Mauritius", "+230"},
        {"Mayotte", "+269"},
        {"Mexico", "+52"},
        {"Micronesia", "+691"},
        {"Moldova", "+373"},
        {"Monaco", "+377"},
        {"Mongolia", "+976"},
        {"Montenegro", "+382"},
        {"Montserrat", "+1664"},
        {"Morocco", "+212"},
        {"Mozambique", "+258"},
        {"Myanmar", "+95"},
        {"Namibia", "+264"},
        {"Nauru", "+674"},
        {"Nepal", "+977"},
        {"Netherlands", "+31"},
        {"New Caledonia", "+687"},
        {"New Zealand", "+64"},
        {"Nicaragua", "+505"},
        {"Niger", "+227"},
        {"Nigeria", "+234"},
        {"Niue", "+683"},
        {"Northern Mariana Islands", "+1670"},
        {"North Korea", "+850"},
        {"Norway", "+47"},
        {"Oman", "+968"},
        {"Pakistan", "+92"},
        {"Palau", "+680"},
        {"Panama", "+507"},
        {"Papua New Guinea", "+675"},
        {"Paraguay", "+595"},
        {"Peru", "+51"},
        {"Philippines", "+63"},
        {"Poland", "+48"},
        {"Portugal", "+351"},
        {"Puerto Rico", "+1787"},
        {"Qatar", "+974"},
        {"Romania", "+40"},
        {"Russia", "+7"},
        {"Rwanda", "+250"},
        {"Saint Helena, Ascension", "+290"},
        {"Saint Kitts and Nevis", "+1869"},
        {"Saint Lucia", "+1758"},
        {"Saint Pierre and Miquelon", "+508"},
        {"Saint Vincent and the Grenadines", "+1784"},
        {"Samoa", "+685"},
        {"San Marino", "+378"},
        {"Sao Tome and Principe", "+239"},
        {"Saudi Arabia", "+966"},
        {"Senegal", "+221"},
        {"Serbia", "+381"},
        {"Seychelles", "+248"},
        {"Sierra Leone", "+232"},
        {"Singapore", "+65"},
        {"Slovakia", "+421"},
        {"Slovenia", "+386"},
        {"Solomon Islands", "+677"},
        {"Somalia", "+252"},
        {"South Africa", "+27"},
        {"Spain", "+34"},
        {"Sri Lanka", "+94"},
        {"Sudan", "+249"},
        {"Suriname", "+597"},
        {"Swaziland", "+268"},
        {"Sweden", "+46"},
        {"Switzerland", "+41"},
        {"Syria", "+963"},
        {"Taiwan", "+886"},
        {"Tajikistan", "+992"},
        {"Tanzania", "+255"},
        {"Thailand", "+66"},
        {"Timor Leste", "+670"},
        {"Togo", "+228"},
        {"Tokelau", "+690"},
        {"Tonga", "+676"},
        {"Trinidad and Tobago", "+1868"},
        {"Tunisia", "+216"},
        {"Turkey", "+90"},
        {"Turkmenistan", "+993"},
        {"Turks and Caicos Islands", "+1649"},
        {"Tuvalu", "+688"},
        {"Uganda", "+256"},
        {"Ukraine", "+380"},
        {"United Arab Emirates", "+971"},
        {"United Kingdom", "+44"},
        {"United States", "+1"},
        {"Uruguay", "+598"},
        {"US Virgin Islands", "+1340"},
        {"Uzbekistan", "+998"},
        {"Vanuatu", "+678"},
        {"Venezuela", "+58"},
        {"Vietnam", "+84"},
        {"Wallis and Futuna", "+681"},
        {"Yemen", "+967"},
        {"Zambia", "+260"},
        {"Zimbabwe", "+263"}};
        /*
         * Create or Update user Fields
         */
        public static bool IS_MY_PROFILE_VALUES_LOADED = false;
        public static int TOTAL_CIRCLES_COUNT = 0;
        /*
         Thread trying */
        public static int TRYING_TIME = 100;//45;//timese loop will 
        public static Int32 WAITING_TIME = 500;//ms
        public static int SEND_INTERVAL = 9;//will send a request to server after this time loop 
        public static bool IS_NETWORK_INTERFACE_AVAILABLE = true;
        public static bool IsInternetAvailable { get; set; }
        /**/
        //public static long ContactListResponsTimeInterval = 0;

        //public static dynamic TMP_TAG_OBJ = null;
        //     public static string TMP_RECOVERY_MOBILE = null;
        public static int FEED_STATIC_MAP_HIT = 0;
        public static int MY_AUDIO_ALBUMS_COUNT = -1;
        public static int MY_VIDEO_ALBUMS_COUNT = -1;
        public static int MY_IMAGE_ALBUMS_COUNT = -1;
        public static long MaxFileLimit500MBinBytes = 524288000;
        public static long MaxFileLimit250MBinBytes = 262144000;
        public static long MaxFileLimit2GB = 2147483648;
        public static int MediaItemsSequenceCount = 0;//, HashTagContentsSequenceCount = 0;

        public static int SearchTrendsSeq = 0;

        public static long MaxAllFeedsTime = -999999999999999;
        public static long MinAllFeedsTime = 99999999999999999;

        public static string ALL_STARTPKT = null; //value=something when first feeds requested,value=null when first feeds found
        public static string ALLNEWSPORTAL_STARTPKT = null;
        public static string ALLPAGE_STARTPKT = null;
        public static string ALLCELEBRITY_STARTPKT = null;
        public static string ALLMEDIA_STARTPKT = null;
        public static string ALLCIRCLE_STARTPKT = null;

        public static string PROFILEMY_STARTPKT = null;
        public static string PROFILEPROFILE_STARTPKT = null;
        public static string PROFILECIRCLE_STARTPKT = null;
        public static string PROFILENEWSPORTAL_STARTPKT = null;
        public static string PROFILEPAGE_STARTPKT = null;
        public static string PROFILEMEDIA_STARTPKT = null;
        public static string PROFILECELEBRITY_STARTPKT = null;

        public static string SAVEDALL_STARTPKT = null; //value=something when first feeds requested,value=null when first feeds found
        public static string SAVEDNEWSPORTAL_STARTPKT = null;
        public static string SAVEDPAGE_STARTPKT = null;
        public static string SAVEDCELEBRITY_STARTPKT = null;
        public static string SAVEDMEDIA_STARTPKT = null;
        public static string SAVEDCIRCLE_STARTPKT = null;

        public static int CONTENT_SHOW_LIMIT = 10;
        public static bool ALL_CIRCLE_FETCHED = false;
        public static bool STORAGE_FEEDS_REMOVED = false;
        public static bool STORAGE_FEEDS_STARTSAVED = false;

        public static int NEW_STICKER_UNSEEN_COUNT = 0;
        public static bool HAVE_NO_MORE_STICKERS = false;

        public static string PROFILE_IMAGE_ALBUM_NAME = "Profile Photos";
        public static string COVER_IMAGE_ALBUM_NAME = "Cover Photos";
        public static string FEED_IMAGE_ALBUM_NAME = "Feed Photos";

        public static bool EMAIL_VERIFICATION_FROM_WALLET = false;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
///<summary>
///for Version Mangagment
///32 bit
///  https://images.ringid.com/official/desktop/ringID_Mini.exe
/// https://images.ringid.com/official/desktop/ringID.exe
///64bit
///  https://images.ringid.com/official/desktop/ringID_Mini.exe
/// https://images.ringid.com/official/desktop/ringID.exe
///</summary>
namespace Models.Constants
{
    public class AppConfig
    {
        public const string DESKTOP_REALEASE_VERSION = "4.4.1.0";
        public const bool FORCE_UPDATE = true;

        //public const string UPDATE_URL = "https://images.ringid.com/official/desktop/ringidUpdate.txt";
        public const string UPDATE_URL = "https://images.ringid.com/official/desktop/updaterMini.txt";
        //  public const string UPDATE_URL = "http://192.168.1.37/ringid/ringidUpdate.txt";
        //Main URL: https://images.ringid.com/official/desktop/ringID.exe
        //Last Mini UrL: https://images.ringid.com/official/desktop/ringID_Mini.exe

        //public static string VERSION_PC = "147";//FOR Production;
        public static string VERSION_PC = "148";//FOR DEV;

        public const string DESKTOP_FIRST_REALEASE_VERSION = "4.3.4.0";
        public const string DESKTOP_VALID_REALEASE_VERSION = "4.4.1.0";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Constants
{
    public static class ChatConstants
    {

        public const int NO_OF_CONCURRENT_FILE_TRANFER = 1;

        //PACKET TYPE
        public const int PACKET_TYPE_FRIEND_MESSAGE = 6; //Custom
        public const int PACKET_TYPE_FRIEND_MESSAGE_EDIT = 7; //Custom
        public const int PACKET_TYPE_GROUP_MESSAGE = 63; //Custom
        public const int PACKET_TYPE_GROUP_MESSAGE_EDIT = 64; //Custom
        public const int PACKET_TYPE_CALL_BUSY_MESSAGE = 26; //Custom
        public const int PACKET_TYPE_ROOM_MESSAGE = 202; //Custom
        public const int PACKET_TYPE_ROOM_MESSAGE_EDIT = 203; //Custom
        public const int PACKET_TYPE_FEED_MESSAGE = 204; //Custom

        //STATUS
        public const int STATUS_SENDING = -1; //Custom
        public const int STATUS_DELETED = 0;
        public const int STATUS_SENT = 1;
        public const int STATUS_DELIVERED = 2;
        public const int STATUS_SEEN = 3;
        public const int STATUS_VIEWED_PLAYED = 4;
        public const int STATUS_PENDING = 21; //Custom
        public const int STATUS_FAILED = 22; //Custom
        public const int STATUS_DISAPPEARED = 23; //Custom

        //MEMEBER
        public const int MEMBER_TYPE_NOT_MEMBER = 0;
        public const int MEMBER_TYPE_MEMBER = 1;
        public const int MEMBER_TYPE_ADMIN = 2;
        public const int MEMBER_TYPE_OWNER = 3;
        public const int MEMBER_TYPE_REMOVED = 4;
        public const int MEMBER_TYPE_LEAVE = 5;

        //MESSAGE TYPE
        public const int TYPE_DELETE_MSG = 0;
        public const int TYPE_BLANK_MSG = 1;
        public const int TYPE_PLAIN_MSG = 2;
        public const int TYPE_EMOTICON_MSG = 3;
        public const int TYPE_LOCATION_MSG = 4;
        public const int TYPE_LINK_MSG = 5;
        public const int TYPE_DOWNLOADED_STICKER_MSG = 6;
        public const int TYPE_IMAGE_FILE_DIRECTORY = 7;
        public const int TYPE_AUDIO_FILE = 8;
        public const int TYPE_VIDEO_FILE = 9;
        public const int TYPE_IMAGE_FILE_RING_CAMERA = 10;
        public const int TYPE_STREAM_FILE = 11;
        public const int TYPE_CONTACT_SHARE = 12;
        public const int TYPE_PAGING_MSG = 14;
        public const int TYPE_RING_MEDIA_MSG = 15;
        public const int TYPE_GROUP_ACTIVITY = 20;

        //HISTORY DIRECTION
        public const int HISTORY_UP = 1;
        public const int HISTORY_DOWN = 2;

        //File
        public const int FILE_DEFAULT = 0; //Custom
        public const int FILE_MOVED = 1; //Custom
        public const int FILE_TRANSFER_COMPLETED = 2; //Custom
        public const int FILE_TRANSFER_CANCELLED = 3; //Custom

        public const int SDK_MEDIA_SIGNAL = 1;
        public const int SDK_MEDIA_AUDIO = 2;
        public const int SDK_MEDIA_VIDEO = 3;
        public const int SDK_MEDIA_FILE = 4;

        public const int SDK_FILE_EVENT_PROGRESS = 500;
        public const int SDK_FILE_EVENT_COMPLETE = 501;
        public const int SDK_FILE_EVENT_CANCELED = 502;
        public const int SDK_FILE_EVENT_FAILED = 503;
        public const int SDK_FILE_EVENT_MENIFEST = 504;
        public const int SDK_FILE_EVENT_ERROR = 505;
        public const int SDK_FILE_EVENT_PAUSED = 509;

        public const int ERROR_FILE_READ = 1001;
        public const int ERROR_INTERNET_UNAVAILABLE = 1002;
        public const int ERROR_CONNECTION_TIMEOOUT = 1003;

        public const int REGISTRATION_HOLD_DURATION = 180000;

        //VIEW PROJECT

        public static int TYPE_COMMON = 0;
        public static int TYPE_FRIEND = 1;
        public static int TYPE_GROUP = 2;
        public static int TYPE_ROOM = 3;

        public const int DAY_TODAY = 0;
        public const int DAY_YESTERDAY = 1;
        public const int DAY_7_DAYS = 6;
        public const int DAY_30_DAYS = 29;
        public const int DAY_90_DAYS = 89;
        public const int DAY_180_DAYS = 179;
        public const int DAY_365_DAYS = 364;

        public const int SUBTYPE_DATE_TITLE = 0;
        public const int SUBTYPE_FRIEND_CHAT = 1;
        public const int SUBTYPE_GROUP_CHAT = 2;
        public const int SUBTYPE_CALL_LOG = 3;
        public const int SUBTYPE_FRIEND_ACTIVITY = 4;
        public const int SUBTYPE_GROUP_ACTIVITY = 5;
        public const int SUBTYPE_ROOM_CHAT = 6;
        public const int SUBTYPE_ROOM_ACTIVITY = 8;

        public const int CHAT_PLAIN_EMOTICON_VIEW = 0;
        public const int CHAT_STICKER_VIEW = 1;
        public const int CHAT_IMAGE_VIEW = 2;
        public const int CHAT_AUDIO_VIEW = 3;
        public const int CHAT_VIDEO_VIEW = 4;
        public const int CHAT_FILE_VIEW = 5;
        public const int CHAT_LOCATION_VIEW = 6;
        public const int CHAT_DELETE_VIEW = 7;
        public const int CHAT_LINK_VIEW = 8;
        public const int CHAT_RING_MEDIA_VIEW = 9;
        public const int CHAT_CONTACT_VIEW = 10;

        //Feed Media Type
        public const int MEDIA_SHARE_AUDIO = 1;
        public const int MEDIA_SHARE_VIDEO = 2;
        public const int MEDIA_SHARE_IMAGE = 3;
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Constants
{
    public static class ActivityConstants
    {

        public const int MSG_GROUP_MEMBER_ADDED_INTO_GROUP = 1;
        public const int MSG_GROUP_MEMBER_REMOVED_FROM_GROUP = 2;
        public const int MSG_GROUP_MEMBER_MADE_ADMIN = 3;
        public const int MSG_GROUP_MEMBER_REMOVE_FROM_ADMIN = 4;
        public const int MSG_GROUP_MEMBER_MADE_OWNER = 5;
        public const int MSG_GROUP_MEMBER_LEFT_CONVERSATION = 6;
        public const int MSG_GROUP_NAME_CHANGE = 7;
        public const int MSG_GROUP_PROFILE_IMAGE_CHANGE = 8;
        public const int MSG_GROUP_CREATE = 9;

    }
}

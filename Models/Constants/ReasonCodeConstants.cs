﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Constants
{
    public class ReasonCodeConstants
    {
        public const int REASON_CODE_NONE = 0;
        public const string REASON_MSG_NONE = "None";

        public const int REASON_CODE_PERMISSION_DENIED = 1;
        public const string REASON_MSG_PERMISSION_DENIED = "Permission denied";

        public const int REASON_CODE_PASSCODE_SENT_WITHIN_ONE_MINUTE = 2;
        public const string REASON_MSG_PASSCODE_SENT_WITHIN_ONE_MINUTE = "Passcode sent within one minute";

        public const int REASON_CODE_DONT_DISTURB_MODE = 3;
        public const string REASON_MSG_DONT_DISTURB_MODE = "Don't disturb mode";

        public const int REASON_CODE_ALREADY_SHARED = 4;
        public const string REASON_MSG_ALREADY_SHARED = "Already shared";

        public const int REASON_CODE_NOT_TAG_MEMBER = 5;
        public const string REASON_MSG_NOT_TAG_MEMBER = "Not tag member";

        public const int REASON_CODE_TAG_DOES_NOT_EXIST = 6;
        public const string REASON_MSG_TAG_DOES_NOT_EXIST = "Tag does not exist";

        public const int REASON_CODE_SMS_SENDING_FAILED = 7;
        public const string REASON_MSG_SMS_SENDING_FAILED = "Sms sending failed";

        public const int REASON_CODE_EMAIL_SENDING_FAILED = 8;
        public const string REASON_MSG_EMAIL_SENDING_FAILED = "Email sending failed";

        public const int REASON_CODE_FRIEND_OFFLINE = 9;
        public const string REASON_MSG_FRIEND_OFFLINE = "Friend offline";

        public const int REASON_CODE_NOT_FRIEND = 10;
        public const string REASON_MSG_NOT_FRIEND = "Not friend";

        public const int REASON_CODE_USERID_FRIENDID_SAME = 11;
        public const string REASON_MSG_USERID_FRIENDID_SAME = "Both UserID and FriendID same";

        public const int REASON_CODE_ALREADY_FRIEND_REQUESTED = 12;
        public const string REASON_MSG_ALREADY_FRIEND_REQUESTED = "Already friend requested";

        public const int REASON_CODE_EXCEPTION_OCCURED = 13;
        public const string REASON_MSG_EXCEPTION_OCCURED = "Exception occured";

        public const int REASON_CODE_DATABASE_ROLL_BACKED = 14;
        public const string REASON_MSG_DATABASE_ROLL_BACKED = "Database roll backed";

        public const int REASON_CODE_CANT_SHARE_OWN_FEED = 15;
        public const string REASON_MSG_CANT_SHARE_OWN_FEED = "You can't share your own status!";

        public const int REASON_CODE_FRIEND_DID_NOT_FOUND = 16;
        public const string REASON_MSG_FRIEND_DID_NOT_FOUND = "Friend did not found!";

        public const int REASON_CODE_NO_MORE_DATA = 18;
        public const string REASON_MSG_NO_MORE_DATA = "No more data";

        public const int REASON_CODE_LOGGED_IN_FROM_ANOTHER_DEVICE = 21;
        public const string REASON_MSG_LOGGED_IN_FROM_ANOTHER_DEVICE = "Logged in from another device";

        public const int REASON_CODE_PASSWORD_DID_NOT_MATCHED = 24;
        public const string REASON_MSG_PASSWORD_DID_NOT_MATCHED = "Wrong password!";

        public const int REASON_CODE_USER_EXIST = 26;
        public const string REASON_MSG_USER_EXIST = "User exists";

        public const int REASON_CODE_DUPLICATE_DATA = 27;
        public const string REASON_MSG_DUPLICATE_DATA = "Duplicate data";

        public const int REASON_CODE_THIS_IS_SPECIAL_FRIEND = 33;

        public const int REASON_CODE_ALREADY_FRIEND = 35;
        public const string REASON_MSG_ALREADY_FRIEND = " is already friend!";

        public const int REASON_CODE_CONTENT_NOT_FOUND = 36;
        public const string REASON_MSG_CONTENT_NOT_FOUND = "Content not found!";

        public const int REASON_CODE_ANONYMOUS_CHAT_BLOCKED = 37;

        public const int REASON_CODE_DEVICE_ID_DID_NOT_MATCHED = 38;
        public const string REASON_MSG_DEVICE_ID_DID_NOT_MATCHED = "Device Unique ID is mismatching!";

        public const int REASON_CODE_PHONE_NUMBER_IS_UNVERIFIED = 39;
        public const string REASON_MSG_PHONE_NUMBER_IS_UNVERIFIED = "Your phone number in not verified!";

        public const int REASON_CODE_EMAIL_IS_UNVERIFIED = 40;
        public const string REASON_MSG_EMAIL_IS_UNVERIFIED = "Your email is not verified!";

        public const int REASON_CODE_FACEBOOK_IS_UNVERIFIED = 41;
        public const string REASON_MSG_FACEBOOK_IS_UNVERIFIED = "Your are using an unverified facebook account!";

        public const int REASON_CODE_TWITTER_IS_UNVERIFIED = 42;
        public const string REASON_MSG_TWITTER_IS_UNVERIFIED = "Your are using an unverified twitter account!";

        public const int REASON_CODE_FACEBOOK_ID_DID_NOT_MATCHED = 43;
        public const string REASON_MSG_FACEBOOK_ID_DID_NOT_MATCHED = "Wrong facebook account!";

        public const int REASON_CODE_TWITTER_ID_DID_NOT_MATCHED = 44;
        public const string REASON_MSG_TWITTER_ID_DID_NOT_MATCHED = "Wrong twitter account!";

        public const int REASON_CODE_INVALID_RINGID = 45;
        public const string REASON_MSG_INVALID_RINGID = "Wrong ringID!";

        public const int REASON_CODE_INVALID_PHONE_NUMBER = 46;
        public const string REASON_MSG_INVALID_PHONE_NUMBER = "Wrong phone number!";

        public const int REASON_CODE_INVALID_EMAIL = 47;
        public const string REASON_MSG_INVALID_EMAIL = "Wrong email!";

        public const int REASON_CODE_DEVICE_UNIQUE_ID_MENDATORY = 48;
        public const string REASON_MSG_DEVICE_UNIQUE_ID_MENDATORY = "Please provide device unique ID.";

        public const int REASON_CODE_PASSWORD_MENDATORY = 49;
        public const string REASON_MSG_PASSWORD_MENDATORY = "Please provide a valid password.";

        public const int REASON_CODE_LOGIN_TYPE_MENDATORY = 50;
        public const string REASON_MSG_LOGIN_TYPE_MENDATORY = "Please provide login type.";

        public const int REASON_CODE_DIALING_CODE_MENDATORY = 51;
        public const string REASON_MSG_DIALING_CODE_MENDATORY = "Please provide dialing code.";

        public const int REASON_CODE_PHONE_NUMBER_MENDATORY = 52;
        public const string REASON_MSG_PHONE_NUMBER_MENDATORY = "Please provide phone number.";

        public const int REASON_CODE_RING_ID_MENDATORY = 53;
        public const string REASON_MSG_RING_ID_MENDATORY = "Please provide ringID.";

        public const int REASON_CODE_EMAIL_MENDATORY = 54;
        public const string REASON_MSG_EMAIL_MENDATORY = "Please provide email.";

        public const int REASON_CODE_VERSION_MENDATORY = 55;
        public const string REASON_MSG_VERSION_MENDATORY = "Version not found.";

        public const int REASON_CODE_DEVICE_MENDATORY = 56;
        public const string REASON_MSG_DEVICE_MENDATORY = "Device not found.";

        public const int REASON_CODE_INVALID_INFORMATION = 57;
        public const string REASON_MSG_INVALID_INFORMATION = "Please provide correct login details!";

        public const int REASON_CODE_FRIENDS_PENDING_FRIEND_LIMIT_OVER = 65;
        public const string REASON_MSG_FRIENDS_PENDING_FRIEND_LIMIT_OVER = "User has too many pending requests! You can not send him add request.";

        public const int REASON_CODE_USER_HAS_TOO_MANY_FRNDS = 66;
        public const string REASON_MSG_USER_HAS_TOO_MANY_FRNDS = "User has too many friends. You can not accept the request.";

        public const int REASON_CODE_STREAM_SERVER_NOT_READY = 84;
        public const string REASON_MSG_STREAM_SERVER_NOT_READY = "Stream is preparing, please try a bit later.";

        public const int REASON_CODE_RE_INITIALIZE_STREAM = 85;
        public const string REASON_MSG_RE_INITIALIZE_STREAM = "Need to reinitialize steaming.";

        public const int REASON_CODE_INCOMING_REQUEST_NOT_ALLOWED = 1002;
        public const string REASON_MSG_INCOMING_REQUEST_NOT_ALLOWED = "Incoming request not allowed!";

        public const int REASON_CODE_NOT_FOUND = 404;
        public const string REASON_MSG_NOT_FOUND = "Not found";

        public const int REASON_CODE_IMAGE_SERVER_UPLOADING_FAILED = 500;
        public const string REASON_MSG_IMAGE_SERVER_UPLOADING_FAILED = "Image server uploading failed";

        public const int REASON_CODE_INTERNET_UNAVAILABLE = 501;
        public const string REASON_MSG_INTERNET_UNAVAILABLE = "No internet connection!";

        public const int REASON_CODE_ALREADY_SAVED = 5009;

        public const int REASON_CODE_INVALID_MEDIA_PRIVACY = 203;

        public static string GetReason(int reason_code)
        {
            string reason = string.Empty;
            switch (reason_code)
            {
                case REASON_CODE_NONE: //0
                    reason = REASON_MSG_NONE;
                    break;
                case REASON_CODE_PERMISSION_DENIED: //1
                    reason = REASON_MSG_PERMISSION_DENIED;
                    break;
                case REASON_CODE_PASSCODE_SENT_WITHIN_ONE_MINUTE: //2
                    reason = REASON_MSG_PASSCODE_SENT_WITHIN_ONE_MINUTE;
                    break;
                case REASON_CODE_DONT_DISTURB_MODE: //3
                    reason = REASON_MSG_DONT_DISTURB_MODE;
                    break;
                case REASON_CODE_ALREADY_SHARED: //4
                    reason = REASON_MSG_ALREADY_SHARED;
                    break;
                case REASON_CODE_NOT_TAG_MEMBER: //5
                    reason = REASON_MSG_NOT_TAG_MEMBER;
                    break;
                case REASON_CODE_TAG_DOES_NOT_EXIST: //6
                    reason = REASON_MSG_TAG_DOES_NOT_EXIST;
                    break;
                case REASON_CODE_SMS_SENDING_FAILED: //7
                    reason = REASON_MSG_SMS_SENDING_FAILED;
                    break;
                case REASON_CODE_EMAIL_SENDING_FAILED: //8
                    reason = REASON_MSG_EMAIL_SENDING_FAILED;
                    break;
                case REASON_CODE_FRIEND_OFFLINE: //9
                    reason = REASON_MSG_FRIEND_OFFLINE;
                    break;
                case REASON_CODE_NOT_FRIEND: //10
                    reason = REASON_MSG_NOT_FRIEND;
                    break;
                case REASON_CODE_USERID_FRIENDID_SAME: //11
                    reason = REASON_MSG_USERID_FRIENDID_SAME;
                    break;
                case REASON_CODE_ALREADY_FRIEND_REQUESTED: //12
                    reason = REASON_MSG_ALREADY_FRIEND_REQUESTED;
                    break;
                case REASON_CODE_EXCEPTION_OCCURED: //13
                    reason = REASON_MSG_EXCEPTION_OCCURED;
                    break;
                case REASON_CODE_DATABASE_ROLL_BACKED: //14
                    reason = REASON_MSG_DATABASE_ROLL_BACKED;
                    break;
                case REASON_CODE_CANT_SHARE_OWN_FEED: //15
                    reason = REASON_MSG_CANT_SHARE_OWN_FEED;
                    break;
                case REASON_CODE_FRIEND_DID_NOT_FOUND: //16
                    reason = REASON_MSG_FRIEND_DID_NOT_FOUND;
                    break;
                case REASON_CODE_NO_MORE_DATA: //18
                    reason = REASON_MSG_NO_MORE_DATA;
                    break;
                case REASON_CODE_LOGGED_IN_FROM_ANOTHER_DEVICE: //21
                    reason = REASON_MSG_LOGGED_IN_FROM_ANOTHER_DEVICE;
                    break;
                case REASON_CODE_PASSWORD_DID_NOT_MATCHED: //26
                    reason = REASON_MSG_PASSWORD_DID_NOT_MATCHED;
                    break;
                case REASON_CODE_DUPLICATE_DATA: //27
                    reason = REASON_MSG_DUPLICATE_DATA;
                    break;
                case REASON_CODE_ALREADY_FRIEND: //35
                    reason = REASON_MSG_ALREADY_FRIEND;
                    break;
                case REASON_CODE_CONTENT_NOT_FOUND: //36
                    reason = REASON_MSG_CONTENT_NOT_FOUND;
                    break;
                case REASON_CODE_DEVICE_ID_DID_NOT_MATCHED: //38
                    reason = REASON_MSG_DEVICE_ID_DID_NOT_MATCHED;
                    break;
                case REASON_CODE_PHONE_NUMBER_IS_UNVERIFIED: //39
                    reason = REASON_MSG_PHONE_NUMBER_IS_UNVERIFIED;
                    break;
                case REASON_CODE_EMAIL_IS_UNVERIFIED: //40
                    reason = REASON_MSG_EMAIL_IS_UNVERIFIED;
                    break;
                case REASON_CODE_FACEBOOK_IS_UNVERIFIED: //41
                    reason = REASON_MSG_FACEBOOK_IS_UNVERIFIED;
                    break;
                case REASON_CODE_TWITTER_IS_UNVERIFIED: //42
                    reason = REASON_MSG_TWITTER_IS_UNVERIFIED;
                    break;
                case REASON_CODE_FACEBOOK_ID_DID_NOT_MATCHED: //43
                    reason = REASON_MSG_FACEBOOK_ID_DID_NOT_MATCHED;
                    break;
                case REASON_CODE_TWITTER_ID_DID_NOT_MATCHED: //44
                    reason = REASON_MSG_TWITTER_ID_DID_NOT_MATCHED;
                    break;
                case REASON_CODE_INVALID_RINGID: //45
                    reason = REASON_MSG_INVALID_RINGID;
                    break;
                case REASON_CODE_INVALID_PHONE_NUMBER: //46
                    reason = REASON_MSG_INVALID_PHONE_NUMBER;
                    break;
                case REASON_CODE_INVALID_EMAIL: //47
                    reason = REASON_MSG_INVALID_EMAIL;
                    break;
                case REASON_CODE_DEVICE_UNIQUE_ID_MENDATORY: //48
                    reason = REASON_MSG_DEVICE_UNIQUE_ID_MENDATORY;
                    break;
                case REASON_CODE_PASSWORD_MENDATORY: //49
                    reason = REASON_MSG_PASSWORD_MENDATORY;
                    break;
                case REASON_CODE_LOGIN_TYPE_MENDATORY: //50
                    reason = REASON_MSG_LOGIN_TYPE_MENDATORY;
                    break;
                case REASON_CODE_DIALING_CODE_MENDATORY: //51
                    reason = REASON_MSG_DIALING_CODE_MENDATORY;
                    break;
                case REASON_CODE_PHONE_NUMBER_MENDATORY: //52
                    reason = REASON_MSG_PHONE_NUMBER_MENDATORY;
                    break;
                case REASON_CODE_RING_ID_MENDATORY: //53
                    reason = REASON_MSG_RING_ID_MENDATORY;
                    break;
                case REASON_CODE_EMAIL_MENDATORY: //54
                    reason = REASON_MSG_EMAIL_MENDATORY;
                    break;
                case REASON_CODE_VERSION_MENDATORY: //55
                    reason = REASON_MSG_VERSION_MENDATORY;
                    break;
                case REASON_CODE_DEVICE_MENDATORY: //56
                    reason = REASON_MSG_DEVICE_MENDATORY;
                    break;
                case REASON_CODE_INVALID_INFORMATION: //57
                    reason = REASON_MSG_INVALID_INFORMATION;
                    break;
                case REASON_CODE_USER_HAS_TOO_MANY_FRNDS: //66
                    reason = REASON_MSG_USER_HAS_TOO_MANY_FRNDS;
                    break;
                case REASON_CODE_INCOMING_REQUEST_NOT_ALLOWED: //1002
                    reason = REASON_MSG_INCOMING_REQUEST_NOT_ALLOWED;
                    break;
                case REASON_CODE_NOT_FOUND: //404
                    reason = REASON_MSG_NOT_FOUND;
                    break;
                case REASON_CODE_IMAGE_SERVER_UPLOADING_FAILED: //500
                    reason = REASON_MSG_IMAGE_SERVER_UPLOADING_FAILED;
                    break;
                case REASON_CODE_INTERNET_UNAVAILABLE: //501
                    reason = REASON_MSG_INTERNET_UNAVAILABLE;
                    break;
                default:
                    reason = "No Reason";
                    break;
            }

            return reason;
        }

    }
}

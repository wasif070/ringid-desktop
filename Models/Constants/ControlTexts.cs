﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Constants
{
    public class ControlTexts
    {
        public static readonly string TEXT_CALL = "Call";
        public static readonly string TEXT_VIDEO_CALL = "Video Call";
        public static readonly string TEXT_CALL_END = "Call End";
        public static readonly string TEXT_VIEW_PROFILE = "View Profile";
        public static readonly string TEXT_SEND_IM = "Send IM";

        public static readonly string TEXT_EXIT = "Exit";
        public static readonly string TEXT_ADD_TO_ALBUM = "Add to album";
        public static readonly string TEXT_ADD_TO_MY_CHANNEL = "Add to my Channel";

        public static readonly string TEXT_MAKE_PROFILE = "Make Profile Picture";
        public static readonly string TEXT_MAKE_COVER_PHOTO = "Make Cover Photo";
        public static readonly string TEXT_DOWNLOAD = "Download";
        public static readonly string TEXT_REPORT_IMAGE = "Report Image";
        public static readonly string TEXT_DELETE_PHOTO = "Delete photo";

        public static readonly string TEXT_SEND_VIA_CHAT = "Send via Chat";
        public static readonly string TEXT_SHARE_ON_FACEBOOK = "Share on facebook";
        public static readonly string TEXT_COPY_LINK = "Copy link";
        public static readonly string TEXT_REPORT_MEDIA_CONTENT = "Report Media Content";
        public static readonly string TEXT_PLAYLIST = "Show Playlist";
        public static readonly string TEXT_HIDE_PLAYLIST = "Hide Playlist";
        public static readonly string TEXT_SHOW_MORE = "SHOW MORE";
        public static readonly string TEXT_SHOW_MORE_COMMENTS = "Show more comments";
        public static readonly string TEXT_LOADING_MORE_SUGGESTIONS = "LOADING MORE ...";
        public static readonly string TEXT_LOADING_COMMENTS = "LOADING COMMENTS ...";
        public static readonly string TEXT_SHARE = "Share";
        public static readonly string TEXT_LIKE = "Like";
        public static readonly string TEXT_COMMENT = "Comment";

        public static readonly string TEXT_PHONE_NUMBER = "Phone Number";
        public static readonly string TEXT_EMAIL = "Email";
        public static readonly string TEXT_FACEBOOK = "Facebook";
        public static readonly string TEXT_TWITTER = "Twitter";

    }
}

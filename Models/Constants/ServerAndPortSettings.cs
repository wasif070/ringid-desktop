﻿
namespace Models.Constants
{
    public class ServerAndPortSettings
    {
        #region "Frields"
        public readonly static string COMMUNICATION_SERVER_HOST = "auth.ringid.com";//devauth.ringid.com
        public static string STICKER_MARKET_SERVICE_NEW_API_HOST = "devauth.ringid.com";
        public static string AUTHENTICATION_CONTROLLER = null;
        public static string IMAGE_SERVER_UPLOAD_API = null;
        public static string IMAGE_SERVER_RESOURCE = null;
        public static string STICKER_MARKET_API = null;
        public static string STICKER_MARKET_RESOURCE = null;
        public static string VOD_SERVER_UPLOAD_API = null;
        public static string VOD_SERVER_RESOURCE = null;
        public static string BASE_WEB_URL = null;
        public static string AUTH_SERVER_IP = null;
        public static string WALLET_PAYMENT_GATEWAY_URL = null;
        public static int COMMUNICATION_PORT = 0;
        public static int DATA_SIZE = 500;
        public static int HEADER_SIZE = 12;
        public static bool IsBaseRacSuccess = false;
        #endregion "Fields"

        #region "Property"

        public static string D_MINI
        {
            get { return "dmini"; }
        }

        public static string D_FULL
        {
            get { return "dfull"; }
        }

        public static string GetCommunicationServer
        {
            get { return "http://" + COMMUNICATION_SERVER_HOST + "/cnstnts"; }
        }

        public static string GetAuthServer
        {
            get { return AUTHENTICATION_CONTROLLER; }
        }

        public static string GetComportURL
        {
            get { return GetAuthServer + "comports"; }
        }

        public static string GetDigitAccessTokenURL
        {
            get { return BASE_WEB_URL + "token"; }
        }

        public static string MediaShareBase
        {
            get { return BASE_WEB_URL + "player/embed?id="; }
        }

        public static string StreamShareBaseUrl
        {
            get { return "https://dev.ringid.com/liveProfile?"; }
        }

        #endregion "Property"

        #region image

        public static string GetProfileImageUploadingURL
        {
            get { return IMAGE_SERVER_UPLOAD_API + "ringmarket/ImageUploadHandler"; }
        }

        public static string GetCropImageUploadingURL
        {
            get { return IMAGE_SERVER_UPLOAD_API + "ipvringmarket/CropImageHandler"; }
        }

        public static string GetGroupImageUploadingURL
        {
            get { return IMAGE_SERVER_UPLOAD_API + "ringmarket/GroupContentHandler"; }
        }

        public static string GetAlbumImageUploadingURL
        {
            get { return IMAGE_SERVER_UPLOAD_API + "ringmarket/AlbumImageUploadHandler"; }
        }

        public static string GetCommentImageUploadingURL
        {
            get { return IMAGE_SERVER_UPLOAD_API + "ringmarket/CommentImageHandler"; }
        }

        public static string GetCommentAudioUploadingURL
        {
            get { return IMAGE_SERVER_UPLOAD_API + "ringmarket/CommentMp3Handler"; }
        }

        public static string GetCommentVideoUploadingURL
        {
            get { return IMAGE_SERVER_UPLOAD_API + "ringmarket/CommentMp4Handler"; }
        }

        public static string GetImageServerResourceURL
        {
            get { return IMAGE_SERVER_RESOURCE; }
        }

        public static string GetFeelingMiniURL
        {
            get { return IMAGE_SERVER_RESOURCE + "emoticon/d1/dmini/"; }
        }

        public static string GetFeelingFullURL
        {
            get { return IMAGE_SERVER_RESOURCE + "emoticon/d1/dfull/"; }
        }

        public static string ChatImageUploadingUrl
        {
            get { return IMAGE_SERVER_UPLOAD_API + "ringmarket/ChatImageHandler"; }
        }

        public static string ChatMP3UploadingUrl
        {
            get { return IMAGE_SERVER_UPLOAD_API + "ringmarket/Mp3Handler"; }
        }

        public static string ChatMP4UploadingUrl
        {
            get { return IMAGE_SERVER_UPLOAD_API + "ringmarket/Mp4Handler"; }
        }

        public static string ChatSharedFileUploadingUrl
        {
            get { return IMAGE_SERVER_UPLOAD_API; }
        }

        public static string ChatSharedFileDownloadingUrl
        {
            get { return IMAGE_SERVER_RESOURCE; }
        }

        #endregion

        #region VOD

        public static string GetAudioUploadingURL
        {
            get { return VOD_SERVER_UPLOAD_API + "stream/Mp3UploadHandler"; }
        }

        public static string GetVideoUploadingURL { get { return VOD_SERVER_UPLOAD_API + "stream/rMp4UploadHandler"; } }
        //public static string GetVideoUploadingURL { get { return VOD_SERVER_UPLOAD_API + "stream/Mp4UploadHandler"; } }

        public static string GetChannelVideoUploadingURL { get { return VOD_SERVER_UPLOAD_API + "stream/ChannelMP4Handler"; } }

        public static string GetVODThumbImageUploadingURL
        {
            get { return VOD_SERVER_UPLOAD_API + "stream/ThumbImageHandler"; }
        }

        public static string GetVODThumbImageUpResourceURL
        {
            get { return VOD_SERVER_RESOURCE; }
        }

        public static string GetVODServerResourceURL
        {
            get { return VOD_SERVER_RESOURCE; }
        }
        #endregion

        #region sticker

        public static string ChatBackgroundImageServiceUrl
        {
            get { return STICKER_MARKET_API + "ringmarket/ChatBgHandler"; }
        }

        public static string ChatBackgroundImageDownloadUrl
        {
            get { return STICKER_MARKET_RESOURCE + "chatbg/d1"; }
        }

        public static string StickerMarketServiceUrl
        {
            get { return STICKER_MARKET_API + "ringmarket/StickerHandler"; }
        }

        public static string StickerMarketServiceUrlForNewStickerCategoryIDs
        {
            get { return STICKER_MARKET_API + "ringmarket/NewStickerCounter"; }
        }

        public static string StickerMarketServiceUrlForIncreaseDownloadCount
        {
            get { return STICKER_MARKET_API + "ringmarket/StickerDownload"; }
        }

        public static string StickerMarketIServiceUrl
        {
            get { return STICKER_MARKET_API + "ringmarket/IStickeHandler"; }
        }

        public static string StickerMarketDownloadUrl
        {
            get { return STICKER_MARKET_RESOURCE + "stickermarket/d1"; }
        }

        public static string StreamGiftDownloadUrl
        {
            get { return IMAGE_SERVER_UPLOAD_API + "ringStore/StoreImageHandler/gift/d1"; }
        }

        #endregion
    }
}

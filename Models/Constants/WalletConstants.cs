<<<<<<< HEAD
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Constants
{
    public static class WalletConstants
    {
        //json keys
        //1026
        public const string RSP_KEY_MY_WALLET_INFO = "info";
        public const string RSP_KEY_MY_COIN = "my_coin"; // 1046
        public const string RSP_KEY_UTID = "userId";
        public const string RSP_KEY_COIN_NAME = "coinName";//1038
        public const string RSP_KEY_QUANTITY = "quantity"; // in 1039, 1044
        public const string RSP_KEY_COIN_ID = "coinId"; // 1038

        //1039
        public const string RSP_KEY_PRODUCTS = "products";
        public const string RSP_KEY_PRODUCT_ID = "productId";
        public const string RSP_KEY_PRODUCT_NAME = "productName";
        public const string RSP_KEY_PRODUCT_TYPE_ID = "productTypeId";
        public const string RSP_KEY_PRODUCT_CATEGORY_ID = "productCategoryId";
        public const string RSP_KEY_PRODUCT_PRICE_COIN_ID = "productPriceCoinId";
        public const string RSP_KEY_PRODUCT_PRICE_COIN_QUANTITY = "productPriceCoinQuantity";
        public const string RSP_KEY_PRODUCT_PRICE = "productPrice";
        public const string RSP_KEY_PRODUCT_ICON = "productIcon";
        public const string RSP_KEY_ACTIVE = "active"; // 1038
        public const string RSP_KEY_IS_APPLICABLE_FOR_ALL_STORES = "isApplicableForAllStores";
        
        //1038
        public const string REQ_KEY_COIN_BUNDLE_TYPE = "cnBnType";
        public const string REQ_KEY_PAYMENT_METHOD_TYPE = "paymentMethodTypeId";
        public const string RSP_KEY_COIN_BUNDLES = "coinBundle";
        public const string RSP_KEY_BUNDLE_TYPE_ID = "bundleTypeId";
        public const string RSP_KEY_BUNDLE_TYPE_NAME = "bundleTypeName";
        //public const string KEYCOINID = "coinId";
        //public const string KEYCOINNAME = "coinName";
        public const string RSP_KEY_CURRENCY_ISO_CODE = "currencyISOCode";
        public const string RSP_KEY_NETWORK_ID = "networkId";
        public const string RSP_KEY_COIN_BUNDLE_ID = "coinBundleId";
        public const string RSP_KEY_COIN_QUANTITY = "coinQuantity"; // 1045
        public const string RSP_KEY_FREE_COIN_QUANTITY = "freeCoinQuantity";
        public const string RSP_KEY_CUSTOM_MESSAGE = "customMessage";
        public const string RSP_KEY_BUNDLE_PRICE = "bundlePrice";
        //public const string KEYACTIVE = "active";
        public const string RSP_KEY_IS_APPLICABLE_FOR_FIRST_TIME = "isApplicableForFirstTime";
        public const string RSP_KEY_EXCHANGE_RATE = "exchangeRate";
        

        //1046
        public const string RSP_KEY_PAYMENT_RECEIVED_WALLET_INFO = "walletInfo";

        //1045
        public const string RSP_KEY_RULES_LIST = "rulesList";
        public const string RSP_KEY_RULE_ITEM_ID = "ruleItemId";
        public const string RSP_KEY_RULE_NAME = "ruleName";
        public const string RSP_KEY_RULE_DESCRIPTION = "description";
        //public const string KEYCOINQUANTITY = "coinQuantity";

        //1041
        public const string RSP_KEY_MY_REFERRER_STAT = "myRefSts";
        public const string RSP_KEY_REFERRER_ID = "refId";
        public const string RSP_KEY_REFERRAL_COUNT = "refCount";

        //1042
        public const string RSP_KEY_REFERRAL_LIST = "reflist";

        //1044
        public const string RSP_KEY_TOP_CONTRIBUTORS = "topContributors";
        public const string RSP_KEY_CONTRIBUTOR_USER_ID = "fromUserId";

        //1043
        public const string REQ_KEY_REMARK = "rmk"; //1040
        public const string REQ_KEY_CURRENCY_ISO = "curnIso";
        public const string RR_KEY_CASH_AMOUNT = "cashAmnt";
        public const string REQ_KEY_EXCHANGE_RATE = "exchRt";
        public const string REQ_KEY_COIN_QUANTITY = "nocn";
        public const string REQ_KEY_COIN_BUNDLE_ID = "cnReBnId";
        public const string REQ_KEY_COIN_ID = "cnId";
        public const string RSP_KEY_TRANSACTION_ID = "tranId";

        //1040
        public const string REQ_KEY_SENDER_UTID = "toUtId";
        public const string REQ_KEY_GIFT_SALE_DETAILS = "giftSaleDetail";

        //1050
        public const string REQ_KEY_DWELL_DATE = "dtStr";
        public const string REQ_KEY_DWELL_TIME_IN_MINUTES = "tm";
        public const string RSP_KEY_SERVER_TIME = "srTm";

        //1049
        public const string RSP_KEY_CHECKED_IN_AWARDED_QUANTITY = "awardedQuantity";
        public const string RSP_KEY_ALREADY_CHECKED_IN = "alreadyCheckedin";

        //1055
        public const string RSP_KEY_DAILY_CHECK_IN_TYPE_ID = "dailyCheckInDayTypeId";
        public const string RSP_KEY_DAY_NUMBER = "dayNumber";

        //1051
        public const string REQ_KEY_ENCRYPTED_CONTENT_ID = "cntntIdHs";
        public const string REQ_KEY_SOCIAL_MEDIA_TYPE = "smdaT";

        public const int SOCIAL_MEDIA_DEFAULT = 0;
        public const int SOCIAL_MEDIA_FACEBOOK = 1;

        public const int MEDIA_DEFAULT = 0;

        public const int COIN_EARNING_RULE_INVITE_FRIENDS = 1;
        public const int COIN_EARNING_RULE_DAILY_CHECK_IN = 2;
        public const int COIN_EARNING_RULE_MORE_SHARE = 3;

        public const int COIN_ID_GOLD = 1;
        public const int COIN_ID_DEFAULT = -1;

        public const int CHECK_IN_TYPE_DEFAULT = 0;
        public const int CHECK_IN_TYPE_DAILY = 1;
        public const int CHECK_IN_TYPE_CONSECUTIVE = 2;
        public const int CHECK_IN_TYPE_GIFT = 4;

        public const int CHECKED_IN_TODAY = 1;
        public const int CHECKED_IN_PREVIOUS = 2;
        public const int GIFT_DAY = 3;
        public const int UPCOMING = 4;
        public const int RECENT_MISSED = 5;
        public const int BREAK_CYCLE_DAY = 6;

        public const int PAYMENT_METHOD_DEFAULT = 0;
        public const int PAYMENT_METHOD_MOBILE_BANKING = 1;
        public const int PAYMENT_METHOD_INTERNET_BANKING = 2;
        public const int PAYMENT_METHOD_BANK_CARDS = 3;
        public const int PAYMENT_METHOD_SSL_COMMERZ = 1;
        public const int PAYMENT_METHOD_PAYPAL = 5;

        public const int COIN_BUNDLE_TYPE_BUY = 1;
        public const int COIN_BUNDLE_TYPE_SALE = 2;

        public const int PU_COIN_BUNDLE_PANEL = 1;
        public const int PU_ADD_REFERRER = 2;
        public const int PU_SET_REFERRER = 3;
        public const int PU_REFERRAL_LIST = 4;
        public const int PU_CHECK_IN_SUCCESS = 5;
        public const int PU_CHECK_IN_FAIL = 6;
        public const int PU_INVITE = 7;
        public const int PU_COIN_RECHARGE_METHODS = 8;
        public const int PU_MEDIA_SHARE_MESSAGE = 9;
        public const int PU_WALLET_INFO = 10;
    }
}
=======
﻿
namespace Models.Constants
{
    public static class WalletConstants
    {
        //json keys
        //1026
        public const string RSP_KEY_MY_WALLET_INFO = "info";
        public const string RSP_KEY_MY_COIN = "my_coin"; // 1046
        public const string RSP_KEY_UTID = "userId";
        public const string RSP_KEY_COIN_NAME = "coinName";//1038
        public const string RSP_KEY_QUANTITY = "quantity"; // in 1039, 1044
        public const string RSP_KEY_COIN_ID = "coinId"; // 1038

        //1039
        public const string RSP_KEY_PRODUCTS = "products";
        public const string RSP_KEY_PRODUCT_ID = "productId";
        public const string RSP_KEY_PRODUCT_NAME = "productName";
        public const string RSP_KEY_PRODUCT_TYPE_ID = "productTypeId";
        public const string RSP_KEY_PRODUCT_CATEGORY_ID = "productCategoryId";
        public const string RSP_KEY_PRODUCT_PRICE_COIN_ID = "productPriceCoinId";
        public const string RSP_KEY_PRODUCT_PRICE_COIN_QUANTITY = "productPriceCoinQuantity";
        public const string RSP_KEY_PRODUCT_PRICE = "productPrice";
        public const string RSP_KEY_PRODUCT_ICON = "productIcon";
        public const string RSP_KEY_ACTIVE = "active"; // 1038
        public const string RSP_KEY_IS_APPLICABLE_FOR_ALL_STORES = "isApplicableForAllStores";
        
        //1038
        public const string REQ_KEY_COIN_BUNDLE_TYPE = "cnBnType";
        public const string REQ_KEY_PAYMENT_METHOD_TYPE = "paymentMethodTypeId";
        public const string RSP_KEY_COIN_BUNDLES = "coinBundle";
        public const string RSP_KEY_BUNDLE_TYPE_ID = "bundleTypeId";
        public const string RSP_KEY_BUNDLE_TYPE_NAME = "bundleTypeName";
        //public const string KEYCOINID = "coinId";
        //public const string KEYCOINNAME = "coinName";
        public const string RSP_KEY_CURRENCY_ISO_CODE = "currencyISOCode";
        public const string RSP_KEY_NETWORK_ID = "networkId";
        public const string RSP_KEY_COIN_BUNDLE_ID = "coinBundleId";
        public const string RSP_KEY_COIN_QUANTITY = "coinQuantity"; // 1045
        public const string RSP_KEY_FREE_COIN_QUANTITY = "freeCoinQuantity";
        public const string RSP_KEY_CUSTOM_MESSAGE = "customMessage";
        public const string RSP_KEY_BUNDLE_PRICE = "bundlePrice";
        //public const string KEYACTIVE = "active";
        public const string RSP_KEY_IS_APPLICABLE_FOR_FIRST_TIME = "isApplicableForFirstTime";
        public const string RSP_KEY_EXCHANGE_RATE = "exchangeRate";
        

        //1046
        public const string RSP_KEY_PAYMENT_RECEIVED_WALLET_INFO = "walletInfo";

        //1045
        public const string RSP_KEY_RULES_LIST = "rulesList";
        public const string RSP_KEY_RULE_ITEM_ID = "ruleItemId";
        public const string RSP_KEY_RULE_NAME = "ruleName";
        public const string RSP_KEY_RULE_DESCRIPTION = "description";
        //public const string KEYCOINQUANTITY = "coinQuantity";

        //1041
        public const string RSP_KEY_MY_REFERRER_STAT = "myRefSts";
        public const string RSP_KEY_REFERRER_ID = "refId";
        public const string RSP_KEY_REFERRAL_COUNT = "refCount";

        //1042
        public const string RSP_KEY_REFERRAL_LIST = "reflist";

        //1044
        public const string RSP_KEY_TOP_CONTRIBUTORS = "topContributors";
        public const string RSP_KEY_CONTRIBUTOR_USER_ID = "fromUserId";

        //1043
        public const string REQ_KEY_REMARK = "rmk"; //1040
        public const string REQ_KEY_CURRENCY_ISO = "curnIso";
        public const string RR_KEY_CASH_AMOUNT = "cashAmnt";
        public const string REQ_KEY_EXCHANGE_RATE = "exchRt";
        public const string REQ_KEY_COIN_QUANTITY = "nocn";
        public const string REQ_KEY_COIN_BUNDLE_ID = "cnReBnId";
        public const string REQ_KEY_COIN_ID = "cnId";
        public const string RSP_KEY_TRANSACTION_ID = "tranId";

        //1040
        public const string REQ_KEY_SENDER_UTID = "toUtId";
        public const string REQ_KEY_GIFT_SALE_DETAILS = "giftSaleDetail";

        //1050
        public const string REQ_KEY_DWELL_DATE = "dtStr";
        public const string REQ_KEY_DWELL_TIME_IN_MINUTES = "tm";
        public const string RSP_KEY_SERVER_TIME = "srTm";

        //1049
        public const string RSP_KEY_CHECKED_IN_AWARDED_QUANTITY = "awardedQuantity";
        public const string RSP_KEY_ALREADY_CHECKED_IN = "alreadyCheckedin";

        //1055
        public const string RSP_KEY_DAILY_CHECK_IN_TYPE_ID = "dailyCheckInDayTypeId";
        public const string RSP_KEY_DAY_NUMBER = "dayNumber";

        //1051
        public const string REQ_KEY_ENCRYPTED_CONTENT_ID = "cntntIdHs";
        public const string REQ_KEY_SOCIAL_MEDIA_TYPE = "smdaT";

        public const int SOCIAL_MEDIA_DEFAULT = 0;
        public const int SOCIAL_MEDIA_FACEBOOK = 1;

        public const int MEDIA_DEFAULT = 0;

        public const int COIN_EARNING_RULE_INVITE_FRIENDS = 1;
        public const int COIN_EARNING_RULE_DAILY_CHECK_IN = 2;
        public const int COIN_EARNING_RULE_MORE_SHARE = 3;

        public const int COIN_ID_GOLD = 1;
        public const int COIN_ID_DEFAULT = -1;

        public const int CHECK_IN_TYPE_DEFAULT = 0;
        public const int CHECK_IN_TYPE_DAILY = 1;
        public const int CHECK_IN_TYPE_CONSECUTIVE = 2;
        public const int CHECK_IN_TYPE_GIFT = 4;

        public const int CHECKED_IN_TODAY = 1;
        public const int CHECKED_IN_PREVIOUS = 2;
        public const int GIFT_DAY = 3;
        public const int UPCOMING = 4;
        public const int RECENT_MISSED = 5;
        public const int BREAK_CYCLE_DAY = 6;

        public const int PAYMENT_METHOD_DEFAULT = 0;
        public const int PAYMENT_METHOD_MOBILE_BANKING = 1;
        public const int PAYMENT_METHOD_INTERNET_BANKING = 2;
        public const int PAYMENT_METHOD_BANK_CARDS = 3;
        public const int PAYMENT_METHOD_SSL_COMMERZ = 1;
        public const int PAYMENT_METHOD_PAYPAL = 5;

        public const int COIN_BUNDLE_TYPE_BUY = 1;
        public const int COIN_BUNDLE_TYPE_SALE = 2;

        public const int PU_COIN_BUNDLE_PANEL = 1;
        public const int PU_ADD_REFERRER = 2;
        public const int PU_SET_REFERRER = 3;
        public const int PU_REFERRAL_LIST = 4;
        public const int PU_CHECK_IN_SUCCESS = 5;
        public const int PU_CHECK_IN_FAIL = 6;
        public const int PU_INVITE = 7;
        public const int PU_COIN_RECHARGE_METHODS = 8;
        public const int PU_MEDIA_SHARE_MESSAGE = 9;
        public const int PU_WALLET_INFO = 10;
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

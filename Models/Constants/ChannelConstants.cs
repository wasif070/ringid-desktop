﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Constants
{
    public static class ChannelConstants
    {
        public const int MEDIA_TYPE_AUDIO = 1;
        public const int MEDIA_TYPE_VIDEO = 2;

        public const int ACTIVATED = 1;
        public const int DEACTIVATED = 2;

        public const int MEDIA_STATUS_PENDING = 1;
        public const int MEDIA_STATUS_PUBLISHED = 2;
        public const int MEDIA_STATUS_DELETED = 3;

        public const int SUBSCRIBE = 1;
        public const int UNSUBSCRIBE = 2;

        public const int _BACK_TO_CHANNEL_LIST = 1;
        public const int _BACK_TO_CREATE_CHANNEL = 2;
        public const int STREAM_BACK_TO_COUNTRY = 3;

        public const long CHANNEL_MEDIA_DURATION_OFFSET_5_MIN = 300000;
        public const long CHANNEL_MEDIA_DURATION_OFFSET_10_MIN = 600000;
        public const long CHANNEL_MEDIA_DURATION_OFFSET_1_HOUR = 3600000;

        public const int CHANNEL_INFO = 1;
        public const int CHANNEL_PRO_IMAGE = 2;
        public const int CHANNEL_COVER_IMAGE = 3;

        public const int CHANNEL_VIDEO_UPLOAD_MINIMUM_RESOLUTION = 720;
        public const int CHANNEL_MEDIA_UPLOAD_AUDIO_BIT_RATE = 112000;
        public const int CHANNEL_MEDIA_UPLOAD_AUDIO_SAMPLE_RATE = 44100;

    }
}

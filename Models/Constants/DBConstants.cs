﻿using System;
using System.IO;


namespace Models.Constants
{
    public class DBConstants
    {
        public static String DB_NAME = "data.ringID";
        /**/
        public static readonly String ID = "ID";
        public static readonly String CALL_ID = "CALL_ID";
        public static readonly String RING_ID = "RING_ID";
        public static readonly String USER_TABLE_ID = "USER_TABLE_ID";
        public static readonly String SENDER_TABLE_ID = "SENDER_TABLE_ID";
        public static readonly String FULL_NAME = "FULL_NAME";
        public static readonly String FRIEND_TABLE_ID = "FRIEND_TABLE_ID";
        public static readonly String MESSAGE = "MESSAGE";
        public static readonly String MESSAGE_DATE = "MESSAGE_DATE";
        public static readonly String MESSAGE_DELIEVER_DATE = "MESSAGE_DELIEVER_DATE";
        public static readonly String MESSAGE_SEEN_DATE = "MESSAGE_SEEN_DATE";
        public static readonly String MESSAGE_VIEW_DATE = "MESSAGE_VIEW_DATE";
        public static readonly String IS_UNREAD = "IS_UNREAD";
        public static readonly String MESSAGE_TYPE = "MESSAGE_TYPE";
        public static readonly String FILE_STATUS = "FILE_STATUS";
        public static readonly String FILE_ID = "FILE_ID";
        public static readonly String TABLE_TYPE = "TABLE_TYPE";
        public static readonly String STATUS = "STATUS";
        public static readonly String PACKET_TYPE = "PACKET_TYPE";
        public static readonly String TIME_OUT = "TIME_OUT";
        public static readonly String PACKET_ID = "PACKET_ID";
        public static readonly String ACTIVITY_TYPE = "ACTIVITY_TYPE";
        public static readonly String MEMBER_LIST = "MEMBER_LIST";
        public static readonly String ACTIVITY_BY = "ACTIVITY_BY";

        /*tables start*/
        public static readonly String TABLE_RING_LOGIN_SETTINGS = "RING_LOGIN_SETTINGS";
        public static readonly String LOGIN_KEY = "LOGIN_KEY";
        public static readonly String LOGIN_VALUE = "LOGIN_VALUE";

        public static readonly String SETTINGS_KEY = "KEY";
        public static readonly String SETTINGS_VALUE = "VALUE";

        public static readonly String TABLE_RING_FRIEND_CHAT = "RING_FRIEND_CHAT_";
        public static readonly String TABLE_RING_GROUP_CHAT = "RING_GROUP_CHAT_";
        public static readonly String TABLE_RING_ROOM_CHAT = "RING_ROOM_CHAT_";
        public static readonly String TABLE_RING_CIRCLE_MEMBERS_LIST = "RING_CIRCLE_MEMBERS_LIST";
        public static readonly String TABLE_RING_CIRCLE_LIST = "RING_CIRCLE_LIST";
        public static readonly String TABLE_RING_USER_BASIC_INFO = "RING_USER_BASIC_INFO";
        public static readonly String TABLE_RING_CONTACT_LIST = "RING_CONTACT_LIST_";
        public static readonly String TABLE_RING_ALL_USERS_SETTINGS = "RING_ALL_USERS_SETTINGS";
        public static readonly String TABLE_RING_CALL_LOG = "RING_CALL_LOG_";
        public static readonly String TABLE_RING_EMOTICONS = "RING_EMOTICONS";
        public static readonly String TABLE_RING_INSTANT_MESSAGES = "RING_INSTANT_MESSAGES";
        public static readonly String TABLE_RING_NOTIFICATION_HISTORY = "RING_NOTIFICATION_HISTORY";
        public static readonly String TABLE_RING_NOTIFICATION_COUNT = "RING_NOTIFICATION_COUNT";
        public static readonly String TABLE_RING_MARKET_STICKER_CATEGORY = "RING_MARKET_STICKER_CATEGORY";
        public static readonly String TABLE_RING_MARKET_STICKER_COLLECTION = "RING_MARKET_STICKER_COLLECTION";
        public static readonly String TABLE_RING_MARKET_STICKER = "RING_MARKET_STICKER";
        public static readonly String TABLE_RING_ACTIVITY = "RING_ACTIVITY_";
        public static readonly String TABLE_RING_GROUP_LIST = "RING_GROUP_LIST_";
        public static readonly String TABLE_RING_GROUP_MEMBER_LIST = "RING_GROUP_MEMBER_LIST_";
        public static readonly String TABLE_RING_ROOM_LIST = "RING_ROOM_LIST_";
        public static readonly String TABLE_RING_DATE_EVENT = "RING_DATE_EVENT";
        public static readonly String TABLE_RING_DOING = "RING_DOING";
        public static readonly String TABLE_RING_RECENT_MEDIA = "RING_RECENT_MEDIA";
        public static readonly String TABLE_RING_DOWNLOAD_MEDIA = "RING_DOWNLOAD_MEDIA";
        public static readonly String TABLE_RING_MEDIA_SEARCH = "RING_MEDIA_SEARCH";
        public static readonly String TABLE_RING_CHAT_BG_LIST = "RING_CHAT_BG_LIST";
        public static readonly String TABLE_RING_HIDDEN_POSTS_LIST = "RING_HIDDEN_POSTS_LIST_";
        public static readonly String TABLE_RING_HIDDEN_POSTS_USERS_LIST = "RING_HIDDEN_POSTS_USERS_LIST_";
        public static readonly String TABLE_RING_BLOCKED_NON_FRIEND = "RING_BLOCKED_NON_FRIEND";
        public static readonly String TABLE_RING_ROOM_CATEGORY = "RING_ROOM_CATEGORY";
        public static readonly String TABLE_RING_OTHERS_SETTINGS = "RING_OTHERS_SETTINGS";
        /*tables end*/

        public static readonly String LOGIN_TABLE_ID = "LOGIN_TABLE_ID";
        public static readonly String EMAIL = "EMAIL";//                    VARCHAR     50                                    YES              
        public static readonly String GENDER = "GENDER";//                 VARCHAR     50                                    YES              
        public static readonly String MOBILE_PHONE = "MOBILE_PHONE";//               VARCHAR     20                                    YES              
        public static readonly String COUNTRY = "COUNTRY";//               VARCHAR     50                                    YES              
        public static readonly String HOME_CITY = "HOME_CITY";
        public static readonly String CURRENT_CITY = "CURRENT_CITY";
        public static readonly String MARRIAGE_DAY = "MARRIAGE_DAY";
        public static readonly String ABOUT_ME = "ABOUT_ME";
        public static readonly String BIRTHDAY = "BIRTHDAY";//                VARCHAR     50                                    YES              
        public static readonly String PRESENCE = "PRESENCE";//                SMALLINT    5                                     YES              
        public static readonly String FRIENDSHIP_STATUS = "FRIENDSHIP_STATUS";//         SMALLINT    5                                     YES              
        public static readonly String MOBILE_PHONE_DIALING_CODE = "MOBILE_PHONE_DIALING_CODE";//    VARCHAR     10                                    YES              
        public static readonly String UPDATE_TIME = "UPDATE_TIME";//              BIGINT      19                                    NO     
        public static readonly String PROFILE_IMAGE = "PROFILE_IMAGE";//       VARCHAR     200                                   YES 
        public static readonly String PROFILE_IMAGE_ID = "PROFILE_IMAGE_ID";
        public static readonly String COVER_IMAGE = "COVER_IMAGE";//
        public static readonly String COVER_IMAGE_ID = "COVER_IMAGE_ID";
        public static readonly String COVER_IMAGE_X = "COVER_IMAGE_X";
        public static readonly String COVER_IMAGE_Y = "COVER_IMAGE_Y";
        public static readonly String EMAIL_PRIVACY = "EMAIL_PRIVACY";
        public static readonly String MOBILE_PRIVACY = "MOBILE_PRIVACY";
        public static readonly String PROFILE_IMAGE_PRIVACY = "PROFILE_IMAGE_PRIVACY";
        public static readonly String COVER_IMAGE_PRIVACY = "COVER_IMAGE_PRIVACY";
        public static readonly String BIRTHDAY_PRIVACY = "BIRTHDAY_PRIVACY";
        public static readonly String DEVICE_TOKEN = "DEVICE_TOKEN";
        public static readonly String CONTACT_TYPE = "CONTACT_TYPE";
        public static readonly String CONTACT_ADDED_TIME = "CONTACT_ADDED_TIME";
        public static readonly String CONTACT_READ_UNREAD = "CONTACT_READ_UNREAD";
        public static readonly String INCOMING_STATUS = "INCOMING_STATUS";

        //public static readonly String NEW_CONTACT_TYPE = "NEW_CONTACT_TYPE";
        //public static readonly String IS_CHANGE_REQUESTER = "IS_CHANGE_REQUESTER";
        //public static readonly String IS_BLOCKED = "IS_BLOCKED";
        //public static readonly String INCOMING_NOTIFICATION = "INCOMING_NOTIFICATION";
        public static readonly String MATCHED_BY = "MATCHED_BY";
        public static readonly String NUMBER_OF_MUTUAL_FRIENDS = "NUMBER_OF_MUTUAL_FRIENDS";
        public static readonly String NUMBER_OF_CALLS = "NUMBER_OF_CALLS";
        public static readonly String NUMBER_OF_CHATS = "NUMBER_OF_CHATS";
        public static readonly String FAVORITE_RANK = "FAVORITE_RANK";
        public static readonly String CALL_ACCESS = "CALL_ACCESS";
        public static readonly String CHAT_ACCESS = "CHAT_ACCESS";
        public static readonly String FEED_ACCESS = "FEED_ACCESS";
        public static readonly String CHAT_BG_URL = "CHAT_BG_URL";
        public static readonly String CHAT_MIN_PACKET_ID = "CHAT_MIN_PACKET_ID";
        public static readonly String CHAT_MAX_PACKET_ID = "CHAT_MAX_PACKET_ID";
        public static readonly String IS_SECRET_VISIBLE = "IS_SECRET_VISIBLE";
        public static readonly String IS_BLOCKED_BY_ME = "IS_BLOCKED_BY_ME";
        public static readonly String IS_BLOCKED_BY_FRIEND = "IS_BLOCKED_BY_FRIEND";

        /*table fields*/
        //public static readonly String USER_TABLE_ID = "USER_TABLE_ID";
        public static readonly String CIRCLE_ID = "CIRCLE_ID";
        public static readonly String GROUP_NAME = "GROUP_NAME";
        public static readonly String GROUP_ID = "GROUP_ID";

        public static readonly String GROUP_PROFILE_IMAGE = "GROUP_PROFILE_IMAGE";
        public static readonly String NUMBER_OF_MEMBERS = "NUMBER_OF_MEMBERS";
        public static readonly String IS_PARTIAL = "IS_PARTIAL";
        public static readonly String ADDED_TIME = "ADDED_TIME";
        public static readonly String ROOM_ID = "ROOM_ID";
        public static readonly String ROOM_NAME = "ROOM_NAME";
        public static readonly String ROOM_IMAGE = "ROOM_IMAGE";
        public static readonly String MEMBER_ACCESS_TYPE = "MEMBER_ACCESS_TYPE";
        public static readonly String MEMBER_ADDED_BY = "MEMBER_ADDED_BY";
        public static readonly String ADMIN = "ADMIN";
        public static readonly String CIRCLE_NAME = "CIRCLE_NAME";
        public static readonly String SUPER_ADMIN = "SUPER_ADMIN";
        public static readonly String MEMBER_COUNT = "MEMBER_COUNT";
        public static readonly String INTEGER_STATUS = "INTEGER_STATUS";
        public static readonly String USERINFO_UT = "USERINFO_UT";
        public static readonly String CONTACT_UT = "CONTACT_UT";
        public static readonly String CIRCLE_UT = "CIRCLE_UT";
        public static readonly String CIRCLE_MEMBER_UT = "CIRCLE_MEMBER_UT";
        public static readonly String GROUP_UT = "GROUP_UT";
        public static readonly String BLOCK_UNBLOCK_UT = "BLOCK_UNBLOCK_UT";
        public static readonly String CALL_LOG_UT = "CALL_LOG_UT";
        public static readonly String SMS_LOG_UT = "SMS_LOG_UT";
        public static readonly String LAST_LOGIN_TIME = "LAST_LOGIN_TIME";
        public static readonly string RECORDER_DEVICE_NUMBER = "RECORDER_DEVICE_NUMBER";
        public static readonly string PLAYER_DEVICE_NUMBER = "PLAYER_DEVICE_NUMBER";
        public static readonly string WEBCAM_DEVICE_ID = "WEBCAM_DEVICE_ID";
        public static readonly string SPEAKER_VOLUME = "SPEAKER_VOLUME";
        public static readonly string IS_SEPARATE_CHAT_VIEW = "IS_SEPARATE_CHAT_VIEW";
        public static readonly String ALERT_SOUND = "ALERT_SOUND";
        public static readonly string IM_SOUND = "IM_SOUND";
        public static readonly String IM_NOTIFICATION = "IM_NOTIFICATION";
        public static readonly String IM_POPUP = "IM_POPUP";
        public static readonly string IM_AUTO_DOWNLOAD = "IM_AUTO_DOWNLOAD";
        public static readonly string NEWS_FEED_EXPIRY = "NEWS_FEED_EXPIRY";
        public static readonly string IS_CHAT_LOG_SYNC = "IS_CHAT_LOG_SYNC";
        public static readonly string WALLET_PIN = "WALLET_PIN";

        public static readonly string STATUS_MOOD = "STATUS_MOOD";
        public static readonly String CL_CALL_TYPE = "CALL_TYPE";
        public static readonly String CL_CALL_CATEGORY = "CALL_CATEGORY";
        public static readonly String CL_CALL_DURATION = "CALL_DURATION";
        public static readonly String CL_CALLING_TIME = "CALLING_TIME";
        public static readonly String HIDDEN_POST = "HIDDEN_POST";
        public static readonly String HIDDEN_POST_USER = "HIDDEN_POST_USER";

        public static readonly String SMS_SENDER = "SENDER";
        public static readonly String SMS_RECEIVER = "RECEIVER";
        public static readonly String SMS_MESSAGE = "MESSAGE";
        public static readonly String SMS_STATUS = "STATUS";
        public static readonly String SMS_TIME = "TIME";
        public static readonly String SMS_DESTINATION = "DESTINATION";

        public static readonly String NOTIFICATION_FRIENDID = "FRIENDID";
        public static readonly String NOTIFICATION_UT = "UT";
        public static readonly String NOTIFICATION_N_TYPE = "N_TYPE";
        public static readonly String NOTIFICATION_ACTIVITY_ID = "ACTIVITY_ID";
        public static readonly String NOTIFICATION_NEWSFEED_ID = "NEWSFEED_ID";
        public static readonly String NOTIFICATION_IMAGE_ID = "IMAGE_ID";
        public static readonly String NOTIFICATION_IMAGE_URL = "IMAGE_URL";
        public static readonly String NOTIFICATION_COMMENT_ID = "COMMENT_ID";
        public static readonly String NOTIFICATION_MESSAGE_TYPE = "MESSAGE_TYPE";
        public static readonly String NOTIFICATION_FRIEND_NAME = "FRIEND_NAME";
        public static readonly String NOTIFICATION_IS_READ = "IS_READ";
        public static readonly String NOTIFICATION_NUMBER_OF_LIKECOMMENT = "NUMBER_OF_LIKECOMMENT";

        public static readonly String COUNT_CALL_NOTIFICATION = "COUNT_CALL_NOTIFICATION";
        public static readonly String COUNT_CHAT_NOTIFICATION = "COUNT_CHAT_NOTIFICATION";
        public static readonly String COUNT_ALL_NOTIFICATION = "COUNT_ALL_NOTIFICATION";
        public static readonly String COUNT_ADD_FRIEND_NOTIFICATION = "COUNT_ADD_FRIEND_NOTIFICATION";


        public static readonly String MEDIA_CONTENTID = "MEDIA_CONTENTID";
        public static readonly string MEDIA_TITLE = "MEDIA_TITLE";
        public static readonly string MEDIA_ARTIST = "MEDIA_ARTIST";
        public static readonly String MEDIA_STREAM_URL = "MEDIA_STREAM_URL";
        public static readonly String MEDIA_THUMBURL = "MEDIA_THUMBURL";
        public static readonly String MEDIA_DURAION = "MEDIA_DURATION";
        public static readonly String MEDIA_THUMB_IMAGE_WIDTH = "MEDIA_THUMB_IMAGE_WIDTH";
        public static readonly String MEDIA_THUMB_IMAGE_HEIGHT = "MEDIA_THUMB_IMAGE_HEIGHT";
        public static readonly String MEDIA_ACCESS_COUNT = "MEDIA_ACCESS_COUNT";
        public static readonly String MEDIA_LIKE_COUNT = "MEDIA_LIKE_COUNT";
        public static readonly String MEDIA_COMMENT_COUNT = "MEDIA_COMMENT_COUNT";
        public static readonly String MEDIA_ILIKE = "MEDIA_ILIKE";
        public static readonly String MEDIA_ICOMMENT = "MEDIA_ICOMMENT";
        public static readonly String MEDIA_MEDIA_TYPE = "MEDIA_TYPE";
        public static readonly String MEDIA_ALBUM_ID = "ALBUM_ID";
        public static readonly String MEDIA_ALBUM_NAME = "MEDIA_ALBUM_NAME";
        public static readonly String MEDIA_DOWNLOAD_STATE = "MEDIA_DOWNLOAD_STATE";
        public static readonly String MEDIA_DOWNLOAD_PROGRESS = "MEDIA_DOWNLOAD_PROGRESS";
        public static readonly String MEDIA_DOWNLOAD_TIME = "MEDIA_DOWNLOAD_TIME";
        public static readonly String MEDIA_PLAYING_TIME = "MEDIA_PLAYING_TIME";
        public static readonly String MEDIA_SEARCH_SUGGESTION = "MEDIA_SEARCH_SUGGESTION";

        public static readonly String ML_MESSAGE_ID = "MESSAGE_ID";
        public static readonly String ML_UID = "UID";
        public static readonly String ML_FOLDER_NAME = "FOLDER_NAME";
        public static readonly String ML_SUBJECT = "SUBJECT";
        public static readonly String ML_RCPT_FROM = "RCPT_FROM";
        public static readonly String ML_RCPT_TO = "RCPT_TO";
        public static readonly String ML_RCPT_CC = "RCPT_CC";
        public static readonly String ML_RCPT_BCC = "RCPT_BCC";
        public static readonly String ML_IN_REPLY_TO = "IN_REPLY_TO";
        public static readonly String ML_RECEIVE_DATE = "RECEIVE_DATE";
        public static readonly String ML_SENT_DATE = "SENT_DATE";
        public static readonly String ML_PLAIN_TEXT = "PLAIN_TEXT";
        public static readonly String ML_HTML_TEXT = "HTML_TEXT";
        public static readonly String ML_CONTENT_TYPE = "CONTENT_TYPE";
        public static readonly String ML_SEEN = "SEEN";

        public static readonly String EMO_NAME = "NAME";
        public static readonly String EMO_SYMBOL = "SYMBOL";
        public static readonly String EMO_URL = "URL";
        public static readonly String EMO_TYPE = "TYPE";

        public static readonly String DOING_CATEGORY = "CATEGORY";
        public static readonly String DOING_SORTID = "SORTID";
        public static readonly String DOING_NAME = "NAME";
        public static readonly String DOING_URL = "IMAGE_URL";
        public static readonly String DOING_UPDATE_TIME = "UPDATE_TIME";
        public static readonly String INS_MSG = "INSTANT_MESSAGE";
        public static readonly String INS_MSG_TYPE = "MESSAGE_TYPE";
        public static readonly String CATEGORY_ID = "CATEGORY_ID";
        public static readonly String CATEGORY_NAME = "CATEGORY_NAME";
        public static readonly String COLLECTION_NAME = "COLLECTION_NAME";
        public static readonly String COLLECTION_ID = "COLLECTION_ID";
        public static readonly String BANNER_IMAGE = "BANNER_IMAGE";
        public static readonly String THEME_COLOR = "THEME_COLOR";
        public static readonly String DETAIL_IMAGE = "DETAIL_IMAGE";
        public static readonly String IS_BANNER_VISIBLE = "IS_BANNER_VISIBLE";
        public static readonly String RANK = "RANK";
        public static readonly String IS_NEW = "IS_NEW";
        public static readonly String ICON = "ICON";
        public static readonly String PRIZE = "PRIZE";
        public static readonly String IS_FREE = "IS_FREE";
        public static readonly String IS_TOP = "IS_TOP";
        public static readonly String IS_DOWNLOADED = "IS_DOWNLOADED";
        public static readonly String DESCRIPTION = "DESCRIPTION";
        public static readonly String DOWNLOAD_TIME = "DOWNLOAD_TIME";
        public static readonly String SORT_ORDER = "SORT_ORDER";
        public static readonly String IS_VISIBLE = "IS_VISIBLE";
        public static readonly String IS_DEFAULT = "IS_DEFAULT";
        public static readonly String IS_RECENT = "IS_RECENT";
        public static readonly String LAST_USED_DATE = "LAST_USED_DATE";
        public static readonly String NEW_STICKER_IS_SEEN = "NEW_STICKER_IS_SEEN";

        public static readonly String IMAGE_ID = "IMAGE_ID";
        public static readonly String IMAGE_URL = "IMAGE_URL";

        public static readonly String DATE_EVENT_DAY = "DAY";
        public static readonly String DATE_EVENT_MONTH = "MONTH";
        public static readonly String DATE_EVENT_URL = "URL";

        public static readonly String INDEX_USER_BASIC_INFO_USER_TABLE_ID = "INDEX_USER_BASIC_INFO_USER_TABLE_ID";

        public static readonly String INDEX_CONTACT_LIST_USER_TABLE_ID = "INDEX_CONTACT_LIST_{0}_USER_TABLE_ID";

        public static readonly String INDEX_GROUP_CHAT_GROUP_ID = "INDEX_GROUP_CHAT_{0}_GROUP_ID";
        public static readonly String INDEX_GROUP_CHAT_PACKET_ID = "INDEX_GROUP_CHAT_{0}_PACKET_ID";
        public static readonly String INDEX_GROUP_CHAT_MESSAGE_DATE = "INDEX_GROUP_CHAT_{0}_MESSAGE_DATE";
        public static readonly String INDEX_GROUP_CHAT_MESSAGE_TYPE = "INDEX_GROUP_CHAT_{0}_MESSAGE_TYPE";
        public static readonly String INDEX_GROUP_CHAT_FILE_ID = "INDEX_GROUP_CHAT_{0}_FILE_ID";

        public static readonly String INDEX_ROOM_CHAT_ROOM_ID = "INDEX_ROOM_CHAT_{0}_ROOM_ID";
        public static readonly String INDEX_ROOM_CHAT_PACKET_ID = "INDEX_ROOM_CHAT_{0}_PACKET_ID";
        public static readonly String INDEX_ROOM_CHAT_MESSAGE_DATE = "INDEX_ROOM_CHAT_{0}_MESSAGE_DATE";
        public static readonly String INDEX_ROOM_CHAT_MESSAGE_TYPE = "INDEX_ROOM_CHAT_{0}_MESSAGE_TYPE";

        public static readonly String INDEX_FRIEND_CHAT_FRIEND_TABLE_ID = "INDEX_FRIEND_CHAT_{0}_FRIEND_TABLE_ID";
        public static readonly String INDEX_FRIEND_CHAT_PACKET_ID = "INDEX_FRIEND_CHAT_{0}_PACKET_ID";
        public static readonly String INDEX_FRIEND_CHAT_MESSAGE_DATE = "INDEX_FRIEND_CHAT_{0}_MESSAGE_DATE";
        public static readonly String INDEX_FRIEND_CHAT_MESSAGE_TYPE = "INDEX_FRIEND_CHAT_{0}_MESSAGE_TYPE";
        public static readonly String INDEX_FRIEND_CHAT_FILE_ID = "INDEX_FRIEND_CHAT_{0}_FILE_ID";

        public static readonly String INDEX_CALL_LOG_CALL_ID = "INDEX_CALL_LOG_{0}_CALL_ID";
        public static readonly String INDEX_CALL_LOG_CALLING_TIME = "INDEX_CALL_LOG_{0}_CALLING_TIME";
        public static readonly String INDEX_CALL_LOG_FRIEND_TABLE_ID = "INDEX_CALL_LOG_{0}_FRIEND_TABLE_ID";

        public static readonly String INDEX_ACTIVITY_UPDATE_TIME = "INDEX_ACTIVITY_{0}_UPDATE_TIME";
        public static readonly String INDEX_ACTIVITY_FRIEND_TABLE_ID = "INDEX_ACTIVITY_{0}_FRIEND_TABLE_ID";
        public static readonly String INDEX_ACTIVITY_GROUP_ID = "INDEX_ACTIVITY_{0}_GROUP_ID";

        public static readonly String INDEX_GROUP_LIST_GROUP_ID = "INDEX_GROUP_LIST_{0}_GROUP_ID";

        public static readonly String INDEX_GROUP_MEMBER_LIST_TABLE_ID = "INDEX_GROUP_MEMBER_{0}_LIST_TABLE_ID";
        public static readonly String INDEX_GROUP_MEMBER_LIST_GROUP_ID = "INDEX_GROUP_MEMBER_{0}_LIST_GROUP_ID";

        public static readonly String INDEX_ROOM_LIST_ROOM_ID = "INDEX_ROOM_LIST_{0}_ROOM_ID";

        public static readonly string ADMIN_COUNT = "ADMIN_COUNT";
    }
}

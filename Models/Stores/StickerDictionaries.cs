<<<<<<< HEAD
﻿using Models.Entity;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Stores
{
    public class StickerDictionaries
    {
        private static StickerDictionaries _Instance;
        public static StickerDictionaries Instance
        {
            get { return _Instance = _Instance ?? new StickerDictionaries(); }
            set { _Instance = value; }
        }

        #region Variable

        private ConcurrentDictionary<int, MarketStickerCategoryDTO> _MARKET_STICKER_CATEGORY_DICTIONARY = new ConcurrentDictionary<int, MarketStickerCategoryDTO>();
        private ConcurrentDictionary<int, MarketStickerCollectionDTO> _MARKET_STICKER_COLLECTION_DICTIONARY = new ConcurrentDictionary<int, MarketStickerCollectionDTO>();
        private ConcurrentDictionary<int, List<MarketStickerCategoryDTO>> _MARKET_STICKER_BANNER_DICTIONARY = new ConcurrentDictionary<int, List<MarketStickerCategoryDTO>>();

        private HashSet<int> _MARKET_STICKER_POPULAR_LIST = new HashSet<int>();
        private HashSet<int> _MARKET_STICKER_NEW_LIST = new HashSet<int>();
        private HashSet<int> _MARKET_STICKER_TOP_LIST = new HashSet<int>();
        private HashSet<int> _MARKET_STICKER_ALL_LIST = new HashSet<int>();
        private HashSet<MarketStickerLanguageyDTO> _STICKER_COUNTRYLIST = new HashSet<MarketStickerLanguageyDTO>();
        private HashSet<int> _TEMP_NEW_STICKER_CATEGORY_IDS = new HashSet<int>();

        #endregion Variable

        #region Property

        public ConcurrentDictionary<int, MarketStickerCategoryDTO> MARKET_STICKER_CATEGORY_DICTIONARY { get { return _MARKET_STICKER_CATEGORY_DICTIONARY; } }
        public ConcurrentDictionary<int, MarketStickerCollectionDTO> MARKET_STICKER_COLLECTION_DICTIONARY { get { return _MARKET_STICKER_COLLECTION_DICTIONARY; } }
        public ConcurrentDictionary<int, List<MarketStickerCategoryDTO>> MARKET_STICKER_BANNER_DICTIONARY { get { return _MARKET_STICKER_BANNER_DICTIONARY; } }

        public HashSet<int> MARKET_STICKER_POPULAR_LIST { get { return _MARKET_STICKER_POPULAR_LIST; } }
        public HashSet<int> MARKET_STICKER_NEW_LIST { get { return _MARKET_STICKER_NEW_LIST; } }
        public HashSet<int> MARKET_STICKER_TOP_LIST { get { return _MARKET_STICKER_TOP_LIST; } }
        public HashSet<int> MARKET_STICKER_ALL_LIST { get { return _MARKET_STICKER_ALL_LIST; } }
        public HashSet<MarketStickerLanguageyDTO> STICKER_COUNTRYLIST { get { return _STICKER_COUNTRYLIST; } }
        public HashSet<int> TEMP_NEW_STICKER_CATEGORY_IDS { get { return _TEMP_NEW_STICKER_CATEGORY_IDS; } }

        #endregion Property

        #region Utility Method

        public void ClearStickerDictionaries()
        {
            _MARKET_STICKER_CATEGORY_DICTIONARY.Clear();
            _MARKET_STICKER_POPULAR_LIST.Clear();
            _MARKET_STICKER_NEW_LIST.Clear();
            _MARKET_STICKER_TOP_LIST.Clear();
            _MARKET_STICKER_ALL_LIST.Clear();
            _MARKET_STICKER_COLLECTION_DICTIONARY.Clear();
            _MARKET_STICKER_BANNER_DICTIONARY.Clear();
            _STICKER_COUNTRYLIST.Clear();
            _TEMP_NEW_STICKER_CATEGORY_IDS.Clear();
        }

        #endregion Utility Method
    }
}
=======
﻿using Models.Entity;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Stores
{
    public class StickerDictionaries
    {
        private static StickerDictionaries _Instance;
        public static StickerDictionaries Instance
        {
            get { return _Instance = _Instance ?? new StickerDictionaries(); }
            set { _Instance = value; }
        }

        #region Variable
        private List<MarketStickerDynamicCategoryDTO> _MARKET_STICKER_DYNAMIC_CATEGORY_DICTIONARY = new List<MarketStickerDynamicCategoryDTO>();
        private ConcurrentDictionary<int, MarketStickerCategoryDTO> _MARKET_STICKER_CATEGORY_DICTIONARY = new ConcurrentDictionary<int, MarketStickerCategoryDTO>();
        private ConcurrentDictionary<int, MarketStickerCollectionDTO> _MARKET_STICKER_COLLECTION_DICTIONARY = new ConcurrentDictionary<int, MarketStickerCollectionDTO>();
        private ConcurrentDictionary<int, List<MarketStickerCategoryDTO>> _MARKET_STICKER_BANNER_DICTIONARY = new ConcurrentDictionary<int, List<MarketStickerCategoryDTO>>();
        private HashSet<MarketStickerLanguageyDTO> _STICKER_COUNTRYLIST = new HashSet<MarketStickerLanguageyDTO>();
        private HashSet<int> _MARKET_STICKER_ALL_LIST = new HashSet<int>();
        private HashSet<int> _TEMP_NEW_STICKER_CATEGORY_IDS = new HashSet<int>();

        #endregion Variable

        #region Property

        public List<MarketStickerDynamicCategoryDTO> MARKET_STICKER_DYNAMIC_CATEGORY_DICTIONARY { get { return _MARKET_STICKER_DYNAMIC_CATEGORY_DICTIONARY; } }
        public ConcurrentDictionary<int, MarketStickerCategoryDTO> MARKET_STICKER_CATEGORY_DICTIONARY { get { return _MARKET_STICKER_CATEGORY_DICTIONARY; } }
        public ConcurrentDictionary<int, MarketStickerCollectionDTO> MARKET_STICKER_COLLECTION_DICTIONARY { get { return _MARKET_STICKER_COLLECTION_DICTIONARY; } }
        public ConcurrentDictionary<int, List<MarketStickerCategoryDTO>> MARKET_STICKER_BANNER_DICTIONARY { get { return _MARKET_STICKER_BANNER_DICTIONARY; } }
        public HashSet<MarketStickerLanguageyDTO> STICKER_COUNTRYLIST { get { return _STICKER_COUNTRYLIST; } }
        public HashSet<int> MARKET_STICKER_ALL_LIST { get { return _MARKET_STICKER_ALL_LIST; } }
        public HashSet<int> TEMP_NEW_STICKER_CATEGORY_IDS { get { return _TEMP_NEW_STICKER_CATEGORY_IDS; } }

        #endregion Property

        #region Utility Method

        public void ClearStickerDictionaries()
        {
            _MARKET_STICKER_CATEGORY_DICTIONARY.Clear();
            _MARKET_STICKER_COLLECTION_DICTIONARY.Clear();
            _MARKET_STICKER_BANNER_DICTIONARY.Clear();
            _STICKER_COUNTRYLIST.Clear();
            _TEMP_NEW_STICKER_CATEGORY_IDS.Clear();
            _MARKET_STICKER_DYNAMIC_CATEGORY_DICTIONARY.Clear();
        }

        #endregion Utility Method
    }
}
>>>>>>> 0e74851eaf5f82012a059164c0fdac5f3a495596

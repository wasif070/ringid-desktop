﻿using Models.Entity;
using Models.Utility.Chat;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Stores
{
    public class ChatDictionaries
    {
        private static ChatDictionaries _Instance;
        public static ChatDictionaries Instance
        {
            get { return _Instance = _Instance ?? new ChatDictionaries(); }
            set { _Instance = value; }
        }

        #region Variable

        private ConcurrentDictionary<string, ChatEventDelegate> _CHAT_EVENT_DELEGATE = new ConcurrentDictionary<string, ChatEventDelegate>();
        private ConcurrentDictionary<string, ChatEventHandler> _CHAT_REG_PROGRESS_EVENT = new ConcurrentDictionary<string, ChatEventHandler>();
        private ConcurrentDictionary<string, string> _FEED_MESSAGE_PACKET_IDS = new ConcurrentDictionary<string, string>();
        private ConcurrentDictionary<string, DateEventDTO> _DATE_EVENT_DICTIONARY = new ConcurrentDictionary<string, DateEventDTO>();
        private List<GroupInfoDTO> _TEMP_GROUP_LIST = new List<GroupInfoDTO>();

        #endregion Variable

        #region Property

        public ConcurrentDictionary<string, ChatEventDelegate> CHAT_EVENT_DELEGATE { get { return _CHAT_EVENT_DELEGATE; } }
        public ConcurrentDictionary<string, ChatEventHandler> CHAT_REG_PROGRESS_EVENT { get { return _CHAT_REG_PROGRESS_EVENT; } }
        public ConcurrentDictionary<string, DateEventDTO> DATE_EVENT_DICTIONARY { get { return _DATE_EVENT_DICTIONARY; } }
        public List<GroupInfoDTO> TEMP_GROUP_LIST { get { return _TEMP_GROUP_LIST; } }

        #endregion Property

        #region Utility Method

        public void ClearChatDictionaries()
        {
            CHAT_EVENT_DELEGATE.Clear();
            CHAT_REG_PROGRESS_EVENT.Clear();
            DATE_EVENT_DICTIONARY.Clear();
            TEMP_GROUP_LIST.Clear();
        }

        #endregion Utility Method
    }
}

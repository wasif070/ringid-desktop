﻿using Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Stores
{
    public class DefaultDictionaries
    {
        private static DefaultDictionaries _Instance;
        public static DefaultDictionaries Instance
        {
            get { return _Instance = _Instance ?? new DefaultDictionaries(); }
            set { _Instance = value; }
        }

        #region Variable

        private Dictionary<string, EmoticonDTO> _EMOTICON_DICTIONARY = new Dictionary<string, EmoticonDTO>();

        #endregion Variable

        #region Property

        public Dictionary<string, EmoticonDTO> EMOTICON_DICTIONARY { get { return _EMOTICON_DICTIONARY; } }

        #endregion Property

        #region Utility Method

        #endregion Utility Method
    }
}

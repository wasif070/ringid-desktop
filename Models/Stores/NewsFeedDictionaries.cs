﻿using Auth.utility.Feed;
using Models.Entity;
using System.Collections.Generic;

namespace Models.Stores
{
    public class NewsFeedDictionaries
    {
        private static NewsFeedDictionaries _Instance = new NewsFeedDictionaries();
        public static NewsFeedDictionaries Instance
        {
            get { return _Instance; }
            set { _Instance = value; }
        }

        public void ClearNewsFeedDictionaries()
        {
            _MY_ALBUM_IMAGES.Clear();
            _TEMP_MY_PROFILE_IMAGES.Clear();
            _TEMP_MY_COVER_IMAGES.Clear();
            _TEMP_MY_FEED_IMAGES.Clear();
            //_TEMP_MY_ALBUM_IMAGES.Clear();
            _FRIEND_ALBUM_IMAGES.Clear();
            _TEMP_FRIEND_PROFILE_IMAGES.Clear();
            _TEMP_FRIEND_COVER_IMAGES.Clear();
            _TEMP_FRIEND_FEED_IMAGES.Clear();
            _ALL_NEWS_FEEDS.Clear();
            //_TMP_NEWS_FEEDS.Clear();
            _ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.Clear();
            _MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME.Clear();
            _MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS.Clear();
            _SAVED_NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME.Clear();
            _NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME.Clear();
            _SAVED_PAGES_NEWS_FEEDS_ID_SORTED_BY_TIME.Clear();
            _PAGES_NEWS_FEEDS_ID_SORTED_BY_TIME.Clear();
            _SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY.Clear();
            //_SHARE_NEWS_FEEDS_ID_SORTED_BY_TIME.Clear();
            _TEMP_WHO_SHARE_NEWS_FEEDS.Clear();
            //_UNREAD_NEWS_FEEDS_ID.Clear();
            //_LOADED_NEWS_FEEDS_ID.Clear();
            _MY_NEWS_FEEDS_ID_SORTED_BY_TIME.Clear();
            _FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME.Clear();
            _CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME.Clear();
            //_LOADED_MY_NEWS_FEEDS_ID.Clear();
            _TEMP_LIKE_LIST_FOR_POPUP.Clear();
            //  _COMMENT_LIKE_LIST.Clear();
            //_FEED_COMMENT_LIST.Clear();

            //_TEMP_IMAGE_COMMENT_LIST.Clear();
            ///_TEMP_MEDIA_COMMENT_LIST.Clear();
            _IMAGE_COMMENT_LIKE_LIST.Clear();
            _DOING_DICTIONARY.Clear();
            // _IMAGE_LIKE_LIST.Clear();
            _MAP_SEARCH_HISTORY.Clear();
            //_AUDIO_ALBUM_LIST_BY_UTID.Clear();
            //_VIDEO_ALBUM_LIST_BY_UTID.Clear();
            //_TEMP_MY_SINGLE_MEDIA_DETAILS.Clear();
            _HIDDEN_NEWS_FEEDS.Clear();
            _HIDDEN_FEEDS_USERS.Clear();
            //_MUSICPAGES_NEWS_FEEDS_ID_SORTED_BY_TIME.Clear();
            _MEDIACLOUD_NEWS_FEEDS_ID_SORTED_BY_TIME.Clear();
            _SAVED_ALL_NEWS_FEEDS_ID_SORTED_BY_TIME.Clear();
            _SAVED_CELEB_NEWS_FEEDS_ID_SORTED_BY_TIME.Clear();
            _SAVED_MEDIACLOUD_NEWS_FEEDS_ID_SORTED_BY_TIME.Clear();
        }

        private Dictionary<string, Dictionary<long, ImageDTO>> _MY_ALBUM_IMAGES = new Dictionary<string, Dictionary<long, ImageDTO>>();
        public Dictionary<string, Dictionary<long, ImageDTO>> MY_ALBUM_IMAGES
        {
            get { return _MY_ALBUM_IMAGES; }
        }

        private Dictionary<string, Dictionary<long, ImageDTO>> _TEMP_MY_PROFILE_IMAGES = new Dictionary<string, Dictionary<long, ImageDTO>>();
        public Dictionary<string, Dictionary<long, ImageDTO>> TEMP_MY_PROFILE_IMAGES
        {
            get { return _TEMP_MY_PROFILE_IMAGES; }
        }

        private Dictionary<string, Dictionary<long, ImageDTO>> _TEMP_MY_COVER_IMAGES = new Dictionary<string, Dictionary<long, ImageDTO>>();
        public Dictionary<string, Dictionary<long, ImageDTO>> TEMP_MY_COVER_IMAGES
        {
            get { return _TEMP_MY_COVER_IMAGES; }
        }
        private Dictionary<string, Dictionary<long, ImageDTO>> _TEMP_MY_FEED_IMAGES = new Dictionary<string, Dictionary<long, ImageDTO>>();
        public Dictionary<string, Dictionary<long, ImageDTO>> TEMP_MY_FEED_IMAGES
        {
            get { return _TEMP_MY_FEED_IMAGES; }
        }

        private Dictionary<long, Dictionary<string, Dictionary<long, ImageDTO>>> _FRIEND_ALBUM_IMAGES = new Dictionary<long, Dictionary<string, Dictionary<long, ImageDTO>>>();
        public Dictionary<long, Dictionary<string, Dictionary<long, ImageDTO>>> FRIEND_ALBUM_IMAGES
        {
            get { return _FRIEND_ALBUM_IMAGES; }
        }

        private Dictionary<long, Dictionary<string, Dictionary<long, ImageDTO>>> _TEMP_FRIEND_PROFILE_IMAGES = new Dictionary<long, Dictionary<string, Dictionary<long, ImageDTO>>>();
        public Dictionary<long, Dictionary<string, Dictionary<long, ImageDTO>>> TEMP_FRIEND_PROFILE_IMAGES
        {
            get { return _TEMP_FRIEND_PROFILE_IMAGES; }
        }

        private Dictionary<long, Dictionary<string, Dictionary<long, ImageDTO>>> _TEMP_FRIEND_COVER_IMAGES = new Dictionary<long, Dictionary<string, Dictionary<long, ImageDTO>>>();
        public Dictionary<long, Dictionary<string, Dictionary<long, ImageDTO>>> TEMP_FRIEND_COVER_IMAGES
        {
            get { return _TEMP_FRIEND_COVER_IMAGES; }
        }
        private Dictionary<long, Dictionary<string, Dictionary<long, ImageDTO>>> _TEMP_FRIEND_FEED_IMAGES = new Dictionary<long, Dictionary<string, Dictionary<long, ImageDTO>>>();
        public Dictionary<long, Dictionary<string, Dictionary<long, ImageDTO>>> TEMP_FRIEND_FEED_IMAGES
        {
            get { return _TEMP_FRIEND_FEED_IMAGES; }
        }

        private Dictionary<long, Dictionary<string, Dictionary<long, ImageDTO>>> _CELEBRITY_ALBUM_IMAGES = new Dictionary<long, Dictionary<string, Dictionary<long, ImageDTO>>>();
        public Dictionary<long, Dictionary<string, Dictionary<long, ImageDTO>>> CELEBRITY_ALBUM_IMAGES
        {
            get { return _CELEBRITY_ALBUM_IMAGES; }
        }

        private Dictionary<long, Dictionary<string, Dictionary<long, ImageDTO>>> _TEMP_CELEBRITY_IMAGES = new Dictionary<long, Dictionary<string, Dictionary<long, ImageDTO>>>();
        public Dictionary<long, Dictionary<string, Dictionary<long, ImageDTO>>> TEMP_CELEBRITY_IMAGES
        {
            get { return _TEMP_CELEBRITY_IMAGES; }
        }

        private Dictionary<long, FeedDTO> _ALL_NEWS_FEEDS = new Dictionary<long, FeedDTO>();
        public Dictionary<long, FeedDTO> ALL_NEWS_FEEDS
        {
            get { return _ALL_NEWS_FEEDS; }
        }

        private List<long> _HIDDEN_NEWS_FEEDS = new List<long>();
        public List<long> HIDDEN_NEWS_FEEDS
        {
            get { return _HIDDEN_NEWS_FEEDS; }
        }

        private List<long> _HIDDEN_FEEDS_USERS = new List<long>();
        public List<long> HIDDEN_FEEDS_USERS
        {
            get
            {
                return _HIDDEN_FEEDS_USERS;
            }
        }

        private CustomSortedList _ALL_NEWS_FEEDS_ID_SORTED_BY_TIME = new CustomSortedList();
        public CustomSortedList ALL_NEWS_FEEDS_ID_SORTED_BY_TIME
        {
            get { return _ALL_NEWS_FEEDS_ID_SORTED_BY_TIME; }
        }

        private CustomSortedList _MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME = new CustomSortedList();
        public CustomSortedList MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME
        {
            get { return _MEDIA_NEWS_FEEDS_ID_SORTED_BY_TIME; }
        }

        private CustomSortedList _NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME = new CustomSortedList();
        public CustomSortedList NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME
        {
            get { return _NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME; }
        }
        private CustomSortedList _SAVED_NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME = new CustomSortedList();
        public CustomSortedList SAVED_NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME
        {
            get { return _SAVED_NEWSPORTAL_NEWS_FEEDS_ID_SORTED_BY_TIME; }
        }
        private CustomSortedList _PAGES_NEWS_FEEDS_ID_SORTED_BY_TIME = new CustomSortedList();
        public CustomSortedList PAGES_NEWS_FEEDS_ID_SORTED_BY_TIME
        {
            get { return _PAGES_NEWS_FEEDS_ID_SORTED_BY_TIME; }
        }
        private CustomSortedList _MEDIACLOUD_NEWS_FEEDS_ID_SORTED_BY_TIME = new CustomSortedList();
        public CustomSortedList MEDIACLOUD_NEWS_FEEDS_ID_SORTED_BY_TIME
        {
            get { return _MEDIACLOUD_NEWS_FEEDS_ID_SORTED_BY_TIME; }
        }
        private CustomSortedList _SAVED_ALL_NEWS_FEEDS_ID_SORTED_BY_TIME = new CustomSortedList();
        public CustomSortedList SAVED_ALL_NEWS_FEEDS_ID_SORTED_BY_TIME
        {
            get { return _SAVED_ALL_NEWS_FEEDS_ID_SORTED_BY_TIME; }
        }
        private CustomSortedList _SAVED_CELEB_NEWS_FEEDS_ID_SORTED_BY_TIME = new CustomSortedList();
        public CustomSortedList SAVED_CELEB_NEWS_FEEDS_ID_SORTED_BY_TIME
        {
            get { return _SAVED_CELEB_NEWS_FEEDS_ID_SORTED_BY_TIME; }
        }
        private CustomSortedList _SAVED_MEDIACLOUD_NEWS_FEEDS_ID_SORTED_BY_TIME = new CustomSortedList();
        public CustomSortedList SAVED_MEDIACLOUD_NEWS_FEEDS_ID_SORTED_BY_TIME
        {
            get { return _SAVED_MEDIACLOUD_NEWS_FEEDS_ID_SORTED_BY_TIME; }
        }
        private CustomSortedList _SAVED_PAGES_NEWS_FEEDS_ID_SORTED_BY_TIME = new CustomSortedList();
        public CustomSortedList SAVED_PAGES_NEWS_FEEDS_ID_SORTED_BY_TIME
        {
            get { return _SAVED_PAGES_NEWS_FEEDS_ID_SORTED_BY_TIME; }
        }
        private CustomSortedList _MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS = new CustomSortedList();
        public CustomSortedList MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS
        {
            get { return _MEDIA_TRENDING_NEWS_FEEDS_ID_SORTED_BY_VIEWS; }
        }
        //private CustomSortedList _MUSICPAGES_NEWS_FEEDS_ID_SORTED_BY_TIME = new CustomSortedList();
        //public CustomSortedList MUSICPAGES_NEWS_FEEDS_ID_SORTED_BY_TIME
        //{
        //    get { return _MUSICPAGES_NEWS_FEEDS_ID_SORTED_BY_TIME; }
        //}
        private Dictionary<long, long> _SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY = new Dictionary<long, long>();
        public Dictionary<long, long> SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY
        {
            get { return _SHARE_FEEDID_PARENT_FEED_ID_DICTIONARY; }
        }

        //private Dictionary<long, CustomSortedList> _SHARE_NEWS_FEEDS_ID_SORTED_BY_TIME = new Dictionary<long, CustomSortedList>();
        //public Dictionary<long, CustomSortedList> SHARE_NEWS_FEEDS_ID_SORTED_BY_TIME
        //{
        //    get { return _SHARE_NEWS_FEEDS_ID_SORTED_BY_TIME; }
        //}

        private Dictionary<long, Dictionary<string, List<FeedDTO>>> _TEMP_WHO_SHARE_NEWS_FEEDS = new Dictionary<long, Dictionary<string, List<FeedDTO>>>();
        public Dictionary<long, Dictionary<string, List<FeedDTO>>> TEMP_WHO_SHARE_NEWS_FEEDS
        {
            get { return _TEMP_WHO_SHARE_NEWS_FEEDS; }
        }
        //private List<long> _LOADED_NEWS_FEEDS_ID = new List<long>();
        //public List<long> LOADED_NEWS_FEEDS_ID
        //{
        //    get { return _LOADED_NEWS_FEEDS_ID; }
        //}

        //private List<long> _UNREAD_NEWS_FEEDS_ID = new List<long>();
        //public List<long> UNREAD_NEWS_FEEDS_ID
        //{
        //    get { return _UNREAD_NEWS_FEEDS_ID; }
        //}

        private CustomSortedList _MY_NEWS_FEEDS_ID_SORTED_BY_TIME = new CustomSortedList();
        public CustomSortedList MY_NEWS_FEEDS_ID_SORTED_BY_TIME
        {
            get { return _MY_NEWS_FEEDS_ID_SORTED_BY_TIME; }
        }

        private Dictionary<long, CustomSortedList> _FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME = new Dictionary<long, CustomSortedList>();
        public Dictionary<long, CustomSortedList> FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME
        {
            get { return _FRIEND_NEWS_FEEDS_ID_SORTED_BY_TIME; }
        }

        private Dictionary<long, CustomSortedList> _CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME = new Dictionary<long, CustomSortedList>();
        public Dictionary<long, CustomSortedList> CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME
        {
            get { return _CIRCLE_NEWS_FEEDS_ID_SORTED_BY_TIME; }
        }
        //private List<long> _LOADED_MY_NEWS_FEEDS_ID = new List<long>();
        //public List<long> LOADED_MY_NEWS_FEEDS_ID
        //{
        //    get { return _LOADED_MY_NEWS_FEEDS_ID; }
        //}

        //<nfid<uid,userbasicinfo>>
        private Dictionary<long, UserBasicInfoDTO> _TEMP_LIKE_LIST_FOR_POPUP = new Dictionary<long, UserBasicInfoDTO>();
        public Dictionary<long, UserBasicInfoDTO> TEMP_LIKE_LIST_FOR_POPUP
        {
            get { return _TEMP_LIKE_LIST_FOR_POPUP; }
        }
        //<nfid<cmntid,cmntdto>>
        //private Dictionary<long, Dictionary<long, CommentDTO>> _FEED_COMMENT_LIST = new Dictionary<long, Dictionary<long, CommentDTO>>();
        //public Dictionary<long, Dictionary<long, CommentDTO>> FEED_COMMENT_LIST
        //{
        //    get { return _FEED_COMMENT_LIST; }
        //}
        //private Dictionary<string, Dictionary<long, FeedDTO>> _TMP_NEWS_FEEDS = new Dictionary<string, Dictionary<long, FeedDTO>>();
        //public Dictionary<string, Dictionary<long, FeedDTO>> TMP_NEWS_FEEDS
        //{
        //    get { return _TMP_NEWS_FEEDS; }
        //}

        //private Dictionary<long, Dictionary<long, CommentDTO>> _TEMP_IMAGE_COMMENT_LIST = new Dictionary<long, Dictionary<long, CommentDTO>>();
        //public Dictionary<long, Dictionary<long, CommentDTO>> TEMP_IMAGE_COMMENT_LIST
        //{
        //    get { return _TEMP_IMAGE_COMMENT_LIST; }
        //}
        //private Dictionary<long, Dictionary<long, CommentDTO>> _TEMP_MEDIA_COMMENT_LIST = new Dictionary<long, Dictionary<long, CommentDTO>>();
        //public Dictionary<long, Dictionary<long, CommentDTO>> TEMP_MEDIA_COMMENT_LIST
        //{
        //    get { return _TEMP_MEDIA_COMMENT_LIST; }
        //}
        //private Dictionary<long,UserBasicInfoDTO> _IMAGE_LIKE_LIST = new Dictionary<long,UserBasicInfoDTO>();
        //public Dictionary<long,UserBasicInfoDTO> IMAGE_LIKE_LIST
        //{
        //    get { return _IMAGE_LIKE_LIST; }
        //}

        private Dictionary<long, UserBasicInfoDTO> _IMAGE_COMMENT_LIKE_LIST = new Dictionary<long, UserBasicInfoDTO>();
        public Dictionary<long, UserBasicInfoDTO> IMAGE_COMMENT_LIKE_LIST
        {
            get { return _IMAGE_COMMENT_LIKE_LIST; }
        }

        private Dictionary<long, DoingDTO> _DOING_DICTIONARY = new Dictionary<long, DoingDTO>();
        public Dictionary<long, DoingDTO> DOING_DICTIONARY
        {
            get { return _DOING_DICTIONARY; }
        }

        private Dictionary<string, List<LocationDTO>> _MAP_SEARCH_HISTORY = new Dictionary<string, List<LocationDTO>>();
        public Dictionary<string, List<LocationDTO>> MAP_SEARCH_HISTORY
        {
            get { return _MAP_SEARCH_HISTORY; }
        }

        //private Dictionary<long, Dictionary<long, MediaContentDTO>> _AUDIO_ALBUM_LIST_BY_UTID = new Dictionary<long, Dictionary<long, MediaContentDTO>>();
        //public Dictionary<long, Dictionary<long, MediaContentDTO>> AUDIO_ALBUM_LIST_BY_UTID // <utid,<albumid,albumdto1>>
        //{
        //    get { return _AUDIO_ALBUM_LIST_BY_UTID; }
        //}

        //private Dictionary<long, Dictionary<long, MediaContentDTO>> _VIDEO_ALBUM_LIST_BY_UTID = new Dictionary<long, Dictionary<long, MediaContentDTO>>();
        //public Dictionary<long, Dictionary<long, MediaContentDTO>> VIDEO_ALBUM_LIST_BY_UTID // <utid,<albumid,albumdto1>>
        //{
        //    get { return _VIDEO_ALBUM_LIST_BY_UTID; }
        //}

        private Dictionary<long, List<SingleMediaDTO>> _TEMP_MY_SINGLE_MEDIA_DETAILS = new Dictionary<long, List<SingleMediaDTO>>();
        public Dictionary<long, List<SingleMediaDTO>> TEMP_SINGLE_ALBUM_CONTENTS
        {
            get { return _TEMP_MY_SINGLE_MEDIA_DETAILS; }
        }
        private Dictionary<long, SingleMediaDTO> _RECENT_MEDIA_ALBUM_DETAILS = new Dictionary<long, SingleMediaDTO>();
        public Dictionary<long, SingleMediaDTO> RECENT_MEDIA_ALBUM_DETAILS
        {
            get { return _RECENT_MEDIA_ALBUM_DETAILS; }
        }

    }
}

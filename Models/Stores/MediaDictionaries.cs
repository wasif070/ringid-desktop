﻿using Models.Entity;
using System.Collections.Generic;

namespace Models.Stores
{
    public class MediaDictionaries
    {
        private static MediaDictionaries _Instance = new MediaDictionaries();
        public static MediaDictionaries Instance
        {
            get { return _Instance; }
            set { _Instance = value; }
        }

        public void ClearMediaDictionaries()
        {
            HashTagSuggestions.Clear();
            //RECENT_MEDIAS.Clear();
            MEDIA_SEARCHES.Clear();
            MEDIA_TRENDS.Clear();
            TEMP_SEARCH_SGTNS.Clear();
        }

        //private Dictionary<long, HashTagDTO> _HASHTAG_SUGGESTIONS = new Dictionary<long, HashTagDTO>();
        //public Dictionary<long, HashTagDTO> HASHTAG_SUGGESTIONS
        //{
        //    get { return _HASHTAG_SUGGESTIONS; }
        //}

        private List<HashTagDTO> _hashTagSuggestions = new List<HashTagDTO>();
        public List<HashTagDTO> HashTagSuggestions
        {
            get
            {
                return _hashTagSuggestions;
            }
        }
        //private Dictionary<long, SingleMediaDTO> _RECENT_MEDIAS = new Dictionary<long, SingleMediaDTO>();
        //public Dictionary<long, SingleMediaDTO> RECENT_MEDIAS
        //{
        //    get { return _RECENT_MEDIAS; }
        //}
        //private Dictionary<long, SingleMediaDTO> _DOWNLOAD_MEDIAS = new Dictionary<long, SingleMediaDTO>();
        //public Dictionary<long, SingleMediaDTO> DOWNLOAD_MEDIAS
        //{
        //    get { return _DOWNLOAD_MEDIAS; }
        //}

        private List<SearchMediaDTO> _MEDIA_SEARCHES = new List<SearchMediaDTO>();
        public List<SearchMediaDTO> MEDIA_SEARCHES
        {
            get { return _MEDIA_SEARCHES; }
        }

        private List<SearchMediaDTO> _MEDIA_TRENDS = new List<SearchMediaDTO>();
        public List<SearchMediaDTO> MEDIA_TRENDS
        {
            get { return _MEDIA_TRENDS; }
        }

        private Dictionary<string, List<SearchMediaDTO>> _TEMP_SEARCH_SGTNS = new Dictionary<string, List<SearchMediaDTO>>();
        public Dictionary<string, List<SearchMediaDTO>> TEMP_SEARCH_SGTNS
        {
            get { return _TEMP_SEARCH_SGTNS; }
        }

        //private Dictionary<long, List<SingleMediaDTO>> _TEMP_HASHTAG_CONTENTS = new Dictionary<long, List<SingleMediaDTO>>();
        //public Dictionary<long, List<SingleMediaDTO>> TEMP_HASHTAG_CONTENTS
        //{
        //    get { return _TEMP_HASHTAG_CONTENTS; }
        //}
    }
}

﻿using Models.Entity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Models.Stores
{
    public class RingDictionaries
    {

        private static RingDictionaries _Instance = new RingDictionaries();
        public static RingDictionaries Instance
        {
            get
            { return _Instance; }
            set { _Instance = value; }
        }

        public void ClearRingDictionaries()
        {
            _PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY.Clear();
            _SEARCH_STRING_QUEUE = new ConcurrentQueue<string>();
            _SEARCH_FRIENDS_DICTIONARY.Clear();
            //_NOTIFICATION_DICTIONARY.Clear();
            //_TEMP_NOTIFICATION_DICTIONARY.Clear();
            _WORK_DICTIONARY.Clear();
            _EDUCATION_DICTIONARY.Clear();
            _SKILL_DICTIONARY.Clear();
            //_CIRCLE_LIST_DICTIONARY.Clear();
            //_CIRCLE_MEMBERS_LIST_DICTIONARY.Clear();
        }
        private ConcurrentDictionary<string, JObject> _PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY = new ConcurrentDictionary<string, JObject>();

        public ConcurrentDictionary<string, JObject> PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY
        {
            get { return _PACKET_GOT_RESPONSE_FROM_SERVER_DICTIONARY; }
        }

        private ConcurrentQueue<string> _SEARCH_STRING_QUEUE = new ConcurrentQueue<string>();

        public ConcurrentQueue<string> SEARCH_STRING_QUEUE
        {
            get { return _SEARCH_STRING_QUEUE; }
        }

        private Dictionary<int, Dictionary<long, UserBasicInfoDTO>> _SEARCH_FRIENDS_DICTIONARY = new Dictionary<int, Dictionary<long, UserBasicInfoDTO>>();
        public Dictionary<int, Dictionary<long, UserBasicInfoDTO>> SEARCH_FRIENDS_DICTIONARY
        {
            get { return _SEARCH_FRIENDS_DICTIONARY; }
        }
        /*
        private Dictionary<long, NotificationDTO> _NOTIFICATION_DICTIONARY = new Dictionary<long, NotificationDTO>();
        public Dictionary<long, NotificationDTO> NOTIFICATION_DICTIONARY
        {
            get { return _NOTIFICATION_DICTIONARY; }
        }
        */
        /*
        private Dictionary<string, Dictionary<long, NotificationDTO>> _TEMP_NOTIFICATION_DICTIONARY = new Dictionary<string, Dictionary<long, NotificationDTO>>();
        public Dictionary<string, Dictionary<long, NotificationDTO>> TEMP_NOTIFICATION_DICTIONARY
        {
            get { return _TEMP_NOTIFICATION_DICTIONARY; }
        }
        */
        private Dictionary<long, Dictionary<Guid, EducationDTO>> _EDUCATION_DICTIONARY = new Dictionary<long, Dictionary<Guid, EducationDTO>>();
        public Dictionary<long, Dictionary<Guid, EducationDTO>> EDUCATION_DICTIONARY
        {
            get { return _EDUCATION_DICTIONARY; }
        }
        private Dictionary<long, Dictionary<Guid, WorkDTO>> _WORK_DICTIONARY = new Dictionary<long, Dictionary<Guid, WorkDTO>>();
        public Dictionary<long, Dictionary<Guid, WorkDTO>> WORK_DICTIONARY
        {
            get { return _WORK_DICTIONARY; }
        }
        private Dictionary<long, Dictionary<Guid, SkillDTO>> _SKILL_DICTIONARY = new Dictionary<long, Dictionary<Guid, SkillDTO>>();
        public Dictionary<long, Dictionary<Guid, SkillDTO>> SKILL_DICTIONARY
        {
            get { return _SKILL_DICTIONARY; }
        }
        //<circleid,circledto>
        //private Dictionary<long, CircleDTO> _CIRCLE_LIST_DICTIONARY = new Dictionary<long, CircleDTO>();
        //public Dictionary<long, CircleDTO> CIRCLE_LIST_DICTIONARY
        //{
        //    get { return _CIRCLE_LIST_DICTIONARY; }
        //}
        //<circleid,<uid,circlememberdto>>
        /*
        private Dictionary<long, Dictionary<long, CircleMemberDTO>> _CIRCLE_MEMBERS_LIST_DICTIONARY = new Dictionary<long, Dictionary<long, CircleMemberDTO>>();
        public Dictionary<long, Dictionary<long, CircleMemberDTO>> CIRCLE_MEMBERS_LIST_DICTIONARY
        {
            get { return _CIRCLE_MEMBERS_LIST_DICTIONARY; }
        }
        */
        private List<NotificationDTO> _NOTIFICATION_LISTS = new List<NotificationDTO>();
        public List<NotificationDTO> NOTIFICATION_LISTS { get { return _NOTIFICATION_LISTS; } }

        private Dictionary<int, string> _SEARCHING_UI_NAME_DICTIONARY = new Dictionary<int,string>();
        public Dictionary<int, string> SEARCHING_UI_NAME_DICTIONARY
        {
            get { return _SEARCHING_UI_NAME_DICTIONARY; }
        }
    }
}

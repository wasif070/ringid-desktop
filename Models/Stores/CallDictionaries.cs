﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Entity;

namespace Models.Stores
{
    public class CallDictionaries
    {
        #region Single Instance
        private static CallDictionaries _Instance = new CallDictionaries();
        public static CallDictionaries Instance
        {
            get
            {
                return _Instance;
            }
            set
            {
                _Instance = value;
            }
        }
        #endregion

        #region dictionaries
        private ConcurrentDictionary<string, CallerDTO> _CALLERS_INFO_DICTIONARY = new ConcurrentDictionary<string, CallerDTO>();
        public ConcurrentDictionary<string, CallerDTO> CALLERS_INFO_DICTIONARY
        {
            get
            {
                return _CALLERS_INFO_DICTIONARY;
            }
        }

        #endregion



        #region clear Dictionaries
        public void ClearCallDictionaries()
        {
            CALLERS_INFO_DICTIONARY.Clear();
            /**/
            //       deQ();
        }
        //private void deQ()
        //{

        //    byte[] imageByte = null;
        //    while (ImageBytes.TryDequeue(out imageByte))
        //    {

        //    }
        //    byte[] imageByte2 = null;
        //    while (ImageFormattedPackets.TryDequeue(out imageByte2))
        //    {

        //    }
        //}
        #endregion
    }
}

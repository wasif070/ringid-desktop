﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Models.Entity;
using Models.Utility;

namespace Models.Stores
{
    public class FriendDictionaries
    {
        #region "Properties"
        public static FriendDictionaries Instance { get; set; }

        public ConcurrentDictionary<long, UserBasicInfoDTO> FRIEND_NEED_TO_FETCH_DICTIONARY { get; set; }
        public Dictionary<long, UserBasicInfoDTO> UID_USERBASICINFO_DICTIONARY { get; set; }
        public List<UserBasicInfoDTO> TEMP_CONTACT_LIST { get; set; }
        public Dictionary<long, UserBasicInfoDTO> UID_USERBASICINFO_SUGGESTION_DICTIONARY { get; set; }
        public Dictionary<long, UserBasicInfoDTO> TMP_FRIEND_CONTACT_LIST_DICTIONARY { get; set; }
        public Dictionary<long, FriendInfoDTO> FRIEND_CONTACT_LIST_DICTIONARY { get; set; }
        public Dictionary<long, FriendInfoDTO> FRIEND_MUTUAL_LIST_DICTIONARY { get; set; }
        public List<long> TMP_FRIEND_MUTUAL_LIST_DICTIONARY { get; set; }
        public Dictionary<long, bool> SUGGESTION_UTIDS_DICTIONARY { get; set; }
        #endregion "Properties"

        #region "Public Methods"

        public void ClearFriendDictionaries()
        {
            FRIEND_NEED_TO_FETCH_DICTIONARY.Clear();
            //UTID_UID_DICTIONARY.Clear();
            UID_USERBASICINFO_DICTIONARY.Clear();
            TEMP_CONTACT_LIST.Clear();
            UID_USERBASICINFO_SUGGESTION_DICTIONARY.Clear();
            TMP_FRIEND_CONTACT_LIST_DICTIONARY.Clear();
            TMP_FRIEND_MUTUAL_LIST_DICTIONARY.Clear();
            SUGGESTION_UTIDS_DICTIONARY.Clear();
            FRIEND_CONTACT_LIST_DICTIONARY.Clear();
            FRIEND_MUTUAL_LIST_DICTIONARY.Clear();
        }

        public void SetFriendDictionaries()
        {
            FRIEND_NEED_TO_FETCH_DICTIONARY = new ConcurrentDictionary<long, UserBasicInfoDTO>();
            //UTID_UID_DICTIONARY = new Dictionary<long, long>();
            UID_USERBASICINFO_DICTIONARY = new Dictionary<long, UserBasicInfoDTO>();
            TEMP_CONTACT_LIST = new List<UserBasicInfoDTO>();
            UID_USERBASICINFO_SUGGESTION_DICTIONARY = new Dictionary<long, UserBasicInfoDTO>();
            TMP_FRIEND_CONTACT_LIST_DICTIONARY = new Dictionary<long, UserBasicInfoDTO>();
            TMP_FRIEND_MUTUAL_LIST_DICTIONARY = new List<long>();
            SUGGESTION_UTIDS_DICTIONARY = new Dictionary<long, bool>();
            FRIEND_CONTACT_LIST_DICTIONARY = new Dictionary<long, FriendInfoDTO>();
            FRIEND_MUTUAL_LIST_DICTIONARY = new Dictionary<long, FriendInfoDTO>();
        }

        public UserBasicInfoDTO GetFromUserBasicInfoDictionary(long userTableID)
        {
            UserBasicInfoDTO friendDetailsInDictionary = null;
            UID_USERBASICINFO_DICTIONARY.TryGetValue(userTableID, out friendDetailsInDictionary);
            return friendDetailsInDictionary;
        }

        public UserBasicInfoDTO GetFromBasicInfoDictionaryNotNuallable(long userTableID, Newtonsoft.Json.Linq.JObject _JobjFromResponse = null)
        {
            lock (FriendDictionaries.Instance.UID_USERBASICINFO_DICTIONARY)
            {
                UserBasicInfoDTO userBasicInfo = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(userTableID);
                if (userBasicInfo == null)
                {
                    userBasicInfo = new UserBasicInfoDTO();
                    userBasicInfo.UserTableID = userTableID;
                    AddIntoBasicInfoDictionary(userTableID, userBasicInfo);
                }
                if (_JobjFromResponse != null)
                    HelperMethodsModel.BindAddFriendDetails(_JobjFromResponse, userBasicInfo);
                return userBasicInfo;
            }

        }

        public void AddOrUPdateInBasicInfoDictionary(long userTableID, UserBasicInfoDTO userBasicInfoDto, bool replace)
        {
            if (replace)
            {
                UID_USERBASICINFO_DICTIONARY[userTableID] = userBasicInfoDto;
            }
        }

        public void AddOrUPdateInBasicInfoDictionaryWithLock(UserBasicInfoDTO userBasicInfoDto)
        {
            lock (UID_USERBASICINFO_DICTIONARY)
            {
                UID_USERBASICINFO_DICTIONARY[userBasicInfoDto.UserTableID] = userBasicInfoDto;
            }
        }

        public void AddIntoBasicInfoDictionary(long userTableID, UserBasicInfoDTO userBasicInfoDto)
        {
            UID_USERBASICINFO_DICTIONARY.Add(userTableID, userBasicInfoDto);
        }

        public bool IfExistInBasicInfoDictionary(long userTableID)
        {
            return UID_USERBASICINFO_DICTIONARY.ContainsKey(userTableID);
        }

        public List<UserBasicInfoDTO> GetUserBasicInfoDictionaryAsList()
        {
            return UID_USERBASICINFO_DICTIONARY.Values.ToList();
        }

        #endregion "Public methods"

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using log4net;
using Models.Constants;
using Models.Entity;
using Models.Stores;
using Newtonsoft.Json.Linq;

namespace Models.Utility
{
    public class HelperMethodsModel
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(HelperMethodsModel).Name);

        public static void BindMyProfileDetails(JObject jObject)
        {
            try
            {
                int bDay = 0, bMnth = 0, bYr = 0, mDay = 0, mMnth = 0, mYr = 0;
                if (jObject[JsonKeys.Country] != null) DefaultSettings.userProfile.Country = (string)jObject[JsonKeys.Country];
                if (jObject[JsonKeys.Gender] != null) DefaultSettings.userProfile.Gender = (string)jObject[JsonKeys.Gender];
                if (jObject[JsonKeys.CoverImage] != null) DefaultSettings.userProfile.CoverImage = (string)jObject[JsonKeys.CoverImage];
                if (jObject[JsonKeys.CoverImageId] != null) DefaultSettings.userProfile.CoverImageId = (Guid)jObject[JsonKeys.CoverImageId];
                if (jObject[JsonKeys.CropImageX] != null) DefaultSettings.userProfile.CropImageX = (int)jObject[JsonKeys.CropImageX];
                if (jObject[JsonKeys.CropImageY] != null) DefaultSettings.userProfile.CropImageY = (int)jObject[JsonKeys.CropImageY];
                if (jObject[JsonKeys.BirthDay] != null) bDay = (int)jObject[JsonKeys.BirthDay];
                if (jObject[JsonKeys.BirthMonth] != null) bMnth = (int)jObject[JsonKeys.BirthMonth];
                if (jObject[JsonKeys.BirthYear] != null) bYr = (int)jObject[JsonKeys.BirthYear];
                //if (bDay > 0) DefaultSettings.userProfile.BirthDay = (long)(new DateTime(bYr, bMnth, bDay) - new DateTime(1970, 1, 1)).TotalMilliseconds;
                //else DefaultSettings.userProfile.BirthDay = 0;
                if (bDay > 0 && bMnth > 0 && bYr > 0) DefaultSettings.userProfile.BirthDate = new DateTime(bYr, bMnth, bDay);
                else DefaultSettings.userProfile.BirthDate = DateTime.MinValue;

                //Marriagedate
                if (jObject[JsonKeys.MarriageDay] != null) mDay = (int)jObject[JsonKeys.MarriageDay];
                if (jObject[JsonKeys.MarriageMonth] != null) mMnth = (int)jObject[JsonKeys.MarriageMonth];
                if (jObject[JsonKeys.MarriageYear] != null) mYr = (int)jObject[JsonKeys.MarriageYear];
                if (mDay > 0 && mMnth > 0 && mYr > 0) DefaultSettings.userProfile.MarriageDate = new DateTime(mYr, mMnth, mDay);
                else DefaultSettings.userProfile.MarriageDate = DateTime.MinValue;                
                //if (mDay > 0) DefaultSettings.userProfile.MarriageDay = (long)(new DateTime(mYr, mMnth, mDay) - new DateTime(1970, 1, 1)).TotalMilliseconds;
                //else DefaultSettings.userProfile.MarriageDay = 0;                

                if (jObject[JsonKeys.HomeCity] != null) DefaultSettings.userProfile.HomeCity = (string)jObject[JsonKeys.HomeCity];
                if (jObject[JsonKeys.CurrentCity] != null) DefaultSettings.userProfile.CurrentCity = (string)jObject[JsonKeys.CurrentCity];
                if (jObject[JsonKeys.AboutMe] != null) DefaultSettings.userProfile.AboutMe = (string)jObject[JsonKeys.AboutMe];
                if (jObject[JsonKeys.Privacy] != null)
                {
                    JArray privacy = (JArray)jObject[JsonKeys.Privacy];
                    DefaultSettings.userProfile.Privacy = new short[8];
                    for (int j = 0; j < privacy.Count; j++) DefaultSettings.userProfile.Privacy[j] = (short)privacy.ElementAt(j);
                    DefaultSettings.userProfile.EmailPrivacy = DefaultSettings.userProfile.Privacy[0];
                    DefaultSettings.userProfile.MobilePrivacy = DefaultSettings.userProfile.Privacy[1];
                    DefaultSettings.userProfile.ProfileImagePrivacy = DefaultSettings.userProfile.Privacy[2];
                    DefaultSettings.userProfile.BirthdayPrivacy = DefaultSettings.userProfile.Privacy[3];
                    DefaultSettings.userProfile.CoverImagePrivacy = DefaultSettings.userProfile.Privacy[4];
                }
                if (jObject[JsonKeys.AlbumId] != null) DefaultSettings.userProfile.AlbumId = (Guid)jObject[JsonKeys.AlbumId];
            }
            catch (Exception e)
            {
                log.Error("EXCEPTION in bindMyProfileDetails => " + e.Message + "\n" + e.StackTrace);
            }
        }

        public static void BindSignInBackgroundIntials()
        {
            JObject jObject = DefaultSettings.VALUE_LOGIN_USER_INFO;
            try
            {
                if (DefaultSettings.userProfile == null) DefaultSettings.userProfile = new UserBasicInfoDTO();
                DefaultSettings.userProfile.FriendShipStatus = -1;
                if (jObject[JsonKeys.UserIdentity] != null)
                {
                    DefaultSettings.userProfile.RingID = long.Parse(jObject[JsonKeys.UserIdentity].ToString());
                    DefaultSettings.LOGIN_RING_ID = DefaultSettings.userProfile.RingID;
                }
                if (jObject[JsonKeys.UserTableID] != null)
                {
                    DefaultSettings.userProfile.UserTableID = (long)jObject[JsonKeys.UserTableID];
                    DefaultSettings.LOGIN_TABLE_ID = DefaultSettings.userProfile.UserTableID;
                }
                if (jObject[JsonKeys.FullName] != null) DefaultSettings.userProfile.FullName = (string)jObject[JsonKeys.FullName];
            }
            catch (Exception e)
            {
                log.Error("EXCEPTION in bindSignInBackgroundDetails => " + e.Message + "\n" + e.StackTrace);
            }
        }

        public static void BindSignInDetails(JObject jObject)
        {
            try
            {
                DefaultSettings.FRIEND_LIST_LOADED = false;
                if (DefaultSettings.userProfile == null) DefaultSettings.userProfile = new UserBasicInfoDTO();
                DefaultSettings.userProfile.FriendShipStatus = -1;
                if (jObject[JsonKeys.UserIdentity] != null)
                {
                    DefaultSettings.userProfile.RingID = long.Parse(jObject[JsonKeys.UserIdentity].ToString());
                    DefaultSettings.LOGIN_RING_ID = DefaultSettings.userProfile.RingID;
                }
                if (jObject[JsonKeys.UserTableID] != null)
                {
                    DefaultSettings.userProfile.UserTableID = (long)jObject[JsonKeys.UserTableID];
                    DefaultSettings.LOGIN_TABLE_ID = DefaultSettings.userProfile.UserTableID;
                }
                if (jObject[JsonKeys.FullName] != null) DefaultSettings.userProfile.FullName = (string)jObject[JsonKeys.FullName];
                if (jObject[JsonKeys.SessionId] != null) DefaultSettings.LOGIN_SESSIONID = (string)jObject[JsonKeys.SessionId];
                if (jObject[JsonKeys.IsEmailVerified] != null) DefaultSettings.userProfile.IsEmailVerified = (int)jObject[JsonKeys.IsEmailVerified];
                if (jObject[JsonKeys.IsMyNumberVerified] != null) DefaultSettings.userProfile.IsMobileNumberVerified = (int)jObject[JsonKeys.IsMyNumberVerified];
                if (jObject[JsonKeys.Email] != null) DefaultSettings.userProfile.Email = (string)jObject[JsonKeys.Email];
                else DefaultSettings.userProfile.Email = "";
                if (jObject[JsonKeys.MobilePhone] != null) DefaultSettings.userProfile.MobileNumber = (string)jObject[JsonKeys.MobilePhone];
                else DefaultSettings.userProfile.MobileNumber = "";
                if (jObject[JsonKeys.DialingCode] != null) DefaultSettings.userProfile.MobileDialingCode = (string)jObject[JsonKeys.DialingCode];
                else DefaultSettings.userProfile.MobileDialingCode = "";
                if (jObject[JsonKeys.SocialMediaId] != null) DefaultSettings.userProfile.SocialMediaId = (string)jObject[JsonKeys.SocialMediaId];
                else DefaultSettings.userProfile.SocialMediaId = "";
                if (jObject[JsonKeys.ProfileImage] != null) DefaultSettings.userProfile.ProfileImage = (string)jObject[JsonKeys.ProfileImage];
                if (jObject[JsonKeys.ProfileImageId] != null) DefaultSettings.userProfile.ProfileImageId = (Guid)jObject[JsonKeys.ProfileImageId];
                if (jObject[JsonKeys.EmoticonVersion] != null) DefaultSettings.userProfile.EmoticonVersion = (int)jObject[JsonKeys.EmoticonVersion];
                if (jObject[JsonKeys.LastStatus] != null) DefaultSettings.userProfile.Presence = (int)jObject[JsonKeys.LastStatus];
                else DefaultSettings.userProfile.Presence = StatusConstants.PRESENCE_OFFLINE;
                if (jObject[JsonKeys.OfflineServerIp] != null) DefaultSettings.RING_OFFLINE_IM_IP = (string)jObject[JsonKeys.OfflineServerIp];
                if (jObject[JsonKeys.OfflineServerPrt] != null) DefaultSettings.RING_OFFLINE_IM_PORT = (int)jObject[JsonKeys.OfflineServerPrt];
                if (jObject[JsonKeys.Mood] != null) DefaultSettings.userProfile.Mood = (int)jObject[JsonKeys.Mood];
                if (jObject[JsonKeys.CallAccess] != null) DefaultSettings.userProfile.CallAccess = (int)jObject[JsonKeys.CallAccess];
                if (jObject[JsonKeys.ChatAccess] != null) DefaultSettings.userProfile.ChatAccess = (int)jObject[JsonKeys.ChatAccess];
                if (jObject[JsonKeys.FeedAccess] != null) DefaultSettings.userProfile.FeedAccess = (int)jObject[JsonKeys.FeedAccess];
                if (jObject[JsonKeys.AnonymousCallAccess] != null) DefaultSettings.userProfile.AnonymousCallAccess = (int)jObject[JsonKeys.AnonymousCallAccess];
                if (jObject[JsonKeys.AnonymousChatAccess] != null) DefaultSettings.userProfile.AnonymousChatAccess = (int)jObject[JsonKeys.AnonymousChatAccess];
                if (jObject[JsonKeys.LastOnlineTime] != null) DefaultSettings.userProfile.LastOnlineTime = (long)jObject[JsonKeys.LastOnlineTime];
                if (jObject[JsonKeys.Password] != null) { DefaultSettings.VALUE_LOGIN_USER_PASSWORD = (string)jObject[JsonKeys.Password]; }
                else DefaultSettings.VALUE_LOGIN_USER_PASSWORD = string.Empty;
            }
            catch (Exception e)
            {
                log.Error("EXCEPTION in bindSignInDetails => " + e.Message + "\n" + e.StackTrace);
            }
        }

        public static UserBasicInfoDTO BindAddFriendDetails(JObject jObject, UserBasicInfoDTO userBasicInfoDTO = null)
        {
            try
            {
                if (userBasicInfoDTO == null) userBasicInfoDTO = new UserBasicInfoDTO();
                if (jObject[JsonKeys.UserIdentity] != null) userBasicInfoDTO.RingID = long.Parse(jObject[JsonKeys.UserIdentity].ToString());
                if (jObject[JsonKeys.UserTableID] != null) userBasicInfoDTO.UserTableID = (long)jObject[JsonKeys.UserTableID];
                if (jObject[JsonKeys.FullName] != null) userBasicInfoDTO.FullName = (string)jObject[JsonKeys.FullName];
                if (jObject[JsonKeys.Gender] != null) userBasicInfoDTO.Gender = (string)jObject[JsonKeys.Gender];
                if (jObject[JsonKeys.FriendshipStatus] != null) userBasicInfoDTO.FriendShipStatus = (int)jObject[JsonKeys.FriendshipStatus];
                if (jObject[JsonKeys.ProfileImage] != null) userBasicInfoDTO.ProfileImage = (string)jObject[JsonKeys.ProfileImage];
                if (jObject[JsonKeys.ProfileImageId] != null) userBasicInfoDTO.ProfileImageId = (Guid)jObject[JsonKeys.ProfileImageId];
                if (jObject[JsonKeys.NumberOfMutualFriend] != null) userBasicInfoDTO.NumberOfMutualFriends = (int)jObject[JsonKeys.NumberOfMutualFriend];
                if (jObject[JsonKeys.MatchedBy] != null) userBasicInfoDTO.MatchedBy = (int)jObject[JsonKeys.MatchedBy];
                if (jObject[JsonKeys.CallAccess] != null) userBasicInfoDTO.CallAccess = (int)jObject[JsonKeys.CallAccess];
                if (jObject[JsonKeys.ChatAccess] != null) userBasicInfoDTO.ChatAccess = (int)jObject[JsonKeys.ChatAccess];
                if (jObject[JsonKeys.FeedAccess] != null) userBasicInfoDTO.FeedAccess = (int)jObject[JsonKeys.FeedAccess];
                if (jObject[JsonKeys.UpdateTime] != null) userBasicInfoDTO.UpdateTime = (long)jObject[JsonKeys.UpdateTime];
                if (jObject[JsonKeys.ContactType] != null) userBasicInfoDTO.ContactType = (int)jObject[JsonKeys.ContactType];
                return userBasicInfoDTO;
            }
            catch (Exception e)
            {
                log.Error("EXCEPTION in BindAddFriendDetails => " + e.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        public static NewsDTO BindNewsInfoDetails(JObject jObject)
        {
            try
            {
                NewsDTO newsDTO = new NewsDTO();
                if (jObject[JsonKeys.NewsPortalCategoryId] != null) newsDTO.NewsCategoryId = (int)jObject[JsonKeys.NewsPortalCategoryId];
                if (jObject[JsonKeys.NewsCategoryId] != null) newsDTO.NewsCategoryId = (int)jObject[JsonKeys.NewsCategoryId];
                if (jObject[JsonKeys.NewsShortDescription] != null) newsDTO.NewsShortDescription = (string)jObject[JsonKeys.NewsShortDescription];
                if (jObject[JsonKeys.NewsDescription] != null) newsDTO.NewsDescription = (string)jObject[JsonKeys.NewsDescription];
                if (jObject[JsonKeys.Id] != null) newsDTO.NewsId = (long)jObject[JsonKeys.Id];
                if (jObject[JsonKeys.NewsPortalTitle] != null) newsDTO.NewsTitle = (string)jObject[JsonKeys.NewsPortalTitle];
                if (jObject[JsonKeys.NewsPortalUrl] != null) newsDTO.NewsUrl = (string)jObject[JsonKeys.NewsPortalUrl];
                if (jObject[JsonKeys.NewsPortalFeedtype] != null) newsDTO.NewsPortalFeedtype = (int)jObject[JsonKeys.NewsPortalFeedtype];
                if (jObject[JsonKeys.NewsPortalExUrlOp] != null) newsDTO.ExUrlOp = (bool)jObject[JsonKeys.NewsPortalExUrlOp];
                if (jObject[JsonKeys.PageInfo] != null)
                {
                    JObject pageInfo = (JObject)jObject[JsonKeys.PageInfo];
                    if (pageInfo[JsonKeys.PageId] != null) newsDTO.PId = (long)pageInfo[JsonKeys.PageId];
                    if (pageInfo[JsonKeys.FullName] != null) newsDTO.PName = (string)pageInfo[JsonKeys.FullName];
                    if (pageInfo[JsonKeys.SubscriberCount] != null) newsDTO.SCount = (int)pageInfo[JsonKeys.SubscriberCount];
                }
                return newsDTO;
            }
            catch (Exception e)
            {
                log.Error("EXCEPTION in newsPortalInfoDTO => " + e.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        public static FeedDTO BindFeedDetails(JObject jObject)
        {
            try
            {
                FeedDTO feed = new FeedDTO();
                if (jObject[JsonKeys.NewsfeedId] != null) feed.NewsfeedId = (Guid)jObject[JsonKeys.NewsfeedId];
                if (jObject[JsonKeys.UserIdentity] != null) feed.RingID = (long)jObject[JsonKeys.UserIdentity];
                if (jObject[JsonKeys.UserTableID] != null) feed.UserTableID = (long)jObject[JsonKeys.UserTableID];
                if (jObject[JsonKeys.Status] != null) feed.Status = (string)jObject[JsonKeys.Status];
                else feed.Status = "";
                if (jObject[JsonKeys.Time] != null) feed.Time = (long)jObject[JsonKeys.Time];
                if (jObject[JsonKeys.FeedPrivacy] != null) feed.Privacy = (int)jObject[JsonKeys.FeedPrivacy];
                if (jObject[JsonKeys.ActualTime] != null) feed.ActualTime = (long)jObject[JsonKeys.ActualTime];
                if (jObject[JsonKeys.BookPostType] != null) feed.BookPostType = (short)jObject[JsonKeys.BookPostType];
                if (jObject[JsonKeys.NumberOfComments] != null) feed.NumberOfComments = (long)jObject[JsonKeys.NumberOfComments];
                if (jObject[JsonKeys.NumberOfLikes] != null) feed.NumberOfLikes = (long)jObject[JsonKeys.NumberOfLikes];
                if (jObject[JsonKeys.NumberOfShares] != null) feed.NumberOfShares = (long)jObject[JsonKeys.NumberOfShares];
                if (jObject[JsonKeys.ILike] != null) feed.ILike = (short)jObject[JsonKeys.ILike];
                if (jObject[JsonKeys.IComment] != null) feed.IComment = (short)jObject[JsonKeys.IComment];
                if (jObject[JsonKeys.IShare] != null) feed.IShare = (short)jObject[JsonKeys.IShare];
                if (jObject[JsonKeys.FullName] != null) feed.FullName = (string)jObject[JsonKeys.FullName];
                if (jObject[JsonKeys.ActivistFirstName] != null) feed.ActivistFirstName = (string)jObject[JsonKeys.ActivistFirstName];
                if (jObject[JsonKeys.ProfileImage] != null) feed.ProfileImage = (string)jObject[JsonKeys.ProfileImage];
                if (jObject[JsonKeys.ProfileImagePrivacy] != null) feed.ProfileImagePrivacy = (short)jObject[JsonKeys.ProfileImagePrivacy];
                if (jObject[JsonKeys.SubType] != null) feed.FeedSubType = (short)jObject[JsonKeys.SubType];
                if (jObject[JsonKeys.FeedCategory] != null) feed.FeedCategory = (short)jObject[JsonKeys.FeedCategory];
                if (jObject[JsonKeys.NewsPortalFeedInfo] != null)
                {
                    JObject npFeedInfo = (JObject)jObject[JsonKeys.NewsPortalFeedInfo];
                    feed.NewsInfo = BindNewsInfoDetails(npFeedInfo);
                }
                if (jObject[JsonKeys.ImageList] != null)
                {
                    feed.ImageList = new List<ImageDTO>();
                    JArray imageList = (JArray)jObject[JsonKeys.ImageList];
                    foreach (JObject singleImage in imageList)
                    {
                        ImageDTO img = BindImageDetails(singleImage);
                        img.NewsFeedId = feed.NewsfeedId;
                        SetMissingInfo(jObject, img);
                        feed.ImageList.Add(img);
                    }
                    feed.ImageList.Sort((x, y) => x.ImageId.CompareTo(y.ImageId));
                }
                else
                {
                    if (jObject[JsonKeys.ImageUrl] != null && jObject[JsonKeys.ImageId] != null && jObject[JsonKeys.ImageType] != null)
                    {
                        ImageDTO singleImage = BindImageDetails(jObject);
                        singleImage.NewsFeedId = feed.NewsfeedId;
                        SetMissingInfo(jObject, singleImage);
                        feed.ImageList = new List<ImageDTO>();
                        feed.ImageList.Add(singleImage);
                    }
                }
                if (jObject[JsonKeys.SharedFeedId] != null) feed.SharedFeedId = (long)jObject[JsonKeys.SharedFeedId];
                if (jObject[JsonKeys.TotalImage] != null) feed.TotalImage = (int)jObject[JsonKeys.TotalImage];
                if (jObject[JsonKeys.GroupId] != null)
                    feed.GroupId = (long)jObject[JsonKeys.GroupId];
                if (jObject[JsonKeys.GroupName] != null) feed.CircleName = (string)jObject[JsonKeys.GroupName];
                if (jObject[JsonKeys.Activity] != null) feed.Activity = (short)jObject[JsonKeys.Activity];
                if (jObject[JsonKeys.ActivistId] != null) feed.ActivistId = (long)jObject[JsonKeys.ActivistId];
                if (jObject[JsonKeys.ImageInCollection] != null) feed.ImageInCollection = (short)jObject[JsonKeys.ImageInCollection];
                if (jObject[JsonKeys.ShowContinue] != null) feed.ShowContinue = (bool)jObject[JsonKeys.ShowContinue];
                if (jObject[JsonKeys.LinkType] != null) feed.LinkMediaType = (int)jObject[JsonKeys.LinkType];
                if (jObject[JsonKeys.Success] != null) feed.sucs = (bool)jObject[JsonKeys.Success];
                if (jObject[JsonKeys.Location] != null && ((string)jObject[JsonKeys.Location]).Length > 0)
                {
                    if (feed.locationDTO == null) feed.locationDTO = new LocationDTO();
                    feed.locationDTO.LocationName = (string)jObject[JsonKeys.Location];
                }
                if (jObject[JsonKeys.Latitude] != null)
                {
                    double d = (double)jObject[JsonKeys.Latitude];
                    if (d != 9999.0)
                    {
                        if (feed.locationDTO == null) feed.locationDTO = new LocationDTO();
                        feed.locationDTO.Latitude = d;
                    }
                }
                if (jObject[JsonKeys.Longitude] != null)
                {
                    double d = (double)jObject[JsonKeys.Longitude];
                    if (d != 9999.0)
                    {
                        if (feed.locationDTO == null) feed.locationDTO = new LocationDTO();
                        feed.locationDTO.Longitude = d;
                    }
                }
                if (jObject[JsonKeys.DoingList] != null)
                {
                    JArray array = (JArray)jObject[JsonKeys.DoingList];
                    foreach (JObject obj in array)
                    {
                        if (obj[JsonKeys.Name] != null)//if (obj[JsonKeys.Id] != null && obj[JsonKeys.Name] != null && obj[JsonKeys.Category] != null && obj[JsonKeys.Url] != null)
                        {
                            feed.DoingActivity = (string)obj[JsonKeys.Name];//temporarily "Feeling " + 
                            if (obj[JsonKeys.Url] != null)
                                feed.DoingImageUrl = (string)obj[JsonKeys.Url];
                            else if (obj[JsonKeys.Id] != null)
                            {
                                long id = (long)obj[JsonKeys.Id];
                                DoingDTO dto = null;
                                if (NewsFeedDictionaries.Instance.DOING_DICTIONARY.TryGetValue(id, out dto))
                                {
                                    feed.DoingImageUrl = dto.ImgUrl;
                                }
                            }
                            break;
                        }
                    }
                }
                if (jObject[JsonKeys.FutId] != null) feed.FriendUserTableId = (long)jObject[JsonKeys.FutId];
                if (jObject[JsonKeys.FriendFirstName] != null) feed.FriendFirstName = (string)jObject[JsonKeys.FriendFirstName];
                if (jObject[JsonKeys.WhoShareList] != null)
                {
                    feed.WhoShareList = new List<FeedDTO>();
                    foreach (JObject obj in ((JArray)jObject[JsonKeys.WhoShareList]))
                    {
                        FeedDTO dto = BindFeedDetails(obj);
                        feed.WhoShareList.Add(dto);
                    }
                }
                if (jObject[JsonKeys.OriginalFeed] != null) feed.ParentFeed = BindFeedDetails((JObject)jObject[JsonKeys.OriginalFeed]);
                if (jObject[JsonKeys.FriendFirstName] != null) feed.FriendFirstName = (string)jObject[JsonKeys.FriendFirstName];
                if (jObject[JsonKeys.LinkDescription] != null) feed.PreviewDesc = (string)jObject[JsonKeys.LinkDescription];
                if (jObject[JsonKeys.LinkDomain] != null)
                    feed.PreviewDomain = (string)jObject[JsonKeys.LinkDomain];
                if (jObject[JsonKeys.LinkImageURL] != null) feed.PreviewImgUrl = (string)jObject[JsonKeys.LinkImageURL];
                if (jObject[JsonKeys.LinkTitle] != null) feed.PreviewTitle = (string)jObject[JsonKeys.LinkTitle];
                if (jObject[JsonKeys.LinkURL] != null) feed.PreviewUrl = (string)jObject[JsonKeys.LinkURL];
                if (jObject[JsonKeys.TotalTaggedFriends] != null) feed.TotalTaggedFriends = (short)jObject[JsonKeys.TotalTaggedFriends];
                if (jObject[JsonKeys.FriendsTagList] != null)
                {
                    feed.TaggedFriendsList = new List<UserBasicInfoDTO>();
                    JArray array = (JArray)jObject[JsonKeys.FriendsTagList];
                    foreach (JObject obj in array)
                    {
                        if (obj[JsonKeys.UserTableID] != null && obj[JsonKeys.Name] != null)
                        {
                            UserBasicInfoDTO user = null;
                            long utid = (long)obj[JsonKeys.UserTableID];
                            user = FriendDictionaries.Instance.GetFromUserBasicInfoDictionary(utid);
                            if (user == null)
                            {
                                string name = (string)obj[JsonKeys.Name];
                                user = new UserBasicInfoDTO();
                                user.FullName = name;
                                user.UserTableID = utid;
                                user.RingID = feed.RingID;
                                if (obj[JsonKeys.ProfileImage] != null)
                                    user.ProfileImage = (string)obj[JsonKeys.ProfileImage];
                                FriendDictionaries.Instance.AddIntoBasicInfoDictionary(user.UserTableID, user);
                            }
                            feed.TaggedFriendsList.Add(user);
                        }
                    }
                }
                if (jObject[JsonKeys.MediaContentList] != null)
                {
                    JArray array = (JArray)jObject[JsonKeys.MediaContentList];
                    feed.MediaContent = new MediaContentDTO();
                    feed.MediaContent.MediaList = new List<SingleMediaDTO>();
                    foreach (JObject obj in array)
                    {
                        SingleMediaDTO dto = BindSingleMediaDTO(obj);
                        feed.MediaContent.MediaList.Add(dto);
                        feed.MediaContent.MediaType = dto.MediaType;
                    }
                }
                if (jObject[JsonKeys.IsSaved] != null) feed.IsSaved = (bool)jObject[JsonKeys.IsSaved];
                if (jObject[JsonKeys.IsEdited] != null) feed.Edited = (bool)jObject[JsonKeys.IsEdited];
                if (jObject[JsonKeys.Validity] != null) feed.Validity = (int)jObject[JsonKeys.Validity];
                if (jObject[JsonKeys.StatusTags] != null)
                {
                    JArray array = (JArray)jObject[JsonKeys.StatusTags];
                    feed.StatusTags = GetStatusTagListFromJArray(array);
                }
                return feed;
            }
            catch (Exception e)
            {
                log.Error("EXCEPTION in BindFeedDetails => " + e.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        public static List<StatusTagDTO> GetStatusTagListFromJArray(JArray array)
        {
            List<StatusTagDTO> lst = new List<StatusTagDTO>();
            foreach (JObject obj in array)
            {
                StatusTagDTO statusTag = new StatusTagDTO();
                if (obj[JsonKeys.FullName] != null) statusTag.FullName = (string)obj[JsonKeys.FullName];
                if (obj[JsonKeys.Position] != null) statusTag.Index = (int)obj[JsonKeys.Position];
                if (obj[JsonKeys.UserTableID] != null) statusTag.UserTableID = (long)obj[JsonKeys.UserTableID];
                lst.Add(statusTag);
            }
            lst.Sort((x, y) => y.Index.CompareTo(x.Index));
            return lst;
        }

        static public bool URLExists(string url)
        {
            bool result = false;
            if (DefaultSettings.IsInternetAvailable)
            {
                WebRequest webRequest = WebRequest.Create(url);
                webRequest.Timeout = 5000; // miliseconds
                webRequest.Method = "HEAD";
                HttpWebResponse response = null;
                try
                {
                    response = (HttpWebResponse)webRequest.GetResponse();
                    result = true;
                }
                finally
                {
                    if (response != null) response.Close();
                    webRequest = null;
                }
            }
            return result;
        }

        public static SingleMediaDTO BindSingleMediaDTO(JObject jObject, SingleMediaDTO dto = null)
        {
            try
            {
                if (dto == null) dto = new SingleMediaDTO();
                if (jObject[JsonKeys.ContentId] != null) dto.ContentId = (long)jObject[JsonKeys.ContentId];
                if (jObject[JsonKeys.Title] != null) dto.Title = (string)jObject[JsonKeys.Title];
                if (jObject[JsonKeys.Artist] != null) dto.Artist = (string)jObject[JsonKeys.Artist];
                if (jObject[JsonKeys.MediaType] != null) if (dto.MediaType == 0) dto.MediaType = (int)jObject[JsonKeys.MediaType];
                if (jObject[JsonKeys.StreamUrl] != null) dto.StreamUrl = (string)jObject[JsonKeys.StreamUrl];
                if (jObject[JsonKeys.ThumbUrl] != null)
                {
                    dto.ThumbUrl = (string)jObject[JsonKeys.ThumbUrl];
                    if (string.IsNullOrWhiteSpace(dto.ThumbUrl)) dto.ThumbUrl = null;
                }

                if (dto.MediaType == 2 && !string.IsNullOrEmpty(dto.StreamUrl) && string.IsNullOrEmpty(dto.ThumbUrl))
                    dto.ThumbUrl = dto.StreamUrl.Replace(".mp4", ".jpg");
                if (jObject[JsonKeys.MediaDuration] != null) dto.Duration = (long)jObject[JsonKeys.MediaDuration];
                if (jObject[JsonKeys.ThumbImageWidth] != null) dto.ThumbImageWidth = (int)jObject[JsonKeys.ThumbImageWidth];
                if (jObject[JsonKeys.MediaPrivacy] != null) dto.Privacy = (int)jObject[JsonKeys.MediaPrivacy];
                if (jObject[JsonKeys.ThumbImageHeight] != null) dto.ThumbImageHeight = (int)jObject[JsonKeys.ThumbImageHeight];
                if (jObject[JsonKeys.AlbumName] != null) dto.AlbumName = (string)jObject[JsonKeys.AlbumName];
                if (jObject[JsonKeys.AlbumId] != null) dto.AlbumId = (long)jObject[JsonKeys.AlbumId];
                if (jObject[JsonKeys.AccessCount] != null) dto.AccessCount = (short)jObject[JsonKeys.AccessCount];
                if (jObject[JsonKeys.LikeCount] != null) dto.LikeCount = (short)jObject[JsonKeys.LikeCount];
                if (jObject[JsonKeys.CommentCount] != null) dto.CommentCount = (short)jObject[JsonKeys.CommentCount];
                if (jObject[JsonKeys.UserTableID] != null) dto.UserTableID = (long)jObject[JsonKeys.UserTableID];
                if (jObject[JsonKeys.ILike] != null) dto.ILike = (short)jObject[JsonKeys.ILike];
                if (jObject[JsonKeys.IComment] != null) dto.IComment = (short)jObject[JsonKeys.IComment];
                if (jObject[JsonKeys.IShare] != null)
                {
                    short ish = (short)jObject[JsonKeys.IShare];
                    dto.IShare = (ish > dto.IShare) ? ish : dto.IShare;
                }
                if (jObject[JsonKeys.NumberOfShares] != null)
                {
                    short nsh = (short)jObject[JsonKeys.NumberOfShares];
                    dto.ShareCount = (nsh > dto.ShareCount) ? nsh : dto.ShareCount;
                }
                if (jObject[JsonKeys.FullName] != null) dto.FullName = (string)jObject[JsonKeys.FullName];
                if (jObject[JsonKeys.ProfileImage] != null) dto.ProfileImage = (string)jObject[JsonKeys.ProfileImage];
                return dto;
            }
            catch (Exception e)
            {
                log.Error("EXCEPTION in BindSingleMediaDTO => " + e.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        public static MediaContentDTO BindFeedMedias(JObject jObj_mdDto, JArray jArr_mdLst)
        {
            try
            {
                MediaContentDTO mediaContentDTO = new MediaContentDTO();
                mediaContentDTO.MediaList = new List<SingleMediaDTO>();
                if (jArr_mdLst != null)
                {
                    foreach (JObject Ob in jArr_mdLst)
                    {
                        SingleMediaDTO dto = BindSingleMediaDTO(Ob);
                        mediaContentDTO.MediaList.Add(dto);
                    }
                }
                if (jObj_mdDto != null)
                {
                    if (jObj_mdDto[JsonKeys.MediaList] != null)
                    {
                        JArray array = (JArray)jObj_mdDto[JsonKeys.MediaList];
                        foreach (JObject Ob in array)
                        {
                            SingleMediaDTO prevDto = null;
                            if (mediaContentDTO.MediaList.Count > 0)
                                foreach (var item in mediaContentDTO.MediaList)
                                {
                                    if ((Ob[JsonKeys.ContentId] != null && item.ContentId == (long)Ob[JsonKeys.ContentId]) || (Ob[JsonKeys.StreamUrl] != null && item.StreamUrl != null && item.StreamUrl.Equals((string)Ob[JsonKeys.StreamUrl])))
                                    {
                                        prevDto = item;
                                        break;
                                    }
                                }
                            if (prevDto != null)
                            {
                                SingleMediaDTO dto = BindSingleMediaDTO(Ob, prevDto);
                            }
                            else
                            {
                                SingleMediaDTO dto = BindSingleMediaDTO(Ob);
                                mediaContentDTO.MediaList.Add(dto);
                            }
                        }
                    }
                    if (jObj_mdDto[JsonKeys.AlbumName] != null) mediaContentDTO.AlbumName = (string)jObj_mdDto[JsonKeys.AlbumName];
                    if (jObj_mdDto[JsonKeys.MediaAlbumImageURL] != null) mediaContentDTO.AlbumImageUrl = (string)jObj_mdDto[JsonKeys.MediaAlbumImageURL];
                    if (jObj_mdDto[JsonKeys.MediaType] != null) mediaContentDTO.MediaType = (int)jObj_mdDto[JsonKeys.MediaType];
                    if (jObj_mdDto[JsonKeys.AlbumId] != null) mediaContentDTO.AlbumId = (long)jObj_mdDto[JsonKeys.AlbumId];

                    if (jObj_mdDto[JsonKeys.UserTableID] != null) mediaContentDTO.UserTableID = (long)jObj_mdDto[JsonKeys.UserTableID];
                }
                return mediaContentDTO;
            }
            catch (Exception e)
            {
                log.Error("EXCEPTION in BindFeedMedias => " + e.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        private static void SetMissingInfo(JObject jObject, ImageDTO img)
        {
            if (img.UserTableID <= 0 && string.IsNullOrEmpty(img.FullName))
            {
                if (jObject[JsonKeys.UserIdentity] != null) img.UserTableID = (long)jObject[JsonKeys.UserTableID];
                if (jObject[JsonKeys.FullName] != null) img.FullName = (string)jObject[JsonKeys.FullName];
            }
        }

        public static CircleMemberDTO BindCircleMemberDetails(JObject jObject)
        {
            try
            {
                CircleMemberDTO circleMember = new CircleMemberDTO();
                if (jObject[JsonKeys.Id] != null) circleMember.PivotId = (long)jObject[JsonKeys.Id];
                if (jObject[JsonKeys.GroupId] != null) circleMember.CircleId = (long)jObject[JsonKeys.GroupId];
                if (jObject[JsonKeys.UserIdentity] != null) circleMember.RingID = (long)jObject[JsonKeys.UserIdentity];
                if (jObject[JsonKeys.UserTableID] != null) circleMember.UserTableID = (long)jObject[JsonKeys.UserTableID];
                if (jObject[JsonKeys.UpdateTime] != null) circleMember.UpdateTime = (long)jObject[JsonKeys.UpdateTime];
                if (jObject[JsonKeys.Admin] != null) circleMember.IsAdmin = (bool)jObject[JsonKeys.Admin];
                if (jObject[JsonKeys.IntegerStatus] != null) circleMember.IntegerStatus = (short)jObject[JsonKeys.IntegerStatus];
                if (jObject[JsonKeys.FullName] != null) circleMember.FullName = (string)jObject[JsonKeys.FullName];
                if (jObject[JsonKeys.ProfileImage] != null) circleMember.ProfileImage = (string)jObject[JsonKeys.ProfileImage];
                return circleMember;
            }
            catch (Exception e)
            {
                log.Error("EXCEPTION in BindCircleMemberDetails => " + e.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        public static CircleDTO BindCircleDetails(JObject jObject)
        {
            try
            {
                CircleDTO circle = new CircleDTO();
                if (jObject[JsonKeys.GroupName] != null) circle.CircleName = (string)jObject[JsonKeys.GroupName];
                if (jObject[JsonKeys.GroupId] != null) circle.CircleId = (long)jObject[JsonKeys.GroupId];
                if (jObject[JsonKeys.SuperAdmin] != null) circle.SuperAdmin = (long)jObject[JsonKeys.SuperAdmin];
                if (jObject[JsonKeys.UpdateTime] != null) circle.UpdateTime = (long)jObject[JsonKeys.UpdateTime];
                if (jObject[JsonKeys.UserTableID] != null) circle.UserTableID = (long)jObject[JsonKeys.UserTableID];
                if (jObject[JsonKeys.IntegerStatus] != null) circle.IntegerStatus = (short)jObject[JsonKeys.IntegerStatus];
                if (jObject[JsonKeys.MemberOrMediaCount] != null) circle.MemberCount = (int)jObject[JsonKeys.MemberOrMediaCount];
                if (jObject[JsonKeys.MembershipType] != null) circle.MembershipStatus = (int)jObject[JsonKeys.MembershipType];
                if (jObject[JsonKeys.AdminCount] != null) circle.AdminCount = (int)jObject[JsonKeys.AdminCount];
                if (jObject[JsonKeys.IsHidden] != null) circle.IsProfileHidden = (bool)jObject[JsonKeys.IsHidden];
                return circle;
            }
            catch (Exception e)
            {
                log.Error("EXCEPTION in BindCircleDetails => " + e.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        public static CelebrityDTO BindCelebrityDetails(JObject jObject)
        {
            try
            {
                CelebrityDTO dto = new CelebrityDTO();
                if (jObject[JsonKeys.FullName] != null) dto.CelebrityName = (string)jObject[JsonKeys.FullName];
                if (jObject[JsonKeys.UserTableID] != null) dto.UserTableID = (long)jObject[JsonKeys.UserTableID];
                if (jObject[JsonKeys.ProfileImage] != null) dto.ProfileImage = (string)jObject[JsonKeys.ProfileImage];
                if (jObject[JsonKeys.CoverImage] != null) dto.CoverImage = (string)jObject[JsonKeys.CoverImage];
                if (jObject[JsonKeys.UserIdentity] != null) dto.RingID = (long)jObject[JsonKeys.UserIdentity];
                if (jObject[JsonKeys.IsUserFeedHidden] != null) dto.IsProfileHidden = (bool)jObject[JsonKeys.IsUserFeedHidden];
                if (jObject[JsonKeys.CelebrityInfo] != null)
                {
                    JObject celebInfo = (JObject)jObject[JsonKeys.CelebrityInfo];
                    if (celebInfo[JsonKeys.Followercount] != null) dto.SubscriberCount = (int)celebInfo[JsonKeys.Followercount];
                    if (celebInfo[JsonKeys.PostCount] != null) dto.PostCount = (int)celebInfo[JsonKeys.PostCount];
                    if (celebInfo[JsonKeys.CelebrityCountry] != null) dto.CelebrityCountry = (string)celebInfo[JsonKeys.CelebrityCountry];
                    if (celebInfo[JsonKeys.IsFollower] != null) dto.IsSubscribed = (bool)celebInfo[JsonKeys.IsFollower];
                    if (celebInfo[JsonKeys.OnlineTime] != null) dto.OnlineTime = (long)celebInfo[JsonKeys.OnlineTime];
                    if (celebInfo[JsonKeys.MoodMessage] != null) dto.MoodMessage = (string)celebInfo[JsonKeys.MoodMessage];
                    if (celebInfo[JsonKeys.Category] != null)
                    {
                        JArray CategoryArray = (JArray)celebInfo[JsonKeys.Category];
                        dto.CelebrityCatNameArray = new List<string>();
                        for (int j = 0; j < CategoryArray.Count; j++)
                        {
                            string temp = (string)CategoryArray.ElementAt(j);
                            dto.CelebrityCatNameArray.Add(temp);
                        }
                    }
                }
                return dto;
            }
            catch (Exception e)
            {
                log.Error("EXCEPTION in BindCelebrityDetails => " + e.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        public static MusicPageDTO BindMusicPageDetails(JObject jObject)
        {
            try
            {
                MusicPageDTO dto = new MusicPageDTO();
                if (jObject[JsonKeys.FullName] != null) dto.MusicPageName = (string)jObject[JsonKeys.FullName];
                if (jObject[JsonKeys.ProfileImage] != null) dto.ProfileImage = (string)jObject[JsonKeys.ProfileImage];
                if (jObject[JsonKeys.CoverImage] != null) dto.CoverImage = (string)jObject[JsonKeys.CoverImage];
                if (jObject[JsonKeys.UserTableID] != null) dto.UserTableID = (long)jObject[JsonKeys.UserTableID];
                if (jObject[JsonKeys.UserIdentity] != null) dto.RingID = (long)jObject[JsonKeys.UserIdentity];
                if (jObject[JsonKeys.IsUserFeedHidden] != null) dto.IsProfileHidden = (bool)jObject[JsonKeys.IsUserFeedHidden];
                if (jObject[JsonKeys.NewsPortalDTO] != null)
                {
                    JObject NpDto = (JObject)jObject[JsonKeys.NewsPortalDTO];
                    if (NpDto[JsonKeys.NewsPortalSlogan] != null) dto.PageSlogan = (string)NpDto[JsonKeys.NewsPortalSlogan];
                    if (NpDto[JsonKeys.PageId] != null) dto.MusicPageId = (long)NpDto[JsonKeys.PageId];
                    if (NpDto[JsonKeys.Subscribe] != null) dto.IsSubscribed = (bool)NpDto[JsonKeys.Subscribe];
                    if (NpDto[JsonKeys.NewsPortalCatName] != null) dto.PageCatName = (string)NpDto[JsonKeys.NewsPortalCatName];
                    if (NpDto[JsonKeys.NewsPortalCatId] != null) dto.PageCatId = (long)NpDto[JsonKeys.NewsPortalCatId];
                    if (NpDto[JsonKeys.SubscriberCount] != null) dto.SubscriberCount = (int)NpDto[JsonKeys.SubscriberCount];
                    if (NpDto[JsonKeys.UserTableID] != null) dto.UserTableID = (long)NpDto[JsonKeys.UserTableID];
                }
                return dto;
            }
            catch (Exception e)
            {
                log.Error("EXCEPTION in BindMusicPageDetails => " + e.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        public static NewsPortalDTO BindNewsPortalDetails(JObject jObject)
        {
            try
            {
                NewsPortalDTO dto = new NewsPortalDTO();
                if (jObject[JsonKeys.FullName] != null) dto.PortalName = (string)jObject[JsonKeys.FullName];
                if (jObject[JsonKeys.ProfileImage] != null) dto.ProfileImage = (string)jObject[JsonKeys.ProfileImage];
                if (jObject[JsonKeys.CoverImage] != null) dto.CoverImage = (string)jObject[JsonKeys.CoverImage];
                if (jObject[JsonKeys.UserTableID] != null) dto.UserTableID = (long)jObject[JsonKeys.UserTableID];
                if (jObject[JsonKeys.UserIdentity] != null) dto.RingID = (long)jObject[JsonKeys.UserIdentity];
                if (jObject[JsonKeys.IsUserFeedHidden] != null) dto.IsProfileHidden = (bool)jObject[JsonKeys.IsUserFeedHidden];
                if (jObject[JsonKeys.NewsPortalDTO] != null)
                {
                    JObject NpDto = (JObject)jObject[JsonKeys.NewsPortalDTO];
                    if (NpDto[JsonKeys.NewsPortalSlogan] != null) dto.PortalSlogan = (string)NpDto[JsonKeys.NewsPortalSlogan];
                    if (NpDto[JsonKeys.PageId] != null) dto.PortalId = (long)NpDto[JsonKeys.PageId];
                    if (NpDto[JsonKeys.Subscribe] != null) dto.IsSubscribed = (bool)NpDto[JsonKeys.Subscribe];
                    if (NpDto[JsonKeys.NewsPortalCatName] != null) dto.PortalCatName = (string)NpDto[JsonKeys.NewsPortalCatName];
                    if (NpDto[JsonKeys.NewsPortalCatId] != null) dto.PortalCatId = (long)NpDto[JsonKeys.NewsPortalCatId];
                    if (NpDto[JsonKeys.SubscriberCount] != null) dto.SubscriberCount = (int)NpDto[JsonKeys.SubscriberCount];
                    if (NpDto[JsonKeys.FullName] != null) dto.PortalName = (string)NpDto[JsonKeys.FullName];
                    if (NpDto[JsonKeys.UserTableID] != null) dto.UserTableID = (long)NpDto[JsonKeys.UserTableID];
                }
                return dto;
            }
            catch (Exception e)
            {
                log.Error("EXCEPTION in NewsPortalDTO => " + e.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        public static PageInfoDTO BindPageDetails(JObject jObject)
        {
            try
            {
                PageInfoDTO dto = new PageInfoDTO();
                if (jObject[JsonKeys.FullName] != null) dto.PageName = (string)jObject[JsonKeys.FullName];
                if (jObject[JsonKeys.ProfileImage] != null) dto.ProfileImage = (string)jObject[JsonKeys.ProfileImage];
                if (jObject[JsonKeys.CoverImage] != null) dto.CoverImage = (string)jObject[JsonKeys.CoverImage];
                if (jObject[JsonKeys.UserTableID] != null) dto.UserTableID = (long)jObject[JsonKeys.UserTableID];
                if (jObject[JsonKeys.UserIdentity] != null) dto.RingID = (long)jObject[JsonKeys.UserIdentity];
                if (jObject[JsonKeys.IsUserFeedHidden] != null) dto.IsProfileHidden = (bool)jObject[JsonKeys.IsUserFeedHidden];
                if (jObject[JsonKeys.NewsPortalDTO] != null)
                {
                    JObject NpDto = (JObject)jObject[JsonKeys.NewsPortalDTO];
                    if (NpDto[JsonKeys.NewsPortalSlogan] != null) dto.PageSlogan = (string)NpDto[JsonKeys.NewsPortalSlogan];
                    if (NpDto[JsonKeys.PageId] != null) dto.PageId = (long)NpDto[JsonKeys.PageId];
                    if (NpDto[JsonKeys.Subscribe] != null) dto.IsSubscribed = (bool)NpDto[JsonKeys.Subscribe];
                    if (NpDto[JsonKeys.NewsPortalCatName] != null) dto.PageCatName = (string)NpDto[JsonKeys.NewsPortalCatName];
                    if (NpDto[JsonKeys.NewsPortalCatId] != null) dto.PageCatId = (long)NpDto[JsonKeys.NewsPortalCatId];
                    if (NpDto[JsonKeys.SubscriberCount] != null) dto.SubscriberCount = (int)NpDto[JsonKeys.SubscriberCount];
                    if (NpDto[JsonKeys.FullName] != null) dto.PageName = (string)NpDto[JsonKeys.FullName];
                    if (NpDto[JsonKeys.UserTableID] != null) dto.UserTableID = (long)NpDto[JsonKeys.UserTableID];
                }
                return dto;
            }
            catch (Exception e)
            {
                log.Error("EXCEPTION in PageInfoDTO => " + e.Message + "\n" + e.StackTrace);
                return null;
            }
        }

        public static void UpdateFeedDTOsForSingleMediaFeed(SingleMediaDTO updatedSingleMediaDTO)
        {
            try
            {
                lock (NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
                {
                    foreach (var item in NewsFeedDictionaries.Instance.ALL_NEWS_FEEDS)
                    {
                        FeedDTO feeddtoInDictionary = item.Value;
                        if (feeddtoInDictionary.MediaContent != null
                            && feeddtoInDictionary.MediaContent.MediaList != null
                            && feeddtoInDictionary.MediaContent.MediaList.Count == 1
                            && feeddtoInDictionary.MediaContent.MediaList.ElementAt(0).ContentId == updatedSingleMediaDTO.ContentId)
                        {
                            SingleMediaDTO smdtoD = feeddtoInDictionary.MediaContent.MediaList.ElementAt(0);
                            smdtoD.ShareCount = updatedSingleMediaDTO.ShareCount;
                            smdtoD.IShare = updatedSingleMediaDTO.IShare;
                            smdtoD.LikeCount = updatedSingleMediaDTO.LikeCount;
                            smdtoD.ILike = updatedSingleMediaDTO.ILike;
                            smdtoD.CommentCount = updatedSingleMediaDTO.CommentCount;
                            smdtoD.IComment = updatedSingleMediaDTO.IComment;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.StackTrace + ex.Message);
            }
        }
        /// <summary>
        /// 
        /////"imageList":[{"imgId":3901,"iurl":"2110012548/1441285899734.jpg","ih":225,"iw":300,"imT":2,"albId":"profileimages",
        //"albn":"Profile Photos","tm":1441285902815,"nl":0,"il":0,"ic":0,"nc":0}],
        /// </summary>
        /// <param name="singleImage"></param>
        /// <returns></returns>
        public static ImageDTO BindImageDetails(JObject singleImage)
        {
            ImageDTO imageDTO = new ImageDTO();
            if (singleImage[JsonKeys.ImageId] != null) imageDTO.ImageId = (long)singleImage[JsonKeys.ImageId];
            if (singleImage[JsonKeys.ImageUrl] != null) imageDTO.ImageUrl = (string)singleImage[JsonKeys.ImageUrl];
            if (singleImage[JsonKeys.ImageCaption] != null) imageDTO.Caption = (string)singleImage[JsonKeys.ImageCaption];
            if (singleImage[JsonKeys.ImageHeight] != null) imageDTO.ImageHeight = (int)singleImage[JsonKeys.ImageHeight];
            if (singleImage[JsonKeys.ImageWidth] != null) imageDTO.ImageWidth = (int)singleImage[JsonKeys.ImageWidth];
            if (singleImage[JsonKeys.ImageType] != null) imageDTO.ImageType = (int)singleImage[JsonKeys.ImageType];
            if (singleImage[JsonKeys.NumberOfLikes] != null) imageDTO.NumberOfLikes = (long)singleImage[JsonKeys.NumberOfLikes];
            if (singleImage[JsonKeys.ILike] != null) imageDTO.iLike = (int)singleImage[JsonKeys.ILike];
            if (singleImage[JsonKeys.NumberOfComments] != null) imageDTO.NumberOfComments = (long)singleImage[JsonKeys.NumberOfComments];
            if (singleImage[JsonKeys.IComment] != null) imageDTO.iComment = (int)singleImage[JsonKeys.IComment];
            if (singleImage[JsonKeys.AlbumId] != null) imageDTO.AlbumId = (string)singleImage[JsonKeys.AlbumId];
            if (singleImage[JsonKeys.AlbumName] != null) imageDTO.AlbumName = (string)singleImage[JsonKeys.AlbumName];
            if (singleImage[JsonKeys.Time] != null) imageDTO.Time = (long)singleImage[JsonKeys.Time];
            if (singleImage[JsonKeys.FutId] != null) imageDTO.FriendTableId = (long)singleImage[JsonKeys.FutId];
            if (singleImage[JsonKeys.UserTableID] != null) imageDTO.UserTableID = (long)singleImage[JsonKeys.UserTableID];
            if (singleImage[JsonKeys.FullName] != null) imageDTO.FullName = (string)singleImage[JsonKeys.FullName];
            if (singleImage[JsonKeys.MediaPrivacy] != null) imageDTO.Privacy = (int)singleImage[JsonKeys.MediaPrivacy];
            return imageDTO;
        }

        public static NotificationDTO BindNotificationInfo(JObject singleNotification)
        {
            NotificationDTO dto = new NotificationDTO();
            dto.ID = singleNotification[JsonKeys.NtUUID] != null ? Guid.Parse(singleNotification[JsonKeys.NtUUID].ToString()) : Guid.Empty;
            dto.UpdateTime = (long)singleNotification[JsonKeys.UpdateTime];
            dto.NotificationType = singleNotification[JsonKeys.NotificationType] != null ? (int)singleNotification[JsonKeys.NotificationType] : 0;
            dto.ActivityID = singleNotification[JsonKeys.ActionId] != null ? (int)singleNotification[JsonKeys.ActionId] : 0;
            dto.MessageType = singleNotification[JsonKeys.NotificationMessageType] != null ? (int)singleNotification[JsonKeys.NotificationMessageType] : 0;
            dto.NumberOfLikeOrComment = singleNotification[JsonKeys.LikeOrComment] != null ? (long)singleNotification[JsonKeys.LikeOrComment] : 0;
            dto.NewsfeedID = singleNotification[JsonKeys.NewsfeedId] != null ? (Guid)singleNotification[JsonKeys.NewsfeedId] : Guid.Empty;
            dto.ImageID = singleNotification[JsonKeys.ImageId] != null ? (Guid)singleNotification[JsonKeys.ImageId] : (singleNotification[JsonKeys.ContentId] != null ? (Guid)singleNotification[JsonKeys.ContentId] : Guid.Empty);
            dto.CommentID = singleNotification[JsonKeys.CommentId] != null ? (Guid)singleNotification[JsonKeys.CommentId] : Guid.Empty;
            dto.IsRead = false;

            JObject friendObj = (JObject)singleNotification[JsonKeys.FriendDTO];
            dto.FriendTableID = (long)friendObj[JsonKeys.UserTableID];
            dto.FriendRingID = (long)friendObj[JsonKeys.RID];
            dto.FriendName = (string)friendObj[JsonKeys.FullName];
            dto.ImageUrl = friendObj[JsonKeys.ProfileImage] != null ? (string)friendObj[JsonKeys.ProfileImage] : string.Empty;
            dto.PreviousIds = new List<Guid>();
            return dto;
        }

        public static string ParseRingIDFromResponse(string response)
        {
            try
            {
                JObject jObject = JObject.Parse(response);
                if (jObject[JsonKeys.IsSuccess] != null && (bool)jObject[JsonKeys.IsSuccess])
                {
                    string RingID = (string)jObject[JsonKeys.RingId];
                    if (!string.IsNullOrEmpty(RingID))
                    {
                        ServerAndPortSettings.AUTH_SERVER_IP = (string)jObject[JsonKeys.AuthServerIP];
                        ServerAndPortSettings.COMMUNICATION_PORT = (int)jObject[JsonKeys.ComPort];
                        return RingID;
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("EXCEPTION in RingIDFromSmidResponse => " + e.Message + "\n" + e.StackTrace);
            }
            return null;
        }

        public static int Get_country_index_from_contry_code(int startindex, String code)
        {
            for (int f = startindex; f < DefaultSettings.COUNTRY_MOBILE_CODE.Length; f++)
            {
                if (DefaultSettings.COUNTRY_MOBILE_CODE[f, 1].Equals(code)) return f;
            }
            return 0;
        }

        public static string Get_country_name_from_contry_code(int startindex, String code)
        {
            for (int f = startindex; f < DefaultSettings.COUNTRY_MOBILE_CODE.Length; f++)
            {
                if (DefaultSettings.COUNTRY_MOBILE_CODE[f, 1].Equals(code)) return DefaultSettings.COUNTRY_MOBILE_CODE[f, 0];
            }
            return "";
        }

        public static string EncryptPassword(string password)
        {
            string newPass = "";
            if (password != null && password.Length > 0)
            {
                for (int i = 0; i < password.Length; i++)
                {
                    int val = (int)password[i];
                    if (val < 2 || val > 126) newPass += password[i];
                    else if (val % 2 == 0) newPass += (char)(password[i] - 1);
                    else newPass += (char)(password[i] + 1);
                }
            }
            return newPass;
        }

        public static bool IsValidEmail(String input)
        {
            return Regex.IsMatch(input, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
        }

        public static bool IsValidNumber(String input)
        {
            return Regex.IsMatch(input, @"^\d+$");
        }

        public static string PhoneNumberStarred(string mbldc_mbl)
        {
            if (mbldc_mbl.Contains('-'))
            {
                string mblDc = mbldc_mbl.Split(new Char[] { '-' })[0];
                string mbl = mbldc_mbl.Split(new Char[] { '-' })[1];
                if (mbl.Length > 5)
                {
                    StringBuilder sb = new StringBuilder(mbl);
                    for (int i = 3; i < mbl.Length - 2; i++) sb[i] = '*';
                    return mblDc + "-" + sb.ToString();
                }
            }
            return mbldc_mbl;
        }

        public static long GetTimeStampFromGUID(Guid guid)
        {
            string timeUUID = guid.ToString();
            char[] delimiterChars = { '-' };
            string[] uuid_parts = timeUUID.Split(delimiterChars);
            long msb = Int64.Parse(uuid_parts[0], System.Globalization.NumberStyles.HexNumber);//Convert.ToInt64(uuid_parts[0]);
            msb <<= 16;
            msb |= Int64.Parse(uuid_parts[1], System.Globalization.NumberStyles.HexNumber);//Convert.ToInt64(uuid_parts[1]);
            msb <<= 16;
            msb |= Int64.Parse(uuid_parts[2], System.Globalization.NumberStyles.HexNumber);//Convert.ToInt64(uuid_parts[2]);
            return ((msb & 0x0FFFL) << 48 | ((msb >> 16) & 0x0FFFFL) << 32 | msb >> 32);
        }

        public static string LoginValidationMsg(string userid, string pass, int login_type)
        {
            if (login_type == SettingsConstants.MOBILE_LOGIN && !IsValidNumber(userid)) return "Please provide a valid Mobile Number!";
            if (login_type == SettingsConstants.EMAIL_LOGIN && !IsValidEmail(userid)) return "Please provide a valid E-mail Address!";
            if (login_type == SettingsConstants.RINGID_LOGIN && !IsValidNumber(userid)) return "Please provide a valid ringID number!";
            if (string.IsNullOrEmpty(pass)) return "Must provide a password!";
            return string.Empty;
        }

        public static string RingidorMailorMobileValidationMsg(string userid, int type)
        {
            if (string.IsNullOrEmpty(userid))
            {
                if (type == SettingsConstants.MOBILE_LOGIN) return "Please enter a Mobile Number!";
                else if (type == SettingsConstants.EMAIL_LOGIN) return "Please enter a valid email address.";
                else if (type == SettingsConstants.RINGID_LOGIN) return "Please enter an ringID Number!";
            }
            if (type == SettingsConstants.MOBILE_LOGIN && !IsValidNumber(userid)) return "Please provide a valid Mobile Number!";
            if (type == SettingsConstants.EMAIL_LOGIN && !IsValidEmail(userid)) return "Please provide a valid E-mail Address!";
            if (type == SettingsConstants.RINGID_LOGIN && !IsValidNumber(userid)) return "Please provide a valid ringID number!";
            return string.Empty;
        }

        public static string PhoneNumberValidationMsg(string phone)
        {
            if (string.IsNullOrEmpty(phone)) return "Please enter a Mobile Number!";
            else
                if (!IsValidNumber(phone)) return "Please provide a valid Mobile Number!";
            return "";
        }

        public static string NamePasswordValidationMsg(string name, string pass, string retypePass)
        {

            if (string.IsNullOrEmpty(name)) return "Must provide a name!";
            if (!string.IsNullOrEmpty(name))
            {
                if (!(char.IsLetter(name[0])) && (!(char.IsNumber(name[0])))) return "Name must starts with alpha numeric characters!";
                if (name.Length > 30) return "The maximum length of name is 30 characters!";
                if (name.Length < 3) return "The minimum length of name is 3 characters!";

            }
            if (pass == null || pass.Length < 1) return "Must provide a password!";
            if (retypePass == null || retypePass.Length < 1) return "Must retype the password!";
            if (!pass.Equals(retypePass)) return "Passwords do not match!";
            return PasswordValidationMsg(pass);
        }

        public static string PasswordValidationMsg(string password)
        {
            if (password.Length < 6 || password.Length > 12) return "Your password must be 6 - 12 characters in length. Please try another.";
            if (password.Contains(",")) return NotificationMessages.COMMA_NOT_ALLOWED + "in Password";
            if (password.StartsWith(" ") || password.EndsWith(" ")) return "Password can't start or end with spaces";
            if (DefaultSettings.VALUE_NEW_USER_NAME > 0 && password.Contains(DefaultSettings.VALUE_NEW_USER_NAME.ToString())) return "Password can't contain  RingID";
            return "";
        }

        public static void ClearAllModelDictionaries()
        {
            try
            {
                RingDictionaries.Instance.ClearRingDictionaries();
                FriendDictionaries.Instance.ClearFriendDictionaries();
                NewsFeedDictionaries.Instance.ClearNewsFeedDictionaries();
                StickerDictionaries.Instance.ClearStickerDictionaries();
                CallDictionaries.Instance.ClearCallDictionaries();
                ChatDictionaries.Instance.ClearChatDictionaries();
                MediaDictionaries.Instance.ClearMediaDictionaries();
            }
            catch (Exception e)
            {
                log.Error("Exception in ClearAllModelDictionaries ==> " + e.Message + "\n" + e.StackTrace);
            }
        }

        public static JObject SingleJobject(string iurl, string cptn, int w, int h)
        {
            JObject obj = new JObject();
            obj[JsonKeys.ImageUrl] = iurl;
            obj[JsonKeys.ImageCaption] = cptn;
            obj[JsonKeys.ImageHeight] = h;
            obj[JsonKeys.ImageWidth] = w;
            return obj;
        }

        public static int GetImageTypeFromAlbum(string albumId)
        {
            if (albumId.Equals(DefaultSettings.PROFILE_IMAGE_ALBUM_ID, StringComparison.Ordinal)) return SettingsConstants.TYPE_PROFILE_IMAGE;
            else if (albumId.Equals(DefaultSettings.COVER_IMAGE_ALBUM_ID, StringComparison.Ordinal)) return SettingsConstants.TYPE_COVER_IMAGE;
            else if (albumId.Equals(DefaultSettings.FEED_IMAGE_ALBUM_ID, StringComparison.Ordinal)) return SettingsConstants.TYPE_NORMAL_BOOK_IMAGE;
            return 0;
        }

        public static string GetAlbumIDFromType(int imageType)
        {
            if (imageType == SettingsConstants.TYPE_PROFILE_IMAGE) return DefaultSettings.PROFILE_IMAGE_ALBUM_ID;
            else if (imageType == SettingsConstants.TYPE_COVER_IMAGE) return DefaultSettings.COVER_IMAGE_ALBUM_ID;
            else if (imageType == SettingsConstants.TYPE_NORMAL_BOOK_IMAGE) return DefaultSettings.FEED_IMAGE_ALBUM_ID;
            return null;
        }

        public static string FirstLetterToLower(string str)
        {
            if (str == null) return null;
            if (str.Length > 1) return char.ToLower(str[0]) + str.Substring(1);
            return str.ToUpper();
        }

        public static WorkDTO BindWorkDetails(JObject singleObj)
        {
            WorkDTO work = new WorkDTO();
            if (singleObj[JsonKeys.WorkId] != null) work.Id = (Guid)singleObj[JsonKeys.WorkId];
            if (singleObj[JsonKeys.Position] != null) work.Position = (string)singleObj[JsonKeys.Position];
            if (singleObj[JsonKeys.CompanyName] != null) work.CompanyName = (string)singleObj[JsonKeys.CompanyName];
            if (singleObj[JsonKeys.City] != null) work.City = (string)singleObj[JsonKeys.City];
            if (singleObj[JsonKeys.Description] != null) work.Description = (string)singleObj[JsonKeys.Description];
            if (singleObj[JsonKeys.FromTime] != null) work.FromTime = (long)singleObj[JsonKeys.FromTime];
            if (singleObj[JsonKeys.ToTime] != null) work.ToTime = (long)singleObj[JsonKeys.ToTime];
            if (singleObj[JsonKeys.UpdateTime] != null) work.UpdateTime = (long)singleObj[JsonKeys.UpdateTime];
            return work;
        }

        public static EducationDTO BindEducationDetails(JObject singleObj)
        {
            EducationDTO education = new EducationDTO();
            if (singleObj[JsonKeys.SchoolId] != null) education.Id = (Guid)singleObj[JsonKeys.SchoolId];
            if (singleObj[JsonKeys.SchoolName] != null) education.SchoolName = (string)singleObj[JsonKeys.SchoolName];
            if (singleObj[JsonKeys.Concentration] != null) education.Concentration = (string)singleObj[JsonKeys.Concentration];
            if (singleObj[JsonKeys.Description] != null) education.Description = (string)singleObj[JsonKeys.Description];
            if (singleObj[JsonKeys.AttendedFor] != null) education.AttendedFor = (int)singleObj[JsonKeys.AttendedFor];
            if (singleObj[JsonKeys.Degree] != null) education.Degree = (string)singleObj[JsonKeys.Degree];
            if (singleObj[JsonKeys.Graduated] != null) education.Graduated = (bool)singleObj[JsonKeys.Graduated];
            if (singleObj[JsonKeys.IsSchool] != null) education.IsSchool = (bool)singleObj[JsonKeys.IsSchool];
            if (singleObj[JsonKeys.FromTime] != null) education.FromTime = (long)singleObj[JsonKeys.FromTime];
            if (singleObj[JsonKeys.ToTime] != null) education.ToTime = (long)singleObj[JsonKeys.ToTime];
            if (singleObj[JsonKeys.UpdateTime] != null) education.UpdateTime = (long)singleObj[JsonKeys.UpdateTime];
            return education;
        }

        public static SkillDTO BindSkillDetails(JObject singleObj)
        {
            SkillDTO Skill = new SkillDTO();
            if (singleObj[JsonKeys.SkillId] != null) Skill.Id = (Guid)singleObj[JsonKeys.SkillId];
            if (singleObj[JsonKeys.Skill] != null) Skill.SkillName = (string)singleObj[JsonKeys.Skill];
            if (singleObj[JsonKeys.Description] != null) Skill.Description = (string)singleObj[JsonKeys.Description];
            if (singleObj[JsonKeys.UpdateTime] != null) Skill.UpdateTime = (long)singleObj[JsonKeys.UpdateTime];
            return Skill;
        }
    }
}

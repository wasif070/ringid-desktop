﻿using log4net;
using Models.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Auth.utility.Feed
{
    public class CustomSortedList : List<CustomSortedList.Data>
    {
        private readonly ILog log = LogManager.GetLogger(typeof(CustomSortedList).Name);
        private readonly object syncLock = new object();


        public void InsertDataFromServer(long nfId, long tm, int feedCategory, bool CheckDuplicateNeeded = true)
        {
            Data data = new Data(nfId, tm, true, (feedCategory == SettingsConstants.SPECIAL_FEED) ? true : false);
            InsertData(data, CheckDuplicateNeeded);
        }

        public void InsertDataFromServer(long nfId)
        {
            Data data = new Data(nfId, 0, true, false);
            InsertDataSequentially(data);
        }

        public void InsertData(long nfId, long tm, int feedCategory)
        {
            Data data = new Data(nfId, tm, false, (feedCategory == SettingsConstants.SPECIAL_FEED) ? true : false);
            InsertData(data);
        }

        private void InsertDataSequentially(Data data)
        {
            lock (syncLock)
            {
                int spclFeedCount = 0, removeIdx = -1, len = this.Count;
                for (int i = 0; i < len; i++)
                {
                    Data dt = this[i];
                    if (dt.Spceicality) spclFeedCount++;
                    if (removeIdx < 0 && dt.NfId == data.NfId) removeIdx = i;
                }
                if (removeIdx >= 0) this.RemoveAt(removeIdx);

                int max = this.Count;
                int min = 0;
                int pivot;

                if (!data.Spceicality)
                {
                    min += spclFeedCount;
                }
                else
                {
                    max = (spclFeedCount > 0) ? spclFeedCount : 0;
                }

                while (max > min)
                {
                    pivot = (min + max) / 2;
                    min = pivot + 1;
                }

                this.Insert(min, data);
            }

        }

        private void InsertData(Data data, bool CheckDuplicateNeeded = true)
        {
            lock (syncLock)
            {
                int spclFeedCount = 0, removeIdx = -1, len = this.Count;
                if (CheckDuplicateNeeded)
                {
                    for (int i = 0; i < len; i++)
                    {
                        Data dt = this[i];
                        if (dt.Spceicality) spclFeedCount++;
                        if (removeIdx < 0 && dt.NfId == data.NfId) { removeIdx = i; } //break;
                    }
                    if (removeIdx >= 0) this.RemoveAt(removeIdx);
                }
                int max = this.Count;
                int min = 0;
                int pivot;

                if (!data.Spceicality)
                {
                    min += spclFeedCount;
                }
                else
                {
                    max = (spclFeedCount > 0) ? spclFeedCount : 0;
                }

                while (max > min)
                {
                    pivot = (min + max) / 2;
                    if (data.Tm < this[pivot].Tm)
                    {
                        min = pivot + 1;
                    }
                    else
                    {
                        max = pivot;
                    }
                }
                this.Insert(min, data);
            }
        }

        public void RemoveData(long nfId)
        {
            lock (syncLock)
            {
                Data temp = null;
                if (this.Count > 0)
                {
                    foreach (Data data in this)
                    {
                        if (data.NfId == nfId)
                        {
                            temp = data;
                            break;
                        }
                    }
                }

                this.Remove(temp);
            }
        }

        public Data GetData(long nfId)
        {
            lock (syncLock)
            {
                Data temp = null;
                if (this.Count > 0)
                {
                    foreach (Data data in this)
                    {
                        if (data.NfId == nfId)
                        {
                            temp = data;
                            break;
                        }
                    }
                }
                return temp;
            }
        }

        public int GetIndex(long nfId)
        {
            lock (syncLock)
            {
                //Data temp = null;
                if (this.Count > 0)
                {
                    //foreach (Data data in this)
                    //{
                    //    if (data.GetNfId() == nfId)
                    //    {
                    //        temp = data;
                    //        return this.IndexOf(temp);
                    //    }
                    //}
                    int len = this.Count;
                    for (int i = 0; i < this.Count; i++)
                    {
                        if (this[i].NfId == nfId) return i;
                    }
                }
                return -1;
            }
        }

        public bool HsDataFromServer()
        {
            lock (syncLock)
            {
                if (this.Count > 0)
                {
                    foreach (Data data in this)
                    {
                        if (data.FromServer)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }

        public int GetIndex(Data data)
        {
            lock (syncLock)
            {
                return this.IndexOf(data);
            }
        }

        public Data GetFirstData()
        {
            lock (syncLock)
            {
                Data temp = null;
                temp = this.Where(P => !P.Spceicality).FirstOrDefault();
                return temp;
            }
        }

        public Data GetLastData()
        {
            lock (syncLock)
            {
                Data temp = null;
                temp = this.Where(P => !P.Spceicality).LastOrDefault();
                return temp;
            }
        }

        public class Data
        {
            private long nfId;
            private long tm;
            private bool fromServer;
            private bool isSpecial;

            public long NfId
            {
                get { return nfId; }
            }
            public long Tm
            {
                get { return tm; }
            }
            public bool FromServer
            {
                get { return fromServer; }
            }
            public bool Spceicality
            {
                get { return isSpecial; }
            }

            public Data(long nfId, long tm, bool fromServer, bool isSpecial = false)
            {
                this.nfId = nfId;
                this.tm = tm;
                this.fromServer = fromServer;
                this.isSpecial = isSpecial;
            }

            //public long GetNfId()
            //{
            //    return nfId;
            //}

            //public long GetTm()
            //{
            //    return tm;
            //}

            //public bool GetFromServer()
            //{
            //    return fromServer;
            //}

            //public bool GetSpceicality()
            //{
            //    return isSpecial;
            //}
        }

    }
}

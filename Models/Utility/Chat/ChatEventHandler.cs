﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Utility.Chat
{

    public delegate void ChatEventDelegate(ChatEventArgs args);

    public class ChatEventArgs
    {
        public string PacketID { get; set; }
        public long FriendTableID { get; set; }
        public long GroupID { get; set; }
        public long AnonymousID { get; set; }
        public string RoomID { get; set; }
        public long FileID { get; set; }
        public bool Status { get; set; }
        public int NumberOfMembers { get; set; }
        public string ServerIP { get; set; }
        public int ServerPort { get; set; }
    }

    public class ChatEventHandler
    {
        public static object CHAT_REG_EVENT_LOCKER = new object();
        ArrayList delegates = new ArrayList();

        private event ChatEventDelegate _OnCompleted;
        public long FriendTableID { get; set; }
        public long GroupID { get; set; }
        public string RoomID { get; set; }
        public long Time { get; set; }

        public event ChatEventDelegate OnCompleted
        {
            add
            {
                _OnCompleted += value;
                delegates.Add(value);
            }

            remove
            {
                _OnCompleted -= value;
                delegates.Remove(value);
            }
        }

        public Array RemoveAllEvents()
        {
            Array temp = delegates.ToArray(typeof(ChatEventDelegate));

            foreach (ChatEventDelegate ced in delegates)
            {
                _OnCompleted -= ced;
            }
            delegates.Clear();

            return temp;
        }

    }

}

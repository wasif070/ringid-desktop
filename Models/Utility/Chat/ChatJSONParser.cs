﻿using log4net;
using Models.Constants;
using Models.Entity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Models.Utility.Chat
{
    public class ChatJSONParser
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(ChatJSONParser).Name);

        public static void ParseMessage(MessageDTO messageDTO)
        {
            try
            {
                switch (messageDTO.MessageType)
                {
                    case ChatConstants.TYPE_PLAIN_MSG:
                    case ChatConstants.TYPE_EMOTICON_MSG:
                    case ChatConstants.TYPE_DOWNLOADED_STICKER_MSG:
                        if (!String.IsNullOrWhiteSpace(messageDTO.OriginalMessage))
                        {
                            messageDTO.IsChatMessage = true;
                            messageDTO.Message = messageDTO.OriginalMessage;
                        }
                        break;
                    case ChatConstants.TYPE_IMAGE_FILE_DIRECTORY:
                    case ChatConstants.TYPE_IMAGE_FILE_RING_CAMERA:
                        if (!String.IsNullOrWhiteSpace(messageDTO.OriginalMessage))
                        {
                            messageDTO.IsChatMessage = true;
                            JObject obj = JObject.Parse(messageDTO.OriginalMessage);
                            if (obj["u"] != null)
                            {
                                messageDTO.Message = obj["u"].ToString();
                            }
                            if (obj["c"] != null)
                            {
                                messageDTO.Caption = obj["c"].ToString();
                            }
                            if (obj["w"] != null)
                            {
                                messageDTO.Width = (int)obj["w"];
                            }
                            if (obj["h"] != null)
                            {
                                messageDTO.Height = (int)obj["h"];
                            }
                        }
                        break;
                    case ChatConstants.TYPE_AUDIO_FILE:
                    case ChatConstants.TYPE_VIDEO_FILE:
                        if (!String.IsNullOrWhiteSpace(messageDTO.OriginalMessage))
                        {
                            messageDTO.IsChatMessage = true;
                            JObject obj = JObject.Parse(messageDTO.OriginalMessage);
                            if (obj["u"] != null)
                            {
                                messageDTO.Message = obj["u"].ToString();
                            }
                            if (obj["w"] != null)
                            {
                                messageDTO.FileSize = (long)obj["w"];
                            }
                            if (obj["h"] != null)
                            {
                                messageDTO.Duration = (int)obj["h"];
                            }
                        }
                        break;
                    case ChatConstants.TYPE_LINK_MSG:
                        if (!String.IsNullOrWhiteSpace(messageDTO.OriginalMessage))
                        {
                            messageDTO.IsChatMessage = true;
                            JObject obj = JObject.Parse(messageDTO.OriginalMessage);
                            if (obj["m"] != null)
                            {
                                messageDTO.Message = obj["m"].ToString();
                            }
                            if (obj["u"] != null)
                            {
                                messageDTO.LinkUrl = obj["u"].ToString();
                            }
                            if (obj["t"] != null)
                            {
                                messageDTO.LinkTitle = obj["t"].ToString();
                            }
                            if (obj["i"] != null)
                            {
                                messageDTO.LinkImageUrl = obj["i"].ToString();
                            }
                            if (obj["d"] != null)
                            {
                                messageDTO.LinkDescription = obj["d"].ToString();
                            }
                        }
                        break;
                    case ChatConstants.TYPE_LOCATION_MSG:
                        if (!String.IsNullOrWhiteSpace(messageDTO.OriginalMessage))
                        {
                            messageDTO.IsChatMessage = true;
                            JObject obj = JObject.Parse(messageDTO.OriginalMessage);
                            if (obj["loc"] != null)
                            {
                                messageDTO.Message = obj["loc"].ToString();
                            }
                            if (obj["la"] != null)
                            {
                                messageDTO.Latitude = (float)obj["la"];
                            }
                            if (obj["lo"] != null)
                            {
                                messageDTO.Longitude = (float)obj["lo"];
                            }
                        }
                        break;
                    case ChatConstants.TYPE_RING_MEDIA_MSG:
                        if (!String.IsNullOrWhiteSpace(messageDTO.OriginalMessage))
                        {
                            messageDTO.IsChatMessage = true;
                            JObject obj = JObject.Parse(messageDTO.OriginalMessage);
                            if (obj[JsonKeys.MediaType] != null)
                            {
                                messageDTO.MediaType = (int)obj[JsonKeys.MediaType];
                            }
                            if (obj[JsonKeys.Title] != null)
                            {
                                messageDTO.MediaTitle = obj[JsonKeys.Title].ToString();
                            }
                            if (obj[JsonKeys.AlbumName] != null)
                            {
                                messageDTO.MediaAlbum = obj[JsonKeys.AlbumName].ToString();
                            }
                            if (obj[JsonKeys.ThumbUrl] != null)
                            {
                                messageDTO.LinkImageUrl = obj[JsonKeys.ThumbUrl].ToString();
                            }
                            if (obj[JsonKeys.Artist] != null)
                            {
                                messageDTO.MediaArtist = obj[JsonKeys.Artist].ToString();
                            }
                            if (obj[JsonKeys.MediaDuration] != null)
                            {
                                messageDTO.Duration = (int)obj[JsonKeys.MediaDuration];
                            }
                            if (obj[JsonKeys.ThumbImageWidth] != null)
                            {
                                messageDTO.Width = (int)obj[JsonKeys.ThumbImageWidth];
                            }
                            if (obj[JsonKeys.ThumbImageHeight] != null)
                            {
                                messageDTO.Height = (int)obj[JsonKeys.ThumbImageHeight];
                            }
                        }
                        break;
                    case ChatConstants.TYPE_STREAM_FILE:
                        if (!String.IsNullOrWhiteSpace(messageDTO.OriginalMessage))
                        {
                            messageDTO.IsChatMessage = true;
                            JObject obj = JObject.Parse(messageDTO.OriginalMessage);
                            if (obj["n"] != null)
                            {
                                messageDTO.Message = obj["n"].ToString();
                            }
                            if (obj["s"] != null)
                            {
                                messageDTO.FileSize = (long)obj["s"];
                            }
                            if (obj["i"] != null)
                            {
                                messageDTO.FileID = (long)obj["i"];
                            }
                            if (obj["m"] != null)
                            {
                                messageDTO.FileMenifest = obj["m"].ToString();
                            }
                            if (obj["c"] != null)
                            {
                                messageDTO.IsFileCancelled = (bool)obj["c"];
                            }
                        }
                        break;
                    case ChatConstants.TYPE_CONTACT_SHARE:
                        if (!String.IsNullOrWhiteSpace(messageDTO.OriginalMessage))
                        {
                            messageDTO.IsChatMessage = true;
                            JObject obj = JObject.Parse(messageDTO.OriginalMessage);
                            if (obj["u"] != null)
                            {
                                messageDTO.SharedTableID = (long)obj["u"];
                            }
                            if (obj["r"] != null)
                            {
                                messageDTO.SharedRingID = (long)obj["r"];
                            }
                            if (obj["n"] != null)
                            {
                                messageDTO.SharedFullName = obj["n"].ToString();
                            }
                            if (obj["p"] != null)
                            {
                                messageDTO.SharedProfileImage = obj["p"].ToString();
                            }
                        }
                        break;
                    case ChatConstants.TYPE_DELETE_MSG:
                        if (messageDTO.SenderTableID != DefaultSettings.LOGIN_TABLE_ID)
                        {
                            messageDTO.IsChatMessage = true;
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("Error: ParseMessage() ==> OriginalMessage = [" + messageDTO.OriginalMessage + "]\n" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static string MakeImageMessage(string imageUrl, string caption, double imageW, double imageH)
        {
            JObject mediaObj = new JObject();
            mediaObj["u"] = imageUrl ?? String.Empty;
            mediaObj["c"] = caption ?? String.Empty;
            mediaObj["w"] = imageW;
            mediaObj["h"] = imageH;
            return JsonConvert.SerializeObject(mediaObj, Formatting.None);
        }

        public static string MakeMultimediaMessage(string mediaUrl, long fileSize, int duration)
        {
            JObject mediaObj = new JObject();
            mediaObj["u"] = mediaUrl ?? String.Empty;
            mediaObj["w"] = fileSize;
            mediaObj["h"] = duration;
            return JsonConvert.SerializeObject(mediaObj, Formatting.None);
        }

        public static string MakeLocationMessage(float latitude, float longitude, string location)
        {
            JObject pakToSend = new JObject();
            pakToSend["la"] = latitude;
            pakToSend["lo"] = longitude;
            pakToSend["loc"] = location ?? String.Empty;
            return JsonConvert.SerializeObject(pakToSend, Formatting.None);
        }

        public static string MakeLinkMessage(string message, string url, string title, string imageUrl, string description)
        {
            JObject pakToSend = new JObject();
            pakToSend["m"] = message ?? String.Empty;
            pakToSend["u"] = url ?? String.Empty;
            pakToSend["t"] = title == null ? String.Empty : (title.Length > 197 ? title.Substring(0, 197) + "..." : title);
            pakToSend["i"] = imageUrl == null ? String.Empty : (Path.HasExtension(imageUrl) ? imageUrl : String.Empty);
            pakToSend["d"] = description == null ? String.Empty : (description.Length > 297 ? description.Substring(0, 297) + "..." : description);
            return JsonConvert.SerializeObject(pakToSend, Formatting.None);
        }

        public static string MakeStreamMessage(string filePath, long fileLength, long fileID, string menifestUrl = null, bool isFileCancelled = false)
        {
            JObject pakToSend = new JObject();
            pakToSend["n"] = filePath ?? String.Empty;
            pakToSend["s"] = fileLength;
            pakToSend["i"] = fileID;
            pakToSend["m"] = menifestUrl ?? String.Empty;
            pakToSend["c"] = isFileCancelled;
            return JsonConvert.SerializeObject(pakToSend, Formatting.None);
        }

        public static string MakeContactMessage(long userTableId, long ringId, string fullName, string profileImage)
        {
            JObject pakToSend = new JObject();
            pakToSend["u"] = userTableId;
            pakToSend["r"] = ringId;
            pakToSend["n"] = fullName ?? String.Empty;
            pakToSend["p"] = profileImage ?? String.Empty;
            return JsonConvert.SerializeObject(pakToSend, Formatting.None);
        }

        public static string MakeRingMediaMessage(SingleMediaDTO dto)
        {
            JObject pakToSend = new JObject();
            pakToSend[JsonKeys.ContentId] = dto.ContentId;
            pakToSend[JsonKeys.Title] = dto.Title;
            pakToSend[JsonKeys.Artist] = dto.Artist;
            pakToSend[JsonKeys.MediaType] = dto.MediaType;
            pakToSend[JsonKeys.StreamUrl] = dto.StreamUrl;
            pakToSend[JsonKeys.ThumbUrl] = dto.ThumbUrl;
            pakToSend[JsonKeys.MediaDuration] = dto.Duration;
            pakToSend[JsonKeys.ThumbImageWidth] = dto.ThumbImageWidth;
            pakToSend[JsonKeys.ThumbImageHeight] = dto.ThumbImageHeight;
            pakToSend[JsonKeys.MediaPrivacy] = dto.Privacy;
            pakToSend[JsonKeys.AlbumName] = dto.AlbumName;
            pakToSend[JsonKeys.AlbumId] = dto.AlbumId;
            pakToSend[JsonKeys.AccessCount] = dto.AccessCount;
            pakToSend[JsonKeys.LikeCount] = dto.LikeCount;
            pakToSend[JsonKeys.CommentCount] = dto.CommentCount;
            pakToSend[JsonKeys.UserTableID] = dto.UserTableID;
            pakToSend[JsonKeys.ILike] = dto.ILike;
            pakToSend[JsonKeys.IComment] = dto.IComment;
            pakToSend[JsonKeys.IShare] = dto.IShare;
            pakToSend[JsonKeys.NumberOfShares] = dto.ShareCount;
            pakToSend[JsonKeys.FullName] = dto.FullName;
            pakToSend[JsonKeys.ProfileImage] = dto.ProfileImage;
            return JsonConvert.SerializeObject(pakToSend, Formatting.None);
        }

    }
}

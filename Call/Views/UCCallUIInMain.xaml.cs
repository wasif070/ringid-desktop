﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Call.Utility;
using Call.ViewModels;

namespace Call.Views
{
    /// <summary>
    /// Interaction logic for UCCallUIInMain.xaml
    /// </summary>
    public partial class UCCallUIInMain : UserControl, INotifyPropertyChanged
    {
        #region"Fields"
        public event CallDelegates.DelegateBool OnRemovedFromUI;
        public bool needToLoadMainWindow = true;
        private WNFullScreenViwer tempContainer;

        #endregion

        #region"Ctors"
        public UCCallUIInMain()
        {
            InitializeComponent();
            DataModel = CallHelperMethods.CallViewModel;
            this.DataContext = this;
        }
        #endregion"Ctors"

        #region "Data Change Property"
        private VMCall dataModel;
        public VMCall DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }

        private bool isOpenVolumPopup = false;
        public bool IsOpenVolumPopup
        {
            get { return isOpenVolumPopup; }
            set
            {
                if (value == isOpenVolumPopup) return;
                isOpenVolumPopup = value;
                OnPropertyChanged("IsOpenVolumPopup");
            }
        }

        #endregion //"Property"

        #region "Commands and Command Methods"

        private ICommand loadedMainControl;
        public ICommand LoadedMainControl
        {
            get
            {
                if (loadedMainControl == null) loadedMainControl = new RelayCommand(param => OnLoadedMainControl());
                return loadedMainControl;
            }
        }
        private void OnLoadedMainControl()
        {
            if (!DataModel.IsInFullScreen)
            {
                if (DataModel.CallUiState == CallStates.UA_OUTGOING_CALL)
                    DataModel.LoaderImageSource = CallImages.OUTGOING_ANIMATION;
                //CallHelperMethods.CallViewModel.FriendProfileSourceMini = ImageUtility.GetDetaultProfileImageWithName(DataModel.FullName);
                //CallHelperMethods.CallViewModel.FriendProfileSource = ImageUtility.GetDetaultProfileImageWithName(model.ShortInfoModel, 150, 150);
                //CallHelperMethods.CallViewModel.MyProfileImge = ImageUtility.GetDetaultProfileImageWithName(RingIDViewModel.Instance.MyBasicInfoModel.ShortInfoModel, 150, 150);

                Task tsk = new Task(delegate
                {
                    changeProfileImages();
                });
                tsk.Start();
            }
            if (CallHelperMethods.UserControlToAddCallView.Content == this)
            {
                DataModel.IsInFullScreen = false;
                needToLoadMainWindow = true;
            }
            else needToLoadMainWindow = false;
        }

        private ICommand unLoadedMainControl;
        public ICommand UnLoadedMainControl
        {
            get
            {
                if (unLoadedMainControl == null) unLoadedMainControl = new RelayCommand(param => OnUnLoadedMainControl());
                return unLoadedMainControl;
            }
        }
        private void OnUnLoadedMainControl()
        {
            if (OnRemovedFromUI != null) OnRemovedFromUI(needToLoadMainWindow);
        }

        private ICommand mouseEnterCommand;
        public ICommand MouseEnterCommand
        {
            get
            {
                if (mouseEnterCommand == null) mouseEnterCommand = new RelayCommand(param => OnMouseEnterCommand());
                return mouseEnterCommand;
            }
        }
        private void OnMouseEnterCommand()
        {
        }

        private ICommand mouseLeaveCommand;
        public ICommand MouseLeaveCommand
        {
            get
            {
                if (mouseLeaveCommand == null) mouseLeaveCommand = new RelayCommand(param => OnMouseLeaveCommand());
                return mouseLeaveCommand;
            }
        }
        private void OnMouseLeaveCommand()
        {

        }

        private ICommand goToSmallScreen;
        public ICommand GoToSmallScreen
        {
            get
            {
                if (goToSmallScreen == null) goToSmallScreen = new RelayCommand(param => OnGoToSmallScreen());
                return goToSmallScreen;
            }
        }
        public void OnGoToSmallScreen()
        {
            hideUI(false);
        }

        private ICommand doubleClickOnScreen;
        public ICommand DoubleClickOnScreen
        {
            get
            {
                if (doubleClickOnScreen == null) doubleClickOnScreen = new RelayCommand(param => OnDoubleClickOnScreen());
                return doubleClickOnScreen;
            }
        }
        private void OnDoubleClickOnScreen()
        {
            needToLoadMainWindow = false;
            if (tempContainer == null)
            {
                tempContainer = new WNFullScreenViwer();
            }
            if (tempContainer._TempControl.Child == null)
            {
                DataModel.IsInFullScreen = true;
                tempContainer.Show();
                CallHelperMethods.UserControlToAddCallView.Content = null;
                tempContainer._TempControl.Child = this;
            }
            else
            {
                tempContainer._TempControl.Child = null;
                AddInMiddlePanel();
                tempContainer.Close();
                tempContainer = null;
            }
        }

        private ICommand endCall;
        public ICommand EndCallCommand
        {
            get
            {
                if (endCall == null) endCall = new RelayCommand(param => OnEndCallCommand());
                return endCall;
            }
        }
        public void OnEndCallCommand()
        {
            if (tempContainer != null)
            {
                tempContainer.Close();
                tempContainer = null;
            }
            CallStates.CurrentCallState = CallStates.UA_IDLE;
            hideUI(true);
        }

        private ICommand contextMenuPopup;
        public ICommand ContextMenuPopup
        {
            get
            {
                if (contextMenuPopup == null) contextMenuPopup = new RelayCommand(param => OnContextMenuPopup(param));
                return contextMenuPopup;
            }
            set { contextMenuPopup = value; }
        }
        private void OnContextMenuPopup(object sender)
        {
            DataModel.IsOpenContextMenu = true;
        }

        private ICommand chatButtonCommand;
        public ICommand ChatButtonCommand
        {
            get
            {
                if (chatButtonCommand == null) chatButtonCommand = new RelayCommand(pararm => OnChatButton(pararm));
                return chatButtonCommand;
            }
            set { chatButtonCommand = value; }
        }
        private void OnChatButton(object pararm)
        {
        }

        private ICommand muteButtonCommand;
        public ICommand MuteButtonCommand
        {
            get
            {
                if (muteButtonCommand == null) muteButtonCommand = new RelayCommand(param => OnMuteButtonClick(param));
                return muteButtonCommand;
            }
            set { muteButtonCommand = value; }
        }
        private void OnMuteButtonClick(object param)
        {
            DataModel.IsMuted = true;
        }

        private ICommand unMuteButtonCommand;
        public ICommand UnMuteButtonCommand
        {
            get
            {
                if (unMuteButtonCommand == null) unMuteButtonCommand = new RelayCommand(param => OnUnMuteButtonClick(param));
                return unMuteButtonCommand;
            }
            set { unMuteButtonCommand = value; }
        }
        private void OnUnMuteButtonClick(object param)
        {
            DataModel.IsMuted = false;
        }

        private ICommand startVideoButtonCommand;
        public ICommand StartVideoButtonCommand
        {
            get
            {
                if (startVideoButtonCommand == null) startVideoButtonCommand = new RelayCommand(param => OnStartVideoCommand(param));
                return startVideoButtonCommand;
            }
            set { startVideoButtonCommand = value; }
        }
        private void OnStartVideoCommand(object param)
        {
            DataModel.IsVideoStopped = true;
        }

        private ICommand stopVideoButtonCommand;
        public ICommand StopVideoButtonCommand
        {
            get
            {
                if (stopVideoButtonCommand == null)
                    stopVideoButtonCommand = new RelayCommand(param => OnStopVideoButtonMainClick(param));
                return stopVideoButtonCommand;
            }
            set { stopVideoButtonCommand = value; }
        }
        private void OnStopVideoButtonMainClick(object param)
        {
            DataModel.IsVideoStopped = false;
        }

        private ICommand volumeButtonCommand;
        public ICommand VolumeButtonCommand
        {
            get
            {
                if (volumeButtonCommand == null) volumeButtonCommand = new RelayCommand(param => OnVolumeButtonClick(param));
                return volumeButtonCommand;
            }
            set { volumeButtonCommand = value; }
        }
        private void OnVolumeButtonClick(object param)
        {
            IsOpenVolumPopup = true;
            float currentVolumn = MasterVolumControl.GetCurrentVolumnLabel();
        }

        private int volumnInView;
        public int VolumnInView
        {
            get { return volumnInView; }
            set { volumnInView = value; OnPropertyChanged("VolumnInView"); }
        }
        #endregion

        #region "Uitily Methods"
        private void addMiddlePanelSwitcher()
        {
            //if (MiddlePanelSwitcher.pageSwitcher.Content == this)
            //{
            //    if (DataModel.UserBasicInfoModel != null && DataModel.UserBasicInfoModel.ShortInfoModel.UserTableID > 0)
            //        View.ViewModel.RingIDViewModel.Instance.OnFriendCallChatButtonClicked(DataModel.UserBasicInfoModel.ShortInfoModel.UserTableID);
            //    else
            //        RingIDViewModel.Instance.OnAllFeedsClicked(new object());
            //}
        }

        public void AddInMiddlePanel()
        {
            //MiddlePanelSwitcher.AddUserControl(this);
            DataModel.IsInFullScreen = false;
        }

        private void hideUI(bool isCallEnded)
        {
            addMiddlePanelSwitcher();
        }

        private void changeProfileImages()
        {
            try
            {
                System.Windows.Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    //ImageSource myImage = ImageUtility.GetImageSoruceFromBasicInfo(RingIDViewModel.Instance.MyBasicInfoModel, ImageUtility.IMG_THUMB, 150, 150);
                    //ImageSource freindProfile = ImageUtility.GetImageSoruceFromBasicInfo(RingIDViewModel.Instance.VMCall.UserBasicInfoModel, ImageUtility.IMG_THUMB, 150, 150);
                    //if (RingIDViewModel.Instance.VMCall != null)
                    //{
                    //    RingIDViewModel.Instance.VMCall.MyProfileImge = myImage;
                    //    RingIDViewModel.Instance.VMCall.FriendProfileSourceMini = freindProfile;
                    //    RingIDViewModel.Instance.VMCall.FriendProfileSource = freindProfile;
                    //}
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }
        #endregion "Utility Methods"

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

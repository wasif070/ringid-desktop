﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Call.Utility;
using Call.ViewModels;
using log4net;

namespace Call.Views
{
    /// <summary>
    /// Interaction logic for WNCallInSeparateView.xaml
    /// </summary>
    public partial class WNCallInSeparateView : Window, INotifyPropertyChanged
    {
        #region"Fields"
        private static readonly ILog logger = LogManager.GetLogger(typeof(WNCallInSeparateView).Name);
        public event CallDelegates.DelegateBool OnClosedWindow;
        UCBusyMessages busyMessagesPopup;
        #endregion

        #region"Ctors"
        public WNCallInSeparateView()
        {
            InitializeComponent();
            this.DataContext = this;
            this.Loaded += WNCallInSeparateView_Loaded;
            //this.Owner = Application.Current.MainWindow;
            DataModel = CallHelperMethods.CallViewModel;
            this.MouseDown += WNCallUiInSmallScreen_MouseDown;
        }

        #endregion"Ctors"

        #region "Data Change Property"
        private VMCall dataModel;
        public VMCall DataModel
        {
            get { return dataModel; }
            set { dataModel = value; OnPropertyChanged("DataModel"); }
        }
        #endregion //"Property"

        #region "Commands and Command Methods"

        private ICommand mouseEnterCommand;
        public ICommand MouseEnterCommand
        {
            get
            {
                if (mouseEnterCommand == null) mouseEnterCommand = new RelayCommand(param => OnMouseEnterCommand());
                return mouseEnterCommand;
            }
        }
        private void OnMouseEnterCommand()
        {
        }

        private ICommand mouseLeaveCommand;
        public ICommand MouseLeaveCommand
        {
            get
            {
                if (mouseLeaveCommand == null) mouseLeaveCommand = new RelayCommand(param => OnMouseLeaveCommand());
                return mouseLeaveCommand;
            }
        }
        private void OnMouseLeaveCommand()
        {

        }

        private ICommand showInMainView;
        public ICommand ShowInMainView
        {
            get
            {
                if (showInMainView == null)
                {
                    showInMainView = new RelayCommand(param => OnShowInMainView(param));
                } return showInMainView;
            }
        }
        private void OnShowInMainView(object param)
        {
            closeWindow(false);
        }

        private ICommand goToSmallScreen;
        public ICommand GoToSmallScreen
        {
            get
            {
                if (goToSmallScreen == null) goToSmallScreen = new RelayCommand(param => OnGoToSmallScreen());
                return goToSmallScreen;
            }
        }
        public void OnGoToSmallScreen()
        {
            setLoacation();
            Keyboard.Focus(this);
        }

        private ICommand doubleClickOnScreen;
        public ICommand DoubleClickOnScreen
        {
            get
            {
                if (doubleClickOnScreen == null) doubleClickOnScreen = new RelayCommand(param => OnDoubleClickOnScreen());
                return doubleClickOnScreen;
            }
        }
        private void OnDoubleClickOnScreen()
        {
            if (CallStates.CurrentCallState == CallStates.UA_OUTGOING_CALL || CallStates.CurrentCallState == CallStates.UA_ONCALL)
            {
                closeWindow(false);
            }
        }

        private ICommand endCall;
        public ICommand EndCallCommand
        {
            get
            {
                if (endCall == null) endCall = new RelayCommand(param => OnEndCallCommand());
                return endCall;
            }
        }
        public void OnEndCallCommand()
        {
            CallStates.CurrentCallState = CallStates.UA_IDLE;
            closeWindow(true);
        }

        private ICommand audioCallAcceptCommand;
        public ICommand AudioCallAcceptCommand
        {
            get
            {
                if (audioCallAcceptCommand == null) audioCallAcceptCommand = new RelayCommand(param => OnAudioCallAcceptCommand());
                return audioCallAcceptCommand;
            }
            set { audioCallAcceptCommand = value; }
        }
        private void OnAudioCallAcceptCommand()
        {
            DataModel.IsAcceptedWithVideo = false;
            CallHelperMethods.InitOnCall();
        }

        private ICommand videoCallAcceptCommand;
        public ICommand VideoCallAcceptCommand
        {
            get
            {
                if (videoCallAcceptCommand == null) videoCallAcceptCommand = new RelayCommand(param => OnVideoCallAcceptClick());
                return videoCallAcceptCommand;
            }
            set { videoCallAcceptCommand = value; }
        }
        private void OnVideoCallAcceptClick()
        {
            DataModel.IsAcceptedWithVideo = true;
            CallHelperMethods.InitOnCall();
        }

        private ICommand busyMessageCommand;
        public ICommand BusyMessageCommand
        {
            get
            {
                if (busyMessageCommand == null) busyMessageCommand = new RelayCommand(param => OnBusyMessageClick());
                return busyMessageCommand;
            }
            set { busyMessageCommand = value; }
        }
        private void OnBusyMessageClick()
        {
            if (busyMessagesPopup == null) busyMessagesPopup = new UCBusyMessages(_mainGrid);
            busyMessagesPopup.Show();
            busyMessagesPopup.OnClickedItem += (messge) =>
            {
                try
                {
                    DataModel.BusyMessage = messge;
                    busyMessagesPopup.Hide();
                    OnEndCallCommand();
                }
                finally { }
            };
            busyMessagesPopup.OnRemovedUserControl += () =>
            {
                busyMessagesPopup = null;
            };
        }

        private ICommand contextMenuPopup;
        public ICommand ContextMenuPopup
        {
            get
            {
                if (contextMenuPopup == null) contextMenuPopup = new RelayCommand(param => OnContextMenuPopup());
                return contextMenuPopup;
            }
            set { contextMenuPopup = value; }
        }
        private void OnContextMenuPopup()
        {
            DataModel.IsOpenContextMenu = true;
        }

        private ICommand chatButtonCommand;
        public ICommand ChatButtonCommand
        {
            get
            {
                if (chatButtonCommand == null) chatButtonCommand = new RelayCommand(pararm => OnChatButton(pararm));
                return chatButtonCommand;
            }
            set { chatButtonCommand = value; }
        }
        private void OnChatButton(object pararm)
        {
        }

        private ICommand muteButtonCommand;
        public ICommand MuteButtonCommand
        {
            get
            {
                if (muteButtonCommand == null) muteButtonCommand = new RelayCommand(param => OnMuteButtonClick(param));
                return muteButtonCommand;
            }
            set { muteButtonCommand = value; }
        }
        private void OnMuteButtonClick(object param)
        {
            DataModel.IsMuted = true;
        }

        private ICommand unMuteButtonCommand;
        public ICommand UnMuteButtonCommand
        {
            get
            {
                if (unMuteButtonCommand == null) unMuteButtonCommand = new RelayCommand(param => OnUnMuteButtonClick(param));
                return unMuteButtonCommand;
            }
            set { unMuteButtonCommand = value; }
        }
        private void OnUnMuteButtonClick(object param)
        {
            DataModel.IsMuted = false;
        }

        private ICommand startVideoButtonCommand;
        public ICommand StartVideoButtonCommand
        {
            get
            {
                if (startVideoButtonCommand == null) startVideoButtonCommand = new RelayCommand(param => OnStartVideoCommand(param));
                return startVideoButtonCommand;
            }
            set { startVideoButtonCommand = value; }
        }
        private void OnStartVideoCommand(object param)
        {
            DataModel.IsVideoStopped = false;
        }

        private ICommand stopVideoButtonCommand;
        public ICommand StopVideoButtonCommand
        {
            get
            {
                if (stopVideoButtonCommand == null)
                    stopVideoButtonCommand = new RelayCommand(param => OnStopVideoButtonMainClick(param));
                return stopVideoButtonCommand;
            }
            set { stopVideoButtonCommand = value; }
        }
        private void OnStopVideoButtonMainClick(object param)
        {
            DataModel.IsVideoStopped = true;
        }
        #endregion

        #region "Uitily Methods"

        private void setLoacation()
        {

            var desktopWorkingArea = System.Windows.SystemParameters.WorkArea;
            this.Left = desktopWorkingArea.Right - this.Width;
            this.Top = desktopWorkingArea.Bottom / 2;
        }

        private void closeWindow(bool isCallEnd)
        {
            if (OnClosedWindow != null) OnClosedWindow(isCallEnd);
            CloseThis();
        }

        public void CloseThis()
        {
            this.Loaded -= WNCallInSeparateView_Loaded;
            this.MouseDown -= WNCallUiInSmallScreen_MouseDown;
            this.Close();
        }

        private void changeProfileImages()
        {
            try
            {
                System.Windows.Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    //ImageSource freindProfile = ImageUtility.GetDetaultProfileImageWithName(CallHelperMethods.CallViewModel.FullName, 150, 150);
                    //if (CallHelperMethods.CallViewModel != null)
                    //{
                    //    CallHelperMethods.CallViewModel.FriendProfileSourceMini = freindProfile;
                    //    CallHelperMethods.CallViewModel.FriendProfileSource = freindProfile;
                    //}
                });
            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace);
            }
        }
        #endregion "Utility Methods"

        #region"Event Triggers"

        private void WNCallInSeparateView_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                setLoacation();
                //if (CallStates.CurrentCallState == CallStates.UA_INCOMING_CALL) CallHelperMethods.CallViewModel.LoaderImageSource = CallImages.INCOMING_CALL_ANIMATION;
                //CallHelperMethods.CallViewModel.FriendProfileSourceMini = ImageUtility.GetDetaultProfileImageWithName(model.ShortInfoModel);
                //CallHelperMethods.CallViewModel.FriendProfileSource = ImageUtility.GetDetaultProfileImageWithName(model.ShortInfoModel, 150, 150);
                Task tsk = new Task(delegate
                {
                    changeProfileImages();
                });
                tsk.Start();
            }
            catch (Exception)
            {
            }
        }
        void WNCallUiInSmallScreen_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }
        #endregion"Event Triggers"

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

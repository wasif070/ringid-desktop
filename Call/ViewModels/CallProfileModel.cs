﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Call.ViewModels
{
    class CallProfileModel : BaseViewModel
    {
        private long userTableID;
        public long UserTableID
        {
            get { return userTableID; }
            set
            {
                if (value == userTableID) return;
                userTableID = value;
            }
        }

        private string fullName;
        public string FullName
        {
            get { return fullName; }
            set
            {
                if (value == fullName) return;
                fullName = value;
                this.OnPropertyChanged("FullName");
            }
        }

        private string profileImage;
        public string ProfileImage
        {
            get { return profileImage; }
            set
            {
                if (value == profileImage) return;
                profileImage = value;
                this.OnPropertyChanged("ProfileImage");
            }
        }

        private long userIdentity;
        public long UserIdentity
        {
            get { return userIdentity; }
            set
            {
                if (value == userIdentity) return;
                userIdentity = value;
                this.OnPropertyChanged("UserIdentity");
            }
        }
    }
}

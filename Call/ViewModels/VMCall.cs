﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using Call.Utility;

namespace Call.ViewModels
{
    public class VMCall : BaseViewModel
    {
        #region  "Fieldes"
        #endregion "Fields"

        #region "Constructor"
        public VMCall() { }
        #endregion "Constructor"

        #region "Data Change Property"
        private long userTableID;
        public long UserTableID
        {
            get { return userTableID; }
            set
            {
                if (value == userTableID) return;
                userTableID = value;
            }
        }

        private string fullName;
        public string FullName
        {
            get { return fullName; }
            set
            {
                if (value == fullName) return;
                fullName = value;
                this.OnPropertyChanged("FullName");
            }
        }

        private string profileImage;
        public string ProfileImage
        {
            get { return profileImage; }
            set
            {
                if (value == profileImage) return;
                profileImage = value;
                this.OnPropertyChanged("ProfileImage");
            }
        }

        private long userIdentity;
        public long UserIdentity
        {
            get { return userIdentity; }
            set
            {
                if (value == userIdentity) return;
                userIdentity = value;
                this.OnPropertyChanged("UserIdentity");
            }
        }
        private int callUiState = 1;
        public int CallUiState
        {
            get { return callUiState; }
            set { callUiState = value; OnPropertyChanged("CallUiState"); }
        }

        private string netWorkImage = CallImages.CALL_NET3;
        public string NetWorkImage
        {
            get { return netWorkImage; }
            set { netWorkImage = value; OnPropertyChanged("NetWorkImage"); }
        }

        private ImageSource myProfileImge = null;
        public ImageSource MyProfileImge
        {
            get { return myProfileImge; }
            set { myProfileImge = value; OnPropertyChanged("MyProfileImge"); }
        }

        private string loaderImageSource = CallImages.OUTGOING_ANIMATION;
        public string LoaderImageSource
        {
            get { return loaderImageSource; }
            set { loaderImageSource = value; OnPropertyChanged("LoaderImageSource"); }
        }

        private ImageSource friendProfileSource = null;
        public ImageSource FriendProfileSource
        {
            get { return friendProfileSource; }
            set { friendProfileSource = value; OnPropertyChanged("FriendProfileSource"); }
        }

        private ImageSource friendProfileSourceMini = null;
        public ImageSource FriendProfileSourceMini
        {
            get { return friendProfileSourceMini; }
            set { friendProfileSourceMini = value; OnPropertyChanged("FriendProfileSourceMini"); }
        }

        private ImageSource miniVideoImageSource = null;
        public ImageSource MiniVideoImageSource
        {
            get { return miniVideoImageSource; }
            set { miniVideoImageSource = value; OnPropertyChanged("MiniVideoImageSource"); }
        }

        private ImageSource fullVideoImageSource = null;
        public ImageSource FullVideoImageSource
        {
            get { return fullVideoImageSource; }
            set { fullVideoImageSource = value; OnPropertyChanged("FullVideoImageSource"); }
        }

        private string errorText;
        public string ErrorText
        {
            get { return errorText; }
            set { errorText = value; OnPropertyChanged("ErrorText"); }
        }

        private string duration = "00:00:00";
        public string Duration
        {
            get { return duration; }
            set { duration = value; OnPropertyChanged("Duration"); }
        }

        private string netWorkStrength = "Good";
        public string NetWorkStrength
        {
            get { return netWorkStrength; }
            set { netWorkStrength = value; OnPropertyChanged("NetWorkStrength"); }
        }

        private string volumnPercentag;
        public string VolumnPercentag
        {
            get { return volumnPercentag; }
            set { volumnPercentag = value; OnPropertyChanged("VolumnPercentag"); }
        }

        private int callState;
        public int CallState
        {
            get { return callState; }
            set
            {
                if (value == callState) return;
                callState = value;
                //CallStates.CurrentCallState = value;
                ChangeCallButtonsVisibility();
                OnPropertyChanged("CallState");
            }
        }

        private bool isInFullScreen = false;
        public bool IsInFullScreen
        {
            get { return isInFullScreen; }
            set
            {
                if (isInFullScreen == value) return;
                isInFullScreen = value;
                OnPropertyChanged("IsInFullScreen");
            }
        }

        private bool isVideoStopped = true;
        public bool IsVideoStopped
        {
            get { return isVideoStopped; }
            set { if (isVideoStopped == value) return; isVideoStopped = value; OnPropertyChanged("IsVideoStopped"); }
        }

        private bool isMuted = false;
        public bool IsMuted
        {
            get { return isMuted; }
            set
            {
                if (value == isMuted) return;
                isMuted = value;
                OnPropertyChanged("IsMuted");
                CallStates.IsMuted = value;
            }
        }

        private bool isVideoCall = false;
        public bool IsVideoCall
        {
            get { return isVideoCall; }
            set
            {
                if (value == isVideoCall) return;
                isVideoCall = value;
                OnPropertyChanged("IsVideoCall");
            }
        }

        private bool isOpenContextMenu = false;
        public bool IsOpenContextMenu
        {
            get { return isOpenContextMenu; }
            set
            {
                if (value == isOpenContextMenu) return;
                isOpenContextMenu = value;
                OnPropertyChanged("IsOpenContextMenu");
            }
        }

        private int numberOfVideo = 0;
        public int NumberOfVideo
        {
            get { return numberOfVideo; }
            set { numberOfVideo = value; OnPropertyChanged("NumberOfVideo"); }
        }

        private bool isAcceptedWithVideo = false;
        public bool IsAcceptedWithVideo
        {
            get { return isAcceptedWithVideo; }
            set { isAcceptedWithVideo = value; }
        }

        private bool isNeedToSendEndSignal = false;
        public bool IsNeedToSendEndSignal
        {
            get { return isNeedToSendEndSignal; }
            set { isNeedToSendEndSignal = value; }
        }

        private bool isNeedToToggleVideo = false;
        public bool IsNeedToToggleVideo
        {
            get { return isNeedToToggleVideo; }
            set { isNeedToToggleVideo = value; }
        }

        private string busyMessage = string.Empty;
        public string BusyMessage
        {
            get { return busyMessage; }
            set { busyMessage = value; }
        }

        private bool isNeedToShowMyVideo = false;
        public bool IsNeedToShowMyVideo
        {
            get { return isNeedToShowMyVideo; }
            set
            {
                isNeedToShowMyVideo = value;
                this.OnPropertyChanged("IsNeedToShowMyVideo");
            }
        }

        private bool isNeedToShowFriendVideo = false;
        public bool IsNeedToShowFriendVideo
        {
            get { return isNeedToShowFriendVideo; }
            set
            {
                isNeedToShowFriendVideo = value;
                this.OnPropertyChanged("IsNeedToShowFriendVideo");
            }
        }

        #endregion //"Property"

        #region "Uitily Methods"
        private void ChangeCallButtonsVisibility()
        {
            if (CallState > 0)
            {
                if (CallState == CallStates.UA_CALL_ACCEPTED)
                    CallUiState = CallStates.UA_ONCALL;
                else if (CallState == CallStates.UA_ONCALL || CallState == CallStates.HOLD_CALL)
                    CallUiState = CallStates.UA_ONCALL;
                else if (CallState == CallStates.UA_INCOMING_CALL)
                    CallUiState = CallStates.UA_INCOMING_CALL;
                else if (CallState == CallStates.UA_OUTGOING_CALL)
                    CallUiState = CallStates.UA_OUTGOING_CALL;
            }
        }
        #endregion "Utility Methods"
    }
}

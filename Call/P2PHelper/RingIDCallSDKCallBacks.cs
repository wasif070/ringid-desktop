﻿using System;
using System.Runtime.InteropServices;
using Call.dtos;
using Call.listeners;
using Call.utility;

namespace Call.P2PHelper
{
//    public class RingIDCallSDKCallBacks : OnReceiveListner
//    {
//        #region "Private Fields"

//        private const string path = ConfigFile.CALL_SDK;
//        private static RingIDCallSDKCallBacks _instance;
//        //private const int SDK_DATA_SIGNAL = 1;
//        //private const int SDK_DATA_AUDIO = 2;
//        //private const int SDK_DATA_VIDEO = 3;
//        //const int VIDEO_QUALITY_LOW = 202;
//        //const int VIDEO_QUALITY_HIGH = 204;
//        //const int VIDEO_SHOULD_STOP = 203;
//        private UiCommunicator uiCommunicator;

//        log4net.ILog log = log4net.LogManager.GetLogger(typeof(RingIDCallSDKCallBacks).Name);

//        #endregion

//        #region "Delegates"
//        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
//        private delegate void Delegate_ipv_SetNotifyClientMethodCallback(int eventType);
//        Delegate_ipv_SetNotifyClientMethodCallback Obj_ipv_SetNotifyClientMethodCallback;
//        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
//        private static extern void ipv_SetNotifyClientMethodCallback([MarshalAs(UnmanagedType.FunctionPtr)] Delegate_ipv_SetNotifyClientMethodCallback ii);
//        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
//        private delegate void Delegate_ipv_SetNotifyClientMethodForFriendCallback(int eventType, long friendName, int mediaName);
//        Delegate_ipv_SetNotifyClientMethodForFriendCallback Obj_ipv_SetNotifyClientMethodForFriendCallback;
//        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
//        private static extern void ipv_SetNotifyClientMethodForFriendCallback([MarshalAs(UnmanagedType.FunctionPtr)] Delegate_ipv_SetNotifyClientMethodForFriendCallback Obj_ipv_SetNotifyClientMethodForFriendCallback2);

//        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
//        private delegate void Delegate_ipv_SetNotifyClientMethodWithReceivedBytesCallback(int eventType, long friendName, int mediaName, int dataLength, IntPtr data);
//        Delegate_ipv_SetNotifyClientMethodWithReceivedBytesCallback Obj_ipv_SetNotifyClientMethodWithReceivedBytesCallback;
//        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
//        private static extern void ipv_SetNotifyClientMethodWithReceivedBytesCallback([MarshalAs(UnmanagedType.FunctionPtr)] Delegate_ipv_SetNotifyClientMethodWithReceivedBytesCallback Obj_ipv_SetNotifyClientMethodWithReceivedBytesCallback2);

//        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
//        private delegate void Delegate_SetNotifyClientWithVideoDataCallback(long CallID, IntPtr frame, int iLen, int iHeight, int iWidth, int iOrienttation);
//        Delegate_SetNotifyClientWithVideoDataCallback Obj_SetNotifyClientWithVideoDataCallback;
//        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
//        private static extern void SetNotifyClientWithVideoDataCallback([MarshalAs(UnmanagedType.FunctionPtr)] Delegate_SetNotifyClientWithVideoDataCallback ii);


//        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
//        private delegate void Delegate_SetNotifyClientWithAudioDataCallback(long CallID, IntPtr data, int in_size);
//        Delegate_SetNotifyClientWithAudioDataCallback Obj_SetNotifyClientWithAudioDataCallback;
//        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
//        private static extern void SetNotifyClientWithAudioDataCallback([MarshalAs(UnmanagedType.FunctionPtr)] Delegate_SetNotifyClientWithAudioDataCallback ii);

//        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
//        public delegate void Delegate_SetNotifyClientWithVideoNotificationCallback(long FriendId, int eventType);
//        Delegate_SetNotifyClientWithVideoNotificationCallback Obj_SetNotifyClientWithVideoNotificationCallback;
//        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
//        public static extern void SetNotifyClientWithVideoNotificationCallback([MarshalAs(UnmanagedType.FunctionPtr)] Delegate_SetNotifyClientWithVideoNotificationCallback ii);
//        #endregion "Delegates"

//        #region "Private Methods"

//        private void initDelegateMethods()
//        {
//            Obj_ipv_SetNotifyClientMethodCallback = new Delegate_ipv_SetNotifyClientMethodCallback(notifyClientMethod);
//            ipv_SetNotifyClientMethodCallback(Obj_ipv_SetNotifyClientMethodCallback);

//            Obj_ipv_SetNotifyClientMethodForFriendCallback = new Delegate_ipv_SetNotifyClientMethodForFriendCallback(notifyClientMethodForFriend);
//            ipv_SetNotifyClientMethodForFriendCallback(Obj_ipv_SetNotifyClientMethodForFriendCallback);

//            Obj_ipv_SetNotifyClientMethodWithReceivedBytesCallback = new Delegate_ipv_SetNotifyClientMethodWithReceivedBytesCallback(notifyClientMethodWithReceivedBytes);
//            ipv_SetNotifyClientMethodWithReceivedBytesCallback(Obj_ipv_SetNotifyClientMethodWithReceivedBytesCallback);

//            //new
//            Obj_SetNotifyClientWithVideoDataCallback = new Delegate_SetNotifyClientWithVideoDataCallback(FramesFromLibrary);
//            SetNotifyClientWithVideoDataCallback(Obj_SetNotifyClientWithVideoDataCallback);

//            Obj_SetNotifyClientWithAudioDataCallback = new Delegate_SetNotifyClientWithAudioDataCallback(AudioFromLibrary);
//            SetNotifyClientWithAudioDataCallback(Obj_SetNotifyClientWithAudioDataCallback);

//            Obj_SetNotifyClientWithVideoNotificationCallback = new Delegate_SetNotifyClientWithVideoNotificationCallback(VideoNotification);
//            SetNotifyClientWithVideoNotificationCallback(Obj_SetNotifyClientWithVideoNotificationCallback);
//        }

//        private void notifyClientMethod(int eventType)
//        {
//        }

//        private void notifyClientMethodForFriend(int eventType, long friendName, int mediaName)
//        {
//            if (uiCommunicator != null)
//            {
//                uiCommunicator.process_notifyClientMethodForFriend(eventType, friendName, mediaName);
//            }
//        }

//        private void notifyClientMethodWithReceivedBytes(int eventType, long friendName, int mediaName, int dataLength, IntPtr data2)
//        {
//            byte[] data = new byte[dataLength];
//            Marshal.Copy(data2, data, 0, dataLength);
//            //if (mediaName == ConfigFile.MediaType.IPV_MEDIA_AUDIO && eventType == ConfigFile.EventType.DATA_REVEIVED)
//            //{
//            //    OnVoiceSignalReceive(data, data.Length, SDK_DATA_SIGNAL);
//            //}
//            if (eventType == ConfigFile.EventType.DATA_RECEIVED)
//            {
//                OnVoiceSignalReceive(data, data.Length);
//            }
//        }

//        public long GetCurrentTimeStamp()
//        {
//            long milliseconds = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
//            return milliseconds;
//        }
//        /*
//         void notifyClientMethodWithVideoDataIos(LongLong lFriendID, unsigned char data[], int dataLenth, int iVideoHeight, int iVideoWidth, int iOrienttation)
//         */
//        private void FramesFromLibrary(long CallID, IntPtr frame, int iLen, int iHeight, int iWidth, int iOrienttation)
//        {
//            //  Console.WriteLine("length==>" + iLen + " width==>" + iWidth + "height==>" + iHeight);
//            byte[] dataTemp = new byte[iLen];
//            Marshal.Copy(frame, dataTemp, 0, iLen);
//            uiCommunicator.process_VIDEO_DATA(dataTemp, iLen, iHeight, iWidth, iOrienttation);
//        }

//        private void AudioFromLibrary(long CallID, IntPtr data, int Length)
//        {
//            int bytlength = Length * 2;
//            byte[] dataTemp = new byte[bytlength];
//            Marshal.Copy(data, dataTemp, 0, bytlength);
//            uiCommunicator.process_AUDIO_DATA(dataTemp);
//            //Marshal.Copy(data, managedArrayAudio, 0, 160);
//            //if (iTempAudio == 0)
//            //{
//            //    System.IO.File.WriteAllText("log3.txt", "Inside AudioFromLibrary, iLen = " + in_size + "\r\n");

//            //    System.IO.File.WriteAllBytes("incoming.pcm", managedArrayAudio);

//            //    iTempAudio++;
//            //}
//        }
//        // this methods will return video Qality
//        private void VideoNotification(long CallID, int eventType)
//        {
//            Console.WriteLine("***************eventType==>" + eventType);
//            if (uiCommunicator != null)
//                uiCommunicator.process_VideoNotification(CallID, eventType);
//        }
//        #endregion "Private Methods"

//        #region "Public Methods"

//        public static RingIDCallSDKCallBacks Instance
//        {
//            get
//            {
//                if (_instance == null)
//                {
//                    _instance = new RingIDCallSDKCallBacks();

//                }
//                return _instance;
//            }
//        }

//        public void LinkWithConnectivityLib(UiCommunicator uiCommunicator2)
//        {
//            _instance.initDelegateMethods();
//            this.uiCommunicator = uiCommunicator2;
//        }

//        #endregion "Public Methods"

//        #region OnReceiveListner Members

//        public void OnVoiceSignalReceive(byte[] receivedBuffer, int Length)
//        {
//            try
//            {
//                int packetType = receivedBuffer[0];
//                VoiceMessageDTO messageDTO = null;
//                switch (packetType)
//                {
//                    case VoiceConstants.VOICE_MEDIA: //0
//                        break;
//                    case VoiceConstants.VOICE_REGISTER_CONFIRMATION://3
//                        messageDTO = VoicePacketGenerator.getRegisterConfirmationPacket(receivedBuffer);
//                        if (messageDTO != null && uiCommunicator != null)
//                            uiCommunicator.process_VOICE_REGISTER_CONFIRMATION(messageDTO);
//                        break;
//                    case VoiceConstants.CALL_STATE.CALLING://5
//                        messageDTO = VoicePacketGenerator.getSignalingPacket(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_CALLING(messageDTO);
//                        break;
//                    case VoiceConstants.CALL_STATE.RINGING://6
//                        messageDTO = VoicePacketGenerator.getSignalingPacket(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_RINGING(messageDTO);
//                        break;
//                    case VoiceConstants.CALL_STATE.IN_CALL://7
//                        messageDTO = VoicePacketGenerator.getIncallPacket(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_IN_CALL(messageDTO);
//                        break;
//                    case VoiceConstants.CALL_STATE.ANSWER://8
//                        messageDTO = VoicePacketGenerator.getSignalingPacketAnwser(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_ANSWER(messageDTO);
//                        break;
//                    case VoiceConstants.CALL_STATE.BUSY://9
//                        messageDTO = VoicePacketGenerator.getSignalingPacket(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_BUSY(messageDTO);
//                        break;
//                    case VoiceConstants.CALL_STATE.CANCELED://10
//                        messageDTO = VoicePacketGenerator.getSignalingPacket(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_CANCELED(messageDTO);
//                        break;
//                    case VoiceConstants.CALL_STATE.CONNECTED://11
//                        messageDTO = VoicePacketGenerator.getSignalingPacket(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_CONNECTED(messageDTO);
//                        break;

//                    case VoiceConstants.CALL_STATE.DISCONNECTED://12
//                        messageDTO = VoicePacketGenerator.getSignalingPacket(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_DISCONNECTED(messageDTO);
//                        break;
//                    case VoiceConstants.CALL_STATE.BYE://13
//                        messageDTO = VoicePacketGenerator.getSignalingPacket(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_BYE(messageDTO);
//                        break;
//                    case VoiceConstants.CALL_STATE.NO_ANSWER://15
//                        messageDTO = VoicePacketGenerator.getSignalingPacket(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_NO_ANSWER(messageDTO);
//                        break;
//                    case VoiceConstants.CALL_STATE.IN_CALL_CONFIRMATION://18
//                        messageDTO = VoicePacketGenerator.getIncallPacket(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_IN_CALL_CONFIRMATION(messageDTO);
//                        break;
//                    case VoiceConstants.CALL_STATE.VOICE_REGISTER_PUSH://20
//                        messageDTO = VoicePacketGenerator.getPushConfirmationPacket(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_VOICE_REGISTER_PUSH(messageDTO);
//                        break;
//                    case VoiceConstants.CALL_STATE.VOICE_REGISTER_PUSH_CONFIRMATION://21
//                        messageDTO = VoicePacketGenerator.getPushConfirmationPacket(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_VOICE_REGISTER_PUSH_CONFIRMATION(messageDTO);
//                        break;
//                    case VoiceConstants.CALL_STATE.VOICE_CALL_HOLD://22
//                        messageDTO = VoicePacketGenerator.getSignalingPacket(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_VOICE_CALL_HOLD(messageDTO);
//                        break;
//                    case VoiceConstants.CALL_STATE.VOICE_CALL_HOLD_CONFIRMATION://23
//                        messageDTO = VoicePacketGenerator.getSignalingPacket(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_VOICE_CALL_HOLD_CONFIRMATION(messageDTO);
//                        break;
//                    case VoiceConstants.CALL_STATE.VOICE_CALL_UNHOLD://24
//                        messageDTO = VoicePacketGenerator.getSignalingPacket(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_VOICE_CALL_UNHOLD(messageDTO);
//                        break;
//                    case VoiceConstants.CALL_STATE.VOICE_UNHOLD_CONFIRMATION://25
//                        messageDTO = VoicePacketGenerator.getSignalingPacket(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_VOICE_UNHOLD_CONFIRMATION(messageDTO);
//                        break;
//                    case VoiceConstants.CALL_STATE.VOICE_BUSY_MESSAGE://26
//                        messageDTO = VoicePacketGenerator.getVoiceBusyMsg(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_VOICE_BUSY_MESSAGE(messageDTO);
//                        break;
//                    case VoiceConstants.CALL_STATE.VOICE_BUSY_MESSAGE_CONFIRMATION://27
//                        messageDTO = VoicePacketGenerator.getSignalingPacket(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_VOICE_BUSY_MESSAGE_CONFIRMATION(messageDTO);
//                        break;

//                    case VoiceConstants.VIDEO_CALL.BINDING_PORT_CONFIRMATION://41
//                        messageDTO = VoicePacketGenerator.getPortFormSignal(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_VIDEO_BINDING_PORT_CONFIRMATION(messageDTO);
//                        break;
//                    case VoiceConstants.VIDEO_CALL.START://42
//                        messageDTO = VoicePacketGenerator.getSignalingPacket(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_VIDEO_START(messageDTO);
//                        break;
//                    case VoiceConstants.VIDEO_CALL.START_CONFIRMATION://43
//                        messageDTO = VoicePacketGenerator.getSignalingPacket(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_VIDEO_START_CONFIRMATION(messageDTO);
//                        break;
//                    case VoiceConstants.VIDEO_CALL.END://45
//                        messageDTO = VoicePacketGenerator.getSignalingPacketVideoEnd(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_VIDEO_END(messageDTO);
//                        break;
//                    case VoiceConstants.VIDEO_CALL.END_CONFIRMATION://46
//                        messageDTO = VoicePacketGenerator.getSignalingPacket(receivedBuffer);
//                        if (messageDTO != null)
//                            uiCommunicator.process_VIDEO_END_CONFIRMATION(messageDTO);
//                        break;
//                }
//#if CALL_LOG
//                if (messageDTO != null && messageDTO.PacketType > 0 && messageDTO.PacketID != null)
//                {
//                    System.Diagnostics.Debug.WriteLine(messageDTO);
//                }
//#endif
//            }
//            catch (Exception ex)
//            {
//                log.Error(ex.StackTrace);
//            }
//        }

//        #endregion
//    }
}

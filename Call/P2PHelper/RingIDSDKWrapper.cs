﻿using System;
using System.Runtime.InteropServices;
using Call.utility;

namespace Call.P2PHelper
{
    //    public class RingIDSDKWrapper
    //    {
    //        #region "Private fields"
    //        const string path = ConfigFile.CALL_SDK;
    //        private static log4net.ILog log = log4net.LogManager.GetLogger(typeof(RingIDSDKWrapper).Name);
    //        #endregion

    //        #region "Delegates"

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern int InitializeLibrary(long username);

    //        //[DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        //public static extern int ipv_InitializeLibrary(long username, string loc, int level);

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern int ipv_IsLoadRingIDSDK();

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern int ipv_SetAuthenticationServer(string cAuthServerIP, int iAuthServerPort, string cAppSessionId);

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern void ipv_SetLogFileProperty(string loc, int logLevel, bool bCreate);

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern SessionStatus ipv_CreateSession(long CallID, int mediaType, string cRelayServerIP, int iRelayServerPort);

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern void ipv_SetRelayServerInformation(long CallID, int mediaType, string cRelayServerIP, int iRelayServerPort);

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern void ipv_StartP2PCall(long CallID, int mediaType, int bCaller);

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern bool ipv_IsConnectionTypeHostToHost(long CallID, int mediaType);

    //        //[DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        //private static extern int ipv_Send(long CallID, int mediaType, byte[] data, int iLen);
    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern int ipv_SendTo(long CallID, int mediaType, byte[] data, int iLen, string cDestinationIP, int iDestinationPort);

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern int ipv_Recv(long CallID, int mediaType, byte[] data, int iLen);

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern string ipv_GetSelectedIPAddress(long CallID, int mediaType);

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern int ipv_GetSelectedPort(long CallID, int mediaType);

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern int ipv_CloseSession(long CallID, int mediaType);

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern void ipv_Release();

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern void ipv_InterfaceChanged();

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern void ipv_SetLogFileLocation(string loc);

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern void ipv_SetTimeOutForSocket(int time_in_sec);

    //        //new for video
    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern int StartVideoCall(long CallID, int iHeight, int iWidth);

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern int StartAudioCall(long CallID);

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern int SetLoggingState(bool loggingState, int logLevel);

    //        /*
    //         * in Lib
    //         int SendVideoData(const LongLong& lFriendID, unsigned char *in_data, unsigned int in_size, unsigned int orientation_type=0, int device_orientation=0)
    //         */
    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern int SendVideoData(long CallID, byte[] in_data, int in_size, int orientation_type = 0, int device_orientation = 0);
    //        //     public static extern int SendVideoData(long CallID, byte[] in_data, int in_size, int orientation_type);

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern int SendAudioData(long CallID, short[] in_data, int in_size);

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern int StopVideoCall(long CallID);

    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern int StopAudioCall(long CallID);

    //        //29-05-2016
    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern int CheckDeviceCapability(long CallID, int iHeightHigh, int iWidthHigh, int iHeightLow, int iWidthLow);
    //        //public static extern int CheckDeviceCapability(long CallID, int iHeightHigh, int iWidthHigh);

    //        //20-07-2016
    //        [DllImport(path, CallingConvention = CallingConvention.Cdecl)]
    //        public static extern int SetDeviceCapabilityResults(int iNotification, int iHeightHigh, int iWidthHigh, int iHeightLow, int iWidthLow);
    //        #endregion "Delegates"

    //        #region "Private Methods"
    //        #endregion

    //        #region "Public Methods"
    //        public static long GetLong(string CallID)
    //        {
    //            return Convert.ToInt64(Convert.ToDecimal(CallID));
    //        }
    //        public static int SendPCMDATA(byte[] rowData, long CallID)
    //        {
    //            short[] sdata = new short[(int)(rowData.Length / 2)];
    //            Buffer.BlockCopy(rowData, 0, sdata, 0, rowData.Length);
    //            return SendAudioData(CallID, sdata, sdata.Length);
    //        }

    //        public static int SendAudioSingalingDataGram(byte[] datagram, long CallID, string voiceServerIP, int port)
    //        {
    //            if (port > 0)
    //            {
    //                int sentData = ipv_SendTo(CallID, ConfigFile.MediaType.IPV_MEDIA_AUDIO, datagram, datagram.Length, voiceServerIP, port);
    //#if DEBUG
    //                log.Debug("*********Sent Audio Signaling type==>" + datagram[0] + " voiceServerIP==>" + voiceServerIP + "  port==>" + port + "  returned==>" + sentData);
    //#endif
    //                return sentData;
    //            }
    //            return -1;
    //        }

    //        public static int SendVideoSingalingDataGram(byte[] datagram, long CallID, string voiceServerIP, int port)
    //        {
    //            if (port > 0)
    //            {
    //                int sentData = ipv_SendTo(CallID, ConfigFile.MediaType.IPV_MEDIA_VIDEO, datagram, datagram.Length, voiceServerIP, port);
    //#if DEBUG
    //                log.Debug("*********Sending Video signal==>" + datagram[0] + " voiceServerIP==>" + voiceServerIP + "  port==>" + port + "  returned==>" + sentData);
    //#endif
    //                return sentData;
    //            }
    //            return -1;
    //        }
    //        #endregion "Public Methods"
    //    }
}
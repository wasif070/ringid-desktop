﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Call.Utility
{
    internal class RelayCommand : ICommand
    {

        private readonly Action<object> _excecute;
        private readonly Predicate<object> _canExecute;
        private EventHandler eCanExecuteChanged;

        public RelayCommand(Action<object> _excecute)
            : this(_excecute, null)
        {
        }

        public RelayCommand(Action<object> _excecute, Predicate<object> _canExecute)
        {
            if (_excecute == null)
            {
                throw new Exception("Execute command can't be NULL.");
            }

            this._excecute = _excecute;
            this._canExecute = _canExecute;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                eCanExecuteChanged += value;
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                eCanExecuteChanged -= value;
                CommandManager.RequerySuggested -= value;
            }
        }


        public bool CanExecute(object parameter)
        {
            return _canExecute == null ? true : _canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            _excecute(parameter);
        }

        public void RaiseCanExecuteChanged()
        {
            if (_canExecute != null) OnCanExecuteChanged();
        }

        protected virtual void OnCanExecuteChanged()
        {
            if (eCanExecuteChanged != null)
            {
                Application.Current.Dispatcher.BeginInvoke((Action)(() =>
                {
                    eCanExecuteChanged(this, EventArgs.Empty);
                }));
            }
        }
    }
}

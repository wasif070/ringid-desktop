﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Call.Utility
{
    class AudioFiles
    {
        public static string AudioFolderLocation = string.Empty;
        public static readonly string CLIP_ON = AudioFolderLocation + "on.wav";
        public static readonly string CLIP_OFF = AudioFolderLocation + "off.wav";
        public static readonly string CLIP_CALL_PROGRESS = AudioFolderLocation + "progress.wav";
        public static readonly string CLIP_RINGING = AudioFolderLocation + "ringing.wav";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Call.Utility
{
    public class CallImages
    {
        private static string BASE_IMAGES = "/Resources/Images/";
        public static string OUTGOING_ANIMATION { get { return BASE_IMAGES + "outgoingCallAnim.gif"; } }//outgoingCallAnim.gif/calling.gif
        public static string MESSAGE { get { return BASE_IMAGES + "messege.png"; } }
        public static string MESSAGE_H { get { return BASE_IMAGES + "messege_h.png"; } }

        public static string CHAT_WITH_CALLING_FRIEND { get { return BASE_IMAGES + "chat.png"; } }
        public static string CHAT_WITH_CALLING_FRIEND_H { get { return BASE_IMAGES + "chat_h.png"; } }
        public static string ANSWER_WITH_VOICE_ONLY { get { return BASE_IMAGES + "answer_with_voice_Only.png"; } }
        public static string ANSWER_WITH_VOICE_ONLY_H { get { return BASE_IMAGES + "answer_with_voice_Only_h.png"; } }

        public static string IGNORE_CALL { get { return BASE_IMAGES + "ignore.png"; } }
        public static string IGNORE_CALL_H { get { return BASE_IMAGES + "ignore_h.png"; } }
        public static string INCOMING_CALL_ANIMATION { get { return BASE_IMAGES + "incoming_call_animation.gif"; } }
        public static string CALL_ACCEPT { get { return BASE_IMAGES + "receive.png"; } }
        public static string CALL_ACCEPT_H { get { return BASE_IMAGES + "receive_h.png"; } }
        public static string CALL_HOLD { get { return BASE_IMAGES + "hold.png"; } }
        public static string CALL_HOLD_H { get { return BASE_IMAGES + "hold_h.png"; } }
        public static string CALL_UNHOLD { get { return BASE_IMAGES + "unhold.png"; } }
        public static string CALL_UNHOLD_H { get { return BASE_IMAGES + "unhold_h.png"; } }
        public static string CALL_MUTE { get { return BASE_IMAGES + "mute.png"; } }
        public static string CALL_MUTE_H { get { return BASE_IMAGES + "mute_h.png"; } }
        public static string CALL_UMMUTE { get { return BASE_IMAGES + "unmute.png"; } }
        public static string CALL_UMMUTE_H { get { return BASE_IMAGES + "unmute_h.png"; } }
        public static string BUTTON_VOLUME { get { return BASE_IMAGES + "speker.png"; } }
        public static string BUTTON_VOLUME_H { get { return BASE_IMAGES + "speker_h.png"; } }

        public static string BUTTON_VIDEO { get { return BASE_IMAGES + "video.png"; } }
        public static string BUTTON_VIDEO_H { get { return BASE_IMAGES + "video_h.png"; } }

        public static string BUTTON_STOP_VIDEO { get { return BASE_IMAGES + "video_off.png"; } }
        public static string BUTTON_STOP_VIDEO_H { get { return BASE_IMAGES + "video_off_h.png"; } }
        public static string CALL_END { get { return BASE_IMAGES + "end_call.png"; } }
        public static string CALL_END_H { get { return BASE_IMAGES + "end_call_h.png"; } }
        public static string CALL_TOP_BAR_INCOMING { get { return BASE_IMAGES + "call_top_bar_incoming.png"; } }
        public static string CALL_TOP_BAR_OUTGOING { get { return BASE_IMAGES + "call_top_bar_outgoing.png"; } }

        public static string CALL_NET0 { get { return BASE_IMAGES + "net0.png"; } }
        public static string CALL_NET1 { get { return BASE_IMAGES + "net1.png"; } }
        public static string CALL_NET2 { get { return BASE_IMAGES + "net2.png"; } }
        public static string CALL_NET3 { get { return BASE_IMAGES + "net3.png"; } }
        public static string CALL_NET4 { get { return BASE_IMAGES + "net4.png"; } }


        public static string ACCEPT_CALL_MINI { get { return BASE_IMAGES + "accept_call_mini.png"; } }
        public static string ACCEPT_CALL_MINI_H { get { return BASE_IMAGES + "accept_call_mini_h.png"; } }
        public static string BUSY_MSG_MINI { get { return BASE_IMAGES + "busy_msg_mini.png"; } }
        public static string BUSY_MSG_MINI_H { get { return BASE_IMAGES + "busy_msg_mini_h.png"; } }
        public static string END_CALL_MINI { get { return BASE_IMAGES + "end_call_mini.png"; } }
        public static string END_CALL_MINI_H { get { return BASE_IMAGES + "end_call_mini_h.png"; } }
        public static string REJECT_END_CALL_MINI { get { return BASE_IMAGES + "reject_end_call_mini.png"; } }
        public static string REJECT_END_CALL_MINI_H { get { return BASE_IMAGES + "reject_end_call_mini_h.png"; } }
        public static string BUTTON_CLOSE_BOLD { get { return BASE_IMAGES + "close_bold.png"; } }
        public static string BUTTON_CLOSE_BOLD_WHITE { get { return BASE_IMAGES + "close_bold_white.png"; } }
        public static string BUTTON_CLOSE_BOLD_H { get { return BASE_IMAGES + "close_bold_h.png"; } }
    }
}

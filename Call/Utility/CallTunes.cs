﻿using System;
using System.IO;
using System.Media;
using log4net;
using NAudio.Wave;

namespace Call.Utility
{
    public class CallTunes
    {
        #region "Private Fields"
        private static readonly ILog log = LogManager.GetLogger(typeof(CallTunes).Name);
        private WaveOut waveOut;
        private WaveFileReader reader;
        private SoundPlayer ClipOffTune;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        #endregion "Constructors"

        #region "Private methods"
        private byte[] ReadAllBytes(string fileName)
        {
            byte[] buffer = null;
            using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                buffer = new byte[fs.Length];
                fs.Read(buffer, 0, (int)fs.Length);
            }
            return buffer;
        }
        #endregion "Private methods"

        #region "Public Methods"

        public void PlayProgressTune()
        {
            try
            {
                reader = new WaveFileReader(AudioFiles.CLIP_CALL_PROGRESS);
                LoopStream loop = new LoopStream(reader);
                waveOut = new WaveOut();
                waveOut.Stop();
                waveOut.Init(loop);
                waveOut.Play();
            }
            catch (Exception)
            {
            }
        }

        public void PlayRingTune()
        {
            try
            {
                reader = new WaveFileReader(AudioFiles.CLIP_RINGING);
                LoopStream loop = new LoopStream(reader);
                waveOut = new WaveOut();
                waveOut.Stop();
                waveOut.Init(loop);
                waveOut.Play();
            }
            catch (Exception ex)
            {
                log.Error("PlayRingtoneOrProgress==>" + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public void PlayCallEndTune()
        {
            if (ClipOffTune == null) ClipOffTune = new SoundPlayer(new MemoryStream(ReadAllBytes(AudioFiles.CLIP_OFF), false));
            ClipOffTune.Play();
        }

        public void StopTune()
        {
            if (waveOut != null)
            {
                waveOut.Stop();
                waveOut.Dispose();
                waveOut = null;
            }
            if (reader != null)
                reader.Flush();
        }
        #endregion "Public methods"

    }
}

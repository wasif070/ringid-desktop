﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using Call.Models;
using Call.Utility.Services;
using Call.ViewModels;
using Call.Views;
using callsdkwrapper;
using log4net;

namespace Call.Utility
{
    public class CallHelperMethods
    {
        #region "Private Fields"
        private static readonly ILog logger = LogManager.GetLogger(typeof(CallHelperMethods).Name);

        private static UCCallUIInMain callUIInMain;
        //  private static Call.WNCallInSeparateView callInSeparateView;
        private static CallTunes callTunes;
        private static PCMDataPlayerRecorder pcmDataPlayerRecorder;
        private static WebcamRGBData webcamRGBData;
        public static UserControl UserControlToAddCallView = null;
        public static VMCall CallViewModel { get; set; }
        #endregion "Private Fields"

        #region "Private Methods"

        private static void startDuration()
        {
            //MainSwitcher.CallController.StartACall();
        }

        private static void addErrorLog(string msg)
        {
            if (logger.IsErrorEnabled)
            {
                logger.Error(msg);
            }
        }

        #endregion "Private Methods"

        #region "Utility Methods"

        public static void InitCallSDK()
        {
            //try
            //{
            //    if (MainSwitcher.CallController == null)
            //    {
            //        MainSwitcher.CallController = new CallController();
            //    }
            //    MainSwitcher.CallController.InitCallSDK();
            //    System.Threading.Thread.Sleep(3000);
            //}
            //catch (Exception ex)
            //{
            //    logger.Error("**********************Failded To connect CallSDK ***********************" + ex.Message + "\n" + ex.StackTrace);
            //}
        }

        public static void StartAudioPlayerAndRecorder(bool callEndIfAudioDeviceProblem)
        {
            //MainSwitcher.CallController.StartAudioPlayerAndRecorder(callEndIfAudioDeviceProblem);
        }

        public static void StopPlayerAndRecorder()
        {
            //MainSwitcher.CallController.StopPlayerAndRecorder();
        }

        public static void PlayClipOffSound()
        {
            //AudioFilesAndSettings.Play(AudioFilesAndSettings.CLIP_OFF_TUNE);
        }

        public static void ProcessConnected()
        {
            Application.Current.Dispatcher.Invoke((Action)delegate
            {
                StartAudioPlayerAndRecorder(true);
            });
            startDuration();
        }
        public static void InitOnCall()
        {
            //CallViewModel.CallState = CallStates.UA_ONCALL;
            //CallStates.CurrentCallState = CallStates.UA_ONCALL;
            //stopCallTune();
            //RunAudioPlayerRecorder();
            //if (CallViewModel.IsNeedToShowMyVideo)
            //    StartWebcam();
        }

        public static IList<string> GetInstanceMessages()
        {
            IList<string> strings = null;
            return strings;
        }

        public static int GetSavedRecoderDevice()
        {
            return 0;
        }

        public static int GetSavedPlayerDevice()
        {
            return 0;
        }

        public static void SetSavedRecoderDevice(int index)
        {

        }

        public static void SetSavedPlayerDevice(int index)
        {

        }


        [DllImport("gdi32.dll")]
        private static extern bool DeleteObject(IntPtr hObject);
        #endregion "Utility Methods"

        #region "Modify UI"

        public static float GetAngale(int orientation)
        {
            float angle = 0;
            switch (orientation)
            {
                case 1:
                    angle = -90;
                    break;
                case 2:
                    angle = 180;
                    break;
                case 3:
                    angle = 90;
                    break;
            }
            return angle;
        }

        public static Bitmap RotateImageByAngle(System.Drawing.Image oldBitmap, float angle)
        {
            var newBitmap = new Bitmap(oldBitmap.Width, oldBitmap.Height);
            newBitmap.SetResolution(oldBitmap.HorizontalResolution, oldBitmap.VerticalResolution);
            var graphics = Graphics.FromImage(newBitmap);
            graphics.TranslateTransform((float)oldBitmap.Width / 2, (float)oldBitmap.Height / 2);
            graphics.RotateTransform(angle);
            graphics.TranslateTransform(-(float)oldBitmap.Width / 2, -(float)oldBitmap.Height / 2);
            graphics.DrawImage(oldBitmap, new System.Drawing.Point(0, 0));
            return newBitmap;
        }

        public static void ChangeNetorkStrength(NetworkStrength strength)
        {
        }

        public static void ShowCallUiInMainView(CallerDTO dto, CallMediaType callType)
        {
            if (CallStates.CurrentCallState == CallStates.UA_IDLE)
            {
                CallViewModel.CallState = CallStates.UA_OUTGOING_CALL;
                CallStates.CurrentCallState = CallStates.UA_OUTGOING_CALL;
                CallStatusService callStatusService = new CallStatusService();
                callStatusService.OnNeedToCloseUI += (needToSwitchSeparateWindow) =>
                {
                    closeCallUI();
                };
                callStatusService.StartThread();
                if (callTunes == null) callTunes = new CallTunes();
                callTunes.PlayProgressTune();
            }
            if (callUIInMain == null)
            {
                callUIInMain = new UCCallUIInMain();
                callUIInMain.OnRemovedFromUI += (isNeedToShowSeparateWindow) =>
                {
                    try
                    {
                        if (CallStates.CurrentCallState == CallStates.UA_IDLE)
                        {
                            CallStates.CurrentCallState = CallStates.UA_IDLE;
                            if (callTunes != null) callTunes.StopTune();
                            //if (callInSeparateView != null)
                            //{
                            //    callInSeparateView.Close();
                            //}
                            callUIInMain = null;
                            if (CallViewModel != null)
                                CallViewModel.IsInFullScreen = false;
                            //if (MiddlePanelSwitcher.pageSwitcher.Content == null)
                            //{
                            //    if (CallViewModel != null && CallViewModel.CallProfileModel.UserTableID > 0)
                            //        View.ViewModel.RingIDViewModel.Instance.OnFriendCallChatButtonClicked(CallViewModel.UserBasicInfoModel.ShortInfoModel.UserTableID);
                            //    else
                            //        RingIDViewModel.Instance.OnAllFeedsClicked(new object());
                            //}
                        }
                        else if (isNeedToShowSeparateWindow)
                        {
                            ShowCallSeparateWindow(dto, callType);
                            callUIInMain = null;
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.StackTrace);
                    }
                };
            }
            callUIInMain.AddInMiddlePanel();
        }


        public static void ShowCallSeparateWindow(CallerDTO dto, CallMediaType callType)
        {
            if (CallStates.CurrentCallState == CallStates.UA_IDLE)
            {
                if (callType == CallMediaType.Video) CallViewModel.IsNeedToShowMyVideo = true;
                CallViewModel.CallState = CallStates.UA_INCOMING_CALL;
                CallStates.CurrentCallState = CallStates.UA_INCOMING_CALL;
                CallStatusService callStatusService = new CallStatusService();
                callStatusService.OnNeedToCloseUI += (needToSwitchSeparateWindow) =>
                {
                    closeCallUI();
                };
                callStatusService.StartThread();
                if (callTunes == null) callTunes = new CallTunes();
                callTunes.PlayRingTune();
            }
            //if (callInSeparateView == null) callInSeparateView = new UI.Call.WNCallInSeparateView();
            //callInSeparateView.Show();
            //callInSeparateView.OnClosedWindow += (isCallEnded) =>
            //{
            //    if (CallStates.CurrentCallState == CallStates.UA_IDLE)
            //    {
            //        if (callUIInMain != null)
            //            callUIInMain.OnEndCallCommand();
            //        if (callTunes != null) callTunes.StopTune();
            //    }
            //    else
            //    {
            //        ShowCallUiInMainView(dto, callType);
            //    }
            //    callInSeparateView = null;
            //};
        }

        private static void closeCallUI()
        {
            try
            {
                if (callTunes != null)
                    callTunes.StopTune();
                if (callUIInMain != null)
                {
                    callUIInMain.OnEndCallCommand();
                    callUIInMain = null;
                }
                //if (callInSeparateView != null)
                //{
                //    callInSeparateView.CloseThis();
                //    callInSeparateView = null;
                //}
                StopAudioPlayerRecorder();
            }
            finally
            {
                CallStates.CurrentCallState = CallStates.UA_IDLE;
            }
        }

        private static void showMyRGBDataInView(byte[] array)
        {
            try
            {
                int PixelSize = 3;
                if (array == null) return;
                int stride = Contstants.FrameSettings.FRAME_WIDTH * PixelSize;
                GCHandle handle = GCHandle.Alloc(array, GCHandleType.Pinned);
                int scan0 = (int)handle.AddrOfPinnedObject();
                scan0 += (Contstants.FrameSettings.FRAME_HEIGHT - 1) * stride;
                Bitmap b = new Bitmap(Contstants.FrameSettings.FRAME_WIDTH, Contstants.FrameSettings.FRAME_HEIGHT, -stride, System.Drawing.Imaging.PixelFormat.Format24bppRgb, (IntPtr)scan0);
                handle.Free();
                IntPtr hBitmap = b.GetHbitmap();
                try
                {
                    System.Windows.Application.Current.Dispatcher.Invoke((Action)delegate
                   {
                       BitmapSource retval = Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                       CallViewModel.MiniVideoImageSource = retval;
                   }, System.Windows.Threading.DispatcherPriority.Render);
                }
                finally
                {
                    DeleteObject(hBitmap);
                    b.Dispose();
                }
            }
            finally { }
        }

        #endregion "Modify UI"

        #region "Audio/Vedio"
        private static void stopCallTune()
        {
            if (callTunes != null)
                callTunes.StopTune();
        }

        public static void RunAudioPlayerRecorder()
        {

            if (pcmDataPlayerRecorder == null) pcmDataPlayerRecorder = new PCMDataPlayerRecorder();
            pcmDataPlayerRecorder.StartPlayerRecorder();
            pcmDataPlayerRecorder.OnPCMdataAvailable += (dataBytes) =>
            {
                pcmDataPlayerRecorder.AddVoiceSampleByteToPlay(dataBytes);
            };
        }

        public static void StopAudioPlayerRecorder()
        {
            if (pcmDataPlayerRecorder != null) pcmDataPlayerRecorder.StopPlayerRecorder();
        }

        public static void StartWebcam()
        {
            if (webcamRGBData == null)
            {
                webcamRGBData = new WebcamRGBData();
                webcamRGBData.OnRGBDataFound += (rgbDataBytes) =>
                {
                    showMyRGBDataInView(rgbDataBytes);
                };
            }
            webcamRGBData.StartWebCam();
        }

        public static void StopWebcam()
        {
            if (webcamRGBData != null)
                webcamRGBData.StopWebCam();
        }

        public static void RestartPlayerRecorder()
        {
            if (webcamRGBData != null) webcamRGBData.RestartWebCam();
        }

        #endregion"Audio/Video"

        #region "CallSDK Helpers"

        public static void unRegisterEventHandler()
        {
            //MainSwitcher.CallController.BaseCallService.unRegisterEventHandler();
        }

        //public static BaseApiStatus Register(CallUserType callUserType, long friendId, string voiceChatServerAddress, int voiceChatServerPort, string fullName, int deviceType, int onlineStatus, int mood, bool idc, bool success, int appType, string friendDeviceToken, string callId, long callTime, bool isTransferredFromAuth, long transferredFriendIdFromAuth, CallMediaType callType, int isP2PEnable, int isVoipPush, bool isGsmCallActive)
        //{
        //    return MainSwitcher.CallController.BaseCallService.Register(callUserType, friendId, voiceChatServerAddress, voiceChatServerPort, fullName, deviceType, onlineStatus, mood, idc, success, appType, friendDeviceToken, callId, callTime, isTransferredFromAuth, transferredFriendIdFromAuth, callType, isP2PEnable, isVoipPush, isGsmCallActive);
        //}

        //public static BaseApiStatus Answer(long friendId, CallMediaType callType)
        //{
        //    return MainSwitcher.CallController.BaseCallService.Answer(friendId, callType);
        //}

        //public static BaseApiStatus Busy(long friendId)
        //{
        //    return MainSwitcher.CallController.BaseCallService.Busy(friendId);
        //}

        //public static BaseApiStatus BusyWithMessage(long friendId, string message, string packetID)
        //{
        //    return MainSwitcher.CallController.BaseCallService.BusyWithMessage(friendId, message, packetID);
        //}

        //public static BaseApiStatus Cancel(long friendId)
        //{
        //    return MainSwitcher.CallController.BaseCallService.Cancel(friendId);
        //}

        //public static BaseApiStatus Bye(long friendId)
        //{
        //    return MainSwitcher.CallController.BaseCallService.Bye(friendId);
        //}

        //public static BaseApiStatus Hold(long friendId)
        //{
        //    return MainSwitcher.CallController.BaseCallService.Hold(friendId);
        //}

        //public BaseApiStatus Unhold(long friendId)
        //{
        //    return MainSwitcher.CallController.BaseCallService.Unhold(friendId);
        //}

        //public static BaseApiStatus VideoStart(long friendId)
        //{
        //    return MainSwitcher.CallController.BaseCallService.VideoStart(friendId);
        //}

        //public static BaseApiStatus VideoEnd(long friendId)
        //{
        //    return MainSwitcher.CallController.BaseCallService.VideoEnd(friendId);
        //}

        //public static BaseApiStatus VideoSenderSideEnd(long friendId)
        //{
        //    return MainSwitcher.CallController.BaseCallService.VideoSenderSideEnd(friendId);
        //}

        public static void VoiceChatReset()
        {
            //MainSwitcher.CallController.BaseCallService.VoiceChatReset();
        }

        public static void ReinitializeVoiceChat(long userId)
        {
            //  MainSwitcher.CallController.BaseCallService.ReinitializeVoiceChat(userId);
        }

        public static void SetVideoResolution(int jvideoHeight, int jvideoWidth)
        {
            //  MainSwitcher.CallController.BaseCallService.setVideoResolution(jvideoHeight, jvideoWidth);
        }

        //public static string Version()
        //{
        //    return MainSwitcher.CallController.BaseCallService.Version();
        //}

        //public static int CheckDeviceCapability(long randomID, int iHeightHigh, int iWidthHigh, int iHeightLow, int iWidthLow)
        //{
        //    return MainSwitcher.CallController.BaseCallService.CheckDeviceCapability(randomID, iHeightHigh, iWidthHigh, iHeightLow, iWidthLow);
        //}

        //public static int SetDeviceCapabilityResults(int iNotification, int iHeightHigh, int iWidthHigh, int iHeightLow, int iWidthLow)
        //{
        //    return MainSwitcher.CallController.BaseCallService.SetDeviceCapabilityResults(iNotification, iHeightHigh, iWidthHigh, iHeightLow, iWidthLow);
        //}

        //public static void SendPCMData(byte[] rowData)
        //{
        //    MainSwitcher.CallController.SendPCMData(rowData);
        //}

        //public static void ProcessMyRGBData(byte[] rgbbyte)
        //{
        //    MainSwitcher.CallController.ProcessMyRGBData(rgbbyte);
        //}

        /// <summary>
        /// add for new methods for encode and decode audio data 31-08-2016
        /// int StartAudioEncodeDecodeSession();

        ///array<short> ^EncodeAudioFrame(array<short> ^pcmData);

        ///array<short> ^DecodeAudioFrame(array<short> ^encodedAudioData);

        ///int StopAudioEncodeDecodeSession();
        /// </summary>

        public static void StartAudioEncodeDecodeSession()
        {
            //MainSwitcher.CallController.BaseCallService.StartAudioEncodeDecodeSession();
        }

        //public static byte[] EncodeAudioFrame(byte[] pcmData)
        //{
        //    return MainSwitcher.CallController.BaseCallService.EncodeAudioFrame(pcmData);
        //}

        //public static byte[] DecodeAudioFrame(byte[] encodedAudioData)
        //{
        //    return MainSwitcher.CallController.BaseCallService.DecodeAudioFrame(encodedAudioData);
        //}

        public static void StopAudioEncodeDecodeSession()
        {
            //MainSwitcher.CallController.BaseCallService.StopAudioEncodeDecodeSession();
        }

        #endregion //"CallSDK Helpers"
    }
}

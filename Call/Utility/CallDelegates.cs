﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Call.Utility
{
    public class CallDelegates
    {
        public delegate void DelegateBool(bool isSuccess);
        public delegate void DelegateByte(byte[] myBytes);
        public delegate void DelegateNotify();
        public delegate void DelegateString(string string1);
    }
}

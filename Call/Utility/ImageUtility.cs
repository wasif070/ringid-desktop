﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

namespace Call.Utility
{
    class ImageUtility
    {
        public static BitmapImage GetDetaultProfileImageWithName(string shortName, System.Drawing.Color colorPicker, int width, int height, int fontSize)
        {
            if (string.IsNullOrEmpty(shortName)) shortName = "U";
            try
            {
                using (System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(width, height))
                {
                    for (int x = 0; x < bitmap.Height; ++x)
                        for (int y = 0; y < bitmap.Width; ++y)
                            bitmap.SetPixel(x, y, colorPicker);

                    Font font = new Font("Segoe UI", fontSize, System.Drawing.FontStyle.Regular, GraphicsUnit.Pixel);
                    System.Drawing.Point atpoint = new System.Drawing.Point(bitmap.Width / 2, bitmap.Height / 2);
                    SolidBrush brush = new SolidBrush(System.Drawing.Color.White);
                    StringFormat sf = new StringFormat();
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Center;
                    using (Graphics graphics = Graphics.FromImage(bitmap))
                    {
                        graphics.DrawString(shortName, font, brush, atpoint, sf);
                        graphics.Dispose();
                        using (MemoryStream ms = new MemoryStream())
                        {
                            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                            BitmapImage image = new BitmapImage();
                            image.BeginInit();
                            image.CacheOption = BitmapCacheOption.OnLoad;
                            ms.Seek(0, SeekOrigin.Begin);
                            image.StreamSource = ms;
                            image.EndInit();
                            return image;
                        }
                    }
                }
            }
            catch (Exception) { }
            return null;
        }
    }
}

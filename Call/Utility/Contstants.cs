﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Call.Utility
{
    public class Contstants
    {
        public static int LastNetWorkStrengthType = 202;
        public static long LAST_RTP_SENT_TIME;
        public static bool IN_CALL = false;
        private static DateTime Jan1st1970Utc = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static long CurrentTimeMillis()
        {
            return (long)((DateTime.UtcNow - Jan1st1970Utc).TotalMilliseconds);
        }

        public class FrameSettings
        {
            public const int FRAME_WIDTH_MID = 320;
            public const int FRAME_HEIGHT_MID = 240;

            public const int FRAME_WIDTH_MAX = 640;
            public const int FRAME_HEIGHT_MAX = 480;

            public static int FRAME_WIDTH = FRAME_WIDTH_MAX;
            public static int FRAME_HEIGHT = FRAME_HEIGHT_MAX;
        }

        public static void SetMaxVideoQuality()
        {
            FrameSettings.FRAME_WIDTH = FrameSettings.FRAME_WIDTH_MAX;
            FrameSettings.FRAME_HEIGHT = FrameSettings.FRAME_HEIGHT_MAX;
        }

        public static void SetMinimumVideoQuality()
        {
            FrameSettings.FRAME_WIDTH = FrameSettings.FRAME_WIDTH_MID;
            FrameSettings.FRAME_HEIGHT = FrameSettings.FRAME_HEIGHT_MID;
        }
    }
}

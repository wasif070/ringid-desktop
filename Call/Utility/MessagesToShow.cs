﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Call.Utility
{
    class MessagesToShow
    {
        private static readonly string are_you_sure = "Are you sure ";
        public static readonly string VIDEO_CALL_NOTIFICATION = "Video Calling is currently not supported on Desktop. This feature will be available soon.";
        public static readonly string ROUTE_CALL_NOTIFICATION = "Making free calls to landlines and mobile numbers is currently not supported on Desktop. This feature will be available soon.";
        public static readonly string SMS_NOTIFICATION = "Sending free SMS is currently not supported on Desktop. This feature will be available soon.";
        public static readonly string RING_MAIL_NOTIFICATION = "Secure Ring Mail is currently not supported on Desktop. This feature will be available soon.";
        public static readonly string FRIEND_ACCESS_NOTIFICAITON = "You have a request of changing friend information access permission to '{0}'. " + are_you_sure + "you want to accept?";
        //   public static readonly string INTERNET_UNAVAILABLE = "Internet unavailable";
        public static readonly string INTERNET_UNAVAILABLE = "No internet connection!";
        public static readonly string INVALID_SESSION = "Invalid Session";
        public static readonly string CAN_NOT_PROCESS = "Can not process";
        public static readonly string ACCESS_DENIED = "Permission Denied";
        public static readonly string TEXT_OFFLINE = "Offline";
        public static readonly string TEXT_FRIEND_AWAY = "Friend is Away!";
        public static readonly string TEXT_FRIEND_OFFLINE = "User Offline!";
        public static readonly string TEXT_DO_NOT_DISTURB = "Do Not Disturb!";
        public static readonly string TEXT_NO_BINDING_PORT = "No voice binding port";
        public static readonly string TEXT_CALL_FAILED = "Can not call right now";
        public static readonly string TEXT_CALLING = "Calling...";
        public static readonly string TEXT_INCOMING_CALL = "Incoming call";
        public static readonly string TEXT_INCOMING_VIDEO_CALL = "Incoming video call";
        public static readonly string TEXT_RINGING = "Ringing...";
        public static readonly string TEXT_AUTHENTICATING = "Authenticating...";
        public static readonly string TEXT_SENDING_PUSH = "Sending push";
        public static readonly string TEXT_CONNECTING = "Connecting...";
        public static readonly string TEXT_DIVERTED_CALL = "Diverted call";
        public static readonly string TEXT_USER_BUSY = "User Busy";
        public static readonly string TEXT_USER_IN_CALL = "User in Call";
        public static readonly string TEXT_NO_ANSWER = "No Answer";
        public static readonly string TEXT_NO_Response = "No Response";
        public static readonly string TEXT_USER_UNREACHABLE = "User currently unreachable";
        public static readonly string TEXT_CANCELED = "Cancelled";
        public static readonly string TEXT_CALL_END = "Call Ended";
        public static readonly string TEXT_CALL_DROPPED = "Call Droped";
        public static readonly string TEXT_CALL_MUTED = "Call Muted";
        public static readonly string TEXT_CALL_FAILED_REASON = "Call Failed";
        public static readonly string TEXT_CALL_ESTABLISHED = "Call Established";
        public static readonly string TEXT_AUDIO_PROBLEM = "Audio device problem";
        public static readonly string TEXT_MICROPHONE_PROBLEM = "Microphone problem";
        public static readonly string TEXT_AUDIO_PROBLEM_FRIEND = "Contact have audio device problem";
        public static readonly string TEXT_USER_LOGGED_OUT = "User logged out";
    }
}

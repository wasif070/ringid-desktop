﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using AForge.Video;
using AForge.Video.DirectShow;
using log4net;

namespace Call.Utility
{
    public class WebcamRGBData
    {
        #region "Private Fields"
        private FilterInfoCollection videosources = null;
        VideoCaptureDevice videoSource;
        private static readonly ILog logger = LogManager.GetLogger(typeof(WebcamRGBData).Name);
        public event Call.Utility.CallDelegates.DelegateByte OnRGBDataFound;
        #endregion "Private Fields"

        #region "Public Fields"
        #endregion "Public Fields"

        #region "Constructors"
        public WebcamRGBData()
        {
        }
        #endregion "Constructors"

        #region "Properties"

        #endregion "Properties"

        #region "Event Trigger"
        //Bitmap bitmap = null;
        private void videoSource_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            try
            {
                Bitmap bitmap = (Bitmap)eventArgs.Frame.Clone();
                bitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);
                byte[] rgbbyte = BmpToArray(bitmap);
                OnRGBDataFound(rgbbyte);
            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace);
            }
        }

        #endregion "Event Trigger"

        #region "Private methods"
        [DllImport("gdi32.dll")]
        private static extern bool DeleteObject(IntPtr hObject);

        public byte[] BmpToArray(Bitmap value)
        {
            BitmapData data = value.LockBits(new Rectangle(0, 0, value.Width, value.Height), ImageLockMode.ReadOnly, value.PixelFormat);
            try
            {
                IntPtr ptr = data.Scan0;
                int bytes = Math.Abs(data.Stride) * value.Height;
                byte[] rgbValues = new byte[bytes];
                Marshal.Copy(ptr, rgbValues, 0, bytes);
                return rgbValues;
            }
            finally
            {
                value.UnlockBits(data);
            }
        }
        #endregion "Private methods"

        #region "Public Methods"

        public static bool IsVideoDeviceExists()
        {
            FilterInfoCollection sourcesses = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            if (sourcesses.Count > 0)
                return true;
            return false;
        }

        public bool StartWebCam()
        {
            if (videoSource == null || !videoSource.IsRunning)
            {
                videosources = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                if (videosources.Count > 0)
                {
                    videoSource = new VideoCaptureDevice(videosources[0].MonikerString);
                    try
                    {
                        if (videoSource.VideoCapabilities.Length > 0)
                        {
                            string highestSolution = "0;0";
                            for (int i = 0; i < videoSource.VideoCapabilities.Length; i++)
                            {
                                if (videoSource.VideoCapabilities[i].FrameSize.Width == Contstants.FrameSettings.FRAME_WIDTH && videoSource.VideoCapabilities[i].FrameSize.Height == Contstants.FrameSettings.FRAME_HEIGHT)
                                {
                                    highestSolution = videoSource.VideoCapabilities[i].FrameSize.Width.ToString() + ";" + i.ToString();
                                    break;
                                }
                            }
                            videoSource.VideoResolution = videoSource.VideoCapabilities[Convert.ToInt32(highestSolution.Split(';')[1])];
                        }
                    }
                    finally { }
                    videoSource.NewFrame += new NewFrameEventHandler(videoSource_NewFrame);
                    videoSource.Start();
                    return true;
                }
            }
            return false;
        }

        public void StopWebCam()
        {
            try
            {
                if (videoSource != null && videoSource.IsRunning)
                {
                    videoSource.NewFrame -= new NewFrameEventHandler(videoSource_NewFrame);
                    if (videoSource != null)
                        videoSource.Stop();
                    videoSource = null;
                }
            }
            finally { }
        }

        public void RestartWebCam()
        {
            try
            {
                StopWebCam();
                StartWebCam();
            }
            finally { }
        }

        public bool IsRunning
        {
            get
            {
                if (videoSource != null && videoSource.IsRunning)
                { return videoSource.IsRunning; }
                return false;
            }
        }
        #endregion "Public methods"
    }
}

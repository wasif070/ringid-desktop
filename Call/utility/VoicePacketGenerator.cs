﻿
namespace Call.utility
{
    //public class VoicePacketGenerator
    //{
    //    #region "Private Fields"
    //    private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(VoicePacketGenerator).Name);
    //    #endregion

    //    #region "Private Methods"
    //    private static int addUserIDInArray(long userIdentity, byte[] data, int i)
    //    {
    //        data[i++] = (byte)(userIdentity >> 56);
    //        data[i++] = (byte)(userIdentity >> 48);
    //        data[i++] = (byte)(userIdentity >> 40);
    //        data[i++] = (byte)(userIdentity >> 32);
    //        data[i++] = (byte)(userIdentity >> 24);
    //        data[i++] = (byte)(userIdentity >> 16);
    //        data[i++] = (byte)(userIdentity >> 8);
    //        data[i++] = (byte)(userIdentity);
    //        return i;
    //    }
    //    private static int getIntegerFromByte(int stratPoint, byte[] bytes)
    //    {
    //        int result = 0;
    //        result += (bytes[stratPoint++] & 0xFF) << 24;
    //        result += (bytes[stratPoint++] & 0xFF) << 16;
    //        result += (bytes[stratPoint++] & 0xFF) << 8;
    //        result += (bytes[stratPoint++] & 0xFF);
    //        return result;
    //    }
    //    private static long getLong(byte[] data, int startPoint, int Length)
    //    {
    //        long result = 0;
    //        for (int i = (Length - 1); i > -1; i--)
    //        {
    //            result |= (data[startPoint++] & 0xFFL) << (8 * i);
    //        }
    //        return result;
    //    }
    //    private static void ShowErrorLog(string stackTrace)
    //    {
    //        logger.Error(stackTrace);
    //    }
    //    #endregion

    //    #region "Make Packet"
    //    public static byte[] makeRegisterPacket(int packetType, long userIdentity, long friendIdentity, string packetID, int calltype)
    //    {
    //        byte[] packetIdByte = Encoding.UTF8.GetBytes(packetID);
    //        int totalDataLenght = 19 + packetIdByte.Length;
    //        byte[] data = new byte[totalDataLenght];
    //        int i = 0;
    //        data[i++] = (byte)packetType;
    //        i = addUserIDInArray(userIdentity, data, i);
    //        i = addUserIDInArray(friendIdentity, data, i);
    //        data[i++] = (byte)packetIdByte.Length;
    //        for (int n = 0; n < packetIdByte.Length; n++)
    //        {
    //            data[i++] = packetIdByte[n];
    //        }
    //        data[i++] = (byte)calltype;
    //        return data;
    //    }

    //    public static byte[] makeUnRegisterPacket(int packetType, long userIdentity)
    //    {
    //        byte[] data = new byte[9];
    //        int i = 0;
    //        data[i++] = (byte)packetType;
    //        i = addUserIDInArray(userIdentity, data, i);

    //        return data;
    //    }

    //    public static byte[] makeSignalingPacket(int packetType, string packetID, long userIdentity, long friendIdentity)
    //    {
    //        byte[] packetIDByte = Encoding.UTF8.GetBytes(packetID);
    //        int totalDataLenght = 18 + packetIDByte.Length;
    //        byte[] data = new byte[totalDataLenght];
    //        int i = 0;
    //        data[i++] = (byte)packetType;
    //        i = addUserIDInArray(userIdentity, data, i);
    //        i = addUserIDInArray(friendIdentity, data, i);
    //        data[i++] = (byte)packetIDByte.Length;
    //        for (int n = 0; n < packetIDByte.Length; n++)
    //        {
    //            data[i++] = packetIDByte[n];
    //        }
    //        return data;
    //    }

    //    public static byte[] makeSignalingPacket(int packetType, string packetID, long userIdentity, long friendIdentity, int callType)
    //    {
    //        byte[] packetIDByte = Encoding.UTF8.GetBytes(packetID);
    //        int totalDataLenght = 19 + packetIDByte.Length;
    //        byte[] data = new byte[totalDataLenght];
    //        int i = 0;
    //        data[i++] = (byte)packetType;
    //        i = addUserIDInArray(userIdentity, data, i);
    //        i = addUserIDInArray(friendIdentity, data, i);
    //        data[i++] = (byte)packetIDByte.Length;
    //        for (int n = 0; n < packetIDByte.Length; n++)
    //        {
    //            data[i++] = packetIDByte[n];
    //        }
    //        data[i++] = (byte)callType;
    //        return data;
    //    }
    //    public static byte[] makeTransferSignalingPacket(int packetType, String packetID, long userIdentity, long friendIdentity, long transferuserId)
    //    {
    //        byte[] packetIDByte = Encoding.UTF8.GetBytes(packetID);
    //        int totalDataLenght = 26 + packetIDByte.Length;
    //        byte[] data = new byte[totalDataLenght];
    //        int i = 0;
    //        data[i++] = (byte)packetType;

    //        addUserIDInArray(userIdentity, data, i);
    //        addUserIDInArray(friendIdentity, data, i);
    //        data[i++] = (byte)packetIDByte.Length;
    //        for (int n = 0; n < packetIDByte.Length; n++)
    //        {
    //            data[i++] = packetIDByte[n];
    //        }
    //        addUserIDInArray(transferuserId, data, i);
    //        return data;
    //    }

    //    public static byte[] makeTransferConfirmationOrConnectedSignalings(int packetType, String packetID, long userIdentity, long friendIdentityOrTransferID)
    //    {
    //        byte[] packetIDByte = Encoding.UTF8.GetBytes(packetID);
    //        int totalDataLenght = 18 + packetIDByte.Length;
    //        byte[] data = new byte[totalDataLenght];
    //        int i = 0;
    //        data[i++] = (byte)packetType;
    //        data[i++] = (byte)packetIDByte.Length;
    //        for (int n = 0; n < packetIDByte.Length; n++)
    //        {
    //            data[i++] = packetIDByte[n];
    //        }
    //        addUserIDInArray(userIdentity, data, i);
    //        addUserIDInArray(friendIdentityOrTransferID, data, i);
    //        return data;
    //    }

    //    public static byte[] makeTransferTransferBsyPacketWithMsg(int packetType, String packetID, long userIdentity, long friendIdentityOrTransferID, String msg)
    //    {
    //        //Packet Type+PacketID Length+Packet ID+User Identity Length+User Idenity+Friend Identity Length+Friend Idenity+Message Length+Message
    //        byte[] packetIDByte = Encoding.UTF8.GetBytes(packetID);
    //        byte[] msgByte = Encoding.UTF8.GetBytes(msg);
    //        //msg.getBytes();

    //        int totalDataLenght = 19 + packetIDByte.Length + msgByte.Length;
    //        byte[] data = new byte[totalDataLenght];
    //        int i = 0;
    //        data[i++] = (byte)packetType;
    //        data[i++] = (byte)packetIDByte.Length;
    //        for (int n = 0; n < packetIDByte.Length; n++)
    //        {
    //            data[i++] = packetIDByte[n];
    //        }

    //        addUserIDInArray(userIdentity, data, i);
    //        addUserIDInArray(friendIdentityOrTransferID, data, i);
    //        data[i++] = (byte)msgByte.Length;
    //        for (int n = 0; n < msgByte.Length; n++)
    //        {
    //            data[i++] = msgByte[n];
    //        }
    //        return data;
    //    }
    //    public static byte[] MakeVideoEndSignal(int packetType, string packetID, long userIdentity, long friendIdentity, int endType)
    //    {
    //        //PACKET_TYPE + USER_IDENTITY + FRIEND_IDENTITY + PACKETID_LENGTH + PACKET_ID
    //        byte[] packetIDByte = Encoding.UTF8.GetBytes(packetID);
    //        int totalDataLenght = 19 + packetIDByte.Length;
    //        byte[] data = new byte[totalDataLenght];
    //        int i = 0;
    //        data[i++] = (byte)packetType;
    //        i = addUserIDInArray(userIdentity, data, i);
    //        i = addUserIDInArray(friendIdentity, data, i);
    //        data[i++] = (byte)packetIDByte.Length;
    //        for (int n = 0; n < packetIDByte.Length; n++)
    //        {
    //            data[i++] = packetIDByte[n];
    //        }
    //        data[i++] = (byte)endType;
    //        return data;
    //    }
    //    public static byte[] makePushPacket(string packetID, long userIdentity, string fullName, long friendIdentity, int platform, int onlineStatus, int appType, string frinedDeviceToken,
    //        int calltype = 1, int isDivertedCall = 0, long transferedUserID = 0, int isP2PEnabled = 0, int remotePushType = 1)
    //    {
    //        byte[] packetIDByte = Encoding.UTF8.GetBytes(packetID);
    //        byte[] fullNameByte = Encoding.UTF8.GetBytes(fullName);//fullName.getBytes();
    //        byte[] frinedDeviceTokenByte = Encoding.UTF8.GetBytes(frinedDeviceToken);// frinedDeviceToken.getBytes();
    //        int deviceTokenLength = frinedDeviceTokenByte.Length;
    //        int fullNameLength = fullNameByte.Length;

    //        int totalDataLenght = 25 + packetIDByte.Length + fullNameLength + deviceTokenLength;
    //        byte[] data = new byte[totalDataLenght];
    //        try
    //        {
    //            int i = 0;
    //            data[i++] = (byte)VoiceConstants.CALL_STATE.VOICE_REGISTER_PUSH;

    //            i = addUserIDInArray(userIdentity, data, i);
    //            i = addUserIDInArray(friendIdentity, data, i);
    //            data[i++] = (byte)packetIDByte.Length;
    //            for (int n = 0; n < packetIDByte.Length; n++)
    //            {
    //                data[i++] = packetIDByte[n];
    //            }

    //            data[i++] = (byte)fullNameLength;
    //            for (int n = 0; n < fullNameLength; n++)
    //            {
    //                data[i++] = fullNameByte[n];
    //            }

    //            data[i++] = (byte)platform;
    //            data[i++] = (byte)onlineStatus;
    //            data[i++] = (byte)appType;
    //            data[i++] = (byte)(deviceTokenLength >> 8);
    //            data[i++] = (byte)(deviceTokenLength);
    //            try
    //            {
    //                for (int n = 0; n < deviceTokenLength; n++)
    //                {
    //                    data[i++] = frinedDeviceTokenByte[n];
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //                logger.Error("makePushPacket==>" + ex.Message + " ==>" + ex.Message + "\n" + ex.StackTrace);
    //            }
    //            data[i++] = (byte)calltype;
    //            //newly added
    //            data[i++] = (byte)isDivertedCall;
    //            i = addUserIDInArray(transferedUserID, data, i);
    //            data[i++] = (byte)isP2PEnabled;
    //            data[i++] = (byte)remotePushType;
    //        }
    //        catch (Exception ex)
    //        {
    //            ShowErrorLog(ex.Message + "\n" + ex.StackTrace);
    //        }
    //        return data;
    //    }

    //    public static byte[] makeBusyMessage(String packetID, long userIdentity, long friendIdentity, String msg, long messageDate)
    //    { /* Busy Signal Packet with Specific Message Text(VOICE_BUSY_MESSAGE): Packet Type+PacketID Length+Packet ID+User Identity Length+User Idenity+Friend Identity Length+Friend Idenity+Message Length+Message   Packet Type(1 byte) +PacketID Length(1 byte) +Packet ID (1 byte) +User Identity Length(1 byte) +Freind id Length (1 byte) +Message legth (1 byte)+ message date 8 byte  */

    //        byte[] packetIDByte = Encoding.UTF8.GetBytes(packetID);
    //        //Packet Type+User Idenity+Friend Idenity+PacketID Length+Packet ID+Message Length+Message + message date 8 byte
    //        byte[] messageByte = Encoding.UTF8.GetBytes(msg);
    //        int totalDataLenght = 27 + packetIDByte.Length + messageByte.Length;
    //        byte[] data = new byte[totalDataLenght];
    //        int i = 0;
    //        data[i++] = (byte)VoiceConstants.CALL_STATE.VOICE_BUSY_MESSAGE;

    //        try
    //        {
    //            i = addUserIDInArray(userIdentity, data, i);
    //            i = addUserIDInArray(friendIdentity, data, i);
    //            data[i++] = (byte)packetIDByte.Length;
    //            for (int n = 0; n < packetIDByte.Length; n++)
    //            {
    //                data[i++] = packetIDByte[n];
    //            }
    //            data[i++] = (byte)(messageByte.Length);
    //            for (int n = 0; n < messageByte.Length; n++)
    //            {
    //                data[i++] = messageByte[n];
    //            }
    //            i = addUserIDInArray(messageDate, data, i);
    //        }
    //        catch (Exception ex)
    //        {
    //            ShowErrorLog(ex.Message + "\n" + ex.StackTrace);
    //        }
    //        return data;
    //    }


    //    #endregion "Male Packet"

    //    #region "Get VoiceMessagDTO"
    //    public static VoiceMessageDTO getRegisterConfirmationPacket(byte[] receivedBuffer)
    //    {
    //        //Packet Type + Friend Idenity + PacketID Length+Packet ID + Voice Communication Port + Video Communication Port
    //        try
    //        {
    //            VoiceMessageDTO messageDTO = new VoiceMessageDTO();
    //            messageDTO = new VoiceMessageDTO();
    //            int totalRead = 0;
    //            int packetType = receivedBuffer[0];
    //            totalRead++;

    //            long friendIdentity = getLong(receivedBuffer, totalRead, 8);
    //            totalRead += 8;

    //            int packetIDLength = receivedBuffer[totalRead];
    //            totalRead++;
    //            //   string packetID = new String(receivedBuffer, totalRead, packetIDLength); E
    //            string packetID = Encoding.UTF8.GetString(receivedBuffer, totalRead, packetIDLength);
    //            totalRead += packetIDLength;

    //            int bindingPort = getIntegerFromByte(totalRead, receivedBuffer);
    //            totalRead += 4;

    //            int vedioComunicationPort = (int)getIntegerFromByte(totalRead, receivedBuffer);
    //            totalRead += 4;
    //            messageDTO.PacketType = packetType;
    //            messageDTO.PacketID = packetID;
    //            messageDTO.VoiceBindingPort = bindingPort;
    //            messageDTO.FriendTableID = friendIdentity;
    //            messageDTO.VideoCommunicationPort = vedioComunicationPort;
    //            return messageDTO;
    //        }
    //        catch (Exception ex)
    //        {
    //            ShowErrorLog(ex.Message + "\n" + ex.StackTrace);
    //        }
    //        return null;
    //    }
    //    public static VoiceMessageDTO getSignalingPacket(byte[] recievedBuffer)
    //    {

    //        try
    //        {
    //            VoiceMessageDTO messageDTO = new VoiceMessageDTO();
    //            int totalRead = 0;
    //            int packetType = recievedBuffer[0];
    //            totalRead++;

    //            long userIdentity = getLong(recievedBuffer, totalRead, 8);
    //            totalRead += 8;

    //            long friendIdentity = getLong(recievedBuffer, totalRead, 8);
    //            totalRead += 8;

    //            int packetIDLength = recievedBuffer[totalRead];
    //            totalRead++;
    //            string packetID = Encoding.UTF8.GetString(recievedBuffer, totalRead, packetIDLength);
    //            totalRead += packetIDLength;

    //            messageDTO.PacketType = packetType;
    //            messageDTO.UserTableID = friendIdentity;
    //            messageDTO.FriendTableID = userIdentity;
    //            messageDTO.PacketID = packetID;
    //            return messageDTO;
    //        }
    //        catch (Exception ex)
    //        {
    //            ShowErrorLog(ex.Message + "\n" + ex.StackTrace);
    //        }
    //        return null;
    //    }
    //    public static VoiceMessageDTO getPortFormSignal(byte[] receivedBuffer)
    //    {
    //        //PACKET_TYPE + FRIEND_IDENTITY + PACKETID_LENGTH + PACKET_ID + VIDEO_COMMUNICATION_PORT
    //        try
    //        {
    //            VoiceMessageDTO messageDTO = new VoiceMessageDTO();
    //            int totalRead = 0;
    //            int packetType = receivedBuffer[0];
    //            totalRead++;

    //            long friendIdentity = getLong(receivedBuffer, totalRead, 8);
    //            totalRead += 8;

    //            int packetIDLength = receivedBuffer[totalRead];
    //            totalRead++;
    //            //   string packetID = new String(receivedBuffer, totalRead, packetIDLength); E
    //            string packetID = Encoding.UTF8.GetString(receivedBuffer, totalRead, packetIDLength);
    //            totalRead += packetIDLength;

    //            int vedioComunicationPort = (int)getIntegerFromByte(totalRead, receivedBuffer);
    //            totalRead += 4;
    //            messageDTO.PacketType = packetType;
    //            messageDTO.FriendTableID = friendIdentity;
    //            messageDTO.PacketID = packetID;
    //            messageDTO.VideoCommunicationPort = vedioComunicationPort;
    //            return messageDTO;
    //        }
    //        catch (Exception ex)
    //        {
    //            ShowErrorLog(ex.Message + "\n" + ex.StackTrace);
    //        }
    //        return null;
    //    }
    //    public static VoiceMessageDTO getSignalingPacketVideoEnd(byte[] recievedBuffer)
    //    {

    //        try
    //        {
    //            VoiceMessageDTO messageDTO = new VoiceMessageDTO();
    //            int totalRead = 0;
    //            int packetType = recievedBuffer[0];
    //            totalRead++;

    //            long userIdentity = getLong(recievedBuffer, totalRead, 8);
    //            totalRead += 8;

    //            long friendIdentity = getLong(recievedBuffer, totalRead, 8);
    //            totalRead += 8;

    //            int packetIDLength = recievedBuffer[totalRead];
    //            totalRead++;
    //            string packetID = Encoding.UTF8.GetString(recievedBuffer, totalRead, packetIDLength);
    //            totalRead += packetIDLength;

    //            int endType = VoiceConstants.END_VIDEO_CALL_ONE_SIDE;
    //            if (recievedBuffer.Length > totalRead)
    //            {
    //                endType = recievedBuffer[totalRead];
    //                totalRead++;
    //            }

    //            messageDTO.PacketType = packetType;
    //            messageDTO.UserTableID = friendIdentity;
    //            messageDTO.FriendTableID = userIdentity;
    //            messageDTO.PacketID = packetID;

    //            messageDTO.VideoCallEndType = endType;
    //            return messageDTO;
    //        }
    //        catch (Exception ex)
    //        {
    //            ShowErrorLog(ex.Message + "\n" + ex.StackTrace);
    //        }
    //        return null;
    //    }
    //    public static VoiceMessageDTO getSignalingPacketAnwser(byte[] recievedBuffer)
    //    {

    //        try
    //        {
    //            VoiceMessageDTO messageDTO = new VoiceMessageDTO();
    //            int totalRead = 0;
    //            int packetType = recievedBuffer[0];
    //            totalRead++;
    //            long userIdentity = getLong(recievedBuffer, totalRead, 8);
    //            totalRead += 8;
    //            long friendIdentity = getLong(recievedBuffer, totalRead, 8);
    //            totalRead += 8;
    //            int packetIDLength = recievedBuffer[totalRead];
    //            totalRead++;
    //            string packetID = Encoding.UTF8.GetString(recievedBuffer, totalRead, packetIDLength);
    //            totalRead += packetIDLength;
    //            int callType = VoiceConstants.END_VIDEO_CALL_ONE_SIDE;
    //            if (recievedBuffer.Length > totalRead)
    //            {
    //                callType = recievedBuffer[totalRead];
    //            }

    //            totalRead++;

    //            messageDTO.PacketType = packetType;
    //            messageDTO.PacketID = packetID;
    //            messageDTO.UserTableID = friendIdentity;
    //            messageDTO.FriendTableID = userIdentity;
    //            messageDTO.CallType = callType;
    //            return messageDTO;
    //        }
    //        catch (Exception ex)
    //        {
    //            ShowErrorLog(ex.Message + "\n" + ex.StackTrace);
    //        }
    //        return null;
    //    }
    //    public static VoiceMessageDTO getIncallPacket(byte[] recievedBuffer)
    //    {

    //        try
    //        {
    //            VoiceMessageDTO messageDTO = new VoiceMessageDTO();
    //            int totalRead = 0;
    //            int packetType = recievedBuffer[0];
    //            totalRead++;

    //            long userIdentity = getLong(recievedBuffer, totalRead, 8);
    //            totalRead += 8;

    //            long friendIdentity = getLong(recievedBuffer, totalRead, 8);
    //            totalRead += 8;

    //            int packetIDLength = recievedBuffer[totalRead];
    //            totalRead++;
    //            string packetID = Encoding.UTF8.GetString(recievedBuffer, totalRead, packetIDLength);// new String(recievedBuffer, totalRead, packetIDLength);
    //            totalRead += packetIDLength;

    //            messageDTO.PacketType = packetType;
    //            messageDTO.PacketID = packetID;
    //            messageDTO.UserTableID = friendIdentity;
    //            messageDTO.FriendTableID = userIdentity;
    //            return messageDTO;
    //        }
    //        catch (Exception ex)
    //        {
    //            ShowErrorLog(ex.Message + "\n" + ex.StackTrace);
    //        }
    //        return null;
    //    }
    //    public static VoiceMessageDTO getVoiceBusyMsg(byte[] recievedBuffer)
    //    {
    //        //Packet Type+User Idenity+Friend Idenity+PacketID Length+Packet ID+Message Length+Message


    //        try
    //        {
    //            VoiceMessageDTO messageDTO = new VoiceMessageDTO();
    //            int totalRead = 0;
    //            int packetType = recievedBuffer[0];
    //            totalRead++;

    //            long userIdentity = getLong(recievedBuffer, totalRead, 8);
    //            totalRead += 8;

    //            long friendIdentity = getLong(recievedBuffer, totalRead, 8);
    //            totalRead += 8;

    //            int packetIDLength = recievedBuffer[totalRead];
    //            totalRead++;
    //            string packetID = Encoding.UTF8.GetString(recievedBuffer, totalRead, packetIDLength);
    //            //new String(recievedBuffer, totalRead, packetIDLength);
    //            totalRead += packetIDLength;

    //            int pushMsgLength = recievedBuffer[totalRead];
    //            totalRead++;
    //            string pushMessage = Encoding.UTF8.GetString(recievedBuffer, totalRead, pushMsgLength);//new String(recievedBuffer, totalRead, pushMsgLength);
    //            totalRead += pushMsgLength;
    //            long messageDate = 0;
    //            if (recievedBuffer.Length > totalRead)
    //            {
    //                try
    //                {
    //                    messageDate = getLong(recievedBuffer, totalRead, 8);
    //                    totalRead += 8;
    //                }
    //                catch (Exception)
    //                {

    //                }
    //            }

    //            messageDTO.PacketType = packetType;
    //            messageDTO.PacketID = packetID;
    //            messageDTO.UserTableID = friendIdentity;
    //            messageDTO.FriendTableID = userIdentity;
    //            messageDTO.VoiceBusyMessage = pushMessage;
    //            messageDTO.MessageDate = messageDate;
    //            return messageDTO;
    //        }
    //        catch (Exception ex)
    //        {
    //            ShowErrorLog(ex.Message + "\n" + ex.StackTrace);
    //        }
    //        return null;
    //    }
    //    public static VoiceMessageDTO getPushConfirmationPacket(byte[] recievedBuffer)
    //    {

    //        try
    //        {
    //            VoiceMessageDTO messageDTO = new VoiceMessageDTO();
    //            int totalRead = 0;
    //            int packetType = recievedBuffer[0];
    //            totalRead++;

    //            long friendIdentity = getLong(recievedBuffer, totalRead, 8);
    //            totalRead += 8;

    //            int packetIDLength = recievedBuffer[totalRead];
    //            totalRead++;
    //            string packetID = Encoding.UTF8.GetString(recievedBuffer, totalRead, packetIDLength);
    //            totalRead += packetIDLength;
    //            messageDTO.PacketType = packetType;
    //            messageDTO.PacketID = packetID;
    //            messageDTO.FriendTableID = friendIdentity;
    //            return messageDTO;
    //        }
    //        catch (Exception ex)
    //        {
    //            ShowErrorLog(ex.Message + "\n" + ex.StackTrace);
    //        }
    //        return null;
    //    }
    //    #endregion "Get VoiceMessagDTO"
    //}
}
